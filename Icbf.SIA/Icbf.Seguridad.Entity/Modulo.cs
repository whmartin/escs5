using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    public class Modulo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdModulo
        {
            get;
            set;
        }
        public String NombreModulo
        {
            get;
            set;
        }
        public int Posicion
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCreacion
        {
            get;
            set;
        }
        public String UsuarioModificacion
        {
            get;
            set;
        }
        public DateTime FechaCreacion
        {
            get;
            set;
        }
        public DateTime FechaModificacion
        {
            get;
            set;
        }
        public Modulo()
        {
        }
    }
}
