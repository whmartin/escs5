﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    public class Auditoria
    {
        public DateTime Fecha
        {
            set;
            get;
        }
        public String NombreUsuario
        {
            set;
            get;
        }
        public String Operacion
        {
            set;
            get;
        }
        public String ParametrosOperacion
        {
            set;
            get;
        }
        public String Tabla
        {
            set;
            get;
        }
        public String DireccionIp
        {
            set;
            get;
        }
        public String Navegador
        {
            set;
            get;
        }

        public Auditoria()
        {
        }
    }
}
