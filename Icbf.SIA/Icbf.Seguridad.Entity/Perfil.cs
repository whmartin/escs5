using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    public class Perfil : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdPerfil
        {
            get;
            set;
        }
        public String NombrePerfil
        {
            get;
            set;
        }
        public String UsuarioCreacion
        {
            get;
            set;
        }
        public String UsuarioModificacion
        {
            get;
            set;
        }
        public DateTime FechaCreacion
        {
            get;
            set;
        }
        public DateTime FechaModificacion
        {
            get;
            set;
        }
        public Perfil()
        {
        }
    }
}
