﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    public class RolPermiso : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRolPermiso
        {
            set;
            get;
        }
        public int IdPermiso
        {
            set;
            get;
        }
        public String Rol
        {
            get;
            set;
        }
        public RolPermiso()
        {
        }
        public String UsuarioCreacion
        {
            set;
            get;
        }
        public String UsuarioModificacion
        {
            set;
            get;
        }
        public DateTime FechaCreacion
        {
            set;
            get;
        }
        public DateTime FechaModificacion
        {
            set;
            get;
        }
    }
}
