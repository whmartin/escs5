﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    public class Rol : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRol
        {
            set;
            get;
        }
        public string Providerkey
        {
            set;
            get;
        }
        public string NombreRol
        {
            set;
            get;
        }
        public string DescripcionRol
        {
            set;
            get;
        }
        public Boolean Estado
        {
            set;
            get;
        }
        public String UsuarioCreacion
        {
            set;
            get;
        }
        public String UsuarioModificacion
        {
            set;
            get;
        }
        public DateTime FechaCreacion
        {
            set;
            get;
        }
        public DateTime FechaModificacion
        {
            set;
            get;
        }
        
        public List<Permiso> Permisos
        {
            set;
            get;
        }

        public bool EsAdministrador
        {
            set;
            get;
        }
        public Rol()
        {
            Providerkey = "";
            Permisos = new List<Permiso>();
            
        }
    }
}
