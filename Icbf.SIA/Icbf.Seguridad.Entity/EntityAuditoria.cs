﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    [Serializable]
    public abstract class EntityAuditoria
    {
        public int IdRegistro
        {
            get;
            set;
        }
        public string Usuario
        {
            get;
            set;
        }
        public string Programa
        {
            get;
            set;
        }
        public string Operacion
        {
            get;
            set;
        }
        public string ParametrosOperacion
        {
            get;
            set;
        }
        public string Tabla
        {
            get;
            set;
        }
        public string DireccionIP
        {
            get;
            set;
        }
        public string Navegador
        {
            get;
            set;
        }
        public bool ProgramaGeneraLog
        {
            get;
            set;
        }

        public EntityAuditoria()
        {
            IdRegistro = 0;
            Usuario = "";
            Programa = "";
            Operacion = "";
            ParametrosOperacion = "";
            Tabla = "";
            DireccionIP = "";
            Navegador = "";
            ProgramaGeneraLog = false;
        }
    }
}
