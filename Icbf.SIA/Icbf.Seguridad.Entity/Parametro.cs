using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    public class Parametro : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdParametro
        {
            get;
            set;
        }
        public String NombreParametro
        {
            get;
            set;
        }
        public String ValorParametro
        {
            get;
            set;
        }
        public String ImagenParametro
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String Funcionalidad
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Parametro()
        {
        }
    }
}
