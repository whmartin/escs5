﻿//-----------------------------------------------------------------------
// <copyright file="ProgramaFuncion.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncionRol.</summary>
// <author>Ingenian Software</author>
// <date>22/02/2017 0355 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    /// <summary>
    /// Clase Entidad ProgramaFuncionRol con sus atributos para transportar los datos entre las diferentes capas
    /// </summary>
    public class ProgramaFuncionRol : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Clase Entidad Programa con sus atributos para transportar los datos entre las diferentes capas
        /// </summary>
        public ProgramaFuncionRol()
        {
        }

        /// <summary>
        /// Gets or sets IdProgramaFuncionRol.
        /// </summary>
        /// <value>The IdProgramaFuncionRol</value>
        public int IdProgramaFuncionRol
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdPRogramaFuncion.
        /// </summary>
        /// <value>The IdPRogramaFuncion</value>
        public int IdProgramaFuncion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdRol.
        /// </summary>
        /// <value>The IdRol</value>
        public int IdRol
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdPrograma.
        /// </summary>
        /// <value>The IdPrograma</value>
        public int IdPrograma
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombrePrograma.
        /// </summary>
        /// <value>The NombrePrograma</value>
        public string NombrePrograma
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreRol.
        /// </summary>
        /// <value>The NombreRol</value>
        public string NombreRol
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreFuncion.
        /// </summary>
        /// <value>The NombreFuncion</value>
        public string NombreFuncion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Estado.
        /// </summary>
        /// <value>The Estado</value>
        public bool Estado
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioCrea.
        /// </summary>
        /// <value>The UsuarioCrea</value>
        public string UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioModifica.
        /// </summary>
        /// <value>The UsuarioModifica</value>
        public string UsuarioModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaCrea.
        /// </summary>
        /// <value>The FechaCrea</value>
        public DateTime FechaCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaModifica.
        /// </summary>
        /// <value>The FechaModifica</value>
        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}
