using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    public class Programa : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdPrograma
        {
            get;
            set;
        }
        public int IdModulo
        {
            get;
            set;
        }
        public String NombrePrograma
        {
            get;
            set;
        }
        public String CodigoPrograma
        {
            get;
            set;
        }
        public int Posicion
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public Boolean VisibleMenu
        {
            set;
            get;
        }
        public String UsuarioCreacion
        {
            get;
            set;
        }
        public String UsuarioModificacion
        {
            get;
            set;
        }
        public DateTime FechaCreacion
        {
            get;
            set;
        }
        public DateTime FechaModificacion
        {
            get;
            set;
        }
        public Boolean GeneraLog
        {
            set;
            get;
        }

        public String NombreModulo
        {
            set;
            get;
        }
        public int Permiso
        {
            get;
            set;
        }

        public int EsReporte 
        {
            get;
            set;
        }

        public int IdReporte
        {
            get;
            set;
        }

        public Programa()
        {
        }
    }
}
