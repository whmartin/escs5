using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Seguridad.Entity
{
    public class Permiso : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdPermiso
        {
            get;
            set;
        }
        public int IdPrograma
        {
            get;
            set;
        }
        public int IdRol
        {
            get;
            set;
        }
        public Boolean Insertar
        {
            get;
            set;
        }
        public Boolean Modificar
        {
            get;
            set;
        }
        public Boolean Eliminar
        {
            get;
            set;
        }
        public Boolean Consultar
        {
            get;
            set;
        }
        public String UsuarioCreacion
        {
            get;
            set;
        }
        public String UsuarioModificacion
        {
            get;
            set;
        }
        public DateTime FechaCreacion
        {
            get;
            set;
        }
        public DateTime FechaModificacion
        {
            get;
            set;
        }
        

        public String NombrePrograma
        {
            set;
            get;
        }
        public Permiso()
        {
        }
    }
}
