/****** Object:  Trigger [PROVEEDOR].[TG_Aud_InfoFinancieraEntidad]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para InfoFinancieraEntidad
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_InfoFinancieraEntidad]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_InfoFinancieraEntidad]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_InfoFinancieraEntidad]
  ON  [PROVEEDOR].[InfoFinancieraEntidad]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_InfoFinancieraEntidad]
  ([IdInfoFin],[IdEntidad_old], [IdEntidad_new],[IdVigencia_old], [IdVigencia_new],[ActivoCte_old], [ActivoCte_new],[ActivoTotal_old], [ActivoTotal_new],[PasivoCte_old], [PasivoCte_new],[PasivoTotal_old], [PasivoTotal_new],[Patrimonio_old], [Patrimonio_new],[GastosInteresFinancieros_old], [GastosInteresFinancieros_new],[UtilidadOperacional_old], [UtilidadOperacional_new],[ConfirmaIndicadoresFinancieros_old], [ConfirmaIndicadoresFinancieros_new],[RupRenovado_old], [RupRenovado_new],[EstadoValidacion_old], [EstadoValidacion_new],[ObservacionesInformacionFinanciera_old], [ObservacionesInformacionFinanciera_new],[ObservacionesValidadorICBF_old], [ObservacionesValidadorICBF_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[NroRevision_old], [NroRevision_new],[Finalizado_old], [Finalizado_new],[Operacion])
SELECT deleted.IdInfoFin,deleted.IdEntidad,inserted.IdEntidad,deleted.IdVigencia,inserted.IdVigencia,deleted.ActivoCte,inserted.ActivoCte,deleted.ActivoTotal,inserted.ActivoTotal,deleted.PasivoCte,inserted.PasivoCte,deleted.PasivoTotal,inserted.PasivoTotal,deleted.Patrimonio,inserted.Patrimonio,deleted.GastosInteresFinancieros,inserted.GastosInteresFinancieros,deleted.UtilidadOperacional,inserted.UtilidadOperacional,deleted.ConfirmaIndicadoresFinancieros,inserted.ConfirmaIndicadoresFinancieros,deleted.RupRenovado,inserted.RupRenovado,deleted.EstadoValidacion,inserted.EstadoValidacion,deleted.ObservacionesInformacionFinanciera,inserted.ObservacionesInformacionFinanciera,deleted.ObservacionesValidadorICBF,inserted.ObservacionesValidadorICBF,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,deleted.NroRevision,inserted.NroRevision,deleted.Finalizado,inserted.Finalizado,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdInfoFin = deleted.IdInfoFinEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_InfoFinancieraEntidad]
  ([IdInfoFin],[IdEntidad_old],[IdVigencia_old],[ActivoCte_old],[ActivoTotal_old],[PasivoCte_old],[PasivoTotal_old],[Patrimonio_old],[GastosInteresFinancieros_old],[UtilidadOperacional_old],[ConfirmaIndicadoresFinancieros_old],[RupRenovado_old],[EstadoValidacion_old],[ObservacionesInformacionFinanciera_old],[ObservacionesValidadorICBF_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[NroRevision_old],[Finalizado_old],[Operacion])
SELECT deleted.IdInfoFin,deleted.IdEntidad,deleted.IdVigencia,deleted.ActivoCte,deleted.ActivoTotal,deleted.PasivoCte,deleted.PasivoTotal,deleted.Patrimonio,deleted.GastosInteresFinancieros,deleted.UtilidadOperacional,deleted.ConfirmaIndicadoresFinancieros,deleted.RupRenovado,deleted.EstadoValidacion,deleted.ObservacionesInformacionFinanciera,deleted.ObservacionesValidadorICBF,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,deleted.NroRevision,deleted.Finalizado,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_InfoFinancieraEntidad]
  ([IdInfoFin],[IdEntidad_new],[IdVigencia_new],[ActivoCte_new],[ActivoTotal_new],[PasivoCte_new],[PasivoTotal_new],[Patrimonio_new],[GastosInteresFinancieros_new],[UtilidadOperacional_new],[ConfirmaIndicadoresFinancieros_new],[RupRenovado_new],[EstadoValidacion_new],[ObservacionesInformacionFinanciera_new],[ObservacionesValidadorICBF_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[NroRevision_new],[Finalizado_new],[Operacion])
SELECT inserted.IdInfoFin,inserted.IdEntidad,inserted.IdVigencia,inserted.ActivoCte,inserted.ActivoTotal,inserted.PasivoCte,inserted.PasivoTotal,inserted.Patrimonio,inserted.GastosInteresFinancieros,inserted.UtilidadOperacional,inserted.ConfirmaIndicadoresFinancieros,inserted.RupRenovado,inserted.EstadoValidacion,inserted.ObservacionesInformacionFinanciera,inserted.ObservacionesValidadorICBF,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,inserted.NroRevision,inserted.Finalizado,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO