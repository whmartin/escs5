USE [SIA]
GO



IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/ValidarProveedor')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/ValidarProveedor'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
			   ([IdPrograma]
			   ,[IdRol]
			   ,[Insertar]
			   ,[Modificar]
			   ,[Eliminar]
			   ,[Consultar]
			   ,[UsuarioCreacion]
			   ,[FechaCreacion])           
		 VALUES
			   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/ValidarProveedor')
			   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
			   ,1
			   ,1
			   ,0
			   ,1
			   ,'Administrador'
			   ,GETDATE()) 
	END
END      
GO



IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/ValidarTercero')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/ValidarTercero'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
			   ([IdPrograma]
			   ,[IdRol]
			   ,[Insertar]
			   ,[Modificar]
			   ,[Eliminar]
			   ,[Consultar]
			   ,[UsuarioCreacion]
			   ,[FechaCreacion])           
		 VALUES
			   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/ValidarTercero')
			   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
			   ,1
			   ,1
			   ,0
			   ,1
			   ,'Administrador'
			   ,GETDATE()) 
	END
END      
GO


IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/CambiarEstadoTercero')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/CambiarEstadoTercero'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
			   ([IdPrograma]
			   ,[IdRol]
			   ,[Insertar]
			   ,[Modificar]
			   ,[Eliminar]
			   ,[Consultar]
			   ,[UsuarioCreacion]
			   ,[FechaCreacion])           
		 VALUES
			   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/CambiarEstadoTercero')
			   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
			   ,1
			   ,1
			   ,0
			   ,1
			   ,'Administrador'
			   ,GETDATE()) 
	END
END      
GO