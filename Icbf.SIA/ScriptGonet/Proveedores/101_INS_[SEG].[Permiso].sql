USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:      Inserta�permisos en [SEG].[Permiso]
-- =============================================
SET IDENTITY_INSERT [SEG].[Permiso] ON
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2683, 265, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D60100BFEA AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2684, 266, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D60134E5F6 AS DateTime), NULL, NULL)
--INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2685, 268, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D60100BFEA AS DateTime), NULL, NULL)
--INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2686, 269, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D60100BFEA AS DateTime), NULL, NULL)
--INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2687, 270, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D700DB123D AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2688, 271, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D900B6D37D AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2689, 273, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D900E3A9E5 AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2695, 279, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DB00AD0579 AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2696, 280, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DB00D13491 AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2697, 281, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DB00F04D85 AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2698, 282, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DB0103E250 AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2699, 283, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DB010BC5B6 AS DateTime), NULL, NULL)

INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2703, 287, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DC01139889 AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2704, 288, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DC013A4390 AS DateTime), NULL, NULL)
INSERT [SEG].[Permiso] ([IdPermiso], [IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) VALUES (2705, 289, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DC014A3DD8 AS DateTime), NULL, NULL)

SET IDENTITY_INSERT [SEG].[Permiso] OFF