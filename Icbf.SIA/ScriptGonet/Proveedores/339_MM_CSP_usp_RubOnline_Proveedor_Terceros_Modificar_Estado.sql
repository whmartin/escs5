USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Modificar_Estado]    Script Date: 07/23/2013 02:05:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_Modificar_Estado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Modificar_Estado]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Modificar_Estado]    Script Date: 07/23/2013 02:05:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013-07-
-- Description:	Procedimiento almacenado que cambia el estado del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Modificar_Estado]
	@IDTERCERO int ,
	@IDESTADOTERCERO int,
	@USUARIOMODIFICA varchar(100)
AS
BEGIN
	UPDATE 
		Oferente.TERCERO
	SET 
		Oferente.TERCERO.IDESTADOTERCERO = @IDESTADOTERCERO, 
		Oferente.TERCERO.USUARIOMODIFICA = @USUARIOMODIFICA,
		Oferente.TERCERO.FECHAMODIFICA = GETDATE()
	WHERE
		Oferente.TERCERO.IDTERCERO = @IDTERCERO
END

GO

