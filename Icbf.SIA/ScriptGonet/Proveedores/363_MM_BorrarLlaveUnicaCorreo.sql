USE [SIA]
GO

/****** Object:  Index [EmailUnico]    Script Date: 07/24/2013 11:44:19 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[ContactoEntidad]') AND name = N'EmailUnico')
DROP INDEX [EmailUnico] ON [Proveedor].[ContactoEntidad] WITH ( ONLINE = OFF )
GO
