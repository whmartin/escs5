USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]    Script Date: 06/24/2013 12:23:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]    Script Date: 06/24/2013 12:23:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]
	@IdSucursal INT
AS
BEGIN
 SELECT IdSucursal, IdEntidad, Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado,IdZona,Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Sucursal] WHERE  IdSucursal = @IdSucursal
END

GO

