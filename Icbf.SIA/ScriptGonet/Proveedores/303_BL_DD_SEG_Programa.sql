USE [SIA] 
GO
-- =============================================
-- Author:          Juan Carlos Valverde Sámano    
-- Create date:         20/01/2014
-- Description:     Elimina un permiso y programa del CodPrograma Seguridad/Parametro
-- =============================================
/****** Script que elimina el submenu de parametros del menu de seguridad  ******/

IF EXISTS (SELECT 1 FROM [SEG].[Programa] WHERE CodigoPrograma = 'Seguridad/Parametro')
BEGIN

	IF EXISTS (SELECT 1 FROM SEG.Permiso WHERE IdPrograma = (SELECT IdPrograma FROM [SEG].[Programa] WHERE CodigoPrograma = 'Seguridad/Parametro'))
	BEGIN 	
		DELETE SEG.Permiso WHERE IdPrograma = (SELECT IdPrograma FROM [SEG].[Programa] WHERE CodigoPrograma = 'Seguridad/Parametro')
	END

	DELETE [SEG].[Programa] WHERE CodigoPrograma = 'Seguridad/Parametro'

END
GO
  