USE [SIA]
GO

/****** Object:  Table [Proveedor].[TipoDocIdentifica]    Script Date: 06/25/2013 18:20:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocIdentifica]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoDocIdentifica]
GO

USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocIdentifica]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoDocIdentifica]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/19/2013 12:25:26 AM
-- =============================================
CREATE TABLE [Proveedor].[TipoDocIdentifica](
 [IdTipoDocIdentifica] [INT]  IDENTITY(1,1) NOT NULL,
 [CodigoDocIdentifica] [NUMERIC]  NOT NULL,
 [Descripcion] [NVARCHAR] (128) NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_TipoDocIdentifica] PRIMARY KEY CLUSTERED 
(
 	[IdTipoDocIdentifica] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
