USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]    Script Date: 06/14/2013 19:32:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]
	@CodigoTipodeentidadPublica NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeentidadPublica] WHERE CodigoTipodeentidadPublica = CASE WHEN @CodigoTipodeentidadPublica IS NULL THEN CodigoTipodeentidadPublica ELSE @CodigoTipodeentidadPublica END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
GO