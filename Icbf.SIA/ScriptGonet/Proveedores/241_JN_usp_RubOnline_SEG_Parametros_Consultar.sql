USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametros_Consultar]    Script Date: 06/26/2013 14:43:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_Parametros_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_SEG_Parametros_Consultar]
GO


-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que consulta un(a) Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametros_Consultar]
	@NombreParametro NVARCHAR(128) = NULL, @ValorParametro NVARCHAR(256) = NULL, @Estado BIT = NULL,
	@Funcionalidad NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdParametro, NombreParametro, ValorParametro, ImagenParametro, Estado, Funcionalidad, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [SEG].[Parametro] 
 WHERE NombreParametro LIKE CASE WHEN @NombreParametro IS NULL THEN NombreParametro ELSE @NombreParametro + '%' END 
 AND ValorParametro = CASE WHEN @ValorParametro IS NULL THEN ValorParametro ELSE @ValorParametro END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
 AND Funcionalidad = CASE WHEN @Funcionalidad IS NULL THEN Funcionalidad ELSE @Funcionalidad END
END
