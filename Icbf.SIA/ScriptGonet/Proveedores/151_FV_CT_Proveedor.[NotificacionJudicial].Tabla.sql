USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Elimina un CONSTRAINT de la entidad [Proveedor].[NotificacionJudicial]
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_NotificacionJudicial_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]'))
ALTER TABLE [Proveedor].[NotificacionJudicial] DROP CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Elimina la entidad [Proveedor].[NotificacionJudicial]
-- =============================================
/****** Object:  Table [Proveedor].[NotificacionJudicial]    Script Date: 06/24/2013 12:46:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]') AND type in (N'U'))
DROP TABLE [Proveedor].[NotificacionJudicial]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Crea la entidad [Proveedor].[NotificacionJudicial]
-- =============================================
/****** Object:  Table [Proveedor].[NotificacionJudicial]    Script Date: 06/24/2013 12:46:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[NotificacionJudicial](
	[IdNotJudicial] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NULL,
	[IdDepartamento] [int] NULL,
	[IdMunicipio] [int] NULL,
	[IdZona] [int] NULL,
	[Direccion] [nvarchar](250) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_NotificacionJudicial] PRIMARY KEY CLUSTERED 
(
	[IdNotJudicial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[NotificacionJudicial]  WITH CHECK ADD  CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO

ALTER TABLE [Proveedor].[NotificacionJudicial] CHECK CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente]
GO


