/****** Object:  Trigger [PROVEEDOR].[TG_Aud_TipoDocumentoPrograma]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para TipoDocumentoPrograma
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_TipoDocumentoPrograma]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_TipoDocumentoPrograma]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_TipoDocumentoPrograma]
  ON  [PROVEEDOR].[TipoDocumentoPrograma]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_TipoDocumentoPrograma]
  ([IdTipoDocumentoPrograma],[IdTipoDocumento_old], [IdTipoDocumento_new],[IdPrograma_old], [IdPrograma_new],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[MaxPermitidoKB_old], [MaxPermitidoKB_new],[ExtensionesPermitidas_old], [ExtensionesPermitidas_new],[ObligRupNoRenovado_old], [ObligRupNoRenovado_new],[ObligRupRenovado_old], [ObligRupRenovado_new],[ObligPersonaJuridica_old], [ObligPersonaJuridica_new],[ObligPersonaNatural_old], [ObligPersonaNatural_new],[ObligSectorPrivado_old], [ObligSectorPrivado_new],[ObligSectorPublico_old], [ObligSectorPublico_new],[ObligEntNacional_old], [ObligEntNacional_new],[ObligEntExtranjera_old], [ObligEntExtranjera_new],[Operacion])
SELECT deleted.IdTipoDocumentoPrograma,deleted.IdTipoDocumento,inserted.IdTipoDocumento,deleted.IdPrograma,inserted.IdPrograma,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,deleted.MaxPermitidoKB,inserted.MaxPermitidoKB,deleted.ExtensionesPermitidas,inserted.ExtensionesPermitidas,deleted.ObligRupNoRenovado,inserted.ObligRupNoRenovado,deleted.ObligRupRenovado,inserted.ObligRupRenovado,deleted.ObligPersonaJuridica,inserted.ObligPersonaJuridica,deleted.ObligPersonaNatural,inserted.ObligPersonaNatural,deleted.ObligSectorPrivado,inserted.ObligSectorPrivado,deleted.ObligSectorPublico,inserted.ObligSectorPublico,deleted.ObligEntNacional,inserted.ObligEntNacional,deleted.ObligEntExtranjera,inserted.ObligEntExtranjera,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdTipoDocumentoPrograma = deleted.IdTipoDocumentoProgramaEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_TipoDocumentoPrograma]
  ([IdTipoDocumentoPrograma],[IdTipoDocumento_old],[IdPrograma_old],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[MaxPermitidoKB_old],[ExtensionesPermitidas_old],[ObligRupNoRenovado_old],[ObligRupRenovado_old],[ObligPersonaJuridica_old],[ObligPersonaNatural_old],[ObligSectorPrivado_old],[ObligSectorPublico_old],[ObligEntNacional_old],[ObligEntExtranjera_old],[Operacion])
SELECT deleted.IdTipoDocumentoPrograma,deleted.IdTipoDocumento,deleted.IdPrograma,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,deleted.MaxPermitidoKB,deleted.ExtensionesPermitidas,deleted.ObligRupNoRenovado,deleted.ObligRupRenovado,deleted.ObligPersonaJuridica,deleted.ObligPersonaNatural,deleted.ObligSectorPrivado,deleted.ObligSectorPublico,deleted.ObligEntNacional,deleted.ObligEntExtranjera,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_TipoDocumentoPrograma]
  ([IdTipoDocumentoPrograma],[IdTipoDocumento_new],[IdPrograma_new],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[MaxPermitidoKB_new],[ExtensionesPermitidas_new],[ObligRupNoRenovado_new],[ObligRupRenovado_new],[ObligPersonaJuridica_new],[ObligPersonaNatural_new],[ObligSectorPrivado_new],[ObligSectorPublico_new],[ObligEntNacional_new],[ObligEntExtranjera_new],[Operacion])
SELECT inserted.IdTipoDocumentoPrograma,inserted.IdTipoDocumento,inserted.IdPrograma,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,inserted.MaxPermitidoKB,inserted.ExtensionesPermitidas,inserted.ObligRupNoRenovado,inserted.ObligRupRenovado,inserted.ObligPersonaJuridica,inserted.ObligPersonaNatural,inserted.ObligSectorPrivado,inserted.ObligSectorPublico,inserted.ObligEntNacional,inserted.ObligEntExtranjera,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO