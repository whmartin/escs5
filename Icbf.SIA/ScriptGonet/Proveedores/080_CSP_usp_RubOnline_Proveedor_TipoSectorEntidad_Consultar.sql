USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]    Script Date: 06/14/2013 19:32:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]
	@IdTipoSectorEntidad INT
AS
BEGIN
 SELECT IdTipoSectorEntidad, CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoSectorEntidad] WHERE  IdTipoSectorEntidad = @IdTipoSectorEntidad
END
GO