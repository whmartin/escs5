USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]    Script Date: 07/30/2013 16:18:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]    Script Date: 07/30/2013 16:18:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]
		@IdDocAdjunto INT,	@IdTercero INT,	
		@LinkDocumento NVARCHAR(256),	
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN

	IF (@IdTercero = 0) SET @IdTercero = NULL  

	UPDATE Proveedor.DocAdjuntoTercero 
	SET IdTercero = @IdTercero,
	LinkDocumento = @LinkDocumento, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdDocAdjunto = @IdDocAdjunto
END


GO


