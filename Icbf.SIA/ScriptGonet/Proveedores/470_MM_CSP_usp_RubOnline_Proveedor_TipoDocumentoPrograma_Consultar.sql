USE [Proveedores]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]    Script Date: 09/04/2013 17:37:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]
GO

USE [Proveedores]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]    Script Date: 09/04/2013 17:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]
@IdTipoDocumentoPrograma INT
AS
BEGIN
SELECT
	IdTipoDocumentoPrograma,
	IdTipoDocumento,
	IdPrograma,
	Estado,
	MaxPermitidoKB,
	ExtensionesPermitidas,
	ObligRupNoRenovado,
	ObligRupRenovado,
	ObligPersonaJuridica,
	ObligPersonaNatural,
	ObligSectorPublico,
	ObligSectorPrivado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[TipoDocumentoPrograma]
WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma AND
	  Estado = 1
END
GO


