USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]    Script Date: 07/24/2013 11:17:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]    Script Date: 07/24/2013 11:17:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]
	AS
BEGIN
 SELECT IdTipoSectorEntidad,Descripcion FROM [Proveedor].[TipoSectorEntidad] WHERE  Estado = 1
 ORDER BY Descripcion
END


GO


