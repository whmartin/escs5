USE [SIA]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]    Script Date: 06/14/2013 19:31:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]
	@IdEstadoValidacionDocumental INT
AS
BEGIN
	DELETE Proveedor.EstadoValidacionDocumental WHERE IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END
GO