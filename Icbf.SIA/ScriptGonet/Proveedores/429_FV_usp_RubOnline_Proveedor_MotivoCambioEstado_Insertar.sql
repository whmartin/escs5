USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]    Script Date: 07/30/2013 23:10:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]    Script Date: 07/30/2013 23:10:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que guarda un nuevo cambio de estado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]
@IdTercero INT, @DatosBasicos BIT, @Financiera BIT, @Experiencia BIT, @IdTemporal NVARCHAR (20), @Motivo NVARCHAR (128), @UsuarioCrea NVARCHAR (250)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION t1
		INSERT INTO Proveedor.MotivoCambioEstado (IdTercero, IdTemporal, Motivo, DatosBasicos, Financiera, Experiencia, FechaCrea, UsuarioCrea)
			VALUES (@IdTercero, @IdTemporal, @Motivo, @DatosBasicos, @Financiera, @Experiencia, GETDATE(), @UsuarioCrea)


		DECLARE @PorValidar NVARCHAR (10)
		SET @PorValidar = (SELECT
			IdEstadoTercero
		FROM Oferente.EstadoTercero
		WHERE CodigoEstadotercero = '005')
		UPDATE Oferente.TERCERO
		SET IDESTADOTERCERO = @PorValidar
		WHERE IDTERCERO = @IdTercero

		--Se obtiene el id de el proveedor para asi actualizar su respectivo estado
		DECLARE @IdEntidad INT
		SET @IdEntidad = (SELECT DISTINCT TOP(1)
			Proveedor.EntidadProvOferente.IdEntidad
		FROM Proveedor.EntidadProvOferente
		INNER JOIN Oferente.TERCERO
			ON Proveedor.EntidadProvOferente.IdTercero = Oferente.TERCERO.IDTERCERO
		WHERE (Oferente.TERCERO.IDTERCERO = @IdTercero))
		
		PRINT 'BIEN'

		--Se actualiza el estado a Datos Básicos
		SET @PorValidar = (SELECT
			IdEstadoDatosBasicos 
		FROM Proveedor.EstadoDatosBasicos    
		WHERE Descripcion  = 'EN VALIDACIÓN')
		IF @DatosBasicos = 1
		BEGIN
		UPDATE Proveedor.EntidadProvOferente
		SET IdEstado = @PorValidar
		WHERE Proveedor.EntidadProvOferente.IdEntidad = @IdEntidad
		END
		--Se actualiza el estado a Financiera
		SET @PorValidar = (SELECT
			IdEstadoValidacionDocumental 
		FROM Proveedor.EstadoValidacionDocumental    
		WHERE CodigoEstadoValidacionDocumental    = '05')
		IF @Financiera = 1
		BEGIN
		UPDATE Proveedor.InfoFinancieraEntidad
		SET EstadoValidacion = @PorValidar
		WHERE Proveedor.InfoFinancieraEntidad.IdEntidad = @IdEntidad
		END
		--Se actualiza el estado a Experiencia
		
		IF @Experiencia = 1
		SET @PorValidar = (SELECT
			IdEstadoValidacionDocumental 
		FROM Proveedor.EstadoValidacionDocumental    
		WHERE CodigoEstadoValidacionDocumental    = '05')
		BEGIN
		UPDATE Proveedor.InfoExperienciaEntidad
		SET EstadoDocumental = @PorValidar
		WHERE Proveedor.InfoExperienciaEntidad.IdEntidad = @IdEntidad
		END
		
COMMIT TRANSACTION t1
END TRY
BEGIN CATCH
ROLLBACK TRANSACTION t1
SELECT
	ERROR_NUMBER() AS ErrorNumber,
	ERROR_MESSAGE() AS ErrorMessage;
END CATCH;
END
GO


