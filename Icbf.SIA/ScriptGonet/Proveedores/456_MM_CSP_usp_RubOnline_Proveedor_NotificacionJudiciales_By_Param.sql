USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]    Script Date: 08/04/2013 00:22:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]    Script Date: 08/04/2013 00:22:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian Valencia
-- Create date: 21/06/2013
-- Description:	Obtiene las notificaiones judiciales del proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param] 
	@IdEntidad INT = NULL
	
AS
BEGIN
	--DECLARE @IdTipoPersona INT 
	--DECLARE @IdTipoIdentificacion INT
	--DECLARE @NumeroIdentificacion NVARCHAR(20) 
	--DECLARE @DV NVARCHAR(2) 
	--DECLARE @Proveedor NVARCHAR(250) 
	--DECLARE @CorreoElectronico NVARCHAR(50)
	
	--SET @IdTipoPersona = null
	--SET @IdTipoIdentificacion = null
	--SET @NumeroIdentificacion = null
	--SET @DV = null
	--SET @Proveedor = ''
	--SET @CorreoElectronico= null
	
	SELECT      Proveedor.NotificacionJudicial.IdNotJudicial,
			   (SELECT DIV.Departamento.NombreDepartamento 
			    FROM DIV.Departamento
			    WHERE DIV.Departamento.IdDepartamento = NotificacionJudicial.IdDepartamento) AS NombreDepartamento,
			   (SELECT DIV.Municipio.NombreMunicipio 
			    FROM DIV.Municipio
			    WHERE DIV.Municipio.IdDepartamento = NotificacionJudicial.IdDepartamento AND
					  DIV.Municipio.IdMunicipio = NotificacionJudicial.IdMunicipio) AS NombreMunicipio,
		        Proveedor.NotificacionJudicial.Direccion, Proveedor.NotificacionJudicial.UsuarioCrea
   FROM         Proveedor.EntidadProvOferente 
				INNER JOIN Oferente.TERCERO 
				ON Proveedor.EntidadProvOferente.IdTercero = Oferente.TERCERO.IDTERCERO 
				INNER JOIN Proveedor.NotificacionJudicial 
				ON Proveedor.EntidadProvOferente.IdEntidad = Proveedor.NotificacionJudicial.IdEntidad AND 
                   Proveedor.EntidadProvOferente.IdEntidad = Proveedor.NotificacionJudicial.IdEntidad	
    WHERE       Proveedor.EntidadProvOferente.IdEntidad  = CASE WHEN @IdEntidad IS NULL THEN Proveedor.EntidadProvOferente.IdEntidad  ELSE @IdEntidad END
				
	
END


GO


