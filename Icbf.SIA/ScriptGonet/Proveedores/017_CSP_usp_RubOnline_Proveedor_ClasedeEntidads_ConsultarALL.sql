USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]    Script Date: 06/14/2013 19:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]
	@IdTipodeActividad INT = NULL
AS
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[ClasedeEntidad] 
 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END AND Estado = 1
END
GO