/****** Object:  Trigger [PROVEEDOR].[TG_Aud_ReglasEstadoProveedor]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para ReglasEstadoProveedor
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_ReglasEstadoProveedor]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_ReglasEstadoProveedor]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_ReglasEstadoProveedor]
  ON  [PROVEEDOR].[ReglasEstadoProveedor]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_ReglasEstadoProveedor]
  ([IdValidacionEstado],[IdEstadoDatosBasicos_old], [IdEstadoDatosBasicos_new],[IdEstadoDatosFinancieros_old], [IdEstadoDatosFinancieros_new],[IdEstadoDatosExperiencia_old], [IdEstadoDatosExperiencia_new],[IdEstadoProveedor_old], [IdEstadoProveedor_new],[IdEstadoTercero_old], [IdEstadoTercero_new],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[Fechamodifica_old], [Fechamodifica_new],[Operacion])
SELECT deleted.IdValidacionEstado,deleted.IdEstadoDatosBasicos,inserted.IdEstadoDatosBasicos,deleted.IdEstadoDatosFinancieros,inserted.IdEstadoDatosFinancieros,deleted.IdEstadoDatosExperiencia,inserted.IdEstadoDatosExperiencia,deleted.IdEstadoProveedor,inserted.IdEstadoProveedor,deleted.IdEstadoTercero,inserted.IdEstadoTercero,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.Fechamodifica,inserted.Fechamodifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdValidacionEstado = deleted.IdValidacionEstadoEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_ReglasEstadoProveedor]
  ([IdValidacionEstado],[IdEstadoDatosBasicos_old],[IdEstadoDatosFinancieros_old],[IdEstadoDatosExperiencia_old],[IdEstadoProveedor_old],[IdEstadoTercero_old],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[Fechamodifica_old],[Operacion])
SELECT deleted.IdValidacionEstado,deleted.IdEstadoDatosBasicos,deleted.IdEstadoDatosFinancieros,deleted.IdEstadoDatosExperiencia,deleted.IdEstadoProveedor,deleted.IdEstadoTercero,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.Fechamodifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_ReglasEstadoProveedor]
  ([IdValidacionEstado],[IdEstadoDatosBasicos_new],[IdEstadoDatosFinancieros_new],[IdEstadoDatosExperiencia_new],[IdEstadoProveedor_new],[IdEstadoTercero_new],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[Fechamodifica_new],[Operacion])
SELECT inserted.IdValidacionEstado,inserted.IdEstadoDatosBasicos,inserted.IdEstadoDatosFinancieros,inserted.IdEstadoDatosExperiencia,inserted.IdEstadoProveedor,inserted.IdEstadoTercero,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.Fechamodifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO