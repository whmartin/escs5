USE [SIA]
GO
-- =============================================
-- Author:		Carlos Cubillos
-- Description:	Se agrega funcionalidad para determinar los documentos a adjuntar según tipo persona y sector para proveedores.
-- =============================================


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]    Script Date: 10/01/2013 16:23:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]    Script Date: 10/01/2013 16:23:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jonnathan NIño
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta una lista de DocExperienciaEntidad por IdTemporal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup] 
( 
	@IdTemporal varchar(20), 
	@RupRenovado varchar(128)=NULL, 
	@TipoPersona varchar(1),  
	@TipoSector varchar(1)
)
AS
DECLARE @IdPrograma int;
SELECT @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/INFOEXPERIENCIAENTIDAD';


SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdExpEntidad) as IdExpEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Descripcion) as Descripcion,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdExpEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Descripcion,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--				WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocExperienciaEntidad AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION EPERIENCIA
		AND IdTemporal = @IdTemporal
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdExpEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Descripcion,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--		WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION EXPERIENCIA
AND tdp.estado=1
) as Tabla
Where Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento




GO


