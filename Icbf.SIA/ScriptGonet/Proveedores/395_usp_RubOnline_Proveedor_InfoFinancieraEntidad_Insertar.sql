USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]    Script Date: 07/27/2013 10:43:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]    Script Date: 07/27/2013 10:43:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]
		@IdInfoFin INT OUTPUT, 	@IdEntidad INT,	@IdVigencia INT,	@ActivoCte NUMERIC(21, 3),	@ActivoTotal NUMERIC(21, 3),	@PasivoCte NUMERIC(21, 3),	@PasivoTotal NUMERIC(21, 3),	@Patrimonio NUMERIC(21, 3),	@GastosInteresFinancieros NUMERIC(21, 3),	@UtilidadOperacional NUMERIC(21, 3),	@ConfirmaIndicadoresFinancieros BIT,	@RupRenovado BIT,	@EstadoValidacion INT,	@ObservacionesInformacionFinanciera NVARCHAR(256),	@ObservacionesValidadorICBF NVARCHAR(256), @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20) = NULL
AS
BEGIN
	INSERT INTO Proveedor.InfoFinancieraEntidad(IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, NroRevision)
					  VALUES(@IdEntidad, @IdVigencia, @ActivoCte, @ActivoTotal, @PasivoCte, @PasivoTotal, @Patrimonio, @GastosInteresFinancieros, @UtilidadOperacional, @ConfirmaIndicadoresFinancieros, @RupRenovado, @EstadoValidacion, @ObservacionesInformacionFinanciera, @ObservacionesValidadorICBF, @UsuarioCrea, GETDATE(), 1)
	SELECT @IdInfoFin = @@IDENTITY 		
	
	UPDATE [Proveedor].[DocFinancieraProv] 
	set IdInfoFin = @IdInfoFin
	where IdTemporal = @IdTemporal
	
	SELECT @IdInfoFin
	
END



GO


