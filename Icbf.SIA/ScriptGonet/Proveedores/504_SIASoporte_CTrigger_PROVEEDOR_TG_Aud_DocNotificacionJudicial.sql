/****** Object:  Trigger [PROVEEDOR].[TG_Aud_DocNotificacionJudicial]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para DocNotificacionJudicial
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_DocNotificacionJudicial]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_DocNotificacionJudicial]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_DocNotificacionJudicial]
  ON  [PROVEEDOR].[DocNotificacionJudicial]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_DocNotificacionJudicial]
  ([IdDocNotJudicial],[IdNotJudicial_old], [IdNotJudicial_new],[IdDocumento_old], [IdDocumento_new],[Descripcion_old], [Descripcion_new],[LinkDocumento_old], [LinkDocumento_new],[Anno_old], [Anno_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdDocNotJudicial,deleted.IdNotJudicial,inserted.IdNotJudicial,deleted.IdDocumento,inserted.IdDocumento,deleted.Descripcion,inserted.Descripcion,deleted.LinkDocumento,inserted.LinkDocumento,deleted.Anno,inserted.Anno,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdDocNotJudicial = deleted.IdDocNotJudicialEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_DocNotificacionJudicial]
  ([IdDocNotJudicial],[IdNotJudicial_old],[IdDocumento_old],[Descripcion_old],[LinkDocumento_old],[Anno_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdDocNotJudicial,deleted.IdNotJudicial,deleted.IdDocumento,deleted.Descripcion,deleted.LinkDocumento,deleted.Anno,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_DocNotificacionJudicial]
  ([IdDocNotJudicial],[IdNotJudicial_new],[IdDocumento_new],[Descripcion_new],[LinkDocumento_new],[Anno_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdDocNotJudicial,inserted.IdNotJudicial,inserted.IdDocumento,inserted.Descripcion,inserted.LinkDocumento,inserted.Anno,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO