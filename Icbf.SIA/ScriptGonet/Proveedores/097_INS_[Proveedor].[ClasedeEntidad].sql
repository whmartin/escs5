USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:      Inserta registros en [Proveedor].[ClasedeEntidad]
-- =============================================
SET IDENTITY_INSERT [Proveedor].[ClasedeEntidad] ON
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, 1, N'SOCIEDAD ANONIMA', 1, N'Administrador', CAST(0x0000A1DC014F5818 AS DateTime), N'Administrador', CAST(0x0000A1DD0131C13E AS DateTime))
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, 1, N'SUCURSAL DE SOCIEDAD EXTRANJERA', 1, N'Administrador', CAST(0x0000A1DC014FA183 AS DateTime), NULL, NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, 2, N'ONG', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), NULL, NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, 2, N'SUCURSAL DE SOCIEDAD EXTRANJERA', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'Administrador', CAST(0x0000A1DC0158C506 AS DateTime))
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, 2, N'WWW', 1, N'Administrador', CAST(0x0000A1DD0131D00F AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[ClasedeEntidad] OFF
