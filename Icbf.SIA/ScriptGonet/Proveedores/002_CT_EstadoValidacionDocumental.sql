USE [SIA]
GO

-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Crea la entidad [Proveedor].[EstadoValidacionDocumental]
-- =============================================
/****** Object:  Table [Proveedor].[EstadoValidacionDocumental]    Script Date: 06/14/2013 19:19:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoValidacionDocumental]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[EstadoValidacionDocumental](
	[IdEstadoValidacionDocumental] [int] IDENTITY(1,1) NOT NULL,
	[CodigoEstadoValidacionDocumental] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoValidacionDocumental] PRIMARY KEY CLUSTERED 
(
	[IdEstadoValidacionDocumental] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO