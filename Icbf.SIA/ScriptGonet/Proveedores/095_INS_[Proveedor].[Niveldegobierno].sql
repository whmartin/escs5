USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en [Proveedor].[Niveldegobierno]
-- =============================================
SET IDENTITY_INSERT [Proveedor].[Niveldegobierno] ON
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'NACIONAL', 1, N'Administrador', CAST(0x0000A1DB00F64738 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'DEPARTAMENTAL', 1, N'Administrador', CAST(0x0000A1DB00F66280 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'003', N'DISTRITAL', 1, N'Administrador', CAST(0x0000A1DB00F66E30 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, N'004', N'MUNICIPAL', 1, N'Administrador', CAST(0x0000A1DB00F73C84 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'       SSS', N'          SS', 1, N'Administrador', CAST(0x0000A1DD0133BAE5 AS DateTime), N'Administrador', CAST(0x0000A1DD0133CF00 AS DateTime))
SET IDENTITY_INSERT [Proveedor].[Niveldegobierno] OFF