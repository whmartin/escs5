USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en  [Proveedor].[RamaoEstructura]
-- =============================================
/****** Object:  Table [Proveedor].[RamaoEstructura]    Script Date: 06/14/2013 19:40:35 ******/
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura] ON
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'EJECUTIVA', 1, N'Administrador', CAST(0x0000A1DB00D4D18A AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'JUDICIAL', 1, N'Administrador', CAST(0x0000A1DB00D4E726 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'003', N'LEGISLATIVA', 1, N'Administrador', CAST(0x0000A1DB00D4F782 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, N'004', N'�RGANOS DE CONTROL', 1, N'Administrador', CAST(0x0000A1DB00D50899 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'005', N'�RGANOS ELECTORALES', 1, N'Administrador', CAST(0x0000A1DB00D51804 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (6, N'006', N'�RGANOS AUT�NOMOS', 1, N'Administrador', CAST(0x0000A1DB00D526CC AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura] OFF