USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]    Script Date: 06/28/2013 18:38:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]    Script Date: 06/28/2013 18:38:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]
@IdTipoDocumentoPrograma INT OUTPUT, @IdTipoDocumento INT, @IdPrograma INT, @Estado INT, @MaxPermitidoKB INT, @ExtensionesPermitidas NVARCHAR(50), @ObligRupNoRenovado INT, @ObligRupRenovado INT, @ObligPersonaJuridica INT, @ObligPersonaNatural INT, @UsuarioCrea NVARCHAR (250)
AS
BEGIN
INSERT INTO Proveedor.TipoDocumentoPrograma (IdTipoDocumento, IdPrograma, Estado, MaxPermitidoKB, ExtensionesPermitidas, ObligRupNoRenovado, ObligRupRenovado, ObligPersonaJuridica, ObligPersonaNatural, UsuarioCrea, FechaCrea)
	VALUES (@IdTipoDocumento, @IdPrograma, 1, @MaxPermitidoKB, @ExtensionesPermitidas, @ObligRupNoRenovado, @ObligRupRenovado, @ObligPersonaJuridica, @ObligPersonaNatural, @UsuarioCrea, GETDATE())
SELECT
	@IdTipoDocumentoPrograma = @@IDENTITY
END
GO


