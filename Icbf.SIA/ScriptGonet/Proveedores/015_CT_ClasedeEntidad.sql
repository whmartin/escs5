USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Crea la entidad [Proveedor].[ClasedeEntidad]
-- =============================================
/****** Object:  Table [Proveedor].[ClasedeEntidad]    Script Date: 06/14/2013 19:19:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ClasedeEntidad](
	[IdClasedeEntidad] [int] IDENTITY(1,1) NOT NULL,
	[IdTipodeActividad] [int] NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ClasedeEntidad] PRIMARY KEY CLUSTERED 
(
	[IdClasedeEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  ForeignKey [FK_ClasedeEntidad_TipodeActividad]    Script Date: 06/14/2013 19:19:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad]  WITH CHECK ADD  CONSTRAINT [FK_ClasedeEntidad_TipodeActividad] FOREIGN KEY([IdTipodeActividad])
REFERENCES [Proveedor].[TipodeActividad] ([IdTipodeActividad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad] CHECK CONSTRAINT [FK_ClasedeEntidad_TipodeActividad]
GO