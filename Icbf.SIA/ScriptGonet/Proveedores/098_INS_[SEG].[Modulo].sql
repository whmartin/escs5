
 
 
USE [SIA]
GO
 -- =============================================
-- Author:        Bayron Lara Gonet
-- Create date:  22/05/2013 11:55 AM
-- Description:   Creacion del menu de oferentes
-- =============================================
 
IF NOT EXISTS (SELECT * FROM [SEG].[Modulo] WHERE  [NombreModulo] = 'Proveedores')
BEGIN
 
      INSERT INTO [SEG].[Modulo]
           ([NombreModulo]
           ,[Posicion]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion])        
     VALUES
           ('Proveedores'
           ,0
           ,1
           ,'GoNet'
           ,GETDATE())
 
END
GO
 