USE QoS
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Registra el aspnet_Rol PROVEEDORES en la entidad [QoS].[dbo].[aspnet_Roles] 
-- =============================================
IF NOT EXISTS (SELECT 1 FROM [QoS].[dbo].[aspnet_Roles] 
				WHERE [RoleName] = 'PROVEEDORES' 
				AND  [ApplicationId] = (SELECT ApplicationId  FROM aspnet_Applications WHERE ApplicationName = 'RUBOGCB'))
BEGIN

INSERT INTO [QoS].[dbo].[aspnet_Roles]
           ([ApplicationId]          
           ,[RoleName]
           ,[LoweredRoleName]
           ,[Description])           
     VALUES
           ((SELECT ApplicationId  FROM aspnet_Applications WHERE ApplicationName = 'RUBOGCB')      
           ,'PROVEEDORES'
           ,'proveedores'
           ,'Usuarios proveedores que se registran desde internet') 
END          
GO