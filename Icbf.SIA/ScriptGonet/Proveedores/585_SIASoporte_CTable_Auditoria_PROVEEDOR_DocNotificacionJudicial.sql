/****** Object:  Table [Auditoria].[PROVEEDOR_DocNotificacionJudicial]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para DocNotificacionJudicial ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_DocNotificacionJudicial]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_DocNotificacionJudicial]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_DocNotificacionJudicial]([IdDocNotJudicial] [int]  NULL, [IdNotJudicial_old] [int]  NULL,[IdNotJudicial_new] [int]  NULL,[IdDocumento_old] [int]  NULL,[IdDocumento_new] [int]  NULL,[Descripcion_old] [nvarchar] (128)  NULL,[Descripcion_new] [nvarchar] (128)  NULL,[LinkDocumento_old] [nvarchar] (256)  NULL,[LinkDocumento_new] [nvarchar] (256)  NULL,[Anno_old] [numeric] (4,0) NULL,[Anno_new] [numeric] (4,0) NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO