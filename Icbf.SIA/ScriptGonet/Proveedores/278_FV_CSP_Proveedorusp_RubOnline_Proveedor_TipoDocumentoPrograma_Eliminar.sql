USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]    Script Date: 06/28/2013 18:38:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]    Script Date: 06/28/2013 18:38:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]
@IdTipoDocumentoPrograma INT
AS
BEGIN
	UPDATE Proveedor.TipoDocumentoPrograma
	SET Estado = 0
	WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma
END
GO


