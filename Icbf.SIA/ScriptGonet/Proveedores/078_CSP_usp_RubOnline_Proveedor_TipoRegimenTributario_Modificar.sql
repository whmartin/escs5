USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]    Script Date: 06/14/2013 19:32:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]
		@IdTipoRegimenTributario INT,	@CodigoRegimenTributario NVARCHAR(128),	@Descripcion NVARCHAR(128),	@IdTipoPersona INT,	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoRegimenTributario SET CodigoRegimenTributario = @CodigoRegimenTributario, Descripcion = @Descripcion, IdTipoPersona = @IdTipoPersona, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoRegimenTributario = @IdTipoRegimenTributario
END
GO