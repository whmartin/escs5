USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]    Script Date: 07/29/2013 12:19:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]    Script Date: 07/29/2013 12:19:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoExperienciaEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima] (@IdEntidad INT, @IdExpEntidad INT)
as
begin
	select top 1
		v.IdValidarInfoExperienciaEntidad,
		e.IdEntidad,
		i.IdExpEntidad,		
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
			from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and i.IdExpEntidad = @IdExpEntidad
		and v.NroRevision = i.NroRevision
	order by NroRevision DESC
end

GO


