USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]    Script Date: 06/28/2013 14:54:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]    Script Date: 06/28/2013 14:54:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
		@IdTipoCargoEntidad INT OUTPUT, @CodigoTipoCargoEntidad NVARCHAR(10),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCargoEntidad(CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoCargoEntidad,@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCargoEntidad = @@IDENTITY 		
END

GO


