USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde Sámano    
-- Create date:         17/01/2014
-- Description:         Crea la entidad [PROVEEDOR].[ParametrosEmail]
-- =============================================
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[ParametrosEmail]') AND type in (N'U'))
BEGIN
	DROP TABLE [PROVEEDOR].[ParametrosEmail]
END

CREATE TABLE [PROVEEDOR].[ParametrosEmail](
	IdParametroID				INT IDENTITY(1,1) NOT NULL,
	CodigoParametroEmail		NVARCHAR(100) NOT NULL,
	DescripcionParametrosEmail	NVARCHAR(255),
	ValorMesesParametroEmail			INT
)
ALTER TABLE [PROVEEDOR].[ParametrosEmail]
ADD CONSTRAINT UNIC_ParametrosEmail
UNIQUE (CodigoParametroEmail)

INSERT INTO [PROVEEDOR].[ParametrosEmail]
VALUES('001','Numero de dias antes de eliminación de usuario(Envio Email)',10)
INSERT INTO [PROVEEDOR].ParametrosEmail
VALUES('002','Numero de meeses para eliminar usuario inactivo',3)

SELECT * FROM [PROVEEDOR].[ParametrosEmail]