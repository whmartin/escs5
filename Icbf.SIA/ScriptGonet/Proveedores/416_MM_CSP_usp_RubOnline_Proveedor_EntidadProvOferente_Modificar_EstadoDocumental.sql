USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar_EstadoDocumental]    Script Date: 07/29/2013 23:01:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar_EstadoDocumental]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar_EstadoDocumental]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar_EstadoDocumental]    Script Date: 07/29/2013 23:01:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  21/07/2013 15:07:30
-- Description:	Procedimiento almacenado que actualiza el estado documental de EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar_EstadoDocumental]
		@IdEntidad INT,	
		@IdEstado INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL,
		@Finalizado BIT = NULL
AS
BEGIN
	UPDATE Proveedor.EntidadProvOferente 
			SET IdEstado =ISNULL( @IdEstado, IdEstado ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE(),
			    Finalizado = ISNULL(@Finalizado,Finalizado)
			   WHERE IdEntidad = @IdEntidad 
END


GO


