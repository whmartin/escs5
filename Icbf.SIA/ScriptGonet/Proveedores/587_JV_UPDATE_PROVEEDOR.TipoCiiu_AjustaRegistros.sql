USE [SIA]
GO
/*
Autor: Juan Carlos Valverde Sámano
Fecha:30/SEPT/2014
Descripción: Actualiza en la entidad PROVEEDOR.TipoCiiu
para dejar la información de acuerdo a lo que solicita el usuario.
*/

UPDATE PROVEEDOR.TipoCiiu
SET Descripcion='Investigaciones  y desarrollo experimental en el campo de las ciencias sociales y las humanidades',
Estado=1
WHERE CodigoCiiu='7220'

UPDATE PROVEEDOR.TipoCiiu
SET Descripcion='Actividades de consultoría informática  y actividades de administración de instalaciones informáticas',
Estado=1
WHERE CodigoCiiu='6202'
