USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]    Script Date: 07/03/2013 21:23:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]    Script Date: 07/03/2013 21:23:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]
		@IdValidarTercero INT OUTPUT, 	@IdTercero INT,	@Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarTercero(IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdTercero, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarTercero = @@IDENTITY 		
END

GO


