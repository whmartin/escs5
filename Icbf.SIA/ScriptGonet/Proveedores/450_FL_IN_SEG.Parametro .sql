USE [SIA]
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Inserta parameto comfiguracion correo Porveedores
-- =============================================

DELETE FROM SEG.Parametro WHERE NombreParametro='MensajeMailsInconsistenciasProveedor' 

INSERT INTO SEG.Parametro 
SELECT 'MensajeMailsInconsistenciasProveedor','Sistema de Información Terceros y Gestión de  Proveedores del Instituto Colombiano de Bienestar Familiar',NULL,1,1,'Administrador',GETDATE(),NULL,NULL

