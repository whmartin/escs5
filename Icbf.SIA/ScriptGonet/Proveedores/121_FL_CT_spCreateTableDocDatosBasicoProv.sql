USE [SIA]
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/18/2013 6:02:08 PM
-- Description:  Elimina CONSTRAINTs e la entidad [Proveedor].[DocDatosBasicoProv]
-- =============================================


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv] DROP CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv] DROP CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[DocDatosBasicoProv]    Script Date: 06/24/2013 21:33:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocDatosBasicoProv]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Crea la entidad [Proveedor].[DocDatosBasicoProv]
-- =============================================
/****** Object:  Table [Proveedor].[DocDatosBasicoProv]    Script Date: 06/24/2013 21:33:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[DocDatosBasicoProv](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[NombreDocumento] [nvarchar](128) NOT NULL,
	[LinkDocumento] [nvarchar](256) NOT NULL,
	[Observaciones] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdTipoDocumento] [int] NULL,
 CONSTRAINT [PK_DocDatosBasicoProv] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[DocDatosBasicoProv]  WITH CHECK ADD  CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO

ALTER TABLE [Proveedor].[DocDatosBasicoProv] CHECK CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente]
GO

ALTER TABLE [Proveedor].[DocDatosBasicoProv]  WITH CHECK ADD  CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO

ALTER TABLE [Proveedor].[DocDatosBasicoProv] CHECK CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento]
GO


