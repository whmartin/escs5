/****** Object:  Trigger [PROVEEDOR].[TG_Aud_DocAdjuntoTercero]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para DocAdjuntoTercero
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_DocAdjuntoTercero]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_DocAdjuntoTercero]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_DocAdjuntoTercero]
  ON  [PROVEEDOR].[DocAdjuntoTercero]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_DocAdjuntoTercero]
  ([IDDOCADJUNTO],[IDTERCERO_old], [IDTERCERO_new],[IDDOCUMENTO_old], [IDDOCUMENTO_new],[DESCRIPCION_old], [DESCRIPCION_new],[LINKDOCUMENTO_old], [LINKDOCUMENTO_new],[ANNO_old], [ANNO_new],[FECHACREA_old], [FECHACREA_new],[USUARIOCREA_old], [USUARIOCREA_new],[FECHAMODIFICA_old], [FECHAMODIFICA_new],[USUARIOMODIFICA_old], [USUARIOMODIFICA_new],[IdTemporal_old], [IdTemporal_new],[Activo_old], [Activo_new],[Operacion])
SELECT deleted.IDDOCADJUNTO,deleted.IDTERCERO,inserted.IDTERCERO,deleted.IDDOCUMENTO,inserted.IDDOCUMENTO,deleted.DESCRIPCION,inserted.DESCRIPCION,deleted.LINKDOCUMENTO,inserted.LINKDOCUMENTO,deleted.ANNO,inserted.ANNO,deleted.FECHACREA,inserted.FECHACREA,deleted.USUARIOCREA,inserted.USUARIOCREA,deleted.FECHAMODIFICA,inserted.FECHAMODIFICA,deleted.USUARIOMODIFICA,inserted.USUARIOMODIFICA,deleted.IdTemporal,inserted.IdTemporal,deleted.Activo,inserted.Activo,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IDDOCADJUNTO = deleted.IDDOCADJUNTOEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_DocAdjuntoTercero]
  ([IDDOCADJUNTO],[IDTERCERO_old],[IDDOCUMENTO_old],[DESCRIPCION_old],[LINKDOCUMENTO_old],[ANNO_old],[FECHACREA_old],[USUARIOCREA_old],[FECHAMODIFICA_old],[USUARIOMODIFICA_old],[IdTemporal_old],[Activo_old],[Operacion])
SELECT deleted.IDDOCADJUNTO,deleted.IDTERCERO,deleted.IDDOCUMENTO,deleted.DESCRIPCION,deleted.LINKDOCUMENTO,deleted.ANNO,deleted.FECHACREA,deleted.USUARIOCREA,deleted.FECHAMODIFICA,deleted.USUARIOMODIFICA,deleted.IdTemporal,deleted.Activo,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_DocAdjuntoTercero]
  ([IDDOCADJUNTO],[IDTERCERO_new],[IDDOCUMENTO_new],[DESCRIPCION_new],[LINKDOCUMENTO_new],[ANNO_new],[FECHACREA_new],[USUARIOCREA_new],[FECHAMODIFICA_new],[USUARIOMODIFICA_new],[IdTemporal_new],[Activo_new],[Operacion])
SELECT inserted.IDDOCADJUNTO,inserted.IDTERCERO,inserted.IDDOCUMENTO,inserted.DESCRIPCION,inserted.LINKDOCUMENTO,inserted.ANNO,inserted.FECHACREA,inserted.USUARIOCREA,inserted.FECHAMODIFICA,inserted.USUARIOMODIFICA,inserted.IdTemporal,inserted.Activo,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO