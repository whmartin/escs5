USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]    Script Date: 06/14/2013 19:32:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]
	AS
BEGIN
 SELECT IdTipoSectorEntidad,Descripcion FROM [Proveedor].[TipoSectorEntidad] WHERE  Estado = 1
END
GO