/****** Object:  Trigger [PROVEEDOR].[TG_Aud_Sucursal]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para Sucursal
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_Sucursal]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_Sucursal]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_Sucursal]
  ON  [PROVEEDOR].[Sucursal]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_Sucursal]
  ([IdSucursal],[IdEntidad_old], [IdEntidad_new],[Indicativo_old], [Indicativo_new],[Telefono_old], [Telefono_new],[Extension_old], [Extension_new],[Celular_old], [Celular_new],[Correo_old], [Correo_new],[Estado_old], [Estado_new],[Departamento_old], [Departamento_new],[Municipio_old], [Municipio_new],[IdZona_old], [IdZona_new],[Direccion_old], [Direccion_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Nombre_old], [Nombre_new],[Editable_old], [Editable_new],[Operacion])
SELECT deleted.IdSucursal,deleted.IdEntidad,inserted.IdEntidad,deleted.Indicativo,inserted.Indicativo,deleted.Telefono,inserted.Telefono,deleted.Extension,inserted.Extension,deleted.Celular,inserted.Celular,deleted.Correo,inserted.Correo,deleted.Estado,inserted.Estado,deleted.Departamento,inserted.Departamento,deleted.Municipio,inserted.Municipio,deleted.IdZona,inserted.IdZona,deleted.Direccion,inserted.Direccion,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,deleted.Nombre,inserted.Nombre,deleted.Editable,inserted.Editable,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdSucursal = deleted.IdSucursalEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_Sucursal]
  ([IdSucursal],[IdEntidad_old],[Indicativo_old],[Telefono_old],[Extension_old],[Celular_old],[Correo_old],[Estado_old],[Departamento_old],[Municipio_old],[IdZona_old],[Direccion_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Nombre_old],[Editable_old],[Operacion])
SELECT deleted.IdSucursal,deleted.IdEntidad,deleted.Indicativo,deleted.Telefono,deleted.Extension,deleted.Celular,deleted.Correo,deleted.Estado,deleted.Departamento,deleted.Municipio,deleted.IdZona,deleted.Direccion,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,deleted.Nombre,deleted.Editable,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_Sucursal]
  ([IdSucursal],[IdEntidad_new],[Indicativo_new],[Telefono_new],[Extension_new],[Celular_new],[Correo_new],[Estado_new],[Departamento_new],[Municipio_new],[IdZona_new],[Direccion_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Nombre_new],[Editable_new],[Operacion])
SELECT inserted.IdSucursal,inserted.IdEntidad,inserted.Indicativo,inserted.Telefono,inserted.Extension,inserted.Celular,inserted.Correo,inserted.Estado,inserted.Departamento,inserted.Municipio,inserted.IdZona,inserted.Direccion,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,inserted.Nombre,inserted.Editable,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO