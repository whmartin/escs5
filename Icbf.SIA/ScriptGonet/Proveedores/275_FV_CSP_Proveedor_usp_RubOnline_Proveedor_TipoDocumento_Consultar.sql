USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]    Script Date: 06/28/2013 18:41:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]    Script Date: 06/28/2013 18:41:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Fabian valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]
	@IdTipoDocumento INT =NULL
AS
BEGIN
	SELECT     Proveedor.TipoDocumento.*
	FROM         Proveedor.TipoDocumento
	WHERE IdTipoDocumento =
	CASE
		WHEN @IdTipoDocumento IS NULL THEN IdTipoDocumento ELSE @IdTipoDocumento
	END
	AND Estado = 1
END


GO


