use [SIA]
GO  
--Autor: Mauricio Martinez
--Descripcion: Para mover el menu de TipoPrograma
--Fecha: 2013/07/03 06:17pm

delete from [Proveedor].[TablaParametrica]
	where Url = 'Proveedor/TipoDocumentoPrograma'
GO  
update [SEG].[Programa]
set [VisibleMenu] = 1, 
	[Padre] = null,
	[NombrePrograma] = 'Tipo Documento Programa'
	where [CodigoPrograma] = 'Proveedor/TipoDocumentoPrograma'
