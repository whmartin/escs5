USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]    Script Date: 06/24/2013 12:23:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]    Script Date: 06/24/2013 12:23:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabi�n Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los estados del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]
	@Estado BIT = NULL
AS
BEGIN

 SELECT IdEstadoTercero, 
		CodigoEstadotercero, 
		DescripcionEstado, 
		Estado, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
 FROM [Oferente].[EstadoTercero] 
 WHERE Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
GO


