USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]    Script Date: 06/14/2013 19:32:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]
	@CodigoRegimenTributario NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@IdTipoPersona INT = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoRegimenTributario] WHERE CodigoRegimenTributario = CASE WHEN @CodigoRegimenTributario IS NULL THEN CodigoRegimenTributario ELSE @CodigoRegimenTributario END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN IdTipoPersona ELSE @IdTipoPersona END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
GO