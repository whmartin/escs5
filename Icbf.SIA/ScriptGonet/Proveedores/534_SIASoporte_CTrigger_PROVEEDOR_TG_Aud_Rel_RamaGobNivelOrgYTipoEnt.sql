/****** Object:  Trigger [PROVEEDOR].[TG_Aud_Rel_RamaGobNivelOrgYTipoEnt]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para Rel_RamaGobNivelOrgYTipoEnt
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_Rel_RamaGobNivelOrgYTipoEnt]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_Rel_RamaGobNivelOrgYTipoEnt]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_Rel_RamaGobNivelOrgYTipoEnt]
  ON  [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_Rel_RamaGobNivelOrgYTipoEnt]
  ([IdRelacion],[IdRamaEstructura_old], [IdRamaEstructura_new],[IdNivelGobierno_old], [IdNivelGobierno_new],[IdNivelOrganizacional_old], [IdNivelOrganizacional_new],[IdTipodeentidadPublica_old], [IdTipodeentidadPublica_new],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdRelacion,deleted.IdRamaEstructura,inserted.IdRamaEstructura,deleted.IdNivelGobierno,inserted.IdNivelGobierno,deleted.IdNivelOrganizacional,inserted.IdNivelOrganizacional,deleted.IdTipodeentidadPublica,inserted.IdTipodeentidadPublica,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdRelacion = deleted.IdRelacionEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_Rel_RamaGobNivelOrgYTipoEnt]
  ([IdRelacion],[IdRamaEstructura_old],[IdNivelGobierno_old],[IdNivelOrganizacional_old],[IdTipodeentidadPublica_old],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdRelacion,deleted.IdRamaEstructura,deleted.IdNivelGobierno,deleted.IdNivelOrganizacional,deleted.IdTipodeentidadPublica,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_Rel_RamaGobNivelOrgYTipoEnt]
  ([IdRelacion],[IdRamaEstructura_new],[IdNivelGobierno_new],[IdNivelOrganizacional_new],[IdTipodeentidadPublica_new],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdRelacion,inserted.IdRamaEstructura,inserted.IdNivelGobierno,inserted.IdNivelOrganizacional,inserted.IdTipodeentidadPublica,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO