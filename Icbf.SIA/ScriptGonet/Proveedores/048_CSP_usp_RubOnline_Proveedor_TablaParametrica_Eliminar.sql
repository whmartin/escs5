USE [SIA]
GO

-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Elimina una tabla parametrica de Proveedor.TablaParametrica
-- =============================================

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]    Script Date: 06/14/2013 19:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]
	@IdTablaParametrica INT
AS
BEGIN
	DELETE Proveedor.TablaParametrica WHERE IdTablaParametrica = @IdTablaParametrica
END
GO