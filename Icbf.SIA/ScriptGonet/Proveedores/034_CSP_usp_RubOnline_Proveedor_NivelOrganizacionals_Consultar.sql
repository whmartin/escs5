USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]    Script Date: 06/14/2013 19:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]
	@CodigoNivelOrganizacional NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[NivelOrganizacional] WHERE CodigoNivelOrganizacional = CASE WHEN @CodigoNivelOrganizacional IS NULL THEN CodigoNivelOrganizacional ELSE @CodigoNivelOrganizacional END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
GO