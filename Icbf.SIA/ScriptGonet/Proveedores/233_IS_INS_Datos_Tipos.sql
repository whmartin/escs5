USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Creacion de tipos TipoDocIdentifica, TipoCodigoUNSPSC, TipoCargoEntidad, ParametrosEmail, EstadoValidacionDocumental, EstadoDatosBasicos
-- =============================================
/****** Object:  Table [Proveedor].[TipoDocIdentifica]    Script Date: 06/23/2013 23:25:48 ******/

IF NOT EXISTS( SELECT [IdTipoDocIdentifica] FROM [Proveedor].[TipoDocIdentifica] WHERE  [Descripcion] = 'CÉDULA DE CIUDADANÍA') BEGIN
SET IDENTITY_INSERT [Proveedor].[TipoDocIdentifica] ON
INSERT [Proveedor].[TipoDocIdentifica] ([IdTipoDocIdentifica], [CodigoDocIdentifica], [Descripcion], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (1, CAST(1 AS Numeric(18, 0)), N'CÉDULA DE CIUDADANÍA', N'Administrador', CAST(0x0000A1E200203F1A AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[TipoDocIdentifica] OFF
END

/****** Object:  Table [Proveedor].[TipoCodigoUNSPSC]    Script Date: 06/23/2013 23:25:48 ******/
SET IDENTITY_INSERT [Proveedor].[TipoCodigoUNSPSC] ON
IF NOT EXISTS( SELECT [IdTipoCodUNSPSC] FROM [Proveedor].[TipoCodigoUNSPSC] WHERE  [Descripcion] = 'ANIMALES VIVOS') BEGIN
INSERT [Proveedor].[TipoCodigoUNSPSC] ([IdTipoCodUNSPSC], [Codigo], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (1, N'10100000', N'ANIMALES VIVOS', 1, N'Administrador', CAST(0x0000A1E5001C7323 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT [IdTipoCodUNSPSC] FROM [Proveedor].[TipoCodigoUNSPSC] WHERE  [Descripcion] = 'GATOS') BEGIN
INSERT [Proveedor].[TipoCodigoUNSPSC] ([IdTipoCodUNSPSC], [Codigo], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (2, N'10101501', N'GATOS', 1, N'Administrador', CAST(0x0000A1E5001C8207 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT [IdTipoCodUNSPSC] FROM [Proveedor].[TipoCodigoUNSPSC] WHERE  [Descripcion] = 'UNIÓN DE EMPLEADOS') BEGIN
INSERT [Proveedor].[TipoCodigoUNSPSC] ([IdTipoCodUNSPSC], [Codigo], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (3, N'94101807', N'UNIÓN DE EMPLEADOS', 1, N'Administrador', CAST(0x0000A1E5001CE640 AS DateTime), N'Administrador', CAST(0x0000A1E5001DE013 AS DateTime))
END
SET IDENTITY_INSERT [Proveedor].[TipoCodigoUNSPSC] OFF
/****** Object:  Table [Proveedor].[TipoCargoEntidad]    Script Date: 06/23/2013 23:25:48 ******/
SET IDENTITY_INSERT [Proveedor].[TipoCargoEntidad] ON
IF NOT EXISTS( SELECT [IdTipoCargoEntidad] FROM [Proveedor].[TipoCargoEntidad] WHERE  [Descripcion] = 'CARGO UNO') BEGIN
INSERT [Proveedor].[TipoCargoEntidad] ([IdTipoCargoEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (1, N'CARGO UNO', 1, N'Administrador', CAST(0x0000A1E20181316F AS DateTime), N'Administrador', CAST(0x0000A1E301725F5A AS DateTime))
END
IF NOT EXISTS( SELECT [IdTipoCargoEntidad] FROM [Proveedor].[TipoCargoEntidad] WHERE  [Descripcion] = 'CARGO DOS') BEGIN
INSERT [Proveedor].[TipoCargoEntidad] ([IdTipoCargoEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (2, N'CARGO DOS', 1, N'Administrador', CAST(0x0000A1E20181437D AS DateTime), N'Administrador', CAST(0x0000A1E301727A03 AS DateTime))
END
SET IDENTITY_INSERT [Proveedor].[TipoCargoEntidad] OFF
/****** Object:  Table [Proveedor].[ParametrosEmail]    Script Date: 06/23/2013 23:25:48 ******/
SET IDENTITY_INSERT [Proveedor].[ParametrosEmail] ON
IF NOT EXISTS( SELECT [IdParametroID] FROM [Proveedor].[ParametrosEmail] WHERE  [CodigoParametroEmail] = '001') BEGIN
INSERT [Proveedor].[ParametrosEmail] ([IdParametroID], [CodigoParametroEmail], [DescripcionParametrosEmail], [ValorMesesParametroEmail]) 
VALUES (1, N'001', N'Máximo Meses Envio Email', 2)
END
IF NOT EXISTS( SELECT [IdParametroID] FROM [Proveedor].[ParametrosEmail] WHERE  [CodigoParametroEmail] = '002') BEGIN
INSERT [Proveedor].[ParametrosEmail] ([IdParametroID], [CodigoParametroEmail], [DescripcionParametrosEmail], [ValorMesesParametroEmail]) 
VALUES (2, N'002', N'Máximo Meses Elimino Email', 1)
END
SET IDENTITY_INSERT [Proveedor].[ParametrosEmail] OFF
/****** Object:  Table [Proveedor].[EstadoValidacionDocumental]    Script Date: 06/23/2013 23:25:48 ******/
SET IDENTITY_INSERT [Proveedor].[EstadoValidacionDocumental] ON
IF NOT EXISTS( SELECT [IdEstadoValidacionDocumental] FROM [Proveedor].[EstadoValidacionDocumental] WHERE  [CodigoEstadoValidacionDocumental] = '01') BEGIN
INSERT [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental], [CodigoEstadoValidacionDocumental], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (1, N'01', N'PENDIENTE DE VALIDACIÓN', 1, N'administrador', CAST(0x0000A1DC00000000 AS DateTime), N'Administrador', CAST(0x0000A1DD0132630B AS DateTime))
INSERT [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental], [CodigoEstadoValidacionDocumental], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (2, N'02', N'POR AJUSTAR', 1, N'Administrador', CAST(0x0000A1DD013278A0 AS DateTime), N'Administrador', CAST(0x0000A1DD0132FECD AS DateTime))
INSERT [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental], [CodigoEstadoValidacionDocumental], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (3, N'03', N'DATOS PARCIALES', 1, N'Administrador', CAST(0x0000A1DD013278A0 AS DateTime), N'Administrador', CAST(0x0000A1DD0132FECD AS DateTime))
INSERT [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental], [CodigoEstadoValidacionDocumental], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (4, N'04', N'VALIDADO', 1, N'Administrador', CAST(0x0000A1DD013278A0 AS DateTime), N'Administrador', CAST(0x0000A1DD0132FECD AS DateTime))
INSERT [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental], [CodigoEstadoValidacionDocumental], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (5, N'05', N'EN VALIDACIÓN', 1, N'Administrador', CAST(0x0000A1DD013278A0 AS DateTime), N'Administrador', CAST(0x0000A1DD0132FECD AS DateTime))
END
SET IDENTITY_INSERT [Proveedor].[EstadoValidacionDocumental] OFF
/****** Object:  Table [Proveedor].[EstadoDatosBasicos]    Script Date: 06/23/2013 23:25:48 ******/
SET IDENTITY_INSERT [Proveedor].[EstadoDatosBasicos] ON
IF NOT EXISTS( SELECT [IdEstadoDatosBasicos] FROM [Proveedor].[EstadoDatosBasicos] WHERE  [Descripcion] = 'PENDIENTE POR VALIDACIÓN ') BEGIN
INSERT [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (1, N'PENDIENTE POR VALIDACIÓN ', 1, N'Administrador', CAST(0x0000A1E3000F189A AS DateTime), NULL, NULL)
INSERT [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (2, N'VALIDADO', 1, N'Administrador', CAST(0x0000A1E3000F275B AS DateTime), NULL, NULL)
INSERT [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (3, N'POR AJUSTE', 1, N'Administrador', CAST(0x0000A1E3000F3521 AS DateTime), NULL, NULL)
INSERT [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (4, N'DATOS PARCIALES', 1, N'Administrador', CAST(0x0000A1E3000F46BB AS DateTime), NULL, NULL)
END
SET IDENTITY_INSERT [Proveedor].[EstadoDatosBasicos] OFF
