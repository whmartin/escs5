USE [SIA]
-- =============================================
-- Author:		Mauricio Martinez 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Permisos infoProveedor
-- =============================================
GO
DECLARE @idModulo INT;
SELECT @idModulo = IdModulo FROM [SEG].[Modulo] WHERE [NombreModulo] = 'Proveedores'

DECLARE @IdPrograma INT;
IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/InfoFinancieraEntidad') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Info Financiera Entidad', N'PROVEEDOR/InfoFinancieraEntidad', 0, 1, N'administrador',GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/DocFinancieraProv') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Doc Financiera Prov', N'PROVEEDOR/DocFinancieraProv', 0, 1, N'administrador',GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/SUCURSAL') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'SUCURSAL', N'PROVEEDOR/SUCURSAL', 0, 1, N'administrador',GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END

/*Contactos*/
IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/TIPODOCIDENTIFICA') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Tipo Documento Identificacion', N'PROVEEDOR/TIPODOCIDENTIFICA', 0, 1, N'administrador',GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/CONTACTOENTIDAD') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Contacto Entidad', N'PROVEEDOR/CONTACTOENTIDAD', 0, 1, N'administrador',GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/TIPOCARGOENTIDAD') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Cargo Entidad', N'PROVEEDOR/TIPOCARGOENTIDAD', 0, 1, N'administrador',GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END

/*Experiencia*/
IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/INFOEXPERIENCIAENTIDAD') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Experiencia Entidad', N'PROVEEDOR/INFOEXPERIENCIAENTIDAD', 0, 1, N'administrador',GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/TIPOCODIGOUNSPSC') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Tipo C�digo UNSPSC', N'PROVEEDOR/TIPOCODIGOUNSPSC', 0, 1, N'administrador',GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END
