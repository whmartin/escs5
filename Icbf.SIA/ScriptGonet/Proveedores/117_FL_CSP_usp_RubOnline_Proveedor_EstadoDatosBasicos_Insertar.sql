USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]    Script Date: 06/24/2013 21:29:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]    Script Date: 06/24/2013 21:29:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]
		@IdEstadoDatosBasicos INT OUTPUT, 	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoDatosBasicos(Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoDatosBasicos = @@IDENTITY 		
END

GO


