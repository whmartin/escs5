/****** Object:  Trigger [PROVEEDOR].[TG_Aud_ValidarInfoFinancieraEntidad]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para ValidarInfoFinancieraEntidad
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_ValidarInfoFinancieraEntidad]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_ValidarInfoFinancieraEntidad]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_ValidarInfoFinancieraEntidad]
  ON  [PROVEEDOR].[ValidarInfoFinancieraEntidad]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_ValidarInfoFinancieraEntidad]
  ([IdValidarInfoFinancieraEntidad],[NroRevision_old], [NroRevision_new],[IdInfoFin_old], [IdInfoFin_new],[ConfirmaYAprueba_old], [ConfirmaYAprueba_new],[Observaciones_old], [Observaciones_new],[FechaCrea_old], [FechaCrea_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaModifica_old], [FechaModifica_new],[UsuarioModifica_old], [UsuarioModifica_new],[CorreoEnviado_old], [CorreoEnviado_new],[Operacion])
SELECT deleted.IdValidarInfoFinancieraEntidad,deleted.NroRevision,inserted.NroRevision,deleted.IdInfoFin,inserted.IdInfoFin,deleted.ConfirmaYAprueba,inserted.ConfirmaYAprueba,deleted.Observaciones,inserted.Observaciones,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaModifica,inserted.FechaModifica,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.CorreoEnviado,inserted.CorreoEnviado,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdValidarInfoFinancieraEntidad = deleted.IdValidarInfoFinancieraEntidadEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_ValidarInfoFinancieraEntidad]
  ([IdValidarInfoFinancieraEntidad],[NroRevision_old],[IdInfoFin_old],[ConfirmaYAprueba_old],[Observaciones_old],[FechaCrea_old],[UsuarioCrea_old],[FechaModifica_old],[UsuarioModifica_old],[CorreoEnviado_old],[Operacion])
SELECT deleted.IdValidarInfoFinancieraEntidad,deleted.NroRevision,deleted.IdInfoFin,deleted.ConfirmaYAprueba,deleted.Observaciones,deleted.FechaCrea,deleted.UsuarioCrea,deleted.FechaModifica,deleted.UsuarioModifica,deleted.CorreoEnviado,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_ValidarInfoFinancieraEntidad]
  ([IdValidarInfoFinancieraEntidad],[NroRevision_new],[IdInfoFin_new],[ConfirmaYAprueba_new],[Observaciones_new],[FechaCrea_new],[UsuarioCrea_new],[FechaModifica_new],[UsuarioModifica_new],[CorreoEnviado_new],[Operacion])
SELECT inserted.IdValidarInfoFinancieraEntidad,inserted.NroRevision,inserted.IdInfoFin,inserted.ConfirmaYAprueba,inserted.Observaciones,inserted.FechaCrea,inserted.UsuarioCrea,inserted.FechaModifica,inserted.UsuarioModifica,inserted.CorreoEnviado,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO