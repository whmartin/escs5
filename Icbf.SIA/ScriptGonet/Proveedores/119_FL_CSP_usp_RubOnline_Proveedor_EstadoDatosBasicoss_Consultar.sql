USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]    Script Date: 06/24/2013 21:30:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]    Script Date: 06/24/2013 21:30:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]
	@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
     FROM [Proveedor].[EstadoDatosBasicos] 
        WHERE Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
        AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO


