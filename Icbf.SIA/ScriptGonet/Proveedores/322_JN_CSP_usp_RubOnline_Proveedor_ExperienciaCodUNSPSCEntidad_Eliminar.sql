USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que elimina un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]
	@IdExpCOD INT
AS
BEGIN
	DELETE Proveedor.ExperienciaCodUNSPSCEntidad WHERE IdExpCOD = @IdExpCOD
END
