USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]
		@IdTipoCargoEntidad INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCargoEntidad 
	SET Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdTipoCargoEntidad = @IdTipoCargoEntidad
END
