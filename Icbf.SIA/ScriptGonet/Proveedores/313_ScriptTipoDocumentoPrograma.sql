use  [SIA]
GO


-- =================================================
-- Author:          Gonet\Bayron Lara
-- Create date:     16/07/2013 04:56:29 p.m.
-- Description:     Tipo documentos por programa
-- =================================================


delete from [Proveedor].[TipoDocumentoPrograma]
delete from [Proveedor].[TipoDocumento]
go

SET IDENTITY_INSERT [Proveedor].[TipoDocumento] ON
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'101', N'Certificado RUP', 1, N'administador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'001', N'Balance General', 1, N'administrador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'002', N'Estado de Resultados', 1, N'administrador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'003', N'Tarjeta profesional del contador', 1, N'administrador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (7, N'004', N'Certificado de la junta central de contadores', 1, N'administrador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (10, N'005', N'C�dula de Ciudadan�a', 1, N'administrador', CAST(0x0000A1DE00000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (11, N'006', N'C�mara de Comercio o Tarjeta Profesional', 1, N'administrador', CAST(0x0000A1DE00000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (15, N'010', N'Documento Experiencia 01', 1, N'administrador', CAST(0x0000A1FA01488083 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (20, N'011', N'Documento Experiencia 02', 1, N'administrador', CAST(0x0000A1FA014899F9 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (21, N'012', N'Documento Experiencia 03', 1, N'administrador', CAST(0x0000A1FA014899F9 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (22, N'013', N'Documento Experiencia 04', 1, N'administrador', CAST(0x0000A1FA014899F9 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (23, N'014', N'Documento Experiencia 05', 1, N'administrador', CAST(0x0000A1FA014899F9 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[TipoDocumento] OFF



declare @IdPrograma int;
/****** Object:  Table [Proveedor].[TipoDocumentoPrograma]    Script Date: 06/23/2013 22:33:28 ******/

SET IDENTITY_INSERT [Proveedor].[TipoDocumentoPrograma] ON
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionTercero';
IF NOT EXISTS( SELECT [IdTipoDocumentoPrograma] FROM [Proveedor].[TipoDocumentoPrograma] WHERE  [IdPrograma] = @IdPrograma) BEGIN
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (2, 1, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 1, 0)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (10, 10, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 0, 1)
END
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/DocFinancieraProv';
IF NOT EXISTS( SELECT [IdTipoDocumentoPrograma] FROM [Proveedor].[TipoDocumentoPrograma] WHERE  [IdPrograma] = @IdPrograma) BEGIN
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (3, 1, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 256, N'pdf,jpg', 0, 1, NULL, NULL)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (4, 2, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 256, N'pdf,jpg', 1, 0, NULL, NULL)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (5, 3, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 112, N'pdf,jpg', 1, 0, NULL, NULL)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (6, 5, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 112, N'pdf,jpg', 1, 0, NULL, NULL)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (7, 7, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 112, N'pdf', 1, 0, NULL, NULL)
END

select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionProveedores';
IF NOT EXISTS( SELECT [IdTipoDocumentoPrograma] FROM [Proveedor].[TipoDocumentoPrograma] WHERE  [IdPrograma] = @IdPrograma) BEGIN
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (1, 1, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 1, 0)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (8, 10, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 0, 1)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (9, 11, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 0, 1)
END

select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/NotificacionJudicial';
IF NOT EXISTS( SELECT [IdTipoDocumentoPrograma] FROM [Proveedor].[TipoDocumentoPrograma] WHERE  [IdPrograma] = @IdPrograma) BEGIN
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (11, 10, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 1, 0)
END

select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/INFOEXPERIENCIAENTIDAD';
IF NOT EXISTS( SELECT [IdTipoDocumentoPrograma] FROM [Proveedor].[TipoDocumentoPrograma] WHERE  [IdPrograma] = @IdPrograma) BEGIN
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (15, 15, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 1, 0)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (20, 20, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 1, 0)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (21, 21, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 1, 0)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (22, 22, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 1, 0)
INSERT [Proveedor].[TipoDocumentoPrograma] ([IdTipoDocumentoPrograma], [IdTipoDocumento], [IdPrograma], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica], [MaxPermitidoKB], [ExtensionesPermitidas], [ObligRupNoRenovado], [ObligRupRenovado], [ObligPersonaJuridica], [ObligPersonaNatural]) 
VALUES (23, 23, @IdPrograma, 1, N'administrador', getdate(), NULL, NULL, 4096, N'pdf,jpg', NULL, NULL, 1, 0)
END

SET IDENTITY_INSERT [Proveedor].[TipoDocumentoPrograma] OFF