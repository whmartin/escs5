USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]
	@IdExpEntidad INT = NULL,
	@Descripcion NVARCHAR(128) = NULL,
	@LinkDocumento NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdExpEntidad, IdTipoDocGrupo, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[DocExperienciaEntidad] 
 WHERE IdExpEntidad = CASE WHEN @IdExpEntidad IS NULL THEN IdExpEntidad ELSE @IdExpEntidad END 
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
 AND LinkDocumento = CASE WHEN @LinkDocumento IS NULL THEN LinkDocumento ELSE @LinkDocumento END
END
