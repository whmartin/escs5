USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]    Script Date: 06/24/2013 21:16:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]    Script Date: 06/24/2013 21:16:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabi�n Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que consulta un(a) NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]
	@IdNotJudicial INT= NULL,
	@IdEntidad INT = NULL,
	@IdDepartamento INT = NULL,
	@IdMunicipio INT = NULL
AS
BEGIN
 SELECT IdNotJudicial, 
		IdEntidad, 
		IdDepartamento, 
		IdMunicipio, 
		IdZona, Direccion, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
		FROM [Proveedor].[NotificacionJudicial] 
		WHERE IdNotJudicial = CASE WHEN @IdNotJudicial IS NULL THEN IdNotJudicial ELSE @IdNotJudicial END AND 
			  IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END AND 
			  IdDepartamento = CASE WHEN @IdDepartamento IS NULL THEN IdDepartamento ELSE @IdDepartamento END AND 
			  IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END
END

GO


