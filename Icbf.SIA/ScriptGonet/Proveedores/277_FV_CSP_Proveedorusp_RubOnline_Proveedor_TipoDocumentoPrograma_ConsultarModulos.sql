USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]    Script Date: 06/28/2013 19:08:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]    Script Date: 06/28/2013 19:08:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Fabian Valencia
-- Create date: 28/06/2013
-- Description:	Obtiene los programas del proveedor
-- =============================================
CREATE PROCEDURE  [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos] 
	@idModulo INT
AS
BEGIN
	SELECT     IdPrograma, IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion, VisibleMenu,
                       generaLog, Padre
FROM         SEG.Programa
WHERE     (CodigoPrograma   NOT LIKE N'%PROVEEDOR/TIPO%') AND
			IdModulo = @idModulo
END


GO
