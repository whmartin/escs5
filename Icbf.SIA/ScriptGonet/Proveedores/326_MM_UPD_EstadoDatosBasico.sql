use [SIA]
go
-- =============================================
-- Author:          Juan Carlos Valverde Sámano    
-- Create date:         20/01/2014
-- Description:     Se asegura de que existe un registro en la entidad [Proveedor].[EstadoDatosBasicos]
-- =============================================
IF NOT EXISTS(SELECT * FROM [Proveedor].[EstadoDatosBasicos] WHERE Descripcion = 'EN VALIDACIÓN') BEGIN
  insert into [Proveedor].[EstadoDatosBasicos]
  values('EN VALIDACIÓN', 1, 'Administrador', getdate(),null,null)
END