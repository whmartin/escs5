USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]    Script Date: 06/24/2013 21:35:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]    Script Date: 06/24/2013 21:35:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DocDatosBasico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdEntidad INT,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @IdTipoDocumento int, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.DocDatosBasicoProv(IdEntidad, NombreDocumento, LinkDocumento, Observaciones, IdTipoDocumento, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @NombreDocumento, @LinkDocumento, @Observaciones, @IdTipoDocumento, @UsuarioCrea, GETDATE())
	SELECT @IdDocAdjunto = @@IDENTITY 		
END

GO


