USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]    Script Date: 07/04/2013 08:07:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]    Script Date: 07/04/2013 08:07:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]
	@IdValidarTercero INT
AS
BEGIN
 SELECT IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarTercero] WHERE  IdValidarTercero = @IdValidarTercero
END

GO


