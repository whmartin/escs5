USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Realiza un update a la entidad Proveedor.TipoDocumentoPrograma
-- =============================================
DECLARE @idDoc INT

SET @idDoc =(SELECT IdTipoDocumento  FROM Proveedor.TipoDocumento where CodigoTipoDocumento = '006')
DECLARE @idCC INT

SET @idCC =(SELECT IdTipoDocumento  FROM Proveedor.TipoDocumento where CodigoTipoDocumento = '005')
declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'Proveedor/NotificacionJudicial';
PRINT @IdPrograma

UPDATE Proveedor.TipoDocumentoPrograma SET IdTipoDocumento = @idDoc
WHERE IdTipoDocumento = @idCC AND @IdPrograma = IdPrograma 