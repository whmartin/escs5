USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]') AND type in (N'U'))
	ALTER TABLE Proveedor.InfoExperienciaEntidad ALTER COLUMN EntidadContratante NVARCHAR(256) NOT NULL
GO
