USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]    Script Date: 06/24/2013 21:30:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]    Script Date: 06/24/2013 21:30:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]
	@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
     FROM [Proveedor].[EstadoDatosBasicos] 
        WHERE Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO


