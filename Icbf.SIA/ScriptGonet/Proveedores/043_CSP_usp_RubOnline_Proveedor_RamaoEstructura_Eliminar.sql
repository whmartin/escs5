USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]    Script Date: 06/14/2013 19:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que elimina un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]
	@IdRamaEstructura INT
AS
BEGIN
	DELETE Proveedor.RamaoEstructura WHERE IdRamaEstructura = @IdRamaEstructura
END
GO