USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]
	@NumeroIdentificacion NUMERIC(30) = NULL,@PrimerNombre NVARCHAR(128) = NULL,@SegundoNombre NVARCHAR(128) = NULL,
	@PrimerApellido NVARCHAR(128) = NULL,@SegundoApellido NVARCHAR(128) = NULL,@Dependencia NVARCHAR(128) = NULL,@Email NVARCHAR(128) = NULL,@Estado BIT = NULL,
	@IdEntidad INT = NULL
AS
BEGIN
 SELECT CE.IdContacto, CE.IdEntidad, CE.IdSucursal, CE.IdTelContacto, CE.IdTipoDocIdentifica, CE.IdTipoCargoEntidad, CE.NumeroIdentificacion, CE.PrimerNombre, 
	CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido, CE.Dependencia, CE.Email, CE.Estado, CE.UsuarioCrea, CE.FechaCrea, CE.UsuarioModifica, CE.FechaModifica,
	TT.Movil AS Celular, TT.IndicativoTelefono, TT.NumeroTelefono, TT.ExtensionTelefono,
	TC.Descripcion AS Cargo, ISNULL(S.Nombre,'Ninguno') AS Sucursal
 FROM [Proveedor].[ContactoEntidad] AS CE
 LEFT JOIN [Oferente].[TelTerceros] AS TT ON CE.IdTelContacto = TT.IdTelTercero
 LEFT JOIN [Proveedor].[TipoDocIdentifica] AS TD ON CE.IdTipoDocIdentifica = TD.IdTipoDocIdentifica
 LEFT JOIN [Proveedor].[TipoCargoEntidad] AS TC ON CE.IdTipoCargoEntidad = TC.IdTipoCargoEntidad
 LEFT JOIN [Proveedor].[Sucursal] AS S ON CE.IdSucursal=S.IdSucursal
 WHERE NumeroIdentificacion = CASE WHEN @NumeroIdentificacion IS NULL THEN NumeroIdentificacion ELSE @NumeroIdentificacion END 
 AND CE.IdEntidad = CASE WHEN @IdEntidad IS NULL THEN CE.IdEntidad ELSE @IdEntidad END 
 AND PrimerNombre = CASE WHEN @PrimerNombre IS NULL THEN PrimerNombre ELSE @PrimerNombre END 
 AND SegundoNombre = CASE WHEN @SegundoNombre IS NULL THEN SegundoNombre ELSE @SegundoNombre END 
 AND PrimerApellido = CASE WHEN @PrimerApellido IS NULL THEN PrimerApellido ELSE @PrimerApellido END 
 AND SegundoApellido = CASE WHEN @SegundoApellido IS NULL THEN SegundoApellido ELSE @SegundoApellido END 
 AND Dependencia = CASE WHEN @Dependencia IS NULL THEN Dependencia ELSE @Dependencia END 
 AND Email = CASE WHEN @Email IS NULL THEN Email ELSE @Email END 
 AND CE.Estado = CASE WHEN @Estado IS NULL THEN CE.Estado ELSE @Estado END
 ORDER BY CE.PrimerNombre, CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido DESC
END
