USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Elimina CONSTRAINT de la tabla [Proveedor].[DocAdjuntoTercero]
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero] DROP CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Elimina la tabla [Proveedor].[DocAdjuntoTercero]
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO1]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero] DROP CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[DocAdjuntoTercero]    Script Date: 06/24/2013 12:44:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocAdjuntoTercero]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Crea la entidad Proveedor].[DocAdjuntoTercero]
-- =============================================
/****** Object:  Table [Proveedor].[DocAdjuntoTercero]    Script Date: 06/24/2013 12:44:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[DocAdjuntoTercero](
	[IDDOCADJUNTO] [int] IDENTITY(1,1) NOT NULL,
	[IDTERCERO] [int] NULL,
	[IDDOCUMENTO] [int] NULL,
	[DESCRIPCION] [nvarchar](128) NOT NULL,
	[LINKDOCUMENTO] [nvarchar](256) NOT NULL,
	[ANNO] [numeric](4, 0) NULL,
	[FECHACREA] [datetime] NOT NULL,
	[USUARIOCREA] [nvarchar](128) NOT NULL,
	[FECHAMODIFICA] [datetime] NULL,
	[USUARIOMODIFICA] [nvarchar](128) NULL,
 CONSTRAINT [PK_DOCADJUNTOTERCERO] PRIMARY KEY CLUSTERED 
(
	[IDDOCADJUNTO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[DocAdjuntoTercero]  WITH CHECK ADD  CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO] FOREIGN KEY([IDDOCUMENTO])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO

ALTER TABLE [Proveedor].[DocAdjuntoTercero] CHECK CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO]
GO

ALTER TABLE [Proveedor].[DocAdjuntoTercero]  WITH CHECK ADD  CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1] FOREIGN KEY([IDTERCERO])
REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
GO

ALTER TABLE [Proveedor].[DocAdjuntoTercero] CHECK CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1]
GO


