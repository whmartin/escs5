USE [SIA]
GO

-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Crea los Permisos para Gestion tercero y Notificaion Judicial
-- =============================================
--

DECLARE @IdModulo  INT;
DECLARE @IdPrograma INT;
DECLARE @Padre INT;
SET @Padre=(SELECT IdPrograma  FROM seg.programa WHERE CodigoPrograma = 'PROVEEDOR/TABLAPARAMETRICA')
SET @IdModulo = (SELECT IdModulo FROM SEG.Modulo WHERE NombreModulo = 'Proveedores')

INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@IdModulo, N'Tipo Documento Identificacion', N'Proveedor/TipoDocumentoPrograma', 0, 1, N'administrador', GETDATE(), NULL, NULL, 0, 1, @Padre)
SELECT @IdPrograma = @@IDENTITY 

INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 1, 1, N'Administrador', GETDATE(), NULL, NULL)



