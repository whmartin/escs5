USE [SIA]
go
drop table Proveedor.Revision
GO
alter table Proveedor.EntidadProvOferente add Finalizado bit null
GO
alter table Proveedor.InfoExperienciaEntidad add NroRevision int null
GO
alter table Proveedor.InfoExperienciaEntidad add Finalizado bit null
GO
alter table Proveedor.InfoFinancieraEntidad add NroRevision int null
go
alter table Proveedor.InfoFinancieraEntidad add Finalizado bit null
go
alter table Proveedor.ValidarInfoFinancieraEntidad add FechaModifica datetime NULL
go
alter table Proveedor.ValidarInfoFinancieraEntidad add UsuarioModifica nvarchar(128) NULL
GO
alter table Proveedor.ValidarInfoExperienciaEntidad add FechaModifica datetime NULL
go
alter table Proveedor.ValidarInfoExperienciaEntidad add UsuarioModifica nvarchar(128) NULL
GO
alter table Proveedor.ValidarInfoDatosBasicosEntidad add FechaModifica datetime NULL
go
alter table Proveedor.ValidarInfoDatosBasicosEntidad add UsuarioModifica nvarchar(128) NULL
GO
delete from Proveedor.ValidarInfoFinancieraEntidad
GO
delete from Proveedor.ValidarInfoDatosBasicosEntidad
GO
delete from Proveedor.ValidarInfoExperienciaEntidad 
