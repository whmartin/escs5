/****** Object:  Trigger [PROVEEDOR].[TG_Aud_EstadoProveedor]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para EstadoProveedor
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_EstadoProveedor]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_EstadoProveedor]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_EstadoProveedor]
  ON  [PROVEEDOR].[EstadoProveedor]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_EstadoProveedor]
  ([IdEstadoProveedor],[Descripcion_old], [Descripcion_new],[Estado_old], [Estado_new],[IdEstadoTercero_old], [IdEstadoTercero_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdEstadoProveedor,deleted.Descripcion,inserted.Descripcion,deleted.Estado,inserted.Estado,deleted.IdEstadoTercero,inserted.IdEstadoTercero,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdEstadoProveedor = deleted.IdEstadoProveedorEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_EstadoProveedor]
  ([IdEstadoProveedor],[Descripcion_old],[Estado_old],[IdEstadoTercero_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdEstadoProveedor,deleted.Descripcion,deleted.Estado,deleted.IdEstadoTercero,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_EstadoProveedor]
  ([IdEstadoProveedor],[Descripcion_new],[Estado_new],[IdEstadoTercero_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdEstadoProveedor,inserted.Descripcion,inserted.Estado,inserted.IdEstadoTercero,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO