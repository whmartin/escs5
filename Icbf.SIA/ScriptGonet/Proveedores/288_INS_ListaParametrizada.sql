USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Inserta un registro en la entidad [Proveedor].[TablaParametrica]
-- =============================================
DELETE FROM [Proveedor].[TablaParametrica]
WHERE [Url] = 'Proveedor/TipoDocumentoPrograma'

GO

INSERT INTO [Proveedor].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('18'
           ,'Tipo Documento Programa'
           ,'Proveedor/TipoDocumentoPrograma'
           ,1
           ,'administrador'
           ,getdate()
           ,null
           ,null)
GO



