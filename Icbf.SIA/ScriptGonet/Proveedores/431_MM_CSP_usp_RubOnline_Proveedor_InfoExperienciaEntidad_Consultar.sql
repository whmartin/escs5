USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]    Script Date: 07/31/2013 19:29:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]    Script Date: 07/31/2013 19:29:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]
	@IdExpEntidad INT
AS
BEGIN
 SELECT IdExpEntidad, IdEntidad, IdTipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, 
	IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, 
	FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, 
	PorcentParticipacion, AtencionDeptos, JardinOPreJardin, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, NroRevision, Finalizado
 FROM [Proveedor].[InfoExperienciaEntidad] 
 WHERE  IdExpEntidad = @IdExpEntidad
END


GO


