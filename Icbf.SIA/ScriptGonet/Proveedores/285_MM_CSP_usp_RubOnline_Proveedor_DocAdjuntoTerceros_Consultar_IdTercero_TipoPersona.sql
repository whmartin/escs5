USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]    Script Date: 07/02/2013 09:51:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]    Script Date: 07/02/2013 09:51:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Autor: Mauricio Martinez
--Fecha: 2013/07/02 09:52
--Descripcion: Consulta de documentos para terceros por IdTercero y Tipo Persona
CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona] 
( @IdTercero int, @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionTercero';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdTercero) as IdTercero,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		--Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdTercero,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             --d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas
FROM         Proveedor.DocAdjuntoTercero AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTercero = @IdTercero
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdTercero,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
		--	 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento



GO


