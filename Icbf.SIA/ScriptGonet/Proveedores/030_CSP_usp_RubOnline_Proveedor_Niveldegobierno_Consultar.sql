USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]    Script Date: 06/14/2013 19:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]
	@IdNiveldegobierno INT
AS
BEGIN
 SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Niveldegobierno] WHERE  IdNiveldegobierno = @IdNiveldegobierno
END
GO