USE [SIA]
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Seg.Par�metros
-- =============================================
GO

BEGIN TRANSACTION
GO
CREATE TABLE SEG.Tmp_Parametro
	(
	IdParametro int NOT NULL IDENTITY (1, 1),
	NombreParametro nvarchar(128) NOT NULL,
	ValorParametro nvarchar(256) NOT NULL,
	ImagenParametro nvarchar(256) NULL,
	Estado bit NOT NULL,
	Funcionalidad nvarchar(128) NULL,
	UsuarioCrea nvarchar(250) NOT NULL,
	FechaCrea datetime NOT NULL,
	UsuarioModifica nvarchar(250) NULL,
	FechaModifica datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE SEG.Tmp_Parametro SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT SEG.Tmp_Parametro ON
GO
IF EXISTS(SELECT * FROM SEG.Parametro)
	 EXEC('INSERT INTO SEG.Tmp_Parametro (IdParametro, NombreParametro, ValorParametro, ImagenParametro, Estado, Funcionalidad, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica)
		SELECT IdParametro, NombreParametro, ValorParametro, ImagenParametro, Estado, CONVERT(nvarchar(128), Funcionalidad), UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM SEG.Parametro WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT SEG.Tmp_Parametro OFF
GO
DROP TABLE SEG.Parametro
GO
EXECUTE sp_rename N'SEG.Tmp_Parametro', N'Parametro', 'OBJECT' 
GO
ALTER TABLE SEG.Parametro ADD CONSTRAINT
	PK_Parametro PRIMARY KEY CLUSTERED 
	(
	IdParametro
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT