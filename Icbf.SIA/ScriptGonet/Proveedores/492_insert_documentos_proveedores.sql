  USE [Proveedores]
  GO

  INSERT INTO [PROVEEDOR].[TipoDocumento] (CodigoTipoDocumento, Descripcion, Estado, UsuarioCrea, FechaCrea)
  VALUES ('015','Copia de la ley o el acto administrativo de creación',1,'administrador',getdate())
  GO

  INSERT INTO [PROVEEDOR].[TipoDocumento] (CodigoTipoDocumento, Descripcion, Estado, UsuarioCrea, FechaCrea)
  VALUES ('016','Documento que acredite el nombramiento y facultades del representante legal',1,'administrador',getdate())
  GO



  INSERT INTO [PROVEEDOR].[TipoDocumento] (CodigoTipoDocumento, Descripcion, Estado, UsuarioCrea, FechaCrea)
  VALUES ('017','Certificado de existencia y representación legal o de domicilio para entidades extranjeras',1,'administrador',getdate())
  GO

