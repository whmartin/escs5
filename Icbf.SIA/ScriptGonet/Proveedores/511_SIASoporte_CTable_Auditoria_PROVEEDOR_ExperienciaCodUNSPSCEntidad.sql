/****** Object:  Table [Auditoria].[PROVEEDOR_ExperienciaCodUNSPSCEntidad]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para ExperienciaCodUNSPSCEntidad ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_ExperienciaCodUNSPSCEntidad]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_ExperienciaCodUNSPSCEntidad]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_ExperienciaCodUNSPSCEntidad]([IdExpCOD] [int]  NULL, [IdTipoCodUNSPSC_old] [int]  NULL,[IdTipoCodUNSPSC_new] [int]  NULL,[IdExpEntidad_old] [int]  NULL,[IdExpEntidad_new] [int]  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO