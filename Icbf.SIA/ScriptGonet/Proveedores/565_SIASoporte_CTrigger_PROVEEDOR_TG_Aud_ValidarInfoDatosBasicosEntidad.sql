/****** Object:  Trigger [PROVEEDOR].[TG_Aud_ValidarInfoDatosBasicosEntidad]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para ValidarInfoDatosBasicosEntidad
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_ValidarInfoDatosBasicosEntidad]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_ValidarInfoDatosBasicosEntidad]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_ValidarInfoDatosBasicosEntidad]
  ON  [PROVEEDOR].[ValidarInfoDatosBasicosEntidad]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_ValidarInfoDatosBasicosEntidad]
  ([IdValidarInfoDatosBasicosEntidad],[NroRevision_old], [NroRevision_new],[IdEntidad_old], [IdEntidad_new],[ConfirmaYAprueba_old], [ConfirmaYAprueba_new],[Observaciones_old], [Observaciones_new],[FechaCrea_old], [FechaCrea_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaModifica_old], [FechaModifica_new],[UsuarioModifica_old], [UsuarioModifica_new],[CorreoEnviado_old], [CorreoEnviado_new],[Operacion])
SELECT deleted.IdValidarInfoDatosBasicosEntidad,deleted.NroRevision,inserted.NroRevision,deleted.IdEntidad,inserted.IdEntidad,deleted.ConfirmaYAprueba,inserted.ConfirmaYAprueba,deleted.Observaciones,inserted.Observaciones,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaModifica,inserted.FechaModifica,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.CorreoEnviado,inserted.CorreoEnviado,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdValidarInfoDatosBasicosEntidad = deleted.IdValidarInfoDatosBasicosEntidadEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_ValidarInfoDatosBasicosEntidad]
  ([IdValidarInfoDatosBasicosEntidad],[NroRevision_old],[IdEntidad_old],[ConfirmaYAprueba_old],[Observaciones_old],[FechaCrea_old],[UsuarioCrea_old],[FechaModifica_old],[UsuarioModifica_old],[CorreoEnviado_old],[Operacion])
SELECT deleted.IdValidarInfoDatosBasicosEntidad,deleted.NroRevision,deleted.IdEntidad,deleted.ConfirmaYAprueba,deleted.Observaciones,deleted.FechaCrea,deleted.UsuarioCrea,deleted.FechaModifica,deleted.UsuarioModifica,deleted.CorreoEnviado,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_ValidarInfoDatosBasicosEntidad]
  ([IdValidarInfoDatosBasicosEntidad],[NroRevision_new],[IdEntidad_new],[ConfirmaYAprueba_new],[Observaciones_new],[FechaCrea_new],[UsuarioCrea_new],[FechaModifica_new],[UsuarioModifica_new],[CorreoEnviado_new],[Operacion])
SELECT inserted.IdValidarInfoDatosBasicosEntidad,inserted.NroRevision,inserted.IdEntidad,inserted.ConfirmaYAprueba,inserted.Observaciones,inserted.FechaCrea,inserted.UsuarioCrea,inserted.FechaModifica,inserted.UsuarioModifica,inserted.CorreoEnviado,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO