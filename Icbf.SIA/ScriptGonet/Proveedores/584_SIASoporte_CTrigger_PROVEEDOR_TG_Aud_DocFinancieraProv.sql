/****** Object:  Trigger [PROVEEDOR].[TG_Aud_DocFinancieraProv]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para DocFinancieraProv
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_DocFinancieraProv]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_DocFinancieraProv]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_DocFinancieraProv]
  ON  [PROVEEDOR].[DocFinancieraProv]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_DocFinancieraProv]
  ([IdDocAdjunto],[IdInfoFin_old], [IdInfoFin_new],[NombreDocumento_old], [NombreDocumento_new],[LinkDocumento_old], [LinkDocumento_new],[Observaciones_old], [Observaciones_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[IdTipoDocumento_old], [IdTipoDocumento_new],[IdTemporal_old], [IdTemporal_new],[Operacion])
SELECT deleted.IdDocAdjunto,deleted.IdInfoFin,inserted.IdInfoFin,deleted.NombreDocumento,inserted.NombreDocumento,deleted.LinkDocumento,inserted.LinkDocumento,deleted.Observaciones,inserted.Observaciones,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,deleted.IdTipoDocumento,inserted.IdTipoDocumento,deleted.IdTemporal,inserted.IdTemporal,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdDocAdjunto = deleted.IdDocAdjuntoEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_DocFinancieraProv]
  ([IdDocAdjunto],[IdInfoFin_old],[NombreDocumento_old],[LinkDocumento_old],[Observaciones_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[IdTipoDocumento_old],[IdTemporal_old],[Operacion])
SELECT deleted.IdDocAdjunto,deleted.IdInfoFin,deleted.NombreDocumento,deleted.LinkDocumento,deleted.Observaciones,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,deleted.IdTipoDocumento,deleted.IdTemporal,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_DocFinancieraProv]
  ([IdDocAdjunto],[IdInfoFin_new],[NombreDocumento_new],[LinkDocumento_new],[Observaciones_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[IdTipoDocumento_new],[IdTemporal_new],[Operacion])
SELECT inserted.IdDocAdjunto,inserted.IdInfoFin,inserted.NombreDocumento,inserted.LinkDocumento,inserted.Observaciones,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,inserted.IdTipoDocumento,inserted.IdTemporal,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO