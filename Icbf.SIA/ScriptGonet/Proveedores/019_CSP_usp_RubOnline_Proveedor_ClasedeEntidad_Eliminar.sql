USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]    Script Date: 06/14/2013 19:31:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que elimina un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]
	@IdClasedeEntidad INT
AS
BEGIN
	DELETE Proveedor.ClasedeEntidad WHERE IdClasedeEntidad = @IdClasedeEntidad
END
GO