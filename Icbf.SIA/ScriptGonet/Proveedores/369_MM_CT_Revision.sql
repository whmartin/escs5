USE [SIA]
GO

/****** Object:  Table [Proveedor].[Revision]    Script Date: 07/24/2013 17:24:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Revision]') AND type in (N'U'))
DROP TABLE [Proveedor].[Revision]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[Revision]    Script Date: 07/24/2013 17:24:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[Revision](
	[IdRevision] [int] NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[Finalizado] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Revision] PRIMARY KEY CLUSTERED 
(
	[IdRevision] ASC,
	[IdEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-iniciado 1-finalizado' , @level0type=N'SCHEMA',@level0name=N'Proveedor', @level1type=N'TABLE',@level1name=N'Revision', @level2type=N'COLUMN',@level2name=N'Finalizado'
GO


