

USE [SIA]
GO

/****** Object:  Table [Proveedor].[RamaoEstructura_Niveldegobierno]    Script Date: 07/23/2013 21:38:59 ******/
SET ANSI_NULLS ON
GO
/****** Object:  Table [Proveedor].[RamaoEstructura_Niveldegobierno]    Script Date: 07/23/2013 23:14:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura_Niveldegobierno]') AND type in (N'U'))
DROP TABLE [Proveedor].[RamaoEstructura_Niveldegobierno]
GO
/****** Object:  Table [Proveedor].[RamaoEstructura_Niveldegobierno]    Script Date: 07/23/2013 23:14:28 ******/

-- =============================================
-- Author:		Faiber Losada
-- Create date:  23/07/2013 11:30:20 PM
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura_Niveldegobierno]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[RamaoEstructura_Niveldegobierno](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdRamaEstructura] [int] NOT NULL,
	[IdNiveldegobierno] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
 CONSTRAINT [PK_RamaoEstructura_Niveldegobierno] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[IdNiveldegobierno] ASC,
	[IdRamaEstructura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ON
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (1, 1, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (2, 1, 2, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (3, 1, 3, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (4, 1, 4, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (5, 2, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (6, 3, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (7, 4, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (8, 4, 4, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (9, 4, 3, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (10, 5, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (11, 6, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (12, 6, 2, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (13, 6, 3, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (14, 6, 4, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] OFF


