USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]    Script Date: 06/14/2013 19:32:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All') IS NOT NULL
BEGIN
	DROP PROCEDURE usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All
END
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]
	AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipodeentidadPublica] 
 WHERE  Estado = 1 
END
GO