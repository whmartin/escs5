USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]    Script Date: 06/24/2013 12:29:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]    Script Date: 06/24/2013 12:29:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocAdjuntoTercero WHERE IdDocAdjunto = @IdDocAdjunto 
END

GO


