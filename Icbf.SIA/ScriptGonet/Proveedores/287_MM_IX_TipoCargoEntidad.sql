USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina indice NombreUnico de la entidad [Proveedor].[TipoCargoEntidad]
-- =============================================
/****** Object:  Index [NombreUnico]    Script Date: 07/02/2013 10:35:23 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCargoEntidad]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[TipoCargoEntidad] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Crea indice NombreUnico en la entidad [Proveedor].[TipoCargoEntidad]
-- =============================================
/****** Object:  Index [NombreUnico]    Script Date: 07/02/2013 10:35:25 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoCargoEntidad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


