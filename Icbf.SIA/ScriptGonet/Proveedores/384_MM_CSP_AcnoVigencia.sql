USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]    Script Date: 07/26/2013 11:15:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]    Script Date: 07/26/2013 11:15:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  20/06/2013
-- Description:	Procedimiento almacenado que consulta las vigencias sin incluir el a�o actual
-- =============================================
create PROCEDURE [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]
	@Activo NVARCHAR(1) = NULL
AS
BEGIN
 SELECT TOP 5 IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Global].[Vigencia] 
 WHERE AcnoVigencia < Year(getdate())
 AND Activo = CASE WHEN @Activo IS NULL THEN Activo ELSE @Activo END
 ORDER BY AcnoVigencia DESC
END

GO


