-- =============================================
-- Author:		Carlos Cubillos
-- Description:	Se cambia el estado Datos Parciales que no se usaba por Registrado que será el estado inicial del proveedor
-- =============================================


update [Proveedor].[EstadoDatosBasicos]  set Descripcion='REGISTRADO' WHERE Descripcion='DATOS PARCIALES'