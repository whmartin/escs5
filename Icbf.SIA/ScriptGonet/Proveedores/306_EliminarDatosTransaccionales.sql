USE [SIA]
GO

-- =============================================
-- Author:          Gonet\Bayron Lara
-- Create date:     16/07/2013 04:56:29 p.m.
-- Description:     Limpia los datos de las tablas de proveedores
-- =============================================


--depgoen de modulos de entidad proveedor
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]') AND type in (N'U'))
delete from Proveedor.DocAdjuntoTercero
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]') AND type in (N'U'))
delete from Proveedor.DocDatosBasicoProv
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]') AND type in (N'U'))
delete from Proveedor.DocExperienciaEntidad
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]') AND type in (N'U'))
delete from Proveedor.DocFinancieraProv
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]') AND type in (N'U'))
delete from Proveedor.DocNotificacionJudicial
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]') AND type in (N'U'))
delete from Proveedor.ExperienciaCodUNSPSCEntidad
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoDatosBasicosEntidad]') AND type in (N'U'))
delete from Proveedor.ValidarInfoDatosBasicosEntidad
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoExperienciaEntidad]') AND type in (N'U'))
delete from Proveedor.ValidarInfoExperienciaEntidad
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoFinancieraEntidad]') AND type in (N'U'))
delete from Proveedor.ValidarInfoFinancieraEntidad
go

--depgoen de EntidadProvOferente
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]') AND type in (N'U'))
delete from Proveedor.InfoAdminEntidad
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]') AND type in (N'U'))
delete from Proveedor.InfoExperienciaEntidad
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]') AND type in (N'U'))
delete from Proveedor.InfoFinancieraEntidad
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ContactoEntidad]') AND type in (N'U'))
delete from Proveedor.ContactoEntidad
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Sucursal]') AND type in (N'U'))
delete from Proveedor.Sucursal
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]') AND type in (N'U'))
delete from Proveedor.NotificacionJudicial
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]') AND type in (N'U'))
delete from Proveedor.EntidadProvOferente
go
--depgoen de Tercero
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[MotivoCambioEstado]') AND type in (N'U'))
delete from Proveedor.MotivoCambioEstado
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarTercero]') AND type in (N'U'))
delete from Proveedor.ValidarTercero
go
