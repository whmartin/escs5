USE [Proveedores]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]    Script Date: 09/04/2013 17:34:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]
GO

USE [Proveedores]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]    Script Date: 09/04/2013 17:34:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]
@IdTipoDocumentoPrograma INT, @IdTipoDocumento INT, @IdPrograma INT, @Estado INT, @MaxPermitidoKB INT, @ExtensionesPermitidas NVARCHAR (50), @ObligRupNoRenovado INT, @ObligRupRenovado INT, @ObligPersonaJuridica INT, @ObligPersonaNatural INT, @ObligSectorPrivado INT, @ObligSectorPublico INT, @UsuarioModifica NVARCHAR (250)
AS
BEGIN
UPDATE Proveedor.TipoDocumentoPrograma
SET	IdTipoDocumento = @IdTipoDocumento,
	IdPrograma = @IdPrograma,
	Estado = @Estado,
	MaxPermitidoKB = @MaxPermitidoKB,
	ExtensionesPermitidas = @ExtensionesPermitidas,
	ObligRupNoRenovado = @ObligRupNoRenovado,
	ObligRupRenovado = @ObligRupRenovado,
	ObligPersonaJuridica = @ObligPersonaJuridica,
	ObligPersonaNatural = @ObligPersonaNatural,
	ObligSectorPublico = @ObligSectorPublico,
	ObligSectorPrivado = @ObligSectorPrivado,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma
END
GO


