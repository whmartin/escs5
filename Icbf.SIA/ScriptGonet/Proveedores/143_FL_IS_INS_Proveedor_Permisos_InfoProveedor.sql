USE [SIA]
-- =============================================
-- Author:		Faiber Losada 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Permisos infoProveedor
-- =============================================
GO
DECLARE @idModulo INT;
SELECT @idModulo = IdModulo FROM [SEG].[Modulo] WHERE [NombreModulo] = 'Proveedores'

DECLARE @IdPrograma INT;

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/GestionProveedores') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Gestion Proveedores', N'PROVEEDOR/GestionProveedores', 0, 1, N'administrador', CAST(0x0000A1D600F96838 AS DateTime), NULL, NULL, 1, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D60100BFEA AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/TipoSectorEntidad') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'TipoSectorEntidad', N'PROVEEDOR/TipoSectorEntidad', 0, 1, N'administrador', CAST(0x0000A1D900F96838 AS DateTime), NULL, NULL, 0, 1, 265)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D900B6D37D AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/TIPOREGIMENTRIBUTARIO') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Tipo Regimen Tributario', N'PROVEEDOR/TIPOREGIMENTRIBUTARIO', 0, 1, N'administrador', CAST(0x0000A1D900F96838 AS DateTime), NULL, NULL, 0, 1, 265)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1D900E3A9E5 AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/TIPOENTIDAD') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Tipo Entidad', N'PROVEEDOR/TIPOENTIDAD', 0, 1, N'administrador', CAST(0x0000A1D600F96838 AS DateTime), NULL, NULL, 0, 1, 265)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DB00AD0579 AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/RAMAOESTRUCTURA') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Rama o Estructura', N'PROVEEDOR/RAMAOESTRUCTURA', 0, 1, N'administrador', CAST(0x0000A1D600F96838 AS DateTime), NULL, NULL, 0, 1, 265)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma,1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DB00D13491 AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/TIPODEENTIDADPUBLICA') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Tipode Entidad Publica', N'PROVEEDOR/TIPODEENTIDADPUBLICA', 0, 1, N'administrador', CAST(0x0000A1D600F96838 AS DateTime), NULL, NULL, 0, 1, 265)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DB010BC5B6 AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/TIPODEACTIVIDAD') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Tipo de Actividad', N'PROVEEDOR/TIPODEACTIVIDAD', 0, 1, N'administrador', CAST(0x0000A1D600F96838 AS DateTime), NULL, NULL, 0, 1, 265)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DC013A4390 AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/CLASEDEENTIDAD') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Clase de Entidad', N'PROVEEDOR/CLASEDEENTIDAD', 0, 1, N'administrador', CAST(0x0000A1D600F96838 AS DateTime), NULL, NULL, 0, 1, 265)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1DC014A3DD8 AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/EstadoDatosBasicos') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@idModulo, N'Estado Datos Basicos', N'PROVEEDOR/EstadoDatosBasicos', 0, 1, N'administrador', CAST(0x0000A1D600F96838 AS DateTime), NULL, NULL, 0, 1, 265)
SELECT @IdPrograma = @@IDENTITY 
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', CAST(0x0000A1E300045549 AS DateTime), NULL, NULL)
END

