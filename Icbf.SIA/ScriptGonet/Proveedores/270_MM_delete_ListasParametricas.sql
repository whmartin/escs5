USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina un registro de la entidad [Proveedor].[TablaParametrica]
-- =============================================
delete 
  FROM [Proveedor].[TablaParametrica]
  where Url not in ('PROVEEDOR/TIPOCARGOENTIDAD','PROVEEDOR/TIPOENTIDAD','PROVEEDOR/TIPODEENTIDADPUBLICA','Proveedor/TipoDocumentoPrograma')