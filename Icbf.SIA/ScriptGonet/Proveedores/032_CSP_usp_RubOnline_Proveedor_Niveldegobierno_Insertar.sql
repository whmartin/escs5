USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]    Script Date: 06/14/2013 19:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]
		@IdNiveldegobierno INT OUTPUT, 	@CodigoNiveldegobierno NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Niveldegobierno(CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNiveldegobierno, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNiveldegobierno = @@IDENTITY 		
END
GO