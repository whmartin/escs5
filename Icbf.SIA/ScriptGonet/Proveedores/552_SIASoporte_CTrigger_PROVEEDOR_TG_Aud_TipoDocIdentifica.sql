/****** Object:  Trigger [PROVEEDOR].[TG_Aud_TipoDocIdentifica]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para TipoDocIdentifica
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_TipoDocIdentifica]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_TipoDocIdentifica]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_TipoDocIdentifica]
  ON  [PROVEEDOR].[TipoDocIdentifica]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_TipoDocIdentifica]
  ([IdTipoDocIdentifica],[CodigoDocIdentifica_old], [CodigoDocIdentifica_new],[Descripcion_old], [Descripcion_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdTipoDocIdentifica,deleted.CodigoDocIdentifica,inserted.CodigoDocIdentifica,deleted.Descripcion,inserted.Descripcion,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdTipoDocIdentifica = deleted.IdTipoDocIdentificaEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_TipoDocIdentifica]
  ([IdTipoDocIdentifica],[CodigoDocIdentifica_old],[Descripcion_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdTipoDocIdentifica,deleted.CodigoDocIdentifica,deleted.Descripcion,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_TipoDocIdentifica]
  ([IdTipoDocIdentifica],[CodigoDocIdentifica_new],[Descripcion_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdTipoDocIdentifica,inserted.CodigoDocIdentifica,inserted.Descripcion,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO