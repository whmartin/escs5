USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]    Script Date: 06/14/2013 19:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Fabian Valencia>
-- Create date: <12/06/2013>
-- Description:	<Obtiene el estado a trav�s de su id>
-- =============================================
Create PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]
	 @IdTercero INT
AS
BEGIN
	SELECT     Oferente.EstadoTercero.*
	FROM         Oferente.EstadoTercero
	WHERE		Oferente.EstadoTercero.IdEstadoTercero = @IdTercero
END
GO