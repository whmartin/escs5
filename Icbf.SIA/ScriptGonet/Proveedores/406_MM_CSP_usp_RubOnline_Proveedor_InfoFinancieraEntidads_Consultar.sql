USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]    Script Date: 07/29/2013 16:08:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]    Script Date: 07/29/2013 16:08:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]
	@IdEntidad INT = NULL, @IdVigencia INT = NULL
AS
BEGIN
 SELECT IdInfoFin, IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, Finalizado
 FROM [Proveedor].[InfoFinancieraEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END 
 ORDER BY IdVigencia DESC 
END

GO


