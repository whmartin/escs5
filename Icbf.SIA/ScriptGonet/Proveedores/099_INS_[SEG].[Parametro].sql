USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[SEG].[FK_Parametro_Modulo]') AND parent_object_id = OBJECT_ID(N'[SEG].[Parametro]'))
ALTER TABLE [SEG].[Parametro] DROP CONSTRAINT [FK_Parametro_Modulo]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Crea la entidad [SEG].[Parametro]
-- =============================================
/****** Object:  Table [SEG].[Parametro]    Script Date: 06/15/2013 00:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SEG].[Parametro]') AND type in (N'U'))
DROP TABLE [SEG].[Parametro]
GO

CREATE TABLE [SEG].[Parametro](
	[IdParametro] [int] IDENTITY(1,1) NOT NULL,
	[NombreParametro] [nvarchar](128) NOT NULL,
	[ValorParametro] [nvarchar](256) NOT NULL,
	[ImagenParametro] [nvarchar](256) NULL,
	[Estado] [bit] NOT NULL,
	[Funcionalidad] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Parametro] PRIMARY KEY CLUSTERED 
(
	[IdParametro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [SEG].[Parametro]  WITH CHECK ADD  CONSTRAINT [FK_Parametro_Modulo] FOREIGN KEY([Funcionalidad])
REFERENCES [SEG].[Modulo] ([IdModulo])
GO

ALTER TABLE [SEG].[Parametro] CHECK CONSTRAINT [FK_Parametro_Modulo]
GO


SET IDENTITY_INSERT [SEG].[Parametro] ON
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'MENSAJE DE BIENVENIDA', N'MENSAJE DE BIENVENIDA', N'2013_6_14_1_45_33_Parametro_ICBF.jpg', 1, 1, N'DesarrolloGoNet14', CAST(0x0000A1D90101DE3F AS DateTime), N'Administrador', CAST(0x0000A1DD001DD0F9 AS DateTime))
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'Instrucciones de acceso al sistema, en el mensaje de bienvenida', N'Instrucciones de acceso al sistema, en el mensaje de bienvenida', NULL, 1, 1, N'DesarrolloGoNet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (6, N'Cantidad de vigencias a  mostrar', N'Cantidad de vigencias a  mostrar', NULL, 1, 1, N'DesarrolloGotNet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (7, N'T�tulo de la notificaci�n de bienvenida', N'T�tulo de la notificaci�n de bienvenida', NULL, 1, 1, N'DesarrolloGoNet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (8, N'Mensaje de tiempo para la activaci�n de la cuenta de usuario', N'Mensaje de tiempo para la activaci�n de la cuenta de usuario', NULL, 1, 1, N'DesarrolloGonet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (9, N'T�tulo de bienvenida del registro de persona natural', N'T�tulo de bienvenida del registro de persona natural', NULL, 1, 1, N'DesarrolloGonet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (10, N'Mensaje registro de persona natural', N'Mensaje registro de persona natural', NULL, 1, 1, N'DesarrolloGoNet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (11, N'Meses para eliminar el registro de usuarios', N'Meses para eliminar el registro de usuarios', NULL, 1, 1, N'DesarrolloGoNet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (13, N'Mensaje �para qu� me registro?', N'Mensaje �para qu� me registro?', NULL, 1, 1, N'DesarrolloGoNet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (14, N'Mensaje especificaciones documento adjunto', N'Mensaje especificaciones documento adjunto', NULL, 1, 1, N'DesarrolloGoNet14', CAST(0x0000A1DB00000000 AS DateTime), NULL, NULL)
INSERT [SEG].[Parametro] ([IdParametro], [NombreParametro], [ValorParametro], [ImagenParametro], [Estado], [Funcionalidad], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (16, N'PoliticasContrasena', N'La contrase�a requiere por lo menos una letra may�scula, una letra minuscula, por lo menos un numero, m�nimo 6 caracteres, por lo menos un caracter especial.', NULL, 1, 1, N'Adminsitrador', CAST(0x0000A1DD011D4FB2 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [SEG].[Parametro] OFF