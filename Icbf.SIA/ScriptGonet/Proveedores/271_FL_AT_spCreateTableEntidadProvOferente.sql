USE [SIA]
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/28/2013 9:24:30 AM
-- Description:se adiciona el campo ConsecutivoInterno
-- =============================================
USE [SIA]
GO
 ALTER TABLE proveedor.EntidadProvOferente
  ADD ConsecutivoInterno NVARCHAR(128);
GO


