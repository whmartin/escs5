USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]    Script Date: 07/29/2013 10:42:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]    Script Date: 07/29/2013 10:42:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  12/07/2013 22:42:55 
-- Description:	Procedimiento almacenado que consulta Proveedores para Validar
-- 
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar] 
	@IdEstado INT = NULL,
	@IdTipoPersona INT = NULL,
	@IDTIPODOCIDENTIFICA INT= NULL,
	@NUMEROIDENTIFICACION BIGINT = NULL,
	@IdTipoCiiuPrincipal INT = NULL,
	@IdMunicipioDirComercial INT = NULL,
	@IdDepartamentoDirComercial INT = NULL,
	@IdTipoSector INT = NULL,
	@IdTipoRegimenTributario INT = NULL,
	@Proveedor NVARCHAR(256) = NULL	
	
AS
BEGIN
SELECT     EP.IdEntidad, EP.ConsecutivoInterno, T.IdTipoPersona, TP.NombreTipoPersona, TD.CodDocumento, T.IDTIPODOCIDENTIFICA, 
                      T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, LTRIM(RTRIM(ISNULL(T.PRIMERNOMBRE, '') + ' ' + ISNULL(T.SEGUNDONOMBRE, '') 
                      + ' ' + ISNULL(T.PRIMERAPELLIDO, '') + ' ' + ISNULL(T.SEGUNDOAPELLIDO, '') + ' ' + ISNULL(T.RAZONSOCIAL, ''))) AS Razonsocial, E.Descripcion AS Estado, 
                      EP.IdEstado, EP.UsuarioCrea, EP.IdTipoCiiuPrincipal, 
                      Proveedor.TipoCiiu.CodigoCiiu + '-' + Proveedor.TipoCiiu.Descripcion AS ActividadCiiuPrincipal, Proveedor.TipoSectorEntidad.IdTipoSectorEntidad, 
                      Proveedor.TipoSectorEntidad.Descripcion AS SectorEntidad, Proveedor.TipoRegimenTributario.IdTipoRegimenTributario, 
                      Proveedor.TipoRegimenTributario.Descripcion AS RegimenTributario, EP.IdTipoSector, DIV.Municipio.NombreMunicipio, DIV.Departamento.NombreDepartamento, 
                      IAE.IdMunicipioDirComercial, DIV.Municipio.IdDepartamento, EP.FechaModifica
FROM         Proveedor.EntidadProvOferente AS EP INNER JOIN
                      Oferente.TERCERO AS T ON EP.IdTercero = T.IDTERCERO INNER JOIN
                      Proveedor.InfoAdminEntidad AS IAE ON EP.IdEntidad = IAE.IdEntidad INNER JOIN
                      Oferente.TipoPersona AS TP ON T.IdTipoPersona = TP.IdTipoPersona INNER JOIN
                      Global.TiposDocumentos AS TD ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento INNER JOIN
                      Proveedor.EstadoDatosBasicos AS E ON EP.IdEstado = E.IdEstadoDatosBasicos INNER JOIN
                      Proveedor.TipoCiiu ON EP.IdTipoCiiuPrincipal = Proveedor.TipoCiiu.IdTipoCiiu AND EP.IdTipoCiiuPrincipal = Proveedor.TipoCiiu.IdTipoCiiu INNER JOIN
                      Proveedor.TipoSectorEntidad ON EP.IdTipoSector = Proveedor.TipoSectorEntidad.IdTipoSectorEntidad AND 
                      EP.IdTipoSector = Proveedor.TipoSectorEntidad.IdTipoSectorEntidad INNER JOIN
                      Proveedor.TipoRegimenTributario ON IAE.IdTipoRegTrib = Proveedor.TipoRegimenTributario.IdTipoRegimenTributario AND 
                      IAE.IdTipoRegTrib = Proveedor.TipoRegimenTributario.IdTipoRegimenTributario INNER JOIN
                      DIV.Municipio INNER JOIN
                      DIV.Departamento ON DIV.Municipio.IdDepartamento = DIV.Departamento.IdDepartamento AND DIV.Municipio.IdDepartamento = DIV.Departamento.IdDepartamento ON 
                      IAE.IdMunicipioDirComercial = DIV.Municipio.IdMunicipio
WHERE (EP.IdEstado = CASE WHEN @IdEstado IS NULL THEN EP.IdEstado ELSE @IdEstado END )
AND (T.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN T.IdTipoPersona ELSE @IdTipoPersona END )--1 
AND (T.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN T.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END ) 
AND (T.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN T.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END) 
AND (EP.IdTipoCiiuPrincipal = CASE WHEN @IdTipoCiiuPrincipal IS NULL THEN EP.IdTipoCiiuPrincipal ELSE @IdTipoCiiuPrincipal END) 
AND (IAE.IdMunicipioDirComercial = CASE WHEN @IdMunicipioDirComercial IS NULL THEN IAE.IdMunicipioDirComercial ELSE @IdMunicipioDirComercial END) 
AND (IAE.IdDepartamentoDirComercial = CASE WHEN @IdDepartamentoDirComercial IS NULL THEN IAE.IdDepartamentoDirComercial ELSE @IdDepartamentoDirComercial END) 
AND (EP.IdTipoSector = CASE WHEN @IdTipoSector IS NULL THEN EP.IdTipoSector ELSE @IdTipoSector END) 
AND (IAE.IdTipoRegTrib = CASE WHEN @IdTipoRegimenTributario IS NULL THEN IAE.IdTipoRegTrib ELSE @IdTipoRegimenTributario END) 
AND (
					T.PRIMERNOMBRE LIKE CASE WHEN @Proveedor IS NULL THEN T.PRIMERNOMBRE ELSE  '%'+ @Proveedor +'%' END
					OR T.SEGUNDONOMBRE  LIKE CASE WHEN @Proveedor IS NULL THEN T.SEGUNDONOMBRE ELSE  '%'+ @Proveedor +'%' END
					OR T.PRIMERAPELLIDO LIKE CASE WHEN @Proveedor IS NULL THEN T.PRIMERAPELLIDO ELSE  '%'+ @Proveedor +'%' END
					OR T.SEGUNDOAPELLIDO LIKE CASE WHEN @Proveedor IS NULL THEN T.SEGUNDOAPELLIDO ELSE  '%'+ @Proveedor +'%' END
					OR T.RAZONSOCIAL  LIKE CASE WHEN @Proveedor IS NULL THEN T.RAZONSOCIAL ELSE  '%'+ @Proveedor +'%' END
					OR ISNULL(T.PRIMERNOMBRE,'')+' '+ISNULL(T.SEGUNDONOMBRE,'')+' '+ISNULL(T.PRIMERAPELLIDO,'')+' '+ISNULL(T.SEGUNDOAPELLIDO,'') LIKE '%'+RTRIM(LTRIM(@Proveedor))+'%'
					)
END

GO


