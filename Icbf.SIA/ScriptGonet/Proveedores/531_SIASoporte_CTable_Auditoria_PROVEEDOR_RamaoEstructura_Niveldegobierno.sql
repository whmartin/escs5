/****** Object:  Table [Auditoria].[PROVEEDOR_RamaoEstructura_Niveldegobierno]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para RamaoEstructura_Niveldegobierno ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_RamaoEstructura_Niveldegobierno]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_RamaoEstructura_Niveldegobierno]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_RamaoEstructura_Niveldegobierno]([Id] [int]  NULL, [IdRamaEstructura] [int]  NULL, [IdNiveldegobierno] [int]  NULL, [Estado_old] [bit]  NULL,[Estado_new] [bit]  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO