USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en  [Proveedor].[TipoRegimenTributario]
-- =============================================
SET IDENTITY_INSERT [Proveedor].[TipoRegimenTributario] ON
INSERT [Proveedor].[TipoRegimenTributario] ([IdTipoRegimenTributario], [CodigoRegimenTributario], [Descripcion], [IdTipoPersona], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'SIMPLIFICADO', 1, 1, N'Administrador', CAST(0x0000A1D900EA2B10 AS DateTime), N'Administrador', CAST(0x0000A1D900F82B2F AS DateTime))
INSERT [Proveedor].[TipoRegimenTributario] ([IdTipoRegimenTributario], [CodigoRegimenTributario], [Descripcion], [IdTipoPersona], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'COM�N', 1, 1, N'Administrador', CAST(0x0000A1D900F8618D AS DateTime), N'Administrador', CAST(0x0000A1D900F82B2F AS DateTime))
INSERT [Proveedor].[TipoRegimenTributario] ([IdTipoRegimenTributario], [CodigoRegimenTributario], [Descripcion], [IdTipoPersona], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'003', N'ESPECIAL', 1, 1, N'Administrador', CAST(0x0000A1D900F8779E AS DateTime), N'Administrador', CAST(0x0000A1D900F82B2F AS DateTime))
INSERT [Proveedor].[TipoRegimenTributario] ([IdTipoRegimenTributario], [CodigoRegimenTributario], [Descripcion], [IdTipoPersona], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, N'004', N'COM�N', 2, 1, N'Administrador', CAST(0x0000A1D900F89D17 AS DateTime), N'Administrador', CAST(0x0000A1D900F82B2F AS DateTime))
INSERT [Proveedor].[TipoRegimenTributario] ([IdTipoRegimenTributario], [CodigoRegimenTributario], [Descripcion], [IdTipoPersona], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'005', N'ESPECIAL', 2, 1, N'Administrador', CAST(0x0000A1D900F8BD44 AS DateTime), N'Administrador', CAST(0x0000A1D900F82B2F AS DateTime))
SET IDENTITY_INSERT [Proveedor].[TipoRegimenTributario] OFF