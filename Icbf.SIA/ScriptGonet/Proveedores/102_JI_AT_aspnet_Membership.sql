USE QoS
GO
-- ==========================================================================================
-- Author:		 Jos� Ignacio De Los Reyes
-- Create date:  14/06/2013 18:43:19 AM
-- Description:	 Se Crea Campo Para Validar que El correo sea enviado una sola vez por usuario
-- ==========================================================================================
IF NOT EXISTS(SELECT * 
			  FROM sys.tables 
				INNER JOIN sys.columns ON sys.tables.object_id = columns.object_id
			  WHERE tables.name = 'aspnet_Membership'
				AND columns.name = 'EnvioCorreo')
BEGIN
	ALTER table aspnet_Membership add EnvioCorreo Bit not null default(0)
END--FIN
GO