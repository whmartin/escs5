/****** Object:  Table [Auditoria].[PROVEEDOR_ValidarTercero]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para ValidarTercero ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_ValidarTercero]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_ValidarTercero]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_ValidarTercero]([IdValidarTercero] [int]  NULL, [IdTercero_old] [int]  NULL,[IdTercero_new] [int]  NULL,[ConfirmaYAprueba_old] [bit]  NULL,[ConfirmaYAprueba_new] [bit]  NULL,[Observaciones_old] [nvarchar] (200)  NULL,[Observaciones_new] [nvarchar] (200)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioCrea_old] [nvarchar] (128)  NULL,[UsuarioCrea_new] [nvarchar] (128)  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO