/****** Object:  Table [Auditoria].[PROVEEDOR_Rel_RamaGobNivelOrgYTipoEnt]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para Rel_RamaGobNivelOrgYTipoEnt ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_Rel_RamaGobNivelOrgYTipoEnt]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_Rel_RamaGobNivelOrgYTipoEnt]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_Rel_RamaGobNivelOrgYTipoEnt]([IdRelacion] [int]  NULL, [IdRamaEstructura_old] [int]  NULL,[IdRamaEstructura_new] [int]  NULL,[IdNivelGobierno_old] [int]  NULL,[IdNivelGobierno_new] [int]  NULL,[IdNivelOrganizacional_old] [int]  NULL,[IdNivelOrganizacional_new] [int]  NULL,[IdTipodeentidadPublica_old] [int]  NULL,[IdTipodeentidadPublica_new] [int]  NULL,[Estado_old] [bit]  NULL,[Estado_new] [bit]  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO