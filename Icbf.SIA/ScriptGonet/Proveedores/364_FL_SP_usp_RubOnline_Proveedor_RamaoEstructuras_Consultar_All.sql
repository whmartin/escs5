USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]    Script Date: 07/24/2013 11:57:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]    Script Date: 07/24/2013 11:57:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]
	
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[RamaoEstructura] 
 WHERE Estado = 1
 ORDER BY Descripcion ASC
  END



GO


