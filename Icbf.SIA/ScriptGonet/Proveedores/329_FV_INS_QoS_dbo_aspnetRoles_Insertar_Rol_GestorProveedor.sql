USE QoS
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Se asegura de que exista el Rol 'GESTOR PROVEEDOR'  en la entidad [QoS].[dbo].[aspnet_Roles]
-- =============================================
IF NOT EXISTS (SELECT 1 FROM [QoS].[dbo].[aspnet_Roles] 
				WHERE [RoleName] = 'GESTOR PROVEEDOR' 
				AND  [ApplicationId] = (SELECT ApplicationId  FROM aspnet_Applications WHERE ApplicationName = 'RUBO'))
BEGIN

INSERT INTO [QoS].[dbo].[aspnet_Roles]
           ([ApplicationId]          
           ,[RoleName]
           ,[LoweredRoleName]
           ,[Description])           
     VALUES
           ((SELECT ApplicationId  FROM aspnet_Applications WHERE ApplicationName = 'RUBO')      
           ,'GESTOR PROVEEDOR'
           ,'GESTOR PROVEEDOR'
           ,'Usuarios gestores proveedores que se registran internamente') 
END          
GO