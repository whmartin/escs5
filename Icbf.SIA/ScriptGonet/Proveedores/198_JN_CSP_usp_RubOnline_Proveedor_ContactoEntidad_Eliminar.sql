USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]
GO
-- =============================================
-- Author:		Jonnathan NI�o
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que elimina un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]
	@IdContacto INT
AS
BEGIN
	DELETE Proveedor.ContactoEntidad WHERE IdContacto = @IdContacto
END
