USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]
GO

-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]
	@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCargoEntidad] 
 WHERE Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
