USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar]    Script Date: 06/23/2013 22:14:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar]    Script Date: 06/23/2013 22:14:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/4/2013 11:29:17 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TelTerceros
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar]
		@IdTelTercero INT OUTPUT, 	
		@IdTercero INT,	
		@IndicativoTelefono INT=NULL,	
		@NumeroTelefono INT=NULL,	
		@ExtensionTelefono BIGINT=NULL,	
		@Movil BIGINT=NULL,	
		@IndicativoFax INT=NULL,	
		@NumeroFax INT=NULL, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Oferente.TelTerceros(IdTercero, IndicativoTelefono, NumeroTelefono, ExtensionTelefono, Movil, IndicativoFax, NumeroFax, UsuarioCrea, FechaCrea)
					  VALUES(@IdTercero, @IndicativoTelefono, @NumeroTelefono, @ExtensionTelefono, @Movil, @IndicativoFax, @NumeroFax, @UsuarioCrea, GETDATE())
	SELECT @IdTelTercero = @@IDENTITY 		
END


GO


