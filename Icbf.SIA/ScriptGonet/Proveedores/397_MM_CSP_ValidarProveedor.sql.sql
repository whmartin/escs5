USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]    Script Date: 07/28/2013 16:11:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado de InfoExperiencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]
		@IdExpEntidad INT, @IdEstadoInfoExperienciaEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[InfoExperienciaEntidad] 
	SET EstadoDocumental = @IdEstadoInfoExperienciaEntidad, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdExpEntidad = @IdExpEntidad
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]    Script Date: 07/28/2013 16:11:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoExperienciaEntidad, v.IdExpEntidad, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoExperienciaEntidad v
			right join [Proveedor].[InfoExperienciaEntidad] e on e.IdExpEntidad = v.IdExpEntidad
						inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
			where e.IdEntidad  = @IdEntidad
			--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]    Script Date: 07/28/2013 16:11:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]
	@IdExpEntidad int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoExperienciaEntidad] 
 WHERE IdExpEntidad = CASE WHEN @IdExpEntidad IS NULL THEN IdExpEntidad ELSE @IdExpEntidad END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]    Script Date: 07/28/2013 16:11:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]
		@IdEntidad INT, @IdEstadoInfoDatosBasicosEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[EntidadProvOferente] 
	SET IdEstado = @IdEstadoInfoDatosBasicosEntidad, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdEntidad = @IdEntidad
END






' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]    Script Date: 07/28/2013 16:11:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]
		@IdValidarInfoExperienciaEntidad INT, @IdExpEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoExperienciaEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdExpEntidad = @IdExpEntidad
		and NroRevision = @NroRevision			
		and IdValidarInfoExperienciaEntidad = @IdValidarInfoExperienciaEntidad
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]    Script Date: 07/28/2013 16:11:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]
		@IdValidarInfoExperienciaEntidad INT OUTPUT, @IdExpEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoExperienciaEntidad(IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdExpEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoExperienciaEntidad = @@IDENTITY 		
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]    Script Date: 07/28/2013 16:11:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoExperienciaEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima] (@IdEntidad INT, @IdExpEntidad INT)
as
begin
	select top 1
		v.IdValidarInfoExperienciaEntidad,
		e.IdEntidad,
		i.IdExpEntidad,		
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
			from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and i.IdExpEntidad = @IdExpEntidad
		and e.NroRevision = v.NroRevision
	order by NroRevision DESC
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]    Script Date: 07/28/2013 16:11:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoExperienciaEntidad por IdInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]
	@IdExpEntidad INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoExperienciaEntidad] 
 WHERE  IdExpEntidad = @IdExpEntidad
 ORDER BY IdValidarInfoExperienciaEntidad DESC
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]    Script Date: 07/28/2013 16:11:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]
	@IdValidarInfoExperienciaEntidad INT
AS
BEGIN
 SELECT IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoExperienciaEntidad] WHERE  IdValidarInfoExperienciaEntidad = @IdValidarInfoExperienciaEntidad
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]    Script Date: 07/28/2013 16:11:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoDatosBasicosEntidad, v.IdEntidad, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoDatosBasicosEntidad v
			right join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = v.IdEntidad
			where p.IdEntidad  = @IdEntidad
			--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]    Script Date: 07/28/2013 16:11:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]
	@IdEntidad int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]    Script Date: 07/28/2013 16:11:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]
		@IdValidarInfoDatosBasicosEntidad INT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoDatosBasicosEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdEntidad = @IdEntidad
		and NroRevision = @NroRevision			
		and IdValidarInfoDatosBasicosEntidad = @IdValidarInfoDatosBasicosEntidad
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]    Script Date: 07/28/2013 16:11:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]
		@IdValidarInfoDatosBasicosEntidad INT OUTPUT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoDatosBasicosEntidad(IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoDatosBasicosEntidad = @@IDENTITY 		
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima]    Script Date: 07/28/2013 16:11:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima] (@IdEntidad INT)
as
begin
	select top 1
		v.IdValidarInfoDatosBasicosEntidad,
		e.IdEntidad,
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
	from  Proveedor.EntidadProvOferente e
		inner join Proveedor.ValidarInfoDatosBasicosEntidad v
		on v.IdEntidad = e.IdEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
	order by NroRevision DESC
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]    Script Date: 07/28/2013 16:11:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoDatosBasicosEntidad por IdInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] 
 WHERE  IdEntidad = @IdEntidad
 ORDER BY IdValidarInfoDatosBasicosEntidad DESC
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]    Script Date: 07/28/2013 16:11:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]
	@IdValidarInfoDatosBasicosEntidad INT
AS
BEGIN
 SELECT IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] WHERE  IdValidarInfoDatosBasicosEntidad = @IdValidarInfoDatosBasicosEntidad
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]    Script Date: 07/28/2013 16:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]
		@IdInfoFin INT, @IdEstadoInfoFinancieraEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	update [Proveedor].[InfoFinancieraEntidad]
	SET EstadoValidacion = @IdEstadoInfoFinancieraEntidad,
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdInfoFin = @IdInfoFin
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]    Script Date: 07/28/2013 16:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]
		@IdValidarInfoFinancieraEntidad INT, @IdInfoFin INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoFinancieraEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdInfoFin = @IdInfoFin
		and NroRevision = @NroRevision			
		and IdValidarInfoFinancieraEntidad = @IdValidarInfoFinancieraEntidad
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]    Script Date: 07/28/2013 16:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]
		@IdValidarInfoFinancieraEntidad INT OUTPUT, @IdInfoFin INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoFinancieraEntidad(IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdInfoFin, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoFinancieraEntidad = @@IDENTITY 		
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]    Script Date: 07/28/2013 16:11:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoEntidadEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima] (@IdEntidad INT, @IdVigencia INT)
as
begin
	select top 1
		IdValidarInfoFinancieraEntidad,
		i.IdInfoFin,
		e.IdEntidad,
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and i.IdVigencia = @IdVigencia
		and e.NroRevision = v.NroRevision
	order by NroRevision DESC
end

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]    Script Date: 07/28/2013 16:11:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoFinancieraEntidad por IdInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]
	@IdInfoFin INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoFinancieraEntidad] 
 WHERE  IdInfoFin = @IdInfoFin
 ORDER BY IdValidarInfoFinancieraEntidad DESC
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]    Script Date: 07/28/2013 16:11:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]
	@IdValidarInfoFinancieraEntidad INT
AS
BEGIN
 SELECT IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoFinancieraEntidad] WHERE  IdValidarInfoFinancieraEntidad = @IdValidarInfoFinancieraEntidad
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]    Script Date: 07/28/2013 16:11:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- Autor: Mauricio Martinez
-- Fecha: 2013/07/28
-- Descripcion: Procedimiento usado para validar el proveedor módulos dátos básicos, info financiero o experiencia

CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision](@IdEntidad INT)
as
begin

	update Proveedor.InfoFinancieraEntidad  
	set  Finalizado = 1	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
		and i.EstadoValidacion = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMó CON SI
	
	
	update Proveedor.InfoFinancieraEntidad  
	set NroRevision = v.NroRevision + 1,
		Finalizado = 0	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
		and i.EstadoValidacion <> 4 --SI NO ESTA VALIDADO AUMENTA EL NRO DE REVISION
		and v.ConfirmaYAprueba  <> 1 -- Y SI NO CONFIRMó CON SI
		
	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = 1
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
		and i.EstadoDocumental = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMó CON SI
	
	
	update Proveedor.InfoExperienciaEntidad 
	set NroRevision = v.NroRevision + 1,
		Finalizado = 0
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
		and i.EstadoDocumental <> 4 --SI NO ESTA VALIDADO AUMENTA EL NRO DE REVISION
		and v.ConfirmaYAprueba <> 1 -- Y SI NO CONFIRMó CON SI

	update Proveedor.EntidadProvOferente    
	set Finalizado = 1
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
	where e.IdEntidad = @IdEntidad
		and IdEstado = 2  -- SI ESTA VALIDADO
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMO CON UN SI

		
	update Proveedor.EntidadProvOferente    
	set NroRevision = e.NroRevision + 1,
		Finalizado = 0
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
	where e.IdEntidad = @IdEntidad
		and IdEstado <> 2  -- SI NO ESTA VALIDADO
		and v.ConfirmaYAprueba <> 1 -- Y SI NO HAN PUESTO UN SI
end


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 07/28/2013 16:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


--Autor:Mauricio Martinez
--Fecha:2013/07/15 16:00
--Descripcion: Consulta para mostrar un resumen de los modulos validados

CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
@IdEntidad INT 
AS
BEGIN

			select	0 as Orden, 
					e.IdEntidad, --i.IdExpEntidad, 
					MAX(ISNULL(e.NroRevision,1)) as NroRevision,
					''Datos Básicos'' as Componente,
					MIN(case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END) AS iConfirmaYAprueba,
					e.Finalizado
			from  Proveedor.EntidadProvOferente e
				left join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
			where 
					e.IdEntidad = @IdEntidad 
					and (v.ConfirmaYAprueba = 1 or e.NroRevision = ISNULL(v.NroRevision,e.NroRevision))
			group by e.IdEntidad,e.Finalizado
			union
			select	1 as Orden, 
					e.IdEntidad, --i.IdExpEntidad, 
					MAX(ISNULL(i.NroRevision,e.NroRevision)) as NroRevision,
					''Financiera'' as Componente,
					MIN(case when i.IdInfoFin IS NULL THEN -2 ELSE case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END END) AS iConfirmaYAprueba,
					i.Finalizado
			from  Proveedor.EntidadProvOferente e
				left join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				left join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin					
			where 
					e.IdEntidad = @IdEntidad 
					and (v.ConfirmaYAprueba = 1 or e.NroRevision = ISNULL(v.NroRevision,e.NroRevision))
			group by e.IdEntidad,i.Finalizado
			union
			select	2 as Orden,
					e.IdEntidad, --i.IdExpEntidad, 
					MAX(ISNULL(i.NroRevision,e.NroRevision)) as NroRevision,
					''Experiencias'' as Comoponente,
					MIN(case when i.IdExpEntidad IS NULL THEN -2 ELSE case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END END) AS iConfirmaYAprueba,
					i.Finalizado
			from  Proveedor.EntidadProvOferente e
				left join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				left join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
			where 
					e.IdEntidad = @IdEntidad 
					and (v.ConfirmaYAprueba = 1 or e.NroRevision = ISNULL(v.NroRevision,e.NroRevision))					
			group by e.IdEntidad,i.Finalizado
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]    Script Date: 07/28/2013 16:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoFinancieraEntidad, v.IdInfoFin,AcnoVigencia, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoFinancieraEntidad v
	right join [Proveedor].[InfoFinancieraEntidad] e on e.IdInfoFin = v.IdInfoFin
	inner join [Global].[Vigencia] g on g.IdVigencia = e.IdVigencia
	inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
	where e.IdEntidad  = @IdEntidad
	--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision) 
 
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]    Script Date: 07/28/2013 16:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]
	@IdInfoFin int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoFinancieraEntidad] 
 WHERE IdInfoFin = CASE WHEN @IdInfoFin IS NULL THEN IdInfoFin ELSE @IdInfoFin END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
END


' 
END
GO
