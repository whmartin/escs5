/****** Object:  Table [Auditoria].[PROVEEDOR_NotificacionJudicial]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para NotificacionJudicial ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_NotificacionJudicial]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_NotificacionJudicial]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_NotificacionJudicial]([IdNotJudicial] [int]  NULL, [IdEntidad_old] [int]  NULL,[IdEntidad_new] [int]  NULL,[IdDepartamento_old] [int]  NULL,[IdDepartamento_new] [int]  NULL,[IdMunicipio_old] [int]  NULL,[IdMunicipio_new] [int]  NULL,[IdZona_old] [int]  NULL,[IdZona_new] [int]  NULL,[Direccion_old] [nvarchar] (250)  NULL,[Direccion_new] [nvarchar] (250)  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO