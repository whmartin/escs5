USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Modificar]    Script Date: 06/24/2013 12:26:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Modificar]    Script Date: 06/24/2013 12:26:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabi�n Valencia
-- Create date: 15-06-2013
-- Description:	Actualiza tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Modificar]
	 @IdTercero  INT,
	 @IdDListaTipoDocumento INT= NULL,
	 @IdTipoPersona INT= NULL,
	 @Email nvarchar(40)= NULL,
	 @NumeroIdentificacion nvarchar(128)= NULL,
	 @FechaExpedicionId datetime= NULL,
	 @Sexo nvarchar(1)= NULL,
	 @RazonSocial  nvarchar(128)= NULL,
	 @Digitoverificacion INT= NULL,
	 @PrimerNombre nvarchar(128)= NULL,
	 @SegundoNombre nvarchar(128)= NULL,
	 @PrimerApellido nvarchar(128)= NULL,
	 @SegundoApellido nvarchar(128)= NULL,
	 @FechaNacimiento datetime= NULL,
	 @UsuarioModifica nvarchar(128)= NULL,
	 @FechaModifica datetime= NULL
AS
BEGIN
	UPDATE Oferente.TERCERO SET
	 IDTIPODOCIDENTIFICA= ISNULL(@IdDListaTipoDocumento,IDTIPODOCIDENTIFICA),
		IdTipoPersona= ISNULL(@IdTipoPersona, IdTipoPersona),
		CORREOELECTRONICO = ISNULL( @Email, CORREOELECTRONICO ),
		NUMEROIDENTIFICACION = ISNULL( @NumeroIdentificacion, NUMEROIDENTIFICACION ),
		FECHAEXPEDICIONID = ISNULL( @FechaExpedicionId, FECHAEXPEDICIONID ),
		SEXO = ISNULL( @Sexo, SEXO ),
		RAZONSOCIAL= ISNULL(@RazonSocial, RAZONSOCIAL),
		PRIMERNOMBRE= ISNULL(@PrimerNombre, PRIMERNOMBRE),
		SEGUNDONOMBRE= ISNULL(@SegundoNombre, SEGUNDONOMBRE),
		PRIMERAPELLIDO= ISNULL(@PrimerApellido, PRIMERAPELLIDO),
		SEGUNDOAPELLIDO= ISNULL(@SegundoApellido, SEGUNDOAPELLIDO),
		DIGITOVERIFICACION= ISNULL(@Digitoverificacion, DIGITOVERIFICACION),
		FECHANACIMIENTO= ISNULL(@FechaNacimiento, FECHANACIMIENTO),
		USUARIOMODIFICA = ISNULL( @UsuarioModifica, USUARIOMODIFICA ),
		FECHAMODIFICA = ISNULL( @FechaModifica ,  FECHAMODIFICA )

	WHERE IDTERCERO = @IdTercero
END

GO


