/****** Object:  Trigger [PROVEEDOR].[TG_Aud_RamaoEstructura_Niveldegobierno]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para RamaoEstructura_Niveldegobierno
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_RamaoEstructura_Niveldegobierno]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_RamaoEstructura_Niveldegobierno]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_RamaoEstructura_Niveldegobierno]
  ON  [PROVEEDOR].[RamaoEstructura_Niveldegobierno]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_RamaoEstructura_Niveldegobierno]
  ([Id],[IdRamaEstructura],[IdNiveldegobierno],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[Operacion])
SELECT deleted.Id,deleted.IdRamaEstructura,deleted.IdNiveldegobierno,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.Id = deleted.Id AND inserted.IdRamaEstructura = deleted.IdRamaEstructura AND inserted.IdNiveldegobierno = deleted.IdNiveldegobiernoEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_RamaoEstructura_Niveldegobierno]
  ([Id],[IdRamaEstructura],[IdNiveldegobierno],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[Operacion])
SELECT deleted.Id,deleted.IdRamaEstructura,deleted.IdNiveldegobierno,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_RamaoEstructura_Niveldegobierno]
  ([Id],[IdRamaEstructura],[IdNiveldegobierno],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[Operacion])
SELECT inserted.Id,inserted.IdRamaEstructura,inserted.IdNiveldegobierno,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO