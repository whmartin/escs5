USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Creacion de indices para no permitir duplicados
-- =============================================
/****** Object:  Index [NombreUnico]    Script Date: 06/25/2013 17:48:25 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCiiu]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[TipoCiiu] WITH ( ONLINE = OFF )
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoCiiu] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[TipodeActividad] WITH ( ONLINE = OFF )
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipodeActividad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeentidadPublica]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[TipodeentidadPublica] WITH ( ONLINE = OFF )
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipodeentidadPublica] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Tipoentidad]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[Tipoentidad] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[Tipoentidad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoSectorEntidad]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[TipoSectorEntidad] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoSectorEntidad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[ClasedeEntidad] WITH ( ONLINE = OFF )
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[ClasedeEntidad] 
(
	[IdTipodeActividad] ASC,
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[Niveldegobierno] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[Niveldegobierno] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[NivelOrganizacional]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[NivelOrganizacional] WITH ( ONLINE = OFF )
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[NivelOrganizacional] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[RamaoEstructura] WITH ( ONLINE = OFF )
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[RamaoEstructura] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoRegimenTributario]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[TipoRegimenTributario] WITH ( ONLINE = OFF )
GO
/****** Object:  Index [NombreUnico]    Script Date: 06/22/2013 16:40:51 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoRegimenTributario] 
(
	[IdTipoPersona] ASC,
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO



