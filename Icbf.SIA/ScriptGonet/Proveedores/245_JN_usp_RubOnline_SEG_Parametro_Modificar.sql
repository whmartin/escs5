USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametro_Modificar]    Script Date: 06/26/2013 18:58:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_Parametro_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametro_Modificar]    Script Date: 06/26/2013 18:58:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Modificar]
		@IdParametro INT,	@NombreParametro NVARCHAR(128),	@ValorParametro NVARCHAR(256),	
		@ImagenParametro NVARCHAR(256) = NULL,	@Estado BIT, @Funcionalidad NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Parametro 
	SET NombreParametro = @NombreParametro, ValorParametro = @ValorParametro, 
	ImagenParametro = ISNULL(@ImagenParametro,ImagenParametro), 
	Estado = @Estado, Funcionalidad = @Funcionalidad,
	UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdParametro = @IdParametro
END

GO


