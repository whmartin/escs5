/****** Object:  Trigger [PROVEEDOR].[TG_Aud_ContactoEntidad]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para ContactoEntidad
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_ContactoEntidad]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_ContactoEntidad]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_ContactoEntidad]
  ON  [PROVEEDOR].[ContactoEntidad]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_ContactoEntidad]
  ([IdContacto],[IdEntidad_old], [IdEntidad_new],[IdSucursal_old], [IdSucursal_new],[IdTelContacto_old], [IdTelContacto_new],[IdTipoDocIdentifica_old], [IdTipoDocIdentifica_new],[IdTipoCargoEntidad_old], [IdTipoCargoEntidad_new],[NumeroIdentificacion_old], [NumeroIdentificacion_new],[PrimerNombre_old], [PrimerNombre_new],[SegundoNombre_old], [SegundoNombre_new],[PrimerApellido_old], [PrimerApellido_new],[SegundoApellido_old], [SegundoApellido_new],[Dependencia_old], [Dependencia_new],[Email_old], [Email_new],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdContacto,deleted.IdEntidad,inserted.IdEntidad,deleted.IdSucursal,inserted.IdSucursal,deleted.IdTelContacto,inserted.IdTelContacto,deleted.IdTipoDocIdentifica,inserted.IdTipoDocIdentifica,deleted.IdTipoCargoEntidad,inserted.IdTipoCargoEntidad,deleted.NumeroIdentificacion,inserted.NumeroIdentificacion,deleted.PrimerNombre,inserted.PrimerNombre,deleted.SegundoNombre,inserted.SegundoNombre,deleted.PrimerApellido,inserted.PrimerApellido,deleted.SegundoApellido,inserted.SegundoApellido,deleted.Dependencia,inserted.Dependencia,deleted.Email,inserted.Email,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdContacto = deleted.IdContactoEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_ContactoEntidad]
  ([IdContacto],[IdEntidad_old],[IdSucursal_old],[IdTelContacto_old],[IdTipoDocIdentifica_old],[IdTipoCargoEntidad_old],[NumeroIdentificacion_old],[PrimerNombre_old],[SegundoNombre_old],[PrimerApellido_old],[SegundoApellido_old],[Dependencia_old],[Email_old],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdContacto,deleted.IdEntidad,deleted.IdSucursal,deleted.IdTelContacto,deleted.IdTipoDocIdentifica,deleted.IdTipoCargoEntidad,deleted.NumeroIdentificacion,deleted.PrimerNombre,deleted.SegundoNombre,deleted.PrimerApellido,deleted.SegundoApellido,deleted.Dependencia,deleted.Email,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_ContactoEntidad]
  ([IdContacto],[IdEntidad_new],[IdSucursal_new],[IdTelContacto_new],[IdTipoDocIdentifica_new],[IdTipoCargoEntidad_new],[NumeroIdentificacion_new],[PrimerNombre_new],[SegundoNombre_new],[PrimerApellido_new],[SegundoApellido_new],[Dependencia_new],[Email_new],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdContacto,inserted.IdEntidad,inserted.IdSucursal,inserted.IdTelContacto,inserted.IdTipoDocIdentifica,inserted.IdTipoCargoEntidad,inserted.NumeroIdentificacion,inserted.PrimerNombre,inserted.SegundoNombre,inserted.PrimerApellido,inserted.SegundoApellido,inserted.Dependencia,inserted.Email,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO