USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en  [Proveedor].[TipodeentidadPublica]
-- =============================================
/****** Object:  Table [Proveedor].[TipodeentidadPublica]    Script Date: 06/14/2013 19:40:35 ******/
SET IDENTITY_INSERT [Proveedor].[TipodeentidadPublica] ON
INSERT [Proveedor].[TipodeentidadPublica] ([IdTipodeentidadPublica], [CodigoTipodeentidadPublica], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'INDUSTRIAL Y COMERCIAL DEL ESTADO', 1, N'Administrador', CAST(0x0000A1DB01111A13 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipodeentidadPublica] ([IdTipodeentidadPublica], [CodigoTipodeentidadPublica], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'EMPRESA SOCIAL DEL ESTADO', 1, N'Administrador', CAST(0x0000A1DB011130D7 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipodeentidadPublica] ([IdTipodeentidadPublica], [CodigoTipodeentidadPublica], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'003', N'ESTABLECIMIENTO PUBLICO', 1, N'Administrador', CAST(0x0000A1DB0111412F AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipodeentidadPublica] ([IdTipodeentidadPublica], [CodigoTipodeentidadPublica], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, N'004', N'UNIDAD ADMINISTRATIVA ESPECIAL', 1, N'Administrador', CAST(0x0000A1DB011152C9 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipodeentidadPublica] ([IdTipodeentidadPublica], [CodigoTipodeentidadPublica], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'005', N'AGENCIAS DEL ESTADO', 1, N'Administrador', CAST(0x0000A1DB011162DF AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipodeentidadPublica] ([IdTipodeentidadPublica], [CodigoTipodeentidadPublica], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (6, N'006', N'DEPARTAMENTOS ADMINISTRATIVO', 1, N'Administrador', CAST(0x0000A1DB011172A5 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[TipodeentidadPublica] OFF