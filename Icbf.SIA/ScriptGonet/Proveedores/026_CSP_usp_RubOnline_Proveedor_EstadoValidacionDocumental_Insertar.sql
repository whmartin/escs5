USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]    Script Date: 06/14/2013 19:31:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]
		@IdEstadoValidacionDocumental INT OUTPUT, 	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoValidacionDocumental(CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoEstadoValidacionDocumental, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoValidacionDocumental = @@IDENTITY 		
END
GO