USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]    Script Date: 07/21/2013 18:56:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]    Script Date: 07/21/2013 18:56:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]
	@CodigoCiiu NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCiiu, CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
			FROM [Proveedor].[TipoCiiu] 
			WHERE 
				CodigoCiiu LIKE CASE WHEN @CodigoCiiu IS NULL THEN CodigoCiiu ELSE '%' + @CodigoCiiu + '%' END 
				AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE '%' + @Descripcion + '%' END 
				AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO


