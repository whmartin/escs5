USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en  [Proveedor].[TipoSectorEntidad]
-- =============================================
/****** Object:  Table [Proveedor].[TipoSectorEntidad]    Script Date: 06/14/2013 19:40:35 ******/
SET IDENTITY_INSERT [Proveedor].[TipoSectorEntidad] ON
INSERT [Proveedor].[TipoSectorEntidad] ([IdTipoSectorEntidad], [CodigoSectorEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'Privado', 1, N'Administrador', CAST(0x0000A1D900BA4883 AS DateTime), N'Administrador', CAST(0x0000A1D900BC3A85 AS DateTime))
INSERT [Proveedor].[TipoSectorEntidad] ([IdTipoSectorEntidad], [CodigoSectorEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'Publico', 1, N'Administrador', CAST(0x0000A1D900BC23B3 AS DateTime), N'Administrador', CAST(0x0000A1D900BC3A85 AS DateTime))
SET IDENTITY_INSERT [Proveedor].[TipoSectorEntidad] OFF