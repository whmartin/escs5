USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]    Script Date: 06/14/2013 19:32:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]
	@IdTipoRegimenTributario INT
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoRegimenTributario] WHERE  IdTipoRegimenTributario = @IdTipoRegimenTributario
END
GO