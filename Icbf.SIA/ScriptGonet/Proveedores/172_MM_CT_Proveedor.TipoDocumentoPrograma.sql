USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Creacion de tabla para almacenar la relacion Tipos Documento Programa
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_Programa]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] DROP CONSTRAINT [FK_TipoDocumentoPrograma_Programa]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] DROP CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[TipoDocumentoPrograma]    Script Date: 06/24/2013 12:13:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoDocumentoPrograma]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[TipoDocumentoPrograma]    Script Date: 06/24/2013 12:13:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Proveedor].[TipoDocumentoPrograma](
	[IdTipoDocumentoPrograma] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoDocumento] [int] NOT NULL,
	[IdPrograma] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[MaxPermitidoKB] [int] NULL,
	[ExtensionesPermitidas] [varchar](50) NULL,
	[ObligRupNoRenovado] [int] NULL,
	[ObligRupRenovado] [int] NULL,
	[ObligPersonaJuridica] [int] NULL,
	[ObligPersonaNatural] [int] NULL,
 CONSTRAINT [PK_TipoDocumentoPrograma] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumentoPrograma] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Proveedor].[TipoDocumentoPrograma]  WITH CHECK ADD  CONSTRAINT [FK_TipoDocumentoPrograma_Programa] FOREIGN KEY([IdPrograma])
REFERENCES [SEG].[Programa] ([IdPrograma])
GO

ALTER TABLE [Proveedor].[TipoDocumentoPrograma] CHECK CONSTRAINT [FK_TipoDocumentoPrograma_Programa]
GO

ALTER TABLE [Proveedor].[TipoDocumentoPrograma]  WITH CHECK ADD  CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO

ALTER TABLE [Proveedor].[TipoDocumentoPrograma] CHECK CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento]
GO


