USE [SIA]
GO
-- =============================================
-- Author:		Carlos Cubillos
-- Description:	Se agrega funcionalidad para determinar los documentos a adjuntar según tipo persona y sector para proveedores.
-- =============================================

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]    Script Date: 10/01/2013 16:26:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]    Script Date: 10/01/2013 16:26:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta una lista de DocFinancieraProv por IdInfoFin y si el Rup es renovado o no
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup] 
( @IdInfoFin int, @RupRenovado varchar(128), @TipoPersona varchar(1),  @TipoSector varchar(1))
as

declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/DocFinancieraProv';


SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdInfoFin) as IdInfoFin,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdInfoFin,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
			--CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--				WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocFinancieraProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION FINANCIERA
		AND IdInfoFin = @IdInfoFin
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdInfoFin,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 --CASE substring(CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
					 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
						--		WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION FINANCIERA
AND tdp.Estado=1
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento





GO


