USE [SIA]
GO

-- =============================================
-- Author:          Juan Carlos Valverde Sámano    
-- Create date:         17/01/2014
-- Description:     Consulta una Tabla Paramétrica de [Proveedor].[TablaParametrica]
-- =============================================
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]    Script Date: 06/14/2013 19:31:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]
	@IdTablaParametrica INT
AS
BEGIN
 SELECT IdTablaParametrica, CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TablaParametrica] WHERE  IdTablaParametrica = @IdTablaParametrica
END
GO