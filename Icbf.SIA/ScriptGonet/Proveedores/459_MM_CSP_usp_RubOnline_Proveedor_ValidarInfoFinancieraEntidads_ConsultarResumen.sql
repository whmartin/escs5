USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]    Script Date: 08/05/2013 08:33:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]    Script Date: 08/05/2013 08:33:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoFinancieraEntidad, v.IdInfoFin,AcnoVigencia, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoFinancieraEntidad v
	inner join [Proveedor].[InfoFinancieraEntidad] e on e.IdInfoFin = v.IdInfoFin
	inner join [Global].[Vigencia] g on g.IdVigencia = e.IdVigencia
	inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
	where e.IdEntidad  = @IdEntidad
	--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision) 
	ORDER BY v.FechaCrea DESC
 
END






GO


