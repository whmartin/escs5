USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]    Script Date: 06/24/2013 12:38:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]    Script Date: 06/24/2013 12:38:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Fabi�n Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]
	@IDTIPODOCIDENTIFICA int = NULL,
	@IDESTADOTERCERO int = NULL,
	@IdTipoPersona int = NULL,
	@NUMEROIDENTIFICACION varchar(30) = NULL 
	
AS
BEGIN
	SELECT       Oferente.TERCERO.IDTERCERO, 
				 Oferente.TERCERO.IDTIPODOCIDENTIFICA, 
				 Oferente.TERCERO.IDESTADOTERCERO, 
				 Oferente.TERCERO.IdTipoPersona, 
                 Oferente.TERCERO.NUMEROIDENTIFICACION, 
                 Oferente.TERCERO.DIGITOVERIFICACION, 
                 Oferente.TERCERO.CORREOELECTRONICO, 
                 Oferente.TERCERO.PRIMERNOMBRE, 
                 Oferente.TERCERO.SEGUNDONOMBRE, 
                 Oferente.TERCERO.PRIMERAPELLIDO, 
                 Oferente.TERCERO.SEGUNDOAPELLIDO, 
                 Oferente.TERCERO.RAZONSOCIAL, 
                 Oferente.TERCERO.FECHAEXPEDICIONID, 
                 Oferente.TERCERO.FECHANACIMIENTO, 
                 Oferente.TERCERO.SEXO, 
                 Oferente.TERCERO.FECHACREA, 
                 Oferente.TERCERO.USUARIOCREA, 
                 Oferente.TERCERO.FECHAMODIFICA, 
                 Oferente.TERCERO.USUARIOMODIFICA, 
                 Global.TiposDocumentos.NomTipoDocumento AS NombreTipoIdentificacionPersonaNatural, 
                 Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural, 
                 Oferente.EstadoTercero.CodigoEstadotercero, 
                 Oferente.EstadoTercero.DescripcionEstado, 
                 Oferente.TipoPersona.CodigoTipoPersona, 
                 Oferente.TipoPersona.NombreTipoPersona
	FROM         Oferente.TERCERO 
				 INNER JOIN Global.TiposDocumentos  ON 
				 Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento 
				 INNER JOIN Oferente.EstadoTercero ON 
				 Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero 
				 INNER JOIN Oferente.TipoPersona ON 
				 Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
	WHERE 
				Oferente.TERCERO.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END
				AND Oferente.TERCERO.IDESTADOTERCERO = CASE WHEN @IDESTADOTERCERO IS NULL THEN Oferente.TERCERO.IDESTADOTERCERO ELSE @IDESTADOTERCERO END
				AND Oferente.TERCERO.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona END 
				AND Oferente.TERCERO.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END 
		
	
     
END
GO


