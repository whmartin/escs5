/****** Object:  Trigger [PROVEEDOR].[TG_Aud_InfoExperienciaEntidad]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para InfoExperienciaEntidad
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_InfoExperienciaEntidad]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_InfoExperienciaEntidad]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_InfoExperienciaEntidad]
  ON  [PROVEEDOR].[InfoExperienciaEntidad]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_InfoExperienciaEntidad]
  ([IdExpEntidad],[IdEntidad_old], [IdEntidad_new],[IdTipoSector_old], [IdTipoSector_new],[IdTipoEstadoExp_old], [IdTipoEstadoExp_new],[IdTipoModalidadExp_old], [IdTipoModalidadExp_new],[IdTipoModalidad_old], [IdTipoModalidad_new],[IdTipoPoblacionAtendida_old], [IdTipoPoblacionAtendida_new],[IdTipoRangoExpAcum_old], [IdTipoRangoExpAcum_new],[IdTipoCodUNSPSC_old], [IdTipoCodUNSPSC_new],[IdTipoEntidadContratante_old], [IdTipoEntidadContratante_new],[EntidadContratante_old], [EntidadContratante_new],[FechaInicio_old], [FechaInicio_new],[FechaFin_old], [FechaFin_new],[NumeroContrato_old], [NumeroContrato_new],[ObjetoContrato_old], [ObjetoContrato_new],[Vigente_old], [Vigente_new],[Cuantia_old], [Cuantia_new],[ExperienciaMeses_old], [ExperienciaMeses_new],[EstadoDocumental_old], [EstadoDocumental_new],[UnionTempConsorcio_old], [UnionTempConsorcio_new],[PorcentParticipacion_old], [PorcentParticipacion_new],[AtencionDeptos_old], [AtencionDeptos_new],[JardinOPreJardin_old], [JardinOPreJardin_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[NroRevision_old], [NroRevision_new],[Finalizado_old], [Finalizado_new],[ContratoEjecucion_old], [ContratoEjecucion_new],[Operacion])
SELECT deleted.IdExpEntidad,deleted.IdEntidad,inserted.IdEntidad,deleted.IdTipoSector,inserted.IdTipoSector,deleted.IdTipoEstadoExp,inserted.IdTipoEstadoExp,deleted.IdTipoModalidadExp,inserted.IdTipoModalidadExp,deleted.IdTipoModalidad,inserted.IdTipoModalidad,deleted.IdTipoPoblacionAtendida,inserted.IdTipoPoblacionAtendida,deleted.IdTipoRangoExpAcum,inserted.IdTipoRangoExpAcum,deleted.IdTipoCodUNSPSC,inserted.IdTipoCodUNSPSC,deleted.IdTipoEntidadContratante,inserted.IdTipoEntidadContratante,deleted.EntidadContratante,inserted.EntidadContratante,deleted.FechaInicio,inserted.FechaInicio,deleted.FechaFin,inserted.FechaFin,deleted.NumeroContrato,inserted.NumeroContrato,deleted.ObjetoContrato,inserted.ObjetoContrato,deleted.Vigente,inserted.Vigente,deleted.Cuantia,inserted.Cuantia,deleted.ExperienciaMeses,inserted.ExperienciaMeses,deleted.EstadoDocumental,inserted.EstadoDocumental,deleted.UnionTempConsorcio,inserted.UnionTempConsorcio,deleted.PorcentParticipacion,inserted.PorcentParticipacion,deleted.AtencionDeptos,inserted.AtencionDeptos,deleted.JardinOPreJardin,inserted.JardinOPreJardin,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,deleted.NroRevision,inserted.NroRevision,deleted.Finalizado,inserted.Finalizado,deleted.ContratoEjecucion,inserted.ContratoEjecucion,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdExpEntidad = deleted.IdExpEntidadEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_InfoExperienciaEntidad]
  ([IdExpEntidad],[IdEntidad_old],[IdTipoSector_old],[IdTipoEstadoExp_old],[IdTipoModalidadExp_old],[IdTipoModalidad_old],[IdTipoPoblacionAtendida_old],[IdTipoRangoExpAcum_old],[IdTipoCodUNSPSC_old],[IdTipoEntidadContratante_old],[EntidadContratante_old],[FechaInicio_old],[FechaFin_old],[NumeroContrato_old],[ObjetoContrato_old],[Vigente_old],[Cuantia_old],[ExperienciaMeses_old],[EstadoDocumental_old],[UnionTempConsorcio_old],[PorcentParticipacion_old],[AtencionDeptos_old],[JardinOPreJardin_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[NroRevision_old],[Finalizado_old],[ContratoEjecucion_old],[Operacion])
SELECT deleted.IdExpEntidad,deleted.IdEntidad,deleted.IdTipoSector,deleted.IdTipoEstadoExp,deleted.IdTipoModalidadExp,deleted.IdTipoModalidad,deleted.IdTipoPoblacionAtendida,deleted.IdTipoRangoExpAcum,deleted.IdTipoCodUNSPSC,deleted.IdTipoEntidadContratante,deleted.EntidadContratante,deleted.FechaInicio,deleted.FechaFin,deleted.NumeroContrato,deleted.ObjetoContrato,deleted.Vigente,deleted.Cuantia,deleted.ExperienciaMeses,deleted.EstadoDocumental,deleted.UnionTempConsorcio,deleted.PorcentParticipacion,deleted.AtencionDeptos,deleted.JardinOPreJardin,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,deleted.NroRevision,deleted.Finalizado,deleted.ContratoEjecucion,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_InfoExperienciaEntidad]
  ([IdExpEntidad],[IdEntidad_new],[IdTipoSector_new],[IdTipoEstadoExp_new],[IdTipoModalidadExp_new],[IdTipoModalidad_new],[IdTipoPoblacionAtendida_new],[IdTipoRangoExpAcum_new],[IdTipoCodUNSPSC_new],[IdTipoEntidadContratante_new],[EntidadContratante_new],[FechaInicio_new],[FechaFin_new],[NumeroContrato_new],[ObjetoContrato_new],[Vigente_new],[Cuantia_new],[ExperienciaMeses_new],[EstadoDocumental_new],[UnionTempConsorcio_new],[PorcentParticipacion_new],[AtencionDeptos_new],[JardinOPreJardin_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[NroRevision_new],[Finalizado_new],[ContratoEjecucion_new],[Operacion])
SELECT inserted.IdExpEntidad,inserted.IdEntidad,inserted.IdTipoSector,inserted.IdTipoEstadoExp,inserted.IdTipoModalidadExp,inserted.IdTipoModalidad,inserted.IdTipoPoblacionAtendida,inserted.IdTipoRangoExpAcum,inserted.IdTipoCodUNSPSC,inserted.IdTipoEntidadContratante,inserted.EntidadContratante,inserted.FechaInicio,inserted.FechaFin,inserted.NumeroContrato,inserted.ObjetoContrato,inserted.Vigente,inserted.Cuantia,inserted.ExperienciaMeses,inserted.EstadoDocumental,inserted.UnionTempConsorcio,inserted.PorcentParticipacion,inserted.AtencionDeptos,inserted.JardinOPreJardin,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,inserted.NroRevision,inserted.Finalizado,inserted.ContratoEjecucion,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO