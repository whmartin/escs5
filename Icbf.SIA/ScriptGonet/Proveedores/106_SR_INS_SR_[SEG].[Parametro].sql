USE [SIA]
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta un registro en [SEG].[Parametro]
-- =============================================
IF not exists (SELECT * FROM [SEG].[Parametro] where NombreParametro = 'PoliticasContrasena')
BEGIN

INSERT INTO [SEG].[Parametro]
           ([NombreParametro]
           ,[ValorParametro]
           ,[ImagenParametro]
           ,[Estado]
           ,[Funcionalidad]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('PoliticasContrasena'
           ,'La contrase�a requiere por lo menos una letra may�scula, una letra minuscula, por lo menos un numero, m�nimo 6 caracteres, por lo menos un caracter especial.'
           ,null
           ,'1'
           ,'1'
           ,'Adminsitrador'
           ,Getdate()
           ,NULL
           ,NULL)


end