USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]    Script Date: 06/14/2013 19:32:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]
		@IdTipoSectorEntidad INT OUTPUT, 	@CodigoSectorEntidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoSectorEntidad(CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoSectorEntidad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoSectorEntidad = @@IDENTITY 		
END
GO