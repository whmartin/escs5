/****** Object:  Table [Auditoria].[PROVEEDOR_TipoEntidad]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para TipoEntidad ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_TipoEntidad]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_TipoEntidad]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_TipoEntidad]([IdTipoentidad] [int]  NULL, [CodigoTipoentidad_old] [nvarchar] (128)  NULL,[CodigoTipoentidad_new] [nvarchar] (128)  NULL,[Descripcion_old] [nvarchar] (128)  NULL,[Descripcion_new] [nvarchar] (128)  NULL,[Estado_old] [bit]  NULL,[Estado_new] [bit]  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO