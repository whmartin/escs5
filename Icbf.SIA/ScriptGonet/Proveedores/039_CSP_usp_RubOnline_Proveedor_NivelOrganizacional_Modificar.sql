USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]    Script Date: 06/14/2013 19:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que actualiza un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]
		@IdNivelOrganizacional INT,	@CodigoNivelOrganizacional NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.NivelOrganizacional SET CodigoNivelOrganizacional = @CodigoNivelOrganizacional, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNivelOrganizacional = @IdNivelOrganizacional
END
GO