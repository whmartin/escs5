USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Creacion de tabla para Almacenar Tipos de Documentos
-- =============================================
/****** Object:  Table [Proveedor].[TipoDocumento]    Script Date: 06/24/2013 12:11:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumento]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoDocumento]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[TipoDocumento]    Script Date: 06/24/2013 12:11:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[TipoDocumento](
	[IdTipoDocumento] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoDocumento] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoDocumento] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


