/****** Object:  Trigger [PROVEEDOR].[TG_Aud_ClasedeEntidad]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para ClasedeEntidad
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_ClasedeEntidad]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_ClasedeEntidad]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_ClasedeEntidad]
  ON  [PROVEEDOR].[ClasedeEntidad]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_ClasedeEntidad]
  ([IdClasedeEntidad],[IdTipodeEntidad],[IdTipodeActividad],[CodigoClasedeEntidad_old], [CodigoClasedeEntidad_new],[Descripcion_old], [Descripcion_new],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdClasedeEntidad,deleted.IdTipodeEntidad,deleted.IdTipodeActividad,deleted.CodigoClasedeEntidad,inserted.CodigoClasedeEntidad,deleted.Descripcion,inserted.Descripcion,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdClasedeEntidad = deleted.IdClasedeEntidad AND inserted.IdTipodeEntidad = deleted.IdTipodeEntidad AND inserted.IdTipodeActividad = deleted.IdTipodeActividadEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_ClasedeEntidad]
  ([IdClasedeEntidad],[IdTipodeEntidad],[IdTipodeActividad],[CodigoClasedeEntidad_old],[Descripcion_old],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdClasedeEntidad,deleted.IdTipodeEntidad,deleted.IdTipodeActividad,deleted.CodigoClasedeEntidad,deleted.Descripcion,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_ClasedeEntidad]
  ([IdClasedeEntidad],[IdTipodeEntidad],[IdTipodeActividad],[CodigoClasedeEntidad_new],[Descripcion_new],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdClasedeEntidad,inserted.IdTipodeEntidad,inserted.IdTipodeActividad,inserted.CodigoClasedeEntidad,inserted.Descripcion,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO