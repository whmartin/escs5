USE [Proveedores]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametros_Consultar]    Script Date: 04/09/2013 12:01:49 p.m. ******/
DROP PROCEDURE [dbo].[usp_RubOnline_SEG_Parametros_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametros_Consultar]    Script Date: 04/09/2013 12:01:49 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametros_Consultar]
@NombreParametro NVARCHAR (128) = NULL, @ValorParametro NVARCHAR (256) = NULL, @Estado BIT = NULL,
@Funcionalidad NVARCHAR (128) = NULL
AS
BEGIN

	SELECT
		IdParametro,
		NombreParametro,
		ValorParametro,
		ImagenParametro,
		Estado,
		Funcionalidad,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [SEG].[Parametro]
	WHERE NombreParametro LIKE CASE
		WHEN @NombreParametro IS NULL THEN NombreParametro ELSE @NombreParametro + '%'
	END
	AND ValorParametro LIKE
		CASE
			WHEN @ValorParametro IS NULL THEN ValorParametro ELSE @ValorParametro + '%'
		END
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado 
		END
	AND Funcionalidad LIKE CASE
		WHEN @Funcionalidad IS NULL THEN Funcionalidad ELSE @Funcionalidad + '%'
	END
	
END


GO


