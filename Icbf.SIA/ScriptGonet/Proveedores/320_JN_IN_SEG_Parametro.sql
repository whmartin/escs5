USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Inserta un registro en la entidad SEG.Parametro(NombreParametro
-- =============================================
IF NOT EXISTS(SELECT * FROM SEG.Parametro WHERE NombreParametro='Logitud UNSPSC')
BEGIN
	INSERT INTO SEG.Parametro(NombreParametro, ValorParametro, Estado, Funcionalidad, UsuarioCrea, FechaCrea)
	VALUES ('Logitud UNSPSC','8', 1, 1, 'DesarrolloGoNet14', GETDATE())
END