USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]    Script Date: 08/01/2013 02:59:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]    Script Date: 08/01/2013 02:59:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
		@IdExpEntidad INT,	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	
		@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	
		@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(256),	@FechaInicio DATETIME,	@FechaFin DATETIME,	
		@NumeroContrato NVARCHAR(128),	@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(21,3),	
		@EstadoDocumental INT,	@UnionTempConsorcio BIT,	@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	@JardinOPreJardin BIT, @UsuarioModifica NVARCHAR(250), @Finalizado BIT = NULL
AS
BEGIN
	UPDATE Proveedor.InfoExperienciaEntidad 
	SET IdEntidad = @IdEntidad, IdTipoSector = @IdTipoSector, IdTipoEstadoExp = @IdTipoEstadoExp, IdTipoModalidadExp = @IdTipoModalidadExp, IdTipoModalidad = @IdTipoModalidad, IdTipoPoblacionAtendida = @IdTipoPoblacionAtendida, IdTipoRangoExpAcum = @IdTipoRangoExpAcum, IdTipoCodUNSPSC = @IdTipoCodUNSPSC, IdTipoEntidadContratante = @IdTipoEntidadContratante, EntidadContratante = @EntidadContratante, FechaInicio = @FechaInicio, FechaFin = @FechaFin, NumeroContrato = @NumeroContrato, ObjetoContrato = @ObjetoContrato, Vigente = @Vigente, Cuantia = @Cuantia, EstadoDocumental = @EstadoDocumental, UnionTempConsorcio = @UnionTempConsorcio, PorcentParticipacion = @PorcentParticipacion, AtencionDeptos = @AtencionDeptos, JardinOPreJardin = @JardinOPreJardin, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE(), Finalizado = ISNULL(@Finalizado,Finalizado)
	WHERE IdExpEntidad = @IdExpEntidad
END


GO


