--Permisos para ValidarTercero
USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Crea Permisos para ValidarTercero
-- =============================================
DECLARE @IdModulo  INT;
DECLARE @IdPrograma INT;
DECLARE @Padre INT;

SET @IdModulo = (SELECT IdModulo FROM SEG.Modulo WHERE NombreModulo = 'Proveedores')

INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@IdModulo, N'Validar Tercero', N'Proveedor/ValidarTercero', 0, 1, N'administrador', GETDATE(), NULL, NULL, 1, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 

INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 0, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
