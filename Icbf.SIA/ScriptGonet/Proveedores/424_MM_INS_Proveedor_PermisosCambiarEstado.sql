USE [SIA]
GO


--Permisos para Cambios de estado tercero y proveeedor


DECLARE @IdModulo  INT;
DECLARE @IdPrograma INT;

SET @IdModulo = (SELECT IdModulo FROM SEG.Modulo WHERE NombreModulo = 'Proveedores')

IF not EXISTS(SELECT * FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/CambiarEstadoTercero')
BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@IdModulo, N'Cambiar Estado', N'Proveedor/CambiarEstadoTercero', 0, 1, N'administrador', GETDATE(), NULL, NULL, 1, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 

INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 1, 1, N'Administrador', GETDATE(), NULL, NULL)

END
ELSE
BEGIN
SELECT @IdPrograma = IdPrograma FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/CambiarEstadoTercero'
INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 0, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END