USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]    Script Date: 07/29/2013 18:42:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]    Script Date: 07/29/2013 18:42:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]
		@IdSucursal INT OUTPUT, 	@IdEntidad INT, @Nombre NVARCHAR(256), @Indicativo INT,	@Telefono INT,	@Extension NUMERIC(10) = NULL,	@Celular NUMERIC(10),	@Correo NVARCHAR(256),	@Estado INT, @IdZona INT,	@Departamento INT,	@Municipio INT,	@Direccion NVARCHAR(256), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Sucursal(IdEntidad,Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado, IdZona, Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @Nombre, @Indicativo, @Telefono, @Extension, @Celular, @Correo, @Estado, @IdZona, @Departamento, @Municipio, @Direccion, @UsuarioCrea, GETDATE())
	SELECT @IdSucursal = @@IDENTITY 		
END

GO


