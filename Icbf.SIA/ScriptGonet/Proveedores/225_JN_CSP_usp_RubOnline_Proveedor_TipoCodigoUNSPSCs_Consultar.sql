USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]
	@Codigo NVARCHAR(64) = NULL, @Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCodUNSPSC, Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCodigoUNSPSC] 
 WHERE Codigo LIKE CASE WHEN @Codigo IS NULL THEN Codigo ELSE @Codigo + '%' END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
