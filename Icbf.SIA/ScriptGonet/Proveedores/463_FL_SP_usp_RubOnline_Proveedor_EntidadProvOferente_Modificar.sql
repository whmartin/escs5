USE [Proveedores]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 08/31/2013 11:24:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
GO

USE [Proveedores]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 08/31/2013 11:24:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que actualiza un(a) EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
		@IdEntidad INT,	
		@TipoEntOfProv BIT= NULL,	
		@IdTercero INT= NULL,	
		@IdTipoCiiuPrincipal INT = NULL,	
		@IdTipoCiiuSecundario INT= NULL,	
		@IdTipoSector INT = NULL,	
		@IdTipoClaseEntidad INT = NULL,	
		@IdTipoRamaPublica INT = NULL,	
		@IdTipoNivelGob INT = NULL,	
		@IdTipoNivelOrganizacional INT = NULL,	
		@IdTipoCertificadorCalidad INT = NULL,	
		@FechaCiiuPrincipal DATETIME = NULL,	
		@FechaCiiuSecundario DATETIME = NULL,	
		@FechaConstitucion DATETIME = NULL,	
		@FechaVigencia DATETIME = NULL,	
		@FechaMatriculaMerc DATETIME = NULL,	
		@FechaExpiracion DATETIME = NULL,	
		@TipoVigencia BIT = NULL,	
		@ExenMatriculaMer BIT = NULL,	
		@MatriculaMercantil NVARCHAR(20) = NULL,	
		@ObserValidador NVARCHAR(256) = NULL,	
		@AnoRegistro INT = NULL,	
		@IdEstado INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL
AS
BEGIN
	UPDATE Proveedor.EntidadProvOferente 
			SET TipoEntOfProv = ISNULL(@TipoEntOfProv,TipoEntOfProv) ,
			    IdTercero = ISNULL(@IdTercero,IdTercero), 
			    IdTipoCiiuPrincipal =ISNULL( @IdTipoCiiuPrincipal, IdTipoCiiuPrincipal ),
				IdTipoCiiuSecundario = @IdTipoCiiuSecundario,
				IdTipoSector =ISNULL( @IdTipoSector, IdTipoSector ),
				IdTipoClaseEntidad =ISNULL( @IdTipoClaseEntidad, IdTipoClaseEntidad ),
				IdTipoRamaPublica =ISNULL( @IdTipoRamaPublica, IdTipoRamaPublica ),
				IdTipoNivelGob =ISNULL( @IdTipoNivelGob, IdTipoNivelGob ),
				IdTipoNivelOrganizacional =ISNULL( @IdTipoNivelOrganizacional, IdTipoNivelOrganizacional ),
				IdTipoCertificadorCalidad =ISNULL( @IdTipoCertificadorCalidad, IdTipoCertificadorCalidad ),
				FechaCiiuPrincipal =ISNULL( @FechaCiiuPrincipal, FechaCiiuPrincipal ),
				FechaCiiuSecundario = @FechaCiiuSecundario,
				FechaConstitucion =ISNULL( @FechaConstitucion, FechaConstitucion ),
				FechaVigencia =ISNULL( @FechaVigencia, FechaVigencia ),
				FechaMatriculaMerc =ISNULL( @FechaMatriculaMerc, FechaMatriculaMerc ),
				FechaExpiracion =ISNULL( @FechaExpiracion, FechaExpiracion ),
				TipoVigencia =ISNULL( @TipoVigencia, TipoVigencia ),
				ExenMatriculaMer =ISNULL( @ExenMatriculaMer, ExenMatriculaMer ),
				MatriculaMercantil =ISNULL( @MatriculaMercantil, MatriculaMercantil ),
				ObserValidador =ISNULL( @ObserValidador, ObserValidador ),
				AnoRegistro =ISNULL( @AnoRegistro, AnoRegistro ),
				IdEstado =ISNULL( @IdEstado, IdEstado ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE() 
			   WHERE IdEntidad = @IdEntidad
END

GO


