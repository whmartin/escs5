USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_IdRamaEstructura]    Script Date: 07/24/2013 12:00:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_IdRamaEstructura]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_IdRamaEstructura]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_IdRamaEstructura]    Script Date: 07/24/2013 12:00:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  7/23/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno por Rama o Estructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_IdRamaEstructura] @IdRamaEstructura INT =0,@idEstado INT =1

					
AS
BEGIN

 SELECT ng.IdNiveldegobierno, ng.CodigoNiveldegobierno, ng.Descripcion, ng.Estado, ng.UsuarioCrea, ng.FechaCrea, ng.UsuarioModifica, ng.FechaModifica 
 FROM Proveedor.Niveldegobierno AS ng
	INNER JOIN Proveedor.RamaoEstructura_Niveldegobierno AS rsng ON ng.IdNiveldegobierno=rsng.IdNiveldegobierno
	WHERE rsng.IdRamaEstructura= @IdRamaEstructura AND   ng.Estado =@idEstado
	ORDER BY Descripcion ASC
 END


GO


