USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina indice CodigoUnico de la entidad [Proveedor].[TipoCargoEntidad]
-- =============================================
/****** Object:  Index [CodigoUnico]    Script Date: 06/26/2013 23:42:10 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCargoEntidad]') AND name = N'CodigoUnico')
DROP INDEX [CodigoUnico] ON [Proveedor].[TipoCargoEntidad] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Crea indice CodigoUnico en la entidad [Proveedor].[TipoCargoEntidad]
-- =============================================
/****** Object:  Index [CodigoUnico]    Script Date: 06/26/2013 23:42:12 ******/
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoCargoEntidad] 
(
	[CodigoTipoCargoEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

