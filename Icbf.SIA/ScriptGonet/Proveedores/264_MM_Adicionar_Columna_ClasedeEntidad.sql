USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde Sámano    
-- Create date:         20/01/2014
-- Description:     Crea la entidad Proveedor.Tmp_ClasedeEntidad
-- =============================================
/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Proveedor.ClasedeEntidad
	DROP CONSTRAINT FK_ClasedeEntidad_TipodeActividad
GO
ALTER TABLE Proveedor.TipodeActividad SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'Proveedor.TipodeActividad', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'Proveedor.TipodeActividad', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'Proveedor.TipodeActividad', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE Proveedor.Tmp_ClasedeEntidad
	(
	IdClasedeEntidad int NOT NULL IDENTITY (1, 1),
	CodigoClasedeEntidad varchar(10) NULL,
	Descripcion nvarchar(128) NOT NULL,
	IdTipodeActividad int NOT NULL,
	Estado bit NOT NULL,
	UsuarioCrea nvarchar(250) NOT NULL,
	FechaCrea datetime NOT NULL,
	UsuarioModifica nvarchar(250) NULL,
	FechaModifica datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE Proveedor.Tmp_ClasedeEntidad SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT Proveedor.Tmp_ClasedeEntidad ON
GO
IF EXISTS(SELECT * FROM Proveedor.ClasedeEntidad)
	 EXEC('INSERT INTO Proveedor.Tmp_ClasedeEntidad (IdClasedeEntidad, Descripcion, IdTipodeActividad, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica)
		SELECT IdClasedeEntidad, Descripcion, IdTipodeActividad, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM Proveedor.ClasedeEntidad WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT Proveedor.Tmp_ClasedeEntidad OFF
GO
DROP TABLE Proveedor.ClasedeEntidad
GO
EXECUTE sp_rename N'Proveedor.Tmp_ClasedeEntidad', N'ClasedeEntidad', 'OBJECT' 
GO
ALTER TABLE Proveedor.ClasedeEntidad ADD CONSTRAINT
	PK_ClasedeEntidad PRIMARY KEY CLUSTERED 
	(
	IdClasedeEntidad
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX NombreUnico ON Proveedor.ClasedeEntidad
	(
	IdTipodeActividad,
	Descripcion
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE Proveedor.ClasedeEntidad ADD CONSTRAINT
	FK_ClasedeEntidad_TipodeActividad FOREIGN KEY
	(
	IdTipodeActividad
	) REFERENCES Proveedor.TipodeActividad
	(
	IdTipodeActividad
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'Proveedor.ClasedeEntidad', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'Proveedor.ClasedeEntidad', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'Proveedor.ClasedeEntidad', 'Object', 'CONTROL') as Contr_Per 