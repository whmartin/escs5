--------------No tiene el Use de la base de datos ---------------
USE [SIA]
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Permisos Terceros y Notificacion Judicial
-- =============================================
GO

DECLARE @IdModulo  INT;
DECLARE @IdPrograma INT;
SET @IdModulo = (SELECT IdModulo FROM SEG.Modulo WHERE NombreModulo = 'Proveedores')

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/GestionTercero') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@IdModulo, N'Gesti�n Terceros', N'PROVEEDOR/GestionTercero', 0, 1, N'administrador', GETDATE(), NULL, NULL, 1, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 

INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END

IF NOT EXISTS( SELECT IdModulo FROM [SEG].[Programa] WHERE  CodigoPrograma ='PROVEEDOR/NotificacionJudicial') BEGIN
INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
VALUES (@IdModulo, N'Notificacion Judicial', N'PROVEEDOR/NotificacionJudicial', 0, 1, N'administrador', GETDATE(), NULL, NULL, 0, 1, NULL)
SELECT @IdPrograma = @@IDENTITY 

INSERT [SEG].[Permiso] ([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], [UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion]) 
VALUES (@IdPrograma, 1, 1, 1, 0, 1, N'Administrador', GETDATE(), NULL, NULL)
END