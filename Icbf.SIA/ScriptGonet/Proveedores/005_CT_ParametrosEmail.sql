USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Crea la entidad [Proveedor].[ParametrosEmail]
-- =============================================
/****** Object:  Table [Proveedor].[ParametrosEmail]    Script Date: 06/14/2013 19:19:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ParametrosEmail]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ParametrosEmail](
	[IdParametroID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoParametroEmail] [nvarchar](100) NOT NULL,
	[DescripcionParametrosEmail] [nvarchar](255) NULL,
	[ValorMesesParametroEmail] [int] NULL,
 CONSTRAINT [UNIC_ParametrosEmail] UNIQUE NONCLUSTERED 
(
	[CodigoParametroEmail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO