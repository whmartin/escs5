USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]    Script Date: 06/27/2013 10:48:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]    Script Date: 06/27/2013 10:48:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]
	@CodigoRegimenTributario NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@IdTipoPersona INT = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE CodigoRegimenTributario = CASE WHEN @CodigoRegimenTributario IS NULL THEN CodigoRegimenTributario ELSE @CodigoRegimenTributario END 
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
 AND trt.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN trt.IdTipoPersona ELSE @IdTipoPersona END 
 AND trt.Estado = CASE WHEN @Estado IS NULL THEN trt.Estado ELSE @Estado END
END


GO


