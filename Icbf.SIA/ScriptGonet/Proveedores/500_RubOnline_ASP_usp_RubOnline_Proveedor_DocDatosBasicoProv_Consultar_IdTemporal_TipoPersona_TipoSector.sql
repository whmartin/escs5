USE [SIA]
GO
-- =============================================
-- Author:		Carlos Cubillos
-- Description:	Se corrige procedimiento para que se filtren los documentos inactivos.
-- =============================================


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]    Script Date: 10/15/2013 16:49:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]    Script Date: 10/15/2013 16:49:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdTemporal y TipoPersona y TipoSector
-- =============================================


CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector] 
( @IdTemporal VARCHAR(20), @TipoPersona varchar(2), @TipoSector varchar(2) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionProveedores';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTemporal = @IdTemporal
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
	AND tdp.estado=1
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento






GO


