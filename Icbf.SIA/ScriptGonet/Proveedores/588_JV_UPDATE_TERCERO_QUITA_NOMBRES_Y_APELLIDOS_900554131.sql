USE [SIA]
GO
/*
Autor:Juan Carlos Valverde S�mano
Fecha:01/OCT/2014
Descripci�n: Actualiza el tercero con n�mero de identificaci�n=900554131
dejando nombres y apellidos en blanco, ya que por un error del aplicativo
el registro tenia valor en estos campos tratandose de una persona Juridica.
*/
UPDATE Oferente.TERCERO
SET PRIMERNOMBRE=''
,SEGUNDONOMBRE=''
,PRIMERAPELLIDO=''
,SEGUNDOAPELLIDO=''
WHERE
NUMEROIDENTIFICACION='900554131'



