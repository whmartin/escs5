USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que actualiza un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]
		@IdDocAdjunto INT,	@IdExpEntidad INT,	@IdTipoDocGrupo INT,	@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocExperienciaEntidad 
	SET IdExpEntidad = @IdExpEntidad, IdTipoDocGrupo = @IdTipoDocGrupo, Descripcion = @Descripcion, LinkDocumento = @LinkDocumento, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdDocAdjunto = @IdDocAdjunto
END
