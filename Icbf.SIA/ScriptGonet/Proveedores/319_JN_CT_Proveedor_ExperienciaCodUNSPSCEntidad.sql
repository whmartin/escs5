USE [SIA]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  7/15/2013 11:30:20 PM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]') AND type in (N'U'))
BEGIN
	Print 'Ya Existe La tabla ExperienciaCodUNSPSCEntidad a Crear'
	ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] DROP CONSTRAINT FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC;
	ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] DROP CONSTRAINT FK_IdExpEntidad_InfoExperienciaEntidad;
	DROP TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad];

END
CREATE TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad](
 [IdExpCOD] [INT]  IDENTITY(1,1) NOT NULL,
 [IdTipoCodUNSPSC] [INT]  NOT NULL,
 [IdExpEntidad] [INT]  ,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_ExperienciaCodUNSPSCEntidad] PRIMARY KEY CLUSTERED 
(
 	[IdExpCOD] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]') AND type in (N'U'))
BEGIN
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] ADD CONSTRAINT
	FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC FOREIGN KEY
	(
	IdTipoCodUNSPSC
	) REFERENCES Proveedor.TipoCodigoUNSPSC
	(
	IdTipoCodUNSPSC
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 

ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] ADD CONSTRAINT
	FK_IdExpEntidad_InfoExperienciaEntidad FOREIGN KEY
	(
	IdExpEntidad
	) REFERENCES Proveedor.InfoExperienciaEntidad
	(
	IdExpEntidad
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
end