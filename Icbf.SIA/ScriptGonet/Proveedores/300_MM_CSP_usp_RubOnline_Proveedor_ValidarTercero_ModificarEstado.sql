USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]    Script Date: 07/04/2013 08:45:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]    Script Date: 07/04/2013 08:45:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]
		@IdTercero INT, @IdEstadoTercero INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Oferente.Tercero 
	SET IdEstadoTercero = @IdEstadoTercero, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdTercero = @IdTercero
END

GO


