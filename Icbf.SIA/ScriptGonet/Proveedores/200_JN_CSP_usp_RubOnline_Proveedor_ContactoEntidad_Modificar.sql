USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]
		@IdContacto INT,	@IdEntidad INT, @IdSucursal INT, @IdTelContacto INT,	@IdTipoDocIdentifica INT,	@IdTipoCargoEntidad INT,	@NumeroIdentificacion NUMERIC(30),	@PrimerNombre NVARCHAR(128),	@SegundoNombre NVARCHAR(128),	@PrimerApellido NVARCHAR(128),	@SegundoApellido NVARCHAR(128),	@Dependencia NVARCHAR(128),	@Email NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.ContactoEntidad 
	SET IdEntidad = @IdEntidad, IdSucursal = @IdSucursal, IdTelContacto = @IdTelContacto, IdTipoDocIdentifica = @IdTipoDocIdentifica, IdTipoCargoEntidad = @IdTipoCargoEntidad, NumeroIdentificacion = @NumeroIdentificacion, PrimerNombre = @PrimerNombre, SegundoNombre = @SegundoNombre, PrimerApellido = @PrimerApellido, SegundoApellido = @SegundoApellido, Dependencia = @Dependencia, Email = @Email, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdContacto = @IdContacto
END
