USE SIA
GO---Crea el esquema Auditoria en caso que no exista en la base de datos
---2014/07/31
---Auditoria detallada

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Auditoria')
BEGIN
	EXEC( 'CREATE SCHEMA Auditoria' );
END
GO