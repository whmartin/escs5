USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]    Script Date: 06/14/2013 19:31:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]
	@IdEstadoValidacionDocumental INT
AS
BEGIN
 SELECT IdEstadoValidacionDocumental, CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[EstadoValidacionDocumental] WHERE  IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END
GO