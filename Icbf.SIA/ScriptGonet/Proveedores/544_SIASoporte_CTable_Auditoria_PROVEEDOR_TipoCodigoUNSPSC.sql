/****** Object:  Table [Auditoria].[PROVEEDOR_TipoCodigoUNSPSC]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para TipoCodigoUNSPSC ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_TipoCodigoUNSPSC]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_TipoCodigoUNSPSC]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_TipoCodigoUNSPSC]([IdTipoCodUNSPSC] [int]  NULL, [Codigo_old] [nvarchar] (64)  NULL,[Codigo_new] [nvarchar] (64)  NULL,[Descripcion_old] [nvarchar] (250)  NULL,[Descripcion_new] [nvarchar] (250)  NULL,[Estado_old] [bit]  NULL,[Estado_new] [bit]  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[CodigoSegmento_old] [nvarchar] (50)  NULL,[CodigoSegmento_new] [nvarchar] (50)  NULL,[NombreSegmento_old] [nvarchar] (250)  NULL,[NombreSegmento_new] [nvarchar] (250)  NULL,[CodigoFamilia_old] [nvarchar] (50)  NULL,[CodigoFamilia_new] [nvarchar] (50)  NULL,[NombreFamilia_old] [nvarchar] (250)  NULL,[NombreFamilia_new] [nvarchar] (250)  NULL,[CodigoClase_old] [nvarchar] (50)  NULL,[CodigoClase_new] [nvarchar] (50)  NULL,[NombreClase_old] [nvarchar] (250)  NULL,[NombreClase_new] [nvarchar] (250)  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO