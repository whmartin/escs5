USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]    Script Date: 06/14/2013 19:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]
		@IdTipodeActividad INT = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT Proveedor.ClasedeEntidad.IdClasedeEntidad, Proveedor.ClasedeEntidad.IdTipodeActividad, Proveedor.ClasedeEntidad.Descripcion, Proveedor.ClasedeEntidad.Estado, Proveedor.ClasedeEntidad.UsuarioCrea, Proveedor.ClasedeEntidad.FechaCrea, Proveedor.ClasedeEntidad.UsuarioModifica, Proveedor.ClasedeEntidad.FechaModifica 
 ,Proveedor.TipodeActividad.Descripcion AS TipodeActividad
 FROM [Proveedor].[ClasedeEntidad] 
 INNER JOIN Proveedor.TipodeActividad ON Proveedor.TipodeActividad.IdTipodeActividad = Proveedor.ClasedeEntidad.IdTipodeActividad
 WHERE Proveedor.ClasedeEntidad.IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN Proveedor.ClasedeEntidad.IdTipodeActividad ELSE @IdTipodeActividad END AND Proveedor.ClasedeEntidad.Descripcion = CASE WHEN @Descripcion IS NULL THEN Proveedor.ClasedeEntidad.Descripcion ELSE @Descripcion END AND Proveedor.ClasedeEntidad.Estado = CASE WHEN @Estado IS NULL THEN Proveedor.ClasedeEntidad.Estado ELSE @Estado END
END
GO