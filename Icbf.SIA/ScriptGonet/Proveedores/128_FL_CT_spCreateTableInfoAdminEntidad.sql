USE [SIA]
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Tabla InfoAdminEntidad
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeentidadPublica]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipoRegimenTributario]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Zona]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_Zona]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[InfoAdminEntidad]    Script Date: 06/24/2013 21:13:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[InfoAdminEntidad]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Crea la entidad [Proveedor].[InfoAdminEntidad]
-- =============================================
/****** Object:  Table [Proveedor].[InfoAdminEntidad]    Script Date: 06/24/2013 21:13:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[InfoAdminEntidad](
	[IdInfoAdmin] [int] IDENTITY(1,1) NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdTipoRegTrib] [int] NULL,
	[IdTipoOrigenCapital] [int] NULL,
	[IdTipoActividad] [int] NULL,
	[IdTipoEntidad] [int] NULL,
	[IdTipoNaturalezaJurid] [int] NULL,
	[IdTipoRangosTrabajadores] [int] NULL,
	[IdTipoRangosActivos] [int] NULL,
	[IdRepLegal] [int] NULL,
	[IdTipoCertificaTamano] [int] NULL,
	[IdTipoEntidadPublica] [int] NULL,
	[IdDepartamentoConstituida] [int] NULL,
	[IdMunicipioConstituida] [int] NULL,
	[IdDepartamentoDirComercial] [int] NULL,
	[IdMunicipioDirComercial] [int] NULL,
	[DireccionComercial] [nvarchar](50) NULL,
	[IdZona] [nvarchar](128) NULL,
	[NombreComercial] [nvarchar](128) NULL,
	[NombreEstablecimiento] [nvarchar](128) NULL,
	[Sigla] [nvarchar](50) NULL,
	[PorctjPrivado] [int] NULL,
	[PorctjPublico] [int] NULL,
	[SitioWeb] [nvarchar](128) NULL,
	[NombreEntidadAcreditadora] [nvarchar](1) NULL,
	[Organigrama] [bit] NULL,
	[TotalPnalAnnoPrevio] [numeric](10, 0) NULL,
	[VincLaboral] [numeric](10, 0) NULL,
	[PrestServicios] [numeric](10, 0) NULL,
	[Voluntariado] [numeric](10, 0) NULL,
	[VoluntPermanente] [numeric](10, 0) NULL,
	[Asociados] [numeric](10, 0) NULL,
	[Mision] [numeric](10, 0) NULL,
	[PQRS] [bit] NULL,
	[GestionDocumental] [bit] NULL,
	[AuditoriaInterna] [bit] NULL,
	[ManProcedimiento] [bit] NULL,
	[ManPracticasAmbiente] [bit] NULL,
	[ManComportOrg] [bit] NULL,
	[ManFunciones] [bit] NULL,
	[ProcRegInfoContable] [bit] NULL,
	[PartMesasTerritoriales] [bit] NULL,
	[PartAsocAgremia] [bit] NULL,
	[PartConsejosComun] [bit] NULL,
	[ConvInterInst] [bit] NULL,
	[ProcSeleccGral] [bit] NULL,
	[ProcSeleccEtnico] [bit] NULL,
	[PlanInduccCapac] [bit] NULL,
	[EvalDesemp] [bit] NULL,
	[PlanCualificacion] [bit] NULL,
	[NumSedes] [int] NULL,
	[SedesPropias] [bit] NULL,
	[IdEstado] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InfoAdminEntidad] PRIMARY KEY CLUSTERED 
(
	[IdInfoAdmin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente]
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad] FOREIGN KEY([IdTipoActividad])
REFERENCES [Proveedor].[TipodeActividad] ([IdTipodeActividad])
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad]
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica] FOREIGN KEY([IdTipoEntidadPublica])
REFERENCES [Proveedor].[TipodeentidadPublica] ([IdTipodeentidadPublica])
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica]
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad] FOREIGN KEY([IdTipoEntidad])
REFERENCES [Proveedor].[Tipoentidad] ([IdTipoentidad])
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad]
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario] FOREIGN KEY([IdTipoRegTrib])
REFERENCES [Proveedor].[TipoRegimenTributario] ([IdTipoRegimenTributario])
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario]
GO



