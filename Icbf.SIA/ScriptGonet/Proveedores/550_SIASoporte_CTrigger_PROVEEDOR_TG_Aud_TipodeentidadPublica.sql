/****** Object:  Trigger [PROVEEDOR].[TG_Aud_TipodeentidadPublica]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para TipodeentidadPublica
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_TipodeentidadPublica]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_TipodeentidadPublica]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_TipodeentidadPublica]
  ON  [PROVEEDOR].[TipodeentidadPublica]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_TipodeentidadPublica]
  ([IdTipodeentidadPublica],[CodigoTipodeentidadPublica_old], [CodigoTipodeentidadPublica_new],[Descripcion_old], [Descripcion_new],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdTipodeentidadPublica,deleted.CodigoTipodeentidadPublica,inserted.CodigoTipodeentidadPublica,deleted.Descripcion,inserted.Descripcion,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdTipodeentidadPublica = deleted.IdTipodeentidadPublicaEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_TipodeentidadPublica]
  ([IdTipodeentidadPublica],[CodigoTipodeentidadPublica_old],[Descripcion_old],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdTipodeentidadPublica,deleted.CodigoTipodeentidadPublica,deleted.Descripcion,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_TipodeentidadPublica]
  ([IdTipodeentidadPublica],[CodigoTipodeentidadPublica_new],[Descripcion_new],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdTipodeentidadPublica,inserted.CodigoTipodeentidadPublica,inserted.Descripcion,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO