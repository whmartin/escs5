USE [SIA]
GO

-- =================================================
-- Author:          Gonet\Bayron Lara
-- Create date:     16/07/2013 04:56:29 p.m.
-- Description:     Elimina las tablas y las llaves
-- =================================================



/****** Object:  ForeignKey [FK_ClasedeEntidad_TipodeActividad]    Script Date: 07/16/2013 12:47:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad] DROP CONSTRAINT [FK_ClasedeEntidad_TipodeActividad]
GO
/****** Object:  ForeignKey [FK_DOCADJUNTOTERCERO_TERCERO]    Script Date: 07/16/2013 12:47:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero] DROP CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO]
GO
/****** Object:  ForeignKey [FK_DOCADJUNTOTERCERO_TERCERO1]    Script Date: 07/16/2013 12:47:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO1]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero] DROP CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1]
GO
/****** Object:  ForeignKey [FK_DocDatosBasicoProv_EntidadProvOferente]    Script Date: 07/16/2013 12:47:50 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv] DROP CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente]
GO
/****** Object:  ForeignKey [FK_DocDatosBasicoProv_TipoDocumento]    Script Date: 07/16/2013 12:47:50 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv] DROP CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento]
GO
/****** Object:  ForeignKey [FK_DocExperienciaEntidad_InfoExperienciaEntidad]    Script Date: 07/16/2013 12:48:48 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocExperienciaEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]'))
ALTER TABLE [Proveedor].[DocExperienciaEntidad] DROP CONSTRAINT [FK_DocExperienciaEntidad_InfoExperienciaEntidad]
GO
/****** Object:  ForeignKey [FK_DocExperienciaEntidad_TipoDocumento]    Script Date: 07/16/2013 12:48:48 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocExperienciaEntidad_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]'))
ALTER TABLE [Proveedor].[DocExperienciaEntidad] DROP CONSTRAINT [FK_DocExperienciaEntidad_TipoDocumento]
GO
/****** Object:  ForeignKey [FK_DocFinancieraProv_InfoFinancieraEntidad]    Script Date: 07/16/2013 12:48:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv] DROP CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad]
GO
/****** Object:  ForeignKey [FK_DocFinancieraProv_TipoDocumento]    Script Date: 07/16/2013 12:48:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv] DROP CONSTRAINT [FK_DocFinancieraProv_TipoDocumento]
GO
/****** Object:  ForeignKey [FK_DocNotificacionJudicial_NotificacionJudicial]    Script Date: 07/16/2013 12:49:05 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocNotificacionJudicial_NotificacionJudicial]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]'))
ALTER TABLE [Proveedor].[DocNotificacionJudicial] DROP CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_EstadoDatosBasicos]    Script Date: 07/16/2013 12:49:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_EstadoDatosBasicos]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_Niveldegobierno]    Script Date: 07/16/2013 12:49:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_Niveldegobierno]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_NivelOrganizacional]    Script Date: 07/16/2013 12:49:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_NivelOrganizacional]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_TERCERO]    Script Date: 07/16/2013 12:49:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TERCERO]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_TipoCiiu]    Script Date: 07/16/2013 12:49:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoCiiu]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TipoCiiu]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_TipoSectorEntidad]    Script Date: 07/16/2013 12:49:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoSectorEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad]
GO
/****** Object:  ForeignKey [fk_IdAcuerdoProveedor]    Script Date: 07/16/2013 12:49:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[fk_IdAcuerdoProveedor]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [fk_IdAcuerdoProveedor]
GO
/****** Object:  ForeignKey [FK_IdExpEntidad_InfoExperienciaEntidad]    Script Date: 07/16/2013 12:49:43 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_IdExpEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]'))
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] DROP CONSTRAINT [FK_IdExpEntidad_InfoExperienciaEntidad]
GO
/****** Object:  ForeignKey [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]    Script Date: 07/16/2013 12:49:43 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]'))
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] DROP CONSTRAINT [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_EntidadProvOferente]    Script Date: 07/16/2013 12:49:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_TipodeActividad]    Script Date: 07/16/2013 12:49:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_TipodeentidadPublica]    Script Date: 07/16/2013 12:49:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeentidadPublica]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_Tipoentidad]    Script Date: 07/16/2013 12:49:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_TipoRegimenTributario]    Script Date: 07/16/2013 12:49:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipoRegimenTributario]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_Zona]    Script Date: 07/16/2013 12:49:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Zona]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_Zona]
GO
/****** Object:  ForeignKey [FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]    Script Date: 07/16/2013 12:49:59 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]'))
ALTER TABLE [Proveedor].[InfoExperienciaEntidad] DROP CONSTRAINT [FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]
GO
/****** Object:  ForeignKey [FK_InfoFinancieraEntidad_EntidadProvOferente]    Script Date: 07/16/2013 12:50:07 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente]
GO
/****** Object:  ForeignKey [FK_InfoFinancieraEntidad_EstadoValidacionDocumental]    Script Date: 07/16/2013 12:50:07 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EstadoValidacionDocumental]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental]
GO
/****** Object:  ForeignKey [FK_InfoFinancieraEntidad_Vigencia]    Script Date: 07/16/2013 12:50:07 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_Vigencia]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia]
GO
/****** Object:  ForeignKey [FK_MotivoCambioEstado_TERCERO]    Script Date: 07/16/2013 12:50:13 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_MotivoCambioEstado_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[MotivoCambioEstado]'))
ALTER TABLE [Proveedor].[MotivoCambioEstado] DROP CONSTRAINT [FK_MotivoCambioEstado_TERCERO]
GO
/****** Object:  ForeignKey [FK_NotificacionJudicial_EntidadProvOferente]    Script Date: 07/16/2013 12:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_NotificacionJudicial_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]'))
ALTER TABLE [Proveedor].[NotificacionJudicial] DROP CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente]
GO
/****** Object:  ForeignKey [FK_Sucursal_Departamento]    Script Date: 07/16/2013 12:50:56 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_Departamento]
GO
/****** Object:  ForeignKey [FK_Sucursal_IdEntidad]    Script Date: 07/16/2013 12:50:56 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_IdEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_IdEntidad]
GO
/****** Object:  ForeignKey [FK_Sucursal_Municipio]    Script Date: 07/16/2013 12:50:56 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Municipio]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_Municipio]
GO
/****** Object:  ForeignKey [FK_TipoDocumentoPrograma_Programa]    Script Date: 07/16/2013 12:52:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_Programa]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] DROP CONSTRAINT [FK_TipoDocumentoPrograma_Programa]
GO
/****** Object:  ForeignKey [FK_TipoDocumentoPrograma_TipoDocumento]    Script Date: 07/16/2013 12:52:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] DROP CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento]
GO
/****** Object:  ForeignKey [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]    Script Date: 07/16/2013 12:53:00 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoDatosBasicosEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoDatosBasicosEntidad] DROP CONSTRAINT [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]
GO
/****** Object:  ForeignKey [IdInfoExperienciaEntidad_InfoExperienciaEntidad]    Script Date: 07/16/2013 12:53:10 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoExperienciaEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoExperienciaEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoExperienciaEntidad] DROP CONSTRAINT [IdInfoExperienciaEntidad_InfoExperienciaEntidad]
GO
/****** Object:  ForeignKey [IdInfoFinancieraEntidad_InfoFinancieraEntidad]    Script Date: 07/16/2013 12:53:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoFinancieraEntidad_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoFinancieraEntidad] DROP CONSTRAINT [IdInfoFinancieraEntidad_InfoFinancieraEntidad]
GO
/****** Object:  ForeignKey [IdTercero_Tercero]    Script Date: 07/16/2013 12:53:28 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdTercero_Tercero]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarTercero]'))
ALTER TABLE [Proveedor].[ValidarTercero] DROP CONSTRAINT [IdTercero_Tercero]
GO
/****** Object:  Table [Proveedor].[DocFinancieraProv]    Script Date: 07/16/2013 12:48:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv] DROP CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv] DROP CONSTRAINT [FK_DocFinancieraProv_TipoDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocFinancieraProv]
GO
/****** Object:  Table [Proveedor].[DocNotificacionJudicial]    Script Date: 07/16/2013 12:49:05 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocNotificacionJudicial_NotificacionJudicial]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]'))
ALTER TABLE [Proveedor].[DocNotificacionJudicial] DROP CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocNotificacionJudicial]
GO
/****** Object:  Table [Proveedor].[ValidarInfoFinancieraEntidad]    Script Date: 07/16/2013 12:53:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoFinancieraEntidad_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoFinancieraEntidad] DROP CONSTRAINT [IdInfoFinancieraEntidad_InfoFinancieraEntidad]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoFinancieraEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[ValidarInfoFinancieraEntidad]
GO
/****** Object:  Table [Proveedor].[Sucursal]    Script Date: 07/16/2013 12:50:56 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_Departamento]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_IdEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_IdEntidad]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Municipio]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_Municipio]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Sucursal]') AND type in (N'U'))
DROP TABLE [Proveedor].[Sucursal]
GO
/****** Object:  Table [Proveedor].[ValidarInfoDatosBasicosEntidad]    Script Date: 07/16/2013 12:53:00 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoDatosBasicosEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoDatosBasicosEntidad] DROP CONSTRAINT [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoDatosBasicosEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[ValidarInfoDatosBasicosEntidad]
GO
/****** Object:  Table [Proveedor].[DocDatosBasicoProv]    Script Date: 07/16/2013 12:47:50 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv] DROP CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv] DROP CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocDatosBasicoProv]
GO
/****** Object:  Table [Proveedor].[InfoAdminEntidad]    Script Date: 07/16/2013 12:49:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeentidadPublica]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipoRegimenTributario]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Zona]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_Zona]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[InfoAdminEntidad]
GO
/****** Object:  Table [Proveedor].[InfoFinancieraEntidad]    Script Date: 07/16/2013 12:50:07 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EstadoValidacionDocumental]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_Vigencia]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[InfoFinancieraEntidad]
GO
/****** Object:  Table [Proveedor].[NotificacionJudicial]    Script Date: 07/16/2013 12:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_NotificacionJudicial_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]'))
ALTER TABLE [Proveedor].[NotificacionJudicial] DROP CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]') AND type in (N'U'))
DROP TABLE [Proveedor].[NotificacionJudicial]
GO
/****** Object:  Table [Proveedor].[MotivoCambioEstado]    Script Date: 07/16/2013 12:50:13 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_MotivoCambioEstado_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[MotivoCambioEstado]'))
ALTER TABLE [Proveedor].[MotivoCambioEstado] DROP CONSTRAINT [FK_MotivoCambioEstado_TERCERO]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[MotivoCambioEstado]') AND type in (N'U'))
DROP TABLE [Proveedor].[MotivoCambioEstado]
GO
/****** Object:  Table [Proveedor].[DocExperienciaEntidad]    Script Date: 07/16/2013 12:48:48 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocExperienciaEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]'))
ALTER TABLE [Proveedor].[DocExperienciaEntidad] DROP CONSTRAINT [FK_DocExperienciaEntidad_InfoExperienciaEntidad]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocExperienciaEntidad_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]'))
ALTER TABLE [Proveedor].[DocExperienciaEntidad] DROP CONSTRAINT [FK_DocExperienciaEntidad_TipoDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocExperienciaEntidad]
GO
/****** Object:  Table [Proveedor].[EntidadProvOferente]    Script Date: 07/16/2013 12:49:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_EstadoDatosBasicos]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_Niveldegobierno]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_NivelOrganizacional]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TERCERO]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoCiiu]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TipoCiiu]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoSectorEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[fk_IdAcuerdoProveedor]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [fk_IdAcuerdoProveedor]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]') AND type in (N'U'))
DROP TABLE [Proveedor].[EntidadProvOferente]
GO
/****** Object:  Table [Proveedor].[ValidarInfoExperienciaEntidad]    Script Date: 07/16/2013 12:53:10 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoExperienciaEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoExperienciaEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoExperienciaEntidad] DROP CONSTRAINT [IdInfoExperienciaEntidad_InfoExperienciaEntidad]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoExperienciaEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[ValidarInfoExperienciaEntidad]
GO
/****** Object:  Table [Proveedor].[ValidarTercero]    Script Date: 07/16/2013 12:53:28 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdTercero_Tercero]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarTercero]'))
ALTER TABLE [Proveedor].[ValidarTercero] DROP CONSTRAINT [IdTercero_Tercero]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarTercero]') AND type in (N'U'))
DROP TABLE [Proveedor].[ValidarTercero]
GO
/****** Object:  Table [Proveedor].[DocAdjuntoTercero]    Script Date: 07/16/2013 12:47:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero] DROP CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO1]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero] DROP CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocAdjuntoTercero]
GO
/****** Object:  Table [Proveedor].[ExperienciaCodUNSPSCEntidad]    Script Date: 07/16/2013 12:49:43 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_IdExpEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]'))
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] DROP CONSTRAINT [FK_IdExpEntidad_InfoExperienciaEntidad]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]'))
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] DROP CONSTRAINT [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad]
GO
/****** Object:  Table [Proveedor].[TipoDocumentoPrograma]    Script Date: 07/16/2013 12:52:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_Programa]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] DROP CONSTRAINT [FK_TipoDocumentoPrograma_Programa]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] DROP CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoDocumentoPrograma]
GO
/****** Object:  Table [Proveedor].[ClasedeEntidad]    Script Date: 07/16/2013 12:47:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad] DROP CONSTRAINT [FK_ClasedeEntidad_TipodeActividad]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[ClasedeEntidad]
GO
/****** Object:  Table [Proveedor].[InfoExperienciaEntidad]    Script Date: 07/16/2013 12:49:59 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]'))
ALTER TABLE [Proveedor].[InfoExperienciaEntidad] DROP CONSTRAINT [FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[InfoExperienciaEntidad]
GO
/****** Object:  Table [Proveedor].[Niveldegobierno]    Script Date: 07/16/2013 12:50:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND type in (N'U'))
DROP TABLE [Proveedor].[Niveldegobierno]
GO
/****** Object:  Table [Proveedor].[NivelOrganizacional]    Script Date: 07/16/2013 12:50:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[NivelOrganizacional]') AND type in (N'U'))
DROP TABLE [Proveedor].[NivelOrganizacional]
GO
/****** Object:  Table [Proveedor].[ParametrosEmail]    Script Date: 07/16/2013 12:50:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ParametrosEmail]') AND type in (N'U'))
DROP TABLE [Proveedor].[ParametrosEmail]
GO
/****** Object:  Table [Proveedor].[ContactoEntidad]    Script Date: 07/16/2013 12:47:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ContactoEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[ContactoEntidad]
GO
/****** Object:  Table [Proveedor].[Acuerdos]    Script Date: 07/16/2013 12:47:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Acuerdos]') AND type in (N'U'))
DROP TABLE [Proveedor].[Acuerdos]
GO
/****** Object:  Table [Proveedor].[EstadoDatosBasicos]    Script Date: 07/16/2013 12:49:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoDatosBasicos]') AND type in (N'U'))
DROP TABLE [Proveedor].[EstadoDatosBasicos]
GO
/****** Object:  Table [Proveedor].[EstadoValidacionDocumental]    Script Date: 07/16/2013 12:49:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoValidacionDocumental]') AND type in (N'U'))
DROP TABLE [Proveedor].[EstadoValidacionDocumental]
GO
/****** Object:  Table [Proveedor].[TipoCargoEntidad]    Script Date: 07/16/2013 12:51:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCargoEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoCargoEntidad]
GO
/****** Object:  Table [Proveedor].[TipoCiiu]    Script Date: 07/16/2013 12:51:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCiiu]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoCiiu]
GO
/****** Object:  Table [Proveedor].[TipoCodigoUNSPSC]    Script Date: 07/16/2013 12:51:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCodigoUNSPSC]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoCodigoUNSPSC]
GO
/****** Object:  Table [Proveedor].[TipodeActividad]    Script Date: 07/16/2013 12:51:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipodeActividad]
GO
/****** Object:  Table [Proveedor].[TipodeentidadPublica]    Script Date: 07/16/2013 12:51:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeentidadPublica]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipodeentidadPublica]
GO
/****** Object:  Table [Proveedor].[TipoDocIdentifica]    Script Date: 07/16/2013 12:52:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocIdentifica]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoDocIdentifica]
GO
/****** Object:  Table [Proveedor].[TipoDocumento]    Script Date: 07/16/2013 12:52:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumento]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoDocumento]
GO
/****** Object:  Table [Proveedor].[RamaoEstructura]    Script Date: 07/16/2013 12:50:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND type in (N'U'))
DROP TABLE [Proveedor].[RamaoEstructura]
GO
/****** Object:  Table [Proveedor].[Tipoentidad]    Script Date: 07/16/2013 12:52:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Tipoentidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[Tipoentidad]
GO
/****** Object:  Table [Proveedor].[TipoRegimenTributario]    Script Date: 07/16/2013 12:52:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoRegimenTributario]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoRegimenTributario]
GO
/****** Object:  Table [Proveedor].[TipoSectorEntidad]    Script Date: 07/16/2013 12:52:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoSectorEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoSectorEntidad]
GO
/****** Object:  Table [Proveedor].[TablaParametrica]    Script Date: 07/16/2013 12:51:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TablaParametrica]') AND type in (N'U'))
DROP TABLE [Proveedor].[TablaParametrica]
GO

