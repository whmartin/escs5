/****** Object:  Trigger [PROVEEDOR].[TG_Aud_ExperienciaCodUNSPSCEntidad]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para ExperienciaCodUNSPSCEntidad
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_ExperienciaCodUNSPSCEntidad]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_ExperienciaCodUNSPSCEntidad]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_ExperienciaCodUNSPSCEntidad]
  ON  [PROVEEDOR].[ExperienciaCodUNSPSCEntidad]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_ExperienciaCodUNSPSCEntidad]
  ([IdExpCOD],[IdTipoCodUNSPSC_old], [IdTipoCodUNSPSC_new],[IdExpEntidad_old], [IdExpEntidad_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdExpCOD,deleted.IdTipoCodUNSPSC,inserted.IdTipoCodUNSPSC,deleted.IdExpEntidad,inserted.IdExpEntidad,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdExpCOD = deleted.IdExpCODEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_ExperienciaCodUNSPSCEntidad]
  ([IdExpCOD],[IdTipoCodUNSPSC_old],[IdExpEntidad_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdExpCOD,deleted.IdTipoCodUNSPSC,deleted.IdExpEntidad,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_ExperienciaCodUNSPSCEntidad]
  ([IdExpCOD],[IdTipoCodUNSPSC_new],[IdExpEntidad_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdExpCOD,inserted.IdTipoCodUNSPSC,inserted.IdExpEntidad,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO