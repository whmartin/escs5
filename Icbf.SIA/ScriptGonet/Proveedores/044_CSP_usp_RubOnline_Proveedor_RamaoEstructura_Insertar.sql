USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]    Script Date: 06/14/2013 19:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que guarda un nuevo RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]
		@IdRamaEstructura INT OUTPUT, 	@CodigoRamaEstructura NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.RamaoEstructura(CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRamaEstructura, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdRamaEstructura = @@IDENTITY 		
END
GO