USE [SIA]
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Tabla EntidadProvOferente
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_EstadoDatosBasicos]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_Niveldegobierno]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_NivelOrganizacional]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TERCERO]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoCiiu]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TipoCiiu]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoSectorEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[EntidadProvOferente]    Script Date: 06/24/2013 21:00:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]') AND type in (N'U'))
DROP TABLE [Proveedor].[EntidadProvOferente]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Crea la entidad [Proveedor].[EntidadProvOferente]
-- =============================================
/****** Object:  Table [Proveedor].[EntidadProvOferente]    Script Date: 06/24/2013 21:00:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[EntidadProvOferente](
	[IdEntidad] [int] IDENTITY(1,1) NOT NULL,
	[TipoEntOfProv] [bit] NULL,
	[IdTercero] [int] NOT NULL,
	[IdTipoCiiuPrincipal] [int] NULL,
	[IdTipoCiiuSecundario] [int] NULL,
	[IdTipoSector] [int] NULL,
	[IdTipoClaseEntidad] [int] NULL,
	[IdTipoRamaPublica] [int] NULL,
	[IdTipoNivelGob] [int] NULL,
	[IdTipoNivelOrganizacional] [int] NULL,
	[IdTipoCertificadorCalidad] [int] NULL,
	[FechaCiiuPrincipal] [datetime] NULL,
	[FechaCiiuSecundario] [datetime] NULL,
	[FechaConstitucion] [datetime] NULL,
	[FechaVigencia] [datetime] NULL,
	[FechaMatriculaMerc] [datetime] NULL,
	[FechaExpiracion] [datetime] NULL,
	[TipoVigencia] [bit] NULL,
	[ExenMatriculaMer] [bit] NULL,
	[MatriculaMercantil] [nvarchar](20) NULL,
	[ObserValidador] [nvarchar](256) NULL,
	[AnoRegistro] [int] NULL,
	[IdEstado] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EntidadProvOferente] PRIMARY KEY CLUSTERED 
(
	[IdEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos] FOREIGN KEY([IdEstado])
REFERENCES [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos])
GO

ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos]
GO

ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno] FOREIGN KEY([IdTipoNivelGob])
REFERENCES [Proveedor].[Niveldegobierno] ([IdNiveldegobierno])
GO

ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO

ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional] FOREIGN KEY([IdTipoNivelOrganizacional])
REFERENCES [Proveedor].[NivelOrganizacional] ([IdNivelOrganizacional])
GO

ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional]
GO

ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TERCERO] FOREIGN KEY([IdTercero])
REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
GO

ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TERCERO]
GO

ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TipoCiiu] FOREIGN KEY([IdTipoCiiuPrincipal])
REFERENCES [Proveedor].[TipoCiiu] ([IdTipoCiiu])
GO

ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TipoCiiu]
GO

ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad] FOREIGN KEY([IdTipoSector])
REFERENCES [Proveedor].[TipoSectorEntidad] ([IdTipoSectorEntidad])
GO

ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad]
GO


