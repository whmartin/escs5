USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Creacion de indice unico para Sucursales
-- =============================================
/****** Object:  Index [NombreUnico]    Script Date: 06/24/2013 12:27:40 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Sucursal]') AND name = N'NombreUnico')
DROP INDEX [NombreUnico] ON [Proveedor].[Sucursal] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO

/****** Object:  Index [NombreUnico]    Script Date: 06/24/2013 12:27:43 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[Sucursal] 
(
	[IdEntidad] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


