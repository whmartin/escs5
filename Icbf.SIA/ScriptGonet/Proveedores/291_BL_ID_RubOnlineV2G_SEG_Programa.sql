USE [SIA] 
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Se asegura de que exista un permiso en la entidad [SEG].[Permiso]
-- =============================================

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionProveedores')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'PROVEEDORES')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionProveedores'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
			   ([IdPrograma]
			   ,[IdRol]
			   ,[Insertar]
			   ,[Modificar]
			   ,[Eliminar]
			   ,[Consultar]
			   ,[UsuarioCreacion]
			   ,[FechaCreacion])           
		 VALUES
			   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionProveedores')
			   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'PROVEEDORES')
			   ,1
			   ,1
			   ,1
			   ,1
			   ,'Administrador'
			   ,GETDATE()) 
	END
END      
GO

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionTercero')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'PROVEEDORES')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionTercero'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
				   ([IdPrograma]
				   ,[IdRol]
				   ,[Insertar]
				   ,[Modificar]
				   ,[Eliminar]
				   ,[Consultar]
				   ,[UsuarioCreacion]
				   ,[FechaCreacion])           
			 VALUES
				   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionTercero')
				   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'PROVEEDORES')
				   ,1
				   ,1
				   ,1
				   ,1
				   ,'Administrador'
				   ,GETDATE())  
	END
END
GO




