USE [SIA] 
GO 
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Registra variso parametros en la entidad [SEG].[Parametro]
-- =============================================

IF NOT EXISTS (SELECT 1 FROM [SEG].[Parametro] WHERE [NombreParametro] = 'PASO1 - Ingreso al sistema')
BEGIN

	INSERT INTO [SEG].[Parametro]
			   ([NombreParametro]
			   ,[ValorParametro]
			   ,[ImagenParametro]
			   ,[Estado]
			   ,[Funcionalidad]
			   ,[UsuarioCrea]
			   ,[FechaCrea])			  
		 VALUES
			   ('PASO1 - Ingreso al sistema'
			   ,'1.- Ingresa a la p�gina del Icbf '
			   ,NULL
			   ,1
			   ,'1'
			   ,'Administrador'
			   ,GETDATE())
END
GO


IF NOT EXISTS (SELECT 1 FROM [SEG].[Parametro] WHERE [NombreParametro] = 'PASO2 - Ingreso al sistema')
BEGIN

	INSERT INTO [SEG].[Parametro]
			   ([NombreParametro]
			   ,[ValorParametro]
			   ,[ImagenParametro]
			   ,[Estado]
			   ,[Funcionalidad]
			   ,[UsuarioCrea]
			   ,[FechaCrea])			  
		 VALUES
			   ('PASO2 - Ingreso al sistema'
			   ,'1.- Ingresa a la p�gina del Icbf '
			   ,NULL
			   ,1
			   ,'1'
			   ,'Administrador'
			   ,GETDATE())
END
GO


IF NOT EXISTS (SELECT 1 FROM [SEG].[Parametro] WHERE [NombreParametro] = 'PASO3 - Ingreso al sistema')
BEGIN

	INSERT INTO [SEG].[Parametro]
			   ([NombreParametro]
			   ,[ValorParametro]
			   ,[ImagenParametro]
			   ,[Estado]
			   ,[Funcionalidad]
			   ,[UsuarioCrea]
			   ,[FechaCrea])			  
		 VALUES
			   ('PASO3 - Ingreso al sistema'
			   ,'1.- Ingresa a la p�gina del Icbf '
			   ,NULL
			   ,1
			   ,'1'
			   ,'Administrador'
			   ,GETDATE())
END
GO


IF NOT EXISTS (SELECT 1 FROM [SEG].[Parametro] WHERE [NombreParametro] = 'PASO4 - Ingreso al sistema')
BEGIN

	INSERT INTO [SEG].[Parametro]
			   ([NombreParametro]
			   ,[ValorParametro]
			   ,[ImagenParametro]
			   ,[Estado]
			   ,[Funcionalidad]
			   ,[UsuarioCrea]
			   ,[FechaCrea])			  
		 VALUES
			   ('PASO4 - Ingreso al sistema'
			   ,'1.- Ingresa a la p�gina del Icbf '
			   ,NULL
			   ,1
			   ,'1'
			   ,'Administrador'
			   ,GETDATE())
END
GO

IF NOT EXISTS (SELECT 1 FROM [SEG].[Parametro] WHERE [NombreParametro] = 'TEXTO - Mensaje de bienvenida')
BEGIN

	INSERT INTO [SEG].[Parametro]
			   ([NombreParametro]
			   ,[ValorParametro]
			   ,[ImagenParametro]
			   ,[Estado]
			   ,[Funcionalidad]
			   ,[UsuarioCrea]
			   ,[FechaCrea])			  
		 VALUES
			   ('TEXTO - Mensaje de bienvenida'
			   ,'1.- Ingresa a la p�gina del Icbf '
			   ,NULL
			   ,1
			   ,'1'
			   ,'Administrador'
			   ,GETDATE())
END
GO