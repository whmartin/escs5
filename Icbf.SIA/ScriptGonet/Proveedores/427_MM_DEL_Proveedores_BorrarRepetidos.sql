use [SIA]
GO
delete from Proveedor.DocDatosBasicoProv
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)
					
delete from Proveedor.InfoAdminEntidad
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)

delete Proveedor.ValidarInfoExperienciaEntidad
from Proveedor.ValidarInfoExperienciaEntidad inner join Proveedor.InfoExperienciaEntidad on ValidarInfoExperienciaEntidad.IdExpEntidad = Proveedor.InfoExperienciaEntidad.IdExpEntidad
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)

delete Proveedor.DocExperienciaEntidad
from  Proveedor.InfoExperienciaEntidad inner join Proveedor.DocExperienciaEntidad on DocExperienciaEntidad.IdExpEntidad = Proveedor.InfoExperienciaEntidad.IdExpEntidad
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)
					
delete from Proveedor.InfoExperienciaEntidad
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)

delete Proveedor.ValidarInfoFinancieraEntidad
from Proveedor.ValidarInfoFinancieraEntidad inner join Proveedor.InfoFinancieraEntidad on ValidarInfoFinancieraEntidad.IdInfoFin = Proveedor.InfoFinancieraEntidad.IdInfoFin
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)

delete Proveedor.DocFinancieraProv 
from  Proveedor.InfoFinancieraEntidad inner join Proveedor.DocFinancieraProv on DocFinancieraProv.IdInfoFin = Proveedor.InfoFinancieraEntidad.IdInfoFin
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)

delete from  Proveedor.InfoFinancieraEntidad 
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)

delete from Proveedor.NotificacionJudicial
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)


delete from  Proveedor.ValidarInfoDatosBasicosEntidad
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)

delete from Proveedor.Sucursal
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)

delete from  Proveedor.EntidadProvOferente 
where IdEntidad in (select Max(IdEntidad) from Proveedor.EntidadProvOferente 
					where IdTercero in (select IdTercero from Proveedor.EntidadProvOferente group by IdTercero having count(IdTercero)>1 )
					group by IdTercero)
