USE [Proveedores]
GO

/*Se adiciona obligatoriedad para Sector Publico o Privado*/

ALTER TABLE [Proveedor].[TipoDocumentoPrograma] ADD [ObligSectorPrivado] [int] NULL
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] ADD [ObligSectorPublico] [int] NULL
