USE [SIA]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]    Script Date: 06/14/2013 19:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[NivelOrganizacional] 
 WHERE Estado =1
END
GO