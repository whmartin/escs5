USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]    Script Date: 07/28/2013 16:09:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]
GO
