USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en [Proveedor].[NivelOrganizacional]
-- =============================================
SET IDENTITY_INSERT [Proveedor].[NivelOrganizacional] ON
INSERT [Proveedor].[NivelOrganizacional] ([IdNivelOrganizacional], [CodigoNivelOrganizacional], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'CENTRALIZADA', 1, N'Administrador', CAST(0x0000A1DB01058D20 AS DateTime), N'Administrador', CAST(0x0000A1DD01341909 AS DateTime))
INSERT [Proveedor].[NivelOrganizacional] ([IdNivelOrganizacional], [CodigoNivelOrganizacional], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'DESCENTRALIZADA', 1, N'Administrador', CAST(0x0000A1DB01059EE0 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[NivelOrganizacional] OFF