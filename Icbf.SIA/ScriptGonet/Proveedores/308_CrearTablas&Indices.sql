USE [SIA]
GO

-- =================================================
-- Author:          Gonet\Bayron Lara
-- Create date:     16/07/2013 04:56:29 p.m.
-- Description:     Crear tablas e indices
-- =================================================

/****** Object:  Table [Proveedor].[TablaParametrica]    Script Date: 07/16/2013 12:51:03 ******/
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TablaParametrica]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TablaParametrica](
	[IdTablaParametrica] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTablaParametrica] [nvarchar](128) NOT NULL,
	[NombreTablaParametrica] [nvarchar](128) NOT NULL,
	[Url] [nvarchar](256) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TablaParametrica] PRIMARY KEY CLUSTERED 
(
	[IdTablaParametrica] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TablaParametrica]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TablaParametrica] 
(
	[CodigoTablaParametrica] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipoSectorEntidad]    Script Date: 07/16/2013 12:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoSectorEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoSectorEntidad](
	[IdTipoSectorEntidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoSectorEntidad] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoSectorEntidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoSectorEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoSectorEntidad]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoSectorEntidad] 
(
	[CodigoSectorEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoSectorEntidad]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoSectorEntidad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipoRegimenTributario]    Script Date: 07/16/2013 12:52:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoRegimenTributario]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoRegimenTributario](
	[IdTipoRegimenTributario] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRegimenTributario] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[IdTipoPersona] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoRegimenTributario] PRIMARY KEY CLUSTERED 
(
	[IdTipoRegimenTributario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoRegimenTributario]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoRegimenTributario] 
(
	[IdTipoPersona] ASC,
	[CodigoRegimenTributario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoRegimenTributario]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoRegimenTributario] 
(
	[IdTipoPersona] ASC,
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[Tipoentidad]    Script Date: 07/16/2013 12:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Tipoentidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[Tipoentidad](
	[IdTipoentidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoentidad] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Tipoentidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoentidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Tipoentidad]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[Tipoentidad] 
(
	[CodigoTipoentidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Tipoentidad]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[Tipoentidad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[RamaoEstructura]    Script Date: 07/16/2013 12:50:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[RamaoEstructura](
	[IdRamaEstructura] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRamaEstructura] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RamaoEstructura] PRIMARY KEY CLUSTERED 
(
	[IdRamaEstructura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[RamaoEstructura] 
(
	[CodigoRamaEstructura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[RamaoEstructura] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipoDocumento]    Script Date: 07/16/2013 12:52:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumento]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoDocumento](
	[IdTipoDocumento] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoDocumento] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoDocumento] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumento]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoDocumento] 
(
	[CodigoTipoDocumento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumento]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoDocumento] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipoDocIdentifica]    Script Date: 07/16/2013 12:52:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocIdentifica]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoDocIdentifica](
	[IdTipoDocIdentifica] [int] IDENTITY(1,1) NOT NULL,
	[CodigoDocIdentifica] [numeric](18, 0) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoDocIdentifica] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocIdentifica] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocIdentifica]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoDocIdentifica] 
(
	[CodigoDocIdentifica] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocIdentifica]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoDocIdentifica] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipodeentidadPublica]    Script Date: 07/16/2013 12:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeentidadPublica]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipodeentidadPublica](
	[IdTipodeentidadPublica] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipodeentidadPublica] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipodeentidadPublica] PRIMARY KEY CLUSTERED 
(
	[IdTipodeentidadPublica] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeentidadPublica]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipodeentidadPublica] 
(
	[CodigoTipodeentidadPublica] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeentidadPublica]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipodeentidadPublica] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipodeActividad]    Script Date: 07/16/2013 12:51:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipodeActividad](
	[IdTipodeActividad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipodeActividad] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipodeActividad] PRIMARY KEY CLUSTERED 
(
	[IdTipodeActividad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipodeActividad] 
(
	[CodigoTipodeActividad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipodeActividad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipoCodigoUNSPSC]    Script Date: 07/16/2013 12:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCodigoUNSPSC]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoCodigoUNSPSC](
	[IdTipoCodUNSPSC] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](64) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoCodigoUNSPSC] PRIMARY KEY CLUSTERED 
(
	[IdTipoCodUNSPSC] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCodigoUNSPSC]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoCodigoUNSPSC] 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCodigoUNSPSC]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoCodigoUNSPSC] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipoCiiu]    Script Date: 07/16/2013 12:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCiiu]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoCiiu](
	[IdTipoCiiu] [int] IDENTITY(1,1) NOT NULL,
	[CodigoCiiu] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoCiiu] PRIMARY KEY CLUSTERED 
(
	[IdTipoCiiu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCiiu]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoCiiu] 
(
	[CodigoCiiu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCiiu]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoCiiu] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipoCargoEntidad]    Script Date: 07/16/2013 12:51:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCargoEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoCargoEntidad](
	[IdTipoCargoEntidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoCargoEntidad] [nvarchar](10) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoCargoEntidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoCargoEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCargoEntidad]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoCargoEntidad] 
(
	[CodigoTipoCargoEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCargoEntidad]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoCargoEntidad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[EstadoValidacionDocumental]    Script Date: 07/16/2013 12:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoValidacionDocumental]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[EstadoValidacionDocumental](
	[IdEstadoValidacionDocumental] [int] IDENTITY(1,1) NOT NULL,
	[CodigoEstadoValidacionDocumental] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoValidacionDocumental] PRIMARY KEY CLUSTERED 
(
	[IdEstadoValidacionDocumental] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoValidacionDocumental]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[EstadoValidacionDocumental] 
(
	[CodigoEstadoValidacionDocumental] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoValidacionDocumental]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[EstadoValidacionDocumental] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[EstadoDatosBasicos]    Script Date: 07/16/2013 12:49:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoDatosBasicos]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[EstadoDatosBasicos](
	[IdEstadoDatosBasicos] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoDatosBasicos] PRIMARY KEY CLUSTERED 
(
	[IdEstadoDatosBasicos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoDatosBasicos]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[EstadoDatosBasicos] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[Acuerdos]    Script Date: 07/16/2013 12:47:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Acuerdos]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[Acuerdos](
	[IdAcuerdo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](64) NOT NULL,
	[ContenidoHtml] [nvarchar](max) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Acuerdos] PRIMARY KEY CLUSTERED 
(
	[IdAcuerdo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[ContactoEntidad]    Script Date: 07/16/2013 12:47:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ContactoEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ContactoEntidad](
	[IdContacto] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdSucursal] [int] NOT NULL,
	[IdTelContacto] [int] NOT NULL,
	[IdTipoDocIdentifica] [int] NOT NULL,
	[IdTipoCargoEntidad] [int] NOT NULL,
	[NumeroIdentificacion] [numeric](30, 0) NOT NULL,
	[PrimerNombre] [nvarchar](128) NOT NULL,
	[SegundoNombre] [nvarchar](128) NOT NULL,
	[PrimerApellido] [nvarchar](128) NOT NULL,
	[SegundoApellido] [nvarchar](128) NOT NULL,
	[Dependencia] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ContactoEntidad] PRIMARY KEY CLUSTERED 
(
	[IdContacto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[ContactoEntidad]') AND name = N'EmailUnico')
CREATE UNIQUE NONCLUSTERED INDEX [EmailUnico] ON [Proveedor].[ContactoEntidad] 
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[ParametrosEmail]    Script Date: 07/16/2013 12:50:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ParametrosEmail]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ParametrosEmail](
	[IdParametroID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoParametroEmail] [nvarchar](100) NOT NULL,
	[DescripcionParametrosEmail] [nvarchar](255) NULL,
	[ValorMesesParametroEmail] [int] NULL,
 CONSTRAINT [UNIC_ParametrosEmail] UNIQUE NONCLUSTERED 
(
	[CodigoParametroEmail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[NivelOrganizacional]    Script Date: 07/16/2013 12:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[NivelOrganizacional]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[NivelOrganizacional](
	[IdNivelOrganizacional] [int] IDENTITY(1,1) NOT NULL,
	[CodigoNivelOrganizacional] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_NivelOrganizacional] PRIMARY KEY CLUSTERED 
(
	[IdNivelOrganizacional] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[NivelOrganizacional]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[NivelOrganizacional] 
(
	[CodigoNivelOrganizacional] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[NivelOrganizacional]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[NivelOrganizacional] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[Niveldegobierno]    Script Date: 07/16/2013 12:50:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[Niveldegobierno](
	[IdNiveldegobierno] [int] IDENTITY(1,1) NOT NULL,
	[CodigoNiveldegobierno] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Niveldegobierno] PRIMARY KEY CLUSTERED 
(
	[IdNiveldegobierno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[Niveldegobierno] 
(
	[CodigoNiveldegobierno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[Niveldegobierno] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[InfoExperienciaEntidad]    Script Date: 07/16/2013 12:49:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[InfoExperienciaEntidad](
	[IdExpEntidad] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NULL,
	[IdTipoSector] [int] NULL,
	[IdTipoEstadoExp] [int] NULL,
	[IdTipoModalidadExp] [int] NULL,
	[IdTipoModalidad] [int] NULL,
	[IdTipoPoblacionAtendida] [int] NULL,
	[IdTipoRangoExpAcum] [int] NULL,
	[IdTipoCodUNSPSC] [int] NOT NULL,
	[IdTipoEntidadContratante] [int] NULL,
	[EntidadContratante] [nvarchar](128) NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NOT NULL,
	[NumeroContrato] [nvarchar](128) NULL,
	[ObjetoContrato] [nvarchar](256) NOT NULL,
	[Vigente] [bit] NULL,
	[Cuantia] [numeric](18, 3) NOT NULL,
	[ExperienciaMeses] [numeric](18, 3) NULL,
	[EstadoDocumental] [int] NULL,
	[UnionTempConsorcio] [bit] NULL,
	[PorcentParticipacion] [numeric](5, 0) NULL,
	[AtencionDeptos] [bit] NULL,
	[JardinOPreJardin] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InfoExperienciaEntidad] PRIMARY KEY CLUSTERED 
(
	[IdExpEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[ClasedeEntidad]    Script Date: 07/16/2013 12:47:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SET ANSI_PADDING ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ClasedeEntidad](
	[IdClasedeEntidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoClasedeEntidad] [varchar](10) NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[IdTipodeActividad] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ClasedeEntidad] PRIMARY KEY CLUSTERED 
(
	[IdClasedeEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
--SET ANSI_PADDING OFF
--GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[ClasedeEntidad] 
(
	[IdTipodeActividad] ASC,
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[TipoDocumentoPrograma]    Script Date: 07/16/2013 12:52:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SET ANSI_PADDING ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoDocumentoPrograma](
	[IdTipoDocumentoPrograma] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoDocumento] [int] NOT NULL,
	[IdPrograma] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[MaxPermitidoKB] [int] NULL,
	[ExtensionesPermitidas] [varchar](50) NULL,
	[ObligRupNoRenovado] [int] NULL,
	[ObligRupRenovado] [int] NULL,
	[ObligPersonaJuridica] [int] NULL,
	[ObligPersonaNatural] [int] NULL,
 CONSTRAINT [PK_TipoDocumentoPrograma] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumentoPrograma] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
--SET ANSI_PADDING OFF
--GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]') AND name = N'TipoDocumentoProgramaUnico')
CREATE UNIQUE NONCLUSTERED INDEX [TipoDocumentoProgramaUnico] ON [Proveedor].[TipoDocumentoPrograma] 
(
	[IdTipoDocumento] ASC,
	[IdPrograma] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[ExperienciaCodUNSPSCEntidad]    Script Date: 07/16/2013 12:49:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad](
	[IdExpCOD] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoCodUNSPSC] [int] NOT NULL,
	[IdExpEntidad] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ExperienciaCodUNSPSCEntidad] PRIMARY KEY CLUSTERED 
(
	[IdExpCOD] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[DocAdjuntoTercero]    Script Date: 07/16/2013 12:47:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SET ANSI_PADDING ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[DocAdjuntoTercero](
	[IDDOCADJUNTO] [int] IDENTITY(1,1) NOT NULL,
	[IDTERCERO] [int] NULL,
	[IDDOCUMENTO] [int] NULL,
	[DESCRIPCION] [nvarchar](128) NOT NULL,
	[LINKDOCUMENTO] [nvarchar](256) NOT NULL,
	[ANNO] [numeric](4, 0) NULL,
	[FECHACREA] [datetime] NOT NULL,
	[USUARIOCREA] [nvarchar](128) NOT NULL,
	[FECHAMODIFICA] [datetime] NULL,
	[USUARIOMODIFICA] [nvarchar](128) NULL,
	[IdTemporal] [varchar](20) NULL,
 CONSTRAINT [PK_DOCADJUNTOTERCERO] PRIMARY KEY CLUSTERED 
(
	[IDDOCADJUNTO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
--SET ANSI_PADDING OFF
--GO
/****** Object:  Table [Proveedor].[ValidarTercero]    Script Date: 07/16/2013 12:53:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarTercero]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ValidarTercero](
	[IdValidarTercero] [int] IDENTITY(1,1) NOT NULL,
	[IdTercero] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,
	[Observaciones] [nvarchar](100) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_IdValidarTercero] PRIMARY KEY CLUSTERED 
(
	[IdValidarTercero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[ValidarInfoExperienciaEntidad]    Script Date: 07/16/2013 12:53:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoExperienciaEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ValidarInfoExperienciaEntidad](
	[IdValidarInfoExperienciaEntidad] [int] IDENTITY(1,1) NOT NULL,
	[NroRevision] [int] NOT NULL,
	[IdExpEntidad] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,
	[Observaciones] [nvarchar](100) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_IdValidarInfoExperienciaEntidad] PRIMARY KEY CLUSTERED 
(
	[IdValidarInfoExperienciaEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[EntidadProvOferente]    Script Date: 07/16/2013 12:49:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[EntidadProvOferente](
	[IdEntidad] [int] IDENTITY(1,1) NOT NULL,
	[ConsecutivoInterno] [nvarchar](128) NULL,
	[TipoEntOfProv] [bit] NULL,
	[IdTercero] [int] NOT NULL,
	[IdTipoCiiuPrincipal] [int] NULL,
	[IdTipoCiiuSecundario] [int] NULL,
	[IdTipoSector] [int] NULL,
	[IdTipoClaseEntidad] [int] NULL,
	[IdTipoRamaPublica] [int] NULL,
	[IdTipoNivelGob] [int] NULL,
	[IdTipoNivelOrganizacional] [int] NULL,
	[IdTipoCertificadorCalidad] [int] NULL,
	[FechaCiiuPrincipal] [datetime] NULL,
	[FechaCiiuSecundario] [datetime] NULL,
	[FechaConstitucion] [datetime] NULL,
	[FechaVigencia] [datetime] NULL,
	[FechaMatriculaMerc] [datetime] NULL,
	[FechaExpiracion] [datetime] NULL,
	[TipoVigencia] [bit] NULL,
	[ExenMatriculaMer] [bit] NULL,
	[MatriculaMercantil] [nvarchar](20) NULL,
	[ObserValidador] [nvarchar](256) NULL,
	[AnoRegistro] [int] NULL,
	[IdEstado] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdAcuerdo] [int] NULL,
	[NroRevision] [int] NULL,
 CONSTRAINT [PK_EntidadProvOferente] PRIMARY KEY CLUSTERED 
(
	[IdEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[DocExperienciaEntidad]    Script Date: 07/16/2013 12:48:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SET ANSI_PADDING ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[DocExperienciaEntidad](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdExpEntidad] [int] NULL,
	[IdTipoDocumento] [int] NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[LinkDocumento] [nvarchar](256) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[NombreDocumento] [nvarchar](128) NULL,
	[IdTemporal] [varchar](20) NULL,
 CONSTRAINT [PK_DocExperienciaEntidad] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
--SET ANSI_PADDING OFF
--GO
/****** Object:  Table [Proveedor].[MotivoCambioEstado]    Script Date: 07/16/2013 12:50:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[MotivoCambioEstado]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[MotivoCambioEstado](
	[IdTercero] [int] NULL,
	[IdTemporal] [nvarchar](20) NULL,
	[Motivo] [nvarchar](250) NULL,
	[DatosBasicos] [bit] NULL,
	[Financiera] [bit] NULL,
	[Experiencia] [bit] NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioCrea] [nvarchar](128) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[NotificacionJudicial]    Script Date: 07/16/2013 12:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[NotificacionJudicial](
	[IdNotJudicial] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NULL,
	[IdDepartamento] [int] NULL,
	[IdMunicipio] [int] NULL,
	[IdZona] [int] NULL,
	[Direccion] [nvarchar](250) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_NotificacionJudicial] PRIMARY KEY CLUSTERED 
(
	[IdNotJudicial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[InfoFinancieraEntidad]    Script Date: 07/16/2013 12:50:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[InfoFinancieraEntidad](
	[IdInfoFin] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[ActivoCte] [numeric](18, 3) NOT NULL,
	[ActivoTotal] [numeric](18, 3) NOT NULL,
	[PasivoCte] [numeric](18, 3) NOT NULL,
	[PasivoTotal] [numeric](18, 3) NOT NULL,
	[Patrimonio] [numeric](18, 3) NOT NULL,
	[GastosInteresFinancieros] [numeric](18, 3) NOT NULL,
	[UtilidadOperacional] [numeric](18, 3) NOT NULL,
	[ConfirmaIndicadoresFinancieros] [bit] NOT NULL,
	[RupRenovado] [bit] NOT NULL,
	[EstadoValidacion] [int] NOT NULL,
	[ObservacionesInformacionFinanciera] [nvarchar](256) NULL,
	[ObservacionesValidadorICBF] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InfoFinancieraEntidad] PRIMARY KEY CLUSTERED 
(
	[IdInfoFin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[InfoAdminEntidad]    Script Date: 07/16/2013 12:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[InfoAdminEntidad](
	[IdInfoAdmin] [int] IDENTITY(1,1) NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdTipoRegTrib] [int] NULL,
	[IdTipoOrigenCapital] [int] NULL,
	[IdTipoActividad] [int] NULL,
	[IdTipoEntidad] [int] NULL,
	[IdTipoNaturalezaJurid] [int] NULL,
	[IdTipoRangosTrabajadores] [int] NULL,
	[IdTipoRangosActivos] [int] NULL,
	[IdRepLegal] [int] NULL,
	[IdTipoCertificaTamano] [int] NULL,
	[IdTipoEntidadPublica] [int] NULL,
	[IdDepartamentoConstituida] [int] NULL,
	[IdMunicipioConstituida] [int] NULL,
	[IdDepartamentoDirComercial] [int] NULL,
	[IdMunicipioDirComercial] [int] NULL,
	[DireccionComercial] [nvarchar](50) NULL,
	[IdZona] [nvarchar](128) NULL,
	[NombreComercial] [nvarchar](128) NULL,
	[NombreEstablecimiento] [nvarchar](128) NULL,
	[Sigla] [nvarchar](50) NULL,
	[PorctjPrivado] [int] NULL,
	[PorctjPublico] [int] NULL,
	[SitioWeb] [nvarchar](128) NULL,
	[NombreEntidadAcreditadora] [nvarchar](1) NULL,
	[Organigrama] [bit] NULL,
	[TotalPnalAnnoPrevio] [numeric](10, 0) NULL,
	[VincLaboral] [numeric](10, 0) NULL,
	[PrestServicios] [numeric](10, 0) NULL,
	[Voluntariado] [numeric](10, 0) NULL,
	[VoluntPermanente] [numeric](10, 0) NULL,
	[Asociados] [numeric](10, 0) NULL,
	[Mision] [numeric](10, 0) NULL,
	[PQRS] [bit] NULL,
	[GestionDocumental] [bit] NULL,
	[AuditoriaInterna] [bit] NULL,
	[ManProcedimiento] [bit] NULL,
	[ManPracticasAmbiente] [bit] NULL,
	[ManComportOrg] [bit] NULL,
	[ManFunciones] [bit] NULL,
	[ProcRegInfoContable] [bit] NULL,
	[PartMesasTerritoriales] [bit] NULL,
	[PartAsocAgremia] [bit] NULL,
	[PartConsejosComun] [bit] NULL,
	[ConvInterInst] [bit] NULL,
	[ProcSeleccGral] [bit] NULL,
	[ProcSeleccEtnico] [bit] NULL,
	[PlanInduccCapac] [bit] NULL,
	[EvalDesemp] [bit] NULL,
	[PlanCualificacion] [bit] NULL,
	[NumSedes] [int] NULL,
	[SedesPropias] [bit] NULL,
	[IdEstado] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InfoAdminEntidad] PRIMARY KEY CLUSTERED 
(
	[IdInfoAdmin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[DocDatosBasicoProv]    Script Date: 07/16/2013 12:47:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SET ANSI_PADDING ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[DocDatosBasicoProv](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NULL,
	[NombreDocumento] [nvarchar](128) NOT NULL,
	[LinkDocumento] [nvarchar](256) NOT NULL,
	[Observaciones] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdTipoDocumento] [int] NULL,
	[IdTemporal] [varchar](20) NULL,
 CONSTRAINT [PK_DocDatosBasicoProv] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
--SET ANSI_PADDING OFF
--GO
/****** Object:  Table [Proveedor].[ValidarInfoDatosBasicosEntidad]    Script Date: 07/16/2013 12:53:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoDatosBasicosEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ValidarInfoDatosBasicosEntidad](
	[IdValidarInfoDatosBasicosEntidad] [int] IDENTITY(1,1) NOT NULL,
	[NroRevision] [int] NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,
	[Observaciones] [nvarchar](100) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_IdValidarInfoDatosBasicosEntidad] PRIMARY KEY CLUSTERED 
(
	[IdValidarInfoDatosBasicosEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[Sucursal]    Script Date: 07/16/2013 12:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Sucursal]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[Sucursal](
	[IdSucursal] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[Indicativo] [int] NOT NULL,
	[Telefono] [int] NOT NULL,
	[Extension] [numeric](10, 0) NULL,
	[Celular] [numeric](10, 0) NOT NULL,
	[Correo] [nvarchar](256) NULL,
	[Estado] [int] NOT NULL,
	[Departamento] [int] NOT NULL,
	[Municipio] [int] NOT NULL,
	[IdZona] [int] NULL,
	[Direccion] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[Nombre] [nvarchar](255) NULL,
 CONSTRAINT [PK_IdSucursal] PRIMARY KEY CLUSTERED 
(
	[IdSucursal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Sucursal]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[Sucursal] 
(
	[IdEntidad] ASC,
	[Nombre] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [Proveedor].[ValidarInfoFinancieraEntidad]    Script Date: 07/16/2013 12:53:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoFinancieraEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ValidarInfoFinancieraEntidad](
	[IdValidarInfoFinancieraEntidad] [int] IDENTITY(1,1) NOT NULL,
	[NroRevision] [int] NOT NULL,
	[IdInfoFin] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,
	[Observaciones] [nvarchar](100) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_IdValidarInfoFinancieraEntidad] PRIMARY KEY CLUSTERED 
(
	[IdValidarInfoFinancieraEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[DocNotificacionJudicial]    Script Date: 07/16/2013 12:49:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[DocNotificacionJudicial](
	[IdDocNotJudicial] [int] IDENTITY(1,1) NOT NULL,
	[IdNotJudicial] [int] NULL,
	[IdDocumento] [int] NULL,
	[Descripcion] [nvarchar](128) NULL,
	[LinkDocumento] [nvarchar](256) NULL,
	[Anno] [numeric](4, 0) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_DocNotificacionJudicial] PRIMARY KEY CLUSTERED 
(
	[IdDocNotJudicial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Proveedor].[DocFinancieraProv]    Script Date: 07/16/2013 12:48:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SET ANSI_PADDING ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[DocFinancieraProv](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdInfoFin] [int] NULL,
	[NombreDocumento] [nvarchar](128) NOT NULL,
	[LinkDocumento] [nvarchar](256) NOT NULL,
	[Observaciones] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdTipoDocumento] [int] NULL,
	[IdTemporal] [varchar](20) NULL,
 CONSTRAINT [PK_DocFinancieraProv] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
