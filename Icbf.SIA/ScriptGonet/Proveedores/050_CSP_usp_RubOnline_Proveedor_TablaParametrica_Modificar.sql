USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde Sámano    
-- Create date:         17/01/2014
-- Description:     Modifica información de una tabla parametrica en Proveedor.TablaParametrica
-- =============================================

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]    Script Date: 06/14/2013 19:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]
		@IdTablaParametrica INT,	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TablaParametrica SET CodigoTablaParametrica = @CodigoTablaParametrica, NombreTablaParametrica = @NombreTablaParametrica, Url = @Url, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTablaParametrica = @IdTablaParametrica
END
GO