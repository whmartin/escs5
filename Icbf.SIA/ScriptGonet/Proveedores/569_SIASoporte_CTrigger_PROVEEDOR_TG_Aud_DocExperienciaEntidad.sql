/****** Object:  Trigger [PROVEEDOR].[TG_Aud_DocExperienciaEntidad]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para DocExperienciaEntidad
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_DocExperienciaEntidad]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_DocExperienciaEntidad]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_DocExperienciaEntidad]
  ON  [PROVEEDOR].[DocExperienciaEntidad]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_DocExperienciaEntidad]
  ([IdDocAdjunto],[IdExpEntidad_old], [IdExpEntidad_new],[IdTipoDocumento_old], [IdTipoDocumento_new],[Descripcion_old], [Descripcion_new],[LinkDocumento_old], [LinkDocumento_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[NombreDocumento_old], [NombreDocumento_new],[IdTemporal_old], [IdTemporal_new],[Operacion])
SELECT deleted.IdDocAdjunto,deleted.IdExpEntidad,inserted.IdExpEntidad,deleted.IdTipoDocumento,inserted.IdTipoDocumento,deleted.Descripcion,inserted.Descripcion,deleted.LinkDocumento,inserted.LinkDocumento,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,deleted.NombreDocumento,inserted.NombreDocumento,deleted.IdTemporal,inserted.IdTemporal,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdDocAdjunto = deleted.IdDocAdjuntoEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_DocExperienciaEntidad]
  ([IdDocAdjunto],[IdExpEntidad_old],[IdTipoDocumento_old],[Descripcion_old],[LinkDocumento_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[NombreDocumento_old],[IdTemporal_old],[Operacion])
SELECT deleted.IdDocAdjunto,deleted.IdExpEntidad,deleted.IdTipoDocumento,deleted.Descripcion,deleted.LinkDocumento,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,deleted.NombreDocumento,deleted.IdTemporal,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_DocExperienciaEntidad]
  ([IdDocAdjunto],[IdExpEntidad_new],[IdTipoDocumento_new],[Descripcion_new],[LinkDocumento_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[NombreDocumento_new],[IdTemporal_new],[Operacion])
SELECT inserted.IdDocAdjunto,inserted.IdExpEntidad,inserted.IdTipoDocumento,inserted.Descripcion,inserted.LinkDocumento,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,inserted.NombreDocumento,inserted.IdTemporal,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO