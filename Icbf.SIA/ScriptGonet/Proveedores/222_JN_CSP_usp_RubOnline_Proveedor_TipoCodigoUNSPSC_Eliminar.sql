USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Eliminar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Eliminar]
	@IdTipoCodUNSPSC INT
AS
BEGIN
	DELETE Proveedor.TipoCodigoUNSPSC 
	WHERE IdTipoCodUNSPSC = @IdTipoCodUNSPSC
END
