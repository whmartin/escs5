USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]    Script Date: 06/24/2013 21:18:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]    Script Date: 06/24/2013 21:18:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]
	@IdEntidad INT = NULL
AS
BEGIN
 SELECT IdInfoAdmin, IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, IdTipoRangosTrabajadores, 
 IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, IdDepartamentoDirComercial, 
 IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, PorctjPublico, SitioWeb, 
 NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, Mision, PQRS, 
 GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, PartMesasTerritoriales, 
 PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, NumSedes, SedesPropias, 
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[InfoAdminEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END
END

GO


