/****** Object:  Table [Auditoria].[PROVEEDOR_ReglasEstadoProveedor]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para ReglasEstadoProveedor ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_ReglasEstadoProveedor]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_ReglasEstadoProveedor]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_ReglasEstadoProveedor]([IdValidacionEstado] [int]  NULL, [IdEstadoDatosBasicos_old] [int]  NULL,[IdEstadoDatosBasicos_new] [int]  NULL,[IdEstadoDatosFinancieros_old] [int]  NULL,[IdEstadoDatosFinancieros_new] [int]  NULL,[IdEstadoDatosExperiencia_old] [int]  NULL,[IdEstadoDatosExperiencia_new] [int]  NULL,[IdEstadoProveedor_old] [int]  NULL,[IdEstadoProveedor_new] [int]  NULL,[IdEstadoTercero_old] [int]  NULL,[IdEstadoTercero_new] [int]  NULL,[Estado_old] [bit]  NULL,[Estado_new] [bit]  NULL,[UsuarioCrea_old] [nvarchar] (100)  NULL,[UsuarioCrea_new] [nvarchar] (100)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (100)  NULL,[UsuarioModifica_new] [nvarchar] (100)  NULL,[Fechamodifica_old] [datetime]  NULL,[Fechamodifica_new] [datetime]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO