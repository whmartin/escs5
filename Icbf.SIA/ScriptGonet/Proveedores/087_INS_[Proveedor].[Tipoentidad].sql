USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en  [Proveedor].[Tipoentidad]
-- =============================================
SET IDENTITY_INSERT [Proveedor].[Tipoentidad] ON
INSERT [Proveedor].[Tipoentidad] ([IdTipoentidad], [CodigoTipoentidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'002', N'NACIONAL', 1, N'Administrador', CAST(0x0000A1DB00C4C2B6 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Tipoentidad] ([IdTipoentidad], [CodigoTipoentidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'001', N'EXTRANJERA', 1, N'Administrador', CAST(0x0000A1DB00C4D861 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Tipoentidad] ([IdTipoentidad], [CodigoTipoentidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'EE', N'EE', 1, N'Administrador', CAST(0x0000A1DD01352440 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[Tipoentidad] OFF