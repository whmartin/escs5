USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 07/31/2013 09:04:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 07/31/2013 09:04:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--Autor:Mauricio Martinez
--Fecha:2013/07/15 16:00
--Descripcion: Consulta para mostrar un resumen de los modulos validados
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
@IdEntidad INT 
AS
BEGIN

			select	0 as Orden, 
					e.IdEntidad, 
					MAX(ISNULL(e.NroRevision,1)) as NroRevision,
					'Datos B�sicos' as Componente,
					MIN(case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END) AS iConfirmaYAprueba
					, CAST(MIN(CAST(ISNULL(e.Finalizado,0) as int)) as bit) as Finalizado
					, CASE WHEN ISNULL(MAX(e.IdEstado),0) = 5 THEN 1 ELSE 0 END as Liberar
			from  Proveedor.EntidadProvOferente e
				left join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
			where 
					e.IdEntidad = @IdEntidad
					--and (v.ConfirmaYAprueba = 1 or 
					and e.NroRevision = ISNULL(v.NroRevision,e.NroRevision)
			group by e.IdEntidad
				
			union
			
			select	1 as Orden, 
					e.IdEntidad,  
					MAX(ISNULL(i.NroRevision,e.NroRevision)) as NroRevision,
					'Financiera' as Componente,
					MIN(case when i.IdInfoFin IS NULL THEN -2 ELSE case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END END) AS iConfirmaYAprueba
					, cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado
					, CASE WHEN ISNULL(MAX(i.EstadoValidacion),0) = 5 THEN 1 ELSE 0 END as Liberar
			from  Proveedor.EntidadProvOferente e
				left join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				left join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin					
			where 
					e.IdEntidad  = @IdEntidad
					and e.NroRevision = ISNULL(v.NroRevision,e.NroRevision)
					--and (v.ConfirmaYAprueba = 1 or e.NroRevision = ISNULL(i.NroRevision,e.NroRevision))
			group by e.IdEntidad
			union
			select	2 as Orden,
					e.IdEntidad,					
					MAX(ISNULL(i.NroRevision,e.NroRevision)) as NroRevision,
					'Experiencias' as Comoponente,
					MIN(case when i.IdExpEntidad IS NULL THEN -2 ELSE case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END END) AS iConfirmaYAprueba
					, cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado
					, CASE WHEN ISNULL(MAX(i.EstadoDocumental),0) = 5 THEN 1 ELSE 0 END as Liberar
			from  Proveedor.EntidadProvOferente e
				left join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				left join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
			where 
					e.IdEntidad  = @IdEntidad
					--and (v.ConfirmaYAprueba = 1 or v.NroRevision = ISNULL(i.NroRevision,e.NroRevision))					
					and i.NroRevision = ISNULL(v.NroRevision,e.NroRevision)
			group by e.IdEntidad
END









GO


