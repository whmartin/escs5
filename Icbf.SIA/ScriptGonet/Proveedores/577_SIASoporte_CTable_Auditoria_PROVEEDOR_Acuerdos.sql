/****** Object:  Table [Auditoria].[PROVEEDOR_Acuerdos]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para Acuerdos ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_Acuerdos]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_Acuerdos]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_Acuerdos]([IdAcuerdo] [int]  NULL, [Nombre_old] [nvarchar] (64)  NULL,[Nombre_new] [nvarchar] (64)  NULL,[ContenidoHtml_old] [nvarchar]  NULL,[ContenidoHtml_new] [nvarchar]  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO