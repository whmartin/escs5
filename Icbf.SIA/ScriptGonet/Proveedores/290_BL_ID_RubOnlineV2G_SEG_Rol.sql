USE [SIA] 
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Se asegura de que exista el rol 'PROVEEDORES' en la entidad [SEG].[Rol]
-- =============================================
IF NOT EXISTS (SELECT  1 FROM [SEG].[Rol] WHERE [Nombre] = 'PROVEEDORES')
BEGIN
	INSERT INTO [SEG].[Rol]
           ([providerKey]
           ,[Nombre]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion])           
     VALUES
           (''
           ,'PROVEEDORES'
           ,'Usuario externo del sistema de proveedores'
           ,1
           ,'Administrador'
           ,GETDATE())
          
END
GO


