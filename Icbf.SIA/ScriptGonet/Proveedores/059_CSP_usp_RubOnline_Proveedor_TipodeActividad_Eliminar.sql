
USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]    Script Date: 06/14/2013 19:32:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]
	@IdTipodeActividad INT
AS
BEGIN
	DELETE Proveedor.TipodeActividad WHERE IdTipodeActividad = @IdTipodeActividad
END
GO