USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]    Script Date: 07/22/2013 20:30:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]    Script Date: 07/22/2013 20:30:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:		 Carlos Felipe Cubillos
-- Description:	 Obtiene información de la BD Proveedores para usuarios
-- ==========================================================================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]

@pData Table_Proveedores_IntegracionUsuarios readonly

AS
BEGIN

	SELECT 
		datos.UserId,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=1 ) THEN SEG.Usuario.PrimerNombre
			ELSE null
			END AS PrimerNombre,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=1 ) THEN SEG.Usuario.SegundoNombre
			ELSE null
			END AS SegundoNombre,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=1 ) THEN SEG.Usuario.PrimerApellido
			ELSE null
			END AS PrimerApellido,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=1 ) THEN SEG.Usuario.SegundoApellido
			ELSE null
			END AS SegundoApellido,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=2 ) THEN SEG.Usuario.RazonSocial
			ELSE null
			END AS RazonSocial,
		CASE
			WHEN Oferente.TERCERO.IDTERCERO is null THEN 0
			ELSE 1
			END AS ExisteTercero,
		datos.IsApproved
	FROM @pData datos
	JOIN SEG.Usuario on datos.UserId = SEG.Usuario.providerKey
	LEFT JOIN Oferente.TERCERO on datos.UserId = Oferente.TERCERO.ProviderUserKey
	
END
GO


