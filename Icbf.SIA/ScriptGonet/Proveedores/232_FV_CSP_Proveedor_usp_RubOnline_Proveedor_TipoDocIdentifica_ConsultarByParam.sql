USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]    Script Date: 06/24/2013 12:41:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]    Script Date: 06/24/2013 12:41:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian Valencia
-- Create date: 22/06/2013
-- Description:	Consulta los tipos de documentos parametrizados y obtiene los formatos permitidos como el peso permitido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam] 
	@IdDocumento int,
	@Programa NVARCHAR(30) 
AS
BEGIN
	DECLARE @IdPrograma INT
	SET @IdPrograma = (SELECT DISTINCT IdPrograma  FROM SEG.Programa WHERE CodigoPrograma = @Programa)
	
	
	SELECT     Proveedor.TipoDocumentoPrograma.IdTipoDocumento,  
			   Proveedor.TipoDocumentoPrograma.MaxPermitidoKB, 
               Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas, 
               Proveedor.TipoDocumento.CodigoTipoDocumento, 
               Proveedor.TipoDocumento.Descripcion, 
               Proveedor.TipoDocumento.Estado, 
               Proveedor.TipoDocumento.UsuarioCrea, 
               Proveedor.TipoDocumento.FechaCrea
	FROM       Proveedor.TipoDocumentoPrograma 
			   INNER JOIN
			  Proveedor.TipoDocumento ON 
			  Proveedor.TipoDocumentoPrograma.IdTipoDocumento = Proveedor.TipoDocumento.IdTipoDocumento
	WHERE     (Proveedor.TipoDocumentoPrograma.IdTipoDocumento = @IdDocumento) AND (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma)
END

GO


