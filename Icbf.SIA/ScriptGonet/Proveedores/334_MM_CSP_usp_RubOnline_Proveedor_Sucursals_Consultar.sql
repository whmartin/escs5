USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]    Script Date: 07/22/2013 11:31:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]    Script Date: 07/22/2013 11:31:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]
	@IdEntidad INT = NULL,@Estado INT = NULL
AS
BEGIN
 SELECT IdSucursal, IdEntidad,Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado, IdZona, Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Sucursal] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO


