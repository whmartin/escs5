USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_ObtenerMailsGestorProveedores]    Script Date: 07/22/2013 17:47:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_ObtenerMailsGestorProveedores]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_ObtenerMailsGestorProveedores]
GO

USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_ObtenerMailsGestorProveedores]    Script Date: 07/22/2013 17:47:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RubOnline_ObtenerMailsGestorProveedores] 
	@ApplicationName varchar(50),
	@RoleName varchar(50)
AS
BEGIN
	
DECLARE @CORREOS varchar(max)
DECLARE @Email varchar(50)
DECLARE GProveedores CURSOR FOR

SELECT     aspnet_Membership.Email 
FROM         aspnet_UsersInRoles INNER JOIN
                      aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId AND aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId INNER JOIN
                      aspnet_Applications ON aspnet_Roles.ApplicationId = aspnet_Applications.ApplicationId AND 
                      aspnet_Roles.ApplicationId = aspnet_Applications.ApplicationId INNER JOIN
                      aspnet_Membership ON aspnet_Applications.ApplicationId = aspnet_Membership.ApplicationId AND 
                      aspnet_Applications.ApplicationId = aspnet_Membership.ApplicationId AND aspnet_UsersInRoles.UserId = aspnet_Membership.UserId
WHERE     (aspnet_Applications.ApplicationName = @ApplicationName) AND (aspnet_Roles.RoleName = @RoleName)

OPEN GProveedores
SET @CORREOS=''
FETCH GProveedores INTO  @Email
WHILE (@@FETCH_STATUS = 0 )
BEGIN
	PRINT @Email
	SET @CORREOS = @CORREOS + @Email + ';'
    FETCH GProveedores INTO @Email
END

CLOSE GProveedores

DEALLOCATE GProveedores

SELECT  @CORREOS AS correo
END

GO


