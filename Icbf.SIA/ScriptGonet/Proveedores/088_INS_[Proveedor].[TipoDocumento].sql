USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en  [Proveedor].[TipoDocumento]
-- =============================================
SET IDENTITY_INSERT [Proveedor].[TipoDocumento] ON
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'101', N'Certificado RUP', 1, N'administador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'001', N'Balance General', 1, N'administrador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'002', N'Estado de Resultados', 1, N'administrador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'003', N'Tarjeta profesional del contador', 1, N'administrador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoDocumento] ([IdTipoDocumento], [CodigoTipoDocumento], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (7, N'004', N'Certificado de la junta central de contadores', 1, N'administrador', CAST(0x0000A1D900000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[TipoDocumento] OFF