USE
[SIA]

GO
/****** Object:  Index [NombreUnico]    Script Date: 07/02/2013 10:35:23 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoFinancieraEntidad]') AND name = N'RevisionUnica')
DROP INDEX [RevisionUnica] ON [Proveedor].[ValidarInfoFinancieraEntidad] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO

CREATE UNIQUE NONCLUSTERED INDEX [RevisionUnica] ON [Proveedor].[ValidarInfoFinancieraEntidad]
(
	[IdInfoFin] ASC,
	[NroRevision] ASC	
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Index [NombreUnico]    Script Date: 07/02/2013 10:35:23 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoExperienciaEntidad]') AND name = N'RevisionUnica')
DROP INDEX [RevisionUnica] ON [Proveedor].[ValidarInfoExperienciaEntidad] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO

CREATE UNIQUE NONCLUSTERED INDEX [RevisionUnica] ON [Proveedor].[ValidarInfoExperienciaEntidad]
(
	[IdExpEntidad] ASC,
	[NroRevision] ASC	
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

USE [SIA]
GO
/****** Object:  Index [NombreUnico]    Script Date: 07/02/2013 10:35:23 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoDatosBasicosEntidad]') AND name = N'RevisionUnica')
DROP INDEX [RevisionUnica] ON [Proveedor].[ValidarInfoDatosBasicosEntidad] WITH ( ONLINE = OFF )
GO

CREATE UNIQUE NONCLUSTERED INDEX [RevisionUnica] ON [Proveedor].ValidarInfoDatosBasicosEntidad
(
	[IdEntidad] ASC,
	[NroRevision] ASC	
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
