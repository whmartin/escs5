USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina indice CodigoUnico de la entidad [Proveedor].[TipoRegimenTributario]
-- =============================================
/****** Object:  Index [CodigoUnico]    Script Date: 06/26/2013 23:39:37 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoRegimenTributario]') AND name = N'CodigoUnico')
DROP INDEX [CodigoUnico] ON [Proveedor].[TipoRegimenTributario] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Crea indice CodigoUnico en la entidad [Proveedor].[TipoRegimenTributario]
-- =============================================
/****** Object:  Index [CodigoUnico]    Script Date: 06/26/2013 23:39:39 ******/
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoRegimenTributario] 
(
	[IdTipoPersona] ASC,
	[CodigoRegimenTributario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


