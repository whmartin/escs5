USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]    Script Date: 06/14/2013 19:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]
	@IdTipoCiiu INT
AS
BEGIN
	DELETE Proveedor.TipoCiiu WHERE IdTipoCiiu = @IdTipoCiiu
END
GO