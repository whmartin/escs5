USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]
GO
-- =============================================
-- Author:		Jonnathan NI�o
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que elimina un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]
	@IdExpEntidad INT
AS
BEGIN
	DELETE Proveedor.InfoExperienciaEntidad WHERE IdExpEntidad = @IdExpEntidad
END
