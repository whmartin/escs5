USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocExperienciaEntidad]
GO
-- =============================================
-- Author:		Jonnathan NI�o
-- Create date:  6/22/2013 10:21:28 PM
-- =============================================
CREATE TABLE [Proveedor].[DocExperienciaEntidad](
 [IdDocAdjunto] [INT]  IDENTITY(1,1) NOT NULL,
 [IdExpEntidad] [INT]  NOT NULL,
 [IdTipoDocGrupo] [INT]  ,
 [Descripcion] [NVARCHAR] (128) NOT NULL,
 [LinkDocumento] [NVARCHAR] (256) NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_DocExperienciaEntidad] PRIMARY KEY CLUSTERED 
(
 	[IdDocAdjunto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
