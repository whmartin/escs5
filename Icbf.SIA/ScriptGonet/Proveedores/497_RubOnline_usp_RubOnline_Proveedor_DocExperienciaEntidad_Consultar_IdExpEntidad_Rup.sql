USE [SIA]
GO
-- =============================================
-- Author:		Carlos Cubillos
-- Description:	Se corrige procedimiento para que se filtren los documentos inactivos.
-- =============================================


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]    Script Date: 10/15/2013 16:46:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]    Script Date: 10/15/2013 16:46:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/13/2013 08:10:55 PM
-- Description:	Procedimiento almacenado que consulta una lista de DocExperienciaEntidad por IdInfoExperiencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup] 
( 
	@IdExpEntidad INT, 
	@RupRenovado VARCHAR(128) = NULL,
	@TipoPersona varchar(1),  
	@TipoSector varchar(1)
)
AS

DECLARE @IdPrograma INT;
SELECT @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/INFOEXPERIENCIAENTIDAD';



SELECT	MAX(IdDocAdjunto) AS IdDocAdjunto,
		MAX(IdExpEntidad) AS IdExpEntidad,
		NombreDocumento,
		Max(LinkDocumento) AS LinkDocumento,
		Max(Descripcion) AS Descripcion,
		Max(Obligatorio) AS Obligatorio,
		Max(UsuarioCrea) AS UsuarioCrea,
		Max(FechaCrea) AS FechaCrea,
		Max(UsuarioModifica) AS UsuarioModifica,
		Max(FechaModifica) AS FechaModifica,
		Max(IdTipoDocumento) AS IdTipoDocumento,
		Max(MaxPermitidoKB) AS MaxPermitidoKB,
		Max(ExtensionesPermitidas) AS ExtensionesPermitidas,
		Max(IdTemporal) AS IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdExpEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Descripcion,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--				WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocExperienciaEntidad AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION EXPERIENCIA
		AND IdExpEntidad = @IdExpEntidad
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdInfoFin,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--		WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION EXPERIENCIA
AND tdp.estado=1
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento




GO


