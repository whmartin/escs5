USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]    Script Date: 06/14/2013 19:32:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]
	@IdTipoRegimenTributario INT
AS
BEGIN
	DELETE Proveedor.TipoRegimenTributario WHERE IdTipoRegimenTributario = @IdTipoRegimenTributario
END
GO