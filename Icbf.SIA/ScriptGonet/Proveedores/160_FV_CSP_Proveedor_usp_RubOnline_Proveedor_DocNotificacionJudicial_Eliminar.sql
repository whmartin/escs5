USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]    Script Date: 06/24/2013 12:34:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]    Script Date: 06/24/2013 12:34:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]
	@IdDocNotJudicial INT
AS
BEGIN
	DELETE Proveedor.DocNotificacionJudicial 
	WHERE IdDocNotJudicial = @IdDocNotJudicial 
END

GO


