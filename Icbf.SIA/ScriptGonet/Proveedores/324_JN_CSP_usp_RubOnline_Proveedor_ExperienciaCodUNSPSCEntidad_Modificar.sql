USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  7/15/2013 11:30:19 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]
		@IdExpCOD INT,	@IdTipoCodUNSPSC INT,	@IdExpEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.ExperienciaCodUNSPSCEntidad SET IdTipoCodUNSPSC = @IdTipoCodUNSPSC, IdExpEntidad = @IdExpEntidad, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdExpCOD = @IdExpCOD
END
