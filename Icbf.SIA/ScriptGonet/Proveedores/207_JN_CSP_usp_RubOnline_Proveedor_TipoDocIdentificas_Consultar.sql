USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]
	@CodigoDocIdentifica NUMERIC = NULL,@Descripcion NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdTipoDocIdentifica, CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoDocIdentifica] 
 WHERE CodigoDocIdentifica = CASE WHEN @CodigoDocIdentifica IS NULL THEN CodigoDocIdentifica ELSE @CodigoDocIdentifica END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END
END
