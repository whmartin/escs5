USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]    Script Date: 06/14/2013 19:32:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]
		@IdTipoentidad INT OUTPUT, 	@CodigoTipoentidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Tipoentidad(CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoentidad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoentidad = @@IDENTITY 		
END
GO