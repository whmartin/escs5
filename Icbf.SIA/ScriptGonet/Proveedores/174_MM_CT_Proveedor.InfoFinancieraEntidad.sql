USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Creacion de tabla para almacenar Informacion Financiera de Proveedores
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EstadoValidacionDocumental]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_Vigencia]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] DROP CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[InfoFinancieraEntidad]    Script Date: 06/24/2013 12:14:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[InfoFinancieraEntidad]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[InfoFinancieraEntidad]    Script Date: 06/24/2013 12:14:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[InfoFinancieraEntidad](
	[IdInfoFin] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[ActivoCte] [numeric](18, 3) NOT NULL,
	[ActivoTotal] [numeric](18, 3) NOT NULL,
	[PasivoCte] [numeric](18, 3) NOT NULL,
	[PasivoTotal] [numeric](18, 3) NOT NULL,
	[Patrimonio] [numeric](18, 3) NOT NULL,
	[GastosInteresFinancieros] [numeric](18, 3) NOT NULL,
	[UtilidadOperacional] [numeric](18, 3) NOT NULL,
	[ConfirmaIndicadoresFinancieros] [bit] NOT NULL,
	[RupRenovado] [bit] NOT NULL,
	[EstadoValidacion] [int] NOT NULL,
	[ObservacionesInformacionFinanciera] [nvarchar](256) NULL,
	[ObservacionesValidadorICBF] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InfoFinancieraEntidad] PRIMARY KEY CLUSTERED 
(
	[IdInfoFin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO

ALTER TABLE [Proveedor].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente]
GO

ALTER TABLE [Proveedor].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental] FOREIGN KEY([EstadoValidacion])
REFERENCES [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental])
GO

ALTER TABLE [Proveedor].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental]
GO

ALTER TABLE [Proveedor].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia] FOREIGN KEY([IdVigencia])
REFERENCES [Global].[Vigencia] ([IdVigencia])
GO

ALTER TABLE [Proveedor].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia]
GO


