USE [SIA]
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- =============================================

/****** Object:  Table [Proveedor].[EstadoDatosBasicos]    Script Date: 06/24/2013 21:25:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoDatosBasicos]') AND type in (N'U'))
DROP TABLE [Proveedor].[EstadoDatosBasicos]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Crea la entidad [Proveedor].[EstadoDatosBasicos]
-- =============================================
/****** Object:  Table [Proveedor].[EstadoDatosBasicos]    Script Date: 06/24/2013 21:25:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[EstadoDatosBasicos](
	[IdEstadoDatosBasicos] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoDatosBasicos] PRIMARY KEY CLUSTERED 
(
	[IdEstadoDatosBasicos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


