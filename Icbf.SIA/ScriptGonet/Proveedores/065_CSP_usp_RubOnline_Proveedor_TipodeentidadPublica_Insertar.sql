USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]    Script Date: 06/14/2013 19:32:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]
		@IdTipodeentidadPublica INT OUTPUT, 	@CodigoTipodeentidadPublica NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipodeentidadPublica(CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipodeentidadPublica, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipodeentidadPublica = @@IDENTITY 		
END
GO