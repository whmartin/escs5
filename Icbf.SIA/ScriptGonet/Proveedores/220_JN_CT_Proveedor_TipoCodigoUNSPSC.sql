USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCodigoUNSPSC]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoCodigoUNSPSC]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/22/2013 1:07:57 AM
-- =============================================
CREATE TABLE [Proveedor].[TipoCodigoUNSPSC](
 [IdTipoCodUNSPSC] [INT]  IDENTITY(1,1) NOT NULL,
 [Codigo] [NVARCHAR] (64) NOT NULL,
 [Descripcion] [NVARCHAR] (128) NOT NULL,
 [Estado] [BIT]  NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_TipoCodigoUNSPSC] PRIMARY KEY CLUSTERED 
(
 	[IdTipoCodUNSPSC] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
