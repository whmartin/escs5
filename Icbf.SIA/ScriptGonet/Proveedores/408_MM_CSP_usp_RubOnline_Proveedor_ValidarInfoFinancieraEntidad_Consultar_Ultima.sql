USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]    Script Date: 07/29/2013 12:21:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]    Script Date: 07/29/2013 12:21:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoEntidadEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima] (@IdEntidad INT, @IdVigencia INT)
as
begin
	select top 1
		IdValidarInfoFinancieraEntidad,
		i.IdInfoFin,
		e.IdEntidad,
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and i.IdVigencia = @IdVigencia
		and i.NroRevision = v.NroRevision
	order by NroRevision DESC
end


GO


