USE [SIA]
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EsFundacion' AND Object_ID = Object_ID(N'Oferente.TERCERO'))

BEGIN

    ALTER TABLE Oferente.TERCERO ADD [EsFundacion] [bit] NULL

END
GO