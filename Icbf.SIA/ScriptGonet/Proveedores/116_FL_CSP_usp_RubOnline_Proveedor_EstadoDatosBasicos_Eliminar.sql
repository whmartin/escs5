USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]    Script Date: 06/24/2013 21:29:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]    Script Date: 06/24/2013 21:29:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]
	@IdEstadoDatosBasicos INT
AS
BEGIN
	DELETE Proveedor.EstadoDatosBasicos WHERE IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END

GO


