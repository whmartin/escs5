/****** Object:  Table [Auditoria].[PROVEEDOR_DocAdjuntoTercero]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para DocAdjuntoTercero ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_DocAdjuntoTercero]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_DocAdjuntoTercero]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_DocAdjuntoTercero]([IDDOCADJUNTO] [int]  NULL, [IDTERCERO_old] [int]  NULL,[IDTERCERO_new] [int]  NULL,[IDDOCUMENTO_old] [int]  NULL,[IDDOCUMENTO_new] [int]  NULL,[DESCRIPCION_old] [nvarchar] (128)  NULL,[DESCRIPCION_new] [nvarchar] (128)  NULL,[LINKDOCUMENTO_old] [nvarchar] (256)  NULL,[LINKDOCUMENTO_new] [nvarchar] (256)  NULL,[ANNO_old] [numeric] (4,0) NULL,[ANNO_new] [numeric] (4,0) NULL,[FECHACREA_old] [datetime]  NULL,[FECHACREA_new] [datetime]  NULL,[USUARIOCREA_old] [nvarchar] (128)  NULL,[USUARIOCREA_new] [nvarchar] (128)  NULL,[FECHAMODIFICA_old] [datetime]  NULL,[FECHAMODIFICA_new] [datetime]  NULL,[USUARIOMODIFICA_old] [nvarchar] (128)  NULL,[USUARIOMODIFICA_new] [nvarchar] (128)  NULL,[IdTemporal_old] [varchar] (20)  NULL,[IdTemporal_new] [varchar] (20)  NULL,[Activo_old] [bit]  NULL,[Activo_new] [bit]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO