USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 07/24/2013 17:30:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 07/24/2013 17:30:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Autor:Mauricio Martinez
--Fecha:2013/07/15 16:00
--Descripcion: Consulta para mostrar un resumen de los modulos validados

CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
@IdEntidad INT 
as
begin
--hay información  en InfoDatosFinancierosEntidad y en InfoDatosExperienciaEntidad tienen por defecto el valor Sin validar en ConfirmaYAprueba
--Si no hay información en los módulos mostrar por defecto Sin información en ConfirmaYAprueba
Select Orden, Componente,Max(NroRevision) as NroRevision,Max(iConfirmaYAprueba) as iConfirmaYAprueba
FROM
(
	select top 1 0 as orden, 'Datos Básicos' as Componente, ISNULL(p.NroRevision,1) as NroRevision,ISNULL(case ConfirmaYAprueba when 1 then 1 when 0 then 0 else -1 end ,-2) as iConfirmaYAprueba
	FROM Proveedor.ValidarInfoDatosBasicosEntidad v
			right join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = v.IdEntidad
			where p.IdEntidad  = @IdEntidad
			and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
--			order by ConfirmaYApruebaTexto ASC
	union
	select top 1 1 as orden, 'Financiera' as Componente, ISNULL(p.NroRevision,1) as NroRevision,ISNULL(case ConfirmaYAprueba when 1 then 1 when 0 then 0 else -1 end ,-2) as iConfirmaYAprueba
	FROM Proveedor.ValidarInfoFinancieraEntidad v
			right join [Proveedor].[InfoFinancieraEntidad] e on e.IdInfoFin = v.IdInfoFin
						inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
			where e.IdEntidad  = @IdEntidad
			and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
--			order by ConfirmaYApruebaTexto ASC
	union
	select top 1 2 as orden, 'Experiencias' as Componente, ISNULL(p.NroRevision,1) as NroRevision,ISNULL(case ConfirmaYAprueba when 1 then 1 when 0 then 0 else -1 end ,-2) as iConfirmaYAprueba
	FROM Proveedor.ValidarInfoExperienciaEntidad v
			right join [Proveedor].[InfoExperienciaEntidad] e on e.IdExpEntidad = v.IdExpEntidad
						inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
			where e.IdEntidad  = @IdEntidad
			and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
			--order by ConfirmaYApruebaTexto ASC
	UNION
	(	select 0 as orden , 'Datos Básicos' as Componente, 1 as NroRevision, -2 as iConfirmaYAprueba
		UNION
		select 1 as orden , 'Financiera' as Componente, 1 as NroRevision, -2 as iConfirmaYAprueba
		union
		select 2 as orden , 'Experiencias' as Componente, 1 as NroRevision, -2 as iConfirmaYAprueba
	)
) as Resumen
Group By Orden, Componente
order by Orden
END



GO


