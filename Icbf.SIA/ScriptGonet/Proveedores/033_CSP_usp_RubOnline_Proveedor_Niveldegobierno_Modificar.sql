USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]    Script Date: 06/14/2013 19:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]
		@IdNiveldegobierno INT,	@CodigoNiveldegobierno NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Niveldegobierno SET CodigoNiveldegobierno = @CodigoNiveldegobierno, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNiveldegobierno = @IdNiveldegobierno
END
GO