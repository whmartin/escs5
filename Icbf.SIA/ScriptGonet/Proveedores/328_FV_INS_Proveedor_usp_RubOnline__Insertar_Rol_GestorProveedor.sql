USE [SIA] 
GO

-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Se asegura de que exista un registro en la entidad [SEG].[Rol]
-- =============================================
IF NOT EXISTS(SELECT [Nombre] FROM  [SEG].[Rol] WHERE [Nombre] ='GESTOR PROVEEDOR') BEGIN
INSERT INTO [SEG].[Rol]
           ([providerKey]
           ,[Nombre]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           )
     VALUES
           ('',
           'GESTOR PROVEEDOR',
           'USUARIO INTERNO GESTOR  PROVEEDOR',
           1,
            'Administrador',
            GETDATE())
END           



