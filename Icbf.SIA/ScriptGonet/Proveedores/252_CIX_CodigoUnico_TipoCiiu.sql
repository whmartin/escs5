USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina indice CodigoUnico de la entidad [Proveedor].[TipoCiiu]
-- =============================================
/****** Object:  Index [CodigoUnico]    Script Date: 06/26/2013 22:56:17 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCiiu]') AND name = N'CodigoUnico')
DROP INDEX [CodigoUnico] ON [Proveedor].[TipoCiiu] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Crea indice CodigoUnico en la entidad [Proveedor].[TipoCiiu]
-- =============================================
/****** Object:  Index [CodigoUnico]    Script Date: 06/26/2013 22:56:23 ******/
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoCiiu] 
(
	[CodigoCiiu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


