USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]    Script Date: 06/24/2013 12:20:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]    Script Date: 06/24/2013 12:20:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]
	@IdInfoFin int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[DocFinancieraProv] 
 WHERE IdInfoFin = CASE WHEN @IdInfoFin IS NULL THEN IdInfoFin ELSE @IdInfoFin END
END

GO

