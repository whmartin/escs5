/****** Object:  Trigger [PROVEEDOR].[TG_Aud_TipoCodigoUNSPSC]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para TipoCodigoUNSPSC
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_TipoCodigoUNSPSC]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_TipoCodigoUNSPSC]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_TipoCodigoUNSPSC]
  ON  [PROVEEDOR].[TipoCodigoUNSPSC]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_TipoCodigoUNSPSC]
  ([IdTipoCodUNSPSC],[Codigo_old], [Codigo_new],[Descripcion_old], [Descripcion_new],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[CodigoSegmento_old], [CodigoSegmento_new],[NombreSegmento_old], [NombreSegmento_new],[CodigoFamilia_old], [CodigoFamilia_new],[NombreFamilia_old], [NombreFamilia_new],[CodigoClase_old], [CodigoClase_new],[NombreClase_old], [NombreClase_new],[Operacion])
SELECT deleted.IdTipoCodUNSPSC,deleted.Codigo,inserted.Codigo,deleted.Descripcion,inserted.Descripcion,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,deleted.CodigoSegmento,inserted.CodigoSegmento,deleted.NombreSegmento,inserted.NombreSegmento,deleted.CodigoFamilia,inserted.CodigoFamilia,deleted.NombreFamilia,inserted.NombreFamilia,deleted.CodigoClase,inserted.CodigoClase,deleted.NombreClase,inserted.NombreClase,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdTipoCodUNSPSC = deleted.IdTipoCodUNSPSCEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_TipoCodigoUNSPSC]
  ([IdTipoCodUNSPSC],[Codigo_old],[Descripcion_old],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[CodigoSegmento_old],[NombreSegmento_old],[CodigoFamilia_old],[NombreFamilia_old],[CodigoClase_old],[NombreClase_old],[Operacion])
SELECT deleted.IdTipoCodUNSPSC,deleted.Codigo,deleted.Descripcion,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,deleted.CodigoSegmento,deleted.NombreSegmento,deleted.CodigoFamilia,deleted.NombreFamilia,deleted.CodigoClase,deleted.NombreClase,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_TipoCodigoUNSPSC]
  ([IdTipoCodUNSPSC],[Codigo_new],[Descripcion_new],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[CodigoSegmento_new],[NombreSegmento_new],[CodigoFamilia_new],[NombreFamilia_new],[CodigoClase_new],[NombreClase_new],[Operacion])
SELECT inserted.IdTipoCodUNSPSC,inserted.Codigo,inserted.Descripcion,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,inserted.CodigoSegmento,inserted.NombreSegmento,inserted.CodigoFamilia,inserted.NombreFamilia,inserted.CodigoClase,inserted.NombreClase,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO