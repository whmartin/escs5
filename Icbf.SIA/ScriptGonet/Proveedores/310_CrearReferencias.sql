USE [SIA]
GO


-- =================================================
-- Author:          Gonet\Bayron Lara
-- Create date:     16/07/2013 04:56:29 p.m.
-- Description:     Crear referencias
-- =================================================


/****** Object:  ForeignKey [FK_ClasedeEntidad_TipodeActividad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad]  WITH CHECK ADD  CONSTRAINT [FK_ClasedeEntidad_TipodeActividad] FOREIGN KEY([IdTipodeActividad])
REFERENCES [Proveedor].[TipodeActividad] ([IdTipodeActividad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad] CHECK CONSTRAINT [FK_ClasedeEntidad_TipodeActividad]
GO
/****** Object:  ForeignKey [FK_DOCADJUNTOTERCERO_TERCERO]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero]  WITH CHECK ADD  CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO] FOREIGN KEY([IDDOCUMENTO])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero] CHECK CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO]
GO
/****** Object:  ForeignKey [FK_DOCADJUNTOTERCERO_TERCERO1]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO1]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero]  WITH CHECK ADD  CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1] FOREIGN KEY([IDTERCERO])
REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DOCADJUNTOTERCERO_TERCERO1]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocAdjuntoTercero]'))
ALTER TABLE [Proveedor].[DocAdjuntoTercero] CHECK CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1]
GO
/****** Object:  ForeignKey [FK_DocDatosBasicoProv_EntidadProvOferente]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv]  WITH CHECK ADD  CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv] CHECK CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente]
GO
/****** Object:  ForeignKey [FK_DocDatosBasicoProv_TipoDocumento]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv]  WITH CHECK ADD  CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocDatosBasicoProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocDatosBasicoProv]'))
ALTER TABLE [Proveedor].[DocDatosBasicoProv] CHECK CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento]
GO
/****** Object:  ForeignKey [FK_DocExperienciaEntidad_InfoExperienciaEntidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocExperienciaEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]'))
ALTER TABLE [Proveedor].[DocExperienciaEntidad]  WITH CHECK ADD  CONSTRAINT [FK_DocExperienciaEntidad_InfoExperienciaEntidad] FOREIGN KEY([IdExpEntidad])
REFERENCES [Proveedor].[InfoExperienciaEntidad] ([IdExpEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocExperienciaEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]'))
ALTER TABLE [Proveedor].[DocExperienciaEntidad] CHECK CONSTRAINT [FK_DocExperienciaEntidad_InfoExperienciaEntidad]
GO
/****** Object:  ForeignKey [FK_DocExperienciaEntidad_TipoDocumento]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocExperienciaEntidad_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]'))
ALTER TABLE [Proveedor].[DocExperienciaEntidad]  WITH CHECK ADD  CONSTRAINT [FK_DocExperienciaEntidad_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocExperienciaEntidad_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocExperienciaEntidad]'))
ALTER TABLE [Proveedor].[DocExperienciaEntidad] CHECK CONSTRAINT [FK_DocExperienciaEntidad_TipoDocumento]
GO
/****** Object:  ForeignKey [FK_DocFinancieraProv_InfoFinancieraEntidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv]  WITH CHECK ADD  CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad] FOREIGN KEY([IdInfoFin])
REFERENCES [Proveedor].[InfoFinancieraEntidad] ([IdInfoFin])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv] CHECK CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad]
GO
/****** Object:  ForeignKey [FK_DocFinancieraProv_TipoDocumento]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv]  WITH CHECK ADD  CONSTRAINT [FK_DocFinancieraProv_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv] CHECK CONSTRAINT [FK_DocFinancieraProv_TipoDocumento]
GO
/****** Object:  ForeignKey [FK_DocNotificacionJudicial_NotificacionJudicial]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocNotificacionJudicial_NotificacionJudicial]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]'))
ALTER TABLE [Proveedor].[DocNotificacionJudicial]  WITH CHECK ADD  CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial] FOREIGN KEY([IdNotJudicial])
REFERENCES [Proveedor].[NotificacionJudicial] ([IdNotJudicial])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocNotificacionJudicial_NotificacionJudicial]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]'))
ALTER TABLE [Proveedor].[DocNotificacionJudicial] CHECK CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_EstadoDatosBasicos]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_EstadoDatosBasicos]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos] FOREIGN KEY([IdEstado])
REFERENCES [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_EstadoDatosBasicos]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_Niveldegobierno]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_Niveldegobierno]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno] FOREIGN KEY([IdTipoNivelGob])
REFERENCES [Proveedor].[Niveldegobierno] ([IdNiveldegobierno])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_Niveldegobierno]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_NivelOrganizacional]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_NivelOrganizacional]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional] FOREIGN KEY([IdTipoNivelOrganizacional])
REFERENCES [Proveedor].[NivelOrganizacional] ([IdNivelOrganizacional])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_NivelOrganizacional]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_TERCERO]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TERCERO] FOREIGN KEY([IdTercero])
REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TERCERO]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_TipoCiiu]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoCiiu]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TipoCiiu] FOREIGN KEY([IdTipoCiiuPrincipal])
REFERENCES [Proveedor].[TipoCiiu] ([IdTipoCiiu])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoCiiu]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TipoCiiu]
GO
/****** Object:  ForeignKey [FK_EntidadProvOferente_TipoSectorEntidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoSectorEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad] FOREIGN KEY([IdTipoSector])
REFERENCES [Proveedor].[TipoSectorEntidad] ([IdTipoSectorEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_TipoSectorEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad]
GO
/****** Object:  ForeignKey [fk_IdAcuerdoProveedor]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[fk_IdAcuerdoProveedor]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [fk_IdAcuerdoProveedor] FOREIGN KEY([IdAcuerdo])
REFERENCES [Proveedor].[Acuerdos] ([IdAcuerdo])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[fk_IdAcuerdoProveedor]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [fk_IdAcuerdoProveedor]
GO
/****** Object:  ForeignKey [FK_IdExpEntidad_InfoExperienciaEntidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_IdExpEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]'))
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad]  WITH CHECK ADD  CONSTRAINT [FK_IdExpEntidad_InfoExperienciaEntidad] FOREIGN KEY([IdExpEntidad])
REFERENCES [Proveedor].[InfoExperienciaEntidad] ([IdExpEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_IdExpEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]'))
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] CHECK CONSTRAINT [FK_IdExpEntidad_InfoExperienciaEntidad]
GO
/****** Object:  ForeignKey [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]'))
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad]  WITH CHECK ADD  CONSTRAINT [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC] FOREIGN KEY([IdTipoCodUNSPSC])
REFERENCES [Proveedor].[TipoCodigoUNSPSC] ([IdTipoCodUNSPSC])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ExperienciaCodUNSPSCEntidad]'))
ALTER TABLE [Proveedor].[ExperienciaCodUNSPSCEntidad] CHECK CONSTRAINT [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_EntidadProvOferente]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_TipodeActividad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad] FOREIGN KEY([IdTipoActividad])
REFERENCES [Proveedor].[TipodeActividad] ([IdTipodeActividad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_TipodeentidadPublica]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeentidadPublica]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica] FOREIGN KEY([IdTipoEntidadPublica])
REFERENCES [Proveedor].[TipodeentidadPublica] ([IdTipodeentidadPublica])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeentidadPublica]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_Tipoentidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad] FOREIGN KEY([IdTipoEntidad])
REFERENCES [Proveedor].[Tipoentidad] ([IdTipoentidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_TipoRegimenTributario]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipoRegimenTributario]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario] FOREIGN KEY([IdTipoRegTrib])
REFERENCES [Proveedor].[TipoRegimenTributario] ([IdTipoRegimenTributario])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipoRegimenTributario]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario]
GO
/****** Object:  ForeignKey [FK_InfoAdminEntidad_Zona]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Zona]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_Zona] FOREIGN KEY([IdZona])
REFERENCES [Oferente].[Zona] ([IdZona])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Zona]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_Zona]
GO
/****** Object:  ForeignKey [FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]'))
ALTER TABLE [Proveedor].[InfoExperienciaEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoExperienciaEntidad_TipoCodigoUNSPSC] FOREIGN KEY([IdTipoCodUNSPSC])
REFERENCES [Proveedor].[TipoCodigoUNSPSC] ([IdTipoCodUNSPSC])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]'))
ALTER TABLE [Proveedor].[InfoExperienciaEntidad] CHECK CONSTRAINT [FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]
GO
/****** Object:  ForeignKey [FK_InfoFinancieraEntidad_EntidadProvOferente]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente]
GO
/****** Object:  ForeignKey [FK_InfoFinancieraEntidad_EstadoValidacionDocumental]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EstadoValidacionDocumental]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental] FOREIGN KEY([EstadoValidacion])
REFERENCES [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_EstadoValidacionDocumental]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental]
GO
/****** Object:  ForeignKey [FK_InfoFinancieraEntidad_Vigencia]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_Vigencia]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia] FOREIGN KEY([IdVigencia])
REFERENCES [Global].[Vigencia] ([IdVigencia])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoFinancieraEntidad_Vigencia]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia]
GO
/****** Object:  ForeignKey [FK_MotivoCambioEstado_TERCERO]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_MotivoCambioEstado_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[MotivoCambioEstado]'))
ALTER TABLE [Proveedor].[MotivoCambioEstado]  WITH CHECK ADD  CONSTRAINT [FK_MotivoCambioEstado_TERCERO] FOREIGN KEY([IdTercero])
REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_MotivoCambioEstado_TERCERO]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[MotivoCambioEstado]'))
ALTER TABLE [Proveedor].[MotivoCambioEstado] CHECK CONSTRAINT [FK_MotivoCambioEstado_TERCERO]
GO
/****** Object:  ForeignKey [FK_NotificacionJudicial_EntidadProvOferente]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_NotificacionJudicial_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]'))
ALTER TABLE [Proveedor].[NotificacionJudicial]  WITH CHECK ADD  CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_NotificacionJudicial_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[NotificacionJudicial]'))
ALTER TABLE [Proveedor].[NotificacionJudicial] CHECK CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente]
GO
/****** Object:  ForeignKey [FK_Sucursal_Departamento]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_Departamento] FOREIGN KEY([Departamento])
REFERENCES [DIV].[Departamento] ([IdDepartamento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Departamento]
GO
/****** Object:  ForeignKey [FK_Sucursal_IdEntidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_IdEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_IdEntidad] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_IdEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_IdEntidad]
GO
/****** Object:  ForeignKey [FK_Sucursal_Municipio]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Municipio]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_Municipio] FOREIGN KEY([Municipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Municipio]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Municipio]
GO
/****** Object:  ForeignKey [FK_TipoDocumentoPrograma_Programa]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_Programa]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma]  WITH CHECK ADD  CONSTRAINT [FK_TipoDocumentoPrograma_Programa] FOREIGN KEY([IdPrograma])
REFERENCES [SEG].[Programa] ([IdPrograma])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_Programa]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] CHECK CONSTRAINT [FK_TipoDocumentoPrograma_Programa]
GO
/****** Object:  ForeignKey [FK_TipoDocumentoPrograma_TipoDocumento]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma]  WITH CHECK ADD  CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_TipoDocumentoPrograma_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]'))
ALTER TABLE [Proveedor].[TipoDocumentoPrograma] CHECK CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento]
GO
/****** Object:  ForeignKey [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoDatosBasicosEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoDatosBasicosEntidad]  WITH CHECK ADD  CONSTRAINT [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoDatosBasicosEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoDatosBasicosEntidad] CHECK CONSTRAINT [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]
GO
/****** Object:  ForeignKey [IdInfoExperienciaEntidad_InfoExperienciaEntidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoExperienciaEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoExperienciaEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoExperienciaEntidad]  WITH CHECK ADD  CONSTRAINT [IdInfoExperienciaEntidad_InfoExperienciaEntidad] FOREIGN KEY([IdExpEntidad])
REFERENCES [Proveedor].[InfoExperienciaEntidad] ([IdExpEntidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoExperienciaEntidad_InfoExperienciaEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoExperienciaEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoExperienciaEntidad] CHECK CONSTRAINT [IdInfoExperienciaEntidad_InfoExperienciaEntidad]
GO
/****** Object:  ForeignKey [IdInfoFinancieraEntidad_InfoFinancieraEntidad]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoFinancieraEntidad_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [IdInfoFinancieraEntidad_InfoFinancieraEntidad] FOREIGN KEY([IdInfoFin])
REFERENCES [Proveedor].[InfoFinancieraEntidad] ([IdInfoFin])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdInfoFinancieraEntidad_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarInfoFinancieraEntidad]'))
ALTER TABLE [Proveedor].[ValidarInfoFinancieraEntidad] CHECK CONSTRAINT [IdInfoFinancieraEntidad_InfoFinancieraEntidad]
GO
/****** Object:  ForeignKey [IdTercero_Tercero]    Script Date: 07/16/2013 12:42:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdTercero_Tercero]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarTercero]'))
ALTER TABLE [Proveedor].[ValidarTercero]  WITH CHECK ADD  CONSTRAINT [IdTercero_Tercero] FOREIGN KEY([IdTercero])
REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[IdTercero_Tercero]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ValidarTercero]'))
ALTER TABLE [Proveedor].[ValidarTercero] CHECK CONSTRAINT [IdTercero_Tercero]
GO
