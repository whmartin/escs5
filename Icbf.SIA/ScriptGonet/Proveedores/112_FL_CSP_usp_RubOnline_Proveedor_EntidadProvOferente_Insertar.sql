USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]    Script Date: 06/24/2013 21:10:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]    Script Date: 06/24/2013 21:10:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que guarda un nuevo EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]
		@IdEntidad INT OUTPUT, 	
		@TipoEntOfProv BIT=NULL,	
		@IdTercero INT=NULL,	
		@IdTipoCiiuPrincipal INT=NULL,	
		@IdTipoCiiuSecundario INT=NULL,	
		@IdTipoSector INT=NULL,	
		@IdTipoClaseEntidad INT=NULL,	
		@IdTipoRamaPublica INT=NULL,	
		@IdTipoNivelGob INT=NULL,	
		@IdTipoNivelOrganizacional INT=NULL,	
		@IdTipoCertificadorCalidad INT=NULL,	
		@FechaCiiuPrincipal DATETIME=NULL,	
		@FechaCiiuSecundario DATETIME=NULL,	
		@FechaConstitucion DATETIME=NULL,	
		@FechaVigencia DATETIME=NULL,	
		@FechaMatriculaMerc DATETIME=NULL,	
		@FechaExpiracion DATETIME=NULL,	
		@TipoVigencia BIT=NULL,	
		@ExenMatriculaMer BIT=NULL,	
		@MatriculaMercantil NVARCHAR(20)=NULL,	
		@ObserValidador NVARCHAR(256)=NULL,	
		@AnoRegistro INT=NULL,	
		@IdEstado INT=NULL, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EntidadProvOferente(TipoEntOfProv, IdTercero, IdTipoCiiuPrincipal, IdTipoCiiuSecundario, IdTipoSector, IdTipoClaseEntidad, IdTipoRamaPublica, IdTipoNivelGob, IdTipoNivelOrganizacional, IdTipoCertificadorCalidad, FechaCiiuPrincipal, FechaCiiuSecundario, FechaConstitucion, FechaVigencia, FechaMatriculaMerc, FechaExpiracion, TipoVigencia, ExenMatriculaMer, MatriculaMercantil, ObserValidador, AnoRegistro, IdEstado, UsuarioCrea, FechaCrea)
					  VALUES(@TipoEntOfProv, @IdTercero, @IdTipoCiiuPrincipal, @IdTipoCiiuSecundario, @IdTipoSector, @IdTipoClaseEntidad, @IdTipoRamaPublica, @IdTipoNivelGob, @IdTipoNivelOrganizacional, @IdTipoCertificadorCalidad, @FechaCiiuPrincipal, @FechaCiiuSecundario, @FechaConstitucion, @FechaVigencia, @FechaMatriculaMerc, @FechaExpiracion, @TipoVigencia, @ExenMatriculaMer, @MatriculaMercantil, @ObserValidador, @AnoRegistro, @IdEstado, @UsuarioCrea, GETDATE())
	SELECT @IdEntidad = @@IDENTITY 		
END

GO


