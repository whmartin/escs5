USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[InfoExperienciaEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[InfoExperienciaEntidad]
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:20:20 PM
-- =============================================
CREATE TABLE [Proveedor].[InfoExperienciaEntidad](
 [IdExpEntidad] [INT]  IDENTITY(1,1) NOT NULL,
 [IdEntidad] [INT] ,
 [IdTipoSector] [INT] ,
 [IdTipoEstadoExp] [INT]  ,
 [IdTipoModalidadExp] [INT]  ,
 [IdTipoModalidad] [INT]  ,
 [IdTipoPoblacionAtendida] [INT]  ,
 [IdTipoRangoExpAcum] [INT]  ,
 [IdTipoCodUNSPSC] [INT]  NOT NULL,
 [IdTipoEntidadContratante] [INT]  ,
 [EntidadContratante] [NVARCHAR] (128) NOT NULL,
 [FechaInicio] [DATETIME]  NOT NULL,
 [FechaFin] [DATETIME]  NOT NULL,
 [NumeroContrato] [NVARCHAR] (128),
 [ObjetoContrato] [NVARCHAR] (256) NOT NULL,
 [Vigente] [BIT]  NOT NULL,
 [Cuantia] [NUMERIC] (18,3) NOT NULL,
 [ExperienciaMeses] [NUMERIC] (18,3),
 [EstadoDocumental] [INT] ,
 [UnionTempConsorcio] [BIT] ,
 [PorcentParticipacion] [NUMERIC] (5),
 [AtencionDeptos] [BIT] ,
 [JardinOPreJardin] [BIT] ,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_InfoExperienciaEntidad] PRIMARY KEY CLUSTERED 
(
 	[IdExpEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
