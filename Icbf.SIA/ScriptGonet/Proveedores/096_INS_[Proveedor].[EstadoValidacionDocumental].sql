USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en [Proveedor].[EstadoValidacionDocumental]
-- =============================================
SET IDENTITY_INSERT [Proveedor].[EstadoValidacionDocumental] ON
INSERT [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental], [CodigoEstadoValidacionDocumental], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'01', N'PENDIENTE DE VALIDACI�N', 1, N'administrador', CAST(0x0000A1DC00000000 AS DateTime), N'Administrador', CAST(0x0000A1DD0132630B AS DateTime))
INSERT [Proveedor].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental], [CodigoEstadoValidacionDocumental], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'01', N'DDD', 1, N'Administrador', CAST(0x0000A1DD013278A0 AS DateTime), N'Administrador', CAST(0x0000A1DD0132FECD AS DateTime))
SET IDENTITY_INSERT [Proveedor].[EstadoValidacionDocumental] OFF