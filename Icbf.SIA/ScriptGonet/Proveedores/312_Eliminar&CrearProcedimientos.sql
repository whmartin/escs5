USE [SIA]
GO


-- =================================================
-- Author:          Gonet\Bayron Lara
-- Create date:     16/07/2013 04:56:29 p.m.
-- Description:     Eliminar y crear procedimientos
-- =================================================

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar]    Script Date: 07/16/2013 14:28:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]    Script Date: 07/16/2013 14:28:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup]    Script Date: 07/16/2013 14:28:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]    Script Date: 07/16/2013 14:28:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]    Script Date: 07/16/2013 14:28:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]    Script Date: 07/16/2013 14:28:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]    Script Date: 07/16/2013 14:28:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]    Script Date: 07/16/2013 14:28:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]    Script Date: 07/16/2013 14:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]    Script Date: 07/16/2013 14:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]    Script Date: 07/16/2013 14:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]    Script Date: 07/16/2013 14:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]    Script Date: 07/16/2013 14:28:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]    Script Date: 07/16/2013 14:28:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]    Script Date: 07/16/2013 14:28:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]    Script Date: 07/16/2013 14:28:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]    Script Date: 07/16/2013 14:28:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 07/16/2013 14:28:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]    Script Date: 07/16/2013 14:28:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]    Script Date: 07/16/2013 14:28:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]    Script Date: 07/16/2013 14:28:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]    Script Date: 07/16/2013 14:28:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]    Script Date: 07/16/2013 14:28:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad]    Script Date: 07/16/2013 14:28:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]    Script Date: 07/16/2013 14:28:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]    Script Date: 07/16/2013 14:28:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]    Script Date: 07/16/2013 14:28:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]    Script Date: 07/16/2013 14:28:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]    Script Date: 07/16/2013 14:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]    Script Date: 07/16/2013 14:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]    Script Date: 07/16/2013 14:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]    Script Date: 07/16/2013 14:28:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona]    Script Date: 07/16/2013 14:28:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona]    Script Date: 07/16/2013 14:28:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]    Script Date: 07/16/2013 14:28:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]    Script Date: 07/16/2013 14:28:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]    Script Date: 07/16/2013 14:28:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]    Script Date: 07/16/2013 14:28:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]    Script Date: 07/16/2013 14:28:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]    Script Date: 07/16/2013 14:28:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]    Script Date: 07/16/2013 14:28:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]    Script Date: 07/16/2013 14:28:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]    Script Date: 07/16/2013 14:28:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]    Script Date: 07/16/2013 14:28:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]    Script Date: 07/16/2013 14:28:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]    Script Date: 07/16/2013 14:28:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]    Script Date: 07/16/2013 14:28:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]    Script Date: 07/16/2013 14:28:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]    Script Date: 07/16/2013 14:28:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]    Script Date: 07/16/2013 14:28:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]    Script Date: 07/16/2013 14:28:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]    Script Date: 07/16/2013 14:28:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]    Script Date: 07/16/2013 14:28:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]    Script Date: 07/16/2013 14:28:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]    Script Date: 07/16/2013 14:28:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]    Script Date: 07/16/2013 14:28:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]    Script Date: 07/16/2013 14:28:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]    Script Date: 07/16/2013 14:28:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]    Script Date: 07/16/2013 14:28:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]    Script Date: 07/16/2013 14:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]    Script Date: 07/16/2013 14:28:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]    Script Date: 07/16/2013 14:28:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]    Script Date: 07/16/2013 14:28:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]    Script Date: 07/16/2013 14:28:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]    Script Date: 07/16/2013 14:28:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar]    Script Date: 07/16/2013 14:28:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]    Script Date: 07/16/2013 14:28:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]    Script Date: 07/16/2013 14:28:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]    Script Date: 07/16/2013 14:28:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]    Script Date: 07/16/2013 14:28:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]    Script Date: 07/16/2013 14:28:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]    Script Date: 07/16/2013 14:28:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]    Script Date: 07/16/2013 14:28:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]    Script Date: 07/16/2013 14:28:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]    Script Date: 07/16/2013 14:28:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]    Script Date: 07/16/2013 14:28:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]    Script Date: 07/16/2013 14:28:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]    Script Date: 07/16/2013 14:28:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona]    Script Date: 07/16/2013 14:28:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]    Script Date: 07/16/2013 14:28:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]    Script Date: 07/16/2013 14:28:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]    Script Date: 07/16/2013 14:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 07/16/2013 14:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 07/16/2013 14:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]    Script Date: 07/16/2013 14:28:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]    Script Date: 07/16/2013 14:28:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]    Script Date: 07/16/2013 14:28:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]    Script Date: 07/16/2013 14:28:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]    Script Date: 07/16/2013 14:28:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]    Script Date: 07/16/2013 14:28:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]    Script Date: 07/16/2013 14:28:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]    Script Date: 07/16/2013 14:29:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]    Script Date: 07/16/2013 14:28:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]    Script Date: 07/16/2013 14:28:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]    Script Date: 07/16/2013 14:28:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]    Script Date: 07/16/2013 14:28:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]    Script Date: 07/16/2013 14:28:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]    Script Date: 07/16/2013 14:28:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]    Script Date: 07/16/2013 14:28:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]    Script Date: 07/16/2013 14:28:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]    Script Date: 07/16/2013 14:28:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarEliminarUsuariosInactivos]    Script Date: 07/16/2013 14:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EnviarEliminarUsuariosInactivos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarEliminarUsuariosInactivos]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]    Script Date: 07/16/2013 14:28:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]    Script Date: 07/16/2013 14:28:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]    Script Date: 07/16/2013 14:28:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]    Script Date: 07/16/2013 14:28:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]    Script Date: 07/16/2013 14:28:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Modificar]    Script Date: 07/16/2013 14:28:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]    Script Date: 07/16/2013 14:28:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]    Script Date: 07/16/2013 14:28:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]    Script Date: 07/16/2013 14:28:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]    Script Date: 07/16/2013 14:28:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]    Script Date: 07/16/2013 14:28:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]    Script Date: 07/16/2013 14:28:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]    Script Date: 07/16/2013 14:28:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]    Script Date: 07/16/2013 14:28:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]    Script Date: 07/16/2013 14:28:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]    Script Date: 07/16/2013 14:28:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]    Script Date: 07/16/2013 14:28:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]    Script Date: 07/16/2013 14:28:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]    Script Date: 07/16/2013 14:28:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]    Script Date: 07/16/2013 14:28:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]    Script Date: 07/16/2013 14:28:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]    Script Date: 07/16/2013 14:28:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]    Script Date: 07/16/2013 14:28:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]    Script Date: 07/16/2013 14:28:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar]    Script Date: 07/16/2013 14:28:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]    Script Date: 07/16/2013 14:28:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]    Script Date: 07/16/2013 14:28:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]    Script Date: 07/16/2013 14:28:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]    Script Date: 07/16/2013 14:28:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]    Script Date: 07/16/2013 14:28:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]    Script Date: 07/16/2013 14:28:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]    Script Date: 07/16/2013 14:28:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]    Script Date: 07/16/2013 14:28:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]    Script Date: 07/16/2013 14:28:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]    Script Date: 07/16/2013 14:28:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]    Script Date: 07/16/2013 14:28:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]    Script Date: 07/16/2013 14:28:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]    Script Date: 07/16/2013 14:28:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar]    Script Date: 07/16/2013 14:28:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]    Script Date: 07/16/2013 14:28:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]    Script Date: 07/16/2013 14:28:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]    Script Date: 07/16/2013 14:28:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]    Script Date: 07/16/2013 14:28:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]    Script Date: 07/16/2013 14:28:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]    Script Date: 07/16/2013 14:28:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]    Script Date: 07/16/2013 14:28:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]    Script Date: 07/16/2013 14:28:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]    Script Date: 07/16/2013 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]    Script Date: 07/16/2013 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]    Script Date: 07/16/2013 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]    Script Date: 07/16/2013 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]    Script Date: 07/16/2013 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]    Script Date: 07/16/2013 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll]    Script Date: 07/16/2013 14:28:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]    Script Date: 07/16/2013 14:28:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]    Script Date: 07/16/2013 14:28:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]    Script Date: 07/16/2013 14:28:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]    Script Date: 07/16/2013 14:28:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]    Script Date: 07/16/2013 14:28:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]    Script Date: 07/16/2013 14:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]    Script Date: 07/16/2013 14:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar]    Script Date: 07/16/2013 14:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]    Script Date: 07/16/2013 14:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]    Script Date: 07/16/2013 14:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar]    Script Date: 07/16/2013 14:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]    Script Date: 07/16/2013 14:28:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]    Script Date: 07/16/2013 14:28:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]    Script Date: 07/16/2013 14:28:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]    Script Date: 07/16/2013 14:28:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]    Script Date: 07/16/2013 14:28:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]    Script Date: 07/16/2013 14:28:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]    Script Date: 07/16/2013 14:28:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]    Script Date: 07/16/2013 14:28:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]    Script Date: 07/16/2013 14:28:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar]    Script Date: 07/16/2013 14:28:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]    Script Date: 07/16/2013 14:28:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]    Script Date: 07/16/2013 14:28:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]    Script Date: 07/16/2013 14:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]    Script Date: 07/16/2013 14:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]    Script Date: 07/16/2013 14:28:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]    Script Date: 07/16/2013 14:28:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]    Script Date: 07/16/2013 14:28:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]    Script Date: 07/16/2013 14:28:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]    Script Date: 07/16/2013 14:28:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]    Script Date: 07/16/2013 14:28:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]    Script Date: 07/16/2013 14:28:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]    Script Date: 07/16/2013 14:28:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]    Script Date: 07/16/2013 14:28:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]    Script Date: 07/16/2013 14:28:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]    Script Date: 07/16/2013 14:28:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]    Script Date: 07/16/2013 14:28:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]    Script Date: 07/16/2013 14:28:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]    Script Date: 07/16/2013 14:28:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]    Script Date: 07/16/2013 14:28:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]    Script Date: 07/16/2013 14:28:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]    Script Date: 07/16/2013 14:28:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]    Script Date: 07/16/2013 14:28:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]    Script Date: 07/16/2013 14:28:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]    Script Date: 07/16/2013 14:28:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]    Script Date: 07/16/2013 14:28:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]    Script Date: 07/16/2013 14:28:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]    Script Date: 07/16/2013 14:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[NivelOrganizacional] 
 WHERE Estado =1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]    Script Date: 07/16/2013 14:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]
	@CodigoNivelOrganizacional NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[NivelOrganizacional] WHERE CodigoNivelOrganizacional = CASE WHEN @CodigoNivelOrganizacional IS NULL THEN CodigoNivelOrganizacional ELSE @CodigoNivelOrganizacional END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]    Script Date: 07/16/2013 14:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que actualiza un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]
		@IdNivelOrganizacional INT,	@CodigoNivelOrganizacional NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.NivelOrganizacional SET CodigoNivelOrganizacional = @CodigoNivelOrganizacional, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNivelOrganizacional = @IdNivelOrganizacional
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]    Script Date: 07/16/2013 14:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]
		@IdNivelOrganizacional INT OUTPUT, 	@CodigoNivelOrganizacional NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.NivelOrganizacional(CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNivelOrganizacional, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNivelOrganizacional = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]    Script Date: 07/16/2013 14:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que elimina un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]
	@IdNivelOrganizacional INT
AS
BEGIN
	DELETE Proveedor.NivelOrganizacional WHERE IdNivelOrganizacional = @IdNivelOrganizacional
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]    Script Date: 07/16/2013 14:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]
	@IdNivelOrganizacional INT
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[NivelOrganizacional] WHERE  IdNivelOrganizacional = @IdNivelOrganizacional
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]    Script Date: 07/16/2013 14:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]
	
AS
BEGIN

 SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Niveldegobierno] 
 WHERE  Estado =1
 
 END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]    Script Date: 07/16/2013 14:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]
@CodigoNiveldegobierno NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Niveldegobierno] WHERE CodigoNiveldegobierno = CASE WHEN @CodigoNiveldegobierno IS NULL THEN CodigoNiveldegobierno ELSE @CodigoNiveldegobierno END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]    Script Date: 07/16/2013 14:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]
		@IdNiveldegobierno INT,	@CodigoNiveldegobierno NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Niveldegobierno SET CodigoNiveldegobierno = @CodigoNiveldegobierno, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNiveldegobierno = @IdNiveldegobierno
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]    Script Date: 07/16/2013 14:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]
		@IdNiveldegobierno INT OUTPUT, 	@CodigoNiveldegobierno NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Niveldegobierno(CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNiveldegobierno, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNiveldegobierno = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]    Script Date: 07/16/2013 14:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que elimina un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]
	@IdNiveldegobierno INT
AS
BEGIN
	DELETE Proveedor.Niveldegobierno WHERE IdNiveldegobierno = @IdNiveldegobierno
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]    Script Date: 07/16/2013 14:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]
	@IdNiveldegobierno INT
AS
BEGIN
 SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Niveldegobierno] WHERE  IdNiveldegobierno = @IdNiveldegobierno
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]    Script Date: 07/16/2013 14:28:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]
	@CodigoEstadoValidacionDocumental NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoValidacionDocumental, CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[EstadoValidacionDocumental] WHERE CodigoEstadoValidacionDocumental = CASE WHEN @CodigoEstadoValidacionDocumental IS NULL THEN CodigoEstadoValidacionDocumental ELSE @CodigoEstadoValidacionDocumental END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]    Script Date: 07/16/2013 14:28:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que actualiza un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]
		@IdEstadoValidacionDocumental INT,	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.EstadoValidacionDocumental SET CodigoEstadoValidacionDocumental = @CodigoEstadoValidacionDocumental, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]    Script Date: 07/16/2013 14:28:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]
		@IdEstadoValidacionDocumental INT OUTPUT, 	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoValidacionDocumental(CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoEstadoValidacionDocumental, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoValidacionDocumental = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]    Script Date: 07/16/2013 14:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]
	@IdEstadoValidacionDocumental INT
AS
BEGIN
	DELETE Proveedor.EstadoValidacionDocumental WHERE IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]    Script Date: 07/16/2013 14:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]
	@IdEstadoValidacionDocumental INT
AS
BEGIN
 SELECT IdEstadoValidacionDocumental, CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[EstadoValidacionDocumental] WHERE  IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]    Script Date: 07/16/2013 14:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Fabian Valencia>
-- Create date: <12/06/2013>
-- Description:	<Obtiene el estado a través de su id>
-- =============================================
Create PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]
	 @IdTercero INT
AS
BEGIN
	SELECT     Oferente.EstadoTercero.*
	FROM         Oferente.EstadoTercero
	WHERE		Oferente.EstadoTercero.IdEstadoTercero = @IdTercero
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]    Script Date: 07/16/2013 14:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]
	@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
     FROM [Proveedor].[EstadoDatosBasicos] 
        WHERE Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]    Script Date: 07/16/2013 14:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]
	@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
     FROM [Proveedor].[EstadoDatosBasicos] 
        WHERE Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
        AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]    Script Date: 07/16/2013 14:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que actualiza un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]
		@IdEstadoDatosBasicos INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.EstadoDatosBasicos 
		SET Descripcion = @Descripcion, 
			Estado = @Estado, 
			UsuarioModifica = @UsuarioModifica, 
			FechaModifica = GETDATE() 
		WHERE IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]    Script Date: 07/16/2013 14:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]
		@IdEstadoDatosBasicos INT OUTPUT, 	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoDatosBasicos(Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoDatosBasicos = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]    Script Date: 07/16/2013 14:28:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]
	@IdEstadoDatosBasicos INT
AS
BEGIN
	DELETE Proveedor.EstadoDatosBasicos WHERE IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]    Script Date: 07/16/2013 14:28:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]
	@IdEstadoDatosBasicos INT
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
    FROM [Proveedor].[EstadoDatosBasicos] 
    WHERE  IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]    Script Date: 07/16/2013 14:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]
	@IdVigencia INT = NULL
AS
 
 declare @SMLV int
 
 SELECT @SMLV = Valor FROM oferente.SalarioMinimo s
 inner join [Global].[Vigencia] v on [Año] = [AcnoVigencia]
 and v.Activo = 1 and s.Estado = 1
 and @IdVigencia = IdVigencia

 SELECT @SMLV' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]    Script Date: 07/16/2013 14:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]
	
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[RamaoEstructura] 
 WHERE Estado = 1
  END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar]    Script Date: 07/16/2013 14:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar]
	@CodigoRamaEstructura NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[RamaoEstructura] WHERE CodigoRamaEstructura = CASE WHEN @CodigoRamaEstructura IS NULL THEN CodigoRamaEstructura ELSE @CodigoRamaEstructura END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]    Script Date: 07/16/2013 14:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que actualiza un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]
		@IdRamaEstructura INT,	@CodigoRamaEstructura NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.RamaoEstructura SET CodigoRamaEstructura = @CodigoRamaEstructura, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRamaEstructura = @IdRamaEstructura
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]    Script Date: 07/16/2013 14:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que guarda un nuevo RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]
		@IdRamaEstructura INT OUTPUT, 	@CodigoRamaEstructura NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.RamaoEstructura(CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRamaEstructura, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdRamaEstructura = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]    Script Date: 07/16/2013 14:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que elimina un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]
	@IdRamaEstructura INT
AS
BEGIN
	DELETE Proveedor.RamaoEstructura WHERE IdRamaEstructura = @IdRamaEstructura
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]    Script Date: 07/16/2013 14:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]
	@IdRamaEstructura INT
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[RamaoEstructura] WHERE  IdRamaEstructura = @IdRamaEstructura
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]    Script Date: 07/16/2013 14:28:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]
	@CodigoTablaParametrica NVARCHAR(128) = NULL,
	@NombreTablaParametrica NVARCHAR(128) = NULL,
	@Estado					BIT = NULL
AS
BEGIN
	SELECT
		IdTablaParametrica,
		CodigoTablaParametrica,
		NombreTablaParametrica,
		Url,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[TablaParametrica]
	WHERE CodigoTablaParametrica = CASE WHEN @CodigoTablaParametrica IS NULL 
										THEN CodigoTablaParametrica 
										ELSE @CodigoTablaParametrica
								   END
	AND NombreTablaParametrica LIKE ''%'' + CASE WHEN @NombreTablaParametrica IS NULL 
											   THEN NombreTablaParametrica 
											   ELSE @NombreTablaParametrica
										  END + ''%''
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado
		END
	ORDER BY NombreTablaParametrica
END--FIN PP
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]    Script Date: 07/16/2013 14:28:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]
		@IdTablaParametrica INT,	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TablaParametrica SET CodigoTablaParametrica = @CodigoTablaParametrica, NombreTablaParametrica = @NombreTablaParametrica, Url = @Url, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTablaParametrica = @IdTablaParametrica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]    Script Date: 07/16/2013 14:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]
		@IdTablaParametrica INT OUTPUT, 	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TablaParametrica(CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTablaParametrica, @NombreTablaParametrica, @Url, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTablaParametrica = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]    Script Date: 07/16/2013 14:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]
	@IdTablaParametrica INT
AS
BEGIN
	DELETE Proveedor.TablaParametrica WHERE IdTablaParametrica = @IdTablaParametrica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]    Script Date: 07/16/2013 14:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]
	@IdTablaParametrica INT
AS
BEGIN
 SELECT IdTablaParametrica, CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TablaParametrica] WHERE  IdTablaParametrica = @IdTablaParametrica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar]    Script Date: 07/16/2013 14:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar]
	@IdTipoDocIdentifica INT
AS
BEGIN
 SELECT IdTipoDocIdentifica, CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoDocIdentifica] 
 WHERE  IdTipoDocIdentifica = @IdTipoDocIdentifica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]    Script Date: 07/16/2013 14:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]
	AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipodeentidadPublica] 
 WHERE  Estado = 1 
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]    Script Date: 07/16/2013 14:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]
	@CodigoTipodeentidadPublica NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeentidadPublica] WHERE CodigoTipodeentidadPublica = CASE WHEN @CodigoTipodeentidadPublica IS NULL THEN CodigoTipodeentidadPublica ELSE @CodigoTipodeentidadPublica END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar]    Script Date: 07/16/2013 14:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar]
		@IdTipodeentidadPublica INT,	@CodigoTipodeentidadPublica NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipodeentidadPublica SET CodigoTipodeentidadPublica = @CodigoTipodeentidadPublica, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipodeentidadPublica = @IdTipodeentidadPublica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]    Script Date: 07/16/2013 14:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]
		@IdTipodeentidadPublica INT OUTPUT, 	@CodigoTipodeentidadPublica NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipodeentidadPublica(CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipodeentidadPublica, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipodeentidadPublica = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]    Script Date: 07/16/2013 14:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]
	@IdTipodeentidadPublica INT
AS
BEGIN
	DELETE Proveedor.TipodeentidadPublica WHERE IdTipodeentidadPublica = @IdTipodeentidadPublica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]    Script Date: 07/16/2013 14:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]
	@IdTipodeentidadPublica INT
AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeentidadPublica] WHERE  IdTipodeentidadPublica = @IdTipodeentidadPublica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]    Script Date: 07/16/2013 14:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]
	@CodigoTipodeActividad NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipodeActividad, CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeActividad] WHERE CodigoTipodeActividad = CASE WHEN @CodigoTipodeActividad IS NULL THEN CodigoTipodeActividad ELSE @CodigoTipodeActividad END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]    Script Date: 07/16/2013 14:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]
		@IdTipodeActividad INT,	@CodigoTipodeActividad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipodeActividad SET CodigoTipodeActividad = @CodigoTipodeActividad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipodeActividad = @IdTipodeActividad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]    Script Date: 07/16/2013 14:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]
		@IdTipodeActividad INT OUTPUT, 	@CodigoTipodeActividad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipodeActividad(CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipodeActividad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipodeActividad = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]    Script Date: 07/16/2013 14:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]
	@IdTipodeActividad INT
AS
BEGIN
	DELETE Proveedor.TipodeActividad WHERE IdTipodeActividad = @IdTipodeActividad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll]    Script Date: 07/16/2013 14:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll]
	
AS
BEGIN
 SELECT IdTipodeActividad, CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipodeActividad] 
 WHERE  estado = 1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]    Script Date: 07/16/2013 14:28:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]
	@IdTipodeActividad INT
AS
BEGIN
 SELECT IdTipodeActividad, CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeActividad] WHERE  IdTipodeActividad = @IdTipodeActividad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]    Script Date: 07/16/2013 14:28:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]
	@Codigo NVARCHAR(64) = NULL, @Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL,
	@LongitudUNSPSC INT = NULL
AS
BEGIN
 SELECT IdTipoCodUNSPSC, Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCodigoUNSPSC] 
 WHERE Codigo LIKE CASE WHEN @Codigo IS NULL THEN Codigo ELSE @Codigo + ''%'' END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + ''%'' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
 AND (CASE WHEN @LongitudUNSPSC IS NOT NULL 
		THEN (CASE WHEN LEN(Codigo)=@LongitudUNSPSC THEN 1 ELSE 0 END) 
		ELSE 1 END)=1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]    Script Date: 07/16/2013 14:28:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]
		@IdTipoCodUNSPSC INT, @Codigo NVARCHAR(64),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCodigoUNSPSC 
	SET Codigo = @Codigo, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdTipoCodUNSPSC = @IdTipoCodUNSPSC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]    Script Date: 07/16/2013 14:28:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]
		@IdTipoCodUNSPSC INT OUTPUT, @Codigo NVARCHAR(64),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCodigoUNSPSC(Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Codigo, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCodUNSPSC = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]    Script Date: 07/16/2013 14:28:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]
	@IdTipoCodUNSPSC INT
AS
BEGIN
 SELECT IdTipoCodUNSPSC, Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCodigoUNSPSC] 
 WHERE  IdTipoCodUNSPSC = @IdTipoCodUNSPSC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]    Script Date: 07/16/2013 14:28:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]
	@CodigoCiiu NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCiiu, CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoCiiu] WHERE CodigoCiiu = CASE WHEN @CodigoCiiu IS NULL THEN CodigoCiiu ELSE @CodigoCiiu END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]    Script Date: 07/16/2013 14:28:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]
		@IdTipoCiiu INT,	@CodigoCiiu NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCiiu SET CodigoCiiu = @CodigoCiiu, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoCiiu = @IdTipoCiiu
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]    Script Date: 07/16/2013 14:28:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]
		@IdTipoCiiu INT OUTPUT, 	@CodigoCiiu NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCiiu(CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoCiiu, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCiiu = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]    Script Date: 07/16/2013 14:28:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]
	@IdTipoCiiu INT
AS
BEGIN
	DELETE Proveedor.TipoCiiu WHERE IdTipoCiiu = @IdTipoCiiu
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]    Script Date: 07/16/2013 14:28:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]
	@IdTipoCiiu INT
AS
BEGIN
 SELECT IdTipoCiiu, CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoCiiu] WHERE  IdTipoCiiu = @IdTipoCiiu
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]    Script Date: 07/16/2013 14:28:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]
	@CodigoTipoCargoEntidad NVARCHAR(128) = NULL, @Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCargoEntidad, CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCargoEntidad] 
 WHERE CodigoTipoCargoEntidad LIKE CASE WHEN @CodigoTipoCargoEntidad IS NULL THEN CodigoTipoCargoEntidad ELSE @CodigoTipoCargoEntidad + ''%'' END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + ''%'' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]    Script Date: 07/16/2013 14:28:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]
		@IdTipoCargoEntidad INT, @CodigoTipoCargoEntidad NVARCHAR(10),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCargoEntidad SET CodigoTipoCargoEntidad = @CodigoTipoCargoEntidad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoCargoEntidad = @IdTipoCargoEntidad
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]    Script Date: 07/16/2013 14:28:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
		@IdTipoCargoEntidad INT OUTPUT, @CodigoTipoCargoEntidad NVARCHAR(10),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCargoEntidad(CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoCargoEntidad,@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCargoEntidad = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]    Script Date: 07/16/2013 14:28:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]
	@IdTipoCargoEntidad INT
AS
BEGIN
	DELETE Proveedor.TipoCargoEntidad 
	WHERE IdTipoCargoEntidad = @IdTipoCargoEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar]    Script Date: 07/16/2013 14:28:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar]
	@IdTipoCargoEntidad INT
AS
BEGIN
 SELECT IdTipoCargoEntidad, CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCargoEntidad] 
 WHERE  IdTipoCargoEntidad = @IdTipoCargoEntidad
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]    Script Date: 07/16/2013 14:28:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]
	@CodigoSectorEntidad NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoSectorEntidad, CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoSectorEntidad] WHERE CodigoSectorEntidad = CASE WHEN @CodigoSectorEntidad IS NULL THEN CodigoSectorEntidad ELSE @CodigoSectorEntidad END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]    Script Date: 07/16/2013 14:28:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]
		@IdTipoSectorEntidad INT,	@CodigoSectorEntidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoSectorEntidad SET CodigoSectorEntidad = @CodigoSectorEntidad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoSectorEntidad = @IdTipoSectorEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]    Script Date: 07/16/2013 14:28:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]
		@IdTipoSectorEntidad INT OUTPUT, 	@CodigoSectorEntidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoSectorEntidad(CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoSectorEntidad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoSectorEntidad = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]    Script Date: 07/16/2013 14:28:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que elimina un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]
	@IdTipoSectorEntidad INT
AS
BEGIN
	DELETE Proveedor.TipoSectorEntidad WHERE IdTipoSectorEntidad = @IdTipoSectorEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]    Script Date: 07/16/2013 14:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]
	AS
BEGIN
 SELECT IdTipoSectorEntidad,Descripcion FROM [Proveedor].[TipoSectorEntidad] WHERE  Estado = 1
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]    Script Date: 07/16/2013 14:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]
	@IdTipoSectorEntidad INT
AS
BEGIN
 SELECT IdTipoSectorEntidad, CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoSectorEntidad] WHERE  IdTipoSectorEntidad = @IdTipoSectorEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]    Script Date: 07/16/2013 14:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]
	@IdTipoPersona INT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE t.IdTipoPersona = @IdTipoPersona 
 END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]    Script Date: 07/16/2013 14:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]
	@CodigoRegimenTributario NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@IdTipoPersona INT = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE CodigoRegimenTributario = CASE WHEN @CodigoRegimenTributario IS NULL THEN CodigoRegimenTributario ELSE @CodigoRegimenTributario END 
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
 AND trt.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN trt.IdTipoPersona ELSE @IdTipoPersona END 
 AND trt.Estado = CASE WHEN @Estado IS NULL THEN trt.Estado ELSE @Estado END
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]    Script Date: 07/16/2013 14:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]
		@IdTipoRegimenTributario INT,	@CodigoRegimenTributario NVARCHAR(128),	@Descripcion NVARCHAR(128),	@IdTipoPersona INT,	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoRegimenTributario SET CodigoRegimenTributario = @CodigoRegimenTributario, Descripcion = @Descripcion, IdTipoPersona = @IdTipoPersona, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoRegimenTributario = @IdTipoRegimenTributario
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]    Script Date: 07/16/2013 14:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]
		@IdTipoRegimenTributario INT OUTPUT, 	@CodigoRegimenTributario NVARCHAR(128),	@Descripcion NVARCHAR(128),	@IdTipoPersona INT,	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoRegimenTributario(CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRegimenTributario, @Descripcion, @IdTipoPersona, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoRegimenTributario = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]    Script Date: 07/16/2013 14:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]
	@IdTipoRegimenTributario INT
AS
BEGIN
	DELETE Proveedor.TipoRegimenTributario WHERE IdTipoRegimenTributario = @IdTipoRegimenTributario
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]    Script Date: 07/16/2013 14:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]
	@IdTipoRegimenTributario INT
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t 
 ON trt.IdTipoPersona = t.IdTipoPersona
 --SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 --FROM [Proveedor].[TipoRegimenTributario] 
 WHERE  trt.IdTipoRegimenTributario = @IdTipoRegimenTributario
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]    Script Date: 07/16/2013 14:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]	
AS
BEGIN
 SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Tipoentidad]
 WHERE Estado=1
 ORDER BY IdTipoentidad ASC
 
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar]    Script Date: 07/16/2013 14:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar]
	@CodigoTipoentidad NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Tipoentidad] WHERE CodigoTipoentidad = CASE WHEN @CodigoTipoentidad IS NULL THEN CodigoTipoentidad ELSE @CodigoTipoentidad END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]    Script Date: 07/16/2013 14:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]
		@IdTipoentidad INT,	@CodigoTipoentidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Tipoentidad SET CodigoTipoentidad = @CodigoTipoentidad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoentidad = @IdTipoentidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]    Script Date: 07/16/2013 14:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]
		@IdTipoentidad INT OUTPUT, 	@CodigoTipoentidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Tipoentidad(CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoentidad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoentidad = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]    Script Date: 07/16/2013 14:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que elimina un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]
	@IdTipoentidad INT
AS
BEGIN
	DELETE Proveedor.Tipoentidad WHERE IdTipoentidad = @IdTipoentidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]    Script Date: 07/16/2013 14:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]
	@IdTipoentidad INT
AS
BEGIN
 SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Tipoentidad] WHERE  IdTipoentidad = @IdTipoentidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]    Script Date: 07/16/2013 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Fabian valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]
	@IdTipoDocumento INT =NULL
AS
BEGIN
	SELECT     Proveedor.TipoDocumento.*
	FROM         Proveedor.TipoDocumento
	WHERE IdTipoDocumento =
	CASE
		WHEN @IdTipoDocumento IS NULL THEN IdTipoDocumento ELSE @IdTipoDocumento
	END
	AND Estado = 1
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]    Script Date: 07/16/2013 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]
	@CodigoDocIdentifica NUMERIC = NULL,@Descripcion NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdTipoDocIdentifica, CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoDocIdentifica] 
 WHERE CodigoDocIdentifica = CASE WHEN @CodigoDocIdentifica IS NULL THEN CodigoDocIdentifica ELSE @CodigoDocIdentifica END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + ''%'' END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]    Script Date: 07/16/2013 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]
		@IdTipoDocIdentifica INT,	@CodigoDocIdentifica NUMERIC,	@Descripcion NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoDocIdentifica 
	SET CodigoDocIdentifica = @CodigoDocIdentifica, Descripcion = @Descripcion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdTipoDocIdentifica = @IdTipoDocIdentifica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]    Script Date: 07/16/2013 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]
		@IdTipoDocIdentifica INT OUTPUT, 	@CodigoDocIdentifica NUMERIC,	@Descripcion NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoDocIdentifica(CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoDocIdentifica, @Descripcion, @UsuarioCrea, GETDATE())
	SELECT @IdTipoDocIdentifica = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]    Script Date: 07/16/2013 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que elimina un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]
	@IdTipoDocIdentifica INT
AS
BEGIN
	DELETE Proveedor.TipoDocIdentifica WHERE IdTipoDocIdentifica = @IdTipoDocIdentifica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]    Script Date: 07/16/2013 14:28:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  26/06/2013 
-- Description:	Procedimiento almacenado que consulta un(a) Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]
@Nombre NVARCHAR (64) = NULL
AS
BEGIN

	SELECT
		IdAcuerdo,
		Nombre,
		ContenidoHtml,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[Acuerdos]
	WHERE Nombre =
		CASE
			WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre
		END 
		
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]    Script Date: 07/16/2013 14:28:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  26/06/2013 
-- Description:	Procedimiento almacenado que guarda un nuevo Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]

@IdAcuerdo INT OUTPUT, 
@Nombre NVARCHAR (64), 
@ContenidoHtml NVARCHAR (MAX), 
@UsuarioCrea NVARCHAR (250)

AS
BEGIN

INSERT INTO Proveedor.Acuerdos (Nombre, ContenidoHtml, UsuarioCrea, FechaCrea)
	VALUES (@Nombre, @ContenidoHtml, @UsuarioCrea, GETDATE())
SELECT
	@IdAcuerdo = @@IDENTITY
	
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]    Script Date: 07/16/2013 14:28:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Saul Rodriguez
-- Create date:  27/06/2013 
-- Description:	Procedimiento almacenado que consulta el acuerdo vigente
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]
AS
BEGIN
	SELECT
		IdAcuerdo,
		Nombre,
		ContenidoHtml,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[Acuerdos]
	WHERE IdAcuerdo = (SELECT MAX(IdAcuerdo) FROM [Proveedor].[Acuerdos])
	
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]    Script Date: 07/16/2013 14:28:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  27/06/2013 
-- Description:	Procedimiento almacenado que consulta un(a) Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]
@IdAcuerdo INT
AS
BEGIN
SELECT
	IdAcuerdo,
	Nombre,
	ContenidoHtml,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[Acuerdos]
WHERE IdAcuerdo = @IdAcuerdo
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]    Script Date: 07/16/2013 14:28:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]
		@IdContacto INT,	@IdEntidad INT, @IdSucursal INT, @IdTelContacto INT,	@IdTipoDocIdentifica INT,	@IdTipoCargoEntidad INT,	@NumeroIdentificacion NUMERIC(30),	@PrimerNombre NVARCHAR(128),	@SegundoNombre NVARCHAR(128),	@PrimerApellido NVARCHAR(128),	@SegundoApellido NVARCHAR(128),	@Dependencia NVARCHAR(128),	@Email NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.ContactoEntidad 
	SET IdEntidad = @IdEntidad, IdSucursal = @IdSucursal, IdTelContacto = @IdTelContacto, IdTipoDocIdentifica = @IdTipoDocIdentifica, IdTipoCargoEntidad = @IdTipoCargoEntidad, NumeroIdentificacion = @NumeroIdentificacion, PrimerNombre = @PrimerNombre, SegundoNombre = @SegundoNombre, PrimerApellido = @PrimerApellido, SegundoApellido = @SegundoApellido, Dependencia = @Dependencia, Email = @Email, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdContacto = @IdContacto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]    Script Date: 07/16/2013 14:28:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]
		@IdContacto INT OUTPUT, 	@IdEntidad INT,	@IdSucursal INT, @IdTelContacto INT,	@IdTipoDocIdentifica INT,	
		@IdTipoCargoEntidad INT,	@NumeroIdentificacion NUMERIC(30),	@PrimerNombre NVARCHAR(128),	@SegundoNombre NVARCHAR(128),	
		@PrimerApellido NVARCHAR(128),	@SegundoApellido NVARCHAR(128),	@Dependencia NVARCHAR(128),	@Email NVARCHAR(128),	@Estado BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ContactoEntidad(IdEntidad, IdSucursal, IdTelContacto, IdTipoDocIdentifica, IdTipoCargoEntidad, NumeroIdentificacion, PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido, Dependencia, Email, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @IdSucursal, @IdTelContacto, @IdTipoDocIdentifica, @IdTipoCargoEntidad, @NumeroIdentificacion, @PrimerNombre, @SegundoNombre, @PrimerApellido, @SegundoApellido, @Dependencia, @Email, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdContacto = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]    Script Date: 07/16/2013 14:28:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que elimina un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]
	@IdContacto INT
AS
BEGIN
	DELETE Proveedor.ContactoEntidad WHERE IdContacto = @IdContacto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]    Script Date: 07/16/2013 14:28:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]
	@IdTipodeActividad INT = NULL
AS
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[ClasedeEntidad] 
 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END AND Estado = 1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]    Script Date: 07/16/2013 14:28:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]
		@IdTipodeActividad INT = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT Proveedor.ClasedeEntidad.IdClasedeEntidad, Proveedor.ClasedeEntidad.IdTipodeActividad, Proveedor.ClasedeEntidad.Descripcion, Proveedor.ClasedeEntidad.Estado, Proveedor.ClasedeEntidad.UsuarioCrea, Proveedor.ClasedeEntidad.FechaCrea, Proveedor.ClasedeEntidad.UsuarioModifica, Proveedor.ClasedeEntidad.FechaModifica 
 ,Proveedor.TipodeActividad.Descripcion AS TipodeActividad
 FROM [Proveedor].[ClasedeEntidad] 
 INNER JOIN Proveedor.TipodeActividad ON Proveedor.TipodeActividad.IdTipodeActividad = Proveedor.ClasedeEntidad.IdTipodeActividad
 WHERE Proveedor.ClasedeEntidad.IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN Proveedor.ClasedeEntidad.IdTipodeActividad ELSE @IdTipodeActividad END AND Proveedor.ClasedeEntidad.Descripcion = CASE WHEN @Descripcion IS NULL THEN Proveedor.ClasedeEntidad.Descripcion ELSE @Descripcion END AND Proveedor.ClasedeEntidad.Estado = CASE WHEN @Estado IS NULL THEN Proveedor.ClasedeEntidad.Estado ELSE @Estado END
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Modificar]    Script Date: 07/16/2013 14:28:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Modificar]
		@IdClasedeEntidad INT,	@IdTipodeActividad INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.ClasedeEntidad SET IdTipodeActividad = @IdTipodeActividad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdClasedeEntidad = @IdClasedeEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]    Script Date: 07/16/2013 14:28:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]
		@IdClasedeEntidad INT OUTPUT, 	@IdTipodeActividad INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ClasedeEntidad(IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipodeActividad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdClasedeEntidad = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]    Script Date: 07/16/2013 14:28:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que elimina un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]
	@IdClasedeEntidad INT
AS
BEGIN
	DELETE Proveedor.ClasedeEntidad WHERE IdClasedeEntidad = @IdClasedeEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]    Script Date: 07/16/2013 14:28:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]
	@IdClasedeEntidad INT
AS
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[ClasedeEntidad] WHERE  IdClasedeEntidad = @IdClasedeEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]    Script Date: 07/16/2013 14:28:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado de InfoExperiencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]
		@IdExpEntidad INT, @IdEstadoInfoExperienciaEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[InfoExperienciaEntidad] 
	SET EstadoDocumental = @IdEstadoInfoExperienciaEntidad, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdExpEntidad = @IdExpEntidad
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]    Script Date: 07/16/2013 14:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Fabian Valencia
-- Create date: 28/06/2013
-- Description:	Obtiene los programas del proveedor
-- =============================================
CREATE PROCEDURE  [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos] 
	@idModulo INT
AS
BEGIN
SELECT     IdPrograma, IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion, VisibleMenu,
                       generaLog, Padre
FROM         SEG.Programa
WHERE     (CodigoPrograma  in ( ''PROVEEDOR/DocFinancieraProv'',''PROVEEDOR/GestionProveedores'',''PROVEEDOR/GestionTercero'', ''PROVEEDOR/INFOEXPERIENCIAENTIDAD'' ) AND
			IdModulo = @idModulo)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]    Script Date: 07/16/2013 14:28:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que elimina un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]
	@IdExpEntidad INT
AS
BEGIN
	DELETE Proveedor.InfoExperienciaEntidad WHERE IdExpEntidad = @IdExpEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]    Script Date: 07/16/2013 14:28:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]
	@IdExpEntidad INT
AS
BEGIN
 SELECT IdExpEntidad, IdEntidad, IdTipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, 
	IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, 
	FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, 
	PorcentParticipacion, AtencionDeptos, JardinOPreJardin, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[InfoExperienciaEntidad] 
 WHERE  IdExpEntidad = @IdExpEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]    Script Date: 07/16/2013 14:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:33:04 PM
-- Description:	Procedimiento almacenado que consulta un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]
	@IdEntidad INT = NULL,
	@EntidadContratante NVARCHAR(128) = NULL,
	@NumeroContrato NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdExpEntidad, IdEntidad, IdTipoSector, TS.Descripcion AS TipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, 
 IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, 
 ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, PorcentParticipacion, AtencionDeptos, JardinOPreJardin, 
 IE.UsuarioCrea, IE.FechaCrea, IE.UsuarioModifica, IE.FechaModifica 
 FROM [Proveedor].[InfoExperienciaEntidad] AS IE
 INNER JOIN [Proveedor].[TipoSectorEntidad] AS TS ON IE.idTipoSector = TS.IdTipoSectorEntidad
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND EntidadContratante = CASE WHEN @EntidadContratante IS NULL THEN EntidadContratante ELSE @EntidadContratante END 
 AND ISNULL(NumeroContrato,0) = CASE WHEN @NumeroContrato IS NULL THEN ISNULL(NumeroContrato,0) ELSE @NumeroContrato END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]    Script Date: 07/16/2013 14:28:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
		@IdExpEntidad INT,	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(128),	@FechaInicio DATETIME,	@FechaFin DATETIME,	@NumeroContrato NVARCHAR(128),	@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(18,3),	@EstadoDocumental INT,	@UnionTempConsorcio BIT,	@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	@JardinOPreJardin BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.InfoExperienciaEntidad 
	SET IdEntidad = @IdEntidad, IdTipoSector = @IdTipoSector, IdTipoEstadoExp = @IdTipoEstadoExp, IdTipoModalidadExp = @IdTipoModalidadExp, IdTipoModalidad = @IdTipoModalidad, IdTipoPoblacionAtendida = @IdTipoPoblacionAtendida, IdTipoRangoExpAcum = @IdTipoRangoExpAcum, IdTipoCodUNSPSC = @IdTipoCodUNSPSC, IdTipoEntidadContratante = @IdTipoEntidadContratante, EntidadContratante = @EntidadContratante, FechaInicio = @FechaInicio, FechaFin = @FechaFin, NumeroContrato = @NumeroContrato, ObjetoContrato = @ObjetoContrato, Vigente = @Vigente, Cuantia = @Cuantia, EstadoDocumental = @EstadoDocumental, UnionTempConsorcio = @UnionTempConsorcio, PorcentParticipacion = @PorcentParticipacion, AtencionDeptos = @AtencionDeptos, JardinOPreJardin = @JardinOPreJardin, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdExpEntidad = @IdExpEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]    Script Date: 07/16/2013 14:28:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]
	@IDTIPODOCIDENTIFICA int = NULL,
	@IDESTADOTERCERO int = NULL,
	@IdTipoPersona int = NULL,
	@NUMEROIDENTIFICACION varchar(30) = NULL,
	@USUARIOCREA varchar(128) = NULL,
	@Tercero varchar(128) = NULL,
	@FECHACREA datetime = NULL
	
AS
BEGIN
	SELECT       Oferente.TERCERO.IDTERCERO, 
				 Oferente.TERCERO.IDTIPODOCIDENTIFICA, 
				 Oferente.TERCERO.IDESTADOTERCERO, 
				 Oferente.TERCERO.IdTipoPersona, 
                 Oferente.TERCERO.NUMEROIDENTIFICACION, 
                 Oferente.TERCERO.DIGITOVERIFICACION, 
                 Oferente.TERCERO.CORREOELECTRONICO, 
                 Oferente.TERCERO.PRIMERNOMBRE, 
                 Oferente.TERCERO.SEGUNDONOMBRE, 
                 Oferente.TERCERO.PRIMERAPELLIDO, 
                 Oferente.TERCERO.SEGUNDOAPELLIDO, 
                 Oferente.TERCERO.RAZONSOCIAL, 
                 Oferente.TERCERO.FECHAEXPEDICIONID, 
                 Oferente.TERCERO.FECHANACIMIENTO, 
                 Oferente.TERCERO.SEXO, 
                 Oferente.TERCERO.FECHACREA, 
                 Oferente.TERCERO.USUARIOCREA, 
                 Oferente.TERCERO.FECHAMODIFICA, 
                 Oferente.TERCERO.USUARIOMODIFICA, 
                 Global.TiposDocumentos.NomTipoDocumento AS NombreTipoIdentificacionPersonaNatural, 
                 Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural, 
                 Oferente.EstadoTercero.CodigoEstadotercero, 
                 Oferente.EstadoTercero.DescripcionEstado, 
                 Oferente.TipoPersona.CodigoTipoPersona, 
                 Oferente.TipoPersona.NombreTipoPersona
	FROM         Oferente.TERCERO 
				 INNER JOIN Global.TiposDocumentos  ON 
				 Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento 
				 INNER JOIN Oferente.EstadoTercero ON 
				 Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero 
				 INNER JOIN Oferente.TipoPersona ON 
				 Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
	WHERE 
				Oferente.TERCERO.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END
				AND Oferente.TERCERO.IDESTADOTERCERO = CASE WHEN @IDESTADOTERCERO IS NULL THEN Oferente.TERCERO.IDESTADOTERCERO ELSE @IDESTADOTERCERO END
				AND Oferente.TERCERO.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona END 
				AND Oferente.TERCERO.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END 
				AND Oferente.TERCERO.USUARIOCREA = CASE WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA END 
				AND (
					Oferente.TERCERO.PRIMERNOMBRE LIKE CASE WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERNOMBRE ELSE  ''%''+ @Tercero +''%'' END
					OR Oferente.TERCERO.SEGUNDONOMBRE  LIKE CASE WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDONOMBRE ELSE  ''%''+ @Tercero +''%'' END
					OR Oferente.TERCERO.PRIMERAPELLIDO LIKE CASE WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERAPELLIDO ELSE  ''%''+ @Tercero +''%'' END
					OR Oferente.TERCERO.SEGUNDOAPELLIDO LIKE CASE WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDOAPELLIDO ELSE  ''%''+ @Tercero +''%'' END
					OR Oferente.TERCERO.RAZONSOCIAL  LIKE CASE WHEN @Tercero IS NULL THEN Oferente.TERCERO.RAZONSOCIAL ELSE  ''%''+ @Tercero +''%'' END 
					)
				AND CAST(Oferente.TERCERO.FECHACREA AS date) = CASE WHEN @FECHACREA IS NULL THEN CAST(Oferente.TERCERO.FECHACREA AS date) ELSE cast(@FECHACREA AS date) END 
		
		
	
     
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]    Script Date: 07/16/2013 14:28:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]
		@IdTercero INT, @IdEstadoTercero INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Oferente.Tercero 
	SET IdEstadoTercero = @IdEstadoTercero, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdTercero = @IdTercero
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]    Script Date: 07/16/2013 14:28:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]
		@IdValidarTercero INT OUTPUT, 	@IdTercero INT,	@Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarTercero(IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdTercero, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarTercero = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]    Script Date: 07/16/2013 14:28:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarTercero por IdTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]
	@IdTercero INT
AS
BEGIN
 SELECT TOP 1 IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarTercero] 
 WHERE  IdTercero = @IdTercero
 ORDER BY IdValidarTercero DESC
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]    Script Date: 07/16/2013 14:28:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]
	@IdValidarTercero INT
AS
BEGIN
 SELECT IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarTercero] WHERE  IdValidarTercero = @IdValidarTercero
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]    Script Date: 07/16/2013 14:29:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]
	@IdTercero int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarTercero] 
 WHERE IdTercero = CASE WHEN @IdTercero IS NULL THEN IdTercero ELSE @IdTercero END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
 ORDER BY 1 DESC
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]    Script Date: 07/16/2013 14:28:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]
	@IdExpEntidad int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoExperienciaEntidad] 
 WHERE IdExpEntidad = CASE WHEN @IdExpEntidad IS NULL THEN IdExpEntidad ELSE @IdExpEntidad END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]    Script Date: 07/16/2013 14:28:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]
	@IdExpEntidad INT = NULL
AS
BEGIN
 SELECT IdExpCOD, E.IdTipoCodUNSPSC, IdExpEntidad, E.UsuarioCrea, E.FechaCrea, E.UsuarioModifica, E.FechaModifica,
	T.Codigo, T.Descripcion 
 FROM [Proveedor].[ExperienciaCodUNSPSCEntidad] E
 INNER JOIN [Proveedor].[TipoCodigoUNSPSC] T ON E.IdTipoCodUNSPSC=T.IdTipoCodUNSPSC
 WHERE IdExpEntidad = ISNULL(@IdExpEntidad, IdExpEntidad)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]    Script Date: 07/16/2013 14:28:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:19 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]
		@IdExpCOD INT,	@IdTipoCodUNSPSC INT,	@IdExpEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.ExperienciaCodUNSPSCEntidad SET IdTipoCodUNSPSC = @IdTipoCodUNSPSC, IdExpEntidad = @IdExpEntidad, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdExpCOD = @IdExpCOD
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]    Script Date: 07/16/2013 14:28:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:19 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]
		@IdExpCOD INT OUTPUT, 	@IdTipoCodUNSPSC INT,	@IdExpEntidad INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ExperienciaCodUNSPSCEntidad(IdTipoCodUNSPSC, IdExpEntidad, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoCodUNSPSC, @IdExpEntidad, @UsuarioCrea, GETDATE())
	SELECT @IdExpCOD = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]    Script Date: 07/16/2013 14:28:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que elimina un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]
	@IdExpCOD INT
AS
BEGIN
	DELETE Proveedor.ExperienciaCodUNSPSCEntidad WHERE IdExpCOD = @IdExpCOD
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]    Script Date: 07/16/2013 14:28:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]
	@IdExpCOD INT
AS
BEGIN
 SELECT IdExpCOD, IdTipoCodUNSPSC, IdExpEntidad, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[ExperienciaCodUNSPSCEntidad] WHERE  IdExpCOD = @IdExpCOD
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]    Script Date: 07/16/2013 14:28:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]
		@IdExpEntidad INT OUTPUT, 	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(128),	
		@FechaInicio DATETIME,	@FechaFin DATETIME, @ExperienciaMeses NUMERIC(18,2),	@NumeroContrato NVARCHAR(128),	@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(18,3),	@EstadoDocumental INT,	@UnionTempConsorcio BIT,	@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	@JardinOPreJardin BIT, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20) = NULL
AS
BEGIN
	INSERT INTO Proveedor.InfoExperienciaEntidad(IdEntidad, IdTipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, 
		FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, PorcentParticipacion, AtencionDeptos, JardinOPreJardin, UsuarioCrea, FechaCrea)
	VALUES(@IdEntidad, @IdTipoSector, @IdTipoEstadoExp, @IdTipoModalidadExp, @IdTipoModalidad, @IdTipoPoblacionAtendida, @IdTipoRangoExpAcum, @IdTipoCodUNSPSC, @IdTipoEntidadContratante, @EntidadContratante, 
					  @FechaInicio, @FechaFin, @ExperienciaMeses, @NumeroContrato, @ObjetoContrato, @Vigente, @Cuantia, @EstadoDocumental, @UnionTempConsorcio, @PorcentParticipacion, @AtencionDeptos, @JardinOPreJardin, @UsuarioCrea, GETDATE())
	
	SELECT @IdExpEntidad = @@IDENTITY
					  
	UPDATE [Proveedor].[DocExperienciaEntidad] 
	SET IdExpEntidad = @IdExpEntidad
	WHERE IdTemporal = @IdTemporal
					  
	SELECT @IdExpEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 07/16/2013 14:28:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que actualiza un(a) EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
		@IdEntidad INT,	
		@TipoEntOfProv BIT= NULL,	
		@IdTercero INT= NULL,	
		@IdTipoCiiuPrincipal INT = NULL,	
		@IdTipoCiiuSecundario INT= NULL,	
		@IdTipoSector INT = NULL,	
		@IdTipoClaseEntidad INT = NULL,	
		@IdTipoRamaPublica INT = NULL,	
		@IdTipoNivelGob INT = NULL,	
		@IdTipoNivelOrganizacional INT = NULL,	
		@IdTipoCertificadorCalidad INT = NULL,	
		@FechaCiiuPrincipal DATETIME = NULL,	
		@FechaCiiuSecundario DATETIME = NULL,	
		@FechaConstitucion DATETIME = NULL,	
		@FechaVigencia DATETIME = NULL,	
		@FechaMatriculaMerc DATETIME = NULL,	
		@FechaExpiracion DATETIME = NULL,	
		@TipoVigencia BIT = NULL,	
		@ExenMatriculaMer BIT = NULL,	
		@MatriculaMercantil NVARCHAR(20) = NULL,	
		@ObserValidador NVARCHAR(256) = NULL,	
		@AnoRegistro INT = NULL,	
		@IdEstado INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL
AS
BEGIN
	UPDATE Proveedor.EntidadProvOferente 
			SET TipoEntOfProv = ISNULL(@TipoEntOfProv,TipoEntOfProv) ,
			    IdTercero = ISNULL(@IdTercero,IdTercero), 
			    IdTipoCiiuPrincipal =ISNULL( @IdTipoCiiuPrincipal, IdTipoCiiuPrincipal ),
				IdTipoCiiuSecundario =ISNULL( @IdTipoCiiuSecundario, IdTipoCiiuSecundario ),
				IdTipoSector =ISNULL( @IdTipoSector, IdTipoSector ),
				IdTipoClaseEntidad =ISNULL( @IdTipoClaseEntidad, IdTipoClaseEntidad ),
				IdTipoRamaPublica =ISNULL( @IdTipoRamaPublica, IdTipoRamaPublica ),
				IdTipoNivelGob =ISNULL( @IdTipoNivelGob, IdTipoNivelGob ),
				IdTipoNivelOrganizacional =ISNULL( @IdTipoNivelOrganizacional, IdTipoNivelOrganizacional ),
				IdTipoCertificadorCalidad =ISNULL( @IdTipoCertificadorCalidad, IdTipoCertificadorCalidad ),
				FechaCiiuPrincipal =ISNULL( @FechaCiiuPrincipal, FechaCiiuPrincipal ),
				FechaCiiuSecundario =ISNULL( @FechaCiiuSecundario, FechaCiiuSecundario ),
				FechaConstitucion =ISNULL( @FechaConstitucion, FechaConstitucion ),
				FechaVigencia =ISNULL( @FechaVigencia, FechaVigencia ),
				FechaMatriculaMerc =ISNULL( @FechaMatriculaMerc, FechaMatriculaMerc ),
				FechaExpiracion =ISNULL( @FechaExpiracion, FechaExpiracion ),
				TipoVigencia =ISNULL( @TipoVigencia, TipoVigencia ),
				ExenMatriculaMer =ISNULL( @ExenMatriculaMer, ExenMatriculaMer ),
				MatriculaMercantil =ISNULL( @MatriculaMercantil, MatriculaMercantil ),
				ObserValidador =ISNULL( @ObserValidador, ObserValidador ),
				AnoRegistro =ISNULL( @AnoRegistro, AnoRegistro ),
				IdEstado =ISNULL( @IdEstado, IdEstado ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE() 
			   WHERE IdEntidad = @IdEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 07/16/2013 14:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT [IdEntidad]
      ,[ConsecutivoInterno]
      ,[TipoEntOfProv]
      ,[IdTercero]
      ,[IdTipoCiiuPrincipal]
      ,[IdTipoCiiuSecundario]
      ,[IdTipoSector]
      ,[IdTipoClaseEntidad]
      ,[IdTipoRamaPublica]
      ,[IdTipoNivelGob]
      ,[IdTipoNivelOrganizacional]
      ,[IdTipoCertificadorCalidad]
      ,[FechaCiiuPrincipal]
      ,[FechaCiiuSecundario]
      ,[FechaConstitucion]
      ,[FechaVigencia]
      ,[FechaMatriculaMerc]
      ,[FechaExpiracion]
      ,[TipoVigencia]
      ,[ExenMatriculaMer]
      ,[MatriculaMercantil]
      ,[ObserValidador]
      ,[AnoRegistro]
      ,[IdEstado]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
      ,[IdAcuerdo]
 FROM [Proveedor].[EntidadProvOferente] WHERE  IdEntidad = @IdEntidad
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]    Script Date: 07/16/2013 14:28:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero y los proveedores que se hayan cambiado el estado y envian un correo a los gestores
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]

@IdTemporal nvarchar (20),
@Tercero varchar (30)
AS
BEGIN
--DECLARE @Tercero varchar(30), 
DECLARE @Asunto varchar (100), @Email_Destino varchar (max)
SET @Email_Destino = ''fvalencia@innovasoft.com.co''
--SET @Tercero = ''Tercero''
SET @Asunto = ''Validar '' + @Tercero
Declare @Body varchar (max),

@TableHead varchar (max),
@TableTail varchar (max)
SET NOCOUNT ON;
SET @TableTail = ''</table></body></html>'';
SET @TableHead = ''<html><head>'' +
				 ''<style>'' +
				 ''td {border: solid black 1px;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;font-size:11pt;} '' +
				 ''</style>'' +
				 ''</head>'' +
				 ''<body><table cellpadding=0 cellspacing=0 border=0>'' +
				 ''<tr bgcolor=#ccbfac><td align=center><b>Tipo Identificaci&oacute;n</b></td>'' +
				 ''<td align=center><b>N&uacute;mero Identificaci&oacute;n</b></td>'' +
				 ''<td align=center><b>'' + @Tercero + ''</b></td>'';

SELECT
	@Body = (SELECT
		ROW_NUMBER() OVER (ORDER BY Global.TiposDocumentos.CodDocumento) % 2 AS [TRRow],
		Global.TiposDocumentos.CodDocumento AS [TD],
		Oferente.TERCERO.NUMEROIDENTIFICACION AS [TD],
		(Oferente.TERCERO.PRIMERNOMBRE + '' '' + Oferente.TERCERO.SEGUNDONOMBRE + '' '' + Oferente.TERCERO.PRIMERAPELLIDO + '' '' + Oferente.TERCERO.SEGUNDOAPELLIDO + '' '' + Oferente.TERCERO.RAZONSOCIAL) AS [TD]
	FROM Oferente.TERCERO
	INNER JOIN Global.TiposDocumentos
		ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
	INNER JOIN Oferente.EstadoTercero
		ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
	INNER JOIN Oferente.TipoPersona
		ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
	INNER JOIN Proveedor.MotivoCambioEstado
		ON Oferente.TERCERO.IDTERCERO = Proveedor.MotivoCambioEstado.IdTercero
	WHERE Proveedor.MotivoCambioEstado.IdTemporal = ''1''
	ORDER BY Global.TiposDocumentos.CodDocumento
	FOR xml RAW (''tr''), ELEMENTS)

-- Replace the entity codes and row numbers
SET @Body = REPLACE(@Body, ''_x0020_'', SPACE(1))
SET @Body = REPLACE(@Body, ''_x003D_'', ''='')
SET @Body = REPLACE(@Body, ''<tr><TRRow>1</TRRow>'', ''<tr bgcolor=#e5d7c2>'')
SET @Body = REPLACE(@Body, ''<TRRow>0</TRRow>'', '''')

SELECT
	@Body = @TableHead + @Body + @TableTail



-- Enviar correo
EXEC msdb.dbo.sp_send_dbmail	@profile_name = ''GonetMail'', --colocar perfil que se tenga configurado
								@recipients = @Email_Destino, --destinatario
								@subject = @Asunto, --asunto
								@body = @Body, --cuerpo de correo
								@body_format = ''HTML''; --formato de correo


END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]    Script Date: 07/16/2013 14:28:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero con su peso y formato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]
@IdTercero INT = NULL,
@CodigoDocumento NVARCHAR (20) ,
@Programa NVARCHAR (50)
AS
BEGIN


DECLARE @IdPrograma INT
SET @IdPrograma = (SELECT DISTINCT
	IdPrograma
FROM SEG.Programa
WHERE CodigoPrograma = @Programa)




IF @IdTercero IS NOT NULL
BEGIN
SELECT
	Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	Proveedor.DocAdjuntoTercero.IDDOCADJUNTO,
	Proveedor.DocAdjuntoTercero.IDTERCERO,
	Proveedor.DocAdjuntoTercero.IDDOCUMENTO,
	Proveedor.DocAdjuntoTercero.DESCRIPCION AS Descripcion,
	Proveedor.DocAdjuntoTercero.LINKDOCUMENTO,
	Proveedor.DocAdjuntoTercero.ANNO,
	Proveedor.DocAdjuntoTercero.FECHACREA,
	Proveedor.DocAdjuntoTercero.USUARIOCREA,
	Proveedor.DocAdjuntoTercero.FECHAMODIFICA,
	Proveedor.DocAdjuntoTercero.USUARIOMODIFICA
FROM Proveedor.TipoDocumento
INNER JOIN Proveedor.DocAdjuntoTercero
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.DocAdjuntoTercero.IDDOCUMENTO
RIGHT OUTER JOIN Proveedor.TipoDocumentoPrograma
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.TipoDocumentoPrograma.IdTipoDocumento
WHERE (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma) AND (Proveedor.DocAdjuntoTercero.IDTERCERO = @IdTercero) AND
(Proveedor.TipoDocumento.CodigoTipoDocumento = @CodigoDocumento)
END
ELSE
BEGIN
SELECT
	Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento,Proveedor.TipoDocumento.CodigoTipoDocumento,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	0 AS IDDOCADJUNTO,
	0 AS IDTERCERO,
	Proveedor.TipoDocumento.IdTipoDocumento AS IDDOCUMENTO,
	'''' AS DESCRIPCION,
	'''' AS LINKDOCUMENTO,
	0 AS ANNO,
	GETDATE() AS FECHACREA,
	'''' AS USUARIOCREA,
	GETDATE() AS FECHAMODIFICA,
	'''' AS USUARIOMODIFICA
FROM Proveedor.TipoDocumento
RIGHT OUTER JOIN Proveedor.TipoDocumentoPrograma
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.TipoDocumentoPrograma.IdTipoDocumento
WHERE (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma) AND
(Proveedor.TipoDocumento.CodigoTipoDocumento = @CodigoDocumento)
END

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]    Script Date: 07/16/2013 14:28:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona] 
( @IdTercero int, @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''PROVEEDOR/GestionTercero'';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdTercero) as IdTercero,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		--Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdTercero,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             --d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN ''1'' THEN tdp.ObligPersonaNatural 
								WHEN ''2'' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocAdjuntoTercero AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTercero = @IdTercero
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdTercero,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
		--	 '''' as Observaciones,
			 CASE @TipoPersona	WHEN ''1'' THEN tdp.ObligPersonaNatural 
								WHEN ''2'' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '''' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
WHERE Obligatorio  =1 
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona]    Script Date: 07/16/2013 14:28:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona] 
( @IdTemporal varchar(20), @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''PROVEEDOR/GestionTercero'';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdTercero) as IdTercero,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		--Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdTercero,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             --d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN ''1'' THEN tdp.ObligPersonaNatural 
								WHEN ''2'' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocAdjuntoTercero AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTemporal = @IdTemporal
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdTercero,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
		--	 '''' as Observaciones,
			 CASE @TipoPersona	WHEN ''1'' THEN tdp.ObligPersonaNatural 
								WHEN ''2'' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '''' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]    Script Date: 07/16/2013 14:28:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]
	@IdDocAdjunto INT = NULL,
	@IdTercero INT = NULL,
	@IdDocumento INT = NULL
AS
BEGIN

SELECT			      Proveedor.DOCADJUNTOTERCERO.IDDOCADJUNTO, 
					  Proveedor.DOCADJUNTOTERCERO.IDTERCERO, 
					  Proveedor.DOCADJUNTOTERCERO.IDDOCUMENTO, 
                      Proveedor.DOCADJUNTOTERCERO.DESCRIPCION, 
                      Proveedor.DOCADJUNTOTERCERO.LINKDOCUMENTO, 
                      Proveedor.DOCADJUNTOTERCERO.ANNO, 
                      Proveedor.DOCADJUNTOTERCERO.USUARIOCREA, 
                      Proveedor.DOCADJUNTOTERCERO.FECHACREA, 
                      Proveedor.DOCADJUNTOTERCERO.USUARIOMODIFICA, 
                      Proveedor.DOCADJUNTOTERCERO.FECHAMODIFICA, 
                      Proveedor.TipoDocumento.CodigoTipoDocumento, 
                      Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento
FROM				  Proveedor.DOCADJUNTOTERCERO INNER JOIN
                      Proveedor.TipoDocumento ON Proveedor.DOCADJUNTOTERCERO.IDDOCUMENTO = Proveedor.TipoDocumento.IdTipoDocumento
WHERE				  IdDocAdjunto = CASE WHEN @IdDocAdjunto IS NULL THEN IdDocAdjunto ELSE @IdDocAdjunto END AND 
					  IdTercero = CASE WHEN @IdTercero IS NULL THEN IdTercero ELSE @IdTercero END AND IdDocumento = CASE WHEN @IdDocumento IS NULL THEN IdDocumento ELSE @IdDocumento END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]    Script Date: 07/16/2013 14:28:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]
		@IdDocAdjunto INT,	@IdTercero INT,	
		@LinkDocumento NVARCHAR(256),	
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocAdjuntoTercero 
	SET IdTercero = @IdTercero,
	LinkDocumento = @LinkDocumento, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdDocAdjunto = @IdDocAdjunto
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]    Script Date: 07/16/2013 14:28:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]
		@IdTercero INT = NULL, 	@IdDocumento INT , 	@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Anno INT, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	DECLARE @IdDocAdjunto INT
	SET @IdDocAdjunto = 0
	IF (@IdTercero = 0) SET @IdTercero = NULL  
	INSERT INTO Proveedor.DocAdjuntoTercero(IDTERCERO,IDDOCUMENTO, Descripcion, LinkDocumento, Anno, UsuarioCrea, FechaCrea,idtemporal)
					  VALUES(@IdTercero,@IdDocumento, @Descripcion, @LinkDocumento, @Anno, @UsuarioCrea, GETDATE(),@IdTemporal)
	
	SELECT @IdDocAdjunto = @@IDENTITY 		
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]    Script Date: 07/16/2013 14:28:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocAdjuntoTercero WHERE IdDocAdjunto = @IdDocAdjunto 
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]    Script Date: 07/16/2013 14:28:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]
@IdDocAdjunto INT
AS
BEGIN
SELECT
	IdDocAdjunto,
	IdTercero,
	IdDocumento,
	Descripcion,
	LinkDocumento,
	Anno,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[DocAdjuntoTercero]
WHERE IdDocAdjunto = @IdDocAdjunto
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]    Script Date: 07/16/2013 14:28:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]
	@NumeroIdentificacion NUMERIC(30) = NULL,@PrimerNombre NVARCHAR(128) = NULL,@SegundoNombre NVARCHAR(128) = NULL,
	@PrimerApellido NVARCHAR(128) = NULL,@SegundoApellido NVARCHAR(128) = NULL,@Dependencia NVARCHAR(128) = NULL,@Email NVARCHAR(128) = NULL,@Estado BIT = NULL,
	@IdEntidad INT = NULL
AS
BEGIN
 SELECT CE.IdContacto, CE.IdEntidad, CE.IdSucursal, CE.IdTelContacto, CE.IdTipoDocIdentifica, CE.IdTipoCargoEntidad, CE.NumeroIdentificacion, CE.PrimerNombre, 
	CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido, CE.Dependencia, CE.Email, CE.Estado, CE.UsuarioCrea, CE.FechaCrea, CE.UsuarioModifica, CE.FechaModifica,
	TT.Movil AS Celular, ISNULL(TT.IndicativoTelefono, '''') AS IndicativoTelefono, ISNULL(TT.NumeroTelefono, '''') AS NumeroTelefono, ISNULL(TT.ExtensionTelefono, '''') AS ExtensionTelefono,
	TC.Descripcion AS Cargo
 FROM [Proveedor].[ContactoEntidad] AS CE
 LEFT JOIN [Oferente].[TelTerceros] AS TT ON CE.IdTelContacto = TT.IdTelTercero
 LEFT JOIN [Proveedor].[TipoDocIdentifica] AS TD ON CE.IdTipoDocIdentifica = TD.IdTipoDocIdentifica
 LEFT JOIN [Proveedor].[TipoCargoEntidad] AS TC ON CE.IdTipoCargoEntidad = TC.IdTipoCargoEntidad
 WHERE NumeroIdentificacion = CASE WHEN @NumeroIdentificacion IS NULL THEN NumeroIdentificacion ELSE @NumeroIdentificacion END 
 AND IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND PrimerNombre = CASE WHEN @PrimerNombre IS NULL THEN PrimerNombre ELSE @PrimerNombre END 
 AND SegundoNombre = CASE WHEN @SegundoNombre IS NULL THEN SegundoNombre ELSE @SegundoNombre END 
 AND PrimerApellido = CASE WHEN @PrimerApellido IS NULL THEN PrimerApellido ELSE @PrimerApellido END 
 AND SegundoApellido = CASE WHEN @SegundoApellido IS NULL THEN SegundoApellido ELSE @SegundoApellido END 
 AND Dependencia = CASE WHEN @Dependencia IS NULL THEN Dependencia ELSE @Dependencia END 
 AND Email = CASE WHEN @Email IS NULL THEN Email ELSE @Email END 
 AND CE.Estado = CASE WHEN @Estado IS NULL THEN CE.Estado ELSE @Estado END
 ORDER BY CE.PrimerNombre, CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido DESC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]    Script Date: 07/16/2013 14:28:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]
	@IdExpEntidad INT = NULL,
	@Descripcion NVARCHAR(128) = NULL,
	@LinkDocumento NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdExpEntidad, IdTipoDocumento, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[DocExperienciaEntidad] 
 WHERE IdExpEntidad = CASE WHEN @IdExpEntidad IS NULL THEN IdExpEntidad ELSE @IdExpEntidad END 
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
 AND LinkDocumento = CASE WHEN @LinkDocumento IS NULL THEN LinkDocumento ELSE @LinkDocumento END
END
' 
END
GO
--/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]    Script Date: 07/16/2013 14:28:33 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
--BEGIN
--EXEC dbo.sp_executesql @statement = N'-- =============================================
---- Author:		IIS APPPOOL\PoolGenerador
---- Create date:  6/22/2013 10:21:28 PM
---- Description:	Procedimiento almacenado que actualiza un(a) DocExperienciaEntidad
---- =============================================
--CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]
--		@IdDocAdjunto INT,	@IdExpEntidad INT,	@IdTipoDocGrupo INT,	@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
--AS
--BEGIN
--	UPDATE Proveedor.DocExperienciaEntidad 
--	SET IdExpEntidad = @IdExpEntidad, IdTipoDocGrupo = @IdTipoDocGrupo, Descripcion = @Descripcion, LinkDocumento = @LinkDocumento, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
--	WHERE IdDocAdjunto = @IdDocAdjunto
--END
--' 
--END
--GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]    Script Date: 07/16/2013 14:28:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdExpEntidad INT = NULL,	@IdTipoDocumento INT,	
		@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256), @UsuarioCrea NVARCHAR(250),
		@IdTemporal NVARCHAR(50)
AS
BEGIN
	INSERT INTO Proveedor.DocExperienciaEntidad(IdExpEntidad, IdTipoDocumento, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdExpEntidad, @IdTipoDocumento, @Descripcion, @LinkDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]    Script Date: 07/16/2013 14:28:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocExperienciaEntidad WHERE IdDocAdjunto = @IdDocAdjunto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]    Script Date: 07/16/2013 14:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan NIño
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta una lista de DocExperienciaEntidad por IdTemporal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup] 
( 
	@IdTemporal varchar(20), 
	@RupRenovado varchar(128)=NULL
)
AS
DECLARE @IdPrograma int;
SELECT @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''PROVEEDOR/INFOEXPERIENCIAENTIDAD'';


SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdExpEntidad) as IdExpEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Descripcion) as Descripcion,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdExpEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Descripcion,
			 CASE @RupRenovado	WHEN ''1'' THEN tdp.ObligRupRenovado 
								WHEN ''0'' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocExperienciaEntidad AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION EPERIENCIA
		AND IdTemporal = @IdTemporal
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdExpEntidad,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
			 '''' as Descripcion,
			 CASE @RupRenovado	WHEN ''1'' THEN tdp.ObligRupRenovado 
						WHEN ''0'' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '''' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION EXPERIENCIA
) as Tabla
--Where Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]    Script Date: 07/16/2013 14:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/13/2013 08:10:55 PM
-- Description:	Procedimiento almacenado que consulta una lista de DocExperienciaEntidad por IdInfoExperiencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup] 
( 
	@IdExpEntidad INT, 
	@RupRenovado VARCHAR(128) = NULL
)
AS

DECLARE @IdPrograma INT;
SELECT @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''PROVEEDOR/INFOEXPERIENCIAENTIDAD'';


SELECT	MAX(IdDocAdjunto) AS IdDocAdjunto,
		MAX(IdExpEntidad) AS IdExpEntidad,
		NombreDocumento,
		Max(LinkDocumento) AS LinkDocumento,
		Max(Descripcion) AS Descripcion,
		Max(Obligatorio) AS Obligatorio,
		Max(UsuarioCrea) AS UsuarioCrea,
		Max(FechaCrea) AS FechaCrea,
		Max(UsuarioModifica) AS UsuarioModifica,
		Max(FechaModifica) AS FechaModifica,
		Max(IdTipoDocumento) AS IdTipoDocumento,
		Max(MaxPermitidoKB) AS MaxPermitidoKB,
		Max(ExtensionesPermitidas) AS ExtensionesPermitidas,
		Max(IdTemporal) AS IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdExpEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Descripcion,
			 CASE @RupRenovado	WHEN ''1'' THEN tdp.ObligRupRenovado 
								WHEN ''0'' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocExperienciaEntidad AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION EXPERIENCIA
		AND IdExpEntidad = @IdExpEntidad
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdInfoFin,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
			 '''' as Observaciones,
			 CASE @RupRenovado	WHEN ''1'' THEN tdp.ObligRupRenovado 
						WHEN ''0'' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '''' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION EXPERIENCIA
) as Tabla
--WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar]    Script Date: 07/16/2013 14:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar]
	@IdDocAdjunto INT
AS
BEGIN
 SELECT IdDocAdjunto, IdExpEntidad, IdTipoDocumento, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[DocExperienciaEntidad] 
 WHERE  IdDocAdjunto = @IdDocAdjunto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]    Script Date: 07/16/2013 14:28:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero y los proveedores que tengan estado validado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]

@isTercero int = NULL,
@IDTIPODOCIDENTIFICA int = NULL,
@IdTipoPersona int = NULL,
@NUMEROIDENTIFICACION varchar (30) = NULL,
@USUARIOCREA varchar (128) = NULL,
@Tercero varchar (128) = NULL
AS
BEGIN
IF @isTercero = 1
BEGIN
		SELECT DISTINCT
			Oferente.TERCERO.IDTERCERO,
			Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural,
			Oferente.TERCERO.NUMEROIDENTIFICACION,
			Oferente.TERCERO.PRIMERNOMBRE,
			Oferente.TERCERO.SEGUNDONOMBRE,
			Oferente.TERCERO.PRIMERAPELLIDO,
			Oferente.TERCERO.SEGUNDOAPELLIDO,
			Oferente.TERCERO.RAZONSOCIAL ,
			Oferente.TERCERO.FECHACREA,
			Oferente.EstadoTercero.CodigoEstadotercero,
			Oferente.EstadoTercero.DescripcionEstado
		FROM Oferente.TERCERO
		INNER JOIN Global.TiposDocumentos
			ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
		INNER JOIN Oferente.EstadoTercero
			ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
		INNER JOIN Oferente.TipoPersona
			ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
		WHERE Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END
		AND Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END
		AND Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END
		AND Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END
		AND (
		Oferente.TERCERO.PRIMERNOMBRE LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERNOMBRE ELSE ''%'' + @Tercero + ''%''
		END
		OR Oferente.TERCERO.SEGUNDONOMBRE LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDONOMBRE ELSE ''%'' + @Tercero + ''%''
		END
		OR Oferente.TERCERO.PRIMERAPELLIDO LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERAPELLIDO ELSE ''%'' + @Tercero + ''%''
		END
		OR Oferente.TERCERO.SEGUNDOAPELLIDO LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDOAPELLIDO ELSE ''%'' + @Tercero + ''%''
		END
		OR Oferente.TERCERO.RAZONSOCIAL LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.RAZONSOCIAL ELSE ''%'' + @Tercero + ''%''
		END
		)
		AND Oferente.EstadoTercero.CodigoEstadotercero = ''004'' --Estado validado
END
ELSE
BEGIN
		SELECT DISTINCT
			Oferente.TERCERO.IDTERCERO,
			Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural,
			Oferente.TERCERO.NUMEROIDENTIFICACION,
			Oferente.TERCERO.PRIMERNOMBRE,
			Oferente.TERCERO.SEGUNDONOMBRE,
			Oferente.TERCERO.PRIMERAPELLIDO,
			Oferente.TERCERO.SEGUNDOAPELLIDO,
			Oferente.TERCERO.RAZONSOCIAL ,
			Oferente.TERCERO.FECHACREA,
			Oferente.EstadoTercero.CodigoEstadotercero,
			Oferente.EstadoTercero.DescripcionEstado
		FROM Oferente.TERCERO
		INNER JOIN Global.TiposDocumentos
			ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
		INNER JOIN Oferente.EstadoTercero
			ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
		INNER JOIN Oferente.TipoPersona
			ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
		INNER JOIN Proveedor.EntidadProvOferente
			ON Oferente.TERCERO.IDTERCERO = Proveedor.EntidadProvOferente.IdTercero
		WHERE (Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.PRIMERNOMBRE LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERNOMBRE ELSE ''%'' + @Tercero + ''%''
		END) OR
		(Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.SEGUNDONOMBRE LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDONOMBRE ELSE ''%'' + @Tercero + ''%''
		END) OR
		(Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.PRIMERAPELLIDO LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERAPELLIDO ELSE ''%'' + @Tercero + ''%''
		END) OR
		(Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.SEGUNDOAPELLIDO LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDOAPELLIDO ELSE ''%'' + @Tercero + ''%''
		END) OR
		(Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.RAZONSOCIAL LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.RAZONSOCIAL ELSE ''%'' + @Tercero + ''%''
		END)
		AND Oferente.EstadoTercero.CodigoEstadotercero = ''004''   --Estado validado
END

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]    Script Date: 07/16/2013 14:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]
@IdTipoDocumentoPrograma INT = NULL, @IdTipoDocumento INT = NULL, @IdPrograma INT = NULL
AS
BEGIN
SELECT
	Proveedor.TipoDocumentoPrograma.IdTipoDocumentoPrograma,
	Proveedor.TipoDocumentoPrograma.IdTipoDocumento,
	Proveedor.TipoDocumentoPrograma.IdPrograma,
	Proveedor.TipoDocumentoPrograma.Estado,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	Proveedor.TipoDocumentoPrograma.ObligRupNoRenovado,
	Proveedor.TipoDocumentoPrograma.ObligRupRenovado,
	Proveedor.TipoDocumentoPrograma.ObligPersonaJuridica,
	Proveedor.TipoDocumentoPrograma.ObligPersonaNatural,
	Proveedor.TipoDocumentoPrograma.UsuarioCrea,
	Proveedor.TipoDocumentoPrograma.FechaCrea,
	Proveedor.TipoDocumentoPrograma.UsuarioModifica,
	Proveedor.TipoDocumentoPrograma.FechaModifica,
	Proveedor.TipoDocumento.CodigoTipoDocumento,
	Proveedor.TipoDocumento.Descripcion,
	SEG.Programa.CodigoPrograma,
	SEG.Programa.NombrePrograma
FROM Proveedor.TipoDocumentoPrograma
INNER JOIN Proveedor.TipoDocumento
	ON Proveedor.TipoDocumentoPrograma.IdTipoDocumento = Proveedor.TipoDocumento.IdTipoDocumento
INNER JOIN SEG.Programa
	ON Proveedor.TipoDocumentoPrograma.IdPrograma = SEG.Programa.IdPrograma 
WHERE    IdTipoDocumentoPrograma =
	CASE
		WHEN @IdTipoDocumentoPrograma IS NULL THEN IdTipoDocumentoPrograma ELSE @IdTipoDocumentoPrograma
END AND Proveedor.TipoDocumentoPrograma.IdTipoDocumento =
	CASE
		WHEN @IdTipoDocumento IS NULL THEN Proveedor.TipoDocumentoPrograma.IdTipoDocumento ELSE @IdTipoDocumento
END AND Proveedor.TipoDocumentoPrograma.IdPrograma =
	CASE
		WHEN @IdPrograma IS NULL THEN Proveedor.TipoDocumentoPrograma.IdPrograma ELSE @IdPrograma
END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]    Script Date: 07/16/2013 14:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]
@IdTipoDocumentoPrograma INT, @IdTipoDocumento INT, @IdPrograma INT, @Estado INT, @MaxPermitidoKB INT, @ExtensionesPermitidas NVARCHAR (50), @ObligRupNoRenovado INT, @ObligRupRenovado INT, @ObligPersonaJuridica INT, @ObligPersonaNatural INT, @UsuarioModifica NVARCHAR (250)
AS
BEGIN
UPDATE Proveedor.TipoDocumentoPrograma
SET	IdTipoDocumento = @IdTipoDocumento,
	IdPrograma = @IdPrograma,
	Estado = @Estado,
	MaxPermitidoKB = @MaxPermitidoKB,
	ExtensionesPermitidas = @ExtensionesPermitidas,
	ObligRupNoRenovado = @ObligRupNoRenovado,
	ObligRupRenovado = @ObligRupRenovado,
	ObligPersonaJuridica = @ObligPersonaJuridica,
	ObligPersonaNatural = @ObligPersonaNatural,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]    Script Date: 07/16/2013 14:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]
@IdTipoDocumentoPrograma INT OUTPUT, @IdTipoDocumento INT, @IdPrograma INT, @Estado INT, @MaxPermitidoKB INT, @ExtensionesPermitidas NVARCHAR(50), @ObligRupNoRenovado INT, @ObligRupRenovado INT, @ObligPersonaJuridica INT, @ObligPersonaNatural INT, @UsuarioCrea NVARCHAR (250)
AS
BEGIN
INSERT INTO Proveedor.TipoDocumentoPrograma (IdTipoDocumento, IdPrograma, Estado, MaxPermitidoKB, ExtensionesPermitidas, ObligRupNoRenovado, ObligRupRenovado, ObligPersonaJuridica, ObligPersonaNatural, UsuarioCrea, FechaCrea)
	VALUES (@IdTipoDocumento, @IdPrograma, 1, @MaxPermitidoKB, @ExtensionesPermitidas, @ObligRupNoRenovado, @ObligRupRenovado, @ObligPersonaJuridica, @ObligPersonaNatural, @UsuarioCrea, GETDATE())
SELECT
	@IdTipoDocumentoPrograma = @@IDENTITY
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]    Script Date: 07/16/2013 14:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]
@IdTipoDocumentoPrograma INT
AS
BEGIN
	UPDATE Proveedor.TipoDocumentoPrograma
	SET Estado = 0
	WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]    Script Date: 07/16/2013 14:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Fabian Valencia
-- Create date: 22/06/2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam] 
	@IdDocumento int,
	@Programa NVARCHAR(30) 
AS
BEGIN
	DECLARE @IdPrograma INT
	SET @IdPrograma = (SELECT DISTINCT IdPrograma  FROM SEG.Programa WHERE CodigoPrograma = @Programa)
	
	
	SELECT     Proveedor.TipoDocumentoPrograma.IdTipoDocumento,  
			   Proveedor.TipoDocumentoPrograma.MaxPermitidoKB, 
               Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas, 
               Proveedor.TipoDocumento.CodigoTipoDocumento, 
               Proveedor.TipoDocumento.Descripcion, 
               Proveedor.TipoDocumento.Estado, 
               Proveedor.TipoDocumento.UsuarioCrea, 
               Proveedor.TipoDocumento.FechaCrea
	FROM       Proveedor.TipoDocumentoPrograma 
			   INNER JOIN
			  Proveedor.TipoDocumento ON 
			  Proveedor.TipoDocumentoPrograma.IdTipoDocumento = Proveedor.TipoDocumento.IdTipoDocumento
	WHERE     (Proveedor.TipoDocumentoPrograma.IdTipoDocumento = @IdDocumento) AND (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]    Script Date: 07/16/2013 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]
@IdTipoDocumentoPrograma INT
AS
BEGIN
SELECT
	IdTipoDocumentoPrograma,
	IdTipoDocumento,
	IdPrograma,
	Estado,
	MaxPermitidoKB,
	ExtensionesPermitidas,
	ObligRupNoRenovado,
	ObligRupRenovado,
	ObligPersonaJuridica,
	ObligPersonaNatural,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[TipoDocumentoPrograma]
WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma AND
	  Estado = 1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]    Script Date: 07/16/2013 14:28:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]
		@IdEntidad INT, @IdEstadoInfoDatosBasicosEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[EntidadProvOferente] 
	SET IdEstado = @IdEstadoInfoDatosBasicosEntidad, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdEntidad = @IdEntidad
END






' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]    Script Date: 07/16/2013 14:28:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]
		@IdValidarInfoExperienciaEntidad INT OUTPUT, @IdExpEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoExperienciaEntidad(IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdExpEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoExperienciaEntidad = @@IDENTITY 		
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]    Script Date: 07/16/2013 14:28:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoExperienciaEntidad por IdInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]
	@IdExpEntidad INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoExperienciaEntidad] 
 WHERE  IdExpEntidad = @IdExpEntidad
 ORDER BY IdValidarInfoExperienciaEntidad DESC
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]    Script Date: 07/16/2013 14:28:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]
	@IdValidarInfoExperienciaEntidad INT
AS
BEGIN
 SELECT IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoExperienciaEntidad] WHERE  IdValidarInfoExperienciaEntidad = @IdValidarInfoExperienciaEntidad
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]    Script Date: 07/16/2013 14:28:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]
	@IdContacto INT
AS
BEGIN
 SELECT CE.IdContacto, CE.IdEntidad, CE.IdSucursal, CE.IdTelContacto, CE.IdTipoDocIdentifica, CE.IdTipoCargoEntidad, CE.NumeroIdentificacion, CE.PrimerNombre, 
	CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido, CE.Dependencia, CE.Email, CE.Estado, CE.UsuarioCrea, CE.FechaCrea, CE.UsuarioModifica, CE.FechaModifica,
	TT.Movil AS Celular, ISNULL(TT.IndicativoTelefono, '''') AS IndicativoTelefono, ISNULL(TT.NumeroTelefono, '''') AS NumeroTelefono, ISNULL(TT.ExtensionTelefono, '''') AS ExtensionTelefono,
	TC.Descripcion AS Cargo
 FROM [Proveedor].[ContactoEntidad] AS CE
 LEFT JOIN [Oferente].[TelTerceros] AS TT ON CE.IdTelContacto = TT.IdTelTercero
 LEFT JOIN [Proveedor].[TipoCargoEntidad] AS TC ON CE.IdTipoCargoEntidad = TC.IdTipoCargoEntidad
 WHERE  IdContacto = @IdContacto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]    Script Date: 07/16/2013 14:28:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]
	@IdEntidad int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]    Script Date: 07/16/2013 14:28:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]
		@IdValidarInfoDatosBasicosEntidad INT OUTPUT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoDatosBasicosEntidad(IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoDatosBasicosEntidad = @@IDENTITY 		
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]    Script Date: 07/16/2013 14:28:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoDatosBasicosEntidad por IdInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] 
 WHERE  IdEntidad = @IdEntidad
 ORDER BY IdValidarInfoDatosBasicosEntidad DESC
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]    Script Date: 07/16/2013 14:28:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]
	@IdValidarInfoDatosBasicosEntidad INT
AS
BEGIN
 SELECT IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] WHERE  IdValidarInfoDatosBasicosEntidad = @IdValidarInfoDatosBasicosEntidad
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]    Script Date: 07/16/2013 14:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/8/2013 12:21:51 PM
-- Description:	Procedimiento almacenado que elimina un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]
	@IdInfoFin INT
AS
BEGIN
	DELETE Proveedor.InfoFinancieraEntidad WHERE IdInfoFin = @IdInfoFin
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]    Script Date: 07/16/2013 14:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]
	@IdInfoFin INT
AS
BEGIN
 SELECT IdInfoFin, IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[InfoFinancieraEntidad] WHERE  IdInfoFin = @IdInfoFin
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]    Script Date: 07/16/2013 14:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]
	@IdEntidad INT = NULL,@Estado INT = NULL
AS
BEGIN
 SELECT IdSucursal, IdEntidad,Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado, IdZona, Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Sucursal] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 --AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]    Script Date: 07/16/2013 14:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]
		@IdSucursal INT,	@IdEntidad INT,	@Nombre VARCHAR(255), @Indicativo INT,	@Telefono INT,	@Extension NUMERIC(10) = NULL,	@Celular  NUMERIC(10),	@Correo NVARCHAR(256),	@Estado INT, @IdZona INT,	@Departamento INT,	@Municipio INT,	@Direccion NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Sucursal SET IdEntidad = @IdEntidad, Nombre = @Nombre, Indicativo = @Indicativo, Telefono = @Telefono, Extension = @Extension, Celular = @Celular, Correo = @Correo, Estado = @Estado, IdZona = @IdZona, Departamento = @Departamento, Municipio = @Municipio, Direccion = @Direccion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdSucursal = @IdSucursal
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]    Script Date: 07/16/2013 14:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]
		@IdSucursal INT OUTPUT, 	@IdEntidad INT, @Nombre NVARCHAR(255), @Indicativo INT,	@Telefono INT,	@Extension NUMERIC(10) = NULL,	@Celular NUMERIC(10),	@Correo NVARCHAR(256),	@Estado INT, @IdZona INT,	@Departamento INT,	@Municipio INT,	@Direccion NVARCHAR(256), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Sucursal(IdEntidad,Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado, IdZona, Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @Nombre, @Indicativo, @Telefono, @Extension, @Celular, @Correo, @Estado, @IdZona, @Departamento, @Municipio, @Direccion, @UsuarioCrea, GETDATE())
	SELECT @IdSucursal = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]    Script Date: 07/16/2013 14:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que elimina un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]
	@IdSucursal INT
AS
BEGIN
	DELETE Proveedor.Sucursal WHERE IdSucursal = @IdSucursal
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]    Script Date: 07/16/2013 14:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]
	@IdSucursal INT
AS
BEGIN
 SELECT IdSucursal, IdEntidad, Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado,IdZona,Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Sucursal] WHERE  IdSucursal = @IdSucursal
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]    Script Date: 07/16/2013 14:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que consulta un(a) NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]
	@IdNotJudicial INT= NULL,
	@IdEntidad INT = NULL,
	@IdDepartamento INT = NULL,
	@IdMunicipio INT = NULL
AS
BEGIN
 SELECT IdNotJudicial, 
		IdEntidad, 
		IdDepartamento, 
		IdMunicipio, 
		IdZona, Direccion, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
		FROM [Proveedor].[NotificacionJudicial] 
		WHERE IdNotJudicial = CASE WHEN @IdNotJudicial IS NULL THEN IdNotJudicial ELSE @IdNotJudicial END AND 
			  IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END AND 
			  IdDepartamento = CASE WHEN @IdDepartamento IS NULL THEN IdDepartamento ELSE @IdDepartamento END AND 
			  IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]    Script Date: 07/16/2013 14:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Fabian Valencia
-- Create date: 21/06/2013
-- Description:	Obtiene las notificaiones judiciales del proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param] 
	@IdEntidad INT = NULL
	
AS
BEGIN
	--DECLARE @IdTipoPersona INT 
	--DECLARE @IdTipoIdentificacion INT
	--DECLARE @NumeroIdentificacion NVARCHAR(20) 
	--DECLARE @DV NVARCHAR(2) 
	--DECLARE @Proveedor NVARCHAR(250) 
	--DECLARE @CorreoElectronico NVARCHAR(50)
	
	--SET @IdTipoPersona = null
	--SET @IdTipoIdentificacion = null
	--SET @NumeroIdentificacion = null
	--SET @DV = null
	--SET @Proveedor = ''''
	--SET @CorreoElectronico= null
	
	SELECT      Proveedor.NotificacionJudicial.IdNotJudicial,
			   (SELECT INFRAPAE12.Departamentos.Nomdepartamento 
			    FROM INFRAPAE12.Departamentos
			    WHERE INFRAPAE12.Departamentos.IdDepartamento = NotificacionJudicial.IdDepartamento) AS NombreDepartamento,
			   (SELECT INFRAPAE12.Municipios.NomMunicipio 
			    FROM INFRAPAE12.Municipios
			    WHERE INFRAPAE12.Municipios.IdDepartamento = NotificacionJudicial.IdDepartamento AND
					  INFRAPAE12.Municipios.IdMunicipio = NotificacionJudicial.IdMunicipio) AS NombreMunicipio,
		        Proveedor.NotificacionJudicial.Direccion, Proveedor.NotificacionJudicial.UsuarioCrea
   FROM         Proveedor.EntidadProvOferente 
				INNER JOIN Oferente.TERCERO 
				ON Proveedor.EntidadProvOferente.IdTercero = Oferente.TERCERO.IDTERCERO 
				INNER JOIN Proveedor.NotificacionJudicial 
				ON Proveedor.EntidadProvOferente.IdEntidad = Proveedor.NotificacionJudicial.IdEntidad AND 
                   Proveedor.EntidadProvOferente.IdEntidad = Proveedor.NotificacionJudicial.IdEntidad	
    WHERE       Proveedor.EntidadProvOferente.IdEntidad  = CASE WHEN @IdEntidad IS NULL THEN Proveedor.EntidadProvOferente.IdEntidad  ELSE @IdEntidad END
				
	
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]    Script Date: 07/16/2013 14:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que actualiza un(a) NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]
		@IdNotJudicial INT,	
		@IdEntidad INT,	
		@IdDepartamento INT,	
		@IdMunicipio INT,	
		@IdZona INT,	
		@Direccion NVARCHAR(250), 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.NotificacionJudicial 
	SET IdEntidad = @IdEntidad, 
	IdDepartamento = @IdDepartamento,
	IdMunicipio	= @IdMunicipio,
	IdZona = @IdZona,
	Direccion = @Direccion,
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdNotJudicial = @IdNotJudicial 
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]    Script Date: 07/16/2013 14:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]
		@IdNotJudicial INT OUTPUT, 	
		@IdEntidad INT,	
		@IdDepartamento INT,	
		@IdMunicipio INT , 	
		@IdZona INT , 	
		@Direccion NVARCHAR(250) ,  
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.NotificacionJudicial(IdEntidad, IdDepartamento,IdMunicipio,IdZona, Direccion, UsuarioCrea, FechaCrea)
	VALUES(@IdEntidad, @IdDepartamento,@IdMunicipio,@IdZona,@Direccion, @UsuarioCrea, GETDATE())
	SELECT @IdNotJudicial = @@IDENTITY 		
	RETURN @IdNotJudicial
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]    Script Date: 07/16/2013 14:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdEntidad, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal
 FROM [Proveedor].[DocDatosBasicoProv] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]    Script Date: 07/16/2013 14:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]
		@IdDocAdjunto INT,	@IdEntidad INT,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocDatosBasicoProv SET IdEntidad = @IdEntidad, NombreDocumento = @NombreDocumento, LinkDocumento = @LinkDocumento, Observaciones = @Observaciones, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDocAdjunto = @IdDocAdjunto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]    Script Date: 07/16/2013 14:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DocDatosBasico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdEntidad INT = NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @IdTipoDocumento int, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	INSERT INTO Proveedor.DocDatosBasicoProv(IdEntidad, NombreDocumento, LinkDocumento, Observaciones, IdTipoDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdEntidad, @NombreDocumento, @LinkDocumento, @Observaciones, @IdTipoDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = @@IDENTITY 		
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]    Script Date: 07/16/2013 14:28:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/18/2013 6:02:08 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocDatosBasicoProv WHERE IdDocAdjunto = @IdDocAdjunto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona]    Script Date: 07/16/2013 14:28:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdTemporal y TipoPersona
-- =============================================


CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona] 
( @IdTemporal VARCHAR(20), @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''PROVEEDOR/GestionProveedores'';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN ''1'' THEN tdp.ObligPersonaNatural 
								WHEN ''2'' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTemporal = @IdTemporal
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
			 '''' as Observaciones,
			 CASE @TipoPersona	WHEN ''1'' THEN tdp.ObligPersonaNatural 
								WHEN ''2'' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '''' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona]    Script Date: 07/16/2013 14:28:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdEntidad y TipoPersona
-- =============================================

CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona] 
( @IdEntidad int, @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''PROVEEDOR/GestionProveedores'';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN ''1'' THEN tdp.ObligPersonaNatural 
								WHEN ''2'' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdEntidad = @IdEntidad
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
			 '''' as Observaciones,
			 CASE @TipoPersona	WHEN ''1'' THEN tdp.ObligPersonaNatural 
								WHEN ''2'' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '''' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
WHERE Obligatorio  = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento



' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]    Script Date: 07/16/2013 14:28:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]
	@IdDocAdjunto INT
AS
BEGIN
 SELECT IdDocAdjunto, IdEntidad, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTipoDocumento, IdTemporal
 FROM [Proveedor].[DocDatosBasicoProv] 
 WHERE  IdDocAdjunto = @IdDocAdjunto
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]    Script Date: 07/16/2013 14:28:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  12/07/2013 22:42:55 
-- Description:	Procedimiento almacenado que consulta Proveedores para Validar
-- 
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]
	@IdEstado INT = NULL,
	@IdTipoPersona INT = NULL,
	@IDTIPODOCIDENTIFICA INT= NULL,
	@NUMEROIDENTIFICACION BIGINT = NULL,
	@IdTipoCiiuPrincipal INT = NULL,
	@IdMunicipioDirComercial INT = NULL,
	@IdDepartamentoDirComercial INT = NULL,
	@IdTipoSector INT = NULL,
	@IdTipoRegimenTributario INT = NULL,
	@Proveedor NVARCHAR(256) = NULL	
	
AS
BEGIN
SELECT     EP.IdEntidad, EP.ConsecutivoInterno, T.IdTipoPersona, TP.NombreTipoPersona, TD.CodDocumento, T.IDTIPODOCIDENTIFICA, 
                      T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, LTRIM(RTRIM(ISNULL(T.PRIMERNOMBRE, '''') + '' '' + ISNULL(T.SEGUNDONOMBRE, '''') 
                      + '' '' + ISNULL(T.PRIMERAPELLIDO, '''') + '' '' + ISNULL(T.SEGUNDOAPELLIDO, '''') + '' '' + ISNULL(T.RAZONSOCIAL, ''''))) AS Razonsocial, E.Descripcion AS Estado, 
                      EP.IdEstado, EP.UsuarioCrea, EP.IdTipoCiiuPrincipal, 
                      Proveedor.TipoCiiu.CodigoCiiu + ''-'' + Proveedor.TipoCiiu.Descripcion AS ActividadCiiuPrincipal, Proveedor.TipoSectorEntidad.IdTipoSectorEntidad, 
                      Proveedor.TipoSectorEntidad.Descripcion AS SectorEntidad, Proveedor.TipoRegimenTributario.IdTipoRegimenTributario, 
                      Proveedor.TipoRegimenTributario.Descripcion AS RegimenTributario, EP.IdTipoSector, DIV.Municipio.NombreMunicipio, DIV.Departamento.NombreDepartamento, 
                      IAE.IdMunicipioDirComercial, DIV.Municipio.IdDepartamento
FROM         Proveedor.EntidadProvOferente AS EP INNER JOIN
                      Oferente.TERCERO AS T ON EP.IdTercero = T.IDTERCERO INNER JOIN
                      Proveedor.InfoAdminEntidad AS IAE ON EP.IdEntidad = IAE.IdEntidad INNER JOIN
                      Oferente.TipoPersona AS TP ON T.IdTipoPersona = TP.IdTipoPersona INNER JOIN
                      Global.TiposDocumentos AS TD ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento INNER JOIN
                      Proveedor.EstadoDatosBasicos AS E ON EP.IdEstado = E.IdEstadoDatosBasicos INNER JOIN
                      Proveedor.TipoCiiu ON EP.IdTipoCiiuPrincipal = Proveedor.TipoCiiu.IdTipoCiiu AND EP.IdTipoCiiuPrincipal = Proveedor.TipoCiiu.IdTipoCiiu INNER JOIN
                      Proveedor.TipoSectorEntidad ON EP.IdTipoSector = Proveedor.TipoSectorEntidad.IdTipoSectorEntidad AND 
                      EP.IdTipoSector = Proveedor.TipoSectorEntidad.IdTipoSectorEntidad INNER JOIN
                      Proveedor.TipoRegimenTributario ON IAE.IdTipoRegTrib = Proveedor.TipoRegimenTributario.IdTipoRegimenTributario AND 
                      IAE.IdTipoRegTrib = Proveedor.TipoRegimenTributario.IdTipoRegimenTributario INNER JOIN
                      DIV.Municipio INNER JOIN
                      DIV.Departamento ON DIV.Municipio.IdDepartamento = DIV.Departamento.IdDepartamento AND DIV.Municipio.IdDepartamento = DIV.Departamento.IdDepartamento ON 
                      IAE.IdMunicipioDirComercial = DIV.Municipio.IdMunicipio
WHERE (EP.IdEstado = CASE WHEN @IdEstado IS NULL THEN EP.IdEstado ELSE @IdEstado END )
AND (T.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN T.IdTipoPersona ELSE @IdTipoPersona END )--1 
AND (T.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN T.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END ) 
AND (T.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN T.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END) 
AND (EP.IdTipoCiiuPrincipal = CASE WHEN @IdTipoCiiuPrincipal IS NULL THEN EP.IdTipoCiiuPrincipal ELSE @IdTipoCiiuPrincipal END) 
AND (IAE.IdMunicipioDirComercial = CASE WHEN @IdMunicipioDirComercial IS NULL THEN IAE.IdMunicipioDirComercial ELSE @IdMunicipioDirComercial END) 
AND (IAE.IdDepartamentoDirComercial = CASE WHEN @IdDepartamentoDirComercial IS NULL THEN IAE.IdDepartamentoDirComercial ELSE @IdDepartamentoDirComercial END) 
AND (EP.IdTipoSector = CASE WHEN @IdTipoSector IS NULL THEN EP.IdTipoSector ELSE @IdTipoSector END) 
AND (IAE.IdTipoRegTrib = CASE WHEN @IdTipoRegimenTributario IS NULL THEN IAE.IdTipoRegTrib ELSE @IdTipoRegimenTributario END) 
AND (
					T.PRIMERNOMBRE LIKE CASE WHEN @Proveedor IS NULL THEN T.PRIMERNOMBRE ELSE  ''%''+ @Proveedor +''%'' END
					OR T.SEGUNDONOMBRE  LIKE CASE WHEN @Proveedor IS NULL THEN T.SEGUNDONOMBRE ELSE  ''%''+ @Proveedor +''%'' END
					OR T.PRIMERAPELLIDO LIKE CASE WHEN @Proveedor IS NULL THEN T.PRIMERAPELLIDO ELSE  ''%''+ @Proveedor +''%'' END
					OR T.SEGUNDOAPELLIDO LIKE CASE WHEN @Proveedor IS NULL THEN T.SEGUNDOAPELLIDO ELSE  ''%''+ @Proveedor +''%'' END
					OR T.RAZONSOCIAL  LIKE CASE WHEN @Proveedor IS NULL THEN T.RAZONSOCIAL ELSE  ''%''+ @Proveedor +''%'' END 
					)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]    Script Date: 07/16/2013 14:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]
	@IdTipoPersona INT = NULL,@Tipoidentificacion INT= NULL,@Identificacion NVARCHAR(256) = NULL,
	@Proveedor NVARCHAR(256) = NULL,@IdEstado INT = NULL,@UsuarioCrea NVARCHAR(128)=NULL
AS
BEGIN
 SELECT DISTINCT * FROM (
  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.PRIMERNOMBRE +'' ''+ISNULL(T.SEGUNDONOMBRE,'''') +'' '' + T.PRIMERAPELLIDO +'' ''+ ISNULL(T.SEGUNDOAPELLIDO,'''') ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador,EP.UsuarioCrea
  FROM [Proveedor].[EntidadProvOferente] EP 
     INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     WHERE T.IdTipoPersona=1
UNION 
 SELECT  EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.RAZONSOCIAL ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado ,EP.ObserValidador,EP.UsuarioCrea
  FROM [Proveedor].[EntidadProvOferente] EP 
    INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
    LEFT JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
    INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona
    INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
    INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
    WHERE T.IdTipoPersona=2
    ) DT
     
    
    WHERE IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN IdTipoPersona ELSE @IdTipoPersona END 
		  AND IDTIPODOCIDENTIFICA = CASE WHEN @Tipoidentificacion IS NULL THEN IDTIPODOCIDENTIFICA ELSE @Tipoidentificacion END 
		  AND NUMEROIDENTIFICACION = CASE WHEN @Identificacion IS NULL THEN NUMEROIDENTIFICACION ELSE @Identificacion END 
		  AND (Razonsocila) like CASE WHEN @Proveedor IS NULL THEN (Razonsocila) ELSE ''%''+ @Proveedor +''%'' END 
		  AND IdEstado = CASE WHEN @IdEstado IS NULL THEN IdEstado ELSE @IdEstado END 
		  AND UsuarioCrea = CASE WHEN @UsuarioCrea IS NULL THEN UsuarioCrea ELSE @UsuarioCrea END 

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]    Script Date: 07/16/2013 14:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que guarda un nuevo EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]
		@IdEntidad INT OUTPUT, 	
		@ConsecutivoInterno NVARCHAR(128),
		@TipoEntOfProv BIT=NULL,	
		@IdTercero INT=NULL,	
		@IdTipoCiiuPrincipal INT=NULL,	
		@IdTipoCiiuSecundario INT=NULL,	
		@IdTipoSector INT=NULL,	
		@IdTipoClaseEntidad INT=NULL,	
		@IdTipoRamaPublica INT=NULL,	
		@IdTipoNivelGob INT=NULL,	
		@IdTipoNivelOrganizacional INT=NULL,	
		@IdTipoCertificadorCalidad INT=NULL,	
		@FechaCiiuPrincipal DATETIME=NULL,	
		@FechaCiiuSecundario DATETIME=NULL,	
		@FechaConstitucion DATETIME=NULL,	
		@FechaVigencia DATETIME=NULL,	
		@FechaMatriculaMerc DATETIME=NULL,	
		@FechaExpiracion DATETIME=NULL,	
		@TipoVigencia BIT=NULL,	
		@ExenMatriculaMer BIT=NULL,	
		@MatriculaMercantil NVARCHAR(20)=NULL,	
		@ObserValidador NVARCHAR(256)=NULL,	
		@AnoRegistro INT=NULL,	
		@IdEstado INT=NULL, 
		@UsuarioCrea NVARCHAR(250),
		@IdTemporal VARCHAR(20) = NULL
AS
BEGIN
	INSERT INTO Proveedor.EntidadProvOferente(TipoEntOfProv, IdTercero, IdTipoCiiuPrincipal, IdTipoCiiuSecundario, IdTipoSector, IdTipoClaseEntidad, IdTipoRamaPublica, IdTipoNivelGob, IdTipoNivelOrganizacional, IdTipoCertificadorCalidad, FechaCiiuPrincipal, FechaCiiuSecundario, FechaConstitucion, FechaVigencia, FechaMatriculaMerc, FechaExpiracion, TipoVigencia, ExenMatriculaMer, MatriculaMercantil, ObserValidador, AnoRegistro, IdEstado, UsuarioCrea, FechaCrea)
					  VALUES(@TipoEntOfProv, @IdTercero, @IdTipoCiiuPrincipal, @IdTipoCiiuSecundario, @IdTipoSector, @IdTipoClaseEntidad, @IdTipoRamaPublica, @IdTipoNivelGob, @IdTipoNivelOrganizacional, @IdTipoCertificadorCalidad, @FechaCiiuPrincipal, @FechaCiiuSecundario, @FechaConstitucion, @FechaVigencia, @FechaMatriculaMerc, @FechaExpiracion, @TipoVigencia, @ExenMatriculaMer, @MatriculaMercantil, @ObserValidador, @AnoRegistro, @IdEstado, @UsuarioCrea, GETDATE())
	SELECT @IdEntidad = @@IDENTITY 		
	
	UPDATE [Proveedor].[DocDatosBasicoProv] set IdEntidad = @IdEntidad where IdTemporal = @IdTemporal
	
	UPDATE Proveedor.EntidadProvOferente SET ConsecutivoInterno=@ConsecutivoInterno +  RIGHT(''00000'' + Ltrim(Rtrim(@IdEntidad)),8) WHERE IdEntidad=@IdEntidad
	
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]    Script Date: 07/16/2013 14:28:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]
	@IdEntidad INT = NULL
AS
BEGIN
 SELECT IdInfoAdmin, IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, IdTipoRangosTrabajadores, 
 IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, IdDepartamentoDirComercial, 
 IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, PorctjPublico, SitioWeb, 
 NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, Mision, PQRS, 
 GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, PartMesasTerritoriales, 
 PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, NumSedes, SedesPropias, 
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[InfoAdminEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]    Script Date: 07/16/2013 14:28:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]
		@IdInfoAdmin  INT = NULL,	@IdVigencia  INT = NULL,	@IdEntidad  INT = NULL,	@IdTipoRegTrib  INT = NULL,	@IdTipoOrigenCapital  INT = NULL,	@IdTipoActividad  INT = NULL,	
		@IdTipoEntidad  INT = NULL,	@IdTipoNaturalezaJurid  INT = NULL,	@IdTipoRangosTrabajadores  INT = NULL,	@IdTipoRangosActivos  INT = NULL,	@IdRepLegal  INT = NULL,	
		@IdTipoCertificaTamano  INT = NULL,	@IdTipoEntidadPublica  INT = NULL,	@IdDepartamentoConstituida  INT = NULL,	@IdMunicipioConstituida  INT = NULL,	@IdDepartamentoDirComercial  INT = NULL,	
		@IdMunicipioDirComercial  INT = NULL,	@DireccionComercial NVARCHAR(50)= NULL,	@IdZona  NVARCHAR(128) = NULL,@NombreComercial NVARCHAR(128)= NULL,
		@NombreEstablecimiento NVARCHAR(128) = NULL,	@Sigla NVARCHAR(50)= NULL,	
		@PorctjPrivado  INT = NULL,	@PorctjPublico  INT = NULL,	@SitioWeb NVARCHAR(128)= NULL,	@NombreEntidadAcreditadora NVARCHAR = NULL,	@Organigrama  BIT = NULL,	
		@TotalPnalAnnoPrevio  numeric(10) = NULL,	@VincLaboral  numeric(10) = NULL,	@PrestServicios  numeric(10) = NULL,	@Voluntariado  numeric(10) = NULL,	
		@VoluntPermanente  numeric(10) = NULL,	@Asociados  numeric(10) = NULL,	@Mision  numeric(10) = NULL,	@PQRS  BIT = NULL,	@GestionDocumental  BIT = NULL,	@Auditoria INT = NULL, @AuditoriaInterna  BIT = NULL,	
		@ManProcedimiento  BIT = NULL,	@ManPracticasAmbiente  BIT = NULL,	@ManComportOrg  BIT = NULL,	@ManFunciones  BIT = NULL,	@ProcRegInfoContable  BIT = NULL,	@PartMesasTerritoriales  BIT = NULL,	
		@PartAsocAgremia  BIT = NULL,	@PartConsejosComun  BIT = NULL,	@Conv INT = NULL ,@ConvInterInst  BIT = NULL,	@ProcSeleccGral  BIT = NULL,	@ProcSeleccEtnico  BIT = NULL,	@PlanInduccCapac  BIT = NULL,	
		@EvalDesemp  BIT = NULL,	@PlanCualificacion  BIT = NULL,	@NumSedes  INT = NULL,	@SedesPropias  BIT = NULL, @UsuarioModifica NVARCHAR(250)= NULL
AS
BEGIN
	UPDATE Proveedor.InfoAdminEntidad 
	SET 
	IdVigencia = ISNULL( @IdVigencia, IdVigencia ),
	IdEntidad = ISNULL( @IdEntidad, IdEntidad ),
	IdTipoRegTrib = ISNULL( @IdTipoRegTrib, IdTipoRegTrib ),
	IdTipoOrigenCapital = ISNULL( @IdTipoOrigenCapital,IdTipoOrigenCapital ),
	IdTipoActividad = ISNULL( @IdTipoActividad, IdTipoActividad ),
	IdTipoEntidad = ISNULL( @IdTipoEntidad, IdTipoEntidad ),
	IdTipoNaturalezaJurid = ISNULL( @IdTipoNaturalezaJurid, IdTipoNaturalezaJurid ),
	IdTipoRangosTrabajadores = ISNULL( @IdTipoRangosTrabajadores, IdTipoRangosTrabajadores ),
	IdTipoRangosActivos = ISNULL( @IdTipoRangosActivos, IdTipoRangosActivos ),
	IdRepLegal = ISNULL( @IdRepLegal, IdRepLegal ),
	IdTipoCertificaTamano = ISNULL( @IdTipoCertificaTamano, IdTipoCertificaTamano ),
	IdTipoEntidadPublica = ISNULL( @IdTipoEntidadPublica, IdTipoEntidadPublica ),
	IdDepartamentoConstituida = ISNULL( @IdDepartamentoConstituida, IdDepartamentoConstituida ),
	IdMunicipioConstituida = ISNULL( @IdMunicipioConstituida, IdMunicipioConstituida ),
	IdDepartamentoDirComercial = ISNULL( @IdDepartamentoDirComercial, IdDepartamentoDirComercial ),
	IdMunicipioDirComercial = ISNULL( @IdMunicipioDirComercial, IdMunicipioDirComercial ),
	DireccionComercial = ISNULL( @DireccionComercial, DireccionComercial ),
	IdZona = ISNULL( @IdZona,  IdZona ),
	NombreComercial = ISNULL( @NombreComercial, NombreComercial ),
	NombreEstablecimiento = ISNULL( @NombreEstablecimiento, NombreEstablecimiento ),
	Sigla = ISNULL( @Sigla, Sigla ),
	PorctjPrivado = ISNULL( @PorctjPrivado, PorctjPrivado ),
	PorctjPublico = ISNULL( @PorctjPublico, PorctjPublico ),
	SitioWeb = ISNULL( @SitioWeb, SitioWeb ),
	NombreEntidadAcreditadora = ISNULL( @NombreEntidadAcreditadora, NombreEntidadAcreditadora ),
	Organigrama = ISNULL( @Organigrama, Organigrama ),
	TotalPnalAnnoPrevio = ISNULL( @TotalPnalAnnoPrevio, TotalPnalAnnoPrevio ),
	VincLaboral = ISNULL( @VincLaboral, VincLaboral ),
	PrestServicios = ISNULL( @PrestServicios, PrestServicios ),
	Voluntariado = ISNULL( @Voluntariado, Voluntariado ),
	VoluntPermanente = ISNULL( @VoluntPermanente, VoluntPermanente ),
	Asociados = ISNULL( @Asociados, Asociados ),
	Mision = ISNULL( @Mision, Mision ),
	PQRS = ISNULL( @PQRS, PQRS ),
	GestionDocumental = ISNULL( @GestionDocumental, GestionDocumental ),
	AuditoriaInterna = ISNULL( @AuditoriaInterna, AuditoriaInterna ),
	ManProcedimiento = ISNULL( @ManProcedimiento, ManProcedimiento ),
	ManPracticasAmbiente = ISNULL( @ManPracticasAmbiente, ManPracticasAmbiente ),
	ManComportOrg = ISNULL( @ManComportOrg, ManComportOrg ),
	ManFunciones = ISNULL( @ManFunciones, ManFunciones ),
	ProcRegInfoContable = ISNULL( @ProcRegInfoContable, ProcRegInfoContable ),
	PartMesasTerritoriales = ISNULL( @PartMesasTerritoriales, PartMesasTerritoriales ),
	PartAsocAgremia = ISNULL( @PartAsocAgremia, PartAsocAgremia ),
	PartConsejosComun = ISNULL( @PartConsejosComun, PartConsejosComun ),
	ConvInterInst = ISNULL( @ConvInterInst, ConvInterInst ),
	ProcSeleccGral = ISNULL( @ProcSeleccGral, ProcSeleccGral ),
	ProcSeleccEtnico = ISNULL( @ProcSeleccEtnico, ProcSeleccEtnico ),
	PlanInduccCapac = ISNULL( @PlanInduccCapac, PlanInduccCapac ),
	EvalDesemp = ISNULL( @EvalDesemp, EvalDesemp ),
	PlanCualificacion = ISNULL( @PlanCualificacion, PlanCualificacion ),
	NumSedes = ISNULL( @NumSedes, NumSedes ),
	SedesPropias = ISNULL( @SedesPropias, SedesPropias ),
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdInfoAdmin = @IdInfoAdmin
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]    Script Date: 07/16/2013 14:28:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]
		@IdInfoAdmin INT OUTPUT, 	
		@IdVigencia INT=1,	
		@IdEntidad INT=NULL,	
		@IdTipoRegTrib INT=NULL,	
		@IdTipoOrigenCapital INT=NULL,	
		@IdTipoActividad INT=NULL,	
		@IdTipoEntidad INT=NULL,	
		@IdTipoNaturalezaJurid INT=NULL,	
		@IdTipoRangosTrabajadores INT=NULL,	
		@IdTipoRangosActivos INT=NULL,	
		@IdRepLegal INT=NULL,	
		@IdTipoCertificaTamano INT=NULL,
		@IdTipoEntidadPublica INT=NULL,	
		@IdDepartamentoConstituida INT=NULL,	
		@IdMunicipioConstituida INT=NULL,	
		@IdDepartamentoDirComercial INT=NULL,	
		@IdMunicipioDirComercial INT=NULL,	
		@DireccionComercial NVARCHAR(50)=NULL,	
		@IdZona INT=NULL,
		@NombreComercial NVARCHAR(128)=NULL,	
		@NombreEstablecimiento NVARCHAR(128)=NULL,	
		@Sigla NVARCHAR(50)=NULL,	
		@PorctjPrivado INT=NULL,	
		@PorctjPublico INT=NULL,	
		@SitioWeb NVARCHAR(128)=NULL,	
		@NombreEntidadAcreditadora NVARCHAR (128)=NULL,	
		@Organigrama BIT=NULL,	
		@TotalPnalAnnoPrevio numeric(10)=NULL,	
		@VincLaboral numeric(10)=NULL,	
		@PrestServicios numeric(10)=NULL,	
		@Voluntariado numeric(10)=NULL,	
		@VoluntPermanente numeric(10)=NULL,	
		@Asociados numeric(10)=NULL,	
		@Mision numeric(10)=NULL,	
		@PQRS BIT=NULL,	
		@GestionDocumental BIT=NULL,	
		@AuditoriaInterna BIT=NULL,	
		@ManProcedimiento BIT=NULL,	
		@ManPracticasAmbiente BIT=NULL,	
		@ManComportOrg BIT=NULL,	
		@ManFunciones BIT=NULL,	
		@ProcRegInfoContable BIT=NULL,	
		@PartMesasTerritoriales BIT=NULL,	
		@PartAsocAgremia BIT=NULL,	
		@PartConsejosComun BIT=NULL,	
		@ConvInterInst BIT=NULL,	
		@ProcSeleccGral BIT=NULL,	
		@ProcSeleccEtnico BIT=NULL,	
		@PlanInduccCapac BIT=NULL,	
		@EvalDesemp BIT=NULL,	
		@PlanCualificacion BIT=NULL,	
		@NumSedes INT=NULL,	
		@SedesPropias BIT=NULL, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.InfoAdminEntidad(IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, IdTipoRangosTrabajadores, IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, IdDepartamentoDirComercial, IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, PorctjPublico, SitioWeb, NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, Mision, PQRS, GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, PartMesasTerritoriales, PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, NumSedes, SedesPropias, UsuarioCrea, FechaCrea)
					  VALUES(@IdVigencia, @IdEntidad, @IdTipoRegTrib, @IdTipoOrigenCapital, @IdTipoActividad, @IdTipoEntidad, @IdTipoNaturalezaJurid, @IdTipoRangosTrabajadores, @IdTipoRangosActivos, @IdRepLegal, @IdTipoCertificaTamano, @IdTipoEntidadPublica, @IdDepartamentoConstituida, @IdMunicipioConstituida, @IdDepartamentoDirComercial, @IdMunicipioDirComercial, @DireccionComercial, @IdZona, @NombreComercial, @NombreEstablecimiento, @Sigla, @PorctjPrivado, @PorctjPublico, @SitioWeb, @NombreEntidadAcreditadora, @Organigrama, @TotalPnalAnnoPrevio, @VincLaboral, @PrestServicios, @Voluntariado, @VoluntPermanente, @Asociados, @Mision, @PQRS, @GestionDocumental, @AuditoriaInterna, @ManProcedimiento, @ManPracticasAmbiente, @ManComportOrg, @ManFunciones, @ProcRegInfoContable, @PartMesasTerritoriales, @PartAsocAgremia, @PartConsejosComun, @ConvInterInst, @ProcSeleccGral, @ProcSeleccEtnico, @PlanInduccCapac, @EvalDesemp, @PlanCualificacion, @NumSedes, @SedesPropias, @UsuarioCrea, GETDATE())
	SELECT @IdInfoAdmin = @@IDENTITY 			
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]    Script Date: 07/16/2013 14:28:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que elimina un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]
	@IdInfoAdmin INT
AS
BEGIN
	DELETE Proveedor.InfoAdminEntidad WHERE IdInfoAdmin = @IdInfoAdmin
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad]    Script Date: 07/16/2013 14:28:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT IdInfoAdmin, IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, 
 IdTipoRangosTrabajadores, IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, 
 IdDepartamentoDirComercial, IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, 
 PorctjPublico, SitioWeb, NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, 
 Mision, PQRS, GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, 
 PartMesasTerritoriales, PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, 
 NumSedes, SedesPropias, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[InfoAdminEntidad] 
 WHERE  IdEntidad = @IdEntidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]    Script Date: 07/16/2013 14:28:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]
	@IdInfoAdmin INT
AS
BEGIN
 SELECT IdInfoAdmin, IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, 
 IdTipoRangosTrabajadores, IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, 
 IdDepartamentoDirComercial, IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, 
 PorctjPublico, SitioWeb, NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, 
 Mision, PQRS, GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, 
 PartMesasTerritoriales, PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, 
 NumSedes, SedesPropias, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[InfoAdminEntidad] 
 WHERE  IdInfoAdmin = @IdInfoAdmin
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]    Script Date: 07/16/2013 14:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que guarda un nuevo cambio de estado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]
@IdTercero INT, @DatosBasicos BIT, @Financiera BIT, @Experiencia BIT, @IdTemporal NVARCHAR (20), @Motivo NVARCHAR (128), @UsuarioCrea NVARCHAR (250)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION t1
		INSERT INTO Proveedor.MotivoCambioEstado (IdTercero, IdTemporal, Motivo, DatosBasicos, Financiera, Experiencia, FechaCrea, UsuarioCrea)
			VALUES (@IdTercero, @IdTemporal, @Motivo, @DatosBasicos, @Financiera, @Experiencia, GETDATE(), @UsuarioCrea)


		DECLARE @PorValidar NVARCHAR (10)
		SET @PorValidar = (SELECT
			IdEstadoTercero
		FROM Oferente.EstadoTercero
		WHERE CodigoEstadotercero = ''001'')
		UPDATE Oferente.TERCERO
		SET IDESTADOTERCERO = @PorValidar
		WHERE IDTERCERO = @IdTercero

		--Se obtiene el id de el proveedor para asi actualizar su respectivo estado
		DECLARE @IdEntidad INT
		SET @IdEntidad = (SELECT DISTINCT TOP(1)
			Proveedor.EntidadProvOferente.IdEntidad
		FROM Proveedor.EntidadProvOferente
		INNER JOIN Oferente.TERCERO
			ON Proveedor.EntidadProvOferente.IdTercero = Oferente.TERCERO.IDTERCERO
		WHERE (Oferente.TERCERO.IDTERCERO = 430))
		
		PRINT ''BIEN''

		--Se actualiza el estado a Datos Básicos
		IF @DatosBasicos = 1
		BEGIN
		UPDATE Proveedor.EntidadProvOferente
		SET IdEstado = @PorValidar
		WHERE Proveedor.EntidadProvOferente.IdEntidad = @IdEntidad
		END
		--Se actualiza el estado a Financiera
		IF @Financiera = 1
		BEGIN
		UPDATE Proveedor.InfoFinancieraEntidad
		SET EstadoValidacion = @PorValidar
		WHERE Proveedor.InfoFinancieraEntidad.IdEntidad = @IdEntidad
		END
		--Se actualiza el estado a Experiencia
		IF @Experiencia = 1
		BEGIN
		UPDATE Proveedor.InfoExperienciaEntidad
		SET EstadoDocumental = @PorValidar
		WHERE Proveedor.InfoExperienciaEntidad.IdEntidad = @IdEntidad
		END
		
COMMIT TRANSACTION t1
END TRY
BEGIN CATCH
ROLLBACK TRANSACTION t1
SELECT
	ERROR_NUMBER() AS ErrorNumber,
	ERROR_MESSAGE() AS ErrorMessage;
END CATCH;
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]    Script Date: 07/16/2013 14:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]
	@IdEntidad INT = NULL, @IdVigencia INT = NULL
AS
BEGIN
 SELECT IdInfoFin, IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[InfoFinancieraEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END 
 ORDER BY IdVigencia DESC 
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]    Script Date: 07/16/2013 14:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]
		@IdInfoFin INT,	@IdEntidad INT,	@IdVigencia INT,	@ActivoCte NUMERIC(18, 3),	@ActivoTotal NUMERIC(18, 3),	@PasivoCte NUMERIC(18, 3),	@PasivoTotal NUMERIC(18, 3),	@Patrimonio NUMERIC(18, 3),	@GastosInteresFinancieros NUMERIC(18, 3),	@UtilidadOperacional NUMERIC(18, 3),	@ConfirmaIndicadoresFinancieros BIT,	@RupRenovado BIT,	@EstadoValidacion INT,	@ObservacionesInformacionFinanciera NVARCHAR(256),	@ObservacionesValidadorICBF NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.InfoFinancieraEntidad SET IdEntidad = @IdEntidad, IdVigencia = @IdVigencia, ActivoCte = @ActivoCte, ActivoTotal = @ActivoTotal, PasivoCte = @PasivoCte, PasivoTotal = @PasivoTotal, Patrimonio = @Patrimonio, GastosInteresFinancieros = @GastosInteresFinancieros, UtilidadOperacional = @UtilidadOperacional, ConfirmaIndicadoresFinancieros = @ConfirmaIndicadoresFinancieros, RupRenovado = @RupRenovado, EstadoValidacion = @EstadoValidacion, ObservacionesInformacionFinanciera = @ObservacionesInformacionFinanciera, ObservacionesValidadorICBF = @ObservacionesValidadorICBF, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdInfoFin = @IdInfoFin
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]    Script Date: 07/16/2013 14:28:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]
		@IdInfoFin INT, @IdEstadoInfoFinancieraEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	update [Proveedor].[InfoFinancieraEntidad]
	SET EstadoValidacion = @IdEstadoInfoFinancieraEntidad,
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdInfoFin = @IdInfoFin
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 07/16/2013 14:28:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
--Autor:Mauricio Martinez
--Fecha:2013/07/15 16:00
--Descripcion: Consulta para mostrar un resumen de los modulos validados

CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar] 
@IdEntidad INT 
as
begin
--hay información  en InfoDatosFinancierosEntidad y en InfoDatosExperienciaEntidad tienen por defecto el valor Sin validar en ConfirmaYAprueba
--Si no hay información en los módulos mostrar por defecto Sin información en ConfirmaYAprueba
select top 1 ''Datos Básicos'' as Componente, ISNULL(p.NroRevision,0) as NroRevision,ISNULL(case ConfirmaYAprueba when 1 then 1 when 0 then 0 else -1 end ,-2) as iConfirmaYAprueba
FROM Proveedor.ValidarInfoDatosBasicosEntidad v
			right join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = v.IdEntidad
			where p.IdEntidad  = @IdEntidad
			and ISNULL(p.NroRevision,0) = ISNULL(v.NroRevision,p.NroRevision)
--			order by ConfirmaYApruebaTexto ASC
union
select top 1 ''Financiera'' as Componente, ISNULL(p.NroRevision,0) as NroRevision,ISNULL(case ConfirmaYAprueba when 1 then 1 when 0 then 0 else -1 end ,-2) as iConfirmaYAprueba
FROM Proveedor.ValidarInfoFinancieraEntidad v
			right join [Proveedor].[InfoFinancieraEntidad] e on e.IdInfoFin = v.IdInfoFin
						inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
			where e.IdEntidad  = @IdEntidad
			and ISNULL(p.NroRevision,0) = ISNULL(v.NroRevision,p.NroRevision)
--			order by ConfirmaYApruebaTexto ASC
union
select top 1 ''Experiencias'' as Componente, ISNULL(p.NroRevision,0) as NroRevision,ISNULL(case ConfirmaYAprueba when 1 then 1 when 0 then 0 else -1 end ,-2) as iConfirmaYAprueba
FROM Proveedor.ValidarInfoExperienciaEntidad v
			right join [Proveedor].[InfoExperienciaEntidad] e on e.IdExpEntidad = v.IdExpEntidad
						inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
			where e.IdEntidad  = @IdEntidad
			and ISNULL(p.NroRevision,0) = ISNULL(v.NroRevision,p.NroRevision)
			--order by ConfirmaYApruebaTexto ASC
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]    Script Date: 07/16/2013 14:28:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]
	@IdInfoFin int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoFinancieraEntidad] 
 WHERE IdInfoFin = CASE WHEN @IdInfoFin IS NULL THEN IdInfoFin ELSE @IdInfoFin END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]    Script Date: 07/16/2013 14:28:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]
		@IdValidarInfoFinancieraEntidad INT OUTPUT, @IdInfoFin INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoFinancieraEntidad(IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdInfoFin, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoFinancieraEntidad = @@IDENTITY 		
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]    Script Date: 07/16/2013 14:28:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoFinancieraEntidad por IdInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]
	@IdInfoFin INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoFinancieraEntidad] 
 WHERE  IdInfoFin = @IdInfoFin
 ORDER BY IdValidarInfoFinancieraEntidad DESC
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]    Script Date: 07/16/2013 14:28:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]
	@IdValidarInfoFinancieraEntidad INT
AS
BEGIN
 SELECT IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoFinancieraEntidad] WHERE  IdValidarInfoFinancieraEntidad = @IdValidarInfoFinancieraEntidad
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]    Script Date: 07/16/2013 14:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]
		@IdInfoFin INT OUTPUT, 	@IdEntidad INT,	@IdVigencia INT,	@ActivoCte NUMERIC(18, 3),	@ActivoTotal NUMERIC(18, 3),	@PasivoCte NUMERIC(18, 3),	@PasivoTotal NUMERIC(18, 3),	@Patrimonio NUMERIC(18, 3),	@GastosInteresFinancieros NUMERIC(18, 3),	@UtilidadOperacional NUMERIC(18, 3),	@ConfirmaIndicadoresFinancieros BIT,	@RupRenovado BIT,	@EstadoValidacion INT,	@ObservacionesInformacionFinanciera NVARCHAR(256),	@ObservacionesValidadorICBF NVARCHAR(256), @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20) = NULL
AS
BEGIN
	INSERT INTO Proveedor.InfoFinancieraEntidad(IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @IdVigencia, @ActivoCte, @ActivoTotal, @PasivoCte, @PasivoTotal, @Patrimonio, @GastosInteresFinancieros, @UtilidadOperacional, @ConfirmaIndicadoresFinancieros, @RupRenovado, @EstadoValidacion, @ObservacionesInformacionFinanciera, @ObservacionesValidadorICBF, @UsuarioCrea, GETDATE())
	SELECT @IdInfoFin = @@IDENTITY 		
	
	UPDATE [Proveedor].[DocFinancieraProv] 
	set IdInfoFin = @IdInfoFin
	where IdTemporal = @IdTemporal
	
	SELECT @IdInfoFin
	
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]    Script Date: 07/16/2013 14:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]
	@IdDocNotJudicial INT = NULL,
	@IdNotJudicial INT = NULL,
	@IdDocumento INT = NULL
AS
BEGIN
 SELECT     Proveedor.DocNotificacionJudicial.IdDocNotJudicial, 
		    Proveedor.DocNotificacionJudicial.IdNotJudicial, 
		    Proveedor.DocNotificacionJudicial.IdDocumento, 
            Proveedor.DocNotificacionJudicial.Descripcion, 
            Proveedor.DocNotificacionJudicial.LinkDocumento, 
            Proveedor.DocNotificacionJudicial.Anno, 
            Proveedor.DocNotificacionJudicial.UsuarioCrea, 
            Proveedor.DocNotificacionJudicial.FechaCrea, 
            Proveedor.DocNotificacionJudicial.UsuarioModifica, 
            Proveedor.DocNotificacionJudicial.FechaModifica, 
            Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento
 FROM       Proveedor.DocNotificacionJudicial 
			INNER JOIN Proveedor.TipoDocumento 
			ON Proveedor.DocNotificacionJudicial.IdDocumento = Proveedor.TipoDocumento.IdTipoDocumento
 WHERE		IdDocNotJudicial = CASE WHEN @IdDocNotJudicial IS NULL THEN IdDocNotJudicial ELSE @IdDocNotJudicial END AND 
			IdNotJudicial = CASE WHEN @IdNotJudicial IS NULL THEN IdNotJudicial ELSE @IdNotJudicial END AND 
			IdDocumento = CASE WHEN @IdDocumento IS NULL THEN IdDocumento ELSE @IdDocumento END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]    Script Date: 07/16/2013 14:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que actualiza un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]
@IdDocNotJudicial INT,	
@IdNotJudicial INT,	
@IdDocumento INT,	
@Descripcion NVARCHAR(128),	
@LinkDocumento NVARCHAR(256),	
@Anno INT, 
@UsuarioModifica NVARCHAR(250)
AS
BEGIN
UPDATE Proveedor.DocNotificacionJudicial
SET	IdNotJudicial  = @IdNotJudicial,
	IdDocumento=@IdDocumento,
	Descripcion = @Descripcion,
	LinkDocumento = @LinkDocumento,
	Anno = @Anno,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdDocNotJudicial = @IdDocNotJudicial 
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]    Script Date: 07/16/2013 14:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]
		@IdDocNotJudicial INT OUTPUT , 	
		@IdNotJudicial INT , 	
		@IdDocumento INT , 	
		@Descripcion NVARCHAR(128),	
		@LinkDocumento NVARCHAR(256),	
		@Anno INT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.DocNotificacionJudicial(IdNotJudicial,IdDocumento, Descripcion, LinkDocumento, Anno, UsuarioCrea, FechaCrea)
					  VALUES(@IdNotJudicial,@IdDocumento, @Descripcion, @LinkDocumento, @Anno, @UsuarioCrea, GETDATE())
	SELECT @IdDocNotJudicial = @@IDENTITY 		
	RETURN @@IDENTITY	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]    Script Date: 07/16/2013 14:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]
	@IdDocNotJudicial INT
AS
BEGIN
	DELETE Proveedor.DocNotificacionJudicial 
	WHERE IdDocNotJudicial = @IdDocNotJudicial 
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]    Script Date: 07/16/2013 14:28:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Fabian Valencia
-- Create date: 02/07/2013
-- Description:	Obtiene los documentos de l proveedor notificaiones judiciales
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]
	@IdNotJudicial INT
AS
BEGIN
--	declare @IdNotJudicial int
--set @IdNotJudicial=13

declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''Proveedor/NotificacionJudicial'';
PRINT @IdPrograma
SELECT	MAX(IdDocNotJudicial) as IdDocNotJudicial,
		MAX(IdNotJudicial) as IdNotJudicial,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Descripcion) as Descripcion,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas
		
FROM 
(
SELECT     Proveedor.DocNotificacionJudicial.IdDocNotJudicial ,
			Proveedor.DocNotificacionJudicial.IdNotJudicial,t.Descripcion AS NombreDocumento,
			Proveedor.DocNotificacionJudicial.LinkDocumento, 
			Proveedor.DocNotificacionJudicial.Descripcion ,
			t.IdTipoDocumento  ,t.UsuarioCrea , t.FechaCrea, t.UsuarioModifica ,t.FechaModifica ,
			 
			tdp.MaxPermitidoKB, tdp.ExtensionesPermitidas                   
FROM         SEG.Programa AS p INNER JOIN
                      Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma INNER JOIN
                      Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento INNER JOIN
                      Proveedor.DocNotificacionJudicial ON tdp.IdTipoDocumento = Proveedor.DocNotificacionJudicial.IdDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) AND (Proveedor.DocNotificacionJudicial.IdNotJudicial = @IdNotJudicial)-- MODULO DE GESTION PROVEEDOR
		
UNION                      
SELECT       0 as IdDocNotJudicial,
			 0 as IdNotJudicial,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
			 '''' as Descripcion,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]    Script Date: 07/16/2013 14:28:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]
	@IdInfoFin int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal
 FROM [Proveedor].[DocFinancieraProv] 
 WHERE IdInfoFin = CASE WHEN @IdInfoFin IS NULL THEN IdInfoFin ELSE @IdInfoFin END
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]    Script Date: 07/16/2013 14:28:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]
		@IdDocAdjunto INT,	@IdInfoFin INT,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocFinancieraProv SET IdInfoFin = @IdInfoFin, NombreDocumento = @NombreDocumento, LinkDocumento = @LinkDocumento, Observaciones = @Observaciones, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDocAdjunto = @IdDocAdjunto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]    Script Date: 07/16/2013 14:28:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdInfoFin INT=NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @IdTipoDocumento int, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	INSERT INTO Proveedor.DocFinancieraProv(IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, IdTipoDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdInfoFin, @NombreDocumento, @LinkDocumento, @Observaciones, @IdTipoDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]    Script Date: 07/16/2013 14:28:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que elimina un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocFinancieraProv WHERE IdDocAdjunto = @IdDocAdjunto
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup]    Script Date: 07/16/2013 14:28:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta una lista de DocFinancieraProv por IdTemporal y si el Rup es renovado o no
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup] 
( @IdTemporal varchar(20), @RupRenovado varchar(128))
as

declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''PROVEEDOR/DocFinancieraProv'';


SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdInfoFin) as IdInfoFin,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdInfoFin,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
			--CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
			 CASE @RupRenovado	WHEN ''1'' THEN tdp.ObligRupRenovado 
								WHEN ''0'' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocFinancieraProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION FINANCIERA
		AND IdTemporal = @IdTemporal
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdInfoFin,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
			 '''' as Observaciones,
			 --CASE substring(CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
					 CASE @RupRenovado	WHEN ''1'' THEN tdp.ObligRupRenovado 
								WHEN ''0'' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '''' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION FINANCIERA
) as Tabla
Where Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]    Script Date: 07/16/2013 14:28:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta una lista de DocFinancieraProv por IdInfoFin y si el Rup es renovado o no
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup] 
( @IdInfoFin int, @RupRenovado varchar(128))
as

declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = ''PROVEEDOR/DocFinancieraProv'';


SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdInfoFin) as IdInfoFin,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdInfoFin,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
			--CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
			 CASE @RupRenovado	WHEN ''1'' THEN tdp.ObligRupRenovado 
								WHEN ''0'' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocFinancieraProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION FINANCIERA
		AND IdInfoFin = @IdInfoFin
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdInfoFin,
			 Descripcion   as NombreDocumento,
			 '''' as LinkDocumento,
			 '''' as Observaciones,
			 --CASE substring(CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
					 CASE @RupRenovado	WHEN ''1'' THEN tdp.ObligRupRenovado 
								WHEN ''0'' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '''' as UsuarioCrea,
			 t.FechaCrea,
			 '''' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '''' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION FINANCIERA
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar]    Script Date: 07/16/2013 14:28:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar]
	@IdDocAdjunto INT
AS
BEGIN
 SELECT IdDocAdjunto, IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal FROM [Proveedor].[DocFinancieraProv] WHERE  IdDocAdjunto = @IdDocAdjunto
END

' 
END
GO
