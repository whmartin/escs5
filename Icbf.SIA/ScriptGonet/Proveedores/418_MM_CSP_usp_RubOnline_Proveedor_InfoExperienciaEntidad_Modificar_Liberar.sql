USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]    Script Date: 07/30/2013 02:23:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]    Script Date: 07/30/2013 02:23:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--===================================================================================
--Autor: Mauricio Martinez
--Fecha: 2013/07/30
--Descripcion: Para liberar InfoExperiencia
--===================================================================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]
(@IdEntidad INT, @EstadoDocumental INT, @UsuarioModifica VARCHAR(256), @Finalizado BIT )
as
begin
	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = @Finalizado,
		EstadoDocumental = @EstadoDocumental
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		--and i.EstadoDocumental = 4 --SI ESTA VALIDADO SE FINALIZA
		--and v.ConfirmaYAprueba = 1 -- Y SI CONFIRM� CON SI

end

GO


