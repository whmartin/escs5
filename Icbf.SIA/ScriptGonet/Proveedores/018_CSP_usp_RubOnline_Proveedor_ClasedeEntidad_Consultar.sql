USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]    Script Date: 06/14/2013 19:31:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]
	@IdClasedeEntidad INT
AS
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[ClasedeEntidad] WHERE  IdClasedeEntidad = @IdClasedeEntidad
END
GO