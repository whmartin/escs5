
  USE [Proveedores]
  GO


  UPDATE [PROVEEDOR].[TipoDocumento]
  SET Descripcion = 'Registro Único Tributario - RUT'
  WHERE CodigoTipoDocumento = '101'
  GO

  UPDATE [PROVEEDOR].[TipoDocumento]
  SET Descripcion = 'Certificado de Registro Mercantil, o Tarjeta Profesional para profesionales liberales'
  WHERE CodigoTipoDocumento = '006'
  GO


  UPDATE [PROVEEDOR].[TipoDocumento]
  SET Descripcion = 'Copia tarjeta profesional contador o revisor fiscal'
  WHERE CodigoTipoDocumento = '003'
  GO


  UPDATE [PROVEEDOR].[TipoDocumento]
  SET Descripcion = 'Certificación del contrato ejecutado y liquidado o en ejecución'
  WHERE CodigoTipoDocumento = '010'
  GO




  UPDATE [PROVEEDOR].[TipoDocumento]
  SET Descripcion = 'Certificación de la Junta Central de Contadores, sobre su vigencia'
  WHERE CodigoTipoDocumento = '004'
  GO


  UPDATE [PROVEEDOR].[TipoDocumento]
  SET Descripcion = 'Certificado de registro único de proponentes - RUP'
  WHERE CodigoTipoDocumento = '007'
  GO

  
 