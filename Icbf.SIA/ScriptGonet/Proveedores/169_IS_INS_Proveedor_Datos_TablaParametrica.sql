USE [SIA]
GO

-- =============================================
-- Author:		Jonnathan Ni�o 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Registros de tablas parametricas
-- =============================================
IF NOT EXISTS( SELECT [CodigoTablaParametrica] FROM [Proveedor].[TablaParametrica] WHERE  [Url] = 'PROVEEDOR/TIPODOCIDENTIFICA') BEGIN
INSERT [Proveedor].[TablaParametrica] ([CodigoTablaParametrica], [NombreTablaParametrica], [Url], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (N'12', N'Tipo Documento Identificaci�n', N'PROVEEDOR/TIPODOCIDENTIFICA', 1, N'administrador', CAST(0x0000A1D601137B70 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT [CodigoTablaParametrica] FROM [Proveedor].[TablaParametrica] WHERE  [Url] = 'PROVEEDOR/TIPOCARGOENTIDAD') BEGIN
INSERT [Proveedor].[TablaParametrica] ([CodigoTablaParametrica], [NombreTablaParametrica], [Url], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (N'13', N'Tipo Cargo Entidad', N'PROVEEDOR/TIPOCARGOENTIDAD', 1, N'administrador', CAST(0x0000A1D601137B70 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT [CodigoTablaParametrica] FROM [Proveedor].[TablaParametrica] WHERE  [Url] = 'PROVEEDOR/TIPOCODIGOUNSPSC') BEGIN
INSERT [Proveedor].[TablaParametrica] ([CodigoTablaParametrica], [NombreTablaParametrica], [Url], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (N'14', N'Tipo C�digo UNSPSC', N'PROVEEDOR/TIPOCODIGOUNSPSC', 1, N'administrador', CAST(0x0000A1D601137B70 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT [CodigoTablaParametrica] FROM [Proveedor].[TablaParametrica] WHERE  [Url] = 'PROVEEDOR/ESTADODATOSBASICOS') BEGIN
INSERT [Proveedor].[TablaParametrica] ([CodigoTablaParametrica], [NombreTablaParametrica], [Url], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (N'15', N'Estado Datos B�sicos', N'PROVEEDOR/ESTADODATOSBASICOS', 1, N'administrador', CAST(0x0000A1D601137B70 AS DateTime), NULL, NULL)
END
