USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Creacion de tabla para almacenar Documentacion Financiera de Proveedores
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_InfoFinancieraEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv] DROP CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocFinancieraProv_TipoDocumento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]'))
ALTER TABLE [Proveedor].[DocFinancieraProv] DROP CONSTRAINT [FK_DocFinancieraProv_TipoDocumento]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[DocFinancieraProv]    Script Date: 06/24/2013 12:15:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocFinancieraProv]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocFinancieraProv]
GO

USE [SIA]
GO

/****** Object:  Table [Proveedor].[DocFinancieraProv]    Script Date: 06/24/2013 12:15:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[DocFinancieraProv](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdInfoFin] [int] NOT NULL,
	[NombreDocumento] [nvarchar](128) NOT NULL,
	[LinkDocumento] [nvarchar](256) NOT NULL,
	[Observaciones] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdTipoDocumento] [int] NULL,
 CONSTRAINT [PK_DocFinancieraProv] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[DocFinancieraProv]  WITH CHECK ADD  CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad] FOREIGN KEY([IdInfoFin])
REFERENCES [Proveedor].[InfoFinancieraEntidad] ([IdInfoFin])
GO

ALTER TABLE [Proveedor].[DocFinancieraProv] CHECK CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad]
GO

ALTER TABLE [Proveedor].[DocFinancieraProv]  WITH CHECK ADD  CONSTRAINT [FK_DocFinancieraProv_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [Proveedor].[TipoDocumento] ([IdTipoDocumento])
GO

ALTER TABLE [Proveedor].[DocFinancieraProv] CHECK CONSTRAINT [FK_DocFinancieraProv_TipoDocumento]
GO


