USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]    Script Date: 06/24/2013 21:36:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]    Script Date: 06/24/2013 21:36:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdEntidad, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[DocDatosBasicoProv] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END
END

GO


