USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en  [Proveedor].[TipodeActividad]
-- =============================================
SET IDENTITY_INSERT [Proveedor].[TipodeActividad] ON
INSERT [Proveedor].[TipodeActividad] ([IdTipodeActividad], [CodigoTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'CON ANIMO DE LUCRO', 1, N'Administrador', CAST(0x0000A1DC013C6BC3 AS DateTime), N'Administrador', CAST(0x0000A1DC0158F0E3 AS DateTime))
INSERT [Proveedor].[TipodeActividad] ([IdTipodeActividad], [CodigoTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'SIN ANIMO DE LUCRO', 1, N'Administrador', CAST(0x0000A1DC013C7ECC AS DateTime), N'Administrador', CAST(0x0000A1DC015901D8 AS DateTime))
INSERT [Proveedor].[TipodeActividad] ([IdTipodeActividad], [CodigoTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'QQQ', N'QQ', 1, N'Administrador', CAST(0x0000A1DD01350872 AS DateTime), N'Administrador', CAST(0x0000A1DD01350C25 AS DateTime))
SET IDENTITY_INSERT [Proveedor].[TipodeActividad] OFF