
USE [SIA]
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  7/23/2013 1:25:13 PM
-- Description:	Permite crear las tablas TipodeActividad , ClasedeEntidad y TipoEntidad


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_InfoAdminEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[InfoAdminEntidad]'))
ALTER TABLE [Proveedor].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad]
GO


/****** Object:  ForeignKey [FK_ClasedeEntidad_TipodeActividad]    Script Date: 07/23/2013 23:50:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad] DROP CONSTRAINT [FK_ClasedeEntidad_TipodeActividad]
GO
/****** Object:  ForeignKey [FK_ClasedeEntidad_Tipoentidad]    Script Date: 07/23/2013 23:50:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad] DROP CONSTRAINT [FK_ClasedeEntidad_Tipoentidad]
GO
/****** Object:  Table [Proveedor].[ClasedeEntidad]    Script Date: 07/23/2013 23:50:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[ClasedeEntidad]
GO
/****** Object:  Table [Proveedor].[TipodeActividad]    Script Date: 07/23/2013 23:50:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipodeActividad]
GO
/****** Object:  Table [Proveedor].[TipoEntidad]    Script Date: 07/23/2013 23:50:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoEntidad]
GO
/****** Object:  Table [Proveedor].[TipoEntidad]    Script Date: 07/23/2013 23:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoEntidad](
	[IdTipoentidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoentidad] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Descripcion] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Tipoentidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoentidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoEntidad]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipoEntidad] 
(
	[CodigoTipoentidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoEntidad]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipoEntidad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [Proveedor].[TipoEntidad] ON
INSERT [Proveedor].[TipoEntidad] ([IdTipoentidad], [CodigoTipoentidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'002', N'NACIONAL', 1, N'Administrador', CAST(0x0000A1DB00C4C2B6 AS DateTime), NULL, NULL)
INSERT [Proveedor].[TipoEntidad] ([IdTipoentidad], [CodigoTipoentidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'001', N'EXTRANJERA', 1, N'Administrador', CAST(0x0000A1DB00C4D861 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[TipoEntidad] OFF
/****** Object:  Table [Proveedor].[TipodeActividad]    Script Date: 07/23/2013 23:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipodeActividad](
	[IdTipodeActividad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipodeActividad] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Descripcion] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipodeActividad] PRIMARY KEY CLUSTERED 
(
	[IdTipodeActividad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[TipodeActividad] 
(
	[CodigoTipodeActividad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipodeActividad]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[TipodeActividad] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [Proveedor].[TipodeActividad] ON
INSERT [Proveedor].[TipodeActividad] ([IdTipodeActividad], [CodigoTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'CON ANIMO DE LUCRO', 1, N'Administrador', CAST(0x0000A1DC013C6BC3 AS DateTime), N'Administrador', CAST(0x0000A1DC0158F0E3 AS DateTime))
INSERT [Proveedor].[TipodeActividad] ([IdTipodeActividad], [CodigoTipodeActividad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'SIN ANIMO DE LUCRO', 1, N'Administrador', CAST(0x0000A1DC013C7ECC AS DateTime), N'Administrador', CAST(0x0000A1DC015901D8 AS DateTime))
SET IDENTITY_INSERT [Proveedor].[TipodeActividad] OFF
/****** Object:  Table [Proveedor].[ClasedeEntidad]    Script Date: 07/23/2013 23:50:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[ClasedeEntidad](
	[IdClasedeEntidad] [int] IDENTITY(1,1) NOT NULL,
	[IdTipodeEntidad] [int] NOT NULL,
	[IdTipodeActividad] [int] NOT NULL,
	[CodigoClasedeEntidad] [varchar](10) COLLATE Latin1_General_CI_AS NULL,
	[Descripcion] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ClasedeEntidad] PRIMARY KEY CLUSTERED 
(
	[IdClasedeEntidad] ASC,
	[IdTipodeEntidad] ASC,
	[IdTipodeActividad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [Proveedor].[ClasedeEntidad] ON
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, 1, 1, N'01', N'SOCIEDAD ANÓNIMA', 1, N'Administrador', CAST(0x0000A1DC014F5818 AS DateTime), NULL, NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, 1, 1, N'02', N'SOCIEDAD DE RESPONSABILIDAD LTDA', 1, N'Administrador', CAST(0x0000A1DC014FA183 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, 1, 1, N'03', N'SOCIEDAD POR ACCIONES SIMPLIFICADA -SAS', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, 1, 1, N'04', N'SOCIEDAD COLECTIVA', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, 1, 1, N'05', N'SOCIEDAD ANONIMA', 1, N'Administrador', CAST(0x0000A1DC014F5818 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (6, 1, 1, N'06', N'FUNDACIONES', 1, N'Administrador', CAST(0x0000A1DC014FA183 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (7, 1, 1, N'07', N'SOCIEDAD COMANDITARIA SIMPLE', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (8, 1, 1, N'08', N'SOCIEDAD COMANDITARIA POR ACCIÓN.', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (9, 1, 1, N'09', N'UNIPERSONAL', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (10, 1, 1, N'10', N'PROFESIONALES LIBERALES', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (11, 1, 1, N'11', N'SOCIEDAD DE ECONOMIA MIXTA', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (12, 1, 2, N'12', N'FUNDACIONES', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (13, 1, 2, N'13', N'CAJA DE COMPENSACIÓN', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (14, 1, 2, N'14', N'COOPERATIVAS', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (15, 1, 2, N'15', N'ASOCIACIONES', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (16, 1, 2, N'16', N'ONG', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (17, 1, 2, N'17', N'SECTOR SOLIDARIO', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (18, 1, 2, N'18', N'ORGANIZACIÓN RELIGIOSA', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (19, 1, 2, N'19', N'JUNTAS DE ACCION COMUNAL', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (20, 1, 2, N'20', N'FONDO DE EMPLEADOS', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (21, 1, 2, N'21', N'SOCIEDAD DE ECONOMIA MIXTA', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (22, 1, 2, N'22', N'PROPIEDAD HORIZONTAL', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (23, 2, 1, N'23', N'SOCIEDAD ANONIMA', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (24, 2, 1, N'24', N'SUCURSAL DE SOCIEDAD EXTRANJERA', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (25, 2, 2, N'25', N'ONG', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
INSERT [Proveedor].[ClasedeEntidad] ([IdClasedeEntidad], [IdTipodeEntidad], [IdTipodeActividad], [CodigoClasedeEntidad], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (26, 2, 2, N'26', N'SUCURSAL DE SOCIEDAD EXTRANJERA', 1, N'Administrador', CAST(0x0000A1DC014FB238 AS DateTime), N'', NULL)
SET IDENTITY_INSERT [Proveedor].[ClasedeEntidad] OFF
/****** Object:  ForeignKey [FK_ClasedeEntidad_TipodeActividad]    Script Date: 07/23/2013 23:50:25 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad]  WITH CHECK ADD  CONSTRAINT [FK_ClasedeEntidad_TipodeActividad] FOREIGN KEY([IdTipodeActividad])
REFERENCES [Proveedor].[TipodeActividad] ([IdTipodeActividad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_TipodeActividad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad] CHECK CONSTRAINT [FK_ClasedeEntidad_TipodeActividad]
GO
/****** Object:  ForeignKey [FK_ClasedeEntidad_Tipoentidad]    Script Date: 07/23/2013 23:50:25 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad]  WITH CHECK ADD  CONSTRAINT [FK_ClasedeEntidad_Tipoentidad] FOREIGN KEY([IdTipodeEntidad])
REFERENCES [Proveedor].[TipoEntidad] ([IdTipoentidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ClasedeEntidad_Tipoentidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ClasedeEntidad]'))
ALTER TABLE [Proveedor].[ClasedeEntidad] CHECK CONSTRAINT [FK_ClasedeEntidad_Tipoentidad]
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad] FOREIGN KEY([IdTipoActividad])
REFERENCES [Proveedor].[TipodeActividad] ([IdTipodeActividad])
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad]
GO


ALTER TABLE [Proveedor].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad] FOREIGN KEY([IdTipoEntidad])
REFERENCES [Proveedor].[TipoEntidad] ([IdTipoentidad])
GO

ALTER TABLE [Proveedor].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad]
GO


