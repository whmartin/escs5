USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]    Script Date: 06/24/2013 21:35:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]    Script Date: 06/24/2013 21:35:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]
		@IdDocAdjunto INT,	@IdEntidad INT,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocDatosBasicoProv SET IdEntidad = @IdEntidad, NombreDocumento = @NombreDocumento, LinkDocumento = @LinkDocumento, Observaciones = @Observaciones, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDocAdjunto = @IdDocAdjunto
END

GO


