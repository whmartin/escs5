USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]    Script Date: 07/22/2013 21:35:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]    Script Date: 07/22/2013 21:35:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Fabi�n Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero y los proveedores que tengan estado validado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]

@isTercero int = NULL,
@IDTIPODOCIDENTIFICA int = NULL,
@IdTipoPersona int = NULL,
@NUMEROIDENTIFICACION varchar (30) = NULL,
@USUARIOCREA varchar (128) = NULL,
@Tercero varchar (128) = NULL
AS
BEGIN
IF @isTercero = 1
BEGIN
		SELECT DISTINCT
			Oferente.TERCERO.IDTERCERO,
			Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural,
			Oferente.TERCERO.NUMEROIDENTIFICACION,
			Oferente.TERCERO.PRIMERNOMBRE,
			Oferente.TERCERO.SEGUNDONOMBRE,
			Oferente.TERCERO.PRIMERAPELLIDO,
			Oferente.TERCERO.SEGUNDOAPELLIDO,
			Oferente.TERCERO.RAZONSOCIAL ,
			Oferente.TERCERO.FECHACREA,
			Oferente.EstadoTercero.CodigoEstadotercero,
			Oferente.EstadoTercero.DescripcionEstado
		FROM Oferente.TERCERO
		INNER JOIN Global.TiposDocumentos
			ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
		INNER JOIN Oferente.EstadoTercero
			ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
		INNER JOIN Oferente.TipoPersona
			ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
		WHERE Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END
		AND Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END
		AND Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END
		AND Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END
		AND (
		Oferente.TERCERO.PRIMERNOMBRE LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERNOMBRE ELSE '%' + @Tercero + '%'
		END
		OR Oferente.TERCERO.SEGUNDONOMBRE LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDONOMBRE ELSE '%' + @Tercero + '%'
		END
		OR Oferente.TERCERO.PRIMERAPELLIDO LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERAPELLIDO ELSE '%' + @Tercero + '%'
		END
		OR Oferente.TERCERO.SEGUNDOAPELLIDO LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDOAPELLIDO ELSE '%' + @Tercero + '%'
		END
		OR Oferente.TERCERO.RAZONSOCIAL LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.RAZONSOCIAL ELSE '%' + @Tercero + '%'
		END
		)
		AND Oferente.EstadoTercero.CodigoEstadotercero = '004' --Estado validado
END
ELSE
BEGIN
		DECLARE @Validado INT
		SET @Validado = (SELECT
			IdEstadoDatosBasicos 
		FROM Proveedor.EstadoDatosBasicos    
		WHERE Descripcion  = 'VALIDADO')
		PRINT @Validado
		
		SELECT DISTINCT
			Oferente.TERCERO.IDTERCERO,
			Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural,
			Oferente.TERCERO.NUMEROIDENTIFICACION,
			Oferente.TERCERO.PRIMERNOMBRE,
			Oferente.TERCERO.SEGUNDONOMBRE,
			Oferente.TERCERO.PRIMERAPELLIDO,
			Oferente.TERCERO.SEGUNDOAPELLIDO,
			Oferente.TERCERO.RAZONSOCIAL ,
			Oferente.TERCERO.FECHACREA,
			Oferente.EstadoTercero.CodigoEstadotercero,
			Oferente.EstadoTercero.DescripcionEstado
		FROM Oferente.TERCERO
		INNER JOIN Global.TiposDocumentos
			ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
		INNER JOIN Oferente.EstadoTercero
			ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
		INNER JOIN Oferente.TipoPersona
			ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
		INNER JOIN Proveedor.EntidadProvOferente
			ON Oferente.TERCERO.IDTERCERO = Proveedor.EntidadProvOferente.IdTercero
		WHERE (Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.PRIMERNOMBRE LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERNOMBRE ELSE '%' + @Tercero + '%'
		END) OR
		(Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.SEGUNDONOMBRE LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDONOMBRE ELSE '%' + @Tercero + '%'
		END) OR
		(Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.PRIMERAPELLIDO LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.PRIMERAPELLIDO ELSE '%' + @Tercero + '%'
		END) OR
		(Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.SEGUNDOAPELLIDO LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.SEGUNDOAPELLIDO ELSE '%' + @Tercero + '%'
		END) OR
		(Oferente.TERCERO.IDTIPODOCIDENTIFICA =
			CASE
				WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
			END) AND (Oferente.TERCERO.IdTipoPersona =
			CASE
				WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
			END) AND
		(Oferente.TERCERO.NUMEROIDENTIFICACION =
			CASE
				WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
			END) AND
		(Oferente.TERCERO.USUARIOCREA =
			CASE
				WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
			END) AND
		(Oferente.TERCERO.RAZONSOCIAL LIKE CASE
			WHEN @Tercero IS NULL THEN Oferente.TERCERO.RAZONSOCIAL ELSE '%' + @Tercero + '%'
		END)
		AND Proveedor.EntidadProvOferente.IdEstado  = @Validado   --Estado validado
END

END
GO


