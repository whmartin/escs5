USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]    Script Date: 06/24/2013 12:23:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]    Script Date: 06/24/2013 12:23:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  20/06/2013
-- Description:	Procedimiento almacenado que consulta las vigencias sin incluir el año actual
-- =============================================
create PROCEDURE [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]
	@Activo NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Global].[Vigencia] 
 WHERE AcnoVigencia < Year(getdate())
 AND Activo = CASE WHEN @Activo IS NULL THEN Activo ELSE @Activo END
END
