USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina varios CONSTRAINT de la entidad [Proveedor].[Sucursal]
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_Departamento]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_IdEntidad]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_IdEntidad]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_Sucursal_Municipio]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[Sucursal]'))
ALTER TABLE [Proveedor].[Sucursal] DROP CONSTRAINT [FK_Sucursal_Municipio]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina la entidad [Proveedor].[Sucursal]
-- =============================================
/****** Object:  Table [Proveedor].[Sucursal]    Script Date: 06/26/2013 09:56:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Sucursal]') AND type in (N'U'))
DROP TABLE [Proveedor].[Sucursal]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Cres la entidad [Proveedor].[Sucursal]
-- =============================================
/****** Object:  Table [Proveedor].[Sucursal]    Script Date: 06/26/2013 09:56:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[Sucursal](
	[IdSucursal] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[Indicativo] [int] NOT NULL,
	[Telefono] [int] NOT NULL,
	[Extension] [int] NULL,
	[Celular] [numeric](10, 0) NOT NULL,
	[Correo] [nvarchar](256) NULL,
	[Estado] [int] NOT NULL,
	[Departamento] [int] NOT NULL,
	[Municipio] [int] NOT NULL,
	[IdZona] [int] NULL,
	[Direccion] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[Nombre] [nvarchar](255) NULL,
 CONSTRAINT [PK_IdSucursal] PRIMARY KEY CLUSTERED 
(
	[IdSucursal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_Departamento] FOREIGN KEY([Departamento])
REFERENCES [DIV].[Departamento] ([IdDepartamento])
GO

ALTER TABLE [Proveedor].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Departamento]
GO

ALTER TABLE [Proveedor].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_IdEntidad] FOREIGN KEY([IdEntidad])
REFERENCES [Proveedor].[EntidadProvOferente] ([IdEntidad])
GO

ALTER TABLE [Proveedor].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_IdEntidad]
GO

ALTER TABLE [Proveedor].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_Municipio] FOREIGN KEY([Municipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO

ALTER TABLE [Proveedor].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Municipio]
GO

