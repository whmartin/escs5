USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]    Script Date: 10/16/2014 17:38:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]    Script Date: 10/16/2014 17:38:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 26/08/2014
-- Description:	Consulta un Tercero por parametro CorreoElectronico
-- Modificaci�n: Juan Carlos Valverde S�mano
-- Fecha: 2014/10/16
-- Descripci�n: Se modific� ya que en terceros buscaba el registro con el parametro
-- de entrada @correoElectronico que est� recibiendo el correo electr�nico de la cuenta de usuario
-- con que se cre� el tercero. Esto entraba en conflicto cuando se modificaba la cuenta de correo de
-- registro del TERCERO. Entonces ahora se obtiene el ProviderKey del usuario y se busca en 
-- TERCEROS el tercero creado con tal providerkey.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]
	@correoElectronico NVARCHAR (50)
AS
BEGIN

	SELECT
		IDTERCERO,
		Numeroidentificacion,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica,
		CreadoPorInterno
	FROM oferente.TERCERO
	WHERE ProviderUserKey=(	
	SELECT providerKey FROM SEG.Usuario
	WHERE CorreoElectronico=@correoElectronico)
END




GO


