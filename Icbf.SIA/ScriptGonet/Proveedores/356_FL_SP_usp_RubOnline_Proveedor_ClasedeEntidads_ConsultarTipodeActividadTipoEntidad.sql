USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]    Script Date: 07/23/2013 23:55:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]    Script Date: 07/23/2013 23:55:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]
	@IdTipodeActividad INT = NULL,@IdTipoEntidad INT = NULL
AS
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[ClasedeEntidad] 
 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
	   AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
	   AND Estado = 1
END

GO


