USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]    Script Date: 06/14/2013 19:32:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('usp_RubOnline_Proveedor_Tipoentidad_Consultar') IS NOT NULL
BEGIN
	DROP PROCEDURE usp_RubOnline_Proveedor_Tipoentidad_Consultar
END
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]
	@IdTipoentidad INT
AS
BEGIN
 SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Tipoentidad] WHERE  IdTipoentidad = @IdTipoentidad
END
GO