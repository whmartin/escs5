USE [SIA]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/16/2013 12:40:25 PM
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_ContactoEntidad_TelTerceros]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[ContactoEntidad]'))
ALTER TABLE [Proveedor].[ContactoEntidad] DROP CONSTRAINT [FK_ContactoEntidad_TelTerceros]
GO
/****** Object:  Table [Proveedor].[ContactoEntidad]    Script Date: 06/25/2013 18:17:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ContactoEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[ContactoEntidad]
GO
CREATE TABLE [Proveedor].[ContactoEntidad](
 [IdContacto] [INT]  IDENTITY(1,1) NOT NULL,
 [IdEntidad] [INT]  NOT NULL,
 [IdSucursal] [INT] NOT NULL,
 [IdTelContacto] [INT]  NOT NULL,
 [IdTipoDocIdentifica] [INT]  NOT NULL,
 [IdTipoCargoEntidad] [INT]  NOT NULL,
 [NumeroIdentificacion] [NUMERIC] (30) NOT NULL,
 [PrimerNombre] [NVARCHAR] (128) NOT NULL,
 [SegundoNombre] [NVARCHAR] (128) NOT NULL,
 [PrimerApellido] [NVARCHAR] (128) NOT NULL,
 [SegundoApellido] [NVARCHAR] (128) NOT NULL,
 [Dependencia] [NVARCHAR] (128) NOT NULL,
 [Email] [NVARCHAR] (128) NOT NULL,
 [Estado] [BIT]  NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_ContactoEntidad] PRIMARY KEY CLUSTERED 
(
 	[IdContacto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Oferente.TelTerceros SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Proveedor.ContactoEntidad ADD CONSTRAINT
	FK_ContactoEntidad_TelTerceros FOREIGN KEY
	(
	IdTelContacto
	) REFERENCES Oferente.TelTerceros
	(
	IdTelTercero
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE Proveedor.ContactoEntidad SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Proveedor.TipoCargoEntidad SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Proveedor.ContactoEntidad ADD CONSTRAINT
	FK_ContactoEntidad_TipoCargoEntidad FOREIGN KEY
	(
	IdTipoCargoEntidad
	) REFERENCES Proveedor.TipoCargoEntidad
	(
	IdTipoCargoEntidad
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE Proveedor.ContactoEntidad SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



--ContactoEntidad-TipoDocIdentifica
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Proveedor.TipoDocIdentifica SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Proveedor.ContactoEntidad ADD CONSTRAINT
	FK_ContactoEntidad_TipoDocIdentifica FOREIGN KEY
	(
	IdTipoDocIdentifica
	) REFERENCES Proveedor.TipoDocIdentifica
	(
	IdTipoDocIdentifica
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE Proveedor.ContactoEntidad SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


--Email �nico
CREATE UNIQUE NONCLUSTERED INDEX [EmailUnico] ON [Proveedor].[ContactoEntidad]
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO