USE [SIA]
GO

-- =============================================
-- Author:          Juan Carlos Valverde Sámano    
-- Create date:         17/01/2014
-- Description:     Registra una tabla paramétrica en Proveedor.TablaParametrica
-- =============================================

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]    Script Date: 06/14/2013 19:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]
		@IdTablaParametrica INT OUTPUT, 	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TablaParametrica(CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTablaParametrica, @NombreTablaParametrica, @Url, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTablaParametrica = @@IDENTITY 		
END
GO