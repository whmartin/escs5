/****** Object:  Trigger [PROVEEDOR].[TG_Aud_TablaParametrica]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para TablaParametrica
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_TablaParametrica]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_TablaParametrica]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_TablaParametrica]
  ON  [PROVEEDOR].[TablaParametrica]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_TablaParametrica]
  ([IdTablaParametrica],[CodigoTablaParametrica_old], [CodigoTablaParametrica_new],[NombreTablaParametrica_old], [NombreTablaParametrica_new],[Url_old], [Url_new],[Estado_old], [Estado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdTablaParametrica,deleted.CodigoTablaParametrica,inserted.CodigoTablaParametrica,deleted.NombreTablaParametrica,inserted.NombreTablaParametrica,deleted.Url,inserted.Url,deleted.Estado,inserted.Estado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdTablaParametrica = deleted.IdTablaParametricaEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_TablaParametrica]
  ([IdTablaParametrica],[CodigoTablaParametrica_old],[NombreTablaParametrica_old],[Url_old],[Estado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdTablaParametrica,deleted.CodigoTablaParametrica,deleted.NombreTablaParametrica,deleted.Url,deleted.Estado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_TablaParametrica]
  ([IdTablaParametrica],[CodigoTablaParametrica_new],[NombreTablaParametrica_new],[Url_new],[Estado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdTablaParametrica,inserted.CodigoTablaParametrica,inserted.NombreTablaParametrica,inserted.Url,inserted.Estado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO