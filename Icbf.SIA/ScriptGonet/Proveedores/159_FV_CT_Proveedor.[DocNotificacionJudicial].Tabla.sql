USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Elimina CONSTRAINT de la tabla [Proveedor].[DocNotificacionJudicial]
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_DocNotificacionJudicial_NotificacionJudicial]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]'))
ALTER TABLE [Proveedor].[DocNotificacionJudicial] DROP CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Elimina la tabla [Proveedor].[DocNotificacionJudicial]
-- =============================================
/****** Object:  Table [Proveedor].[DocNotificacionJudicial]    Script Date: 06/24/2013 12:45:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[DocNotificacionJudicial]') AND type in (N'U'))
DROP TABLE [Proveedor].[DocNotificacionJudicial]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         19/01/2014
-- Description:     Crea la entidad [Proveedor].[DocNotificacionJudicial]
-- =============================================
/****** Object:  Table [Proveedor].[DocNotificacionJudicial]    Script Date: 06/24/2013 12:45:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[DocNotificacionJudicial](
	[IdDocNotJudicial] [int] IDENTITY(1,1) NOT NULL,
	[IdNotJudicial] [int] NULL,
	[IdDocumento] [int] NULL,
	[Descripcion] [nvarchar](128) NULL,
	[LinkDocumento] [nvarchar](256) NULL,
	[Anno] [numeric](4, 0) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_DocNotificacionJudicial] PRIMARY KEY CLUSTERED 
(
	[IdDocNotJudicial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Proveedor].[DocNotificacionJudicial]  WITH CHECK ADD  CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial] FOREIGN KEY([IdNotJudicial])
REFERENCES [Proveedor].[NotificacionJudicial] ([IdNotJudicial])
GO

ALTER TABLE [Proveedor].[DocNotificacionJudicial] CHECK CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial]
GO


