USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]    Script Date: 06/25/2013 21:39:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]    Script Date: 06/25/2013 21:39:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Fabian Valencia
-- Create date: 25/06/2013
-- Description:	Obtiene el estado a trav�s de su c�digo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]
	@codigoTercero NVARCHAR (5)
AS
BEGIN
	SELECT
		IdEstadoTercero,
		CodigoEstadotercero,
		DescripcionEstado,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM Oferente.EstadoTercero
	WHERE (CodigoEstadotercero = @codigoTercero)
END


GO


