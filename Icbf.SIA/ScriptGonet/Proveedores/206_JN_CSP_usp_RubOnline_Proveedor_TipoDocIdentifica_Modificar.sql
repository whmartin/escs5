USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]
		@IdTipoDocIdentifica INT,	@CodigoDocIdentifica NUMERIC,	@Descripcion NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoDocIdentifica 
	SET CodigoDocIdentifica = @CodigoDocIdentifica, Descripcion = @Descripcion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdTipoDocIdentifica = @IdTipoDocIdentifica
END
