


USE [SIA]
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  7/23/2013 1:25:13 PM
-- Description:	

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_Niveldegobierno]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO

/****** Object:  Table [Proveedor].[RamaoEstructura_Niveldegobierno]    Script Date: 07/24/2013 08:58:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura_Niveldegobierno]') AND type in (N'U'))
DROP TABLE [Proveedor].[RamaoEstructura_Niveldegobierno]
GO

/****** Object:  Table [Proveedor].[Niveldegobierno]    Script Date: 07/24/2013 08:58:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND type in (N'U'))
DROP TABLE [Proveedor].[Niveldegobierno]
GO

/****** Object:  Table [Proveedor].[RamaoEstructura]    Script Date: 07/24/2013 08:58:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND type in (N'U'))
DROP TABLE [Proveedor].[RamaoEstructura]
GO

/****** Object:  Table [Proveedor].[RamaoEstructura_Niveldegobierno]    Script Date: 07/24/2013 08:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura_Niveldegobierno]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[RamaoEstructura_Niveldegobierno](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdRamaEstructura] [int] NOT NULL,
	[IdNiveldegobierno] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
 CONSTRAINT [PK_RamaoEstructura_Niveldegobierno] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[IdNiveldegobierno] ASC,
	[IdRamaEstructura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ON
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (1, 1, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (2, 1, 2, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (3, 1, 3, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (4, 1, 4, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (5, 2, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (6, 3, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (7, 4, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (8, 4, 4, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (9, 4, 3, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (10, 5, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (11, 6, 1, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (12, 6, 2, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (13, 6, 3, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] ([Id], [IdRamaEstructura], [IdNiveldegobierno], [Estado], [UsuarioCrea], [FechaCrea]) VALUES (14, 6, 4, 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime))
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura_Niveldegobierno] OFF
/****** Object:  Table [Proveedor].[RamaoEstructura]    Script Date: 07/24/2013 08:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[RamaoEstructura](
	[IdRamaEstructura] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRamaEstructura] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Descripcion] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RamaoEstructura] PRIMARY KEY CLUSTERED 
(
	[IdRamaEstructura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[RamaoEstructura] 
(
	[CodigoRamaEstructura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[RamaoEstructura] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura] ON
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'EJECUTIVA', 1, N'Administrador', CAST(0x0000A1DB00D4D18A AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'JUDICIAL', 1, N'Administrador', CAST(0x0000A1DB00D4E726 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'003', N'LEGISLATIVA', 1, N'Administrador', CAST(0x0000A1DB00D4F782 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, N'004', N'�RGANOS DE CONTROL', 1, N'Administrador', CAST(0x0000A1DB00D50899 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'005', N'�RGANOS ELECTORALES', 1, N'Administrador', CAST(0x0000A1DB00D51804 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (6, N'006', N'�RGANOS AUT�NOMOS', 1, N'Administrador', CAST(0x0000A1DB00D526CC AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura] OFF
/****** Object:  Table [Proveedor].[Niveldegobierno]    Script Date: 07/24/2013 08:58:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[Niveldegobierno](
	[IdNiveldegobierno] [int] IDENTITY(1,1) NOT NULL,
	[IdRamaEstructura] [int] NOT NULL,
	[CodigoNiveldegobierno] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Descripcion] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Niveldegobierno] PRIMARY KEY CLUSTERED 
(
	[IdNiveldegobierno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[Niveldegobierno] 
(
	[CodigoNiveldegobierno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [Proveedor].[Niveldegobierno] ON
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, 1, N'01', N'NACIONAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, 1, N'02', N'DEPARTAMENTAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, 1, N'03', N'DISTRITAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, 1, N'04', N'MUNICIPAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, 2, N'05', N'NACIONAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (6, 3, N'06', N'NACIONAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (7, 4, N'07', N'NACIONAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (8, 4, N'08', N'MUNICIPAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (9, 4, N'09', N'DISTRITAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (10, 5, N'10', N'NACIONAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (11, 6, N'11', N'NACIONAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (12, 6, N'12', N'DEPARTAMENTAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (13, 6, N'13', N'DISTRITAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [IdRamaEstructura], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (14, 6, N'14', N'MUNICIPAL', 1, N'Administrador', CAST(0x0000A20400000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[Niveldegobierno] OFF

go

ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno] FOREIGN KEY([IdTipoNivelGob])
REFERENCES [Proveedor].[Niveldegobierno] ([IdNiveldegobierno])
GO

ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO

