/****** Object:  Table [Auditoria].[PROVEEDOR_DocExperienciaEntidad]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para DocExperienciaEntidad ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_DocExperienciaEntidad]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_DocExperienciaEntidad]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_DocExperienciaEntidad]([IdDocAdjunto] [int]  NULL, [IdExpEntidad_old] [int]  NULL,[IdExpEntidad_new] [int]  NULL,[IdTipoDocumento_old] [int]  NULL,[IdTipoDocumento_new] [int]  NULL,[Descripcion_old] [nvarchar] (128)  NULL,[Descripcion_new] [nvarchar] (128)  NULL,[LinkDocumento_old] [nvarchar] (256)  NULL,[LinkDocumento_new] [nvarchar] (256)  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[NombreDocumento_old] [nvarchar] (128)  NULL,[NombreDocumento_new] [nvarchar] (128)  NULL,[IdTemporal_old] [varchar] (20)  NULL,[IdTemporal_new] [varchar] (20)  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO