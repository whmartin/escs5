USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Creacion de indice unico para tipodocumentoprograma
-- =============================================
/****** Object:  Index [TipoDocumentoProgramaUnico]    Script Date: 06/24/2013 12:29:18 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoDocumentoPrograma]') AND name = N'TipoDocumentoProgramaUnico')
DROP INDEX [TipoDocumentoProgramaUnico] ON [Proveedor].[TipoDocumentoPrograma] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO

/****** Object:  Index [TipoDocumentoProgramaUnico]    Script Date: 06/24/2013 12:29:20 ******/
CREATE UNIQUE NONCLUSTERED INDEX [TipoDocumentoProgramaUnico] ON [Proveedor].[TipoDocumentoPrograma] 
(
	[IdTipoDocumento] ASC,
	[IdPrograma] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

