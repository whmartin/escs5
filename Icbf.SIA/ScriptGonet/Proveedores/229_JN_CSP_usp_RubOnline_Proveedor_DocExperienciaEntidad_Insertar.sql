USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]
GO
-- =============================================
-- Author:		Jonnathan NI�o
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdExpEntidad INT,	@IdTipoDocGrupo INT,	@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.DocExperienciaEntidad(IdExpEntidad, IdTipoDocGrupo, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea)
					  VALUES(@IdExpEntidad, @IdTipoDocGrupo, @Descripcion, @LinkDocumento, @UsuarioCrea, GETDATE())
	SELECT @IdDocAdjunto = @@IDENTITY 		
END
