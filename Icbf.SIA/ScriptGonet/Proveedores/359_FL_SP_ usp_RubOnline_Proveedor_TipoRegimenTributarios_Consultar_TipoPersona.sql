USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]    Script Date: 07/24/2013 11:21:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]    Script Date: 07/24/2013 11:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]
	@IdTipoPersona INT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE t.IdTipoPersona = @IdTipoPersona 
 ORDER BY Descripcion
 END


GO


