USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar]    Script Date: 07/29/2013 16:02:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar]    Script Date: 07/29/2013 16:02:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar]
@ProviderUserKey UNIQUEIDENTIFIER
AS
BEGIN
SELECT [IDTERCERO]
      ,[IDTIPODOCIDENTIFICA]
      ,[IDESTADOTERCERO]
      ,[IdTipoPersona]
      ,[NUMEROIDENTIFICACION]
      ,[DIGITOVERIFICACION]
      ,[CORREOELECTRONICO]
      ,[PRIMERNOMBRE]
      ,[SEGUNDONOMBRE]
      ,[PRIMERAPELLIDO]
      ,[SEGUNDOAPELLIDO]
      ,[RAZONSOCIAL]
      ,[FECHAEXPEDICIONID]
      ,[FECHANACIMIENTO]
      ,[SEXO]
      ,[ProviderUserKey]
      ,[FECHACREA]
      ,[USUARIOCREA]
      ,[FECHAMODIFICA]
      ,[USUARIOMODIFICA]
      ,[ESFUNDACION]
FROM [Oferente].[Tercero]
WHERE ProviderUserKey = @ProviderUserKey
     
END

GO


