USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]    Script Date: 07/29/2013 11:54:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]    Script Date: 07/29/2013 11:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- Autor: Mauricio Martinez
-- Fecha: 2013/07/28
-- Descripcion: Procedimiento usado para validar el proveedor m�dulos d�tos b�sicos, info financiero o experiencia
  
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision](@IdEntidad INT)
as
begin
 
	update Proveedor.InfoFinancieraEntidad  
	set  Finalizado = 1		
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and i.NroRevision = v.NroRevision
		and i.EstadoValidacion = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRM� CON SI
	
	
	update Proveedor.InfoFinancieraEntidad  
	set NroRevision = v.NroRevision + 1,
		Finalizado = 0	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		and i.EstadoValidacion <> 4 --SI NO ESTA VALIDADO AUMENTA EL NRO DE REVISION
		and v.ConfirmaYAprueba  <> 1 -- Y SI NO CONFIRM� CON SI
		
	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = 1	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		and i.EstadoDocumental = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRM� CON SI
	
	
	update Proveedor.InfoExperienciaEntidad 
	set NroRevision = v.NroRevision + 1,
		Finalizado = 0
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and i.NroRevision = v.NroRevision
		and i.EstadoDocumental <> 4 --SI NO ESTA VALIDADO AUMENTA EL NRO DE REVISION
		and v.ConfirmaYAprueba <> 1 -- Y SI NO CONFIRM� CON SI

	update Proveedor.EntidadProvOferente    
	set Finalizado = 1
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
	where e.IdEntidad = @IdEntidad
		and IdEstado = 2  -- SI ESTA VALIDADO
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMO CON UN SI

		
	update Proveedor.EntidadProvOferente    
	set NroRevision = e.NroRevision + 1,
		Finalizado = 0
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
	where e.IdEntidad = @IdEntidad
		and IdEstado <> 2  -- SI NO ESTA VALIDADO
		and v.ConfirmaYAprueba <> 1 -- Y SI NO HAN PUESTO UN SI

end



GO


