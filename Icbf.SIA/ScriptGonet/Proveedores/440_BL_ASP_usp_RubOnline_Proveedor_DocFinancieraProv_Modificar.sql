USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]    Script Date: 08/01/2013 18:23:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]    Script Date: 08/01/2013 18:23:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]
		@IdDocAdjunto INT,	@IdInfoFin INT=NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocFinancieraProv SET IdInfoFin = @IdInfoFin, NombreDocumento = @NombreDocumento, LinkDocumento = @LinkDocumento, Observaciones = @Observaciones, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDocAdjunto = @IdDocAdjunto
END

GO


