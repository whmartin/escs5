USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]    Script Date: 06/14/2013 19:32:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que elimina un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]
	@IdTipoentidad INT
AS
BEGIN
	DELETE Proveedor.Tipoentidad WHERE IdTipoentidad = @IdTipoentidad
END
GO