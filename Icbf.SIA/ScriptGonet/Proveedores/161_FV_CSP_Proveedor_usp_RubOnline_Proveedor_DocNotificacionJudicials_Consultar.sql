USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]    Script Date: 06/24/2013 12:35:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]    Script Date: 06/24/2013 12:35:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]
	@IdDocNotJudicial INT = NULL,
	@IdNotJudicial INT = NULL,
	@IdDocumento INT = NULL
AS
BEGIN
 SELECT     Proveedor.DocNotificacionJudicial.IdDocNotJudicial, 
		    Proveedor.DocNotificacionJudicial.IdNotJudicial, 
		    Proveedor.DocNotificacionJudicial.IdDocumento, 
            Proveedor.DocNotificacionJudicial.Descripcion, 
            Proveedor.DocNotificacionJudicial.LinkDocumento, 
            Proveedor.DocNotificacionJudicial.Anno, 
            Proveedor.DocNotificacionJudicial.UsuarioCrea, 
            Proveedor.DocNotificacionJudicial.FechaCrea, 
            Proveedor.DocNotificacionJudicial.UsuarioModifica, 
            Proveedor.DocNotificacionJudicial.FechaModifica, 
            Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento
 FROM       Proveedor.DocNotificacionJudicial 
			INNER JOIN Proveedor.TipoDocumento 
			ON Proveedor.DocNotificacionJudicial.IdDocumento = Proveedor.TipoDocumento.IdTipoDocumento
 WHERE		IdDocNotJudicial = CASE WHEN @IdDocNotJudicial IS NULL THEN IdDocNotJudicial ELSE @IdDocNotJudicial END AND 
			IdNotJudicial = CASE WHEN @IdNotJudicial IS NULL THEN IdNotJudicial ELSE @IdNotJudicial END AND 
			IdDocumento = CASE WHEN @IdDocumento IS NULL THEN IdDocumento ELSE @IdDocumento END
END

GO


