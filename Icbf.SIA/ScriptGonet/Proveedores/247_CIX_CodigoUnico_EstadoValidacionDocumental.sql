USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina indice CodigoUnico de la entidad [Proveedor].[EstadoValidacionDocumental]
-- =============================================
/****** Object:  Index [CodigoUnico]    Script Date: 06/26/2013 22:37:55 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[EstadoValidacionDocumental]') AND name = N'CodigoUnico')
DROP INDEX [CodigoUnico] ON [Proveedor].[EstadoValidacionDocumental] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Crea indice CodigoUnico en la entidad [Proveedor].[EstadoValidacionDocumental]
-- =============================================
/****** Object:  Index [CodigoUnico]    Script Date: 06/26/2013 22:38:17 ******/
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[EstadoValidacionDocumental] 
(
	[CodigoEstadoValidacionDocumental] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


