USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]    Script Date: 06/24/2013 12:22:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]    Script Date: 06/24/2013 12:22:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta el salario Minimo Vigente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]
	@IdVigencia INT = NULL
AS
 
 declare @SMLV int
 
 SELECT @SMLV = Valor FROM oferente.SalarioMinimo s
 inner join [Global].[Vigencia] v on [A�o] = [AcnoVigencia]
 and v.Activo = 1 and s.Estado = 1
 and @IdVigencia = IdVigencia

 SELECT @SMLV
GO


