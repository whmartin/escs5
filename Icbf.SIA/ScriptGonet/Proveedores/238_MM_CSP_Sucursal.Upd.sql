USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]    Script Date: 06/26/2013 10:08:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]    Script Date: 06/26/2013 10:08:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]
		@IdSucursal INT,	@IdEntidad INT,	@Nombre VARCHAR(255), @Indicativo INT,	@Telefono INT,	@Extension INT = NULL,	@Celular  NUMERIC(10),	@Correo NVARCHAR(256),	@Estado INT, @IdZona INT,	@Departamento INT,	@Municipio INT,	@Direccion NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Sucursal SET IdEntidad = @IdEntidad, Nombre = @Nombre, Indicativo = @Indicativo, Telefono = @Telefono, Extension = @Extension, Celular = @Celular, Correo = @Correo, Estado = @Estado, IdZona = @IdZona, Departamento = @Departamento, Municipio = @Municipio, Direccion = @Direccion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdSucursal = @IdSucursal
END

GO

