USE [QoS]
GO


IF EXISTS (SELECT * FROM [QoS].[dbo].[aspnet_Applications] WHERE [ApplicationName] = 'RUBO')
BEGIN

	DECLARE @ApplicationId NVARCHAR(100)


	SET @ApplicationId = (SELECT [ApplicationId]
						  FROM [QoS].[dbo].[aspnet_Applications] 
						  WHERE [ApplicationName] = 'RUBO')

	IF NOT EXISTS (SELECT * FROM [QoS].[dbo].[aspnet_Roles] WHERE [RoleName] = 'PROVEEDORES')
	BEGIN

		INSERT INTO [dbo].[aspnet_Roles]
				   ([ApplicationId]          
				   ,[RoleName]
				   ,[LoweredRoleName]
				   ,[Description])
			 VALUES
				   (@ApplicationId        
				   ,'PROVEEDORES'
				   ,'proveedores'
				   ,'Usuarios proveedores que se registran desde internet')


	END
END
GO


