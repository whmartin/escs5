USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina un CONSTRAINT de la entidad [SEG].[Parametro]
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[SEG].[FK_Parametro_Modulo]') AND parent_object_id = OBJECT_ID(N'[SEG].[Parametro]'))
ALTER TABLE [SEG].[Parametro] DROP CONSTRAINT [FK_Parametro_Modulo]
GO
