USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]    Script Date: 06/14/2013 19:31:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]
@CodigoNiveldegobierno NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Niveldegobierno] WHERE CodigoNiveldegobierno = CASE WHEN @CodigoNiveldegobierno IS NULL THEN CodigoNiveldegobierno ELSE @CodigoNiveldegobierno END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END

END
GO