USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]    Script Date: 06/24/2013 12:35:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]    Script Date: 06/24/2013 12:35:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]
		@IdDocNotJudicial INT OUTPUT , 	
		@IdNotJudicial INT , 	
		@IdDocumento INT , 	
		@Descripcion NVARCHAR(128),	
		@LinkDocumento NVARCHAR(256),	
		@Anno INT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.DocNotificacionJudicial(IdNotJudicial,IdDocumento, Descripcion, LinkDocumento, Anno, UsuarioCrea, FechaCrea)
					  VALUES(@IdNotJudicial,@IdDocumento, @Descripcion, @LinkDocumento, @Anno, @UsuarioCrea, GETDATE())
	SELECT @IdDocNotJudicial = @@IDENTITY 		
	RETURN @@IDENTITY	
END

GO


