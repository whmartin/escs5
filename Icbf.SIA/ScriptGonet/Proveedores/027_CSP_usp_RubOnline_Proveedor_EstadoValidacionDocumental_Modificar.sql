USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]    Script Date: 06/14/2013 19:31:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que actualiza un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]
		@IdEstadoValidacionDocumental INT,	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.EstadoValidacionDocumental SET CodigoEstadoValidacionDocumental = @CodigoEstadoValidacionDocumental, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END
GO