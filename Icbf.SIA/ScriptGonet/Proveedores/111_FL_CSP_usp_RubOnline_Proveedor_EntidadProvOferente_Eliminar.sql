USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Eliminar]    Script Date: 06/24/2013 21:09:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Eliminar]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que elimina un(a) EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Eliminar]
	@IdEntidad INT
AS
BEGIN
	DELETE Proveedor.EntidadProvOferente WHERE IdEntidad = @IdEntidad
END
