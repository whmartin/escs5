USE [SIA]
GO

-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Inserta en registro en la entidad [Oferente].[EstadoTercero]  y despues realiza un update.
-- =============================================

IF NOT EXISTS(select * from [Oferente].[EstadoTercero]  where [CodigoEstadotercero] = '005')
BEGIN
INSERT [Oferente].[EstadoTercero] ([CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES ( N'005', N'EN VALIDACI�N', 1, N'Administrador', CAST(0x0000A20000DC4A15 AS DateTime), NULL, NULL)
END

GO
UPDATE [Oferente].[EstadoTercero] 
SET [DescripcionEstado] = UPPER([DescripcionEstado])