USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]    Script Date: 07/02/2013 23:44:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]    Script Date: 07/02/2013 23:44:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que actualiza un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]
@IdDocNotJudicial INT,	
@IdNotJudicial INT,	
@IdDocumento INT,	
@Descripcion NVARCHAR(128),	
@LinkDocumento NVARCHAR(256),	
@Anno INT, 
@UsuarioModifica NVARCHAR(250)
AS
BEGIN
UPDATE Proveedor.DocNotificacionJudicial
SET	IdNotJudicial  = @IdNotJudicial,
	IdDocumento=@IdDocumento,
	Descripcion = @Descripcion,
	LinkDocumento = @LinkDocumento,
	Anno = @Anno,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdDocNotJudicial = @IdDocNotJudicial 
END
GO


