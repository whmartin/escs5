/****** Object:  Trigger [PROVEEDOR].[TG_Aud_ValidarTercero]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para ValidarTercero
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_ValidarTercero]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_ValidarTercero]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_ValidarTercero]
  ON  [PROVEEDOR].[ValidarTercero]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_ValidarTercero]
  ([IdValidarTercero],[IdTercero_old], [IdTercero_new],[ConfirmaYAprueba_old], [ConfirmaYAprueba_new],[Observaciones_old], [Observaciones_new],[FechaCrea_old], [FechaCrea_new],[UsuarioCrea_old], [UsuarioCrea_new],[Operacion])
SELECT deleted.IdValidarTercero,deleted.IdTercero,inserted.IdTercero,deleted.ConfirmaYAprueba,inserted.ConfirmaYAprueba,deleted.Observaciones,inserted.Observaciones,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioCrea,inserted.UsuarioCrea,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdValidarTercero = deleted.IdValidarTerceroEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_ValidarTercero]
  ([IdValidarTercero],[IdTercero_old],[ConfirmaYAprueba_old],[Observaciones_old],[FechaCrea_old],[UsuarioCrea_old],[Operacion])
SELECT deleted.IdValidarTercero,deleted.IdTercero,deleted.ConfirmaYAprueba,deleted.Observaciones,deleted.FechaCrea,deleted.UsuarioCrea,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_ValidarTercero]
  ([IdValidarTercero],[IdTercero_new],[ConfirmaYAprueba_new],[Observaciones_new],[FechaCrea_new],[UsuarioCrea_new],[Operacion])
SELECT inserted.IdValidarTercero,inserted.IdTercero,inserted.ConfirmaYAprueba,inserted.Observaciones,inserted.FechaCrea,inserted.UsuarioCrea,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO