USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]    Script Date: 06/14/2013 19:31:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]
	@IdTipoCiiu INT
AS
BEGIN
 SELECT IdTipoCiiu, CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoCiiu] WHERE  IdTipoCiiu = @IdTipoCiiu
END
GO