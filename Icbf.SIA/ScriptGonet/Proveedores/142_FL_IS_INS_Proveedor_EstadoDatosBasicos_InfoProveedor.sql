USE [SIA]
-- =============================================
-- Author:		Faiber Losada 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	EstadoDatosBasicos
-- =============================================
GO

/****** Object:  Table [Proveedor].[EstadoDatosBasicos]    Script Date: 06/24/2013 22:24:23 ******/
DELETE FROM [Proveedor].[EstadoDatosBasicos]
GO
/****** Object:  Table [Proveedor].[EstadoDatosBasicos]    Script Date: 06/24/2013 22:24:23 ******/
SET IDENTITY_INSERT [Proveedor].[EstadoDatosBasicos] ON
IF NOT EXISTS( SELECT IdEstadoDatosBasicos FROM [Proveedor].[EstadoDatosBasicos] WHERE  Descripcion ='PENDIENTE POR VALIDACIÓN ') BEGIN
INSERT [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (1, N'PENDIENTE POR VALIDACIÓN ', 1, N'Administrador', CAST(0x0000A1E3000F189A AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdEstadoDatosBasicos FROM [Proveedor].[EstadoDatosBasicos] WHERE  Descripcion ='VALIDADO') BEGIN
INSERT [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (2, N'VALIDADO', 1, N'Administrador', CAST(0x0000A1E3000F275B AS DateTime), NULL, NULL)
END

IF NOT EXISTS( SELECT IdEstadoDatosBasicos FROM [Proveedor].[EstadoDatosBasicos] WHERE  Descripcion ='POR AJUSTE') BEGIN
INSERT [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (3, N'POR AJUSTE', 1, N'Administrador', CAST(0x0000A1E3000F3521 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT IdEstadoDatosBasicos FROM [Proveedor].[EstadoDatosBasicos] WHERE  Descripcion ='DATOS PARCIALES') BEGIN
INSERT [Proveedor].[EstadoDatosBasicos] ([IdEstadoDatosBasicos], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (4, N'DATOS PARCIALES', 1, N'Administrador', CAST(0x0000A1E3000F46BB AS DateTime), NULL, NULL)
END
SET IDENTITY_INSERT [Proveedor].[EstadoDatosBasicos] OFF
