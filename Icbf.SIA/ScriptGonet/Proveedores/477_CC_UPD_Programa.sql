USE [SIA]
GO
-- =============================================
-- Author:		Carlos Cubillos
-- Description:	Oculta del menú de proveedores el módulo de Acuerdos
-- =============================================

update SEG.Programa set visiblemenu=0 WHERE CodigoPrograma='Proveedor/Acuerdos'