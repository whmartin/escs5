USE [SIA]
GO

-- =============================================
-- Author:		Fabi�n Valencia
-- Create date:  2013-06-09
-- Description:	Pasa a mayusculas las descripciones
-- =============================================

UPDATE [Proveedor].[TipoSectorEntidad] 
SET Descripcion = UPPER(Descripcion)