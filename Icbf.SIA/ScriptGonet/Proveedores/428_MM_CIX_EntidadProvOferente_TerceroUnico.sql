USE [SIA]
GO

/****** Object:  Index [TerceroUnico]    Script Date: 07/02/2013 10:35:23 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]') AND name = N'TerceroUnico')
DROP INDEX [TerceroUnico] ON [Proveedor].[EntidadProvOferente] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO

/****** Object:  Index [TerceroUnico]    Script Date: 07/02/2013 10:35:25 ******/
CREATE UNIQUE NONCLUSTERED INDEX [TerceroUnico] ON [Proveedor].[EntidadProvOferente] 
(
	[IdTercero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

