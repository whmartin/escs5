
USE [SIA]
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  7/23/2013 1:25:13 PM
-- Description:	Permite crear las tablas Niveldegobierno Y RamaoEstructura

/****** Object:  Table [Proveedor].[Niveldegobierno]    Script Date: 07/23/2013 23:01:48 ******/

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Proveedor].[FK_EntidadProvOferente_Niveldegobierno]') AND parent_object_id = OBJECT_ID(N'[Proveedor].[EntidadProvOferente]'))
ALTER TABLE [Proveedor].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND type in (N'U'))
DROP TABLE [Proveedor].[Niveldegobierno]
GO
/****** Object:  Table [Proveedor].[RamaoEstructura]    Script Date: 07/23/2013 23:01:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND type in (N'U'))
DROP TABLE [Proveedor].[RamaoEstructura]
GO
/****** Object:  Table [Proveedor].[RamaoEstructura]    Script Date: 07/23/2013 23:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[RamaoEstructura](
	[IdRamaEstructura] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRamaEstructura] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Descripcion] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RamaoEstructura] PRIMARY KEY CLUSTERED 
(
	[IdRamaEstructura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[RamaoEstructura] 
(
	[CodigoRamaEstructura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[RamaoEstructura]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[RamaoEstructura] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura] ON
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'EJECUTIVA', 1, N'Administrador', CAST(0x0000A1DB00D4D18A AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'JUDICIAL', 1, N'Administrador', CAST(0x0000A1DB00D4E726 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'003', N'LEGISLATIVA', 1, N'Administrador', CAST(0x0000A1DB00D4F782 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, N'004', N'ÓRGANOS DE CONTROL', 1, N'Administrador', CAST(0x0000A1DB00D50899 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'005', N'ÓRGANOS ELECTORALES', 1, N'Administrador', CAST(0x0000A1DB00D51804 AS DateTime), NULL, NULL)
INSERT [Proveedor].[RamaoEstructura] ([IdRamaEstructura], [CodigoRamaEstructura], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (6, N'006', N'ÓRGANOS AUTÓNOMOS', 1, N'Administrador', CAST(0x0000A1DB00D526CC AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[RamaoEstructura] OFF
/****** Object:  Table [Proveedor].[Niveldegobierno]    Script Date: 07/23/2013 23:01:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[Niveldegobierno](
	[IdNiveldegobierno] [int] IDENTITY(1,1) NOT NULL,
	[CodigoNiveldegobierno] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Descripcion] [nvarchar](128) COLLATE Latin1_General_CI_AS NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) COLLATE Latin1_General_CI_AS NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) COLLATE Latin1_General_CI_AS NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Niveldegobierno] PRIMARY KEY CLUSTERED 
(
	[IdNiveldegobierno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND name = N'CodigoUnico')
CREATE UNIQUE NONCLUSTERED INDEX [CodigoUnico] ON [Proveedor].[Niveldegobierno] 
(
	[CodigoNiveldegobierno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Proveedor].[Niveldegobierno]') AND name = N'NombreUnico')
CREATE UNIQUE NONCLUSTERED INDEX [NombreUnico] ON [Proveedor].[Niveldegobierno] 
(
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [Proveedor].[Niveldegobierno] ON
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'NACIONAL', 1, N'Adminidtrador', CAST(0x0000A1EE00000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'DEPARTAMENTAL', 1, N'Adminidtrador', CAST(0x0000A1EE00000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'003', N'DISTRITAL', 1, N'Adminidtrador', CAST(0x0000A1EE00000000 AS DateTime), NULL, NULL)
INSERT [Proveedor].[Niveldegobierno] ([IdNiveldegobierno], [CodigoNiveldegobierno], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, N'004', N'MUNICIPAL', 1, N'Adminidtrador', CAST(0x0000A1EE00000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Proveedor].[Niveldegobierno] OFF


USE [SIA]
GO

ALTER TABLE [Proveedor].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno] FOREIGN KEY([IdTipoNivelGob])
REFERENCES [Proveedor].[Niveldegobierno] ([IdNiveldegobierno])
GO

ALTER TABLE [Proveedor].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO