/****** Object:  Table [Auditoria].[PROVEEDOR_DocFinancieraProv]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para DocFinancieraProv ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_DocFinancieraProv]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_DocFinancieraProv]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_DocFinancieraProv]([IdDocAdjunto] [int]  NULL, [IdInfoFin_old] [int]  NULL,[IdInfoFin_new] [int]  NULL,[NombreDocumento_old] [nvarchar] (128)  NULL,[NombreDocumento_new] [nvarchar] (128)  NULL,[LinkDocumento_old] [nvarchar] (256)  NULL,[LinkDocumento_new] [nvarchar] (256)  NULL,[Observaciones_old] [nvarchar] (256)  NULL,[Observaciones_new] [nvarchar] (256)  NULL,[UsuarioCrea_old] [nvarchar] (250)  NULL,[UsuarioCrea_new] [nvarchar] (250)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (250)  NULL,[UsuarioModifica_new] [nvarchar] (250)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[IdTipoDocumento_old] [int]  NULL,[IdTipoDocumento_new] [int]  NULL,[IdTemporal_old] [varchar] (20)  NULL,[IdTemporal_new] [varchar] (20)  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO