USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]
		@IdTipoCodUNSPSC INT, @Codigo NVARCHAR(64),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCodigoUNSPSC 
	SET Codigo = @Codigo, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdTipoCodUNSPSC = @IdTipoCodUNSPSC
END
