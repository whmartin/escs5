USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar]    Script Date: 06/23/2013 22:14:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar]    Script Date: 06/23/2013 22:14:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/4/2013 11:29:17 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TelTerceros
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar]
		@IdTelTercero INT,	
		@IdTercero INT=NULL,	
		@IndicativoTelefono INT=NULL,	
		@NumeroTelefono INT=NULL,	
		@ExtensionTelefono BIGINT=NULL,	
		@Movil BIGINT=NULL,	
		@IndicativoFax INT=NULL,	
		@NumeroFax INT=NULL, 
		@UsuarioModifica NVARCHAR(250)=NULL
AS
BEGIN
	UPDATE Oferente.TelTerceros 
	  SET 
		 IdTercero = ISNULL( @IdTercero, IdTercero ),
		 IndicativoTelefono = ISNULL( @IndicativoTelefono, IndicativoTelefono ),
		 NumeroTelefono = ISNULL( @NumeroTelefono, NumeroTelefono ),
		 ExtensionTelefono = ISNULL( @ExtensionTelefono, ExtensionTelefono ),
		 Movil = ISNULL( @Movil, Movil ),
		 IndicativoFax = ISNULL( @IndicativoFax, IndicativoFax ),
		 NumeroFax = ISNULL( @NumeroFax, NumeroFax ),
		 UsuarioModifica =@UsuarioModifica,
		 FechaModifica = GETDATE() 
	 WHERE IdTelTercero = @IdTelTercero
END


GO


