USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero]    Script Date: 06/23/2013 22:13:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero]    Script Date: 06/23/2013 22:13:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/4/2013 11:29:17 AM
-- Description:	Procedimiento almacenado que consulta un(a) TelTerceros por idtercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero]
	@IdTercero INT
AS
BEGIN
 SELECT Top 1 IdTelTercero, IdTercero, IndicativoTelefono, NumeroTelefono, ExtensionTelefono, Movil, IndicativoFax, NumeroFax, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Oferente].[TelTerceros] 
WHERE  IdTercero = @IdTercero 
END

GO


