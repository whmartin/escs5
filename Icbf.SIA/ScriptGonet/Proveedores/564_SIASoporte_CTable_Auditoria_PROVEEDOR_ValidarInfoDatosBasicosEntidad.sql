/****** Object:  Table [Auditoria].[PROVEEDOR_ValidarInfoDatosBasicosEntidad]    Script Date: 7/31/2014 ******//****** Desarrollador:  Bayron Lara ******//****** Crea la tabla de auditoria para ValidarInfoDatosBasicosEntidad ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Auditoria].[PROVEEDOR_ValidarInfoDatosBasicosEntidad]') AND type in (N'U'))
BEGIN
DROP TABLE [Auditoria].[PROVEEDOR_ValidarInfoDatosBasicosEntidad]
END
GO

CREATE TABLE [Auditoria].[PROVEEDOR_ValidarInfoDatosBasicosEntidad]([IdValidarInfoDatosBasicosEntidad] [int]  NULL, [NroRevision_old] [int]  NULL,[NroRevision_new] [int]  NULL,[IdEntidad_old] [int]  NULL,[IdEntidad_new] [int]  NULL,[ConfirmaYAprueba_old] [bit]  NULL,[ConfirmaYAprueba_new] [bit]  NULL,[Observaciones_old] [nvarchar] (200)  NULL,[Observaciones_new] [nvarchar] (200)  NULL,[FechaCrea_old] [datetime]  NULL,[FechaCrea_new] [datetime]  NULL,[UsuarioCrea_old] [nvarchar] (128)  NULL,[UsuarioCrea_new] [nvarchar] (128)  NULL,[FechaModifica_old] [datetime]  NULL,[FechaModifica_new] [datetime]  NULL,[UsuarioModifica_old] [nvarchar] (128)  NULL,[UsuarioModifica_new] [nvarchar] (128)  NULL,[CorreoEnviado_old] [bit]  NULL,[CorreoEnviado_new] [bit]  NULL,[Operacion] [nvarchar](128) NULL
) ON [PRIMARY] 
GO