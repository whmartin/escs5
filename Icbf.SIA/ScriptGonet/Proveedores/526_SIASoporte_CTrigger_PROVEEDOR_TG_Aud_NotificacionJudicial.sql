/****** Object:  Trigger [PROVEEDOR].[TG_Aud_NotificacionJudicial]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para NotificacionJudicial
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_NotificacionJudicial]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_NotificacionJudicial]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_NotificacionJudicial]
  ON  [PROVEEDOR].[NotificacionJudicial]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_NotificacionJudicial]
  ([IdNotJudicial],[IdEntidad_old], [IdEntidad_new],[IdDepartamento_old], [IdDepartamento_new],[IdMunicipio_old], [IdMunicipio_new],[IdZona_old], [IdZona_new],[Direccion_old], [Direccion_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[Operacion])
SELECT deleted.IdNotJudicial,deleted.IdEntidad,inserted.IdEntidad,deleted.IdDepartamento,inserted.IdDepartamento,deleted.IdMunicipio,inserted.IdMunicipio,deleted.IdZona,inserted.IdZona,deleted.Direccion,inserted.Direccion,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdNotJudicial = deleted.IdNotJudicialEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_NotificacionJudicial]
  ([IdNotJudicial],[IdEntidad_old],[IdDepartamento_old],[IdMunicipio_old],[IdZona_old],[Direccion_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[Operacion])
SELECT deleted.IdNotJudicial,deleted.IdEntidad,deleted.IdDepartamento,deleted.IdMunicipio,deleted.IdZona,deleted.Direccion,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_NotificacionJudicial]
  ([IdNotJudicial],[IdEntidad_new],[IdDepartamento_new],[IdMunicipio_new],[IdZona_new],[Direccion_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[Operacion])
SELECT inserted.IdNotJudicial,inserted.IdEntidad,inserted.IdDepartamento,inserted.IdMunicipio,inserted.IdZona,inserted.Direccion,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO