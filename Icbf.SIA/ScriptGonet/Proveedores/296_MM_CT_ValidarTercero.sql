USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Elimina la entidad [Proveedor].[ValidarTercero]
-- =============================================
/****** Object:  Table [Proveedor].[ValidarTercero]    Script Date: 07/03/2013 21:04:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[ValidarTercero]') AND type in (N'U'))
DROP TABLE [Proveedor].[ValidarTercero]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         20/01/2014
-- Description:     Crea la entidad [Proveedor].[ValidarTercero]
-- =============================================
/****** Object:  Table [Proveedor].[ValidarTercero]    Script Date: 07/03/2013 21:04:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Proveedor].[ValidarTercero](
	[IdValidarTercero] [int] IDENTITY(1,1) NOT NULL,
	[IdTercero] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,	
	[Observaciones] [nvarchar](100) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL	
 CONSTRAINT [PK_IdValidarTercero] PRIMARY KEY CLUSTERED 
(
	[IdValidarTercero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [Proveedor].[ValidarTercero]  WITH CHECK ADD  CONSTRAINT [IdTercero_Tercero] FOREIGN KEY([IdTercero])
REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
GO

ALTER TABLE [Proveedor].[ValidarTercero] CHECK CONSTRAINT [IdTercero_Tercero]
GO


