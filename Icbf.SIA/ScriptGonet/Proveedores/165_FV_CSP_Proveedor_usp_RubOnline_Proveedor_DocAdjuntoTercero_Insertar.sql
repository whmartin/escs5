USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]    Script Date: 06/24/2013 12:30:39 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]    Script Date: 06/24/2013 12:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]
@IdTercero INT, @IdDocumento INT, @Descripcion NVARCHAR (128), @LinkDocumento NVARCHAR (256), @Anno INT, @UsuarioCrea NVARCHAR (250)
AS
BEGIN
DECLARE @IdDocAdjunto INT
SET @IdDocAdjunto = 0
INSERT INTO Proveedor.DocAdjuntoTercero (IDTERCERO, IDDOCUMENTO, Descripcion, LinkDocumento, Anno, UsuarioCrea, FechaCrea)
	VALUES (@IdTercero, @IdDocumento, @Descripcion, @LinkDocumento, @Anno, @UsuarioCrea, GETDATE())

SELECT
	@IdDocAdjunto = @@IDENTITY

END

GO