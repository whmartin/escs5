USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
		@IdTipoCargoEntidad INT OUTPUT, 	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCargoEntidad(Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCargoEntidad = @@IDENTITY 		
END
