USE [SIA]
GO

alter table Proveedor.ValidarInfoDatosBasicosEntidad add CorreoEnviado BIT NOT NULL DEFAULT 0

alter table Proveedor.ValidarInfoExperienciaEntidad add CorreoEnviado BIT NOT NULL DEFAULT 0

alter table Proveedor.ValidarInfoFinancieraEntidad add CorreoEnviado BIT NOT NULL DEFAULT 0

