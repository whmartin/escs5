USE [SIA]
GO

/****** Object:  Table [Oferente].[EstadoTercero]    Script Date: 06/24/2013 12:43:15 ******/
-- =============================================
-- Author:		Fabi�n Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Tabla para guardar estado tercero
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Oferente].[EstadoTercero]') AND type in (N'U'))
DROP TABLE [Oferente].[EstadoTercero]
GO

USE [SIA]
GO

/****** Object:  Table [Oferente].[EstadoTercero]    Script Date: 06/24/2013 12:43:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Oferente].[EstadoTercero](
	[IdEstadoTercero] [int] IDENTITY(1,1) NOT NULL,
	[CodigoEstadotercero] [nvarchar](20) NOT NULL,
	[DescripcionEstado] [nvarchar](256) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](256) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](256) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoTercero] PRIMARY KEY CLUSTERED 
(
	[IdEstadoTercero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


