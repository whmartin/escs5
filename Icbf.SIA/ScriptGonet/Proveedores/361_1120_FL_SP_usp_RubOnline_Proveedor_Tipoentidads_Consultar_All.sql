USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]    Script Date: 07/24/2013 11:23:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]    Script Date: 07/24/2013 11:23:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]	
AS
BEGIN
 SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Tipoentidad]
 WHERE Estado=1
 ORDER BY Descripcion ASC
 
END


GO


