USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Inserta registros en  [Proveedor].[TipoCiiu]
-- =============================================
/****** Object:  Table [Proveedor].[TipoCiiu]    Script Date: 06/14/2013 19:40:35 ******/
SET IDENTITY_INSERT [Proveedor].[TipoCiiu] ON
INSERT [Proveedor].[TipoCiiu] ([IdTipoCiiu], [CodigoCiiu], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'7220', N'CONSULTORIA PROGRAMAS DE COMPUTADOR', 1, N'administrador', CAST(0x0000A1D6013E5A7C AS DateTime), N'administrador', CAST(0x0000A1DC011D0A7D AS DateTime))
INSERT [Proveedor].[TipoCiiu] ([IdTipoCiiu], [CodigoCiiu], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'6202', N'TEST', 1, N'administrador', CAST(0x0000A1D6013FEC30 AS DateTime), N'administrador', CAST(0x0000A1DC011D392E AS DateTime))
SET IDENTITY_INSERT [Proveedor].[TipoCiiu] OFF