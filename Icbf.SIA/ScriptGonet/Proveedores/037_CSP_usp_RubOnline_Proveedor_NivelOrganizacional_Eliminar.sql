USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]    Script Date: 06/14/2013 19:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que elimina un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]
	@IdNivelOrganizacional INT
AS
BEGIN
	DELETE Proveedor.NivelOrganizacional WHERE IdNivelOrganizacional = @IdNivelOrganizacional
END
GO