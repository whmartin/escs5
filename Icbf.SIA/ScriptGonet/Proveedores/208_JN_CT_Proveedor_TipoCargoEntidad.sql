USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoCargoEntidad]') AND type in (N'U'))
DROP TABLE [Proveedor].[TipoCargoEntidad]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/19/2013 11:00:36 PM
-- =============================================
CREATE TABLE [Proveedor].[TipoCargoEntidad](
 [IdTipoCargoEntidad] [INT]  IDENTITY(1,1) NOT NULL,
 [Descripcion] [NVARCHAR] (128) NOT NULL,
 [Estado] [BIT]  NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_TipoCargoEntidad] PRIMARY KEY CLUSTERED 
(
 	[IdTipoCargoEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
