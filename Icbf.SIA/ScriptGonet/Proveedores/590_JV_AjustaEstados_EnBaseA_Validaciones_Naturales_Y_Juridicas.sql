USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 22/SEPT/2014
Descripci�n: Revisa todos los proveedores Tipos Natural Y Juridico.
Revisa y ajusta su estado de validaci�n de cada m�dulo, basandose en las
Revisiones que cada registro de informaci�n tenga.
Esto porque, salieron incidentes d�nde aunque los modulos estaban con 
ConfirmaYAprueba=SI (es decir Validados), el estado que se estaba marcando en el m�dulo era incorrecto.
Esto pas� con informaci�n antigua, es decir informaci�n que se creo/modific� con fecha anterior al
mes de JUNIO 2014. Ya que en JUNIO 2014 fue que los estados de validaci�n y sus reglas comenzaron a funcionar
correctamente.
**/

DECLARE @TBL_PROVEEDORES TABLE(IdProv INT IDENTITY,
NUMIDENTIFICACION NVARCHAR(50))
INSERT INTO @TBL_PROVEEDORES
SELECT T.NUMEROIDENTIFICACION FROM Oferente.TERCERO T
INNER JOIN PROVEEDOR.EntidadProvOferente P
ON T.IDTERCERO=P.IdTercero
WHERE T.IDESTADOTERCERO IS NOT NULL

DECLARE  @ContadorPROV INT =1
DECLARE  @TOTALPROV INT =(SELECT COUNT(IdProv) FROM @TBL_PROVEEDORES)

WHILE @ContadorPROV <= @TOTALPROV
BEGIN

	DECLARE @NumIdent NVARCHAR(50)=(SELECT NUMIDENTIFICACION FROM @TBL_PROVEEDORES WHERE IdProv=@ContadorPROV)
	DECLARE @IdEntidad INT


	SET @IdEntidad=(
	SELECT IdEntidad FROM PROVEEDOR.EntidadProvOferente
	WHERE IdTercero=(SELECT IDTERCERO FROM Oferente.TERCERO WHERE
	NUMEROIDENTIFICACION=@NumIdent AND IDESTADOTERCERO IS NOT NULL))


	DECLARE @IdValDatosBasicos INT
	SET @IdValDatosBasicos=(
	SELECT TOP(1) IdValidarInfoDatosBasicosEntidad FROM PROVEEDOR.ValidarInfoDatosBasicosEntidad
	WHERE IdEntidad= @IdEntidad
	ORDER BY NroRevision DESC) 


	------Acturliza estado del m�dula DatosB�sicos en base a los Confirma y Aprueba Existentes-------
	IF ((SELECT ConfirmaYAprueba FROM PROVEEDOR.ValidarInfoDatosBasicosEntidad
	WHERE IdValidarInfoDatosBasicosEntidad=@IdValDatosBasicos)=1)
	BEGIN
		UPDATE PROVEEDOR.EntidadProvOferente
		SET IdEstado=(
		SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
		WHERE Descripcion='VALIDADO')
		WHERE EntidadProvOferente.IdEntidad=@IdEntidad
	END
	ELSE IF ((SELECT ConfirmaYAprueba FROM PROVEEDOR.ValidarInfoDatosBasicosEntidad
	WHERE IdValidarInfoDatosBasicosEntidad=@IdValDatosBasicos)=0)
	BEGIN
		UPDATE PROVEEDOR.EntidadProvOferente
		SET IdEstado=(
		SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
		WHERE Descripcion='POR AJUSTAR')
		WHERE EntidadProvOferente.IdEntidad=@IdEntidad
	END
	ELSE IF ((SELECT ConfirmaYAprueba FROM PROVEEDOR.ValidarInfoDatosBasicosEntidad
	WHERE IdValidarInfoDatosBasicosEntidad=@IdValDatosBasicos) IS NULL)
	BEGIN
		UPDATE PROVEEDOR.EntidadProvOferente
		SET IdEstado=(
		SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
		WHERE Descripcion='POR VALIDAR')
		WHERE EntidadProvOferente.IdEntidad=@IdEntidad		
	END


	DECLARE @CONTADOR INT =1
	DECLARE @Regis INT =0
	DECLARE @IdCheckValidacion INT
	DECLARE @IdValidacionMaxRev INT

	-------------Revisa Estados de Validaci�n del Mod. Info. Financiera--------------------------
	IF EXISTS ( SELECT IdInfoFin FROM PROVEEDOR.InfoFinancieraEntidad
	WHERE IdEntidad=@IdEntidad)
	BEGIN
		IF EXISTS (SELECT IdValidarInfoFinancieraEntidad FROM PROVEEDOR.ValidarInfoFinancieraEntidad
		WHERE IdInfoFin IN (SELECT IdInfoFin FROM PROVEEDOR.InfoFinancieraEntidad
		WHERE IdEntidad=@IdEntidad))
		BEGIN
			DECLARE @TBL_IDS_INFOFIN TABLE (
			Id INT IDENTITY,
			IdInfoFin INT)
			INSERT INTO @TBL_IDS_INFOFIN
			SELECT IdInfoFin FROM PROVEEDOR.InfoFinancieraEntidad
			WHERE IdEntidad=@IdEntidad
			
			SET @CONTADOR=1
			SET @Regis= (SELECT COUNT(*) FROM @TBL_IDS_INFOFIN)
			
			WHILE @CONTADOR <= @Regis
			BEGIN
				SET @IdCheckValidacion =(SELECT IdInfoFin FROM @TBL_IDS_INFOFIN WHERE Id=@CONTADOR)
				IF EXISTS (SELECT IdValidarInfoFinancieraEntidad FROM PROVEEDOR.ValidarInfoFinancieraEntidad WHERE IdInfoFin=@IdCheckValidacion)
				BEGIN
					SET @IdValidacionMaxRev= (SELECT TOP(1) IdValidarInfoFinancieraEntidad FROM PROVEEDOR.ValidarInfoFinancieraEntidad
					WHERE IdInfoFin=@IdCheckValidacion
					Order By NroRevision DESC)
					
					
					IF((SELECT ConfirmaYAprueba FROM PROVEEDOR.ValidarInfoFinancieraEntidad
					WHERE IdValidarInfoFinancieraEntidad=@IdValidacionMaxRev)=1)
					BEGIN
						UPDATE PROVEEDOR.InfoFinancieraEntidad 
						SET EstadoValidacion= (
						SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
						WHERE Descripcion='VALIDADO'
						)
						WHERE IdInfoFin=@IdCheckValidacion
					END
					ELSE IF((SELECT ConfirmaYAprueba FROM PROVEEDOR.ValidarInfoFinancieraEntidad
					WHERE IdValidarInfoFinancieraEntidad=@IdValidacionMaxRev)=0)
					BEGIN
						UPDATE PROVEEDOR.InfoFinancieraEntidad 
						SET EstadoValidacion= (
						SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
						WHERE Descripcion='POR AJUSTAR'
						)
						WHERE IdInfoFin=@IdCheckValidacion
					END
				END
				SET @CONTADOR=@CONTADOR + 1
			END
		END	
	END

	---------------Revisa estados de Validaci�n del Mod. Info. Experiencias----------------------------------
	IF EXISTS (SELECT IdExpEntidad FROM PROVEEDOR.InfoExperienciaEntidad
	WHERE IdEntidad=@IdEntidad)
	BEGIN
		IF EXISTS (SELECT IdValidarInfoExperienciaEntidad FROM PROVEEDOR.ValidarInfoExperienciaEntidad
		WHERE IdExpEntidad IN (SELECT IdExpEntidad FROM PROVEEDOR.InfoExperienciaEntidad
		WHERE IdEntidad=@IdEntidad))
		BEGIN
			DECLARE @TBL_IDS_INFOEXP TABLE (
			Id INT IDENTITY,
			IdExpEntidad INT)
			INSERT INTO @TBL_IDS_INFOEXP
			SELECT IdExpEntidad FROM PROVEEDOR.InfoExperienciaEntidad
			WHERE IdEntidad=@IdEntidad
			
			SET @CONTADOR=1
			SET @Regis= (SELECT COUNT(*) FROM @TBL_IDS_INFOEXP)
			
			WHILE @CONTADOR <= @Regis
			BEGIN
				SET @IdCheckValidacion =(SELECT IdExpEntidad FROM @TBL_IDS_INFOEXP WHERE Id=@CONTADOR)
				IF EXISTS (SELECT IdValidarInfoExperienciaEntidad FROM PROVEEDOR.ValidarInfoExperienciaEntidad WHERE IdExpEntidad=@IdCheckValidacion)
				BEGIN
					SET @IdValidacionMaxRev= (SELECT TOP(1) IdValidarInfoExperienciaEntidad FROM PROVEEDOR.ValidarInfoExperienciaEntidad
					WHERE IdExpEntidad=@IdCheckValidacion
					Order By NroRevision DESC)
					
					
					IF((SELECT ConfirmaYAprueba FROM PROVEEDOR.ValidarInfoExperienciaEntidad
					WHERE IdValidarInfoExperienciaEntidad=@IdValidacionMaxRev)=1)
					BEGIN
						UPDATE PROVEEDOR.InfoExperienciaEntidad 
						SET EstadoDocumental= (
						SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
						WHERE Descripcion='VALIDADO'
						)
						WHERE IdExpEntidad=@IdCheckValidacion
					END
					ELSE IF((SELECT ConfirmaYAprueba FROM PROVEEDOR.ValidarInfoExperienciaEntidad
					WHERE IdValidarInfoExperienciaEntidad=@IdValidacionMaxRev)=0)
					BEGIN
						UPDATE PROVEEDOR.InfoExperienciaEntidad 
						SET EstadoDocumental= (
						SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
						WHERE Descripcion='POR AJUSTAR'
						)
						WHERE IdExpEntidad=@IdCheckValidacion
					END
					ELSE IF((SELECT ConfirmaYAprueba FROM PROVEEDOR.ValidarInfoExperienciaEntidad
					WHERE IdValidarInfoExperienciaEntidad=@IdValidacionMaxRev) IS NULL)
					BEGIN
						UPDATE PROVEEDOR.InfoExperienciaEntidad 
						SET EstadoDocumental= (
						SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
						WHERE Descripcion='POR VALIDAR'
						)
						WHERE IdExpEntidad=@IdCheckValidacion
					END
				END
				SET @CONTADOR=@CONTADOR + 1
			END
		END	
	END
SET @ContadorPROV=@ContadorPROV+1
END
