USE [SIA]
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013-07-
-- Description:	Modifica la columna observaciones de la tabla ValidarTercero
-- =============================================

ALTER TABLE Proveedor.ValidarTercero ALTER COLUMN Observaciones NVARCHAR(200) NOT NULL
