USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 07/29/2013 16:19:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 07/29/2013 16:19:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT [IdEntidad]
      ,[ConsecutivoInterno]
      ,[TipoEntOfProv]
      ,[IdTercero]
      ,[IdTipoCiiuPrincipal]
      ,[IdTipoCiiuSecundario]
      ,[IdTipoSector]
      ,[IdTipoClaseEntidad]
      ,[IdTipoRamaPublica]
      ,[IdTipoNivelGob]
      ,[IdTipoNivelOrganizacional]
      ,[IdTipoCertificadorCalidad]
      ,[FechaCiiuPrincipal]
      ,[FechaCiiuSecundario]
      ,[FechaConstitucion]
      ,[FechaVigencia]
      ,[FechaMatriculaMerc]
      ,[FechaExpiracion]
      ,[TipoVigencia]
      ,[ExenMatriculaMer]
      ,[MatriculaMercantil]
      ,[ObserValidador]
      ,[AnoRegistro]
      ,[IdEstado]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
      ,[IdAcuerdo]
      ,Finalizado
 FROM [Proveedor].[EntidadProvOferente] WHERE  IdEntidad = @IdEntidad
END



GO


