USE [SIA]
GO


 delete
  FROM [SEG].[Permiso]
  where IdRol = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')

Go

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionProveedores')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionProveedores'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
			   ([IdPrograma]
			   ,[IdRol]
			   ,[Insertar]
			   ,[Modificar]
			   ,[Eliminar]
			   ,[Consultar]
			   ,[UsuarioCreacion]
			   ,[FechaCreacion])           
		 VALUES
			   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/GestionProveedores')
			   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
			   ,1
			   ,1
			   ,0
			   ,1
			   ,'Administrador'
			   ,GETDATE()) 
	END
END      
GO

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/NotificacionJudicial')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/NotificacionJudicial'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
				   ([IdPrograma]
				   ,[IdRol]
				   ,[Insertar]
				   ,[Modificar]
				   ,[Eliminar]
				   ,[Consultar]
				   ,[UsuarioCreacion]
				   ,[FechaCreacion])           
			 VALUES
				   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/NotificacionJudicial')
				   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
				   ,1
				   ,1
				   ,0
				   ,1
				   ,'Administrador'
				   ,GETDATE())  
	END
END
GO

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/InfoFinancieraEntidad')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/InfoFinancieraEntidad'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
				   ([IdPrograma]
				   ,[IdRol]
				   ,[Insertar]
				   ,[Modificar]
				   ,[Eliminar]
				   ,[Consultar]
				   ,[UsuarioCreacion]
				   ,[FechaCreacion])           
			 VALUES
				   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/InfoFinancieraEntidad')
				   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
				   ,1
				   ,1
				   ,0
				   ,1
				   ,'Administrador'
				   ,GETDATE())  
	END
END
GO

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/DocFinancieraProv')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/DocFinancieraProv'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
				   ([IdPrograma]
				   ,[IdRol]
				   ,[Insertar]
				   ,[Modificar]
				   ,[Eliminar]
				   ,[Consultar]
				   ,[UsuarioCreacion]
				   ,[FechaCreacion])           
			 VALUES
				   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/DocFinancieraProv')
				   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
				   ,1
				   ,1
				   ,0
				   ,1
				   ,'Administrador'
				   ,GETDATE())  
	END
END
GO

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/SUCURSAL')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/SUCURSAL'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
				   ([IdPrograma]
				   ,[IdRol]
				   ,[Insertar]
				   ,[Modificar]
				   ,[Eliminar]
				   ,[Consultar]
				   ,[UsuarioCreacion]
				   ,[FechaCreacion])           
			 VALUES
				   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/SUCURSAL')
				   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
				   ,1
				   ,1
				   ,0
				   ,1
				   ,'Administrador'
				   ,GETDATE())  
	END
END
GO

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/CONTACTOENTIDAD')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/CONTACTOENTIDAD'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
				   ([IdPrograma]
				   ,[IdRol]
				   ,[Insertar]
				   ,[Modificar]
				   ,[Eliminar]
				   ,[Consultar]
				   ,[UsuarioCreacion]
				   ,[FechaCreacion])           
			 VALUES
				   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/CONTACTOENTIDAD')
				   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
				   ,1
				   ,1
				   ,0
				   ,1
				   ,'Administrador'
				   ,GETDATE())  
	END
END
GO

IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/INFOEXPERIENCIAENTIDAD')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/INFOEXPERIENCIAENTIDAD'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
				   ([IdPrograma]
				   ,[IdRol]
				   ,[Insertar]
				   ,[Modificar]
				   ,[Eliminar]
				   ,[Consultar]
				   ,[UsuarioCreacion]
				   ,[FechaCreacion])           
			 VALUES
				   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'PROVEEDOR/INFOEXPERIENCIAENTIDAD')
				   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
				   ,1
				   ,1
				   ,0
				   ,1
				   ,'Administrador'
				   ,GETDATE())  
	END
END
GO




