/****** Object:  Trigger [PROVEEDOR].[TG_Aud_EntidadProvOferente]    Script Date: 7/31/2014 ******/-- =============================================
-- Author: Bayron Lara
-- Create date: 7/31/2014
-- Description: Crea el trigger de auditoria para EntidadProvOferente
-- =============================================IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TG_Aud_EntidadProvOferente]') AND type in (N'TR'))
BEGIN
DROP TRIGGER [PROVEEDOR].[TG_Aud_EntidadProvOferente]
END
GO

CREATE TRIGGER [PROVEEDOR].[TG_Aud_EntidadProvOferente]
  ON  [PROVEEDOR].[EntidadProvOferente]
  FOR INSERT,DELETE,UPDATE
AS 
BEGIN
 
SET NOCOUNT ON 

IF ((SELECT Count(*) FROM inserted) > 0 ) and ((SELECT Count(*) FROM deleted) > 0 )
BEGIN

----SE REALIZO UN UPDATE 
 
INSERT INTO [Auditoria].[PROVEEDOR_EntidadProvOferente]
  ([IdEntidad],[ConsecutivoInterno_old], [ConsecutivoInterno_new],[TipoEntOfProv_old], [TipoEntOfProv_new],[IdTercero_old], [IdTercero_new],[IdTipoCiiuPrincipal_old], [IdTipoCiiuPrincipal_new],[IdTipoCiiuSecundario_old], [IdTipoCiiuSecundario_new],[IdTipoSector_old], [IdTipoSector_new],[IdTipoClaseEntidad_old], [IdTipoClaseEntidad_new],[IdTipoRamaPublica_old], [IdTipoRamaPublica_new],[IdTipoNivelGob_old], [IdTipoNivelGob_new],[IdTipoNivelOrganizacional_old], [IdTipoNivelOrganizacional_new],[IdTipoCertificadorCalidad_old], [IdTipoCertificadorCalidad_new],[FechaCiiuPrincipal_old], [FechaCiiuPrincipal_new],[FechaCiiuSecundario_old], [FechaCiiuSecundario_new],[FechaConstitucion_old], [FechaConstitucion_new],[FechaVigencia_old], [FechaVigencia_new],[FechaMatriculaMerc_old], [FechaMatriculaMerc_new],[FechaExpiracion_old], [FechaExpiracion_new],[TipoVigencia_old], [TipoVigencia_new],[ExenMatriculaMer_old], [ExenMatriculaMer_new],[MatriculaMercantil_old], [MatriculaMercantil_new],[ObserValidador_old], [ObserValidador_new],[AnoRegistro_old], [AnoRegistro_new],[IdEstado_old], [IdEstado_new],[UsuarioCrea_old], [UsuarioCrea_new],[FechaCrea_old], [FechaCrea_new],[UsuarioModifica_old], [UsuarioModifica_new],[FechaModifica_old], [FechaModifica_new],[IdAcuerdo_old], [IdAcuerdo_new],[NroRevision_old], [NroRevision_new],[Finalizado_old], [Finalizado_new],[IdEstadoProveedor_old], [IdEstadoProveedor_new],[Operacion])
SELECT deleted.IdEntidad,deleted.ConsecutivoInterno,inserted.ConsecutivoInterno,deleted.TipoEntOfProv,inserted.TipoEntOfProv,deleted.IdTercero,inserted.IdTercero,deleted.IdTipoCiiuPrincipal,inserted.IdTipoCiiuPrincipal,deleted.IdTipoCiiuSecundario,inserted.IdTipoCiiuSecundario,deleted.IdTipoSector,inserted.IdTipoSector,deleted.IdTipoClaseEntidad,inserted.IdTipoClaseEntidad,deleted.IdTipoRamaPublica,inserted.IdTipoRamaPublica,deleted.IdTipoNivelGob,inserted.IdTipoNivelGob,deleted.IdTipoNivelOrganizacional,inserted.IdTipoNivelOrganizacional,deleted.IdTipoCertificadorCalidad,inserted.IdTipoCertificadorCalidad,deleted.FechaCiiuPrincipal,inserted.FechaCiiuPrincipal,deleted.FechaCiiuSecundario,inserted.FechaCiiuSecundario,deleted.FechaConstitucion,inserted.FechaConstitucion,deleted.FechaVigencia,inserted.FechaVigencia,deleted.FechaMatriculaMerc,inserted.FechaMatriculaMerc,deleted.FechaExpiracion,inserted.FechaExpiracion,deleted.TipoVigencia,inserted.TipoVigencia,deleted.ExenMatriculaMer,inserted.ExenMatriculaMer,deleted.MatriculaMercantil,inserted.MatriculaMercantil,deleted.ObserValidador,inserted.ObserValidador,deleted.AnoRegistro,inserted.AnoRegistro,deleted.IdEstado,inserted.IdEstado,deleted.UsuarioCrea,inserted.UsuarioCrea,deleted.FechaCrea,inserted.FechaCrea,deleted.UsuarioModifica,inserted.UsuarioModifica,deleted.FechaModifica,inserted.FechaModifica,deleted.IdAcuerdo,inserted.IdAcuerdo,deleted.NroRevision,inserted.NroRevision,deleted.Finalizado,inserted.Finalizado,deleted.IdEstadoProveedor,inserted.IdEstadoProveedor,'MODIFICADO'
  FROM
  inserted
  INNER JOIN deleted
  ON inserted.IdEntidad = deleted.IdEntidadEND
ELSE
BEGIN
if ((SELECT  Count(*) FROM deleted) > 0)
BEGIN
---- SE REALIZO UN DELETE


INSERT INTO [Auditoria].[PROVEEDOR_EntidadProvOferente]
  ([IdEntidad],[ConsecutivoInterno_old],[TipoEntOfProv_old],[IdTercero_old],[IdTipoCiiuPrincipal_old],[IdTipoCiiuSecundario_old],[IdTipoSector_old],[IdTipoClaseEntidad_old],[IdTipoRamaPublica_old],[IdTipoNivelGob_old],[IdTipoNivelOrganizacional_old],[IdTipoCertificadorCalidad_old],[FechaCiiuPrincipal_old],[FechaCiiuSecundario_old],[FechaConstitucion_old],[FechaVigencia_old],[FechaMatriculaMerc_old],[FechaExpiracion_old],[TipoVigencia_old],[ExenMatriculaMer_old],[MatriculaMercantil_old],[ObserValidador_old],[AnoRegistro_old],[IdEstado_old],[UsuarioCrea_old],[FechaCrea_old],[UsuarioModifica_old],[FechaModifica_old],[IdAcuerdo_old],[NroRevision_old],[Finalizado_old],[IdEstadoProveedor_old],[Operacion])
SELECT deleted.IdEntidad,deleted.ConsecutivoInterno,deleted.TipoEntOfProv,deleted.IdTercero,deleted.IdTipoCiiuPrincipal,deleted.IdTipoCiiuSecundario,deleted.IdTipoSector,deleted.IdTipoClaseEntidad,deleted.IdTipoRamaPublica,deleted.IdTipoNivelGob,deleted.IdTipoNivelOrganizacional,deleted.IdTipoCertificadorCalidad,deleted.FechaCiiuPrincipal,deleted.FechaCiiuSecundario,deleted.FechaConstitucion,deleted.FechaVigencia,deleted.FechaMatriculaMerc,deleted.FechaExpiracion,deleted.TipoVigencia,deleted.ExenMatriculaMer,deleted.MatriculaMercantil,deleted.ObserValidador,deleted.AnoRegistro,deleted.IdEstado,deleted.UsuarioCrea,deleted.FechaCrea,deleted.UsuarioModifica,deleted.FechaModifica,deleted.IdAcuerdo,deleted.NroRevision,deleted.Finalizado,deleted.IdEstadoProveedor,'BORRADO'
  FROM deleted

END
ELSE
BEGIN

---- SE REALIZO UN INSERT

INSERT INTO [Auditoria].[PROVEEDOR_EntidadProvOferente]
  ([IdEntidad],[ConsecutivoInterno_new],[TipoEntOfProv_new],[IdTercero_new],[IdTipoCiiuPrincipal_new],[IdTipoCiiuSecundario_new],[IdTipoSector_new],[IdTipoClaseEntidad_new],[IdTipoRamaPublica_new],[IdTipoNivelGob_new],[IdTipoNivelOrganizacional_new],[IdTipoCertificadorCalidad_new],[FechaCiiuPrincipal_new],[FechaCiiuSecundario_new],[FechaConstitucion_new],[FechaVigencia_new],[FechaMatriculaMerc_new],[FechaExpiracion_new],[TipoVigencia_new],[ExenMatriculaMer_new],[MatriculaMercantil_new],[ObserValidador_new],[AnoRegistro_new],[IdEstado_new],[UsuarioCrea_new],[FechaCrea_new],[UsuarioModifica_new],[FechaModifica_new],[IdAcuerdo_new],[NroRevision_new],[Finalizado_new],[IdEstadoProveedor_new],[Operacion])
SELECT inserted.IdEntidad,inserted.ConsecutivoInterno,inserted.TipoEntOfProv,inserted.IdTercero,inserted.IdTipoCiiuPrincipal,inserted.IdTipoCiiuSecundario,inserted.IdTipoSector,inserted.IdTipoClaseEntidad,inserted.IdTipoRamaPublica,inserted.IdTipoNivelGob,inserted.IdTipoNivelOrganizacional,inserted.IdTipoCertificadorCalidad,inserted.FechaCiiuPrincipal,inserted.FechaCiiuSecundario,inserted.FechaConstitucion,inserted.FechaVigencia,inserted.FechaMatriculaMerc,inserted.FechaExpiracion,inserted.TipoVigencia,inserted.ExenMatriculaMer,inserted.MatriculaMercantil,inserted.ObserValidador,inserted.AnoRegistro,inserted.IdEstado,inserted.UsuarioCrea,inserted.FechaCrea,inserted.UsuarioModifica,inserted.FechaModifica,inserted.IdAcuerdo,inserted.NroRevision,inserted.Finalizado,inserted.IdEstadoProveedor,'INSERTADO'
  FROM inserted
END
END
SET NOCOUNT OFF 
END
GO