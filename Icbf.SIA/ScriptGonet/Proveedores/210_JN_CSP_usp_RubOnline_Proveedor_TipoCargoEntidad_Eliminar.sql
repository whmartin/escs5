USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]
	@IdTipoCargoEntidad INT
AS
BEGIN
	DELETE Proveedor.TipoCargoEntidad 
	WHERE IdTipoCargoEntidad = @IdTipoCargoEntidad
END
