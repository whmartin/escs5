USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:     Crea la entidad [Proveedor].[TipoRegimenTributario]
-- =============================================
/****** Object:  Table [Proveedor].[TipoRegimenTributario]    Script Date: 06/14/2013 19:19:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Proveedor].[TipoRegimenTributario]') AND type in (N'U'))
BEGIN
CREATE TABLE [Proveedor].[TipoRegimenTributario](
	[IdTipoRegimenTributario] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRegimenTributario] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[IdTipoPersona] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoRegimenTributario] PRIMARY KEY CLUSTERED 
(
	[IdTipoRegimenTributario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO