USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]
GO
-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]
		@IdExpEntidad INT OUTPUT, 	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	
		@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	
		@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(128),	
		@FechaInicio DATETIME,	@FechaFin DATETIME, @ExperienciaMeses NUMERIC(18,2),	@NumeroContrato NVARCHAR(128),	
		@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(21,3),	@EstadoDocumental INT,	@UnionTempConsorcio BIT,	
		@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	@JardinOPreJardin BIT, @UsuarioCrea NVARCHAR(250),
		@IdTemporal VARCHAR(50)=NULL
AS
BEGIN
	INSERT INTO Proveedor.InfoExperienciaEntidad(IdEntidad, IdTipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, 
		FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, PorcentParticipacion, AtencionDeptos, JardinOPreJardin, UsuarioCrea, FechaCrea)
	VALUES(@IdEntidad, @IdTipoSector, @IdTipoEstadoExp, @IdTipoModalidadExp, @IdTipoModalidad, @IdTipoPoblacionAtendida, @IdTipoRangoExpAcum, @IdTipoCodUNSPSC, @IdTipoEntidadContratante, @EntidadContratante, 
					  @FechaInicio, @FechaFin, @ExperienciaMeses, @NumeroContrato, @ObjetoContrato, @Vigente, @Cuantia, @EstadoDocumental, @UnionTempConsorcio, @PorcentParticipacion, @AtencionDeptos, @JardinOPreJardin, @UsuarioCrea, GETDATE())
	
	SELECT @IdExpEntidad = @@IDENTITY
					  
	UPDATE [Proveedor].[DocExperienciaEntidad] 
	SET IdExpEntidad = @IdExpEntidad
	WHERE IdTemporal = @IdTemporal
					  
	SELECT @IdExpEntidad
END
