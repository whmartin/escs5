USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]    Script Date: 08/02/2013 18:42:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]    Script Date: 08/02/2013 18:42:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  2013-06-09
-- Description:	Procedimiento consulta Inconsistencias la validacion de Datos B�sicos,Datos Financieros y Experiencia del Proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]

@IdEntidad int

AS
BEGIN
--DECLARE @Tercero varchar(30), 
DECLARE @Asunto varchar (100), @Email_Destino varchar (max), @Proveedor varchar (100), @nit varchar (30)
 
 SELECT @Email_Destino=t.correoelectronico,@Proveedor= t.primernombre +' '+ ISNULL( t.segundonombre,'') +' ' + t.primerapellido +' ' + ISNULL( t.segundoapellido,'') +' ' + ISNULL( t. razonsocial,''), @nit=NUMEROIDENTIFICACION FROM Oferente.tercero t 
		INNER JOIN   Proveedor.EntidadProvOferente epo ON t.idtercero= epo.idtercero
		 WHERE epo.IdEntidad =@IdEntidad 
--SET @Tercero = 'Tercero'
--SET @Asunto = 'Validar ' + @Tercero
Declare @Body varchar (max),

@TableHead varchar (max),
@TableTail varchar (max),
@TableBody varchar (max),
@TableBody1 varchar (max),
@TableBody2 varchar (max),
@TableBody3 varchar (max),
@TableFinal varchar (max)

 
SET NOCOUNT ON;
select  @TableBody= '<br/>' + ValorParametro +'<br/>' from SEG.Parametro where NombreParametro='MensajeMailsInconsistenciasProveedor' 
--SET @TableBody= ' <br/> <br/><h3>Sistema de Informaci�n Terceros y Gesti�n de  Proveedores del Instituto Colombiano de Bienestar Familiar </h3><br/><br/> '
SET @TableBody1= ' <br/> <br/><tr><td> Apreciado(a) usuario: </td><tr/><br/><br/> '
SET @TableBody2= ' <br/> <br/><tr><td> Los siguientes son datos que estan inconsistentes, el el registro de proveedores </td><tr/><br/><br/> '
SET @TableBody3= ' <br/> <br/><tr><td> Nit' + @nit +'-' + @Proveedor +'</td><tr/><br/><br/> '
SET @TableFinal= ' <br/> <br/><tr><td> <br/><br/>  Este correo electr�nico fue enviado por un sistema autom�tico. Por favor no responder a este correo </td><tr/><br/><br/> '

SET @TableTail = '</table></body></html>';
SET @TableHead = '<html><head>' +
				 '<style>' +
				 'td {border: solid black 1px;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;font-size:11pt;} ' +
				 '</style>' +
				 '</head>' +
				 '<body><table cellpadding=0 cellspacing=0 border=0>' +
				 '<tr bgcolor=#ccbfac><td align=center><b>M�dulo del sistema</b></td>' +
				 '<td align=center><b>Observaci�n</b></td>'+
				 '<td align=center><b></b></td>';


	SELECT @Body = (SELECT DISTINCT	ROW_NUMBER() OVER (ORDER BY TRRow) % 2 AS[TRRow],[TRRow],TD FROM(

SELECT 'Datos B�sicos'AS [TRRow],vdb.Observaciones AS [TD] FROM Proveedor.ValidarInfoDatosBasicosEntidad vdb
			  INNER JOIN Proveedor.EntidadProvOferente epo ON vdb.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad =@IdEntidad 			  and vdb.ConfirmaYAprueba = 0
			  UNION
SELECT 'Datos Financieros'AS [TRRow],vif.Observaciones AS [TD] FROM Proveedor.ValidarInfoFinancieraEntidad vif
			  INNER JOIN Proveedor.InfoFinancieraEntidad ifr ON vif.IdInfoFin=ifr.IdInfoFin
			  INNER JOIN Proveedor.EntidadProvOferente epo ON ifr.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad =@IdEntidad and vif.ConfirmaYAprueba = 0	  UNION 

SELECT 'Experiencia'AS [TRRow],vee.Observaciones AS [TD] FROM Proveedor.ValidarInfoExperienciaEntidad vee
			  INNER JOIN Proveedor.InfoExperienciaEntidad iee ON iee.IdExpEntidad=vee.IdExpEntidad
			  INNER JOIN Proveedor.EntidadProvOferente epo ON iee.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad=@IdEntidad and vee.ConfirmaYAprueba = 0
			 ) Tbtp
	FOR xml RAW ('tr'), ELEMENTS)

-- Replace the entity codes and row numbers
SET @Body = REPLACE(@Body, '_x0020_', SPACE(1))
SET @Body = REPLACE(@Body, '_x003D_', '=')
SET @Body = REPLACE(@Body, '<tr><TRRow>1</TRRow>', '<tr bgcolor=#e5d7c2>')
SET @Body = REPLACE(@Body, '<TRRow>0</TRRow>', '')

SELECT
	@Body =@TableBody + @TableBody1 + @TableBody2 + @TableBody3 + @TableHead + @Body + @TableTail + '<br/><br/>' + @TableFinal

PRINT @Body

-- Enviar correo
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'GonetMail', --colocar perfil que se tenga configurado
								@recipients = @Email_Destino, --destinatario
								@subject = @Asunto, --asunto
								@body = @Body, --cuerpo de correo
								@body_format = 'HTML'; --formato de correo


END


GO


