USE [SIA]
GO
/****** Object:  Table [Oferente].[EstadoTercero]    Script Date: 06/25/2013 16:49:06 ******/
-- =============================================
-- Author:		Fabián Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Se ingresan datos de estado tercero
-- =============================================
SET IDENTITY_INSERT [Oferente].[EstadoTercero] ON
IF NOT EXISTS( SELECT [IdEstadoTercero] FROM [Oferente].[EstadoTercero] WHERE  [DescripcionEstado] = 'Por Validar') BEGIN
INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (1, N'001', N'Por Validar', 1, N'Administrador', CAST(0x0000A13900000000 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT [IdEstadoTercero] FROM [Oferente].[EstadoTercero] WHERE  [DescripcionEstado] = 'Registrado') BEGIN
INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (4, N'002', N'Registrado', 1, N'Administrador', CAST(0x0000A13900000000 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT [IdEstadoTercero] FROM [Oferente].[EstadoTercero] WHERE  [DescripcionEstado] = 'Por Ajustar') BEGIN
INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (5, N'003', N'Por Ajustar', 1, N'Administrador', CAST(0x0000A13900000000 AS DateTime), NULL, NULL)
END
IF NOT EXISTS( SELECT [IdEstadoTercero] FROM [Oferente].[EstadoTercero] WHERE  [DescripcionEstado] = 'Validado') BEGIN
INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
VALUES (6, N'004', N'Validado', 1, N'Administrador', CAST(0x0000A13900000000 AS DateTime), NULL, NULL)
END
SET IDENTITY_INSERT [Oferente].[EstadoTercero] OFF
