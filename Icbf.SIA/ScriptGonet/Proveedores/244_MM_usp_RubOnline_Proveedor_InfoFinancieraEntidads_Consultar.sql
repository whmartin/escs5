USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]    Script Date: 06/27/2013 02:03:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]    Script Date: 06/27/2013 02:03:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoFinancieraEntidad
-- =============================================
ALTER PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]
	@IdEntidad INT = NULL, @IdVigencia INT = NULL
AS
BEGIN
 SELECT IdInfoFin, IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[InfoFinancieraEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END 
 ORDER BY IdVigencia DESC 
END


[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar] 1,5