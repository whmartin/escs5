USE [SIA]
GO

alter table Proveedor.InfoFinancieraEntidad alter column ActivoCte numeric(21,3) NOT NULL
alter table Proveedor.InfoFinancieraEntidad alter column ActivoTotal numeric(21,3) NOT NULL
alter table Proveedor.InfoFinancieraEntidad alter column PasivoCte numeric(21,3) NOT NULL
alter table Proveedor.InfoFinancieraEntidad alter column PasivoTotal numeric(21,3) NOT NULL
alter table Proveedor.InfoFinancieraEntidad alter column Patrimonio numeric(21,3) NOT NULL
alter table Proveedor.InfoFinancieraEntidad alter column GastosInteresFinancieros numeric(21,3) NOT NULL
alter table Proveedor.InfoFinancieraEntidad alter column UtilidadOperacional numeric(21,3) NOT NULL