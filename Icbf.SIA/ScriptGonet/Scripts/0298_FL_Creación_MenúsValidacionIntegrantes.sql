USE [SIA]
GO

-- =============================================
-- Author:	 Faiber Losada
-- Create date:  23/7/2014 7:00:00 AM
-- Description:	Creaci�n del Men� del parametrizador
-- =============================================

DECLARE @idModulo INT
DECLARE @tblRol TABLE (id INT IDENTITY, idRol INT)
DECLARE @idMenuPadre INT

SELECT @idModulo=IdModulo FROM SEG.Modulo WHERE NombreModulo='ValidarIntegrantes'

--Se adicionan los roles
INSERT INTO @tblRol (idRol) VALUES (1)

IF EXISTS(SELECT * FROM SEG.Modulo WHERE IdModulo=@idModulo)
BEGIN
	--Se crea el Men� del proceso
	
	INSERT INTO SEG.Programa(IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, VisibleMenu, generaLog)
	VALUES (@idModulo, 'ValidarIntegrantes', 'Proveedor/ValidarIntegrantes', 1, 1, 'Administrador', GETDATE(), 1, 1)

	SET @idMenuPadre = SCOPE_IDENTITY()

	INSERT INTO SEG.Permiso(IdPrograma, IdRol, Insertar, Modificar, Eliminar, Consultar, UsuarioCreacion, FechaCreacion)
	SELECT @idMenuPadre, idRol, 1, 0, 0, 1, 'Administrador', GETDATE() FROM @tblRol 

END
ELSE
BEGIN
	PRINT 'Ya existe el m�dulo configurado.'
END