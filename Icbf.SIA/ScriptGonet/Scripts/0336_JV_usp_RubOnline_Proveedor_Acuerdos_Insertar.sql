USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]    Script Date: 08/12/2014 11:26:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]    Script Date: 08/12/2014 11:26:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  26/06/2013 
-- Description:	Procedimiento almacenado que guarda un nuevo Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]

@IdAcuerdo INT OUTPUT, 
@Nombre NVARCHAR (64), 
@ContenidoHtml NVARCHAR (MAX), 
@UsuarioCrea NVARCHAR (250)

AS
BEGIN

INSERT INTO Proveedor.Acuerdos (Nombre, ContenidoHtml, UsuarioCrea, FechaCrea)
	VALUES (@Nombre, @ContenidoHtml, @UsuarioCrea, GETDATE())
SELECT
	@IdAcuerdo = SCOPE_IDENTITY()
	
END





GO


