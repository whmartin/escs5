USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 05/21/2014 17:00:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 05/21/2014 17:00:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






 
--Autor:Mauricio Martinez    
--Fecha:2013/07/15 16:00    
--Descripcion: Consulta para mostrar un resumen de los modulos validados   
--Modificado Por: juan Carlos Valverde S�mano
--Fecha:2014/05/14 13:07
--Descripci�n: Se agreg� retornar Liberar=1 para que el boton de Liberar pueda mostrare
-- en la parte de Validar Proveedor; esto Cuando, El estado del Modulo es en Validaci�n y no hay
-- registros en su entidad de Validaci�n(por ejemplo: Proveedor.ValidarInfoDatosBasicosEntidad)
-- Ya que esto no se tomaba en cuenta, pasaba que entraban registros en Validaci�n y no pod�an liberarse.
--Modificado Por: juan Carlos Valverde S�mano
--Fecha:2014/05/16 13:07
--Descripci�n: Se agrego la parte para que cuando Financiera o Experiencia tenga  alguno de sus registros "EN VALIDACI�N"
-- Retorne Liberar=1 para que en el formulario se muestre el Bot�n.
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    
@IdEntidad INT     
AS    
BEGIN    
    
--======================================================    
--Se obtiene el Nro de la Ultima Revision Datos Basicos    
--======================================================    
--declare @IdEntidad int    
--set @IdEntidad  = 22    
DECLARE @IdEstadoEnValidacionFin_Exp INT;
    
declare @UltimaRevisionDatosBasicos int     
set @UltimaRevisionDatosBasicos = (select Max(v.NroRevision) from  Proveedor.EntidadProvOferente e left join Proveedor.ValidarInfoDatosBasicosEntidad v on v.IdEntidad = e.IdEntidad where e.IdEntidad = @IdEntidad)    
    
--==========================================================    
--Se obtiene el Nro de la Ultima Revision de Info Financiera    
--==========================================================    
    
declare @UltimaRevisionFinanciera int     
set @UltimaRevisionFinanciera = (select Max(v.NroRevision) from  Proveedor.EntidadProvOferente e left join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad left join Proveedor.ValidarInfoFinancieraEntidad v on v.IdInfoFin = i.IdInfoFin where
  
 e.IdEntidad = @IdEntidad)    
    
--===========================================================    
--Se obtiene el Nro de la Ultima Revision de Info Experiencia    
--===========================================================    
    
declare @UltimaRevisionExperiencia int     
set @UltimaRevisionExperiencia = (select Max(v.NroRevision) from  Proveedor.EntidadProvOferente e left join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad left join Proveedor.ValidarInfoExperienciaEntidad v on v.IdExpEntidad = i.IdExpEntidad where e.IdEntidad = @IdEntidad)    
    
/**
SI Existen las tablas temporales... hay que borrarlas.
**/    
IF OBJECT_ID('tempdb.#UltimaRevisionDatosBasicos') IS NOT NULL  
  DROP TABLE #UltimaRevisionDatosBasicos    
IF OBJECT_ID('tempdb.#UltimaRevisionFinanciera') IS NOT NULL  
  DROP TABLE #UltimaRevisionFinanciera    
IF OBJECT_ID('tempdb.#UltimaRevisionExperiencia') IS NOT NULL  
  DROP TABLE #UltimaRevisionExperiencia    
    
    
--===================================================================================================    
--Se obtiene y se almacena temporalmente el registro completo de la ultima revision de Datos Basicos    
--===================================================================================================    
    
select v.IdEntidad, MAX(v.NroRevision) as NroRevision    
into #UltimaRevisionDatosBasicos    
from  Proveedor.EntidadProvOferente e     
 inner join Proveedor.ValidarInfoDatosBasicosEntidad v on v.IdEntidad = e.IdEntidad    
where e.IdEntidad = @IdEntidad    
group by v.IdEntidad    
    
--=====================================================================================================    
--Se obtiene y se almacena temporalmente el registro completo de la ultima revision de Info Financiera    
--=====================================================================================================    
    
select e.IdEntidad, v.IdInfoFin, MAX(v.NroRevision) as NroRevision    
into #UltimaRevisionFinanciera    
from  Proveedor.EntidadProvOferente e     
 inner join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad     
 inner join Proveedor.ValidarInfoFinancieraEntidad v on v.IdInfoFin = i.IdInfoFin    
where e.IdEntidad = @IdEntidad    
group by v.IdInfoFin,e.IdEntidad    
    
--=========================================================================================================    
--Se obtiene y se almacena temporalmente  el registro completo de la ultima revision de Info Experiencias    
--=========================================================================================================    
    
select e.IdEntidad, v.IdExpEntidad, MAX(v.NroRevision) as NroRevision    
into #UltimaRevisionExperiencia    
from  Proveedor.EntidadProvOferente e     
 inner join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad     
 inner join Proveedor.ValidarInfoExperienciaEntidad v on v.IdExpEntidad = i.IdExpEntidad     
where e.IdEntidad = @IdEntidad    
group by v.IdExpEntidad,e.IdEntidad    
    
--=========================================================================================================    
--Se crea tabla temp    
--=========================================================================================================    
IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'Proveedor' 
                 AND  TABLE_NAME = 'Resumen'))
                 BEGIN
DROP TABLE Proveedor.Resumen
END
create table Proveedor.Resumen (Orden int ,IdEntidad int, NroRevision int, Componente varchar(100), iConfirmaYAprueba int, Finalizado BIT, liberar BIT, iCorreoEnviado int)    
--=========================================================================================================    
--Se obtiene el resumen    
--=========================================================================================================    
     
 IF EXISTS(select * from #UltimaRevisionDatosBasicos where IdEntidad = @IdEntidad)    
 BEGIN    
   insert into Proveedor.Resumen     
      select 0 as Orden,     
     e.IdEntidad,     
     MAX(ISNULL(e.NroRevision,1)) as NroRevision,    
     'Datos B�sicos' as Componente,    
     MIN(case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END) AS iConfirmaYAprueba,    
     CAST(MIN(CAST(ISNULL(e.Finalizado,0) as int)) as bit) as Finalizado,    
     CASE WHEN ISNULL(MAX(e.IdEstado),0) = 5 THEN 1 ELSE 0 END as Liberar,
     MAX(case when v.CorreoEnviado IS NULL THEN -1 ELSE v.CorreoEnviado END) AS iCorreoEnviado
   from  Proveedor.EntidadProvOferente e    
    inner join Proveedor.ValidarInfoDatosBasicosEntidad v    
     on v.IdEntidad = e.IdEntidad and v.NroRevision = @UltimaRevisionDatosBasicos    
     inner join #UltimaRevisionDatosBasicos t    
      on v.IdEntidad = t.IdEntidad and v.NroRevision = t.NroRevision     
   where     
     e.IdEntidad = @IdEntidad and t.NroRevision is not null        
   group by e.IdEntidad    
 END    
 ELSE    
 BEGIN    
 DECLARE @IdEstado INT=(SELECT IdEstado FROM Proveedor.EntidadProvOferente
 WHERE IdEntidad=@IdEntidad)
 
   insert into Proveedor.Resumen     
   select  0 as Orden,     
     @IdEntidad as IdEntidad,      
     1 as NroRevision,    
     'Datos B�sicos' as Componente,    
     -1 AS iConfirmaYAprueba,    
     0 as Finalizado,    
     CASE WHEN @IdEstado = 5 THEN 1 ELSE 0 END as Liberar,
     -1 as iCorreoEnviado       
 END    
    
--=========================================================================================================    
--Se obtiene el resumen para financiera    
--=========================================================================================================    
--si hay informacion de financiera    
IF EXISTS(select * from Proveedor.EntidadProvOferente e inner join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad where e.IdEntidad = @IdEntidad)    
BEGIN    
SET @IdEstadoEnValidacionFin_Exp =(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
WHERE Descripcion='EN VALIDACI�N')
DECLARE @IdEstadoFin INT=0;
IF EXISTS(SELECT EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
WHERE IdEntidad=@IdEntidad AND EstadoValidacion=@IdEstadoEnValidacionFin_Exp)
BEGIN
SET @IdEstadoFin=@IdEstadoEnValidacionFin_Exp;
END

 --si hay al menos una informacion si validar    
 IF EXISTS(select * from Proveedor.EntidadProvOferente e     
    left join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad     
    left join Proveedor.ValidarInfoFinancieraEntidad v    
      on v.IdInfoFin = i.IdInfoFin where e.IdEntidad = @IdEntidad and v.IdInfoFin is null)    
 BEGIN    
   insert into Proveedor.Resumen     
   select 1 as Orden,    
     e.IdEntidad,         
     MAX(ISNULL(i.NroRevision,1)) as NroRevision,    
     'Financiera' as Componente,    
     -1 AS iConfirmaYAprueba,    
     cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,    
     CASE WHEN @IdEstadoFin = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,
     -1 as iCorreoEnviado   
   from  Proveedor.EntidadProvOferente e    
    inner join Proveedor.InfoFinancieraEntidad i    
     on e.IdEntidad = i.IdEntidad         
   where     
     e.IdEntidad  = @IdEntidad    
   group by e.IdEntidad    
 END    
 ELSE    
 BEGIN    
  -- todas tienen validacion    
  IF EXISTS(select * from #UltimaRevisionFinanciera where IdEntidad = @IdEntidad)    
  BEGIN    
   insert into Proveedor.Resumen     
   select 1 as Orden,     
     e.IdEntidad,      
     MAX(ISNULL(i.NroRevision,1)) as NroRevision,    
     'Financiera' as Componente,    
     MIN(case when i.IdInfoFin IS NULL THEN -2 ELSE case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END END) AS iConfirmaYAprueba,    
     cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,    
     CASE WHEN @IdEstadoFin = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,
     MAX(case when i.IdInfoFin IS NULL THEN -2 ELSE case when v.CorreoEnviado IS NULL THEN -1 ELSE v.CorreoEnviado END END) AS iCorreoEnviado   
   from      
     Proveedor.EntidadProvOferente e    
     inner join Proveedor.InfoFinancieraEntidad i    
      on e.IdEntidad = i.IdEntidad     
     inner join Proveedor.ValidarInfoFinancieraEntidad v    
      on v.IdInfoFin = i.IdInfoFin and v.NroRevision = @UltimaRevisionFinanciera    
     inner join #UltimaRevisionFinanciera t    
      on t.IdInfoFin = v.IdInfoFin and t.NroRevision = v.NroRevision     
    
   where     
     e.IdEntidad  = @IdEntidad and t.NroRevision is not null    
   group by e.IdEntidad     
  END    
 END    
END     
ELSE /*No hay informacion*/    
BEGIN  

DECLARE @IdEstadoFinanciera INT   
   insert into Proveedor.Resumen     
   select  1 as Orden,     
     @IdEntidad as IdEntidad,      
     1 as NroRevision,    
     'Financiera' as Componente,    
     -2 AS iConfirmaYAprueba,    
     0 as Finalizado,    
     CASE WHEN @IdEstadoFin = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,
     -2 AS iCorreoEnviado      
END    
    
--=========================================================================================================    
--Se obtiene el resumen para experiencias    
--=========================================================================================================    
--si hay informacion de experiencias    
IF EXISTS(select * from Proveedor.EntidadProvOferente e inner join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad where e.IdEntidad = @IdEntidad)    
BEGIN    

SET @IdEstadoEnValidacionFin_Exp =(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
WHERE Descripcion='EN VALIDACI�N')
DECLARE @IdEstadoExp INT=0;
IF EXISTS(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
WHERE IdEntidad=@IdEntidad AND EstadoDocumental=@IdEstadoEnValidacionFin_Exp)
BEGIN
SET @IdEstadoExp=@IdEstadoEnValidacionFin_Exp;
END


 --si hay al menos una informacion si validar    
 IF EXISTS(select * from Proveedor.EntidadProvOferente e     
    left join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad     
    left join Proveedor.ValidarInfoExperienciaEntidad v    
      on v.IdExpEntidad = i.IdExpEntidad where e.IdEntidad = @IdEntidad and v.IdExpEntidad is null)    
 BEGIN    
   insert into Proveedor.Resumen     
   select 2 as Orden,    
     e.IdEntidad,         
     MAX(ISNULL(i.NroRevision,1)) as NroRevision,    
     'Experiencias' as Componente,    
     -1 AS iConfirmaYAprueba,    
     cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,    
     CASE WHEN @IdEstadoExp = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,
     -1 as iCorreoEnviado    
   from  Proveedor.EntidadProvOferente e    
    inner join Proveedor.InfoExperienciaEntidad i    
     on e.IdEntidad = i.IdEntidad         
   where     
     e.IdEntidad  = @IdEntidad    
   group by e.IdEntidad    
 END    
 ELSE    
 BEGIN    
 -- todas tienen validacion    
  IF EXISTS(select * from #UltimaRevisionExperiencia where IdEntidad = @IdEntidad)    
  BEGIN    
   insert into Proveedor.Resumen     
   select 2 as Orden,    
     e.IdEntidad,         
     MAX(ISNULL(i.NroRevision,1)) as NroRevision,    
     'Experiencias' as Componente,    
     MIN(case when i.IdExpEntidad IS NULL THEN -2 ELSE case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END END) AS iConfirmaYAprueba,    
     cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,    
     CASE WHEN @IdEstadoExp = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,
     MAX(case when i.IdExpEntidad IS NULL THEN -2 ELSE case when v.CorreoEnviado IS NULL THEN -1 ELSE v.CorreoEnviado END END) AS iCorreoEnviado
   from  Proveedor.EntidadProvOferente e    
    inner join Proveedor.InfoExperienciaEntidad i    
     on e.IdEntidad = i.IdEntidad     
    inner join Proveedor.ValidarInfoExperienciaEntidad v    
     on v.IdExpEntidad = i.IdExpEntidad and v.NroRevision = @UltimaRevisionExperiencia    
    inner join #UltimaRevisionExperiencia t    
      on t.IdExpEntidad = v.IdExpEntidad and t.NroRevision = v.NroRevision     
   where     
     e.IdEntidad  = @IdEntidad     
   group by e.IdEntidad    
  END    
 END    
END    
ELSE /*No hay informacion*/    
BEGIN    
   insert into Proveedor.Resumen    
   select  2 as Orden,     
     @IdEntidad as IdEntidad,      
     1 as NroRevision,    
     'Experiencias' as Componente,    
     -2 AS iConfirmaYAprueba,    
     0 as Finalizado,    
     CASE WHEN @IdEstadoExp = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,
     -2 AS iCorreoEnviado       
END    
    
select * from Proveedor.Resumen Order By Orden    
drop table Proveedor.Resumen    
drop table #UltimaRevisionDatosBasicos    
drop table #UltimaRevisionFinanciera
drop table #UltimaRevisionExperiencia        
END    
    



GO


