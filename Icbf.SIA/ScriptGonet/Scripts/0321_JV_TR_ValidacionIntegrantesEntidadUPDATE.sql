USE [SIA]
GO

/****** Object:  Trigger [TR_ValidacionIntegrantesEntidadUPDATE]    Script Date: 08/06/2014 13:35:51 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TR_ValidacionIntegrantesEntidadUPDATE]'))
DROP TRIGGER [PROVEEDOR].[TR_ValidacionIntegrantesEntidadUPDATE]
GO

/****** Object:  Trigger [PROVEEDOR].[TR_ValidacionIntegrantesEntidadUPDATE]    Script Date: 08/06/2014 13:36:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/**
-- Autor: Juan Carlos Valverde S�mano
-- Fecha: 30-Julio-2014
-- Descripci�n:	Actualiza Estado de Proveedor de acuerdo al estado de los modulos
-- Datos Basicos e Integrantes.
**/
CREATE TRIGGER [PROVEEDOR].[TR_ValidacionIntegrantesEntidadUPDATE] 
   ON  [PROVEEDOR].[ValidacionIntegrantesEntidad]
   AFTER INSERT,UPDATE,DELETE
AS 
BEGIN
	DECLARE @TBLESTADOS TABLE
	(IdEstadoProveedor INT)

	DECLARE @IdEntidad INT,
	@IdEstadoDatosBasicos INT,
	@IdEstadoIntegrantes INT=0,
	@IdEdoValidacionDocIntegrantes INT =0,
	@IdEnValidacionDBasicos INT=0
	--
	DECLARE @countInserted INT=0,
	@countDeleted INT=0
	--
	SET @countInserted=(SELECT  IDENTIDAD FROM inserted)
	SET @countDeleted=(SELECT  IDENTIDAD FROM deleted)
	---------------------------------
	IF(@countInserted =0 AND @countDeleted>0)
	BEGIN
		--Si solo hay datos en Datos Basicos, es decir, se ejecut� un DELETE...
		SET @IdEntidad = (SELECT  IDENTIDAD FROM deleted)
		SELECT @IdEstadoDatosBasicos=IdEstado FROM PROVEEDOR.EntidadProvOferente
		WHERE IdEntidad = @IdEntidad
		INSERT INTO @TBLESTADOS
		SELECT IdEstadoProveedor
		FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,NULL,1,@IdEstadoIntegrantes)
	END
	ELSE IF (@countDeleted>0 AND @countInserted>0) --SI SE TRATA DE UN UPDATE
	BEGIN
		--Si es un Update y el Estado de validaci�n ha cambiado.
		IF((SELECT IdEstadoValidacionIntegrantes FROM inserted)!=(SELECT IdEstadoValidacionIntegrantes FROM deleted))
		BEGIN


			SET @IdEntidad = (SELECT  IDENTIDAD FROM inserted)
			SET @IdEstadoDatosBasicos = 0
			SET @IdEstadoIntegrantes = 0

			DELETE FROM @TBLESTADOS
			
			
			SELECT @IdEstadoDatosBasicos=IdEstado FROM PROVEEDOR.EntidadProvOferente
			WHERE IdEntidad = @IdEntidad
			
			SELECT @IdEdoValidacionDocIntegrantes = IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
			WHERE Descripcion='EN VALIDACI�N'
			
			SET  @IdEnValidacionDBasicos =(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
			WHERE Descripcion='EN VALIDACI�N')
				
			--SI EXISTE REGISTRO DE VALIDACI�N PARA INTEGRANTES
			IF EXISTS(SELECT IdValidacionIntegrantesEntidad FROM PROVEEDOR.ValidacionIntegrantesEntidad
			WHERE IdEntidad = @IdEntidad)
			BEGIN
					SELECT  @IdEstadoIntegrantes=IdEstadoValidacionIntegrantes FROM PROVEEDOR.ValidacionIntegrantesEntidad
					WHERE IdEntidad = @IdEntidad
			END
			---SI ALGUNO DE LOS DOS ESTADOS ES EN VALIDACI�N
			IF((@IdEnValidacionDBasicos=@IdEstadoDatosBasicos) OR
			(@IdEdoValidacionDocIntegrantes=@IdEstadoIntegrantes))
			BEGIN
				INSERT INTO @TBLESTADOS
				SELECT IdEstadoProveedor
				FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,1,1,@IdEstadoIntegrantes)
			END
			ELSE
			BEGIN
				--Si solo hay informaci�n en Datos Basicos
				IF (@IdEstadoIntegrantes =0)
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,NULL,1,@IdEstadoIntegrantes)
				END
				--Si los estados son Diferentes entonces es Parcial.
				ELSE IF(
				(SELECT Descripcion FROM PROVEEDOR.EstadoDatosBasicos
				WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos) !=
				(SELECT Descripcion FROM PROVEEDOR.EstadoIntegrantes WHERE
				IdEstadoIntegrantes=@IdEstadoIntegrantes))
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,1,NULL,1,@IdEstadoIntegrantes)
				END
				ELSE
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,NULL,1,@IdEstadoIntegrantes)
				END
			END
		END
	END
	ELSE IF(@countInserted>0 AND @countDeleted=0)
	BEGIN
----------------------------------------INSERTED----------------------------


			SET @IdEntidad = (SELECT  IDENTIDAD FROM inserted)
			SET @IdEstadoDatosBasicos = 0
			SET @IdEstadoIntegrantes = 0

			DELETE FROM @TBLESTADOS
			
			
			SELECT @IdEstadoDatosBasicos=IdEstado FROM PROVEEDOR.EntidadProvOferente
			WHERE IdEntidad = @IdEntidad
			
			SELECT @IdEdoValidacionDocIntegrantes = IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
			WHERE Descripcion='EN VALIDACI�N'
			
			SET  @IdEnValidacionDBasicos =(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
			WHERE Descripcion='EN VALIDACI�N')
				
			SELECT  @IdEstadoIntegrantes=IdEstadoValidacionIntegrantes FROM PROVEEDOR.ValidacionIntegrantesEntidad
			WHERE IdEntidad = @IdEntidad
					
			---SI ALGUNO DE LOS DOS ESTADOS ES EN VALIDACI�N
			IF((@IdEnValidacionDBasicos=@IdEstadoDatosBasicos) OR
			(@IdEdoValidacionDocIntegrantes=@IdEstadoIntegrantes))
			BEGIN
				INSERT INTO @TBLESTADOS
				SELECT IdEstadoProveedor
				FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,1,1,@IdEstadoIntegrantes)
			END
			ELSE
			BEGIN
				--Si los estados son Diferentes entonces es Parcial.
				IF(
				(SELECT Descripcion FROM PROVEEDOR.EstadoDatosBasicos
				WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos) !=
				(SELECT Descripcion FROM PROVEEDOR.EstadoIntegrantes WHERE
				IdEstadoIntegrantes=@IdEstadoIntegrantes))
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,1,NULL,1,@IdEstadoIntegrantes)
				END
				ELSE
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,NULL,1,@IdEstadoIntegrantes)
				END
			END
----------------------------------------------------------------------------
	END
		UPDATE PROVEEDOR.EntidadProvOferente
		SET IdEstadoProveedor =(SELECT TOP(1) IdEstadoProveedor FROM @TBLESTADOS)
		WHERE IdEntidad=@IdEntidad
END

GO


