USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  02/26/2014 3:18:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) Segmento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]
	@Codigo NVARCHAR(64) = NULL, 
	@Descripcion NVARCHAR(128) = NULL,
	@Estado BIT = NULL,
	@LongitudUNSPSC INT = NULL,
	@CodigoSegmento NVARCHAR(50)=NULL,
	@CodigoFamilia NVARCHAR(50)=NULL,
	@CodigoClase NVARCHAR(50) = NULL
	
AS
BEGIN
 SELECT IdTipoCodUNSPSC, 
		Codigo, 
		Descripcion, 
		Estado, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica
 FROM	[Proveedor].[TipoCodigoUNSPSC] 
 WHERE Codigo LIKE CASE WHEN @Codigo IS NULL THEN Codigo ELSE @Codigo + '%' END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
 --AND (CASE WHEN @LongitudUNSPSC IS NOT NULL 
	--	THEN (CASE WHEN LEN(Codigo)<=@LongitudUNSPSC THEN 1 ELSE 0 END) 
	--	ELSE 1 END)=1
 AND	CodigoSegmento = CASE WHEN @CodigoSegmento IS NULL THEN CodigoSegmento ELSE @CodigoSegmento END
 AND	CodigoFamilia = CASE WHEN @CodigoFamilia IS NULL THEN CodigoFamilia ELSE @CodigoFamilia END
 AND	CodigoClase = CASE WHEN @CodigoClase IS NULL THEN CodigoClase ELSE @CodigoClase END
END



