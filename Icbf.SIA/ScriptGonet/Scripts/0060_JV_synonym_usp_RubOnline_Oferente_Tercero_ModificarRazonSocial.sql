USE [SIA]
GO

/****** Object:  Synonym [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]    Script Date: 04/02/2014 05:11:39 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'usp_RubOnline_Oferente_Tercero_ModificarRazonSocial')
DROP SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]
GO

USE [SIA]
GO

/****** Object:  Synonym [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]    Script Date: 04/02/2014 05:11:39 ******/
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial] FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]
GO


