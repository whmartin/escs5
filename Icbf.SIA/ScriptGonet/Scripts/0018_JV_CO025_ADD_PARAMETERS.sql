/**
Autor: Juan Carlos Valverde S�mano
Fecha Creaci�n : 25-Feb-2014
Descripci�n: De acuerdo a los requerimientos del CO025, este script agrega dos nuevos par�metros.
**/
USE [SIA]
GO

IF NOT EXISTS(
SELECT IdParametro FROM [SEG].[Parametro]
WHERE NombreParametro='Cambio de estado')
BEGIN
	INSERT INTO [SEG].[Parametro]
			   ([NombreParametro]
			   ,[ValorParametro]
			   ,[ImagenParametro]
			   ,[Estado]
			   ,[Funcionalidad]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('Cambio de estado'
			   ,'Este texto es configurable'
			   ,NULL
			   ,'1'
			   ,'Cambio de estado'
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)           
END


IF NOT EXISTS(
SELECT IdParametro FROM [SEG].[Parametro]
WHERE NombreParametro='�Qu� es un Tercero?')
BEGIN
	INSERT INTO [SEG].[Parametro]
			   ([NombreParametro]
			   ,[ValorParametro]
			   ,[ImagenParametro]
			   ,[Estado]
			   ,[Funcionalidad]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('�Qu� es un Tercero?'
			   ,'Este texto es configurable'
			   ,NULL
			   ,'1'
			   ,'Registrar Tercero'
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)           
END