USE [SIA]
GO

/****** Object:  Trigger [EntidadProvOferenteINSERT]    Script Date: 07/31/2014 10:40:02 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[EntidadProvOferenteINSERT]'))
DROP TRIGGER [PROVEEDOR].[EntidadProvOferenteINSERT]
GO

/****** Object:  Trigger [PROVEEDOR].[EntidadProvOferenteINSERT]    Script Date: 07/31/2014 10:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 11-Abril-2014
-- Description:	Cuando se inserta un Proveedor, se actualizan el estado de Proveedor 
-- en base a la tabla de Proveedor.ReglasEstadoProveedor
--Nota.- Tal vez de inicio no es necesario, pero se ha creado pensando que si cambian las reglas de
-- Estado, solo habria q hacer el Insert en la tabla de reglas y listo.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[EntidadProvOferenteINSERT] 
   ON  [PROVEEDOR].[EntidadProvOferente]
   AFTER INSERT
AS 
BEGIN
DECLARE @IdEntidad INT
,@IdEdoDatosBasicos INT
,@IdEstadoProveedor INT

SELECT @IdEntidad=IdEntidad,
@IdEdoDatosBasicos=IdEstado
FROM inserted

SELECT @IdEstadoProveedor=IdEstadoProveedor
FROM dbo.GetIdEstadoProveedor(@IdEdoDatosBasicos,0,0,NULL,NULL,NULL,NULL)

UPDATE [PROVEEDOR].[EntidadProvOferente]
SET IdEstadoProveedor=@IdEstadoProveedor
WHERE IdEntidad=@IdEntidad
END




GO


