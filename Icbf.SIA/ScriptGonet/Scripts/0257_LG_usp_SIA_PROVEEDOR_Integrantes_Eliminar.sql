USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_Integrantes_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Eliminar]
GO

-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que elimina un(a) Integrantes
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Eliminar]
	@IdIntegrante INT
AS
BEGIN
	DELETE PROVEEDOR.Integrantes WHERE IdIntegrante = @IdIntegrante
END
