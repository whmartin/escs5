USE [SIA]
GO

IF  EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ValidarInfoDatosBasicosEntidad' 
	AND column_name = 'Observaciones')
BEGIN 
	ALTER TABLE PROVEEDOR.ValidarInfoDatosBasicosEntidad 
	ALTER COLUMN Observaciones nvarchar(200) not null;
END