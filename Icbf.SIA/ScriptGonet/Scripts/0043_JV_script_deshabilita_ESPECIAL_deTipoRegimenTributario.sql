USE [SIA]
GO
-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date:  31/03/2014
-- Description:	Script que deshabilita las Opciones "ESPECIAL" de la tabla 
--[Proveedor].[TipoRegimenTributario]
-- =============================================
UPDATE [Proveedor].[TipoRegimenTributario]
SET Estado=0
WHERE Descripcion='ESPECIAL'