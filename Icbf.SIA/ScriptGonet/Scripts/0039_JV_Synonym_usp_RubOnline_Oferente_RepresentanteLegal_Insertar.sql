USE [SIA]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]    Script Date: 03/25/2014 17:34:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]
GO

USE [SIA]
GO

/****** Object:  Synonym [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]    Script Date: 03/30/2014 07:20:18 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'usp_RubOnline_Oferente_RepresentanteLegal_Insertar')
DROP SYNONYM [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]
GO

USE [SIA]
GO

/****** Object:  Synonym [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]    Script Date: 03/30/2014 07:20:18 ******/
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar] FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]
GO