USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_By_IdEntidad]    Script Date: 11/07/2014 05:49:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_By_IdEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_By_IdEntidad]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_By_IdEntidad]    Script Date: 11/07/2014 05:49:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 07/NOV/2014
-- Description:	Obtiene la auditoria de acciones en Datos Basicos de una EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_By_IdEntidad]
@IdEntidad INT
AS
BEGIN
	
	SELECT 
	IdAccion
	,IdEntidad
	,Fecha
	,Usuario
	,IdSistema
	,Sistema
	,Accion
	 FROM AUDITA.LogAccionesDatosBasicos
	 WHERE IdEntidad=@IdEntidad
	
END

GO


