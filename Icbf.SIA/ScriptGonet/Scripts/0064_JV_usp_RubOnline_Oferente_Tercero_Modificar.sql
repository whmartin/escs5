USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Modificar]    Script Date: 04/03/2014 12:16:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Modificar]
GO

USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Modificar]    Script Date: 04/03/2014 12:16:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Gutierrez, Liminal
-- Create date:  07/08/2013 16:04:23 PM
-- Description:	Actualiza tercero, modificaci�n por la adici�n del campo EsFundacion a la tabla Tercero
-- Modificaci�n: 24-FEB-2014
-- Desarrolldaor Juan Carlos Valverde S�mano
-- Descripci�n: En base al control de cambios CO_024, se ha agrega lo sigueinte.
--				Compara si los datos que vienen del formulario de registro de terceros
--				Son los mismos al macenados en SEG.Usuario (nombres y apellidos � raz�n social) 
--				de acuerdo al tipo de Persona, y en caso de ser diferentes actualiza la tabla SEG.Usuario
--				con esta nueva informaci�n.
-- Modificaci�n: 03-ABRIL-2014
-- Desarrolldaor Juan Carlos Valverde S�mano
-- Descripci�n: Se agreg� un validaci�n mas que dice: Si se trata de un Usuario Externo
-- Entonces pasa a actualizar la informaci�n en la entidad de Usuario, en caso de ser un
-- Usuario Interno o Administrador, solo registra el Tercero y termina.
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Modificar]
	 @IdTercero  INT,
	 @IdDListaTipoDocumento INT= NULL,
	 @IdTipoPersona INT= NULL,
	 @Email nvarchar(40)= NULL,
	 @NumeroIdentificacion nvarchar(128)= NULL,
	 @FechaExpedicionId datetime= NULL,
	 @Sexo nvarchar(1)= NULL,
	 @RazonSocial  nvarchar(128)= NULL,
	 @Digitoverificacion INT= NULL,
	 @PrimerNombre nvarchar(128)= NULL,
	 @SegundoNombre nvarchar(128)= NULL,
	 @PrimerApellido nvarchar(128)= NULL,
	 @SegundoApellido nvarchar(128)= NULL,
	 @FechaNacimiento datetime= NULL,
	 @UsuarioModifica nvarchar(128)= NULL,
	 @FechaModifica datetime= NULL,
	 @EsFundacion bit = NULL
AS
BEGIN
	UPDATE Oferente.TERCERO SET
	 IDTIPODOCIDENTIFICA= ISNULL(@IdDListaTipoDocumento,IDTIPODOCIDENTIFICA),
		IdTipoPersona= ISNULL(@IdTipoPersona, IdTipoPersona),
		CORREOELECTRONICO = ISNULL( @Email, CORREOELECTRONICO ),
		NUMEROIDENTIFICACION = ISNULL( @NumeroIdentificacion, NUMEROIDENTIFICACION ),
		FECHAEXPEDICIONID = ISNULL( @FechaExpedicionId, FECHAEXPEDICIONID ),
		SEXO = ISNULL( @Sexo, SEXO ),
		RAZONSOCIAL= ISNULL(@RazonSocial, RAZONSOCIAL),
		PRIMERNOMBRE= ISNULL(@PrimerNombre, PRIMERNOMBRE),
		SEGUNDONOMBRE= ISNULL(@SegundoNombre, SEGUNDONOMBRE),
		PRIMERAPELLIDO= ISNULL(@PrimerApellido, PRIMERAPELLIDO),
		SEGUNDOAPELLIDO= ISNULL(@SegundoApellido, SEGUNDOAPELLIDO),
		DIGITOVERIFICACION= ISNULL(@Digitoverificacion, DIGITOVERIFICACION),
		FECHANACIMIENTO= ISNULL(@FechaNacimiento, FECHANACIMIENTO),
		USUARIOMODIFICA = ISNULL( @UsuarioModifica, USUARIOMODIFICA ),
		FECHAMODIFICA = ISNULL( @FechaModifica ,  FECHAMODIFICA ),
		ESFUNDACION = ISNULL( @EsFundacion ,  ESFUNDACION )

	WHERE IDTERCERO = @IdTercero
DECLARE @ProviderUserKey uniqueidentifier
SET @ProviderUserKey=(
SELECT ProviderUserKey 
FROM Oferente.TERCERO 
WHERE IDTERCERO=@IdTercero )
--VERIFICAR SI SE TRATA DE UN USUARIO EXTERNO (REGISTRADO DESDE PROVEEDORES)---- SI ES ASI PARA PASAR A VALIDAR A ACTUALIZAR SU INFORMACI�N
--EN LA ENTIDAD USUARIO, EN CASO CONTRARIO NO REALIZA LA ACTUALIZACI�N.
IF((SELECT CorreoElectronico FROM SEG.Usuario
WHERE providerKey=@ProviderUserKey) = @Email)
BEGIN
--------------VERIFICAR DATOS CON SEG.Usuario Y SI DIFIEREN ....ACTUALIZARLOS--------------------------------
			
		IF (@IdTipoPersona=1)
		BEGIN
			DECLARE @primer_Nombre NVARCHAR(150),
			 @segundo_Nombre NVARCHAR(150),
			 @primer_Apellido NVARCHAR(150),
			 @segundo_Apellido NVARCHAR(150)
			 
			 SELECT @primer_Nombre=ISNULL([PrimerNombre],''),
			 @segundo_Nombre=ISNULL([SegundoNombre],''),
			 @primer_Apellido=ISNULL([PrimerApellido],''),
			 @segundo_Apellido=ISNULL([SegundoApellido],'')
			 FROM SIA.SEG.Usuario
			 WHERE providerKey=@ProviderUserKey
			 
			 IF(@PrimerNombre!=@primer_Nombre)
			 BEGIN
				 UPDATE SIA.SEG.Usuario SET PrimerNombre=@PrimerNombre
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@SegundoNombre!=@segundo_Nombre)
			 BEGIN
				 UPDATE SIA.SEG.Usuario SET SegundoNombre=@SegundoNombre
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@PrimerApellido!= @primer_Apellido)
			 BEGIN
				 UPDATE SIA.SEG.Usuario SET PrimerApellido=@PrimerApellido
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@SegundoApellido!=@segundo_Apellido)
			 BEGIN
				 UPDATE SIA.SEG.Usuario SET SegundoApellido=@SegundoApellido
				 WHERE providerKey=@ProviderUserKey
			 END
		END 
		ELSE IF (@IdTipoPersona=2)
		BEGIN
			DECLARE @razon_Social NVARCHAR(256)
			SELECT @razon_Social=ISNULL([RazonSocial],'')
			FROM SIA.SEG.Usuario
			WHERE providerKey=@ProviderUserKey
			
			IF(@RAZONSOCIAL!=@razon_Social)
			 BEGIN
				 UPDATE SIA.SEG.Usuario SET RazonSocial=@RAZONSOCIAL
				 WHERE providerKey=@ProviderUserKey
			 END
		END
END	
	
	------------------------------------------------
END

GO


