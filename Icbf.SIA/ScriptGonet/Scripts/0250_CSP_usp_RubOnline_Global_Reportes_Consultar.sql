USE [RubOnline]
GO


If (select Count(*) from sysobjects where type='P' and name='usp_RubOnline_Global_Reportes_Consultar')>0 
BEGIN
	DROP PROCEDURE dbo.usp_RubOnline_Global_Reportes_Consultar;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	SP's para CRUD de Global.Reporte
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Reportes_Consultar]
	 @NombreReporte NVARCHAR(512) = NULL
	,@Servidor NVARCHAR(512) = NULL
AS
BEGIN
 SELECT IdReporte
       ,NombreReporte
       ,Descripcion
       ,Servidor
       ,Carpeta
       ,NombreArchivo
       ,UsuarioCrea
       ,FechaCrea
       ,UsuarioModifica
       ,FechaModifica 
 FROM [Global].[Reporte] (NoLock)
 WHERE (NombreReporte = @NombreReporte Or @NombreReporte Is Null)
       AND (Servidor = @Servidor Or @Servidor Is Null)
END
