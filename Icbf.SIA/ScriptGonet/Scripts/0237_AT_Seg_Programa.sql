USE [RubOnline]
GO

-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  21/05/2014
-- Description:	Agrega columnas para controld e reportes
-- =============================================

IF (SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='Programa' And TABLE_SCHEMA = 'SEG' And COLUMN_NAME = 'EsReporte')=0 ALTER TABLE SEG.Programa ADD EsReporte INT NULL
IF (SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='Programa' And TABLE_SCHEMA = 'SEG' And COLUMN_NAME = 'IdReporte')=0 ALTER TABLE SEG.Programa ADD IdReporte INT NULL
GO
Update SEG.Programa Set EsReporte = 0 Where EsReporte Is Null