USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_AUDITA.LogAccionesDatosBasicos_Get_Fecha_Migracion]    Script Date: 11/06/2014 16:56:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_Fecha_Migracion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_Fecha_Migracion]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_AUDITA.LogAccionesDatosBasicos_Get_Fecha_Migracion]    Script Date: 11/06/2014 16:56:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 06/NOV/2014
-- Description:	Obtiene la Fecha de Migraci�n de alguna Entidad.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_Fecha_Migracion]
	@IdEntidad INT
	AS
BEGIN
SELECT Fecha FROM AUDITA.LogAccionesDatosBasicos
WHERE IdEntidad =@IdEntidad AND Accion='Migraci�n'
END

GO


