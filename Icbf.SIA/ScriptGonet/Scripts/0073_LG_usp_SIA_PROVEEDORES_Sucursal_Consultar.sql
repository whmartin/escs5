USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  03/04/2014 1:56:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]
	@IdSucursal INT
AS
BEGIN
 SELECT IdSucursal, 
		IdEntidad, 
		Nombre, 
		Indicativo, 
		Telefono, 
		Extension, 
		Celular, 
		Correo, 
		Estado,
		IdZona,
		Departamento, 
		Municipio, 
		Direccion, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica,
		CASE 
		WHEN Editable IS NULL  THEN 1
		ELSE Editable
		END AS Editable
		FROM [Proveedor].[Sucursal] WHERE  IdSucursal = @IdSucursal
END

