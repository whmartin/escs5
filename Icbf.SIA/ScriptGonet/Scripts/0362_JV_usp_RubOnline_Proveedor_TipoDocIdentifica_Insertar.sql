USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]    Script Date: 08/12/2014 12:06:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]    Script Date: 08/12/2014 12:06:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]
		@IdTipoDocIdentifica INT OUTPUT, 	@CodigoDocIdentifica NUMERIC,	@Descripcion NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoDocIdentifica(CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoDocIdentifica, @Descripcion, @UsuarioCrea, GETDATE())
	SELECT @IdTipoDocIdentifica = SCOPE_IDENTITY() 		
END


GO


