USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]    Script Date: 08/07/2014 15:15:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]    Script Date: 08/07/2014 15:15:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- Autor: Mauricio Martinez
-- Fecha: 2013/07/28
-- Descripcion: Procedimiento usado para validar el proveedor m�dulos d�tos b�sicos, info financiero o experiencia
-- Autor: Juan Carlos Valverde S�mano
-- Fecha:2014/08/07
-- Descripci�n: Se agreg� el funcionamiento para realizar el update al m�dulo integrantes, y se col�c� un if,else
-- para realizar el update solo en los m�dulos correspondientes al tipo de persona.
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision](@IdEntidad INT)
as
begin

DECLARE @IdTipoPersona INT=(
SELECT IdTipoPersona FROM Oferente.TERCERO
WHERE IDTERCERO = (
SELECT IdTercero FROM PROVEEDOR.EntidadProvOferente
WHERE IdEntidad=@IdEntidad))


 IF(@IdTipoPersona IN(1,2))
 BEGIN
	update Proveedor.InfoFinancieraEntidad  
	set  Finalizado = 1		
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and i.NroRevision = v.NroRevision
		and i.EstadoValidacion = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRM� CON SI
	
	
	update Proveedor.InfoFinancieraEntidad  
	set NroRevision = v.NroRevision + 1,
		Finalizado = 0	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		and i.EstadoValidacion <> 4 --SI NO ESTA VALIDADO AUMENTA EL NRO DE REVISION
		and v.ConfirmaYAprueba  <> 1 -- Y SI NO CONFIRM� CON SI
		
	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = 1	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		and i.EstadoDocumental = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRM� CON SI
	
	
	update Proveedor.InfoExperienciaEntidad 
	set NroRevision = v.NroRevision + 1,
		Finalizado = 0
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and i.NroRevision = v.NroRevision
		and i.EstadoDocumental <> 4 --SI NO ESTA VALIDADO AUMENTA EL NRO DE REVISION
		and v.ConfirmaYAprueba <> 1 -- Y SI NO CONFIRM� CON SI
		
END
ELSE IF (@IdTipoPersona IN(3,4))
BEGIN		
-------------------------------INTEGRANTES------------
	UPDATE PROVEEDOR.ValidacionIntegrantesEntidad
	SET Finalizado=1
	FROM PROVEEDOR.ValidacionIntegrantesEntidad INTVAL
	INNER JOIN PROVEEDOR.ValidarInfoIntegrantesEntidad v
	ON INTVAL.IdEntidad=v.IdEntidad
	WHERE INTVAL.IdEntidad=@IdEntidad
	AND INTVAL.IdEstadoValidacionIntegrantes=4 --Si ESTA VALIDADO
	and v.ConfirmaYAprueba=1 -- Y SI CONFIRMO CON UN SI
	
	UPDATE PROVEEDOR.ValidacionIntegrantesEntidad 
	SET NroRevision=INTVAL.NroRevision +1, Finalizado=0
	FROM PROVEEDOR.ValidacionIntegrantesEntidad INTVAL 
	INNER JOIN PROVEEDOR.ValidarInfoIntegrantesEntidad v
	ON v.IdEntidad=INTVAL.IdEntidad AND v.NroRevision=INTVAL.NroRevision
	WHERE INTVAL.IdEntidad=@IdEntidad
	AND INTVAL.IdEstadoValidacionIntegrantes <> 4 --SI NO ESTA VALIDACO
	and v.ConfirmaYAprueba <> 1 --Y SI NO HAN CONFIRMADO CON UN SI
-------------------------------------------------------
END

	update Proveedor.EntidadProvOferente    
	set Finalizado = 1
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
	where e.IdEntidad = @IdEntidad
		and IdEstado = 2  -- SI ESTA VALIDADO
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMO CON UN SI
			

		
	update Proveedor.EntidadProvOferente    
	set NroRevision = e.NroRevision + 1,
		Finalizado = 0
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad and v.NroRevision = e.NroRevision
	where e.IdEntidad = @IdEntidad
		and IdEstado <> 2  -- SI NO ESTA VALIDADO
		and v.ConfirmaYAprueba <> 1 -- Y SI NO HAN PUESTO UN SI
end


GO


