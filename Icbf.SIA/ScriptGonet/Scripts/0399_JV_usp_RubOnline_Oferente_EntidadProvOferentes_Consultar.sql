USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar]    Script Date: 10/30/2014 11:13:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar]    Script Date: 10/30/2014 11:13:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 30/OCT/2014
-- Description:	Crea el Procedimiento usp_RubOnline_Oferente_EntidadProvOferentes_Consultar
-- el cual recibe un IdTercero y consulta cuantos EntidadProvOferente hay asociados a ese tercero.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar] 
@IdTercero INT
AS
BEGIN
	
	IF EXISTS(SELECT IdEntidad FROM PROVEEDOR.EntidadProvOferente
	WHERE IdTercero=@IdTercero)
	BEGIN
		SELECT IdEntidad as 'IDENTIDAD'
		FROM PROVEEDOR.EntidadProvOferente
		WHERE IdTercero=@IdTercero
	END
	ELSE
	BEGIN
		SELECT 0 as 'IDENTIDAD'
	END
END

GO


