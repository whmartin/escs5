USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  03/10/2014 12:58:36 PM
-- Description:	Procedimiento almacenado que Consulta Experiencias
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]
	@IdEntidad INT = NULL,
	@EntidadContratante NVARCHAR(128) = NULL,
	@NumeroContrato NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdExpEntidad, 
		IdEntidad, 
		IdTipoSector, 
		TS.Descripcion AS TipoSector, 
		IdTipoEstadoExp, 
		IdTipoModalidadExp, 
		IdTipoModalidad, 
		IdTipoPoblacionAtendida, 
		IdTipoRangoExpAcum, 
		IdTipoCodUNSPSC, 
		IdTipoEntidadContratante, 
		EntidadContratante, 
		FechaInicio, 
		FechaFin, 
		ExperienciaMeses, 
		NumeroContrato, 
		ObjetoContrato, 
		Vigente, 
		Cuantia, 
		EstadoDocumental, 
		CASE EstadoDocumental.Descripcion  WHEN 'VALIDADO' THEN 'SI' ELSE '' END AS 'Aprobado', 
		EstadoDocumental.Descripcion AS EstadoDocDescripcion, 
		UnionTempConsorcio, 
		PorcentParticipacion, 
		AtencionDeptos, 
		JardinOPreJardin, 
 IE.UsuarioCrea, IE.FechaCrea, IE.UsuarioModifica, IE.FechaModifica,
		ContratoEjecucion 
 FROM [Proveedor].[InfoExperienciaEntidad] AS IE
 INNER JOIN [Proveedor].[TipoSectorEntidad] AS TS ON IE.idTipoSector = TS.IdTipoSectorEntidad
 JOIN PROVEEDOR.EstadoValidacionDocumental EstadoDocumental
 ON	IE.EstadoDocumental = EstadoDocumental.IdEstadoValidacionDocumental
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND EntidadContratante = CASE WHEN @EntidadContratante IS NULL THEN EntidadContratante ELSE @EntidadContratante END 
 AND ISNULL(NumeroContrato,0) = CASE WHEN @NumeroContrato IS NULL THEN ISNULL(NumeroContrato,0) ELSE @NumeroContrato END
END

