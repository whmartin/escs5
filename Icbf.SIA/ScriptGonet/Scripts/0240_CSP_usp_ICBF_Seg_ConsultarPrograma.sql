USE [RubOnline]
GO


If (select Count(*) from sysobjects where type='P' and name='usp_ICBF_Seg_ConsultarPrograma')>0 
BEGIN
	DROP PROCEDURE dbo.usp_ICBF_Seg_ConsultarPrograma;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	Se agregan los campos EsReporte y IdReporte
-- =============================================

-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 03-09-2012
-- Description:	Procedimiento almacenado que consulta un programa
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarPrograma]
	@IdPrograma INT
AS
BEGIN

SELECT 
	 IdPrograma
	,IdModulo
	,NombrePrograma
	,CodigoPrograma
	,Posicion
	,Estado
	,UsuarioCreacion
	,FechaCreacion
	,UsuarioModificacion
	,FechaModificacion
	,VisibleMenu
	,generaLog
	,EsReporte 
	,IdReporte 
FROM 
	SEG.Programa
WHERE 
	IdPrograma = @IdPrograma


END