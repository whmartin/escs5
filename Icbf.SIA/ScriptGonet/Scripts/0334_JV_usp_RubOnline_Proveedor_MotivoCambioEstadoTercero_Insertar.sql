USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstadoTercero_Insertar]    Script Date: 08/12/2014 07:27:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_MotivoCambioEstadoTercero_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstadoTercero_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstadoTercero_Insertar]    Script Date: 08/12/2014 07:27:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Autor:	 Juan.Valverde, 
-- Fecha:	 12-AGO-2014
-- Descripción: Inserta el Motivo de cambio de estado y cambia el estado del tercero Unicamente.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstadoTercero_Insertar]
@IdTercero INT, @IdTemporal NVARCHAR (20), @Motivo NVARCHAR (128), @UsuarioCrea NVARCHAR (250)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION t1
		INSERT INTO Proveedor.MotivoCambioEstado (IdTercero, IdTemporal, Motivo, DatosBasicos, Financiera, Experiencia, FechaCrea, UsuarioCrea,Integrantes)
			VALUES (@IdTercero, @IdTemporal, @Motivo, 0, 0, 0, GETDATE(), @UsuarioCrea,0)


		DECLARE @PorValidar NVARCHAR (10)
		
		
		SET @PorValidar = (SELECT
			IdEstadoTercero
		FROM Oferente.EstadoTercero
		WHERE CodigoEstadotercero = '005')
		UPDATE Oferente.TERCERO
		SET IDESTADOTERCERO = @PorValidar
		WHERE IDTERCERO = @IdTercero	
COMMIT TRANSACTION t1
END TRY
BEGIN CATCH
ROLLBACK TRANSACTION t1
SELECT
	ERROR_NUMBER() AS ErrorNumber,
	ERROR_MESSAGE() AS ErrorMessage;
END CATCH;
END





GO


