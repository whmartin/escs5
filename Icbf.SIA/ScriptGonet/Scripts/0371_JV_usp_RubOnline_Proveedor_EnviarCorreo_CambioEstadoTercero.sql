USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]    Script Date: 08/15/2014 12:33:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]    Script Date: 08/15/2014 12:33:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Fabi�n Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero y los proveedores que se hayan cambiado el estado y envian un correo a los gestores
-- Modificaci�n: Juan Carlos Valverde S�mano
-- Fecha:	2014-03-03
-- Description; Se agrega en el cuerpo del correo el parametro "Cambio de Estado" que se encuentra en la tabla SEG.Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]

@IdTemporal nvarchar (20),
@Tercero nvarchar (30),
@correos nvarchar(max)
AS
BEGIN
--DECLARE @Tercero varchar(30), 
DECLARE @Asunto varchar (100)
--SET @Tercero = 'Tercero'
SET @Asunto = 'Validar ' + @Tercero
Declare @Body varchar (max),

@TableHead varchar (max),
@TableTail varchar (max),
@paramCambioTercero varchar(max)
SET NOCOUNT ON;

SET @paramCambioTercero=(SELECT ValorParametro FROM SEG.Parametro WHERE NombreParametro='Cambio de estado')

SET @TableTail = '</table></body></html>';
SET @TableHead = '<html><head>' +
				 '<style>' +
				 'td {border: solid black 1px;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;font-size:11pt;} ' +
				 '</style>' +
				 '</head>' +
				 '<body>' + @paramCambioTercero + '<br/><br/><table cellpadding=0 cellspacing=0 border=0>' +
				 '<tr bgcolor=#ccbfac><td align=center><b>Tipo Identificaci&oacute;n</b></td>' +
				 '<td align=center><b>N&uacute;mero Identificaci&oacute;n</b></td>' +
				 '<td align=center><b>' + @Tercero + '</b></td>';

SELECT
	@Body = (SELECT DISTINCT
		ROW_NUMBER() OVER (ORDER BY Global.TiposDocumentos.CodDocumento) % 2 AS [TRRow],
		Global.TiposDocumentos.CodDocumento AS [TD],
		Oferente.TERCERO.NUMEROIDENTIFICACION AS [TD],
		(ISNULL(Oferente.TERCERO.PRIMERNOMBRE,'') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDONOMBRE,'') + ' ' + ISNULL(Oferente.TERCERO.PRIMERAPELLIDO,'') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDOAPELLIDO,'') + ' ' + ISNULL(Oferente.TERCERO.RAZONSOCIAL,'')) AS [TD]
	FROM Oferente.TERCERO
	INNER JOIN Global.TiposDocumentos
		ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
	INNER JOIN Oferente.EstadoTercero
		ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
	INNER JOIN Oferente.TipoPersona
		ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
	INNER JOIN Proveedor.MotivoCambioEstado
		ON Oferente.TERCERO.IDTERCERO = Proveedor.MotivoCambioEstado.IdTercero
	WHERE Proveedor.MotivoCambioEstado.IdTemporal = @IdTemporal
	ORDER BY Global.TiposDocumentos.CodDocumento
	FOR xml RAW ('tr'), ELEMENTS)

-- Replace the entity codes and row numbers
SET @Body = REPLACE(@Body, '_x0020_', SPACE(1))
SET @Body = REPLACE(@Body, '_x003D_', '=')
SET @Body = REPLACE(@Body, '<tr><TRRow>1</TRRow>', '<tr bgcolor=#e5d7c2>')
SET @Body = REPLACE(@Body, '<TRRow>0</TRRow>', '')

SET	@Body = @TableHead + @Body + @TableTail

--PRINT @Body

-- Enviar correo
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'GonetMail', --colocar perfil que se tenga configurado
								@recipients = @correos, --destinatario
								@subject = @Asunto, --asunto
								@body = @Body, --cuerpo de correo
								@body_format = 'HTML'; --formato de correo


END




GO


