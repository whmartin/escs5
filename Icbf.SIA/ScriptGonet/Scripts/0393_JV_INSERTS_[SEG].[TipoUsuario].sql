USE [SIA]
GO

/**
Autor: Juan Carlos Valverde S�mano
Fecha: 11/SEPT/2014
Descripci�n: Inserta la informaci�n en la Entidad [SEG].[TipoUsuario]
**/

IF NOT EXISTS (SELECT IdTipoUsuario FROM [SEG].[TipoUsuario]
WHERE CodigoTipoUsuario='01' AND NombreTipoUsuario='Sede Nacional' AND
Estado='A')
BEGIN
	INSERT INTO [SEG].[TipoUsuario] (CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea)
	VALUES ('01','Sede Nacional','A','Administrador', GETDATE())
END
IF NOT EXISTS (SELECT IdTipoUsuario FROM [SEG].[TipoUsuario]
WHERE CodigoTipoUsuario='02' AND NombreTipoUsuario='Regional' AND
Estado='A')
BEGIN
	INSERT INTO [SEG].[TipoUsuario] (CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea)
	VALUES ('02','Regional','A','Administrador', GETDATE())
END
IF NOT EXISTS (SELECT IdTipoUsuario FROM [SEG].[TipoUsuario]
WHERE CodigoTipoUsuario='03' AND NombreTipoUsuario='Entidad Contratista' AND
Estado='A')
BEGIN
	INSERT INTO [SEG].[TipoUsuario] (CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea)
	VALUES ('03','Entidad Contratista','A','Administrador', GETDATE())
END