USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]    Script Date: 08/12/2014 11:51:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]    Script Date: 08/12/2014 11:51:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]
		@IdNivelOrganizacional INT OUTPUT, 	@CodigoNivelOrganizacional NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.NivelOrganizacional(CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNivelOrganizacional, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNivelOrganizacional = SCOPE_IDENTITY() 		
END


GO


