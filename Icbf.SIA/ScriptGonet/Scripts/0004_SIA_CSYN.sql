USE [SIA]
GO
/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripción: Creación de sinónimos de objetos de la BD Oferente para uso de SIA
*/

/****** Object:  Synonym [dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]    Script Date: 15/10/2013 17:27:48 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion' AND schema_id = SCHEMA_ID(N'dbo'))
DROP SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]
GO

/****** Object:  Synonym [dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]    Script Date: 15/10/2013 17:27:48 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion' AND schema_id = SCHEMA_ID(N'dbo'))
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion] FOR [Oferentes].[DBO].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]
GO

IF OBJECT_ID('[Oferente].[Tercero]') IS NOT NULL
BEGIN
	DROP SYNONYM [Oferente].[Tercero] 
END
GO
CREATE SYNONYM [Oferente].[Tercero] 
FOR [Oferentes].[Oferente].[Tercero]
GO
IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_Tercero_Modificar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_Modificar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_Modificar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_Tercero_Modificar]
GO
IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar]
GO

IF EXISTS (SELECT * FROM SIA.sys.synonyms WHERE name = 'usp_RubOnline_Oferente_Tercero_Consultar') begin
DROP SYNONYM [dbo].usp_RubOnline_Oferente_Tercero_Consultar
END


CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_Consultar] FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_Tercero_Consultar]
GO


IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_Tercero_Insertar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_Insertar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_Tercero_Insertar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_Tercero_Insertar]
GO

IF OBJECT_ID('[Oferente].[TipoPersona]') IS NOT NULL
BEGIN
	DROP SYNONYM [Oferente].[TipoPersona] 
END
GO
CREATE SYNONYM [Oferente].[TipoPersona] 
FOR [Oferentes].[Oferente].[TipoPersona]
GO
IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TipoPersona_Consultar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersona_Consultar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersona_Consultar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TipoPersona_Consultar]
GO
IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TipoPersona_Eliminar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersona_Eliminar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersona_Eliminar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TipoPersona_Eliminar]
GO
IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TipoPersona_Insertar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersona_Insertar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersona_Insertar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TipoPersona_Insertar]
GO
IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TipoPersona_Modificar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersona_Modificar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersona_Modificar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TipoPersona_Modificar]
GO
IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TipoPersonas_Consultar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersonas_Consultar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TipoPersonas_Consultar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TipoPersonas_Consultar]
GO

--

IF OBJECT_ID('[Oferente].[EstadoTercero]') IS NOT NULL
BEGIN
	DROP SYNONYM [Oferente].[EstadoTercero] 
END
GO
CREATE SYNONYM [Oferente].[EstadoTercero] 
FOR [Oferentes].[Oferente].[EstadoTercero]
GO

IF OBJECT_ID('[Oferente].[SalarioMinimo]') IS NOT NULL
BEGIN
	DROP SYNONYM [Oferente].[SalarioMinimo] 
END
GO
CREATE SYNONYM [Oferente].[SalarioMinimo] 
FOR [Oferentes].[Oferente].[SalarioMinimo]
GO


IF OBJECT_ID('[Oferente].[TelTerceros]') IS NOT NULL
BEGIN
	DROP SYNONYM [Oferente].[TelTerceros] 
END
GO
CREATE SYNONYM [Oferente].[TelTerceros] 
FOR [Oferentes].[Oferente].[TelTerceros]
GO

IF OBJECT_ID('[Oferente].[TERCERO]') IS NOT NULL
BEGIN
	DROP SYNONYM [Oferente].[TERCERO] 
END
GO
CREATE SYNONYM [Oferente].[TERCERO] 
FOR [Oferentes].[Oferente].[TERCERO]
GO

IF OBJECT_ID('[Oferente].[TipoPersona]') IS NOT NULL
BEGIN
	DROP SYNONYM [Oferente].[TipoPersona] 
END
GO
CREATE SYNONYM [Oferente].[TipoPersona] 
FOR [Oferentes].[Oferente].[TipoPersona]
GO

IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarDepto]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarDepto] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarDepto] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarDepto]
GO

IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarMunicipio]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarMunicipio] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarMunicipio] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarMunicipio]
GO

IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarSalarioMinimo]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarSalarioMinimo] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarSalarioMinimo] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarSalarioMinimo]
GO

IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar]
GO

IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero]
GO

IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar]
GO


IF OBJECT_ID('[dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar]') IS NOT NULL
BEGIN
	DROP SYNONYM [dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar] 
END
GO
CREATE SYNONYM [dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar] 
FOR [Oferentes].[dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar]
GO
















