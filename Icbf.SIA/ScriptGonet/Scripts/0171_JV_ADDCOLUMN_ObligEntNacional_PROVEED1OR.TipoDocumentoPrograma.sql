USE [SIA]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 23-Abril-2013
Descripci�n: Se agrega el campo ObligEntNacional a la tabla PROVEEDOR.TipoDocumentoPrograma
**/

IF NOT EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'TipoDocumentoPrograma'
AND [COLUMN_NAME] = 'ObligEntNacional'
AND [TABLE_SCHEMA] = 'PROVEEDOR')
BEGIN
	
	ALTER TABLE [PROVEEDOR].[TipoDocumentoPrograma]
	ADD [ObligEntNacional] BIT NOT NULL DEFAULT (0)
END