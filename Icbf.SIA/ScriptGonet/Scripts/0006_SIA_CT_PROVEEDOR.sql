USE [sia]
GO


/*
Usuario Creaci�n: Carlos Cubillos
Fecha Creaci�n: 29/10/2013
Descripcion: Creaci�n de las tablas y constraint propios de Proveedores
*/

/****** Object:  Table [PROVEEDOR].[Acuerdos]    Script Date: 29/10/2013 17:53:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[Acuerdos](
	[IdAcuerdo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](64) NOT NULL,
	[ContenidoHtml] [nvarchar](max) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Acuerdos] PRIMARY KEY CLUSTERED 
(
	[IdAcuerdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[ClasedeEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PROVEEDOR].[ClasedeEntidad](
	[IdClasedeEntidad] [int] IDENTITY(1,1) NOT NULL,
	[IdTipodeEntidad] [int] NOT NULL,
	[IdTipodeActividad] [int] NOT NULL,
	[CodigoClasedeEntidad] [varchar](10) NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ClasedeEntidad] PRIMARY KEY CLUSTERED 
(
	[IdClasedeEntidad] ASC,
	[IdTipodeEntidad] ASC,
	[IdTipodeActividad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PROVEEDOR].[ContactoEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[ContactoEntidad](
	[IdContacto] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdSucursal] [int] NOT NULL,
	[IdTelContacto] [int] NOT NULL,
	[IdTipoDocIdentifica] [int] NOT NULL,
	[IdTipoCargoEntidad] [int] NOT NULL,
	[NumeroIdentificacion] [numeric](30, 0) NOT NULL,
	[PrimerNombre] [nvarchar](128) NOT NULL,
	[SegundoNombre] [nvarchar](128) NOT NULL,
	[PrimerApellido] [nvarchar](128) NOT NULL,
	[SegundoApellido] [nvarchar](128) NOT NULL,
	[Dependencia] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ContactoEntidad] PRIMARY KEY CLUSTERED 
(
	[IdContacto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[DocAdjuntoTercero]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PROVEEDOR].[DocAdjuntoTercero](
	[IDDOCADJUNTO] [int] IDENTITY(1,1) NOT NULL,
	[IDTERCERO] [int] NULL,
	[IDDOCUMENTO] [int] NULL,
	[DESCRIPCION] [nvarchar](128) NOT NULL,
	[LINKDOCUMENTO] [nvarchar](256) NOT NULL,
	[ANNO] [numeric](4, 0) NULL,
	[FECHACREA] [datetime] NOT NULL,
	[USUARIOCREA] [nvarchar](128) NOT NULL,
	[FECHAMODIFICA] [datetime] NULL,
	[USUARIOMODIFICA] [nvarchar](128) NULL,
	[IdTemporal] [varchar](20) NULL,
 CONSTRAINT [PK_DOCADJUNTOTERCERO] PRIMARY KEY CLUSTERED 
(
	[IDDOCADJUNTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PROVEEDOR].[DocDatosBasicoProv]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PROVEEDOR].[DocDatosBasicoProv](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NULL,
	[NombreDocumento] [nvarchar](128) NOT NULL,
	[LinkDocumento] [nvarchar](256) NOT NULL,
	[Observaciones] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdTipoDocumento] [int] NULL,
	[IdTemporal] [varchar](20) NULL,
 CONSTRAINT [PK_DocDatosBasicoProv] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PROVEEDOR].[DocExperienciaEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PROVEEDOR].[DocExperienciaEntidad](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdExpEntidad] [int] NULL,
	[IdTipoDocumento] [int] NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[LinkDocumento] [nvarchar](256) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[NombreDocumento] [nvarchar](128) NULL,
	[IdTemporal] [varchar](20) NULL,
 CONSTRAINT [PK_DocExperienciaEntidad] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PROVEEDOR].[DocFinancieraProv]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PROVEEDOR].[DocFinancieraProv](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdInfoFin] [int] NULL,
	[NombreDocumento] [nvarchar](128) NOT NULL,
	[LinkDocumento] [nvarchar](256) NOT NULL,
	[Observaciones] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdTipoDocumento] [int] NULL,
	[IdTemporal] [varchar](20) NULL,
 CONSTRAINT [PK_DocFinancieraProv] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PROVEEDOR].[DocNotificacionJudicial]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[DocNotificacionJudicial](
	[IdDocNotJudicial] [int] IDENTITY(1,1) NOT NULL,
	[IdNotJudicial] [int] NULL,
	[IdDocumento] [int] NULL,
	[Descripcion] [nvarchar](128) NULL,
	[LinkDocumento] [nvarchar](256) NULL,
	[Anno] [numeric](4, 0) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_DocNotificacionJudicial] PRIMARY KEY CLUSTERED 
(
	[IdDocNotJudicial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[EntidadProvOferente]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[EntidadProvOferente](
	[IdEntidad] [int] IDENTITY(1,1) NOT NULL,
	[ConsecutivoInterno] [nvarchar](128) NULL,
	[TipoEntOfProv] [bit] NULL,
	[IdTercero] [int] NOT NULL,
	[IdTipoCiiuPrincipal] [int] NULL,
	[IdTipoCiiuSecundario] [int] NULL,
	[IdTipoSector] [int] NULL,
	[IdTipoClaseEntidad] [int] NULL,
	[IdTipoRamaPublica] [int] NULL,
	[IdTipoNivelGob] [int] NULL,
	[IdTipoNivelOrganizacional] [int] NULL,
	[IdTipoCertificadorCalidad] [int] NULL,
	[FechaCiiuPrincipal] [datetime] NULL,
	[FechaCiiuSecundario] [datetime] NULL,
	[FechaConstitucion] [datetime] NULL,
	[FechaVigencia] [datetime] NULL,
	[FechaMatriculaMerc] [datetime] NULL,
	[FechaExpiracion] [datetime] NULL,
	[TipoVigencia] [bit] NULL,
	[ExenMatriculaMer] [bit] NULL,
	[MatriculaMercantil] [nvarchar](20) NULL,
	[ObserValidador] [nvarchar](256) NULL,
	[AnoRegistro] [int] NULL,
	[IdEstado] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdAcuerdo] [int] NULL,
	[NroRevision] [int] NULL,
	[Finalizado] [bit] NULL,
 CONSTRAINT [PK_EntidadProvOferente] PRIMARY KEY CLUSTERED 
(
	[IdEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[EstadoDatosBasicos]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[EstadoDatosBasicos](
	[IdEstadoDatosBasicos] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoDatosBasicos] PRIMARY KEY CLUSTERED 
(
	[IdEstadoDatosBasicos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[EstadoValidacionDocumental]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[EstadoValidacionDocumental](
	[IdEstadoValidacionDocumental] [int] IDENTITY(1,1) NOT NULL,
	[CodigoEstadoValidacionDocumental] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoValidacionDocumental] PRIMARY KEY CLUSTERED 
(
	[IdEstadoValidacionDocumental] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[ExperienciaCodUNSPSCEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[ExperienciaCodUNSPSCEntidad](
	[IdExpCOD] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoCodUNSPSC] [int] NOT NULL,
	[IdExpEntidad] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ExperienciaCodUNSPSCEntidad] PRIMARY KEY CLUSTERED 
(
	[IdExpCOD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[InfoAdminEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[InfoAdminEntidad](
	[IdInfoAdmin] [int] IDENTITY(1,1) NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdTipoRegTrib] [int] NULL,
	[IdTipoOrigenCapital] [int] NULL,
	[IdTipoActividad] [int] NULL,
	[IdTipoEntidad] [int] NULL,
	[IdTipoNaturalezaJurid] [int] NULL,
	[IdTipoRangosTrabajadores] [int] NULL,
	[IdTipoRangosActivos] [int] NULL,
	[IdRepLegal] [int] NULL,
	[IdTipoCertificaTamano] [int] NULL,
	[IdTipoEntidadPublica] [int] NULL,
	[IdDepartamentoConstituida] [int] NULL,
	[IdMunicipioConstituida] [int] NULL,
	[IdDepartamentoDirComercial] [int] NULL,
	[IdMunicipioDirComercial] [int] NULL,
	[DireccionComercial] [nvarchar](50) NULL,
	[IdZona] [nvarchar](128) NULL,
	[NombreComercial] [nvarchar](128) NULL,
	[NombreEstablecimiento] [nvarchar](128) NULL,
	[Sigla] [nvarchar](50) NULL,
	[PorctjPrivado] [int] NULL,
	[PorctjPublico] [int] NULL,
	[SitioWeb] [nvarchar](128) NULL,
	[NombreEntidadAcreditadora] [nvarchar](1) NULL,
	[Organigrama] [bit] NULL,
	[TotalPnalAnnoPrevio] [numeric](10, 0) NULL,
	[VincLaboral] [numeric](10, 0) NULL,
	[PrestServicios] [numeric](10, 0) NULL,
	[Voluntariado] [numeric](10, 0) NULL,
	[VoluntPermanente] [numeric](10, 0) NULL,
	[Asociados] [numeric](10, 0) NULL,
	[Mision] [numeric](10, 0) NULL,
	[PQRS] [bit] NULL,
	[GestionDocumental] [bit] NULL,
	[AuditoriaInterna] [bit] NULL,
	[ManProcedimiento] [bit] NULL,
	[ManPracticasAmbiente] [bit] NULL,
	[ManComportOrg] [bit] NULL,
	[ManFunciones] [bit] NULL,
	[ProcRegInfoContable] [bit] NULL,
	[PartMesasTerritoriales] [bit] NULL,
	[PartAsocAgremia] [bit] NULL,
	[PartConsejosComun] [bit] NULL,
	[ConvInterInst] [bit] NULL,
	[ProcSeleccGral] [bit] NULL,
	[ProcSeleccEtnico] [bit] NULL,
	[PlanInduccCapac] [bit] NULL,
	[EvalDesemp] [bit] NULL,
	[PlanCualificacion] [bit] NULL,
	[NumSedes] [int] NULL,
	[SedesPropias] [bit] NULL,
	[IdEstado] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InfoAdminEntidad] PRIMARY KEY CLUSTERED 
(
	[IdInfoAdmin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[InfoExperienciaEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[InfoExperienciaEntidad](
	[IdExpEntidad] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NULL,
	[IdTipoSector] [int] NULL,
	[IdTipoEstadoExp] [int] NULL,
	[IdTipoModalidadExp] [int] NULL,
	[IdTipoModalidad] [int] NULL,
	[IdTipoPoblacionAtendida] [int] NULL,
	[IdTipoRangoExpAcum] [int] NULL,
	[IdTipoCodUNSPSC] [int] NOT NULL,
	[IdTipoEntidadContratante] [int] NULL,
	[EntidadContratante] [nvarchar](256) NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NOT NULL,
	[NumeroContrato] [nvarchar](128) NULL,
	[ObjetoContrato] [nvarchar](256) NOT NULL,
	[Vigente] [bit] NULL,
	[Cuantia] [numeric](21, 3) NOT NULL,
	[ExperienciaMeses] [numeric](18, 3) NULL,
	[EstadoDocumental] [int] NULL,
	[UnionTempConsorcio] [bit] NULL,
	[PorcentParticipacion] [numeric](5, 0) NULL,
	[AtencionDeptos] [bit] NULL,
	[JardinOPreJardin] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[NroRevision] [int] NULL,
	[Finalizado] [bit] NULL,
 CONSTRAINT [PK_InfoExperienciaEntidad] PRIMARY KEY CLUSTERED 
(
	[IdExpEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[InfoFinancieraEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[InfoFinancieraEntidad](
	[IdInfoFin] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[ActivoCte] [numeric](21, 3) NOT NULL,
	[ActivoTotal] [numeric](21, 3) NOT NULL,
	[PasivoCte] [numeric](21, 3) NOT NULL,
	[PasivoTotal] [numeric](21, 3) NOT NULL,
	[Patrimonio] [numeric](21, 3) NOT NULL,
	[GastosInteresFinancieros] [numeric](21, 3) NOT NULL,
	[UtilidadOperacional] [numeric](21, 3) NOT NULL,
	[ConfirmaIndicadoresFinancieros] [bit] NOT NULL,
	[RupRenovado] [bit] NOT NULL,
	[EstadoValidacion] [int] NOT NULL,
	[ObservacionesInformacionFinanciera] [nvarchar](256) NULL,
	[ObservacionesValidadorICBF] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[NroRevision] [int] NULL,
	[Finalizado] [bit] NULL,
 CONSTRAINT [PK_InfoFinancieraEntidad] PRIMARY KEY CLUSTERED 
(
	[IdInfoFin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[MotivoCambioEstado]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[MotivoCambioEstado](
	[IdTercero] [int] NULL,
	[IdTemporal] [nvarchar](20) NULL,
	[Motivo] [nvarchar](250) NULL,
	[DatosBasicos] [bit] NULL,
	[Financiera] [bit] NULL,
	[Experiencia] [bit] NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioCrea] [nvarchar](128) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[Niveldegobierno]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[Niveldegobierno](
	[IdNiveldegobierno] [int] IDENTITY(1,1) NOT NULL,
	[IdRamaEstructura] [int] NOT NULL,
	[CodigoNiveldegobierno] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Niveldegobierno] PRIMARY KEY CLUSTERED 
(
	[IdNiveldegobierno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[NivelOrganizacional]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[NivelOrganizacional](
	[IdNivelOrganizacional] [int] IDENTITY(1,1) NOT NULL,
	[CodigoNivelOrganizacional] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_NivelOrganizacional] PRIMARY KEY CLUSTERED 
(
	[IdNivelOrganizacional] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[NotificacionJudicial]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[NotificacionJudicial](
	[IdNotJudicial] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NULL,
	[IdDepartamento] [int] NULL,
	[IdMunicipio] [int] NULL,
	[IdZona] [int] NULL,
	[Direccion] [nvarchar](250) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_NotificacionJudicial] PRIMARY KEY CLUSTERED 
(
	[IdNotJudicial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[ParametrosEmail]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[ParametrosEmail](
	[IdParametroID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoParametroEmail] [nvarchar](100) NOT NULL,
	[DescripcionParametrosEmail] [nvarchar](255) NULL,
	[ValorMesesParametroEmail] [int] NULL,
 CONSTRAINT [UNIC_ParametrosEmail] UNIQUE NONCLUSTERED 
(
	[CodigoParametroEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[RamaoEstructura]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[RamaoEstructura](
	[IdRamaEstructura] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRamaEstructura] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RamaoEstructura] PRIMARY KEY CLUSTERED 
(
	[IdRamaEstructura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[RamaoEstructura_Niveldegobierno]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[RamaoEstructura_Niveldegobierno](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdRamaEstructura] [int] NOT NULL,
	[IdNiveldegobierno] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
 CONSTRAINT [PK_RamaoEstructura_Niveldegobierno] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[IdNiveldegobierno] ASC,
	[IdRamaEstructura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[Sucursal]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[Sucursal](
	[IdSucursal] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[Indicativo] [int] NOT NULL,
	[Telefono] [int] NOT NULL,
	[Extension] [numeric](10, 0) NULL,
	[Celular] [numeric](10, 0) NOT NULL,
	[Correo] [nvarchar](256) NULL,
	[Estado] [int] NOT NULL,
	[Departamento] [int] NOT NULL,
	[Municipio] [int] NOT NULL,
	[IdZona] [int] NULL,
	[Direccion] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[Nombre] [nvarchar](256) NULL,
 CONSTRAINT [PK_IdSucursal] PRIMARY KEY CLUSTERED 
(
	[IdSucursal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TablaParametrica]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TablaParametrica](
	[IdTablaParametrica] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTablaParametrica] [nvarchar](128) NOT NULL,
	[NombreTablaParametrica] [nvarchar](128) NOT NULL,
	[Url] [nvarchar](256) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TablaParametrica] PRIMARY KEY CLUSTERED 
(
	[IdTablaParametrica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipoCargoEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipoCargoEntidad](
	[IdTipoCargoEntidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoCargoEntidad] [nvarchar](10) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoCargoEntidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoCargoEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipoCiiu]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipoCiiu](
	[IdTipoCiiu] [int] IDENTITY(1,1) NOT NULL,
	[CodigoCiiu] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoCiiu] PRIMARY KEY CLUSTERED 
(
	[IdTipoCiiu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipoCodigoUNSPSC]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipoCodigoUNSPSC](
	[IdTipoCodUNSPSC] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](64) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoCodigoUNSPSC] PRIMARY KEY CLUSTERED 
(
	[IdTipoCodUNSPSC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipodeActividad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipodeActividad](
	[IdTipodeActividad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipodeActividad] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipodeActividad] PRIMARY KEY CLUSTERED 
(
	[IdTipodeActividad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipodeentidadPublica]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipodeentidadPublica](
	[IdTipodeentidadPublica] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipodeentidadPublica] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipodeentidadPublica] PRIMARY KEY CLUSTERED 
(
	[IdTipodeentidadPublica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipoDocIdentifica]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipoDocIdentifica](
	[IdTipoDocIdentifica] [int] IDENTITY(1,1) NOT NULL,
	[CodigoDocIdentifica] [numeric](18, 0) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoDocIdentifica] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocIdentifica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipoDocumento]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipoDocumento](
	[IdTipoDocumento] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoDocumento] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoDocumento] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipoDocumentoPrograma]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PROVEEDOR].[TipoDocumentoPrograma](
	[IdTipoDocumentoPrograma] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoDocumento] [int] NOT NULL,
	[IdPrograma] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[MaxPermitidoKB] [int] NULL,
	[ExtensionesPermitidas] [varchar](50) NULL,
	[ObligRupNoRenovado] [int] NULL,
	[ObligRupRenovado] [int] NULL,
	[ObligPersonaJuridica] [int] NULL,
	[ObligPersonaNatural] [int] NULL,
	[ObligSectorPrivado] [int] NULL,
	[ObligSectorPublico] [int] NULL,
 CONSTRAINT [PK_TipoDocumentoPrograma] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumentoPrograma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PROVEEDOR].[TipoEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipoEntidad](
	[IdTipoentidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoentidad] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Tipoentidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoentidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipoRegimenTributario]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipoRegimenTributario](
	[IdTipoRegimenTributario] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRegimenTributario] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[IdTipoPersona] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoRegimenTributario] PRIMARY KEY CLUSTERED 
(
	[IdTipoRegimenTributario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[TipoSectorEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[TipoSectorEntidad](
	[IdTipoSectorEntidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoSectorEntidad] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoSectorEntidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoSectorEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[ValidarInfoDatosBasicosEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[ValidarInfoDatosBasicosEntidad](
	[IdValidarInfoDatosBasicosEntidad] [int] IDENTITY(1,1) NOT NULL,
	[NroRevision] [int] NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,
	[Observaciones] [nvarchar](100) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaModifica] [datetime] NULL,
	[UsuarioModifica] [nvarchar](128) NULL,
	[CorreoEnviado] [bit] NOT NULL,
 CONSTRAINT [PK_IdValidarInfoDatosBasicosEntidad] PRIMARY KEY CLUSTERED 
(
	[IdValidarInfoDatosBasicosEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[ValidarInfoExperienciaEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[ValidarInfoExperienciaEntidad](
	[IdValidarInfoExperienciaEntidad] [int] IDENTITY(1,1) NOT NULL,
	[NroRevision] [int] NOT NULL,
	[IdExpEntidad] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,
	[Observaciones] [nvarchar](100) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaModifica] [datetime] NULL,
	[UsuarioModifica] [nvarchar](128) NULL,
	[CorreoEnviado] [bit] NOT NULL,
 CONSTRAINT [PK_IdValidarInfoExperienciaEntidad] PRIMARY KEY CLUSTERED 
(
	[IdValidarInfoExperienciaEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[ValidarInfoFinancieraEntidad]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[ValidarInfoFinancieraEntidad](
	[IdValidarInfoFinancieraEntidad] [int] IDENTITY(1,1) NOT NULL,
	[NroRevision] [int] NOT NULL,
	[IdInfoFin] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,
	[Observaciones] [nvarchar](100) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaModifica] [datetime] NULL,
	[UsuarioModifica] [nvarchar](128) NULL,
	[CorreoEnviado] [bit] NOT NULL,
 CONSTRAINT [PK_IdValidarInfoFinancieraEntidad] PRIMARY KEY CLUSTERED 
(
	[IdValidarInfoFinancieraEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PROVEEDOR].[ValidarTercero]    Script Date: 29/10/2013 17:53:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PROVEEDOR].[ValidarTercero](
	[IdValidarTercero] [int] IDENTITY(1,1) NOT NULL,
	[IdTercero] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NOT NULL,
	[Observaciones] [nvarchar](200) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_IdValidarTercero] PRIMARY KEY CLUSTERED 
(
	[IdValidarTercero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] ADD  DEFAULT ((0)) FOR [CorreoEnviado]
GO
ALTER TABLE [PROVEEDOR].[ValidarInfoExperienciaEntidad] ADD  DEFAULT ((0)) FOR [CorreoEnviado]
GO
ALTER TABLE [PROVEEDOR].[ValidarInfoFinancieraEntidad] ADD  DEFAULT ((0)) FOR [CorreoEnviado]
GO
ALTER TABLE [PROVEEDOR].[ClasedeEntidad]  WITH CHECK ADD  CONSTRAINT [FK_ClasedeEntidad_TipodeActividad] FOREIGN KEY([IdTipodeActividad])
REFERENCES [PROVEEDOR].[TipodeActividad] ([IdTipodeActividad])
GO
ALTER TABLE [PROVEEDOR].[ClasedeEntidad] CHECK CONSTRAINT [FK_ClasedeEntidad_TipodeActividad]
GO
ALTER TABLE [PROVEEDOR].[ClasedeEntidad]  WITH CHECK ADD  CONSTRAINT [FK_ClasedeEntidad_Tipoentidad] FOREIGN KEY([IdTipodeEntidad])
REFERENCES [PROVEEDOR].[TipoEntidad] ([IdTipoentidad])
GO
ALTER TABLE [PROVEEDOR].[ClasedeEntidad] CHECK CONSTRAINT [FK_ClasedeEntidad_Tipoentidad]
GO
ALTER TABLE [PROVEEDOR].[DocAdjuntoTercero]  WITH CHECK ADD  CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO] FOREIGN KEY([IDDOCUMENTO])
REFERENCES [PROVEEDOR].[TipoDocumento] ([IdTipoDocumento])
GO
ALTER TABLE [PROVEEDOR].[DocAdjuntoTercero] CHECK CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO]
GO
--ALTER TABLE [PROVEEDOR].[DocAdjuntoTercero]  WITH CHECK ADD  CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1] FOREIGN KEY([IDTERCERO])
--REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
--GO
--ALTER TABLE [PROVEEDOR].[DocAdjuntoTercero] CHECK CONSTRAINT [FK_DOCADJUNTOTERCERO_TERCERO1]
--GO
ALTER TABLE [PROVEEDOR].[DocDatosBasicoProv]  WITH CHECK ADD  CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO
ALTER TABLE [PROVEEDOR].[DocDatosBasicoProv] CHECK CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente]
GO
ALTER TABLE [PROVEEDOR].[DocDatosBasicoProv]  WITH CHECK ADD  CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [PROVEEDOR].[TipoDocumento] ([IdTipoDocumento])
GO
ALTER TABLE [PROVEEDOR].[DocDatosBasicoProv] CHECK CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento]
GO
ALTER TABLE [PROVEEDOR].[DocExperienciaEntidad]  WITH CHECK ADD  CONSTRAINT [FK_DocExperienciaEntidad_InfoExperienciaEntidad] FOREIGN KEY([IdExpEntidad])
REFERENCES [PROVEEDOR].[InfoExperienciaEntidad] ([IdExpEntidad])
GO
ALTER TABLE [PROVEEDOR].[DocExperienciaEntidad] CHECK CONSTRAINT [FK_DocExperienciaEntidad_InfoExperienciaEntidad]
GO
ALTER TABLE [PROVEEDOR].[DocExperienciaEntidad]  WITH CHECK ADD  CONSTRAINT [FK_DocExperienciaEntidad_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [PROVEEDOR].[TipoDocumento] ([IdTipoDocumento])
GO
ALTER TABLE [PROVEEDOR].[DocExperienciaEntidad] CHECK CONSTRAINT [FK_DocExperienciaEntidad_TipoDocumento]
GO
ALTER TABLE [PROVEEDOR].[DocFinancieraProv]  WITH CHECK ADD  CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad] FOREIGN KEY([IdInfoFin])
REFERENCES [PROVEEDOR].[InfoFinancieraEntidad] ([IdInfoFin])
GO
ALTER TABLE [PROVEEDOR].[DocFinancieraProv] CHECK CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad]
GO
ALTER TABLE [PROVEEDOR].[DocFinancieraProv]  WITH CHECK ADD  CONSTRAINT [FK_DocFinancieraProv_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [PROVEEDOR].[TipoDocumento] ([IdTipoDocumento])
GO
ALTER TABLE [PROVEEDOR].[DocFinancieraProv] CHECK CONSTRAINT [FK_DocFinancieraProv_TipoDocumento]
GO
ALTER TABLE [PROVEEDOR].[DocNotificacionJudicial]  WITH CHECK ADD  CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial] FOREIGN KEY([IdNotJudicial])
REFERENCES [PROVEEDOR].[NotificacionJudicial] ([IdNotJudicial])
GO
ALTER TABLE [PROVEEDOR].[DocNotificacionJudicial] CHECK CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial]
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos] FOREIGN KEY([IdEstado])
REFERENCES [PROVEEDOR].[EstadoDatosBasicos] ([IdEstadoDatosBasicos])
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos]
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno] FOREIGN KEY([IdTipoNivelGob])
REFERENCES [PROVEEDOR].[Niveldegobierno] ([IdNiveldegobierno])
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno]
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional] FOREIGN KEY([IdTipoNivelOrganizacional])
REFERENCES [PROVEEDOR].[NivelOrganizacional] ([IdNivelOrganizacional])
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional]
GO
--ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TERCERO] FOREIGN KEY([IdTercero])
--REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
--GO
--ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TERCERO]
--GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TipoCiiu] FOREIGN KEY([IdTipoCiiuPrincipal])
REFERENCES [PROVEEDOR].[TipoCiiu] ([IdTipoCiiu])
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TipoCiiu]
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad] FOREIGN KEY([IdTipoSector])
REFERENCES [PROVEEDOR].[TipoSectorEntidad] ([IdTipoSectorEntidad])
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad]
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [fk_IdAcuerdoProveedor] FOREIGN KEY([IdAcuerdo])
REFERENCES [PROVEEDOR].[Acuerdos] ([IdAcuerdo])
GO
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [fk_IdAcuerdoProveedor]
GO
ALTER TABLE [PROVEEDOR].[ExperienciaCodUNSPSCEntidad]  WITH CHECK ADD  CONSTRAINT [FK_IdExpEntidad_InfoExperienciaEntidad] FOREIGN KEY([IdExpEntidad])
REFERENCES [PROVEEDOR].[InfoExperienciaEntidad] ([IdExpEntidad])
GO
ALTER TABLE [PROVEEDOR].[ExperienciaCodUNSPSCEntidad] CHECK CONSTRAINT [FK_IdExpEntidad_InfoExperienciaEntidad]
GO
ALTER TABLE [PROVEEDOR].[ExperienciaCodUNSPSCEntidad]  WITH CHECK ADD  CONSTRAINT [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC] FOREIGN KEY([IdTipoCodUNSPSC])
REFERENCES [PROVEEDOR].[TipoCodigoUNSPSC] ([IdTipoCodUNSPSC])
GO
ALTER TABLE [PROVEEDOR].[ExperienciaCodUNSPSCEntidad] CHECK CONSTRAINT [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC]
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_EntidadProvOferente]
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad] FOREIGN KEY([IdTipoActividad])
REFERENCES [PROVEEDOR].[TipodeActividad] ([IdTipodeActividad])
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipodeActividad]
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica] FOREIGN KEY([IdTipoEntidadPublica])
REFERENCES [PROVEEDOR].[TipodeentidadPublica] ([IdTipodeentidadPublica])
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica]
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad] FOREIGN KEY([IdTipoEntidad])
REFERENCES [PROVEEDOR].[TipoEntidad] ([IdTipoentidad])
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_Tipoentidad]
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario] FOREIGN KEY([IdTipoRegTrib])
REFERENCES [PROVEEDOR].[TipoRegimenTributario] ([IdTipoRegimenTributario])
GO
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipoRegimenTributario]
GO
--ALTER TABLE [PROVEEDOR].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_Zona] FOREIGN KEY([IdZona])
--REFERENCES [Oferente].[Zona] ([IdZona])
--GO
--ALTER TABLE [PROVEEDOR].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_Zona]
--GO
ALTER TABLE [PROVEEDOR].[InfoExperienciaEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoExperienciaEntidad_TipoCodigoUNSPSC] FOREIGN KEY([IdTipoCodUNSPSC])
REFERENCES [PROVEEDOR].[TipoCodigoUNSPSC] ([IdTipoCodUNSPSC])
GO
ALTER TABLE [PROVEEDOR].[InfoExperienciaEntidad] CHECK CONSTRAINT [FK_InfoExperienciaEntidad_TipoCodigoUNSPSC]
GO
ALTER TABLE [PROVEEDOR].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO
ALTER TABLE [PROVEEDOR].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_EntidadProvOferente]
GO
ALTER TABLE [PROVEEDOR].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental] FOREIGN KEY([EstadoValidacion])
REFERENCES [PROVEEDOR].[EstadoValidacionDocumental] ([IdEstadoValidacionDocumental])
GO
ALTER TABLE [PROVEEDOR].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_EstadoValidacionDocumental]
GO
ALTER TABLE [PROVEEDOR].[InfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia] FOREIGN KEY([IdVigencia])
REFERENCES [Global].[Vigencia] ([IdVigencia])
GO
ALTER TABLE [PROVEEDOR].[InfoFinancieraEntidad] CHECK CONSTRAINT [FK_InfoFinancieraEntidad_Vigencia]
GO
--ALTER TABLE [PROVEEDOR].[MotivoCambioEstado]  WITH CHECK ADD  CONSTRAINT [FK_MotivoCambioEstado_TERCERO] FOREIGN KEY([IdTercero])
--REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
--GO
--ALTER TABLE [PROVEEDOR].[MotivoCambioEstado] CHECK CONSTRAINT [FK_MotivoCambioEstado_TERCERO]
--GO
ALTER TABLE [PROVEEDOR].[NotificacionJudicial]  WITH CHECK ADD  CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO
ALTER TABLE [PROVEEDOR].[NotificacionJudicial] CHECK CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente]
GO
ALTER TABLE [PROVEEDOR].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_Departamento] FOREIGN KEY([Departamento])
REFERENCES [DIV].[Departamento] ([IdDepartamento])
GO
ALTER TABLE [PROVEEDOR].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Departamento]
GO
ALTER TABLE [PROVEEDOR].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_IdEntidad] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO
ALTER TABLE [PROVEEDOR].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_IdEntidad]
GO
ALTER TABLE [PROVEEDOR].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_Municipio] FOREIGN KEY([Municipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
ALTER TABLE [PROVEEDOR].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Municipio]
GO
ALTER TABLE [PROVEEDOR].[TipoDocumentoPrograma]  WITH CHECK ADD  CONSTRAINT [FK_TipoDocumentoPrograma_Programa] FOREIGN KEY([IdPrograma])
REFERENCES [SEG].[Programa] ([IdPrograma])
GO
ALTER TABLE [PROVEEDOR].[TipoDocumentoPrograma] CHECK CONSTRAINT [FK_TipoDocumentoPrograma_Programa]
GO
ALTER TABLE [PROVEEDOR].[TipoDocumentoPrograma]  WITH CHECK ADD  CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento] FOREIGN KEY([IdTipoDocumento])
REFERENCES [PROVEEDOR].[TipoDocumento] ([IdTipoDocumento])
GO
ALTER TABLE [PROVEEDOR].[TipoDocumentoPrograma] CHECK CONSTRAINT [FK_TipoDocumentoPrograma_TipoDocumento]
GO
ALTER TABLE [PROVEEDOR].[ValidarInfoDatosBasicosEntidad]  WITH CHECK ADD  CONSTRAINT [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO
ALTER TABLE [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] CHECK CONSTRAINT [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad]
GO
ALTER TABLE [PROVEEDOR].[ValidarInfoExperienciaEntidad]  WITH CHECK ADD  CONSTRAINT [IdInfoExperienciaEntidad_InfoExperienciaEntidad] FOREIGN KEY([IdExpEntidad])
REFERENCES [PROVEEDOR].[InfoExperienciaEntidad] ([IdExpEntidad])
GO
ALTER TABLE [PROVEEDOR].[ValidarInfoExperienciaEntidad] CHECK CONSTRAINT [IdInfoExperienciaEntidad_InfoExperienciaEntidad]
GO
ALTER TABLE [PROVEEDOR].[ValidarInfoFinancieraEntidad]  WITH CHECK ADD  CONSTRAINT [IdInfoFinancieraEntidad_InfoFinancieraEntidad] FOREIGN KEY([IdInfoFin])
REFERENCES [PROVEEDOR].[InfoFinancieraEntidad] ([IdInfoFin])
GO
ALTER TABLE [PROVEEDOR].[ValidarInfoFinancieraEntidad] CHECK CONSTRAINT [IdInfoFinancieraEntidad_InfoFinancieraEntidad]
GO
--ALTER TABLE [PROVEEDOR].[ValidarTercero]  WITH CHECK ADD  CONSTRAINT [IdTercero_Tercero] FOREIGN KEY([IdTercero])
--REFERENCES [Oferente].[TERCERO] ([IDTERCERO])
--GO
--ALTER TABLE [PROVEEDOR].[ValidarTercero] CHECK CONSTRAINT [IdTercero_Tercero]
--GO
