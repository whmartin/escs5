USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]    Script Date: 04/10/2014 19:01:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]    Script Date: 04/10/2014 19:01:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  5/23/2013 5:33:48 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Tercero
-- Modificaci�n: 24-FEB-2014
-- Desarrolldaor Juan Carlos Valverde S�mano
-- Descripci�n: En base al control de cambios CO_024, se ha agrega lo sigueinte.
--				Compara si los datos que vienen del formulario de registro de terceros
--				Son los mismos al macenados en SEG.Usuario (nombres y apellidos � raz�n social) 
--				de acuerdo al tipo de Persona, y en caso de ser diferentes actualiza la tabla SEG.Usuario
--				con esta nueva informaci�n.
-- Modificaci�n: 03-ABRIL-2014
-- Desarrolldaor Juan Carlos Valverde S�mano
-- Descripci�n: Se agreg� un validaci�n mas que dice: Si se trata de un Usuario Externo
-- Entonces pasa a actualizar la informaci�n en la entidad de Usuario, en caso de ser un
-- Usuario Interno o Administrador, solo registra el Tercero y termina.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]

	@IdTercero INT OUTPUT, 
	@IdDListaTipoDocumento INT, 
	@IdTipoPersona INT=NULL,
	@NumeroIdentificacion NVARCHAR (255), 
	@PrimerNombre NVARCHAR (255), 
	@SegundoNombre NVARCHAR (255), 
	@PrimerApellido NVARCHAR (255), 
	@SegundoApellido NVARCHAR (255), 
	@Email NVARCHAR (255), 
	@Sexo NVARCHAR (1), 
	@ProviderUserKey UNIQUEIDENTIFIER=NULL,
	@DIGITOVERIFICACION INT=NULL,
	@IDESTADOTERCERO INT =NULL,
	@RAZONSOCIAL NVARCHAR (250)=NULL,
	@FechaExpedicionId DATETIME =null, 
	@FechaNacimiento DATETIME =null, 
	@UsuarioCrea NVARCHAR (250),
	@IdTemporal VARCHAR(20)= NULL,
	@CreadoPorInterno BIT = NULL
	
AS
BEGIN

INSERT INTO Oferente.Tercero 
         ([IDTIPODOCIDENTIFICA]
         ,[IDESTADOTERCERO]
         ,[IdTipoPersona]
         ,[ProviderUserKey]
         ,[NUMEROIDENTIFICACION]
         ,[DIGITOVERIFICACION]
         ,[CORREOELECTRONICO]
         ,[PRIMERNOMBRE]
         ,[SEGUNDONOMBRE]
         ,[PRIMERAPELLIDO]
         ,[SEGUNDOAPELLIDO]
         ,[RAZONSOCIAL]
         ,[FECHAEXPEDICIONID]
         ,[FECHANACIMIENTO]
         ,[SEXO]
         ,UsuarioCrea
         ,FechaCrea
         ,CreadoPorInterno)
	VALUES (@IdDListaTipoDocumento,
	        @IDESTADOTERCERO,
	        @IdTipoPersona,
	        @ProviderUserKey,
	        @NumeroIdentificacion, 
	        @DIGITOVERIFICACION,
	        @Email, 
	        @PrimerNombre, 
	        @SegundoNombre, 
	        @PrimerApellido, 
	        @SegundoApellido, 
	        @RAZONSOCIAL,
	        @FechaExpedicionId, 
	        @FechaNacimiento,
	        @Sexo, 
	        @UsuarioCrea, 
	        GETDATE(),
	        @CreadoPorInterno)

SELECT @IdTercero=@@IDENTITY

UPDATE [Proveedor].[DocAdjuntoTercero]  
	set IdTercero = @IdTercero
	where IdTemporal = @IdTemporal


IF(@CreadoPorInterno=0)
BEGIN
	--VERIFICAR SI SE TRATA DE UN USUARIO EXTERNO (REGISTRADO DESDE PROVEEDORES)---- SI ES ASI PARA PASAR A VALIDAR A ACTUALIZAR SU INFORMACI�N
	--EN LA ENTIDAD USUARIO, EN CASO CONTRARIO NO REALIZA LA ACTUALIZACI�N.
	IF((SELECT CorreoElectronico FROM SEG.Usuario
	WHERE providerKey=@ProviderUserKey) = @Email)
	BEGIN
		--------------VERIFICAR DATOS CON SEG.Usuario Y SI DIFIEREN ....ACTUALIZARLOS--------------------------------
		IF (@IdTipoPersona=1)
		BEGIN
			DECLARE @primer_Nombre NVARCHAR(150),
			 @segundo_Nombre NVARCHAR(150),
			 @primer_Apellido NVARCHAR(150),
			 @segundo_Apellido NVARCHAR(150)
			 
			 SELECT @primer_Nombre=ISNULL([PrimerNombre],''),
			 @segundo_Nombre=ISNULL([SegundoNombre],''),
			 @primer_Apellido=ISNULL([PrimerApellido],''),
			 @segundo_Apellido=ISNULL([SegundoApellido],'')
			 FROM SEG.Usuario
			 WHERE providerKey=@ProviderUserKey
			 
			 IF(@PrimerNombre!=@primer_Nombre)
			 BEGIN
				 UPDATE SEG.Usuario SET PrimerNombre=@PrimerNombre
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@SegundoNombre!=@segundo_Nombre)
			 BEGIN
				 UPDATE SEG.Usuario SET SegundoNombre=@SegundoNombre
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@PrimerApellido!= @primer_Apellido)
			 BEGIN
				 UPDATE SEG.Usuario SET PrimerApellido=@PrimerApellido
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@SegundoApellido!=@segundo_Apellido)
			 BEGIN
				 UPDATE SEG.Usuario SET SegundoApellido=@SegundoApellido
				 WHERE providerKey=@ProviderUserKey
			 END
		END 
		ELSE IF (@IdTipoPersona=2)
		BEGIN
			DECLARE @razon_Social NVARCHAR(256)
			SELECT @razon_Social=ISNULL([RazonSocial],'')
			FROM SEG.Usuario
			WHERE providerKey=@ProviderUserKey
			
			IF(@RAZONSOCIAL!=@razon_Social)
			 BEGIN
				 UPDATE SEG.Usuario SET RazonSocial=@RAZONSOCIAL
				 WHERE providerKey=@ProviderUserKey
			 END
		END
	END
		------------------------------------------------
END
	
	SELECT @IdTercero


RETURN @@IDENTITY

END


GO


