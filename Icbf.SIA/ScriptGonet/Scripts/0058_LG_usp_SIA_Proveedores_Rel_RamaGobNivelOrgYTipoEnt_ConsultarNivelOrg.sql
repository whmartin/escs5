USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelOrg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelOrg]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonz�lez
-- Create date:  01/04/2014 17:23 PM
-- Description:	Procedimiento almacenado que consulta NiveleOrganizacional por RamaEstructura y NivelGobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelOrg]

	@IdRamaEstructura INT, @IdNivelGobierno INT
AS
BEGIN

	SELECT	DISTINCT(RelRamaGob.IdNivelOrganizacional),
			CodigoNivelOrganizacional,
			Descripcion
	FROM	PROVEEDOR.Rel_RamaGobNivelOrgYTipoEnt RelRamaGob
	JOIN
			Proveedor.NivelOrganizacional NivelOrg
	ON
			RelRamaGob.IdNivelOrganizacional = NivelOrg.IdNivelOrganizacional
	WHERE	
			RelRamaGob.IdRamaEstructura = @IdRamaEstructura
	AND
			RelRamaGob.IdNivelGobierno = @IdNivelGobierno
	ORDER BY
		Descripcion ASC

END