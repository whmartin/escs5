USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Sucursal_ModificarDefault]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_ModificarDefault]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonz�lez
-- Create date:  03/21/2014 18:23:48 PM
-- Description:	Procedimiento almacenado que modifica la sucursal default de una entidad
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_ModificarDefault]
		@IdEntidad INT,	
		@Nombre VARCHAR(256), 
		@Indicativo INT,	
		@Telefono INT,	
		@Extension NUMERIC(10) = NULL,	
		@Celular  NUMERIC(10),	
		@Correo NVARCHAR(256),	
		@Estado INT, 
		@IdZona INT,	
		@Departamento INT,	
		@Municipio INT,	
		@Direccion NVARCHAR(256), 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN

 UPDATE	PROVEEDOR.Sucursal
 SET	Nombre = @Nombre, 
		Indicativo = @Indicativo, 
		Telefono = @Telefono, 
		Extension = @Extension, 
		Celular = @Celular, 
		Correo = @Correo, 
		Estado = @Estado, 
		IdZona = @IdZona, 
		Departamento = @Departamento, 
		Municipio = @Municipio, 
		Direccion = @Direccion, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
 WHERE	IdEntidad = @IdEntidad
 AND	Editable = 0
 
END