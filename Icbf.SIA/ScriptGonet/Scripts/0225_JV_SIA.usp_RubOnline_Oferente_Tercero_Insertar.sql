USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Insertar]    Script Date: 06/06/2014 16:19:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Insertar]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Insertar]    Script Date: 06/06/2014 16:19:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date:  06/06/2043 5:33:48 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Insertar]

	@IdTercero INT OUTPUT, 
	@IdDListaTipoDocumento INT, 
	@IdTipoPersona INT=NULL,
	@NumeroIdentificacion NVARCHAR (255), 
	@PrimerNombre NVARCHAR (255), 
	@SegundoNombre NVARCHAR (255), 
	@PrimerApellido NVARCHAR (255), 
	@SegundoApellido NVARCHAR (255), 
	@Email NVARCHAR (255), 
	@Sexo NVARCHAR (1), 
	@ProviderUserKey UNIQUEIDENTIFIER=NULL,
	@DIGITOVERIFICACION INT=NULL,
	@IDESTADOTERCERO INT =NULL,
	@RAZONSOCIAL NVARCHAR (250)=NULL,
	@FechaExpedicionId DATETIME =null, 
	@FechaNacimiento DATETIME =null, 
	@UsuarioCrea NVARCHAR (250)
	
AS
BEGIN

INSERT INTO Oferente.TERCERO
         ([IDTIPODOCIDENTIFICA]
         ,[IDESTADOTERCERO]
         ,[IdTipoPersona]
         ,[ProviderUserKey]
         ,[NUMEROIDENTIFICACION]
         ,[DIGITOVERIFICACION]
         ,[CORREOELECTRONICO]
         ,[PRIMERNOMBRE]
         ,[SEGUNDONOMBRE]
         ,[PRIMERAPELLIDO]
         ,[SEGUNDOAPELLIDO]
         ,[RAZONSOCIAL]
         ,[FECHAEXPEDICIONID]
         ,[FECHANACIMIENTO]
         ,[SEXO]
         ,UsuarioCrea
         ,FechaCrea)
	VALUES (@IdDListaTipoDocumento,
	        @IDESTADOTERCERO,
	        @IdTipoPersona,
	        @ProviderUserKey,
	        @NumeroIdentificacion, 
	        @DIGITOVERIFICACION,
	        @Email, 
	        @PrimerNombre, 
	        @SegundoNombre, 
	        @PrimerApellido, 
	        @SegundoApellido, 
	        @RAZONSOCIAL,
	        @FechaExpedicionId, 
	        @FechaNacimiento,
	        @Sexo, 
	        @UsuarioCrea, 
	        GETDATE())

SELECT @IdTercero=@@IDENTITY

	
--VERIFICAR SI SE TRATA DE UN USUARIO EXTERNO (REGISTRADO DESDE PROVEEDORES)---- SI ES ASI PARA PASAR A VALIDAR A ACTUALIZAR SU INFORMACIÓN
--EN LA ENTIDAD USUARIO, EN CASO CONTRARIO NO REALIZA LA ACTUALIZACIÓN.
IF EXISTS (SELECT IdUsuario FROM SEG.Usuario
WHERE providerKey=@ProviderUserKey)
BEGIN
--------------VERIFICAR DATOS CON SEG.Usuario Y SI DIFIEREN ....ACTUALIZARLOS--------------------------------
			
		IF (@IdTipoPersona=1)
		BEGIN
			DECLARE @primer_Nombre NVARCHAR(150),
			 @segundo_Nombre NVARCHAR(150),
			 @primer_Apellido NVARCHAR(150),
			 @segundo_Apellido NVARCHAR(150)
			 
			 SELECT @primer_Nombre=ISNULL([PrimerNombre],''),
			 @segundo_Nombre=ISNULL([SegundoNombre],''),
			 @primer_Apellido=ISNULL([PrimerApellido],''),
			 @segundo_Apellido=ISNULL([SegundoApellido],'')
			 FROM SEG.Usuario
			 WHERE providerKey=@ProviderUserKey
			 
			 IF(@PrimerNombre!=@primer_Nombre)
			 BEGIN
				 UPDATE SEG.Usuario SET PrimerNombre=@PrimerNombre
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@SegundoNombre!=@segundo_Nombre)
			 BEGIN
				 UPDATE SEG.Usuario SET SegundoNombre=@SegundoNombre
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@PrimerApellido!= @primer_Apellido)
			 BEGIN
				 UPDATE SEG.Usuario SET PrimerApellido=@PrimerApellido
				 WHERE providerKey=@ProviderUserKey
			 END
			 
			 IF(@SegundoApellido!=@segundo_Apellido)
			 BEGIN
				 UPDATE SEG.Usuario SET SegundoApellido=@SegundoApellido
				 WHERE providerKey=@ProviderUserKey
			 END
		END 
		ELSE IF (@IdTipoPersona=2)
		BEGIN
			DECLARE @razon_Social NVARCHAR(256)
			SELECT @razon_Social=ISNULL([RazonSocial],'')
			FROM SEG.Usuario
			WHERE providerKey=@ProviderUserKey
			
			IF(@RAZONSOCIAL!=@razon_Social)
			 BEGIN
				 UPDATE SEG.Usuario SET RazonSocial=@RAZONSOCIAL
				 WHERE providerKey=@ProviderUserKey
			 END
		END
END	
	
	------------------------------------------------

RETURN @@IDENTITY

END


GO


