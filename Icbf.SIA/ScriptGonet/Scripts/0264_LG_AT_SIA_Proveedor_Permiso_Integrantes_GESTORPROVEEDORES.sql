USE [SIA]
GO
/***********************************************
Creado Por: Leticia Elizabeth Gonz�lez Flores
El dia : 2013-06-16
Permite : Crear el permiso al programa  Integrantes
***********************************************/
IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/Integrantes')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/Integrantes'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
			   ([IdPrograma]
			   ,[IdRol]
			   ,[Insertar]
			   ,[Modificar]
			   ,[Eliminar]
			   ,[Consultar]
			   ,[UsuarioCreacion]
			   ,[FechaCreacion])           
		 VALUES
			   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Proveedor/Integrantes')
			   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'GESTOR PROVEEDOR')
			   ,1
			   ,1
			   ,0
			   ,1
			   ,'Administrador'
			   ,GETDATE()) 
	END
END      
GO
