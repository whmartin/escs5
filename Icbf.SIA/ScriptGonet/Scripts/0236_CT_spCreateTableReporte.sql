USE [RubOnline]
GO
--USE [RubOnline]
--GO

-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  21/05/2014
-- Description:	Crea tabla para almacenar el listado de los reportes
-- =============================================

IF (SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='Reporte' And TABLE_SCHEMA = 'Global')>0 DROP TABLE Global.Reporte 


CREATE TABLE [Global].[Reporte](
 [IdReporte] [INT]  IDENTITY(1,1) NOT NULL,
 [NombreReporte] [NVARCHAR] (512) NOT NULL,
 [Descripcion] [NVARCHAR] (1024) ,
 [Servidor] [NVARCHAR] (512) NOT NULL,
 [Carpeta] [NVARCHAR] (512) NOT NULL,
 [NombreArchivo] [NVARCHAR] (512) NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_Reporte] PRIMARY KEY CLUSTERED 
(
 	[IdReporte] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
