USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]    Script Date: 03/31/2014 13:23:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]    Script Date: 03/31/2014 13:23:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
--Modifica�do Por: Juan Carlos Valverden S�mano
--Fecha: 31/03/2014
-- Descripci�n: Se agreg� el Filtro de Estado=1 para que retorne s�lo los registros habilitados.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]
	@IdTipoPersona INT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE t.IdTipoPersona = @IdTipoPersona AND trt.Estado =1
 ORDER BY Descripcion
 END



GO


