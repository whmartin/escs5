USE [SIA]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha Creaci�n: 01-Abril-2014
Descripci�n: en base a un requerimiento del CC016, se realiza un reset de este cat�logo 
llevar un mejor orden, y pasar a completar el requerimiento de la relaci�n entre las
tablas RamaoEstructura-Niveldegobierno-NivelOrganizacional-TipodeentidadPublica
**/
GO
TRUNCATE TABLE [PROVEEDOR].[RamaoEstructura]
GO
INSERT INTO [PROVEEDOR].[RamaoEstructura]
           ([CodigoRamaEstructura]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('001','EJECUTIVA',1,'Administrador',GETDATE(),NULL,NULL)
INSERT INTO [PROVEEDOR].[RamaoEstructura]
           ([CodigoRamaEstructura]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('002','JUDICIAL',1,'Administrador',GETDATE(),NULL,NULL)
INSERT INTO [PROVEEDOR].[RamaoEstructura]
           ([CodigoRamaEstructura]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('003','LEGISLATIVA',1,'Administrador',GETDATE(),NULL,NULL)
INSERT INTO [PROVEEDOR].[RamaoEstructura]
           ([CodigoRamaEstructura]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('004','�RGANOS AUTONOMOS',1,'Administrador',GETDATE(),NULL,NULL)
INSERT INTO [PROVEEDOR].[RamaoEstructura]
           ([CodigoRamaEstructura]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('005','�RGANOS DE CONTROL',1,'Administrador',GETDATE(),NULL,NULL)
INSERT INTO [PROVEEDOR].[RamaoEstructura]
           ([CodigoRamaEstructura]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('006','�RGANOS ELECTORALES',1,'Administrador',GETDATE(),NULL,NULL)
GO