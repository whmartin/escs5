USE [SIA]
GO

-- =============================================
-- Author:	 Faiber Losada
-- Create date:  23/7/2014 7:00:00 AM
-- Description:	Creaci�n del Men� del parametrizador
-- Modificaci�n: 12/08]/2014 Juan Carlos Valverde S�mano
-- Descripci�n: La forma en que se estaba configurando el Programa Y Permiso
-- estaba incorrecta.
-- =============================================

DECLARE @IdPrograma INT
DECLARE @idModulo INT 
SELECT @idModulo=IdModulo FROM SEG.Modulo
WHERE NombreModulo='Proveedores'
DECLARE @IdRol INT=
(SELECT IdRol FROM SEG.Rol
WHERE Nombre='ADMINISTRADORSIA')

IF NOT EXISTS(SELECT IdPrograma FROM SEG.Programa WHERE NombrePrograma='ValidarIntegrantes')
BEGIN
	--Se crea el Men� del proceso
	
	INSERT INTO SEG.Programa(IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, VisibleMenu, generaLog)
	VALUES (@idModulo, 'ValidarIntegrantes', 'Proveedor/ValidarIntegrantes', 1, 1, 'Administrador', GETDATE(), 0, 1)

	SET @IdPrograma = SCOPE_IDENTITY()

	INSERT INTO SEG.Permiso(IdPrograma, IdRol, Insertar, Modificar, Eliminar, Consultar, UsuarioCreacion, FechaCreacion)
	VALUES(@IdPrograma, @IdRol, 1, 0, 0, 1, 'Administrador', GETDATE())

END
ELSE
BEGIN
	PRINT 'Ya existe el programa y est� configurado.'
END