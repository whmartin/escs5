/**
Autor: Juan Carlos Valverde S�mano
Fecha Creaci�n: 25-Febrero-2014
Descripci�n: En base al CO_024, para el requerimiento de agregar a la tabla
de Terceros la nueva columna "ConsecutivoInterno"; el presente script sirve para
crear el consecutivo interno de los Terceros que ya se encuentran registrados.
**/
USE [Oferentes]
GO



DECLARE @idTercero INT,
@txtIdTercero VARCHAR(8),
@anioCreacion VARCHAR(4),
@mesCreacion VARCHAR(2)

SELECT
    RowNum = ROW_NUMBER() OVER(ORDER BY IDTERCERO)
    ,IDTERCERO,FECHACREA
INTO #Terceros
FROM Oferente.TERCERO
DECLARE @MaxRownum INT
SET @MaxRownum = (SELECT MAX(RowNum) FROM #Terceros)

DECLARE @Iter INT
SET @Iter = (SELECT MIN(RowNum) FROM #Terceros)

WHILE @Iter <= @MaxRownum
BEGIN
	 SELECT 
	 @idTercero=[IDTERCERO],
	 @txtIdTercero=(RIGHT('0000000'+CAST([IDTERCERO] AS varchar), 8)),
	 @anioCreacion=YEAR([FECHACREA]),
	 @mesCreacion=(RIGHT('0'+CAST(MONTH([FECHACREA]) AS varchar), 2))
	 FROM #Terceros
	 WHERE RowNum = @Iter
	 
	 UPDATE Oferente.TERCERO
	 SET ConsecutivoInterno=('T'+@anioCreacion+@mesCreacion+'-'+@txtIdTercero)
	 WHERE IDTERCERO=@idTercero
   
    SET @Iter = @Iter + 1
END

DROP TABLE #Terceros         
