USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]    Script Date: 08/12/2014 12:11:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]    Script Date: 08/12/2014 12:11:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]
		@IdValidarInfoExperienciaEntidad INT OUTPUT, @IdExpEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoExperienciaEntidad(IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdExpEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoExperienciaEntidad = SCOPE_IDENTITY() 		
END





GO


