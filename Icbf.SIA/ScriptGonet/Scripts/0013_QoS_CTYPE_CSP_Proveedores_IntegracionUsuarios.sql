USE QOS
GO

/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripcion: Creación de tipo y procedimientos almacenados para servicio de integración de usuarios en la BD QoS
*/

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'EnvioCorreo'  
              AND Object_ID = Object_ID(N'aspnet_Membership'))

BEGIN

    ALTER TABLE aspnet_Membership ADD EnvioCorreo BIT NOT NULL DEFAULT(0)

END
GO


/****** Object:  StoredProcedure [dbo].[Table_Proveedores_IntegracionUsuarios]    Script Date: 07/22/2013 20:44:10 ******/
IF EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'Table_Proveedores_IntegracionUsuarios')
DROP type [dbo].[Table_Proveedores_IntegracionUsuarios]
GO

USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[Table_Proveedores_IntegracionUsuarios]    Script Date: 07/22/2013 20:44:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ==========================================================================================
-- Author:		 Carlos Cubillos
-- Create date:  22/07/2013
-- Description:	 Tipo para uso de procedimientos de integración
-- ==========================================================================================
CREATE TYPE [dbo].[Table_Proveedores_IntegracionUsuarios] AS TABLE(
      [UserId] [nvarchar](125) NOT NULL,
      [PrimerNombre] [nvarchar](150) NULL,
      [SegundoNombre] [nvarchar](150) NULL,
      [PrimerApellido] [nvarchar](150) NULL,
      [SegundoApellido] [nvarchar](150) NULL,
      [RazonSocial] [nvarchar](150) NULL,
      [ExisteTercero] [Bit] NULL,
      [IsApproved] [Bit] NULL
)


GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]    Script Date: 07/22/2013 20:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]
GO

USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]    Script Date: 07/22/2013 20:28:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- ==========================================================================================
-- Author:		 Carlos Felipe Cubillos
-- Create date:  14/06/2013 11:38:19 AM
-- Description:	 Obtiene los usuarios creados después de cierto parámetro
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]


@ApplicationName NVARCHAR(256), --- Nombre de la aplicacion en el membership
@RoleName NVARCHAR(256), ---Rol del usuario
@MesesEliminacion INT,  ---Numero de meses para eliminar usuario inactivo
@DiasMail INT --Numero de dias antes de eliminación de usuario(Envio Email)

AS
BEGIN

DECLARE @ApplicationId		uniqueidentifier

SELECT @ApplicationId = [ApplicationId]
	FROM [QoS].[dbo].[aspnet_Applications]
	WHERE [ApplicationName] = @ApplicationName

SELECT [QOS].[DBO].aspnet_Membership.UserId, IsApproved
	FROM [QoS].[dbo].aspnet_UsersInRoles
		INNER JOIN [QoS].[dbo].aspnet_Roles on [QoS].[dbo].aspnet_UsersInRoles.RoleId = [QoS].[dbo].aspnet_Roles.RoleId
		INNER JOIN [QoS].[dbo].aspnet_Users on [QoS].[dbo].aspnet_UsersInRoles.UserId = [QoS].[dbo].aspnet_Users.UserId
											AND [QoS].[dbo].aspnet_Roles.ApplicationId = [QoS].[dbo].aspnet_Roles.ApplicationId
		INNER JOIN [QoS].[dbo].aspnet_Membership on [QoS].[dbo].aspnet_Users.UserId = [QoS].[dbo].aspnet_Membership.UserId
	WHERE [QOS].[DBO].aspnet_Membership.[ApplicationId] = @ApplicationId
		AND aspnet_Roles.RoleName = @RoleName
		AND (GETDATE() >= DATEADD(MONTH, @MesesEliminacion , (DATEADD(DAY, - @DiasMail, CreateDate)))
			OR GETDATE() >= DATEADD(MONTH, @MesesEliminacion, CreateDate))
	
END


GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_UsuariosInactivos]    Script Date: 07/22/2013 20:36:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QoS_Proveedor_UsuariosInactivos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]
GO

USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_UsuariosInactivos]    Script Date: 07/22/2013 20:36:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- ==========================================================================================
-- Author:		 José Ignacio De Los Reyes
-- Create date:  14/06/2013 11:38:19 AM
-- Description:	 Se Crea para enivar correo de suspension de usuario si su estado es inactivo
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]

@ApplicationName NVARCHAR(256), --- Nombre de la aplicacion en el membership
@RoleName NVARCHAR(256), ---Rol del usuario
@MesesEliminacion INT,  ---Numero de meses para eliminar usuario inactivo
@DiasMail INT, --Numero de dias antes de eliminación de usuario(Envio Email)
@TextoEmail NVARCHAR(MAX), --Texto del email enviado
@TituloEmail NVARCHAR(256), --Título del email enviado
@Paso1 NVARCHAR(256), --Paso 1 de ingreso al sistema email
@Paso2 NVARCHAR(256), --Paso 2 de ingreso al sistema email
@Paso3 NVARCHAR(256), --Paso 3 de ingreso al sistema email
@Paso4 NVARCHAR(256), --Paso 4 de ingreso al sistema email
@pData Table_Proveedores_IntegracionUsuarios readonly --Tipo con los datos de integración


AS
BEGIN

	DECLARE @ApplicationId		uniqueidentifier,
			@UserId				uniqueidentifier,
			@IdUsuario			INT,
			@ProviderKey		VARCHAR(125),
			@Index				INT,
			@CantRegistros		INT,
			@Email_Body			NVARCHAR(MAX),
			@Email_Destino		NVARCHAR(512),
			@Usuario			NVARCHAR(300),
			@FechaEliminacion	DATETIME

	SELECT @ApplicationId = [ApplicationId]
	FROM [QoS].[dbo].[aspnet_Applications]
	WHERE [ApplicationName] = @ApplicationName
	
	--Tabla temporal con los que debe enviar correo
	SELECT IDENTITY(INT,1,1) ID, [QOS].[DBO].aspnet_Membership.*, 
	datos.PrimerNombre, datos.SegundoNombre, datos.PrimerApellido, datos.SegundoApellido, datos.RazonSocial
	INTO #TMP_UsuariosInactivos
	FROM [QoS].[dbo].aspnet_UsersInRoles
		INNER JOIN [QoS].[dbo].aspnet_Roles on [QoS].[dbo].aspnet_UsersInRoles.RoleId = [QoS].[dbo].aspnet_Roles.RoleId
		INNER JOIN [QoS].[dbo].aspnet_Users on [QoS].[dbo].aspnet_UsersInRoles.UserId = [QoS].[dbo].aspnet_Users.UserId
											AND [QoS].[dbo].aspnet_Roles.ApplicationId = [QoS].[dbo].aspnet_Roles.ApplicationId
		INNER JOIN [QoS].[dbo].aspnet_Membership on [QoS].[dbo].aspnet_Users.UserId = [QoS].[dbo].aspnet_Membership.UserId
		INNER JOIN @pData datos on [QoS].[dbo].aspnet_UsersInRoles.UserId = datos.UserId
	WHERE [QOS].[DBO].aspnet_Membership.[ApplicationId] = @ApplicationId
		AND ([QOS].[DBO].aspnet_Membership.IsApproved = 0 OR datos.ExisteTercero = 0)
		AND EnvioCorreo = 0
		AND aspnet_Roles.RoleName = @RoleName
		AND GETDATE() >= DATEADD(MONTH, @MesesEliminacion , (DATEADD(DAY, - @DiasMail, CreateDate)))
	
	SELECT @CantRegistros = COUNT(ID) FROM #TMP_UsuariosInactivos
	SET @Index = 1
	
	WHILE(@CantRegistros >= @Index)
	BEGIN
		SELECT @Email_Destino = ''
		
		SELECT @Email_Destino = #TMP_UsuariosInactivos.Email,
			   @UserId = #TMP_UsuariosInactivos.UserId,
			   @Usuario = CASE
					WHEN #TMP_UsuariosInactivos.RazonSocial IS null THEN 
						#TMP_UsuariosInactivos.PrimerNombre + ' ' + #TMP_UsuariosInactivos.SegundoNombre + ' ' + #TMP_UsuariosInactivos.PrimerApellido + ' ' + #TMP_UsuariosInactivos.SegundoApellido
					ELSE #TMP_UsuariosInactivos.RazonSocial
					END
		FROM #TMP_UsuariosInactivos
		WHERE ID = @Index
		
		
		
				
		SELECT @Email_Body = ''
		SELECT @Email_Body = @Email_Body + '<table width=100% height=412 border=0 cellpadding=0 cellspacing=0>
												<tr>
													<td colspan=3 bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td width=10% bgcolor=#81BA3D>&nbsp;</td>
													<td>
														<table width=100% border=0 align=center>
															<tr>
																<td width=5%>&nbsp;</td>
																<td align=center><strong> '
		--Cambiar por parámetro
        SELECT @Email_Body = @Email_Body + @TituloEmail + '						</strong></td>
		
		
																
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td align=center><strong> '
        SELECT @Email_Body = @Email_Body + '						¡Eliminación de Registro!</strong></td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td><strong>Apreciado(a):</strong>&nbsp;' + @Usuario + ',</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>' + @TextoEmail + ':
																</td>
																<td>&nbsp;</td>
															</tr>'  
        SELECT @Email_Body = @Email_Body + '				<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td><strong>Activa tu cuenta</strong> '
        SELECT @Email_Body = @Email_Body + '						para empezar a utilizar el sistema, es muy <strong>fácil y rápido</strong>
																</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>'
		SELECT @Email_Body = @Email_Body + '    					<strong>1.- </strong>' + @Paso1 + '
																</td><td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>2.- </strong>' + @Paso2 + '</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>3.- </strong>' +  @Paso3 + '</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>4.- </strong>' +  @Paso4 + '</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr> '
		SELECT @Email_Body = @Email_Body + '					<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
														</table>
													</td>
													<td width=10% bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>Este correo electrónico fue enviado por un sistema automático, favor de no responderlo. </td>
												</tr>
												<tr> '
        SELECT @Email_Body = @Email_Body + '		<td colspan=3 align=center bgcolor=#81BA3D>Si tienes alguna duda, puedes dirigirte a nuestra sección de  '
        SELECT @Email_Body = @Email_Body + '		<a href=http://www.icbf.gov.co/ target=_blank>Asistencia y Soporte.</a>
													</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>&nbsp;</td>
												</tr>
											</table>'
											
		EXEC msdb.dbo.sp_send_dbmail 
			@profile_name = 'GonetMail', --Colocar perfil que se tenga configurado
			@recipients = @Email_Destino, --Destinatario
			@subject = 'Proceso de Eliminar Usuarios',--Asunto
			@body = @Email_Body, --Cuerpo de correo
			@body_format = 'HTML' ; --Formato de correo
						
		
		UPDATE [QOS].[DBO].aspnet_Membership
		SET EnvioCorreo = 1
		WHERE [QOS].[DBO].aspnet_Membership.UserId = @UserId
		
		SET @Index = (@Index + 1)
	END--FIN WHILE(@CantRegistros >= @Index)
	
	
	--Tabla con usuarios a eliminar.
	SELECT IDENTITY(INT,1,1) ID, [QOS].[DBO].aspnet_Membership.*
	INTO #TMP_UsuariosInactivosAEliminar
	FROM [QoS].[dbo].aspnet_UsersInRoles
		INNER JOIN [QoS].[dbo].aspnet_Roles on [QoS].[dbo].aspnet_UsersInRoles.RoleId = [QoS].[dbo].aspnet_Roles.RoleId
		INNER JOIN [QoS].[dbo].aspnet_Users on [QoS].[dbo].aspnet_UsersInRoles.UserId = [QoS].[dbo].aspnet_Users.UserId
			AND [QoS].[dbo].aspnet_Roles.ApplicationId = [QoS].[dbo].aspnet_Roles.ApplicationId
		INNER JOIN [QoS].[dbo].aspnet_Membership on [QoS].[dbo].aspnet_Users.UserId = [QoS].[dbo].aspnet_Membership.UserId
		INNER JOIN @pData datos on [QoS].[dbo].aspnet_UsersInRoles.UserId = datos.UserId
	WHERE [QOS].[DBO].ASPNET_MEMBERSHIP.[ApplicationId] = @ApplicationId
		AND ([QOS].[DBO].aspnet_Membership.IsApproved = 0 OR datos.ExisteTercero = 0)
		AND EnvioCorreo = 1
		AND aspnet_Roles.RoleName = @RoleName
		AND  GETDATE() >= DATEADD(MONTH, @MesesEliminacion, CreateDate)
		
	SELECT @CantRegistros = COUNT(ID) FROM #TMP_UsuariosInactivosAEliminar
	SET @Index = 1
	
	WHILE(@CantRegistros >= @Index)
	BEGIN
		SELECT @Email_Destino = ''
		
		SELECT @Email_Destino = #TMP_UsuariosInactivosAEliminar.Email,
			   @UserId = #TMP_UsuariosInactivosAEliminar.UserId
		FROM #TMP_UsuariosInactivosAEliminar
		WHERE ID = @Index
		
		--BORRA DE USERIN ROLE
		DELETE [QoS].[dbo].[aspnet_UsersInRoles]
		WHERE [UserId] = @UserId

		--BORRA DE MEMBERSHIP
		DELETE [QoS].[dbo].[aspnet_Membership]
		WHERE [UserId] = @UserId


		--BORRA DE USERS
		DELETE [QoS].[dbo].[aspnet_Users]
		WHERE [UserId] = @UserId
		
		--BORRA DE LA BASE LOCAL
		--DELETE [SEG].[Usuario]
		--WHERE [providerKey] = @UserId

		SET @Index = (@Index + 1)
	END--FIN WHILE(@CantRegistros >= @Index)
	
	
	SELECT * FROM #TMP_UsuariosInactivosAEliminar
	
								
	IF OBJECT_ID('#TMP_UsuariosInactivosAEliminar') IS NOT NULL
	BEGIN
		DROP TABLE #TMP_UsuariosInactivosAEliminar
	END
										
	IF OBJECT_ID('#TMP_UsuariosInactivos') IS NOT NULL
	BEGIN
		DROP TABLE #TMP_UsuariosInactivos
	END
	
	
	
	
END--FIN PP





GO



