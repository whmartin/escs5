USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]    Script Date: 08/12/2014 12:04:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]    Script Date: 08/12/2014 12:04:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]
		@IdTipodeentidadPublica INT OUTPUT, 	@CodigoTipodeentidadPublica NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipodeentidadPublica(CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipodeentidadPublica, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipodeentidadPublica = SCOPE_IDENTITY() 		
END


GO


