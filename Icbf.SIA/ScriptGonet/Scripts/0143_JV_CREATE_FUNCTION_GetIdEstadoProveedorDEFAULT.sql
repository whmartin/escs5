USE [SIA]
GO
IF  EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'EntidadProvOferente'
AND [COLUMN_NAME] = 'IdEstadoProveedor'
AND [TABLE_SCHEMA] = 'PROVEEDOR')
BEGIN
	ALTER TABLE [PROVEEDOR].[EntidadProvOferente] DROP CONSTRAINT [IdEstadoProveedorOfAssociate]
END
/****** Object:  UserDefinedFunction [dbo].[GetIdEstadoProveedorDEFAULT]    Script Date: 04/10/2014 11:15:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetIdEstadoProveedorDEFAULT]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetIdEstadoProveedorDEFAULT]
GO

USE [SIA]
GO

/****** Object:  UserDefinedFunction [dbo].[GetIdEstadoProveedorDEFAULT]    Script Date: 04/10/2014 11:15:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 10-Abril-2014
-- Description:	Retorna el ID Default que tendr� el campo IdEstadoProveedor en la tabla
-- PROVEEDOR.EntidadProvOferente. Est� funci�n se ha creado para poderla utilizar en el 
-- Constraint de Default Value del nuevo campor IdEstadoProveedor en la tabla PROVEEDOR.EntidadProvOferente
-- =============================================
CREATE FUNCTION [dbo].[GetIdEstadoProveedorDEFAULT] 
(
)
RETURNS INT
AS
BEGIN
			
		DECLARE @IdEstadoProveedor INT =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='REGISTRADO')
		RETURN 	@IdEstadoProveedor

END
GO



