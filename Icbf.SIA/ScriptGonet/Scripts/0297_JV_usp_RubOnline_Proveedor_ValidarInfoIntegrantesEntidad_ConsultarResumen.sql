USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ConsultarResumen]    Script Date: 07/25/2014 09:21:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ConsultarResumen]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ConsultarResumen]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ConsultarResumen]    Script Date: 07/25/2014 09:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date:  2014/07/25 09:18 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoIntegrantesEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoIntegrantesEntidad, v.IdEntidad, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoIntegrantesEntidad v
			left join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = v.IdEntidad
			where p.IdEntidad  = @IdEntidad
			--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
			ORDER BY v.FechaCrea DESC
END




GO


