USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]    Script Date: 08/12/2014 11:45:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]    Script Date: 08/12/2014 11:45:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jonnathan Ni�o
-- Create date:  7/15/2013 11:30:19 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]
		@IdExpCOD INT OUTPUT, 	@IdTipoCodUNSPSC INT,	@IdExpEntidad INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ExperienciaCodUNSPSCEntidad(IdTipoCodUNSPSC, IdExpEntidad, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoCodUNSPSC, @IdExpEntidad, @UsuarioCrea, GETDATE())
	SELECT @IdExpCOD = SCOPE_IDENTITY() 		
END


GO


