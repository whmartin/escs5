USE [SIA]

/***
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 10-Abril-2014
Descripci�n: Inserta los estados de Proveedor en la Tabla [PROVEEDOR].[EstadoProveedor],
de acuerdo a requerimientos del control de cambios 016, el estado del Tercero se heredar� de
el estado del Proveedor, por lo que se ha cread una columna IdEstadoTercero.
para que es esta misma tabla se busqu� el estado que debe colocarse al Tercero.
**/
IF NOT EXISTS
(SELECT IdEstadoProveedor FROM [PROVEEDOR].[EstadoProveedor]
WHERE Descripcion='REGISTRADO')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoProveedor]
			   ([Descripcion]
			   ,[Estado]
			   ,[IdEstadoTercero]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('REGISTRADO'
			   ,1
			   ,(SELECT IdEstadoTercero FROM Oferente.EstadoTercero
				 WHERE DescripcionEstado='REGISTRADO')
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO


IF NOT EXISTS
(SELECT IdEstadoProveedor FROM [PROVEEDOR].[EstadoProveedor]
WHERE Descripcion='POR VALIDAR')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoProveedor]
			   ([Descripcion]
			   ,[Estado]
			   ,[IdEstadoTercero]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('POR VALIDAR'
			   ,1
			   ,(SELECT IdEstadoTercero FROM Oferente.EstadoTercero
				 WHERE DescripcionEstado='POR VALIDAR')
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO

IF NOT EXISTS
(SELECT IdEstadoProveedor FROM [PROVEEDOR].[EstadoProveedor]
WHERE Descripcion='POR AJUSTE')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoProveedor]
			   ([Descripcion]
			   ,[Estado]
			   ,[IdEstadoTercero]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('POR AJUSTE'
			   ,1
			   ,(SELECT IdEstadoTercero FROM Oferente.EstadoTercero
				 WHERE DescripcionEstado='POR AJUSTAR')
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO


IF NOT EXISTS
(SELECT IdEstadoProveedor FROM [PROVEEDOR].[EstadoProveedor]
WHERE Descripcion='VALIDADO')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoProveedor]
			   ([Descripcion]
			   ,[Estado]
			   ,[IdEstadoTercero]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('VALIDADO'
			   ,1
			   ,(SELECT IdEstadoTercero FROM Oferente.EstadoTercero
				 WHERE DescripcionEstado='VALIDADO')
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO

IF NOT EXISTS
(SELECT IdEstadoProveedor FROM [PROVEEDOR].[EstadoProveedor]
WHERE Descripcion='EN VALIDACI�N')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoProveedor]
			   ([Descripcion]
			   ,[Estado]
			   ,[IdEstadoTercero]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('EN VALIDACI�N'
			   ,1
			   ,(SELECT IdEstadoTercero FROM Oferente.EstadoTercero
				 WHERE DescripcionEstado='EN VALIDACI�N')
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO


IF NOT EXISTS
(SELECT IdEstadoProveedor FROM [PROVEEDOR].[EstadoProveedor]
WHERE Descripcion='PARCIAL')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoProveedor]
			   ([Descripcion]
			   ,[Estado]
			   ,[IdEstadoTercero]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('PARCIAL'
			   ,1
			   ,(SELECT IdEstadoTercero FROM Oferente.EstadoTercero
				 WHERE DescripcionEstado='PARCIAL')
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO


IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PROVEEDOR.ReglasEstadoProveedor_FechaCrea]') AND type = 'D')
BEGIN
ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor]  WITH CHECK ADD  CONSTRAINT [FK_ReglasEstadoProveedor_EstadoProveedor] FOREIGN KEY([IdEstadoProveedor])
REFERENCES [PROVEEDOR].[EstadoProveedor] ([IdEstadoProveedor])

ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] CHECK CONSTRAINT [FK_ReglasEstadoProveedor_EstadoProveedor]
END


IF EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'EntidadProvOferente'
AND [COLUMN_NAME] = 'IdEstadoProveedor'
AND [TABLE_SCHEMA] = 'PROVEEDOR')
BEGIN
ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_Proveedor_EstadoProveedor] FOREIGN KEY([IdEstadoProveedor])
REFERENCES [PROVEEDOR].[EstadoProveedor] ([IdEstadoProveedor])


ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [FK_Proveedor_EstadoProveedor]
END

