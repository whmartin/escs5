USE [SIA]
GO

/****** Object:  Trigger [TR_ProveedorIntegrantes]    Script Date: 08/05/2014 14:57:06 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TR_ProveedorIntegrantes]'))
DROP TRIGGER [PROVEEDOR].[TR_ProveedorIntegrantes]
GO


/****** Object:  Trigger [PROVEEDOR].[TR_ProveedorIntegrantes]    Script Date: 08/05/2014 14:57:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 05/AGO/2014
-- Description:	Determina que estado colocar al m�dulo integrantes cuando se realizan
-- cambios en alguno de los integrantes asociados a una Entidad.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[TR_ProveedorIntegrantes] 
   ON [PROVEEDOR].[Integrantes] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN

DECLARE @contador INT=0
DECLARE @IdEntidad INT
SELECT @contador=COUNT(IdEntidad) FROM deleted
IF (@contador >0)
BEGIN
	SELECT @IdEntidad=IdEntidad FROM deleted	
END
ELSE
BEGIN
	SELECT @IdEntidad=IdEntidad FROM inserted
END
EXEC [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado] @IdEntidad
END

GO


