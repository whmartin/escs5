USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]    Script Date: 03/25/2014 16:47:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]    Script Date: 03/25/2014 16:47:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocDatosBasicoProv
---- Desarrollador Modificacion: Juan Carlos Valverde S�mano
-- Fecha modificaci�n: 25/03/2014
-- Descripci�n: Se agreg� en el WHERE la condici�n de que solo muestre los registros donde el campo Activo es = 1.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdEntidad, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal
 FROM [Proveedor].[DocDatosBasicoProv] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END
 AND Activo=1
END




GO


