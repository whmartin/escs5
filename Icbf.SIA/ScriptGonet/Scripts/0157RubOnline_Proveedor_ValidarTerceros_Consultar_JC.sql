USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]    Script Date: 04/11/2014 11:30:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]    Script Date: 04/11/2014 11:30:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]
	@IdTercero int = NULL,@Observaciones NVARCHAR(200) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarTercero] 
 WHERE IdTercero = CASE WHEN @IdTercero IS NULL THEN IdTercero ELSE @IdTercero END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
 ORDER BY 1 DESC
END




GO


