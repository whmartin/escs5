USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]    Script Date: 04/06/2014 12:53:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]    Script Date: 04/06/2014 12:53:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que guarda un nuevo EntidadProvOferente
--Modificado por: Juan Carlos Valverde Sámano
--Fecha Modificación: 20/03/2014
--Descripción: En base a un requerimiento de Mejors del CO016 se agrega lo siguiente:
--el procedimiento recibe los parametros primer nombre, segundo nombre, primer apeliido, segundo apellido y 
-- correo electrónico para actualizarlos en TERCEROS.(Por si caso hubo alguna modificación al momento de editar
-- los datos basicos del proveedor.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]
		@IdEntidad INT OUTPUT, 	
		@ConsecutivoInterno NVARCHAR(128),
		@TipoEntOfProv BIT=NULL,	
		@IdTercero INT=NULL,	
		@IdTipoCiiuPrincipal INT=NULL,	
		@IdTipoCiiuSecundario INT=NULL,	
		@IdTipoSector INT=NULL,	
		@IdTipoClaseEntidad INT=NULL,	
		@IdTipoRamaPublica INT=NULL,	
		@IdTipoNivelGob INT=NULL,	
		@IdTipoNivelOrganizacional INT=NULL,	
		@IdTipoCertificadorCalidad INT=NULL,	
		@FechaCiiuPrincipal DATETIME=NULL,	
		@FechaCiiuSecundario DATETIME=NULL,	
		@FechaConstitucion DATETIME=NULL,	
		@FechaVigencia DATETIME=NULL,	
		@FechaMatriculaMerc DATETIME=NULL,	
		@FechaExpiracion DATETIME=NULL,	
		@TipoVigencia BIT=NULL,	
		@ExenMatriculaMer BIT=NULL,	
		@MatriculaMercantil NVARCHAR(20)=NULL,	
		@ObserValidador NVARCHAR(256)=NULL,	
		@AnoRegistro INT=NULL,	
		@IdEstado INT=NULL, 
		@UsuarioCrea NVARCHAR(250),
		@IdTemporal VARCHAR(20) = NULL,
		@PrimerNombre NVARCHAR(256) ='',
		@SegundoNombre NVARCHAR(256) ='',
		@PrimerApellido NVARCHAR(256)='',
		@SegundoApellido NVARCHAR(256)='',
		@CorreoElectronico NVARCHAR(256) = ''
AS
BEGIN
	INSERT INTO Proveedor.EntidadProvOferente(TipoEntOfProv, IdTercero, IdTipoCiiuPrincipal, IdTipoCiiuSecundario, IdTipoSector, IdTipoClaseEntidad, IdTipoRamaPublica, IdTipoNivelGob, IdTipoNivelOrganizacional, IdTipoCertificadorCalidad, FechaCiiuPrincipal, FechaCiiuSecundario, FechaConstitucion, FechaVigencia, FechaMatriculaMerc, FechaExpiracion, TipoVigencia, ExenMatriculaMer, MatriculaMercantil, ObserValidador, AnoRegistro, IdEstado, UsuarioCrea, FechaCrea, NroRevision)
					  VALUES(@TipoEntOfProv, @IdTercero, @IdTipoCiiuPrincipal, @IdTipoCiiuSecundario, @IdTipoSector, @IdTipoClaseEntidad, @IdTipoRamaPublica, @IdTipoNivelGob, @IdTipoNivelOrganizacional, @IdTipoCertificadorCalidad, @FechaCiiuPrincipal, @FechaCiiuSecundario, @FechaConstitucion, @FechaVigencia, @FechaMatriculaMerc, @FechaExpiracion, @TipoVigencia, @ExenMatriculaMer, @MatriculaMercantil, @ObserValidador, @AnoRegistro, @IdEstado, @UsuarioCrea, GETDATE(), 1)
	SELECT @IdEntidad = @@IDENTITY 		
	
	UPDATE [Proveedor].[DocDatosBasicoProv] set IdEntidad = @IdEntidad where IdTemporal = @IdTemporal
	
	UPDATE Proveedor.EntidadProvOferente SET ConsecutivoInterno=@ConsecutivoInterno +  RIGHT('00000' + Ltrim(Rtrim(@IdEntidad)),8) WHERE IdEntidad=@IdEntidad
	
	
	UPDATE Oferentes.Oferente.TERCERO
   SET
   PRIMERNOMBRE=@PrimerNombre,
   SEGUNDONOMBRE=@SegundoNombre,
   PRIMERAPELLIDO=@PrimerApellido,
   SEGUNDOAPELLIDO=@SegundoApellido,
   CORREOELECTRONICO=@CorreoElectronico,
   USUARIOMODIFICA=@UsuarioCrea,
   FECHAMODIFICA=GETDATE()
   WHERE IDTERCERO=@IdTercero
   
   
-------Actualizar el Documento que viene con IDTemporal, colocarle el ID del tercero-------
-----Y actualizar la columna activo para el o los documentos anteriores--------------------
	UPDATE [SIA].[Proveedor].[DocAdjuntoTercero]  
	SET Activo=0
	WHERE IDTERCERO=@IdTercero AND
	IDDOCUMENTO IN (SELECT IDDOCUMENTO FROM [SIA].[Proveedor].[DocAdjuntoTercero] WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal
	
	UPDATE [SIA].[Proveedor].[DocAdjuntoTercero]  
	set IdTercero = @IdTercero
	where IdTemporal = @IdTemporal	
	AND Activo=1	

------------------------------------------------------------------------------------------
   
END






GO


