USE [Oferentes]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha Creaci�n: 01/04/2014
Descripci�n: En base a un requerimiento del CC16
se agrega un estado las a la tabla.
**/
GO

IF NOT EXISTS(SeLECT IdEstadoTercero FROM [Oferentes].[Oferente].[EstadoTercero]
WHERE [DescripcionEstado]='PARCIAL')
BEGIN
	INSERT INTO [Oferentes].[Oferente].[EstadoTercero]
			   ([CodigoEstadotercero]
			   ,[DescripcionEstado]
			   ,[Estado]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('006'
			   ,'PARCIAL'
			   ,1
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
			   
END           
GO
