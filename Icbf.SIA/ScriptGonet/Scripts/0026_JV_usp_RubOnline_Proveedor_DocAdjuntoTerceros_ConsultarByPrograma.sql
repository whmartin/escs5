USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]    Script Date: 03/25/2014 16:39:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]    Script Date: 03/25/2014 16:39:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero con su peso y formato
-- Desarrollador Modificacion: Juan Carlos Valverde S�mano
-- Fecha modificaci�n: 25/03/2014
-- Descripci�n: Se agreg� en el WHERE la condici�n de que solo muestre los registros donde el campo Activo es = 1.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]
@IdTercero INT = NULL,
@CodigoDocumento NVARCHAR (20) ,
@Programa NVARCHAR (50)
AS
BEGIN


DECLARE @IdPrograma INT
SET @IdPrograma = (SELECT DISTINCT
	IdPrograma
FROM SEG.Programa
WHERE CodigoPrograma = @Programa)




IF @IdTercero IS NOT NULL
BEGIN
SELECT
	Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	Proveedor.DocAdjuntoTercero.IDDOCADJUNTO,
	Proveedor.DocAdjuntoTercero.IDTERCERO,
	Proveedor.DocAdjuntoTercero.IDDOCUMENTO,
	Proveedor.DocAdjuntoTercero.DESCRIPCION AS Descripcion,
	Proveedor.DocAdjuntoTercero.LINKDOCUMENTO,
	Proveedor.DocAdjuntoTercero.ANNO,
	Proveedor.DocAdjuntoTercero.FECHACREA,
	Proveedor.DocAdjuntoTercero.USUARIOCREA,
	Proveedor.DocAdjuntoTercero.FECHAMODIFICA,
	Proveedor.DocAdjuntoTercero.USUARIOMODIFICA
FROM Proveedor.TipoDocumento
INNER JOIN Proveedor.DocAdjuntoTercero
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.DocAdjuntoTercero.IDDOCUMENTO
RIGHT OUTER JOIN Proveedor.TipoDocumentoPrograma
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.TipoDocumentoPrograma.IdTipoDocumento
WHERE (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma) AND (Proveedor.DocAdjuntoTercero.IDTERCERO = @IdTercero) AND
(Proveedor.TipoDocumento.CodigoTipoDocumento = @CodigoDocumento) AND (Proveedor.DocAdjuntoTercero.Activo = 1)
END
ELSE
BEGIN
SELECT
	Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento,Proveedor.TipoDocumento.CodigoTipoDocumento,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	0 AS IDDOCADJUNTO,
	0 AS IDTERCERO,
	Proveedor.TipoDocumento.IdTipoDocumento AS IDDOCUMENTO,
	'' AS DESCRIPCION,
	'' AS LINKDOCUMENTO,
	0 AS ANNO,
	GETDATE() AS FECHACREA,
	'' AS USUARIOCREA,
	GETDATE() AS FECHAMODIFICA,
	'' AS USUARIOMODIFICA
FROM Proveedor.TipoDocumento
RIGHT OUTER JOIN Proveedor.TipoDocumentoPrograma
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.TipoDocumentoPrograma.IdTipoDocumento
WHERE (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma) AND
(Proveedor.TipoDocumento.CodigoTipoDocumento = @CodigoDocumento)
END

END

GO


