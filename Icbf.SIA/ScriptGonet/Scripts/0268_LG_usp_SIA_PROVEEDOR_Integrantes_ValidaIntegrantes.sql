USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_Integrantes_ValidaIntegrantes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_ValidaIntegrantes]
GO


-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  23/06/2014 10:55 AM
-- Description:	Procedimiento almacenado que valida el numero de integrantes vs el numero de registrados
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_ValidaIntegrantes]
 @IdEntidad INT
 AS
 BEGIN
	SELECT	COUNT(Integrantes.IdIntegrante) AS Integrantes,
			Entidad.NumIntegrantes,
			Tercero.IdTipoPersona,
			SUM(Integrantes.PorcentajeParticipacion) AS PorcentajeParticipacion
	  FROM	PROVEEDOR.Integrantes Integrantes
	  JOIN	PROVEEDOR.EntidadProvOferente Entidad
	  ON	Integrantes.IdEntidad = Entidad.IdEntidad
	  JOIN	OFERENTE.TERCERO Tercero
	  ON	Entidad.IdTercero = Tercero.IDTERCERO
	  WHERE Integrantes.IdEntidad = @IdEntidad
	  GROUP BY Integrantes.IdEntidad, Entidad.NumIntegrantes, Tercero.IdTipoPersona
END