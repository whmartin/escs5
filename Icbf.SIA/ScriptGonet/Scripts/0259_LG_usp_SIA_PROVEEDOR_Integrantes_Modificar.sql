USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantes_Modificar]    Script Date: 06/27/2014 09:35:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_Integrantes_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Modificar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantes_Modificar]    Script Date: 06/27/2014 09:35:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Integrante
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Modificar]
		@IdIntegrante INT, @IdEntidad INT,	@IdTipoPersona INT,	@IdTipoIdentificacionPersonaNatural INT,	@NumeroIdentificacion NVARCHAR(50),	@PorcentajeParticipacion NUMERIC(5,2),	@ConfirmaCertificado NVARCHAR(5),	@ConfirmaPersona NVARCHAR(5), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	IF EXISTS(SELECT * FROM PROVEEDOR.Integrantes WHERE NumeroIdentificacion=@NumeroIdentificacion AND IdIntegrante <> @IdIntegrante)
	BEGIN
		RAISERROR('Ya existe un registro con este(a) Número de Identificación',16,1)
	    RETURN
	END
	UPDATE PROVEEDOR.Integrantes SET IdTipoPersona = @IdTipoPersona, IdEntidad = @IdEntidad, IdTipoIdentificacionPersonaNatural = @IdTipoIdentificacionPersonaNatural, NumeroIdentificacion = @NumeroIdentificacion, PorcentajeParticipacion = @PorcentajeParticipacion, ConfirmaCertificado = @ConfirmaCertificado, ConfirmaPersona = @ConfirmaPersona, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdIntegrante = @IdIntegrante
END

GO


