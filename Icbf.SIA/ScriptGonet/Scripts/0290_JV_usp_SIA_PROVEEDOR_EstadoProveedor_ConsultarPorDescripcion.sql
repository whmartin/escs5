USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_EstadoProveedor_ConsultarPorDescripcion]    Script Date: 07/09/2014 09:32:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_EstadoProveedor_ConsultarPorDescripcion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_EstadoProveedor_ConsultarPorDescripcion]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_EstadoProveedor_ConsultarPorDescripcion]    Script Date: 07/09/2014 09:32:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date:  09/07/2014
-- Description:	Procedimiento almacenado que consulta un
-- EstadoProveedor a partir de su descripci�n.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_EstadoProveedor_ConsultarPorDescripcion]
	@descripcionEstado NVARCHAR(50)
AS
BEGIN
SELECT IdEstadoProveedor, Descripcion, Estado FROM PROVEEDOR.EstadoProveedor
WHERE Descripcion=@descripcionEstado
END


GO


