USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]    Script Date: 08/12/2014 11:36:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]    Script Date: 08/12/2014 11:36:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdExpEntidad INT = NULL,	@IdTipoDocumento INT,	
		@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256), @UsuarioCrea NVARCHAR(250),
		@IdTemporal NVARCHAR(50)
AS
BEGIN
	INSERT INTO Proveedor.DocExperienciaEntidad(IdExpEntidad, IdTipoDocumento, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdExpEntidad, @IdTipoDocumento, @Descripcion, @LinkDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = SCOPE_IDENTITY() 		
END


GO


