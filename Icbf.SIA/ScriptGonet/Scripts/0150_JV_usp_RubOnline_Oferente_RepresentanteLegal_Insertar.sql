USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]    Script Date: 04/11/2014 10:14:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]
GO

USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]    Script Date: 04/11/2014 10:14:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada 
-- Create date:  6/22/2013 5:33:48 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Insertar]
@IdTercero INT OUTPUT,
@IdDListaTipoDocumento INT , 
@NumeroIdentificacion NVARCHAR (255), 
@PrimerNombre NVARCHAR (255)= NULL, 
@IdTipoPersona INT=NULL,
@SegundoNombre NVARCHAR (255)= NULL, 
@PrimerApellido NVARCHAR (255)= NULL, 
@SegundoApellido NVARCHAR (255)= NULL, 
@RazonSocial NVARCHAR (255)= NULL,
@Digitoverificacion INT= NULL,
@Email NVARCHAR (255) = NULL, 
@Sexo NVARCHAR (1)= NULL, 
@FechaExpedicionId DATETIME =null, 
@FechaNacimiento DATETIME =null, 
@UsuarioCrea NVARCHAR (250),
@CreadoPorInterno BIT= NULL
AS
BEGIN
INSERT INTO Oferente.Tercero 
        ([IDTIPODOCIDENTIFICA],
         NumeroIdentificacion, 
         PrimerNombre, 
         SegundoNombre, 
         PrimerApellido, 
         SegundoApellido, 
         RAZONSOCIAL,
         DIGITOVERIFICACION,
         IdTipoPersona,
         CORREOELECTRONICO, 
         Sexo, 
         FechaExpedicionId, 
         FechaNacimiento,
         UsuarioCrea,
         FechaCrea,
         CreadoPorInterno)
	VALUES (@IdDListaTipoDocumento,
	        @NumeroIdentificacion, 
	        @PrimerNombre, 
	        @SegundoNombre, 
	        @PrimerApellido, 
	        @SegundoApellido, 
	        @RazonSocial,
	        @Digitoverificacion,
	        @IdTipoPersona,
	        @Email, 
	        @Sexo, 
	        @FechaExpedicionId, 
	        @FechaNacimiento, 
	        @UsuarioCrea, GETDATE(),
	        @CreadoPorInterno)

SELECT 	@IdTercero = @@IDENTITY
RETURN @@IDENTITY
END
GO


