USE [SIA]
GO

/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripcion: Creación de tipo y procedimientos almacenados para servicio de integración de usuarios en la BD SIA
*/

/****** Object:  StoredProcedure [dbo].[Table_Proveedores_IntegracionUsuarios]    Script Date: 07/22/2013 20:44:10 ******/
IF EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'Table_Proveedores_IntegracionUsuarios')
DROP type [dbo].[Table_Proveedores_IntegracionUsuarios]
GO


/****** Object:  StoredProcedure [dbo].[Table_Proveedores_IntegracionUsuarios]    Script Date: 07/22/2013 20:44:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ==========================================================================================
-- Author:		 Carlos Cubillos
-- Create date:  22/07/2013
-- Description:	 Tipo para uso de procedimientos de integración
-- ==========================================================================================
CREATE TYPE [dbo].[Table_Proveedores_IntegracionUsuarios] AS TABLE(
      [UserId] [nvarchar](125) NOT NULL,
      [PrimerNombre] [nvarchar](150) NULL,
      [SegundoNombre] [nvarchar](150) NULL,
      [PrimerApellido] [nvarchar](150) NULL,
      [SegundoApellido] [nvarchar](150) NULL,
      [RazonSocial] [nvarchar](150) NULL,
      [ExisteTercero] [Bit] NULL,
      [IsApproved] [Bit] NULL
)


GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_UsuariosInactivos]    Script Date: 07/22/2013 20:44:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QoS_Proveedor_UsuariosInactivos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]
GO


/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_UsuariosInactivos]    Script Date: 07/22/2013 20:44:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- ==========================================================================================
-- Author:		 José Ignacio De Los Reyes
-- Create date:  14/06/2013 11:38:19 AM
-- Description:	 Se Crea para enivar correo de suspension de usuario si su estado es inactivo
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]

@UserId NVARCHAR(256)

AS
BEGIN

	DELETE [SEG].[Usuario]
	WHERE [providerKey] = @UserId
	
END--FIN PP


GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]    Script Date: 07/22/2013 20:30:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]    Script Date: 07/22/2013 20:30:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:		 Carlos Felipe Cubillos
-- Description:	 Obtiene información de la BD Proveedores para usuarios
-- ==========================================================================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_IntegrarUsuarios]

@pData Table_Proveedores_IntegracionUsuarios readonly

AS
BEGIN

	SELECT 
		datos.UserId,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=1 ) THEN SEG.Usuario.PrimerNombre
			ELSE null
			END AS PrimerNombre,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=1 ) THEN SEG.Usuario.SegundoNombre
			ELSE null
			END AS SegundoNombre,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=1 ) THEN SEG.Usuario.PrimerApellido
			ELSE null
			END AS PrimerApellido,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=1 ) THEN SEG.Usuario.SegundoApellido
			ELSE null
			END AS SegundoApellido,
		CASE
			WHEN ((Oferente.TERCERO.IDTERCERO is null OR datos.IsApproved=0) AND SEG.Usuario.IdTipoPersona=2 ) THEN SEG.Usuario.RazonSocial
			ELSE null
			END AS RazonSocial,
		CASE
			WHEN Oferente.TERCERO.IDTERCERO is null THEN 0
			ELSE 1
			END AS ExisteTercero,
		datos.IsApproved
	FROM @pData datos
	JOIN SEG.Usuario on datos.UserId = SEG.Usuario.providerKey
	LEFT JOIN Oferente.TERCERO on datos.UserId = Oferente.TERCERO.ProviderUserKey
	
END
GO


