USE [SIA]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 23-Abril-2013
Descripci�n: Se agrega el campo ObligEntExtranjera a la tabla PROVEEDOR.TipoDocumentoPrograma
**/

IF NOT EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'TipoDocumentoPrograma'
AND [COLUMN_NAME] = 'ObligEntExtranjera'
AND [TABLE_SCHEMA] = 'PROVEEDOR')
BEGIN
	ALTER TABLE [PROVEEDOR].[TipoDocumentoPrograma]
	ADD [ObligEntExtranjera] BIT NOT NULL DEFAULT (0)
END