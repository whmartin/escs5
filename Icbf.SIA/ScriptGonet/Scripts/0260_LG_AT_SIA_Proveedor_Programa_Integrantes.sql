USE [SIA]
GO
/***********************************************
Creado Por: Leticia Elizabeth Gonz�lez Flores
El dia : 2014-06-16
Permite : Crear el programa Integrantes.
***********************************************/
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Proveedor/Integrantes' )
begin
      	 
      insert INTO [SEG].[Programa]([IdModulo],
									[NombrePrograma],
									[CodigoPrograma],
									[Posicion],
									[Estado],
									[UsuarioCreacion],
									[FechaCreacion],
									[UsuarioModificacion],
									[FechaModificacion],
									[VisibleMenu],
									[generaLog],
									[Padre])
      SELECT IdModulo,'Integrantes','Proveedor/Integrantes',1,1,'Administrador',GETDATE(),NULL,NULL,0,1,NULL
      from SEG.Modulo
      where NombreModulo = 'Proveedores'    
      
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
