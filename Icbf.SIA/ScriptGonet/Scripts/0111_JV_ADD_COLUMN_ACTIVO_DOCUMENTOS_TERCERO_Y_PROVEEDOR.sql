/*
Desarrollador: Juan Carlos Valverde S�mano
Fecha Creaci�n: 25-Marzo-2014
Descripci�n:Se agrega el campo Activo tipo BIT a las Tablas
PROVEEDOR.DocAdjuntoTercero y PROVEEDOR.DocDatosBasicoProv
Con el fin de llevar un historial de los Documentos adjuntos
y no eliminar o actualizar el registro.
Esto en base a un requerimiento del Control de cambio 016.
Siempre habr� un solo registro Activo=1 para cada tipo de Documento, 
para cada Tercero y/o proveedor.
*/
USE [SIA]
GO


-----------------------Elimina los Constraints de los DEFAULT en caso de que ya existan----------------------
	DECLARE @sql NVARCHAR(MAX)='0'

    SELECT TOP 1 @sql ='alter table PROVEEDOR.DocAdjuntoTercero drop constraint ['+dc.NAME+N']'
    from sys.default_constraints dc
    JOIN sys.columns c
        ON c.default_object_id = dc.object_id
    WHERE 
        dc.parent_object_id = OBJECT_ID('PROVEEDOR.DocAdjuntoTercero')
    AND c.name = N'Activo'
        
    IF(@sql!='0')
    BEGIN
		EXEC (@sql)
    END

	SET @sql='0'

    SELECT TOP 1 @sql ='alter table PROVEEDOR.DocDatosBasicoProv drop constraint ['+dc.NAME+N']'
    from sys.default_constraints dc
    JOIN sys.columns c
        ON c.default_object_id = dc.object_id
    WHERE 
        dc.parent_object_id = OBJECT_ID('PROVEEDOR.DocDatosBasicoProv')
    AND c.name = N'Activo'
        
    IF(@sql!='0')
    BEGIN
		EXEC (@sql)
    END
    
---------------------------------------------------------------------------------------------------
---------------------------------------------elimina las columnas Activo-----------------------------------
IF  EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'DocAdjuntoTercero' and COLUMN_NAME = 'Activo'
)
BEGIN
	ALTER TABLE PROVEEDOR.DocAdjuntoTercero
	DROP COLUMN Activo
END

IF EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'DocDatosBasicoProv' and COLUMN_NAME = 'Activo'
)
BEGIN
	ALTER TABLE PROVEEDOR.DocDatosBasicoProv
	DROP COLUMN Activo
END

--------------------------------------------------------------------------------------------------
---------------------------------------------------VUELVE A AGREGAR LAS COLUMNAS COLOCANDO AHORA COMO DEFAULT FALSE � '0'-----------------
IF NOT EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'DocAdjuntoTercero' and COLUMN_NAME = 'Activo'
)
BEGIN
	ALTER TABLE PROVEEDOR.DocAdjuntoTercero
	ADD Activo bit NOT NULL DEFAULT 1
END

IF NOT EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'DocDatosBasicoProv' and COLUMN_NAME = 'Activo'
)
BEGIN
	ALTER TABLE PROVEEDOR.DocDatosBasicoProv
	ADD Activo bit NOT NULL DEFAULT 1
END




