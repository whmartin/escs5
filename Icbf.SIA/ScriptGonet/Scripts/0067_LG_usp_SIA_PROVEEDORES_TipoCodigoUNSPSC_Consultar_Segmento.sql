USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Segmento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Segmento]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  02/26/2014 1:56:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) Segmento
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Segmento]

AS
BEGIN
 SELECT DISTINCT(CodigoSegmento),
		NombreSegmento
  FROM	PROVEEDOR.TipoCodigoUNSPSC
  WHERE CodigoSegmento IS NOT NULL
END