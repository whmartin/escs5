USE QoS
GO
/*
Fecha Creaci�n: 29/10/2013
Usuario Creaci�n: Carlos Cubillos
Descripci�n: Creaci�n de los datos de la QoS
*/
IF NOT EXISTS (SELECT applicationName
				FROM aspnet_Applications
				WHERE applicationName = 'SIA')
BEGIN
	INSERT INTO [aspnet_Applications] 
			   ([ApplicationName]
			   ,[LoweredApplicationName]
			   ,[Description])
		 VALUES
			   ('SIA'
			   ,'SIA'
			   ,'Sistema de SIA')
END
GO

DECLARE @ApplicationID  uniqueidentifier



SELECT @ApplicationID = ApplicationID
FROM aspnet_Applications
WHERE applicationName = 'SIA'

IF NOT EXISTS(SELECT ApplicationId
				FROM aspnet_Users
				WHERE ApplicationId = @ApplicationID
				AND UserName = 'Administrador')
BEGIN
	INSERT INTO [dbo].[aspnet_Users]
			   ([ApplicationId]
			   ,[UserName]
			   ,[LoweredUserName]
			   ,[MobileAlias]
			   ,[IsAnonymous]
			   ,[LastActivityDate])
		 VALUES
			   (@ApplicationID
			   ,'Administrador'
			   ,'Administrador'
			   ,'Administrador'
			   ,0
			   ,GETDATE())
END
GO

DECLARE @ApplicationID  uniqueidentifier



SELECT @ApplicationID = ApplicationID
FROM aspnet_Applications
WHERE applicationName = 'SIA'




IF NOT EXISTS(SELECT ApplicationId
				FROM aspnet_Roles
				WHERE ApplicationId = @ApplicationID
				AND RoleName = 'AdministradorSIA')
BEGIN
	INSERT INTO [dbo].[aspnet_Roles]
			   ([ApplicationId]
			   ,[RoleName]
			   ,[LoweredRoleName]
			   ,[Description]
			   ,[Url])
		 VALUES
			   (@ApplicationID
			   ,'AdministradorSIA'
			   ,'AdministradorSIA'
			   ,'AdministradorSIA'
			   ,NULL)
END
GO


DECLARE @ApplicationID  uniqueidentifier



SELECT @ApplicationID = ApplicationID
FROM aspnet_Applications
WHERE applicationName = 'SIA'




IF NOT EXISTS(SELECT ApplicationId
				FROM aspnet_Roles
				WHERE ApplicationId = @ApplicationID
				AND RoleName = 'PROVEEDORES')
BEGIN
	INSERT INTO [dbo].[aspnet_Roles]
			   ([ApplicationId]
			   ,[RoleName]
			   ,[LoweredRoleName]
			   ,[Description]
			   ,[Url])
		 VALUES
			   (@ApplicationID
			   ,'PROVEEDORES'
			   ,'PROVEEDORES'
			   ,'PROVEEDORES web'
			   ,NULL)
END
GO


DECLARE @ApplicationID  uniqueidentifier



SELECT @ApplicationID = ApplicationID
FROM aspnet_Applications
WHERE applicationName = 'SIA'

IF NOT EXISTS(SELECT ApplicationId
				FROM aspnet_Roles
				WHERE ApplicationId = @ApplicationID
				AND RoleName = 'GESTOR PROVEEDOR')
BEGIN
	INSERT INTO [dbo].[aspnet_Roles]
			   ([ApplicationId]
			   ,[RoleName]
			   ,[LoweredRoleName]
			   ,[Description]
			   ,[Url])
		 VALUES
			   (@ApplicationID
			   ,'GESTOR PROVEEDOR'
			   ,'GESTOR PROVEEDOR'
			   ,'USUARIO INTERNO GESTOR  PROVEEDOR'
			   ,NULL)
END
GO


DECLARE @ApplicationID  uniqueidentifier



SELECT @ApplicationID = ApplicationID
FROM aspnet_Applications
WHERE applicationName = 'SIA'


DECLARE @UserId  uniqueidentifier


SELECT @UserId = UserId
FROM aspnet_Users
WHERE ApplicationID = @ApplicationID
AND UserName = 'Administrador'

IF NOT EXISTS (SELECT ApplicationId
				FROM aspnet_Membership
				WHERE ApplicationId = @ApplicationId
				AND UserId = @UserId)
BEGIN				
INSERT INTO [dbo].[aspnet_Membership]
           ([ApplicationId]
           ,[UserId]
           ,[Password]
           ,[PasswordFormat]
           ,[PasswordSalt]
           ,[MobilePIN]
           ,[Email]
           ,[LoweredEmail]
           ,[PasswordQuestion]
           ,[PasswordAnswer]
           ,[IsApproved]
           ,[IsLockedOut]
           ,[CreateDate]
           ,[LastLoginDate]
           ,[LastPasswordChangedDate]
           ,[LastLockoutDate]
           ,[FailedPasswordAttemptCount]
           ,[FailedPasswordAttemptWindowStart]
           ,[FailedPasswordAnswerAttemptCount]
           ,[FailedPasswordAnswerAttemptWindowStart]
           ,[Comment])
     VALUES
           (@ApplicationID
           ,@UserId
           ,'123123$'
           ,0
           ,'YQ7g+OvekgorPZXwlviXyA=='
           ,NULL
           ,'Admin@admin.com'
           ,'Admin@admin.com'
           ,NULL
           ,NULL
           ,1
           ,0
           ,GETDATE()
           ,GETDATE()
           ,GETDATE()
           ,GETDATE()
           ,0
           ,GETDATE()
           ,0
           ,GETDATE()
           ,NULL)
END
GO



DECLARE @ApplicationID  uniqueidentifier



SELECT @ApplicationID = ApplicationID
FROM aspnet_Applications
WHERE applicationName = 'SIA'

DECLARE @RoleID  uniqueidentifier

SELECT @RoleID = RoleId
FROM aspnet_roles
WHERE ApplicationID = @ApplicationID
AND RoleName = 'AdministradorSIA'

DECLARE @UserId  uniqueidentifier


SELECT @UserId = UserId
FROM aspnet_Users
WHERE ApplicationID = @ApplicationID
AND UserName = 'Administrador'

IF NOT EXISTS(SELECT 1
				FROM aspnet_UsersInRoles
				WHERE UserId = @UserId
				AND RoleId = @RoleID)
BEGIN

INSERT INTO [QoS].[dbo].[aspnet_UsersInRoles]
			([UserId]
			,[RoleId])
		VALUES
			(@UserId
			,@RoleID)

END
GO






