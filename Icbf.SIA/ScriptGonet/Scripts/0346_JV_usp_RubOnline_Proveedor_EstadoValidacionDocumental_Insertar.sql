USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]    Script Date: 08/12/2014 11:44:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]    Script Date: 08/12/2014 11:44:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]
		@IdEstadoValidacionDocumental INT OUTPUT, 	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoValidacionDocumental(CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoEstadoValidacionDocumental, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoValidacionDocumental = SCOPE_IDENTITY() 		
END


GO


