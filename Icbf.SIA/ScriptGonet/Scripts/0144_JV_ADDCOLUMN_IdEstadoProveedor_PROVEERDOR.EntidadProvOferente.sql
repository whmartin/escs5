USE [SIA]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 10-Abril-2014
Descripci�n: En base a un requerimiento del CO016, |Se agrega la columna IdEstadoProveedor a la tabla
PROVEDOR.EntidadProvOferente con valor por dafult que retorna la function dbo.GetIdEstadoProveedorDEFAULT()
**/

IF NOT EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'EntidadProvOferente'
AND [COLUMN_NAME] = 'IdEstadoProveedor'
AND [TABLE_SCHEMA] = 'PROVEEDOR')
BEGIN
	
	ALTER TABLE [PROVEEDOR].[EntidadProvOferente]
	ADD [IdEstadoProveedor] INT NOT NULL 
	CONSTRAINT [FK_Proveedor_EstadoProveedor] FOREIGN KEY ([IdEstadoProveedor]) REFERENCES [PROVEEDOR].[EstadoProveedor]
	CONSTRAINT [IdEstadoProveedorOfAssociate] DEFAULT (dbo.GetIdEstadoProveedorDEFAULT())
END
ELSE
BEGIN
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] ADD  CONSTRAINT [IdEstadoProveedorOfAssociate]  DEFAULT ([dbo].[GetIdEstadoProveedorDEFAULT]()) FOR [IdEstadoProveedor]
END

