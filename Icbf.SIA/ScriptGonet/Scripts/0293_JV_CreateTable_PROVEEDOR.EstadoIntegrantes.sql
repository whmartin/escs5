USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 16-Jul-2014
Descripci�n: Crea la Entidad PROVEEDOR.EstadoIntegrantes
que contendr� los estados de validaci�n para el m�dulo Integrantes.
**/
/****** Object:  Table [PROVEEDOR].[EstadoIntegrantes]    Script Date: 07/16/2014 16:59:58 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[IdEstadoIntegrantes_EstadoIntegrantes]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[ValidacionIntegrantesEntidad]'))
ALTER TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad] DROP CONSTRAINT [IdEstadoIntegrantes_EstadoIntegrantes]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[EstadoIntegrantes]') AND type in (N'U'))
DROP TABLE [PROVEEDOR].[EstadoIntegrantes]
GO

/****** Object:  Table [PROVEEDOR].[EstadoIntegrantes]    Script Date: 07/16/2014 17:00:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [PROVEEDOR].[EstadoIntegrantes](
	[IdEstadoIntegrantes] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoIntegrantes] PRIMARY KEY CLUSTERED 
(
	[IdEstadoIntegrantes] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key {0} Column' , @level0type=N'SCHEMA',@level0name=N'PROVEEDOR', @level1type=N'TABLE',@level1name=N'EstadoIntegrantes', @level2type=N'COLUMN',@level2name=N'IdEstadoIntegrantes'
GO


