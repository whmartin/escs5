USE [SIA]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha Creaci�n: 01-Abril-2014
Descripci�n: en base a un requerimiento del CC016, se realiza un reset de este cat�logo 
llevar un mejor orden, y pasar a completar el requerimiento de la relaci�n entre las
tablas RamaoEstructura-Niveldegobierno-NivelOrganizacional-TipodeentidadPublica
**/
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_InfoAdminEntidad_TipodeentidadPublica]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[InfoAdminEntidad]'))
ALTER TABLE [PROVEEDOR].[InfoAdminEntidad] DROP CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica]
GO
TRUNCATE TABLE [PROVEEDOR].[TipodeentidadPublica]
GO


INSERT INTO [PROVEEDOR].[TipodeentidadPublica]
           ([CodigoTipodeentidadPublica]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('001','DEPARTAMENTO ADMINISTRATIVO',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[TipodeentidadPublica]
           ([CodigoTipodeentidadPublica]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('002','UNIDAD ADMINISTRATIVA ESPECIAL',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[TipodeentidadPublica]
           ([CodigoTipodeentidadPublica]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('003','INDUSTRIAL Y COMERCIAL DEL ESTADO',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[TipodeentidadPublica]
           ([CodigoTipodeentidadPublica]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('004','ESTABLECIMIENTO PUBLICO',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[TipodeentidadPublica]
           ([CodigoTipodeentidadPublica]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('005','EMPRESA SOCIAL DEL ESTADO',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[TipodeentidadPublica]
           ([CodigoTipodeentidadPublica]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('006','AGENCIAS DEL ESTADO',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[TipodeentidadPublica]
           ([CodigoTipodeentidadPublica]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('007','NO APLICA',1,'Administrador',GETDATE(),NULL,NULL) 



ALTER TABLE [PROVEEDOR].[InfoAdminEntidad]  WITH CHECK ADD  CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica] FOREIGN KEY([IdTipoEntidadPublica])
REFERENCES [PROVEEDOR].[TipodeentidadPublica] ([IdTipodeentidadPublica])
GO

ALTER TABLE [PROVEEDOR].[InfoAdminEntidad] CHECK CONSTRAINT [FK_InfoAdminEntidad_TipodeentidadPublica]
GO




