USE [SIA]
GO
/**
Autor: Juan Carlos Valverde Sámano
Fecha:20/AGO/2014
Descripción: Script que agrega el registro de validación para aquellos Consorcios
o Uniones Temporales que aún no lo tienen.
**/
DECLARE @TBL AS TABLE(Id int identity, IdEntidad INT)
DECLARE @Contador INT
DECLARE @Regs INT
DECLARE @IdEntidad INT
INSERT @TBL
SELECT DISTINCT(IdEntidad) FROM PROVEEDOR.Integrantes
WHERE IdEntidad NOT IN(
SELECT IdEntidad FROM PROVEEDOR.ValidacionIntegrantesEntidad)

SET @Regs = (SELECT COUNT(IdEntidad) FROM @TBL)
SET @Contador = 1 

WHILE @Contador <= @Regs
BEGIN
    SELECT @IdEntidad= t.IdEntidad
    FROM @TBL t
    WHERE t.Id=@Contador
		INSERT INTO PROVEEDOR.ValidacionIntegrantesEntidad
		(IdEntidad,IdEstadoValidacionIntegrantes, NroRevision, Finalizado, FechaCrea, UsuarioCrea,CorreoEnviado)
		VALUES(@IdEntidad, 1, 1, 0, GETDATE(), 'Administrador', 0)
    SET @Contador = @Contador + 1
 
END


