USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]    Script Date: 04/09/2014 09:58:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]    Script Date: 04/09/2014 09:58:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
--Modificado por: Juan Carlos Valverde S�mano
--Fecha: 31/03/2014
--Descripci�n: Se agreg� el parametro  @IdTipoPersona, ya que el control de cambio 016
--requiere filtrar el contenido de la consulta de acuerdo al tipPersona, se agreg� tambi�n
-- el parametro @IdTipoSector para filtrar.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]
	@IdTipodeActividad INT = NULL,@IdTipoEntidad INT = NULL, @IdTipoPersona INT=NULL, @IdTipoSector INT = NULL
AS
BEGIN
IF(@IdTipoPersona=1)
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[ClasedeEntidad] 
 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
	   AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
	   AND Estado = 1 AND(Descripcion='PROFESIONALES LIBERALES' OR Descripcion='COMERCIANTE INDEPENDIENTE')
	   ORDER BY Descripcion ASC
END
ELSE IF(@IdTipoPersona =2)
BEGIN
 IF(@IdTipoSector=1 AND @IdTipoEntidad=1 AND @IdTipodeActividad=1)
 BEGIN
  SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
  FROM [Proveedor].[ClasedeEntidad] 
  WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
 	    AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
	    AND Estado = 1 AND(Descripcion!='PROFESIONALES LIBERALES' AND Descripcion!='COMERCIANTE INDEPENDIENTE')
	    ORDER BY Descripcion ASC
 END
 ELSE IF(@IdTipoSector=1 AND @IdTipoEntidad=1 AND @IdTipodeActividad=2)
 BEGIN
  SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
  FROM [Proveedor].[ClasedeEntidad] 
  WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
 	    AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
	    AND Estado = 1 
	    ORDER BY Descripcion ASC
 END
END
ELSE 
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[ClasedeEntidad] 
 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
	   AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
	   AND Estado = 1
	   ORDER BY Descripcion ASC
END
END




GO


