USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 08/25/2014 14:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 08/25/2014 14:05:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--Autor:Mauricio Martinez    
--Fecha:2013/07/15 16:00    
--Descripcion: Consulta para mostrar un resumen de los modulos validados   
--Modificado Por: juan Carlos Valverde S�mano
--Fecha:2014/05/14 13:07
--Descripci�n: Se agreg� retornar Liberar=1 para que el boton de Liberar pueda mostrare
-- en la parte de Validar Proveedor; esto Cuando, El estado del Modulo es en Validaci�n y no hay
-- registros en su entidad de Validaci�n(por ejemplo: Proveedor.ValidarInfoDatosBasicosEntidad)
-- Ya que esto no se tomaba en cuenta, pasaba que entraban registros en Validaci�n y no pod�an liberarse.
--Modificado Por: juan Carlos Valverde S�mano
--Fecha:2014/05/16 13:07
--Descripci�n: Se agrego la parte para que cuando Financiera o Experiencia tenga  alguno de sus registros "EN VALIDACI�N"
-- Retorne Liberar=1 para que en el formulario se muestre el Bot�n.
--Modificado:Juan Carlos Valverde S�mano
--Fecha: 28-JUL-2014
--Descripci�n: Se cambi� por completo la manera en que se obtenia el resumen de los m�dulos
--Financiera y Experiencia ya que estaba incorrecto. Solo estaba tomando en cuenta al �ltimo
--registro validado, es decir al registro que habia tenido la ultima revisi�n.
--Modificado por Juan Carlos Valverde S�mano
--Fecha: 25/AGO/2014
--Descripci�n: Se modific� la forma en que se toma el resumen del m�dulo Datos Basicos, 
--como parte del ajuste de colocar NULL a confirma y aprueba cuando se edita un registro ya validado.
------------------------------------------------------------------------------------------
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    
@IdEntidad INT     
AS    
BEGIN    
   DECLARE @IdEstadoEnValidacionFin_Exp INT;
   DECLARE @IdTipoPersona INT=
   (SELECT IdTipoPersona FROM Oferente.TERCERO
   WHERE IDTERCERO=
   (SELECT IdTercero FROM PROVEEDOR.EntidadProvOferente
   WHERE IdEntidad=@IdEntidad))

  
--======================================================    
--Se obtiene el Nro de la Ultima Revision Datos Basicos    
--======================================================    
    
declare @UltimaRevisionDatosBasicos int     
set @UltimaRevisionDatosBasicos = (select Max(v.NroRevision) from  Proveedor.EntidadProvOferente e left join Proveedor.ValidarInfoDatosBasicosEntidad v on v.IdEntidad = e.IdEntidad where e.IdEntidad = @IdEntidad)    

    
--=========================================================================================================    
--Se crea tabla temp    
--=========================================================================================================    
IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'Proveedor' 
                 AND  TABLE_NAME = 'Resumen'))
                 BEGIN
DROP TABLE Proveedor.Resumen
END
create table Proveedor.Resumen (Orden int ,IdEntidad int, NroRevision int, Componente varchar(100), iConfirmaYAprueba int, Finalizado BIT, liberar BIT, iCorreoEnviado int)    
--=========================================================================================================    
--Se obtiene el resumen de Datos Basicos
--=========================================================================================================    

IF EXISTS(SELECT IdValidarInfoDatosBasicosEntidad FROM 
Proveedor.ValidarInfoDatosBasicosEntidad
WHERE IdEntidad=@IdEntidad)    
 BEGIN   
 insert into Proveedor.Resumen  
   select TOP(1) 0 as Orden,     
     e.IdEntidad,     
     ISNULL(e.NroRevision,1) as NroRevision,    
     'Datos B�sicos' as Componente,    
     case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END AS iConfirmaYAprueba,    
     CAST(CAST(ISNULL(e.Finalizado,0) as int) as bit) as Finalizado,    
     CASE WHEN ISNULL(e.IdEstado,0) = 5 THEN 1 ELSE 0 END as Liberar,
     case when v.CorreoEnviado IS NULL THEN -1 ELSE v.CorreoEnviado END AS iCorreoEnviado
   from  Proveedor.EntidadProvOferente e    
    inner join Proveedor.ValidarInfoDatosBasicosEntidad v    
     on v.IdEntidad = e.IdEntidad and v.NroRevision = @UltimaRevisionDatosBasicos  
   where     
     e.IdEntidad = @IdEntidad 
   Order By v.IdValidarInfoDatosBasicosEntidad DESC
 END    
 ELSE    
 BEGIN
 DECLARE @IdEstado INT=(SELECT IdEstado FROM Proveedor.EntidadProvOferente
 WHERE IdEntidad=@IdEntidad)
 insert into Proveedor.Resumen  
   select  0 as Orden,     
     @IdEntidad as IdEntidad,      
     1 as NroRevision,    
     'Datos B�sicos' as Componente,    
     -1 AS iConfirmaYAprueba,    
     0 as Finalizado,    
     CASE WHEN @IdEstado = 5 THEN 1 ELSE 0 END as Liberar,
     -1 as iCorreoEnviado       
 END     

IF(@IdTipoPersona IN (1,2))
BEGIN    
	--=========================================================================================================    
	--Se obtiene el resumen para financiera    
	--=========================================================================================================    
	--------------------------------------FINANCIERA-----------------------------
	DECLARE @tblTempInfoFin AS TABLE(
	IdInfoFin INT, NroRev INT)
	INSERT INTO @tblTempInfoFin
	SELECT 
	InfoFin.IdInfoFin
	,MAX(InfoVal.NroRevision)
	FROM 
	PROVEEDOR.InfoFinancieraEntidad InfoFin
	LEFT JOIN PROVEEDOR.ValidarInfoFinancieraEntidad InfoVal
	ON InfoFin.IdInfoFin=InfoVal.IdInfoFin
	WHERE InfoFin.IdEntidad=@IdEntidad
	GROUP BY (InfoFin.IdInfoFin)


	DECLARE @TBL AS TABLE(
	IdValidarInfoFinancieraEntidad	INT
	,NroRevision	INT
	,IdInfoFin	INT
	,ConfirmaYAprueba INT	
	,EstadoValidacion	INT
	,Finalizado	INT
	,CorreoEnviado INT)
	INSERT INTO @TBL
	SELECT 
	VALIDARFIN.IdValidarInfoFinancieraEntidad
	,ISNULL(FINANCIERA.NroRevision,1)as NroRevision
	,TEMP.IdInfoFin
	,VALIDARFIN.ConfirmaYAprueba
	,(SELECT EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad WHERE IdInfoFin=TEMP.IdInfoFin) As EstadoValidacion
	,(SELECT Finalizado FROM PROVEEDOR.InfoFinancieraEntidad WHERE IdInfoFin=TEMP.IdInfoFin) As Finalizado
	,VALIDARFIN.CorreoEnviado
	FROM PROVEEDOR.InfoFinancieraEntidad FINANCIERA
	INNER JOIN PROVEEDOR.ValidarInfoFinancieraEntidad VALIDARFIN
	ON FINANCIERA.IdInfoFin=VALIDARFIN.IdInfoFin
	RIGHT JOIN @tblTempInfoFin TEMP ON
	VALIDARFIN.IdInfoFin=TEMP.IdInfoFin AND VALIDARFIN.NroRevision=TEMP.NroRev


	----------------------------------------------------------------------------    

	IF EXISTS(select * from Proveedor.EntidadProvOferente e inner join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad where e.IdEntidad = @IdEntidad)    
	BEGIN    

	SET @IdEstadoEnValidacionFin_Exp =(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
	WHERE Descripcion='EN VALIDACI�N')
	DECLARE @IdEstadoFin INT=0;
	IF EXISTS(SELECT EstadoValidacion  FROM PROVEEDOR.InfoFinancieraEntidad
	WHERE IdEntidad=@IdEntidad AND EstadoValidacion=@IdEstadoEnValidacionFin_Exp)
	BEGIN
	SET @IdEstadoFin=@IdEstadoEnValidacionFin_Exp;
	END


	 --si hay al menos una informacion si validar    
	 IF EXISTS(select * from Proveedor.EntidadProvOferente e     
		left join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad     
		left join Proveedor.ValidarInfoFinancieraEntidad v    
		  on v.IdInfoFin = i.IdInfoFin where e.IdEntidad = @IdEntidad and v.IdInfoFin is null)    
	 BEGIN    
	 insert into Proveedor.Resumen  
	   select 1 as Orden,    
		 e.IdEntidad,         
		 MAX(ISNULL(i.NroRevision,1)) as NroRevision,    
		 'Financiera' as Componente,    
		 -1 AS iConfirmaYAprueba,    
		 cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,    
		 CASE WHEN @IdEstadoFin = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,
		 -1 as iCorreoEnviado    
	   from  Proveedor.EntidadProvOferente e    
		inner join Proveedor.InfoFinancieraEntidad i    
		 on e.IdEntidad = i.IdEntidad         
	   where     
		 e.IdEntidad  = @IdEntidad    
	   group by e.IdEntidad    
	 END    
	 ELSE    
	 BEGIN    
	 -- todas tienen validacion  
	 --@TBL  
	 insert into Proveedor.Resumen   
		SELECT 1 as Orden,
		@IdEntidad as IdEntidad,
		(SELECT MAX(NroRevision) FROM @TBL) as NroRevision,
		'Financiera' as Componente,    
		MIN(case when ConfirmaYAprueba IS NULL THEN -1 ELSE ConfirmaYAprueba END) AS iConfirmaYAprueba,
		MIN(Finalizado) As Finalizado,
		CASE WHEN @IdEstadoFin = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,    
		MAX(case when CorreoEnviado IS NULL THEN -1 ELSE CorreoEnviado END ) AS iCorreoEnviado
		FROM 
		@TBL
	END    
	END    
	ELSE /*No hay informacion*/    
	BEGIN    
	insert into Proveedor.Resumen  
	   select  1 as Orden,     
		 @IdEntidad as IdEntidad,      
		 1 as NroRevision,    
		 'Financiera' as Componente,    
		 -2 AS iConfirmaYAprueba,    
		 0 as Finalizado,    
		 CASE WHEN @IdEstadoFin = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as Liberar,
		 -2 AS iCorreoEnviado       
	END      
	    
	--=========================================================================================================    
	--Se obtiene el resumen para experiencias    
	--=========================================================================================================    
	----------------------------------EXPERIENCIAS-----------------------------

	declare @UltimaRevisionExperiencia int     
	set @UltimaRevisionExperiencia = (select Max(v.NroRevision) from  
	Proveedor.EntidadProvOferente e left join Proveedor.InfoExperienciaEntidad i on 
	e.IdEntidad = i.IdEntidad left join Proveedor.ValidarInfoExperienciaEntidad v on 
	v.IdExpEntidad = i.IdExpEntidad where e.IdEntidad = @IdEntidad)    
	----------------------------------------------------------------------------
	DECLARE @tblTempInfoExp AS TABLE(
	IdExpEntidad INT, NroRev INT)
	INSERT INTO @tblTempInfoExp
	SELECT 
	InfoExp.IdExpEntidad
	,MAX(InfoVal.NroRevision)
	FROM 
	PROVEEDOR.InfoExperienciaEntidad InfoExp
	LEFT JOIN PROVEEDOR.ValidarInfoExperienciaEntidad InfoVal
	ON InfoExp.IdExpEntidad=InfoVal.IdExpEntidad
	WHERE InfoExp.IdEntidad=@IdEntidad
	GROUP BY (InfoExp.IdExpEntidad)

	DECLARE @TBLExp AS TABLE(
	IdValidarInfoExperienciaEntidad	INT
	,NroRevision	INT
	,IdExpEntidad	INT
	,ConfirmaYAprueba INT	
	,EstadoDocumental	INT
	,Finalizado	INT
	,CorreoEnviado INT)
	INSERT INTO @TBLExp
	SELECT 
	VALIDAREXP.IdValidarInfoExperienciaEntidad
	,ISNULL(EXPERIENCIAS.NroRevision,1)as NroRevision
	,TEMP.IdExpEntidad
	,VALIDAREXP.ConfirmaYAprueba
	,(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad WHERE 
	IdExpEntidad=TEMP.IdExpEntidad) As EstadoDocumental
	,(SELECT Finalizado FROM PROVEEDOR.InfoExperienciaEntidad WHERE 
	IdExpEntidad=TEMP.IdExpEntidad) As Finalizado
	,VALIDAREXP.CorreoEnviado
	FROM PROVEEDOR.InfoExperienciaEntidad EXPERIENCIAS
	INNER JOIN PROVEEDOR.ValidarInfoExperienciaEntidad VALIDAREXP
	ON EXPERIENCIAS.IdExpEntidad=VALIDAREXP.IdExpEntidad
	RIGHT JOIN @tblTempInfoExp TEMP ON
	VALIDAREXP.IdExpEntidad=TEMP.IdExpEntidad AND VALIDAREXP.NroRevision=TEMP.NroRev


	--SeLECT * FROM @TBLExp
	----------------------------------------------------------------------------    

	IF EXISTS(select * from Proveedor.EntidadProvOferente e inner join 
	Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad where e.IdEntidad 
	= @IdEntidad)    
	BEGIN    
	SET @IdEstadoEnValidacionFin_Exp =(SELECT IdEstadoValidacionDocumental FROM 
	PROVEEDOR.EstadoValidacionDocumental
	WHERE Descripcion='EN VALIDACI�N')
	DECLARE @IdEstadoExp INT=0;
	IF EXISTS(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
	WHERE IdEntidad=@IdEntidad AND EstadoDocumental=@IdEstadoEnValidacionFin_Exp)
	BEGIN
	SET @IdEstadoExp=@IdEstadoEnValidacionFin_Exp;
	END


	 --si hay al menos una informacion si validar    
	 IF EXISTS(select * from Proveedor.EntidadProvOferente e     
		left join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad     
		left join Proveedor.ValidarInfoExperienciaEntidad v    
		  on v.IdExpEntidad = i.IdExpEntidad where e.IdEntidad = @IdEntidad and v.IdExpEntidad is null)    
	 BEGIN   
	 insert into Proveedor.Resumen  
	   select 2 as Orden,    
		 e.IdEntidad,         
		 MAX(ISNULL(i.NroRevision,1)) as NroRevision,    
		 'Experiencias' as Componente,    
		 -1 AS iConfirmaYAprueba,    
		 cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,    
		 CASE WHEN @IdEstadoExp = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as 

	Liberar,
		 -1 as iCorreoEnviado    
	   from  Proveedor.EntidadProvOferente e    
		inner join Proveedor.InfoExperienciaEntidad i    
		 on e.IdEntidad = i.IdEntidad         
	   where     
		 e.IdEntidad  = @IdEntidad    
	   group by e.IdEntidad    
	 END    
	 ELSE    
	 BEGIN    
	 -- todas tienen validacion  
	 --@TBLExp  
	 insert into Proveedor.Resumen   
		SELECT 2 as Orden,
		@IdEntidad,
		(SELECT MAX(NroRevision) FROM @TBLExp) as NroRevision,
		'Experiencias' as Componente,    
		MIN(case when ConfirmaYAprueba IS NULL THEN -1 ELSE ConfirmaYAprueba END) 

	AS iConfirmaYAprueba,
		MIN(Finalizado) As Finalizado,
		CASE WHEN @IdEstadoExp = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as 

	Liberar,    
		MAX(case when CorreoEnviado IS NULL THEN -1 ELSE CorreoEnviado END ) AS 

	iCorreoEnviado
		FROM 
		@TBLExp
	END    
	END    
	ELSE /*No hay informacion*/    
	BEGIN    
	insert into Proveedor.Resumen  
	   select  2 as Orden,     
		 @IdEntidad as IdEntidad,      
		 1 as NroRevision,    
		 'Experiencias' as Componente,    
		 -2 AS iConfirmaYAprueba,    
		 0 as Finalizado,    
		 CASE WHEN @IdEstadoExp = @IdEstadoEnValidacionFin_Exp THEN 1 ELSE 0 END as 
	Liberar,
		 -2 AS iCorreoEnviado       
	END    
END	    
ELSE
BEGIN
	-------INTEGRANTES----------------------
	DECLARE @IdEstadoEnValidacioIntegrantes INT
	=(SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes WHERE Descripcion='EN VALIDACI�N')
	declare @UltimaRevisionIntegrantes int     
	set @UltimaRevisionIntegrantes =(SELECT MAX(NroRevision) FROM Proveedor.ValidarInfoIntegrantesEntidad
	WHERE IdEntidad=@IdEntidad)
	--(select Max(v.NroRevision) from  Proveedor.EntidadProvOferente e left join Proveedor.ValidarInfoIntegrantesEntidad v on v.IdEntidad = e.IdEntidad where e.IdEntidad = @IdEntidad
	--AND v.Activo=1)    
	-----------------------------------------
	

IF EXISTS(SELECT IdValidacionIntegrantesEntidad FROM 
Proveedor.ValidacionIntegrantesEntidad
WHERE IdEntidad=@IdEntidad)  
BEGIN
	insert into Proveedor.Resumen  
	select 3 as Orden,     
     VI.IdEntidad,     
     ISNULL(VI.NroRevision,1) as NroRevision,    
     'Integrantes' as Componente,    
     case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END AS iConfirmaYAprueba,    
     CAST(CAST(ISNULL(VI.Finalizado,0) as int) as bit) as Finalizado,    
     CASE WHEN ISNULL(VI.IdEstadoValidacionIntegrantes,0) = @IdEstadoEnValidacioIntegrantes THEN 1 ELSE 0 END as Liberar,
     case when v.CorreoEnviado IS NULL THEN -1 ELSE v.CorreoEnviado END AS iCorreoEnviado
   from  PROVEEDOR.ValidacionIntegrantesEntidad VI 
    left join Proveedor.ValidarInfoIntegrantesEntidad v    
     on VI.IdEntidad = v.IdEntidad and v.NroRevision = @UltimaRevisionIntegrantes
   where     
     VI.IdEntidad = @IdEntidad 
END
ELSE
BEGIN
	insert into Proveedor.Resumen  
	 select  3 as Orden,     
     @IdEntidad as IdEntidad,      
     1 as NroRevision,    
     'Integrantes' as Componente,    
     -2 AS iConfirmaYAprueba,    
     0 as Finalizado,    
     0 as Liberar,
     -2 AS iCorreoEnviado    
END
END
select * from Proveedor.Resumen Order By Orden    
drop table Proveedor.Resumen    
END    
   


GO


