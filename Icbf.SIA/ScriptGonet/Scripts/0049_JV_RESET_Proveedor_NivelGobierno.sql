USE [SIA]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha Creaci�n: 01-Abril-2014
Descripci�n: en base a un requerimiento del CC016, se realiza un reset de este cat�logo 
llevar un mejor orden, y pasar a completar el requerimiento de la relaci�n entre las
tablas RamaoEstructura-NivelGobierno-NivelOrganizacional-TipodeentidadPublica
**/
GO
TRUNCATE TABLE [PROVEEDOR].[NivelGobierno]
INSERT INTO [PROVEEDOR].[NivelGobierno]
           ([CodigoNivelGobierno]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('001','DEPARTAMENTAL',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[NivelGobierno]
           ([CodigoNivelGobierno]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('002','DISTRITAL',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[NivelGobierno]
           ([CodigoNivelGobierno]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('003','MUNICIPAL',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[NivelGobierno]
           ([CodigoNivelGobierno]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('004','NACIONAL',1,'Administrador',GETDATE(),NULL,NULL) 
