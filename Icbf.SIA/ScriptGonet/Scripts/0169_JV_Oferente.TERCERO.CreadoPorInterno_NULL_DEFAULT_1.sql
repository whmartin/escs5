USE [Oferentes]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 23-Abril-2014
Descripci�n: Se cambian las reglas para Oferente.TERCERO.
El campo BIT CreadoPorInterno Acepta NULL y Por Default es 1.


**/

IF EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'TERCERO'
AND [COLUMN_NAME] = 'CreadoPorInterno'
AND [TABLE_SCHEMA] = 'Oferente')
BEGIN
---Como se tenian ya constraint de Default para la columna CreadoPorInterno, aqu� se eliminan.
---Para Volverlos a Crear, ya que Ten�an CreadoPorInterno BIT Default=0 y debe se 1.
---Ya que Todos son Creados Por Interno, a Ecepxi�n de los Creados Por la Aplicaci�n de �Proveedores.
	DECLARE @sql NVARCHAR(MAX)='0'

    SELECT TOP 1 @sql ='alter table Oferente.TERCERO drop constraint ['+dc.NAME+N']'
    from sys.default_constraints dc
    JOIN sys.columns c
        ON c.default_object_id = dc.object_id
    WHERE 
        dc.parent_object_id = OBJECT_ID('Oferente.TERCERO')
    AND c.name = N'CreadoPorInterno'
        
    IF(@sql!='0')
    BEGIN
		EXEC (@sql)
    END
    
	ALTER TABLE [Oferente].[TERCERO]
	ALTER COLUMN [CreadoPorInterno] BIT NULL 
	ALTER TABLE [Oferente].[TERCERO] 
	ADD CONSTRAINT CreadoPorInterno_DEFAULT DEFAULT 1 FOR CreadoPorInterno
	
	
END