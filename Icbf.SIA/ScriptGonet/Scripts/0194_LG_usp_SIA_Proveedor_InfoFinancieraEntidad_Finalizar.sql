USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Proveedor_InfoFinancieraEntidad_Finalizar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Proveedor_InfoFinancieraEntidad_Finalizar]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  04/30/2014 12:27 PM
-- Description:	Procedimiento almacenado que Finaliza InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_InfoFinancieraEntidad_Finalizar]

	@IdEntidad INT
	
AS
BEGIN

	update Proveedor.InfoFinancieraEntidad  
	set  Finalizado = 1		
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and i.NroRevision = v.NroRevision
		and i.EstadoValidacion = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRM� CON SI
END