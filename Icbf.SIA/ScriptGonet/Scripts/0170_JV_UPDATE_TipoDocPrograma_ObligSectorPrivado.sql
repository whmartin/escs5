USE [SIA]

/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 23-Abril-2014
Descripci�n: Es un Update que marca como obligatorio un Documento cuando
es persona Jur�dica y es Sector Privado.
**/

DECLARE @IDTipoDocPrograma INT =(
SELECT IdTipoDocumentoPrograma FROM 
PROVEEDOR.TipoDocumentoPrograma tdp
INNER JOIN SEG.Programa Prog ON tdp.IdPrograma=Prog.IdPrograma
INNER JOIN PROVEEDOR.TipoDocumento td ON tdp.IdTipoDocumento=td.IdTipoDocumento
WHERE
Prog.CodigoPrograma='PROVEEDOR/GestionProveedores'
AND td.Descripcion='Documento que acredite el nombramiento y facultades del representante legal'
AND tdp.Estado=1
AND tdp.ObligPersonaJuridica=1)


UPDATE PROVEEDOR.TipoDocumentoPrograma
SET ObligSectorPrivado=1
WHERE IdTipoDocumentoPrograma=@IDTipoDocPrograma