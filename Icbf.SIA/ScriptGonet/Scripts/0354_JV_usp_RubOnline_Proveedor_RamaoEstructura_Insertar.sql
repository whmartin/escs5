USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]    Script Date: 08/12/2014 11:54:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]    Script Date: 08/12/2014 11:54:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que guarda un nuevo RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]
		@IdRamaEstructura INT OUTPUT, 	@CodigoRamaEstructura NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.RamaoEstructura(CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRamaEstructura, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdRamaEstructura = SCOPE_IDENTITY() 		
END


GO


