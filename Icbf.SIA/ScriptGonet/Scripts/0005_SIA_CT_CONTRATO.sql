USE [sia]
GO


/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripcion: Creación de las tablas y constraint propios de Contratos
*/

/****** Object:  Table [Contrato].[AmparosAsociados]    Script Date: 29/10/2013 17:55:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[AmparosAsociados](
	[IdAmparo] [int] IDENTITY(1,1) NOT NULL,
	[IdGarantia] [int] NOT NULL,
	[IdTipoAmparo] [int] NOT NULL,
	[FechaVigenciaDesde] [datetime] NOT NULL,
	[VigenciaHasta] [datetime] NOT NULL,
	[ValorParaCalculoAsegurado] [numeric](18, 3) NOT NULL,
	[IdTipoCalculo] [int] NOT NULL,
	[ValorAsegurado] [numeric](18, 3) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_AmparosAsociados] PRIMARY KEY CLUSTERED 
(
	[IdAmparo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[AporteContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[AporteContrato](
	[IdAporte] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [nvarchar](3) NOT NULL,
	[TipoAportante] [nvarchar](11) NOT NULL,
	[NumeroIdenticacionContratista] [bigint] NOT NULL,
	[TipoAporte] [nvarchar](10) NOT NULL,
	[ValorAporte] [int] NOT NULL,
	[DescripcionAporteEspecie] [nvarchar](128) NOT NULL,
	[FechaRP] [datetime] NOT NULL,
	[NumeroRP] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_AporteContrato] PRIMARY KEY CLUSTERED 
(
	[IdAporte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[CategoriaContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[CategoriaContrato](
	[IdCategoriaContrato] [int] IDENTITY(1,1) NOT NULL,
	[NombreCategoriaContrato] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](100) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_CategoriaContrato] PRIMARY KEY CLUSTERED 
(
	[IdCategoriaContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreCategoriaContrato] UNIQUE NONCLUSTERED 
(
	[NombreCategoriaContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[ClausulaContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[ClausulaContrato](
	[IdClausulaContrato] [int] IDENTITY(1,1) NOT NULL,
	[NombreClausulaContrato] [nvarchar](128) NOT NULL,
	[IdTipoClausula] [int] NOT NULL,
	[Contenido] [nvarchar](max) NULL,
	[IdTipoContrato] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ClausulaContrato] PRIMARY KEY CLUSTERED 
(
	[IdClausulaContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreClausulaContrato] UNIQUE NONCLUSTERED 
(
	[NombreClausulaContrato] ASC,
	[IdTipoClausula] ASC,
	[IdTipoContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[Contrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Contrato].[Contrato](
	[IdContrato] [int] IDENTITY(1,1) NOT NULL,
	[FechaRegistro] [datetime] NULL,
	[NumeroProceso] [nvarchar](50) NULL,
	[NumeroContrato] [nvarchar](50) NULL,
	[FechaAdjudicacion] [datetime] NULL,
	[IdModalidad] [int] NULL,
	[IdCategoriaContrato] [int] NULL,
	[IdTipoContrato] [int] NULL,
	[IdCodigoModalidad] [int] NULL,
	[IdModalidadAcademica] [int] NULL,
	[IdCodigoProfesion] [int] NULL,
	[IdNombreProfesion] [int] NULL,
	[IdRegionalContrato] [int] NULL,
	[RequiereActa] [bit] NULL,
	[ManejaAportes] [bit] NULL,
	[ManejaRecursos] [bit] NULL,
	[ManejaVigenciasFuturas] [bit] NULL,
	[IdRegimenContratacion] [int] NULL,
	[CodigoRegional] [int] NULL,
	[NombreSolicitante] [nvarchar](50) NULL,
	[DependenciaSolicitante] [nvarchar](50) NULL,
	[CargoSolicitante] [nvarchar](150) NULL,
	[ObjetoContrato] [nvarchar](300) NULL,
	[AlcanceObjetoContrato] [nvarchar](128) NULL,
	[ValorInicialContrato] [numeric](18, 3) NULL,
	[ValorTotalAdiciones] [numeric](18, 3) NULL,
	[ValorFinalContrato] [numeric](18, 3) NULL,
	[ValorAportesICBF] [numeric](18, 3) NULL,
	[ValorAportesOperador] [numeric](18, 3) NULL,
	[ValorTotalReduccion] [numeric](18, 3) NULL,
	[JustificacionAdicionSuperior50porc] [nvarchar](300) NULL,
	[FechaSuscripcion] [datetime] NULL,
	[FechaInicioEjecucion] [datetime] NULL,
	[FechaFinalizacionInicial] [datetime] NULL,
	[PlazoInicial] [int] NULL,
	[FechaInicialTerminacion] [datetime] NULL,
	[FechaFinalTerminacion] [datetime] NULL,
	[FechaProyectadaLiquidacion] [datetime] NULL,
	[FechaAnulacion] [datetime] NULL,
	[Prorrogas] [int] NULL,
	[PlazoTotal] [int] NULL,
	[FechaFirmaActaInicio] [datetime] NULL,
	[VigenciaFiscalInicial] [int] NULL,
	[VigenciaFiscalFinal] [int] NULL,
	[IdUnidadEjecucion] [int] NULL,
	[IdLugarEjecucion] [int] NULL,
	[DatosAdicionales] [nvarchar](128) NULL,
	[IdEstadoContrato] [int] NULL,
	[IdTipoDocumentoContratista] [nvarchar](3) NULL,
	[IdentificacionContratista] [nvarchar](50) NULL,
	[NombreContratista] [nvarchar](128) NULL,
	[IdFormaPago] [int] NULL,
	[IdTipoEntidad] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[ClaseContrato] [nchar](1) NULL,
	[Consecutivo] [varchar](50) NULL,
	[AfectaPlanCompras] [bit] NULL,
	[IdSolicitante] [int] NULL,
	[IdProducto] [int] NULL,
	[FechaLiquidacion] [datetime] NULL,
	[NumeroDocumentoVigenciaFutura] [int] NULL,
	[RequiereGarantia] [bit] NULL,
 CONSTRAINT [PK_Contrato] PRIMARY KEY CLUSTERED 
(
	[IdContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Contrato].[ContratoVinculacion]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[ContratoVinculacion](
	[IdContrato] [int] NULL,
	[IdContratoAsociado] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[FormaPago]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[FormaPago](
	[IdFormapago] [int] IDENTITY(1,1) NOT NULL,
	[NombreFormaPago] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_FormaPago] PRIMARY KEY CLUSTERED 
(
	[IdFormapago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreFormaPago] UNIQUE NONCLUSTERED 
(
	[NombreFormaPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[Garantia]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[Garantia](
	[IDGarantia] [int] IDENTITY(1,1) NOT NULL,
	[IDContrato] [int] NOT NULL,
	[IDTipoGarantia] [int] NOT NULL,
	[IDterceroaseguradora] [int] NULL,
	[IDtercerosucursal] [int] NULL,
	[IDcontratistaContato] [int] NULL,
	[IDtipobeneficiario] [int] NULL,
	[NITICBF] [nvarchar](50) NULL,
	[DescBeneficiario] [nvarchar](200) NULL,
	[NumGarantia] [nvarchar](128) NOT NULL,
	[FechaExpedicion] [datetime] NOT NULL,
	[FechaVencInicial] [datetime] NOT NULL,
	[FechaRecibo] [datetime] NOT NULL,
	[FechaDevolucion] [datetime] NOT NULL,
	[MotivoDevolucion] [nvarchar](128) NOT NULL,
	[FechaAprobacion] [datetime] NOT NULL,
	[FechaCertificacionPago] [datetime] NOT NULL,
	[Valor] [numeric](18, 3) NULL,
	[Valortotal] [numeric](18, 3) NULL,
	[Estado] [bit] NOT NULL,
	[Anexo] [bit] NOT NULL,
	[ObservacionAnexo] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Garantia] PRIMARY KEY CLUSTERED 
(
	[IDGarantia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[GestionarClausulasContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[GestionarClausulasContrato](
	[IdGestionClausula] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [nvarchar](3) NOT NULL,
	[NombreClausula] [nvarchar](10) NOT NULL,
	[TipoClausula] [int] NOT NULL,
	[Orden] [nvarchar](128) NOT NULL,
	[DescripcionClausula] [nvarchar](128) NOT NULL,
	[OrdenNumero] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_GestionarClausulasContrato] PRIMARY KEY CLUSTERED 
(
	[IdGestionClausula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[GestionarObligacion]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[GestionarObligacion](
	[IdGestionObligacion] [int] IDENTITY(1,1) NOT NULL,
	[IdGestionClausula] [int] NOT NULL,
	[NombreObligacion] [nvarchar](10) NOT NULL,
	[IdTipoObligacion] [int] NOT NULL,
	[Orden] [nvarchar](128) NOT NULL,
	[DescripcionObligacion] [nvarchar](128) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_GestionarObligacion] PRIMARY KEY CLUSTERED 
(
	[IdGestionObligacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[InformacionPresupuestal]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[InformacionPresupuestal](
	[IdInformacionPresupuestal] [int] IDENTITY(1,1) NOT NULL,
	[NumeroCDP] [int] NOT NULL,
	[ValorCDP] [numeric](18, 3) NOT NULL,
	[FechaExpedicionCDP] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InformacionPresupuestal] PRIMARY KEY CLUSTERED 
(
	[IdInformacionPresupuestal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NumeroCDP] UNIQUE NONCLUSTERED 
(
	[NumeroCDP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[InformacionPresupuestalRP]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[InformacionPresupuestalRP](
	[IdInformacionPresupuestalRP] [int] IDENTITY(1,1) NOT NULL,
	[FechaSolicitudRP] [datetime] NOT NULL,
	[NumeroRP] [int] NOT NULL,
	[ValorRP] [int] NOT NULL,
	[FechaExpedicionRP] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InformacionPresupuestalRP] PRIMARY KEY CLUSTERED 
(
	[IdInformacionPresupuestalRP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NumeroRP] UNIQUE NONCLUSTERED 
(
	[NumeroRP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[LugarEjecucion]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[LugarEjecucion](
	[IdContratoLugarEjecucion] [int] NOT NULL,
	[IDDepartamento] [int] NOT NULL,
	[IDMunicipio] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[LugarEjecucionContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[LugarEjecucionContrato](
	[IdContratoLugarEjecucion] [int] IDENTITY(1,1) NOT NULL,
	[IDContrato] [int] NOT NULL,
	[Nacional] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_LugarEjecucionContrato] PRIMARY KEY CLUSTERED 
(
	[IdContratoLugarEjecucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[ModalidadSeleccion]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[ModalidadSeleccion](
	[IdModalidad] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](128) NOT NULL,
	[Sigla] [nvarchar](5) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ModalidadSeleccion] PRIMARY KEY CLUSTERED 
(
	[IdModalidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreModalidadSeleccion] UNIQUE NONCLUSTERED 
(
	[Nombre] ASC,
	[Sigla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[ObjetoContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[ObjetoContrato](
	[IdObjetoContratoContractual] [int] IDENTITY(1,1) NOT NULL,
	[ObjetoContractual] [nvarchar](max) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ObjetoContrato] PRIMARY KEY CLUSTERED 
(
	[IdObjetoContratoContractual] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[Obligacion]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[Obligacion](
	[IdObligacion] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoObligacion] [int] NOT NULL,
	[IdTipoContrato] [int] NOT NULL,
	[Descripcion] [nvarchar](max) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Obligacion] PRIMARY KEY CLUSTERED 
(
	[IdObligacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[RegimenContratacion]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[RegimenContratacion](
	[IdRegimenContratacion] [int] IDENTITY(1,1) NOT NULL,
	[NombreRegimenContratacion] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RegimenContratacion] PRIMARY KEY CLUSTERED 
(
	[IdRegimenContratacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreRegimenContratacion] UNIQUE NONCLUSTERED 
(
	[NombreRegimenContratacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[RelacionarContratistas]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[RelacionarContratistas](
	[IdContratistaContrato] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [int] NOT NULL,
	[NumeroIdentificacion] [bigint] NOT NULL,
	[ClaseEntidad] [nvarchar](1) NOT NULL,
	[PorcentajeParticipacion] [int] NOT NULL,
	[NumeroIdentificacionRepresentanteLegal] [bigint] NOT NULL,
	[EstadoIntegrante] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RelacionarContratistas] PRIMARY KEY CLUSTERED 
(
	[IdContratistaContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[SecuenciaNumeroContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[SecuenciaNumeroContrato](
	[IdSecuenciaContrato] [int] IDENTITY(1,1) NOT NULL,
	[Consecutivo] [int] NOT NULL,
	[CodigoRegional] [int] NOT NULL,
	[AnoVigencia] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_SecuenciaNumeroContrato] PRIMARY KEY CLUSTERED 
(
	[IdSecuenciaContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[SecuenciaNumeroProceso]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[SecuenciaNumeroProceso](
	[IdSecuenciaProceso] [int] IDENTITY(1,1) NOT NULL,
	[Consecutivo] [int] NOT NULL,
	[CodigoRegional] [int] NOT NULL,
	[AnoVigencia] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_SecuenciaNumeroProceso] PRIMARY KEY CLUSTERED 
(
	[IdSecuenciaProceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[SupervisorIntervContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[SupervisorIntervContrato](
	[IDSupervisorInterv] [int] IDENTITY(1,1) NOT NULL,
	[IDContratoSupervisa] [int] NOT NULL,
	[OrigenTipoSupervisor] [bit] NOT NULL,
	[IDTipoSupvInterventor] [int] NOT NULL,
	[IDTerceroExterno] [int] NULL,
	[IDFuncionarioInterno] [int] NULL,
	[IDContratoInterventoria] [int] NULL,
	[IDTerceroInterventoria] [int] NULL,
	[TipoVinculacion] [bit] NOT NULL,
	[FechaInicia] [datetime] NOT NULL,
	[FechaFinaliza] [datetime] NOT NULL,
	[Estado] [bit] NOT NULL,
	[FechaModificaInterventor] [datetime] NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaModifica] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_RelacionarSupervisorInterventor] PRIMARY KEY CLUSTERED 
(
	[IDSupervisorInterv] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[TablaParametrica]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[TablaParametrica](
	[IdTablaParametrica] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTablaParametrica] [nvarchar](128) NOT NULL,
	[NombreTablaParametrica] [nvarchar](128) NOT NULL,
	[Url] [nvarchar](256) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TablaParametrica] PRIMARY KEY CLUSTERED 
(
	[IdTablaParametrica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[TipoAmparo]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[TipoAmparo](
	[IdTipoAmparo] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoAmparo] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdTipoGarantia] [int] NOT NULL,
 CONSTRAINT [PK_TipoAmparo] PRIMARY KEY CLUSTERED 
(
	[IdTipoAmparo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoAmparo] UNIQUE NONCLUSTERED 
(
	[NombreTipoAmparo] ASC,
	[IdTipoGarantia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[TipoClausula]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[TipoClausula](
	[IdTipoClausula] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoClausula] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoClausula] PRIMARY KEY CLUSTERED 
(
	[IdTipoClausula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoClausula] UNIQUE NONCLUSTERED 
(
	[NombreTipoClausula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[TipoContrato]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[TipoContrato](
	[IdTipoContrato] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoContrato] [nvarchar](128) NOT NULL,
	[IdCategoriaContrato] [int] NOT NULL,
	[ActaInicio] [bit] NOT NULL,
	[AporteCofinaciacion] [bit] NOT NULL,
	[RecursoFinanciero] [bit] NOT NULL,
	[RegimenContrato] [int] NOT NULL,
	[DescripcionTipoContrato] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoContrato] PRIMARY KEY CLUSTERED 
(
	[IdTipoContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoContrato] UNIQUE NONCLUSTERED 
(
	[NombreTipoContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[TipoDocumento]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[TipoDocumento](
	[IdTipoDocumento] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoDocumento] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoDocumento] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[TipoGarantia]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[TipoGarantia](
	[IdTipoGarantia] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoGarantia] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoGarantia] PRIMARY KEY CLUSTERED 
(
	[IdTipoGarantia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoGarantia] UNIQUE NONCLUSTERED 
(
	[NombreTipoGarantia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[TipoObligacion]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[TipoObligacion](
	[IdTipoObligacion] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoObligacion] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoObligacion] PRIMARY KEY CLUSTERED 
(
	[IdTipoObligacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[TipoSupvInterventor]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[TipoSupvInterventor](
	[IdTipoSupvInterventor] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoSupvInterventor] PRIMARY KEY CLUSTERED 
(
	[IdTipoSupvInterventor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoSupvInterventor] UNIQUE NONCLUSTERED 
(
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Contrato].[UnidadMedida]    Script Date: 29/10/2013 17:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Contrato].[UnidadMedida](
	[IdNumeroContrato] [int] IDENTITY(1,1) NOT NULL,
	[NumeroContrato] [nvarchar](50) NULL,
	[FechaInicioEjecuciónContrato] [datetime] NOT NULL,
	[FechaTerminacionInicialContrato] [datetime] NOT NULL,
	[FechaFinalTerminacionContrato] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_UnidadMedida] PRIMARY KEY CLUSTERED 
(
	[IdNumeroContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [Contrato].[ClausulaContrato]  WITH CHECK ADD  CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU] FOREIGN KEY([IdTipoClausula])
REFERENCES [Contrato].[TipoClausula] ([IdTipoClausula])
GO
ALTER TABLE [Contrato].[ClausulaContrato] CHECK CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]
GO
ALTER TABLE [Contrato].[ClausulaContrato]  WITH CHECK ADD  CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCONT] FOREIGN KEY([IdTipoContrato])
REFERENCES [Contrato].[TipoContrato] ([IdTipoContrato])
GO
ALTER TABLE [Contrato].[ClausulaContrato] CHECK CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCONT]
GO
ALTER TABLE [Contrato].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_CategoriaContrato] FOREIGN KEY([IdCategoriaContrato])
REFERENCES [Contrato].[CategoriaContrato] ([IdCategoriaContrato])
GO
ALTER TABLE [Contrato].[Contrato] CHECK CONSTRAINT [FK_Contrato_CategoriaContrato]
GO
ALTER TABLE [Contrato].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_FormaPago] FOREIGN KEY([IdFormaPago])
REFERENCES [Contrato].[FormaPago] ([IdFormapago])
GO
ALTER TABLE [Contrato].[Contrato] CHECK CONSTRAINT [FK_Contrato_FormaPago]
GO
ALTER TABLE [Contrato].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_ModalidadSeleccion] FOREIGN KEY([IdModalidad])
REFERENCES [Contrato].[ModalidadSeleccion] ([IdModalidad])
GO
ALTER TABLE [Contrato].[Contrato] CHECK CONSTRAINT [FK_Contrato_ModalidadSeleccion]
GO
ALTER TABLE [Contrato].[Garantia]  WITH CHECK ADD  CONSTRAINT [FK_Garantia_RelacionarContratistas] FOREIGN KEY([IDcontratistaContato])
REFERENCES [Contrato].[RelacionarContratistas] ([IdContratistaContrato])
GO
ALTER TABLE [Contrato].[Garantia] CHECK CONSTRAINT [FK_Garantia_RelacionarContratistas]
GO
ALTER TABLE [Contrato].[GestionarClausulasContrato]  WITH CHECK ADD  CONSTRAINT [FK_GestionarClausulasContrato_ClausulaContrato] FOREIGN KEY([TipoClausula])
REFERENCES [Contrato].[ClausulaContrato] ([IdClausulaContrato])
GO
ALTER TABLE [Contrato].[GestionarClausulasContrato] CHECK CONSTRAINT [FK_GestionarClausulasContrato_ClausulaContrato]
GO
ALTER TABLE [Contrato].[GestionarObligacion]  WITH CHECK ADD  CONSTRAINT [FK_GestionarObligacion_GestionarClausulasContrato] FOREIGN KEY([IdGestionClausula])
REFERENCES [Contrato].[GestionarClausulasContrato] ([IdGestionClausula])
GO
ALTER TABLE [Contrato].[GestionarObligacion] CHECK CONSTRAINT [FK_GestionarObligacion_GestionarClausulasContrato]
GO
ALTER TABLE [Contrato].[GestionarObligacion]  WITH CHECK ADD  CONSTRAINT [FK_GestionarObligacion_TipoObligacion] FOREIGN KEY([IdTipoObligacion])
REFERENCES [Contrato].[TipoObligacion] ([IdTipoObligacion])
GO
ALTER TABLE [Contrato].[GestionarObligacion] CHECK CONSTRAINT [FK_GestionarObligacion_TipoObligacion]
GO
ALTER TABLE [Contrato].[Obligacion]  WITH CHECK ADD  CONSTRAINT [FK_OBLIGACI_FK_OBLIGA_TIPOCONT] FOREIGN KEY([IdTipoContrato])
REFERENCES [Contrato].[TipoContrato] ([IdTipoContrato])
GO
ALTER TABLE [Contrato].[Obligacion] CHECK CONSTRAINT [FK_OBLIGACI_FK_OBLIGA_TIPOCONT]
GO
ALTER TABLE [Contrato].[Obligacion]  WITH CHECK ADD  CONSTRAINT [FK_Obligacion_TipoObligacion1] FOREIGN KEY([IdTipoObligacion])
REFERENCES [Contrato].[TipoObligacion] ([IdTipoObligacion])
GO
ALTER TABLE [Contrato].[Obligacion] CHECK CONSTRAINT [FK_Obligacion_TipoObligacion1]
GO
ALTER TABLE [Contrato].[SupervisorIntervContrato]  WITH CHECK ADD  CONSTRAINT [FK_SupervisorIntervContrato_TipoSupvInterventor] FOREIGN KEY([IDTipoSupvInterventor])
REFERENCES [Contrato].[TipoSupvInterventor] ([IdTipoSupvInterventor])
GO
ALTER TABLE [Contrato].[SupervisorIntervContrato] CHECK CONSTRAINT [FK_SupervisorIntervContrato_TipoSupvInterventor]
GO
ALTER TABLE [Contrato].[TipoAmparo]  WITH CHECK ADD  CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA] FOREIGN KEY([IdTipoGarantia])
REFERENCES [Contrato].[TipoGarantia] ([IdTipoGarantia])
GO
ALTER TABLE [Contrato].[TipoAmparo] CHECK CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]
GO
ALTER TABLE [Contrato].[TipoContrato]  WITH CHECK ADD  CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI] FOREIGN KEY([IdCategoriaContrato])
REFERENCES [Contrato].[CategoriaContrato] ([IdCategoriaContrato])
GO
ALTER TABLE [Contrato].[TipoContrato] CHECK CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'IdInformacionPresupuestal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número del CDP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'NumeroCDP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor del CDP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'ValorCDP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha expedición CDP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'FechaExpedicionCDP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificación del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Registro de información Presupuestal Información de CDP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'IdInformacionPresupuestalRP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de Solicitud RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'FechaSolicitudRP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número del RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'NumeroRP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor del RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'ValorRP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha expedición RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'FechaExpedicionRP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificación del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Registro de información Presupuestal Información de RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP'
GO
