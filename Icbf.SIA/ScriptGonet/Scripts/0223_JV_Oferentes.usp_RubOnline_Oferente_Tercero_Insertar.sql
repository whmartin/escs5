USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Insertar]    Script Date: 06/06/2014 16:19:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Insertar]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Insertar]    Script Date: 06/06/2014 16:19:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  5/23/2013 5:33:48 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Tercero
-- Modificado Por: Juan Carlos Valverde S�mano
-- Descripci�n: El procedimiento contenia llamados a Otra BD. lo cual era Incorrecto.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Insertar]

	@IdTercero INT OUTPUT, 
	@IdDListaTipoDocumento INT, 
	@IdTipoPersona INT=NULL,
	@NumeroIdentificacion NVARCHAR (255), 
	@PrimerNombre NVARCHAR (255), 
	@SegundoNombre NVARCHAR (255), 
	@PrimerApellido NVARCHAR (255), 
	@SegundoApellido NVARCHAR (255), 
	@Email NVARCHAR (255), 
	@Sexo NVARCHAR (1), 
	@ProviderUserKey UNIQUEIDENTIFIER=NULL,
	@DIGITOVERIFICACION INT=NULL,
	@IDESTADOTERCERO INT =NULL,
	@RAZONSOCIAL NVARCHAR (250)=NULL,
	@FechaExpedicionId DATETIME =null, 
	@FechaNacimiento DATETIME =null, 
	@UsuarioCrea NVARCHAR (250)
	
AS
BEGIN

INSERT INTO Oferente.Tercero 
         ([IDTIPODOCIDENTIFICA]
         ,[IDESTADOTERCERO]
         ,[IdTipoPersona]
         ,[ProviderUserKey]
         ,[NUMEROIDENTIFICACION]
         ,[DIGITOVERIFICACION]
         ,[CORREOELECTRONICO]
         ,[PRIMERNOMBRE]
         ,[SEGUNDONOMBRE]
         ,[PRIMERAPELLIDO]
         ,[SEGUNDOAPELLIDO]
         ,[RAZONSOCIAL]
         ,[FECHAEXPEDICIONID]
         ,[FECHANACIMIENTO]
         ,[SEXO]
         ,UsuarioCrea
         ,FechaCrea)
	VALUES (@IdDListaTipoDocumento,
	        @IDESTADOTERCERO,
	        @IdTipoPersona,
	        @ProviderUserKey,
	        @NumeroIdentificacion, 
	        @DIGITOVERIFICACION,
	        @Email, 
	        @PrimerNombre, 
	        @SegundoNombre, 
	        @PrimerApellido, 
	        @SegundoApellido, 
	        @RAZONSOCIAL,
	        @FechaExpedicionId, 
	        @FechaNacimiento,
	        @Sexo, 
	        @UsuarioCrea, 
	        GETDATE())

SELECT @IdTercero=@@IDENTITY

RETURN @@IDENTITY

END


GO


