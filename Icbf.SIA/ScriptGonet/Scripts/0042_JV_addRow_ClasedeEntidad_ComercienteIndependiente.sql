/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha de Creaci�n: 31/03/2014
Descripci�n: Agregar un rehistro a la tabla [SIA].[PROVEEDOR].[ClasedeEntidad]
**/
USE [SIA]
GO

IF NOT EXISTS(SELECT IdClasedeEntidad FROM  [PROVEEDOR].[ClasedeEntidad]
			WHERE Descripcion='COMERCIANTE INDEPENDIENTE')
BEGIN			
INSERT INTO [PROVEEDOR].[ClasedeEntidad]
           ([IdTipodeEntidad]
           ,[IdTipodeActividad]
           ,[CodigoClasedeEntidad]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           (1
           ,1
           ,(SELECT MAX(CodigoClasedeEntidad) + 1  FROM [PROVEEDOR].[ClasedeEntidad])
           ,'COMERCIANTE INDEPENDIENTE'
           ,1
           ,'Administrador'
           ,GETDATE()
           ,NULL
           ,NULL)
END
GO


