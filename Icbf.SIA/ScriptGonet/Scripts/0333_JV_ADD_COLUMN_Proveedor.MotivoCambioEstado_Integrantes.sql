USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 12-AGO-2014
Descripci�n:Agrega a la entidad Proveedor.MotivoCambioEstado la columna que indica si se cambio
el estado para M�dulo Integrantes.
**/
IF NOT EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'MotivoCambioEstado'
AND [COLUMN_NAME] = 'Integrantes'
AND [TABLE_SCHEMA] = 'PROVEEDOR')
BEGIN
	
	ALTER TABLE [PROVEEDOR].[MotivoCambioEstado]
	ADD [Integrantes] BIT NOT NULL DEFAULT (0)
END
