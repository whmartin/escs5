USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_Integrantes_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Consultar]
GO

-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que consulta un(a) Integrantes
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Consultar]
	@IdIntegrante INT
AS
BEGIN
 SELECT IdIntegrante, IdEntidad, IdTipoPersona, IdTipoIdentificacionPersonaNatural, NumeroIdentificacion, PorcentajeParticipacion, ConfirmaCertificado, ConfirmaPersona, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [PROVEEDOR].[Integrantes] WHERE  IdIntegrante = @IdIntegrante
END
