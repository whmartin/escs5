USE [SIA]
GO
/*
Autor: Juan Carlos Valverde S�mano
Fecha: 17/07/2014
Descripci�n: Crea la entidad PROVEEDOR.ValidacionIntegrantesEntidad, 
que almacenar� el estado de validaci�n para el conjunto de integrantes
asociados a una EntidadProvOferente.
*/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[IdEntidad_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[ValidacionIntegrantesEntidad]'))
ALTER TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad] DROP CONSTRAINT [IdEntidad_EntidadProvOferente]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[IdEstadoIntegrantes_EstadoIntegrantes]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[ValidacionIntegrantesEntidad]'))
ALTER TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad] DROP CONSTRAINT [IdEstadoIntegrantes_EstadoIntegrantes]
GO

/****** Object:  Table [PROVEEDOR].[ValidacionIntegrantesEntidad]    Script Date: 07/17/2014 18:04:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[ValidacionIntegrantesEntidad]') AND type in (N'U'))
DROP TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad]
GO


/****** Object:  Table [PROVEEDOR].[ValidacionIntegrantesEntidad]    Script Date: 07/17/2014 18:04:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad](
	[IdValidacionIntegrantesEntidad] [int] IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[IdEstadoValidacionIntegrantes] [int] NOT NULL,
	[NroRevision][int],
	[Finalizado][bit],
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaModifica] [datetime] NULL,
	[UsuarioModifica] [nvarchar](128) NULL,
	[CorreoEnviado] [bit] NOT NULL,
 CONSTRAINT [PK_IdValidacionIntegrantesEntidad] PRIMARY KEY CLUSTERED 
(
	[IdValidacionIntegrantesEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad]  WITH CHECK ADD  CONSTRAINT [IdEntidad_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO

ALTER TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad] CHECK CONSTRAINT [IdEntidad_EntidadProvOferente]
GO

ALTER TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad]  WITH CHECK ADD  CONSTRAINT [IdEstadoIntegrantes_EstadoIntegrantes] FOREIGN KEY([IdEstadoValidacionIntegrantes])
REFERENCES [PROVEEDOR].[EstadoIntegrantes] ([IdEstadoIntegrantes])
GO

ALTER TABLE [PROVEEDOR].[ValidacionIntegrantesEntidad] CHECK CONSTRAINT [IdEstadoIntegrantes_EstadoIntegrantes]
GO


