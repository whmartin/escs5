USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar]    Script Date: 05/06/2014 09:23:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar]    Script Date: 05/06/2014 09:23:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--===================================================================================
--Autor: Mauricio Martinez
--Fecha: 2013/07/30
--Descripcion: Para liberar InfoFinanciera
--===================================================================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar]
(@IdEntidad INT, @EstadoValidacion INT, @UsuarioModifica VARCHAR(256), @Finalizado BIT )
as
begin

DECLARE @idEdoEnValidacion INT =(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN')
	update Proveedor.InfoFinancieraEntidad 
	set Finalizado = @Finalizado,
		EstadoValidacion = @EstadoValidacion,
		UsuarioModifica = @UsuarioModifica
	where 
		IdEntidad = @IdEntidad 
		AND EstadoValidacion=@idEdoEnValidacion

end



GO


