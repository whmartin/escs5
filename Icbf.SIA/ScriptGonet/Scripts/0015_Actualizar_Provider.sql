use QOS

/*
Fecha Creaci�n: 29/10/2013
Usuario Creaci�n: Carlos Cubillos
Descripci�n: A trav�s del select se recupera el provider que se le genera al usuario administrador para la aplicacion SIA, debe ingresarse en el INSERT posterior y ejecutarlo.
*/

SELECT USERID FROM aspnet_Users WHERE UserName='Administrador' and ApplicationId=(SELECT ApplicationId from aspnet_Applications where ApplicationName='SIA') 



use sia
INSERT INTO [SIA].[SEG].[Usuario]
           ([providerKey]
           ,[IdTipoDocumento]
           ,[NumeroDocumento]
           ,[PrimerNombre]
           ,[SegundoNombre]
           ,[PrimerApellido]
           ,[SegundoApellido]
           ,[TelefonoContacto]
           ,[CorreoElectronico]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           ,[UsuarioModificacion]
           ,[FechaModificacion]
           ,[IdTipoUsuario]
           ,[IdRegional]
           ,[IdEntidadContratista]
           ,[IdTipoPersona]
           ,[RazonSocial]
           ,[DV])
     VALUES
           ('98C25B4A-459D-4B16-8C34-FDA1E041FE1D'--Llene aqu� provider.
           ,1
           ,'1'
           ,'Administrador'
           ,'Administrador'
           ,'Administrador'
           ,'Administrador'
           ,''
           ,'Administrador@Administrador.com'
           ,1
           ,'Administrador'
           ,GETDATE()
           ,null
           ,null
           ,1
           ,null
           ,null
           ,null
           ,null
           ,null)
GO

