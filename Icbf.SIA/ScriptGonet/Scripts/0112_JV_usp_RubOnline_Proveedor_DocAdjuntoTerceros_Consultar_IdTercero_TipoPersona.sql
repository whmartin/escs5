USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]    Script Date: 04/06/2014 12:48:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]    Script Date: 04/06/2014 12:48:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona] 
( @IdTercero int, @TipoPersona varchar(128), @IdTemporal NVARCHAR(20)= NULL )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionTercero';

DECLARE @TBL_DOCS_TERCERO 
TABLE(
IDROW INT IDENTITY,
IdDocAdjunto INT, 
IdTercero INT, 
NombreDocumento NVARCHAR(256),
LinkDocumento NVARCHAR(256),
Obligatorio INT,
UsuarioCrea NVARCHAR(256),
FechaCrea DATETIME,
UsuarioModifica NVARCHAR(256),
FechaModifica DATETIME,
IdDocumento INT,
MaxPermitidoKB INT,
ExtensionesPermitidas NVARCHAR(100),
IdTemporal NVARCHAR(20))

INSERT INTO @TBL_DOCS_TERCERO
SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdTercero) as IdTercero,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		--Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdTercero,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             --d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocAdjuntoTercero AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdDocumento = tdp.IdTipoDocumento
WHERE    d.Activo=1 AND (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTercero = @IdTercero
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdTercero,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
		--	 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
AND tdp.estado=1
) as Tabla
WHERE Obligatorio  =1 
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento

IF (@IdTemporal IS NOT NULL)
BEGIN
UPDATE @TBL_DOCS_TERCERO
SET IdTemporal=@IdTemporal
--Juan.Valverde--Actualizar el IdTemporal en los archivos Originales, para poderlos manejar en el form.
UPDATE PROVEEDOR.DocAdjuntoTercero
SET IdTemporal=@IdTemporal
WHERE IDDOCADJUNTO IN (SELECT IdDocAdjunto FROM @TBL_DOCS_TERCERO)
END



DECLARE @nRegistros Int --Almacena la cantidad de registro que retorna la consulta.
SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_TERCERO)
DECLARE @nWhile Int --Almacenará la cantidad de veces que se esta recorriendo en el Bucle.
SET @nWhile=1

--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdDoc INT
SET @IdDoc=(SELECT IdDocAdjunto FROM @TBL_DOCS_TERCERO WHERE IDROW=@nWhile)

UPDATE @TBL_DOCS_TERCERO
SET LinkDocumento=ISNULL((SELECT LINKDOCUMENTO FROM PROVEEDOR.DocAdjuntoTercero WHERE IDDOCADJUNTO=@IdDoc),'')
WHERE IdDocAdjunto=@IdDoc

SET @nWhile=@nWhile+1
END



SELECT 
IdDocAdjunto,
IdTercero,
NombreDocumento,
LinkDocumento,
Obligatorio,
UsuarioCrea,
FechaCrea,
UsuarioModifica,
FechaModifica,
IdDocumento,
MaxPermitidoKB,
ExtensionesPermitidas,
IdTemporal
 FROM @TBL_DOCS_TERCERO 



GO


