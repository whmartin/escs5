USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoExperienciaEntidad
-- Modificacion: 2014/03/19
-- Autor: Leticia Elizabeth Gonzalez
-- Descripción: Asigna ConfirmaYAprueba = NULL
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]
		@IdValidarInfoExperienciaEntidad INT, @IdExpEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT = NULL, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoExperienciaEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdExpEntidad = @IdExpEntidad
		and NroRevision = @NroRevision			
		and IdValidarInfoExperienciaEntidad = @IdValidarInfoExperienciaEntidad
END



