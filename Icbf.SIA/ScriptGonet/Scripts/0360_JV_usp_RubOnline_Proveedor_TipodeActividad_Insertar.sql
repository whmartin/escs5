USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]    Script Date: 08/12/2014 12:03:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]    Script Date: 08/12/2014 12:03:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]
		@IdTipodeActividad INT OUTPUT, 	@CodigoTipodeActividad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipodeActividad(CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipodeActividad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipodeActividad = SCOPE_IDENTITY() 		
END


GO


