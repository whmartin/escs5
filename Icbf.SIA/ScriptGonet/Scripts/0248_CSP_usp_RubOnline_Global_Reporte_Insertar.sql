USE [RubOnline]
GO


If (select Count(*) from sysobjects where type='P' and name='usp_RubOnline_Global_Reporte_Insertar')>0 
BEGIN
	DROP PROCEDURE dbo.usp_RubOnline_Global_Reporte_Insertar;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	SP's para CRUD de Global.Reporte
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Reporte_Insertar]
		 @IdReporte INT OUTPUT
		,@NombreReporte NVARCHAR(512)
		,@Descripcion NVARCHAR(1024)
		,@Servidor NVARCHAR(512)
		,@Carpeta NVARCHAR(512)
		,@NombreArchivo NVARCHAR(512)
		,@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.Reporte(NombreReporte, Descripcion, Servidor, Carpeta, NombreArchivo, UsuarioCrea, FechaCrea)
					  VALUES(@NombreReporte, @Descripcion, @Servidor, @Carpeta, @NombreArchivo, @UsuarioCrea, GETDATE())
	SELECT @IdReporte = @@IDENTITY 		
END
