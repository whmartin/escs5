USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]    Script Date: 04/06/2014 12:49:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]    Script Date: 04/06/2014 12:49:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]
		@IdTercero INT = NULL, 	@IdDocumento INT , 	@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Anno INT, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	DECLARE @IdDocAdjunto INT
	SET @IdDocAdjunto = 0
	IF (@IdTercero = 0) SET @IdTercero = NULL  
	UPDATE Proveedor.DocAdjuntoTercero
	SET Activo=0 
	WHERE IdTemporal=@IdTemporal and IDDOCUMENTO=@IdDocumento AND IDTERCERO IS NULL
	INSERT INTO Proveedor.DocAdjuntoTercero(IDDOCUMENTO, Descripcion, LinkDocumento, Anno, UsuarioCrea, FechaCrea,idtemporal)
					  VALUES(@IdDocumento, @Descripcion, @LinkDocumento, @Anno, @UsuarioCrea, GETDATE(),@IdTemporal)
	
	SELECT @IdDocAdjunto = @@IDENTITY 		
	
END


GO


