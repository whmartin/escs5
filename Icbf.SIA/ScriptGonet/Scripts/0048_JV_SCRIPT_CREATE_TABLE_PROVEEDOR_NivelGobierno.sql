USE [SIA]
GO
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha creaci�n: 01/04/2014
Descripci�n: Atendiendo un requerimiento del CC16, not� que el 
cat�logo Niveldegobierno no tiene la estructura necesaria para cumplir
con lo solicitado. por ello se crea est� nueva tabla.
**/
/****** Object:  Table [PROVEEDOR].[NivelGobierno]    Script Date: 04/01/2014 12:21:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[NivelGobierno]') AND type in (N'U'))
DROP TABLE [PROVEEDOR].[NivelGobierno]
GO

USE [SIA]
GO

/****** Object:  Table [PROVEEDOR].[NivelGobierno]    Script Date: 04/01/2014 12:21:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [PROVEEDOR].[NivelGobierno](
	[IdNivelGobierno] [int] IDENTITY(1,1) NOT NULL,
	[CodigoNivelGobierno] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_NivelGobierno] PRIMARY KEY CLUSTERED 
(
	[IdNivelGobierno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key {0} Column' , @level0type=N'SCHEMA',@level0name=N'PROVEEDOR', @level1type=N'TABLE',@level1name=N'NivelGobierno', @level2type=N'COLUMN',@level2name=N'IdNivelGobierno'
GO


