USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]    Script Date: 06/17/2014 13:53:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]    Script Date: 06/17/2014 13:53:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  5/23/2013 5:33:49 PM
-- Description:	Procedimiento almacenado que consulta un(a) Tercero por tipo y numero de documento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion]
@IdTipoIdentificacion INT, @NumeroIdentificacion nvarchar(128)
AS
BEGIN
SELECT [IDTERCERO]
      ,[IDTIPODOCIDENTIFICA]
      ,[IDESTADOTERCERO]
      ,[IdTipoPersona]
      ,[NUMEROIDENTIFICACION]
      ,[DIGITOVERIFICACION]
      ,[CORREOELECTRONICO]
      ,[PRIMERNOMBRE]
      ,[SEGUNDONOMBRE]
      ,[PRIMERAPELLIDO]
      ,[SEGUNDOAPELLIDO]
      ,[RAZONSOCIAL]
      ,[FECHAEXPEDICIONID]
      ,[FECHANACIMIENTO]
      ,[SEXO]
      ,[FECHACREA]
      ,[USUARIOCREA]
      ,[FECHAMODIFICA]
      ,[USUARIOMODIFICA]
      ,[ProviderUserKey]
FROM [Oferente].[Tercero] WITH(NOLOCK)
WHERE IDTIPODOCIDENTIFICA = @IdTipoIdentificacion AND NUMEROIDENTIFICACION=@NumeroIdentificacion
     
END


GO


