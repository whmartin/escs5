USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AUDITA].[FK_LogAccionesDatosBasicos_EntidadProvOferente]') AND parent_object_id = OBJECT_ID(N'[AUDITA].[LogAccionesDatosBasicos]'))
ALTER TABLE [AUDITA].[LogAccionesDatosBasicos] DROP CONSTRAINT [FK_LogAccionesDatosBasicos_EntidadProvOferente]
GO

/****** Object:  Table [AUDITA].[LogAccionesDatosBasicos]    Script Date: 11/06/2014 17:00:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AUDITA].[LogAccionesDatosBasicos]') AND type in (N'U'))
DROP TABLE [AUDITA].[LogAccionesDatosBasicos]
GO

/****** Object:  Table [AUDITA].[LogAccionesDatosBasicos]    Script Date: 11/06/2014 17:00:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/**
Autor:Juan Carlos Valverde S�mano
Fecha:06/NOV/2014
Descripci�n: Crea la entidad para guardar las acciones realizadas en Datos B�sicos.
Esto por el requerimiento de datos de Auditoria solicitado en el CC de Migraci�n:
CC_PRO_MIG_14034_V3_30092014.pdf
**/
CREATE TABLE [AUDITA].[LogAccionesDatosBasicos](
	[IdAccion] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IdEntidad] [int] NULL,
	[Fecha] [datetime] NULL,
	[Usuario] [nvarchar](250) NULL,
	[IdSistema] [int] NULL,
	[Sistema] [nvarchar](500) NULL,
	[Accion] [nvarchar](50) NULL,
 CONSTRAINT [PK_AUDITA.LogAccionesDatosBasicos] PRIMARY KEY CLUSTERED 
(
	[IdAccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [AUDITA].[LogAccionesDatosBasicos]  WITH CHECK ADD  CONSTRAINT [FK_LogAccionesDatosBasicos_EntidadProvOferente] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO

ALTER TABLE [AUDITA].[LogAccionesDatosBasicos] CHECK CONSTRAINT [FK_LogAccionesDatosBasicos_EntidadProvOferente]
GO


