USE [SIA]
GO

/****** Object:  UserDefinedFunction [dbo].[GetIdEstadoProveedor]    Script Date: 04/11/2014 16:56:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetIdEstadoProveedor]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetIdEstadoProveedor]
GO

USE [SIA]
GO

/****** Object:  UserDefinedFunction [dbo].[GetIdEstadoProveedor]    Script Date: 04/11/2014 16:56:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 11-Abril-2014
-- Description:	Recibe los IDs de Estado de cada uno de los módulos de información
--Y Retorna los IdsEstado que deben ser actualizados en Proveedor y en Tercero.
-- Si recibe true en alguna de los parametros BIT, entonces se buscan los estados en la
-- tabla x ya sea PARCIAL o EN VALIDACIÓN.
-- =============================================
CREATE FUNCTION [dbo].[GetIdEstadoProveedor]  
(	
@IdEstadoDatosBasicos INT,
@IdEstadoInfoFinanciera INT,
@IdEstadoInfoExperiencia INT,
@esParcial BIT=NULL,
@esEnValidacion BIT=NULL
)
RETURNS @tblEstados TABLE 
(IdEstadoProveedor INT,
IdEstadoTercero INT)
AS
BEGIN

IF(@esParcial IS NULL AND @esEnValidacion IS NULL)
BEGIN
IF EXISTS(SELECT IdEstadoProveedor,IdEstadoTercero 
FROM [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos
AND IdEstadoDatosFinancieros=@IdEstadoInfoFinanciera
AND	IdEstadoDatosExperiencia=@IdEstadoInfoExperiencia)
BEGIN
	INSERT INTO @tblEstados
	SELECT IdEstadoProveedor,IdEstadoTercero 
	FROM [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
	WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos
	AND IdEstadoDatosFinancieros=@IdEstadoInfoFinanciera
	AND	IdEstadoDatosExperiencia=@IdEstadoInfoExperiencia
END
ELSE
BEGIN
	INSERT INTO @tblEstados
	SELECT IdEstadoProveedor,IdEstadoTercero 
	FROM PROVEEDOR.EstadoProveedor
	WHERE Descripcion='PARCIAL'
END
END
ELSE IF(@esParcial=1)
BEGIN
INSERT INTO @tblEstados
SELECT IdEstadoProveedor,IdEstadoTercero 
FROM PROVEEDOR.EstadoProveedor
WHERE Descripcion='PARCIAL'
END
ELSE IF (@esEnValidacion=1)
BEGIN
INSERT INTO @tblEstados
SELECT IdEstadoProveedor,IdEstadoTercero 
FROM PROVEEDOR.EstadoProveedor
WHERE Descripcion='EN VALIDACIÓN'
END
RETURN
END
GO




