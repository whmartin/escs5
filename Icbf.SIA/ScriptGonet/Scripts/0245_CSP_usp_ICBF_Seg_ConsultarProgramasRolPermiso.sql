USE [RubOnline]
GO


If (select Count(*) from sysobjects where type='P' and name='usp_ICBF_Seg_ConsultarProgramasRolPermiso')>0 
BEGIN
	DROP PROCEDURE dbo.usp_ICBF_Seg_ConsultarProgramasRolPermiso;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	Se agregan los campos EsReporte y IdReporte
-- =============================================
-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  programas que tiene asignado un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarProgramasRolPermiso]
	@pNombreRol NVARCHAR(250)
AS
BEGIN

SELECT P.IdPrograma
      ,P.IdModulo
      ,P.NombrePrograma
      ,P.CodigoPrograma
      ,P.Posicion
      ,P.Estado
      ,P.VisibleMenu
      ,P.UsuarioCreacion
      ,P.FechaCreacion
      ,P.UsuarioModificacion
      ,P.FechaModificacion
      ,P.generaLog
      ,((CASE WHEN PE.Consultar = 1 THEN 1 ELSE 0 END) + (CASE WHEN PE.Eliminar = 1 THEN 2 ELSE 0 END) + (CASE WHEN PE.Modificar  = 1 THEN 4 ELSE 0 END) + (CASE WHEN PE.Insertar  = 1 THEN 8 ELSE 0 END)) Permiso
      ,P.EsReporte
      ,P.IdReporte 
FROM SEG.Programa P (NoLock)
	INNER JOIN SEG.Permiso PE (NoLock) ON P.IdPrograma = PE.IdPrograma
	INNER JOIN SEG.Rol RO (NoLock) ON RO.IdRol = PE.IdRol 	
WHERE RO.Nombre = @pNombreRol	

END

