USE [Oferentes]
GO

/****** Object:  Trigger [descUpdateConsecutivoInterno]    Script Date: 02/25/2014 13:08:15 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[Oferente].[descUpdateConsecutivoInterno]'))
DROP TRIGGER [Oferente].[descUpdateConsecutivoInterno]
GO

USE [Oferentes]
GO

/****** Object:  Trigger [Oferente].[descUpdateConsecutivoInterno]    Script Date: 02/25/2014 13:08:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 25-FEB-2014
-- Description:	Update of ConsecutivoInterno column AFTER insert a row on Terceros table
-- =============================================
CREATE TRIGGER [Oferente].[descUpdateConsecutivoInterno]
   ON  [Oferente].[TERCERO] FOR INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @idTercero INT = (SELECT IDTERCERO FROM INSERTED),
	@txtIdTercero VARCHAR(8),
	@anioCreacion VARCHAR(4),
	@mesCreacion VARCHAR(2)
	
	SELECT 
	@txtIdTercero=(RIGHT('0000000'+CAST([IDTERCERO] AS varchar), 8)),
	@anioCreacion=YEAR([FECHACREA]),
	@mesCreacion=(RIGHT('0'+CAST(MONTH([FECHACREA]) AS varchar), 2))
	FROM [Oferente].[TERCERO]
	WHERE IDTERCERO=@idTercero
	
	UPDATE Oferente.TERCERO
	SET ConsecutivoInterno=('T'+@anioCreacion+@mesCreacion+'-'+@txtIdTercero)
	WHERE IDTERCERO=@idTercero
	
	

END

GO


