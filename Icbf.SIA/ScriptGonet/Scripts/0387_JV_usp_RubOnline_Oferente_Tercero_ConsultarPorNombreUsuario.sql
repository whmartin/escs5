USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]    Script Date: 08/26/2014 11:17:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]    Script Date: 08/26/2014 11:17:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 26/08/2014
-- Description:	Consulta un Tercero por parametro CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]
	@correoElectronico NVARCHAR (50)
AS
BEGIN
	SELECT
		IDTERCERO,
		Numeroidentificacion,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica,
		CreadoPorInterno
	FROM oferente.TERCERO
	WHERE (correoelectronico = @correoElectronico)
END



GO


