USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 08/19/2014 22:06:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 08/19/2014 22:06:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- Modificaci�n Por: Juan Carlos Valverde S�mano
-- Fecha Modificaci�n: 02/04/2014
-- Descripci�n:  Se agreo al Select el providerKey del TERCERO
-- Modificaci�n Por: Juan Carlos Valverde S�mano
-- Fecha Modificaci�n: 19/08/2014
-- Descripci�n:  Se agreo al Select el IdTipoPersona
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT P.IdEntidad
      ,P.ConsecutivoInterno
      ,P.TipoEntOfProv
      ,P.IdTercero
      ,P.IdTipoCiiuPrincipal
      ,P.IdTipoCiiuSecundario
      ,P.IdTipoSector
      ,P.IdTipoClaseEntidad
      ,P.IdTipoRamaPublica
      ,P.IdTipoNivelGob
      ,P.IdTipoNivelOrganizacional
      ,P.IdTipoCertificadorCalidad
      ,P.FechaCiiuPrincipal
      ,P.FechaCiiuSecundario
      ,P.FechaConstitucion
      ,P.FechaVigencia
      ,P.FechaMatriculaMerc
      ,P.FechaExpiracion
      ,P.TipoVigencia
      ,P.ExenMatriculaMer
      ,P.MatriculaMercantil
      ,P.ObserValidador
      ,P.AnoRegistro
      ,P.IdEstado
      ,E.Descripcion
      ,P.UsuarioCrea
      ,P.FechaCrea
      ,P.UsuarioModifica
      ,P.FechaModifica
      ,P.IdAcuerdo
      ,P.Finalizado
      ,T.ProviderUserKey
      ,ISNULL(I.IdTipoEntidad,0) IdTipoEntidad
      ,P.IdEstadoProveedor
      ,P.NumIntegrantes
      ,T.IdTipoPersona
      ,(SELECT Descripcion FROM PROVEEDOR.EstadoProveedor WHERE IdEstadoProveedor=P.IdEstadoProveedor) AS 'DescEstadoProv'
 FROM [Proveedor].[EntidadProvOferente] P
 INNER JOIN Oferente.TERCERO T ON P.IdTercero=T.IDTERCERO 
 INNER JOIN PROVEEDOR.InfoAdminEntidad I ON P.IdEntidad =I.IdEntidad
 INNER JOIN PROVEEDOR.EstadoDatosBasicos E ON P.IdEstado=E.IdEstadoDatosBasicos
 WHERE  P.IdEntidad = @IdEntidad
END



GO


