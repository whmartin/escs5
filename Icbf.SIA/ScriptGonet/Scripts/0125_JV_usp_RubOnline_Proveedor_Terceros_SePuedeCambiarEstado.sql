USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_SePuedeCambiarEstado]    Script Date: 04/08/2014 14:44:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_SePuedeCambiarEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_SePuedeCambiarEstado]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_SePuedeCambiarEstado]    Script Date: 04/08/2014 14:44:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 28-Feb-2014
-- Description:	De acuerdo a los requerimientos del control de cambios 026
--Aqu� se recibe el ID del tercero y se verifica si est� asociado a un Proveedor
--y de ser as� verifica el estado del proveedor, ya que se es 'VALIDADO'
-- El Tercero no podr� cambiar de estado 'VALIDADO' a--> 'En Validaci�n'
--  retorna 0 .... indica que no se puede validar el tercero
--  retorna 1 .... indica que si se puede validar el tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_SePuedeCambiarEstado] 
	-- Add the parameters for the stored procedure here
	@idTercero INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF EXISTS(
SELECT IdEntidad 
FROM PROVEEDOR.EntidadProvOferente 
where IdTercero=@idTercero)
BEGIN
	IF EXISTS
	(SELECT ESTADO.Descripcion
	FROM PROVEEDOR.EntidadProvOferente PROV
	INNER JOIN Proveedor.EstadoDatosBasicos ESTADO 
	ON PROV.IdEstado=ESTADO.IdEstadoDatosBasicos
	where IdTercero=@idTercero AND ESTADO.Descripcion='VALIDADO')
	BEGIN
		SELECT 0 'sePuedeCambiar'
	END
	ELSE
	BEGIN 
		SELECT 1 'sePuedeCambiar'
	END
END
ELSE
BEGIN
	SELECT 1 'sePuedeCambiar'
END
END



GO


