USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InsertarValidarInfoIntegrantesEntidad]    Script Date: 30/07/2014 5:12:39 ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InsertarValidarInfoIntegrantesEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InsertarValidarInfoIntegrantesEntidad]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InsertarValidarInfoIntegrantesEntidad]    Script Date: 30/07/2014 5:12:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Faiber Losada	
-- Create date:  2014/07/29 9:01:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoIntegrantesEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InsertarValidarInfoIntegrantesEntidad]
		@IdValidarInfoIntegrantesEntidad INT OUTPUT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(200),	@ConfirmaYAprueba BIT = NULL, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoIntegrantesEntidad(IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoIntegrantesEntidad = @@IDENTITY 		
END








GO


