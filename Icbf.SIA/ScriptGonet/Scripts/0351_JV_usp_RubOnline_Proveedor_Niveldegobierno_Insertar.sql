USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]    Script Date: 08/12/2014 11:50:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]    Script Date: 08/12/2014 11:50:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]
		@IdNiveldegobierno INT OUTPUT, 	@CodigoNiveldegobierno NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Niveldegobierno(CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNiveldegobierno, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNiveldegobierno = SCOPE_IDENTITY() 		
END


GO


