USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 30-JUL-2014
Descripci�n: Agrega a la Entidad PROVEEDOR.ReglasEstadoProveedor la columna
IdEstadoIntegrantes, para as� poder agregar las reglas para cuando se trata de
un proveedor tipo Consorcio o uni�n temporal.
**/
IF NOT EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'ReglasEstadoProveedor' and COLUMN_NAME = 'IdEstadoIntegrantes'
)
BEGIN
	ALTER TABLE PROVEEDOR.ReglasEstadoProveedor
	ADD IdEstadoIntegrantes INT
END