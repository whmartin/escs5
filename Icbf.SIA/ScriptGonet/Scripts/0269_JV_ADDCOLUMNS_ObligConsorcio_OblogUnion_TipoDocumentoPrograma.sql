USE [SIA]
GO
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 25/06/2014
Descripci�n: Se agregan los campos binarios ObligConsorcio y ObligUnionTemporal a la Tabla Proveedor.TipoDocumentoPrograma, 
**/


IF EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'TipoDocumentoPrograma'
AND [COLUMN_NAME] = 'ObligConsorcioUnion'
AND [TABLE_SCHEMA] = 'Proveedor')
BEGIN
	ALTER TABLE PROVEEDOR.TipoDocumentoPrograma
	DROP COLUMN ObligConsorcioUnion
END

IF NOT EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'TipoDocumentoPrograma'
AND [COLUMN_NAME] = 'ObligConsorcio'
AND [TABLE_SCHEMA] = 'Proveedor')
BEGIN
	ALTER TABLE [Proveedor].[TipoDocumentoPrograma]
	ADD [ObligConsorcio] BIT NOT NULL DEFAULT (0)
END

IF NOT EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'TipoDocumentoPrograma'
AND [COLUMN_NAME] = 'ObligUnionTemporal'
AND [TABLE_SCHEMA] = 'Proveedor')
BEGIN
	ALTER TABLE [Proveedor].[TipoDocumentoPrograma]
	ADD [ObligUnionTemporal] BIT NOT NULL DEFAULT (0)
END



