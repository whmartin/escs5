USE [SIA]
GO
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 10-Abril-2014
Descripci�n: Control de Cambios 016, Esta tabla se ha creado para llevar el registro de las reglas de validaci�n
para el estado del proveedor.
En base a los Estado de los m�dulos DatosBasicos, Financiera y Entidad.
**/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_ReglasEstadoProveedor_EstadoDatosBasicos]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[ReglasEstadoProveedor]'))
ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] DROP CONSTRAINT [FK_ReglasEstadoProveedor_EstadoDatosBasicos]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_ReglasEstadoProveedor_EstadoProveedor]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[ReglasEstadoProveedor]'))
ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] DROP CONSTRAINT [FK_ReglasEstadoProveedor_EstadoProveedor]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PROVEEDOR.ReglasEstadoProveedor_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] DROP CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_Estado]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PROVEEDOR.ReglasEstadoProveedor_UsuarioCrea]') AND type = 'D')
BEGIN
ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] DROP CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_UsuarioCrea]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PROVEEDOR.ReglasEstadoProveedor_FechaCrea]') AND type = 'D')
BEGIN
ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] DROP CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_FechaCrea]
END

GO

USE [SIA]
GO

/****** Object:  Table [PROVEEDOR].[ReglasEstadoProveedor]    Script Date: 04/10/2014 12:47:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[ReglasEstadoProveedor]') AND type in (N'U'))
DROP TABLE [PROVEEDOR].[ReglasEstadoProveedor]
GO

USE [SIA]
GO

/****** Object:  Table [PROVEEDOR].[ReglasEstadoProveedor]    Script Date: 04/10/2014 12:47:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [PROVEEDOR].[ReglasEstadoProveedor](
	[IdValidacionEstado] [int] IDENTITY(1,1) NOT NULL,
	[IdEstadoDatosBasicos] [int] NOT NULL,
	[IdEstadoDatosFinancieros] [int] NULL,
	[IdEstadoDatosExperiencia] [int] NULL,
	[IdEstadoProveedor] [int] NOT NULL,
	[IdEstadoTercero] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](100) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](100) NULL,
	[Fechamodifica] [datetime] NULL,
 CONSTRAINT [PK_PROVEEDOR.ReglasEstadoProveedor] PRIMARY KEY CLUSTERED 
(
	[IdValidacionEstado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor]  WITH CHECK ADD  CONSTRAINT [FK_ReglasEstadoProveedor_EstadoDatosBasicos] FOREIGN KEY([IdEstadoDatosBasicos])
REFERENCES [PROVEEDOR].[EstadoDatosBasicos] ([IdEstadoDatosBasicos])
GO

ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] CHECK CONSTRAINT [FK_ReglasEstadoProveedor_EstadoDatosBasicos]
GO

ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor]  WITH CHECK ADD  CONSTRAINT [FK_ReglasEstadoProveedor_EstadoProveedor] FOREIGN KEY([IdEstadoProveedor])
REFERENCES [PROVEEDOR].[EstadoProveedor] ([IdEstadoProveedor])
GO

ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] CHECK CONSTRAINT [FK_ReglasEstadoProveedor_EstadoProveedor]
GO

ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] ADD  CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_Estado]  DEFAULT ((1)) FOR [Estado]
GO

ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] ADD  CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_UsuarioCrea]  DEFAULT (N'Administrador') FOR [UsuarioCrea]
GO

ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] ADD  CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_FechaCrea]  DEFAULT (getdate()) FOR [FechaCrea]
GO


