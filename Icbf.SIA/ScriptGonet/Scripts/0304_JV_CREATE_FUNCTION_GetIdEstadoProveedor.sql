USE [SIA]
GO

/****** Object:  UserDefinedFunction [dbo].[GetIdEstadoProveedor]    Script Date: 07/30/2014 12:03:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetIdEstadoProveedor]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetIdEstadoProveedor]
GO

/****** Object:  UserDefinedFunction [dbo].[GetIdEstadoProveedor]    Script Date: 07/30/2014 12:03:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 11-Abril-2014
-- Description:	Recibe los IDs de Estado de cada uno de los m�dulos de informaci�n
--Y Retorna los IdsEstado que deben ser actualizados en Proveedor y en Tercero.
-- Si recibe true en alguna de los parametros BIT, entonces se buscan los estados en la
-- tabla x ya sea PARCIAL o EN VALIDACI�N.
-- Modificado: 30/07/2014 Juan Carlos Valverde S�mano
-- Descripci�n: Se elimin� de esta funci�n el IdEstadoTercero. Antes est� funci�n retornaba
-- el IdEstadoProveedor y el IdEstadoTercero, Ahora solo el IdEstadoTercero ya que el tercero no
-- heredara nunca el estado del proveedor.
-- =============================================
CREATE FUNCTION [dbo].[GetIdEstadoProveedor]  
(	
@IdEstadoDatosBasicos INT,
@IdEstadoInfoFinanciera INT,
@IdEstadoInfoExperiencia INT,
@esParcial BIT=NULL,
@esEnValidacion BIT=NULL,
@esConsorcioOUnion BIT=NULL,
@IdEstadoIntegrantes INT=NULL
)
RETURNS @tblEstados TABLE 
(IdEstadoProveedor INT)
AS
BEGIN

IF(@esParcial IS NULL AND @esEnValidacion IS NULL)
BEGIN
IF(@esConsorcioOUnion IS NULL)
BEGIN
	IF EXISTS(SELECT IdEstadoProveedor 
	FROM [PROVEEDOR].[ReglasEstadoProveedor]
	WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos
	AND IdEstadoDatosFinancieros=@IdEstadoInfoFinanciera
	AND	IdEstadoDatosExperiencia=@IdEstadoInfoExperiencia)
	BEGIN
		INSERT INTO @tblEstados
		SELECT IdEstadoProveedor 
		FROM [PROVEEDOR].[ReglasEstadoProveedor]
		WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos
		AND IdEstadoDatosFinancieros=@IdEstadoInfoFinanciera
		AND	IdEstadoDatosExperiencia=@IdEstadoInfoExperiencia
	END
	ELSE
	BEGIN
		INSERT INTO @tblEstados
		SELECT IdEstadoProveedor 
		FROM PROVEEDOR.EstadoProveedor
		WHERE Descripcion='PARCIAL'
	END
END
ELSE IF(@esConsorcioOUnion = 1)
BEGIN
		INSERT INTO @tblEstados
		SELECT IdEstadoProveedor 
		FROM [PROVEEDOR].[ReglasEstadoProveedor]
		WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos
		AND IdEstadoIntegrantes=@IdEstadoIntegrantes
END
END
ELSE IF(@esParcial=1)
BEGIN
INSERT INTO @tblEstados
SELECT IdEstadoProveedor 
FROM PROVEEDOR.EstadoProveedor
WHERE Descripcion='PARCIAL'
END
ELSE IF (@esEnValidacion=1)
BEGIN
INSERT INTO @tblEstados
SELECT IdEstadoProveedor 
FROM PROVEEDOR.EstadoProveedor
WHERE Descripcion='EN VALIDACI�N'
END
RETURN
END



GO


