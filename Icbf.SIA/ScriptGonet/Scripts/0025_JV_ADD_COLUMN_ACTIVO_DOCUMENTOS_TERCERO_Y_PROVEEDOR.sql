/*
Desarrollador: Juan Carlos Valverde S�mano
Fecha Creaci�n: 25-Marzo-2014
Descripci�n:Se agrega el campo Activo tipo BIT a las Tablas
PROVEEDOR.DocAdjuntoTercero y PROVEEDOR.DocDatosBasicoProv
Con el fin de llevar un historial de los Documentos adjuntos
y no eliminar o actualizar el registro.
Esto en base a un requerimiento del Control de cambio 016.
Siempre habr� un solo registro Activo=1 para cada tipo de Documento, 
para cada Tercero y/o proveedor.
*/
USE [SIA]
GO
IF NOT EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'DocAdjuntoTercero' and COLUMN_NAME = 'Activo'
)
BEGIN
	ALTER TABLE PROVEEDOR.DocAdjuntoTercero
	ADD Activo bit NOT NULL DEFAULT 1
END

IF NOT EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'DocDatosBasicoProv' and COLUMN_NAME = 'Activo'
)
BEGIN
	ALTER TABLE PROVEEDOR.DocDatosBasicoProv
	ADD Activo bit NOT NULL DEFAULT 1
END




