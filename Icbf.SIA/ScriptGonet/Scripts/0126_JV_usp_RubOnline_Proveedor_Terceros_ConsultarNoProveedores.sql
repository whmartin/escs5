USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_ConsultarNoProveedores]    Script Date: 04/08/2014 14:51:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Terceros_ConsultarNoProveedores]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_ConsultarNoProveedores]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_ConsultarNoProveedores]    Script Date: 04/08/2014 14:51:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date:  2014-04-08
-- Description:	Procedimiento almacenado que consulta los Tercero que aun no son Proveedores.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_ConsultarNoProveedores]
	@IDTIPODOCIDENTIFICA int = NULL,
	@IDESTADOTERCERO int = NULL,
	@IdTipoPersona int = NULL,
	@NUMEROIDENTIFICACION varchar(30) = NULL,
	@USUARIOCREA varchar(128) = NULL,
	@Tercero varchar(128) = NULL,
	@FECHACREA datetime = NULL
	
AS
BEGIN
	SELECT       Oferente.TERCERO.IDTERCERO, 
				 Oferente.TERCERO.IDTIPODOCIDENTIFICA, 
				 Oferente.TERCERO.IDESTADOTERCERO, 
				 Oferente.TERCERO.IdTipoPersona, 
                 Oferente.TERCERO.NUMEROIDENTIFICACION, 
                 Oferente.TERCERO.DIGITOVERIFICACION, 
                 Oferente.TERCERO.CORREOELECTRONICO, 
                 Oferente.TERCERO.PRIMERNOMBRE, 
                 Oferente.TERCERO.SEGUNDONOMBRE, 
                 Oferente.TERCERO.PRIMERAPELLIDO, 
                 Oferente.TERCERO.SEGUNDOAPELLIDO, 
                 Oferente.TERCERO.RAZONSOCIAL, 
                 Oferente.TERCERO.FECHAEXPEDICIONID, 
                 Oferente.TERCERO.FECHANACIMIENTO, 
                 Oferente.TERCERO.SEXO, 
                 Oferente.TERCERO.FECHACREA, 
                 Oferente.TERCERO.USUARIOCREA, 
                 Oferente.TERCERO.FECHAMODIFICA, 
                 Oferente.TERCERO.USUARIOMODIFICA, 
                 Global.TiposDocumentos.NomTipoDocumento AS NombreTipoIdentificacionPersonaNatural, 
                 Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural, 
                 Oferente.EstadoTercero.CodigoEstadotercero, 
                 Oferente.EstadoTercero.DescripcionEstado, 
                 Oferente.TipoPersona.CodigoTipoPersona, 
                 Oferente.TipoPersona.NombreTipoPersona
	FROM         Oferente.TERCERO 
				 INNER JOIN Global.TiposDocumentos  ON 
				 Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento 
				 INNER JOIN Oferente.EstadoTercero ON 
				 Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero 
				 INNER JOIN Oferente.TipoPersona ON 
				 Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
				 LEFT JOIN PROVEEDOR.EntidadProvOferente ON Oferente.TERCERO.IDTERCERO=PROVEEDOR.EntidadProvOferente.IdTercero
				 
	WHERE  
				Oferente.TERCERO.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END
				AND Oferente.TERCERO.IDESTADOTERCERO = CASE WHEN @IDESTADOTERCERO IS NULL THEN Oferente.TERCERO.IDESTADOTERCERO ELSE @IDESTADOTERCERO END
				AND Oferente.TERCERO.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona END 
				AND Oferente.TERCERO.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END 
				AND Oferente.TERCERO.USUARIOCREA = CASE WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA END 
				AND (
				isnull(Oferente.TERCERO.PRIMERNOMBRE,'') + ' ' + isnull(Oferente.TERCERO.SEGUNDONOMBRE,'') + ' ' + isnull(Oferente.TERCERO.PRIMERAPELLIDO,'') + ' ' + isnull(Oferente.TERCERO.SEGUNDOAPELLIDO,'') + ' '+ isnull(Oferente.TERCERO.RAZONSOCIAL,'')  LIKE CASE WHEN @Tercero IS NULL THEN isnull(Oferente.TERCERO.PRIMERNOMBRE,'') + ' ' + isnull(Oferente.TERCERO.SEGUNDONOMBRE,'') + ' ' + isnull(Oferente.TERCERO.PRIMERAPELLIDO,'') + ' ' + isnull(Oferente.TERCERO.SEGUNDOAPELLIDO,'') + ' '+ isnull(Oferente.TERCERO.RAZONSOCIAL,'') ELSE  '%'+ @Tercero +'%' END					
				)
				AND CAST(Oferente.TERCERO.FECHACREA AS date) = CASE WHEN @FECHACREA IS NULL THEN CAST(Oferente.TERCERO.FECHACREA AS date) ELSE cast(@FECHACREA AS date) END 	
				AND PROVEEDOR.EntidadProvOferente.IdTercero IS NULL
				
	
     
END





GO


