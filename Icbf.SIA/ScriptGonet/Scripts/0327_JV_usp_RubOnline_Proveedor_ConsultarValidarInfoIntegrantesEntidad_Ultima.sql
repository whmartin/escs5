USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ConsultarValidarInfoIntegrantesEntidad_Ultima]    Script Date: 08/08/2014 11:57:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ConsultarValidarInfoIntegrantesEntidad_Ultima]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ConsultarValidarInfoIntegrantesEntidad_Ultima]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ConsultarValidarInfoIntegrantesEntidad_Ultima]    Script Date: 08/08/2014 11:57:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Faiber Losada	
-- Create date:  2014/07/29 9:01:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoIntegrantesEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ConsultarValidarInfoIntegrantesEntidad_Ultima] (@IdEntidad INT)
as
begin
	select top 1
		v.IdValidarInfoIntegrantesEntidad,
		e.IdEntidad,
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
	from  Proveedor.ValidacionIntegrantesEntidad e
		inner join Proveedor.ValidarInfoIntegrantesEntidad v
		on v.IdEntidad = e.IdEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
	order by NroRevision DESC
end



GO


