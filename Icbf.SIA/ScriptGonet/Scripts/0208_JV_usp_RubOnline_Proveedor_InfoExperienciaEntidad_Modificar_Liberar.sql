USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]    Script Date: 05/06/2014 09:22:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]    Script Date: 05/06/2014 09:22:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




--===================================================================================
--Autor: Mauricio Martinez
--Fecha: 2013/07/30
--Descripcion: Para liberar InfoExperiencia
--===================================================================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]
(@IdEntidad INT, @EstadoDocumental INT, @UsuarioModifica VARCHAR(256), @Finalizado BIT )
as
begin
DECLARE @idEdoEnValidacion INT =(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN')

	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = @Finalizado,
		EstadoDocumental = @EstadoDocumental
	where 
		IdEntidad = @IdEntidad 
		AND EstadoDocumental=@idEdoEnValidacion


end




GO


