USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 30/OCT/2014
Descripci�n: Elimina el Sinonimo usp_RubOnline_Oferente_EntidadProvOferentes_Consultar
ya que el uso de este era incorrecto en el aplicativo.
**/

/****** Object:  Synonym [dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar]    Script Date: 10/30/2014 10:13:57 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'usp_RubOnline_Oferente_EntidadProvOferentes_Consultar')
DROP SYNONYM [dbo].[usp_RubOnline_Oferente_EntidadProvOferentes_Consultar]
GO


