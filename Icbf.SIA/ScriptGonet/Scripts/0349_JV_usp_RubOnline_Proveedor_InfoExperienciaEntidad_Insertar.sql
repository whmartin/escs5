USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]    Script Date: 08/12/2014 11:48:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]    Script Date: 08/12/2014 11:48:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  03/10/2014 13:03:14 PM
-- Description:	Procedimiento almacenado que Inserta Experiencias
-- =============================================
CREATE  PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]
		@IdExpEntidad INT OUTPUT, 	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	
		@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	
		@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(256),	
		@FechaInicio DATETIME,	@FechaFin DATETIME, @ExperienciaMeses NUMERIC(18,2),	@NumeroContrato NVARCHAR(128),	
		@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(21,3),	@EstadoDocumental INT,	@UnionTempConsorcio BIT,	
		@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	@JardinOPreJardin BIT, @UsuarioCrea NVARCHAR(250),
		@IdTemporal VARCHAR(50)=NULL,
		@Finalizado BIT, @ContratoEjecucion BIT
AS
BEGIN
	INSERT INTO Proveedor.InfoExperienciaEntidad(IdEntidad, IdTipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, 
		FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, PorcentParticipacion, AtencionDeptos, JardinOPreJardin, UsuarioCrea, FechaCrea, NroRevision, Finalizado, ContratoEjecucion)
	VALUES(@IdEntidad, @IdTipoSector, @IdTipoEstadoExp, @IdTipoModalidadExp, @IdTipoModalidad, @IdTipoPoblacionAtendida, @IdTipoRangoExpAcum, @IdTipoCodUNSPSC, @IdTipoEntidadContratante, @EntidadContratante, 
					  @FechaInicio, @FechaFin, @ExperienciaMeses, @NumeroContrato, @ObjetoContrato, @Vigente, @Cuantia, @EstadoDocumental, @UnionTempConsorcio, @PorcentParticipacion, @AtencionDeptos, @JardinOPreJardin, @UsuarioCrea, GETDATE(),1,@Finalizado, @ContratoEjecucion)
	
	SELECT @IdExpEntidad = SCOPE_IDENTITY()
					  
	UPDATE [Proveedor].[DocExperienciaEntidad] 
	SET IdExpEntidad = @IdExpEntidad
	WHERE IdTemporal = @IdTemporal
					  
	SELECT @IdExpEntidad
END





GO


