USE [SIA]

TRUNCATE TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]
DECLARE @IdRamaEstructura INT, @IdNivelGobierno INT, @IdNivelOrganizacional INT, @IdTipodeentidadPublica INT	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DEPARTAMENTAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'CENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'DEPARTAMENTO ADMINISTRATIVO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DEPARTAMENTAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'UNIDAD ADMINISTRATIVA ESPECIAL'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DEPARTAMENTAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'INDUSTRIAL Y COMERCIAL DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DEPARTAMENTAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'ESTABLECIMIENTO PUBLICO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DEPARTAMENTAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'EMPRESA SOCIAL DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DISTRITAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'CENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'DEPARTAMENTO ADMINISTRATIVO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DISTRITAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'UNIDAD ADMINISTRATIVA ESPECIAL'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DISTRITAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'INDUSTRIAL Y COMERCIAL DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DISTRITAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'ESTABLECIMIENTO PUBLICO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DISTRITAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'EMPRESA SOCIAL DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'MUNICIPAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'CENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'DEPARTAMENTO ADMINISTRATIVO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'MUNICIPAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'UNIDAD ADMINISTRATIVA ESPECIAL'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'MUNICIPAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'INDUSTRIAL Y COMERCIAL DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'MUNICIPAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'ESTABLECIMIENTO PUBLICO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'MUNICIPAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'EMPRESA SOCIAL DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'CENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'DEPARTAMENTO ADMINISTRATIVO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'CENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'AGENCIAS DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'UNIDAD ADMINISTRATIVA ESPECIAL'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'INDUSTRIAL Y COMERCIAL DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'ESTABLECIMIENTO PUBLICO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'EMPRESA SOCIAL DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'EJECUTIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'DESCENTRALIZADA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'AGENCIAS DEL ESTADO'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'JUDICIAL'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'LEGISLATIVA'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'�RGANOS AUTONOMOS'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DEPARTAMENTAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'�RGANOS AUTONOMOS'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DISTRITAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'�RGANOS AUTONOMOS'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'MUNICIPAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'�RGANOS AUTONOMOS'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'�RGANOS DE CONTROL'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'DEPARTAMENTAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'�RGANOS DE CONTROL'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'MUNICIPAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'�RGANOS DE CONTROL'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
	
SELECT @IdRamaEstructura = IdRamaEstructura FROM Proveedor.RamaoEstructura WHERE Descripcion = 	'�RGANOS ELECTORALES'
SELECT @IdNivelGobierno = IdNivelGobierno FROM Proveedor.NivelGobierno WHERE Descripcion = 	'NACIONAL'
SELECT @IdNivelOrganizacional = IdNivelOrganizacional FROM PROVEEDOR.NivelOrganizacional WHERE Descripcion = 	'NO APLICA'
SELECT @IdTipodeentidadPublica = IdTipodeentidadPublica FROM Proveedor.TipodeentidadPublica WHERE Descripcion = 	'NO APLICA'
INSERT INTO [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](IdRamaEstructura,IdNivelGobierno,IdNivelOrganizacional,IdTipodeentidadPublica,Estado,UsuarioCrea,FechaCrea) VALUES (@IdRamaEstructura,@IdNivelGobierno,@IdNivelOrganizacional,@IdTipodeentidadPublica,1,'Administrador',GETDATE())	
