USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]    Script Date: 04/28/2014 09:40:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]
GO

USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]    Script Date: 04/28/2014 09:40:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		JHON DIAZ
-- Create date:  5/23/2013 5:33:49 PM
-- Description:	Procedimiento almacenado que consulta un(a) Tercero
-- Modificaci�n: Juan Carlos Valverde S�mano
-- Fecha: 20140410
-- Descripci�n: Se agrego el campo CreadoporInterno a la Consulta
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]
@IdTercero INT
AS
BEGIN
--SELECT [IDTERCERO]
--      ,[IDTIPODOCIDENTIFICA]
--      ,[IDESTADOTERCERO]
--      ,[IdTipoPersona]
--      ,[NUMEROIDENTIFICACION]
--      ,[DIGITOVERIFICACION]
--      ,[CORREOELECTRONICO]
--      ,[PRIMERNOMBRE]
--      ,[SEGUNDONOMBRE]
--      ,[PRIMERAPELLIDO]
--      ,[SEGUNDOAPELLIDO]
--      ,[RAZONSOCIAL]
--      ,[FECHAEXPEDICIONID]
--      ,[FECHANACIMIENTO]
--      ,[SEXO]
--      ,[FECHACREA]
--      ,[USUARIOCREA]
--      ,[FECHAMODIFICA]
--      ,[USUARIOMODIFICA]
--	  ,[ProviderUserKey]
--	  ,[CreadoPorInterno]
--FROM [Oferente].[Tercero]
--WHERE IdTercero = @IdTercero
  
  SELECT T.[IDTERCERO]
      ,T.[IDTIPODOCIDENTIFICA]
      ,T.[IDESTADOTERCERO]
      ,T.[IdTipoPersona]
      ,T.[ConsecutivoInterno]
      ,T.[NUMEROIDENTIFICACION]
      ,T.[DIGITOVERIFICACION]
      ,T.[CORREOELECTRONICO]
      ,T.[PRIMERNOMBRE]
      ,T.[SEGUNDONOMBRE]
      ,T.[PRIMERAPELLIDO]
      ,T.[SEGUNDOAPELLIDO]
      ,T.[RAZONSOCIAL]
      ,T.[FECHAEXPEDICIONID]
      ,T.[FECHANACIMIENTO]
      ,T.[SEXO]
      ,T.[FechaCrea]
      ,T.[USUARIOCREA]
      ,T.[FECHAMODIFICA]
      ,T.[USUARIOMODIFICA]
	  ,T.[ProviderUserKey]
	  ,T.[CreadoPorInterno]
FROM  Oferente.TERCERO T 

WHERE T.IdTercero = @IdTercero
     
END



GO


