USE [SIA]
GO
/***
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 16-HUL-2014
Descripci�n: Crea los estados de validaci�n para 
el m�dulo intgrantes.
**/
IF NOT EXISTS
(SELECT IdEstadoIntegrantes FROM [PROVEEDOR].[EstadoIntegrantes]
WHERE Descripcion='REGISTRADO')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoIntegrantes]
			   ([Descripcion]
			   ,[Estado]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('REGISTRADO'
			   ,1
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO

IF NOT EXISTS
(SELECT IdEstadoIntegrantes FROM [PROVEEDOR].[EstadoIntegrantes]
WHERE Descripcion='POR VALIDAR')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoIntegrantes]
			   ([Descripcion]
			   ,[Estado]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('POR VALIDAR'
			   ,1
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO

IF NOT EXISTS
(SELECT IdEstadoIntegrantes FROM [PROVEEDOR].[EstadoIntegrantes]
WHERE Descripcion='POR AJUSTAR')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoIntegrantes]
			   ([Descripcion]
			   ,[Estado]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('POR AJUSTAR'
			   ,1
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO

IF NOT EXISTS
(SELECT IdEstadoIntegrantes FROM [PROVEEDOR].[EstadoIntegrantes]
WHERE Descripcion='VALIDADO')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoIntegrantes]
			   ([Descripcion]
			   ,[Estado]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('VALIDADO'
			   ,1
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO


IF NOT EXISTS
(SELECT IdEstadoIntegrantes FROM [PROVEEDOR].[EstadoIntegrantes]
WHERE Descripcion='EN VALIDACI�N')
BEGIN
	INSERT INTO [PROVEEDOR].[EstadoIntegrantes]
			   ([Descripcion]
			   ,[Estado]
			   ,[UsuarioCrea]
			   ,[FechaCrea]
			   ,[UsuarioModifica]
			   ,[FechaModifica])
		 VALUES
			   ('EN VALIDACI�N'
			   ,1
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO
