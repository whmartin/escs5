USE [SIA]
GO

/****** Object:  Trigger [TR_InfoExperienciaEntidadUPDATE]    Script Date: 07/30/2014 11:10:13 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TR_InfoExperienciaEntidadUPDATE]'))
DROP TRIGGER [PROVEEDOR].[TR_InfoExperienciaEntidadUPDATE]
GO

/****** Object:  Trigger [PROVEEDOR].[TR_InfoExperienciaEntidadUPDATE]    Script Date: 07/30/2014 11:10:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 16-Abril-2014
-- Description:	Actualiza Estado de Proveedor y de Tercero despues de haber actualizado el estado del este m�dulo.
-- Modificado:  30-Julio-2014 Juan Carlos Valverde S�mano
-- Descripci�n: Se elimin� la parte que heredaba el estado VALIDADO del Proveedor al Tercero, 
-- Esto dado por el requerimiento de que Tercero nunca debe heredar estado de Proveedor.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[TR_InfoExperienciaEntidadUPDATE] 
   ON  [PROVEEDOR].[InfoExperienciaEntidad]
   AFTER INSERT,UPDATE
AS 
BEGIN

DECLARE @huboCambioDeEstado BIT =1;
IF EXISTS( SELECT EstadoDocumental FROM deleted)
BEGIN
	IF((SELECT TOP(1) EstadoDocumental FROM inserted)!=(SELECT TOP(1) EstadoDocumental FROM deleted))
	BEGIN
	SET @huboCambioDeEstado=1
	END
	ELSE BEGIN SET @huboCambioDeEstado=0 END
END

--...Se Dispara Solo si hubo cambio de estado de InfoExperienciaEntidad O si es un INSERT
IF(@huboCambioDeEstado=1)
BEGIN


	DECLARE @TBLESTADOS TABLE
	(IdEstadoProveedor INT)


	DECLARE @IdEntidad INT,
	@IdTercero INT,
	@IdEstadoDatosBasicos INT,
	@IdEstadoInfoFinanciera INT,
	@IdEstadoInfoExperiencia INT,
	@EsParcial BIT,
	@EsEnValidacion BIT
	,@IdEdoValidacionDocumental INT=0

		SET @IdEntidad = (SELECT TOP(1) IdEntidad FROM inserted)
		SET @IdTercero= (SELECT IdTercero FROM PROVEEDOR.EntidadProvOferente WHERE IdEntidad=@IdEntidad)
		SET @IdEstadoDatosBasicos = 0
		SET @IdEstadoInfoFinanciera = 0
		SET @IdEstadoInfoExperiencia = 0
		SET @EsParcial=0
		SET @EsEnValidacion=0
		
		DELETE FROM @TBLESTADOS
		
		
		IF EXISTS(SELECT IdEstado FROM PROVEEDOR.EntidadProvOferente
		WHERE IdEntidad = @IdEntidad)
		BEGIN
		SELECT @IdEstadoDatosBasicos=IdEstado FROM PROVEEDOR.EntidadProvOferente
		WHERE IdEntidad = @IdEntidad
		END

		--SI EXISTE AL MENOS UN REGISTRO EN InfoFinancieraEntidad
		IF EXISTS(SELECT EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
		WHERE IdEntidad = @IdEntidad)
		BEGIN
			--SE ASEGURA QUE SOLO EXISTA UN REGISTRO
			IF((SELECT COUNT(EstadoValidacion) FROM PROVEEDOR.InfoFinancieraEntidad
			WHERE IdEntidad = @IdEntidad)=1)
			BEGIN
				SELECT @IdEstadoInfoFinanciera=EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
				WHERE IdEntidad = @IdEntidad
			END
			--SI HAY MAS DE UN REGISTRO
			ELSE IF((SELECT COUNT(EstadoValidacion) FROM PROVEEDOR.InfoFinancieraEntidad
			WHERE IdEntidad = @IdEntidad)>1)
			BEGIN
				--SI, AUNQUE HAY VARIOS, TODOS TIENEN EL MISMO ESTADO.
				IF((SELECT COUNT(DISTINCT(EstadoValidacion)) FROM PROVEEDOR.InfoFinancieraEntidad
				WHERE IdEntidad = @IdEntidad)=1)
				BEGIN
					SELECT TOP(1) @IdEstadoInfoFinanciera=EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
					WHERE IdEntidad = @IdEntidad
				END
				--SI HAY VARIOS Y AL MENOS UNO TIENE ESTADO DIFERENTE
				IF((SELECT COUNT(DISTINCT(EstadoValidacion)) FROM PROVEEDOR.InfoFinancieraEntidad
				WHERE IdEntidad = @IdEntidad)>1)
				BEGIN
						SET @IdEdoValidacionDocumental=(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
				WHERE Descripcion='EN VALIDACI�N')
				
				IF EXISTS(SELECT EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
					WHERE IdEntidad = @IdEntidad AND EstadoValidacion=@IdEdoValidacionDocumental)
				BEGIN
					SET @EsEnValidacion=1;
				END
				ELSE
				BEGIN
					SET @EsParcial=1;
				END
				END
			END
		END
		
		IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
		WHERE Descripcion='EN VALIDACI�N')=@IdEstadoInfoFinanciera)
		BEGIN
		SET @EsEnValidacion=1;
		END


		--IF(@EsParcial=1)
		--BEGIN
		IF((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
			WHERE Descripcion='EN VALIDACI�N') =@IdEstadoDatosBasicos)
			BEGIN
				SET @EsEnValidacion=1;
			END
		--END
		IF( @EsEnValidacion=0)
		BEGIN
			--SI EXISTE AL MENOS UN REGISTRO EN InfoExperienciaEntidad
			IF EXISTS(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
			WHERE IdEntidad = @IdEntidad)
			BEGIN
				--SE ASEGURA QUE SOLO EXISTA UN REGISTRO
				IF((SELECT COUNT(EstadoDocumental) FROM PROVEEDOR.InfoExperienciaEntidad
				WHERE IdEntidad = @IdEntidad)=1)
				BEGIN
					SELECT @IdEstadoInfoExperiencia=EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
					WHERE IdEntidad = @IdEntidad
					IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
					WHERE Descripcion='EN VALIDACI�N')=@IdEstadoInfoExperiencia)
					BEGIN
						SET @EsEnValidacion=1;
					END
				END
				--SI EXISTE MAS DE UN REGISTRO
				ELSE IF((SELECT COUNT(EstadoDocumental) FROM PROVEEDOR.InfoExperienciaEntidad
				WHERE IdEntidad = @IdEntidad)>1)
				BEGIN
					--SI, AUNQUE HAY VARIOS, TODOS TIENEN EL MISMO ESTADO.
					IF((SELECT COUNT(DISTINCT(EstadoDocumental)) FROM PROVEEDOR.InfoExperienciaEntidad
					WHERE IdEntidad = @IdEntidad)=1)
					BEGIN
						SELECT TOP(1) @IdEstadoInfoExperiencia=EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
						WHERE IdEntidad = @IdEntidad
						IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
						WHERE Descripcion='EN VALIDACI�N')=@IdEstadoInfoExperiencia)
						BEGIN
							SET @EsEnValidacion=1;
						END
					END
					ELSE 		--SI  HAY VARIOS Y AL MENOS UNO TIENE ESTADO DIFERENTE
					IF((SELECT COUNT(DISTINCT(EstadoDocumental)) FROM PROVEEDOR.InfoExperienciaEntidad
					WHERE IdEntidad = @IdEntidad)>1)
					BEGIN 
						SET @IdEdoValidacionDocumental=(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
						WHERE Descripcion='EN VALIDACI�N')
						IF EXISTS(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
							WHERE IdEntidad = @IdEntidad AND EstadoDocumental=@IdEdoValidacionDocumental)
						BEGIN
							SET @EsEnValidacion=1;
						END
						ELSE
						BEGIN
							SET @EsParcial=1;
						END
					END
				END
			END
		END



		IF (@EsEnValidacion=1)
		BEGIN
		INSERT INTO @TBLESTADOS
		SELECT IdEstadoProveedor
		FROM dbo.GetIdEstadoProveedor(0,0,0,NULL,1,NULL,NULL)
		END
		ELSE IF(@EsParcial=1)
		BEGIN
		INSERT INTO @TBLESTADOS
		SELECT IdEstadoProveedor
		FROM dbo.GetIdEstadoProveedor(0,0,0,1,NULL,NULL,NULL)
		END
		ELSE
		BEGIN 
		INSERT INTO @TBLESTADOS
		SELECT IdEstadoProveedor
		FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,@IdEstadoInfoFinanciera,@IdEstadoInfoExperiencia,NULL,NULL,NULL,NULL)
		END
		

		
		
		UPDATE PROVEEDOR.EntidadProvOferente
		SET IdEstadoProveedor =(SELECT TOP(1) IdEstadoProveedor FROM @TBLESTADOS)
		WHERE IdEntidad=@IdEntidad
		

END
END




GO


