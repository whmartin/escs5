USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]    Script Date: 08/12/2014 11:31:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]    Script Date: 08/12/2014 11:31:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]
		@IdContacto INT OUTPUT, 	@IdEntidad INT,	@IdSucursal INT, @IdTelContacto INT,	@IdTipoDocIdentifica INT,	
		@IdTipoCargoEntidad INT,	@NumeroIdentificacion NUMERIC(30),	@PrimerNombre NVARCHAR(128),	@SegundoNombre NVARCHAR(128),	
		@PrimerApellido NVARCHAR(128),	@SegundoApellido NVARCHAR(128),	@Dependencia NVARCHAR(128),	@Email NVARCHAR(128),	@Estado BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ContactoEntidad(IdEntidad, IdSucursal, IdTelContacto, IdTipoDocIdentifica, IdTipoCargoEntidad, NumeroIdentificacion, PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido, Dependencia, Email, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @IdSucursal, @IdTelContacto, @IdTipoDocIdentifica, @IdTipoCargoEntidad, @NumeroIdentificacion, @PrimerNombre, @SegundoNombre, @PrimerApellido, @SegundoApellido, @Dependencia, @Email, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdContacto = SCOPE_IDENTITY() 		
END


GO


