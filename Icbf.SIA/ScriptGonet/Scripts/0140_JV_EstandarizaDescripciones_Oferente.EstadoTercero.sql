USE [Oferentes]
GO
/**
Desarrollador: Juan Carlos Valverde Sámano
Fecha: 11-Abril-2014
Descripción: SE observó inconsistencia en la DescripciónEstado
en Oferente.EstadoTercero. En Desarrollo existian Descripciones Diferentes a 
las de Pruebas. Y como de esta descripción dependen otros procedimientos, 
este script es para colocar todo igual.
**/

UPDATE Oferente.EstadoTercero
SET DescripcionEstado='REGISTRADO'
WHERE DescripcionEstado='INACTIVO'

UPDATE Oferente.EstadoTercero
SET DescripcionEstado='POR VALIDAR'
WHERE DescripcionEstado='ACTIVO'

UPDATE Oferente.EstadoTercero
SET DescripcionEstado='POR AJUSTAR'
WHERE DescripcionEstado='Por Ajustar'

UPDATE Oferente.EstadoTercero
SET DescripcionEstado='EN VALIDACIÓN'
WHERE DescripcionEstado='EN VALIDACION'