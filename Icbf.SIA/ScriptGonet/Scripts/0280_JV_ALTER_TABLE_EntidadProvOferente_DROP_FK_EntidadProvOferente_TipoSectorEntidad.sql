USE [SIA]
GO

/*
Creado Por: Juan Carlos Valverde Samano
Fecha: 25-06-2014
Descripci�n: Se quita la llave FK en la tabla PROVEEDOR.EntidadProvOferente que hacie referencia a la tabla
PROVEEDOR.TipoSectorEntidad, ya que para Tipo de Personas Consorcio o Uni�n Temporal no hay TipoSector.
*/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_EntidadProvOferente_TipoSectorEntidad]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[EntidadProvOferente]'))
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_TipoSectorEntidad]
GO




