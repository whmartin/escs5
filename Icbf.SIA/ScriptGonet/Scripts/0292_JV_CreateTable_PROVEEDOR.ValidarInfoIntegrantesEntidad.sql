USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha:16-JUL-2014
Descripci�n: Crea la entidad para almacenar las validaciones de 
los Integrantes de cada EntidadProveedor.
**/

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[IdInfoIntegrantesEntidad_InfoIntegrantesEntidad]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[ValidarInfoIntegrantesEntidad]'))
ALTER TABLE [PROVEEDOR].[ValidarInfoIntegrantesEntidad] DROP CONSTRAINT [IdInfoIntegrantesEntidad_InfoIntegrantesEntidad]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ValidarIn__Corre__40BB4618]') AND type = 'D')
BEGIN
ALTER TABLE [PROVEEDOR].[ValidarInfoIntegrantesEntidad] DROP CONSTRAINT [DF__ValidarIn__Corre__40BB4618]
END

GO

/****** Object:  Table [PROVEEDOR].[ValidarInfoIntegrantesEntidad]    Script Date: 07/16/2014 11:37:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[ValidarInfoIntegrantesEntidad]') AND type in (N'U'))
DROP TABLE [PROVEEDOR].[ValidarInfoIntegrantesEntidad]
GO

/****** Object:  Table [PROVEEDOR].[ValidarInfoIntegrantesEntidad]    Script Date: 07/16/2014 11:37:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [PROVEEDOR].[ValidarInfoIntegrantesEntidad](
	[IdValidarInfoIntegrantesEntidad] [int] IDENTITY(1,1) NOT NULL,
	[NroRevision] [int] NOT NULL,
	[IdEntidad] [int] NOT NULL,
	[ConfirmaYAprueba] [bit] NULL,
	[Observaciones] [nvarchar](200) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaModifica] [datetime] NULL,
	[UsuarioModifica] [nvarchar](128) NULL,
	[CorreoEnviado] [bit] NOT NULL,
 CONSTRAINT [PK_IdValidarInfoIntegrantesEntidad] PRIMARY KEY CLUSTERED 
(
	[IdValidarInfoIntegrantesEntidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key {0} Column' , @level0type=N'SCHEMA',@level0name=N'PROVEEDOR', @level1type=N'TABLE',@level1name=N'ValidarInfoIntegrantesEntidad', @level2type=N'COLUMN',@level2name=N'IdValidarInfoIntegrantesEntidad'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to EntidadProvOferente table' , @level0type=N'SCHEMA',@level0name=N'PROVEEDOR', @level1type=N'TABLE',@level1name=N'ValidarInfoIntegrantesEntidad', @level2type=N'COLUMN',@level2name=N'IdEntidad'
GO

ALTER TABLE [PROVEEDOR].[ValidarInfoIntegrantesEntidad]  WITH CHECK ADD  CONSTRAINT [IdInfoIntegrantesEntidad_InfoIntegrantesEntidad] FOREIGN KEY([IdEntidad])
REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
GO

ALTER TABLE [PROVEEDOR].[ValidarInfoIntegrantesEntidad] CHECK CONSTRAINT [IdInfoIntegrantesEntidad_InfoIntegrantesEntidad]
GO

ALTER TABLE [PROVEEDOR].[ValidarInfoIntegrantesEntidad] ADD  DEFAULT ((0)) FOR [CorreoEnviado]
GO


