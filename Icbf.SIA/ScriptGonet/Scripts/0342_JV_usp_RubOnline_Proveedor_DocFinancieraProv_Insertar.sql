USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]    Script Date: 08/12/2014 11:37:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]    Script Date: 08/12/2014 11:37:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdInfoFin INT=NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @IdTipoDocumento int, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	INSERT INTO Proveedor.DocFinancieraProv(IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, IdTipoDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdInfoFin, @NombreDocumento, @LinkDocumento, @Observaciones, @IdTipoDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = SCOPE_IDENTITY() 		
END



GO


