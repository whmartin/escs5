USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]    Script Date: 08/18/2014 13:16:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]    Script Date: 08/18/2014 13:16:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que consulta un(a) Integrantes
-- Modificado por: Juan Carlos Valverde S�mano
-- Fecha: 10/AGO/2014
-- Descripci�n: Se concaten� Tipo Documento y Numero de Identificaci�n al Nombre o Raz�n Social seg�n el Tipo de Persona.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]
	@IdEntidad INT = NULL
AS
BEGIN
		SELECT	IdIntegrante,
				CASE Integrantes.IdTipoPersona 
				WHEN 1 THEN (TD.CodDocumento+' '+ T.NUMEROIDENTIFICACION+' '+ T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') )
				ELSE TD.CodDocumento+' '+ T.NUMEROIDENTIFICACION+' '+T.RAZONSOCIAL END as Integrante, 
				TP.NombreTipoPersona, 
				PorcentajeParticipacion
		 FROM [PROVEEDOR].[Integrantes] Integrantes
		 INNER JOIN oferente.TERCERO T ON Integrantes.NumeroIdentificacion=T.NUMEROIDENTIFICACION
		 INNER JOIN Oferente.TipoPersona TP ON Integrantes.IdTipoPersona=TP.IdTipoPersona  
		 INNER JOIN Global.TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		 WHERE Integrantes.IdEntidad = CASE WHEN @IdEntidad IS NULL 
			THEN Integrantes.IdEntidad ELSE @IdEntidad END
			AND T.IDESTADOTERCERO IS NOT NULL
		 
END



GO


