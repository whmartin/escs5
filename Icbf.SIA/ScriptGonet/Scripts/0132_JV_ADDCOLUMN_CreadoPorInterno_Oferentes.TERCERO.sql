USE [Oferentes]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 10-Abril-2014
Descripci�n: Se agrega el campo binario CreadoPorInterno a la Tabla Oferente.TERCERO, el cual indicar� si un TERCERO ha sido creado por un Usuario INTERNO (Administrado)=1 o por un expterno=0
**/

IF NOT EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'TERCERO'
AND [COLUMN_NAME] = 'CreadoPorInterno'
AND [TABLE_SCHEMA] = 'Oferente')
BEGIN
	
	ALTER TABLE [Oferente].[TERCERO]
	ADD [CreadoPorInterno] BIT NOT NULL DEFAULT (0)
END