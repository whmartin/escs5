USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidacionIntegrantesEntidad_Modificar_EstadoIntegrantes]    Script Date: 08/07/2014 12:50:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidacionIntegrantesEntidad_Modificar_EstadoIntegrantes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidacionIntegrantesEntidad_Modificar_EstadoIntegrantes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidacionIntegrantesEntidad_Modificar_EstadoIntegrantes]    Script Date: 08/07/2014 12:50:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date:  07/AGO/2014
-- Description:	Procedimiento almacenado que actualiza el estado de validacion del modulo integrantes
-- para una EntidadProveedor.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidacionIntegrantesEntidad_Modificar_EstadoIntegrantes]
		@IdEntidad INT,	
		@IdEstadoIntegrantes INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL,
		@Finalizado BIT = NULL
AS
BEGIN
	UPDATE Proveedor.ValidacionIntegrantesEntidad 
			SET IdEstadoValidacionIntegrantes =ISNULL( @IdEstadoIntegrantes, IdEstadoValidacionIntegrantes ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE(),
			    Finalizado = ISNULL(@Finalizado,Finalizado)
			   WHERE IdEntidad = @IdEntidad 
END




GO


