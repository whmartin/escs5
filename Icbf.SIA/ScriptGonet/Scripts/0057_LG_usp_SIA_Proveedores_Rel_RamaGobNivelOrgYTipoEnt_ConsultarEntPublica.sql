USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarEntPublica]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarEntPublica]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonz�lez
-- Create date:  01/04/2014 17:31 PM
-- Description:	Procedimiento almacenado que consulta TipoEntidadPublica por RamaEstructura, NivelGobierno y NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarEntPublica]

	@IdRamaEstructura INT, @IdNivelGobierno INT, @IdNivelOrganizacional INT
AS
BEGIN

	SELECT	RelRamaGob.IdTipodeentidadPublica,
			CodigoTipodeentidadPublica,
			Descripcion
	FROM	PROVEEDOR.Rel_RamaGobNivelOrgYTipoEnt RelRamaGob
	JOIN
			Proveedor.TipodeentidadPublica TipoEntidad
	ON
			RelRamaGob.IdTipodeentidadPublica = TipoEntidad.IdTipodeentidadPublica
	WHERE	
			RelRamaGob.IdRamaEstructura = @IdRamaEstructura
	AND
			RelRamaGob.IdNivelGobierno = @IdNivelGobierno
	AND
			RelRamaGob.IdNivelOrganizacional = @IdNivelOrganizacional
	ORDER BY
		Descripcion ASC

END