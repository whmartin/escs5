USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]    Script Date: 08/12/2014 11:59:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]    Script Date: 08/12/2014 11:59:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]
		@IdTablaParametrica INT OUTPUT, 	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TablaParametrica(CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTablaParametrica, @NombreTablaParametrica, @Url, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTablaParametrica = SCOPE_IDENTITY() 		
END


GO


