USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]    Script Date: 09/12/2014 10:07:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]    Script Date: 09/12/2014 10:07:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCargoEntidad
-- Modificado Por: Juan Carlos Valverde S�mano
-- Fecha: 12/SEPT/2014
-- Descripci�n se agreg� la validaci�n para no permitir ingresar registros con el mismo
-- C�digo ni con la misma Descripci�n.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
		@IdTipoCargoEntidad INT OUTPUT, @CodigoTipoCargoEntidad NVARCHAR(10),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN

IF EXISTS (SELECT IdTipoCargoEntidad FROM PROVEEDOR.TipoCargoEntidad WHERE CodigoTipoCargoEntidad=@CodigoTipoCargoEntidad)
BEGIN
 RAISERROR ('CodigoUnico', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
END
ELSE IF EXISTS (SELECT IdTipoCargoEntidad FROM PROVEEDOR.TipoCargoEntidad WHERE Descripcion=@Descripcion)
BEGIN
	RAISERROR ('NombreUnico', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
END
ELSE
BEGIN
	INSERT INTO Proveedor.TipoCargoEntidad(CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoCargoEntidad,@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCargoEntidad = SCOPE_IDENTITY() 		
END
END




GO


