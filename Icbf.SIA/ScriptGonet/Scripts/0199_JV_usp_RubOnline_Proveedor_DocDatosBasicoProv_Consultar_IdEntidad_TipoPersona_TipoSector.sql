USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona_TipoSector]    Script Date: 05/02/2014 19:01:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona_TipoSector]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona_TipoSector]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona_TipoSector]    Script Date: 05/02/2014 19:01:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdEntidad y TipoPersona y TipoSector
-- Desarrollador Modificacion: Juan Carlos Valverde S�mano
-- Fecha modificaci�n: 25/03/2014
-- Descripci�n: Se agreg� en el WHERE la condici�n de que solo muestre los registros donde el campo Activo es = 1.
-- =============================================

CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona_TipoSector] 
( @IdEntidad int, @TipoPersona varchar(1),  @TipoSector varchar(1),  @IdTemporal NVARCHAR(20)= NULL,@TipoEntidad varchar(2) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionProveedores';
DECLARE @TBL_DOCS_ENTIDAD 
TABLE(
IDROW INT IDENTITY,
IdDocAdjunto INT, 
IdEntidad INT, 
NombreDocumento NVARCHAR(256),
LinkDocumento NVARCHAR(256),
Observaciones NVARCHAR(MAX),
Obligatorio INT,
UsuarioCrea NVARCHAR(256),
FechaCrea DATETIME,
UsuarioModifica NVARCHAR(256),
FechaModifica DATETIME,
IdTipoDocumento INT,
MaxPermitidoKB INT,
ExtensionesPermitidas NVARCHAR(100),
IdTemporal NVARCHAR(20))
INSERT INTO @TBL_DOCS_ENTIDAD
SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             /*CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,*/
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN CASE @TipoSector	
									WHEN '1' THEN CASE @TipoEntidad
										WHEN '1' THEN CASE  tdp.ObligSectorPrivado
											WHEN 1 THEN tdp.ObligEntNacional
											ELSE tdp.ObligSectorPrivado
											END
										WHEN '2' THEN CASE tdp.ObligSectorPrivado 
											WHEN 1 THEN tdp.ObligEntExtranjera
											ELSE tdp.ObligSectorPrivado
											END
										ELSE tdp.ObligSectorPrivado
										END 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
			 t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE    d.Activo=1 AND (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdEntidad = @IdEntidad
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN CASE @TipoSector	
									WHEN '1' THEN CASE @TipoEntidad
										WHEN '1' THEN CASE  tdp.ObligSectorPrivado
											WHEN 1 THEN tdp.ObligEntNacional
											ELSE tdp.ObligSectorPrivado
											END
										WHEN '2' THEN CASE tdp.ObligSectorPrivado 
											WHEN 1 THEN tdp.ObligEntExtranjera
											ELSE tdp.ObligSectorPrivado
											END
										ELSE tdp.ObligSectorPrivado
										END 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
AND tdp.estado=1
) as Tabla
WHERE Obligatorio  = 1 
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento


IF (@IdTemporal IS NOT NULL)
BEGIN
UPDATE @TBL_DOCS_ENTIDAD
SET IdTemporal=@IdTemporal
--Juan.Valverde--Actualizar el IdTemporal en los archivos Originales, para poderlos manejar en el form.
UPDATE PROVEEDOR.DocDatosBasicoProv
SET IdTemporal=@IdTemporal
WHERE IdDocAdjunto IN (SELECT IdDocAdjunto FROM @TBL_DOCS_ENTIDAD)
END

DECLARE @nRegistros Int --Almacena la cantidad de registro que retorna la consulta.
SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_ENTIDAD)
DECLARE @nWhile Int --Almacenar� la cantidad de veces que se esta recorriendo en el Bucle.
SET @nWhile=1

--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdDoc INT
SET @IdDoc=(SELECT IdDocAdjunto FROM @TBL_DOCS_ENTIDAD WHERE IDROW=@nWhile)

UPDATE @TBL_DOCS_ENTIDAD
SET LinkDocumento=ISNULL((SELECT LINKDOCUMENTO FROM PROVEEDOR.DocDatosBasicoProv WHERE IDDOCADJUNTO=@IdDoc),'')
WHERE IdDocAdjunto=@IdDoc

SET @nWhile=@nWhile+1
END


SELECT 
IdDocAdjunto,
IdEntidad, 
NombreDocumento,
LinkDocumento,
Observaciones,
Obligatorio,
UsuarioCrea,
FechaCrea,
UsuarioModifica,
FechaModifica,
IdTipoDocumento,
MaxPermitidoKB,
ExtensionesPermitidas,
IdTemporal
 FROM @TBL_DOCS_ENTIDAD 




GO


