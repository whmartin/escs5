USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Usuario_ModificarRazonSocial]    Script Date: 04/02/2014 05:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_Usuario_ModificarRazonSocial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_SEG_Usuario_ModificarRazonSocial]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Usuario_ModificarRazonSocial]    Script Date: 04/02/2014 05:28:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 02/04/2014
-- Description:	Modifica la Raz�n Social Para un Usuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Usuario_ModificarRazonSocial]
@razonSocial NVARCHAR(150), @providerLey NVARCHAR(125)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE SEG.Usuario
	SET RazonSocial=@razonSocial
	WHERE providerKey=@providerLey
END

GO


