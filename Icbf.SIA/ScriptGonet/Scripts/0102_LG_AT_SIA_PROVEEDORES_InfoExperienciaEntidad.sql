USE[SIA]
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'ContratoEjecucion' and Object_ID = Object_ID(N'PROVEEDOR.InfoExperienciaEntidad'))
BEGIN
	ALTER TABLE PROVEEDOR.InfoExperienciaEntidad
	ADD ContratoEjecucion BIT
END