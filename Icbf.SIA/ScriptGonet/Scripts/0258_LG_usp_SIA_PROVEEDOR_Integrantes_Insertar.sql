USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantes_Insertar]    Script Date: 06/27/2014 09:30:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_Integrantes_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantes_Insertar]    Script Date: 06/27/2014 09:30:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Integrante
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Insertar]
		@IdIntegrante INT OUTPUT, @IdEntidad INT,	@IdTipoPersona INT,	@IdTipoIdentificacionPersonaNatural INT,	@NumeroIdentificacion NVARCHAR(50),	@PorcentajeParticipacion NUMERIC(5,2),	@ConfirmaCertificado NVARCHAR(5),	@ConfirmaPersona NVARCHAR(5), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	IF EXISTS(SELECT * FROM PROVEEDOR.Integrantes WHERE NumeroIdentificacion=@NumeroIdentificacion)
	BEGIN
		RAISERROR('Ya existe un registro con este(a) Número de Identificación',16,1)
	    RETURN
	END
	INSERT INTO PROVEEDOR.Integrantes(IdTipoPersona, IdEntidad, IdTipoIdentificacionPersonaNatural, NumeroIdentificacion, PorcentajeParticipacion, ConfirmaCertificado, ConfirmaPersona, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoPersona, @IdEntidad, @IdTipoIdentificacionPersonaNatural, @NumeroIdentificacion, @PorcentajeParticipacion, @ConfirmaCertificado, @ConfirmaPersona, @UsuarioCrea, GETDATE())
	SELECT @IdIntegrante = @@IDENTITY 		
END

GO


