USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]    Script Date: 08/12/2014 12:08:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]    Script Date: 08/12/2014 12:08:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]
		@IdTipoRegimenTributario INT OUTPUT, 	@CodigoRegimenTributario NVARCHAR(128),	@Descripcion NVARCHAR(128),	@IdTipoPersona INT,	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoRegimenTributario(CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRegimenTributario, @Descripcion, @IdTipoPersona, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoRegimenTributario = SCOPE_IDENTITY() 		
END


GO


