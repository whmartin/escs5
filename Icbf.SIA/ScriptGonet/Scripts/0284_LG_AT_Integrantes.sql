USE[SIA]
GO
/**
Creado Por: Leticia Gonzalez
Fecha: 27-06-2014
Descripción: Alter Table a Columna NumeroIdentificacion de a Entidad Integrantes, Se modifica el Tipo de Dato.
**/
IF  EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'Integrantes'
AND [COLUMN_NAME] = 'NumeroIdentificacion'
AND [TABLE_SCHEMA] = 'Proveedor')
begin
    ALTER TABLE [PROVEEDOR].[Integrantes]
	ALTER COLUMN NumeroIdentificacion NVARCHAR(256) 
end
