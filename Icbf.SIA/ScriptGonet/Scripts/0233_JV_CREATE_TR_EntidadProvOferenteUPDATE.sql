USE [SIA]
GO

/****** Object:  Trigger [TR_EntidadProvOferenteUPDATE]    Script Date: 06/06/2014 17:45:14 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TR_EntidadProvOferenteUPDATE]'))
DROP TRIGGER [PROVEEDOR].[TR_EntidadProvOferenteUPDATE]
GO



/****** Object:  Trigger [PROVEEDOR].[TR_EntidadProvOferenteUPDATE]    Script Date: 06/06/2014 17:45:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 16-Abril-2014
-- Description:	Actualiza Estado de Proveedor y de Tercero(Sólo cuando es VALIDADO) despues de haber actualizado el estado del este módulo.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[TR_EntidadProvOferenteUPDATE] 
   ON  [PROVEEDOR].[EntidadProvOferente]
   AFTER UPDATE
AS 
BEGIN


IF((SELECT IdEstado FROM inserted)!=(SELECT IdEstado FROM deleted))
BEGIN

	DECLARE @TBLESTADOS TABLE
	(IdEstadoProveedor INT, 
	IdEstadoTercero INT)

	DECLARE @IdEntidad INT,
	@IdTercero INT,
	@IdEstadoDatosBasicos INT,
	@IdEstadoInfoFinanciera INT,
	@IdEstadoInfoExperiencia INT,
	@EsParcial BIT,
	@EsEnValidacion BIT,
	@IdEdoValidacionDocumental INT=0

		SET @IdEntidad = (SELECT  IDENTIDAD FROM inserted)
		SET @IdTercero= (SELECT IdTercero FROM inserted)
		SET @IdEstadoDatosBasicos = 0
		SET @IdEstadoInfoFinanciera = 0
		SET @IdEstadoInfoExperiencia = 0
		SET @EsParcial=0
		SET @EsEnValidacion=0
		
		DELETE FROM @TBLESTADOS
		
		
		IF EXISTS(SELECT IdEstado FROM PROVEEDOR.EntidadProvOferente
		WHERE IdEntidad = @IdEntidad)
		BEGIN
		SELECT @IdEstadoDatosBasicos=IdEstado FROM PROVEEDOR.EntidadProvOferente
		WHERE IdEntidad = @IdEntidad
		END

		--SI EXISTE AL MENOS UN REGISTRO EN InfoFinancieraEntidad
		IF EXISTS(SELECT EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
		WHERE IdEntidad = @IdEntidad)
		BEGIN
			--SE ASEGURA QUE SOLO EXISTA UN REGISTRO
			IF((SELECT COUNT(EstadoValidacion) FROM PROVEEDOR.InfoFinancieraEntidad
			WHERE IdEntidad = @IdEntidad)=1)
			BEGIN
				SELECT @IdEstadoInfoFinanciera=EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
				WHERE IdEntidad = @IdEntidad
			END
			--SI HAY MAS DE UN REGISTRO
			ELSE IF((SELECT COUNT(EstadoValidacion) FROM PROVEEDOR.InfoFinancieraEntidad
			WHERE IdEntidad = @IdEntidad)>1)
			BEGIN
				--SI, AUNQUE HAY VARIOS, TODOS TIENEN EL MISMO ESTADO.
				IF((SELECT COUNT(DISTINCT(EstadoValidacion)) FROM PROVEEDOR.InfoFinancieraEntidad
				WHERE IdEntidad = @IdEntidad)=1)
				BEGIN
					SELECT TOP(1) @IdEstadoInfoFinanciera=EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
					WHERE IdEntidad = @IdEntidad
				END
				--SI HAY VARIOS Y AL MENOS UNO TIENE ESTADO DIFERENTE
				IF((SELECT COUNT(DISTINCT(EstadoValidacion)) FROM PROVEEDOR.InfoFinancieraEntidad
				WHERE IdEntidad = @IdEntidad)>1)
				BEGIN
				SET @IdEdoValidacionDocumental=(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
				WHERE Descripcion='EN VALIDACIÓN')
				
				IF EXISTS(SELECT EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
					WHERE IdEntidad = @IdEntidad AND EstadoValidacion=@IdEdoValidacionDocumental)
				BEGIN
					SET @EsEnValidacion=1;
				END
				ELSE
				BEGIN
					SET @EsParcial=1;
				END
				END
			END
		END
		
		
		IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
		WHERE Descripcion='EN VALIDACIÓN')=@IdEstadoInfoFinanciera)
		BEGIN
		SET @EsEnValidacion=1;
		END

		--IF(@EsParcial=1)
		--BEGIN
		IF((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
			WHERE Descripcion='EN VALIDACIÓN') =@IdEstadoDatosBasicos)
			BEGIN
				SET @EsEnValidacion=1;
			END
		--END
		IF( @EsEnValidacion=0)
		BEGIN
			--SI EXISTE AL MENOS UN REGISTRO EN InfoExperienciaEntidad
			IF EXISTS(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
			WHERE IdEntidad = @IdEntidad)
			BEGIN
				--SE ASEGURA QUE SOLO EXISTA UN REGISTRO
				IF((SELECT COUNT(EstadoDocumental) FROM PROVEEDOR.InfoExperienciaEntidad
				WHERE IdEntidad = @IdEntidad)=1)
				BEGIN
					SELECT @IdEstadoInfoExperiencia=EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
					WHERE IdEntidad = @IdEntidad
					IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
					WHERE Descripcion='EN VALIDACIÓN')=@IdEstadoInfoExperiencia)
					BEGIN
						SET @EsEnValidacion=1;
					END
				END
				--SI EXISTE MAS DE UN REGISTRO
				ELSE IF((SELECT COUNT(EstadoDocumental) FROM PROVEEDOR.InfoExperienciaEntidad
				WHERE IdEntidad = @IdEntidad)>1)
				BEGIN
					--SI, AUNQUE HAY VARIOS, TODOS TIENEN EL MISMO ESTADO.
					IF((SELECT COUNT(DISTINCT(EstadoDocumental)) FROM PROVEEDOR.InfoExperienciaEntidad
					WHERE IdEntidad = @IdEntidad)=1)
					BEGIN
						SELECT TOP(1) @IdEstadoInfoExperiencia=EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
						WHERE IdEntidad = @IdEntidad
						IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
						WHERE Descripcion='EN VALIDACIÓN')=@IdEstadoInfoExperiencia)
						BEGIN
							SET @EsEnValidacion=1;
						END
					END
					ELSE 		--SI  HAY VARIOS Y AL MENOS UNO TIENE ESTADO DIFERENTE
					IF((SELECT COUNT(DISTINCT(EstadoDocumental)) FROM PROVEEDOR.InfoExperienciaEntidad
					WHERE IdEntidad = @IdEntidad)>1)
					BEGIN 
						--------------------
						SET @IdEdoValidacionDocumental=(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
						WHERE Descripcion='EN VALIDACIÓN')
						IF EXISTS(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
							WHERE IdEntidad = @IdEntidad AND EstadoDocumental=@IdEdoValidacionDocumental)
						BEGIN
							SET @EsEnValidacion=1;
						END
						ELSE
						BEGIN
							SET @EsParcial=1;
						END
						-----------
					END
				END
			END
		END


		IF (@EsEnValidacion=1)
		BEGIN
		INSERT INTO @TBLESTADOS
		SELECT IdEstadoProveedor,IdEstadoTercero
		FROM dbo.GetIdEstadoProveedor(0,0,0,NULL,1)
		END
		ELSE IF(@EsParcial=1)
		BEGIN
		INSERT INTO @TBLESTADOS
		SELECT IdEstadoProveedor,IdEstadoTercero
		FROM dbo.GetIdEstadoProveedor(0,0,0,1,NULL)
		END
		ELSE
		BEGIN 
		INSERT INTO @TBLESTADOS
		SELECT IdEstadoProveedor,IdEstadoTercero
		FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,@IdEstadoInfoFinanciera,@IdEstadoInfoExperiencia,NULL,NULL)
		END
		
		
		
		
		UPDATE PROVEEDOR.EntidadProvOferente
		SET IdEstadoProveedor =(SELECT TOP(1) IdEstadoProveedor FROM @TBLESTADOS)
		WHERE IdEntidad=@IdEntidad
		
		IF((SELECT Descripcion FROm PROVEEDOR.EstadoProveedor
		WHERE IdEstadoProveedor=(SELECT TOP(1) IdEstadoProveedor FROM @TBLESTADOS))='VALIDADO')
		BEGIN
			UPDATE Oferente.TERCERO
			SET IDESTADOTERCERO=(SELECT TOP(1)IdEstadoTercero FROM @TBLESTADOS)
			WHERE IDTERCERO=@IdTercero
		END
	
END
	

END




GO


