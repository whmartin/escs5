USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_ReglasEstadoProveedor_EstadoProveedor]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[ReglasEstadoProveedor]'))
ALTER TABLE [PROVEEDOR].[ReglasEstadoProveedor] DROP CONSTRAINT [FK_ReglasEstadoProveedor_EstadoProveedor]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_Proveedor_EstadoProveedor]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[EntidadProvOferente]'))
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] DROP CONSTRAINT [FK_Proveedor_EstadoProveedor]
GO

/****** Object:  Table [PROVEEDOR].[EstadoProveedor]    Script Date: 04/10/2014 10:25:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[EstadoProveedor]') AND type in (N'U'))
DROP TABLE [PROVEEDOR].[EstadoProveedor]
GO

USE [SIA]
GO

/****** Object:  Table [PROVEEDOR].[EstadoProveedor]    Script Date: 04/10/2014 10:25:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [PROVEEDOR].[EstadoProveedor](
	[IdEstadoProveedor] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[IdEstadoTercero] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoProveedor] PRIMARY KEY CLUSTERED 
(
	[IdEstadoProveedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
