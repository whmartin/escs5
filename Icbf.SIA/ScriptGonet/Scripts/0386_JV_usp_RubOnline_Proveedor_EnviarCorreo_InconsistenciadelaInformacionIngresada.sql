USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]    Script Date: 08/26/2014 10:38:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]    Script Date: 08/26/2014 10:38:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  2013-06-09
-- Description:	Procedimiento consulta Inconsistencias la validacion de Datos B�sicos,Datos Financieros y Experiencia del Proveedor
-- Modificado por: Juan Carlos Valverde S�mano
-- Fecha: 2014-05-14
-- Descripci�n: En la l�nea 166 tenia quemado el dato @IdEntidad.... ten�a un n�mero 24...asi quemado. 
-- coloqu� el parametro correspondiente que es @IdEntidad.
-- Se agreg� Tambi�n la parte para tomar la direcci�n de correo Destino del Usuario que Cre� al Tercero
-- En los Casos de haber sido creado por un Interno.
-- Modificado por: Juan Carlos Valverde S�mano
-- Fecha: 2014-08-11
-- Descripci�n:Se agreg� al comunicado la parte del m�dulo Integrantes.
-- Modificaco por: Juan Carlos Valverde S�mano
-- Fecha: 2014-08-26
-- Descripci�n: Se agreg� el par�metro @Asunto.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]

@IdEntidad int
,@Asunto nvarchar(500)

AS
BEGIN
DECLARE  @Email_Destino varchar (max), @Proveedor varchar (100), @nit varchar (30)
DECLARE @Titulo varchar(max) 
Declare @Body varchar (max),
@TableHead varchar (max),
@TableTail varchar (max),
@TableBody varchar (max),
@BodyTitulo varchar(max),
@Body1 varchar (max),
@Body2 varchar (max),
@Body3 varchar (max),
@Final varchar (max),
@IdtipoPersona INT

SELECT @IdTipoPersona=IdTipoPersona FROM Oferente.TERCERO
WHERE IDTERCERO=(SELECT 
IdTercero FROM PROVEEDOR.EntidadProvOferente
WHERE IdEntidad=@IdEntidad)

IF((SELECT T.CreadoPorInterno FROM  Oferente.tercero T
INNER JOIN PROVEEDOR.EntidadProvOferente P
ON P.IdTercero=T.IdTercero
WHERE P.IdEntidad=@IdEntidad)=1)
BEGIN
SELECT @Email_Destino=S.CorreoElectronico,@Proveedor= t.primernombre +' '+ ISNULL( t.segundonombre,'') +' ' + t.primerapellido +' ' + ISNULL( t.segundoapellido,'') +' ' + ISNULL( t. razonsocial,''), @nit=NUMEROIDENTIFICACION FROM Oferente.tercero t 
		INNER JOIN   Proveedor.EntidadProvOferente epo ON t.idtercero= epo.idtercero
		INNER JOIN SEG.Usuario S ON t.ProviderUserKey=S.providerKey
		 WHERE epo.IdEntidad =@IdEntidad 
END
ELSE
BEGIN
SELECT @Email_Destino=t.correoelectronico,@Proveedor= t.primernombre +' '+ ISNULL( t.segundonombre,'') +' ' + t.primerapellido +' ' + ISNULL( t.segundoapellido,'') +' ' + ISNULL( t. razonsocial,''), @nit=NUMEROIDENTIFICACION FROM Oferente.tercero t 
		INNER JOIN   Proveedor.EntidadProvOferente epo ON t.idtercero= epo.idtercero
		 WHERE epo.IdEntidad =@IdEntidad 
END		 

select @Titulo = ValorParametro from SEG.Parametro where NombreParametro='MensajeMailsInconsistenciasProveedor'

SET @TableHead = '<html><body style="font-family:Verdana"><TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD bgColor=#81ba3d colSpan=3></TD></TR>
<TR>
<TD bgColor=#81ba3d colSpan=3><BR/><BR/></TD></TR>
<TR>
<TD bgColor=#81ba3d width="10%"></TD>
<TD>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD width="5%"><BR/></TD>
<TD align=center><BR/></TD>
<TD width="5%"><BR/></TD></TR>
<TR>
<TD></TD>
<TD align=center><STRONG>' + @Titulo + '</STRONG></TD>
<TD></TD></TR>
<TR>
<TD width="5%"></TD>
<TD></TD>
<TD width="5%"></TD></TR>
<TR>
<TD></TD>
<TD><BR/><BR/><STRONG>Apreciado(a):</STRONG></TD>
<TD></TD></TR>
<TR>
<TD></TD>
<TD></TD>
<TD></TD></TR>
<TR>
<TD></TD>
<TD><BR/>Los siguientes son datos que estan inconsistentes, en el registro de proveedores
	<br/> <br/> Nit ' + @nit +' - ( ' + @Proveedor +' ) <br/><br/>
</TD>
<TD></TD></TR>
<TR>
<TD></TD>
<TD></TD>
<TD></TD></TR>
<TR>
<TD></TD>
<TD align = "center"><BR/><table cellpadding=0 cellspacing=0 border=0 width="80%">' +
				 '<tr bgcolor=#ccbfac>'+
				 '<td align=center><b>M�dulo del sistema</b></td>' +
				 '<td align=center><b>Observaci�n</b></td></tr>';

				 
set @TableTail  = '</table><BR/><BR/>
				 </TD>
				<TD></TD>
				</TR>
				<TR>
				<TD bgColor=#81ba3d colSpan=3 align=center></TD></TR>
				<TR>
				<TD bgColor=#81ba3d colSpan=3 align=center><BR/><BR/>Este correo electr�nico fue enviado por un sistema autom�tico, favor de no responderlo. </TD></TR>
				<TR>
				<TD bgColor=#81ba3d colSpan=3 align=center>Si tienes alguna duda, puedes dirigirte a nuestra secci�n de <A href="http://www.icbf.gov.co/" target=_blank>Asistencia y Soporte.</A></TD></TR>
				</TBODY></TABLE>
				</TD>
				<TD bgColor=#81ba3d width="10%"></TD></TR>
				</TBODY></TABLE></P>
				</body></html>';

IF(@IdTipoPersona IN(1,2))
BEGIN
	SELECT @TableBody = (SELECT DISTINCT	ROW_NUMBER() OVER (ORDER BY TRRow) % 2 AS[TRRow],[TRRow],TD FROM(

SELECT 'Datos B�sicos'AS [TRRow],vdb.Observaciones AS [TD] FROM Proveedor.ValidarInfoDatosBasicosEntidad vdb
			  INNER JOIN Proveedor.EntidadProvOferente epo ON vdb.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad =@IdEntidad 			  and vdb.ConfirmaYAprueba = 0 and CorreoEnviado != 1
			  UNION
SELECT 'Datos Financieros'AS [TRRow],vif.Observaciones AS [TD] FROM Proveedor.ValidarInfoFinancieraEntidad vif
			  INNER JOIN Proveedor.InfoFinancieraEntidad ifr ON vif.IdInfoFin=ifr.IdInfoFin
			  INNER JOIN Proveedor.EntidadProvOferente epo ON ifr.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad =@IdEntidad and vif.ConfirmaYAprueba = 0	and CorreoEnviado != 1  UNION 

SELECT 'Experiencia'AS [TRRow],vee.Observaciones AS [TD] FROM Proveedor.ValidarInfoExperienciaEntidad vee
			  INNER JOIN Proveedor.InfoExperienciaEntidad iee ON iee.IdExpEntidad=vee.IdExpEntidad
			  INNER JOIN Proveedor.EntidadProvOferente epo ON iee.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad=@IdEntidad and vee.ConfirmaYAprueba = 0 and CorreoEnviado != 1
			 ) Tbtp
	FOR xml RAW ('tr'), ELEMENTS)
END
ELSE IF (@IdTipoPersona IN(3,4))
BEGIN
	SELECT @TableBody = (SELECT DISTINCT	ROW_NUMBER() OVER (ORDER BY TRRow) % 2 AS[TRRow],[TRRow],TD FROM(

SELECT 'Datos B�sicos'AS [TRRow],vdb.Observaciones AS [TD] FROM Proveedor.ValidarInfoDatosBasicosEntidad vdb
			  INNER JOIN Proveedor.EntidadProvOferente epo ON vdb.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad =@IdEntidad 			  and vdb.ConfirmaYAprueba = 0 and CorreoEnviado != 1
			  UNION
SELECT 'Datos Integrantes'AS [TRRow],vif.Observaciones AS [TD] FROM Proveedor.ValidarInfoIntegrantesEntidad vif
			  INNER JOIN Proveedor.ValidacionIntegrantesEntidad epo ON vif.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad =@IdEntidad and vif.ConfirmaYAprueba = 0	and vif.CorreoEnviado != 1 
			 ) Tbtp
	FOR xml RAW ('tr'), ELEMENTS)
END
-- Replace the entity codes and row numbers
SET @TableBody = REPLACE(@TableBody, '_x0020_', SPACE(1))
SET @TableBody = REPLACE(@TableBody, '_x003D_', '=')
SET @TableBody = REPLACE(@TableBody, '<tr><TRRow>1</TRRow>', '<tr bgcolor=#e5d7c2>')
SET @TableBody = REPLACE(@TableBody, '<TRRow>0</TRRow>', '')
SET @TableBody = REPLACE(@TableBody, 'TRRow', 'td')

SELECT
	@Body = @TableHead + @TableBody + @TableTail



-- Enviar correo
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'GonetMail', --colocar perfil que se tenga configurado
								@recipients = @Email_Destino, --destinatario
								@subject = @Asunto, --asunto
								@body = @Body, --cuerpo de correo
								@body_format = 'HTML'; --formato de correo


--=================================
-- se marca como correo enviado  --
--=================================
UPDATE Proveedor.ValidarInfoDatosBasicosEntidad
SET CorreoEnviado = 1
FROM Proveedor.ValidarInfoDatosBasicosEntidad vdb
INNER JOIN Proveedor.EntidadProvOferente epo ON vdb.IdEntidad=epo.IdEntidad
WHERE epo.IdEntidad =@IdEntidad and vdb.ConfirmaYAprueba = 0 and vdb.CorreoEnviado != 1
IF(@IdtipoPersona IN(1,2))
BEGIN
	UPDATE Proveedor.ValidarInfoFinancieraEntidad
	SET CorreoEnviado = 1
	FROM Proveedor.ValidarInfoFinancieraEntidad vif
	INNER JOIN Proveedor.InfoFinancieraEntidad ifr ON vif.IdInfoFin=ifr.IdInfoFin
	INNER JOIN Proveedor.EntidadProvOferente epo ON ifr.IdEntidad=epo.IdEntidad
	WHERE epo.IdEntidad =@IdEntidad and vif.ConfirmaYAprueba = 0 and vif.CorreoEnviado != 1

	UPDATE Proveedor.ValidarInfoExperienciaEntidad
	SET CorreoEnviado = 1
	FROM Proveedor.ValidarInfoExperienciaEntidad vee
	INNER JOIN Proveedor.InfoExperienciaEntidad iee ON iee.IdExpEntidad=vee.IdExpEntidad
	INNER JOIN Proveedor.EntidadProvOferente epo ON iee.IdEntidad=epo.IdEntidad
	WHERE epo.IdEntidad=@IdEntidad and vee.ConfirmaYAprueba = 0 and vee.CorreoEnviado != 1
END	
ELSE IF(@IdtipoPersona IN(3,4))
BEGIN
UPDATE Proveedor.ValidarInfoIntegrantesEntidad
SET CorreoEnviado = 1
FROM Proveedor.ValidarInfoIntegrantesEntidad vdb
INNER JOIN Proveedor.ValidacionIntegrantesEntidad epo ON vdb.IdEntidad=epo.IdEntidad
WHERE epo.IdEntidad =@IdEntidad and vdb.ConfirmaYAprueba = 0 and vdb.CorreoEnviado != 1
END
END




GO


