USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Consultar]    Script Date: 30/07/2014 5:11:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Consultar]    Script Date: 30/07/2014 5:11:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Faiber Losada	
-- Create date:  2014/07/29 9:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoIntegrantesEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Consultar]
	@IdEntidad int = NULL,@Observaciones NVARCHAR(200) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoIntegrantesEntidad, IdEntidad, NroRevision, Observaciones, ISNULL(ConfirmaYAprueba,0) AS ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoIntegrantesEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
  AND (@Observaciones IS NULL OR Observaciones = @Observaciones)
  AND (@ConfirmaYAprueba IS NULL OR ConfirmaYAprueba = @ConfirmaYAprueba)
 ORDER BY FechaCrea DESC
END







GO


