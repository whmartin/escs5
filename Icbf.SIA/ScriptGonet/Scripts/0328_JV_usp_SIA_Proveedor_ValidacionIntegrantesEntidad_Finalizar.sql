USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Proveedor_ValidacionIntegrantesEntidad_Finalizar]    Script Date: 08/08/2014 12:42:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Proveedor_ValidacionIntegrantesEntidad_Finalizar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Proveedor_ValidacionIntegrantesEntidad_Finalizar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Proveedor_ValidacionIntegrantesEntidad_Finalizar]    Script Date: 08/08/2014 12:42:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde
-- Create date:  09/AGO/2014
-- Description:	Procedimiento almacenado que Finaliza el M�dulo Integrantes
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_ValidacionIntegrantesEntidad_Finalizar]

	@IdEntidad INT
	
AS
BEGIN


	update Proveedor.ValidacionIntegrantesEntidad    
	set Finalizado = 1
	from Proveedor.ValidacionIntegrantesEntidad e
			inner join Proveedor.ValidarInfoIntegrantesEntidad v
					on v.IdEntidad = e.IdEntidad AND v.NroRevision=e.NroRevision
	where e.IdEntidad = @IdEntidad
		and e.IdEstadoValidacionIntegrantes = 4  -- SI ESTA VALIDADO
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMO CON UN SI

END
GO


