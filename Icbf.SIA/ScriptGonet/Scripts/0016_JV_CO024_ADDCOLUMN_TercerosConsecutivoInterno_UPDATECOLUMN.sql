/**
Autor: Juan Carlos Valverde S�mano
Fecha Creaci�n: 25-Febrero-2014
Descripci�n: En base al CO_024, para el requerimiento de agregar a la tabla
de Terceros la nueva columna "ConsecutivoInterno"; el presente script sirve para
crear el consecutivo interno de los Terceros que ya se encuentran registrados.
**/


USE Oferentes
GO

IF NOT EXISTS(
  SELECT object_id 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[Oferente].[TERCERO]') 
         AND name = 'ConsecutivoInterno')
         BEGIN
			ALTER TABLE Oferente.TERCERO
			ADD   ConsecutivoInterno VARCHAR(16)			
         END