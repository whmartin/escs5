USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]    Script Date: 06/25/2014 12:05:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]    Script Date: 06/25/2014 12:05:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdTemporal y TipoPersona y TipoSector
-- =============================================


CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector] 
( @IdTemporal VARCHAR(20), @TipoPersona varchar(2), @TipoSector varchar(2), @TipoEntidad varchar(2) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionProveedores';
DECLARE @TBLDOCS TABLE
(IDROW INT IDENTITY,
IdDocAdjunto INT,
IdEntidad INT,
NombreDocumento NVARCHAR(256),
LinkDocumento NVARCHAR(256),
Observaciones NVARCHAR(MAX),
Obligatorio INT,
UsuarioCrea NVARCHAR(256),
FechaCrea DATETIME,
UsuarioModifica NVARCHAR(256),
FechaModifica DATETIME,
IdTipoDocumento INT,
MaxPermitidoKB INT,
ExtensionesPermitidas NVARCHAR(50),
IdTemporal NVARCHAR(20))

INSERT INTO @TBLDOCS
SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '3' THEN tdp.ObligConsorcio
								WHEN '4' THEN tdp.ObligUnionTemporal
								WHEN '2' THEN CASE @TipoSector	
									WHEN '1' THEN CASE @TipoEntidad
										WHEN '1' THEN CASE  tdp.ObligSectorPrivado
											WHEN 1 THEN tdp.ObligEntNacional
											ELSE tdp.ObligSectorPrivado
											END
										WHEN '2' THEN CASE tdp.ObligSectorPrivado 
											WHEN 1 THEN tdp.ObligEntExtranjera
											ELSE tdp.ObligSectorPrivado
											END
										ELSE tdp.ObligSectorPrivado
										END 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE    d.Activo=1 AND (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTemporal = @IdTemporal
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
             					WHEN '3' THEN tdp.ObligConsorcio
								WHEN '4' THEN tdp.ObligUnionTemporal
								WHEN '2' THEN CASE @TipoSector	
									WHEN '1' THEN CASE @TipoEntidad
										WHEN '1' THEN CASE  tdp.ObligSectorPrivado
											WHEN 1 THEN tdp.ObligEntNacional
											ELSE tdp.ObligSectorPrivado
											END
										WHEN '2' THEN CASE tdp.ObligSectorPrivado 
											WHEN 1 THEN tdp.ObligEntExtranjera
											ELSE tdp.ObligSectorPrivado
											END
										ELSE tdp.ObligSectorPrivado
										END 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
	AND tdp.estado=1
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento




DECLARE @nRegistros Int --Almacena la cantidad de registro que retorna la consulta.
SET @nRegistros=(SELECT COUNT(*) FROM @TBLDOCS)
DECLARE @nWhile Int --Almacenará la cantidad de veces que se esta recorriendo en el Bucle.
SET @nWhile=1

--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdDoc INT
SET @IdDoc=(SELECT IdDocAdjunto FROM @TBLDOCS WHERE IDROW=@nWhile)

UPDATE @TBLDOCS
SET LinkDocumento=ISNULL((SELECT LINKDOCUMENTO FROM PROVEEDOR.DocDatosBasicoProv WHERE IDDOCADJUNTO=@IdDoc),'')
WHERE IdDocAdjunto=@IdDoc

SET @nWhile=@nWhile+1
END

SELECT IDROW,
IdDocAdjunto,
IdEntidad,
NombreDocumento,
LinkDocumento,
Observaciones,
Obligatorio,
UsuarioCrea,
FechaCrea,
UsuarioModifica,
FechaModifica,
IdTipoDocumento,
MaxPermitidoKB,
ExtensionesPermitidas,
IdTemporal
FROM @TBLDOCS
GO


