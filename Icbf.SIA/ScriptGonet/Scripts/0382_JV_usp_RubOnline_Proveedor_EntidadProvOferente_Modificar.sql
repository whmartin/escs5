USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 08/25/2014 09:14:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 08/25/2014 09:14:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/** =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que actualiza un(a) EntidadProvOferente
-- Desarrollador Modificaci�n: Juan Carlos Valverde S�mano
-- Fecha modificaci�n: 26-03-2014
-- Descripci�n: en base a un requerimiento del control de cambios 016, aqu� se agregan las lineas para colocar en
-- NULL la columna ConfirmaYAprueba de la tabla PROVEEDOR.ValidarInfoDatosBasicosEntidad. ya que cuando se realiza una 
-- Modificaci�n el estado del proveedor cambia, pero tambi�n debe cambiar el estado de el m�dulo de informaci�n b�sica.
--Modificado por: Juan Carlos Valverde S�mano
--Fecha Modificaci�n: 20/03/2014
--Descripci�n: En base a un requerimiento de Mejors del CO016 se agrega lo siguiente:
--el procedimiento recibe los parametros primer nombre, segundo nombre, primer apeliido, segundo apellido y 
-- correo electr�nico para actualizarlos en TERCEROS.(Por si caso hubo alguna modificaci�n al momento de editar
-- los datos basicos del proveedor.
-- Modificaco por: Juan Carlos Valverde S�mano
-- Fecha:25/AGO/2014
-- Descripci�n: Se ha colocado que cuando un proveedore esta en VALIDADO y se modifiquen sus datos, entoncces
-- aparte de cambiar al estado Por validar, ahora se coloca que para el mod. Datos Basicos se cree
-- una nueva revisi�n con el mismo n�mero de consecutivo que la �ltima pero con ConfirmaYAprueba=NULL
-- Y en Observaciones ="Actualiz� datos. Pendiente Validaci�n"-
-- Y tambi�n se incrementa en 1 el contador NroRevisi�n que est� en la entidad PROVEEDOR.EntidadProvOferente-
-- =============================================
**/
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
		@IdEntidad INT,	
		@TipoEntOfProv BIT= NULL,	
		@IdTercero INT= NULL,	
		@IdTipoCiiuPrincipal INT = NULL,	
		@IdTipoCiiuSecundario INT= NULL,	
		@IdTipoSector INT = NULL,	
		@IdTipoClaseEntidad INT = NULL,	
		@IdTipoRamaPublica INT = NULL,	
		@IdTipoNivelGob INT = NULL,	
		@IdTipoNivelOrganizacional INT = NULL,	
		@IdTipoCertificadorCalidad INT = NULL,	
		@FechaCiiuPrincipal DATETIME = NULL,	
		@FechaCiiuSecundario DATETIME = NULL,	
		@FechaConstitucion DATETIME = NULL,	
		@FechaVigencia DATETIME = NULL,	
		@FechaMatriculaMerc DATETIME = NULL,	
		@FechaExpiracion DATETIME = NULL,	
		@TipoVigencia BIT = NULL,	
		@ExenMatriculaMer BIT = NULL,	
		@MatriculaMercantil NVARCHAR(20) = NULL,	
		@ObserValidador NVARCHAR(256) = NULL,	
		@AnoRegistro INT = NULL,	
		@IdEstado INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL,
		@Finalizado bit = NULL,
		@PrimerNombre NVARCHAR(256) ='',
		@SegundoNombre NVARCHAR(256) ='',
		@PrimerApellido NVARCHAR(256)='',
		@SegundoApellido NVARCHAR(256)='',
		@CorreoElectronico NVARCHAR(256) = '',
		@IdTemporal VARCHAR(20) = NULL,
		@NumIntegrantes INT = NULL
		
		
AS
BEGIN
DECLARE @finalizado_UPDATE bit 
IF(@Finalizado is NULL)
BEGIN
SET @finalizado_UPDATE=(SELECT Finalizado FROM Proveedor.EntidadProvOferente WHERE IdEntidad = @IdEntidad);
END
ELSE
BEGIN
SET @finalizado_UPDATE=@Finalizado
END

----------Almacenamos el estado de datos Basicos antes del UPDATE
DECLARE @VarIdEstadoDatosBasicos INT
SELECT @VarIdEstadoDatosBasicos=IdEstado 
FROM PROVEEDOR.EntidadProvOferente
WHERE IdEntidad=@IdEntidad
----------------------------------------------------
	UPDATE Proveedor.EntidadProvOferente 
			SET TipoEntOfProv = ISNULL(@TipoEntOfProv,TipoEntOfProv) ,
			    IdTercero = ISNULL(@IdTercero,IdTercero), 
			    IdTipoCiiuPrincipal =ISNULL( @IdTipoCiiuPrincipal, IdTipoCiiuPrincipal ),
				IdTipoCiiuSecundario = @IdTipoCiiuSecundario,
				IdTipoSector =ISNULL( @IdTipoSector, IdTipoSector ),
				IdTipoClaseEntidad =ISNULL( @IdTipoClaseEntidad, IdTipoClaseEntidad ),
				IdTipoRamaPublica =ISNULL( @IdTipoRamaPublica, IdTipoRamaPublica ),
				IdTipoNivelGob =ISNULL( @IdTipoNivelGob, IdTipoNivelGob ),
				IdTipoNivelOrganizacional =ISNULL( @IdTipoNivelOrganizacional, IdTipoNivelOrganizacional ),
				IdTipoCertificadorCalidad =ISNULL( @IdTipoCertificadorCalidad, IdTipoCertificadorCalidad ),
				FechaCiiuPrincipal =ISNULL( @FechaCiiuPrincipal, FechaCiiuPrincipal ),
				FechaCiiuSecundario = @FechaCiiuSecundario,
				FechaConstitucion =ISNULL( @FechaConstitucion, FechaConstitucion ),
				FechaVigencia =ISNULL( @FechaVigencia, FechaVigencia ),
				FechaMatriculaMerc =ISNULL( @FechaMatriculaMerc, FechaMatriculaMerc ),
				FechaExpiracion =ISNULL( @FechaExpiracion, FechaExpiracion ),
				TipoVigencia =ISNULL( @TipoVigencia, TipoVigencia ),
				ExenMatriculaMer =ISNULL( @ExenMatriculaMer, ExenMatriculaMer ),
				MatriculaMercantil =ISNULL( @MatriculaMercantil, MatriculaMercantil ),
				ObserValidador =ISNULL( @ObserValidador, ObserValidador ),
				AnoRegistro =ISNULL( @AnoRegistro, AnoRegistro ),
				IdEstado =ISNULL( @IdEstado, IdEstado ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE(),
			    Finalizado=@finalizado_UPDATE,
			    NumIntegrantes= @NumIntegrantes
			   WHERE IdEntidad = @IdEntidad
			   
			   
			   --------//----------------
			   /**
			   Ajuste ConfirmaYAPrueba=NULL y Observaciones ="Actualiz� datos. Pendiente validaci�n"
			   ... cuando se modifica datos basicos y ya estaba validado.
			    **/
			   IF(@VarIdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
			   WHERE Descripcion='VALIDADO'))
			   BEGIN
					
					INSERT INTO PROVEEDOR.ValidarInfoDatosBasicosEntidad
					(NroRevision,IdEntidad,ConfirmaYAprueba,Observaciones,FechaCrea,UsuarioCrea,FechaModifica,UsuarioModifica,CorreoEnviado)
					SELECT NroRevision, IdEntidad, NULL, 'Actualiz� datos. Pendiente Validaci�n.',GETDATE(),@UsuarioModifica,NULL,NULL,0 FROM 
					PROVEEDOR.ValidarInfoDatosBasicosEntidad
					WHERE IdEntidad=@IdEntidad AND NroRevision =(SELECT  MAX(NroRevision) FROM 
					PROVEEDOR.ValidarInfoDatosBasicosEntidad WHERE IdEntidad=@IdEntidad)
					
					UPDATE PROVEEDOR.EntidadProvOferente
					SET NroRevision=NroRevision +1
					WHERE IdEntidad=@IdEntidad

			   END
			   

			   
			   ----------------//--------------------------
			   /*ACTUALIZA DATOS DE TERCERO*/
			   
			   	UPDATE Oferente.TERCERO
   SET
   PRIMERNOMBRE=@PrimerNombre,
   SEGUNDONOMBRE=@SegundoNombre,
   PRIMERAPELLIDO=@PrimerApellido,
   SEGUNDOAPELLIDO=@SegundoApellido,
   CORREOELECTRONICO=@CorreoElectronico,
   USUARIOMODIFICA=@UsuarioModifica,
   FECHAMODIFICA=GETDATE()
   WHERE IDTERCERO=@IdTercero
			
   

-----Actualizar el Documento que viene con IDTemporal, colocarle el ID del tercero-------
-----Y actualizar la columna activo para el o los documentos anteriores--------------------
DECLARE @TBL_DOCS_ORIGINAL TABLE
(IDROW INT IDENTITY, IDDOCADJUNTO INT,IDTERCERO INT,IDDOCUMENTO INT,IdTemporal NVARCHAR(20),ACTIVO BIT)

DECLARE @TBL_DOCS_TEMPORAL TABLE
(IDROW INT IDENTITY, IDDOCADJUNTO INT,IDTERCERO INT,IDDOCUMENTO INT,IdTemporal NVARCHAR(20),ACTIVO BIT)

INSERT INTO @TBL_DOCS_ORIGINAL
	SELECT IDDOCADJUNTO,IDTERCERO,IDDOCUMENTO,IdTemporal,ACTIVO FROM [Proveedor].[DocAdjuntoTercero] 
	WHERE IDTERCERO=@IdTercero AND
	IDDOCUMENTO IN (SELECT IDDOCUMENTO FROM [Proveedor].[DocAdjuntoTercero] WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal

INSERT INTO @TBL_DOCS_TEMPORAL
	SELECT IDDOCADJUNTO,IDTERCERO,IDDOCUMENTO,IdTemporal,ACTIVO FROM [Proveedor].[DocAdjuntoTercero] 
	WHERE IDTERCERO IS NULL
	AND IDDOCUMENTO IN (SELECT IDDOCUMENTO FROM [Proveedor].[DocAdjuntoTercero] WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal AND Activo=1
	
DECLARE @nRegistros Int --Almacena la cantidad de registro que retorna la consulta.
SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_ORIGINAL)
DECLARE @nWhile Int --Almacenar� la cantidad de veces que se esta recorriendo en el Bucle.
DECLARE @IDDOCTEMP INT
DECLARE @IDDOCORIGINAL INT
SET @nWhile=1

--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdDoc INT
SELECT @IDDOCORIGINAL=IDDOCADJUNTO, 
@IdDoc=IDDOCUMENTO FROM @TBL_DOCS_ORIGINAL WHERE IDROW=@nWhile
IF EXISTS(SELECT IDROW FROM @TBL_DOCS_TEMPORAL WHERE IDDOCUMENTO=@IdDoc)
BEGIN
SET @IDDOCTEMP=(SELECT IDDOCADJUNTO FROM @TBL_DOCS_TEMPORAL WHERE IDDOCUMENTO=@IdDoc)
UPDATE [Proveedor].[DocAdjuntoTercero] 
SET Activo=0 WHERE IDDOCADJUNTO=@IDDOCORIGINAL

UPDATE [Proveedor].[DocAdjuntoTercero] 
SET IDTERCERO=@IdTercero WHERE IDDOCADJUNTO=@IDDOCTEMP
END

SET @nWhile=@nWhile+1
END

SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_TEMPORAL)
SET @nWhile=1
--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdTipo_Doc INT
SELECT @IDDOCTEMP=IDDOCADJUNTO, 
@IdTipo_Doc=IDDOCUMENTO FROM @TBL_DOCS_TEMPORAL WHERE IDROW=@nWhile
IF NOT EXISTS(SELECT * FROM @TBL_DOCS_ORIGINAL WHERE IDDOCUMENTO=@IdTipo_Doc)
BEGIN
UPDATE [Proveedor].[DocAdjuntoTercero] 
SET IDTERCERO=@IdTercero WHERE IDDOCADJUNTO=@IDDOCTEMP
END
SET @nWhile=@nWhile+1
END

------------------------------------------------------------------------------------------
-----Actualizar el Documento que viene con IDTemporal, colocarle el ID del tercero-------
-----Y actualizar la columna activo para el o los documentos anteriores--------------------

DECLARE @TBL_DOCS_ORIGINAL_BASIC TABLE
(IDROW INT IDENTITY, IDDOCADJUNTO INT,IDTERCERO INT,IDDOCUMENTO INT,IdTemporal NVARCHAR(20),ACTIVO BIT)

DECLARE @TBL_DOCS_TEMPORAL_BASIC TABLE
(IDROW INT IDENTITY, IDDOCADJUNTO INT,IDTERCERO INT,IDDOCUMENTO INT,IdTemporal NVARCHAR(20),ACTIVO BIT)


INSERT INTO @TBL_DOCS_ORIGINAL_BASIC
	SELECT IdDocAdjunto, IdEntidad, IdTipoDocumento,IdTemporal,Activo FROM [Proveedor].[DocDatosBasicoProv]  
	WHERE IdEntidad=@IdEntidad AND
	IdTipoDocumento IN (SELECT IdTipoDocumento FROM [Proveedor].[DocDatosBasicoProv]   WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal

INSERT INTO @TBL_DOCS_TEMPORAL_BASIC
	SELECT IdDocAdjunto, IdEntidad, IdTipoDocumento,IdTemporal,Activo FROM [Proveedor].[DocDatosBasicoProv]  
	WHERE IdEntidad IS NULL
	AND IdTipoDocumento IN (SELECT IdTipoDocumento FROM [Proveedor].[DocDatosBasicoProv]  WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal AND Activo=1


SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_ORIGINAL_BASIC)
SET @nWhile=1

--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN
DECLARE @IdTipoDoc INT
SELECT @IDDOCORIGINAL=IDDOCADJUNTO, 
@IdTipoDoc=IDDOCUMENTO FROM @TBL_DOCS_ORIGINAL_BASIC WHERE IDROW=@nWhile
IF EXISTS(SELECT IDROW FROM @TBL_DOCS_TEMPORAL_BASIC WHERE IDDOCUMENTO=@IdTipoDoc)
BEGIN
SET @IDDOCTEMP=(SELECT IDDOCADJUNTO FROM @TBL_DOCS_TEMPORAL_BASIC WHERE IDDOCUMENTO=@IdTipoDoc)
UPDATE [Proveedor].[DocDatosBasicoProv]
SET Activo=0 WHERE IDDOCADJUNTO=@IDDOCORIGINAL

UPDATE [Proveedor].[DocDatosBasicoProv]
SET IdEntidad=@IdEntidad WHERE IDDOCADJUNTO=@IDDOCTEMP
END

SET @nWhile=@nWhile+1
END	

SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_TEMPORAL_BASIC)
SET @nWhile=1
--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @Id_TipoDoc INT
SELECT @IDDOCTEMP=IDDOCADJUNTO, 
@Id_TipoDoc=IDDOCUMENTO FROM @TBL_DOCS_TEMPORAL_BASIC WHERE IDROW=@nWhile
IF NOT EXISTS(SELECT * FROM @TBL_DOCS_ORIGINAL_BASIC WHERE IDDOCUMENTO=@Id_TipoDoc)
BEGIN
UPDATE [Proveedor].[DocDatosBasicoProv]
SET IdEntidad=@IdEntidad WHERE IDDOCADJUNTO=@IDDOCTEMP
END
SET @nWhile=@nWhile+1
END
------------------------------------------------------------------------------------------
END



GO


