USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelGobierno]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelGobierno]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonz�lez
-- Create date:  01/04/2014 17:17 PM
-- Description:	Procedimiento almacenado que consulta Niveles de Gobiernno por RamaEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelGobierno]

	@IdRamaEstructura INT
AS
BEGIN

	SELECT	DISTINCT(RelRamaGob.IdNivelGobierno),
			CodigoNivelGobierno,
			Descripcion
	FROM	PROVEEDOR.Rel_RamaGobNivelOrgYTipoEnt RelRamaGob
	JOIN
			Proveedor.NivelGobierno NivelGob
	ON
			RelRamaGob.IdNivelGobierno = NivelGob.IdNivelGobierno
	WHERE	
			RelRamaGob.IdRamaEstructura = @IdRamaEstructura
			
	ORDER BY
			Descripcion ASC

END