USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]    Script Date: 04/04/2014 16:10:55 ******/
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]    Script Date: 04/04/2014 16:10:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		JHON DIAZ
-- Create date:  5/23/2013 5:33:49 PM
-- Description:	Procedimiento almacenado que consulta un(a) Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]
@IdTercero INT
AS
BEGIN
SELECT [IDTERCERO]
      ,[IDTIPODOCIDENTIFICA]
      ,[IDESTADOTERCERO]
      ,[IdTipoPersona]
      ,[NUMEROIDENTIFICACION]
      ,[DIGITOVERIFICACION]
      ,[CORREOELECTRONICO]
      ,[PRIMERNOMBRE]
      ,[SEGUNDONOMBRE]
      ,[PRIMERAPELLIDO]
      ,[SEGUNDOAPELLIDO]
      ,[RAZONSOCIAL]
      ,[FECHAEXPEDICIONID]
      ,[FECHANACIMIENTO]
      ,[SEXO]
      ,[FECHACREA]
      ,[USUARIOCREA]
      ,[FECHAMODIFICA]
      ,[USUARIOMODIFICA]
	  ,[ProviderUserKey]
FROM [Oferente].[Tercero]
WHERE IdTercero = @IdTercero
     
END
GO


