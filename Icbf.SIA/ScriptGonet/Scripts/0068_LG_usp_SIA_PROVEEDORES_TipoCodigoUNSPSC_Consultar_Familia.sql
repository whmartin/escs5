USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Familia]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Familia]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  02/26/2014 3:16:21 PM
-- Description:	Procedimiento almacenado que consulta un(a) Familias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Familia]
	@CodigoSegmento NVARCHAR(50)
AS
BEGIN
 SELECT DISTINCT(CodigoFamilia),
		NombreFamilia
  FROM	PROVEEDOR.TipoCodigoUNSPSC
  WHERE	CodigoSegmento = @CodigoSegmento
END