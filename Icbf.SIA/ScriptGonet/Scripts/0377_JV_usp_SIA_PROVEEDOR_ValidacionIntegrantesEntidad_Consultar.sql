USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_ValidacionIntegrantesEntidad_Consultar]    Script Date: 08/20/2014 10:04:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_ValidacionIntegrantesEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_ValidacionIntegrantesEntidad_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_ValidacionIntegrantesEntidad_Consultar]    Script Date: 08/20/2014 10:04:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 07/AGO/2014
-- Description:	Realiza una consulta al registro de validaci�n de Integrantes para una Entidad.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_ValidacionIntegrantesEntidad_Consultar]
@IdEntidad INT
AS
BEGIN
	SELECT 
	IdValidacionIntegrantesEntidad
	,IdEntidad
	,IdEstadoValidacionIntegrantes
	,(SELECT Descripcion FROM PROVEEDOR.EstadoProveedor
	WHERE IdEstadoProveedor=IdEstadoValidacionIntegrantes) AS 'EstadoValidacion'
	,NroRevision
	,Finalizado
	,FechaCrea
	,UsuarioCrea
	,FechaModifica
	,UsuarioModifica
	,CorreoEnviado
	FROM 
	PROVEEDOR.ValidacionIntegrantesEntidad
	WHERE IdEntidad=@IdEntidad
	


END

GO


