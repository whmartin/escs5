USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 09-AGO-2014
Descripci�n: Agrega a la Entidad PROVEEDOR.ValidarInfoIntegrantesEntidad la columna
Activo, para as� poder llevar el historial de Revisiones del m�dulo integrantes
**/
IF NOT EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'ValidarInfoIntegrantesEntidad' and COLUMN_NAME = 'Activo'
)
BEGIN
	ALTER TABLE PROVEEDOR.ValidarInfoIntegrantesEntidad
	ADD Activo BIT DEFAULT(1)
END


