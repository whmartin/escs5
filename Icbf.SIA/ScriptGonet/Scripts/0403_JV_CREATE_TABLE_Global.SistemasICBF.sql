USE [SIA]
GO

/****** Object:  Table [Global].[SistemasICBF]    Script Date: 11/06/2014 16:49:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Global].[SistemasICBF]') AND type in (N'U'))
DROP TABLE [Global].[SistemasICBF]
GO

/****** Object:  Table [Global].[SistemasICBF]    Script Date: 11/06/2014 16:49:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 06/NOV/2014
Descripci�n: Crea la entidad que almacena los sistemas que pertenecen al ICBF
**/
CREATE TABLE [Global].[SistemasICBF](
	[IdSistema] [int] IDENTITY(1,1) NOT NULL,
	[Sistema] [nvarchar](200) NULL,
	[NombreCompleto] [nvarchar](500) NULL,
 CONSTRAINT [PK_Global.SistemasICBF] PRIMARY KEY CLUSTERED 
(
	[IdSistema] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


