USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 04/02/2014 06:43:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 04/02/2014 06:43:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Faiber Losada Zu�iga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- Modificaci�n Por: Juan Carlos Valverde S�mano
--Fecha Modificaci�n: 02/04/2014
--Descripci�n:  Se agreo al Select el providerKey del TERCERO
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT P.IdEntidad
      ,P.ConsecutivoInterno
      ,P.TipoEntOfProv
      ,P.IdTercero
      ,P.IdTipoCiiuPrincipal
      ,P.IdTipoCiiuSecundario
      ,P.IdTipoSector
      ,P.IdTipoClaseEntidad
      ,P.IdTipoRamaPublica
      ,P.IdTipoNivelGob
      ,P.IdTipoNivelOrganizacional
      ,P.IdTipoCertificadorCalidad
      ,P.FechaCiiuPrincipal
      ,P.FechaCiiuSecundario
      ,P.FechaConstitucion
      ,P.FechaVigencia
      ,P.FechaMatriculaMerc
      ,P.FechaExpiracion
      ,P.TipoVigencia
      ,P.ExenMatriculaMer
      ,P.MatriculaMercantil
      ,P.ObserValidador
      ,P.AnoRegistro
      ,P.IdEstado
      ,P.UsuarioCrea
      ,P.FechaCrea
      ,P.UsuarioModifica
      ,P.FechaModifica
      ,P.IdAcuerdo
      ,P.Finalizado
      ,T.ProviderUserKey
 FROM [Proveedor].[EntidadProvOferente] P
 INNER JOIN Oferente.TERCERO T ON P.IdTercero=T.IDTERCERO 
 WHERE  P.IdEntidad = @IdEntidad
END
GO


