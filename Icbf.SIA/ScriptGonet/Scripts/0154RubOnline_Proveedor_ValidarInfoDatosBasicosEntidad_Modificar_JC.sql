USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]    Script Date: 04/11/2014 10:40:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]    Script Date: 04/11/2014 10:40:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoDatosBasicosEntidad
-- Modificacion: 2014/03/19
-- Autor: Leticia Elizabeth Gonzalez
-- Descripción: Asigna ConfirmaYAprueba = NULL
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]
		@IdValidarInfoDatosBasicosEntidad INT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(200),	@ConfirmaYAprueba BIT = NULL, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoDatosBasicosEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdEntidad = @IdEntidad
		and NroRevision = @NroRevision			
		and IdValidarInfoDatosBasicosEntidad = @IdValidarInfoDatosBasicosEntidad
END




GO


