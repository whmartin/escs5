USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]    Script Date: 08/12/2014 11:46:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]    Script Date: 08/12/2014 11:46:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]
		@IdInfoAdmin INT OUTPUT, 	
		@IdVigencia INT=1,	
		@IdEntidad INT=NULL,	
		@IdTipoRegTrib INT=NULL,	
		@IdTipoOrigenCapital INT=NULL,	
		@IdTipoActividad INT=NULL,	
		@IdTipoEntidad INT=NULL,	
		@IdTipoNaturalezaJurid INT=NULL,	
		@IdTipoRangosTrabajadores INT=NULL,	
		@IdTipoRangosActivos INT=NULL,	
		@IdRepLegal INT=NULL,	
		@IdTipoCertificaTamano INT=NULL,
		@IdTipoEntidadPublica INT=NULL,	
		@IdDepartamentoConstituida INT=NULL,	
		@IdMunicipioConstituida INT=NULL,	
		@IdDepartamentoDirComercial INT=NULL,	
		@IdMunicipioDirComercial INT=NULL,	
		@DireccionComercial NVARCHAR(50)=NULL,	
		@IdZona INT=NULL,
		@NombreComercial NVARCHAR(128)=NULL,	
		@NombreEstablecimiento NVARCHAR(128)=NULL,	
		@Sigla NVARCHAR(50)=NULL,	
		@PorctjPrivado INT=NULL,	
		@PorctjPublico INT=NULL,	
		@SitioWeb NVARCHAR(128)=NULL,	
		@NombreEntidadAcreditadora NVARCHAR (128)=NULL,	
		@Organigrama BIT=NULL,	
		@TotalPnalAnnoPrevio numeric(10)=NULL,	
		@VincLaboral numeric(10)=NULL,	
		@PrestServicios numeric(10)=NULL,	
		@Voluntariado numeric(10)=NULL,	
		@VoluntPermanente numeric(10)=NULL,	
		@Asociados numeric(10)=NULL,	
		@Mision numeric(10)=NULL,	
		@PQRS BIT=NULL,	
		@GestionDocumental BIT=NULL,	
		@AuditoriaInterna BIT=NULL,	
		@ManProcedimiento BIT=NULL,	
		@ManPracticasAmbiente BIT=NULL,	
		@ManComportOrg BIT=NULL,	
		@ManFunciones BIT=NULL,	
		@ProcRegInfoContable BIT=NULL,	
		@PartMesasTerritoriales BIT=NULL,	
		@PartAsocAgremia BIT=NULL,	
		@PartConsejosComun BIT=NULL,	
		@ConvInterInst BIT=NULL,	
		@ProcSeleccGral BIT=NULL,	
		@ProcSeleccEtnico BIT=NULL,	
		@PlanInduccCapac BIT=NULL,	
		@EvalDesemp BIT=NULL,	
		@PlanCualificacion BIT=NULL,	
		@NumSedes INT=NULL,	
		@SedesPropias BIT=NULL, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.InfoAdminEntidad(IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, IdTipoRangosTrabajadores, IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, IdDepartamentoDirComercial, IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, PorctjPublico, SitioWeb, NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, Mision, PQRS, GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, PartMesasTerritoriales, PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, NumSedes, SedesPropias, UsuarioCrea, FechaCrea)
					  VALUES(@IdVigencia, @IdEntidad, @IdTipoRegTrib, @IdTipoOrigenCapital, @IdTipoActividad, @IdTipoEntidad, @IdTipoNaturalezaJurid, @IdTipoRangosTrabajadores, @IdTipoRangosActivos, @IdRepLegal, @IdTipoCertificaTamano, @IdTipoEntidadPublica, @IdDepartamentoConstituida, @IdMunicipioConstituida, @IdDepartamentoDirComercial, @IdMunicipioDirComercial, @DireccionComercial, @IdZona, @NombreComercial, @NombreEstablecimiento, @Sigla, @PorctjPrivado, @PorctjPublico, @SitioWeb, @NombreEntidadAcreditadora, @Organigrama, @TotalPnalAnnoPrevio, @VincLaboral, @PrestServicios, @Voluntariado, @VoluntPermanente, @Asociados, @Mision, @PQRS, @GestionDocumental, @AuditoriaInterna, @ManProcedimiento, @ManPracticasAmbiente, @ManComportOrg, @ManFunciones, @ProcRegInfoContable, @PartMesasTerritoriales, @PartAsocAgremia, @PartConsejosComun, @ConvInterInst, @ProcSeleccGral, @ProcSeleccEtnico, @PlanInduccCapac, @EvalDesemp, @PlanCualificacion, @NumSedes, @SedesPropias, @UsuarioCrea, GETDATE())
	SELECT @IdInfoAdmin = SCOPE_IDENTITY() 			
END


GO


