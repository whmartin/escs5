USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 06/NOV/2014
Descripci�n: Inserta Los Sistemas Pertenecientes al ICBF
**/
IF NOT EXISTS(SELECT IdSistema FROM Global.SistemasICBF WHERE Sistema='Oferentes')
BEGIN
	INSERT INTO Global.SistemasICBF	(Sistema, NombreCompleto)
	VALUES('Oferentes','Oferentes')
END


IF NOT EXISTS(SELECT IdSistema FROM Global.SistemasICBF WHERE Sistema='SIA')
BEGIN
	INSERT INTO Global.SistemasICBF	(Sistema, NombreCompleto)
	VALUES('SIA','Sistema de informaci�n de Apoyo')
END