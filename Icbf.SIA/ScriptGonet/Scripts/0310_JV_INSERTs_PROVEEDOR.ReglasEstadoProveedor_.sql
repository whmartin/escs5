USE [SIA]
GO

/**
Autor: Juan Carlos Valverde S�mano
Fecha: 30-Julio.2014
Descripci�n: Crea las reglas de estados de validaci�n para Proveedores de Tipo
Consorcio o Uni�n Temporal.
**/
IF NOT EXISTS(SELECT IdValidacionEstado FROM PROVEEDOR.ReglasEstadoProveedor
WHERE IdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='REGISTRADO')
AND IdEstadoProveedor =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='REGISTRADO') AND
IdEstadoIntegrantes = (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes WHERE
Descripcion='REGISTRADO') AND IdEstadoDatosExperiencia IS NULL AND IdEstadoDatosFinancieros IS NULL)
BEGIN
	INSERT INTO PROVEEDOR.ReglasEstadoProveedor
	(IdEstadoDatosBasicos, IdEstadoIntegrantes, IdEstadoProveedor, Estado, UsuarioCrea, FechaCrea)
	VALUES(
	(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
	WHERE Descripcion='REGISTRADO'),CASE WHEN 'REGISTRADO'='' THEN 0 ELSE (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='REGISTRADO') END,(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE
	Descripcion='REGISTRADO'), 1,'Administrador',GETDATE())
END
IF NOT EXISTS(SELECT IdValidacionEstado FROM PROVEEDOR.ReglasEstadoProveedor
WHERE IdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR')
AND IdEstadoProveedor =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR') AND
IdEstadoIntegrantes = (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes WHERE
Descripcion='POR VALIDAR') AND IdEstadoDatosExperiencia IS NULL AND IdEstadoDatosFinancieros IS NULL)
BEGIN
	INSERT INTO PROVEEDOR.ReglasEstadoProveedor
	(IdEstadoDatosBasicos, IdEstadoIntegrantes, IdEstadoProveedor, Estado, UsuarioCrea, FechaCrea)
	VALUES(
	(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
	WHERE Descripcion='POR VALIDAR'),CASE WHEN 'POR VALIDAR'='' THEN 0 ELSE (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='POR VALIDAR') END,(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE
	Descripcion='POR VALIDAR'), 1,'Administrador',GETDATE())
END
IF NOT EXISTS(SELECT IdValidacionEstado FROM PROVEEDOR.ReglasEstadoProveedor
WHERE IdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTAR')
AND IdEstadoProveedor =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTAR') AND
IdEstadoIntegrantes = (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes WHERE
Descripcion='POR AJUSTAR') AND IdEstadoDatosExperiencia IS NULL AND IdEstadoDatosFinancieros IS NULL)
BEGIN
	INSERT INTO PROVEEDOR.ReglasEstadoProveedor
	(IdEstadoDatosBasicos, IdEstadoIntegrantes, IdEstadoProveedor, Estado, UsuarioCrea, FechaCrea)
	VALUES(
	(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
	WHERE Descripcion='POR AJUSTAR'),CASE WHEN 'POR AJUSTAR'='' THEN 0 ELSE (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='POR AJUSTAR') END,(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE
	Descripcion='POR AJUSTAR'), 1,'Administrador',GETDATE())
END
IF NOT EXISTS(SELECT IdValidacionEstado FROM PROVEEDOR.ReglasEstadoProveedor
WHERE IdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO')
AND IdEstadoProveedor =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO') AND
IdEstadoIntegrantes = (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes WHERE
Descripcion='VALIDADO') AND IdEstadoDatosExperiencia IS NULL AND IdEstadoDatosFinancieros IS NULL)
BEGIN
	INSERT INTO PROVEEDOR.ReglasEstadoProveedor
	(IdEstadoDatosBasicos, IdEstadoIntegrantes, IdEstadoProveedor, Estado, UsuarioCrea, FechaCrea)
	VALUES(
	(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
	WHERE Descripcion='VALIDADO'),CASE WHEN 'VALIDADO'='' THEN 0 ELSE (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='VALIDADO') END,(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE
	Descripcion='VALIDADO'), 1,'Administrador',GETDATE())
END
IF NOT EXISTS(SELECT IdValidacionEstado FROM PROVEEDOR.ReglasEstadoProveedor
WHERE IdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='REGISTRADO')
AND IdEstadoProveedor =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='REGISTRADO') AND
IdEstadoIntegrantes = 0 AND IdEstadoDatosExperiencia IS NULL AND IdEstadoDatosFinancieros IS NULL)
BEGIN
	INSERT INTO PROVEEDOR.ReglasEstadoProveedor
	(IdEstadoDatosBasicos, IdEstadoIntegrantes, IdEstadoProveedor, Estado, UsuarioCrea, FechaCrea)
	VALUES(
	(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
	WHERE Descripcion='REGISTRADO'),CASE WHEN ''='' THEN 0 ELSE (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='') END,(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE
	Descripcion='REGISTRADO'), 1,'Administrador',GETDATE())
END
IF NOT EXISTS(SELECT IdValidacionEstado FROM PROVEEDOR.ReglasEstadoProveedor
WHERE IdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR')
AND IdEstadoProveedor =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR') AND
IdEstadoIntegrantes = 0 AND IdEstadoDatosExperiencia IS NULL AND IdEstadoDatosFinancieros IS NULL)
BEGIN
	INSERT INTO PROVEEDOR.ReglasEstadoProveedor
	(IdEstadoDatosBasicos, IdEstadoIntegrantes, IdEstadoProveedor, Estado, UsuarioCrea, FechaCrea)
	VALUES(
	(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
	WHERE Descripcion='POR VALIDAR'),CASE WHEN ''='' THEN 0 ELSE (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='') END,(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE
	Descripcion='POR VALIDAR'), 1,'Administrador',GETDATE())
END
IF NOT EXISTS(SELECT IdValidacionEstado FROM PROVEEDOR.ReglasEstadoProveedor
WHERE IdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTAR')
AND IdEstadoProveedor =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTAR') AND
IdEstadoIntegrantes = 0 AND IdEstadoDatosExperiencia IS NULL AND IdEstadoDatosFinancieros IS NULL)
BEGIN
	INSERT INTO PROVEEDOR.ReglasEstadoProveedor
	(IdEstadoDatosBasicos, IdEstadoIntegrantes, IdEstadoProveedor, Estado, UsuarioCrea, FechaCrea)
	VALUES(
	(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
	WHERE Descripcion='POR AJUSTAR'),CASE WHEN ''='' THEN 0 ELSE (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='') END,(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE
	Descripcion='POR AJUSTAR'), 1,'Administrador',GETDATE())
END
IF NOT EXISTS(SELECT IdValidacionEstado FROM PROVEEDOR.ReglasEstadoProveedor
WHERE IdEstadoDatosBasicos=(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO')
AND IdEstadoProveedor =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO') AND
IdEstadoIntegrantes = 0 AND IdEstadoDatosExperiencia IS NULL AND IdEstadoDatosFinancieros IS NULL)
BEGIN
	INSERT INTO PROVEEDOR.ReglasEstadoProveedor
	(IdEstadoDatosBasicos, IdEstadoIntegrantes, IdEstadoProveedor, Estado, UsuarioCrea, FechaCrea)
	VALUES(
	(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
	WHERE Descripcion='VALIDADO'),CASE WHEN ''='' THEN 0 ELSE (SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='') END,(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE
	Descripcion='VALIDADO'), 1,'Administrador',GETDATE())
END