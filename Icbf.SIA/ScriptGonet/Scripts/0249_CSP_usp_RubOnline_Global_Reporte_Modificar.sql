USE [RubOnline]
GO


If (select Count(*) from sysobjects where type='P' and name='usp_RubOnline_Global_Reporte_Modificar')>0 
BEGIN
	DROP PROCEDURE dbo.usp_RubOnline_Global_Reporte_Modificar;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	SP's para CRUD de Global.Reporte
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Reporte_Modificar]
		@IdReporte INT,	@NombreReporte NVARCHAR(512),	@Descripcion NVARCHAR(1024),	@Servidor NVARCHAR(512),	@Carpeta NVARCHAR(512),	@NombreArchivo NVARCHAR(512), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.Reporte SET NombreReporte = @NombreReporte
	                         ,Descripcion = @Descripcion
	                         ,Servidor = @Servidor
	                         ,Carpeta = @Carpeta
	                         ,NombreArchivo = @NombreArchivo
	                         ,UsuarioModifica = @UsuarioModifica
	                         ,FechaModifica = GETDATE() 
	WHERE IdReporte = @IdReporte
END
