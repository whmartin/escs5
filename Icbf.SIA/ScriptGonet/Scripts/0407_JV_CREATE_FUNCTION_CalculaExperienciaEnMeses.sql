USE [SIA]
GO

/****** Object:  UserDefinedFunction [dbo].[CalculaExperienciaEnMeses]    Script Date: 11/26/2014 07:54:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculaExperienciaEnMeses]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CalculaExperienciaEnMeses]
GO


/****** Object:  UserDefinedFunction [dbo].[CalculaExperienciaEnMeses]    Script Date: 11/26/2014 07:54:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 26-NOV-2014
-- Description:	Recibe como parametros la fechaInicio y la FechaFin, y calcula y retorna la experiencia en Meses
-- Teniendo en cuenta que todos los meses son de 30 d�as a excepci�n de febrero que es de 28 d�as.
-- =============================================
CREATE FUNCTION [dbo].[CalculaExperienciaEnMeses] 
(
@fechaInicio AS DATE
,@fechaFin AS DATE
)
RETURNS DECIMAL(10,3)
AS
BEGIN
		
DECLARE @year1 AS INT= DATEPART(year,@fechaInicio)
DECLARE @month1 AS INT= DATEPART(month,@fechaInicio)
DECLARE @day1 AS INT= DATEPART(day,@fechaInicio)
DECLARE @year2 AS INT= DATEPART(year,@fechaFin)
DECLARE @month2 AS INT= DATEPART(month,@fechaFin)
DECLARE @day2 AS INT= (DATEPART(day,@fechaFin)+1)

DECLARE @DifAnios AS INT =0;
DECLARE @DifMeses AS INT =0;
DECLARE @DifDias AS INT =0;
DECLARE @TotalDiasMes AS INT = 30;
DECLARE @strArrMonths AS NVARCHAR(150)='1,2,3,4,5,6,7,8,9,10,11,12,1,2,3,4,5,6,7,8,9,10,11';
--Diferencia En A�os
IF(@year1 != @year2)
BEGIN
	SET @DifAnios=@year2-@year1;
	IF(@month1>@month2)
	BEGIN
		SET @DifAnios=@DifAnios-1;
	END
	IF(@month1=@month2)
	BEGIN
		IF(@day1>(@day2-1))
		BEGIN
			SET @DifAnios=@DifAnios-1;
		END
	END
END

--Diferencia en meses
IF(@month1 != @month2)
BEGIN

	DECLARE @indiceEncontrado AS INT=0;
	DECLARE @TBL_MESES AS TABLE(
	ID INT IDENTITY,
	MES INT)
	INSERT INTO @TBL_MESES
	SELECT splitdata FROM dbo.fnSplitString(@strArrMonths,',')

	SET @indiceEncontrado=(
	SELECT TOP(1) ID FROM @TBL_MESES
	WHERE MES=@month1)
	
	
	DECLARE @IndiceMes2 INT=0;
	
	SET @IndiceMes2=(SELECT TOP(1)ID FROM @TBL_MESES
	WHERE MES=@month2 AND ID>@indiceEncontrado)
	
	SET @DifMeses=(@IndiceMes2-@indiceEncontrado);
	
	IF(@day1>(@day2-1))
	BEGIN
	 SET @DifMeses=@DifMeses-1;
	END
	
END

--Diferencia en D�as
IF(@day1 != (@day2-1))
BEGIN
	DECLARE @TBL_DIAS AS TABLE(
	ID INT IDENTITY,
	DIA INT)

	IF(@month1=2 AND @month2=2)
	BEGIN
		SET @TotalDiasMes=28;
		INSERT INTO @TBL_DIAS
		SELECT splitdata FROM dbo.fnSplitString('1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28',',')
		
	END
	ELSE
	BEGIN
		SET @TotalDiasMes=30;
		INSERT INTO @TBL_DIAS
		SELECT splitdata FROM dbo.fnSplitString('1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30',',')
	END
	DECLARE @IndiceDia1 AS INT =0
	SET @IndiceDia1=(
	SELECT TOP(1) ID FROM @TBL_DIAS
	WHERE DIA=@day1)

	DECLARE @IndiceDia2 AS INT=0
	SET @IndiceDia2=(
	SELECT TOP(1) ID FROM @TBL_DIAS
	WHERE DIA=(@day2-1) AND ID>@IndiceDia1)
	

	SET @DifDias=1+(@IndiceDia2-@IndiceDia1);	
END
ELSE IF (@year1=@year2 AND @month1=@month2)
BEGIN
	SET @DifDias=1;
END

IF(@DifDias=31)
BEGIN
	SET @DifDias=30;
END

DECLARE @ExpMeses AS DECIMAL(10,3)
SET @ExpMeses=(((@DifAnios*12)+@DifMeses) + CONVERT(DECIMAL(10,3),(CONVERT(DECIMAL(10,3),@DifDias)/CONVERT(DECIMAL(10,3),@TotalDiasMes))))
RETURN 	@ExpMeses

END


GO


