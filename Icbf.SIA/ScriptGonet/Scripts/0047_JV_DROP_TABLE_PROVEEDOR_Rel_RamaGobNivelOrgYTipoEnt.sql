USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_Rel_RamaGobNivelOrgYTipoEnt_NivelGobierno]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]'))
ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] DROP CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_NivelGobierno]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_Rel_RamaGobNivelOrgYTipoEnt_NivelOrganizacional]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]'))
ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] DROP CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_NivelOrganizacional]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_Rel_RamaGobNivelOrgYTipoEnt_RamaoEstructura]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]'))
ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] DROP CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_RamaoEstructura]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_Rel_RamaGobNivelOrgYTipoEnt_TipodeentidadPublica]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]'))
ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] DROP CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_TipodeentidadPublica]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rel_RamaGobNivelOrgYTipoEnt_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] DROP CONSTRAINT [DF_Rel_RamaGobNivelOrgYTipoEnt_Estado]
END

GO

USE [SIA]
GO

/****** Object:  Table [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]    Script Date: 04/01/2014 13:24:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]') AND type in (N'U'))
DROP TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]
GO