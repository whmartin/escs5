USE [SIA]
GO
-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date:  16/06/2014
-- Description:	Inserta en Oferente.TipoPersona los tipos CONSORCIO y UNI�N TEMPORAL
-- =============================================

IF NOT EXISTS
(SELECT IdTipoPersona FROM [Oferente].[TipoPersona]
WHERE NombreTipoPersona='CONSORCIO')
BEGIN

INSERT INTO [Oferente].[TipoPersona]
           ([CodigoTipoPersona]
           ,[NombreTipoPersona]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('003'
           ,'CONSORCIO'
           ,1
           ,'Administrador'
           ,GETDATE()
           ,NULL
           ,NULL)
END

IF NOT EXISTS
(SELECT IdTipoPersona FROM [Oferente].[TipoPersona]
WHERE NombreTipoPersona='UNI�N TEMPORAL')
BEGIN

INSERT INTO [Oferente].[TipoPersona]
           ([CodigoTipoPersona]
           ,[NombreTipoPersona]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('004'
           ,'UNI�N TEMPORAL'
           ,1
           ,'Administrador'
           ,GETDATE()
           ,NULL
           ,NULL)
END           