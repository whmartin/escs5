USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar_ConTipoSector]    Script Date: 07/07/2014 23:03:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar_ConTipoSector]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar_ConTipoSector]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar_ConTipoSector]    Script Date: 07/07/2014 23:03:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Juan Carlos Valverde S�mano
-- Create date:  07/07/2014 
-- Description:	Procedimiento Almacenado que consulta a EntidadProvOferente con un TipoSector
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar_ConTipoSector]
	@IdTipoPersona INT = NULL,@Tipoidentificacion INT= NULL,@Identificacion NVARCHAR(256) = NULL,
	@Proveedor NVARCHAR(256) = NULL,@IdEstado INT = NULL,@UsuarioCrea NVARCHAR(128)=NULL,
	@IdTipoSector INT = NULL
AS
BEGIN
 SELECT DISTINCT * FROM (
  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador,EP.UsuarioCrea, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor, EP.IdTipoSector, EP.TipoEntOfProv, T.DIGITOVERIFICACION
  FROM [Proveedor].[EntidadProvOferente] EP 
     INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
     WHERE T.IdTipoPersona=1
UNION 
 SELECT  EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.RAZONSOCIAL ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado ,EP.ObserValidador,EP.UsuarioCrea, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor, EP.IdTipoSector, EP.TipoEntOfProv, T.DIGITOVERIFICACION
  FROM [Proveedor].[EntidadProvOferente] EP 
    INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
    LEFT JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
    INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona
    INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
    INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
    INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
    WHERE T.IdTipoPersona=2 OR T.IdTipoPersona=3 OR T.IdTipoPersona=4
    ) DT
     
    
    WHERE IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN IdTipoPersona ELSE @IdTipoPersona END 
		  AND IDTIPODOCIDENTIFICA = CASE WHEN @Tipoidentificacion IS NULL THEN IDTIPODOCIDENTIFICA ELSE @Tipoidentificacion END 
		  AND NUMEROIDENTIFICACION = CASE WHEN @Identificacion IS NULL THEN NUMEROIDENTIFICACION ELSE @Identificacion END 
		  AND (Razonsocila) like CASE WHEN @Proveedor IS NULL THEN (Razonsocila) ELSE '%'+ @Proveedor +'%' END 
		  AND IdEstadoProveedor = CASE WHEN @IdEstado IS NULL THEN IdEstadoProveedor ELSE @IdEstado END 
		  AND UsuarioCrea = CASE WHEN @UsuarioCrea IS NULL THEN UsuarioCrea ELSE @UsuarioCrea END 
		  AND IdTipoSector = @IdTipoSector

END

GO


