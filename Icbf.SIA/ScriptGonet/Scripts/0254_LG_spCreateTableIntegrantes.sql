USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description: Crea la entidad [PROVEEDOR].[Integrantes]
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[Integrantes]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla Integrantes a crear'
RETURN
END
CREATE TABLE [PROVEEDOR].[Integrantes](
 [IdIntegrante] [INT]  IDENTITY(1,1) NOT NULL,
 [IdEntidad] [INT]  NOT NULL,
 [IdTipoPersona] [INT]  NOT NULL,
 [IdTipoIdentificacionPersonaNatural] [INT]  NOT NULL,
 [NumeroIdentificacion] [INT]  NOT NULL,
 [PorcentajeParticipacion] [DECIMAL] (5,2)  NOT NULL,
 [ConfirmaCertificado] [NVARCHAR] (5) ,
 [ConfirmaPersona] [NVARCHAR] (5) ,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_Integrantes] PRIMARY KEY CLUSTERED 
(
 	[IdIntegrante] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Tabla que Almacena los datos de los integrantes del proveedor', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdIntegrante' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = IdIntegrante;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdEntidad' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Id del Proveedor', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = IdEntidad;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdTipoPersona' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Tipo de Persona o Asociación', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = IdTipoPersona;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdTipoIdentificacionPersonaNatural' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Tipo de Identificación', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = IdTipoIdentificacionPersonaNatural;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'NumeroIdentificacion' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Número de Identificación', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = NumeroIdentificacion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'PorcentajeParticipacion' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Porcentaje de Participación', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = PorcentajeParticipacion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'ConfirmaCertificado' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Confirma Certificado', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = ConfirmaCertificado;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'ConfirmaPersona' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Confirma Persona', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = ConfirmaPersona;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('PROVEEDOR.Integrantes') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('PROVEEDOR.Integrantes')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = PROVEEDOR, 
@level1type = N'TABLE',  @level1name = Integrantes,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
