USE SIA
GO

UPDATE [PROVEEDOR].[TipoCodigoUNSPSC] 
SET estado = 0
where  CodigoSegmento is null
and  NombreSegmento is null
and  CodigoFamilia is null
and  NombreFamilia is null
and  CodigoClase is null
and  NombreClase is null