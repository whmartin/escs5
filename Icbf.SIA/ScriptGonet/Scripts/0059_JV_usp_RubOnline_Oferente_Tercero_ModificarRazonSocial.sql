USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]    Script Date: 04/02/2014 05:07:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]
GO

USE [Oferentes]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]    Script Date: 04/02/2014 05:07:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 02/04/2014
-- Description:	Modfica la Raz�n Social de un Tercero.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]
	-- Add the parameters for the stored procedure here
	@idTercero INT, @razonSocial NVARCHAR(256)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE Oferente.TERCERO 
	SET RAZONSOCIAL=@razonSocial
	WHERE IDTERCERO=@idTercero
END

GO


