USE [SIA]
GO
/**
Desarrollador: Juan Carlos Valverde S�mano.
Fecha Creaci�n: 20-03-2014
Descripci�n: Modifica la Columna ConfirmaYAprueba de la tabla
Proveedor.ValidarInfoDatosBasicosEntidad, ya que
de esta manera se maneja en ValidarProveedor/Revisi�n, 
cuando el Valor es NULL, muestra el Texto sin Validar.
Y esto a surgido a Necesidad del Contro de cambios 16
que solicita cambiar el estado automaticamente cuando 
el proveedor modifica sus datos b�sicos.
**/

ALTER TABLE Proveedor.ValidarInfoDatosBasicosEntidad
ALTER COLUMN ConfirmaYAprueba bit NULL