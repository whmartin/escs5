USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]    Script Date: 03/25/2014 17:19:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]    Script Date: 03/25/2014 17:19:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocAdjuntoTercero
-- Modificación por: Juan Carlos Valverde Sámano
-- Fecha Modificación: 25/03/2014
-- Descripción: En base al requerimiento del Control de Cambios 016, se ha realizado el cambio
-- para que este UPDATE solo modifique la Columna Activo colocandole un false, Y enseguida se realiza un INSERT
-- a la misma Tabla con los datos para el Nuevo Doc, y el campo Activo es por defautl true.
-- de esta manera no se borran los registros de los documentos, se mantiene el historial y solo
-- un documento es el Activo.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]
		@IdDocAdjunto INT,	@IdTercero INT,	
		@LinkDocumento NVARCHAR(256),	
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN

DECLARE
@IdDocumento INT,
@Descripcion NVARCHAR(128),
@Anno INT

SELECT @IdDocumento=[IDDOCUMENTO],
@Descripcion=[DESCRIPCION],
@Anno=[ANNO]
FROM Proveedor.DocAdjuntoTercero 
WHERE IdDocAdjunto = @IdDocAdjunto

	IF (@IdTercero = 0) SET @IdTercero = NULL  

	UPDATE Proveedor.DocAdjuntoTercero 
	SET Activo=0,
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdDocAdjunto = @IdDocAdjunto

EXEC usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar	
@IdTercero,@IdDocumento,@Descripcion,@LinkDocumento, @Anno, @UsuarioModifica, NULL
END




GO


