USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]    Script Date: 08/12/2014 11:52:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]    Script Date: 08/12/2014 11:52:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]
		@IdNotJudicial INT OUTPUT, 	
		@IdEntidad INT,	
		@IdDepartamento INT,	
		@IdMunicipio INT , 	
		@IdZona INT , 	
		@Direccion NVARCHAR(250) ,  
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.NotificacionJudicial(IdEntidad, IdDepartamento,IdMunicipio,IdZona, Direccion, UsuarioCrea, FechaCrea)
	VALUES(@IdEntidad, @IdDepartamento,@IdMunicipio,@IdZona,@Direccion, @UsuarioCrea, GETDATE())
	SELECT @IdNotJudicial = SCOPE_IDENTITY() 		
	RETURN @IdNotJudicial
END

GO


