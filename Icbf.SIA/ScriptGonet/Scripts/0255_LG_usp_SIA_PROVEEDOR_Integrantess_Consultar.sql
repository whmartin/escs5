USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]    Script Date: 07/02/2014 12:19:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]    Script Date: 07/02/2014 12:19:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que consulta un(a) Integrantes
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]
	@IdEntidad INT = NULL
AS
BEGIN
		SELECT	IdIntegrante,
				CASE Integrantes.IdTipoPersona 
				WHEN 1 THEN ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') )
				ELSE T.RAZONSOCIAL END as Integrante, 
				TP.NombreTipoPersona, 
				PorcentajeParticipacion
		 FROM [PROVEEDOR].[Integrantes] Integrantes
		 INNER JOIN oferente.TERCERO T ON Integrantes.NumeroIdentificacion=T.NUMEROIDENTIFICACION
		 INNER JOIN Oferente.TipoPersona TP ON Integrantes.IdTipoPersona=TP.IdTipoPersona  
		 WHERE Integrantes.IdEntidad = CASE WHEN @IdEntidad IS NULL 
			THEN Integrantes.IdEntidad ELSE @IdEntidad END
		 
END

GO


