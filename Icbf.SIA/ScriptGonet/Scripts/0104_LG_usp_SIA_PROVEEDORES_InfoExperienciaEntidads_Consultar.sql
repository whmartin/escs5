USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  03/10/2014 13:01:25 PM
-- Description:	Procedimiento almacenado que Consulta Experiencias
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]
	@IdExpEntidad INT
AS
BEGIN
 SELECT IdExpEntidad, IdEntidad, IdTipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, 
	IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, 
	FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, 
	PorcentParticipacion, AtencionDeptos, JardinOPreJardin, IE.UsuarioCrea, IE.FechaCrea, IE.UsuarioModifica, IE.FechaModifica, NroRevision, Finalizado, ContratoEjecucion,
	EstadoDocumental.Descripcion AS EstadoDocDescripcion
 FROM [Proveedor].[InfoExperienciaEntidad] IE
 JOIN PROVEEDOR.EstadoValidacionDocumental EstadoDocumental
 ON	IE.EstadoDocumental = EstadoDocumental.IdEstadoValidacionDocumental
 WHERE  IdExpEntidad = @IdExpEntidad
END



