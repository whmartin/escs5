USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]    Script Date: 08/12/2014 12:12:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]    Script Date: 08/12/2014 12:12:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]
		@IdValidarInfoFinancieraEntidad INT OUTPUT, @IdInfoFin INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoFinancieraEntidad(IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdInfoFin, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoFinancieraEntidad = SCOPE_IDENTITY() 		
END




GO


