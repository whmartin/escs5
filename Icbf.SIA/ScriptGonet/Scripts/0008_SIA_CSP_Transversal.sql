USE [SIA]
GO

/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripcion: Creación de procedimientos almacenados correspondientes a las tablas transversales de la aplicación.
*/

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_Consultar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que consulta un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Consultar]
	@IdArchivo NUMERIC(18,0)
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , IdFormatoArchivo
      , FechaRegistro
      , NombreArchivo
      , ServidorFTP
      , Estado
      , ResumenCarga
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
 FROM [Estructura].[Archivo] 
 WHERE  IdArchivo = @IdArchivo
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_Consultar_NombreIDTabla]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonte\Jorge vizcaino
-- Create date: 2013-05-21
-- Description:	Procedimiento almacenado que consulta un(a) Archivo por id y nombre de tablas relacionados
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Consultar_NombreIDTabla]
	@idTabla varchar(256) = NULL,
	@NombreTabla varchar(256) = NULL
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , IdFormatoArchivo
      , FechaRegistro
      , NombreArchivo
      , ServidorFTP
      , Estado
      , ResumenCarga
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
      , NombreArchivoOri      
 FROM [Estructura].[Archivo] 
 WHERE idtabla = CASE WHEN @idTabla IS NULL THEN idtabla ELSE @idTabla END
 and	NombreTabla = CASE WHEN @NombreTabla IS NULL THEN NombreTabla ELSE @NombreTabla END
 
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_ConsultarList]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que consulta un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_ConsultarList]
	@IdModalidad INT 
   ,@FechaRegistro DATETIME 
   ,@Estado NVARCHAR(1) = NULL
   ,@Usuario NVARCHAR(250) = NULL
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , [Archivo].[IdFormatoArchivo]
      , FechaRegistro
      , NombreArchivo
      , [Archivo].[Estado]
      , ResumenCarga
      , [Archivo].[UsuarioCrea]
      , [Archivo].[FechaCrea]
      , [Archivo].[UsuarioModifica]
      , [Archivo].[FechaModifica]
      , [TipoEstructura].[NombreEstructura]
 FROM [Estructura].[Archivo] 
	Inner Join [Estructura].[FormatoArchivo] on [Archivo].[IdFormatoArchivo]=[FormatoArchivo].[IdFormatoArchivo]
    Inner Join [Estructura].[TipoEstructura] on [FormatoArchivo].[IdTipoEstructura]=[TipoEstructura].[IdTipoEstructura]
 WHERE  IdModalidad=@IdModalidad 
    And [Archivo].[Estado] = CASE WHEN @Estado = 'N' THEN [Archivo].[Estado] ELSE @Estado END
    And DATEDIFF(dd, @FechaRegistro, CASE WHEN @FechaRegistro = '1900/01/01' THEN @FechaRegistro ELSE [FechaRegistro] END) = 0
    --And @Usuario = CASE WHEN Len(@Usuario) = 0  THEN @Usuario ELSE [Archivo].[UsuarioCrea] END
    And [Archivo].[UsuarioCrea] = CASE WHEN @Usuario IS NULL THEN [Archivo].[UsuarioCrea] ELSE @Usuario END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_ConsultarList_GCB]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta formatos de archivo

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_ConsultarList_GCB]
   @FechaRegistro DATETIME 
   ,@Usuario NVARCHAR(250) = NULL
   ,@IdLlave INT
   ,@IdFormatoArchivo INT
   ,@Sigla NVARCHAR(3)
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , [Archivo].[IdFormatoArchivo]
      , FechaRegistro
      , NombreArchivo
      , [Archivo].[Estado]
      , ResumenCarga
      , [Archivo].[UsuarioCrea]
      , [Archivo].[FechaCrea]
      , [Archivo].[UsuarioModifica]
      , [Archivo].[FechaModifica]
      , [TipoEstructura].[NombreEstructura]
 FROM [Estructura].[Archivo] Inner Join [Estructura].[FormatoArchivo] on [Archivo].[IdFormatoArchivo]=[FormatoArchivo].[IdFormatoArchivo]
      Inner Join [Estructura].[TipoEstructura] on [FormatoArchivo].[IdTipoEstructura]=[TipoEstructura].[IdTipoEstructura]
 WHERE  DATEDIFF(dd, @FechaRegistro, CASE WHEN @FechaRegistro = '1900/01/01' THEN @FechaRegistro ELSE [FechaRegistro] END) = 0
        And @Usuario = CASE WHEN Len(@Usuario) = 0  THEN @Usuario ELSE [Archivo].[UsuarioCrea] END
        AND [Archivo].[IdFormatoArchivo]=@IdFormatoArchivo
        AND NombreArchivo like '%_' + @Sigla + CONVERT(NVARCHAR(256),@IdLlave) + '_%'
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_ConsultarList_GCB_Informe]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta los archivos de informe

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_ConsultarList_GCB_Informe]
   @FechaRegistro DATETIME 
   ,@Usuario NVARCHAR(250) = NULL
   ,@IdInformeMensual INT
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , [Archivo].[IdFormatoArchivo]
      , FechaRegistro
      , NombreArchivo
      , [Archivo].[Estado]
      , ResumenCarga
      , [Archivo].[UsuarioCrea]
      , [Archivo].[FechaCrea]
      , [Archivo].[UsuarioModifica]
      , [Archivo].[FechaModifica]
      , [TipoEstructura].[NombreEstructura]
 FROM [Estructura].[Archivo] Inner Join [Estructura].[FormatoArchivo] on [Archivo].[IdFormatoArchivo]=[FormatoArchivo].[IdFormatoArchivo]
      Inner Join [Estructura].[TipoEstructura] on [FormatoArchivo].[IdTipoEstructura]=[TipoEstructura].[IdTipoEstructura]
 WHERE  DATEDIFF(dd, @FechaRegistro, CASE WHEN @FechaRegistro = '1900/01/01' THEN @FechaRegistro ELSE [FechaRegistro] END) = 0
        And @Usuario = CASE WHEN Len(@Usuario) = 0  THEN @Usuario ELSE [Archivo].[UsuarioCrea] END
        AND [Archivo].[IdFormatoArchivo]=2
        AND NombreArchivo like '%_INF' + CONVERT(NVARCHAR(256),@IdInformeMensual) + '_%'
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_Eliminar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que elimina un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Eliminar]
	@IdArchivo NUMERIC(18,0)
AS
BEGIN
	DELETE Estructura.Archivo WHERE IdArchivo = @IdArchivo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_Insertar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que guarda un nuevo Archivo
-- =============================================
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date: 05/04/2013
-- Description: Agrega el campo NombreArchivoOri para guardar el nombre del archivo que carga un usuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Insertar]
		    @IdArchivo NUMERIC(18,0) OUTPUT
		, 	@IdUsuario INT
		,	@IdFormatoArchivo INT
		,	@FechaRegistro DATETIME
		,	@NombreArchivo NVARCHAR(128)
		,	@ServidorFTP NVARCHAR(256)=NULL
		,	@Estado NVARCHAR(1)
		,	@ResumenCarga NVARCHAR(256)
		,   @UsuarioCrea NVARCHAR(250)
		,	@NombreArchivoOri NVARCHAR(256)
		,	@idTabla varchar(256) = NULL
		,	@NombreTabla varchar(256) = NULL
AS
BEGIN
	INSERT INTO Estructura.Archivo(IdUsuario
	                             , IdFormatoArchivo
	                             , FechaRegistro
	                             , NombreArchivo
	                             , ServidorFTP
	                             , Estado
	                             , ResumenCarga
	                             , UsuarioCrea
	                             , FechaCrea
	                             , NombreArchivoOri
	                             ,NombreTabla
	                             ,IdTabla)
					  VALUES(@IdUsuario
					       , @IdFormatoArchivo
					       , @FechaRegistro
					       , @NombreArchivo
					       , @ServidorFTP
					       , @Estado
					       , @ResumenCarga
					       , @UsuarioCrea
					       , GETDATE()
					       , @NombreArchivoOri
					       , @NombreTabla
					       , @idTabla)
	SELECT @IdArchivo = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_Modificar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que actualiza un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Modificar]
		    @IdArchivo NUMERIC(18,0)
		,	@IdUsuario INT
		,	@IdFormatoArchivo INT
		,	@FechaRegistro DATETIME
		,	@NombreArchivo NVARCHAR(128)
		,	@Estado NVARCHAR(1)
		,	@ResumenCarga NVARCHAR(256)
		, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Estructura.Archivo SET IdUsuario = @IdUsuario, IdFormatoArchivo = @IdFormatoArchivo, FechaRegistro = @FechaRegistro, NombreArchivo = @NombreArchivo, Estado = @Estado, ResumenCarga = @ResumenCarga, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdArchivo = @IdArchivo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_sConsultar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que consulta un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_sConsultar]
	@IdUsuario INT = NULL
   ,@IdFormatoArchivo INT = NULL
   ,@FechaRegistro DATETIME
AS
BEGIN
 SELECT IdArchivo
       ,IdUsuario
       ,IdFormatoArchivo
       ,FechaRegistro
       ,NombreArchivo
       ,ServidorFTP
       ,Estado
       ,ResumenCarga
       ,UsuarioCrea
       ,FechaCrea
       ,UsuarioModifica
       ,FechaModifica 
 FROM [Estructura].[Archivo] 
 WHERE IdUsuario = CASE WHEN @IdUsuario IS NULL THEN IdUsuario ELSE @IdUsuario END 
        AND IdFormatoArchivo = CASE WHEN @IdFormatoArchivo IS NULL THEN IdFormatoArchivo ELSE @IdFormatoArchivo END
        AND DATEDIFF(dd, @FechaRegistro, CASE WHEN @FechaRegistro = '1900/01/01' THEN @FechaRegistro ELSE [FechaRegistro] END) = 0
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Consultar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que consulta un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Consultar]
	@IdCorreoElectronico NUMERIC(18,0)
AS
BEGIN
 SELECT IdCorreoElectronico, IdArchivo, Destinatario, Mensaje, Estado, FechaIngreso, FechaEnvio, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, TipoCorreo FROM [Estructura].[CorreoElectronico] WHERE  IdCorreoElectronico = @IdCorreoElectronico
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Consultar_Envio]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que consulta un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Consultar_Envio]
AS
BEGIN
 SELECT 
	TOP 1
	IdCorreoElectronico, 
	IdArchivo, 
	Destinatario, 
	Mensaje, 
	Estado, 
	FechaIngreso, 
	FechaEnvio, 
	UsuarioCrea, 
	FechaCrea, 
	UsuarioModifica, 
	FechaModifica,
	TipoCorreo
FROM 
	[Estructura].[CorreoElectronico] 
WHERE  
	Estado = 'P'
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Eliminar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que elimina un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Eliminar]
	@IdCorreoElectronico NUMERIC(18,0)
AS
BEGIN
	DELETE Estructura.CorreoElectronico WHERE IdCorreoElectronico = @IdCorreoElectronico
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Insertar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que guarda un nuevo CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Insertar]
		@IdCorreoElectronico NUMERIC(18,0) OUTPUT, 	
		@IdArchivo NUMERIC(18,0),	
		@Destinatario NVARCHAR(256),	
		@Mensaje NVARCHAR(4000),	
		@Estado NVARCHAR(1),	
		@FechaIngreso DATETIME,	
		@FechaEnvio DATETIME, 
		@UsuarioCrea NVARCHAR(250),
		@TipoCorreo CHAR(1)
AS
BEGIN
	INSERT INTO Estructura.CorreoElectronico(IdArchivo, Destinatario, Mensaje, Estado, FechaIngreso, FechaEnvio, UsuarioCrea, FechaCrea, TipoCorreo)
					  VALUES(@IdArchivo, @Destinatario, @Mensaje, @Estado, @FechaIngreso, @FechaEnvio, @UsuarioCrea, GETDATE(), @TipoCorreo)
	SELECT @IdCorreoElectronico = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Modificar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que actualiza un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Modificar]
		@IdCorreoElectronico NUMERIC(18,0),	
		@IdArchivo NUMERIC(18,0),	
		@Destinatario NVARCHAR(256),	
		@Mensaje NVARCHAR(4000),	
		@Estado NVARCHAR(1),	
		@FechaIngreso DATETIME,	
		@FechaEnvio DATETIME, 
		@UsuarioModifica NVARCHAR(250),
		@TipoCorreo CHAR(1)
AS
BEGIN
	IF(@FechaEnvio IS NULL)
		SET @FechaEnvio = GETDATE()
	UPDATE 
		Estructura.CorreoElectronico 
	SET 
		IdArchivo = @IdArchivo, 
		Destinatario = @Destinatario, 
		Mensaje = @Mensaje, 
		Estado = @Estado, 
		FechaIngreso = @FechaIngreso, 
		FechaEnvio = @FechaEnvio, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE(), 
		TipoCorreo = @TipoCorreo
	WHERE 
		IdCorreoElectronico = @IdCorreoElectronico
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_CorreoElectronico_sConsultar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que consulta un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_sConsultar]
	@IdArchivo NUMERIC(18,0) = NULL,@Destinatario NVARCHAR(256) = NULL,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdCorreoElectronico, IdArchivo, Destinatario, Mensaje, Estado, FechaIngreso, FechaEnvio, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, TipoCorreo FROM [Estructura].[CorreoElectronico] WHERE IdArchivo = CASE WHEN @IdArchivo IS NULL THEN IdArchivo ELSE @IdArchivo END AND Destinatario = CASE WHEN @Destinatario IS NULL THEN Destinatario ELSE @Destinatario END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_FormatoArchivo_Consultar]    Script Date: 29/10/2013 23:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta formatos de archivo

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_FormatoArchivo_Consultar]
	@TablaTemporar varchar(256)
   ,@Ext  varchar(10) = NULL
AS
BEGIN
	SELECT IdFormatoArchivo
		,IdTipoEstructura
		,IdModalidad
		,TablaTemporal
		,Separador
		,Extension
		,Estado
		,FechaCrea
		,UsuarioCrea
		,UsuarioModifica
		,FechaModifica
	FROM Estructura.FormatoArchivo
	WHERE TablaTemporal = @TablaTemporar
    AND Extension = CASE WHEN @Ext IS NULL THEN Extension ELSE @Ext END
END

GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Barrio_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:34:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Barrio
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Barrio_Consultar]
	@IdBarrio INT
AS
BEGIN
 SELECT IdBarrio
      , CodigoBarrio
      , NombreBarrio
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
 FROM [DIV].[Barrio] 
 WHERE  IdBarrio = @IdBarrio
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Barrio_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:34:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Barrio
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Barrio_sConsultar]
    @IdComuna NVARCHAR(256) = NULL
   ,@CodigoBarrio NVARCHAR(256) = NULL
   ,@NombreBarrio NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdBarrio
      , IdComuna
      , CodigoBarrio
      , NombreBarrio
      , Estado
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
  FROM [DIV].[Barrio] 
  WHERE IdComuna = CASE WHEN @IdComuna IS NULL THEN IdComuna ELSE @IdComuna END 
        AND CodigoBarrio = CASE WHEN @CodigoBarrio IS NULL THEN CodigoBarrio ELSE @CodigoBarrio END 
        AND NombreBarrio = CASE WHEN @NombreBarrio IS NULL THEN NombreBarrio ELSE @NombreBarrio END
  ORDER BY NombreBarrio DESC      
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_CentroPoblado_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:54:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) CentroPoblado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroPoblado_Consultar]
	@IdCentroPoblado INT
AS
BEGIN
 SELECT IdCentroPoblado
      , IdMunicipio
      , CodigoCentroPoblado
      , NombreCentroPoblado
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
 FROM [DIV].[CentroPoblado] 
 WHERE  IdCentroPoblado = @IdCentroPoblado
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_CentroPoblado_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:54:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) CentroPoblado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroPoblado_sConsultar]
	@IdMunicipio INT = NULL,@CodigoCentroPoblado NVARCHAR(256) = NULL,@NombreCentroPoblado NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdCentroPoblado
      , IdMunicipio
      , CodigoCentroPoblado
      , NombreCentroPoblado
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica
 FROM [DIV].[CentroPoblado] 
 WHERE IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END AND CodigoCentroPoblado = CASE WHEN @CodigoCentroPoblado IS NULL THEN CodigoCentroPoblado ELSE @CodigoCentroPoblado END AND NombreCentroPoblado = CASE WHEN @NombreCentroPoblado IS NULL THEN NombreCentroPoblado ELSE @NombreCentroPoblado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_CentroZonal_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/26/2012 2:57:50 AM
-- Description:	Procedimiento almacenado que consulta un(a) CentroZonal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroZonal_Consultar]
	@IdCentroZonal INT
AS
BEGIN
 SELECT IdCentroZonal, IdMunicipio, IdRegional, CodigoMunicipio, CodigoCentroZonal, NombreCentroZonal, Direccion, Telefonos, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[CentroZonal] WHERE  IdCentroZonal = @IdCentroZonal
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_CentroZonal_Insertar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/26/2012 2:57:50 AM
-- Description:	Procedimiento almacenado que guarda un nuevo CentroZonal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroZonal_Insertar]
		@IdCentroZonal INT OUTPUT
		, 	@IdMunicipio INT
		,	@IdRegional INT
		,	@CodigoMunicipio NVARCHAR(5)
		,	@CodigoCentroZonal NVARCHAR(4)
		,	@NombreCentroZonal NVARCHAR(45)
		,	@Direccion NVARCHAR(100)
		,	@Telefonos NVARCHAR(100)
		,	@Estado NVARCHAR(1)
		, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO DIV.CentroZonal(IdMunicipio, IdRegional, CodigoMunicipio, CodigoCentroZonal, NombreCentroZonal, Direccion, Telefonos, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdMunicipio, @IdRegional, @CodigoMunicipio, @CodigoCentroZonal, @NombreCentroZonal, @Direccion, @Telefonos, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdCentroZonal = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_CentroZonal_Modificar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/26/2012 2:57:50 AM
-- Description:	Procedimiento almacenado que actualiza un(a) CentroZonal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroZonal_Modificar]
		@IdCentroZonal INT
		,	@IdMunicipio INT
		,	@IdRegional INT
		,	@CodigoMunicipio NVARCHAR(5)
		,	@CodigoCentroZonal NVARCHAR(4)
		,	@NombreCentroZonal NVARCHAR(45)
		,	@Direccion NVARCHAR(100)
		,	@Telefonos NVARCHAR(100)
		,	@Estado NVARCHAR(1)
		, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE 
		DIV.CentroZonal SET IdMunicipio = @IdMunicipio
		, IdRegional = @IdRegional
		, CodigoMunicipio = @CodigoMunicipio
		, CodigoCentroZonal = @CodigoCentroZonal
		, NombreCentroZonal = @NombreCentroZonal
		, Direccion = @Direccion
		, Telefonos = @Telefonos
		, Estado = @Estado
		, UsuarioModifica = @UsuarioModifica
		, FechaModifica = GETDATE() 
	WHERE IdCentroZonal = @IdCentroZonal
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_CentroZonal_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/26/2012 2:57:50 AM
-- Description:	Procedimiento almacenado que consulta un(a) CentroZonal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroZonal_sConsultar]
	@IdMunicipio INT = NULL
	,@IdRegional INT = NULL
	,@NombreCentroZonal NVARCHAR(45) = NULL
	,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdCentroZonal
	, IdMunicipio
	, IdRegional
	, CodigoMunicipio
	, CodigoCentroZonal
	, NombreCentroZonal
	, Direccion
	, Telefonos
	, Estado
	, UsuarioCrea
	, FechaCrea
	, UsuarioModifica
	, FechaModifica 
FROM [DIV].[CentroZonal] 
WHERE 
	IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END 
	AND IdRegional = CASE WHEN @IdRegional IS NULL THEN IdRegional ELSE @IdRegional END 
	AND NombreCentroZonal = CASE WHEN @NombreCentroZonal IS NULL THEN NombreCentroZonal ELSE @NombreCentroZonal END 
	AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Cobertura_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que consulta un(a) Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_Consultar]
	@IdCobertura INT
AS
BEGIN
 SELECT IdCobertura, IdCentroZonal, IdMunicipio, IdVigencia, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[Cobertura] WHERE  IdCobertura = @IdCobertura
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Cobertura_Eliminar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que elimina un(a) Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_Eliminar]
	@IdCobertura INT
AS
BEGIN
	DELETE DIV.Cobertura WHERE IdCobertura = @IdCobertura
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Cobertura_Insertar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que guarda un nuevo Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_Insertar]
		@IdCobertura INT OUTPUT, 	@IdCentroZonal INT,	@IdMunicipio INT,	@IdVigencia INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO DIV.Cobertura(IdCentroZonal, IdMunicipio, IdVigencia, UsuarioCrea, FechaCrea)
					  VALUES(@IdCentroZonal, @IdMunicipio, @IdVigencia, @UsuarioCrea, GETDATE())
	SELECT @IdCobertura = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Cobertura_Modificar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que actualiza un(a) Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_Modificar]
		@IdCobertura INT,	@IdCentroZonal INT,	@IdMunicipio INT,	@IdVigencia INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE DIV.Cobertura SET IdCentroZonal = @IdCentroZonal, IdMunicipio = @IdMunicipio, IdVigencia = @IdVigencia, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdCobertura = @IdCobertura
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Cobertura_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que consulta un(a) Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_sConsultar]
	@IdCentroZonal INT = NULL,@IdMunicipio INT = NULL,@IdVigencia INT = NULL
AS
BEGIN
 SELECT IdCobertura, IdCentroZonal, IdMunicipio, IdVigencia, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[Cobertura] WHERE IdCentroZonal = CASE WHEN @IdCentroZonal IS NULL THEN IdCentroZonal ELSE @IdCentroZonal END AND IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Comuna_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:44:50 PM
-- Description:	Procedimiento almacenado que consulta un(a) Comuna
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Comuna_Consultar]
	@IdComuna INT
AS
BEGIN
 SELECT IdComuna
      , IdMunicipio
      , CodigoComuna
      , NombreComuna
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
  FROM [DIV].[Comuna] 
  WHERE  IdComuna = @IdComuna
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Comuna_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:44:50 PM
-- Description:	Procedimiento almacenado que consulta un(a) Comuna
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Comuna_sConsultar]
	@IdMunicipio INT = NULL,@CodigoComuna NVARCHAR(256) = NULL,@NombreComuna NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdComuna
      , IdMunicipio
      , CodigoComuna
      , NombreComuna
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
  FROM [DIV].[Comuna] 
  WHERE IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END AND CodigoComuna = CASE WHEN @CodigoComuna IS NULL THEN CodigoComuna ELSE @CodigoComuna END AND NombreComuna = CASE WHEN @NombreComuna IS NULL THEN NombreComuna ELSE @NombreComuna END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Departamento_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 4:44:03 PM
-- Description:	Procedimiento almacenado que consulta un(a) Departamento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Departamento_Consultar]
	@IdDepartamento INT
AS
BEGIN
 SELECT IdDepartamento, 
		IdPais, 
		CodigoDepartamento, 
		NombreDepartamento, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Departamento] WHERE  IdDepartamento = @IdDepartamento
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Departamento_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 4:44:03 PM
-- Description:	Procedimiento almacenado que consulta un(a) Departamento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Departamento_sConsultar]
	@IdPais INT = NULL,@CodigoDepartamento NVARCHAR(128) = NULL,@NombreDepartamento NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdDepartamento, 
		IdPais, 
		CodigoDepartamento, 
		NombreDepartamento, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Departamento] 
	WHERE IdPais = CASE WHEN @IdPais IS NULL THEN IdPais ELSE @IdPais END 
	      AND CodigoDepartamento = CASE WHEN @CodigoDepartamento IS NULL THEN CodigoDepartamento ELSE @CodigoDepartamento END 
	      AND NombreDepartamento = CASE WHEN @NombreDepartamento IS NULL THEN NombreDepartamento ELSE @NombreDepartamento END
	ORDER BY NombreDepartamento desc
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Municipio_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:03:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) Municipio
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Municipio_Consultar]
	@IdMunicipio INT
AS
BEGIN
 SELECT IdMunicipio, 
		IdDepartamento, 
		CodigoMunicipio, 
		NombreMunicipio, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Municipio] WHERE  IdMunicipio = @IdMunicipio
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Municipio_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:03:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) Municipio
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Municipio_sConsultar]
	@IdDepartamento INT = NULL,@CodigoMunicipio NVARCHAR(128) = NULL,@NombreMunicipio NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdMunicipio, 
		IdDepartamento, 
		CodigoMunicipio, 
		NombreMunicipio, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Municipio] 
	WHERE IdDepartamento = CASE WHEN @IdDepartamento IS NULL THEN IdDepartamento ELSE @IdDepartamento END 
	      AND CodigoMunicipio = CASE WHEN @CodigoMunicipio IS NULL THEN CodigoMunicipio ELSE @CodigoMunicipio END 
	      AND NombreMunicipio = CASE WHEN @NombreMunicipio IS NULL THEN NombreMunicipio ELSE @NombreMunicipio END
	ORDER BY NombreMunicipio DESC      
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Pais_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 3:52:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Pais
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Pais_Consultar]
	@IdPais INT
AS
BEGIN
 SELECT IdPais, 
		CodigoPais, 
		NombrePais, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Pais] WHERE  IdPais = @IdPais
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Pais_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 3:52:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Pais
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Pais_sConsultar]
	@CodigoPais NVARCHAR(128) = NULL,@NombrePais NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdPais, 
		CodigoPais, 
		NombrePais, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Pais] WHERE CodigoPais = CASE WHEN @codigoPais IS NULL THEN CodigoPais ELSE @CodigoPais END AND NombrePais = CASE WHEN @NombrePais IS NULL THEN NombrePais ELSE @NombrePais END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Regional_Consultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  11/12/2012 8:16:01
-- Description:	Procedimiento almacenado que consulta un(a) Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_Consultar]
	@IdRegional INT
AS
BEGIN
 SELECT IdRegional, CodigoRegional, NombreRegional, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[Regional] WHERE  IdRegional = @IdRegional
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Regional_Eliminar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  11/12/2012 8:16:01
-- Description:	Procedimiento almacenado que elimina un(a) Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_Eliminar]
	@IdRegional INT
AS
BEGIN
	DELETE DIV.Regional WHERE IdRegional = @IdRegional
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Regional_Insertar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  11/12/2012 8:16:01
-- Description:	Procedimiento almacenado que guarda un nuevo Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_Insertar]
		@IdRegional INT OUTPUT, 	@CodigoRegional NVARCHAR(128),	@NombreRegional NVARCHAR(256), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO DIV.Regional(CodigoRegional, NombreRegional, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRegional, @NombreRegional, @UsuarioCrea, GETDATE())
	SELECT @IdRegional = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Regional_Modificar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  11/12/2012 8:16:01
-- Description:	Procedimiento almacenado que actualiza un(a) Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_Modificar]
		@IdRegional INT,	@CodigoRegional NVARCHAR(128),	@NombreRegional NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE DIV.Regional SET CodigoRegional = @CodigoRegional, NombreRegional = @NombreRegional, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRegional = @IdRegional
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Regional_sConsultar]    Script Date: 29/10/2013 23:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/10/2012 11:52:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Regional
-- =============================================
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  12/15/2012 12:15:00 PM
-- Description:	Procedimiento almacenado que consulta un(a) Regional Ordenado por Nombre DESC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_sConsultar]
	@CodigoRegional NVARCHAR(2) = NULL,@NombreRegional NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdRegional, CodigoRegional, NombreRegional, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[Regional] WHERE CodigoRegional = CASE WHEN @CodigoRegional IS NULL THEN CodigoRegional ELSE @CodigoRegional END AND NombreRegional = CASE WHEN @NombreRegional IS NULL THEN NombreRegional ELSE @NombreRegional END ORDER BY NombreRegional DESC
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_CargoTipoCargo_Consultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargo_Consultar]
	@IdCargo INT
	,@TipoCargo NVARCHAR(1)
AS
BEGIN
 SELECT C.*, E.NombreCargo 
 FROM [Global].[CargoTipoCargo] C
 INNER JOIN ECO.Cargo E ON C.IdCargo=E.IdCargo
 WHERE C.IdCargo=@IdCargo AND C.TipoCargo=@TipoCargo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_CargoTipoCargo_Eliminar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que elimina un(a) CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargo_Eliminar]
	@IdCargo INT
	,@TipoCargo NVARCHAR(1)
AS
BEGIN
	DELETE Global.CargoTipoCargo WHERE IdCargo=@IdCargo AND TipoCargo=@TipoCargo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_CargoTipoCargo_Insertar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que guarda un nuevo CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargo_Insertar]
		@IdCargo INT,	@TipoCargo NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.CargoTipoCargo(IdCargo, TipoCargo, UsuarioCrea, FechaCrea)
					  VALUES(@IdCargo, @TipoCargo, @UsuarioCrea, GETDATE())
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_CargoTipoCargo_Modificar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que actualiza un(a) CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargo_Modificar]
		@IdCargo INT,	@TipoCargo NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.CargoTipoCargo 
	SET IdCargo = @IdCargo, TipoCargo = @TipoCargo, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdCargo=@IdCargo AND TipoCargo=@TipoCargo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_CargoTipoCargos_Consultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF/Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargos_Consultar]
	@TipoCargo NVARCHAR(1)
AS
BEGIN
 SELECT C.*, E.NombreCargo 
 FROM [Global].[CargoTipoCargo] C
 INNER JOIN ECO.Cargo E ON C.IdCargo=E.IdCargo
 WHERE TipoCargo=@TipoCargo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_MotivoEstado_Consultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que consulta un(a) MotivoEstado
-- =============================================
create PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_Consultar]
	@IdMotivoEstado INT
AS
BEGIN
 SELECT IdMotivoEstado, CodigoMotivoEstado, NombreMotivoEstado, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[MotivoEstado] WHERE  IdMotivoEstado = @IdMotivoEstado
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_MotivoEstado_Eliminar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que elimina un(a) MotivoEstado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_Eliminar]
	@IdMotivoEstado INT
AS
BEGIN
	DELETE Global.MotivoEstado WHERE IdMotivoEstado = @IdMotivoEstado
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_MotivoEstado_Insertar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que guarda un nuevo MotivoEstado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_Insertar]
		@IdMotivoEstado INT OUTPUT, 	@CodigoMotivoEstado NVARCHAR(128),	@NombreMotivoEstado NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.MotivoEstado(CodigoMotivoEstado, NombreMotivoEstado, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoMotivoEstado, @NombreMotivoEstado, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdMotivoEstado = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_MotivoEstado_Modificar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que actualiza un(a) MotivoEstado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_Modificar]
		@IdMotivoEstado INT,	@CodigoMotivoEstado NVARCHAR(128),	@NombreMotivoEstado NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.MotivoEstado SET CodigoMotivoEstado = @CodigoMotivoEstado, NombreMotivoEstado = @NombreMotivoEstado, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdMotivoEstado = @IdMotivoEstado
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_MotivoEstado_sConsultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que consulta un(a) MotivoEstado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_sConsultar]
	@CodigoMotivoEstado NVARCHAR(128) = NULL,@NombreMotivoEstado NVARCHAR(256) = NULL,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdMotivoEstado, CodigoMotivoEstado, NombreMotivoEstado, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[MotivoEstado] WHERE CodigoMotivoEstado = CASE WHEN @CodigoMotivoEstado IS NULL THEN CodigoMotivoEstado ELSE @CodigoMotivoEstado END AND NombreMotivoEstado = CASE WHEN @NombreMotivoEstado IS NULL THEN NombreMotivoEstado ELSE @NombreMotivoEstado END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_ServicioVigencia_Consultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que consulta un(a) ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_Consultar]
	@IdServicioVigencia INT
AS
BEGIN
 SELECT IdServicioVigencia, IdVigencia, IdServicio, IdRubro, CodigoServicio, NombreServicio, CodigoRubro, NombreRubro, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[ServicioVigencia] WHERE  IdServicioVigencia = @IdServicioVigencia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_ServicioVigencia_Eliminar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que elimina un(a) ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_Eliminar]
	@IdServicioVigencia INT
AS
BEGIN
	DELETE Global.ServicioVigencia WHERE IdServicioVigencia = @IdServicioVigencia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_ServicioVigencia_Insertar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que guarda un nuevo ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_Insertar]
		@IdServicioVigencia INT OUTPUT, 	@IdVigencia INT,	@IdServicio INT,	@IdRubro INT,	@CodigoServicio NVARCHAR(128),	@NombreServicio NVARCHAR(256),	@CodigoRubro NVARCHAR(128),	@NombreRubro NVARCHAR(256),	@Estado NVARCHAR, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.ServicioVigencia(IdVigencia, IdServicio, IdRubro, CodigoServicio, NombreServicio, CodigoRubro, NombreRubro, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdVigencia, @IdServicio, @IdRubro, @CodigoServicio, @NombreServicio, @CodigoRubro, @NombreRubro, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdServicioVigencia = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_ServicioVigencia_Modificar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que actualiza un(a) ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_Modificar]
		@IdServicioVigencia INT,	@IdVigencia INT,	@IdServicio INT,	@IdRubro INT,	@CodigoServicio NVARCHAR(128),	@NombreServicio NVARCHAR(256),	@CodigoRubro NVARCHAR(128),	@NombreRubro NVARCHAR(256),	@Estado NVARCHAR, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.ServicioVigencia SET IdVigencia = @IdVigencia, IdServicio = @IdServicio, IdRubro = @IdRubro, CodigoServicio = @CodigoServicio, NombreServicio = @NombreServicio, CodigoRubro = @CodigoRubro, NombreRubro = @NombreRubro, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdServicioVigencia = @IdServicioVigencia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_ServicioVigencia_sConsultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que consulta un(a) ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_sConsultar]
	@IdVigencia INT = NULL,@IdServicio INT = NULL,@IdRubro INT = NULL,@CodigoServicio NVARCHAR(128) = NULL,@CodigoRubro NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdServicioVigencia, IdVigencia, IdServicio, IdRubro, CodigoServicio, NombreServicio, CodigoRubro, NombreRubro, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[ServicioVigencia] 
 WHERE IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END AND IdServicio = CASE WHEN @IdServicio IS NULL THEN IdServicio ELSE @IdServicio END AND IdRubro = CASE WHEN @IdRubro IS NULL THEN IdRubro ELSE @IdRubro END AND CodigoServicio = CASE WHEN @CodigoServicio IS NULL THEN CodigoServicio ELSE @CodigoServicio END AND CodigoRubro = CASE WHEN @CodigoRubro IS NULL THEN CodigoRubro ELSE @CodigoRubro END
 ORDER BY NombreServicio desc
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_TipoDocumento_Consultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        ICBF\Tommy.Puccini
-- Create date:  12/12/2012 9:48:16
-- Description:   Procedimiento almacenado que consulta un(a) TipoDocumento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TipoDocumento_Consultar]
      @IdTipoDocumento INT
AS
BEGIN
SELECT [IdTipoDocumento]
      ,[CodDocumento]
      ,[NomTipoDocumento]
  FROM [Global].[TiposDocumentos]
WHERE  IdTipoDocumento = @IdTipoDocumento
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_TipoDocumento_sConsultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  12/12/2012 9:48:16
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TipoDocumento_sConsultar]
	@CodigoDocumento NVARCHAR(10) = NULL,@NombreDocumento NVARCHAR(50) = NULL
AS
BEGIN
 SELECT IdTipoDocumento, CodigoDocumento, NombreDocumento, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[TipoDocumento] WHERE CodigoDocumento = CASE WHEN @CodigoDocumento IS NULL THEN CodigoDocumento ELSE @CodigoDocumento END AND NombreDocumento = CASE WHEN @NombreDocumento IS NULL THEN NombreDocumento ELSE @NombreDocumento END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_TituloObtenido_Consultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que consulta un(a) TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_Consultar]
	@IdTituloObtenido INT
AS
BEGIN
 SELECT IdTituloObtenido, CodigoTituloObtenido, NombreTituloObtenido, Estado, 
		UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Global].[TituloObtenido] 
 WHERE  IdTituloObtenido = @IdTituloObtenido
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_TituloObtenido_Eliminar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que elimina un(a) TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_Eliminar]
	@IdTituloObtenido INT
AS
BEGIN
	DELETE Global.TituloObtenido WHERE IdTituloObtenido = @IdTituloObtenido
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_TituloObtenido_Insertar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que guarda un nuevo TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_Insertar]
		@IdTituloObtenido INT OUTPUT, 	@CodigoTituloObtenido NVARCHAR(128),	@NombreTituloObtenido NVARCHAR(128),	@Estado NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.TituloObtenido(CodigoTituloObtenido, NombreTituloObtenido, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTituloObtenido, @NombreTituloObtenido, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTituloObtenido = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_TituloObtenido_Modificar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que actualiza un(a) TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_Modificar]
		@IdTituloObtenido INT,	@CodigoTituloObtenido NVARCHAR(128),	@NombreTituloObtenido NVARCHAR(128),	@Estado NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.TituloObtenido SET CodigoTituloObtenido = @CodigoTituloObtenido, NombreTituloObtenido = @NombreTituloObtenido, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTituloObtenido = @IdTituloObtenido
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_TituloObtenido_sConsultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que consulta un(a) TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_sConsultar]
	@CodigoTituloObtenido NVARCHAR(128) = NULL,@NombreTituloObtenido NVARCHAR(128) = NULL,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdTituloObtenido, CodigoTituloObtenido, NombreTituloObtenido, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[TituloObtenido] WHERE CodigoTituloObtenido = CASE WHEN @CodigoTituloObtenido IS NULL THEN CodigoTituloObtenido ELSE @CodigoTituloObtenido END AND NombreTituloObtenido = CASE WHEN @NombreTituloObtenido IS NULL THEN NombreTituloObtenido ELSE @NombreTituloObtenido END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencia_Consultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que consulta un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_Consultar]
	@IdVigencia INT
AS
BEGIN
 SELECT IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[Vigencia] WHERE  IdVigencia = @IdVigencia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencia_Eliminar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que elimina un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_Eliminar]
	@IdVigencia INT
AS
BEGIN
	DELETE Global.Vigencia WHERE IdVigencia = @IdVigencia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencia_Insertar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que guarda un nuevo Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_Insertar]
		@IdVigencia INT OUTPUT, 	@AcnoVigencia INT,	@Activo NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.Vigencia(AcnoVigencia, Activo, UsuarioCrea, FechaCrea)
					  VALUES(@AcnoVigencia, @Activo, @UsuarioCrea, GETDATE())
	SELECT @IdVigencia = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencia_Modificar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que actualiza un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_Modificar]
		@IdVigencia INT,	@AcnoVigencia INT,	@Activo NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.Vigencia SET AcnoVigencia = @AcnoVigencia, Activo = @Activo, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdVigencia = @IdVigencia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que consulta un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]
	@AcnoVigencia INT = NULL,@Activo NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[Vigencia] WHERE AcnoVigencia = CASE WHEN @AcnoVigencia IS NULL THEN AcnoVigencia ELSE @AcnoVigencia END AND Activo = CASE WHEN @Activo IS NULL THEN Activo ELSE @Activo END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]    Script Date: 29/10/2013 23:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  20/06/2013
-- Description:	Procedimiento almacenado que consulta las vigencias sin incluir el año actual
-- =============================================
create PROCEDURE [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]
	@Activo NVARCHAR(1) = NULL
AS
BEGIN
 SELECT TOP 5 IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Global].[Vigencia] 
 WHERE AcnoVigencia < Year(getdate())
 AND Activo = CASE WHEN @Activo IS NULL THEN Activo ELSE @Activo END
 ORDER BY AcnoVigencia DESC
END


GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]    Script Date: 10/30/2013 00:38:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]    Script Date: 10/30/2013 00:38:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 03-10-2012
-- Description:	Procedimiento almacenado que consulta un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]
	@ProviderKey varchar(125)
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
  FROM SEG.Usuario 
  WHERE
	providerKey = @ProviderKey
END


GO

/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarAuditoria]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarAuditoria]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  la auditoria
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarAuditoria]
	@pNombrePrograma NVARCHAR(200),
	@pIdRegistro NUMERIC(18,0)
AS
BEGIN

SELECT
	A.fecha Fecha,
	U.PrimerNombre + '' '' + U.PrimerApellido,
	A.operacion Operacion,
	A.parametrosOperacion ParametrosOperacion,
	A.tabla Tabla,
	A.direccionIp DireccionIp,
	A.navegador Navegador
FROM
	AUDITA.LogSIA A
LEFT OUTER JOIN
	SEG.Usuario U ON A.usuario = U.IdUsuario
WHERE
	A.programa = @pNombrePrograma
AND
	A.idRegistro = 	@pIdRegistro


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarHijoPermisoModuloRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarHijoPermisoModuloRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jorge Vizcaino
-- Create date: 19-04-2013
-- Description:	Procedimiento almacenado que consulta los permisos hijos de un Rol por Identificador del Módulo y Nombre de Rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarHijoPermisoModuloRol]
	@nombreRol NVARCHAR(250),
	@IdModulo INT,
	@ID int = null
AS
BEGIN

	if (@ID IS NULL) 
	BEGIN		
		SELECT 
			P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
		FROM 
			SEG.Permiso PR
		INNER JOIN 
			SEG.Rol R ON PR.IdRol = R.IdRol
		INNER JOIN
			SEG.Programa P ON PR.IdPrograma = P.IdPrograma
		INNER JOIN
			SEG.Modulo M ON P.IdModulo = M.IdModulo			
		WHERE
			R.Nombre = @nombreRol	
		AND
			M.IdModulo = @IdModulo
		AND
			(PR.Insertar = 1 OR PR.Modificar =1 OR PR.Eliminar = 1 OR PR.Consultar=1)	
		AND
			P.VisibleMenu = 1			
		AND 
			P.padre IS NULL
		GROUP BY
			P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
		ORDER BY
			P.Posicion DESC	
	END
	ELSE 
	BEGIN
		SELECT 
			P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
		FROM 
			SEG.Permiso PR
		INNER JOIN 
			SEG.Rol R ON PR.IdRol = R.IdRol
		INNER JOIN
			SEG.Programa P ON PR.IdPrograma = P.IdPrograma
		INNER JOIN
			SEG.Modulo M ON P.IdModulo = M.IdModulo			
		WHERE
			R.Nombre = @nombreRol	
		AND
			M.IdModulo = @IdModulo
		AND
			(PR.Insertar = 1 OR PR.Modificar =1 OR PR.Eliminar = 1 OR PR.Consultar=1)	
		AND
			P.VisibleMenu = 1	
		AND 
			P.padre = @ID 
		GROUP BY
			P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
		ORDER BY
			P.Posicion DESC	
	END 


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarModulo]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarModulo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Yuri Gereda
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que consulta un Módulo
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarModulo]
	@IdModulo INT
AS
BEGIN

SELECT 
	IdModulo, NombreModulo, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion
FROM 
	SEG.Modulo
WHERE 
	IdModulo = @IdModulo


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarModulos]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarModulos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Yuri Gereda
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado que consulta un Módulo por Nombre y Estado
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarModulos]
	@pNombreModulo NVARCHAR(200) = NULL,
	@pEstado	   BIT = NULL
AS
BEGIN

SELECT 
	IdModulo, NombreModulo, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion
FROM 
	SEG.Modulo
WHERE 
	NombreModulo  LIKE CASE WHEN @pNombreModulo IS NULL THEN NombreModulo ELSE ''%''+@pNombreModulo+''%'' END
AND
	Estado = CASE WHEN @pEstado IS NULL THEN	Estado ELSE @pEstado END

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarModulosRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarModulosRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Yuri Gereda
-- Create date: 06-11-2012
-- Description:	Procedimiento almacenado que consulta los Módulos permitidos para un  Rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarModulosRol]
	@nombreRol NVARCHAR(250)
AS
BEGIN

SELECT 
	M.IdModulo, M.NombreModulo, M.Posicion, M.Estado, M.UsuarioCreacion, M.FechaCreacion, M.UsuarioModificacion, M.FechaModificacion
FROM 
	SEG.ProgramaRol PR
INNER JOIN 
	SEG.Rol R ON PR.IdRol = R.IdRol
INNER JOIN
	SEG.Programa P ON PR.IdPrograma = P.IdPrograma
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo			
WHERE
	R.Nombre = @nombreRol	
GROUP BY
	M.IdModulo, M.NombreModulo, M.Posicion, M.Estado, M.UsuarioCreacion, M.FechaCreacion, M.UsuarioModificacion, M.FechaModificacion	
ORDER BY
	M.Posicion DESC	

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarPermisoModuloRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarPermisoModuloRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Yuri Gereda
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado que consulta los permisos de un Rol por Identificador del Módulo y Nombre de Rol
-- =============================================
/****** 
Ultima Modifiacion: 04/22/2013 09:33:00
Modificador Por: Jorge Vizcaino Argota ******/
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarPermisoModuloRol]
	@nombreRol NVARCHAR(250),
	@IdModulo INT
AS
BEGIN

SELECT 
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
FROM 
	SEG.Permiso PR
INNER JOIN 
	SEG.Rol R ON PR.IdRol = R.IdRol
INNER JOIN
	SEG.Programa P ON PR.IdPrograma = P.IdPrograma
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo			
WHERE
	R.Nombre = @nombreRol	
AND
	M.IdModulo = @IdModulo
AND
	(PR.Insertar = 1 OR PR.Modificar =1 OR PR.Eliminar = 1 OR PR.Consultar=1)	
AND
	P.VisibleMenu = 1	
AND 
	P.padre is null	
GROUP BY
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
ORDER BY
	P.Posicion DESC	

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarPermisosRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarPermisosRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Yuri Gereda
-- Create date:  13/11/2012 10:38:00 a.m.
-- Description:	Procedimiento almacenado que consulta los permisos de un Rol por Identificador del Rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarPermisosRol]
	@pIdRol INT
AS
BEGIN

SELECT
	PE.IdPermiso, PE.IdPrograma, PR.NombrePrograma, PE.IdRol, PE.Insertar, PE.Modificar, PE.Eliminar, PE.Consultar, PE.UsuarioCreacion, PE.FechaCreacion, PE.UsuarioModificacion, PE.FechaModificacion
FROM
	SEG.Permiso PE	
INNER JOIN
	SEG.Rol RO ON PE.IdRol = RO.IdRol
INNER JOIN
	SEG.Programa PR	ON PE.IdPrograma = PR.IdPrograma
WHERE
	RO.IdRol = @pIdRol
	
UNION

SELECT 
	-1, IdPrograma, NombrePrograma, @pIdRol , 0, 0, 0, 0, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion
FROM 
	SEG.Programa 
WHERE 
	IdPrograma NOT IN(	SELECT
							PE.IdPrograma
						FROM
							SEG.Permiso PE	
						INNER JOIN
							SEG.Rol RO ON PE.IdRol = RO.IdRol
						WHERE
							RO.IdRol = @pIdRol)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarPermisosRolSeguridad]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarPermisosRolSeguridad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Yuri Gereda
-- Create date:  13/11/2012 10:38:00 a.m.
-- Description:	Procedimiento almacenado que consulta los permisos de un Rol por Nombre de Rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarPermisosRolSeguridad]
	@nombreRol NVARCHAR(250)
AS
BEGIN

SELECT 
	M.IdModulo, M.NombreModulo, M.Posicion, M.Estado, M.UsuarioCreacion, M.FechaCreacion, M.UsuarioModificacion, M.FechaModificacion
FROM 
	SEG.Permiso PR
INNER JOIN 
	SEG.Rol R ON PR.IdRol = R.IdRol
INNER JOIN
	SEG.Programa P ON PR.IdPrograma = P.IdPrograma
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo			
WHERE
	R.Nombre = @nombreRol	
AND
	(PR.Insertar = 1 OR PR.Modificar =1 OR PR.Eliminar = 1 OR PR.Consultar=1)
AND 
	P.VisibleMenu = 1	
GROUP BY
	M.IdModulo, M.NombreModulo, M.Posicion, M.Estado, M.UsuarioCreacion, M.FechaCreacion, M.UsuarioModificacion, M.FechaModificacion	
ORDER BY
	M.Posicion DESC	

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarPrograma]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarPrograma]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 03-09-2012
-- Description:	Procedimiento almacenado que consulta un programa
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarPrograma]
	@IdPrograma INT
AS
BEGIN

SELECT 
	IdPrograma, IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion, VisibleMenu, generaLog
FROM 
	SEG.Programa
WHERE 
	IdPrograma = @IdPrograma


END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarProgramas]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarProgramas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  programas
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarProgramas]
	@pIdModulo INT,
	@pNombreModulo NVARCHAR(250)
AS
BEGIN

SELECT 
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
FROM 
	SEG.Programa P
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo	
WHERE 
	P.IdModulo = CASE WHEN @pIdModulo = -1 THEN P.IdModulo ELSE @pIdModulo END
AND
	P.NombrePrograma LIKE CASE WHEN @pNombreModulo = '''' THEN P.NombrePrograma ELSE ''%''+@pNombreModulo+''%'' END


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarProgramasAsignadosRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarProgramasAsignadosRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta programas asignados a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarProgramasAsignadosRol]
	@pIdRol INT
AS
BEGIN

SELECT 
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo
FROM 
	SEG.Programa P
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo	
INNER JOIN
	SEG.ProgramaRol ON P.IdPrograma = SEG.ProgramaRol.IdPrograma
WHERE 
	SEG.ProgramaRol.IdRol = @pIdRol

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarProgramasExcluidosRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarProgramasExcluidosRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  programas sin asignar a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarProgramasExcluidosRol]
	@pIdRol INT
AS
BEGIN

SELECT 
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo
FROM 
	SEG.Programa P
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo	
WHERE 
	P.IdPrograma NOT IN (SELECT SEG.ProgramaRol.IdPrograma FROM SEG.ProgramaRol
			WHERE SEG.ProgramaRol.IdRol = @pIdRol)
	AND P.Estado = 1

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarProgramasRolPermiso]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarProgramasRolPermiso]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  programas que tiene asignado un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarProgramasRolPermiso]
	@pNombreRol NVARCHAR(250)
AS
BEGIN

SELECT
	P.IdPrograma, 
	P.IdModulo, 
	P.NombrePrograma, 
	P.CodigoPrograma, 
	P.Posicion, 
	P.Estado, 
	P.VisibleMenu,
	P.UsuarioCreacion, 
	P.FechaCreacion, 
	P.UsuarioModificacion, 
	P.FechaModificacion,
	P.generaLog,
	((CASE WHEN PE.Consultar = 1 THEN 1 ELSE 0 END) + (CASE WHEN PE.Eliminar = 1 THEN 2 ELSE 0 END) + (CASE WHEN PE.Modificar  = 1 THEN 4 ELSE 0 END) + (CASE WHEN PE.Insertar  = 1 THEN 8 ELSE 0 END)) Permiso
FROM
	SEG.Programa P
INNER JOIN
	SEG.Permiso PE ON P.IdPrograma = PE.IdPrograma
INNER JOIN
	SEG.Rol RO ON RO.IdRol = PE.IdRol 	
WHERE
	RO.Nombre = @pNombreRol	


END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado que consulta un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarRol]
	@pIdRol varchar(125)
AS
BEGIN
  SELECT [IdRol]
	   ,[providerKey]
	   ,[Nombre]
	   ,[Descripcion]
	   ,[Estado]
      ,UsuarioCreacion
      ,FechaCreacion
      ,UsuarioModificacion
      ,FechaModificacion
  FROM SEG.Rol
  WHERE
	IdRol = @pIdRol
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarRoles]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado para la consulta de roles
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarRoles]
	@Nombre NVARCHAR(20)
	, @Estado BIT
AS
BEGIN
	IF @Estado IS NULL
	BEGIN
		SELECT [IdRol]
			,[providerKey]
			,[Nombre]
			,[Descripcion]
			,[Estado]
		FROM SEG.Rol
		WHERE [Nombre] LIKE (''%'' + @Nombre + ''%'')
	END
	ELSE
	BEGIN
		SELECT 
			[IdRol]
			,[providerKey]
			,[Nombre]
			,[Descripcion]
			,[Estado]
		FROM SEG.Rol
		WHERE [Nombre] LIKE (''%'' + @Nombre + ''%'')
			AND [Estado] = @Estado
	END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarRolPorProviderKey]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarRolPorProviderKey]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado que consulta un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarRolPorProviderKey]
	@ProviderKey varchar(125)
AS
BEGIN
  SELECT [IdRol]
	   ,[providerKey]
	   ,[Nombre]
	   ,[Descripcion]
	   ,[Estado]
      ,UsuarioCreacion
      ,FechaCreacion
      ,UsuarioModificacion
      ,FechaModificacion
  FROM SEG.Rol
  WHERE
	providerKey = @ProviderKey
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_EliminarModulo]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_EliminarModulo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que elimina un(a) Modulo
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_EliminarModulo]
	@IdModulo INT
AS
BEGIN
	DELETE SEG.Modulo WHERE IdModulo = @IdModulo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_EliminarPermisosRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_EliminarPermisosRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  19/10/2012 09:36:11 a.m.
-- Description:	Procedimiento almacenado que elimina un(a) Permiso
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_EliminarPermisosRol]
	@pIdRol INT
AS
BEGIN
	DELETE SEG.Permiso WHERE IdRol = @pIdRol
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_EliminarPrograma]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_EliminarPrograma]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que elimina un(a) Programa
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_EliminarPrograma]
	@IdPrograma INT
AS
BEGIN
	DELETE SEG.Programa WHERE IdPrograma = @IdPrograma
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_EliminarProgramasRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_EliminarProgramasRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier  Sosa Parada
-- Create date:  15/11/2012
-- Description:	Procedimiento almacenado que elimina programas permitidos a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_EliminarProgramasRol]
	@pIdRol INT
AS
BEGIN
	DELETE FROM SEG.ProgramaRol WHERE IdRol = @pIdRol
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_InsertarModulo]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_InsertarModulo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que guarda un nuevo Modulo
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarModulo]
		@IdModulo INT OUTPUT, 	@NombreModulo NVARCHAR(250),	@Posicion INT,	@Estado BIT, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.Modulo(NombreModulo, Posicion, Estado, UsuarioCreacion, FechaCreacion)
					  VALUES(@NombreModulo, @Posicion, @Estado, @UsuarioCreacion, GETDATE())
	SELECT @IdModulo = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_InsertarPermiso]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_InsertarPermiso]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:38:00 a.m.
-- Description:	Procedimiento almacenado que guarda un nuevo Permiso
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarPermiso]
		@IdPermiso INT OUTPUT, 	@IdPrograma INT,	@IdRol INT,	@Insertar BIT,	@Modificar BIT,	@Eliminar BIT,	@Consultar BIT, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.Permiso(IdPrograma, IdRol, Insertar, Modificar, Eliminar, Consultar, UsuarioCreacion, FechaCreacion)
					  VALUES(@IdPrograma, @IdRol, @Insertar, @Modificar, @Eliminar, @Consultar, @UsuarioCreacion, GETDATE())
	SELECT @IdPermiso = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_InsertarPrograma]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_InsertarPrograma]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que guarda un nuevo Programa
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarPrograma]
		@IdPrograma INT OUTPUT, 	
		@IdModulo INT,	
		@NombrePrograma NVARCHAR(250),	
		@CodigoPrograma NVARCHAR(250),	
		@Posicion INT,	
		@Estado INT, 
		@UsuarioCreacion NVARCHAR(250),
		@VisibleMenu	BIT,
		@GeneraLog	BIT
AS
BEGIN
	INSERT INTO SEG.Programa(IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, VisibleMenu, generaLog)
					  VALUES(@IdModulo, @NombrePrograma, @CodigoPrograma, @Posicion, @Estado, @UsuarioCreacion, GETDATE(), @VisibleMenu, @GeneraLog)
	SELECT @IdPrograma = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_InsertarProgramaRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_InsertarProgramaRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que guarda programa permitido a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarProgramaRol]
		@pIdRol INT
		, @pIdPrograma INT
		, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.ProgramaRol([IdRol], [IdPrograma], [UsuarioCreacion], FechaCreacion)
					  VALUES(@pIdRol, @pIdPrograma, @UsuarioCreacion, GETDATE())
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_InsertarRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_InsertarRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que guarda información de un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarRol]
		  @IdRol INT OUTPUT
		, @ProviderKey VARCHAR(125)
		, @Nombre NVARCHAR(20)
		, @Descripcion NVARCHAR(200)
		, @Estado BIT
		, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.Rol([providerKey], [Nombre], [Descripcion], [Estado], [UsuarioCreacion], FechaCreacion)
					  VALUES(@ProviderKey, @Nombre, @Descripcion, @Estado, @UsuarioCreacion, GETDATE())
	SELECT @IdRol = @@IDENTITY 					  
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ModificarModulo]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ModificarModulo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que actualiza un(a) Modulo
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ModificarModulo]
		@IdModulo INT,	@NombreModulo NVARCHAR(250),	@Posicion INT,	@Estado BIT, @UsuarioModificacion NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Modulo SET NombreModulo = @NombreModulo, Posicion = @Posicion, Estado = @Estado, UsuarioModificacion = @UsuarioModificacion, FechaModificacion = GETDATE() WHERE IdModulo = @IdModulo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ModificarPermiso]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ModificarPermiso]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:38:00 a.m.
-- Description:	Procedimiento almacenado que actualiza un(a) Permiso
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ModificarPermiso]
		@IdPermiso INT,	@IdPrograma INT,	@IdRol INT,	@Insertar BIT,	@Modificar BIT,	@Eliminar BIT,	@Consultar BIT, @UsuarioModificacion NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Permiso SET IdPrograma = @IdPrograma, IdRol = @IdRol, Insertar = @Insertar, Modificar = @Modificar, Eliminar = @Eliminar, Consultar = @Consultar, UsuarioModificacion = @UsuarioModificacion, FechaModificacion = GETDATE() WHERE IdPermiso = @IdPermiso
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ModificarPrograma]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ModificarPrograma]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  12/10/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que actualiza un(a) Programa
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ModificarPrograma]
		@IdPrograma INT,	@IdModulo INT,	@NombrePrograma NVARCHAR(250),	@CodigoPrograma NVARCHAR(250),	@Posicion INT,	@Estado INT, @UsuarioModificacion NVARCHAR(250),	@VisibleMenu	BIT, @GeneraLog	BIT
AS
BEGIN
	UPDATE SEG.Programa SET IdModulo = @IdModulo, NombrePrograma = @NombrePrograma, CodigoPrograma = @CodigoPrograma, Posicion = @Posicion, Estado = @Estado, UsuarioModificacion = @UsuarioModificacion, FechaModificacion = GETDATE(), VisibleMenu = @VisibleMenu, generaLog = @GeneraLog  WHERE IdPrograma = @IdPrograma
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ModificarRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ModificarRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que modifica información de un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ModificarRol]
		@pIdRol INT
		,@ProviderKey VARCHAR(125)
		, @Nombre NVARCHAR(20)
		, @Descripcion NVARCHAR(200)
		, @Estado BIT
		, @UsuarioModificacion NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Rol
	SET providerKey = @ProviderKey
		, [Nombre]=@Nombre
		, [Descripcion]=@Descripcion
		, [Estado]=@Estado
		, [UsuarioModificacion]=@UsuarioModificacion
		, FechaModificacion=GETDATE()
	WHERE IdRol=@pIdRol
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ProgramasModuloRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ProgramasModuloRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Yuri Gereda
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta programas asignados a un rol por módulo indicado
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ProgramasModuloRol]
	@nombreRol NVARCHAR(250),
	@IdModulo INT
AS
BEGIN

SELECT 
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
FROM 
	SEG.ProgramaRol PR
INNER JOIN 
	SEG.Rol R ON PR.IdRol = R.IdRol
INNER JOIN
	SEG.Programa P ON PR.IdPrograma = P.IdPrograma
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo			
WHERE
	R.Nombre = @nombreRol	
AND
	M.IdModulo = @IdModulo
GROUP BY
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
ORDER BY
	P.Posicion DESC	

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_Seg_EliminarProgramasRol]    Script Date: 29/10/2013 21:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_Seg_EliminarProgramasRol]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Oscar Javier  Sosa Parada
-- Create date:  16/10/2012
-- Description:	Procedimiento almacenado que elimina programas permitidos a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_Seg_EliminarProgramasRol]
	@pIdRol INT
AS
BEGIN
	DELETE FROM SEG.ProgramaRol WHERE IdRol = @pIdRol
END
' 
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuario]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ConsultarUsuario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta planes de acción
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuario]
	@IdUsuario int
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
      ,SEG.Usuario.IdTipoUsuario
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
  FROM SEG.Usuario 
  WHERE
	IdUsuario = @IdUsuario
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorDocumento]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 03-16-2012
-- Description:	Procedimiento almacenado que consulta un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorDocumento]
	@pIdTipoDocumento INT
	,@pNumeroDocumento NVARCHAR(17)
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
  FROM SEG.Usuario 
  WHERE
	SEG.Usuario.IdTipoDocumento = @pIdTipoDocumento
    AND SEG.Usuario.NumeroDocumento = @pNumeroDocumento
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 03-10-2012
-- Description:	Procedimiento almacenado que consulta un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]
	@ProviderKey varchar(125)
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
  FROM SEG.Usuario 
  WHERE
	providerKey = @ProviderKey
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuarios]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ConsultarUsuarios]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 03-10-2012
-- Description:	Procedimiento almacenado que consulta un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarios]
	@IdPerfil int,
	@NumeroDocumento NVARCHAR(250),
	@PrimerNombre NVARCHAR(250),
	@PrimerApellido NVARCHAR(250),
	@Estado BIT = NULL
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,Global.TiposDocumentos.NomTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
  FROM SEG.Usuario INNER JOIN [Global].[TiposDocumentos] ON SEG.Usuario.IdTipoDocumento=[Global].[TiposDocumentos].[IdTipoDocumento]
WHERE
	SEG.Usuario.NumeroDocumento = CASE WHEN @NumeroDocumento = '''' THEN SEG.Usuario.NumeroDocumento ELSE @NumeroDocumento END
AND
	SEG.Usuario.PrimerNombre LIKE 	 CASE WHEN @PrimerNombre = '''' THEN SEG.Usuario.PrimerNombre ELSE ''%''+@PrimerNombre+''%'' END
AND
	SEG.Usuario.PrimerApellido LIKE 	CASE WHEN @PrimerApellido = '''' THEN SEG.Usuario.PrimerApellido ELSE ''%''+@PrimerApellido+''%'' END
AND 
	SEG.Usuario.Estado = CASE WHEN @Estado	IS NULL THEN SEG.Usuario.Estado ELSE @Estado END

END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_InsertarUsuario]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_InsertarUsuario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  08/10/2012
-- Description:	Procedimiento almacenado que guarda información de un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_InsertarUsuario]
		  @IdUsuario INT OUTPUT
		, @IdTipoPersona INT
		, @IdTipoDocumento INT
		, @NumeroDocumento NVARCHAR(17)
		, @DV NVARCHAR(1)
		, @PrimerNombre NVARCHAR(150)
		, @SegundoNombre NVARCHAR(150)
		, @PrimerApellido NVARCHAR(150)
		, @SegundoApellido NVARCHAR(150)
		, @RazonSocial NVARCHAR(150)
		, @TelefonoContacto NVARCHAR(10)
		, @CorreoElectronico NVARCHAR(150)
		, @Estado BIT
		, @ProviderKey VARCHAR(125)
		, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.Usuario([IdTipoPersona]
		   ,[IdTipoDocumento]
           ,[NumeroDocumento]
		   ,[DV]
           ,[PrimerNombre]
           ,[SegundoNombre]
           ,[PrimerApellido]
           ,[SegundoApellido]
		   ,[RazonSocial]
           ,[TelefonoContacto]
           ,[CorreoElectronico]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           ,[providerKey])
	VALUES(@IdTipoPersona
		   ,@IdTipoDocumento
           ,@NumeroDocumento
		   ,@DV
           ,@PrimerNombre
           ,@SegundoNombre
           ,@PrimerApellido
           ,@SegundoApellido
		   ,@RazonSocial
           ,@TelefonoContacto
           ,@CorreoElectronico
           ,@Estado
           ,@UsuarioCreacion
           ,GETDATE()
           ,@ProviderKey)
	SELECT @IdUsuario = @@IDENTITY
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ModificarUsuario]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ModificarUsuario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  16/10/2012
-- Description:	Procedimiento almacenado que modifica información de un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ModificarUsuario]
		  @IdUsuario INT
		, @IdTipoPersona INT
		, @IdTipoDocumento INT
		, @NumeroDocumento NVARCHAR(17)
		, @DV NVARCHAR(1)
		, @PrimerNombre NVARCHAR(150)
		, @SegundoNombre NVARCHAR(150)
		, @PrimerApellido NVARCHAR(150)
		, @SegundoApellido NVARCHAR(150)
		, @RazonSocial NVARCHAR(150)
		, @CorreoElectronico NVARCHAR(150)
		, @TelefonoContacto NVARCHAR(10)
		, @Estado BIT
		, @UsuarioModificacion NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Usuario
		SET [IdTipoDocumento] = @IdTipoDocumento
		   ,[NumeroDocumento] = @NumeroDocumento
		   ,[PrimerNombre] = @PrimerNombre
		   ,[SegundoNombre] = @SegundoNombre
		   ,[PrimerApellido] = @PrimerApellido
		   ,[SegundoApellido] = @SegundoApellido
		   ,[CorreoElectronico] = @CorreoElectronico
		   ,[Estado] = @Estado
		   ,[UsuarioModificacion] = @UsuarioModificacion
		   ,[FechaModificacion] = GETDATE()
		   ,[IdTipoPersona] = @IdTipoPersona
		   ,[DV] = @DV
		   ,[RazonSocial] = @RazonSocial
		   ,[TelefonoContacto] = @TelefonoContacto
	WHERE [IdUsuario] = @IdUsuario
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametro_Consultar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_Parametro_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que consulta un(a) Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Consultar]
	@IdParametro INT
AS
BEGIN
 SELECT IdParametro, NombreParametro, ValorParametro, ImagenParametro, Estado, Funcionalidad, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [SEG].[Parametro] 
 WHERE  IdParametro = @IdParametro
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametro_Eliminar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_Parametro_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que elimina un(a) Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Eliminar]
	@IdParametro INT
AS
BEGIN
	DELETE SEG.Parametro WHERE IdParametro = @IdParametro
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametro_Insertar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_Parametro_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Insertar]
		@IdParametro INT OUTPUT, 	@NombreParametro NVARCHAR(128),	@ValorParametro NVARCHAR(256),	
		@ImagenParametro NVARCHAR(256),	@Estado BIT, @Funcionalidad INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.Parametro(NombreParametro, ValorParametro, ImagenParametro, Estado, Funcionalidad, UsuarioCrea, FechaCrea)
					  VALUES(@NombreParametro, @ValorParametro, @ImagenParametro, @Estado, @Funcionalidad, @UsuarioCrea, GETDATE())
	SELECT @IdParametro = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametro_Modificar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_Parametro_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Modificar]
		@IdParametro INT,	@NombreParametro NVARCHAR(128),	@ValorParametro NVARCHAR(256),	
		@ImagenParametro NVARCHAR(256) = NULL,	@Estado BIT, @Funcionalidad NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Parametro 
	SET NombreParametro = @NombreParametro, ValorParametro = @ValorParametro, 
	ImagenParametro = ISNULL(@ImagenParametro,ImagenParametro), 
	Estado = @Estado, Funcionalidad = @Funcionalidad,
	UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdParametro = @IdParametro
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_Parametros_Consultar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_Parametros_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametros_Consultar]
@NombreParametro NVARCHAR (128) = NULL, @ValorParametro NVARCHAR (256) = NULL, @Estado BIT = NULL,
@Funcionalidad NVARCHAR (128) = NULL
AS
BEGIN

	SELECT
		IdParametro,
		NombreParametro,
		ValorParametro,
		ImagenParametro,
		Estado,
		Funcionalidad,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [SEG].[Parametro]
	WHERE NombreParametro LIKE CASE
		WHEN @NombreParametro IS NULL THEN NombreParametro ELSE @NombreParametro + ''%''
	END
	AND ValorParametro LIKE
		CASE
			WHEN @ValorParametro IS NULL THEN ValorParametro ELSE @ValorParametro + ''%''
		END
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado 
		END
	AND Funcionalidad LIKE CASE
		WHEN @Funcionalidad IS NULL THEN Funcionalidad ELSE @Funcionalidad + ''%''
	END
	
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_TalentoHumanoPromotor_Consultar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_TalentoHumanoPromotor_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta los promotores por contrato

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TalentoHumanoPromotor_Consultar]
	      @IdContrato INT
AS
BEGIN
SELECT     PAE.TalentoHumano.IdTalentoHumano, 
           PAE.TalentoHumano.PrimerNombre, 
           PAE.TalentoHumano.SegundoNombre, 
           PAE.TalentoHumano.PrimerApellido, 
           PAE.TalentoHumano.SegundoApellido,
           PAE.TalentoHumano.Identificacion
         FROM         ECO.Cargo INNER JOIN
                      PAE.TalentoHumano ON ECO.Cargo.IdCargo = PAE.TalentoHumano.IdCargo INNER JOIN
                      GCB.ContratoTalentoHumano ON PAE.TalentoHumano.IdTalentoHumano = GCB.ContratoTalentoHumano.IdTalentoHumano
WHERE     (ECO.Cargo.NombreCargo = ''PROMOTOR DE DERECHOS'') 
AND (GCB.ContratoTalentoHumano.IdContrato = @IdContrato)

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_TalentoHumanoPromotor_Consultar_Cantidad]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_TalentoHumanoPromotor_Consultar_Cantidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta los promotores por contrato

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TalentoHumanoPromotor_Consultar_Cantidad]
	      @IdContrato INT,
	      @CantidadMax INT
AS
BEGIN
	SELECT     PAE.TalentoHumano.IdTalentoHumano, 
			   PAE.TalentoHumano.PrimerNombre, 
			   PAE.TalentoHumano.SegundoNombre, 
			   PAE.TalentoHumano.PrimerApellido, 
			   PAE.TalentoHumano.SegundoApellido,
			   PAE.TalentoHumano.Identificacion
	FROM       ECO.Cargo INNER JOIN
				PAE.TalentoHumano ON ECO.Cargo.IdCargo = PAE.TalentoHumano.IdCargo INNER JOIN
				GCB.ContratoTalentoHumano ON PAE.TalentoHumano.IdTalentoHumano = GCB.ContratoTalentoHumano.IdTalentoHumano
	WHERE     (ECO.Cargo.NombreCargo = ''PROMOTOR DE DERECHOS'') 
	AND (GCB.ContratoTalentoHumano.IdContrato = @IdContrato)
	and   PAE.TalentoHumano.IdTalentoHumano NOT IN (SELECT IdPromotor
														from GCB.GruposContrato
														group by IdPromotor
														HAVING COUNT(IdPromotor) >= @CantidadMax )

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_TipoUsuario_Consultar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_TipoUsuario_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que consulta un(a) TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_Consultar]
	@IdTipoUsuario INT
AS
BEGIN
 SELECT IdTipoUsuario, CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SEG].[TipoUsuario] WHERE  IdTipoUsuario = @IdTipoUsuario
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_TipoUsuario_Eliminar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_TipoUsuario_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que elimina un(a) TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_Eliminar]
	@IdTipoUsuario INT
AS
BEGIN
	DELETE SEG.TipoUsuario WHERE IdTipoUsuario = @IdTipoUsuario
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_TipoUsuario_Insertar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_TipoUsuario_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que guarda un nuevo TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_Insertar]
		@IdTipoUsuario INT OUTPUT, 	@CodigoTipoUsuario NVARCHAR(128),	@NombreTipoUsuario NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.TipoUsuario(CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoUsuario, @NombreTipoUsuario, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoUsuario = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_TipoUsuario_Modificar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_TipoUsuario_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que actualiza un(a) TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_Modificar]
		@IdTipoUsuario INT,	@CodigoTipoUsuario NVARCHAR(128),	@NombreTipoUsuario NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.TipoUsuario SET CodigoTipoUsuario = @CodigoTipoUsuario, NombreTipoUsuario = @NombreTipoUsuario, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoUsuario = @IdTipoUsuario
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_SEG_TipoUsuario_sConsultar]    Script Date: 29/10/2013 22:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_SEG_TipoUsuario_sConsultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que consulta un(a) TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_sConsultar]
	@CodigoTipoUsuario NVARCHAR(128) = NULL,@NombreTipoUsuario NVARCHAR(256) = NULL,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdTipoUsuario, CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SEG].[TipoUsuario] WHERE CodigoTipoUsuario = CASE WHEN @CodigoTipoUsuario IS NULL THEN CodigoTipoUsuario ELSE @CodigoTipoUsuario END AND NombreTipoUsuario = CASE WHEN @NombreTipoUsuario IS NULL THEN NombreTipoUsuario ELSE @NombreTipoUsuario END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
' 
END
GO

/****** Object:  StoredProcedure [dbo].[usp_Global_Seg_ConsultarTodosTipoDocumento]    Script Date: 10/30/2013 00:52:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Global_Seg_ConsultarTodosTipoDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Global_Seg_ConsultarTodosTipoDocumento]
GO


/****** Object:  StoredProcedure [dbo].[usp_Global_Seg_ConsultarTodosTipoDocumento]    Script Date: 10/30/2013 00:52:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:        Oscar Javier Sosa Parada
-- Create date: 08-10-2012
-- Description:   Procedimiento almacenado para la consulta de tipos de documento
-- =============================================
CREATE PROCEDURE [dbo].[usp_Global_Seg_ConsultarTodosTipoDocumento]
AS
BEGIN
  SELECT 
         IdTipoDocumento
      ,CodDocumento
      ,NomTipoDocumento
  FROM Global.TiposDocumentos
END


GO