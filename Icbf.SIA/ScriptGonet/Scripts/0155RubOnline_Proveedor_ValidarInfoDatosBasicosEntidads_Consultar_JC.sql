USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]    Script Date: 04/11/2014 10:43:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]    Script Date: 04/11/2014 10:43:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]
	@IdEntidad int = NULL,@Observaciones NVARCHAR(200) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
 ORDER BY FechaCrea DESC
END






GO


