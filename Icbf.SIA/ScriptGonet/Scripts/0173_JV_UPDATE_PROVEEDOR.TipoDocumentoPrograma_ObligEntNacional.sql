USE [SIA]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 23-Abri-2014
Descripci�n: Update PROVEEDOR.TipoDocumentoPrograma SET ObligEntNacional=1 cuando
TipoPersona Juridica Y Sector Privado y Doc.='Documento que acredite el nombramiento y facultades del representante legal'
**/

DECLARE @IDTipoDocPrograma INT =(
SELECT IdTipoDocumentoPrograma FROM 
PROVEEDOR.TipoDocumentoPrograma tdp
INNER JOIN SEG.Programa Prog ON tdp.IdPrograma=Prog.IdPrograma
INNER JOIN PROVEEDOR.TipoDocumento td ON tdp.IdTipoDocumento=td.IdTipoDocumento
WHERE
Prog.CodigoPrograma='PROVEEDOR/GestionProveedores'
AND td.Descripcion='Documento que acredite el nombramiento y facultades del representante legal'
AND tdp.Estado=1
AND tdp.ObligPersonaJuridica=1
AND tdp.ObligSectorPrivado=1)


UPDATE PROVEEDOR.TipoDocumentoPrograma
SET ObligEntNacional=1
WHERE IdTipoDocumentoPrograma=@IDTipoDocPrograma
