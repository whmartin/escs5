USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado]    Script Date: 08/20/2014 08:53:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado]    Script Date: 08/20/2014 08:53:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 05/AGO/2014
-- Description: Determina de acuerdo al n�mero de integrantes definidos para una entidad,
-- y los ya asociados a este; que estado de validaci�n debe asignarse al m�dulo Integrantes.
-- o Elimina el registro de Validaci�n del m�dulo si es que los integrantes no han sido asociados
-- en su totalidad.
-- Modificado Por: Juan Carlos Valverde S�mano
-- Fecha: 20/AGO/2014
-- Descripci�n: De acuerdo a un an�lisis realizado con Sandra.Mendez se ha hecho el cambio para que
-- la informaci�n del m�dulo integrantes pueda ser validada a�n cuando no se ha completado en su totalidad
-- la asociaci�n de Integrantes; en la conclusi�n de que eso es responsabilidad del usuario.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado]
@IdEntidad INT
AS
BEGIN
	DECLARE 
	@IntegrantesAsocidos INT

	DECLARE @IdEstadoRegistrado INT =(
	SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='REGISTRADO')
	DECLARE @IdEstadoPorValidar INT = (
	SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='POR VALIDAR')

	

	SET @IntegrantesAsocidos=(SELECT COUNT(IdIntegrante) FROM PROVEEDOR.Integrantes 
	WHERE IdEntidad=@IdEntidad)
	IF(@IntegrantesAsocidos =0)
	BEGIN
		DELETE FROM PROVEEDOR.ValidacionIntegrantesEntidad
		WHERE IdEntidad=@IdEntidad
		UPDATE PROVEEDOR.ValidarInfoIntegrantesEntidad
		SET Activo=0 WHERE IdEntidad=@IdEntidad
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT IdValidacionIntegrantesEntidad FROM PROVEEDOR.ValidacionIntegrantesEntidad
		WHERE IdEntidad=@IdEntidad)
		BEGIN
			UPDATE PROVEEDOR.ValidacionIntegrantesEntidad
			SET IdEstadoValidacionIntegrantes=@IdEstadoPorValidar,
			FechaModifica = GETDATE(),
			UsuarioModifica= 'TR_ProveedorIntegrantes'
			WHERE IdEntidad=@IdEntidad
		END
		ELSE
		BEGIN
			DECLARE @CheckNroRevision INT=1
			IF EXISTS (SELECT NroRevision FROM 
			PROVEEDOR.ValidarInfoIntegrantesEntidad
			WHERE IdEntidad=@IdEntidad)
			BEGIN		
				SET @CheckNroRevision=((SELECT MAX(NroRevision) FROM 
				PROVEEDOR.ValidarInfoIntegrantesEntidad
				WHERE IdEntidad=@IdEntidad) +1)
			END
			INSERT INTO PROVEEDOR.ValidacionIntegrantesEntidad
			(IdEntidad, IdEstadoValidacionIntegrantes, NroRevision, Finalizado, FechaCrea, UsuarioCrea, CorreoEnviado)
			VALUES(@IdEntidad, @IdEstadoRegistrado, @CheckNroRevision, 0, GETDATE(), 'TR_ProveedorIntegrantes',0)
		END
	END
END


GO


