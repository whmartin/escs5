USE[SIA]
GO

IF NOT EXISTS(select * from sys.columns 
            where Name = N'Editable' and Object_ID = Object_ID(N'PROVEEDOR.Sucursal'))
BEGIN
	ALTER TABLE PROVEEDOR.Sucursal
	ADD Editable INT
END

IF EXISTS(select * from sys.columns 
            where Name = N'Indicativo' and Object_ID = Object_ID(N'PROVEEDOR.Sucursal'))
BEGIN
	ALTER TABLE PROVEEDOR.Sucursal
	ALTER COLUMN Indicativo INT NULL
END

IF EXISTS(select * from sys.columns 
            where Name = N'Telefono' and Object_ID = Object_ID(N'PROVEEDOR.Sucursal'))
BEGIN
	ALTER TABLE PROVEEDOR.Sucursal
	ALTER COLUMN Telefono INT NULL
END