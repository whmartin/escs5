/*
Desarrollador: Juan Carlos Valverde S�mano
Fecha: 07-Mayo-2014
Descrpci�n: Son Updates que estandarizan los estados de Datos B�sicos,
InfoFinanciera, InfoExperiencia y Estado de Proveedor.
**/
USE [SIA]
UPDATE PROVEEDOR.EstadoDatosBasicos
SET Descripcion='POR AJUSTAR'
WHERE Descripcion='POR AJUSTE'


UPDATE PROVEEDOR.EstadoValidacionDocumental
SET Descripcion='REGISTRADO'
WHERE Descripcion='PENDIENTE DE VALIDACI�N'

UPDATE PROVEEDOR.EstadoValidacionDocumental
SET Descripcion='PARCIAL'
WHERE Descripcion='DATOS PARCIALES'




DECLARE @Codigo NVARCHAR(2)
DECLARE @Cod INT = 
(SELECT TOP(1)CodigoEstadoValidacionDocumental 
FROM PROVEEDOR.EstadoValidacionDocumental
ORDER BY IdEstadoValidacionDocumental DESC)+1

IF(LEN(@Cod)=1)
BEGIN
SET @Codigo=('0' + CAST(@Cod AS VARCHAR))
END
ELSE IF(LEN(@Cod)>1)
BEGIN
SET @Codigo=CAST(@Cod AS VARCHAR)
END

IF NOT EXISTS (SELECT IdEstadoValidacionDocumental FROM 
PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR VALIDAR')
BEGIN
INSERT INTO PROVEEDOR.EstadoValidacionDocumental
(CodigoEstadoValidacionDocumental,Descripcion,Estado,UsuarioCrea,FechaCrea)
VaLUES(@Codigo,'POR VALIDAR',1,'Administrador',GETDATE())
END

-----------------------------------------------
DECLARE @IdPARCIAL INT =(
SELECT IdEstadoTercero FROM Oferente.EstadoTercero
WHERE DescripcionEstado='PARCIAL')
USE[Oferentes]
UPDATE  Oferente.TERCERO
SET IDESTADOTERCERO=1
WHERE IDESTADOTERCERO=@IdPARCIAL

DELETE FROM Oferente.EstadoTercero
WHERE DescripcionEstado='PARCIAL'


-------------------------------------------------