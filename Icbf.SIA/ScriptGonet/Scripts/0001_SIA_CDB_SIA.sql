USE master
GO
/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripcion: Creación de la Base de Datos de SIA

Se debe configrurar la ubicacion de los archivos de base de datos..

*/
IF NOT EXISTS(SELECT NAME
			FROM sys.databases
			WHERE name = 'SIA')
BEGIN
	CREATE DATABASE [SIA] ON  PRIMARY 
	( NAME = N'SIA', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\SIA.mdf' , SIZE = 3072KB , FILEGROWTH = 1024KB )
	 LOG ON 
	( NAME = N'SIA_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\SIA_Log.ldf' , SIZE = 1024KB , FILEGROWTH = 10%)
END
GO