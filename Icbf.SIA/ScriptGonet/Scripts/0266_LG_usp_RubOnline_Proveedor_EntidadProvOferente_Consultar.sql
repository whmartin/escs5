USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]
GO

-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- Modificado: 10/04/2014 Por: Leticia Elizabeth Gonzalez
-- Descricion: Se agrega la columna EstadoProveedor
-- Modificado: 19/06/2014 Leticia Elizabeth González
-- Descripcion: Se agrega la comlumna IdTipoSector y Digito de Verificacion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]
	@IdTipoPersona INT = NULL,@Tipoidentificacion INT= NULL,@Identificacion NVARCHAR(256) = NULL,
	@Proveedor NVARCHAR(256) = NULL,@IdEstado INT = NULL,@UsuarioCrea NVARCHAR(128)=NULL
AS
BEGIN
 SELECT DISTINCT * FROM (
  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador,EP.UsuarioCrea, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor, EP.IdTipoSector, EP.TipoEntOfProv, T.DIGITOVERIFICACION
  FROM [Proveedor].[EntidadProvOferente] EP 
     INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
     WHERE T.IdTipoPersona=1
UNION 
 SELECT  EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.RAZONSOCIAL ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado ,EP.ObserValidador,EP.UsuarioCrea, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor, EP.IdTipoSector, EP.TipoEntOfProv, T.DIGITOVERIFICACION
  FROM [Proveedor].[EntidadProvOferente] EP 
    INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
    LEFT JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
    INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona
    INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
    INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
    INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
    WHERE T.IdTipoPersona=2
    ) DT
     
    
    WHERE IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN IdTipoPersona ELSE @IdTipoPersona END 
		  AND IDTIPODOCIDENTIFICA = CASE WHEN @Tipoidentificacion IS NULL THEN IDTIPODOCIDENTIFICA ELSE @Tipoidentificacion END 
		  AND NUMEROIDENTIFICACION = CASE WHEN @Identificacion IS NULL THEN NUMEROIDENTIFICACION ELSE @Identificacion END 
		  AND (Razonsocila) like CASE WHEN @Proveedor IS NULL THEN (Razonsocila) ELSE '%'+ @Proveedor +'%' END 
		  AND IdEstadoProveedor = CASE WHEN @IdEstado IS NULL THEN IdEstadoProveedor ELSE @IdEstado END 
		  AND UsuarioCrea = CASE WHEN @UsuarioCrea IS NULL THEN UsuarioCrea ELSE @UsuarioCrea END 

END

