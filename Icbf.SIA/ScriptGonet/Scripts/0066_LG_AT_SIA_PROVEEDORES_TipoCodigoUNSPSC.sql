USE [SIA]
GO

IF EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'Descripcion' and Object_ID = Object_ID(N'PROVEEDOR.TipoCodigoUNSPSC'))
BEGIN
	ALTER TABLE PROVEEDOR.TipoCodigoUNSPSC
	ALTER COLUMN Descripcion NVARCHAR(250)
END

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'CodigoSegmento' and Object_ID = Object_ID(N'PROVEEDOR.TipoCodigoUNSPSC'))
BEGIN
	ALTER TABLE PROVEEDOR.TipoCodigoUNSPSC
	ADD CodigoSegmento NVARCHAR(50)
END

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'NombreSegmento' and Object_ID = Object_ID(N'PROVEEDOR.TipoCodigoUNSPSC'))
BEGIN
	ALTER TABLE PROVEEDOR.TipoCodigoUNSPSC
	ADD NombreSegmento NVARCHAR(250)
END

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'CodigoFamilia' and Object_ID = Object_ID(N'PROVEEDOR.TipoCodigoUNSPSC'))
BEGIN
	ALTER TABLE PROVEEDOR.TipoCodigoUNSPSC
	ADD CodigoFamilia NVARCHAR(50)
END

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'NombreFamilia' and Object_ID = Object_ID(N'PROVEEDOR.TipoCodigoUNSPSC'))
BEGIN
	ALTER TABLE PROVEEDOR.TipoCodigoUNSPSC
	ADD NombreFamilia NVARCHAR(250)
END

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'CodigoClase' and Object_ID = Object_ID(N'PROVEEDOR.TipoCodigoUNSPSC'))
BEGIN
	ALTER TABLE PROVEEDOR.TipoCodigoUNSPSC
	ADD CodigoClase NVARCHAR(50)
END

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'NombreClase' and Object_ID = Object_ID(N'PROVEEDOR.TipoCodigoUNSPSC'))
BEGIN
	ALTER TABLE PROVEEDOR.TipoCodigoUNSPSC
	ADD NombreClase NVARCHAR(250)
END
