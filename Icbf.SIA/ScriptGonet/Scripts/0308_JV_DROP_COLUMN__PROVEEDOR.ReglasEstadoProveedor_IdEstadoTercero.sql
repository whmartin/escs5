USE [SIA]
GO
/**
Autor: Juan Carlos Valverde S�mano
Fecha: 30-Jul-2014
Descripci�n: Elimina de la Entidad PROVEEDIOR.ReglasEstadoProveedor la columna
IdEstadoTercero, ya que est� ya no es necesaria. Estaba en funci�on cunado el
Tercero Heredaba el estado del Proveedor.
**/

IF  EXISTS(
SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'PROVEEDOR' and TABLE_NAME = 'ReglasEstadoProveedor' and COLUMN_NAME = 'IdEstadoTercero'
)
BEGIN
	ALTER TABLE PROVEEDOR.ReglasEstadoProveedor
	DROP COLUMN IdEstadoTercero
END