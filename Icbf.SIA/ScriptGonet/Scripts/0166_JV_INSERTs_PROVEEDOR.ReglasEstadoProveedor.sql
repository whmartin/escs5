USE [SIA]
/**
Desarrollador: Juan Carlos Valverde Sámano
Fecha: 11-Abril-2014
Descripción: Realiza INSERTs en la Tabla de reglas para Obtener el 
Estado del Proveedor.
**/
GO
TRUNCATE TABLE [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
GO

/****** Object:  Index [UNIQUE_PROVEEDOR_ReglasEstadoProveedor]    Script Date: 04/21/2014 18:52:59 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[ReglasEstadoProveedor]') AND name = N'UNIQUE_PROVEEDOR_ReglasEstadoProveedor')
DROP INDEX [UNIQUE_PROVEEDOR_ReglasEstadoProveedor] ON [PROVEEDOR].[ReglasEstadoProveedor] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO

/****** Object:  Index [UNIQUE_PROVEEDOR_ReglasEstadoProveedor]    Script Date: 04/21/2014 18:53:10 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UNIQUE_PROVEEDOR_ReglasEstadoProveedor] ON [PROVEEDOR].[ReglasEstadoProveedor] 
(IdEstadoDatosBasicos,IdEstadoDatosFinancieros,IdEstadoDatosExperiencia)
GO
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='REGISTRADO'),(0),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='REGISTRADO'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='REGISTRADO'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(0),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='VALIDADO'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='EN VALIDACIÓN'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='EN VALIDACIÓN'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(0),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='EN VALIDACIÓN'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='EN VALIDACIÓN'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTE'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTE'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(0),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTE'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR AJUSTE'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(0),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='POR VALIDAR'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(0),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR VALIDAR'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='EN VALIDACIÓN'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='POR AJUSTE'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='VALIDADO'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='PENDIENTE DE VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
INSERT INTO [SIA].[PROVEEDOR].[ReglasEstadoProveedor]
           ([IdEstadoDatosBasicos]
           ,[IdEstadoDatosFinancieros]
           ,[IdEstadoDatosExperiencia]
           ,[IdEstadoProveedor]
           ,[IdEstadoTercero]
           ,[Estado])
     VALUES
           ((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos WHERE Descripcion='VALIDADO'),(0),(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='POR AJUSTAR'),(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),(SELECT IdEstadoTercero FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='PARCIAL'),1)
