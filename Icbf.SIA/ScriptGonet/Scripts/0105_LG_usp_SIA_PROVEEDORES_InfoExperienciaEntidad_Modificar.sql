USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
GO

USE [SIA]
GO
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  03/10/2014 13:02:15 PM
-- Description:	Procedimiento almacenado que Modifica Experiencias
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
		@IdExpEntidad INT,	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	
		@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	
		@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(256),	@FechaInicio DATETIME,	@FechaFin DATETIME,	
		@NumeroContrato NVARCHAR(128),	@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(21,3),	@ExperienciaMeses NUMERIC(21,3),
		@EstadoDocumental INT,	@UnionTempConsorcio BIT,	@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	
		@JardinOPreJardin BIT, @UsuarioModifica NVARCHAR(250), @Finalizado BIT = NULL, @ContratoEjecucion BIT
AS
BEGIN
	UPDATE Proveedor.InfoExperienciaEntidad 
	SET IdEntidad = @IdEntidad, IdTipoSector = @IdTipoSector, IdTipoEstadoExp = @IdTipoEstadoExp, 
	IdTipoModalidadExp = @IdTipoModalidadExp, IdTipoModalidad = @IdTipoModalidad, 
	IdTipoPoblacionAtendida = @IdTipoPoblacionAtendida, IdTipoRangoExpAcum = @IdTipoRangoExpAcum, 
	IdTipoCodUNSPSC = @IdTipoCodUNSPSC, IdTipoEntidadContratante = @IdTipoEntidadContratante, 
	EntidadContratante = @EntidadContratante, FechaInicio = @FechaInicio, FechaFin = @FechaFin, 
	NumeroContrato = @NumeroContrato, ObjetoContrato = @ObjetoContrato, Vigente = @Vigente, 
	Cuantia = @Cuantia, ExperienciaMeses = @ExperienciaMeses,
	EstadoDocumental = @EstadoDocumental, UnionTempConsorcio = @UnionTempConsorcio, 
	PorcentParticipacion = @PorcentParticipacion, AtencionDeptos = @AtencionDeptos, 
	JardinOPreJardin = @JardinOPreJardin, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE(), 
	Finalizado = ISNULL(@Finalizado,Finalizado),
	ContratoEjecucion = @ContratoEjecucion
	WHERE IdExpEntidad = @IdExpEntidad
END



