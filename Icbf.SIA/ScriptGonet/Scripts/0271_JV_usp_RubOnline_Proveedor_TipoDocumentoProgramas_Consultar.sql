USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]    Script Date: 06/25/2014 10:10:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]    Script Date: 06/25/2014 10:10:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Fabian valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]
@IdTipoDocumentoPrograma INT = NULL, @IdTipoDocumento INT = NULL, @IdPrograma INT = NULL
AS
BEGIN
SELECT
	Proveedor.TipoDocumentoPrograma.IdTipoDocumentoPrograma,
	Proveedor.TipoDocumentoPrograma.IdTipoDocumento,
	Proveedor.TipoDocumentoPrograma.IdPrograma,
	Proveedor.TipoDocumentoPrograma.Estado,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	Proveedor.TipoDocumentoPrograma.ObligRupNoRenovado,
	Proveedor.TipoDocumentoPrograma.ObligRupRenovado,
	Proveedor.TipoDocumentoPrograma.ObligPersonaJuridica,
	Proveedor.TipoDocumentoPrograma.ObligPersonaNatural,
	Proveedor.TipoDocumentoPrograma.ObligSectorPublico,
	Proveedor.TipoDocumentoPrograma.ObligSectorPrivado,
	Proveedor.TipoDocumentoPrograma.ObligEntNacional,
	Proveedor.TipoDocumentoPrograma.ObligEntExtranjera,
	Proveedor.TipoDocumentoPrograma.UsuarioCrea,
	Proveedor.TipoDocumentoPrograma.FechaCrea,
	Proveedor.TipoDocumentoPrograma.UsuarioModifica,
	Proveedor.TipoDocumentoPrograma.FechaModifica,
	Proveedor.TipoDocumento.CodigoTipoDocumento,
	Proveedor.TipoDocumento.Descripcion,
	SEG.Programa.CodigoPrograma,
	SEG.Programa.NombrePrograma,
	Proveedor.TipoDocumentoPrograma.ObligConsorcio,
	Proveedor.TipoDocumentoPrograma.ObligUnionTemporal
FROM Proveedor.TipoDocumentoPrograma
INNER JOIN Proveedor.TipoDocumento
	ON Proveedor.TipoDocumentoPrograma.IdTipoDocumento = Proveedor.TipoDocumento.IdTipoDocumento
INNER JOIN SEG.Programa
	ON Proveedor.TipoDocumentoPrograma.IdPrograma = SEG.Programa.IdPrograma 
WHERE    IdTipoDocumentoPrograma =
	CASE
		WHEN @IdTipoDocumentoPrograma IS NULL THEN IdTipoDocumentoPrograma ELSE @IdTipoDocumentoPrograma
END AND Proveedor.TipoDocumentoPrograma.IdTipoDocumento =
	CASE
		WHEN @IdTipoDocumento IS NULL THEN Proveedor.TipoDocumentoPrograma.IdTipoDocumento ELSE @IdTipoDocumento
END AND Proveedor.TipoDocumentoPrograma.IdPrograma =
	CASE
		WHEN @IdPrograma IS NULL THEN Proveedor.TipoDocumentoPrograma.IdPrograma ELSE @IdPrograma
END
END



	

GO


