USE [SIA]
/**
Desarrollador: Juan Carlos Valverde Sámano
Fecha: 23-Abri-2014
Descripción: Update PROVEEDOR.TipoDocumentoPrograma SET ObligEntExtranjera=1 cuando
TipoPersona Juridica Y Sector Privado y Doc.='Certificado de existencia y representación legal o de domicilio para entidades extranjeras'
**/

DECLARE @IDTipoDocPrograma INT =(
SELECT IdTipoDocumentoPrograma FROM 
PROVEEDOR.TipoDocumentoPrograma tdp
INNER JOIN SEG.Programa Prog ON tdp.IdPrograma=Prog.IdPrograma
INNER JOIN PROVEEDOR.TipoDocumento td ON tdp.IdTipoDocumento=td.IdTipoDocumento
WHERE
Prog.CodigoPrograma='PROVEEDOR/GestionProveedores'
AND td.Descripcion='Certificado de existencia y representación legal o de domicilio para entidades extranjeras'
AND tdp.Estado=1
AND tdp.ObligPersonaJuridica=1
AND tdp.ObligSectorPrivado=1)


UPDATE PROVEEDOR.TipoDocumentoPrograma
SET ObligEntExtranjera=1
WHERE IdTipoDocumentoPrograma=@IDTipoDocPrograma