USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]    Script Date: 03/25/2014 16:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]    Script Date: 03/25/2014 16:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero
---- Desarrollador Modificacion: Juan Carlos Valverde S�mano
-- Fecha modificaci�n: 25/03/2014
-- Descripci�n: Se agreg� en el WHERE la condici�n de que solo muestre los registros donde el campo Activo es = 1.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]
	@IdDocAdjunto INT = NULL,
	@IdTercero INT = NULL,
	@IdDocumento INT = NULL
AS
BEGIN

SELECT			      Proveedor.DOCADJUNTOTERCERO.IDDOCADJUNTO, 
					  Proveedor.DOCADJUNTOTERCERO.IDTERCERO, 
					  Proveedor.DOCADJUNTOTERCERO.IDDOCUMENTO, 
                      Proveedor.DOCADJUNTOTERCERO.DESCRIPCION, 
                      Proveedor.DOCADJUNTOTERCERO.LINKDOCUMENTO, 
                      Proveedor.DOCADJUNTOTERCERO.ANNO, 
                      Proveedor.DOCADJUNTOTERCERO.USUARIOCREA, 
                      Proveedor.DOCADJUNTOTERCERO.FECHACREA, 
                      Proveedor.DOCADJUNTOTERCERO.USUARIOMODIFICA, 
                      Proveedor.DOCADJUNTOTERCERO.FECHAMODIFICA, 
                      Proveedor.TipoDocumento.CodigoTipoDocumento, 
                      Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento
FROM				  Proveedor.DOCADJUNTOTERCERO INNER JOIN
                      Proveedor.TipoDocumento ON Proveedor.DOCADJUNTOTERCERO.IDDOCUMENTO = Proveedor.TipoDocumento.IdTipoDocumento
WHERE				  IdDocAdjunto = CASE WHEN @IdDocAdjunto IS NULL THEN IdDocAdjunto ELSE @IdDocAdjunto END AND 
					  IdTercero = CASE WHEN @IdTercero IS NULL THEN IdTercero ELSE @IdTercero END AND IdDocumento = CASE WHEN @IdDocumento IS NULL THEN IdDocumento ELSE @IdDocumento END
					  AND Proveedor.DOCADJUNTOTERCERO.Activo=1
					  
END

GO


