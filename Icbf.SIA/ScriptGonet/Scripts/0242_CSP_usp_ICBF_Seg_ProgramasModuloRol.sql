USE [RubOnline]
GO


If (select Count(*) from sysobjects where type='P' and name='usp_ICBF_Seg_ProgramasModuloRol')>0 
BEGIN
	DROP PROCEDURE dbo.usp_ICBF_Seg_ProgramasModuloRol;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	Se agregan los campos EsReporte y IdReporte
-- =============================================
-- =============================================
-- Author:		Yuri Gereda
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta programas asignados a un rol por módulo indicado
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ProgramasModuloRol]
	@nombreRol NVARCHAR(250),
	@IdModulo INT
AS
BEGIN

SELECT P.IdPrograma
      ,P.IdModulo
      ,P.NombrePrograma
      ,P.CodigoPrograma
      ,P.Posicion
      ,P.Estado
      ,P.UsuarioCreacion
      ,P.FechaCreacion
      ,P.UsuarioModificacion
      ,P.FechaModificacion
      ,M.NombreModulo
      ,P.VisibleMenu
      ,P.EsReporte 
      ,P.IdReporte
FROM SEG.ProgramaRol PR (NoLock)
	 INNER JOIN SEG.Rol R (NoLock) ON  PR.IdRol = R.IdRol
	 INNER JOIN SEG.Programa P (NoLock) ON PR.IdPrograma = P.IdPrograma
	 INNER JOIN SEG.Modulo M (NoLock) ON P.IdModulo = M.IdModulo			
WHERE R.Nombre = @nombreRol	
	  AND M.IdModulo = @IdModulo
GROUP BY P.IdPrograma
        ,P.IdModulo
        ,P.NombrePrograma
        ,P.CodigoPrograma
        ,P.Posicion
        ,P.Estado
        ,P.UsuarioCreacion
        ,P.FechaCreacion
        ,P.UsuarioModificacion
        ,P.FechaModificacion
        ,M.NombreModulo
        ,P.VisibleMenu
        ,P.EsReporte 
        ,P.IdReporte        
ORDER BY P.Posicion DESC	

END
