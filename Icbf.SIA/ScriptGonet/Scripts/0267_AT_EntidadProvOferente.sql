USE [SIA]
GO
/**
Desarrollador: Leticia Elizabeth Gonzalez
Fecha: 20/06/2014
Descripción: Se agrega el campo NumIntegrantes a la tabla EntidadProvOferente que indica el numero de integrantes
para el tipo de consorcio o union temporal
**/

IF NOT EXISTS(
SELECT TOP 1 *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE [TABLE_NAME] = 'EntidadProvOferente'
AND [COLUMN_NAME] = 'NumIntegrantes'
AND [TABLE_SCHEMA] = 'Proveedor')
BEGIN
 ALTER TABLE [Proveedor].EntidadProvOferente
 ADD NumIntegrantes INT NULL 
END