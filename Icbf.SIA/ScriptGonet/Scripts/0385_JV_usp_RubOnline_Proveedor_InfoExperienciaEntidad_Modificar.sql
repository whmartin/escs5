USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]    Script Date: 08/25/2014 17:38:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]    Script Date: 08/25/2014 17:38:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  03/10/2014 13:02:15 PM
-- Description:	Procedimiento almacenado que Modifica Experiencias
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
		@IdExpEntidad INT,	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	
		@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	
		@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(256),	@FechaInicio DATETIME,	@FechaFin DATETIME,	
		@NumeroContrato NVARCHAR(128),	@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(21,3),	@ExperienciaMeses NUMERIC(21,3),
		@EstadoDocumental INT,	@UnionTempConsorcio BIT,	@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	
		@JardinOPreJardin BIT, @UsuarioModifica NVARCHAR(250), @Finalizado BIT = NULL, @ContratoEjecucion BIT
AS
BEGIN
DECLARE @IdEstadoVal INT
SELECT @IdEstadoVal=IdEstadoValidacionDocumental FROM 
PROVEEDOR.EstadoValidacionDocumental
WHERE Descripcion='VALIDADO'
DECLARE @NumRev INT=0
DECLARE @IdEstadoAntesDeUpdate INT=0

SELECT @NumRev=NroRevision FROM PROVEEDOR.InfoExperienciaEntidad
WHERE IdExpEntidad=@IdExpEntidad

SELECT @IdEstadoAntesDeUpdate=EstadoDocumental FROM 
PROVEEDOR.InfoExperienciaEntidad
WHERE IdExpEntidad=@IdExpEntidad

-----------Se realiza el update de la experiencia-------------------

	UPDATE Proveedor.InfoExperienciaEntidad 
	SET IdEntidad = @IdEntidad, IdTipoSector = @IdTipoSector, IdTipoEstadoExp = @IdTipoEstadoExp, 
	IdTipoModalidadExp = @IdTipoModalidadExp, IdTipoModalidad = @IdTipoModalidad, 
	IdTipoPoblacionAtendida = @IdTipoPoblacionAtendida, IdTipoRangoExpAcum = @IdTipoRangoExpAcum, 
	IdTipoCodUNSPSC = @IdTipoCodUNSPSC, IdTipoEntidadContratante = @IdTipoEntidadContratante, 
	EntidadContratante = @EntidadContratante, FechaInicio = @FechaInicio, FechaFin = @FechaFin, 
	NumeroContrato = @NumeroContrato, ObjetoContrato = @ObjetoContrato, Vigente = @Vigente, 
	Cuantia = @Cuantia, ExperienciaMeses = @ExperienciaMeses,
	EstadoDocumental = @EstadoDocumental, UnionTempConsorcio = @UnionTempConsorcio, 
	PorcentParticipacion = @PorcentParticipacion, AtencionDeptos = @AtencionDeptos, 
	JardinOPreJardin = @JardinOPreJardin, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE(), 
	Finalizado = ISNULL(@Finalizado,Finalizado),
	ContratoEjecucion = @ContratoEjecucion
	WHERE IdExpEntidad = @IdExpEntidad
------------------------------------------------------------------------------

IF(@IdEstadoAntesDeUpdate=@IdEstadoVal)
BEGIN

	
	INSERT  INTO PROVEEDOR.ValidarInfoExperienciaEntidad
	(NroRevision, IdExpEntidad, ConfirmaYAprueba, Observaciones, FechaCrea, UsuarioCrea, FechaModifica, UsuarioModifica,CorreoEnviado)
	SELECT NroRevision, IdExpEntidad,NULL,'Actualiz� datos. Pendiente Validaci�n',GETDATE(), @UsuarioModifica, NULL,NULL,0 FROM PROVEEDOR.ValidarInfoExperienciaEntidad
	WHERE IdExpEntidad=@IdExpEntidad AND NroRevision=@NumRev
	
	UPDATE PROVEEDOR.InfoExperienciaEntidad
	SET NroRevision=NroRevision+1
	WHERE IdExpEntidad=@IdExpEntidad
END

END




GO


