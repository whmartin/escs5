USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]    Script Date: 08/12/2014 12:01:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]    Script Date: 08/12/2014 12:01:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]
		@IdTipoCiiu INT OUTPUT, 	@CodigoCiiu NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCiiu(CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoCiiu, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCiiu = SCOPE_IDENTITY() 		
END


GO


