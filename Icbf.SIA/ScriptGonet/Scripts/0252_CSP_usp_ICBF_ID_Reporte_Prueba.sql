USE [RubOnline]
GO


----- este es un registro de un reporte dinamico para efecto de pruebas

INSERT INTO [Global].[Reporte]
           ([NombreReporte]
           ,[Descripcion]
           ,[Servidor]
           ,[Carpeta]
           ,[NombreArchivo]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('Unidades de Servicio'
           ,'Unidades de servicio por departamento/municipio'
           ,'http://172.16.9.89/Reportserver_MSSQLSERVERR2'
           ,'CuentameSoporte/Generales'
           ,'ICBFRubUnidadesServicio'
           ,'Jose.Molina'
           ,2014/06/05
           ,Null
           ,Null)
GO
