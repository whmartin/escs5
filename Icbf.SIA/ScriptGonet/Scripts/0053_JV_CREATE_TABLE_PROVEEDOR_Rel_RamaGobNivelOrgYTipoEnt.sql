USE [SIA]
GO

/****** Object:  Table [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]    Script Date: 04/01/2014 13:24:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt](
	[IdRelacion] [int] IDENTITY(1,1) NOT NULL,
	[IdRamaEstructura] [int] NULL,
	[IdNivelGobierno] [int] NULL,
	[IdNivelOrganizacional] [int] NULL,
	[IdTipodeentidadPublica] [int] NULL,
	[Estado] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Rel_RamaGobNivelOrgYTipoEnt] PRIMARY KEY CLUSTERED 
(
	[IdRelacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]  WITH CHECK ADD  CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_NivelGobierno] FOREIGN KEY([IdNivelGobierno])
REFERENCES [PROVEEDOR].[NivelGobierno] ([IdNivelGobierno])
GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] CHECK CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_NivelGobierno]
GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]  WITH CHECK ADD  CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_NivelOrganizacional] FOREIGN KEY([IdNivelOrganizacional])
REFERENCES [PROVEEDOR].[NivelOrganizacional] ([IdNivelOrganizacional])
GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] CHECK CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_NivelOrganizacional]
GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]  WITH CHECK ADD  CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_RamaoEstructura] FOREIGN KEY([IdRamaEstructura])
REFERENCES [PROVEEDOR].[RamaoEstructura] ([IdRamaEstructura])
GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] CHECK CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_RamaoEstructura]
GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt]  WITH CHECK ADD  CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_TipodeentidadPublica] FOREIGN KEY([IdTipodeentidadPublica])
REFERENCES [PROVEEDOR].[TipodeentidadPublica] ([IdTipodeentidadPublica])
GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] CHECK CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_TipodeentidadPublica]
GO

ALTER TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] ADD  CONSTRAINT [DF_Rel_RamaGobNivelOrgYTipoEnt_Estado]  DEFAULT ((1)) FOR [Estado]
GO


