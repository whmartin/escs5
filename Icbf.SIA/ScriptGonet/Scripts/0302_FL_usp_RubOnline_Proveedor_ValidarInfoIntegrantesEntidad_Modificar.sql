USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Modificar]    Script Date: 30/07/2014 5:09:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Modificar]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Modificar]    Script Date: 30/07/2014 5:09:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Faiber Losada	
-- Create date:  2014/07/29 9:01:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoIntegrantesEntidad

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Modificar]
		@IdValidarInfoIntegrantesEntidad INT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(200),	@ConfirmaYAprueba BIT = NULL, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoIntegrantesEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdEntidad = @IdEntidad
		and NroRevision = @NroRevision			
		and IdValidarInfoIntegrantesEntidad = @IdValidarInfoIntegrantesEntidad
END






GO


