USE [SIA]
GO
/**
Creado Por: Juan Carlos Valverde S�mano
Fecha Creaci�n: 25-06-2014
Descripci�n: Agrega los documentos Obligatorios y Configuraci�n para Terceros y Proveedores de Tipo Consorcion o Uni�n Temporal.
***/
DECLARE @CodDocumento INT
DECLARE @varCodigoDoc VARCHAR(3)

IF NOT EXISTS ( SELECT IdTipoDocumento FROM PROVEEDOR.TipoDocumento WHERE
Descripcion='Documento de conformaci�n del Consorcio o Uni�n Temporal')
BEGIN
	SET @CodDocumento=(
	SELECT TOP(1)CodigoTipoDocumento FROM PROVEEDOR.TipoDocumento
	ORDER BY IdTipoDocumento DESC)
	SET @varCodigoDoc=(SELECT RIGHT('000' + Ltrim(Rtrim(@CodDocumento +1)),3))

		INSERT INTO PROVEEDOR.TipoDocumento(CodigoTipoDocumento,Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica)
		VALUES (@varCodigoDoc,'Documento de conformaci�n del Consorcio o Uni�n Temporal',1,'Administrador', GETDATE(), NULL,NULL)
END

IF NOT EXISTS ( SELECT IdTipoDocumento FROM PROVEEDOR.TipoDocumento WHERE
Descripcion='Poder que faculta al representante legal')
BEGIN
	SET @CodDocumento=(
	SELECT TOP(1)CodigoTipoDocumento FROM PROVEEDOR.TipoDocumento
	ORDER BY IdTipoDocumento DESC)
	SET @varCodigoDoc=(SELECT RIGHT('000' + Ltrim(Rtrim(@CodDocumento +1)),3))

		INSERT INTO PROVEEDOR.TipoDocumento(CodigoTipoDocumento,Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica)
		VALUES (@varCodigoDoc,'Poder que faculta al representante legal',1,'Administrador', GETDATE(), NULL,NULL)
END



DECLARE @IdPrograma INT =(
SELECT IdPrograma FROM SEG.Programa WHERE 
CodigoPrograma='PROVEEDOR/GestionProveedores')

DECLARE @IdDocumento INT


SET @IdDocumento=(SELECT IdTipoDocumento FROM PROVEEDOR.TipoDocumento 
WHERE Descripcion='Documento de conformaci�n del Consorcio o Uni�n Temporal')

IF NOT EXISTS(SELECT IdTipoDocumentoPrograma FROM PROVEEDOR.TipoDocumentoPrograma
WHERE IdTipoDocumento=@IdDocumento AND IdPrograma= @IdPrograma)
BEGIN
INSERT INTO PROVEEDOR.TipoDocumentoPrograma (IdTipoDocumento, IdPrograma,Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, MaxPermitidoKB, ExtensionesPermitidas, ObligRupNoRenovado, ObligRupRenovado, ObligPersonaJuridica, ObligPersonaNatural, ObligSectorPrivado, ObligSectorPublico, ObligEntNacional, ObligEntExtranjera, ObligConsorcio, ObligUnionTemporal)
VALUES(@IdDocumento, @IdPrograma, 1,'Administrador', GETDATE(), NULL,NULL,4096,'jpg,pdf',0,0,0,0,0,0,0,0,1,1)
END

SET @IdDocumento=(SELECT IdTipoDocumento FROM PROVEEDOR.TipoDocumento 
WHERE Descripcion='Poder que faculta al representante legal')

IF NOT EXISTS(SELECT IdTipoDocumentoPrograma FROM PROVEEDOR.TipoDocumentoPrograma
WHERE IdTipoDocumento=@IdDocumento AND IdPrograma= @IdPrograma)
BEGIN
INSERT INTO PROVEEDOR.TipoDocumentoPrograma (IdTipoDocumento, IdPrograma, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, MaxPermitidoKB, ExtensionesPermitidas, ObligRupNoRenovado, ObligRupRenovado, ObligPersonaJuridica, ObligPersonaNatural, ObligSectorPrivado, ObligSectorPublico, ObligEntNacional, ObligEntExtranjera, ObligConsorcio, ObligUnionTemporal)
VALUES(@IdDocumento, @IdPrograma, 1,'Administrador', GETDATE(), NULL,NULL,4096,'jpg,pdf',0,0,0,0,0,0,0,0,1,1)
END


SET @IdDocumento=(SELECT IdTipoDocumento FROM PROVEEDOR.TipoDocumento 
WHERE Descripcion='C�dula Representante Legal')


IF NOT EXISTS(SELECT IdTipoDocumentoPrograma FROM PROVEEDOR.TipoDocumentoPrograma
WHERE IdTipoDocumento=@IdDocumento AND IdPrograma= @IdPrograma)
BEGIN
INSERT INTO PROVEEDOR.TipoDocumentoPrograma (IdTipoDocumento, IdPrograma, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, MaxPermitidoKB, ExtensionesPermitidas, ObligRupNoRenovado, ObligRupRenovado, ObligPersonaJuridica, ObligPersonaNatural, ObligSectorPrivado, ObligSectorPublico, ObligEntNacional, ObligEntExtranjera, ObligConsorcio, ObligUnionTemporal)
VALUES(@IdDocumento, @IdPrograma, 1,'Administrador', GETDATE(), NULL,NULL,4096,'jpg,pdf',0,0,0,0,0,0,0,0,1,1)
END
ELSE 
BEGIN
UPDATE PROVEEDOR.TipoDocumentoPrograma
SET ObligConsorcio=1,
ObligUnionTemporal=1
WHERE IdTipoDocumento=@IdDocumento
AND IdPrograma=@IdPrograma
END


SET @IdPrograma =(
SELECT IdPrograma FROM SEG.Programa WHERE 
CodigoPrograma='PROVEEDOR/GestionTercero')

SET @IdDocumento=(SELECT IdTipoDocumento FROM PROVEEDOR.TipoDocumento 
WHERE Descripcion='Registro �nico Tributario - RUT')


UPDATE PROVEEDOR.TipoDocumentoPrograma
SET ObligConsorcio=1,
ObligUnionTemporal=1
WHERE IdTipoDocumento=@IdDocumento
AND IdPrograma=@IdPrograma
