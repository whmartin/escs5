USE [Oferentes]
GO

/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripcion: Inserción de registros para la tabla EstadoTercero en Oferentes usada en Proveedores
*/

IF((SELECT COUNT(*) FROM [Oferente].[EstadoTercero]) = 0)
BEGIN


SET IDENTITY_INSERT [Oferente].[EstadoTercero] ON 

INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'POR VALIDAR', 1, N'Administrador', CAST(0x0000A13900000000 AS DateTime), NULL, NULL)
INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (4, N'002', N'REGISTRADO', 1, N'Administrador', CAST(0x0000A13900000000 AS DateTime), NULL, NULL)
INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (5, N'003', N'POR AJUSTAR', 1, N'Administrador', CAST(0x0000A13900000000 AS DateTime), NULL, NULL)
INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (6, N'004', N'VALIDADO', 1, N'Administrador', CAST(0x0000A13900000000 AS DateTime), NULL, NULL)
INSERT [Oferente].[EstadoTercero] ([IdEstadoTercero], [CodigoEstadotercero], [DescripcionEstado], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (7, N'005', N'EN VALIDACIÓN', 1, N'Administrador', CAST(0x0000A20000DC4A15 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Oferente].[EstadoTercero] OFF

END

GO