USE [SIA]
GO

/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripcion: Creación de las tablas y constraint transversales utilizadas por la app SIA
*/

/****** Object:  Table [AUDITA].[LogSIA]    Script Date: 10/29/2013 20:04:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [AUDITA].[LogSIA](
	[idLog] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[usuario] [nvarchar](250) NOT NULL,
	[programa] [nvarchar](200) NOT NULL,
	[operacion] [nvarchar](20) NOT NULL,
	[parametrosOperacion] [text] NOT NULL,
	[tabla] [nvarchar](50) NOT NULL,
	[idRegistro] [numeric](18, 0) NOT NULL,
	[direccionIp] [nvarchar](20) NOT NULL,
	[navegador] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_LogSIA] PRIMARY KEY CLUSTERED 
(
	[idLog] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


/****** Object:  Table [DIV].[Barrio]    Script Date: 29/10/2013 18:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[Barrio](
	[IdBarrio] [int] IDENTITY(1,1) NOT NULL,
	[IdComuna] [int] NOT NULL,
	[CodigoBarrio] [nvarchar](10) NOT NULL,
	[NombreBarrio] [nvarchar](55) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdBarrio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoBarrio] UNIQUE NONCLUSTERED 
(
	[CodigoBarrio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreBarrio] UNIQUE NONCLUSTERED 
(
	[CodigoBarrio] ASC,
	[NombreBarrio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[CategoriaCentroPoblado]    Script Date: 29/10/2013 18:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[CategoriaCentroPoblado](
	[IdCategoriaCentroPoblado] [int] IDENTITY(1,1) NOT NULL,
	[CodigoCategoriaCentroPoblado] [nvarchar](5) NOT NULL,
	[NombreCategoriaCentroPoblado] [nvarchar](40) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCategoriaCentroPoblado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoCategoriaCentroPoblado] UNIQUE NONCLUSTERED 
(
	[CodigoCategoriaCentroPoblado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreCategoriaCentroPoblado] UNIQUE NONCLUSTERED 
(
	[NombreCategoriaCentroPoblado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[CentroPoblado]    Script Date: 29/10/2013 18:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[CentroPoblado](
	[IdCentroPoblado] [int] IDENTITY(1,1) NOT NULL,
	[IdDepartamento] [int] NOT NULL,
	[IdMunicipio] [int] NOT NULL,
	[CodigoCentroPoblado] [nvarchar](8) NOT NULL,
	[NombreCentroPoblado] [nvarchar](100) NOT NULL,
	[IdCategoriaCentroPoblado] [int] NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCentroPoblado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoCentroPoblado] UNIQUE NONCLUSTERED 
(
	[CodigoCentroPoblado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreCentroPoblado] UNIQUE NONCLUSTERED 
(
	[CodigoCentroPoblado] ASC,
	[NombreCentroPoblado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[CentroZonal]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[CentroZonal](
	[IdCentroZonal] [int] IDENTITY(1,1) NOT NULL,
	[IdMunicipio] [int] NOT NULL,
	[CodigoMunicipio] [nvarchar](5) NOT NULL,
	[CodigoCentroZonal] [nvarchar](4) NOT NULL,
	[NombreCentroZonal] [nvarchar](45) NOT NULL,
	[Direccion] [nvarchar](100) NOT NULL,
	[Telefonos] [nvarchar](100) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdRegional] [int] NULL,
 CONSTRAINT [PK__CentroZo__1C027691442B18F2] PRIMARY KEY CLUSTERED 
(
	[IdCentroZonal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoCentroZonal] UNIQUE NONCLUSTERED 
(
	[CodigoCentroZonal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreCentroZonal] UNIQUE NONCLUSTERED 
(
	[CodigoCentroZonal] ASC,
	[NombreCentroZonal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[Cobertura]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[Cobertura](
	[IdCobertura] [int] IDENTITY(1,1) NOT NULL,
	[IdCentroZonal] [int] NOT NULL,
	[IdMunicipio] [int] NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Cobertura] PRIMARY KEY CLUSTERED 
(
	[IdCobertura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[CoberturaCentroZonal]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[CoberturaCentroZonal](
	[IdCoberturaCentroZonal] [int] IDENTITY(1,1) NOT NULL,
	[IdMunicipio] [int] NOT NULL,
	[IdCentroZonal] [int] NOT NULL,
	[CodigoMunicipio] [nvarchar](5) NOT NULL,
	[CodigoCentroZonal] [nvarchar](4) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCoberturaCentroZonal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[Comuna]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[Comuna](
	[IdComuna] [int] IDENTITY(1,1) NOT NULL,
	[IdMunicipio] [int] NOT NULL,
	[CodigoComuna] [nvarchar](7) NOT NULL,
	[NombreComuna] [nvarchar](60) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdComuna] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoComuna] UNIQUE NONCLUSTERED 
(
	[CodigoComuna] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreComuna] UNIQUE NONCLUSTERED 
(
	[CodigoComuna] ASC,
	[NombreComuna] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[ComunidadIndigena]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[ComunidadIndigena](
	[IdComunidadIndigena] [int] IDENTITY(1,1) NOT NULL,
	[CodigoComunidadIndigena] [nvarchar](4) NOT NULL,
	[NombreComunidadIndigena] [nvarchar](100) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdComunidadIndigena] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoComunidadIndigena] UNIQUE NONCLUSTERED 
(
	[CodigoComunidadIndigena] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreComunidadIndigena] UNIQUE NONCLUSTERED 
(
	[NombreComunidadIndigena] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[Departamento]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[Departamento](
	[IdDepartamento] [int] IDENTITY(1,1) NOT NULL,
	[IdPais] [int] NOT NULL,
	[CodigoDepartamento] [nvarchar](2) NOT NULL,
	[NombreDepartamento] [nvarchar](25) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoDepartamento] UNIQUE NONCLUSTERED 
(
	[CodigoDepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreDepartamento] UNIQUE NONCLUSTERED 
(
	[NombreDepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[Municipio]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[Municipio](
	[IdMunicipio] [int] IDENTITY(1,1) NOT NULL,
	[IdDepartamento] [int] NOT NULL,
	[CodigoMunicipio] [nvarchar](5) NOT NULL,
	[NombreMunicipio] [nvarchar](50) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdMunicipio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoMunicipio] UNIQUE NONCLUSTERED 
(
	[CodigoMunicipio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreMunicipio] UNIQUE NONCLUSTERED 
(
	[CodigoMunicipio] ASC,
	[NombreMunicipio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[MunicipioResguardo]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[MunicipioResguardo](
	[IdMunicipioResguardo] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMunicipio] [nvarchar](5) NOT NULL,
	[CodigoResguardo] [nvarchar](3) NOT NULL,
	[IdMunicipio] [int] NOT NULL,
	[IdResguardo] [int] NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdMunicipioResguardo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[Pais]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[Pais](
	[IdPais] [int] IDENTITY(1,1) NOT NULL,
	[CodigoPais] [nvarchar](3) NOT NULL,
	[NombrePais] [nvarchar](50) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoPais] UNIQUE NONCLUSTERED 
(
	[CodigoPais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombrePais] UNIQUE NONCLUSTERED 
(
	[NombrePais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[PuebloIndigena]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[PuebloIndigena](
	[IdPuebloIndigena] [int] IDENTITY(1,1) NOT NULL,
	[CodigoPuebloIndigena] [nvarchar](4) NOT NULL,
	[NombrePuebloIndigena] [nvarchar](100) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPuebloIndigena] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoPuebloIndigena] UNIQUE NONCLUSTERED 
(
	[CodigoPuebloIndigena] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombrePuebloIndigena] UNIQUE NONCLUSTERED 
(
	[NombrePuebloIndigena] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[Regional]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[Regional](
	[IdRegional] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRegional] [nvarchar](2) NOT NULL,
	[NombreRegional] [nvarchar](256) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK__Regional__FC9034F833008CF0] PRIMARY KEY CLUSTERED 
(
	[IdRegional] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoRegional] UNIQUE NONCLUSTERED 
(
	[CodigoRegional] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreRegional] UNIQUE NONCLUSTERED 
(
	[NombreRegional] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[Resguardo]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[Resguardo](
	[IdResguardo] [int] IDENTITY(1,1) NOT NULL,
	[CodigoResguardo] [nvarchar](10) NOT NULL,
	[NombreResguardo] [nvarchar](100) NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdResguardo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__CodigoResguardo] UNIQUE NONCLUSTERED 
(
	[CodigoResguardo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [CU__NombreResguardo] UNIQUE NONCLUSTERED 
(
	[CodigoResguardo] ASC,
	[NombreResguardo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DIV].[ResguardoComunidadIndigena]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DIV].[ResguardoComunidadIndigena](
	[IdResguardoComunidadIndigena] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMunicipio] [nvarchar](5) NOT NULL,
	[CodigoResguardo] [nvarchar](3) NOT NULL,
	[CodigoComunidadIndigena] [nvarchar](3) NOT NULL,
	[IdMunicipio] [int] NOT NULL,
	[IdResguardo] [int] NOT NULL,
	[IdComunidadIndigena] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdResguardoComunidadIndigena] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Estructura].[Archivo]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Estructura].[Archivo](
	[IdArchivo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdFormatoArchivo] [int] NOT NULL,
	[FechaRegistro] [datetime] NOT NULL,
	[NombreArchivo] [nvarchar](128) NOT NULL,
	[Estado] [nvarchar](1) NOT NULL,
	[ResumenCarga] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[ServidorFTP] [nvarchar](256) NULL,
	[NombreTabla] [varchar](256) NULL,
	[IdTabla] [varchar](256) NULL,
	[NombreArchivoOri] [nvarchar](256) NULL,
 CONSTRAINT [PK_Archivo] PRIMARY KEY CLUSTERED 
(
	[IdArchivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Estructura].[Columna]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Estructura].[Columna](
	[IdColumna] [int] IDENTITY(1,1) NOT NULL,
	[IdFormatoArchivo] [int] NOT NULL,
	[CodigoColumna] [nvarchar](128) NULL,
	[NombreColumna] [nvarchar](128) NOT NULL,
	[TipoColumna] [nvarchar](128) NOT NULL,
	[Longitud] [int] NULL,
	[Obligatorio] [char](1) NOT NULL,
	[Posicion] [int] NOT NULL,
	[Estado] [char](1) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Columna] PRIMARY KEY CLUSTERED 
(
	[IdColumna] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Estructura].[ColumnaDepurador]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Estructura].[ColumnaDepurador](
	[IdColumnaDepurador] [int] IDENTITY(1,1) NOT NULL,
	[IdColumna] [int] NOT NULL,
	[IdDepurador] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](128) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_COLUMNADEPURADOR] PRIMARY KEY CLUSTERED 
(
	[IdColumnaDepurador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Estructura].[CorreoElectronico]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Estructura].[CorreoElectronico](
	[IdCorreoElectronico] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IdArchivo] [numeric](18, 0) NULL,
	[Destinatario] [nvarchar](256) NOT NULL,
	[Mensaje] [nvarchar](4000) NOT NULL,
	[Estado] [nvarchar](1) NOT NULL,
	[FechaIngreso] [datetime] NOT NULL,
	[FechaEnvio] [datetime] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[TipoCorreo] [char](1) NOT NULL,
 CONSTRAINT [PK_CorreoElectronico] PRIMARY KEY CLUSTERED 
(
	[IdCorreoElectronico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Estructura].[Depurador]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Estructura].[Depurador](
	[IdDepurador] [int] IDENTITY(1,1) NOT NULL,
	[CodigoDepurador] [nvarchar](256) NOT NULL,
	[NombreDepurador] [nvarchar](256) NOT NULL,
	[Descripcion] [nvarchar](512) NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaModifica] [datetime] NULL,
	[UsuarioModifica] [nvarchar](128) NULL,
 CONSTRAINT [PK_DEPURADOR] PRIMARY KEY CLUSTERED 
(
	[IdDepurador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Estructura].[FormatoArchivo]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Estructura].[FormatoArchivo](
	[IdFormatoArchivo] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoEstructura] [int] NOT NULL,
	[IdModalidad] [int] NOT NULL,
	[TablaTemporal] [nvarchar](128) NULL,
	[Separador] [nvarchar](8) NOT NULL,
	[Extension] [nvarchar](5) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[ValidarCampos] [bit] NOT NULL,
 CONSTRAINT [PK_FormatoArchivo] PRIMARY KEY CLUSTERED 
(
	[IdFormatoArchivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Estructura].[Modalidad]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Estructura].[Modalidad](
	[IdModalidad] [int] IDENTITY(1,1) NOT NULL,
	[NombreModalidad] [nvarchar](128) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[GrupoModalidad] [varchar](64) NULL,
 CONSTRAINT [PK_Modalidad] PRIMARY KEY CLUSTERED 
(
	[IdModalidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Estructura].[TipoEstructura]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Estructura].[TipoEstructura](
	[IdTipoEstructura] [int] IDENTITY(1,1) NOT NULL,
	[NombreEstructura] [nvarchar](256) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[GrupoEstructura] [varchar](64) NULL,
 CONSTRAINT [PK_TipoEstructura] PRIMARY KEY CLUSTERED 
(
	[IdTipoEstructura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Global].[ImagenesReportes]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Global].[ImagenesReportes](
	[IdImagen] [int] IDENTITY(1,1) NOT NULL,
	[CodigoImagen] [nvarchar](256) NOT NULL,
	[RutaImagen] [nvarchar](256) NOT NULL,
	[Descripcion] [nvarchar](256) NULL,
 CONSTRAINT [PK_ImagenesReportes] PRIMARY KEY CLUSTERED 
(
	[IdImagen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CodigoImagen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Global].[MotivoEstado]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Global].[MotivoEstado](
	[IdMotivoEstado] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMotivoEstado] [nvarchar](128) NOT NULL,
	[NombreMotivoEstado] [nvarchar](256) NOT NULL,
	[Estado] [nvarchar](1) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_MotivoEstado] PRIMARY KEY CLUSTERED 
(
	[IdMotivoEstado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Global].[ServicioVigencia]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Global].[ServicioVigencia](
	[IdServicioVigencia] [int] IDENTITY(1,1) NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[IdServicio] [int] NOT NULL,
	[IdRubro] [int] NOT NULL,
	[CodigoServicio] [nvarchar](128) NOT NULL,
	[NombreServicio] [nvarchar](256) NOT NULL,
	[CodigoRubro] [nvarchar](128) NOT NULL,
	[NombreRubro] [nvarchar](256) NOT NULL,
	[Estado] [nvarchar](1) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ServicioVigencia] PRIMARY KEY CLUSTERED 
(
	[IdServicioVigencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Global].[TiposDocumentos]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Global].[TiposDocumentos](
	[IdTipoDocumento] [int] IDENTITY(1,1) NOT NULL,
	[CodDocumento] [varchar](10) NOT NULL,
	[NomTipoDocumento] [varchar](50) NULL,
 CONSTRAINT [PK_TIPOS_DOCUMENTOS] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [Global].[Vigencia]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Global].[Vigencia](
	[IdVigencia] [int] IDENTITY(1,1) NOT NULL,
	[AcnoVigencia] [int] NOT NULL,
	[Activo] [nvarchar](1) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Vigencia] PRIMARY KEY CLUSTERED 
(
	[IdVigencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [SEG].[Modulo]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SEG].[Modulo](
	[IdModulo] [int] IDENTITY(1,1) NOT NULL,
	[NombreModulo] [nvarchar](250) NOT NULL,
	[Posicion] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCreacion] [nvarchar](250) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[UsuarioModificacion] [nvarchar](250) NULL,
	[FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK_Modulo] PRIMARY KEY CLUSTERED 
(
	[IdModulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [SEG].[Parametro]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SEG].[Parametro](
	[IdParametro] [int] IDENTITY(1,1) NOT NULL,
	[NombreParametro] [nvarchar](128) NOT NULL,
	[ValorParametro] [nvarchar](256) NOT NULL,
	[ImagenParametro] [nvarchar](256) NULL,
	[Estado] [bit] NOT NULL,
	[Funcionalidad] [nvarchar](128) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Parametro] PRIMARY KEY CLUSTERED 
(
	[IdParametro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [SEG].[Permiso]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SEG].[Permiso](
	[IdPermiso] [int] IDENTITY(1,1) NOT NULL,
	[IdPrograma] [int] NOT NULL,
	[IdRol] [int] NOT NULL,
	[Insertar] [bit] NOT NULL,
	[Modificar] [bit] NOT NULL,
	[Eliminar] [bit] NOT NULL,
	[Consultar] [bit] NOT NULL,
	[UsuarioCreacion] [nvarchar](250) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[UsuarioModificacion] [nvarchar](250) NULL,
	[FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK_Permiso] PRIMARY KEY CLUSTERED 
(
	[IdPermiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [SEG].[Programa]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SEG].[Programa](
	[IdPrograma] [int] IDENTITY(1,1) NOT NULL,
	[IdModulo] [int] NOT NULL,
	[NombrePrograma] [nvarchar](250) NOT NULL,
	[CodigoPrograma] [nvarchar](250) NOT NULL,
	[Posicion] [int] NOT NULL,
	[Estado] [int] NOT NULL,
	[UsuarioCreacion] [nvarchar](250) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[UsuarioModificacion] [nvarchar](250) NULL,
	[FechaModificacion] [datetime] NULL,
	[VisibleMenu] [bit] NOT NULL,
	[generaLog] [bit] NOT NULL,
	[Padre] [int] NULL,
 CONSTRAINT [PK_Programa] PRIMARY KEY CLUSTERED 
(
	[IdPrograma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [SEG].[ProgramaRol]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SEG].[ProgramaRol](
	[IdPrograma] [int] NOT NULL,
	[IdRol] [int] NOT NULL,
	[UsuarioCreacion] [nvarchar](250) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_ProgramaRol] PRIMARY KEY CLUSTERED 
(
	[IdPrograma] ASC,
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [SEG].[Rol]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SEG].[Rol](
	[IdRol] [int] IDENTITY(1,1) NOT NULL,
	[providerKey] [varchar](125) NOT NULL,
	[Nombre] [nvarchar](20) NOT NULL,
	[Descripcion] [nvarchar](200) NOT NULL,
	[Estado] [bit] NULL,
	[UsuarioCreacion] [nvarchar](250) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[UsuarioModificacion] [nvarchar](250) NULL,
	[FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SEG].[TipoUsuario]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SEG].[TipoUsuario](
	[IdTipoUsuario] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoUsuario] [nvarchar](128) NOT NULL,
	[NombreTipoUsuario] [nvarchar](256) NOT NULL,
	[Estado] [nvarchar](1) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoUsuario] PRIMARY KEY CLUSTERED 
(
	[IdTipoUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [SEG].[Usuario]    Script Date: 29/10/2013 18:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SEG].[Usuario](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[providerKey] [varchar](125) NOT NULL,
	[IdTipoDocumento] [int] NOT NULL,
	[NumeroDocumento] [nvarchar](17) NOT NULL,
	[PrimerNombre] [nvarchar](150) NOT NULL,
	[SegundoNombre] [nvarchar](150) NULL,
	[PrimerApellido] [nvarchar](150) NOT NULL,
	[SegundoApellido] [nvarchar](150) NULL,
	[TelefonoContacto] [nvarchar](10) NOT NULL,
	[CorreoElectronico] [nvarchar](100) NULL,
	[Estado] [bit] NULL,
	[UsuarioCreacion] [nvarchar](250) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[UsuarioModificacion] [nvarchar](250) NULL,
	[FechaModificacion] [datetime] NULL,
	[IdTipoUsuario] [int] NOT NULL,
	[IdRegional] [int] NULL,
	[IdEntidadContratista] [int] NULL,
	[IdTipoPersona] [int] NULL,
	[RazonSocial] [nvarchar](150) NULL,
	[DV] [nvarchar](1) NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [Estructura].[CorreoElectronico] ADD  DEFAULT ('F') FOR [TipoCorreo]
GO
ALTER TABLE [Estructura].[FormatoArchivo] ADD  CONSTRAINT [DF_FormatoArchivo_ValidarCampos]  DEFAULT ((0)) FOR [ValidarCampos]
GO
ALTER TABLE [SEG].[Programa] ADD  DEFAULT ((1)) FOR [VisibleMenu]
GO
ALTER TABLE [SEG].[Programa] ADD  DEFAULT ((1)) FOR [generaLog]
GO
ALTER TABLE [SEG].[Usuario] ADD  DEFAULT ((1)) FOR [IdTipoUsuario]
GO
ALTER TABLE [DIV].[Barrio]  WITH CHECK ADD  CONSTRAINT [FK_Barrio_Comuna] FOREIGN KEY([IdComuna])
REFERENCES [DIV].[Comuna] ([IdComuna])
GO
ALTER TABLE [DIV].[Barrio] CHECK CONSTRAINT [FK_Barrio_Comuna]
GO
ALTER TABLE [DIV].[CentroPoblado]  WITH CHECK ADD  CONSTRAINT [FK_CentroPoblado_CategoriaCentroPoblado] FOREIGN KEY([IdCategoriaCentroPoblado])
REFERENCES [DIV].[CategoriaCentroPoblado] ([IdCategoriaCentroPoblado])
GO
ALTER TABLE [DIV].[CentroPoblado] CHECK CONSTRAINT [FK_CentroPoblado_CategoriaCentroPoblado]
GO
ALTER TABLE [DIV].[CentroPoblado]  WITH CHECK ADD  CONSTRAINT [FK_CentroPoblado_Departamento] FOREIGN KEY([IdDepartamento])
REFERENCES [DIV].[Departamento] ([IdDepartamento])
GO
ALTER TABLE [DIV].[CentroPoblado] CHECK CONSTRAINT [FK_CentroPoblado_Departamento]
GO
ALTER TABLE [DIV].[CentroPoblado]  WITH CHECK ADD  CONSTRAINT [FK_CentroPoblado_Municipio] FOREIGN KEY([IdMunicipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
ALTER TABLE [DIV].[CentroPoblado] CHECK CONSTRAINT [FK_CentroPoblado_Municipio]
GO
ALTER TABLE [DIV].[CentroZonal]  WITH CHECK ADD  CONSTRAINT [FK_CentroZonal_Municipio] FOREIGN KEY([IdMunicipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
ALTER TABLE [DIV].[CentroZonal] CHECK CONSTRAINT [FK_CentroZonal_Municipio]
GO
ALTER TABLE [DIV].[CentroZonal]  WITH CHECK ADD  CONSTRAINT [FK_CentroZonal_Regional] FOREIGN KEY([IdRegional])
REFERENCES [DIV].[Regional] ([IdRegional])
GO
ALTER TABLE [DIV].[CentroZonal] CHECK CONSTRAINT [FK_CentroZonal_Regional]
GO
ALTER TABLE [DIV].[Cobertura]  WITH CHECK ADD  CONSTRAINT [FK_Cobertura_CentroZonal] FOREIGN KEY([IdCentroZonal])
REFERENCES [DIV].[CentroZonal] ([IdCentroZonal])
GO
ALTER TABLE [DIV].[Cobertura] CHECK CONSTRAINT [FK_Cobertura_CentroZonal]
GO
ALTER TABLE [DIV].[Cobertura]  WITH CHECK ADD  CONSTRAINT [FK_Cobertura_Municipio] FOREIGN KEY([IdMunicipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
ALTER TABLE [DIV].[Cobertura] CHECK CONSTRAINT [FK_Cobertura_Municipio]
GO
ALTER TABLE [DIV].[Cobertura]  WITH CHECK ADD  CONSTRAINT [FK_Cobertura_Municipio1] FOREIGN KEY([IdMunicipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
ALTER TABLE [DIV].[Cobertura] CHECK CONSTRAINT [FK_Cobertura_Municipio1]
GO
ALTER TABLE [DIV].[Cobertura]  WITH CHECK ADD  CONSTRAINT [FK_Cobertura_Vigencia] FOREIGN KEY([IdVigencia])
REFERENCES [Global].[Vigencia] ([IdVigencia])
GO
ALTER TABLE [DIV].[Cobertura] CHECK CONSTRAINT [FK_Cobertura_Vigencia]
GO
ALTER TABLE [DIV].[CoberturaCentroZonal]  WITH NOCHECK ADD  CONSTRAINT [FK_CoberturaCentroZonal_CentroZonal_IdCentroZonal] FOREIGN KEY([IdCentroZonal])
REFERENCES [DIV].[CentroZonal] ([IdCentroZonal])
GO
ALTER TABLE [DIV].[CoberturaCentroZonal] CHECK CONSTRAINT [FK_CoberturaCentroZonal_CentroZonal_IdCentroZonal]
GO
ALTER TABLE [DIV].[CoberturaCentroZonal]  WITH NOCHECK ADD  CONSTRAINT [FK_CoberturaCentroZonal_Municipio_IdMunicipio] FOREIGN KEY([IdMunicipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
ALTER TABLE [DIV].[CoberturaCentroZonal] CHECK CONSTRAINT [FK_CoberturaCentroZonal_Municipio_IdMunicipio]
GO
ALTER TABLE [DIV].[Comuna]  WITH CHECK ADD  CONSTRAINT [FK_Comuna_Municipio] FOREIGN KEY([IdMunicipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
ALTER TABLE [DIV].[Comuna] CHECK CONSTRAINT [FK_Comuna_Municipio]
GO
ALTER TABLE [DIV].[Departamento]  WITH CHECK ADD  CONSTRAINT [FK_Departamento_Pais] FOREIGN KEY([IdPais])
REFERENCES [DIV].[Pais] ([IdPais])
GO
ALTER TABLE [DIV].[Departamento] CHECK CONSTRAINT [FK_Departamento_Pais]
GO
ALTER TABLE [DIV].[Municipio]  WITH CHECK ADD  CONSTRAINT [FK_Municipio_Departamento] FOREIGN KEY([IdDepartamento])
REFERENCES [DIV].[Departamento] ([IdDepartamento])
GO
ALTER TABLE [DIV].[Municipio] CHECK CONSTRAINT [FK_Municipio_Departamento]
GO
ALTER TABLE [DIV].[MunicipioResguardo]  WITH CHECK ADD  CONSTRAINT [FK_MunicipioResguardo_Municipio] FOREIGN KEY([IdMunicipio])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO
ALTER TABLE [DIV].[MunicipioResguardo] CHECK CONSTRAINT [FK_MunicipioResguardo_Municipio]
GO
ALTER TABLE [DIV].[MunicipioResguardo]  WITH CHECK ADD  CONSTRAINT [FK_MunicipioResguardo_Resguardo] FOREIGN KEY([IdResguardo])
REFERENCES [DIV].[Resguardo] ([IdResguardo])
GO
ALTER TABLE [DIV].[MunicipioResguardo] CHECK CONSTRAINT [FK_MunicipioResguardo_Resguardo]
GO
ALTER TABLE [Estructura].[Archivo]  WITH CHECK ADD  CONSTRAINT [FK_Archivo_FormatoArchivo] FOREIGN KEY([IdFormatoArchivo])
REFERENCES [Estructura].[FormatoArchivo] ([IdFormatoArchivo])
GO
ALTER TABLE [Estructura].[Archivo] CHECK CONSTRAINT [FK_Archivo_FormatoArchivo]
GO
ALTER TABLE [Estructura].[Archivo]  WITH CHECK ADD  CONSTRAINT [FK_Archivo_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [SEG].[Usuario] ([IdUsuario])
GO
ALTER TABLE [Estructura].[Archivo] CHECK CONSTRAINT [FK_Archivo_Usuario]
GO
ALTER TABLE [Estructura].[Columna]  WITH CHECK ADD  CONSTRAINT [FK_Columna_FormatoArchivo] FOREIGN KEY([IdFormatoArchivo])
REFERENCES [Estructura].[FormatoArchivo] ([IdFormatoArchivo])
GO
ALTER TABLE [Estructura].[Columna] CHECK CONSTRAINT [FK_Columna_FormatoArchivo]
GO
ALTER TABLE [Estructura].[ColumnaDepurador]  WITH CHECK ADD  CONSTRAINT [FK_ColumnaDepurador_Columna] FOREIGN KEY([IdColumna])
REFERENCES [Estructura].[Columna] ([IdColumna])
GO
ALTER TABLE [Estructura].[ColumnaDepurador] CHECK CONSTRAINT [FK_ColumnaDepurador_Columna]
GO
ALTER TABLE [Estructura].[ColumnaDepurador]  WITH CHECK ADD  CONSTRAINT [FK_ColumnaDepurador_Depurador] FOREIGN KEY([IdDepurador])
REFERENCES [Estructura].[Depurador] ([IdDepurador])
GO
ALTER TABLE [Estructura].[ColumnaDepurador] CHECK CONSTRAINT [FK_ColumnaDepurador_Depurador]
GO
ALTER TABLE [Estructura].[CorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_CorreoElectronico_Archivo] FOREIGN KEY([IdArchivo])
REFERENCES [Estructura].[Archivo] ([IdArchivo])
GO
ALTER TABLE [Estructura].[CorreoElectronico] CHECK CONSTRAINT [FK_CorreoElectronico_Archivo]
GO
ALTER TABLE [Estructura].[FormatoArchivo]  WITH CHECK ADD  CONSTRAINT [FK_FormatoArchivo_Modalidad] FOREIGN KEY([IdModalidad])
REFERENCES [Estructura].[Modalidad] ([IdModalidad])
GO
ALTER TABLE [Estructura].[FormatoArchivo] CHECK CONSTRAINT [FK_FormatoArchivo_Modalidad]
GO
ALTER TABLE [Estructura].[FormatoArchivo]  WITH CHECK ADD  CONSTRAINT [FK_FormatoArchivo_TipoEstructura] FOREIGN KEY([IdTipoEstructura])
REFERENCES [Estructura].[TipoEstructura] ([IdTipoEstructura])
GO
ALTER TABLE [Estructura].[FormatoArchivo] CHECK CONSTRAINT [FK_FormatoArchivo_TipoEstructura]
GO

ALTER TABLE [Global].[ServicioVigencia]  WITH CHECK ADD  CONSTRAINT [FK_ServicioVigencia_Vigencia] FOREIGN KEY([IdVigencia])
REFERENCES [Global].[Vigencia] ([IdVigencia])
GO
ALTER TABLE [Global].[ServicioVigencia] CHECK CONSTRAINT [FK_ServicioVigencia_Vigencia]
GO
ALTER TABLE [SEG].[Permiso]  WITH CHECK ADD  CONSTRAINT [FK_Permiso_Programa] FOREIGN KEY([IdPrograma])
REFERENCES [SEG].[Programa] ([IdPrograma])
GO
ALTER TABLE [SEG].[Permiso] CHECK CONSTRAINT [FK_Permiso_Programa]
GO
ALTER TABLE [SEG].[Permiso]  WITH CHECK ADD  CONSTRAINT [FK_Permiso_Rol] FOREIGN KEY([IdRol])
REFERENCES [SEG].[Rol] ([IdRol])
GO
ALTER TABLE [SEG].[Permiso] CHECK CONSTRAINT [FK_Permiso_Rol]
GO
ALTER TABLE [SEG].[Programa]  WITH CHECK ADD  CONSTRAINT [FK_Programa_Modulo] FOREIGN KEY([IdModulo])
REFERENCES [SEG].[Modulo] ([IdModulo])
GO
ALTER TABLE [SEG].[Programa] CHECK CONSTRAINT [FK_Programa_Modulo]
GO
ALTER TABLE [SEG].[Usuario]  WITH NOCHECK ADD  CONSTRAINT [FK_Usuario_Regional_IdRegional] FOREIGN KEY([IdRegional])
REFERENCES [DIV].[Regional] ([IdRegional])
GO
ALTER TABLE [SEG].[Usuario] NOCHECK CONSTRAINT [FK_Usuario_Regional_IdRegional]
GO
ALTER TABLE [SEG].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_TiposDocumentos] FOREIGN KEY([IdTipoDocumento])
REFERENCES [Global].[TiposDocumentos] ([IdTipoDocumento])
GO
ALTER TABLE [SEG].[Usuario] CHECK CONSTRAINT [FK_Usuario_TiposDocumentos]
GO
ALTER TABLE [SEG].[Usuario]  WITH NOCHECK ADD  CONSTRAINT [FK_Usuario_TipoUsuario_IdTipoUsuario] FOREIGN KEY([IdTipoUsuario])
REFERENCES [SEG].[TipoUsuario] ([IdTipoUsuario])
GO
ALTER TABLE [SEG].[Usuario] NOCHECK CONSTRAINT [FK_Usuario_TipoUsuario_IdTipoUsuario]
GO
