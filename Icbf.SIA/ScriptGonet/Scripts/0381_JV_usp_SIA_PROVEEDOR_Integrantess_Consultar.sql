USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]    Script Date: 08/21/2014 16:28:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]    Script Date: 08/21/2014 16:28:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que consulta un(a) Integrantes
-- Modificado por: Juan Carlos Valverde S�mano
-- Fecha: 10/AGO/2014
-- Descripci�n: Se concaten� Tipo Documento y Numero de Identificaci�n al Nombre o Raz�n Social seg�n el Tipo de Persona.
-- Modificado Por: Juan Carlos Valverde S�mano
-- Fecha: 19/AGO/2014
-- Descripci�n: Se agrega al resultado de la Columna los Datos de Representante legal cuando
-- se trata de una persona Jur�dica, y se agrega tambi�n el Link Documento para personas Juridicas
-- para el Documento: Certificado de existencia y representaci�n legal o de domicilio para entidades extranjeras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]
	@IdEntidad INT = NULL
AS
BEGIN

DECLARE @IdTipoDocumento INT=
(SELECT IdTipoDocumento FROM PROVEEDOR.TipoDocumento
WHERE Descripcion='Certificado de existencia y representaci�n legal o de domicilio para entidades extranjeras')

		SELECT	IdIntegrante,
				CASE Integrantes.IdTipoPersona 
				WHEN 1 THEN (TD.CodDocumento+' '+ T.NUMEROIDENTIFICACION+' '+ T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') )
				ELSE TD.CodDocumento+' '+ T.NUMEROIDENTIFICACION+' '+T.RAZONSOCIAL END as Integrante, 
				TP.IdTipoPersona,
				TP.NombreTipoPersona, 
				PorcentajeParticipacion,
				ISNULL(CASE Integrantes.IdTipoPersona 
				WHEN 2 THEN (
				ISNULL((SELECT CodDocumento FROM GLOBAL.TiposDocumentos WHERE IdTipoDocumento=(SELECT IDTIPODOCIDENTIFICA FROM Oferente.TERCERO WHERE IDTERCERO=INFO.IdRepLegal)),'') +' '+ 
				(SELECT ISNULL(NUMEROIDENTIFICACION,'')+' '+ISNULL(PRIMERNOMBRE,'')+' '+
				ISNULL(SEGUNDONOMBRE,'')+' '+ISNULL(PRIMERAPELLIDO,'')+' '+ISNULL(SEGUNDOAPELLIDO,'') FROM Oferente.TERCERO WHERE IDTERCERO=INFO.IdRepLegal))
				ELSE ' ' END,'') AS 'Representante',
				CASE Integrantes.IdTipoPersona
				WHEN 2 THEN (SELECT LinkDocumento FROM PROVEEDOR.DocDatosBasicoProv
				WHERE IdTipoDocumento=@IdTipoDocumento AND IdEntidad=
				(SELECT IdEntidad FROM PROVEEDOR.EntidadProvOferente WHERE IdTercero=
				(SELECT IdTercero FROM Oferente.TERCERO WHERE NUMEROIDENTIFICACION=Integrantes.NumeroIdentificacion)) AND Activo=1)
				ELSE ' ' END AS 'LinkDoc'
		 FROM [PROVEEDOR].[Integrantes] Integrantes
		 INNER JOIN oferente.TERCERO T ON Integrantes.NumeroIdentificacion=T.NUMEROIDENTIFICACION
		 INNER JOIN Oferente.TipoPersona TP ON Integrantes.IdTipoPersona=TP.IdTipoPersona  
		 INNER JOIN Global.TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		 LEFT JOIN PROVEEDOR.EntidadProvOferente PROV ON T.IdTercero=PROV.IdTercero
		 LEFT JOIN PROVEEDOR.InfoAdminEntidad INFO ON PROV.IdEntidad=INFO.IdEntidad
		 WHERE Integrantes.IdEntidad = CASE WHEN @IdEntidad IS NULL 
			THEN Integrantes.IdEntidad ELSE @IdEntidad END
			AND T.IDESTADOTERCERO IS NOT NULL
		 
END


GO


