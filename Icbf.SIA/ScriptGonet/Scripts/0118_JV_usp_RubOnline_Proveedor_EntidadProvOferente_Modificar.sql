USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 04/06/2014 12:54:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 04/06/2014 12:54:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que actualiza un(a) EntidadProvOferente
-- Desarrollador Modificaci�n: Juan Carlos Valverde S�mano
-- Fecha modificaci�n: 26-03-2014
-- Descripci�n: en base a un requerimiento del control de cambios 016, aqu� se agregan las lineas para colocar en
-- NULL la columna ConfirmaYAprueba de la tabla PROVEEDOR.ValidarInfoDatosBasicosEntidad. ya que cuando se realiza una 
-- Modificaci�n el estado del proveedor cambia, pero tambi�n debe cambiar el estado de el m�dulo de informaci�n b�sica.
--Modificado por: Juan Carlos Valverde S�mano
--Fecha Modificaci�n: 20/03/2014
--Descripci�n: En base a un requerimiento de Mejors del CO016 se agrega lo siguiente:
--el procedimiento recibe los parametros primer nombre, segundo nombre, primer apeliido, segundo apellido y 
-- correo electr�nico para actualizarlos en TERCEROS.(Por si caso hubo alguna modificaci�n al momento de editar
-- los datos basicos del proveedor.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
		@IdEntidad INT,	
		@TipoEntOfProv BIT= NULL,	
		@IdTercero INT= NULL,	
		@IdTipoCiiuPrincipal INT = NULL,	
		@IdTipoCiiuSecundario INT= NULL,	
		@IdTipoSector INT = NULL,	
		@IdTipoClaseEntidad INT = NULL,	
		@IdTipoRamaPublica INT = NULL,	
		@IdTipoNivelGob INT = NULL,	
		@IdTipoNivelOrganizacional INT = NULL,	
		@IdTipoCertificadorCalidad INT = NULL,	
		@FechaCiiuPrincipal DATETIME = NULL,	
		@FechaCiiuSecundario DATETIME = NULL,	
		@FechaConstitucion DATETIME = NULL,	
		@FechaVigencia DATETIME = NULL,	
		@FechaMatriculaMerc DATETIME = NULL,	
		@FechaExpiracion DATETIME = NULL,	
		@TipoVigencia BIT = NULL,	
		@ExenMatriculaMer BIT = NULL,	
		@MatriculaMercantil NVARCHAR(20) = NULL,	
		@ObserValidador NVARCHAR(256) = NULL,	
		@AnoRegistro INT = NULL,	
		@IdEstado INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL,
		@Finalizado bit = NULL,
		@PrimerNombre NVARCHAR(256) ='',
		@SegundoNombre NVARCHAR(256) ='',
		@PrimerApellido NVARCHAR(256)='',
		@SegundoApellido NVARCHAR(256)='',
		@CorreoElectronico NVARCHAR(256) = '',
		@IdTemporal VARCHAR(20) = NULL
		
		
AS
BEGIN
DECLARE @finalizado_UPDATE bit 
IF(@Finalizado is NULL)
BEGIN
SET @finalizado_UPDATE=(SELECT Finalizado FROM Proveedor.EntidadProvOferente WHERE IdEntidad = @IdEntidad);
END
ELSE
BEGIN
SET @finalizado_UPDATE=@Finalizado
END
	UPDATE Proveedor.EntidadProvOferente 
			SET TipoEntOfProv = ISNULL(@TipoEntOfProv,TipoEntOfProv) ,
			    IdTercero = ISNULL(@IdTercero,IdTercero), 
			    IdTipoCiiuPrincipal =ISNULL( @IdTipoCiiuPrincipal, IdTipoCiiuPrincipal ),
				IdTipoCiiuSecundario = @IdTipoCiiuSecundario,
				IdTipoSector =ISNULL( @IdTipoSector, IdTipoSector ),
				IdTipoClaseEntidad =ISNULL( @IdTipoClaseEntidad, IdTipoClaseEntidad ),
				IdTipoRamaPublica =ISNULL( @IdTipoRamaPublica, IdTipoRamaPublica ),
				IdTipoNivelGob =ISNULL( @IdTipoNivelGob, IdTipoNivelGob ),
				IdTipoNivelOrganizacional =ISNULL( @IdTipoNivelOrganizacional, IdTipoNivelOrganizacional ),
				IdTipoCertificadorCalidad =ISNULL( @IdTipoCertificadorCalidad, IdTipoCertificadorCalidad ),
				FechaCiiuPrincipal =ISNULL( @FechaCiiuPrincipal, FechaCiiuPrincipal ),
				FechaCiiuSecundario = @FechaCiiuSecundario,
				FechaConstitucion =ISNULL( @FechaConstitucion, FechaConstitucion ),
				FechaVigencia =ISNULL( @FechaVigencia, FechaVigencia ),
				FechaMatriculaMerc =ISNULL( @FechaMatriculaMerc, FechaMatriculaMerc ),
				FechaExpiracion =ISNULL( @FechaExpiracion, FechaExpiracion ),
				TipoVigencia =ISNULL( @TipoVigencia, TipoVigencia ),
				ExenMatriculaMer =ISNULL( @ExenMatriculaMer, ExenMatriculaMer ),
				MatriculaMercantil =ISNULL( @MatriculaMercantil, MatriculaMercantil ),
				ObserValidador =ISNULL( @ObserValidador, ObserValidador ),
				AnoRegistro =ISNULL( @AnoRegistro, AnoRegistro ),
				IdEstado =ISNULL( @IdEstado, IdEstado ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE(),
			    Finalizado=@finalizado_UPDATE
			   WHERE IdEntidad = @IdEntidad
			   
			   DECLARE @IdValidarInfoDatosBasicosEntidad INT
			   
			   
			   SET @IdValidarInfoDatosBasicosEntidad = (
			   SELECT IdValidarInfoDatosBasicosEntidad FROM 
			   PROVEEDOR.ValidarInfoDatosBasicosEntidad
			   WHERE IdEntidad=@IdEntidad AND NroRevision =(SELECT  MAX(NroRevision) FROM 
			   PROVEEDOR.ValidarInfoDatosBasicosEntidad WHERE IdEntidad=@IdEntidad))
			   
			   UPDATE PROVEEDOR.ValidarInfoDatosBasicosEntidad
			   SET ConfirmaYAprueba=NULL 
			   WHERE IdValidarInfoDatosBasicosEntidad=@IdValidarInfoDatosBasicosEntidad
			   
			   
			   /*ACTUALIZA DATOS DE TERCERO*/
			   
			   	UPDATE Oferentes.Oferente.TERCERO
   SET
   PRIMERNOMBRE=@PrimerNombre,
   SEGUNDONOMBRE=@SegundoNombre,
   PRIMERAPELLIDO=@PrimerApellido,
   SEGUNDOAPELLIDO=@SegundoApellido,
   CORREOELECTRONICO=@CorreoElectronico,
   USUARIOMODIFICA=@UsuarioModifica,
   FECHAMODIFICA=GETDATE()
   WHERE IDTERCERO=@IdTercero
			
   
-------Actualizar el Documento que viene con IDTemporal, colocarle el ID del tercero-------
-----Y actualizar la columna activo para el o los documentos anteriores--------------------
	UPDATE [SIA].[Proveedor].[DocAdjuntoTercero]  
	SET Activo=0
	WHERE IDTERCERO=@IdTercero AND
	IDDOCUMENTO IN (SELECT IDDOCUMENTO FROM [SIA].[Proveedor].[DocAdjuntoTercero] WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal
	
	UPDATE [SIA].[Proveedor].[DocAdjuntoTercero]  
	set IdTercero = @IdTercero
	where IdTemporal = @IdTemporal	
	AND Activo=1	

------------------------------------------------------------------------------------------


-----Actualizar el Documento que viene con IDTemporal, colocarle el ID del tercero-------
-----Y actualizar la columna activo para el o los documentos anteriores--------------------
	UPDATE [SIA].[Proveedor].[DocDatosBasicoProv]  
	SET Activo=0
	WHERE IdEntidad=@IdEntidad AND
	IdTipoDocumento IN (SELECT IdTipoDocumento FROM [SIA].[Proveedor].[DocDatosBasicoProv]  WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal
	
	UPDATE [SIA].[Proveedor].[DocDatosBasicoProv]
	set IdEntidad = @IdEntidad
	where IdTemporal = @IdTemporal	
	AND Activo=1	

------------------------------------------------------------------------------------------

END




GO


