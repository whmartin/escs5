USE [SIA]
GO

/****** Object:  Index [UNIQUE_PROVEEDOR_ReglasEstadoProveedor]    Script Date: 07/30/2014 13:39:45 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[ReglasEstadoProveedor]') AND name = N'UNIQUE_PROVEEDOR_ReglasEstadoProveedor')
DROP INDEX [UNIQUE_PROVEEDOR_ReglasEstadoProveedor] ON [PROVEEDOR].[ReglasEstadoProveedor] WITH ( ONLINE = OFF )
GO

/**
Autor: Juan Carlos Valverde S�mano
Fecha: 30-Jul-2014
Descripci�n: El INDEX ya existia, pero ahora se le agrega la columna
IdEstadoIntegrantes, ya que se agregaran las reglas de validaci�n para
los proveedores Tipo Consorcio o Uni�n Temporal.
**/
/****** Object:  Index [UNIQUE_PROVEEDOR_ReglasEstadoProveedor]    Script Date: 07/30/2014 13:40:02 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UNIQUE_PROVEEDOR_ReglasEstadoProveedor] ON [PROVEEDOR].[ReglasEstadoProveedor] 
(
	[IdEstadoDatosBasicos] ASC,
	[IdEstadoDatosFinancieros] ASC,
	[IdEstadoDatosExperiencia] ASC,
	[IdEstadoIntegrantes] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


