/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha creaci�n: 18-Marzo-2014
Descripci�n Inserta el Nuevo documento solicitado en el control de cambios CO_016,
Para el registro de personas Jur�dicas; E inserta la configuraci�n para este nuevo documento.
**/
USE [SIA]
GO
DECLARE @idTipoDoc INT
DECLARE @idPrograma INT
SET @idPrograma=(select  IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionTercero');
INSERT INTO Proveedor.TipoDocumento
(CodigoTipoDocumento,
Descripcion,
Estado,
UsuarioCrea,
FechaCrea,
UsuarioModifica,
FechaModifica)
VALUES(
(SELECT TOP(1) RIGHT ('000' +CAST((CONVERT(INT,CodigoTipoDocumento)+1) as varchar),3)
FROM Proveedor.TipoDocumento ORDER BY IdTipoDocumento DESC),
'C�dula Representante Legal',
1,
'administrador',
GETDATE(),
NULL,
NULL
)

SET @idTipoDoc=(SELECT @@IDENTITY);

INSERT INTO [PROVEEDOR].[TipoDocumentoPrograma]
           ([IdTipoDocumento]
           ,[IdPrograma]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica]
           ,[MaxPermitidoKB]
           ,[ExtensionesPermitidas]
           ,[ObligRupNoRenovado]
           ,[ObligRupRenovado]
           ,[ObligPersonaJuridica]
           ,[ObligPersonaNatural]
           ,[ObligSectorPrivado]
           ,[ObligSectorPublico])
     VALUES
           (@idTipoDoc
           ,@idPrograma
           ,1
           ,'Administrador'
           ,GETDATE()
           ,NULL
           ,NULL
           ,4096
           ,'jpg,pdf'
           ,0
           ,0
           ,1
           ,0
           ,0
           ,0)