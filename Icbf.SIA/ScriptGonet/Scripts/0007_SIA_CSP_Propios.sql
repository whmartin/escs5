USE [SIA]
GO
/*
Usuario Creación: Carlos Cubillos
Fecha Creación: 29/10/2013
Descripcion: Creación de procedimientos almacenados propios de proveedores y contratos.
*/
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]
	@IdAmparo INT
AS
BEGIN
 SELECT IdAmparo, IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AmparosAsociados] WHERE  IdAmparo = @IdAmparo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]
	@IdAmparo INT
AS
BEGIN
	DELETE Contrato.AmparosAsociados WHERE IdAmparo = @IdAmparo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]
		@IdAmparo INT OUTPUT, 	@IdGarantia INT,	@IdTipoAmparo INT,	@FechaVigenciaDesde DATETIME,	@VigenciaHasta DATETIME,	@ValorParaCalculoAsegurado NUMERIC(18,3),	@IdTipoCalculo INT,	@ValorAsegurado NUMERIC(18,3), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.AmparosAsociados(IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea)
					  VALUES(@IdGarantia, @IdTipoAmparo, @FechaVigenciaDesde, @VigenciaHasta, @ValorParaCalculoAsegurado, @IdTipoCalculo, @ValorAsegurado, @UsuarioCrea, GETDATE())
	SELECT @IdAmparo = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]
		@IdAmparo INT,	@IdGarantia INT,	@IdTipoAmparo INT,	@FechaVigenciaDesde DATETIME,	@VigenciaHasta DATETIME,	@ValorParaCalculoAsegurado NUMERIC(18,3),	@IdTipoCalculo INT,	@ValorAsegurado NUMERIC(18,3), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.AmparosAsociados SET IdGarantia = @IdGarantia, IdTipoAmparo = @IdTipoAmparo, FechaVigenciaDesde = @FechaVigenciaDesde, VigenciaHasta = @VigenciaHasta, ValorParaCalculoAsegurado = @ValorParaCalculoAsegurado, IdTipoCalculo = @IdTipoCalculo, ValorAsegurado = @ValorAsegurado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdAmparo = @IdAmparo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociadoss_Consultar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]
	@IdGarantia INT = NULL,@FechaVigenciaDesde DATETIME = NULL,@VigenciaHasta DATETIME = NULL
AS
BEGIN
 SELECT IdAmparo, IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AmparosAsociados] WHERE IdGarantia = CASE WHEN @IdGarantia IS NULL THEN IdGarantia ELSE @IdGarantia END AND FechaVigenciaDesde = CASE WHEN @FechaVigenciaDesde IS NULL THEN FechaVigenciaDesde ELSE @FechaVigenciaDesde END AND VigenciaHasta = CASE WHEN @VigenciaHasta IS NULL THEN VigenciaHasta ELSE @VigenciaHasta END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar_Table]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar_Table]
	@NumeroGarantia NVARCHAR(50) = NULL,
	@IdTipoAmparo INT = NULL,
	@VigenciaDesde DATETIME = NULL,
	@VigenciaHasta	DATETIME = NULL	
AS
BEGIN
 SELECT IdAmparo, amparo.IdGarantia, amparo.IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, 
 IdTipoCalculo, ValorAsegurado, amparo.UsuarioCrea, amparo.FechaCrea, amparo.UsuarioModifica, amparo.FechaModifica ,
 tipoamparo.NombreTipoAmparo as tipoamparo, garantia.NumGarantia
 FROM [Contrato].[AmparosAsociados] amparo inner join Contrato.Garantia garantia
 on (garantia.IDGarantia = amparo.IdGarantia) INNER JOIN Contrato.TipoAmparo tipoamparo
 on (tipoamparo.IdTipoAmparo = amparo.IdTipoAmparo)
 WHERE amparo.IdTipoAmparo = CASE WHEN @IdTipoAmparo IS NULL THEN amparo.IdTipoAmparo ELSE @IdTipoAmparo END 
 AND FechaVigenciaDesde >= CASE WHEN @VigenciaDesde IS NULL THEN FechaVigenciaDesde ELSE @VigenciaDesde END 
 AND VigenciaHasta <= CASE WHEN @VigenciaHasta IS NULL THEN VigenciaHasta ELSE @VigenciaHasta END
 AND garantia.NumGarantia = CASE WHEN @NumeroGarantia IS NULL THEN garantia.NumGarantia ELSE @NumeroGarantia END
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]
	@IdAporte INT
AS
BEGIN
 SELECT IdAporte, IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AporteContrato] WHERE  IdAporte = @IdAporte
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Eliminar
***********************************************/
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]
	@IdAporte INT
AS
BEGIN
	DELETE Contrato.AporteContrato WHERE IdAporte = @IdAporte
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Insertar
***********************************************/


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]
		@IdAporte INT OUTPUT, 	@IdContrato NVARCHAR(3),	@TipoAportante NVARCHAR(11),	@NumeroIdenticacionContratista bigint,	@TipoAporte NVARCHAR(10),	@ValorAporte INT,	@DescripcionAporteEspecie NVARCHAR(128),	@FechaRP DATETIME,	@NumeroRP INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.AporteContrato(IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea)
					  VALUES(@IdContrato, @TipoAportante, @NumeroIdenticacionContratista, @TipoAporte, @ValorAporte, @DescripcionAporteEspecie, @FechaRP, @NumeroRP, @UsuarioCrea, GETDATE())
	SELECT @IdAporte = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]
		@IdAporte INT,	@IdContrato NVARCHAR(3),	@TipoAportante NVARCHAR(11),	@NumeroIdenticacionContratista bigint,	@TipoAporte NVARCHAR(10),	@ValorAporte INT,	@DescripcionAporteEspecie NVARCHAR(128),	@FechaRP DATETIME,	@NumeroRP INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.AporteContrato SET IdContrato = @IdContrato, TipoAportante = @TipoAportante, NumeroIdenticacionContratista = @NumeroIdenticacionContratista, TipoAporte = @TipoAporte, ValorAporte = @ValorAporte, DescripcionAporteEspecie = @DescripcionAporteEspecie, FechaRP = @FechaRP, NumeroRP = @NumeroRP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdAporte = @IdAporte
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]
	@IdAporte INT = NULL,@IdContrato NVARCHAR(3) = NULL,@TipoAportante NVARCHAR(11) = NULL,@NumeroIdenticacionContratista bigint = NULL,@TipoAporte NVARCHAR(10) = NULL,@ValorAporte INT = NULL,@DescripcionAporteEspecie NVARCHAR(128) = NULL,@FechaRP DATETIME = NULL,@NumeroRP INT = NULL
AS
BEGIN
 SELECT IdAporte, IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AporteContrato] WHERE IdAporte = CASE WHEN @IdAporte IS NULL THEN IdAporte ELSE @IdAporte END AND IdContrato = CASE WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato END AND TipoAportante = CASE WHEN @TipoAportante IS NULL THEN TipoAportante ELSE @TipoAportante END AND NumeroIdenticacionContratista = CASE WHEN @NumeroIdenticacionContratista IS NULL THEN NumeroIdenticacionContratista ELSE @NumeroIdenticacionContratista END AND TipoAporte = CASE WHEN @TipoAporte IS NULL THEN TipoAporte ELSE @TipoAporte END AND ValorAporte = CASE WHEN @ValorAporte IS NULL THEN ValorAporte ELSE @ValorAporte END AND DescripcionAporteEspecie = CASE WHEN @DescripcionAporteEspecie IS NULL THEN DescripcionAporteEspecie ELSE @DescripcionAporteEspecie END AND FechaRP = CASE WHEN @FechaRP IS NULL THEN FechaRP ELSE @FechaRP END AND NumeroRP = CASE WHEN @NumeroRP IS NULL THEN NumeroRP ELSE @NumeroRP END
 ORDER BY IdAporte,NumeroIdenticacionContratista, TipoAporte 
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]
	@IdCategoriaContrato INT
AS
BEGIN
 SELECT IdCategoriaContrato, NombreCategoriaContrato, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[CategoriaContrato] WHERE  IdCategoriaContrato = @IdCategoriaContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]
	@IdCategoriaContrato INT
AS
BEGIN
	DELETE Contrato.CategoriaContrato WHERE IdCategoriaContrato = @IdCategoriaContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]
		@IdCategoriaContrato INT OUTPUT, 	@NombreCategoriaContrato NVARCHAR(50),	@Descripcion NVARCHAR(100),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.CategoriaContrato(NombreCategoriaContrato, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreCategoriaContrato, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdCategoriaContrato = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]
		@IdCategoriaContrato INT,	@NombreCategoriaContrato NVARCHAR(50),	@Descripcion NVARCHAR(100),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.CategoriaContrato SET NombreCategoriaContrato = @NombreCategoriaContrato, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdCategoriaContrato = @IdCategoriaContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el sp_usp_RubOnline_Contrato_CategoriaContratos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]

	@NombreCategoriaContrato NVARCHAR (50) = NULL,
	@DescripcionCategoriaContrato NVARCHAR (50) = NULL,
	@Estado BIT = NULL

AS
BEGIN

	SELECT
		IdCategoriaContrato,
		NombreCategoriaContrato,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[CategoriaContrato]
	WHERE NombreCategoriaContrato LIKE '%' + 
		CASE
			WHEN @NombreCategoriaContrato IS NULL THEN NombreCategoriaContrato ELSE @NombreCategoriaContrato
		END + '%'
	AND Descripcion LIKE '%' + 
		CASE
			WHEN @DescripcionCategoriaContrato IS NULL THEN Descripcion ELSE @DescripcionCategoriaContrato
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreCategoriaContrato 

END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que consulta un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]
	@IdClausulaContrato INT
AS
BEGIN
 SELECT IdClausulaContrato, NombreClausulaContrato, IdTipoClausula, Contenido, IdTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ClausulaContrato] WHERE  IdClausulaContrato = @IdClausulaContrato
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ClausulaContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]
	@IdClausulaContrato INT
	AS
	BEGIN
		DELETE Contrato.ClausulaContrato WHERE IdClausulaContrato = @IdClausulaContrato
	END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que inserta un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]
		@IdClausulaContrato INT OUTPUT, 	
		@NombreClausulaContrato NVARCHAR(128),	
		@IdTipoClausula INT,	
		@Contenido nvarchar(MAX),	
		@IdTipoContrato INT,	
		@Estado BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ClausulaContrato(NombreClausulaContrato, IdTipoClausula, Contenido, IdTipoContrato, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreClausulaContrato, @IdTipoClausula, @Contenido, @IdTipoContrato, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdClausulaContrato = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que modifica un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]
		@IdClausulaContrato INT,	@NombreClausulaContrato NVARCHAR(128),	@IdTipoClausula INT,	@Contenido nvarchar(MAX),	@IdTipoContrato INT,	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ClausulaContrato SET NombreClausulaContrato = @NombreClausulaContrato, IdTipoClausula = @IdTipoClausula, Contenido = @Contenido, IdTipoContrato = @IdTipoContrato, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdClausulaContrato = @IdClausulaContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ClausulaContratos_Consultar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]

	@NombreClausulaContrato NVARCHAR (128) = NULL, 
	@Contenido nvarchar(MAX) = NULL,
	@IdTipoClausula INT = NULL, 
	@IdTipoContrato INT = NULL,
	@Estado bit = NULL

AS
BEGIN

	SELECT
		IdClausulaContrato,
		NombreClausulaContrato,
		IdTipoClausula,
		Contenido,
		IdTipoContrato,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[ClausulaContrato]
	WHERE NombreClausulaContrato LIKE '%' + 
		CASE
			WHEN @NombreClausulaContrato IS NULL THEN NombreClausulaContrato ELSE @NombreClausulaContrato
		END + '%'
	AND Contenido LIKE '%' + 
		CASE
			WHEN @Contenido IS NULL THEN Contenido ELSE @Contenido
		END + '%'
	AND IdTipoClausula =
		CASE
			WHEN @IdTipoClausula IS NULL THEN IdTipoClausula ELSE @IdTipoClausula
		END
	AND IdTipoContrato =
		CASE
			WHEN @IdTipoContrato IS NULL THEN IdTipoContrato ELSE @IdTipoContrato
		END
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreClausulaContrato

END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Consulta_ContratistaContrato]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  08/01/2013 11:48:15 AM
-- Description:	Procedimiento almacenado Consulta los Contratista asociados a un contrato
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Consulta_ContratistaContrato]
@IDCONTRATO INT
AS

	SELECT 
	Contrato.RelacionarContratistas.IdContratistaContrato ,
	Contrato.RelacionarContratistas.NumeroIdentificacion,
	Oferente.TERCERO.RAZONSOCIAL,
	CASE Contrato.RelacionarContratistas.ClaseEntidad
		WHEN 1 THEN 'Contratista'
		WHEN 2 THEN 'Consorcio/Unión Temporal'
	END ENTIDAD
	FROM
	Contrato.RelacionarContratistas,
	Oferente.TERCERO
	where
	Oferente.TERCERO.NUMEROIDENTIFICACION = Contrato.RelacionarContratistas.NUMEROIDENTIFICACION
	AND IDCONTRATO = @IDCONTRATO



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que consulta un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]
	@TipoSupervisorInterventor INT
AS
BEGIN
 SELECT TipoSupervisorInterventor, NumeroContrato, NumeroIdentificacion, NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ConsultarSupervisorInterventor] WHERE  TipoSupervisorInterventor = @TipoSupervisorInterventor
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que elimina un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]
	@TipoSupervisorInterventor INT
AS
BEGIN
	DELETE Contrato.ConsultarSupervisorInterventor WHERE TipoSupervisorInterventor = @TipoSupervisorInterventor
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que guarda un nuevo ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]
		@TipoSupervisorInterventor INT OUTPUT, 	@NumeroContrato NVARCHAR(50),	@NumeroIdentificacion INT,	@NombreRazonSocialSupervisorInterventor NVARCHAR(50),	@NumeroIdentificacionDirectorInterventoria INT,	@NombreRazonSocialDirectorInterventoria NVARCHAR(50), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ConsultarSupervisorInterventor(NumeroContrato, NumeroIdentificacion, NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroContrato, @NumeroIdentificacion, @NombreRazonSocialSupervisorInterventor, @NumeroIdentificacionDirectorInterventoria, @NombreRazonSocialDirectorInterventoria, @UsuarioCrea, GETDATE())
	SELECT @TipoSupervisorInterventor = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que actualiza un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]
		@TipoSupervisorInterventor INT,	@NumeroContrato NVARCHAR(50),	@NumeroIdentificacion INT,	@NombreRazonSocialSupervisorInterventor NVARCHAR(50),	@NumeroIdentificacionDirectorInterventoria INT,	@NombreRazonSocialDirectorInterventoria NVARCHAR(50), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ConsultarSupervisorInterventor SET NumeroContrato = @NumeroContrato, NumeroIdentificacion = @NumeroIdentificacion, NombreRazonSocialSupervisorInterventor = @NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria = @NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria = @NombreRazonSocialDirectorInterventoria, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE TipoSupervisorInterventor = @TipoSupervisorInterventor
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que consulta un(a) ConsultarSupervisorInterventor
-- Ajste en el formato de las fechas
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar]
	@TipoSupervisorInterventor INT = NULL,
	@NumeroContrato NVARCHAR(50) = NULL,
	@NumeroIdentificacion varchar(128) = NULL,
	@NombreRazonSocialSupervisorInterventor NVARCHAR(128) = NULL,
	@NumeroIdentificacionDirectorInterventoria varchar(128) = NULL,
	@NombreRazonSocialDirectorInterventoria NVARCHAR(128) = NULL
AS
BEGIN
 SELECT 
  SUPCON.IDSUPERVISORINTERV,
 TIPCON.NOMBRE AS TIPOSUPERVISORINTERVENTOR, 
 CONVERT(VARCHAR(10),CON.NUMEROCONTRATO,103) AS NUMEROCONTRATO, 
 CONVERT(VARCHAR(10),CON.FECHASUSCRIPCION,103) AS FECHASUSCRIPCION,
 CONVERT(VARCHAR(10),CON.FECHAFINALTERMINACION,103) AS FECHAFINALTERMINACION,
 CONINVERT.NUMEROCONTRATO as NUMEROCONTRATOINTER,
 CONVERT(VARCHAR(10),SUPCON.FECHAINICIA,103) AS FECHAINICIA,
 CONVERT(VARCHAR(10),SUPCON.FECHAFINALIZA,103) AS FECHAFINALIZA,
 CONVERT(VARCHAR(10),SUPCON.FECHAMODIFICAINTERVENTOR,103) AS FECHAMODIFICAINTERVENTOR,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN (SELECT GLOBAL.TIPOSDOCUMENTOS.NOMTIPODOCUMENTO FROM GLOBAL.TIPOSDOCUMENTOS WHERE IDTIPODOCUMENTO = TERCERO.IDTIPODOCIDENTIFICA)END AS TIPOIDENTIFICACIONSUPINV,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.NUMEROIDENTIFICACION END NUMEROIDENTIFICACION, 
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.RAZONSOCIAL END NOMBRERAZONSOCIALSUPERVISORINTERVENTOR,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.PRIMERAPELLIDO END PRIMERAPELLIDO,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.SEGUNDOAPELLIDO END SEGUNDOAPELLIDO,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.PRIMERNOMBRE END PRIMERNOMBRE,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.SEGUNDONOMBRE END SEGUNDONOMBRE,
 DIRINTER.NUMEROIDENTIFICACION NUMEROIDENTIFICACIONDIRECTORINTERVENTORIA, 
 DIRINTER.PRIMERAPELLIDO AS PRIMERAPELLIDODIRECT,
 DIRINTER.SEGUNDOAPELLIDO AS SEGUNDOAPELLIDODIRECT,
 DIRINTER.PRIMERNOMBRE AS PRIMERNOMBREDIRECT,
 DIRINTER.SEGUNDONOMBRE AS SEGUNDONOMBREDIRECT,
 DIRINTER.RAZONSOCIAL NOMBRERAZONSOCIALDIRECTORINTERVENTORIA, 
 SUPCON.USUARIOCREA, 
 SUPCON.FECHACREA, 
 SUPCON.USUARIOMODIFICA, 
 SUPCON.FECHAMODIFICA 
 FROM [Contrato].[SupervisorIntervContrato] SUPCON INNER JOIN Contrato.TipoSupvInterventor TIPCON 
 ON (TIPCON.IdTipoSupvInterventor = SUPCON.IDTipoSupvInterventor) INNER JOIN Contrato.Contrato CON
 ON (CON.IdContrato = SUPCON.IDContratoSupervisa) LEFT outer JOIN Oferente.TERCERO TERCERO
 ON (TERCERO.IDTERCERO = SUPCON.IDTerceroExterno) LEFT outer JOIN Oferente.TERCERO DIRINTER
 ON (DIRINTER.IDTERCERO = SUPCON.IDTERCEROINTERVENTORIA) LEFT OUTER JOIN CONTRATO.CONTRATO CONINVERT
 ON (CONINVERT.IDCONTRATO = SUPCON.IDCONTRATOINTERVENTORIA)
 WHERE SUPCON.IdTipoSupvInterventor = CASE WHEN @TipoSupervisorInterventor IS NULL THEN SUPCON.IdTipoSupvInterventor ELSE @TipoSupervisorInterventor END 
 AND CON.NumeroContrato = CASE WHEN @NumeroContrato IS NULL THEN CON.NumeroContrato ELSE @NumeroContrato END 
 AND TERCERO.NUMEROIDENTIFICACION = CASE WHEN @NumeroIdentificacion IS NULL THEN TERCERO.NUMEROIDENTIFICACION ELSE @NumeroIdentificacion END
 AND ((TERCERO.RAZONSOCIAL LIKE '%' + CASE WHEN @NombreRazonSocialSupervisorInterventor IS NULL THEN TERCERO.RAZONSOCIAL ELSE @NombreRazonSocialSupervisorInterventor END + '%')
	OR (TERCERO.PRIMERNOMBRE + ' ' +TERCERO.SEGUNDONOMBRE + ' ' + TERCERO.PRIMERAPELLIDO + ' ' + TERCERO.SEGUNDOAPELLIDO LIKE '%' + CASE WHEN @NombreRazonSocialSupervisorInterventor IS NULL THEN TERCERO.PRIMERNOMBRE + ' ' +TERCERO.SEGUNDONOMBRE + ' ' + TERCERO.PRIMERAPELLIDO + ' ' + TERCERO.SEGUNDOAPELLIDO ELSE @NombreRazonSocialSupervisorInterventor END + '%'))
 AND (DIRINTER.NUMEROIDENTIFICACION = @NumeroIdentificacionDirectorInterventoria OR @NumeroIdentificacionDirectorInterventoria IS NULL)
 AND ((DIRINTER.RAZONSOCIAL LIKE '%' + @NombreRazonSocialDirectorInterventoria OR @NombreRazonSocialDirectorInterventoria IS NULL)
	OR (DIRINTER.PRIMERNOMBRE + ' ' +DIRINTER.SEGUNDONOMBRE + ' ' + DIRINTER.PRIMERAPELLIDO + ' ' + DIRINTER.SEGUNDOAPELLIDO LIKE '%' + @NombreRazonSocialDirectorInterventoria OR @NombreRazonSocialDirectorInterventoria IS NULL))
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que consulta un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]
	@IdContrato INT
AS
BEGIN

	 SELECT IdContrato,
		FechaRegistro,
		NumeroProceso,
		NumeroContrato,
		FechaAdjudicacion,
		IdModalidad,
		IdCategoriaContrato,
		IdTipoContrato,
		IdCodigoModalidad,
		IdModalidadAcademica,
		IdCodigoProfesion,
		IdNombreProfesion,
		IdRegionalContrato,
		RequiereActa,
		ManejaAportes,
		ManejaRecursos,
		ManejaVigenciasFuturas,
		IdRegimenContratacion,
		CodigoRegional,
		NombreSolicitante,
		DependenciaSolicitante,
		CargoSolicitante,
		ObjetoContrato,
		AlcanceObjetoContrato,
		ValorInicialContrato,
		ValorTotalAdiciones,
		ValorFinalContrato,
		ValorAportesICBF,
		ValorAportesOperador,
		ValorTotalReduccion,
		JustificacionAdicionSuperior50porc,
		FechaSuscripcion,
		FechaInicioEjecucion,
		FechaFinalizacionInicial,
		PlazoInicial,
		FechaInicialTerminacion,
		FechaFinalTerminacion,
		FechaProyectadaLiquidacion,
		FechaAnulacion,
		Prorrogas,
		PlazoTotal,
		FechaFirmaActaInicio,
		VigenciaFiscalInicial,
		VigenciaFiscalFinal,
		IdUnidadEjecucion,
		IdLugarEjecucion,
		DatosAdicionales,
		IdEstadoContrato,
		IdTipoDocumentoContratista,
		IdentificacionContratista,
		NombreContratista,
		IdFormaPago,
		IdTipoEntidad,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica,
		ClaseContrato,
		Consecutivo,
		AfectaPlanCompras,
		IdSolicitante,
		IdProducto,
		FechaLiquidacion,
		NumeroDocumentoVigenciaFutura
	 FROM [Contrato].[Contrato] 
	 WHERE  IdContrato = @IdContrato
	 
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que elimina un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]
	@IdContrato INT
AS
BEGIN
	DELETE Contrato.Contrato WHERE IdContrato = @IdContrato
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Insertar]
@IdContrato	int	output,
@FechaRegistro	datetime,
@NumeroProceso	nvarchar(50),
@NumeroContrato	nvarchar(50),
@FechaAdjudicacion	datetime,
@IdModalidad	int,
@IdCategoriaContrato	int,
@IdTipoContrato	int,
@IdCodigoModalidad	int,
@IdModalidadAcademica	int,
@IdCodigoProfesion	int,
@IdNombreProfesion	int,
@IdRegionalContrato	int,
@RequiereActa	bit,
@ManejaAportes	bit,
@ManejaRecursos	bit,
@ManejaVigenciasFuturas	bit,
@IdRegimenContratacion	int,
@CodigoRegional	int,
@NombreSolicitante	nvarchar(50),
@DependenciaSolicitante	nvarchar(50),
@CargoSolicitante	nvarchar(150),
@ObjetoContrato	nvarchar(300),
@AlcanceObjetoContrato	nvarchar(128),
@ValorInicialContrato	numeric(18, 3),
@ValorTotalAdiciones	numeric(18, 3),
@ValorFinalContrato	numeric(18, 3),
@ValorAportesICBF	numeric(18, 3),
@ValorAportesOperador	numeric(18, 3),
@ValorTotalReduccion	numeric(18, 3),
@JustificacionAdicionSuperior50porc	nvarchar(300),
@FechaSuscripcion	datetime,
@FechaInicioEjecucion	datetime,
@FechaFinalizacionInicial	datetime,
@PlazoInicial	int,
@FechaInicialTerminacion	datetime,
@FechaFinalTerminacion	datetime,
@FechaProyectadaLiquidacion	datetime,
@FechaAnulacion	datetime,
@Prorrogas	int,
@PlazoTotal	int,
@FechaFirmaActaInicio	datetime,
@VigenciaFiscalInicial	int,
@VigenciaFiscalFinal	int,
@IdUnidadEjecucion	int,
@IdLugarEjecucion	int,
@DatosAdicionales	nvarchar(128),
@IdEstadoContrato	int,
@IdTipoDocumentoContratista	nvarchar(3),
@IdentificacionContratista	nvarchar(50),
@NombreContratista	nvarchar(128),
@IdFormaPago	int,
@IdTipoEntidad	int,
@UsuarioCrea	nvarchar(250),
@ClaseContrato	nchar(1),
@Consecutivo	varchar(50),
@AfectaPlanCompras	bit,
@IdSolicitante	int,
@IdProducto	int,
@FechaLiquidacion	datetime,
@NumeroDocumentoVigenciaFutura	int,
@RequiereGarantia bit = null
AS
BEGIN
	INSERT INTO Contrato.Contrato(
	FechaRegistro,
	NumeroProceso,
	NumeroContrato,
	FechaAdjudicacion,
	IdModalidad,
	IdCategoriaContrato,
	IdTipoContrato,
	IdCodigoModalidad,
	IdModalidadAcademica,
	IdCodigoProfesion,
	IdNombreProfesion,
	IdRegionalContrato,
	RequiereActa,
	ManejaAportes,
	ManejaRecursos,
	ManejaVigenciasFuturas,
	IdRegimenContratacion,
	CodigoRegional,
	NombreSolicitante,
	DependenciaSolicitante,
	CargoSolicitante,
	ObjetoContrato,
	AlcanceObjetoContrato,
	ValorInicialContrato,
	ValorTotalAdiciones,
	ValorFinalContrato,
	ValorAportesICBF,
	ValorAportesOperador,
	ValorTotalReduccion,
	JustificacionAdicionSuperior50porc,
	FechaSuscripcion,
	FechaInicioEjecucion,
	FechaFinalizacionInicial,
	PlazoInicial,
	FechaInicialTerminacion,
	FechaFinalTerminacion,
	FechaProyectadaLiquidacion,
	FechaAnulacion,
	Prorrogas,
	PlazoTotal,
	FechaFirmaActaInicio,
	VigenciaFiscalInicial,
	VigenciaFiscalFinal,
	IdUnidadEjecucion,
	IdLugarEjecucion,
	DatosAdicionales,
	IdEstadoContrato,
	IdTipoDocumentoContratista,
	IdentificacionContratista,
	NombreContratista,
	IdFormaPago,
	IdTipoEntidad,
	UsuarioCrea,
	FechaCrea,
	ClaseContrato,
	Consecutivo,
	AfectaPlanCompras,
	IdSolicitante,
	IdProducto,
	FechaLiquidacion,
	NumeroDocumentoVigenciaFutura,
	RequiereGarantia
	)
VALUES(
	@FechaRegistro,
	@NumeroProceso,
	@NumeroContrato,
	@FechaAdjudicacion,
	@IdModalidad,
	@IdCategoriaContrato,
	@IdTipoContrato,
	@IdCodigoModalidad,
	@IdModalidadAcademica,
	@IdCodigoProfesion,
	@IdNombreProfesion,
	@IdRegionalContrato,
	@RequiereActa,
	@ManejaAportes,
	@ManejaRecursos,
	@ManejaVigenciasFuturas,
	@IdRegimenContratacion,
	@CodigoRegional,
	@NombreSolicitante,
	@DependenciaSolicitante,
	@CargoSolicitante,
	@ObjetoContrato,
	@AlcanceObjetoContrato,
	@ValorInicialContrato,
	@ValorTotalAdiciones,
	@ValorFinalContrato,
	@ValorAportesICBF,
	@ValorAportesOperador,
	@ValorTotalReduccion,
	@JustificacionAdicionSuperior50porc,
	@FechaSuscripcion,
	@FechaInicioEjecucion,
	@FechaFinalizacionInicial,
	@PlazoInicial,
	@FechaInicialTerminacion,
	@FechaFinalTerminacion,
	@FechaProyectadaLiquidacion,
	@FechaAnulacion,
	@Prorrogas,
	@PlazoTotal,
	@FechaFirmaActaInicio,
	@VigenciaFiscalInicial,
	@VigenciaFiscalFinal,
	@IdUnidadEjecucion,
	@IdLugarEjecucion,
	@DatosAdicionales,
	@IdEstadoContrato,
	@IdTipoDocumentoContratista,
	@IdentificacionContratista,
	@NombreContratista,
	@IdFormaPago,
	@IdTipoEntidad,
	@UsuarioCrea,
	GETDATE(),
	@ClaseContrato,
	@Consecutivo,
	@AfectaPlanCompras,
	@IdSolicitante,
	@IdProducto,
	@FechaLiquidacion,
	@NumeroDocumentoVigenciaFutura,
	@RequiereGarantia)
	SELECT @IdContrato = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]
@IdContrato		int,
@FechaRegistro	datetime,
@NumeroProceso	nvarchar(50),
@NumeroContrato	nvarchar(50),
@FechaAdjudicacion	datetime,
@IdModalidad	int,
@IdCategoriaContrato	int,
@IdTipoContrato	int,
@IdCodigoModalidad	int,
@IdModalidadAcademica	int,
@IdCodigoProfesion	int,
@IdNombreProfesion	int,
@IdRegionalContrato	int,
@RequiereActa	bit,
@ManejaAportes	bit,
@ManejaRecursos	bit,
@ManejaVigenciasFuturas	bit,
@IdRegimenContratacion	int,
@CodigoRegional	int,
@NombreSolicitante	nvarchar(50),
@DependenciaSolicitante	nvarchar(50),
@CargoSolicitante	nvarchar(150),
@ObjetoContrato	nvarchar(300),
@AlcanceObjetoContrato	nvarchar(128),
@ValorInicialContrato	numeric(18, 3),
@ValorTotalAdiciones	numeric(18, 3),
@ValorFinalContrato	numeric(18, 3),
@ValorAportesICBF	numeric(18, 3),
@ValorAportesOperador	numeric(18, 3),
@ValorTotalReduccion	numeric(18, 3),
@JustificacionAdicionSuperior50porc	nvarchar(300),
@FechaSuscripcion	datetime,
@FechaInicioEjecucion	datetime,
@FechaFinalizacionInicial	datetime,
@PlazoInicial	int,
@FechaInicialTerminacion	datetime,
@FechaFinalTerminacion	datetime,
@FechaProyectadaLiquidacion	datetime,
@FechaAnulacion	datetime,
@Prorrogas	int,
@PlazoTotal	int,
@FechaFirmaActaInicio	datetime,
@VigenciaFiscalInicial	int,
@VigenciaFiscalFinal	int,
@IdUnidadEjecucion	int,
@IdLugarEjecucion	int,
@DatosAdicionales	nvarchar(128),
@IdEstadoContrato	int,
@IdTipoDocumentoContratista	nvarchar(3),
@IdentificacionContratista	nvarchar(50),
@NombreContratista	nvarchar(128),
@IdFormaPago	int,
@IdTipoEntidad	int,
@UsuarioModifica NVARCHAR(250),
@ClaseContrato	nchar(1),
@Consecutivo	varchar(50),
@AfectaPlanCompras	bit,
@IdSolicitante	int,
@IdProducto	int,
@FechaLiquidacion	datetime,
@NumeroDocumentoVigenciaFutura	int,
@RequiereGarantia bit = null
AS
BEGIN
	UPDATE Contrato.Contrato 
	SET FechaRegistro = @FechaRegistro,
	NumeroProceso = @NumeroProceso,
	NumeroContrato = @NumeroContrato,
	FechaAdjudicacion = @FechaAdjudicacion,
	IdModalidad = @IdModalidad,
	IdCategoriaContrato = @IdCategoriaContrato,
	IdTipoContrato = @IdTipoContrato,
	IdCodigoModalidad = @IdCodigoModalidad,
	IdModalidadAcademica = @IdModalidadAcademica,
	IdCodigoProfesion = @IdCodigoProfesion,
	IdNombreProfesion = @IdNombreProfesion,
	IdRegionalContrato = @IdRegionalContrato,
	RequiereActa = @RequiereActa,
	ManejaAportes = @ManejaAportes,
	ManejaRecursos = @ManejaRecursos,
	ManejaVigenciasFuturas = @ManejaVigenciasFuturas,
	IdRegimenContratacion = @IdRegimenContratacion,
	CodigoRegional = @CodigoRegional,
	NombreSolicitante = @NombreSolicitante,
	DependenciaSolicitante = @DependenciaSolicitante,
	CargoSolicitante = @CargoSolicitante,
	ObjetoContrato = @ObjetoContrato,
	AlcanceObjetoContrato = @AlcanceObjetoContrato,
	ValorInicialContrato = @ValorInicialContrato,
	ValorTotalAdiciones = @ValorTotalAdiciones,
	ValorFinalContrato = @ValorFinalContrato,
	ValorAportesICBF = @ValorAportesICBF,
	ValorAportesOperador = @ValorAportesOperador,
	ValorTotalReduccion = @ValorTotalReduccion,
	JustificacionAdicionSuperior50porc = @JustificacionAdicionSuperior50porc,
	FechaSuscripcion = @FechaSuscripcion,
	FechaInicioEjecucion = @FechaInicioEjecucion,
	FechaFinalizacionInicial = @FechaFinalizacionInicial,
	PlazoInicial = @PlazoInicial,
	FechaInicialTerminacion = @FechaInicialTerminacion,
	FechaFinalTerminacion = @FechaFinalTerminacion,
	FechaProyectadaLiquidacion =@FechaProyectadaLiquidacion,
	FechaAnulacion = @FechaAnulacion,
	Prorrogas = @Prorrogas,
	PlazoTotal = @PlazoTotal,
	FechaFirmaActaInicio = @FechaFirmaActaInicio,
	VigenciaFiscalInicial = @VigenciaFiscalInicial,
	VigenciaFiscalFinal = @VigenciaFiscalFinal,
	IdUnidadEjecucion = @IdUnidadEjecucion,
	IdLugarEjecucion = @IdLugarEjecucion,
	DatosAdicionales = @DatosAdicionales,
	IdEstadoContrato = @IdEstadoContrato,
	IdTipoDocumentoContratista = @IdTipoDocumentoContratista,
	IdentificacionContratista = @IdentificacionContratista,
	NombreContratista = @NombreContratista,
	IdFormaPago = @IdFormaPago,
	IdTipoEntidad = @IdTipoEntidad,
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE(),
	ClaseContrato = @ClaseContrato,
	Consecutivo = @Consecutivo,
	AfectaPlanCompras = @AfectaPlanCompras,
	IdSolicitante = @IdSolicitante,
	IdProducto = @IdProducto,
	FechaLiquidacion = @FechaLiquidacion,
	NumeroDocumentoVigenciaFutura = @NumeroDocumentoVigenciaFutura,
	@RequiereGarantia = RequiereGarantia
	WHERE IdContrato = @IdContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que consulta un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]
@FechaRegistro DATETIME = NULL,
@NumeroProceso NVARCHAR (50) = NULL,
@NumeroContrato nvarchar (50) = NULL,
@IdModalidad INT = NULL,
@IdCategoriaContrato INT = NULL,
@IdTipoContrato INT = NULL,
@ClaseContrato	nchar(1) = null
AS
BEGIN

SELECT
	CONVERT(date, FechaRegistro),
	IdContrato,
	FechaRegistro,
	NumeroProceso,
	NumeroContrato,
	FechaAdjudicacion,
	IdModalidad,
	IdCategoriaContrato,
	IdTipoContrato,
	IdCodigoModalidad,
	IdModalidadAcademica,
	IdCodigoProfesion,
	IdNombreProfesion,
	IdRegionalContrato,
	RequiereActa,
	ManejaAportes,
	ManejaRecursos,
	ManejaVigenciasFuturas,
	IdRegimenContratacion,
	CodigoRegional,
	NombreSolicitante,
	DependenciaSolicitante,
	CargoSolicitante,
	ObjetoContrato,
	AlcanceObjetoContrato,
	ValorInicialContrato,
	ValorTotalAdiciones,
	ValorFinalContrato,
	ValorAportesICBF,
	ValorAportesOperador,
	ValorTotalReduccion,
	JustificacionAdicionSuperior50porc,
	FechaSuscripcion,
	FechaInicioEjecucion,
	FechaFinalizacionInicial,
	PlazoInicial,
	FechaInicialTerminacion,
	FechaFinalTerminacion,
	FechaProyectadaLiquidacion,
	FechaAnulacion,
	Prorrogas,
	PlazoTotal,
	FechaFirmaActaInicio,
	VigenciaFiscalInicial,
	VigenciaFiscalFinal,
	IdUnidadEjecucion,
	IdLugarEjecucion,
	DatosAdicionales,
	IdEstadoContrato,
	IdTipoDocumentoContratista,
	IdentificacionContratista,
	NombreContratista,
	IdFormaPago,
	IdTipoEntidad,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica,
	ClaseContrato,
	Consecutivo,
	AfectaPlanCompras,
	IdSolicitante,
	IdProducto,
	FechaLiquidacion,
	NumeroDocumentoVigenciaFutura,
	RequiereGarantia
FROM Contrato.Contrato
WHERE (NumeroProceso = @NumeroProceso OR @NumeroProceso IS NULL)
AND (NumeroContrato = @NumeroContrato OR @NumeroContrato IS NULL)
AND (IdModalidad = @IdModalidad OR @IdModalidad IS NULL)
AND (IdCategoriaContrato = @IdCategoriaContrato OR @IdCategoriaContrato IS NULL)
AND (IdTipoContrato = @IdTipoContrato OR @IdTipoContrato IS NULL)
AND FechaRegistro =
--AND (CONVERT(date, FechaRegistro) = CONVERT(date, @FechaRegistro))
	CASE
		WHEN @FechaRegistro IS NULL THEN FechaRegistro ELSE @FechaRegistro
	END
AND ClaseContrato = ISNULL(@ClaseContrato,ClaseContrato)
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contratos_Consultar_lupa]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que consulta un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar_lupa]
@FechaRegistro DATETIME = NULL,
@NumeroProceso NVARCHAR (50) = NULL,
@NumeroContrato nvarchar (50) = NULL,
@IdModalidad INT = NULL,
@IdCategoriaContrato INT = NULL,
@IdTipoContrato INT = NULL,
@ClaseContrato	nchar(1) = null
AS
BEGIN

SELECT
	CONVERT(date, FechaRegistro),
	IdContrato,
	FechaRegistro,
	NumeroProceso,
	NumeroContrato,
	FechaAdjudicacion,
	contr.IdModalidad,
	contr.IdCategoriaContrato,
	contr.IdTipoContrato,
	IdCodigoModalidad,
	IdModalidadAcademica,
	IdCodigoProfesion,
	IdNombreProfesion,
	IdRegionalContrato,
	RequiereActa,
	ManejaAportes,
	ManejaRecursos,
	ManejaVigenciasFuturas,
	IdRegimenContratacion,
	CodigoRegional,
	NombreSolicitante,
	DependenciaSolicitante,
	CargoSolicitante,
	ObjetoContrato,
	AlcanceObjetoContrato,
	ValorInicialContrato,
	ValorTotalAdiciones,
	ValorFinalContrato,
	ValorAportesICBF,
	ValorAportesOperador,
	ValorTotalReduccion,
	JustificacionAdicionSuperior50porc,
	FechaSuscripcion,
	FechaInicioEjecucion,
	FechaFinalizacionInicial,
	PlazoInicial,
	FechaInicialTerminacion,
	FechaFinalTerminacion,
	FechaProyectadaLiquidacion,
	FechaAnulacion,
	Prorrogas,
	PlazoTotal,
	FechaFirmaActaInicio,
	VigenciaFiscalInicial,
	VigenciaFiscalFinal,
	IdUnidadEjecucion,
	IdLugarEjecucion,
	DatosAdicionales,
	IdEstadoContrato,
	IdTipoDocumentoContratista,
	IdentificacionContratista,
	NombreContratista,
	IdFormaPago,
	IdTipoEntidad,
	contr.UsuarioCrea,
	contr.FechaCrea,
	contr.UsuarioModifica,
	contr.FechaModifica,
	ClaseContrato,
	Consecutivo,
	AfectaPlanCompras,
	IdSolicitante,
	IdProducto,
	FechaLiquidacion,
	NumeroDocumentoVigenciaFutura,
	RequiereGarantia,
	modalidad.Nombre as Modalidad,
	categoria.NombreCategoriaContrato as CategoriaContrato,
	tipcontr.NombreTipoContrato as TipoContrato
FROM Contrato.Contrato contr inner join Contrato.TipoContrato tipcontr
on (tipcontr.IdTipoContrato = contr.IdTipoContrato) left join Contrato.ModalidadSeleccion modalidad
on (modalidad.IdModalidad = contr.IdModalidad) left join Contrato.CategoriaContrato categoria
on (categoria.IdCategoriaContrato = contr.IdCategoriaContrato)
WHERE (NumeroProceso = @NumeroProceso OR @NumeroProceso IS NULL)
AND (NumeroContrato = @NumeroContrato OR @NumeroContrato IS NULL)
AND (contr.IdModalidad = @IdModalidad OR @IdModalidad IS NULL)
AND (contr.IdCategoriaContrato = @IdCategoriaContrato OR @IdCategoriaContrato IS NULL)
AND (contr.IdTipoContrato = @IdTipoContrato OR @IdTipoContrato IS NULL)
AND FechaRegistro =
--AND (CONVERT(date, FechaRegistro) = CONVERT(date, @FechaRegistro))
	CASE
		WHEN @FechaRegistro IS NULL THEN FechaRegistro ELSE @FechaRegistro
	END
AND ClaseContrato = ISNULL(@ClaseContrato,ClaseContrato)
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/20/2013 10:09:07 PM
-- Description:	Procedimiento almacenado que consulta un(a) Estado de un Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]
	@EstadoContrato NVARCHAR(128) output, @NumeroContrato NUMERIC(25) 
AS
BEGIN
	DECLARE @EstadoActual NVARCHAR(128)
	SET @EstadoActual = 'DESCONOCIDO'

	-- SUSCRITO
		SELECT @EstadoActual = 'SUSCRITO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND FechaSuscripcion IS NOT NULL

	-- EJECUCION
		SELECT @EstadoActual = 'EJECUCION'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() BETWEEN FechaInicioEjecucion AND FechaFinalTerminacion

		-- SUSPENDIDO
		/*SELECT @EstadoActual = 'SUSPENDIDO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato*/

	-- TERMINADO
		SELECT @EstadoActual = 'TERMINADO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() > FechaFinalTerminacion 

	-- LIQUIDADO
		SELECT @EstadoActual = 'LIQUIDADO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() >= FechaProyectadaLiquidacion 

		-- ANULADO
		/*SELECT @EstadoActual = 'ANULADO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND FechaSuscripcion IS NOT NULL	*/

	-- PERDIDA COMPETENCIA
		SELECT @EstadoActual = 'PERDIDA'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() > FechaFinalTerminacion AND DATEDIFF(m,FechaFinalTerminacion,GETDATE()) >= 30
	

	SELECT @EstadoContrato
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]
	@IdFormapago INT
AS
BEGIN
 SELECT IdFormapago, NombreFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[FormaPago] WHERE  IdFormapago = @IdFormapago
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]
	@IdFormapago INT
AS
BEGIN
	DELETE Contrato.FormaPago WHERE IdFormapago = @IdFormapago
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]
		@IdFormapago INT OUTPUT, 	@NombreFormaPago NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.FormaPago(NombreFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreFormaPago, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdFormapago = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]
		@IdFormapago INT,	@NombreFormaPago NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.FormaPago SET NombreFormaPago = @NombreFormaPago, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdFormapago = @IdFormapago
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPagos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]

	@NombreFormaPago NVARCHAR (50) = NULL,
	@Descripcion NVARCHAR (128) = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdFormapago,
		NombreFormaPago,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[FormaPago]
	WHERE NombreFormaPago  LIKE '%' + 
		CASE
			WHEN @NombreFormaPago IS NULL THEN NombreFormaPago ELSE @NombreFormaPago
		END + '%'
	AND Descripcion  LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreFormaPago

END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]
@IDGarantia INT,
@IDSUCURSAL INT
AS
BEGIN

	SELECT
		[Contrato].[Garantia].IDGarantia,
		[Contrato].[Garantia].IDContrato,
		[Contrato].[Garantia].IDTipoGarantia,
		[Contrato].[Garantia].IDterceroaseguradora,
		[Contrato].[Garantia].IDtercerosucursal,
		[Contrato].[Garantia].IDcontratistaContato,
		[Contrato].[Garantia].IDtipobeneficiario,
		[Contrato].[Garantia].NITICBF,
		[Contrato].[Garantia].DescBeneficiario,
		[Contrato].[Garantia].NumGarantia,
		[Contrato].[Garantia].FechaExpedicion,
		[Contrato].[Garantia].FechaVencInicial,
		[Contrato].[Garantia].FechaRecibo,
		[Contrato].[Garantia].FechaDevolucion,
		[Contrato].[Garantia].MotivoDevolucion,
		[Contrato].[Garantia].FechaAprobacion,
		[Contrato].[Garantia].FechaCertificacionPago,
		[Contrato].[Garantia].Valor,
		[Contrato].[Garantia].Valortotal,
		[Contrato].[Garantia].Estado,
		[Contrato].[Garantia].Anexo,
		[Contrato].[Garantia].ObservacionAnexo,
		[Contrato].[Garantia].UsuarioCrea,
		[Contrato].[Garantia].FechaCrea,
		[Contrato].[Garantia].UsuarioModifica,
		[Contrato].[Garantia].FechaModifica,
		Oferente.TERCERO.RAZONSOCIAL,
		Oferente.TERCERO.PRIMERNOMBRE,
		Oferente.TERCERO.SEGUNDONOMBRE,
		Oferente.TERCERO.PRIMERAPELLIDO,
		Oferente.TERCERO.SEGUNDOAPELLIDO,
		Proveedor.Sucursal.Nombre,
		Proveedor.Sucursal.Direccion,
		Proveedor.Sucursal.Correo,
		Proveedor.Sucursal.Indicativo,
		Proveedor.Sucursal.Telefono,
		Proveedor.Sucursal.Extension,
		Proveedor.Sucursal.Celular,
		DIV.Departamento.NombreDepartamento,
		DIV.Municipio.NombreMunicipio
	FROM	[Contrato].[Garantia],
			Oferente.TERCERO,
			Proveedor.Sucursal,
			Oferente.TERCERO SUCURSALA,
			Proveedor.EntidadProvOferente,
			DIV.Departamento,
			DIV.Municipio
	WHERE [Contrato].[Garantia].IDGarantia = @IDGarantia
	AND Oferente.TERCERO.IDTERCERO = [Contrato].[Garantia].IDterceroaseguradora
	AND SUCURSALA.IDTERCERO = Proveedor.EntidadProvOferente.IdTercero
	AND Proveedor.EntidadProvOferente.IdEntidad = Proveedor.Sucursal.IdEntidad
	AND (Proveedor.Sucursal.IdSucursal = @IDSUCURSAL OR @IDSUCURSAL IS NULL)
	AND Proveedor.Sucursal.Departamento = DIV.Departamento.IdDepartamento
	AND Proveedor.Sucursal.Municipio = DIV.Municipio.IdMunicipio
	END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ares\Henry
-- Create date:  06/08/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que elimina un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]
	@IDGarantia INT
AS
BEGIN
	DELETE Contrato.Garantia 
	WHERE IDGarantia = @IDGarantia
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Insertar]
@IDGarantia INT OUTPUT, 	
@IDContrato INT,	
@IDTipoGarantia INT,	
@IDterceroaseguradora INT,
@IDtercerosucursal INT,
@IDcontratistaContato INT,
@IDtipobeneficiario INT,
@NITICBF NVARCHAR(50),
@DescBeneficiario NVARCHAR(200),
@NumGarantia NVARCHAR(128),	
@FechaExpedicion DATETIME,	
@FechaVencInicial DATETIME,	
@FechaRecibo DATETIME,	
@FechaDevolucion DATETIME,	
@MotivoDevolucion NVARCHAR(128),	
@FechaAprobacion DATETIME,	
@FechaCertificacionPago DATETIME,	
@Estado BIT,	
@Anexo BIT,	
@ObservacionAnexo NVARCHAR(256), 
@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.Garantia(IDContrato, IDTipoGarantia, IDterceroaseguradora, IDtercerosucursal,IDcontratistaContato,IDtipobeneficiario,NITICBF,DescBeneficiario, NumGarantia, FechaExpedicion, FechaVencInicial, FechaRecibo, FechaDevolucion, MotivoDevolucion, FechaAprobacion, FechaCertificacionPago, Estado, Anexo, ObservacionAnexo, UsuarioCrea, FechaCrea)
					  VALUES(@IDContrato, @IDTipoGarantia,@IDterceroaseguradora, @IDtercerosucursal,@IDcontratistaContato,@IDtipobeneficiario,@NITICBF ,@DescBeneficiario, @NumGarantia, @FechaExpedicion, @FechaVencInicial, @FechaRecibo, @FechaDevolucion, @MotivoDevolucion, @FechaAprobacion, @FechaCertificacionPago, @Estado, @Anexo, @ObservacionAnexo, @UsuarioCrea, GETDATE())
	SELECT @IDGarantia = @@IDENTITY 		
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ares\Andrés Morales
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]
@IDGarantia INT, 	
@IDContrato INT,	
@IDTipoGarantia INT,	
@IDterceroaseguradora INT,
@IDtercerosucursal INT,
@IDcontratistaContato INT,
@IDtipobeneficiario INT,
@NITICBF NVARCHAR(50),
@DescBeneficiario NVARCHAR(200),
@NumGarantia NVARCHAR(128),	
@FechaExpedicion DATETIME,	
@FechaVencInicial DATETIME,	
@FechaRecibo DATETIME,	
@FechaDevolucion DATETIME,	
@MotivoDevolucion NVARCHAR(128),	
@FechaAprobacion DATETIME,	
@FechaCertificacionPago DATETIME,	
@Estado BIT,	
@Anexo BIT,	
@ObservacionAnexo NVARCHAR(256), 
@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.Garantia 
	SET IDContrato = @IDContrato, 
	IDTipoGarantia = @IDTipoGarantia, 
	@IDterceroaseguradora = IDterceroaseguradora,
    @IDtercerosucursal = IDtercerosucursal,
    @IDcontratistaContato = IDcontratistaContato,
    @IDtipobeneficiario = IDtipobeneficiario,
    @NITICBF = NITICBF,
    @DescBeneficiario =DescBeneficiario,
	NumGarantia = @NumGarantia, 
	FechaExpedicion = @FechaExpedicion, 
	FechaVencInicial = @FechaVencInicial, 
	FechaRecibo = @FechaRecibo, 
	FechaDevolucion = @FechaDevolucion, 
	MotivoDevolucion = @MotivoDevolucion, 
	FechaAprobacion = @FechaAprobacion, 
	FechaCertificacionPago = @FechaCertificacionPago, 
	Estado = @Estado, 
	Anexo = @Anexo, 
	ObservacionAnexo = @ObservacionAnexo, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IDGarantia = @IDGarantia
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantias_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ares\Jonathan Acosta
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- EXEC [usp_RubOnline_Contrato_Garantias_Consultar]
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantias_Consultar]
@IDGarantia INT = NULL,
@NumGarantia NVARCHAR (128) = NULL,
@NumContrato NUMERIC (25) = NULL,
@Estado bit = null,
@IDterceroaseguradora INT = NULL
AS
BEGIN
SELECT
	[Contrato].[Garantia].IDGarantia,
	[Contrato].[Garantia].IDContrato,
	[Contrato].[Garantia].IDTipoGarantia,
	[Contrato].[Garantia].IDterceroaseguradora,
	[Contrato].[Garantia].IDtercerosucursal,
	[Contrato].[Garantia].IDcontratistaContato,
	[Contrato].[Garantia].IDtipobeneficiario,
	[Contrato].[Garantia].NITICBF,
	[Contrato].[Garantia].DescBeneficiario,
	[Contrato].[Garantia].NumGarantia,
	[Contrato].[Garantia].FechaExpedicion,
	[Contrato].[Garantia].FechaVencInicial,
	[Contrato].[Garantia].FechaRecibo,
	[Contrato].[Garantia].FechaDevolucion,
	[Contrato].[Garantia].MotivoDevolucion,
	[Contrato].[Garantia].FechaAprobacion,
	[Contrato].[Garantia].FechaCertificacionPago,
	[Contrato].[Garantia].Valor,
	[Contrato].[Garantia].Valortotal,
	[Contrato].[Garantia].Estado,
	[Contrato].[Garantia].Anexo,
	[Contrato].[Garantia].ObservacionAnexo,
	[Contrato].[Garantia].UsuarioCrea,
	[Contrato].[Garantia].FechaCrea,
	[Contrato].[Garantia].UsuarioModifica,
	[Contrato].[Garantia].FechaModifica,
	Oferente.TERCERO.RAZONSOCIAL RAZONSOCIAL_ASEGURADORA,
	Oferente.TERCERO.PRIMERNOMBRE,
	Oferente.TERCERO.SEGUNDONOMBRE,
	Oferente.TERCERO.PRIMERAPELLIDO,
	Oferente.TERCERO.SEGUNDOAPELLIDO,
	contrato.contrato.NumeroContrato,
	Contrato.RelacionarContratistas.NumeroIdentificacion,
	CASE CONTRATISTA.IdTipoPersona
		WHEN 2 THEN CONTRATISTA.RAZONSOCIAL
		WHEN 1 THEN CONTRATISTA.PRIMERNOMBRE + ' ' + CONTRATISTA.PRIMERAPELLIDO
	END RAZONSOCIAL_CONTRATISTA,
	Oferente.TERCERO.NUMEROIDENTIFICACION NIT_ASEGURADORA
FROM	[Contrato].[Garantia],
		Oferente.TERCERO,
		Oferente.TERCERO CONTRATISTA,
		contrato.contrato,
		Contrato.RelacionarContratistas
WHERE ([Contrato].[Garantia].IDGarantia = @IDGarantia OR @IDGarantia IS NULL)
AND Oferente.TERCERO.IDTERCERO = [Contrato].[Garantia].IDterceroaseguradora
AND contrato.contrato.IdContrato = [Contrato].[Garantia].IDContrato
AND Contrato.RelacionarContratistas.IdContrato = [Contrato].[Garantia].IDContrato
--AND Contrato.RelacionarContratistas.IdContratistaContrato = [Contrato].[Garantia].IDcontratistaContato
AND CONTRATISTA.NUMEROIDENTIFICACION = Contrato.RelacionarContratistas.NumeroIdentificacion
AND ([Contrato].[Garantia].IDContrato = @NumContrato OR @NumContrato IS NULL)
AND ([Contrato].[Garantia].NumGarantia = @NumGarantia OR @NumGarantia IS NULL)
AND ([Contrato].[Garantia].Estado = @Estado OR @Estado IS NULL)
AND ([Contrato].[Garantia].IDterceroaseguradora = @IDterceroaseguradora OR @IDterceroaseguradora IS NULL)
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantias_Consultar_lupa]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ares\Jonathan Acosta
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantias_Consultar_lupa]
	@IDTipoGarantia INT = NULL,
	@NumGarantia NVARCHAR(128) = NULL,
	@NumContrato nvarchar(50) = NULL,
	@Estado		bit = null
AS
BEGIN
 SELECT IDGarantia, garantia.IDContrato, garantia.IDTipoGarantia, NumGarantia, 
 FechaExpedicion, FechaVencInicial, FechaRecibo, FechaDevolucion, 
 MotivoDevolucion, FechaAprobacion, FechaCertificacionPago, Valor, 
 garantia.Estado, Anexo, ObservacionAnexo, garantia.UsuarioCrea, garantia.FechaCrea, garantia.UsuarioModifica, 
 garantia.FechaModifica, contrato.NumeroContrato,
 tipogaratia.NombreTipoGarantia as TipoGarantia
 FROM [Contrato].[Garantia] as garantia inner join Contrato.Contrato as contrato 
 on (contrato.IdContrato = garantia.IDContrato) INNER JOIN Contrato.TipoGarantia tipogaratia
 on (tipogaratia.IdTipoGarantia = garantia.IDTipoGarantia)
 WHERE garantia.IDTipoGarantia = CASE WHEN @IDTipoGarantia IS NULL THEN garantia.IDTipoGarantia ELSE @IDTipoGarantia END 
 AND NumGarantia = CASE WHEN @NumGarantia IS NULL THEN NumGarantia ELSE @NumGarantia END
 AND contrato.NumeroContrato = CASE WHEN @NumContrato IS NULL THEN NumeroContrato ELSE @NumContrato END
 AND garantia.Estado = CASE WHEN @Estado IS NULL THEN garantia.Estado ELSE @Estado END
END





GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GarantiaSucursal_consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ares\Jonathan Acosta
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta las sucursales de una aseguradora
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GarantiaSucursal_consultar]
@IDTERCERO INT
AS
/*
EXEC DBO.usp_RubOnline_Contrato_GarantiaSucursal_consultar 277
*/
SELECT
	  Proveedor.Sucursal.IdSucursal,
      DIV.Departamento.NombreDepartamento,
      DIV.Municipio.NombreMunicipio,
      Proveedor.Sucursal.Nombre,
      Proveedor.Sucursal.Direccion,
      Proveedor.Sucursal.Correo,
      Proveedor.Sucursal.Indicativo,
      Proveedor.Sucursal.Telefono,
      Proveedor.Sucursal.Extension,
      Proveedor.Sucursal.Celular
FROM Oferente.TERCERO
INNER JOIN Proveedor.EntidadProvOferente
      ON Oferente.TERCERO.IDTERCERO = Proveedor.EntidadProvOferente.IdTercero
INNER JOIN Proveedor.Sucursal
      ON Proveedor.EntidadProvOferente.IdEntidad = Proveedor.Sucursal.IdEntidad
INNER JOIN DIV.Departamento
      ON Proveedor.Sucursal.Departamento = DIV.Departamento.IdDepartamento
INNER JOIN DIV.Municipio
      ON Proveedor.Sucursal.Municipio = DIV.Municipio.IdMunicipio
WHERE Proveedor.EntidadProvOferente.IdTercero = @IDTERCERO
--AND Oferente.TERCERO.RAZONSOCIAL LIKE '%ASEGURADORA%'



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]
	@IdGestionClausula INT
AS
BEGIN
 SELECT IdGestionClausula, IdContrato, NombreClausula, TipoClausula, Orden, OrdenNumero, DescripcionClausula, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarClausulasContrato] WHERE  IdGestionClausula = @IdGestionClausula
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]
	@IdGestionClausula INT
AS
BEGIN
	DELETE Contrato.GestionarClausulasContrato WHERE IdGestionClausula = @IdGestionClausula
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]
		@IdGestionClausula INT OUTPUT, 	@IdContrato NVARCHAR(3),	@NombreClausula NVARCHAR(10),	@TipoClausula INT,	@Orden NVARCHAR(128), @OrdenNumero INT, @DescripcionClausula NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.GestionarClausulasContrato(IdContrato, NombreClausula, TipoClausula, Orden, OrdenNumero, DescripcionClausula, UsuarioCrea, FechaCrea)
				VALUES(@IdContrato, @NombreClausula, @TipoClausula, @Orden,  @OrdenNumero, @DescripcionClausula, @UsuarioCrea, GETDATE())
	SELECT @IdGestionClausula = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]
		@IdGestionClausula INT,	@IdContrato NVARCHAR(3),	@NombreClausula NVARCHAR(10),	@TipoClausula INT,	@Orden NVARCHAR(128), @OrdenNumero INT,	@DescripcionClausula NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.GestionarClausulasContrato SET IdContrato = @IdContrato, NombreClausula = @NombreClausula, TipoClausula = @TipoClausula, Orden = @Orden, OrdenNumero = @OrdenNumero, DescripcionClausula = @DescripcionClausula, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdGestionClausula = @IdGestionClausula
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado consula las clausuas de los contratos
   -- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]
	@IdGestionClausula INT = NULL,
	@IdContrato NVARCHAR(3) = NULL,
	@NombreClausula NVARCHAR(10) = NULL,
	@TipoClausula INT = NULL,
	@Orden NVARCHAR(128) = NULL,
	@Ordennumero NVARCHAR(128) = NULL,
	@DescripcionClausula NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdGestionClausula, IdContrato, NombreClausula, TipoClausula, Orden, DescripcionClausula, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[GestionarClausulasContrato] 
 WHERE 
 IdGestionClausula = 
 CASE WHEN @IdGestionClausula IS NULL THEN IdGestionClausula 
 ELSE @IdGestionClausula END AND 
 IdContrato = 
 CASE WHEN @IdContrato IS NULL THEN IdContrato 
 ELSE @IdContrato END AND 
 NombreClausula = 
 CASE WHEN @NombreClausula IS NULL THEN NombreClausula 
 ELSE @NombreClausula END AND 
 TipoClausula = 
 CASE WHEN @TipoClausula IS NULL THEN TipoClausula 
 ELSE @TipoClausula END AND 
 Orden = 
 CASE WHEN @Orden IS NULL THEN Orden 
 ELSE @Orden END AND 
 DescripcionClausula = 
 CASE WHEN @DescripcionClausula IS NULL THEN DescripcionClausula 
 ELSE @DescripcionClausula END
 AND (Ordennumero = @Ordennumero OR @Ordennumero IS NULL)
 ORDER BY 
 IdContrato,
 [Contrato].[GestionarClausulasContrato].OrdenNumero
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]
	@IdGestionObligacion INT
AS
BEGIN
 SELECT IdGestionObligacion, IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarObligacion] WHERE  IdGestionObligacion = @IdGestionObligacion
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]
	@IdGestionObligacion INT
AS
BEGIN
	DELETE Contrato.GestionarObligacion WHERE IdGestionObligacion = @IdGestionObligacion
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]
		@IdGestionObligacion INT OUTPUT, 	@IdGestionClausula INT,	@NombreObligacion NVARCHAR(10),	@IdTipoObligacion INT,	@Orden NVARCHAR(128),	@DescripcionObligacion NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.GestionarObligacion(IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea)
					  VALUES(@IdGestionClausula, @NombreObligacion, @IdTipoObligacion, @Orden, @DescripcionObligacion, @UsuarioCrea, GETDATE())
	SELECT @IdGestionObligacion = @@IDENTITY 		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]
		@IdGestionObligacion INT,	@IdGestionClausula INT,	@NombreObligacion NVARCHAR(10),	@IdTipoObligacion INT,	@Orden NVARCHAR(128),	@DescripcionObligacion NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.GestionarObligacion SET IdGestionClausula = @IdGestionClausula, NombreObligacion = @NombreObligacion, IdTipoObligacion = @IdTipoObligacion, Orden = @Orden, DescripcionObligacion = @DescripcionObligacion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdGestionObligacion = @IdGestionObligacion
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacions_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]
	@IdGestionObligacion INT = NULL,@IdGestionClausula INT = NULL,@NombreObligacion NVARCHAR(10) = NULL,@IdTipoObligacion INT = NULL,@Orden NVARCHAR(128) = NULL,@DescripcionObligacion NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdGestionObligacion, IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarObligacion] WHERE IdGestionObligacion = CASE WHEN @IdGestionObligacion IS NULL THEN IdGestionObligacion ELSE @IdGestionObligacion END AND IdGestionClausula = CASE WHEN @IdGestionClausula IS NULL THEN IdGestionClausula ELSE @IdGestionClausula END AND NombreObligacion = CASE WHEN @NombreObligacion IS NULL THEN NombreObligacion ELSE @NombreObligacion END AND IdTipoObligacion = CASE WHEN @IdTipoObligacion IS NULL THEN IdTipoObligacion ELSE @IdTipoObligacion END AND Orden = CASE WHEN @Orden IS NULL THEN Orden ELSE @Orden END AND DescripcionObligacion = CASE WHEN @DescripcionObligacion IS NULL THEN DescripcionObligacion ELSE @DescripcionObligacion END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Consultar]
	@IdInformacionPresupuestal INT
AS
BEGIN
 SELECT IdInformacionPresupuestal, NumeroCDP, ValorCDP, FechaExpedicionCDP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestal] WHERE  IdInformacionPresupuestal = @IdInformacionPresupuestal
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que elimina un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]
	@IdInformacionPresupuestal INT
AS
BEGIN
	DELETE Contrato.InformacionPresupuestal WHERE IdInformacionPresupuestal = @IdInformacionPresupuestal
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]
		@IdInformacionPresupuestal INT OUTPUT, 	@NumeroCDP INT,	@ValorCDP NUMERIC(18,3),	@FechaExpedicionCDP DATETIME, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.InformacionPresupuestal(NumeroCDP, ValorCDP, FechaExpedicionCDP, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroCDP, @ValorCDP, @FechaExpedicionCDP, @UsuarioCrea, GETDATE())
	SELECT @IdInformacionPresupuestal = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]
		@IdInformacionPresupuestal INT,	@NumeroCDP INT,	@ValorCDP NUMERIC(18,3),	@FechaExpedicionCDP DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.InformacionPresupuestal SET NumeroCDP = @NumeroCDP, ValorCDP = @ValorCDP, FechaExpedicionCDP = @FechaExpedicionCDP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdInformacionPresupuestal = @IdInformacionPresupuestal
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]
	@IdInformacionPresupuestalRP INT
AS
BEGIN
 SELECT IdInformacionPresupuestalRP, FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestalRP] WHERE  IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que elimina un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]
	@IdInformacionPresupuestalRP INT
AS
BEGIN
	DELETE Contrato.InformacionPresupuestalRP WHERE IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]
		@IdInformacionPresupuestalRP INT OUTPUT, 	@FechaSolicitudRP DATETIME,	@NumeroRP INT,	@ValorRP INT,	@FechaExpedicionRP DATETIME, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.InformacionPresupuestalRP(FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea)
					  VALUES(@FechaSolicitudRP, @NumeroRP, @ValorRP, @FechaExpedicionRP, @UsuarioCrea, GETDATE())
	SELECT @IdInformacionPresupuestalRP = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]
		@IdInformacionPresupuestalRP INT,	@FechaSolicitudRP DATETIME,	@NumeroRP INT,	@ValorRP INT,	@FechaExpedicionRP DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.InformacionPresupuestalRP SET FechaSolicitudRP = @FechaSolicitudRP, NumeroRP = @NumeroRP, ValorRP = @ValorRP, FechaExpedicionRP = @FechaExpedicionRP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]
	@FechaSolicitudRP DATETIME = NULL,@NumeroRP INT = NULL
AS
BEGIN
 SELECT IdInformacionPresupuestalRP, FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestalRP] WHERE FechaSolicitudRP = CASE WHEN @FechaSolicitudRP IS NULL THEN FechaSolicitudRP ELSE @FechaSolicitudRP END AND NumeroRP = CASE WHEN @NumeroRP IS NULL THEN NumeroRP ELSE @NumeroRP END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]
	@NumeroCDP INT = NULL,@FechaExpedicionCDP DATETIME = NULL
AS
BEGIN
 SELECT IdInformacionPresupuestal, NumeroCDP, ValorCDP, FechaExpedicionCDP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestal] WHERE NumeroCDP = CASE WHEN @NumeroCDP IS NULL THEN NumeroCDP ELSE @NumeroCDP END AND FechaExpedicionCDP = CASE WHEN @FechaExpedicionCDP IS NULL THEN FechaExpedicionCDP ELSE @FechaExpedicionCDP END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que elimina un(a) LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
	DELETE Contrato.LugarEjecucion WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que guarda un nuevo LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]
		@IdContratoLugarEjecucion INT, 	
		@IDDepartamento INT,	
		@IDMunicipio INT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.LugarEjecucion(IdContratoLugarEjecucion,IDDepartamento, IDMunicipio, UsuarioCrea, FechaCrea)
	VALUES(@IdContratoLugarEjecucion,@IDDepartamento, @IDMunicipio, @UsuarioCrea, GETDATE())
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
 SELECT 
	IdContratoLugarEjecucion, IDContrato, 
	Nacional, UsuarioCrea, FechaCrea, 
	UsuarioModifica, FechaModifica 
	FROM [Contrato].[LugarEjecucionContrato] 
	WHERE  IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que elimina un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
	DELETE Contrato.LugarEjecucionContrato WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que guarda un nuevo LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]
		@IdContratoLugarEjecucion INT OUTPUT, 	
		@IDContrato INT,	
		@Nacional BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.LugarEjecucionContrato(IDContrato,  Nacional, UsuarioCrea, FechaCrea)
					  VALUES(@IDContrato,  @Nacional, @UsuarioCrea, GETDATE())
	SELECT @IdContratoLugarEjecucion = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que actualiza un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]
		@IdContratoLugarEjecucion INT,	
		@IDContrato INT,	
		@Nacional BIT, 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE 
		Contrato.LugarEjecucionContrato 
	SET IDContrato = @IDContrato, 
		Nacional = @Nacional, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContratos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Cesar Casanova\@Res
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContratos_Consultar]

	@NUMEROCONTRATO NVARCHAR (50) = NULL,
	@IDDEPARTAMENTO INT = NULL,
	@IDMUNICIPIO INT = NULL,
	@NIVELNACIONAL bit = NULL

AS
BEGIN

	SELECT
		LUGAREJECUCIONCTR.IDCONTRATOLUGAREJECUCION,
		LUGAREJECUCIONCTR.IDCONTRATO,
		CONTR.NUMEROCONTRATO,
		NACIONAL,
		LUGAREJECUCION.IDDEPARTAMENTO,
		DEPTO.NOMBREDEPARTAMENTO,
		LUGAREJECUCION.IDMUNICIPIO,
		MUNPIO.NOMBREMUNICIPIO
	FROM [CONTRATO].[LUGAREJECUCIONCONTRATO] LUGAREJECUCIONCTR
	INNER JOIN CONTRATO.CONTRATO CONTR
		ON (CONTR.IDCONTRATO = LUGAREJECUCIONCTR.IDCONTRATO)
	LEFT JOIN CONTRATO.LUGAREJECUCION LUGAREJECUCION
		ON (LUGAREJECUCION.IDCONTRATOLUGAREJECUCION = LUGAREJECUCIONCTR.IDCONTRATOLUGAREJECUCION)
	LEFT JOIN DIV.DEPARTAMENTO DEPTO
		ON (DEPTO.IDDEPARTAMENTO = LUGAREJECUCION.IDDEPARTAMENTO)
	LEFT JOIN DIV.MUNICIPIO MUNPIO
		ON (MUNPIO.IDMUNICIPIO = LUGAREJECUCION.IDMUNICIPIO)
	WHERE (CONTR.NUMEROCONTRATO = @NUMEROCONTRATO OR @NUMEROCONTRATO IS NULL)
	AND (LUGAREJECUCION.IDDEPARTAMENTO = @IDDEPARTAMENTO OR @IDDEPARTAMENTO IS NULL)
	AND (LUGAREJECUCION.IDMUNICIPIO = @IDMUNICIPIO OR @IDMUNICIPIO IS NULL)
	AND (NACIONAL = @NIVELNACIONAL OR @NIVELNACIONAL IS NULL)
	END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]
@IdContratoLugarEjecucion	INT
AS
BEGIN
 SELECT IdContratoLugarEjecucion, IDDepartamento, IDMunicipio, 
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[LugarEjecucion] 
 WHERE 
 IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]
	@IdModalidad INT
AS
BEGIN
 SELECT IdModalidad, Nombre, Sigla, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ModalidadSeleccion] WHERE  IdModalidad = @IdModalidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]
	@IdModalidad INT
AS
BEGIN
	DELETE Contrato.ModalidadSeleccion WHERE IdModalidad = @IdModalidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]
		@IdModalidad INT OUTPUT, 	@Nombre NVARCHAR(128),	@Sigla NVARCHAR(5),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ModalidadSeleccion(Nombre, Sigla, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Nombre, @Sigla, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdModalidad = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]
		@IdModalidad INT,	@Nombre NVARCHAR(128),	@Sigla NVARCHAR(5),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ModalidadSeleccion SET Nombre = @Nombre, Sigla = @Sigla, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdModalidad = @IdModalidad
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:                  @ReS\Cesar Casanova
-- Create date:         15/06/2013 10:29
-- Description:          Procedimiento almacenado que consulta ModalidadSeleccion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]
@Nombre NVARCHAR (128) = NULL, @Sigla NVARCHAR (5) = NULL, @Estado BIT = NULL
AS
BEGIN
SELECT
	IdModalidad,
	Nombre,
	Sigla,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[ModalidadSeleccion]
WHERE Nombre LIKE '%' + 
	CASE
		WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre
	END  + '%'
	AND Sigla LIKE '%' + 
	CASE
		WHEN @Sigla IS NULL THEN Sigla ELSE @Sigla
	END  + '%'
	AND Estado =
	CASE
		WHEN @Estado IS NULL THEN Estado ELSE @Estado
	END
ORDER BY Nombre
		
END





GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContrato_Consultar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]
	@IdObjetoContratoContractual INT
AS
BEGIN
 SELECT IdObjetoContratoContractual, ObjetoContractual, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[ObjetoContrato] 
 WHERE  IdObjetoContratoContractual = @IdObjetoContratoContractual
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContrato_Eliminar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]
	@IdObjetoContratoContractual INT
AS
BEGIN
	DELETE Contrato.ObjetoContrato WHERE IdObjetoContratoContractual = @IdObjetoContratoContractual
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]
		@IdObjetoContratoContractual INT OUTPUT, 	
		@ObjetoContractual nvarchar(MAX),	
		@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	if not EXISTS (SELECT 1 FROM Contrato.ObjetoContrato WHERE ObjetoContractual = @ObjetoContractual)
	BEGIN
	INSERT INTO Contrato.ObjetoContrato(ObjetoContractual, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@ObjetoContractual, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdObjetoContratoContractual = @@IDENTITY 		
	END
	ELSE
	BEGIN
		RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]
		@IdObjetoContratoContractual INT,	
		@ObjetoContractual nvarchar(MAX),	
		@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM Contrato.ObjetoContrato WHERE ObjetoContractual = @ObjetoContractual AND IdObjetoContratoContractual NOT IN (@IdObjetoContratoContractual))
	BEGIN
		UPDATE Contrato.ObjetoContrato SET ObjetoContractual = @ObjetoContractual, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdObjetoContratoContractual = @IdObjetoContratoContractual
	END
	ELSE
	BEGIN
		RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]
@IdObjetoContratoContractual INT = NULL, 
@ObjetoContractual nvarchar(MAX) = NULL, 
@Estado BIT = NULL
AS
BEGIN

SELECT
	IdObjetoContratoContractual,
	ObjetoContractual,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[ObjetoContrato]
WHERE IdObjetoContratoContractual =
	CASE
		WHEN @IdObjetoContratoContractual IS NULL THEN IdObjetoContratoContractual ELSE @IdObjetoContratoContractual
	END 
	AND ObjetoContractual LIKE '%' + 
	CASE
		WHEN @ObjetoContractual IS NULL THEN ObjetoContractual ELSE @ObjetoContractual
	END   + '%'
	AND Estado =
	CASE
		WHEN @Estado IS NULL THEN Estado ELSE @Estado
	END
	
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_Obligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]
	@IdObligacion INT
AS
BEGIN
 SELECT IdObligacion, IdTipoObligacion, IdTipoContrato, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[Obligacion] WHERE  IdObligacion = @IdObligacion
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_Obligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]
	@IdObligacion INT
AS
BEGIN
	DELETE Contrato.Obligacion WHERE IdObligacion = @IdObligacion
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que inserta Obligacion
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]
		@IdObligacion INT OUTPUT, 	
		@IdTipoObligacion INT,	
		@IdTipoContrato INT,	
		@Descripcion nvarchar(MAX),	
		@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.Obligacion(IdTipoObligacion, IdTipoContrato, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoObligacion, @IdTipoContrato, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdObligacion = @@IDENTITY 		
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que actualiza Obligacion
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]
		@IdObligacion INT,	@IdTipoObligacion INT,	@IdTipoContrato INT,	
		@Descripcion nvarchar(MAX),	
		@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.Obligacion SET IdTipoObligacion = @IdTipoObligacion, IdTipoContrato = @IdTipoContrato, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdObligacion = @IdObligacion
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:                  @ReS\Cesar Casanova
-- Create date:         15/06/2013 08:10
-- Description:          Procedimiento almacenado que consulta Obligacion
-- Modificado por Jonathan Acosta - Consulta por estado del contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]
@Descripcion nvarchar(MAX) = NULL, 
@IdTipoObligacion INT = NULL, 
@IdTipoContrato INT = NULL, @Estado BIT = NULL
AS
BEGIN

SELECT
	[Contrato].[Obligacion].IdTipoContrato,
	[Contrato].[Obligacion].IdObligacion,
	[Contrato].[Obligacion].IdTipoObligacion,
	[Contrato].[Obligacion].Descripcion,
	[Contrato].[Obligacion].Estado,
	[Contrato].[Obligacion].UsuarioCrea,
	[Contrato].[Obligacion].FechaCrea,
	[Contrato].[Obligacion].UsuarioModifica,
	[Contrato].[Obligacion].FechaModifica
FROM	[Contrato].[Obligacion],
		[Contrato].[TipoContrato]
WHERE [Contrato].[Obligacion].IdTipoObligacion =
	CASE
		WHEN @IdTipoObligacion IS NULL THEN [Contrato].[Obligacion].IdTipoObligacion ELSE @IdTipoObligacion
	END
AND [Contrato].[Obligacion].Descripcion LIKE '%' + 
	CASE
		WHEN @Descripcion IS NULL THEN [Contrato].[Obligacion].Descripcion ELSE @Descripcion
	END  + '%'
AND [Contrato].[Obligacion].IdTipoContrato =
	CASE
		WHEN @IdTipoContrato IS NULL THEN [Contrato].[Obligacion].IdTipoContrato ELSE @IdTipoContrato
	END
AND ([Contrato].[Obligacion].Estado = @Estado OR @Estado IS NULL)
AND [Contrato].[Obligacion].IdTipoContrato = [Contrato].[TipoContrato].IdTipoContrato

END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]
	@IdRegimenContratacion INT
AS
BEGIN
 SELECT IdRegimenContratacion, NombreRegimenContratacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[RegimenContratacion] WHERE  IdRegimenContratacion = @IdRegimenContratacion
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]
	@IdRegimenContratacion INT
AS
BEGIN
	DELETE Contrato.RegimenContratacion WHERE IdRegimenContratacion = @IdRegimenContratacion
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]
		@IdRegimenContratacion INT OUTPUT, 	@NombreRegimenContratacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.RegimenContratacion(NombreRegimenContratacion, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreRegimenContratacion, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdRegimenContratacion = @@IDENTITY 		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]
		@IdRegimenContratacion INT,	@NombreRegimenContratacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.RegimenContratacion SET NombreRegimenContratacion = @NombreRegimenContratacion, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRegimenContratacion = @IdRegimenContratacion
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacions_Consultar
Modificado por Jonathan Acosta
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]

@NombreRegimenContratacion NVARCHAR (128) = NULL,
@Descripcion NVARCHAR (128) = NULL,
@Estado bit = null

AS
BEGIN

	SELECT
		IdRegimenContratacion,
		NombreRegimenContratacion,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[RegimenContratacion]
	WHERE NombreRegimenContratacion LIKE '%' + 
		CASE
			WHEN @NombreRegimenContratacion IS NULL THEN NombreRegimenContratacion ELSE @NombreRegimenContratacion
		END + '%'
	AND Descripcion LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreRegimenContratacion

END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]
@IdContratistaContrato INT
AS
BEGIN
SELECT
	IdContratistaContrato,
	IdContrato,
	NumeroIdentificacion,
	ClaseEntidad,
	PorcentajeParticipacion,
	EstadoIntegrante,
	NumeroIdentificacionRepresentanteLegal,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[RelacionarContratistas]
WHERE IdContratistaContrato = @IdContratistaContrato
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Eliminar]
@IdContratistaContrato INT
AS
BEGIN
DELETE Contrato.RelacionarContratistas
WHERE IdContratistaContrato = @IdContratistaContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Insertar]
		@IdContratistaContrato INT OUTPUT, 	@IdContrato INT,	@NumeroIdentificacion BIGINT,	@ClaseEntidad NVARCHAR,	@PorcentajeParticipacion INT,	@NumeroIdentificacionRepresentanteLegal BIGINT, @EstadoIntegrante bit, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	DECLARE @VALIDAPORCENTAJE INT
	IF  EXISTS (SELECT IdContrato FROM Contrato.RelacionarContratistas WHERE IdContrato = @IdContrato)
	BEGIN
		SET @VALIDAPORCENTAJE = (SELECT SUM(PorcentajeParticipacion)+@PorcentajeParticipacion  FROM Contrato.RelacionarContratistas
		WHERE IdContrato = @IdContrato)
		IF  @VALIDAPORCENTAJE <= 100 
		BEGIN 
			INSERT INTO Contrato.RelacionarContratistas(IdContrato, NumeroIdentificacion, ClaseEntidad, PorcentajeParticipacion, NumeroIdentificacionRepresentanteLegal, EstadoIntegrante,  UsuarioCrea, FechaCrea)
			VALUES(@IdContrato, @NumeroIdentificacion, @ClaseEntidad, @PorcentajeParticipacion, @NumeroIdentificacionRepresentanteLegal, @EstadoIntegrante,  @UsuarioCrea, GETDATE())
		END
		ELSE
		BEGIN
			RAISERROR ('ERROR',16,1);
			RETURN
		END
	END
	ELSE
	BEGIN	 
		INSERT INTO Contrato.RelacionarContratistas(IdContrato, NumeroIdentificacion, ClaseEntidad, PorcentajeParticipacion, NumeroIdentificacionRepresentanteLegal, EstadoIntegrante,  UsuarioCrea, FechaCrea)
		VALUES(@IdContrato, @NumeroIdentificacion, @ClaseEntidad, @PorcentajeParticipacion, @NumeroIdentificacionRepresentanteLegal, @EstadoIntegrante,  @UsuarioCrea, GETDATE())
	END 
	SELECT @IdContratistaContrato = @@IDENTITY
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]
		@IdContratistaContrato INT,	
		@IdContrato INT,	
		@NumeroIdentificacion BIGINT,	
		@ClaseEntidad NVARCHAR,	
		@PorcentajeParticipacion INT,	
		@NumeroIdentificacionRepresentanteLegal BIGINT, 
		@EstadoIntegrante bit, 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN

	UPDATE 
	Contrato.RelacionarContratistas 
	SET 
	IdContrato = @IdContrato, 
	NumeroIdentificacion = @NumeroIdentificacion, 
	ClaseEntidad = @ClaseEntidad, 
	PorcentajeParticipacion = @PorcentajeParticipacion, 
	NumeroIdentificacionRepresentanteLegal = @NumeroIdentificacionRepresentanteLegal, 
	EstadoIntegrante = @EstadoIntegrante, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE 
	IdContratistaContrato = @IdContratistaContrato
	
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistass_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistass_Consultar]

	@IdContratistaContrato INT = NULL,
	@IdContrato INT = NULL,
	@NumeroIdentificacion BIGINT = NULL,
	@ClaseEntidad NVARCHAR = NULL,
	@PorcentajeParticipacion INT = NULL,
	@NumeroIdentificacionRepresentanteLegal BIGINT = NULL,
	@EstadoIntegrante Bit = Null

AS
BEGIN

	SELECT
		IdContratistaContrato,
		IdContrato,
		NumeroIdentificacion,
		ClaseEntidad,
		PorcentajeParticipacion,
		NumeroIdentificacionRepresentanteLegal,
		EstadoIntegrante,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[RelacionarContratistas]
	WHERE IdContratistaContrato =
		CASE
			WHEN @IdContratistaContrato IS NULL THEN IdContratistaContrato ELSE @IdContratistaContrato
		END AND IdContrato =
		CASE
			WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato
		END AND NumeroIdentificacion =
		CASE
			WHEN @NumeroIdentificacion IS NULL THEN NumeroIdentificacion ELSE @NumeroIdentificacion
		END AND ClaseEntidad =
		CASE
			WHEN @ClaseEntidad IS NULL THEN ClaseEntidad ELSE @ClaseEntidad
		END AND PorcentajeParticipacion =
		CASE
			WHEN @PorcentajeParticipacion IS NULL THEN PorcentajeParticipacion ELSE @PorcentajeParticipacion
		END AND NumeroIdentificacionRepresentanteLegal =
		CASE
			WHEN @NumeroIdentificacionRepresentanteLegal IS NULL THEN NumeroIdentificacionRepresentanteLegal ELSE @NumeroIdentificacionRepresentanteLegal
		END
	END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]
	@IDSupervisorInterv INT
AS
BEGIN
 SELECT IDSupervisorInterv, IDContratoSupervisa, OrigenTipoSupervisor, 
 IDTipoSupvInterventor, IDTerceroExterno, IDFuncionarioInterno, 
 IDContratoInterventoria, TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, 
 FechaCrea, UsuarioCrea, FechaModifica, UsuarioModifica, IDTerceroInterventoria
 FROM [Contrato].[SupervisorIntervContrato] 
 WHERE  IDSupervisorInterv = @IDSupervisorInterv
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]
	@IDSupervisorInterv INT
AS
BEGIN
	DELETE Contrato.SupervisorIntervContrato 
	WHERE IDSupervisorInterv = @IDSupervisorInterv
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]
		@IDSupervisorInterv INT OUTPUT, 	
		@IDContratoSupervisa INT,	
		@OrigenTipoSupervisor bit,	
		@IDTipoSupvInterventor INT,	
		@IDTerceroExterno INT,	
		@IDFuncionarioInterno INT,	
		@IDContratoInterventoria INT,
		@IDTerceroInterventoria	int,
		@TipoVinculacion bit,	
		@FechaInicia DATETIME,	
		@FechaFinaliza DATETIME,	
		@Estado bit,	
		@FechaModificaInterventor DATETIME,	
		@UsuarioCrea nvarchar(128),
		@UsuarioModifica nvarchar(128)
AS
BEGIN
	INSERT INTO Contrato.SupervisorIntervContrato(IDContratoSupervisa, OrigenTipoSupervisor, IDTipoSupvInterventor, IDTerceroExterno, IDFuncionarioInterno, IDContratoInterventoria, TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, UsuarioCrea, FechaCrea, IDTerceroInterventoria,UsuarioModifica, FechaModifica)
	VALUES(@IDContratoSupervisa, @OrigenTipoSupervisor, @IDTipoSupvInterventor, @IDTerceroExterno, @IDFuncionarioInterno, @IDContratoInterventoria, @TipoVinculacion, @FechaInicia, @FechaFinaliza, @Estado, @FechaModificaInterventor, @UsuarioCrea, GETDATE(), @IDTerceroInterventoria,@UsuarioModifica,GETDATE())
	SELECT @IDSupervisorInterv = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que actualiza un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]
@IDSupervisorInterv INT, 	
@IDContratoSupervisa INT,	
@OrigenTipoSupervisor bit,	
@IDTipoSupvInterventor INT,	
@IDTerceroExterno INT,	
@IDFuncionarioInterno INT,	
@IDContratoInterventoria INT,
@IDTerceroInterventoria int,	
@TipoVinculacion bit,	
@FechaInicia DATETIME,	
@FechaFinaliza DATETIME,	
@Estado bit,	
@FechaModificaInterventor DATETIME,	
@UsuarioModifica nvarchar(128)
AS
BEGIN
	UPDATE Contrato.SupervisorIntervContrato 
	SET IDContratoSupervisa = @IDContratoSupervisa, 
	OrigenTipoSupervisor = @OrigenTipoSupervisor, 
	IDTipoSupvInterventor = @IDTipoSupvInterventor, 
	IDTerceroExterno = @IDTerceroExterno, 
	IDFuncionarioInterno = @IDFuncionarioInterno, 
	IDContratoInterventoria = @IDContratoInterventoria,
	IDTerceroInterventoria = @IDTerceroInterventoria,
	TipoVinculacion = @TipoVinculacion, 
	FechaInicia = @FechaInicia, 
	FechaFinaliza = @FechaFinaliza, 
	Estado = @Estado, 
	FechaModificaInterventor = @FechaModificaInterventor, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IDSupervisorInterv = @IDSupervisorInterv
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]
	@OrigenTipoSupervisor bit = NULL,
	@IDTipoSupvInterventor INT = NULL,
	@IDTerceroExterno INT = NULL,
	@TipoVinculacion bit = NULL,
	@FechaInicia DATETIME = NULL,
	@FechaFinaliza DATETIME = NULL,
	@Estado bit = NULL,
	@FechaModificaInterventor DATETIME = NULL
AS
BEGIN
 SELECT IDSupervisorInterv, IDContratoSupervisa, OrigenTipoSupervisor, IDTipoSupvInterventor, 
 IDTerceroExterno, IDFuncionarioInterno, IDContratoInterventoria, 
 TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, 
 FechaCrea, UsuarioCrea, FechaModifica, UsuarioModifica,IDTerceroInterventoria 
 FROM [Contrato].[SupervisorIntervContrato] 
 WHERE OrigenTipoSupervisor = CASE WHEN @OrigenTipoSupervisor IS NULL THEN OrigenTipoSupervisor ELSE @OrigenTipoSupervisor END AND IDTipoSupvInterventor = CASE WHEN @IDTipoSupvInterventor IS NULL THEN IDTipoSupvInterventor ELSE @IDTipoSupvInterventor END AND IDTerceroExterno = CASE WHEN @IDTerceroExterno IS NULL THEN IDTerceroExterno ELSE @IDTerceroExterno END AND TipoVinculacion = CASE WHEN @TipoVinculacion IS NULL THEN TipoVinculacion ELSE @TipoVinculacion END AND FechaInicia = CASE WHEN @FechaInicia IS NULL THEN FechaInicia ELSE @FechaInicia END AND FechaFinaliza = CASE WHEN @FechaFinaliza IS NULL THEN FechaFinaliza ELSE @FechaFinaliza END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END AND FechaModificaInterventor = CASE WHEN @FechaModificaInterventor IS NULL THEN FechaModificaInterventor ELSE @FechaModificaInterventor END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]
	@NumeroContrato NVARCHAR(11) output, @CodigoRegional INT ,@AnoVigencia INT, @UsuarioCrea NVARCHAR(250) 
AS
BEGIN
	DECLARE @Consecutivo INT
	/*Validar esta creada un consecutivo por CodigoRegional y AnoVigencia*/
	if not exists(SELECT * FROM [Contrato].[SecuenciaNumeroContrato] WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia)
	BEGIN
			SET @Consecutivo = 1	
	END
	ELSE
	BEGIN
			SELECT @Consecutivo = MAX(Consecutivo) + 1 
			FROM [Contrato].[SecuenciaNumeroContrato] 
			WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia
	END
	/*Registra el la secuencia*/
	INSERT INTO Contrato.SecuenciaNumeroContrato(Consecutivo, CodigoRegional, AnoVigencia, UsuarioCrea, FechaCrea)
					  VALUES(@Consecutivo, @CodigoRegional, @AnoVigencia, @UsuarioCrea, GETDATE())
	/*
		Armar numero contrato
	*/
	SELECT @NumeroContrato = right( '00' + cast( @CodigoRegional AS varchar(2)), 2 ) + right( '00000' + cast( @Consecutivo AS varchar(5)), 5 ) + cast( @AnoVigencia AS varchar(4))  

	print @NumeroContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/20/2013 10:09:07 PM
-- Description:	Procedimiento almacenado que consulta un(a) SecuenciaNumeroProceso
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]
	@NumeroProceso NVARCHAR(25) output, @CodigoRegional INT ,@Sigla nvarchar(3), @AnoVigencia INT, @UsuarioCrea NVARCHAR(250) 
AS
BEGIN
	DECLARE @Consecutivo INT
	DECLARE @AnoVigenciaTransformado NVARCHAR(2)
	DECLARE @DelimitadorSecuencia NVARCHAR(1)

	SET @DelimitadorSecuencia = '.'
	/*Validar esta creada un consecutivo por CodigoRegional y AnoVigencia*/
	if not exists(SELECT * FROM [Contrato].[SecuenciaNumeroProceso] WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia)
	BEGIN
			SET @Consecutivo = 1	
	END
	ELSE
	BEGIN
			SELECT @Consecutivo = MAX(Consecutivo) + 1 
			FROM [Contrato].[SecuenciaNumeroProceso] 
			WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia
	END
	
	/*
		Armar numero contrato
	*/
		
	SET @AnoVigenciaTransformado = CASE WHEN LEN(CONVERT(NVARCHAR(4),@AnoVigencia)) = 4 THEN SUBSTRING(CONVERT(NVARCHAR(4),@AnoVigencia),3,2) ELSE CONVERT(NVARCHAR(4),@AnoVigencia) END
	
	SELECT @NumeroProceso = right( '00' + cast( @CodigoRegional AS varchar(2)), 2 ) + @DelimitadorSecuencia + @Sigla + @DelimitadorSecuencia + @AnoVigenciaTransformado + @DelimitadorSecuencia + right( '00000' + cast( @Consecutivo AS varchar(5)), 5 )   

	print @NumeroProceso

	/*Registra el la secuencia*/
	 IF @NumeroProceso IS NOT NULL
	 BEGIN
		INSERT INTO Contrato.SecuenciaNumeroProceso(Consecutivo, CodigoRegional, AnoVigencia, UsuarioCrea, FechaCrea)
		VALUES(@Consecutivo, @CodigoRegional, @AnoVigencia, @UsuarioCrea, GETDATE())
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que consulta un(a) TablaParametrica
-- =============================================

create PROCEDURE [dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]
	@CodigoTablaParametrica NVARCHAR(128) = NULL,
	@NombreTablaParametrica NVARCHAR(128) = NULL,
	@Estado					BIT = NULL
AS
BEGIN
	SELECT
		IdTablaParametrica,
		CodigoTablaParametrica,
		NombreTablaParametrica,
		Url,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TablaParametrica]
	WHERE CodigoTablaParametrica = CASE WHEN @CodigoTablaParametrica IS NULL 
										THEN CodigoTablaParametrica 
										ELSE @CodigoTablaParametrica
								   END
	AND NombreTablaParametrica LIKE '%' + CASE WHEN @NombreTablaParametrica IS NULL 
											   THEN NombreTablaParametrica 
											   ELSE @NombreTablaParametrica
										  END + '%'
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado
		END
	ORDER BY NombreTablaParametrica
END--FIN PP


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]
	@IdTipoAmparo INT
AS
BEGIN
 SELECT IdTipoAmparo, NombreTipoAmparo, IdTipoGarantia, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoAmparo] WHERE  IdTipoAmparo = @IdTipoAmparo
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]
	@IdTipoAmparo INT
AS
BEGIN
	DELETE Contrato.TipoAmparo WHERE IdTipoAmparo = @IdTipoAmparo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]
		@IdTipoAmparo INT OUTPUT, 	
		@NombreTipoAmparo NVARCHAR(128),	@IdTipoGarantia INT,	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoAmparo(NombreTipoAmparo, IdTipoGarantia, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoAmparo, @IdTipoGarantia, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoAmparo = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]
		@IdTipoAmparo INT,	
		@NombreTipoAmparo NVARCHAR(128),	
		@IdTipoGarantia INT,	@Estado BIT, 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoAmparo SET NombreTipoAmparo = @NombreTipoAmparo, IdTipoGarantia = @IdTipoGarantia, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoAmparo = @IdTipoAmparo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Consultar los tipos de amparos
Modificado por Jonathan Acosta
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]

	@NombreTipoAmparo NVARCHAR (128) = NULL, @IdTipoGarantia INT = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdTipoAmparo,
		NombreTipoAmparo,
		IdTipoGarantia,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoAmparo]
	WHERE NombreTipoAmparo LIKE '%' + 
		CASE
			WHEN @NombreTipoAmparo IS NULL THEN NombreTipoAmparo ELSE @NombreTipoAmparo
		END  + '%'
		AND IdTipoGarantia =
		CASE
			WHEN @IdTipoGarantia IS NULL THEN IdTipoGarantia ELSE @IdTipoGarantia
		END
	AND (Estado = @Estado OR @Estado IS NULL)

END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]
	@IdTipoClausula INT
AS
BEGIN
 SELECT IdTipoClausula, NombreTipoClausula, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoClausula] WHERE  IdTipoClausula = @IdTipoClausula
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]
	@IdTipoClausula INT
AS
BEGIN
	DELETE Contrato.TipoClausula WHERE IdTipoClausula = @IdTipoClausula
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]
		@IdTipoClausula INT OUTPUT, 	@NombreTipoClausula NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoClausula(NombreTipoClausula, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoClausula, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoClausula = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]
		@IdTipoClausula INT,	@NombreTipoClausula NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoClausula SET NombreTipoClausula = @NombreTipoClausula, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoClausula = @IdTipoClausula
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausulas_Consultar
SE ADICIONA EL ORDEN POR NOMBRE TIPO CLAUSULA
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]

	@NombreTipoClausula NVARCHAR (50) = NULL,
	@Descripcion NVARCHAR (128) = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdTipoClausula,
		NombreTipoClausula,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoClausula]
	WHERE NombreTipoClausula LIKE '%' + 
		CASE
			WHEN @NombreTipoClausula IS NULL THEN NombreTipoClausula ELSE @NombreTipoClausula
		END + '%'
	AND Descripcion LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreTipoClausula

END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]
	@IdTipoContrato INT
AS
BEGIN
 SELECT IdTipoContrato, NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion, RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoContrato] WHERE  IdTipoContrato = @IdTipoContrato
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]
	@IdTipoContrato INT
AS
BEGIN
	DELETE Contrato.TipoContrato WHERE IdTipoContrato = @IdTipoContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]
		@IdTipoContrato INT OUTPUT, 	@NombreTipoContrato NVARCHAR(128),	@IdCategoriaContrato INT,	@ActaInicio BIT,	@AporteCofinaciacion BIT,	@RecursoFinanciero BIT,	@RegimenContrato INT,	@DescripcionTipoContrato NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoContrato(NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion, RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoContrato, @IdCategoriaContrato, @ActaInicio, @AporteCofinaciacion, @RecursoFinanciero, @RegimenContrato, @DescripcionTipoContrato, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoContrato = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]
		@IdTipoContrato INT,	@NombreTipoContrato NVARCHAR(128),	@IdCategoriaContrato INT,	@ActaInicio BIT,	@AporteCofinaciacion BIT,	@RecursoFinanciero BIT,	@RegimenContrato INT,	@DescripcionTipoContrato NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoContrato SET NombreTipoContrato = @NombreTipoContrato, IdCategoriaContrato = @IdCategoriaContrato, ActaInicio = @ActaInicio, AporteCofinaciacion = @AporteCofinaciacion, RecursoFinanciero = @RecursoFinanciero, RegimenContrato = @RegimenContrato, DescripcionTipoContrato = @DescripcionTipoContrato, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoContrato = @IdTipoContrato
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:                  @ReS\Cesar Casanova
-- Create date:         09/008/2013 10:01
-- Description:          Procedimiento almacenado que consulta los tipos de contratos
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]
@NombreTipoContrato NVARCHAR (128) = NULL,
@IdCategoriaContrato INT = NULL,
@Estado BIT = NULL,
@ACTAINICIO BIT = NULL,
@APORTECOFINANCIACION BIT = NULL,
@RECURSOFINANCIERO BIT = NULL,
@REGIMENCONTRATO INT = NULL,
@DESCRIPCIONTIPOCONTRATO NVARCHAR (128) = NULL
AS
BEGIN

	SELECT
		IdTipoContrato,
		NombreTipoContrato,
		IdCategoriaContrato,
		ActaInicio,
		AporteCofinaciacion,
		RecursoFinanciero,
		RegimenContrato,
		DescripcionTipoContrato,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoContrato]
	WHERE NombreTipoContrato LIKE '%' + 
		CASE
			WHEN @NombreTipoContrato IS NULL THEN NombreTipoContrato ELSE @NombreTipoContrato
		END + '%'
	AND IdCategoriaContrato =
		CASE
			WHEN @IdCategoriaContrato IS NULL THEN IdCategoriaContrato ELSE @IdCategoriaContrato
		END
	AND (Estado = @Estado OR @Estado IS NULL)
	AND (ACTAINICIO = @ACTAINICIO OR @ACTAINICIO IS NULL)
	AND (APORTECOFINACIACION = @APORTECOFINANCIACION OR @APORTECOFINANCIACION IS NULL)
	AND (RECURSOFINANCIERO = @RECURSOFINANCIERO OR @RECURSOFINANCIERO IS NULL)
	AND (REGIMENCONTRATO = @REGIMENCONTRATO OR @REGIMENCONTRATO IS NULL)
	AND (DESCRIPCIONTIPOCONTRATO LIKE '%' +  @DESCRIPCIONTIPOCONTRATO + '%' OR @DESCRIPCIONTIPOCONTRATO IS NULL)
	ORDER BY NombreTipoContrato

END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]
	@IdTipoGarantia INT
AS
BEGIN
 SELECT IdTipoGarantia, NombreTipoGarantia, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoGarantia] WHERE  IdTipoGarantia = @IdTipoGarantia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]
	@IdTipoGarantia INT
AS
BEGIN
	DELETE Contrato.TipoGarantia WHERE IdTipoGarantia = @IdTipoGarantia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]
		@IdTipoGarantia INT OUTPUT, 	@NombreTipoGarantia NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoGarantia(NombreTipoGarantia, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoGarantia, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoGarantia = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Modificar]
		@IdTipoGarantia INT,	@NombreTipoGarantia NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoGarantia SET NombreTipoGarantia = @NombreTipoGarantia, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoGarantia = @IdTipoGarantia
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantias_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]
@NombreTipoGarantia NVARCHAR (128) = NULL,
@Estado bit = null
AS
BEGIN
SELECT
	IdTipoGarantia,
	NombreTipoGarantia,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[TipoGarantia]
WHERE NombreTipoGarantia LIKE '%' + 
	CASE
		WHEN @NombreTipoGarantia IS NULL THEN NombreTipoGarantia ELSE @NombreTipoGarantia
	END + '%'
AND (Estado = @Estado OR @Estado IS NULL)
ORDER BY NombreTipoGarantia
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]
	@IdTipoObligacion INT
AS
BEGIN
 SELECT IdTipoObligacion, NombreTipoObligacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoObligacion] WHERE  IdTipoObligacion = @IdTipoObligacion
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]
	@IdTipoObligacion INT
AS
BEGIN
	DELETE Contrato.TipoObligacion WHERE IdTipoObligacion = @IdTipoObligacion
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]
		@IdTipoObligacion INT OUTPUT, 	@NombreTipoObligacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoObligacion(NombreTipoObligacion, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoObligacion, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoObligacion = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Modificar]
		@IdTipoObligacion INT,	@NombreTipoObligacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoObligacion SET NombreTipoObligacion = @NombreTipoObligacion, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoObligacion = @IdTipoObligacion
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacions_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]
@NombreTipoObligacion NVARCHAR (128) = NULL,
@Descripcion NVARCHAR (128) = NULL,
@Estado bit = null
AS
BEGIN

	SELECT
		IdTipoObligacion,
		NombreTipoObligacion,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoObligacion]
	WHERE NombreTipoObligacion LIKE '%' + 
		CASE
			WHEN @NombreTipoObligacion IS NULL THEN NombreTipoObligacion ELSE @NombreTipoObligacion
		END + '%'
	AND Descripcion LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreTipoObligacion

END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]
	@IdTipoSupvInterventor INT
AS
BEGIN
 SELECT IdTipoSupvInterventor, Nombre, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoSupvInterventor] WHERE  IdTipoSupvInterventor = @IdTipoSupvInterventor
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]
	@IdTipoSupvInterventor INT
AS
BEGIN
	DELETE Contrato.TipoSupvInterventor WHERE IdTipoSupvInterventor = @IdTipoSupvInterventor
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]
		@IdTipoSupvInterventor INT OUTPUT, 	@Nombre NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoSupvInterventor(Nombre, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Nombre, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoSupvInterventor = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]
		@IdTipoSupvInterventor INT,	@Nombre NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoSupvInterventor SET Nombre = @Nombre, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoSupvInterventor = @IdTipoSupvInterventor
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]

	@Nombre NVARCHAR (128) = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdTipoSupvInterventor,
		Nombre,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoSupvInterventor]
	WHERE Nombre LIKE '%' + 
		CASE
			WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)

END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]
	@IdNumeroContrato INT
AS
BEGIN
 SELECT IdNumeroContrato,NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[UnidadMedida] WHERE  IdNumeroContrato = @IdNumeroContrato
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]
	@IdNumeroContrato INT
AS
BEGIN
	DELETE Contrato.UnidadMedida WHERE IdNumeroContrato = @IdNumeroContrato
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]
		@IdNumeroContrato INT OUTPUT, 
		@NumeroContrato	NVARCHAR(50),
		@FechaInicioEjecuciónContrato DATETIME,	
		@FechaTerminacionInicialContrato DATETIME,	
		@FechaFinalTerminacionContrato DATETIME, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.UnidadMedida(NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroContrato, @FechaInicioEjecuciónContrato, @FechaTerminacionInicialContrato, @FechaFinalTerminacionContrato, @UsuarioCrea, GETDATE())
	SELECT @IdNumeroContrato = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]
		@IdNumeroContrato INT,	
		@NumeroContrato NVARCHAR(50),
		@FechaInicioEjecuciónContrato DATETIME,	@FechaTerminacionInicialContrato DATETIME,	@FechaFinalTerminacionContrato DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.UnidadMedida 
	SET NumeroContrato = @NumeroContrato,
	FechaInicioEjecuciónContrato = @FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato = @FechaTerminacionInicialContrato, FechaFinalTerminacionContrato = @FechaFinalTerminacionContrato, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNumeroContrato = @IdNumeroContrato
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedidas_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]
	@IdNumeroContrato INT = NULL,
	@NumeroContrato NVARCHAR(50) = NULL,
	@FechaInicioEjecuciónContrato DATETIME = NULL,
	@FechaTerminacionInicialContrato DATETIME = NULL,
	@FechaFinalTerminacionContrato DATETIME = NULL
AS
BEGIN
 SELECT IdNumeroContrato,NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[UnidadMedida] 
 WHERE 
 IdNumeroContrato = CASE WHEN @IdNumeroContrato IS NULL THEN IdNumeroContrato 
 ELSE  @IdNumeroContrato END 
 AND FechaInicioEjecuciónContrato = CASE WHEN @FechaInicioEjecuciónContrato IS NULL THEN FechaInicioEjecuciónContrato ELSE @FechaInicioEjecuciónContrato END 
 AND FechaTerminacionInicialContrato = CASE WHEN @FechaTerminacionInicialContrato IS NULL THEN FechaTerminacionInicialContrato ELSE @FechaTerminacionInicialContrato END 
 AND FechaFinalTerminacionContrato = CASE  WHEN @FechaFinalTerminacionContrato IS NULL THEN FechaFinalTerminacionContrato ELSE @FechaFinalTerminacionContrato END
 AND NumeroContrato = CASE WHEN @NumeroContrato = '' THEN NumeroContrato ELSE  @NumeroContrato END 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contratos_Valor_Aportes]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ares\Andrés Morales
-- Create date:  10/08/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que calcula el valor total de los aportes
--EXEC dbo.[usp_RubOnline_Contratos_Valor_Aportes] 19,'Contratista'
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Contratos_Valor_Aportes]

	@IDCONTRATO int,
	@TIPOAPORTANTE NVARCHAR(11)

AS
BEGIN

	DECLARE
	@VALOR NUMERIC(18,0),
	@VALORICBF NUMERIC(18,0)

	SELECT  @VALOR = SUM(Contrato.AporteContrato.ValorAporte)
	FROM 
	Contrato.AporteContrato
	where
	Contrato.AporteContrato.IdContrato = @IDCONTRATO
	AND Contrato.AporteContrato.TipoAportante = @TIPOAPORTANTE

	UPDATE Contrato.Contrato SET Contrato.Contrato.ValorAportesOperador = @VALOR
	WHERE Contrato.Contrato.IDContrato = @IDCONTRATO
	---APORTE ICBF

	SELECT  @VALORICBF = SUM(Contrato.AporteContrato.ValorAporte)
	FROM 
	Contrato.AporteContrato
	where
	Contrato.AporteContrato.IdContrato = @IDCONTRATO
	AND Contrato.AporteContrato.TipoAportante = @TIPOAPORTANTE

	UPDATE Contrato.Contrato SET Contrato.Contrato.ValorAportesICBF = @VALORICBF
	WHERE Contrato.Contrato.IDContrato = @IDCONTRATO

END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  27/06/2013 
-- Description:	Procedimiento almacenado que consulta un(a) Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]
@IdAcuerdo INT
AS
BEGIN
SELECT
	IdAcuerdo,
	Nombre,
	ContenidoHtml,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[Acuerdos]
WHERE IdAcuerdo = @IdAcuerdo
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Saul Rodriguez
-- Create date:  27/06/2013 
-- Description:	Procedimiento almacenado que consulta el acuerdo vigente
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]
AS
BEGIN
	SELECT
		IdAcuerdo,
		Nombre,
		ContenidoHtml,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[Acuerdos]
	WHERE IdAcuerdo = (SELECT MAX(IdAcuerdo) FROM [Proveedor].[Acuerdos])
	
END





GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  26/06/2013 
-- Description:	Procedimiento almacenado que guarda un nuevo Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]

@IdAcuerdo INT OUTPUT, 
@Nombre NVARCHAR (64), 
@ContenidoHtml NVARCHAR (MAX), 
@UsuarioCrea NVARCHAR (250)

AS
BEGIN

INSERT INTO Proveedor.Acuerdos (Nombre, ContenidoHtml, UsuarioCrea, FechaCrea)
	VALUES (@Nombre, @ContenidoHtml, @UsuarioCrea, GETDATE())
SELECT
	@IdAcuerdo = @@IDENTITY
	
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  26/06/2013 
-- Description:	Procedimiento almacenado que consulta un(a) Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]
@Nombre NVARCHAR (64) = NULL
AS
BEGIN

	SELECT
		IdAcuerdo,
		Nombre,
		ContenidoHtml,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[Acuerdos]
	WHERE Nombre =
		CASE
			WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre
		END 
		
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Consultar]
	@IdClasedeEntidad INT
AS
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[ClasedeEntidad] WHERE  IdClasedeEntidad = @IdClasedeEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que elimina un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]
	@IdClasedeEntidad INT
AS
BEGIN
	DELETE Proveedor.ClasedeEntidad WHERE IdClasedeEntidad = @IdClasedeEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]
		@IdClasedeEntidad INT OUTPUT, 	@IdTipodeActividad INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ClasedeEntidad(IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipodeActividad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdClasedeEntidad = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Modificar]
		@IdClasedeEntidad INT,	@IdTipodeActividad INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.ClasedeEntidad SET IdTipodeActividad = @IdTipodeActividad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdClasedeEntidad = @IdClasedeEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_Consultar]
		@IdTipodeActividad INT = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT Proveedor.ClasedeEntidad.IdClasedeEntidad, Proveedor.ClasedeEntidad.IdTipodeActividad, Proveedor.ClasedeEntidad.Descripcion, Proveedor.ClasedeEntidad.Estado, Proveedor.ClasedeEntidad.UsuarioCrea, Proveedor.ClasedeEntidad.FechaCrea, Proveedor.ClasedeEntidad.UsuarioModifica, Proveedor.ClasedeEntidad.FechaModifica 
 ,Proveedor.TipodeActividad.Descripcion AS TipodeActividad
 FROM [Proveedor].[ClasedeEntidad] 
 INNER JOIN Proveedor.TipodeActividad ON Proveedor.TipodeActividad.IdTipodeActividad = Proveedor.ClasedeEntidad.IdTipodeActividad
 WHERE Proveedor.ClasedeEntidad.IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN Proveedor.ClasedeEntidad.IdTipodeActividad ELSE @IdTipodeActividad END AND Proveedor.ClasedeEntidad.Descripcion = CASE WHEN @Descripcion IS NULL THEN Proveedor.ClasedeEntidad.Descripcion ELSE @Descripcion END AND Proveedor.ClasedeEntidad.Estado = CASE WHEN @Estado IS NULL THEN Proveedor.ClasedeEntidad.Estado ELSE @Estado END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL]
	@IdTipodeActividad INT = NULL
AS
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[ClasedeEntidad] 
 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END AND Estado = 1
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]
	@IdTipodeActividad INT = NULL,@IdTipoEntidad INT = NULL
AS
BEGIN
 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[ClasedeEntidad] 
 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
	   AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
	   AND Estado = 1
	   ORDER BY Descripcion ASC
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan NIño
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]
	@IdContacto INT
AS
BEGIN
 SELECT CE.IdContacto, CE.IdEntidad, CE.IdSucursal, CE.IdTelContacto, CE.IdTipoDocIdentifica, CE.IdTipoCargoEntidad, CE.NumeroIdentificacion, CE.PrimerNombre, 
	CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido, CE.Dependencia, CE.Email, CE.Estado, CE.UsuarioCrea, CE.FechaCrea, CE.UsuarioModifica, CE.FechaModifica,
	TT.Movil AS Celular, TT.IndicativoTelefono, TT.NumeroTelefono, TT.ExtensionTelefono,
	TC.Descripcion AS Cargo
 FROM [Proveedor].[ContactoEntidad] AS CE
 LEFT JOIN [Oferente].[TelTerceros] AS TT ON CE.IdTelContacto = TT.IdTelTercero
 LEFT JOIN [Proveedor].[TipoCargoEntidad] AS TC ON CE.IdTipoCargoEntidad = TC.IdTipoCargoEntidad
 WHERE  IdContacto = @IdContacto
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que elimina un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]
	@IdContacto INT
AS
BEGIN
	DELETE Proveedor.ContactoEntidad WHERE IdContacto = @IdContacto
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]
		@IdContacto INT OUTPUT, 	@IdEntidad INT,	@IdSucursal INT, @IdTelContacto INT,	@IdTipoDocIdentifica INT,	
		@IdTipoCargoEntidad INT,	@NumeroIdentificacion NUMERIC(30),	@PrimerNombre NVARCHAR(128),	@SegundoNombre NVARCHAR(128),	
		@PrimerApellido NVARCHAR(128),	@SegundoApellido NVARCHAR(128),	@Dependencia NVARCHAR(128),	@Email NVARCHAR(128),	@Estado BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ContactoEntidad(IdEntidad, IdSucursal, IdTelContacto, IdTipoDocIdentifica, IdTipoCargoEntidad, NumeroIdentificacion, PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido, Dependencia, Email, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @IdSucursal, @IdTelContacto, @IdTipoDocIdentifica, @IdTipoCargoEntidad, @NumeroIdentificacion, @PrimerNombre, @SegundoNombre, @PrimerApellido, @SegundoApellido, @Dependencia, @Email, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdContacto = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Modificar]
		@IdContacto INT,	@IdEntidad INT, @IdSucursal INT, @IdTelContacto INT,	@IdTipoDocIdentifica INT,	@IdTipoCargoEntidad INT,	@NumeroIdentificacion NUMERIC(30),	@PrimerNombre NVARCHAR(128),	@SegundoNombre NVARCHAR(128),	@PrimerApellido NVARCHAR(128),	@SegundoApellido NVARCHAR(128),	@Dependencia NVARCHAR(128),	@Email NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.ContactoEntidad 
	SET IdEntidad = @IdEntidad, IdSucursal = @IdSucursal, IdTelContacto = @IdTelContacto, IdTipoDocIdentifica = @IdTipoDocIdentifica, IdTipoCargoEntidad = @IdTipoCargoEntidad, NumeroIdentificacion = @NumeroIdentificacion, PrimerNombre = @PrimerNombre, SegundoNombre = @SegundoNombre, PrimerApellido = @PrimerApellido, SegundoApellido = @SegundoApellido, Dependencia = @Dependencia, Email = @Email, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdContacto = @IdContacto
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidads_Consultar]
	@NumeroIdentificacion NUMERIC(30) = NULL,@PrimerNombre NVARCHAR(128) = NULL,@SegundoNombre NVARCHAR(128) = NULL,
	@PrimerApellido NVARCHAR(128) = NULL,@SegundoApellido NVARCHAR(128) = NULL,@Dependencia NVARCHAR(128) = NULL,@Email NVARCHAR(128) = NULL,@Estado BIT = NULL,
	@IdEntidad INT = NULL
AS
BEGIN
 SELECT CE.IdContacto, CE.IdEntidad, CE.IdSucursal, CE.IdTelContacto, CE.IdTipoDocIdentifica, CE.IdTipoCargoEntidad, CE.NumeroIdentificacion, CE.PrimerNombre, 
	CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido, CE.Dependencia, CE.Email, CE.Estado, CE.UsuarioCrea, CE.FechaCrea, CE.UsuarioModifica, CE.FechaModifica,
	TT.Movil AS Celular, TT.IndicativoTelefono, TT.NumeroTelefono, TT.ExtensionTelefono,
	TC.Descripcion AS Cargo, ISNULL(S.Nombre,'Ninguno') AS Sucursal
 FROM [Proveedor].[ContactoEntidad] AS CE
 LEFT JOIN [Oferente].[TelTerceros] AS TT ON CE.IdTelContacto = TT.IdTelTercero
 LEFT JOIN [Proveedor].[TipoDocIdentifica] AS TD ON CE.IdTipoDocIdentifica = TD.IdTipoDocIdentifica
 LEFT JOIN [Proveedor].[TipoCargoEntidad] AS TC ON CE.IdTipoCargoEntidad = TC.IdTipoCargoEntidad
 LEFT JOIN [Proveedor].[Sucursal] AS S ON CE.IdSucursal=S.IdSucursal
 WHERE NumeroIdentificacion = CASE WHEN @NumeroIdentificacion IS NULL THEN NumeroIdentificacion ELSE @NumeroIdentificacion END 
 AND CE.IdEntidad = CASE WHEN @IdEntidad IS NULL THEN CE.IdEntidad ELSE @IdEntidad END 
 AND PrimerNombre = CASE WHEN @PrimerNombre IS NULL THEN PrimerNombre ELSE @PrimerNombre END 
 AND SegundoNombre = CASE WHEN @SegundoNombre IS NULL THEN SegundoNombre ELSE @SegundoNombre END 
 AND PrimerApellido = CASE WHEN @PrimerApellido IS NULL THEN PrimerApellido ELSE @PrimerApellido END 
 AND SegundoApellido = CASE WHEN @SegundoApellido IS NULL THEN SegundoApellido ELSE @SegundoApellido END 
 AND Dependencia = CASE WHEN @Dependencia IS NULL THEN Dependencia ELSE @Dependencia END 
 AND Email = CASE WHEN @Email IS NULL THEN Email ELSE @Email END 
 AND CE.Estado = CASE WHEN @Estado IS NULL THEN CE.Estado ELSE @Estado END
 ORDER BY CE.PrimerNombre, CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido DESC
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]
@IdDocAdjunto INT
AS
BEGIN
SELECT
	IdDocAdjunto,
	IdTercero,
	IdDocumento,
	Descripcion,
	LinkDocumento,
	Anno,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[DocAdjuntoTercero]
WHERE IdDocAdjunto = @IdDocAdjunto
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocAdjuntoTercero WHERE IdDocAdjunto = @IdDocAdjunto 
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar]
		@IdTercero INT = NULL, 	@IdDocumento INT , 	@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Anno INT, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	DECLARE @IdDocAdjunto INT
	SET @IdDocAdjunto = 0
	IF (@IdTercero = 0) SET @IdTercero = NULL  
	INSERT INTO Proveedor.DocAdjuntoTercero(IDTERCERO,IDDOCUMENTO, Descripcion, LinkDocumento, Anno, UsuarioCrea, FechaCrea,idtemporal)
					  VALUES(@IdTercero,@IdDocumento, @Descripcion, @LinkDocumento, @Anno, @UsuarioCrea, GETDATE(),@IdTemporal)
	
	SELECT @IdDocAdjunto = @@IDENTITY 		
	
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar]
		@IdDocAdjunto INT,	@IdTercero INT,	
		@LinkDocumento NVARCHAR(256),	
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN

	IF (@IdTercero = 0) SET @IdTercero = NULL  

	UPDATE Proveedor.DocAdjuntoTercero 
	SET IdTercero = @IdTercero,
	LinkDocumento = @LinkDocumento, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdDocAdjunto = @IdDocAdjunto
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar]
	@IdDocAdjunto INT = NULL,
	@IdTercero INT = NULL,
	@IdDocumento INT = NULL
AS
BEGIN

SELECT			      Proveedor.DOCADJUNTOTERCERO.IDDOCADJUNTO, 
					  Proveedor.DOCADJUNTOTERCERO.IDTERCERO, 
					  Proveedor.DOCADJUNTOTERCERO.IDDOCUMENTO, 
                      Proveedor.DOCADJUNTOTERCERO.DESCRIPCION, 
                      Proveedor.DOCADJUNTOTERCERO.LINKDOCUMENTO, 
                      Proveedor.DOCADJUNTOTERCERO.ANNO, 
                      Proveedor.DOCADJUNTOTERCERO.USUARIOCREA, 
                      Proveedor.DOCADJUNTOTERCERO.FECHACREA, 
                      Proveedor.DOCADJUNTOTERCERO.USUARIOMODIFICA, 
                      Proveedor.DOCADJUNTOTERCERO.FECHAMODIFICA, 
                      Proveedor.TipoDocumento.CodigoTipoDocumento, 
                      Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento
FROM				  Proveedor.DOCADJUNTOTERCERO INNER JOIN
                      Proveedor.TipoDocumento ON Proveedor.DOCADJUNTOTERCERO.IDDOCUMENTO = Proveedor.TipoDocumento.IdTipoDocumento
WHERE				  IdDocAdjunto = CASE WHEN @IdDocAdjunto IS NULL THEN IdDocAdjunto ELSE @IdDocAdjunto END AND 
					  IdTercero = CASE WHEN @IdTercero IS NULL THEN IdTercero ELSE @IdTercero END AND IdDocumento = CASE WHEN @IdDocumento IS NULL THEN IdDocumento ELSE @IdDocumento END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona] 
( @IdTemporal varchar(20), @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionTercero';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdTercero) as IdTercero,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		--Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdTercero,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             --d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocAdjuntoTercero AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTemporal = @IdTemporal
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdTercero,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
		--	 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
AND tdp.estado=1
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona] 
( @IdTercero int, @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionTercero';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdTercero) as IdTercero,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		--Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdTercero,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             --d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocAdjuntoTercero AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTercero = @IdTercero
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdTercero,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
		--	 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
AND tdp.estado=1
) as Tabla
WHERE Obligatorio  =1 
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero con su peso y formato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma]
@IdTercero INT = NULL,
@CodigoDocumento NVARCHAR (20) ,
@Programa NVARCHAR (50)
AS
BEGIN


DECLARE @IdPrograma INT
SET @IdPrograma = (SELECT DISTINCT
	IdPrograma
FROM SEG.Programa
WHERE CodigoPrograma = @Programa)




IF @IdTercero IS NOT NULL
BEGIN
SELECT
	Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	Proveedor.DocAdjuntoTercero.IDDOCADJUNTO,
	Proveedor.DocAdjuntoTercero.IDTERCERO,
	Proveedor.DocAdjuntoTercero.IDDOCUMENTO,
	Proveedor.DocAdjuntoTercero.DESCRIPCION AS Descripcion,
	Proveedor.DocAdjuntoTercero.LINKDOCUMENTO,
	Proveedor.DocAdjuntoTercero.ANNO,
	Proveedor.DocAdjuntoTercero.FECHACREA,
	Proveedor.DocAdjuntoTercero.USUARIOCREA,
	Proveedor.DocAdjuntoTercero.FECHAMODIFICA,
	Proveedor.DocAdjuntoTercero.USUARIOMODIFICA
FROM Proveedor.TipoDocumento
INNER JOIN Proveedor.DocAdjuntoTercero
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.DocAdjuntoTercero.IDDOCUMENTO
RIGHT OUTER JOIN Proveedor.TipoDocumentoPrograma
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.TipoDocumentoPrograma.IdTipoDocumento
WHERE (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma) AND (Proveedor.DocAdjuntoTercero.IDTERCERO = @IdTercero) AND
(Proveedor.TipoDocumento.CodigoTipoDocumento = @CodigoDocumento)
END
ELSE
BEGIN
SELECT
	Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento,Proveedor.TipoDocumento.CodigoTipoDocumento,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	0 AS IDDOCADJUNTO,
	0 AS IDTERCERO,
	Proveedor.TipoDocumento.IdTipoDocumento AS IDDOCUMENTO,
	'' AS DESCRIPCION,
	'' AS LINKDOCUMENTO,
	0 AS ANNO,
	GETDATE() AS FECHACREA,
	'' AS USUARIOCREA,
	GETDATE() AS FECHAMODIFICA,
	'' AS USUARIOMODIFICA
FROM Proveedor.TipoDocumento
RIGHT OUTER JOIN Proveedor.TipoDocumentoPrograma
	ON Proveedor.TipoDocumento.IdTipoDocumento = Proveedor.TipoDocumentoPrograma.IdTipoDocumento
WHERE (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma) AND
(Proveedor.TipoDocumento.CodigoTipoDocumento = @CodigoDocumento)
END

END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]
	@IdDocAdjunto INT
AS
BEGIN
 SELECT IdDocAdjunto, IdEntidad, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTipoDocumento, IdTemporal
 FROM [Proveedor].[DocDatosBasicoProv] 
 WHERE  IdDocAdjunto = @IdDocAdjunto
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdEntidad y TipoPersona
-- =============================================

CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona] 
( @IdEntidad int, @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionProveedores';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdEntidad = @IdEntidad
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
WHERE Obligatorio  = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona_TipoSector]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdEntidad y TipoPersona y TipoSector
-- =============================================

CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona_TipoSector] 
( @IdEntidad int, @TipoPersona varchar(1),  @TipoSector varchar(1)  )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionProveedores';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             /*CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,*/
			CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
			 t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdEntidad = @IdEntidad
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
AND tdp.estado=1
) as Tabla
WHERE Obligatorio  = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento







GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdTemporal y TipoPersona
-- =============================================


CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona] 
( @IdTemporal VARCHAR(20), @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionProveedores';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTemporal = @IdTemporal
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta DocDatosBasicoProv por IdTemporal y TipoPersona y TipoSector
-- =============================================


CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector] 
( @IdTemporal VARCHAR(20), @TipoPersona varchar(2), @TipoSector varchar(2) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionProveedores';

SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdEntidad) as IdEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocDatosBasicoProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTemporal = @IdTemporal
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico
								ELSE 
									tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
	AND tdp.estado=1
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento







GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/18/2013 6:02:08 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocDatosBasicoProv WHERE IdDocAdjunto = @IdDocAdjunto
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DocDatosBasico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdEntidad INT = NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @IdTipoDocumento int, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	INSERT INTO Proveedor.DocDatosBasicoProv(IdEntidad, NombreDocumento, LinkDocumento, Observaciones, IdTipoDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdEntidad, @NombreDocumento, @LinkDocumento, @Observaciones, @IdTipoDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = @@IDENTITY 		
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]
		@IdDocAdjunto INT,	@IdEntidad INT = NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocDatosBasicoProv SET IdEntidad = @IdEntidad, NombreDocumento = @NombreDocumento, LinkDocumento = @LinkDocumento, Observaciones = @Observaciones, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDocAdjunto = @IdDocAdjunto
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdEntidad, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal
 FROM [Proveedor].[DocDatosBasicoProv] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar]
	@IdDocAdjunto INT
AS
BEGIN
 SELECT IdDocAdjunto, IdExpEntidad, IdTipoDocumento, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[DocExperienciaEntidad] 
 WHERE  IdDocAdjunto = @IdDocAdjunto
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/13/2013 08:10:55 PM
-- Description:	Procedimiento almacenado que consulta una lista de DocExperienciaEntidad por IdInfoExperiencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup] 
( 
	@IdExpEntidad INT, 
	@RupRenovado VARCHAR(128) = NULL,
	@TipoPersona varchar(1),  
	@TipoSector varchar(1)
)
AS

DECLARE @IdPrograma INT;
SELECT @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/INFOEXPERIENCIAENTIDAD';



SELECT	MAX(IdDocAdjunto) AS IdDocAdjunto,
		MAX(IdExpEntidad) AS IdExpEntidad,
		NombreDocumento,
		Max(LinkDocumento) AS LinkDocumento,
		Max(Descripcion) AS Descripcion,
		Max(Obligatorio) AS Obligatorio,
		Max(UsuarioCrea) AS UsuarioCrea,
		Max(FechaCrea) AS FechaCrea,
		Max(UsuarioModifica) AS UsuarioModifica,
		Max(FechaModifica) AS FechaModifica,
		Max(IdTipoDocumento) AS IdTipoDocumento,
		Max(MaxPermitidoKB) AS MaxPermitidoKB,
		Max(ExtensionesPermitidas) AS ExtensionesPermitidas,
		Max(IdTemporal) AS IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdExpEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Descripcion,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--				WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocExperienciaEntidad AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION EXPERIENCIA
		AND IdExpEntidad = @IdExpEntidad
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdInfoFin,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--		WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION EXPERIENCIA
AND tdp.estado=1
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento





GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Jonnathan NIño
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta una lista de DocExperienciaEntidad por IdTemporal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup] 
( 
	@IdTemporal varchar(20), 
	@RupRenovado varchar(128)=NULL, 
	@TipoPersona varchar(1),  
	@TipoSector varchar(1)
)
AS
DECLARE @IdPrograma int;
SELECT @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/INFOEXPERIENCIAENTIDAD';


SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdExpEntidad) as IdExpEntidad,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Descripcion) as Descripcion,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdExpEntidad,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Descripcion,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--				WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocExperienciaEntidad AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION EPERIENCIA
		AND IdTemporal = @IdTemporal
		AND tdp.estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdExpEntidad,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Descripcion,
			 --CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
				--		WHEN '0' THEN tdp.ObligRupNoRenovado END AS Obligatorio,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION EXPERIENCIA
AND tdp.estado=1
) as Tabla
Where Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento






GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocExperienciaEntidad WHERE IdDocAdjunto = @IdDocAdjunto
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdExpEntidad INT = NULL,	@IdTipoDocumento INT,	
		@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256), @UsuarioCrea NVARCHAR(250),
		@IdTemporal NVARCHAR(50)
AS
BEGIN
	INSERT INTO Proveedor.DocExperienciaEntidad(IdExpEntidad, IdTipoDocumento, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdExpEntidad, @IdTipoDocumento, @Descripcion, @LinkDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que actualiza un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar]
		@IdDocAdjunto INT,	@IdExpEntidad INT=NULL,	@IdTipoDocumento INT,	@Descripcion NVARCHAR(128),	
		@LinkDocumento NVARCHAR(256), @UsuarioModifica NVARCHAR(250), @IdTemporal VARCHAR(50)=NULL
AS
BEGIN
	UPDATE Proveedor.DocExperienciaEntidad 
	SET IdExpEntidad = ISNULL(@IdExpEntidad,IdExpEntidad), IdTipoDocumento = @IdTipoDocumento, Descripcion = @Descripcion, 
	LinkDocumento = @LinkDocumento, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE(), IdTemporal=ISNULL(@IdTemporal,IdTemporal)
	WHERE IdDocAdjunto = @IdDocAdjunto
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]
	@IdExpEntidad INT = NULL,
	@Descripcion NVARCHAR(128) = NULL,
	@LinkDocumento NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdExpEntidad, D.IdTipoDocumento, D.Descripcion, LinkDocumento, TD.Descripcion AS DescripcionDocumento,
	D.UsuarioCrea, D.FechaCrea, D.UsuarioModifica, D.FechaModifica 
 FROM [Proveedor].[DocExperienciaEntidad] D
 INNER JOIN [Proveedor].[TipoDocumento] TD ON D.IdTipoDocumento=TD.IdTipoDocumento
 WHERE IdExpEntidad = CASE WHEN @IdExpEntidad IS NULL THEN IdExpEntidad ELSE @IdExpEntidad END 
 AND D.Descripcion = CASE WHEN @Descripcion IS NULL THEN D.Descripcion ELSE @Descripcion END 
 AND LinkDocumento = CASE WHEN @LinkDocumento IS NULL THEN LinkDocumento ELSE @LinkDocumento END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar]
	@IdDocAdjunto INT
AS
BEGIN
 SELECT IdDocAdjunto, IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal FROM [Proveedor].[DocFinancieraProv] WHERE  IdDocAdjunto = @IdDocAdjunto
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta una lista de DocFinancieraProv por IdInfoFin y si el Rup es renovado o no
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup] 
( @IdInfoFin int, @RupRenovado varchar(128), @TipoPersona varchar(1),  @TipoSector varchar(1))
as

declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/DocFinancieraProv';


SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdInfoFin) as IdInfoFin,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(ObligatorioRup) as ObligatorioRup,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdInfoFin,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
			--CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
			CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
								WHEN '0' THEN tdp.ObligRupNoRenovado END AS ObligatorioRup,
			CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocFinancieraProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION FINANCIERA
		AND IdInfoFin = @IdInfoFin
		AND tdp.Estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdInfoFin,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 --CASE substring(CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
			 CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
								WHEN '0' THEN tdp.ObligRupNoRenovado END AS ObligatorioRup,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION FINANCIERA
AND tdp.Estado=1
) as Tabla
Where Obligatorio = 1 and ObligatorioRup=1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento








GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta una lista de DocFinancieraProv por IdTemporal y si el Rup es renovado o no
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup] 
( @IdTemporal varchar(20), @RupRenovado varchar(128), @TipoPersona varchar(1),  @TipoSector varchar(1))
as

declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/DocFinancieraProv';


SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdInfoFin) as IdInfoFin,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(ObligatorioRup) as ObligatorioRup,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdTipoDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
FROM 
(SELECT      d.IdDocAdjunto, 
		 	 d.IdInfoFin,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             d.Observaciones,
			--CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
			 CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
								WHEN '0' THEN tdp.ObligRupNoRenovado END AS ObligatorioRup,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocFinancieraProv AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdTipoDocumento = tdp.IdTipoDocumento
WHERE     (tdp.IdPrograma = @idPrograma) -- MODULO DE INFORMACION FINANCIERA
		AND IdTemporal = @IdTemporal
	    AND tdp.Estado=1
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdInfoFin,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Observaciones,
			 --CASE substring(CodigoTipoDocumento, 1, 1) WHEN @RupRenovado THEN 1 ELSE 0 END AS Obligatorio,
			 CASE @RupRenovado	WHEN '1' THEN tdp.ObligRupRenovado 
								WHEN '0' THEN tdp.ObligRupNoRenovado END AS ObligatorioRup,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN 
								CASE @TipoSector	
									WHEN '1' THEN tdp.ObligSectorPrivado 
									WHEN '2' THEN tdp.ObligSectorPublico 
								ELSE tdp.ObligPersonaJuridica END END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE INFORMACION FINANCIERA
AND tdp.Estado=1
) as Tabla
Where Obligatorio = 1 and ObligatorioRup=1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento








GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que elimina un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocFinancieraProv WHERE IdDocAdjunto = @IdDocAdjunto
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdInfoFin INT=NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @IdTipoDocumento int, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	INSERT INTO Proveedor.DocFinancieraProv(IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, IdTipoDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdInfoFin, @NombreDocumento, @LinkDocumento, @Observaciones, @IdTipoDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]
		@IdDocAdjunto INT,	@IdInfoFin INT=NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocFinancieraProv SET IdInfoFin = @IdInfoFin, NombreDocumento = @NombreDocumento, LinkDocumento = @LinkDocumento, Observaciones = @Observaciones, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDocAdjunto = @IdDocAdjunto
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]
	@IdInfoFin int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal
 FROM [Proveedor].[DocFinancieraProv] 
 WHERE IdInfoFin = CASE WHEN @IdInfoFin IS NULL THEN IdInfoFin ELSE @IdInfoFin END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date: 02/07/2013
-- Description:	Obtiene los documentos de l proveedor notificaiones judiciales
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]
	@IdNotJudicial INT
AS
BEGIN
--	declare @IdNotJudicial int
--set @IdNotJudicial=13

declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'Proveedor/NotificacionJudicial';
PRINT @IdPrograma
SELECT	MAX(IdDocNotJudicial) as IdDocNotJudicial,
		MAX(IdNotJudicial) as IdNotJudicial,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Descripcion) as Descripcion,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas
		
FROM 
(
SELECT     Proveedor.DocNotificacionJudicial.IdDocNotJudicial ,
			Proveedor.DocNotificacionJudicial.IdNotJudicial,t.Descripcion AS NombreDocumento,
			Proveedor.DocNotificacionJudicial.LinkDocumento, 
			Proveedor.DocNotificacionJudicial.Descripcion ,
			t.IdTipoDocumento  ,t.UsuarioCrea , t.FechaCrea, t.UsuarioModifica ,t.FechaModifica ,
			 
			tdp.MaxPermitidoKB, tdp.ExtensionesPermitidas                   
FROM         SEG.Programa AS p INNER JOIN
                      Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma INNER JOIN
                      Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento INNER JOIN
                      Proveedor.DocNotificacionJudicial ON tdp.IdTipoDocumento = Proveedor.DocNotificacionJudicial.IdDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) AND (Proveedor.DocNotificacionJudicial.IdNotJudicial = @IdNotJudicial)-- MODULO DE GESTION PROVEEDOR
		
UNION                      
SELECT       0 as IdDocNotJudicial,
			 0 as IdNotJudicial,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Descripcion,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]
	@IdDocNotJudicial INT
AS
BEGIN
	DELETE Proveedor.DocNotificacionJudicial 
	WHERE IdDocNotJudicial = @IdDocNotJudicial 
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]
		@IdDocNotJudicial INT OUTPUT , 	
		@IdNotJudicial INT , 	
		@IdDocumento INT , 	
		@Descripcion NVARCHAR(128),	
		@LinkDocumento NVARCHAR(256),	
		@Anno INT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.DocNotificacionJudicial(IdNotJudicial,IdDocumento, Descripcion, LinkDocumento, Anno, UsuarioCrea, FechaCrea)
					  VALUES(@IdNotJudicial,@IdDocumento, @Descripcion, @LinkDocumento, @Anno, @UsuarioCrea, GETDATE())
	SELECT @IdDocNotJudicial = @@IDENTITY 		
	RETURN @@IDENTITY	
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que actualiza un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]
@IdDocNotJudicial INT,	
@IdNotJudicial INT,	
@IdDocumento INT,	
@Descripcion NVARCHAR(128),	
@LinkDocumento NVARCHAR(256),	
@Anno INT, 
@UsuarioModifica NVARCHAR(250)
AS
BEGIN
UPDATE Proveedor.DocNotificacionJudicial
SET	IdNotJudicial  = @IdNotJudicial,
	IdDocumento=@IdDocumento,
	Descripcion = @Descripcion,
	LinkDocumento = @LinkDocumento,
	Anno = @Anno,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdDocNotJudicial = @IdDocNotJudicial 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]
	@IdDocNotJudicial INT = NULL,
	@IdNotJudicial INT = NULL,
	@IdDocumento INT = NULL
AS
BEGIN
 SELECT     Proveedor.DocNotificacionJudicial.IdDocNotJudicial, 
		    Proveedor.DocNotificacionJudicial.IdNotJudicial, 
		    Proveedor.DocNotificacionJudicial.IdDocumento, 
            Proveedor.DocNotificacionJudicial.Descripcion, 
            Proveedor.DocNotificacionJudicial.LinkDocumento, 
            Proveedor.DocNotificacionJudicial.Anno, 
            Proveedor.DocNotificacionJudicial.UsuarioCrea, 
            Proveedor.DocNotificacionJudicial.FechaCrea, 
            Proveedor.DocNotificacionJudicial.UsuarioModifica, 
            Proveedor.DocNotificacionJudicial.FechaModifica, 
            Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento
 FROM       Proveedor.DocNotificacionJudicial 
			INNER JOIN Proveedor.TipoDocumento 
			ON Proveedor.DocNotificacionJudicial.IdDocumento = Proveedor.TipoDocumento.IdTipoDocumento
 WHERE		IdDocNotJudicial = CASE WHEN @IdDocNotJudicial IS NULL THEN IdDocNotJudicial ELSE @IdDocNotJudicial END AND 
			IdNotJudicial = CASE WHEN @IdNotJudicial IS NULL THEN IdNotJudicial ELSE @IdNotJudicial END AND 
			IdDocumento = CASE WHEN @IdDocumento IS NULL THEN IdDocumento ELSE @IdDocumento END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Consultar]
	@IdTipoPersona INT = NULL,@Tipoidentificacion INT= NULL,@Identificacion NVARCHAR(256) = NULL,
	@Proveedor NVARCHAR(256) = NULL,@IdEstado INT = NULL,@UsuarioCrea NVARCHAR(128)=NULL
AS
BEGIN
 SELECT DISTINCT * FROM (
  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador,EP.UsuarioCrea
  FROM [Proveedor].[EntidadProvOferente] EP 
     INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     WHERE T.IdTipoPersona=1
UNION 
 SELECT  EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.RAZONSOCIAL ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado ,EP.ObserValidador,EP.UsuarioCrea
  FROM [Proveedor].[EntidadProvOferente] EP 
    INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
    LEFT JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
    INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona
    INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
    INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
    WHERE T.IdTipoPersona=2
    ) DT
     
    
    WHERE IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN IdTipoPersona ELSE @IdTipoPersona END 
		  AND IDTIPODOCIDENTIFICA = CASE WHEN @Tipoidentificacion IS NULL THEN IDTIPODOCIDENTIFICA ELSE @Tipoidentificacion END 
		  AND NUMEROIDENTIFICACION = CASE WHEN @Identificacion IS NULL THEN NUMEROIDENTIFICACION ELSE @Identificacion END 
		  AND (Razonsocila) like CASE WHEN @Proveedor IS NULL THEN (Razonsocila) ELSE '%'+ @Proveedor +'%' END 
		  AND IdEstado = CASE WHEN @IdEstado IS NULL THEN IdEstado ELSE @IdEstado END 
		  AND UsuarioCrea = CASE WHEN @UsuarioCrea IS NULL THEN UsuarioCrea ELSE @UsuarioCrea END 

END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT [IdEntidad]
      ,[ConsecutivoInterno]
      ,[TipoEntOfProv]
      ,[IdTercero]
      ,[IdTipoCiiuPrincipal]
      ,[IdTipoCiiuSecundario]
      ,[IdTipoSector]
      ,[IdTipoClaseEntidad]
      ,[IdTipoRamaPublica]
      ,[IdTipoNivelGob]
      ,[IdTipoNivelOrganizacional]
      ,[IdTipoCertificadorCalidad]
      ,[FechaCiiuPrincipal]
      ,[FechaCiiuSecundario]
      ,[FechaConstitucion]
      ,[FechaVigencia]
      ,[FechaMatriculaMerc]
      ,[FechaExpiracion]
      ,[TipoVigencia]
      ,[ExenMatriculaMer]
      ,[MatriculaMercantil]
      ,[ObserValidador]
      ,[AnoRegistro]
      ,[IdEstado]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
      ,[IdAcuerdo]
      ,Finalizado
 FROM [Proveedor].[EntidadProvOferente] WHERE  IdEntidad = @IdEntidad
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que elimina un(a) EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Eliminar]
	@IdEntidad INT
AS
BEGIN
	DELETE Proveedor.EntidadProvOferente WHERE IdEntidad = @IdEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que guarda un nuevo EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]
		@IdEntidad INT OUTPUT, 	
		@ConsecutivoInterno NVARCHAR(128),
		@TipoEntOfProv BIT=NULL,	
		@IdTercero INT=NULL,	
		@IdTipoCiiuPrincipal INT=NULL,	
		@IdTipoCiiuSecundario INT=NULL,	
		@IdTipoSector INT=NULL,	
		@IdTipoClaseEntidad INT=NULL,	
		@IdTipoRamaPublica INT=NULL,	
		@IdTipoNivelGob INT=NULL,	
		@IdTipoNivelOrganizacional INT=NULL,	
		@IdTipoCertificadorCalidad INT=NULL,	
		@FechaCiiuPrincipal DATETIME=NULL,	
		@FechaCiiuSecundario DATETIME=NULL,	
		@FechaConstitucion DATETIME=NULL,	
		@FechaVigencia DATETIME=NULL,	
		@FechaMatriculaMerc DATETIME=NULL,	
		@FechaExpiracion DATETIME=NULL,	
		@TipoVigencia BIT=NULL,	
		@ExenMatriculaMer BIT=NULL,	
		@MatriculaMercantil NVARCHAR(20)=NULL,	
		@ObserValidador NVARCHAR(256)=NULL,	
		@AnoRegistro INT=NULL,	
		@IdEstado INT=NULL, 
		@UsuarioCrea NVARCHAR(250),
		@IdTemporal VARCHAR(20) = NULL
AS
BEGIN
	INSERT INTO Proveedor.EntidadProvOferente(TipoEntOfProv, IdTercero, IdTipoCiiuPrincipal, IdTipoCiiuSecundario, IdTipoSector, IdTipoClaseEntidad, IdTipoRamaPublica, IdTipoNivelGob, IdTipoNivelOrganizacional, IdTipoCertificadorCalidad, FechaCiiuPrincipal, FechaCiiuSecundario, FechaConstitucion, FechaVigencia, FechaMatriculaMerc, FechaExpiracion, TipoVigencia, ExenMatriculaMer, MatriculaMercantil, ObserValidador, AnoRegistro, IdEstado, UsuarioCrea, FechaCrea, NroRevision)
					  VALUES(@TipoEntOfProv, @IdTercero, @IdTipoCiiuPrincipal, @IdTipoCiiuSecundario, @IdTipoSector, @IdTipoClaseEntidad, @IdTipoRamaPublica, @IdTipoNivelGob, @IdTipoNivelOrganizacional, @IdTipoCertificadorCalidad, @FechaCiiuPrincipal, @FechaCiiuSecundario, @FechaConstitucion, @FechaVigencia, @FechaMatriculaMerc, @FechaExpiracion, @TipoVigencia, @ExenMatriculaMer, @MatriculaMercantil, @ObserValidador, @AnoRegistro, @IdEstado, @UsuarioCrea, GETDATE(), 1)
	SELECT @IdEntidad = @@IDENTITY 		
	
	UPDATE [Proveedor].[DocDatosBasicoProv] set IdEntidad = @IdEntidad where IdTemporal = @IdTemporal
	
	UPDATE Proveedor.EntidadProvOferente SET ConsecutivoInterno=@ConsecutivoInterno +  RIGHT('00000' + Ltrim(Rtrim(@IdEntidad)),8) WHERE IdEntidad=@IdEntidad
	
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que actualiza un(a) EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar]
		@IdEntidad INT,	
		@TipoEntOfProv BIT= NULL,	
		@IdTercero INT= NULL,	
		@IdTipoCiiuPrincipal INT = NULL,	
		@IdTipoCiiuSecundario INT= NULL,	
		@IdTipoSector INT = NULL,	
		@IdTipoClaseEntidad INT = NULL,	
		@IdTipoRamaPublica INT = NULL,	
		@IdTipoNivelGob INT = NULL,	
		@IdTipoNivelOrganizacional INT = NULL,	
		@IdTipoCertificadorCalidad INT = NULL,	
		@FechaCiiuPrincipal DATETIME = NULL,	
		@FechaCiiuSecundario DATETIME = NULL,	
		@FechaConstitucion DATETIME = NULL,	
		@FechaVigencia DATETIME = NULL,	
		@FechaMatriculaMerc DATETIME = NULL,	
		@FechaExpiracion DATETIME = NULL,	
		@TipoVigencia BIT = NULL,	
		@ExenMatriculaMer BIT = NULL,	
		@MatriculaMercantil NVARCHAR(20) = NULL,	
		@ObserValidador NVARCHAR(256) = NULL,	
		@AnoRegistro INT = NULL,	
		@IdEstado INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL
AS
BEGIN
	UPDATE Proveedor.EntidadProvOferente 
			SET TipoEntOfProv = ISNULL(@TipoEntOfProv,TipoEntOfProv) ,
			    IdTercero = ISNULL(@IdTercero,IdTercero), 
			    IdTipoCiiuPrincipal =ISNULL( @IdTipoCiiuPrincipal, IdTipoCiiuPrincipal ),
				IdTipoCiiuSecundario = @IdTipoCiiuSecundario,
				IdTipoSector =ISNULL( @IdTipoSector, IdTipoSector ),
				IdTipoClaseEntidad =ISNULL( @IdTipoClaseEntidad, IdTipoClaseEntidad ),
				IdTipoRamaPublica =ISNULL( @IdTipoRamaPublica, IdTipoRamaPublica ),
				IdTipoNivelGob =ISNULL( @IdTipoNivelGob, IdTipoNivelGob ),
				IdTipoNivelOrganizacional =ISNULL( @IdTipoNivelOrganizacional, IdTipoNivelOrganizacional ),
				IdTipoCertificadorCalidad =ISNULL( @IdTipoCertificadorCalidad, IdTipoCertificadorCalidad ),
				FechaCiiuPrincipal =ISNULL( @FechaCiiuPrincipal, FechaCiiuPrincipal ),
				FechaCiiuSecundario = @FechaCiiuSecundario,
				FechaConstitucion =ISNULL( @FechaConstitucion, FechaConstitucion ),
				FechaVigencia =ISNULL( @FechaVigencia, FechaVigencia ),
				FechaMatriculaMerc =ISNULL( @FechaMatriculaMerc, FechaMatriculaMerc ),
				FechaExpiracion =ISNULL( @FechaExpiracion, FechaExpiracion ),
				TipoVigencia =ISNULL( @TipoVigencia, TipoVigencia ),
				ExenMatriculaMer =ISNULL( @ExenMatriculaMer, ExenMatriculaMer ),
				MatriculaMercantil =ISNULL( @MatriculaMercantil, MatriculaMercantil ),
				ObserValidador =ISNULL( @ObserValidador, ObserValidador ),
				AnoRegistro =ISNULL( @AnoRegistro, AnoRegistro ),
				IdEstado =ISNULL( @IdEstado, IdEstado ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE() 
			   WHERE IdEntidad = @IdEntidad
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar_EstadoDocumental]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  21/07/2013 15:07:30
-- Description:	Procedimiento almacenado que actualiza el estado documental de EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Modificar_EstadoDocumental]
		@IdEntidad INT,	
		@IdEstado INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL,
		@Finalizado BIT = NULL
AS
BEGIN
	UPDATE Proveedor.EntidadProvOferente 
			SET IdEstado =ISNULL( @IdEstado, IdEstado ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE(),
			    Finalizado = ISNULL(@Finalizado,Finalizado)
			   WHERE IdEntidad = @IdEntidad 
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  12/07/2013 22:42:55 
-- Description:	Procedimiento almacenado que consulta Proveedores para Validar
-- 
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar] 
	@IdEstado INT = NULL,
	@IdTipoPersona INT = NULL,
	@IDTIPODOCIDENTIFICA INT= NULL,
	@NUMEROIDENTIFICACION BIGINT = NULL,
	@IdTipoCiiuPrincipal INT = NULL,
	@IdMunicipioDirComercial INT = NULL,
	@IdDepartamentoDirComercial INT = NULL,
	@IdTipoSector INT = NULL,
	@IdTipoRegimenTributario INT = NULL,
	@Proveedor NVARCHAR(256) = NULL	
	
AS
BEGIN
SELECT     EP.IdEntidad, EP.ConsecutivoInterno, T.IdTipoPersona, TP.NombreTipoPersona, TD.CodDocumento, T.IDTIPODOCIDENTIFICA, 
                      T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, LTRIM(RTRIM(ISNULL(T.PRIMERNOMBRE, '') + ' ' + ISNULL(T.SEGUNDONOMBRE, '') 
                      + ' ' + ISNULL(T.PRIMERAPELLIDO, '') + ' ' + ISNULL(T.SEGUNDOAPELLIDO, '') + ' ' + ISNULL(T.RAZONSOCIAL, ''))) AS Razonsocial, E.Descripcion AS Estado, 
                      EP.IdEstado, EP.UsuarioCrea, EP.IdTipoCiiuPrincipal, 
                      Proveedor.TipoCiiu.CodigoCiiu + '-' + Proveedor.TipoCiiu.Descripcion AS ActividadCiiuPrincipal, Proveedor.TipoSectorEntidad.IdTipoSectorEntidad, 
                      Proveedor.TipoSectorEntidad.Descripcion AS SectorEntidad, Proveedor.TipoRegimenTributario.IdTipoRegimenTributario, 
                      Proveedor.TipoRegimenTributario.Descripcion AS RegimenTributario, EP.IdTipoSector, DIV.Municipio.NombreMunicipio, DIV.Departamento.NombreDepartamento, 
                      IAE.IdMunicipioDirComercial, DIV.Municipio.IdDepartamento, EP.FechaCrea
FROM         Proveedor.EntidadProvOferente AS EP INNER JOIN
                      Oferente.TERCERO AS T ON EP.IdTercero = T.IDTERCERO INNER JOIN
                      Proveedor.InfoAdminEntidad AS IAE ON EP.IdEntidad = IAE.IdEntidad INNER JOIN
                      Oferente.TipoPersona AS TP ON T.IdTipoPersona = TP.IdTipoPersona INNER JOIN
                      Global.TiposDocumentos AS TD ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento INNER JOIN
                      Proveedor.EstadoDatosBasicos AS E ON EP.IdEstado = E.IdEstadoDatosBasicos INNER JOIN
                      Proveedor.TipoCiiu ON EP.IdTipoCiiuPrincipal = Proveedor.TipoCiiu.IdTipoCiiu AND EP.IdTipoCiiuPrincipal = Proveedor.TipoCiiu.IdTipoCiiu INNER JOIN
                      Proveedor.TipoSectorEntidad ON EP.IdTipoSector = Proveedor.TipoSectorEntidad.IdTipoSectorEntidad AND 
                      EP.IdTipoSector = Proveedor.TipoSectorEntidad.IdTipoSectorEntidad INNER JOIN
                      Proveedor.TipoRegimenTributario ON IAE.IdTipoRegTrib = Proveedor.TipoRegimenTributario.IdTipoRegimenTributario AND 
                      IAE.IdTipoRegTrib = Proveedor.TipoRegimenTributario.IdTipoRegimenTributario INNER JOIN
                      DIV.Municipio INNER JOIN
                      DIV.Departamento ON DIV.Municipio.IdDepartamento = DIV.Departamento.IdDepartamento AND DIV.Municipio.IdDepartamento = DIV.Departamento.IdDepartamento ON 
                      IAE.IdMunicipioDirComercial = DIV.Municipio.IdMunicipio
WHERE (EP.IdEstado = CASE WHEN @IdEstado IS NULL THEN EP.IdEstado ELSE @IdEstado END )
AND (T.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN T.IdTipoPersona ELSE @IdTipoPersona END )--1 
AND (T.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN T.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END ) 
AND (T.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN T.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END) 
AND (EP.IdTipoCiiuPrincipal = CASE WHEN @IdTipoCiiuPrincipal IS NULL THEN EP.IdTipoCiiuPrincipal ELSE @IdTipoCiiuPrincipal END) 
AND (IAE.IdMunicipioDirComercial = CASE WHEN @IdMunicipioDirComercial IS NULL THEN IAE.IdMunicipioDirComercial ELSE @IdMunicipioDirComercial END) 
AND (IAE.IdDepartamentoDirComercial = CASE WHEN @IdDepartamentoDirComercial IS NULL THEN IAE.IdDepartamentoDirComercial ELSE @IdDepartamentoDirComercial END) 
AND (EP.IdTipoSector = CASE WHEN @IdTipoSector IS NULL THEN EP.IdTipoSector ELSE @IdTipoSector END) 
AND (IAE.IdTipoRegTrib = CASE WHEN @IdTipoRegimenTributario IS NULL THEN IAE.IdTipoRegTrib ELSE @IdTipoRegimenTributario END) 
AND (
					T.PRIMERNOMBRE LIKE CASE WHEN @Proveedor IS NULL THEN T.PRIMERNOMBRE ELSE  '%'+ @Proveedor +'%' END
					OR T.SEGUNDONOMBRE  LIKE CASE WHEN @Proveedor IS NULL THEN T.SEGUNDONOMBRE ELSE  '%'+ @Proveedor +'%' END
					OR T.PRIMERAPELLIDO LIKE CASE WHEN @Proveedor IS NULL THEN T.PRIMERAPELLIDO ELSE  '%'+ @Proveedor +'%' END
					OR T.SEGUNDOAPELLIDO LIKE CASE WHEN @Proveedor IS NULL THEN T.SEGUNDOAPELLIDO ELSE  '%'+ @Proveedor +'%' END
					OR T.RAZONSOCIAL  LIKE CASE WHEN @Proveedor IS NULL THEN T.RAZONSOCIAL ELSE  '%'+ @Proveedor +'%' END
					OR ISNULL(T.PRIMERNOMBRE,'')+' '+ISNULL(T.SEGUNDONOMBRE,'')+' '+ISNULL(T.PRIMERAPELLIDO,'')+' '+ISNULL(T.SEGUNDOAPELLIDO,'') LIKE '%'+RTRIM(LTRIM(@Proveedor))+'%'
					)
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero y los proveedores que se hayan cambiado el estado y envian un correo a los gestores
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero]

@IdTemporal nvarchar (20),
@Tercero varchar (30),
@correos varchar(max)
AS
BEGIN
--DECLARE @Tercero varchar(30), 
DECLARE @Asunto varchar (100), @Email_Destino varchar (max)
SET @Email_Destino = @correos
--SET @Tercero = 'Tercero'
SET @Asunto = 'Validar ' + @Tercero
Declare @Body varchar (max),

@TableHead varchar (max),
@TableTail varchar (max)
SET NOCOUNT ON;
SET @TableTail = '</table></body></html>';
SET @TableHead = '<html><head>' +
				 '<style>' +
				 'td {border: solid black 1px;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;font-size:11pt;} ' +
				 '</style>' +
				 '</head>' +
				 '<body><table cellpadding=0 cellspacing=0 border=0>' +
				 '<tr bgcolor=#ccbfac><td align=center><b>Tipo Identificaci&oacute;n</b></td>' +
				 '<td align=center><b>N&uacute;mero Identificaci&oacute;n</b></td>' +
				 '<td align=center><b>' + @Tercero + '</b></td>';

SELECT
	@Body = (SELECT DISTINCT
		ROW_NUMBER() OVER (ORDER BY Global.TiposDocumentos.CodDocumento) % 2 AS [TRRow],
		Global.TiposDocumentos.CodDocumento AS [TD],
		Oferente.TERCERO.NUMEROIDENTIFICACION AS [TD],
		(ISNULL(Oferente.TERCERO.PRIMERNOMBRE,'') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDONOMBRE,'') + ' ' + ISNULL(Oferente.TERCERO.PRIMERAPELLIDO,'') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDOAPELLIDO,'') + ' ' + ISNULL(Oferente.TERCERO.RAZONSOCIAL,'')) AS [TD]
	FROM Oferente.TERCERO
	INNER JOIN Global.TiposDocumentos
		ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
	INNER JOIN Oferente.EstadoTercero
		ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
	INNER JOIN Oferente.TipoPersona
		ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
	INNER JOIN Proveedor.MotivoCambioEstado
		ON Oferente.TERCERO.IDTERCERO = Proveedor.MotivoCambioEstado.IdTercero
	WHERE Proveedor.MotivoCambioEstado.IdTemporal = @IdTemporal
	ORDER BY Global.TiposDocumentos.CodDocumento
	FOR xml RAW ('tr'), ELEMENTS)

-- Replace the entity codes and row numbers
SET @Body = REPLACE(@Body, '_x0020_', SPACE(1))
SET @Body = REPLACE(@Body, '_x003D_', '=')
SET @Body = REPLACE(@Body, '<tr><TRRow>1</TRRow>', '<tr bgcolor=#e5d7c2>')
SET @Body = REPLACE(@Body, '<TRRow>0</TRRow>', '')

SELECT
	@Body = @TableHead + @Body + @TableTail

--PRINT @Body

-- Enviar correo
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'GonetMail', --colocar perfil que se tenga configurado
								@recipients = @Email_Destino, --destinatario
								@subject = @Asunto, --asunto
								@body = @Body, --cuerpo de correo
								@body_format = 'HTML'; --formato de correo


END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Faiber Losada
-- Create date:  2013-06-09
-- Description:	Procedimiento consulta Inconsistencias la validacion de Datos Básicos,Datos Financieros y Experiencia del Proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada]

@IdEntidad int

AS
BEGIN
DECLARE @Asunto varchar (100), @Email_Destino varchar (max), @Proveedor varchar (100), @nit varchar (30)
DECLARE @Titulo varchar(max) 
Declare @Body varchar (max),
@TableHead varchar (max),
@TableTail varchar (max),
@TableBody varchar (max),
@BodyTitulo varchar(max),
@Body1 varchar (max),
@Body2 varchar (max),
@Body3 varchar (max),
@Final varchar (max)

SELECT @Email_Destino=t.correoelectronico,@Proveedor= t.primernombre +' '+ ISNULL( t.segundonombre,'') +' ' + t.primerapellido +' ' + ISNULL( t.segundoapellido,'') +' ' + ISNULL( t. razonsocial,''), @nit=NUMEROIDENTIFICACION FROM Oferente.tercero t 
		INNER JOIN   Proveedor.EntidadProvOferente epo ON t.idtercero= epo.idtercero
		 WHERE epo.IdEntidad =@IdEntidad 

select @Titulo = ValorParametro from SEG.Parametro where NombreParametro='MensajeMailsInconsistenciasProveedor'

SET @TableHead = '<html><body style="font-family:Verdana"><TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD bgColor=#81ba3d colSpan=3></TD></TR>
<TR>
<TD bgColor=#81ba3d colSpan=3><BR/><BR/></TD></TR>
<TR>
<TD bgColor=#81ba3d width="10%"></TD>
<TD>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD width="5%"><BR/></TD>
<TD align=center><BR/></TD>
<TD width="5%"><BR/></TD></TR>
<TR>
<TD></TD>
<TD align=center><STRONG>' + @Titulo + '</STRONG></TD>
<TD></TD></TR>
<TR>
<TD width="5%"></TD>
<TD></TD>
<TD width="5%"></TD></TR>
<TR>
<TD></TD>
<TD><BR/><BR/><STRONG>Apreciado(a):</STRONG></TD>
<TD></TD></TR>
<TR>
<TD></TD>
<TD></TD>
<TD></TD></TR>
<TR>
<TD></TD>
<TD><BR/>Los siguientes son datos que estan inconsistentes, en el registro de proveedores
	<br/> <br/> Nit ' + @nit +' - ( ' + @Proveedor +' ) <br/><br/>
</TD>
<TD></TD></TR>
<TR>
<TD></TD>
<TD></TD>
<TD></TD></TR>
<TR>
<TD></TD>
<TD align = "center"><BR/><table cellpadding=0 cellspacing=0 border=0 width="80%">' +
				 '<tr bgcolor=#ccbfac>'+
				 '<td align=center><b>Módulo del sistema</b></td>' +
				 '<td align=center><b>Observación</b></td></tr>';

				 
set @TableTail  = '</table><BR/><BR/>
				 </TD>
				<TD></TD>
				</TR>
				<TR>
				<TD bgColor=#81ba3d colSpan=3 align=center></TD></TR>
				<TR>
				<TD bgColor=#81ba3d colSpan=3 align=center><BR/><BR/>Este correo electrónico fue enviado por un sistema automático, favor de no responderlo. </TD></TR>
				<TR>
				<TD bgColor=#81ba3d colSpan=3 align=center>Si tienes alguna duda, puedes dirigirte a nuestra sección de <A href="http://www.icbf.gov.co/" target=_blank>Asistencia y Soporte.</A></TD></TR>
				</TBODY></TABLE>
				</TD>
				<TD bgColor=#81ba3d width="10%"></TD></TR>
				</TBODY></TABLE></P>
				</body></html>';


	SELECT @TableBody = (SELECT DISTINCT	ROW_NUMBER() OVER (ORDER BY TRRow) % 2 AS[TRRow],[TRRow],TD FROM(

SELECT 'Datos Básicos'AS [TRRow],vdb.Observaciones AS [TD] FROM Proveedor.ValidarInfoDatosBasicosEntidad vdb
			  INNER JOIN Proveedor.EntidadProvOferente epo ON vdb.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad =@IdEntidad 			  and vdb.ConfirmaYAprueba = 0 and CorreoEnviado != 1
			  UNION
SELECT 'Datos Financieros'AS [TRRow],vif.Observaciones AS [TD] FROM Proveedor.ValidarInfoFinancieraEntidad vif
			  INNER JOIN Proveedor.InfoFinancieraEntidad ifr ON vif.IdInfoFin=ifr.IdInfoFin
			  INNER JOIN Proveedor.EntidadProvOferente epo ON ifr.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad =@IdEntidad and vif.ConfirmaYAprueba = 0	and CorreoEnviado != 1  UNION 

SELECT 'Experiencia'AS [TRRow],vee.Observaciones AS [TD] FROM Proveedor.ValidarInfoExperienciaEntidad vee
			  INNER JOIN Proveedor.InfoExperienciaEntidad iee ON iee.IdExpEntidad=vee.IdExpEntidad
			  INNER JOIN Proveedor.EntidadProvOferente epo ON iee.IdEntidad=epo.IdEntidad
			  WHERE epo.IdEntidad=@IdEntidad and vee.ConfirmaYAprueba = 0 and CorreoEnviado != 1
			 ) Tbtp
	FOR xml RAW ('tr'), ELEMENTS)

-- Replace the entity codes and row numbers
SET @TableBody = REPLACE(@TableBody, '_x0020_', SPACE(1))
SET @TableBody = REPLACE(@TableBody, '_x003D_', '=')
SET @TableBody = REPLACE(@TableBody, '<tr><TRRow>1</TRRow>', '<tr bgcolor=#e5d7c2>')
SET @TableBody = REPLACE(@TableBody, '<TRRow>0</TRRow>', '')
SET @TableBody = REPLACE(@TableBody, 'TRRow', 'td')

SELECT
	@Body = @TableHead + @TableBody + @TableTail

PRINT @Body

-- Enviar correo
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'GonetMail', --colocar perfil que se tenga configurado
								@recipients = @Email_Destino, --destinatario
								@subject = @Asunto, --asunto
								@body = @Body, --cuerpo de correo
								@body_format = 'HTML'; --formato de correo


--=================================
-- se marca como correo enviado  --
--=================================
UPDATE Proveedor.ValidarInfoDatosBasicosEntidad
SET CorreoEnviado = 1
FROM Proveedor.ValidarInfoDatosBasicosEntidad vdb
INNER JOIN Proveedor.EntidadProvOferente epo ON vdb.IdEntidad=epo.IdEntidad
WHERE epo.IdEntidad =24 and vdb.ConfirmaYAprueba = 0 and vdb.CorreoEnviado != 1

UPDATE Proveedor.ValidarInfoFinancieraEntidad
SET CorreoEnviado = 1
FROM Proveedor.ValidarInfoFinancieraEntidad vif
INNER JOIN Proveedor.InfoFinancieraEntidad ifr ON vif.IdInfoFin=ifr.IdInfoFin
INNER JOIN Proveedor.EntidadProvOferente epo ON ifr.IdEntidad=epo.IdEntidad
WHERE epo.IdEntidad =@IdEntidad and vif.ConfirmaYAprueba = 0 and vif.CorreoEnviado != 1

UPDATE Proveedor.ValidarInfoExperienciaEntidad
SET CorreoEnviado = 1
FROM Proveedor.ValidarInfoExperienciaEntidad vee
INNER JOIN Proveedor.InfoExperienciaEntidad iee ON iee.IdExpEntidad=vee.IdExpEntidad
INNER JOIN Proveedor.EntidadProvOferente epo ON iee.IdEntidad=epo.IdEntidad
WHERE epo.IdEntidad=@IdEntidad and vee.ConfirmaYAprueba = 0 and vee.CorreoEnviado != 1


END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]
	@IdEstadoDatosBasicos INT
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
    FROM [Proveedor].[EstadoDatosBasicos] 
    WHERE  IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]
	@IdEstadoDatosBasicos INT
AS
BEGIN
	DELETE Proveedor.EstadoDatosBasicos WHERE IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]
		@IdEstadoDatosBasicos INT OUTPUT, 	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoDatosBasicos(Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoDatosBasicos = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que actualiza un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]
		@IdEstadoDatosBasicos INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.EstadoDatosBasicos 
		SET Descripcion = @Descripcion, 
			Estado = @Estado, 
			UsuarioModifica = @UsuarioModifica, 
			FechaModifica = GETDATE() 
		WHERE IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar]
	@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
     FROM [Proveedor].[EstadoDatosBasicos] 
        WHERE Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
        AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
        ORDER BY Descripcion ASC
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]
	@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
     FROM [Proveedor].[EstadoDatosBasicos] 
        WHERE Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Fabian Valencia>
-- Create date: <12/06/2013>
-- Description:	<Obtiene el estado a través de su id>
-- =============================================
Create PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]
	 @IdTercero INT
AS
BEGIN
	SELECT     Oferente.EstadoTercero.*
	FROM         Oferente.EstadoTercero
	WHERE		Oferente.EstadoTercero.IdEstadoTercero = @IdTercero
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]
	@IdEstadoValidacionDocumental INT
AS
BEGIN
 SELECT IdEstadoValidacionDocumental, CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[EstadoValidacionDocumental] WHERE  IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]
	@IdEstadoValidacionDocumental INT
AS
BEGIN
	DELETE Proveedor.EstadoValidacionDocumental WHERE IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]
		@IdEstadoValidacionDocumental INT OUTPUT, 	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoValidacionDocumental(CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoEstadoValidacionDocumental, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoValidacionDocumental = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que actualiza un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]
		@IdEstadoValidacionDocumental INT,	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.EstadoValidacionDocumental SET CodigoEstadoValidacionDocumental = @CodigoEstadoValidacionDocumental, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]
	@CodigoEstadoValidacionDocumental NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoValidacionDocumental, CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[EstadoValidacionDocumental] WHERE CodigoEstadoValidacionDocumental = CASE WHEN @CodigoEstadoValidacionDocumental IS NULL THEN CodigoEstadoValidacionDocumental ELSE @CodigoEstadoValidacionDocumental END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]
	@IdExpCOD INT
AS
BEGIN
 SELECT IdExpCOD, IdTipoCodUNSPSC, IdExpEntidad, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[ExperienciaCodUNSPSCEntidad] WHERE  IdExpCOD = @IdExpCOD
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que elimina un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]
	@IdExpCOD INT
AS
BEGIN
	DELETE Proveedor.ExperienciaCodUNSPSCEntidad WHERE IdExpCOD = @IdExpCOD
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:19 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar]
		@IdExpCOD INT OUTPUT, 	@IdTipoCodUNSPSC INT,	@IdExpEntidad INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ExperienciaCodUNSPSCEntidad(IdTipoCodUNSPSC, IdExpEntidad, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoCodUNSPSC, @IdExpEntidad, @UsuarioCrea, GETDATE())
	SELECT @IdExpCOD = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:19 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar]
		@IdExpCOD INT,	@IdTipoCodUNSPSC INT,	@IdExpEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.ExperienciaCodUNSPSCEntidad SET IdTipoCodUNSPSC = @IdTipoCodUNSPSC, IdExpEntidad = @IdExpEntidad, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdExpCOD = @IdExpCOD
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]
	@IdExpEntidad INT = NULL
AS
BEGIN
 SELECT IdExpCOD, E.IdTipoCodUNSPSC, IdExpEntidad, E.UsuarioCrea, E.FechaCrea, E.UsuarioModifica, E.FechaModifica,
	T.Codigo, T.Descripcion 
 FROM [Proveedor].[ExperienciaCodUNSPSCEntidad] E
 INNER JOIN [Proveedor].[TipoCodigoUNSPSC] T ON E.IdTipoCodUNSPSC=T.IdTipoCodUNSPSC
 WHERE IdExpEntidad = ISNULL(@IdExpEntidad, IdExpEntidad)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]
	@IdInfoAdmin INT
AS
BEGIN
 SELECT IdInfoAdmin, IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, 
 IdTipoRangosTrabajadores, IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, 
 IdDepartamentoDirComercial, IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, 
 PorctjPublico, SitioWeb, NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, 
 Mision, PQRS, GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, 
 PartMesasTerritoriales, PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, 
 NumSedes, SedesPropias, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[InfoAdminEntidad] 
 WHERE  IdInfoAdmin = @IdInfoAdmin
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT IdInfoAdmin, IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, 
 IdTipoRangosTrabajadores, IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, 
 IdDepartamentoDirComercial, IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, 
 PorctjPublico, SitioWeb, NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, 
 Mision, PQRS, GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, 
 PartMesasTerritoriales, PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, 
 NumSedes, SedesPropias, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[InfoAdminEntidad] 
 WHERE  IdEntidad = @IdEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que elimina un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]
	@IdInfoAdmin INT
AS
BEGIN
	DELETE Proveedor.InfoAdminEntidad WHERE IdInfoAdmin = @IdInfoAdmin
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar]
		@IdInfoAdmin INT OUTPUT, 	
		@IdVigencia INT=1,	
		@IdEntidad INT=NULL,	
		@IdTipoRegTrib INT=NULL,	
		@IdTipoOrigenCapital INT=NULL,	
		@IdTipoActividad INT=NULL,	
		@IdTipoEntidad INT=NULL,	
		@IdTipoNaturalezaJurid INT=NULL,	
		@IdTipoRangosTrabajadores INT=NULL,	
		@IdTipoRangosActivos INT=NULL,	
		@IdRepLegal INT=NULL,	
		@IdTipoCertificaTamano INT=NULL,
		@IdTipoEntidadPublica INT=NULL,	
		@IdDepartamentoConstituida INT=NULL,	
		@IdMunicipioConstituida INT=NULL,	
		@IdDepartamentoDirComercial INT=NULL,	
		@IdMunicipioDirComercial INT=NULL,	
		@DireccionComercial NVARCHAR(50)=NULL,	
		@IdZona INT=NULL,
		@NombreComercial NVARCHAR(128)=NULL,	
		@NombreEstablecimiento NVARCHAR(128)=NULL,	
		@Sigla NVARCHAR(50)=NULL,	
		@PorctjPrivado INT=NULL,	
		@PorctjPublico INT=NULL,	
		@SitioWeb NVARCHAR(128)=NULL,	
		@NombreEntidadAcreditadora NVARCHAR (128)=NULL,	
		@Organigrama BIT=NULL,	
		@TotalPnalAnnoPrevio numeric(10)=NULL,	
		@VincLaboral numeric(10)=NULL,	
		@PrestServicios numeric(10)=NULL,	
		@Voluntariado numeric(10)=NULL,	
		@VoluntPermanente numeric(10)=NULL,	
		@Asociados numeric(10)=NULL,	
		@Mision numeric(10)=NULL,	
		@PQRS BIT=NULL,	
		@GestionDocumental BIT=NULL,	
		@AuditoriaInterna BIT=NULL,	
		@ManProcedimiento BIT=NULL,	
		@ManPracticasAmbiente BIT=NULL,	
		@ManComportOrg BIT=NULL,	
		@ManFunciones BIT=NULL,	
		@ProcRegInfoContable BIT=NULL,	
		@PartMesasTerritoriales BIT=NULL,	
		@PartAsocAgremia BIT=NULL,	
		@PartConsejosComun BIT=NULL,	
		@ConvInterInst BIT=NULL,	
		@ProcSeleccGral BIT=NULL,	
		@ProcSeleccEtnico BIT=NULL,	
		@PlanInduccCapac BIT=NULL,	
		@EvalDesemp BIT=NULL,	
		@PlanCualificacion BIT=NULL,	
		@NumSedes INT=NULL,	
		@SedesPropias BIT=NULL, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.InfoAdminEntidad(IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, IdTipoRangosTrabajadores, IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, IdDepartamentoDirComercial, IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, PorctjPublico, SitioWeb, NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, Mision, PQRS, GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, PartMesasTerritoriales, PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, NumSedes, SedesPropias, UsuarioCrea, FechaCrea)
					  VALUES(@IdVigencia, @IdEntidad, @IdTipoRegTrib, @IdTipoOrigenCapital, @IdTipoActividad, @IdTipoEntidad, @IdTipoNaturalezaJurid, @IdTipoRangosTrabajadores, @IdTipoRangosActivos, @IdRepLegal, @IdTipoCertificaTamano, @IdTipoEntidadPublica, @IdDepartamentoConstituida, @IdMunicipioConstituida, @IdDepartamentoDirComercial, @IdMunicipioDirComercial, @DireccionComercial, @IdZona, @NombreComercial, @NombreEstablecimiento, @Sigla, @PorctjPrivado, @PorctjPublico, @SitioWeb, @NombreEntidadAcreditadora, @Organigrama, @TotalPnalAnnoPrevio, @VincLaboral, @PrestServicios, @Voluntariado, @VoluntPermanente, @Asociados, @Mision, @PQRS, @GestionDocumental, @AuditoriaInterna, @ManProcedimiento, @ManPracticasAmbiente, @ManComportOrg, @ManFunciones, @ProcRegInfoContable, @PartMesasTerritoriales, @PartAsocAgremia, @PartConsejosComun, @ConvInterInst, @ProcSeleccGral, @ProcSeleccEtnico, @PlanInduccCapac, @EvalDesemp, @PlanCualificacion, @NumSedes, @SedesPropias, @UsuarioCrea, GETDATE())
	SELECT @IdInfoAdmin = @@IDENTITY 			
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]
		@IdInfoAdmin  INT = NULL,	@IdVigencia  INT = NULL,	@IdEntidad  INT = NULL,	@IdTipoRegTrib  INT = NULL,	@IdTipoOrigenCapital  INT = NULL,	@IdTipoActividad  INT = NULL,	
		@IdTipoEntidad  INT = NULL,	@IdTipoNaturalezaJurid  INT = NULL,	@IdTipoRangosTrabajadores  INT = NULL,	@IdTipoRangosActivos  INT = NULL,	@IdRepLegal  INT = NULL,	
		@IdTipoCertificaTamano  INT = NULL,	@IdTipoEntidadPublica  INT = NULL,	@IdDepartamentoConstituida  INT = NULL,	@IdMunicipioConstituida  INT = NULL,	@IdDepartamentoDirComercial  INT = NULL,	
		@IdMunicipioDirComercial  INT = NULL,	@DireccionComercial NVARCHAR(50)= NULL,	@IdZona  NVARCHAR(128) = NULL,@NombreComercial NVARCHAR(128)= NULL,
		@NombreEstablecimiento NVARCHAR(128) = NULL,	@Sigla NVARCHAR(50)= NULL,	
		@PorctjPrivado  INT = NULL,	@PorctjPublico  INT = NULL,	@SitioWeb NVARCHAR(128)= NULL,	@NombreEntidadAcreditadora NVARCHAR = NULL,	@Organigrama  BIT = NULL,	
		@TotalPnalAnnoPrevio  numeric(10) = NULL,	@VincLaboral  numeric(10) = NULL,	@PrestServicios  numeric(10) = NULL,	@Voluntariado  numeric(10) = NULL,	
		@VoluntPermanente  numeric(10) = NULL,	@Asociados  numeric(10) = NULL,	@Mision  numeric(10) = NULL,	@PQRS  BIT = NULL,	@GestionDocumental  BIT = NULL,	@Auditoria INT = NULL, @AuditoriaInterna  BIT = NULL,	
		@ManProcedimiento  BIT = NULL,	@ManPracticasAmbiente  BIT = NULL,	@ManComportOrg  BIT = NULL,	@ManFunciones  BIT = NULL,	@ProcRegInfoContable  BIT = NULL,	@PartMesasTerritoriales  BIT = NULL,	
		@PartAsocAgremia  BIT = NULL,	@PartConsejosComun  BIT = NULL,	@Conv INT = NULL ,@ConvInterInst  BIT = NULL,	@ProcSeleccGral  BIT = NULL,	@ProcSeleccEtnico  BIT = NULL,	@PlanInduccCapac  BIT = NULL,	
		@EvalDesemp  BIT = NULL,	@PlanCualificacion  BIT = NULL,	@NumSedes  INT = NULL,	@SedesPropias  BIT = NULL, @UsuarioModifica NVARCHAR(250)= NULL
AS
BEGIN
	UPDATE Proveedor.InfoAdminEntidad 
	SET 
	IdVigencia = ISNULL( @IdVigencia, IdVigencia ),
	IdEntidad = ISNULL( @IdEntidad, IdEntidad ),
	IdTipoRegTrib = ISNULL( @IdTipoRegTrib, IdTipoRegTrib ),
	IdTipoOrigenCapital = ISNULL( @IdTipoOrigenCapital,IdTipoOrigenCapital ),
	IdTipoActividad = ISNULL( @IdTipoActividad, IdTipoActividad ),
	IdTipoEntidad = ISNULL( @IdTipoEntidad, IdTipoEntidad ),
	IdTipoNaturalezaJurid = ISNULL( @IdTipoNaturalezaJurid, IdTipoNaturalezaJurid ),
	IdTipoRangosTrabajadores = ISNULL( @IdTipoRangosTrabajadores, IdTipoRangosTrabajadores ),
	IdTipoRangosActivos = ISNULL( @IdTipoRangosActivos, IdTipoRangosActivos ),
	IdRepLegal = ISNULL( @IdRepLegal, IdRepLegal ),
	IdTipoCertificaTamano = ISNULL( @IdTipoCertificaTamano, IdTipoCertificaTamano ),
	IdTipoEntidadPublica = ISNULL( @IdTipoEntidadPublica, IdTipoEntidadPublica ),
	IdDepartamentoConstituida = ISNULL( @IdDepartamentoConstituida, IdDepartamentoConstituida ),
	IdMunicipioConstituida = ISNULL( @IdMunicipioConstituida, IdMunicipioConstituida ),
	IdDepartamentoDirComercial = ISNULL( @IdDepartamentoDirComercial, IdDepartamentoDirComercial ),
	IdMunicipioDirComercial = ISNULL( @IdMunicipioDirComercial, IdMunicipioDirComercial ),
	DireccionComercial = ISNULL( @DireccionComercial, DireccionComercial ),
	IdZona = ISNULL( @IdZona,  IdZona ),
	NombreComercial = ISNULL( @NombreComercial, NombreComercial ),
	NombreEstablecimiento = ISNULL( @NombreEstablecimiento, NombreEstablecimiento ),
	Sigla = ISNULL( @Sigla, Sigla ),
	PorctjPrivado = ISNULL( @PorctjPrivado, PorctjPrivado ),
	PorctjPublico = ISNULL( @PorctjPublico, PorctjPublico ),
	SitioWeb = ISNULL( @SitioWeb, SitioWeb ),
	NombreEntidadAcreditadora = ISNULL( @NombreEntidadAcreditadora, NombreEntidadAcreditadora ),
	Organigrama = ISNULL( @Organigrama, Organigrama ),
	TotalPnalAnnoPrevio = ISNULL( @TotalPnalAnnoPrevio, TotalPnalAnnoPrevio ),
	VincLaboral = ISNULL( @VincLaboral, VincLaboral ),
	PrestServicios = ISNULL( @PrestServicios, PrestServicios ),
	Voluntariado = ISNULL( @Voluntariado, Voluntariado ),
	VoluntPermanente = ISNULL( @VoluntPermanente, VoluntPermanente ),
	Asociados = ISNULL( @Asociados, Asociados ),
	Mision = ISNULL( @Mision, Mision ),
	PQRS = ISNULL( @PQRS, PQRS ),
	GestionDocumental = ISNULL( @GestionDocumental, GestionDocumental ),
	AuditoriaInterna = ISNULL( @AuditoriaInterna, AuditoriaInterna ),
	ManProcedimiento = ISNULL( @ManProcedimiento, ManProcedimiento ),
	ManPracticasAmbiente = ISNULL( @ManPracticasAmbiente, ManPracticasAmbiente ),
	ManComportOrg = ISNULL( @ManComportOrg, ManComportOrg ),
	ManFunciones = ISNULL( @ManFunciones, ManFunciones ),
	ProcRegInfoContable = ISNULL( @ProcRegInfoContable, ProcRegInfoContable ),
	PartMesasTerritoriales = ISNULL( @PartMesasTerritoriales, PartMesasTerritoriales ),
	PartAsocAgremia = ISNULL( @PartAsocAgremia, PartAsocAgremia ),
	PartConsejosComun = ISNULL( @PartConsejosComun, PartConsejosComun ),
	ConvInterInst = ISNULL( @ConvInterInst, ConvInterInst ),
	ProcSeleccGral = ISNULL( @ProcSeleccGral, ProcSeleccGral ),
	ProcSeleccEtnico = ISNULL( @ProcSeleccEtnico, ProcSeleccEtnico ),
	PlanInduccCapac = ISNULL( @PlanInduccCapac, PlanInduccCapac ),
	EvalDesemp = ISNULL( @EvalDesemp, EvalDesemp ),
	PlanCualificacion = ISNULL( @PlanCualificacion, PlanCualificacion ),
	NumSedes = ISNULL( @NumSedes, NumSedes ),
	SedesPropias = ISNULL( @SedesPropias, SedesPropias ),
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdInfoAdmin = @IdInfoAdmin
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar]
	@IdEntidad INT = NULL
AS
BEGIN
 SELECT IdInfoAdmin, IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, IdTipoRangosTrabajadores, 
 IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, IdDepartamentoDirComercial, 
 IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, PorctjPublico, SitioWeb, 
 NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, Mision, PQRS, 
 GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, PartMesasTerritoriales, 
 PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, NumSedes, SedesPropias, 
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[InfoAdminEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar]
	@IdExpEntidad INT
AS
BEGIN
 SELECT IdExpEntidad, IdEntidad, IdTipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, 
	IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, 
	FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, 
	PorcentParticipacion, AtencionDeptos, JardinOPreJardin, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, NroRevision, Finalizado
 FROM [Proveedor].[InfoExperienciaEntidad] 
 WHERE  IdExpEntidad = @IdExpEntidad
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que elimina un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]
	@IdExpEntidad INT
AS
BEGIN
	DELETE Proveedor.InfoExperienciaEntidad WHERE IdExpEntidad = @IdExpEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar]
		@IdExpEntidad INT OUTPUT, 	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	
		@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	
		@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(256),	
		@FechaInicio DATETIME,	@FechaFin DATETIME, @ExperienciaMeses NUMERIC(18,2),	@NumeroContrato NVARCHAR(128),	
		@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(21,3),	@EstadoDocumental INT,	@UnionTempConsorcio BIT,	
		@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	@JardinOPreJardin BIT, @UsuarioCrea NVARCHAR(250),
		@IdTemporal VARCHAR(50)=NULL,
		@Finalizado BIT
AS
BEGIN
	INSERT INTO Proveedor.InfoExperienciaEntidad(IdEntidad, IdTipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, 
		FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, PorcentParticipacion, AtencionDeptos, JardinOPreJardin, UsuarioCrea, FechaCrea, NroRevision, Finalizado)
	VALUES(@IdEntidad, @IdTipoSector, @IdTipoEstadoExp, @IdTipoModalidadExp, @IdTipoModalidad, @IdTipoPoblacionAtendida, @IdTipoRangoExpAcum, @IdTipoCodUNSPSC, @IdTipoEntidadContratante, @EntidadContratante, 
					  @FechaInicio, @FechaFin, @ExperienciaMeses, @NumeroContrato, @ObjetoContrato, @Vigente, @Cuantia, @EstadoDocumental, @UnionTempConsorcio, @PorcentParticipacion, @AtencionDeptos, @JardinOPreJardin, @UsuarioCrea, GETDATE(),1,@Finalizado)
	
	SELECT @IdExpEntidad = @@IDENTITY
					  
	UPDATE [Proveedor].[DocExperienciaEntidad] 
	SET IdExpEntidad = @IdExpEntidad
	WHERE IdTemporal = @IdTemporal
					  
	SELECT @IdExpEntidad
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar]
		@IdExpEntidad INT,	@IdEntidad INT,	@IdTipoSector INT,	@IdTipoEstadoExp INT,	@IdTipoModalidadExp INT,	
		@IdTipoModalidad INT,	@IdTipoPoblacionAtendida INT,	@IdTipoRangoExpAcum INT,	@IdTipoCodUNSPSC INT,	
		@IdTipoEntidadContratante INT,	@EntidadContratante NVARCHAR(256),	@FechaInicio DATETIME,	@FechaFin DATETIME,	
		@NumeroContrato NVARCHAR(128),	@ObjetoContrato NVARCHAR(256),	@Vigente BIT,	@Cuantia NUMERIC(21,3),	@ExperienciaMeses NUMERIC(21,3),
		@EstadoDocumental INT,	@UnionTempConsorcio BIT,	@PorcentParticipacion NUMERIC(5),	@AtencionDeptos BIT,	
		@JardinOPreJardin BIT, @UsuarioModifica NVARCHAR(250), @Finalizado BIT = NULL
AS
BEGIN
	UPDATE Proveedor.InfoExperienciaEntidad 
	SET IdEntidad = @IdEntidad, IdTipoSector = @IdTipoSector, IdTipoEstadoExp = @IdTipoEstadoExp, 
	IdTipoModalidadExp = @IdTipoModalidadExp, IdTipoModalidad = @IdTipoModalidad, 
	IdTipoPoblacionAtendida = @IdTipoPoblacionAtendida, IdTipoRangoExpAcum = @IdTipoRangoExpAcum, 
	IdTipoCodUNSPSC = @IdTipoCodUNSPSC, IdTipoEntidadContratante = @IdTipoEntidadContratante, 
	EntidadContratante = @EntidadContratante, FechaInicio = @FechaInicio, FechaFin = @FechaFin, 
	NumeroContrato = @NumeroContrato, ObjetoContrato = @ObjetoContrato, Vigente = @Vigente, 
	Cuantia = @Cuantia, ExperienciaMeses = @ExperienciaMeses,
	EstadoDocumental = @EstadoDocumental, UnionTempConsorcio = @UnionTempConsorcio, 
	PorcentParticipacion = @PorcentParticipacion, AtencionDeptos = @AtencionDeptos, 
	JardinOPreJardin = @JardinOPreJardin, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE(), 
	Finalizado = ISNULL(@Finalizado,Finalizado)
	WHERE IdExpEntidad = @IdExpEntidad
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--===================================================================================
--Autor: Mauricio Martinez
--Fecha: 2013/07/30
--Descripcion: Para liberar InfoExperiencia
--===================================================================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]
(@IdEntidad INT, @EstadoDocumental INT, @UsuarioModifica VARCHAR(256), @Finalizado BIT )
as
begin
	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = @Finalizado,
		EstadoDocumental = @EstadoDocumental
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		--and i.EstadoDocumental = 4 --SI ESTA VALIDADO SE FINALIZA
		--and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMó CON SI

end


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:33:04 PM
-- Description:	Procedimiento almacenado que consulta un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar]
	@IdEntidad INT = NULL,
	@EntidadContratante NVARCHAR(128) = NULL,
	@NumeroContrato NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdExpEntidad, IdEntidad, IdTipoSector, TS.Descripcion AS TipoSector, IdTipoEstadoExp, IdTipoModalidadExp, IdTipoModalidad, IdTipoPoblacionAtendida, 
 IdTipoRangoExpAcum, IdTipoCodUNSPSC, IdTipoEntidadContratante, EntidadContratante, FechaInicio, FechaFin, ExperienciaMeses, NumeroContrato, 
 ObjetoContrato, Vigente, Cuantia, EstadoDocumental, UnionTempConsorcio, PorcentParticipacion, AtencionDeptos, JardinOPreJardin, 
 IE.UsuarioCrea, IE.FechaCrea, IE.UsuarioModifica, IE.FechaModifica 
 FROM [Proveedor].[InfoExperienciaEntidad] AS IE
 INNER JOIN [Proveedor].[TipoSectorEntidad] AS TS ON IE.idTipoSector = TS.IdTipoSectorEntidad
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND EntidadContratante = CASE WHEN @EntidadContratante IS NULL THEN EntidadContratante ELSE @EntidadContratante END 
 AND ISNULL(NumeroContrato,0) = CASE WHEN @NumeroContrato IS NULL THEN ISNULL(NumeroContrato,0) ELSE @NumeroContrato END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]
	@IdInfoFin INT
AS
BEGIN
 SELECT IdInfoFin, IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, Finalizado, NroRevision FROM [Proveedor].[InfoFinancieraEntidad] WHERE  IdInfoFin = @IdInfoFin
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/8/2013 12:21:51 PM
-- Description:	Procedimiento almacenado que elimina un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]
	@IdInfoFin INT
AS
BEGIN
	DELETE Proveedor.InfoFinancieraEntidad WHERE IdInfoFin = @IdInfoFin
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]
		@IdInfoFin INT OUTPUT, 	@IdEntidad INT,	@IdVigencia INT,	@ActivoCte NUMERIC(21, 3),	@ActivoTotal NUMERIC(21, 3),	@PasivoCte NUMERIC(21, 3),	@PasivoTotal NUMERIC(21, 3),	@Patrimonio NUMERIC(21, 3),	@GastosInteresFinancieros NUMERIC(21, 3),	@UtilidadOperacional NUMERIC(21, 3),	@ConfirmaIndicadoresFinancieros BIT,	@RupRenovado BIT,	@EstadoValidacion INT,	@ObservacionesInformacionFinanciera NVARCHAR(256),	@ObservacionesValidadorICBF NVARCHAR(256), @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20) = NULL, @Finalizado BIT = NULL
AS
BEGIN
	INSERT INTO Proveedor.InfoFinancieraEntidad(IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, NroRevision, Finalizado)
					  VALUES(@IdEntidad, @IdVigencia, @ActivoCte, @ActivoTotal, @PasivoCte, @PasivoTotal, @Patrimonio, @GastosInteresFinancieros, @UtilidadOperacional, @ConfirmaIndicadoresFinancieros, @RupRenovado, @EstadoValidacion, @ObservacionesInformacionFinanciera, @ObservacionesValidadorICBF, @UsuarioCrea, GETDATE(), 1, @Finalizado)
	SELECT @IdInfoFin = @@IDENTITY 		
	
	UPDATE [Proveedor].[DocFinancieraProv] 
	set IdInfoFin = @IdInfoFin
	where IdTemporal = @IdTemporal
	
	SELECT @IdInfoFin
	
END





GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]
		@IdInfoFin INT,	@IdEntidad INT,	@IdVigencia INT,	@ActivoCte NUMERIC(21, 3),	@ActivoTotal NUMERIC(21, 3),	@PasivoCte NUMERIC(21, 3),	@PasivoTotal NUMERIC(21, 3),	@Patrimonio NUMERIC(21, 3),	@GastosInteresFinancieros NUMERIC(21, 3),	@UtilidadOperacional NUMERIC(21, 3),	@ConfirmaIndicadoresFinancieros BIT,	@RupRenovado BIT,	@EstadoValidacion INT,	@ObservacionesInformacionFinanciera NVARCHAR(256),	@ObservacionesValidadorICBF NVARCHAR(256), @UsuarioModifica NVARCHAR(250), @Finalizado BIT = NULL
AS
BEGIN
	UPDATE Proveedor.InfoFinancieraEntidad SET IdEntidad = @IdEntidad, IdVigencia = @IdVigencia, ActivoCte = @ActivoCte, ActivoTotal = @ActivoTotal, PasivoCte = @PasivoCte, PasivoTotal = @PasivoTotal, Patrimonio = @Patrimonio, GastosInteresFinancieros = @GastosInteresFinancieros, UtilidadOperacional = @UtilidadOperacional, ConfirmaIndicadoresFinancieros = @ConfirmaIndicadoresFinancieros, RupRenovado = @RupRenovado, EstadoValidacion = @EstadoValidacion, ObservacionesInformacionFinanciera = @ObservacionesInformacionFinanciera, ObservacionesValidadorICBF = @ObservacionesValidadorICBF, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() , Finalizado = ISNULL(@Finalizado, Finalizado) WHERE IdInfoFin = @IdInfoFin
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--===================================================================================
--Autor: Mauricio Martinez
--Fecha: 2013/07/30
--Descripcion: Para liberar InfoFinanciera
--===================================================================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar]
(@IdEntidad INT, @EstadoValidacion INT, @UsuarioModifica VARCHAR(256), @Finalizado BIT )
as
begin
	update Proveedor.InfoFinancieraEntidad 
	set Finalizado = @Finalizado,
		EstadoValidacion = @EstadoValidacion,
		UsuarioModifica = @UsuarioModifica
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		--and i.EstadoDocumental = 4 --SI ESTA VALIDADO SE FINALIZA
		--and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMó CON SI

end

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]
	@IdEntidad INT = NULL, @IdVigencia INT = NULL
AS
BEGIN
 SELECT IdInfoFin, IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, Finalizado
 FROM [Proveedor].[InfoFinancieraEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END 
 ORDER BY IdVigencia DESC 
END


GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que guarda un nuevo cambio de estado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]
@IdTercero INT, @DatosBasicos BIT, @Financiera BIT, @Experiencia BIT, @IdTemporal NVARCHAR (20), @Motivo NVARCHAR (128), @UsuarioCrea NVARCHAR (250)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION t1
		INSERT INTO Proveedor.MotivoCambioEstado (IdTercero, IdTemporal, Motivo, DatosBasicos, Financiera, Experiencia, FechaCrea, UsuarioCrea)
			VALUES (@IdTercero, @IdTemporal, @Motivo, @DatosBasicos, @Financiera, @Experiencia, GETDATE(), @UsuarioCrea)


		DECLARE @PorValidar NVARCHAR (10)
		SET @PorValidar = (SELECT
			IdEstadoTercero
		FROM Oferente.EstadoTercero
		WHERE CodigoEstadotercero = '005')
		UPDATE Oferente.TERCERO
		SET IDESTADOTERCERO = @PorValidar
		WHERE IDTERCERO = @IdTercero

		

		--Se obtiene el id de el proveedor para asi actualizar su respectivo estado
		DECLARE @IdEntidad INT
		SET @IdEntidad = (SELECT DISTINCT TOP(1)
			Proveedor.EntidadProvOferente.IdEntidad
		FROM Proveedor.EntidadProvOferente
		INNER JOIN Oferente.TERCERO
			ON Proveedor.EntidadProvOferente.IdTercero = Oferente.TERCERO.IDTERCERO
		WHERE (Oferente.TERCERO.IDTERCERO = @IdTercero))
		
		PRINT 'BIEN'

		--Se actualiza el estado a Datos Básicos
		SET @PorValidar = (SELECT
			IdEstadoDatosBasicos 
		FROM Proveedor.EstadoDatosBasicos    
		WHERE Descripcion  = 'EN VALIDACIÓN')
		
		--Carlos Cubillos: Se establece que independiente de si está marcado DatosBasicos se modifica el estado de este.
		
		--IF @DatosBasicos = 1
		--BEGIN
		--UPDATE Proveedor.EntidadProvOferente
		--SET IdEstado = @PorValidar, Finalizado  = 0
		--WHERE Proveedor.EntidadProvOferente.IdEntidad = @IdEntidad
		--END
		
		UPDATE Proveedor.EntidadProvOferente
		SET IdEstado = @PorValidar, Finalizado  = 0
		WHERE Proveedor.EntidadProvOferente.IdEntidad = @IdEntidad
		
		
		--Se actualiza el estado a Financiera
		SET @PorValidar = (SELECT
			IdEstadoValidacionDocumental 
		FROM Proveedor.EstadoValidacionDocumental
		WHERE CodigoEstadoValidacionDocumental    = '05')
		IF @Financiera = 1
		BEGIN
		UPDATE Proveedor.InfoFinancieraEntidad
		SET EstadoValidacion = @PorValidar, Finalizado  = 0
		WHERE Proveedor.InfoFinancieraEntidad.IdEntidad = @IdEntidad
		END
		
		
		
		--Se actualiza el estado a Experiencia
		--Carlos Cubillos: Se cambio el set antes del if, estaba actualizando siempre
		SET @PorValidar = (SELECT
			IdEstadoValidacionDocumental 
		FROM Proveedor.EstadoValidacionDocumental    
		WHERE CodigoEstadoValidacionDocumental    = '05')
		IF @Experiencia = 1
		BEGIN
		UPDATE Proveedor.InfoExperienciaEntidad
		SET EstadoDocumental = @PorValidar, Finalizado  = 0
		WHERE Proveedor.InfoExperienciaEntidad.IdEntidad = @IdEntidad
		END
		
		
		
		
COMMIT TRANSACTION t1
END TRY
BEGIN CATCH
ROLLBACK TRANSACTION t1
SELECT
	ERROR_NUMBER() AS ErrorNumber,
	ERROR_MESSAGE() AS ErrorMessage;
END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Consultar]
	@IdNiveldegobierno INT
AS
BEGIN
 SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Niveldegobierno] WHERE  IdNiveldegobierno = @IdNiveldegobierno
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que elimina un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]
	@IdNiveldegobierno INT
AS
BEGIN
	DELETE Proveedor.Niveldegobierno WHERE IdNiveldegobierno = @IdNiveldegobierno
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]
		@IdNiveldegobierno INT OUTPUT, 	@CodigoNiveldegobierno NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Niveldegobierno(CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNiveldegobierno, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNiveldegobierno = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Modificar]
		@IdNiveldegobierno INT,	@CodigoNiveldegobierno NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Niveldegobierno SET CodigoNiveldegobierno = @CodigoNiveldegobierno, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNiveldegobierno = @IdNiveldegobierno
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar]
@CodigoNiveldegobierno NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Niveldegobierno] WHERE CodigoNiveldegobierno = CASE WHEN @CodigoNiveldegobierno IS NULL THEN CodigoNiveldegobierno ELSE @CodigoNiveldegobierno END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END

END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]
	
AS
BEGIN

 SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Niveldegobierno] 
 WHERE  Estado =1
 
 END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_IdRamaEstructura]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  7/23/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno por Rama o Estructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_IdRamaEstructura] @IdRamaEstructura INT =0,@idEstado INT =1

					
AS
BEGIN

 SELECT ng.IdNiveldegobierno, ng.CodigoNiveldegobierno, ng.Descripcion, ng.Estado, ng.UsuarioCrea, ng.FechaCrea, ng.UsuarioModifica, ng.FechaModifica 
 FROM Proveedor.Niveldegobierno AS ng
	INNER JOIN Proveedor.RamaoEstructura_Niveldegobierno AS rsng ON ng.IdNiveldegobierno=rsng.IdNiveldegobierno
	WHERE rsng.IdRamaEstructura= @IdRamaEstructura AND   ng.Estado =@idEstado
	ORDER BY Descripcion ASC
 END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]
	@IdNivelOrganizacional INT
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[NivelOrganizacional] WHERE  IdNivelOrganizacional = @IdNivelOrganizacional
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que elimina un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]
	@IdNivelOrganizacional INT
AS
BEGIN
	DELETE Proveedor.NivelOrganizacional WHERE IdNivelOrganizacional = @IdNivelOrganizacional
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]
		@IdNivelOrganizacional INT OUTPUT, 	@CodigoNivelOrganizacional NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.NivelOrganizacional(CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNivelOrganizacional, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNivelOrganizacional = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que actualiza un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Modificar]
		@IdNivelOrganizacional INT,	@CodigoNivelOrganizacional NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.NivelOrganizacional SET CodigoNivelOrganizacional = @CodigoNivelOrganizacional, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNivelOrganizacional = @IdNivelOrganizacional
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar]
	@CodigoNivelOrganizacional NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[NivelOrganizacional] WHERE CodigoNivelOrganizacional = CASE WHEN @CodigoNivelOrganizacional IS NULL THEN CodigoNivelOrganizacional ELSE @CodigoNivelOrganizacional END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All]
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[NivelOrganizacional] 
 WHERE Estado =1
 ORDER BY Descripcion ASC
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]
		@IdNotJudicial INT OUTPUT, 	
		@IdEntidad INT,	
		@IdDepartamento INT,	
		@IdMunicipio INT , 	
		@IdZona INT , 	
		@Direccion NVARCHAR(250) ,  
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.NotificacionJudicial(IdEntidad, IdDepartamento,IdMunicipio,IdZona, Direccion, UsuarioCrea, FechaCrea)
	VALUES(@IdEntidad, @IdDepartamento,@IdMunicipio,@IdZona,@Direccion, @UsuarioCrea, GETDATE())
	SELECT @IdNotJudicial = @@IDENTITY 		
	RETURN @IdNotJudicial
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que actualiza un(a) NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]
		@IdNotJudicial INT,	
		@IdEntidad INT,	
		@IdDepartamento INT,	
		@IdMunicipio INT,	
		@IdZona INT,	
		@Direccion NVARCHAR(250), 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.NotificacionJudicial 
	SET IdEntidad = @IdEntidad, 
	IdDepartamento = @IdDepartamento,
	IdMunicipio	= @IdMunicipio,
	IdZona = @IdZona,
	Direccion = @Direccion,
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdNotJudicial = @IdNotJudicial 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian Valencia
-- Create date: 21/06/2013
-- Description:	Obtiene las notificaiones judiciales del proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param] 
	@IdEntidad INT = NULL
	
AS
BEGIN
	--DECLARE @IdTipoPersona INT 
	--DECLARE @IdTipoIdentificacion INT
	--DECLARE @NumeroIdentificacion NVARCHAR(20) 
	--DECLARE @DV NVARCHAR(2) 
	--DECLARE @Proveedor NVARCHAR(250) 
	--DECLARE @CorreoElectronico NVARCHAR(50)
	
	--SET @IdTipoPersona = null
	--SET @IdTipoIdentificacion = null
	--SET @NumeroIdentificacion = null
	--SET @DV = null
	--SET @Proveedor = ''
	--SET @CorreoElectronico= null
	
	SELECT      Proveedor.NotificacionJudicial.IdNotJudicial,
			   (SELECT DIV.Departamento.NombreDepartamento 
			    FROM DIV.Departamento
			    WHERE DIV.Departamento.IdDepartamento = NotificacionJudicial.IdDepartamento) AS NombreDepartamento,
			   (SELECT DIV.Municipio.NombreMunicipio 
			    FROM DIV.Municipio
			    WHERE DIV.Municipio.IdDepartamento = NotificacionJudicial.IdDepartamento AND
					  DIV.Municipio.IdMunicipio = NotificacionJudicial.IdMunicipio) AS NombreMunicipio,
		        Proveedor.NotificacionJudicial.Direccion, Proveedor.NotificacionJudicial.UsuarioCrea
   FROM         Proveedor.EntidadProvOferente 
				INNER JOIN Oferente.TERCERO 
				ON Proveedor.EntidadProvOferente.IdTercero = Oferente.TERCERO.IDTERCERO 
				INNER JOIN Proveedor.NotificacionJudicial 
				ON Proveedor.EntidadProvOferente.IdEntidad = Proveedor.NotificacionJudicial.IdEntidad AND 
                   Proveedor.EntidadProvOferente.IdEntidad = Proveedor.NotificacionJudicial.IdEntidad	
    WHERE       Proveedor.EntidadProvOferente.IdEntidad  = CASE WHEN @IdEntidad IS NULL THEN Proveedor.EntidadProvOferente.IdEntidad  ELSE @IdEntidad END
				
	
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que consulta un(a) NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicials_Consultar]
	@IdNotJudicial INT= NULL,
	@IdEntidad INT = NULL,
	@IdDepartamento INT = NULL,
	@IdMunicipio INT = NULL
AS
BEGIN
 SELECT IdNotJudicial, 
		IdEntidad, 
		IdDepartamento, 
		IdMunicipio, 
		IdZona, Direccion, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
		FROM [Proveedor].[NotificacionJudicial] 
		WHERE IdNotJudicial = CASE WHEN @IdNotJudicial IS NULL THEN IdNotJudicial ELSE @IdNotJudicial END AND 
			  IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END AND 
			  IdDepartamento = CASE WHEN @IdDepartamento IS NULL THEN IdDepartamento ELSE @IdDepartamento END AND 
			  IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]
	@IdRamaEstructura INT
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[RamaoEstructura] WHERE  IdRamaEstructura = @IdRamaEstructura
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que elimina un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]
	@IdRamaEstructura INT
AS
BEGIN
	DELETE Proveedor.RamaoEstructura WHERE IdRamaEstructura = @IdRamaEstructura
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que guarda un nuevo RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Insertar]
		@IdRamaEstructura INT OUTPUT, 	@CodigoRamaEstructura NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.RamaoEstructura(CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRamaEstructura, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdRamaEstructura = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que actualiza un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]
		@IdRamaEstructura INT,	@CodigoRamaEstructura NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.RamaoEstructura SET CodigoRamaEstructura = @CodigoRamaEstructura, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRamaEstructura = @IdRamaEstructura
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar]
	@CodigoRamaEstructura NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[RamaoEstructura] WHERE CodigoRamaEstructura = CASE WHEN @CodigoRamaEstructura IS NULL THEN CodigoRamaEstructura ELSE @CodigoRamaEstructura END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]
	
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[RamaoEstructura] 
 WHERE Estado = 1
 ORDER BY Descripcion ASC
  END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]
	@IdVigencia INT = NULL
AS
 
 declare @SMLV int
 
 SELECT @SMLV = Valor FROM oferente.SalarioMinimo s
 inner join [Global].[Vigencia] v on [Año] = [AcnoVigencia]
 and v.Activo = 1 and s.Estado = 1
 and @IdVigencia = IdVigencia

 SELECT @SMLV
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Consultar]
	@IdSucursal INT
AS
BEGIN
 SELECT IdSucursal, IdEntidad, Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado,IdZona,Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Sucursal] WHERE  IdSucursal = @IdSucursal
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que elimina un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]
	@IdSucursal INT
AS
BEGIN
	DELETE Proveedor.Sucursal WHERE IdSucursal = @IdSucursal
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]
		@IdSucursal INT OUTPUT, 	@IdEntidad INT, @Nombre NVARCHAR(256), @Indicativo INT,	@Telefono INT,	@Extension NUMERIC(10) = NULL,	@Celular NUMERIC(10),	@Correo NVARCHAR(256),	@Estado INT, @IdZona INT,	@Departamento INT,	@Municipio INT,	@Direccion NVARCHAR(256), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Sucursal(IdEntidad,Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado, IdZona, Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @Nombre, @Indicativo, @Telefono, @Extension, @Celular, @Correo, @Estado, @IdZona, @Departamento, @Municipio, @Direccion, @UsuarioCrea, GETDATE())
	SELECT @IdSucursal = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]
		@IdSucursal INT,	@IdEntidad INT,	@Nombre VARCHAR(256), @Indicativo INT,	@Telefono INT,	@Extension NUMERIC(10) = NULL,	@Celular  NUMERIC(10),	@Correo NVARCHAR(256),	@Estado INT, @IdZona INT,	@Departamento INT,	@Municipio INT,	@Direccion NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Sucursal SET IdEntidad = @IdEntidad, Nombre = @Nombre, Indicativo = @Indicativo, Telefono = @Telefono, Extension = @Extension, Celular = @Celular, Correo = @Correo, Estado = @Estado, IdZona = @IdZona, Departamento = @Departamento, Municipio = @Municipio, Direccion = @Direccion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdSucursal = @IdSucursal
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursals_Consultar]
	@IdEntidad INT = NULL,@Estado INT = NULL
AS
BEGIN
 SELECT IdSucursal, IdEntidad,Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado, IdZona, Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Sucursal] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Consultar]
	@IdTablaParametrica INT
AS
BEGIN
 SELECT IdTablaParametrica, CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TablaParametrica] WHERE  IdTablaParametrica = @IdTablaParametrica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Eliminar]
	@IdTablaParametrica INT
AS
BEGIN
	DELETE Proveedor.TablaParametrica WHERE IdTablaParametrica = @IdTablaParametrica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]
		@IdTablaParametrica INT OUTPUT, 	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TablaParametrica(CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTablaParametrica, @NombreTablaParametrica, @Url, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTablaParametrica = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]
		@IdTablaParametrica INT,	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TablaParametrica SET CodigoTablaParametrica = @CodigoTablaParametrica, NombreTablaParametrica = @NombreTablaParametrica, Url = @Url, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTablaParametrica = @IdTablaParametrica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]
	@CodigoTablaParametrica NVARCHAR(128) = NULL,
	@NombreTablaParametrica NVARCHAR(128) = NULL,
	@Estado					BIT = NULL
AS
BEGIN
	SELECT
		IdTablaParametrica,
		CodigoTablaParametrica,
		NombreTablaParametrica,
		Url,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[TablaParametrica]
	WHERE CodigoTablaParametrica = CASE WHEN @CodigoTablaParametrica IS NULL 
										THEN CodigoTablaParametrica 
										ELSE @CodigoTablaParametrica
								   END
	AND NombreTablaParametrica LIKE '%' + CASE WHEN @NombreTablaParametrica IS NULL 
											   THEN NombreTablaParametrica 
											   ELSE @NombreTablaParametrica
										  END + '%'
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado
		END
	ORDER BY NombreTablaParametrica
END--FIN PP

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]
	@IDTIPODOCIDENTIFICA int = NULL,
	@IDESTADOTERCERO int = NULL,
	@IdTipoPersona int = NULL,
	@NUMEROIDENTIFICACION varchar(30) = NULL,
	@USUARIOCREA varchar(128) = NULL,
	@Tercero varchar(128) = NULL,
	@FECHACREA datetime = NULL
	
AS
BEGIN
	SELECT       Oferente.TERCERO.IDTERCERO, 
				 Oferente.TERCERO.IDTIPODOCIDENTIFICA, 
				 Oferente.TERCERO.IDESTADOTERCERO, 
				 Oferente.TERCERO.IdTipoPersona, 
                 Oferente.TERCERO.NUMEROIDENTIFICACION, 
                 Oferente.TERCERO.DIGITOVERIFICACION, 
                 Oferente.TERCERO.CORREOELECTRONICO, 
                 Oferente.TERCERO.PRIMERNOMBRE, 
                 Oferente.TERCERO.SEGUNDONOMBRE, 
                 Oferente.TERCERO.PRIMERAPELLIDO, 
                 Oferente.TERCERO.SEGUNDOAPELLIDO, 
                 Oferente.TERCERO.RAZONSOCIAL, 
                 Oferente.TERCERO.FECHAEXPEDICIONID, 
                 Oferente.TERCERO.FECHANACIMIENTO, 
                 Oferente.TERCERO.SEXO, 
                 Oferente.TERCERO.FECHACREA, 
                 Oferente.TERCERO.USUARIOCREA, 
                 Oferente.TERCERO.FECHAMODIFICA, 
                 Oferente.TERCERO.USUARIOMODIFICA, 
                 Global.TiposDocumentos.NomTipoDocumento AS NombreTipoIdentificacionPersonaNatural, 
                 Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural, 
                 Oferente.EstadoTercero.CodigoEstadotercero, 
                 Oferente.EstadoTercero.DescripcionEstado, 
                 Oferente.TipoPersona.CodigoTipoPersona, 
                 Oferente.TipoPersona.NombreTipoPersona
	FROM         Oferente.TERCERO 
				 INNER JOIN Global.TiposDocumentos  ON 
				 Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento 
				 INNER JOIN Oferente.EstadoTercero ON 
				 Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero 
				 INNER JOIN Oferente.TipoPersona ON 
				 Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
	WHERE  
				Oferente.TERCERO.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END
				AND Oferente.TERCERO.IDESTADOTERCERO = CASE WHEN @IDESTADOTERCERO IS NULL THEN Oferente.TERCERO.IDESTADOTERCERO ELSE @IDESTADOTERCERO END
				AND Oferente.TERCERO.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona END 
				AND Oferente.TERCERO.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END 
				AND Oferente.TERCERO.USUARIOCREA = CASE WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA END 
				AND (
				isnull(Oferente.TERCERO.PRIMERNOMBRE,'') + ' ' + isnull(Oferente.TERCERO.SEGUNDONOMBRE,'') + ' ' + isnull(Oferente.TERCERO.PRIMERAPELLIDO,'') + ' ' + isnull(Oferente.TERCERO.SEGUNDOAPELLIDO,'') + ' '+ isnull(Oferente.TERCERO.RAZONSOCIAL,'')  LIKE CASE WHEN @Tercero IS NULL THEN isnull(Oferente.TERCERO.PRIMERNOMBRE,'') + ' ' + isnull(Oferente.TERCERO.SEGUNDONOMBRE,'') + ' ' + isnull(Oferente.TERCERO.PRIMERAPELLIDO,'') + ' ' + isnull(Oferente.TERCERO.SEGUNDOAPELLIDO,'') + ' '+ isnull(Oferente.TERCERO.RAZONSOCIAL,'') ELSE  '%'+ @Tercero +'%' END					
				)
				AND CAST(Oferente.TERCERO.FECHACREA AS date) = CASE WHEN @FECHACREA IS NULL THEN CAST(Oferente.TERCERO.FECHACREA AS date) ELSE cast(@FECHACREA AS date) END 	
	
     
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero y los proveedores que tengan estado validado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]

@isTercero int = NULL,
@IDTIPODOCIDENTIFICA int = NULL,
@IdTipoPersona int = NULL,
@NUMEROIDENTIFICACION varchar (30) = NULL,
@USUARIOCREA varchar (128) = NULL,
@Tercero varchar (128) = NULL
AS
BEGIN
IF @isTercero = 1
BEGIN
SELECT DISTINCT
	Oferente.TERCERO.IDTERCERO,
	Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural,
	Oferente.TERCERO.NUMEROIDENTIFICACION,
	Oferente.TERCERO.PRIMERNOMBRE,
	Oferente.TERCERO.SEGUNDONOMBRE,
	Oferente.TERCERO.PRIMERAPELLIDO,
	Oferente.TERCERO.SEGUNDOAPELLIDO,
	Oferente.TERCERO.RAZONSOCIAL,
	Oferente.TERCERO.FECHACREA,
	Oferente.EstadoTercero.CodigoEstadotercero,
	Oferente.EstadoTercero.DescripcionEstado
FROM Oferente.TERCERO
INNER JOIN Global.TiposDocumentos
	ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
INNER JOIN Oferente.EstadoTercero
	ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
INNER JOIN Oferente.TipoPersona
	ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
WHERE Oferente.TERCERO.IDTIPODOCIDENTIFICA =
	CASE
		WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
	END
AND Oferente.TERCERO.IdTipoPersona =
	CASE
		WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
	END
AND Oferente.TERCERO.NUMEROIDENTIFICACION =
	CASE
		WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
	END
--AND Oferente.TERCERO.USUARIOCREA =
--	CASE
--		WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
--	END
AND (
ISNULL(Oferente.TERCERO.PRIMERNOMBRE, '') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDONOMBRE, '') + ' ' + ISNULL(Oferente.TERCERO.PRIMERAPELLIDO, '') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDOAPELLIDO, '') + ' ' + ISNULL(Oferente.TERCERO.RAZONSOCIAL, '') LIKE CASE
	WHEN @Tercero IS NULL THEN ISNULL(Oferente.TERCERO.PRIMERNOMBRE, '') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDONOMBRE, '') + ' ' + ISNULL(Oferente.TERCERO.PRIMERAPELLIDO, '') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDOAPELLIDO, '') + ' ' + ISNULL(Oferente.TERCERO.RAZONSOCIAL, '') ELSE '%' + @Tercero + '%'
END
)
AND Oferente.EstadoTercero.CodigoEstadotercero = '004' --Estado validado
END
ELSE
BEGIN
DECLARE @Validado INT
SET @Validado = (SELECT
	IdEstadoDatosBasicos
FROM Proveedor.EstadoDatosBasicos
WHERE Descripcion = 'VALIDADO')
PRINT @Validado

/*Estado validado*/


SELECT DISTINCT
	IdTercero,
	CodDocumento AS CodigoTipoIdentificacionPersonaNatural,
	NUMEROIDENTIFICACION,
	PRIMERNOMBRE,
	SEGUNDONOMBRE,
	PRIMERAPELLIDO,
	SEGUNDOAPELLIDO,
	RAZONSOCIAL,
	FECHACREA,
	IdEstado AS CodigoEstadotercero,
	Estado AS DescripcionEstado
FROM (SELECT
	EP.IdEntidad,
	EP.ConsecutivoInterno,
	T.IdTipoPersona,
	Tp.NombreTipoPersona,
	TD.CodDocumento,
	T.IDTIPODOCIDENTIFICA,
	T.NUMEROIDENTIFICACION AS NumeroIdentificacion,
	EP.IdTercero,
	(T.PRIMERNOMBRE + ' ' + ISNULL(T.SEGUNDONOMBRE, '') + ' ' + T.PRIMERAPELLIDO + ' ' + ISNULL(T.SEGUNDOAPELLIDO, '')) AS Razonsocila,
	E.Descripcion AS Estado,
	EP.IdEstado,
	EP.ObserValidador,
	EP.UsuarioCrea,
	T.PRIMERNOMBRE,
	T.SEGUNDONOMBRE,
	T.PRIMERAPELLIDO,
	T.SEGUNDOAPELLIDO,
	T.RAZONSOCIAL,
	EP.FechaCrea
FROM [Proveedor].[EntidadProvOferente] EP
INNER JOIN oferente.TERCERO T
	ON EP.IdTercero = T.IDTERCERO
INNER JOIN Proveedor.InfoAdminEntidad IAE
	ON EP.IdEntidad = IAE.IdEntidad
INNER JOIN Oferente.TipoPersona TP
	ON T.IdTipoPersona = TP.IdTipoPersona
INNER JOIN [Global].TiposDocumentos TD
	ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento
INNER JOIN Proveedor.EstadoDatosBasicos E
	ON EP.IdEstado = E.IdEstadoDatosBasicos
WHERE T.IdTipoPersona = 1 UNION SELECT
	EP.IdEntidad,
	EP.ConsecutivoInterno,
	T.IdTipoPersona,
	Tp.NombreTipoPersona,
	TD.CodDocumento,
	T.IDTIPODOCIDENTIFICA,
	T.NUMEROIDENTIFICACION AS NumeroIdentificacion,
	EP.IdTercero,
	(T.RAZONSOCIAL) AS Razonsocila,
	E.Descripcion AS Estado,
	EP.IdEstado,
	EP.ObserValidador,
	EP.UsuarioCrea,
	T.PRIMERNOMBRE,
	T.SEGUNDONOMBRE,
	T.PRIMERAPELLIDO,
	T.SEGUNDOAPELLIDO,
	T.RAZONSOCIAL,
	EP.FechaCrea
FROM [Proveedor].[EntidadProvOferente] EP
INNER JOIN oferente.TERCERO T
	ON EP.IdTercero = T.IDTERCERO
LEFT JOIN Proveedor.InfoAdminEntidad IAE
	ON EP.IdEntidad = IAE.IdEntidad
INNER JOIN Oferente.TipoPersona TP
	ON T.IdTipoPersona = TP.IdTipoPersona
INNER JOIN [Global].TiposDocumentos TD
	ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento
INNER JOIN Proveedor.EstadoDatosBasicos E
	ON EP.IdEstado = E.IdEstadoDatosBasicos
WHERE T.IdTipoPersona = 2)
DT

WHERE IdTipoPersona =
	CASE
		WHEN @IdTipoPersona IS NULL THEN IdTipoPersona ELSE @IdTipoPersona
	END
AND IDTIPODOCIDENTIFICA =
	CASE
		WHEN @IDTIPODOCIDENTIFICA IS NULL THEN IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
	END
AND NUMEROIDENTIFICACION =
	CASE
		WHEN @NUMEROIDENTIFICACION IS NULL THEN NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
	END
AND (Razonsocila) LIKE CASE
	WHEN @Tercero IS NULL THEN (Razonsocila) ELSE '%' + @Tercero + '%'
END
AND IdEstado =
	CASE
		WHEN @Validado IS NULL THEN IdEstado ELSE @Validado
	END
--AND UsuarioCrea =
--	CASE
--		WHEN @UsuarioCrea IS NULL THEN UsuarioCrea ELSE @UsuarioCrea
--	END



END

END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Terceros_Modificar_Estado]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013-07-
-- Description:	Procedimiento almacenado que cambia el estado del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Modificar_Estado]
	@IDTERCERO int ,
	@IDESTADOTERCERO int,
	@USUARIOMODIFICA varchar(100)
AS
BEGIN
	UPDATE 
		Oferente.TERCERO
	SET 
		Oferente.TERCERO.IDESTADOTERCERO = @IDESTADOTERCERO, 
		Oferente.TERCERO.USUARIOMODIFICA = @USUARIOMODIFICA,
		Oferente.TERCERO.FECHAMODIFICA = GETDATE()
	WHERE
		Oferente.TERCERO.IDTERCERO = @IDTERCERO
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar]
	@IdTipoCargoEntidad INT
AS
BEGIN
 SELECT IdTipoCargoEntidad, CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCargoEntidad] 
 WHERE  IdTipoCargoEntidad = @IdTipoCargoEntidad
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]
	@IdTipoCargoEntidad INT
AS
BEGIN
	DELETE Proveedor.TipoCargoEntidad 
	WHERE IdTipoCargoEntidad = @IdTipoCargoEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
		@IdTipoCargoEntidad INT OUTPUT, @CodigoTipoCargoEntidad NVARCHAR(10),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCargoEntidad(CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoCargoEntidad,@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCargoEntidad = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar]
		@IdTipoCargoEntidad INT, @CodigoTipoCargoEntidad NVARCHAR(10),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCargoEntidad SET CodigoTipoCargoEntidad = @CodigoTipoCargoEntidad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoCargoEntidad = @IdTipoCargoEntidad
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]
	@CodigoTipoCargoEntidad NVARCHAR(128) = NULL, @Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCargoEntidad, CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCargoEntidad] 
 WHERE CodigoTipoCargoEntidad LIKE CASE WHEN @CodigoTipoCargoEntidad IS NULL THEN CodigoTipoCargoEntidad ELSE @CodigoTipoCargoEntidad + '%' END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Consultar]
	@IdTipoCiiu INT
AS
BEGIN
 SELECT IdTipoCiiu, CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoCiiu] WHERE  IdTipoCiiu = @IdTipoCiiu
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]
	@IdTipoCiiu INT
AS
BEGIN
	DELETE Proveedor.TipoCiiu WHERE IdTipoCiiu = @IdTipoCiiu
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]
		@IdTipoCiiu INT OUTPUT, 	@CodigoCiiu NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCiiu(CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoCiiu, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCiiu = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]
		@IdTipoCiiu INT,	@CodigoCiiu NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCiiu SET CodigoCiiu = @CodigoCiiu, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoCiiu = @IdTipoCiiu
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]
	@CodigoCiiu NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCiiu, CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
			FROM [Proveedor].[TipoCiiu] 
			WHERE 
				CodigoCiiu LIKE CASE WHEN @CodigoCiiu IS NULL THEN CodigoCiiu ELSE '%' + @CodigoCiiu + '%' END 
				AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE '%' + @Descripcion + '%' END 
				AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]
	@IdTipoCodUNSPSC INT
AS
BEGIN
 SELECT IdTipoCodUNSPSC, Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCodigoUNSPSC] 
 WHERE  IdTipoCodUNSPSC = @IdTipoCodUNSPSC
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Eliminar]
	@IdTipoCodUNSPSC INT
AS
BEGIN
	DELETE Proveedor.TipoCodigoUNSPSC 
	WHERE IdTipoCodUNSPSC = @IdTipoCodUNSPSC
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]
		@IdTipoCodUNSPSC INT OUTPUT, @Codigo NVARCHAR(64),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCodigoUNSPSC(Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Codigo, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCodUNSPSC = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar]
		@IdTipoCodUNSPSC INT, @Codigo NVARCHAR(64),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCodigoUNSPSC 
	SET Codigo = @Codigo, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdTipoCodUNSPSC = @IdTipoCodUNSPSC
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar]
	@Codigo NVARCHAR(64) = NULL, @Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL,
	@LongitudUNSPSC INT = NULL
AS
BEGIN
 SELECT IdTipoCodUNSPSC, Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCodigoUNSPSC] 
 WHERE Codigo LIKE CASE WHEN @Codigo IS NULL THEN Codigo ELSE @Codigo + '%' END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
 AND (CASE WHEN @LongitudUNSPSC IS NOT NULL 
		THEN (CASE WHEN LEN(Codigo)<=@LongitudUNSPSC THEN 1 ELSE 0 END) 
		ELSE 1 END)=1
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]
	@IdTipodeActividad INT
AS
BEGIN
 SELECT IdTipodeActividad, CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeActividad] WHERE  IdTipodeActividad = @IdTipodeActividad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll]
	
AS
BEGIN
 SELECT IdTipodeActividad, CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipodeActividad] 
 WHERE  estado = 1
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]
	@IdTipodeActividad INT
AS
BEGIN
	DELETE Proveedor.TipodeActividad WHERE IdTipodeActividad = @IdTipodeActividad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Insertar]
		@IdTipodeActividad INT OUTPUT, 	@CodigoTipodeActividad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipodeActividad(CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipodeActividad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipodeActividad = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]
		@IdTipodeActividad INT,	@CodigoTipodeActividad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipodeActividad SET CodigoTipodeActividad = @CodigoTipodeActividad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipodeActividad = @IdTipodeActividad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]
	@CodigoTipodeActividad NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipodeActividad, CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeActividad] WHERE CodigoTipodeActividad = CASE WHEN @CodigoTipodeActividad IS NULL THEN CodigoTipodeActividad ELSE @CodigoTipodeActividad END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]
	@IdTipodeentidadPublica INT
AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeentidadPublica] WHERE  IdTipodeentidadPublica = @IdTipodeentidadPublica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]
	@IdTipodeentidadPublica INT
AS
BEGIN
	DELETE Proveedor.TipodeentidadPublica WHERE IdTipodeentidadPublica = @IdTipodeentidadPublica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]
		@IdTipodeentidadPublica INT OUTPUT, 	@CodigoTipodeentidadPublica NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipodeentidadPublica(CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipodeentidadPublica, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipodeentidadPublica = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar]
		@IdTipodeentidadPublica INT,	@CodigoTipodeentidadPublica NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipodeentidadPublica SET CodigoTipodeentidadPublica = @CodigoTipodeentidadPublica, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipodeentidadPublica = @IdTipodeentidadPublica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar]
	@CodigoTipodeentidadPublica NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipodeentidadPublica] 
 WHERE CodigoTipodeentidadPublica = CASE WHEN @CodigoTipodeentidadPublica IS NULL THEN CodigoTipodeentidadPublica ELSE @CodigoTipodeentidadPublica END 
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
 ORDER BY Descripcion ASC
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar_All]
	AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipodeentidadPublica] 
 WHERE  Estado = 1 
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar]
	@IdTipoDocIdentifica INT
AS
BEGIN
 SELECT IdTipoDocIdentifica, CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoDocIdentifica] 
 WHERE  IdTipoDocIdentifica = @IdTipoDocIdentifica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date: 22/06/2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam] 
	@IdDocumento int,
	@Programa NVARCHAR(30) 
AS
BEGIN
	DECLARE @IdPrograma INT
	SET @IdPrograma = (SELECT DISTINCT IdPrograma  FROM SEG.Programa WHERE CodigoPrograma = @Programa)
	
	
	SELECT     Proveedor.TipoDocumentoPrograma.IdTipoDocumento,  
			   Proveedor.TipoDocumentoPrograma.MaxPermitidoKB, 
               Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas, 
               Proveedor.TipoDocumento.CodigoTipoDocumento, 
               Proveedor.TipoDocumento.Descripcion, 
               Proveedor.TipoDocumento.Estado, 
               Proveedor.TipoDocumento.UsuarioCrea, 
               Proveedor.TipoDocumento.FechaCrea
	FROM       Proveedor.TipoDocumentoPrograma 
			   INNER JOIN
			  Proveedor.TipoDocumento ON 
			  Proveedor.TipoDocumentoPrograma.IdTipoDocumento = Proveedor.TipoDocumento.IdTipoDocumento
	WHERE     (Proveedor.TipoDocumentoPrograma.IdTipoDocumento = @IdDocumento) AND (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que elimina un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]
	@IdTipoDocIdentifica INT
AS
BEGIN
	DELETE Proveedor.TipoDocIdentifica WHERE IdTipoDocIdentifica = @IdTipoDocIdentifica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]
		@IdTipoDocIdentifica INT OUTPUT, 	@CodigoDocIdentifica NUMERIC,	@Descripcion NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoDocIdentifica(CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoDocIdentifica, @Descripcion, @UsuarioCrea, GETDATE())
	SELECT @IdTipoDocIdentifica = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar]
		@IdTipoDocIdentifica INT,	@CodigoDocIdentifica NUMERIC,	@Descripcion NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoDocIdentifica 
	SET CodigoDocIdentifica = @CodigoDocIdentifica, Descripcion = @Descripcion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdTipoDocIdentifica = @IdTipoDocIdentifica
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar]
	@CodigoDocIdentifica NUMERIC = NULL,@Descripcion NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdTipoDocIdentifica, CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoDocIdentifica] 
 WHERE CodigoDocIdentifica = CASE WHEN @CodigoDocIdentifica IS NULL THEN CodigoDocIdentifica ELSE @CodigoDocIdentifica END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]
	@IdTipoDocumento INT =NULL
AS
BEGIN
	SELECT     Proveedor.TipoDocumento.*
	FROM         Proveedor.TipoDocumento
	WHERE IdTipoDocumento =
	CASE
		WHEN @IdTipoDocumento IS NULL THEN IdTipoDocumento ELSE @IdTipoDocumento
	END
	AND Estado = 1
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]
@IdTipoDocumentoPrograma INT
AS
BEGIN
SELECT
	IdTipoDocumentoPrograma,
	IdTipoDocumento,
	IdPrograma,
	Estado,
	MaxPermitidoKB,
	ExtensionesPermitidas,
	ObligRupNoRenovado,
	ObligRupRenovado,
	ObligPersonaJuridica,
	ObligPersonaNatural,
	ObligSectorPublico,
	ObligSectorPrivado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[TipoDocumentoPrograma]
WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma 
--AND Estado = 1
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date: 28/06/2013
-- Description:	Obtiene los programas del proveedor
-- =============================================
CREATE PROCEDURE  [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos] 
	@idModulo INT
AS
BEGIN
SELECT     IdPrograma, IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion, VisibleMenu,
                       generaLog, Padre
FROM         SEG.Programa
WHERE     (CodigoPrograma  in ( 'PROVEEDOR/DocFinancieraProv','PROVEEDOR/GestionProveedores','PROVEEDOR/GestionTercero', 'PROVEEDOR/INFOEXPERIENCIAENTIDAD' ) AND
			IdModulo = @idModulo)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]
@IdTipoDocumentoPrograma INT
AS
BEGIN
	UPDATE Proveedor.TipoDocumentoPrograma
	SET Estado = 0
	WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]
@IdTipoDocumentoPrograma INT OUTPUT, @IdTipoDocumento INT, @IdPrograma INT, @Estado INT, @MaxPermitidoKB INT, @ExtensionesPermitidas NVARCHAR(50), @ObligRupNoRenovado INT, @ObligRupRenovado INT, @ObligPersonaJuridica INT, @ObligPersonaNatural INT,  @ObligSectorPrivado INT, @ObligSectorPublico INT, @UsuarioCrea NVARCHAR (250)
AS
BEGIN
INSERT INTO Proveedor.TipoDocumentoPrograma (IdTipoDocumento, IdPrograma, Estado, MaxPermitidoKB, ExtensionesPermitidas, ObligRupNoRenovado, ObligRupRenovado, ObligPersonaJuridica, ObligPersonaNatural, ObligSectorPrivado, ObligSectorPublico, UsuarioCrea, FechaCrea)
	VALUES (@IdTipoDocumento, @IdPrograma, 1, @MaxPermitidoKB, @ExtensionesPermitidas, @ObligRupNoRenovado, @ObligRupRenovado, @ObligPersonaJuridica, @ObligPersonaNatural, @ObligSectorPrivado, @ObligSectorPublico, @UsuarioCrea, GETDATE())
SELECT
	@IdTipoDocumentoPrograma = @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]
@IdTipoDocumentoPrograma INT, @IdTipoDocumento INT, @IdPrograma INT, @Estado INT, @MaxPermitidoKB INT, @ExtensionesPermitidas NVARCHAR (50), @ObligRupNoRenovado INT, @ObligRupRenovado INT, @ObligPersonaJuridica INT, @ObligPersonaNatural INT, @ObligSectorPrivado INT, @ObligSectorPublico INT, @UsuarioModifica NVARCHAR (250)
AS
BEGIN
UPDATE Proveedor.TipoDocumentoPrograma
SET	IdTipoDocumento = @IdTipoDocumento,
	IdPrograma = @IdPrograma,
	Estado = @Estado,
	MaxPermitidoKB = @MaxPermitidoKB,
	ExtensionesPermitidas = @ExtensionesPermitidas,
	ObligRupNoRenovado = @ObligRupNoRenovado,
	ObligRupRenovado = @ObligRupRenovado,
	ObligPersonaJuridica = @ObligPersonaJuridica,
	ObligPersonaNatural = @ObligPersonaNatural,
	ObligSectorPublico = @ObligSectorPublico,
	ObligSectorPrivado = @ObligSectorPrivado,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fabian valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar]
@IdTipoDocumentoPrograma INT = NULL, @IdTipoDocumento INT = NULL, @IdPrograma INT = NULL
AS
BEGIN
SELECT
	Proveedor.TipoDocumentoPrograma.IdTipoDocumentoPrograma,
	Proveedor.TipoDocumentoPrograma.IdTipoDocumento,
	Proveedor.TipoDocumentoPrograma.IdPrograma,
	Proveedor.TipoDocumentoPrograma.Estado,
	Proveedor.TipoDocumentoPrograma.MaxPermitidoKB,
	Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas,
	Proveedor.TipoDocumentoPrograma.ObligRupNoRenovado,
	Proveedor.TipoDocumentoPrograma.ObligRupRenovado,
	Proveedor.TipoDocumentoPrograma.ObligPersonaJuridica,
	Proveedor.TipoDocumentoPrograma.ObligPersonaNatural,
	Proveedor.TipoDocumentoPrograma.ObligSectorPublico,
	Proveedor.TipoDocumentoPrograma.ObligSectorPrivado,
	Proveedor.TipoDocumentoPrograma.UsuarioCrea,
	Proveedor.TipoDocumentoPrograma.FechaCrea,
	Proveedor.TipoDocumentoPrograma.UsuarioModifica,
	Proveedor.TipoDocumentoPrograma.FechaModifica,
	Proveedor.TipoDocumento.CodigoTipoDocumento,
	Proveedor.TipoDocumento.Descripcion,
	SEG.Programa.CodigoPrograma,
	SEG.Programa.NombrePrograma
FROM Proveedor.TipoDocumentoPrograma
INNER JOIN Proveedor.TipoDocumento
	ON Proveedor.TipoDocumentoPrograma.IdTipoDocumento = Proveedor.TipoDocumento.IdTipoDocumento
INNER JOIN SEG.Programa
	ON Proveedor.TipoDocumentoPrograma.IdPrograma = SEG.Programa.IdPrograma 
WHERE    IdTipoDocumentoPrograma =
	CASE
		WHEN @IdTipoDocumentoPrograma IS NULL THEN IdTipoDocumentoPrograma ELSE @IdTipoDocumentoPrograma
END AND Proveedor.TipoDocumentoPrograma.IdTipoDocumento =
	CASE
		WHEN @IdTipoDocumento IS NULL THEN Proveedor.TipoDocumentoPrograma.IdTipoDocumento ELSE @IdTipoDocumento
END AND Proveedor.TipoDocumentoPrograma.IdPrograma =
	CASE
		WHEN @IdPrograma IS NULL THEN Proveedor.TipoDocumentoPrograma.IdPrograma ELSE @IdPrograma
END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Consultar]
	@IdTipoentidad INT
AS
BEGIN
 SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Tipoentidad] WHERE  IdTipoentidad = @IdTipoentidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que elimina un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Eliminar]
	@IdTipoentidad INT
AS
BEGIN
	DELETE Proveedor.Tipoentidad WHERE IdTipoentidad = @IdTipoentidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Insertar]
		@IdTipoentidad INT OUTPUT, 	@CodigoTipoentidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Tipoentidad(CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoentidad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoentidad = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]
		@IdTipoentidad INT,	@CodigoTipoentidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Tipoentidad SET CodigoTipoentidad = @CodigoTipoentidad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoentidad = @IdTipoentidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar]
	@CodigoTipoentidad NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[Tipoentidad] WHERE CodigoTipoentidad = CASE WHEN @CodigoTipoentidad IS NULL THEN CodigoTipoentidad ELSE @CodigoTipoentidad END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]	
AS
BEGIN
 SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Tipoentidad]
 WHERE Estado=1
 ORDER BY Descripcion ASC
 
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]
	@IdTipoRegimenTributario INT
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t 
 ON trt.IdTipoPersona = t.IdTipoPersona
 --SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 --FROM [Proveedor].[TipoRegimenTributario] 
 WHERE  trt.IdTipoRegimenTributario = @IdTipoRegimenTributario
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar]
	@IdTipoRegimenTributario INT
AS
BEGIN
	DELETE Proveedor.TipoRegimenTributario WHERE IdTipoRegimenTributario = @IdTipoRegimenTributario
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]
		@IdTipoRegimenTributario INT OUTPUT, 	@CodigoRegimenTributario NVARCHAR(128),	@Descripcion NVARCHAR(128),	@IdTipoPersona INT,	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoRegimenTributario(CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRegimenTributario, @Descripcion, @IdTipoPersona, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoRegimenTributario = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar]
		@IdTipoRegimenTributario INT,	@CodigoRegimenTributario NVARCHAR(128),	@Descripcion NVARCHAR(128),	@IdTipoPersona INT,	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoRegimenTributario SET CodigoRegimenTributario = @CodigoRegimenTributario, Descripcion = @Descripcion, IdTipoPersona = @IdTipoPersona, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoRegimenTributario = @IdTipoRegimenTributario
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]
	@CodigoRegimenTributario NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@IdTipoPersona INT = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE CodigoRegimenTributario = CASE WHEN @CodigoRegimenTributario IS NULL THEN CodigoRegimenTributario ELSE @CodigoRegimenTributario END 
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
 AND trt.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN trt.IdTipoPersona ELSE @IdTipoPersona END 
 AND trt.Estado = CASE WHEN @Estado IS NULL THEN trt.Estado ELSE @Estado END
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]
	@IdTipoPersona INT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE t.IdTipoPersona = @IdTipoPersona 
 ORDER BY Descripcion
 END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar]
	@IdTipoSectorEntidad INT
AS
BEGIN
 SELECT IdTipoSectorEntidad, CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoSectorEntidad] WHERE  IdTipoSectorEntidad = @IdTipoSectorEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]
	AS
BEGIN
 SELECT IdTipoSectorEntidad,Descripcion FROM [Proveedor].[TipoSectorEntidad] WHERE  Estado = 1
 ORDER BY Descripcion
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que elimina un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]
	@IdTipoSectorEntidad INT
AS
BEGIN
	DELETE Proveedor.TipoSectorEntidad WHERE IdTipoSectorEntidad = @IdTipoSectorEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]
		@IdTipoSectorEntidad INT OUTPUT, 	@CodigoSectorEntidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoSectorEntidad(CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoSectorEntidad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoSectorEntidad = @@IDENTITY 		
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]
		@IdTipoSectorEntidad INT,	@CodigoSectorEntidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoSectorEntidad SET CodigoSectorEntidad = @CodigoSectorEntidad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoSectorEntidad = @IdTipoSectorEntidad
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]
	@CodigoSectorEntidad NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoSectorEntidad, CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoSectorEntidad] WHERE CodigoSectorEntidad = CASE WHEN @CodigoSectorEntidad IS NULL THEN CodigoSectorEntidad ELSE @CodigoSectorEntidad END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]
	@IdValidarInfoDatosBasicosEntidad INT
AS
BEGIN
 SELECT IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] WHERE  IdValidarInfoDatosBasicosEntidad = @IdValidarInfoDatosBasicosEntidad
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoDatosBasicosEntidad por IdInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] 
 WHERE  IdEntidad = @IdEntidad
 ORDER BY IdValidarInfoDatosBasicosEntidad DESC
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima] (@IdEntidad INT)
as
begin
	select top 1
		v.IdValidarInfoDatosBasicosEntidad,
		e.IdEntidad,
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
	from  Proveedor.EntidadProvOferente e
		inner join Proveedor.ValidarInfoDatosBasicosEntidad v
		on v.IdEntidad = e.IdEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
	order by NroRevision DESC
end

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]
		@IdValidarInfoDatosBasicosEntidad INT OUTPUT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoDatosBasicosEntidad(IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoDatosBasicosEntidad = @@IDENTITY 		
END





GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar]
		@IdValidarInfoDatosBasicosEntidad INT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoDatosBasicosEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdEntidad = @IdEntidad
		and NroRevision = @NroRevision			
		and IdValidarInfoDatosBasicosEntidad = @IdValidarInfoDatosBasicosEntidad
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]
		@IdEntidad INT, @IdEstadoInfoDatosBasicosEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[EntidadProvOferente] 
	SET IdEstado = @IdEstadoInfoDatosBasicosEntidad, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdEntidad = @IdEntidad
END







GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar]
	@IdEntidad int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
 ORDER BY FechaCrea DESC
END





GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoDatosBasicosEntidad, v.IdEntidad, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoDatosBasicosEntidad v
			left join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = v.IdEntidad
			where p.IdEntidad  = @IdEntidad
			--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
			ORDER BY v.FechaCrea DESC
END








GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]
	@IdValidarInfoExperienciaEntidad INT
AS
BEGIN
 SELECT IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoExperienciaEntidad] WHERE  IdValidarInfoExperienciaEntidad = @IdValidarInfoExperienciaEntidad
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoExperienciaEntidad por IdInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]
	@IdExpEntidad INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoExperienciaEntidad] 
 WHERE  IdExpEntidad = @IdExpEntidad
 ORDER BY IdValidarInfoExperienciaEntidad DESC
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoExperienciaEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima] (@IdEntidad INT, @IdExpEntidad INT)
as
begin
	select top 1
		v.IdValidarInfoExperienciaEntidad,
		e.IdEntidad,
		i.IdExpEntidad,		
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
			from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and i.IdExpEntidad = @IdExpEntidad
		and v.NroRevision = i.NroRevision
	order by NroRevision DESC
end


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar]
		@IdValidarInfoExperienciaEntidad INT OUTPUT, @IdExpEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoExperienciaEntidad(IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdExpEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoExperienciaEntidad = @@IDENTITY 		
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar]
		@IdValidarInfoExperienciaEntidad INT, @IdExpEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoExperienciaEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdExpEntidad = @IdExpEntidad
		and NroRevision = @NroRevision			
		and IdValidarInfoExperienciaEntidad = @IdValidarInfoExperienciaEntidad
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado de InfoExperiencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]
		@IdExpEntidad INT, @IdEstadoInfoExperienciaEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[InfoExperienciaEntidad] 
	SET EstadoDocumental = @IdEstadoInfoExperienciaEntidad, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdExpEntidad = @IdExpEntidad
END






GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar]
	@IdExpEntidad int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoExperienciaEntidad] 
 WHERE IdExpEntidad = CASE WHEN @IdExpEntidad IS NULL THEN IdExpEntidad ELSE @IdExpEntidad END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
 ORDER BY FechaCrea DESC
END





GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoExperienciaEntidad, v.IdExpEntidad, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoExperienciaEntidad v
			inner join [Proveedor].[InfoExperienciaEntidad] e on e.IdExpEntidad = v.IdExpEntidad
						inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
			where e.IdEntidad  = @IdEntidad
			--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
			ORDER BY v.FechaCrea DESC
END








GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]
	@IdValidarInfoFinancieraEntidad INT
AS
BEGIN
 SELECT IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoFinancieraEntidad] WHERE  IdValidarInfoFinancieraEntidad = @IdValidarInfoFinancieraEntidad
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoFinancieraEntidad por IdInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]
	@IdInfoFin INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoFinancieraEntidad] 
 WHERE  IdInfoFin = @IdInfoFin
 ORDER BY IdValidarInfoFinancieraEntidad DESC
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoEntidadEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima] (@IdEntidad INT, @IdVigencia INT)
as
begin
	select top 1
		IdValidarInfoFinancieraEntidad,
		i.IdInfoFin,
		e.IdEntidad,
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and i.IdVigencia = @IdVigencia
		and i.NroRevision = v.NroRevision
	order by NroRevision DESC
end



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar]
		@IdValidarInfoFinancieraEntidad INT OUTPUT, @IdInfoFin INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoFinancieraEntidad(IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdInfoFin, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoFinancieraEntidad = @@IDENTITY 		
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]
		@IdValidarInfoFinancieraEntidad INT, @IdInfoFin INT, @NroRevision INT, @Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoFinancieraEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdInfoFin = @IdInfoFin
		and NroRevision = @NroRevision			
		and IdValidarInfoFinancieraEntidad = @IdValidarInfoFinancieraEntidad
END



GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]
		@IdInfoFin INT, @IdEstadoInfoFinancieraEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	update [Proveedor].[InfoFinancieraEntidad]
	SET EstadoValidacion = @IdEstadoInfoFinancieraEntidad,
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdInfoFin = @IdInfoFin
END






GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]
	@IdInfoFin int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoFinancieraEntidad] 
 WHERE IdInfoFin = CASE WHEN @IdInfoFin IS NULL THEN IdInfoFin ELSE @IdInfoFin END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
 ORDER BY FechaCrea DESC
END




GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoFinancieraEntidad, v.IdInfoFin,AcnoVigencia, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoFinancieraEntidad v
	inner join [Proveedor].[InfoFinancieraEntidad] e on e.IdInfoFin = v.IdInfoFin
	inner join [Global].[Vigencia] g on g.IdVigencia = e.IdVigencia
	inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
	where e.IdEntidad  = @IdEntidad
	--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision) 
	ORDER BY v.FechaCrea DESC
 
END







GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
--Autor:Mauricio Martinez  
--Fecha:2013/07/15 16:00  
--Descripcion: Consulta para mostrar un resumen de los modulos validados  
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar]  
@IdEntidad INT   
AS  
BEGIN  
  
--======================================================  
--Se obtiene el Nro de la Ultima Revision Datos Basicos  
--======================================================  
--declare @IdEntidad int  
--set @IdEntidad  = 22  
  
declare @UltimaRevisionDatosBasicos int   
set @UltimaRevisionDatosBasicos = (select Max(v.NroRevision) from  Proveedor.EntidadProvOferente e left join Proveedor.ValidarInfoDatosBasicosEntidad v on v.IdEntidad = e.IdEntidad where e.IdEntidad = @IdEntidad)  
  
--==========================================================  
--Se obtiene el Nro de la Ultima Revision de Info Financiera  
--==========================================================  
  
declare @UltimaRevisionFinanciera int   
set @UltimaRevisionFinanciera = (select Max(v.NroRevision) from  Proveedor.EntidadProvOferente e left join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad left join Proveedor.ValidarInfoFinancieraEntidad v on v.IdInfoFin = i.IdInfoFin where
 e.IdEntidad = @IdEntidad)  
  
--===========================================================  
--Se obtiene el Nro de la Ultima Revision de Info Experiencia  
--===========================================================  
  
declare @UltimaRevisionExperiencia int   
set @UltimaRevisionExperiencia = (select Max(v.NroRevision) from  Proveedor.EntidadProvOferente e left join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad left join Proveedor.ValidarInfoExperienciaEntidad v on v.IdExpEntidad = i.IdExpEntidad where e.IdEntidad = @IdEntidad)  
  
--===================================================================================================  
--Se obtiene y se almacena temporalmente el registro completo de la ultima revision de Datos Basicos  
--===================================================================================================  
  
select v.IdEntidad, MAX(v.NroRevision) as NroRevision  
into #UltimaRevisionDatosBasicos  
from  Proveedor.EntidadProvOferente e   
 inner join Proveedor.ValidarInfoDatosBasicosEntidad v on v.IdEntidad = e.IdEntidad  
where e.IdEntidad = @IdEntidad  
group by v.IdEntidad  
  
--=====================================================================================================  
--Se obtiene y se almacena temporalmente el registro completo de la ultima revision de Info Financiera  
--=====================================================================================================  
  
select e.IdEntidad, v.IdInfoFin, MAX(v.NroRevision) as NroRevision  
into #UltimaRevisionFinanciera  
from  Proveedor.EntidadProvOferente e   
 inner join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad   
 inner join Proveedor.ValidarInfoFinancieraEntidad v on v.IdInfoFin = i.IdInfoFin  
where e.IdEntidad = @IdEntidad  
group by v.IdInfoFin,e.IdEntidad  
  
--=========================================================================================================  
--Se obtiene y se almacena temporalmente  el registro completo de la ultima revision de Info Experiencias  
--=========================================================================================================  
  
select e.IdEntidad, v.IdExpEntidad, MAX(v.NroRevision) as NroRevision  
into #UltimaRevisionExperiencia  
from  Proveedor.EntidadProvOferente e   
 inner join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad   
 inner join Proveedor.ValidarInfoExperienciaEntidad v on v.IdExpEntidad = i.IdExpEntidad   
where e.IdEntidad = @IdEntidad  
group by v.IdExpEntidad,e.IdEntidad  
  
--=========================================================================================================  
--Se crea tabla temp  
--=========================================================================================================  
create table Proveedor.Resumen (Orden int ,IdEntidad int, NroRevision int, Componente varchar(100), iConfirmaYAprueba int, Finalizado BIT, liberar BIT)  
--=========================================================================================================  
--Se obtiene el resumen  
--=========================================================================================================  
   
 IF EXISTS(select * from #UltimaRevisionDatosBasicos where IdEntidad = @IdEntidad)  
 BEGIN  
   insert into Proveedor.Resumen   
      select 0 as Orden,   
     e.IdEntidad,   
     MAX(ISNULL(e.NroRevision,1)) as NroRevision,  
     'Datos Básicos' as Componente,  
     MIN(case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END) AS iConfirmaYAprueba,  
     CAST(MIN(CAST(ISNULL(e.Finalizado,0) as int)) as bit) as Finalizado,  
     CASE WHEN ISNULL(MAX(e.IdEstado),0) = 5 THEN 1 ELSE 0 END as Liberar  
   from  Proveedor.EntidadProvOferente e  
    inner join Proveedor.ValidarInfoDatosBasicosEntidad v  
     on v.IdEntidad = e.IdEntidad --and v.NroRevision = @UltimaRevisionDatosBasicos  
     inner join #UltimaRevisionDatosBasicos t  
      on v.IdEntidad = t.IdEntidad and v.NroRevision = t.NroRevision   
   where   
     e.IdEntidad = @IdEntidad and t.NroRevision is not null      
   group by e.IdEntidad  
 END  
 ELSE  
 BEGIN  
   insert into Proveedor.Resumen   
   select  0 as Orden,   
     @IdEntidad as IdEntidad,    
     1 as NroRevision,  
     'Datos Básicos' as Componente,  
     -1 AS iConfirmaYAprueba,  
     0 as Finalizado,  
     0 as Liberar     
 END  
  
--=========================================================================================================  
--Se obtiene el resumen para financiera  
--=========================================================================================================  
--si hay informacion de financiera  
IF EXISTS(select * from Proveedor.EntidadProvOferente e inner join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad where e.IdEntidad = @IdEntidad)  
BEGIN  
 --si hay al menos una informacion si validar  
 IF EXISTS(select * from Proveedor.EntidadProvOferente e   
    left join Proveedor.InfoFinancieraEntidad i on e.IdEntidad = i.IdEntidad   
    left join Proveedor.ValidarInfoFinancieraEntidad v  
      on v.IdInfoFin = i.IdInfoFin where e.IdEntidad = @IdEntidad and v.IdInfoFin is null)  
 BEGIN  
   insert into Proveedor.Resumen   
   select 1 as Orden,  
     e.IdEntidad,       
     MAX(ISNULL(i.NroRevision,1)) as NroRevision,  
     'Financiera' as Componente,  
     -1 AS iConfirmaYAprueba,  
     cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,  
     CASE WHEN ISNULL(MAX(i.EstadoValidacion),0) = 5 THEN 1 ELSE 0 END as Liberar  
   from  Proveedor.EntidadProvOferente e  
    inner join Proveedor.InfoFinancieraEntidad i  
     on e.IdEntidad = i.IdEntidad       
   where   
     e.IdEntidad  = @IdEntidad  
   group by e.IdEntidad  
 END  
 ELSE  
 BEGIN  
  -- todas tienen validacion  
  IF EXISTS(select * from #UltimaRevisionFinanciera where IdEntidad = @IdEntidad)  
  BEGIN  
   insert into Proveedor.Resumen   
   select 1 as Orden,   
     e.IdEntidad,    
     MAX(ISNULL(i.NroRevision,1)) as NroRevision,  
     'Financiera' as Componente,  
     MIN(case when i.IdInfoFin IS NULL THEN -2 ELSE case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END END) AS iConfirmaYAprueba,  
     cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,  
     CASE WHEN ISNULL(MAX(i.EstadoValidacion),0) = 5 THEN 1 ELSE 0 END as Liberar  
   from    
     Proveedor.EntidadProvOferente e  
     inner join Proveedor.InfoFinancieraEntidad i  
      on e.IdEntidad = i.IdEntidad   
     inner join Proveedor.ValidarInfoFinancieraEntidad v  
      on v.IdInfoFin = i.IdInfoFin --and v.NroRevision = @UltimaRevisionFinanciera  
     inner join #UltimaRevisionFinanciera t  
      on t.IdInfoFin = v.IdInfoFin and t.NroRevision = v.NroRevision   
  
   where   
     e.IdEntidad  = @IdEntidad and t.NroRevision is not null  
   group by e.IdEntidad   
  END  
 END  
END   
ELSE /*No hay informacion*/  
BEGIN  
   insert into Proveedor.Resumen   
   select  1 as Orden,   
     @IdEntidad as IdEntidad,    
     1 as NroRevision,  
     'Financiera' as Componente,  
     -2 AS iConfirmaYAprueba,  
     0 as Finalizado,  
     0 as Liberar     
END  
  
--=========================================================================================================  
--Se obtiene el resumen para experiencias  
--=========================================================================================================  
--si hay informacion de experiencias  
IF EXISTS(select * from Proveedor.EntidadProvOferente e inner join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad where e.IdEntidad = @IdEntidad)  
BEGIN  
 --si hay al menos una informacion si validar  
 IF EXISTS(select * from Proveedor.EntidadProvOferente e   
    left join Proveedor.InfoExperienciaEntidad i on e.IdEntidad = i.IdEntidad   
    left join Proveedor.ValidarInfoExperienciaEntidad v  
      on v.IdExpEntidad = i.IdExpEntidad where e.IdEntidad = @IdEntidad and v.IdExpEntidad is null)  
 BEGIN  
   insert into Proveedor.Resumen   
   select 2 as Orden,  
     e.IdEntidad,       
     MAX(ISNULL(i.NroRevision,1)) as NroRevision,  
     'Experiencias' as Componente,  
     -1 AS iConfirmaYAprueba,  
     cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,  
     CASE WHEN ISNULL(MAX(i.EstadoDocumental),0) = 5 THEN 1 ELSE 0 END as Liberar  
   from  Proveedor.EntidadProvOferente e  
    inner join Proveedor.InfoExperienciaEntidad i  
     on e.IdEntidad = i.IdEntidad       
   where   
     e.IdEntidad  = @IdEntidad  
   group by e.IdEntidad  
 END  
 ELSE  
 BEGIN  
 -- todas tienen validacion  
  IF EXISTS(select * from #UltimaRevisionExperiencia where IdEntidad = @IdEntidad)  
  BEGIN  
   insert into Proveedor.Resumen   
   select 2 as Orden,  
     e.IdEntidad,       
     MAX(ISNULL(i.NroRevision,1)) as NroRevision,  
     'Experiencias' as Componente,  
     MIN(case when i.IdExpEntidad IS NULL THEN -2 ELSE case when v.ConfirmaYAprueba IS NULL THEN -1 ELSE v.ConfirmaYAprueba END END) AS iConfirmaYAprueba,  
     cast(MIN(cast( ISNULL(i.Finalizado,0) as int)) as bit) as Finalizado,  
     CASE WHEN ISNULL(MAX(i.EstadoDocumental),0) = 5 THEN 1 ELSE 0 END as Liberar  
   from  Proveedor.EntidadProvOferente e  
    inner join Proveedor.InfoExperienciaEntidad i  
     on e.IdEntidad = i.IdEntidad   
    inner join Proveedor.ValidarInfoExperienciaEntidad v  
     on v.IdExpEntidad = i.IdExpEntidad -- and v.NroRevision = @UltimaRevisionExperiencia  
    inner join #UltimaRevisionExperiencia t  
      on t.IdExpEntidad = v.IdExpEntidad and t.NroRevision = v.NroRevision   
   where   
     e.IdEntidad  = @IdEntidad   
   group by e.IdEntidad  
  END  
 END  
END  
ELSE /*No hay informacion*/  
BEGIN  
   insert into Proveedor.Resumen  
   select  2 as Orden,   
     @IdEntidad as IdEntidad,    
     1 as NroRevision,  
     'Experiencias' as Componente,  
     -2 AS iConfirmaYAprueba,  
     0 as Finalizado,  
     0 as Liberar     
END  
  
select * from Proveedor.Resumen Order By Orden  
drop table Proveedor.Resumen  
  
END  
  
  
  
  
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- Autor: Mauricio Martinez
-- Fecha: 2013/07/28
-- Descripcion: Procedimiento usado para validar el proveedor módulos dátos básicos, info financiero o experiencia
  
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision](@IdEntidad INT)
as
begin
 
	update Proveedor.InfoFinancieraEntidad  
	set  Finalizado = 1		
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and i.NroRevision = v.NroRevision
		and i.EstadoValidacion = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMó CON SI
	
	
	update Proveedor.InfoFinancieraEntidad  
	set NroRevision = v.NroRevision + 1,
		Finalizado = 0	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoFinancieraEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoFinancieraEntidad v
					on v.IdInfoFin = i.IdInfoFin
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		and i.EstadoValidacion <> 4 --SI NO ESTA VALIDADO AUMENTA EL NRO DE REVISION
		and v.ConfirmaYAprueba  <> 1 -- Y SI NO CONFIRMó CON SI
		
	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = 1	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		and i.EstadoDocumental = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMó CON SI
	
	
	update Proveedor.InfoExperienciaEntidad 
	set NroRevision = v.NroRevision + 1,
		Finalizado = 0
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and i.NroRevision = v.NroRevision
		and i.EstadoDocumental <> 4 --SI NO ESTA VALIDADO AUMENTA EL NRO DE REVISION
		and v.ConfirmaYAprueba <> 1 -- Y SI NO CONFIRMó CON SI

	update Proveedor.EntidadProvOferente    
	set Finalizado = 1
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
	where e.IdEntidad = @IdEntidad
		and IdEstado = 2  -- SI ESTA VALIDADO
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMO CON UN SI

		
	update Proveedor.EntidadProvOferente    
	set NroRevision = e.NroRevision + 1,
		Finalizado = 0
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad and v.NroRevision = e.NroRevision
	where e.IdEntidad = @IdEntidad
		and IdEstado <> 2  -- SI NO ESTA VALIDADO
		and v.ConfirmaYAprueba <> 1 -- Y SI NO HAN PUESTO UN SI

end

GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]
	@IdValidarTercero INT
AS
BEGIN
 SELECT IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarTercero] WHERE  IdValidarTercero = @IdValidarTercero
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarTercero por IdTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]
	@IdTercero INT
AS
BEGIN
 SELECT TOP 1 IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarTercero] 
 WHERE  IdTercero = @IdTercero
 ORDER BY IdValidarTercero DESC
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]
		@IdValidarTercero INT OUTPUT, 	@IdTercero INT,	@Observaciones NVARCHAR(100),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarTercero(IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdTercero, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarTercero = @@IDENTITY 		
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]
		@IdTercero INT, @IdEstadoTercero INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Oferente.Tercero 
	SET IdEstadoTercero = @IdEstadoTercero, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdTercero = @IdTercero
END


GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]    Script Date: 29/10/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTerceros_Consultar]
	@IdTercero int = NULL,@Observaciones NVARCHAR(100) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarTercero] 
 WHERE IdTercero = CASE WHEN @IdTercero IS NULL THEN IdTercero ELSE @IdTercero END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
 ORDER BY 1 DESC
END



GO




/****** Object:  StoredProcedure [dbo].[usp_ICBF_Aud_InsertarLogAuditoria]    Script Date: 10/29/2013 19:52:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Aud_InsertarLogAuditoria]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ICBF_Aud_InsertarLogAuditoria]
GO


/****** Object:  StoredProcedure [dbo].[usp_ICBF_Aud_InsertarLogAuditoria]    Script Date: 10/29/2013 19:52:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 14/01/2013
-- Description:	Procedimiento para Guardar la Auditoria del Sistema
-- =============================================
-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 23/01/2013
-- Description:	Ajuste longitud del parametro @pOperacion de 20 a 200
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Aud_InsertarLogAuditoria]
	@pUsuario NVARCHAR(250),
	@pPrograma NVARCHAR(200),
	@pOperacion NVARCHAR(20),
	@pParametrosOperacion TEXT,
	@pTabla NVARCHAR(50),
	@pIdRegistro NUMERIC(18,0),
	@pDireccionIp NVARCHAR(20),
	@pNavegador NVARCHAR(250)
AS
BEGIN

INSERT INTO [AUDITA].[LogSIA](
	[fecha],
	[usuario],
	[programa],
	[operacion],
	[parametrosOperacion],
	[tabla],
	[idRegistro],
	[direccionIp],
	[navegador])
VALUES (
	GETDATE(),
	@pUsuario,
	@pPrograma,
	@pOperacion,
	@pParametrosOperacion,
	@pTabla,
	@pIdRegistro,
	@pDireccionIp,
	@pNavegador)

END

GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]    Script Date: 10/30/2013 00:30:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]    Script Date: 10/30/2013 00:30:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los estados del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]
	@Estado BIT = NULL
AS
BEGIN

 SELECT IdEstadoTercero, 
		CodigoEstadotercero, 
		DescripcionEstado, 
		Estado, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
 FROM [Oferente].[EstadoTercero] 
 WHERE Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]    Script Date: 10/30/2013 00:31:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]    Script Date: 10/30/2013 00:31:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Fabian Valencia
-- Create date: 25/06/2013
-- Description:	Obtiene el estado a través de su código
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]
	@codigoTercero NVARCHAR (5)
AS
BEGIN
	SELECT
		IdEstadoTercero,
		CodigoEstadotercero,
		DescripcionEstado,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM oferente.EstadoTercero
	WHERE (CodigoEstadotercero = @codigoTercero)
END

GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]    Script Date: 10/30/2013 00:33:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]    Script Date: 10/30/2013 00:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  5/23/2013 5:33:48 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]

	@IdTercero INT OUTPUT, 
	@IdDListaTipoDocumento INT, 
	@IdTipoPersona INT=NULL,
	@NumeroIdentificacion NVARCHAR (255), 
	@PrimerNombre NVARCHAR (255), 
	@SegundoNombre NVARCHAR (255), 
	@PrimerApellido NVARCHAR (255), 
	@SegundoApellido NVARCHAR (255), 
	@Email NVARCHAR (255), 
	@Sexo NVARCHAR (1), 
	@ProviderUserKey UNIQUEIDENTIFIER=NULL,
	@DIGITOVERIFICACION INT=NULL,
	@IDESTADOTERCERO INT =NULL,
	@RAZONSOCIAL NVARCHAR (250)=NULL,
	@FechaExpedicionId DATETIME =null, 
	@FechaNacimiento DATETIME =null, 
	@UsuarioCrea NVARCHAR (250),
	@IdTemporal VARCHAR(20)= NULL
	
AS
BEGIN

INSERT INTO Oferente.Tercero 
         ([IDTIPODOCIDENTIFICA]
         ,[IDESTADOTERCERO]
         ,[IdTipoPersona]
         ,[ProviderUserKey]
         ,[NUMEROIDENTIFICACION]
         ,[DIGITOVERIFICACION]
         ,[CORREOELECTRONICO]
         ,[PRIMERNOMBRE]
         ,[SEGUNDONOMBRE]
         ,[PRIMERAPELLIDO]
         ,[SEGUNDOAPELLIDO]
         ,[RAZONSOCIAL]
         ,[FECHAEXPEDICIONID]
         ,[FECHANACIMIENTO]
         ,[SEXO]
         ,UsuarioCrea
         ,FechaCrea)
	VALUES (@IdDListaTipoDocumento,
	        @IDESTADOTERCERO,
	        @IdTipoPersona,
	        @ProviderUserKey,
	        @NumeroIdentificacion, 
	        @DIGITOVERIFICACION,
	        @Email, 
	        @PrimerNombre, 
	        @SegundoNombre, 
	        @PrimerApellido, 
	        @SegundoApellido, 
	        @RAZONSOCIAL,
	        @FechaExpedicionId, 
	        @FechaNacimiento,
	        @Sexo, 
	        @UsuarioCrea, 
	        GETDATE())

SELECT @IdTercero=@@IDENTITY

UPDATE [Proveedor].[DocAdjuntoTercero]  
	set IdTercero = @IdTercero
	where IdTemporal = @IdTemporal
	
	SELECT @IdTercero


RETURN @@IDENTITY

END

GO







