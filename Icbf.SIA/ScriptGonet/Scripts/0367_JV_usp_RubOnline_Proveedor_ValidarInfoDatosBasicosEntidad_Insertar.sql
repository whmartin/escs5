USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]    Script Date: 08/12/2014 12:10:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]
GO



/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]    Script Date: 08/12/2014 12:10:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar]
		@IdValidarInfoDatosBasicosEntidad INT OUTPUT, @IdEntidad INT, @NroRevision INT, @Observaciones NVARCHAR(200),	@ConfirmaYAprueba BIT = NULL, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarInfoDatosBasicosEntidad(IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @NroRevision, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarInfoDatosBasicosEntidad = SCOPE_IDENTITY() 		
END







GO


