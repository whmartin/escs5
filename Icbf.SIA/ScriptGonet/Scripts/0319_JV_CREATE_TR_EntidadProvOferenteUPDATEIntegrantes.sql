USE [SIA]
GO

/****** Object:  Trigger [TR_EntidadProvOferenteUPDATEIntegrantes]    Script Date: 08/05/2014 16:55:25 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[TR_EntidadProvOferenteUPDATEIntegrantes]'))
DROP TRIGGER [PROVEEDOR].[TR_EntidadProvOferenteUPDATEIntegrantes]
GO

/****** Object:  Trigger [PROVEEDOR].[TR_EntidadProvOferenteUPDATEIntegrantes]    Script Date: 08/05/2014 16:55:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Carlos Valverde S�mano
-- Create date: 05/AGO/2014
-- Description:	Este disparador se ejecutar� cuando para un consorcio o uni�n temporal se 
-- modifique el n�mero de integrantes.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[TR_EntidadProvOferenteUPDATEIntegrantes] 
   ON  [PROVEEDOR].[EntidadProvOferente]
   AFTER UPDATE
AS 
BEGIN
	DECLARE @IdEntidad INT
	SELECT @IdEntidad=IdEntidad FROM inserted
    DECLARE @CodigoTipoPersona NVARCHAR(5)
    SELECT @CodigoTipoPersona=CodigoTipoPersona FROM Oferente.TipoPersona
    WHERE IdTipoPersona =(
    SELECT TER.IdTipoPersona FROM 
    PROVEEDOR.EntidadProvOferente PROV
    INNER JOIN Oferente.TERCERO TER ON
    PROV.IdTercero=TER.IDTERCERO
    WHERE PROV.IdEntidad=@IdEntidad)
    
    IF(@CodigoTipoPersona NOT IN ('001','002'))
    BEGIN
		IF((SELECT NumIntegrantes FROM inserted)!=(SELECT NumIntegrantes FROM deleted))
		BEGIN
			IF(@CodigoTipoPersona='003')
			BEGIN
				DECLARE @PORCENTAJE FLOAT;
				DECLARE @NumIntegrantes INT;
				SELECT @NumIntegrantes=NumIntegrantes FROM PROVEEDOR.EntidadProvOferente
				WHERE IdEntidad=@IdEntidad
				SET @PORCENTAJE = CAST(100 AS FLOAT)/CAST(@NumIntegrantes AS FLOAT)
				
				UPDATE PROVEEDOR.Integrantes
				SET PorcentajeParticipacion=@PORCENTAJE
				WHERE IdEntidad=@IdEntidad
			END
			ELSE
			BEGIN
				EXEC [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado] @IdEntidad		
			END			
		END
    END
END

GO


