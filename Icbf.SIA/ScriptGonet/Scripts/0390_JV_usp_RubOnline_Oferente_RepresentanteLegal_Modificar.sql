USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Modificar]    Script Date: 09/01/2014 15:01:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Modificar]
GO


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Modificar]    Script Date: 09/01/2014 15:01:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Caros Valverde Sámano
-- Create date:  17/07/2014
-- Description:	Actualiza la información de un representante Legal
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_RepresentanteLegal_Modificar]
	 @IdTercero  INT,
	 @IdDListaTipoDocumento INT= NULL,
	 @IdTipoPersona INT= NULL,
	 @Email nvarchar(40)= NULL,
	 @NumeroIdentificacion nvarchar(128)= NULL,
	 @FechaExpedicionId datetime= NULL,
	 @Sexo nvarchar(1)= NULL,
	 @RazonSocial  nvarchar(128)= NULL,
	 @Digitoverificacion INT= NULL,
	 @PrimerNombre nvarchar(128)= NULL,
	 @SegundoNombre nvarchar(128)= NULL,
	 @PrimerApellido nvarchar(128)= NULL,
	 @SegundoApellido nvarchar(128)= NULL,
	 @FechaNacimiento datetime= NULL,
	 @UsuarioModifica nvarchar(128)= NULL,
	 @FechaModifica datetime= NULL,
	 @EsFundacion bit = NULL,
	 @IdTemporal VARCHAR(20)= NULL,
	 @CreadoPorInterno BIT=NULL	 
AS
BEGIN
	UPDATE Oferente.TERCERO SET
	 IDTIPODOCIDENTIFICA= ISNULL(@IdDListaTipoDocumento,IDTIPODOCIDENTIFICA),
		IdTipoPersona= ISNULL(@IdTipoPersona, IdTipoPersona),
		CORREOELECTRONICO = ISNULL( @Email, CORREOELECTRONICO ),
		NUMEROIDENTIFICACION = ISNULL( @NumeroIdentificacion, NUMEROIDENTIFICACION ),
		FECHAEXPEDICIONID = ISNULL( @FechaExpedicionId, FECHAEXPEDICIONID ),
		SEXO = ISNULL( @Sexo, SEXO ),
		RAZONSOCIAL= ISNULL(@RazonSocial, RAZONSOCIAL),
		PRIMERNOMBRE= ISNULL(@PrimerNombre, PRIMERNOMBRE),
		SEGUNDONOMBRE= ISNULL(@SegundoNombre, SEGUNDONOMBRE),
		PRIMERAPELLIDO= ISNULL(@PrimerApellido, PRIMERAPELLIDO),
		SEGUNDOAPELLIDO= ISNULL(@SegundoApellido, SEGUNDOAPELLIDO),
		DIGITOVERIFICACION= ISNULL(@Digitoverificacion, DIGITOVERIFICACION),
		FECHANACIMIENTO= ISNULL(@FechaNacimiento, FECHANACIMIENTO),
		USUARIOMODIFICA = ISNULL( @UsuarioModifica, USUARIOMODIFICA ),
		FECHAMODIFICA = ISNULL( @FechaModifica ,  FECHAMODIFICA ),
		ESFUNDACION = ISNULL( @EsFundacion ,  ESFUNDACION ),
		CreadoPorInterno = @CreadoPorInterno

	WHERE IDTERCERO = @IdTercero

END






GO


