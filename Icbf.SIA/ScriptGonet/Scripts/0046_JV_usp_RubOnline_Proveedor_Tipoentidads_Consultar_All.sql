USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]    Script Date: 04/01/2014 04:47:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]    Script Date: 04/01/2014 04:47:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
--Modificado por: Juan Carlos Valverde S�mano
--Fecha: 01/04/2014
--Descripci�n: en base a un requerimiento del control de cambios 16, se agregan los
-- param�tros: @IdTipoPersona y @IdSector para realizar un filtrado.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]	
@IdTipoPersona INT=NULL, @IdSector INT=NULL
AS
BEGIN
	IF ((@IdTipoPersona=1 AND @IdSector=1) OR (@IdTipoPersona=2 AND @IdSector=2))
	BEGIN
		SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
		FROM [Proveedor].[Tipoentidad]
		WHERE Estado=1 AND Descripcion!='EXTRANJERA'
		ORDER BY Descripcion ASC 
	END
	ELSE IF ((@IdTipoPersona=2 AND @IdSector=1)) 
	BEGIN
		SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
		FROM [Proveedor].[Tipoentidad]
		WHERE Estado=1
		ORDER BY Descripcion ASC
	END
	ELSE
		BEGIN
		SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
		FROM [Proveedor].[Tipoentidad]
		WHERE Estado=1
		ORDER BY Descripcion ASC
	END
END




GO



