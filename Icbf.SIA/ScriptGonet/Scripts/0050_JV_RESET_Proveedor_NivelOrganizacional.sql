USE [SIA]
/**
Desarrollador: Juan Carlos Valverde S�mano
Fecha Creaci�n: 01-Abril-2014
Descripci�n: en base a un requerimiento del CC016, se realiza un reset de este cat�logo 
llevar un mejor orden, y pasar a completar el requerimiento de la relaci�n entre las
tablas RamaoEstructura-NivelGobierno-NivelOrganizacional-TipodeentidadPublica
**/
GO



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[PROVEEDOR].[FK_EntidadProvOferente_NivelOrganizacional]') AND parent_object_id = OBJECT_ID(N'[PROVEEDOR].[EntidadProvOferente]'))
ALTER TABLE [PROVEEDOR].[EntidadProvOferente] DROP CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional]
GO


TRUNCATE TABLE [PROVEEDOR].[NivelOrganizacional]
GO
INSERT INTO [PROVEEDOR].[NivelOrganizacional]
           ([CodigoNivelOrganizacional]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('001','CENTRALIZADA',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[NivelOrganizacional]
           ([CodigoNivelOrganizacional]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('002','DESCENTRALIZADA',1,'Administrador',GETDATE(),NULL,NULL) 
INSERT INTO [PROVEEDOR].[NivelOrganizacional]
           ([CodigoNivelOrganizacional]
           ,[Descripcion]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('003','NO APLICA',1,'Administrador',GETDATE(),NULL,NULL) 


ALTER TABLE [PROVEEDOR].[EntidadProvOferente]  WITH CHECK ADD  CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional] FOREIGN KEY([IdTipoNivelOrganizacional])
REFERENCES [PROVEEDOR].[NivelOrganizacional] ([IdNivelOrganizacional])
GO

ALTER TABLE [PROVEEDOR].[EntidadProvOferente] CHECK CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional]
GO


