USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]    Script Date: 08/08/2013 21:11:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]    Script Date: 08/08/2013 21:11:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]
	@IDGarantia INT,
	@IDSUCURSAL INT
AS
BEGIN
 SELECT 
 [Contrato].[Garantia].IDGarantia, 
 [Contrato].[Garantia].IDContrato,
 [Contrato].[Garantia].IDTipoGarantia, 
 [Contrato].[Garantia].IDterceroaseguradora,
 [Contrato].[Garantia].IDtercerosucursal,
 [Contrato].[Garantia].IDcontratistaContato,
 [Contrato].[Garantia].IDtipobeneficiario,
 [Contrato].[Garantia].NITICBF,
 [Contrato].[Garantia].DescBeneficiario,
 [Contrato].[Garantia].NumGarantia, 
 [Contrato].[Garantia].FechaExpedicion, 
 [Contrato].[Garantia].FechaVencInicial, 
 [Contrato].[Garantia].FechaRecibo, 
 [Contrato].[Garantia].FechaDevolucion, 
 [Contrato].[Garantia].MotivoDevolucion, 
 [Contrato].[Garantia].FechaAprobacion, 
 [Contrato].[Garantia].FechaCertificacionPago, 
 [Contrato].[Garantia].Valor, 
 [Contrato].[Garantia].Estado, 
 [Contrato].[Garantia].Anexo, 
 [Contrato].[Garantia].ObservacionAnexo, 
 [Contrato].[Garantia].UsuarioCrea, 
 [Contrato].[Garantia].FechaCrea, 
 [Contrato].[Garantia].UsuarioModifica, 
 [Contrato].[Garantia].FechaModifica,
 Oferente.TERCERO.RAZONSOCIAL,
 Oferente.TERCERO.PRIMERNOMBRE,
 Oferente.TERCERO.SEGUNDONOMBRE,
 Oferente.TERCERO.PRIMERAPELLIDO,
 Oferente.TERCERO.SEGUNDOAPELLIDO,
Proveedor.Sucursal.Nombre,
Proveedor.Sucursal.Direccion,
Proveedor.Sucursal.Correo,
Proveedor.Sucursal.Indicativo,
Proveedor.Sucursal.Telefono,
Proveedor.Sucursal.Extension,
Proveedor.Sucursal.Celular,
DIV.Departamento.NombreDepartamento,
DIV.Municipio.NombreMunicipio
  FROM [Contrato].[Garantia], 
 Oferente.TERCERO,
 Proveedor.Sucursal, 
 Oferente.TERCERO SUCURSALA,
 Proveedor.EntidadProvOferente,
 DIV.Departamento,
 DIV.Municipio
 WHERE  
 [Contrato].[Garantia].IDGarantia = @IDGarantia
 AND Oferente.TERCERO.IDTERCERO = [Contrato].[Garantia].IDterceroaseguradora
 AND SUCURSALA.IDTERCERO = Proveedor.EntidadProvOferente.IdTercero
 AND Proveedor.EntidadProvOferente.IdEntidad = Proveedor.Sucursal.IdEntidad
 AND (Proveedor.Sucursal.IdSucursal = @IDSUCURSAL or  @IDSUCURSAL IS NULL) 
 AND Proveedor.Sucursal.Departamento = DIV.Departamento.IdDepartamento
 AND Proveedor.Sucursal.Municipio = DIV.Municipio.IdMunicipio 
 END

GO

