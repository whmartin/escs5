USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_Obligacion_TipoObligacion1]') AND parent_object_id = OBJECT_ID(N'[Contrato].[Obligacion]'))
ALTER TABLE [Contrato].[Obligacion] DROP CONSTRAINT [FK_Obligacion_TipoObligacion1]
GO

   -- =============================================
   -- Author:              @ReS\Andrés Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea la llave foranea con la tabla obligación
   -- =============================================

TRUNCATE TABLE [Contrato].[Obligacion]
GO


USE [SIA]
GO

ALTER TABLE [Contrato].[Obligacion]  WITH CHECK ADD  CONSTRAINT [FK_Obligacion_TipoObligacion1] FOREIGN KEY([IdTipoObligacion])
REFERENCES [Contrato].[TipoObligacion] ([IdTipoObligacion])
GO

ALTER TABLE [Contrato].[Obligacion] CHECK CONSTRAINT [FK_Obligacion_TipoObligacion1]
GO

