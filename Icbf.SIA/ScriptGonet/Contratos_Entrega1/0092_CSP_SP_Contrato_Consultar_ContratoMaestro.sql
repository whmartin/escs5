USE [SIA]
GO

/****** Object:  StoredProcedure [AUDITA].[usp_RubOnline_Contrato_Consultar_ContratoMaestro]    Script Date: 08/05/2013 23:56:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AUDITA].[usp_RubOnline_Contrato_Consultar_ContratoMaestro]') AND type in (N'P', N'PC'))
DROP PROCEDURE [AUDITA].[usp_RubOnline_Contrato_Consultar_ContratoMaestro]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [AUDITA].[usp_RubOnline_Contrato_Consultar_ContratoMaestro]    Script Date: 08/05/2013 23:56:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/27/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Contrato vinculacion
-- =============================================
CREATE PROCEDURE [AUDITA].[usp_RubOnline_Contrato_Consultar_ContratoMaestro]
@pIdContratoAsociado	INT
AS
BEGIN
	SET NOCOUNT ON;
SELECT IdContrato,
FechaRegistro,
NumeroProceso,
NumeroContrato,
FechaAdjudicacion,
IdModalidad,
IdCategoriaContrato,
IdTipoContrato,
IdCodigoModalidad,
IdModalidadAcademica,
IdCodigoProfesion,
IdNombreProfesion,
IdRegionalContrato,
RequiereActa,
ManejaAportes,
ManejaRecursos,
ManejaVigenciasFuturas,
IdRegimenContratacion,
CodigoRegional,
NombreSolicitante,
DependenciaSolicitante,
CargoSolicitante,
ObjetoContrato,
AlcanceObjetoContrato,
ValorInicialContrato,
ValorTotalAdiciones,
ValorFinalContrato,
ValorAportesICBF,
ValorAportesOperador,
ValorTotalReduccion,
JustificacionAdicionSuperior50porc,
FechaSuscripcion,
FechaInicioEjecucion,
FechaFinalizacionInicial,
PlazoInicial,
FechaInicialTerminacion,
FechaFinalTerminacion,
FechaProyectadaLiquidacion,
FechaAnulacion,
Prorrogas,
PlazoTotal,
FechaFirmaActaInicio,
VigenciaFiscalInicial,
VigenciaFiscalFinal,
IdUnidadEjecucion,
IdLugarEjecucion,
DatosAdicionales,
IdEstadoContrato,
IdTipoDocumentoContratista,
IdentificacionContratista,
NombreContratista,
IdFormaPago,
IdTipoEntidad,
contrato.UsuarioCrea,
contrato.FechaCrea,
contrato.UsuarioModifica,
contrato.FechaModifica,
ClaseContrato,
Consecutivo,
AfectaPlanCompras,
IdSolicitante,
IdProducto,
FechaLiquidacion,
NumeroDocumentoVigenciaFutura,
RequiereGarantia
 FROM [Contrato].[Contrato] contrato inner join Contrato.RelacionContrato asociado
 on (asociado.IdContratoMaestro = contrato.IdContrato)
 where asociado.IdContratoAsociado = @pIdContratoAsociado
END

GO

