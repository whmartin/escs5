USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]    Script Date: 08/23/2013 09:20:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]
GO

-- =============================================
-- Author:                  @ReS\Cesar Casanova
-- Create date:         15/06/2013 10:29
-- Description:          Procedimiento almacenado que consulta ModalidadSeleccion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]
@Nombre NVARCHAR (128) = NULL, @Sigla NVARCHAR (3) = NULL, @Estado BIT = NULL
AS
BEGIN
SELECT
	IdModalidad,
	Nombre,
	Sigla,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[ModalidadSeleccion]
WHERE Nombre LIKE '%' + 
	CASE
		WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre
	END  + '%'
	AND Sigla LIKE '%' + 
	CASE
		WHEN @Sigla IS NULL THEN Sigla ELSE @Sigla
	END  + '%'
	AND Estado =
	CASE
		WHEN @Estado IS NULL THEN Estado ELSE @Estado
	END
ORDER BY Nombre
		
END



GO


