USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Henry
   -- Create date:         25/07/2013 10:20am
   -- Description:         Inserta en la tabla para ser visualizada en la aplicación
   --						y el permiso al usuario administrador
   -- =============================================
IF NOT EXISTS(SELECT IDPROGRAMA FROM SEG.PROGRAMA 
WHERE
	CODIGOPROGRAMA = 'Contratos/ObjetoContrato' and 
	NOMBREPROGRAMA = 'Objeto Contrato' )
BEGIN
      DECLARE @idpadre int
     
      SELECT @idpadre = IdPrograma 
	  FROM SEG.Programa 
	  WHERE 
	  NombrePrograma = 'Contrato'
---	  
      INSERT INTO SEG.Programa 
      SELECT 
      IdModulo,'Objeto Contrato','Contratos/ObjetoContrato',1,1,'Administrador',GETDATE(),'','',1,1,NULL,NULL,NULL
      FROM SEG.Modulo
      WHERE 
      NOMBREMODULO = 'CONTRATOS'    
---
      INSERT INTO SEG.PERMISO 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      FROM SEG.Programa
      WHERE 
      CODIGOPROGRAMA = 'Contratos/ObjetoContrato' and 
      NOMBREPROGRAMA = 'Objeto Contrato'
END
ELSE
BEGIN
      PRINT 'El VALOR A INSERTAR YA EXISTE EN LA BASE DE DATOS'
END