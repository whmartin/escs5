USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_GestionarObligacion_GestionarClausulasContrato]') AND parent_object_id = OBJECT_ID(N'[Contrato].[GestionarObligacion]'))
ALTER TABLE [Contrato].[GestionarObligacion] DROP CONSTRAINT [FK_GestionarObligacion_GestionarClausulasContrato]
GO

truncate table [Contrato].[GestionarObligacion]


USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea la realaci�n con la tabla GestionarClausulaContrato - [GestionarObligacion]
   -- =============================================
ALTER TABLE [Contrato].[GestionarObligacion]  WITH CHECK ADD  CONSTRAINT [FK_GestionarObligacion_GestionarClausulasContrato] FOREIGN KEY([IdGestionClausula])
REFERENCES [Contrato].[GestionarClausulasContrato] ([IdGestionClausula])
GO

ALTER TABLE [Contrato].[GestionarObligacion] CHECK CONSTRAINT [FK_GestionarObligacion_GestionarClausulasContrato]
GO


