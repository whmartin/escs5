USE [SIA]
GO

-- =============================================
-- Author:		Ares
-- Description:	Procedimiento almacenado que crea el procedimiento para modificar la relacion de contratistas
-- =============================================

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]    Script Date: 08/12/2013 20:14:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]
		@IdContratistaContrato INT,	
		@IdContrato INT,	
		@NumeroIdentificacion BIGINT,	
		@ClaseEntidad NVARCHAR,	
		@PorcentajeParticipacion INT,	
		@NumeroIdentificacionRepresentanteLegal BIGINT, 
		@EstadoIntegrante bit, 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN

	UPDATE 
	Contrato.RelacionarContratistas 
	SET 
	IdContrato = @IdContrato, 
	NumeroIdentificacion = @NumeroIdentificacion, 
	ClaseEntidad = @ClaseEntidad, 
	PorcentajeParticipacion = @PorcentajeParticipacion, 
	NumeroIdentificacionRepresentanteLegal = @NumeroIdentificacionRepresentanteLegal, 
	EstadoIntegrante = @EstadoIntegrante, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE 
	IdContratistaContrato = @IdContratistaContrato
	
END
GO


