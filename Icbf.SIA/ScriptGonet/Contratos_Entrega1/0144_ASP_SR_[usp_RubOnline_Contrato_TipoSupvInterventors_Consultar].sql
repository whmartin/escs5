USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]    Script Date: 08/15/2013 20:22:56 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]

	@Nombre NVARCHAR (128) = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdTipoSupvInterventor,
		Nombre,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoSupvInterventor]
	WHERE Nombre LIKE '%' + 
		CASE
			WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)

END

GO