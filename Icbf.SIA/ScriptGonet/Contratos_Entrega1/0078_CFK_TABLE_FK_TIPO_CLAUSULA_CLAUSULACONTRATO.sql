USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_GestionarClausulasContrato_ClausulaContrato]') AND parent_object_id = OBJECT_ID(N'[Contrato].[GestionarClausulasContrato]'))
ALTER TABLE [Contrato].[GestionarClausulasContrato] DROP CONSTRAINT [FK_GestionarClausulasContrato_ClausulaContrato]
GO

   -- =============================================
   -- Author:              @ReS\Andrés Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea la llave foranea clausula contrato
   -- =============================================

TRUNCATE TABLE [Contrato].[GestionarClausulasContrato]
GO

USE [SIA]
GO

ALTER TABLE [Contrato].[GestionarClausulasContrato]  WITH CHECK ADD  CONSTRAINT [FK_GestionarClausulasContrato_ClausulaContrato] FOREIGN KEY([TipoClausula])
REFERENCES [Contrato].[ClausulaContrato] ([IdClausulaContrato])
GO

ALTER TABLE [Contrato].[GestionarClausulasContrato] CHECK CONSTRAINT [FK_GestionarClausulasContrato_ClausulaContrato]
GO

