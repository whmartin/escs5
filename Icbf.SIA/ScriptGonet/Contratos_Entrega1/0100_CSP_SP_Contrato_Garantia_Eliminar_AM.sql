USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]    Script Date: 08/06/2013 19:36:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]    Script Date: 08/06/2013 19:36:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ares\Henry
-- Create date:  06/08/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que elimina un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]
	@IDGarantia INT
AS
BEGIN
	DELETE Contrato.Garantia 
	WHERE IDGarantia = @IDGarantia
END


GO


