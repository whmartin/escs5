USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_Garantia_RelacionarContratistas]') AND parent_object_id = OBJECT_ID(N'[Contrato].[Garantia]'))
ALTER TABLE [Contrato].[Garantia] DROP CONSTRAINT [FK_Garantia_RelacionarContratistas]
GO

-- =============================================
-- Author:		Ares\Andres Morales
-- Create date:  13/08/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que crea el constraint relacionar contratistas
-- EXEC [usp_RubOnline_Contrato_Garantias_Consultar]
-- =============================================

USE [SIA]
GO

ALTER TABLE [Contrato].[Garantia]  WITH CHECK ADD  CONSTRAINT [FK_Garantia_RelacionarContratistas] FOREIGN KEY([IDcontratistaContato])
REFERENCES [Contrato].[RelacionarContratistas] ([IdContratistaContrato])
GO

ALTER TABLE [Contrato].[Garantia] CHECK CONSTRAINT [FK_Garantia_RelacionarContratistas]
GO


