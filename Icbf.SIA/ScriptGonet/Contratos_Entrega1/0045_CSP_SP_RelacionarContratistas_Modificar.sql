USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]    Script Date: 07/30/2013 22:40:16 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]
GO
-- =============================================
-- Author:              @ReS\Henry Gomez
-- Create date:         30/06/2013 11:46am
-- Description:         Crea la tabla para actualizar la relaci�n de contratistas contrato
--						
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]
@IdContratistaContrato INT, @IdContrato INT, @NumeroIdentificacion BIGINT, @ClaseEntidad NVARCHAR, @PorcentajeParticipacion INT, @NumeroIdentificacionRepresentanteLegal BIGINT, @UsuarioModifica NVARCHAR (250)
AS
BEGIN
UPDATE Contrato.RelacionarContratistas
SET	IdContrato = @IdContrato,
	NumeroIdentificacion = @NumeroIdentificacion,
	ClaseEntidad = @ClaseEntidad,
	PorcentajeParticipacion = @PorcentajeParticipacion,
	NumeroIdentificacionRepresentanteLegal = @NumeroIdentificacionRepresentanteLegal,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdContratistaContrato = @IdContratistaContrato
END
GO