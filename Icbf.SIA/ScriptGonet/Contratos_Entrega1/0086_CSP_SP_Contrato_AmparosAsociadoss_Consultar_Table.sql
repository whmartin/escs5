USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar_Table]    Script Date: 08/05/2013 23:48:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar_Table]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar_Table]
GO

USE [SIA]
GO


   -- =============================================
   -- Author:              @ReS\Andrés Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea el procedimiento para consulyar amparos asociados
   -- =============================================

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar_Table]    Script Date: 08/05/2013 23:48:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar_Table]
	@NumeroGarantia NVARCHAR(50) = NULL,
	@IdTipoAmparo INT = NULL,
	@VigenciaDesde DATETIME = NULL,
	@VigenciaHasta	DATETIME = NULL	
AS
BEGIN
 SELECT IdAmparo, amparo.IdGarantia, amparo.IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, 
 IdTipoCalculo, ValorAsegurado, amparo.UsuarioCrea, amparo.FechaCrea, amparo.UsuarioModifica, amparo.FechaModifica ,
 tipoamparo.NombreTipoAmparo as tipoamparo, garantia.NumGarantia
 FROM [Contrato].[AmparosAsociados] amparo inner join Contrato.Garantia garantia
 on (garantia.IDGarantia = amparo.IdGarantia) INNER JOIN Contrato.TipoAmparo tipoamparo
 on (tipoamparo.IdTipoAmparo = amparo.IdTipoAmparo)
 WHERE amparo.IdTipoAmparo = CASE WHEN @IdTipoAmparo IS NULL THEN amparo.IdTipoAmparo ELSE @IdTipoAmparo END 
 AND FechaVigenciaDesde >= CASE WHEN @VigenciaDesde IS NULL THEN FechaVigenciaDesde ELSE @VigenciaDesde END 
 AND VigenciaHasta <= CASE WHEN @VigenciaHasta IS NULL THEN VigenciaHasta ELSE @VigenciaHasta END
 AND garantia.NumGarantia = CASE WHEN @NumeroGarantia IS NULL THEN garantia.NumGarantia ELSE @NumeroGarantia END
END


GO


