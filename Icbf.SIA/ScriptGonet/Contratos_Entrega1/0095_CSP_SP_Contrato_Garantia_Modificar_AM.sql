USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]    Script Date: 08/06/2013 19:36:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]    Script Date: 08/06/2013 19:36:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ares\Andr�s Morales
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]
@IDGarantia INT, 	
@IDContrato INT,	
@IDTipoGarantia INT,	
@IDterceroaseguradora INT,
@IDtercerosucursal INT,
@IDcontratistaContato INT,
@IDtipobeneficiario INT,
@NITICBF NVARCHAR(50),
@DescBeneficiario NVARCHAR(200),
@NumGarantia NVARCHAR(128),	
@FechaExpedicion DATETIME,	
@FechaVencInicial DATETIME,	
@FechaRecibo DATETIME,	
@FechaDevolucion DATETIME,	
@MotivoDevolucion NVARCHAR(128),	
@FechaAprobacion DATETIME,	
@FechaCertificacionPago DATETIME,	
@Estado BIT,	
@Anexo BIT,	
@ObservacionAnexo NVARCHAR(256), 
@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.Garantia 
	SET IDContrato = @IDContrato, 
	IDTipoGarantia = @IDTipoGarantia, 
	@IDterceroaseguradora = IDterceroaseguradora,
    @IDtercerosucursal = IDtercerosucursal,
    @IDcontratistaContato = IDcontratistaContato,
    @IDtipobeneficiario = IDtipobeneficiario,
    @NITICBF = NITICBF,
    @DescBeneficiario =DescBeneficiario,
	NumGarantia = @NumGarantia, 
	FechaExpedicion = @FechaExpedicion, 
	FechaVencInicial = @FechaVencInicial, 
	FechaRecibo = @FechaRecibo, 
	FechaDevolucion = @FechaDevolucion, 
	MotivoDevolucion = @MotivoDevolucion, 
	FechaAprobacion = @FechaAprobacion, 
	FechaCertificacionPago = @FechaCertificacionPago, 
	Estado = @Estado, 
	Anexo = @Anexo, 
	ObservacionAnexo = @ObservacionAnexo, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IDGarantia = @IDGarantia
END


GO


