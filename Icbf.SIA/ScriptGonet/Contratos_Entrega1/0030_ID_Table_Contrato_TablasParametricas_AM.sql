USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Andrés Morales
   -- Create date:         30/06/2013 12:00m
   -- Description:         Inserta en la tabla para ser visualizada en la aplicación
   --						y el permiso al usuario administrador
   -- =============================================
IF NOT EXISTS(SELECT IDPROGRAMA FROM SEG.PROGRAMA 
WHERE
	CODIGOPROGRAMA = 'Contratos/TablaParametrica' and 
	NOMBREPROGRAMA = 'Listas Paramétricas' )
BEGIN
      DECLARE @idpadre int
     
      SELECT @idpadre = IdPrograma 
	  FROM SEG.Programa 
	  WHERE 
	  NombrePrograma = 'Contrato'
---	  
      INSERT INTO SEG.Programa 
      SELECT 
      IdModulo,'Listas Paramétricas','Contratos/TablaParametrica',1,1,'Administrador',GETDATE(),'','',1,1,NULL,NULL,NULL
      FROM SEG.Modulo
      WHERE 
      NOMBREMODULO = 'CONTRATOS'    
---
      INSERT INTO SEG.PERMISO 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      FROM SEG.Programa
      WHERE 
      CODIGOPROGRAMA = 'Contratos/TablaParametrica' and 
      NOMBREPROGRAMA = 'Listas Paramétricas' 
END
ELSE
BEGIN
      PRINT 'El VALOR A INSERTAR YA EXISTE EN LA BASE DE DATOS'
END