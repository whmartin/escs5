USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contratos_Valor_Aportes]    Script Date: 08/13/2013 17:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contratos_Valor_Aportes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contratos_Valor_Aportes]
GO

-- =============================================
-- Author:		Ares\Andr�s Morales
-- Create date:  10/08/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que calcula el valor total de los aportes
--EXEC dbo.[usp_RubOnline_Contratos_Valor_Aportes] 19,'Contratista'
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Contratos_Valor_Aportes]

	@IDCONTRATO int,
	@TIPOAPORTANTE NVARCHAR(11)

AS
BEGIN

	DECLARE
	@VALOR NUMERIC(18,0),
	@VALORICBF NUMERIC(18,0)

	SELECT  @VALOR = SUM(Contrato.AporteContrato.ValorAporte)
	FROM 
	Contrato.AporteContrato
	where
	Contrato.AporteContrato.IdContrato = @IDCONTRATO
	AND Contrato.AporteContrato.TipoAportante = @TIPOAPORTANTE

	UPDATE Contrato.Contrato SET Contrato.Contrato.ValorAportesOperador = @VALOR
	WHERE Contrato.Contrato.IDContrato = @IDCONTRATO
	---APORTE ICBF

	SELECT  @VALORICBF = SUM(Contrato.AporteContrato.ValorAporte)
	FROM 
	Contrato.AporteContrato
	where
	Contrato.AporteContrato.IdContrato = @IDCONTRATO
	AND Contrato.AporteContrato.TipoAportante = @TIPOAPORTANTE

	UPDATE Contrato.Contrato SET Contrato.Contrato.ValorAportesICBF = @VALORICBF
	WHERE Contrato.Contrato.IDContrato = @IDCONTRATO

END


