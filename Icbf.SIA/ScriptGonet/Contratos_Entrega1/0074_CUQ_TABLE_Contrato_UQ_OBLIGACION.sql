USE [SIA]
GO

/****** Object:  Index [UQ_OBLIGACION]    Script Date: 08/03/2013 12:03:35 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Contrato].[Obligacion]') AND name = N'UQ_OBLIGACION')
DROP INDEX [UQ_OBLIGACION] ON [Contrato].[Obligacion] WITH ( ONLINE = OFF )
GO

   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea un indice en la tabla obligaci�n 
   -- =============================================

TRUNCATE TABLE [Contrato].[Obligacion]
GO

USE [SIA]
GO

/****** Object:  Index [UQ_OBLIGACION]    Script Date: 08/03/2013 12:03:37 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_OBLIGACION] ON [Contrato].[Obligacion] 
(
	[IdTipoObligacion] ASC,
	[IdTipoContrato] ASC,
	[Descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

