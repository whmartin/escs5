USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]    Script Date: 08/15/2013 19:57:58 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]
GO

-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:        Realiza una consulta a la entidad [Contrato].[ObjetoContrato]
--					   En base a varios parametros.
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]
@IdObjetoContratoContractual INT = NULL, @ObjetoContractual NVARCHAR (500) = NULL, @Estado BIT = NULL
AS
BEGIN

SELECT
	IdObjetoContratoContractual,
	ObjetoContractual,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[ObjetoContrato]
WHERE IdObjetoContratoContractual =
	CASE
		WHEN @IdObjetoContratoContractual IS NULL THEN IdObjetoContratoContractual ELSE @IdObjetoContratoContractual
	END 
	AND ObjetoContractual LIKE '%' + 
	CASE
		WHEN @ObjetoContractual IS NULL THEN ObjetoContractual ELSE @ObjetoContractual
	END   + '%'
	AND Estado =
	CASE
		WHEN @Estado IS NULL THEN Estado ELSE @Estado
	END
	
END
GO