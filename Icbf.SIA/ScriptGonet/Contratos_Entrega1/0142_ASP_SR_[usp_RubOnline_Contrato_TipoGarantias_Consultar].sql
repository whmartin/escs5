USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]    Script Date: 08/15/2013 20:13:24 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantias_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]
@NombreTipoGarantia NVARCHAR (128) = NULL,
@Estado bit = null
AS
BEGIN
SELECT
	IdTipoGarantia,
	NombreTipoGarantia,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[TipoGarantia]
WHERE NombreTipoGarantia LIKE '%' + 
	CASE
		WHEN @NombreTipoGarantia IS NULL THEN NombreTipoGarantia ELSE @NombreTipoGarantia
	END + '%'
AND (Estado = @Estado OR @Estado IS NULL)
ORDER BY NombreTipoGarantia
END



GO