USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]    Script Date: 07/30/2013 23:12:32 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]
GO

-- =============================================
-- Author:                  @ReS\Cesar Casanova
-- Create date:         15/06/2013 08:10
-- Description:          Procedimiento almacenado que consulta Obligacion
-- Modificado por Jonathan Acosta - Consulta por estado del contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]
@Descripcion nvarchar (128) = NULL, @IdTipoObligacion INT = NULL, @IdTipoContrato INT = NULL, @Estado BIT = NULL
AS
BEGIN
SELECT
	IdObligacion,
	IdTipoObligacion,
	IdTipoContrato,
	Descripcion,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[Obligacion]
WHERE IdTipoObligacion =
	CASE
		WHEN @IdTipoObligacion IS NULL THEN IdTipoObligacion ELSE @IdTipoObligacion
	END
AND Descripcion =
	CASE
		WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
	END
AND IdTipoContrato =
	CASE
		WHEN @IdTipoContrato IS NULL THEN IdTipoContrato ELSE @IdTipoContrato
	END
AND (Estado = @Estado OR @Estado IS NULL)
ORDER BY IdTipoContrato, Descripcion
END


GO