USE [SIA]
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
El dia : 07/06/2013
Permite : Crear el modulo y asignarle permisos a administrador
***********************************************/
IF not exists(select IdModulo from SEG.Modulo where NombreModulo = 'CONTRATOS')
BEGIN
INSERT INTO [SEG].[Modulo]
           ([NombreModulo]
           ,[Posicion]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           )
     VALUES
           ('CONTRATOS'
           ,0
           ,'True'
           ,'@ReS'
           ,GETDATE()
           )
END
ELSE
BEGIN
      print 'YA EXISTE EL MODULO A CREAR'
END