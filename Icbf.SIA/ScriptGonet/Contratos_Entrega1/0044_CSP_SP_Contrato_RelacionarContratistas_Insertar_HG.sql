USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Insertar]    Script Date: 07/30/2013 21:20:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Insertar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Henry Gomez
Permite : Crea el procedimiento para insertar Contratistas - Contrato
***********************************************/


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Insertar]
		@IdContratistaContrato INT OUTPUT, 	@IdContrato INT,	@NumeroIdentificacion BIGINT,	@ClaseEntidad NVARCHAR,	@PorcentajeParticipacion INT,	@NumeroIdentificacionRepresentanteLegal BIGINT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.RelacionarContratistas(IdContrato, NumeroIdentificacion, ClaseEntidad, PorcentajeParticipacion, NumeroIdentificacionRepresentanteLegal, UsuarioCrea, FechaCrea)
					  VALUES(@IdContrato, @NumeroIdentificacion, @ClaseEntidad, @PorcentajeParticipacion, @NumeroIdentificacionRepresentanteLegal, @UsuarioCrea, GETDATE())
	SELECT @IdContratistaContrato = @@IDENTITY 		
END
GO


