
USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         06/08/2013 12:00m
   -- Description:         Cambia el estado de las opciones del menu a no visibles
   --
   -- =============================================

UPDATE SEG.PROGRAMA SET VisibleMenu = '0'  WHERE NombrePrograma =  'Categoria contrato' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'  WHERE NombrePrograma =  'Tipo Clausula' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Forma de pago' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'  WHERE NombrePrograma =  'Modalidad selecci�n' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'  WHERE NombrePrograma =  'Obligaci�n' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0' WHERE NombrePrograma =  'Regimen contrataci�n' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0' WHERE NombrePrograma =  'Tipo amparo' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Tipo garantia' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'  WHERE NombrePrograma =  'Tipo interventor' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Convenio marco' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Tipo contrato' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Clausula Contrato' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Garant�a y amparo' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Amparos' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Objeto contrato' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Garant�a' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Estado del Contrato' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Cl�usulas y Obligaciones' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Tipo Obligaci�n' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Relacionar Supervisor' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Consulta Supervisor' 
GO
--
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Informaci�n Presupuestal' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Valor aporte' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Valor aporte icbf' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Unidad de medida' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Aporte contrato' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Aporte ICBF' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Informaci�n rp' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Lugar ejecuci�n' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Gestionar obligaciones' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Relaci�n Contrtista' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Relaci�n Consorcio' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Contratos relacionados' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Relaci�n contratos adhesi�n' 
GO
UPDATE SEG.PROGRAMA SET VisibleMenu = '0'   WHERE NombrePrograma =  'Relacionar supervisor' 
GO