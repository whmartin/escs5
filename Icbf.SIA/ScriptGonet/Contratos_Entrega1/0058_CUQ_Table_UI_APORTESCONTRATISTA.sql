USE [SIA]
GO

/****** Object:  Index [UI_APORTESCONTRATISTA]    Script Date: 08/01/2013 19:45:29 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Contrato].[AporteContrato]') AND name = N'UI_APORTESCONTRATISTA')
DROP INDEX [UI_APORTESCONTRATISTA] ON [Contrato].[AporteContrato] WITH ( ONLINE = OFF )
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Andr�s Morales
Permite : Crea el indice
***********************************************/

/****** Object:  Index [UI_APORTESCONTRATISTA]    Script Date: 08/01/2013 19:45:33 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UI_APORTESCONTRATISTA] ON [Contrato].[AporteContrato] 
(
	[IdContrato] ASC,
	[NumeroIdenticacionContratista] ASC,
	[TipoAporte] ASC,
	[ValorAporte] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO




