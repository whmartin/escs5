USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]    Script Date: 08/21/2013 16:23:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el sp_usp_RubOnline_Contrato_CategoriaContratos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]

	@NombreCategoriaContrato NVARCHAR (50) = NULL,
	@DescripcionCategoriaContrato NVARCHAR (50) = NULL,
	@Estado BIT = NULL

AS
BEGIN

	SELECT
		IdCategoriaContrato,
		NombreCategoriaContrato,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[CategoriaContrato]
	WHERE NombreCategoriaContrato LIKE '%' + 
		CASE
			WHEN @NombreCategoriaContrato IS NULL THEN NombreCategoriaContrato ELSE @NombreCategoriaContrato
		END + '%'
	AND Descripcion LIKE '%' + 
		CASE
			WHEN @DescripcionCategoriaContrato IS NULL THEN Descripcion ELSE @DescripcionCategoriaContrato
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreCategoriaContrato 

END


GO


