USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]    Script Date: 08/20/2013 14:36:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ClausulaContratos_Consultar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]

	@NombreClausulaContrato NVARCHAR (128) = NULL, 
	@Contenido NVARCHAR (128) = NULL,
	@IdTipoClausula INT = NULL, 
	@IdTipoContrato INT = NULL,
	@Estado bit = NULL

AS
BEGIN

	SELECT
		IdClausulaContrato,
		NombreClausulaContrato,
		IdTipoClausula,
		Contenido,
		IdTipoContrato,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[ClausulaContrato]
	WHERE NombreClausulaContrato LIKE '%' + 
		CASE
			WHEN @NombreClausulaContrato IS NULL THEN NombreClausulaContrato ELSE @NombreClausulaContrato
		END + '%'
	AND Contenido LIKE '%' + 
		CASE
			WHEN @Contenido IS NULL THEN Contenido ELSE @Contenido
		END + '%'
	AND IdTipoClausula =
		CASE
			WHEN @IdTipoClausula IS NULL THEN IdTipoClausula ELSE @IdTipoClausula
		END
	AND IdTipoContrato =
		CASE
			WHEN @IdTipoContrato IS NULL THEN IdTipoContrato ELSE @IdTipoContrato
		END
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreClausulaContrato

END


