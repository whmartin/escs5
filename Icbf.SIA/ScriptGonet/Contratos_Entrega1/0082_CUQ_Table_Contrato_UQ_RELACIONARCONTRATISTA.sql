USE [SIA]
GO

/****** Object:  Index [UQ_RELACIONARCONTRATISTA]    Script Date: 08/05/2013 23:14:01 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Contrato].[RelacionarContratistas]') AND name = N'UQ_RELACIONARCONTRATISTA')
DROP INDEX [UQ_RELACIONARCONTRATISTA] ON [Contrato].[RelacionarContratistas] WITH ( ONLINE = OFF )
GO

   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea el indice �nico en la tabla relacionarcontratistas
   -- =============================================

TRUNCATE TABLE [Contrato].[RelacionarContratistas]
GO

USE [SIA]
GO

/****** Object:  Index [UQ_RELACIONARCONTRATISTA]    Script Date: 08/05/2013 23:14:10 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_RELACIONARCONTRATISTA] ON [Contrato].[RelacionarContratistas] 
(
	[IdContrato] ASC,
	[NumeroIdentificacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


