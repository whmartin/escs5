USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]    Script Date: 07/30/2013 23:05:17 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacions_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]
@NombreTipoObligacion NVARCHAR (128) = NULL,
@Estado bit = null
AS
BEGIN
SELECT
	IdTipoObligacion,
	NombreTipoObligacion,
	Descripcion,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[TipoObligacion]
WHERE NombreTipoObligacion =
	CASE
		WHEN @NombreTipoObligacion IS NULL THEN NombreTipoObligacion ELSE @NombreTipoObligacion
	END
AND (Estado = @Estado OR @Estado IS NULL)
ORDER BY NombreTipoObligacion
END

GO