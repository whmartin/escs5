USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]    Script Date: 08/15/2013 20:09:45 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]
GO
-- =============================================
-- Author:                  @ReS\Cesar Casanova
-- Create date:         09/008/2013 10:01
-- Description:          Procedimiento almacenado que consulta los tipos de contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]
@NombreTipoContrato NVARCHAR (128) = NULL,
@IdCategoriaContrato INT = NULL,
@Estado BIT = NULL,
@ACTAINICIO BIT = NULL,
@APORTECOFINANCIACION BIT = NULL,
@RECURSOFINANCIERO BIT = NULL,
@REGIMENCONTRATO INT = NULL,
@DESCRIPCIONTIPOCONTRATO NVARCHAR (128) = NULL
AS
BEGIN

	SELECT
		IdTipoContrato,
		NombreTipoContrato,
		IdCategoriaContrato,
		ActaInicio,
		AporteCofinaciacion,
		RecursoFinanciero,
		RegimenContrato,
		DescripcionTipoContrato,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoContrato]
	WHERE NombreTipoContrato LIKE '%' + 
		CASE
			WHEN @NombreTipoContrato IS NULL THEN NombreTipoContrato ELSE @NombreTipoContrato
		END + '%'
	AND IdCategoriaContrato =
		CASE
			WHEN @IdCategoriaContrato IS NULL THEN IdCategoriaContrato ELSE @IdCategoriaContrato
		END
	AND (Estado = @Estado OR @Estado IS NULL)
	AND (ACTAINICIO = @ACTAINICIO OR @ACTAINICIO IS NULL)
	AND (APORTECOFINACIACION = @APORTECOFINANCIACION OR @APORTECOFINANCIACION IS NULL)
	AND (RECURSOFINANCIERO = @RECURSOFINANCIERO OR @RECURSOFINANCIERO IS NULL)
	AND (REGIMENCONTRATO = @REGIMENCONTRATO OR @REGIMENCONTRATO IS NULL)
	AND (DESCRIPCIONTIPOCONTRATO LIKE '%' +  @DESCRIPCIONTIPOCONTRATO + '%' OR @DESCRIPCIONTIPOCONTRATO IS NULL)
	ORDER BY NombreTipoContrato

END
GO