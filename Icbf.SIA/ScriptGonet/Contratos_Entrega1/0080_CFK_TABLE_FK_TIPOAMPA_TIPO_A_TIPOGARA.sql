USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoAmparo]'))
ALTER TABLE [Contrato].[TipoAmparo] DROP CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]
GO

   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea la llave foranea con la tabla tipoamparo
   -- =============================================

TRUNCATE TABLE [Contrato].[TipoAmparo]
GO

USE [SIA]
GO

ALTER TABLE [Contrato].[TipoAmparo]  WITH CHECK ADD  CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA] FOREIGN KEY([IdTipoGarantia])
REFERENCES [Contrato].[TipoGarantia] ([IdTipoGarantia])
GO

ALTER TABLE [Contrato].[TipoAmparo] CHECK CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]
GO


