USE [SIA]
GO

/****** Object:  Index [UQ_NombreClausulaContrato]    Script Date: 08/13/2013 21:08:41 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]') AND name = N'UQ_NombreClausulaContrato')
ALTER TABLE [Contrato].[ClausulaContrato] DROP CONSTRAINT [UQ_NombreClausulaContrato]
GO



USE [SIA]
GO
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         09/008/2013 10:01
   -- Description:          CREA LA LLAVE UNICA EN CLAUSULA CONTRATO
   -- =============================================
/****** Object:  Index [UQ_NombreClausulaContrato]    Script Date: 08/13/2013 21:08:43 ******/
ALTER TABLE [Contrato].[ClausulaContrato] ADD  CONSTRAINT [UQ_NombreClausulaContrato] UNIQUE NONCLUSTERED 
(
	[NombreClausulaContrato] ASC,
	[IdTipoClausula] ASC,
	[IdTipoContrato] ASC
	--[Contenido] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


