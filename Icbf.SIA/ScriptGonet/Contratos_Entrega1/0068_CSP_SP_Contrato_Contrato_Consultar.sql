USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]    Script Date: 08/05/2013 09:27:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]
GO


-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que consulta un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]
	@IdContrato INT
AS
BEGIN
 SELECT IdContrato,
FechaRegistro,
NumeroProceso,
NumeroContrato,
FechaAdjudicacion,
IdModalidad,
IdCategoriaContrato,
IdTipoContrato,
IdCodigoModalidad,
IdModalidadAcademica,
IdCodigoProfesion,
IdNombreProfesion,
IdRegionalContrato,
RequiereActa,
ManejaAportes,
ManejaRecursos,
ManejaVigenciasFuturas,
IdRegimenContratacion,
CodigoRegional,
NombreSolicitante,
DependenciaSolicitante,
CargoSolicitante,
ObjetoContrato,
AlcanceObjetoContrato,
ValorInicialContrato,
ValorTotalAdiciones,
ValorFinalContrato,
ValorAportesICBF,
ValorAportesOperador,
ValorTotalReduccion,
JustificacionAdicionSuperior50porc,
FechaSuscripcion,
FechaInicioEjecucion,
FechaFinalizacionInicial,
PlazoInicial,
FechaInicialTerminacion,
FechaFinalTerminacion,
FechaProyectadaLiquidacion,
FechaAnulacion,
Prorrogas,
PlazoTotal,
FechaFirmaActaInicio,
VigenciaFiscalInicial,
VigenciaFiscalFinal,
IdUnidadEjecucion,
IdLugarEjecucion,
DatosAdicionales,
IdEstadoContrato,
IdTipoDocumentoContratista,
IdentificacionContratista,
NombreContratista,
IdFormaPago,
IdTipoEntidad,
UsuarioCrea,
FechaCrea,
UsuarioModifica,
FechaModifica,
ClaseContrato,
Consecutivo,
AfectaPlanCompras,
IdSolicitante,
IdProducto,
FechaLiquidacion,
NumeroDocumentoVigenciaFutura
 FROM [Contrato].[Contrato] 
 WHERE  IdContrato = @IdContrato
END

GO

