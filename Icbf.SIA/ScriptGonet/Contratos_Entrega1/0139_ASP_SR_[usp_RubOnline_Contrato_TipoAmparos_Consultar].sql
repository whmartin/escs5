USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]    Script Date: 08/15/2013 20:03:37 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Consultar los tipos de amparos
Modificado por Jonathan Acosta
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]

	@NombreTipoAmparo NVARCHAR (50) = NULL, @IdTipoGarantia INT = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdTipoAmparo,
		NombreTipoAmparo,
		IdTipoGarantia,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoAmparo]
	WHERE NombreTipoAmparo LIKE '%' + 
		CASE
			WHEN @NombreTipoAmparo IS NULL THEN NombreTipoAmparo ELSE @NombreTipoAmparo
		END  + '%'
		AND IdTipoGarantia =
		CASE
			WHEN @IdTipoGarantia IS NULL THEN IdTipoGarantia ELSE @IdTipoGarantia
		END
	AND (Estado = @Estado OR @Estado IS NULL)

END
GO