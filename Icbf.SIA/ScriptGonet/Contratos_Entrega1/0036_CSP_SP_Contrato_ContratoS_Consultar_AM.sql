USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]    Script Date: 07/30/2013 11:19:53 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contratos_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]
GO

-- =============================================
-- Author:		@Res\Jonatha Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que consulta un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]
@FechaRegistro DATETIME = NULL,
@NumeroProceso NVARCHAR (50) = NULL,
@NumeroContrato nvarchar (50) = NULL,
@IdModalidad INT = NULL,
@IdCategoriaContrato INT = NULL,
@IdTipoContrato INT = NULL,
@ClaseContrato nchar (1) = null
AS
BEGIN

SELECT
	CONVERT(date, FechaRegistro),
	IdContrato,
	FechaRegistro,
	NumeroProceso,
	NumeroContrato,
	FechaAdjudicacion,
	IdModalidad,
	IdCategoriaContrato,
	IdTipoContrato,
	IdCodigoModalidad,
	IdModalidadAcademica,
	IdCodigoProfesion,
	IdNombreProfesion,
	IdRegionalContrato,
	RequiereActa,
	ManejaAportes,
	ManejaRecursos,
	ManejaVigenciasFuturas,
	IdRegimenContratacion,
	CodigoRegional,
	NombreSolicitante,
	DependenciaSolicitante,
	CargoSolicitante,
	ObjetoContrato,
	AlcanceObjetoContrato,
	ValorInicialContrato,
	ValorTotalAdiciones,
	ValorFinalContrato,
	ValorAportesICBF,
	ValorAportesOperador,
	ValorTotalReduccion,
	JustificacionAdicionSuperior50porc,
	FechaSuscripcion,
	FechaInicioEjecucion,
	FechaFinalizacionInicial,
	PlazoInicial,
	FechaInicialTerminacion,
	FechaFinalTerminacion,
	FechaProyectadaLiquidacion,
	FechaAnulacion,
	Prorrogas,
	PlazoTotal,
	FechaFirmaActaInicio,
	VigenciaFiscalInicial,
	VigenciaFiscalFinal,
	IdUnidadEjecucion,
	IdLugarEjecucion,
	DatosAdicionales,
	IdEstadoContrato,
	IdTipoDocumentoContratista,
	IdentificacionContratista,
	NombreContratista,
	IdFormaPago,
	IdTipoEntidad,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
	--ClaseContrato,
	--Consecutivo,
	--AfectaPlanCompras,
	--IdSolicitante,
	--IdProducto,
	--FechaLiquidacion,
	--NumeroDocumentoVigenciaFutura
FROM Contrato.Contrato
WHERE (NumeroProceso = @NumeroProceso OR @NumeroProceso IS NULL)
AND (NumeroContrato = @NumeroContrato OR @NumeroContrato IS NULL)
AND (IdModalidad = @IdModalidad OR @IdModalidad IS NULL)
AND (IdCategoriaContrato = @IdCategoriaContrato OR @IdCategoriaContrato IS NULL)
AND (IdTipoContrato = @IdTipoContrato OR @IdTipoContrato IS NULL)
AND FechaRegistro =
	--AND (CONVERT(date, FechaRegistro) = CONVERT(date, @FechaRegistro))
	CASE
		WHEN @FechaRegistro IS NULL THEN FechaRegistro ELSE @FechaRegistro
	END
--AND ClaseContrato = ISNULL(@ClaseContrato, ClaseContrato)
END


GO