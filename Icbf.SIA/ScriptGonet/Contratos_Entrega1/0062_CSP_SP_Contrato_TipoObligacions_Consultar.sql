USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]    Script Date: 08/01/2013 23:06:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacions_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]
	@NombreTipoObligacion NVARCHAR(128) = NULL,
	@Descripcion NVARCHAR(128) = NULL,
	@Estado bit = null
AS
BEGIN
 SELECT IdTipoObligacion, NombreTipoObligacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoObligacion]
 WHERE NombreTipoObligacion = CASE WHEN @NombreTipoObligacion IS NULL THEN NombreTipoObligacion ELSE @NombreTipoObligacion END
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END
 AND (Estado = @Estado OR @Estado IS NULL)
 ORDER BY NombreTipoObligacion
END

GO


