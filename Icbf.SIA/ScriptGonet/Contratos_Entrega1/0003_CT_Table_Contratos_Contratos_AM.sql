USE [SIA]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Andres Morales
El dia : 26/07/2013
Permite : Script consolidado que crea por primera vez
		  las tablas correspondientes a los casos de uso
001,002,003,004,005,006,007,008,009,010,011,012,013,
014,015,016,017,018,019,020,021,022,023,024,025,026,
027,028,029,031,033
***********************************************/

/****** Object:  ForeignKey [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]    Script Date: 07/26/2013 16:56:00 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]') AND parent_object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]'))
ALTER TABLE [Contrato].[ClausulaContrato] DROP CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]
GO
/****** Object:  ForeignKey [FK_CLAUSULA_FK_CLAUSU_TIPOCONT]    Script Date: 07/26/2013 16:56:00 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_CLAUSULA_FK_CLAUSU_TIPOCONT]') AND parent_object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]'))
ALTER TABLE [Contrato].[ClausulaContrato] DROP CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCONT]
GO
/****** Object:  ForeignKey [FK_OBLIGACI_FK_OBLIGA_TIPOCONT]    Script Date: 07/26/2013 16:56:59 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_OBLIGACI_FK_OBLIGA_TIPOCONT]') AND parent_object_id = OBJECT_ID(N'[Contrato].[Obligacion]'))
ALTER TABLE [Contrato].[Obligacion] DROP CONSTRAINT [FK_OBLIGACI_FK_OBLIGA_TIPOCONT]
GO
/****** Object:  ForeignKey [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]    Script Date: 07/26/2013 16:57:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoAmparo]'))
ALTER TABLE [Contrato].[TipoAmparo] DROP CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]
GO
/****** Object:  ForeignKey [FK_TIPOCONT_FK_TIPO_C_CATEGORI]    Script Date: 07/26/2013 16:57:37 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOCONT_FK_TIPO_C_CATEGORI]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoContrato]'))
ALTER TABLE [Contrato].[TipoContrato] DROP CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI]
GO
/****** Object:  Table [Contrato].[ClausulaContrato]    Script Date: 07/26/2013 16:56:00 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]') AND parent_object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]'))
ALTER TABLE [Contrato].[ClausulaContrato] DROP CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_CLAUSULA_FK_CLAUSU_TIPOCONT]') AND parent_object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]'))
ALTER TABLE [Contrato].[ClausulaContrato] DROP CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCONT]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[ClausulaContrato]
GO
/****** Object:  Table [Contrato].[Obligacion]    Script Date: 07/26/2013 16:56:59 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_OBLIGACI_FK_OBLIGA_TIPOCONT]') AND parent_object_id = OBJECT_ID(N'[Contrato].[Obligacion]'))
ALTER TABLE [Contrato].[Obligacion] DROP CONSTRAINT [FK_OBLIGACI_FK_OBLIGA_TIPOCONT]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[Obligacion]') AND type in (N'U'))
DROP TABLE [Contrato].[Obligacion]
GO
/****** Object:  Table [Contrato].[TipoContrato]    Script Date: 07/26/2013 16:57:37 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOCONT_FK_TIPO_C_CATEGORI]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoContrato]'))
ALTER TABLE [Contrato].[TipoContrato] DROP CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[TipoContrato]
GO
/****** Object:  Table [Contrato].[TipoAmparo]    Script Date: 07/26/2013 16:57:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoAmparo]'))
ALTER TABLE [Contrato].[TipoAmparo] DROP CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoAmparo]') AND type in (N'U'))
DROP TABLE [Contrato].[TipoAmparo]
GO
/****** Object:  Table [Contrato].[TipoClausula]    Script Date: 07/26/2013 16:57:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoClausula]') AND type in (N'U'))
DROP TABLE [Contrato].[TipoClausula]
GO
/****** Object:  Table [Contrato].[TipoGarantia]    Script Date: 07/26/2013 16:57:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoGarantia]') AND type in (N'U'))
DROP TABLE [Contrato].[TipoGarantia]
GO
/****** Object:  Table [Contrato].[TipoObligacion]    Script Date: 07/26/2013 16:57:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoObligacion]') AND type in (N'U'))
DROP TABLE [Contrato].[TipoObligacion]
GO
/****** Object:  Table [Contrato].[TipoSupvInterventor]    Script Date: 07/26/2013 16:57:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoSupvInterventor]') AND type in (N'U'))
DROP TABLE [Contrato].[TipoSupvInterventor]
GO
/****** Object:  Table [Contrato].[UnidadMedida]    Script Date: 07/26/2013 16:57:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[UnidadMedida]') AND type in (N'U'))
DROP TABLE [Contrato].[UnidadMedida]
GO
/****** Object:  Table [Contrato].[RegimenContratacion]    Script Date: 07/26/2013 16:57:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[RegimenContratacion]') AND type in (N'U'))
DROP TABLE [Contrato].[RegimenContratacion]
GO
/****** Object:  Table [Contrato].[SecuenciaNumeroContrato]    Script Date: 07/26/2013 16:57:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[SecuenciaNumeroContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[SecuenciaNumeroContrato]
GO
/****** Object:  Table [Contrato].[SecuenciaNumeroProceso]    Script Date: 07/26/2013 16:57:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[SecuenciaNumeroProceso]') AND type in (N'U'))
DROP TABLE [Contrato].[SecuenciaNumeroProceso]
GO
/****** Object:  Table [Contrato].[SupervisorIntervContrato]    Script Date: 07/26/2013 16:57:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[SupervisorIntervContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[SupervisorIntervContrato]
GO
/****** Object:  Table [Contrato].[TablaParametrica]    Script Date: 07/26/2013 16:57:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TablaParametrica]') AND type in (N'U'))
DROP TABLE [Contrato].[TablaParametrica]
GO
/****** Object:  Table [Contrato].[AmparosAsociados]    Script Date: 07/26/2013 16:55:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[AmparosAsociados]') AND type in (N'U'))
DROP TABLE [Contrato].[AmparosAsociados]
GO
/****** Object:  Table [Contrato].[AporteContrato]    Script Date: 07/26/2013 16:55:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[AporteContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[AporteContrato]
GO
/****** Object:  Table [Contrato].[CategoriaContrato]    Script Date: 07/26/2013 16:55:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[CategoriaContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[CategoriaContrato]
GO
/****** Object:  Table [Contrato].[Contrato]    Script Date: 07/26/2013 16:56:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[Contrato]') AND type in (N'U'))
DROP TABLE [Contrato].[Contrato]
GO
/****** Object:  Table [Contrato].[FormaPago]    Script Date: 07/26/2013 16:56:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[FormaPago]') AND type in (N'U'))
DROP TABLE [Contrato].[FormaPago]
GO
/****** Object:  Table [Contrato].[Garantia]    Script Date: 07/26/2013 16:56:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[Garantia]') AND type in (N'U'))
DROP TABLE [Contrato].[Garantia]
GO
/****** Object:  Table [Contrato].[GestionarClausulasContrato]    Script Date: 07/26/2013 16:56:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[GestionarClausulasContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[GestionarClausulasContrato]
GO
/****** Object:  Table [Contrato].[GestionarObligacion]    Script Date: 07/26/2013 16:56:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[GestionarObligacion]') AND type in (N'U'))
DROP TABLE [Contrato].[GestionarObligacion]
GO
/****** Object:  Table [Contrato].[InformacionPresupuestal]    Script Date: 07/26/2013 16:56:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[InformacionPresupuestal]') AND type in (N'U'))
DROP TABLE [Contrato].[InformacionPresupuestal]
GO
/****** Object:  Table [Contrato].[InformacionPresupuestalRP]    Script Date: 07/26/2013 16:56:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[InformacionPresupuestalRP]') AND type in (N'U'))
DROP TABLE [Contrato].[InformacionPresupuestalRP]
GO
/****** Object:  Table [Contrato].[LugarEjecucion]    Script Date: 07/26/2013 16:56:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[LugarEjecucion]') AND type in (N'U'))
DROP TABLE [Contrato].[LugarEjecucion]
GO
/****** Object:  Table [Contrato].[LugarEjecucionContrato]    Script Date: 07/26/2013 16:56:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[LugarEjecucionContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[LugarEjecucionContrato]
GO
/****** Object:  Table [Contrato].[ModalidadSeleccion]    Script Date: 07/26/2013 16:56:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ModalidadSeleccion]') AND type in (N'U'))
DROP TABLE [Contrato].[ModalidadSeleccion]
GO
/****** Object:  Table [Contrato].[ObjetoContrato]    Script Date: 07/26/2013 16:56:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ObjetoContrato]') AND type in (N'U'))
DROP TABLE [Contrato].[ObjetoContrato]
GO
/****** Object:  Table [Contrato].[ObjetoContrato]    Script Date: 07/26/2013 16:56:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ObjetoContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[ObjetoContrato](
	[IdObjetoContratoContractual] [int] IDENTITY(1,1) NOT NULL,
	[ObjetoContractual] [nvarchar](500) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ObjetoContrato] PRIMARY KEY CLUSTERED 
(
	[IdObjetoContratoContractual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[ModalidadSeleccion]    Script Date: 07/26/2013 16:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ModalidadSeleccion]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[ModalidadSeleccion](
	[IdModalidad] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](128) NOT NULL,
	[Sigla] [nvarchar](3) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ModalidadSeleccion] PRIMARY KEY CLUSTERED 
(
	[IdModalidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreModalidadSeleccion] UNIQUE NONCLUSTERED 
(
	[Nombre] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[LugarEjecucionContrato]    Script Date: 07/26/2013 16:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[LugarEjecucionContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[LugarEjecucionContrato](
	[IdContratoLugarEjecucion] [int] IDENTITY(1,1) NOT NULL,
	[IDContrato] [int] NOT NULL,
	[Nacional] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_LugarEjecucionContrato] PRIMARY KEY CLUSTERED 
(
	[IdContratoLugarEjecucion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[LugarEjecucion]    Script Date: 07/26/2013 16:56:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[LugarEjecucion]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[LugarEjecucion](
	[IdContratoLugarEjecucion] [int] NOT NULL,
	[IDDepartamento] [int] NOT NULL,
	[IDMunicipio] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[InformacionPresupuestalRP]    Script Date: 07/26/2013 16:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[InformacionPresupuestalRP]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[InformacionPresupuestalRP](
	[IdInformacionPresupuestalRP] [int] IDENTITY(1,1) NOT NULL,
	[FechaSolicitudRP] [datetime] NOT NULL,
	[NumeroRP] [int] NOT NULL,
	[ValorRP] [int] NOT NULL,
	[FechaExpedicionRP] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InformacionPresupuestalRP] PRIMARY KEY CLUSTERED 
(
	[IdInformacionPresupuestalRP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NumeroRP] UNIQUE NONCLUSTERED 
(
	[NumeroRP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'IdInformacionPresupuestalRP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'IdInformacionPresupuestalRP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'FechaSolicitudRP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de Solicitud RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'FechaSolicitudRP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'NumeroRP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número del RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'NumeroRP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'ValorRP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor del RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'ValorRP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'FechaExpedicionRP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha expedición RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'FechaExpedicionRP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificación del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestalRP', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Registro de información Presupuestal Información de RP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestalRP'
GO
/****** Object:  Table [Contrato].[InformacionPresupuestal]    Script Date: 07/26/2013 16:56:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[InformacionPresupuestal]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[InformacionPresupuestal](
	[IdInformacionPresupuestal] [int] IDENTITY(1,1) NOT NULL,
	[NumeroCDP] [int] NOT NULL,
	[ValorCDP] [numeric](18, 3) NOT NULL,
	[FechaExpedicionCDP] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_InformacionPresupuestal] PRIMARY KEY CLUSTERED 
(
	[IdInformacionPresupuestal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NumeroCDP] UNIQUE NONCLUSTERED 
(
	[NumeroCDP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', N'COLUMN',N'IdInformacionPresupuestal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'IdInformacionPresupuestal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', N'COLUMN',N'NumeroCDP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número del CDP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'NumeroCDP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', N'COLUMN',N'ValorCDP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor del CDP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'ValorCDP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', N'COLUMN',N'FechaExpedicionCDP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha expedición CDP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'FechaExpedicionCDP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificación del registro' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'Contrato', N'TABLE',N'InformacionPresupuestal', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Registro de información Presupuestal Información de CDP' , @level0type=N'SCHEMA',@level0name=N'Contrato', @level1type=N'TABLE',@level1name=N'InformacionPresupuestal'
GO
/****** Object:  Table [Contrato].[GestionarObligacion]    Script Date: 07/26/2013 16:56:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[GestionarObligacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[GestionarObligacion](
	[IdGestionObligacion] [int] IDENTITY(1,1) NOT NULL,
	[IdGestionClausula] [int] NOT NULL,
	[NombreObligacion] [nvarchar](10) NOT NULL,
	[IdTipoObligacion] [int] NOT NULL,
	[Orden] [nvarchar](128) NOT NULL,
	[DescripcionObligacion] [nvarchar](128) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_GestionarObligacion] PRIMARY KEY CLUSTERED 
(
	[IdGestionObligacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[GestionarClausulasContrato]    Script Date: 07/26/2013 16:56:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[GestionarClausulasContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[GestionarClausulasContrato](
	[IdGestionClausula] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [nvarchar](3) NOT NULL,
	[NombreClausula] [nvarchar](10) NOT NULL,
	[TipoClausula] [int] NOT NULL,
	[Orden] [nvarchar](128) NOT NULL,
	[DescripcionClausula] [nvarchar](128) NOT NULL,
	[OrdenNumero] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_GestionarClausulasContrato] PRIMARY KEY CLUSTERED 
(
	[IdGestionClausula] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[Garantia]    Script Date: 07/26/2013 16:56:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[Garantia]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[Garantia](
	[IDGarantia] [int] IDENTITY(1,1) NOT NULL,
	[IDContrato] [int] NOT NULL,
	[IDTipoGarantia] [int] NOT NULL,
	[NumGarantia] [nvarchar](128) NOT NULL,
	[FechaExpedicion] [datetime] NOT NULL,
	[FechaVencInicial] [datetime] NOT NULL,
	[FechaRecibo] [datetime] NOT NULL,
	[FechaDevolucion] [datetime] NOT NULL,
	[MotivoDevolucion] [nvarchar](128) NOT NULL,
	[FechaAprobacion] [datetime] NOT NULL,
	[FechaCertificacionPago] [datetime] NOT NULL,
	[Valor] [numeric](18, 3) NOT NULL,
	[Estado] [bit] NOT NULL,
	[Anexo] [bit] NOT NULL,
	[ObservacionAnexo] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Garantia] PRIMARY KEY CLUSTERED 
(
	[IDGarantia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[FormaPago]    Script Date: 07/26/2013 16:56:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[FormaPago]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[FormaPago](
	[IdFormapago] [int] IDENTITY(1,1) NOT NULL,
	[NombreFormaPago] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_FormaPago] PRIMARY KEY CLUSTERED 
(
	[IdFormapago] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreFormaPago] UNIQUE NONCLUSTERED 
(
	[NombreFormaPago] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[Contrato]    Script Date: 07/26/2013 16:56:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[Contrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[Contrato](
	[IdContrato] [int] IDENTITY(1,1) NOT NULL,
	[FechaRegistro] [datetime] NULL,
	[NumeroProceso] [nvarchar](50) NULL,
	[NumeroContrato] [nvarchar](50) NULL,
	[FechaAdjudicacion] [datetime] NULL,
	[IdModalidad] [int] NULL,
	[IdCategoriaContrato] [int] NULL,
	[IdTipoContrato] [int] NULL,
	[IdCodigoModalidad] [int] NULL,
	[IdModalidadAcademica] [int] NULL,
	[IdCodigoProfesion] [int] NULL,
	[IdNombreProfesion] [int] NULL,
	[IdRegionalContrato] [int] NULL,
	[RequiereActa] [bit] NULL,
	[ManejaAportes] [bit] NULL,
	[ManejaRecursos] [bit] NULL,
	[ManejaVigenciasFuturas] [bit] NULL,
	[IdRegimenContratacion] [int] NULL,
	[CodigoRegional] [int] NULL,
	[NombreSolicitante] [nvarchar](50) NULL,
	[DependenciaSolicitante] [nvarchar](50) NULL,
	[CargoSolicitante] [nvarchar](150) NULL,
	[ObjetoContrato] [nvarchar](300) NULL,
	[AlcanceObjetoContrato] [nvarchar](128) NULL,
	[ValorInicialContrato] [numeric](18, 3) NULL,
	[ValorTotalAdiciones] [numeric](18, 3) NULL,
	[ValorFinalContrato] [numeric](18, 3) NULL,
	[ValorAportesICBF] [numeric](18, 3) NULL,
	[ValorAportesOperador] [numeric](18, 3) NULL,
	[ValorTotalReduccion] [numeric](18, 3) NULL,
	[JustificacionAdicionSuperior50porc] [nvarchar](300) NULL,
	[FechaSuscripcion] [datetime] NULL,
	[FechaInicioEjecucion] [datetime] NULL,
	[FechaFinalizacionInicial] [datetime] NULL,
	[PlazoInicial] [int] NULL,
	[FechaInicialTerminacion] [datetime] NULL,
	[FechaFinalTerminacion] [datetime] NULL,
	[FechaProyectadaLiquidacion] [datetime] NULL,
	[FechaAnulacion] [datetime] NULL,
	[Prorrogas] [int] NULL,
	[PlazoTotal] [int] NULL,
	[FechaFirmaActaInicio] [datetime] NULL,
	[VigenciaFiscalInicial] [int] NULL,
	[VigenciaFiscalFinal] [int] NULL,
	[IdUnidadEjecucion] [int] NULL,
	[IdLugarEjecucion] [int] NULL,
	[DatosAdicionales] [nvarchar](128) NULL,
	[IdEstadoContrato] [int] NULL,
	[IdTipoDocumentoContratista] [nvarchar](3) NULL,
	[IdentificacionContratista] [nvarchar](50) NULL,
	[NombreContratista] [nvarchar](128) NULL,
	[IdFormaPago] [int] NULL,
	[IdTipoEntidad] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Contrato] PRIMARY KEY CLUSTERED 
(
	[IdContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[CategoriaContrato]    Script Date: 07/26/2013 16:55:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[CategoriaContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[CategoriaContrato](
	[IdCategoriaContrato] [int] IDENTITY(1,1) NOT NULL,
	[NombreCategoriaContrato] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](100) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_CategoriaContrato] PRIMARY KEY CLUSTERED 
(
	[IdCategoriaContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreCategoriaContrato] UNIQUE NONCLUSTERED 
(
	[NombreCategoriaContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[AporteContrato]    Script Date: 07/26/2013 16:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[AporteContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[AporteContrato](
	[IdAporte] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [nvarchar](3) NOT NULL,
	[TipoAportante] [nvarchar](11) NOT NULL,
	[NumeroIdenticacionContratista] [bigint] NOT NULL,
	[TipoAporte] [nvarchar](10) NOT NULL,
	[ValorAporte] [int] NOT NULL,
	[DescripcionAporteEspecie] [nvarchar](128) NOT NULL,
	[FechaRP] [datetime] NOT NULL,
	[NumeroRP] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_AporteContrato] PRIMARY KEY CLUSTERED 
(
	[IdAporte] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[AmparosAsociados]    Script Date: 07/26/2013 16:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[AmparosAsociados]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[AmparosAsociados](
	[IdAmparo] [int] IDENTITY(1,1) NOT NULL,
	[IdGarantia] [int] NOT NULL,
	[IdTipoAmparo] [int] NOT NULL,
	[FechaVigenciaDesde] [datetime] NOT NULL,
	[VigenciaHasta] [datetime] NOT NULL,
	[ValorParaCalculoAsegurado] [numeric](18, 3) NOT NULL,
	[IdTipoCalculo] [int] NOT NULL,
	[ValorAsegurado] [numeric](18, 3) NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_AmparosAsociados] PRIMARY KEY CLUSTERED 
(
	[IdAmparo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[TablaParametrica]    Script Date: 07/26/2013 16:57:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TablaParametrica]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[TablaParametrica](
	[IdTablaParametrica] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTablaParametrica] [nvarchar](128) NOT NULL,
	[NombreTablaParametrica] [nvarchar](128) NOT NULL,
	[Url] [nvarchar](256) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TablaParametrica] PRIMARY KEY CLUSTERED 
(
	[IdTablaParametrica] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[SupervisorIntervContrato]    Script Date: 07/26/2013 16:57:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[SupervisorIntervContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[SupervisorIntervContrato](
	[IDSupervisorInterv] [int] IDENTITY(1,1) NOT NULL,
	[IDContratoSupervisa] [int] NOT NULL,
	[OrigenTipoSupervisor] [bit] NOT NULL,
	[IDTipoSupvInterventor] [int] NOT NULL,
	[IDTerceroExterno] [int] NULL,
	[IDFuncionarioInterno] [int] NULL,
	[IDContratoInterventoria] [int] NULL,
	[IDTerceroInterventoria] [int] NULL,
	[TipoVinculacion] [bit] NOT NULL,
	[FechaInicia] [datetime] NOT NULL,
	[FechaFinaliza] [datetime] NOT NULL,
	[Estado] [bit] NOT NULL,
	[FechaModificaInterventor] [datetime] NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](128) NOT NULL,
	[FechaModifica] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_RelacionarSupervisorInterventor] PRIMARY KEY CLUSTERED 
(
	[IDSupervisorInterv] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[SecuenciaNumeroProceso]    Script Date: 07/26/2013 16:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[SecuenciaNumeroProceso]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[SecuenciaNumeroProceso](
	[IdSecuenciaProceso] [int] IDENTITY(1,1) NOT NULL,
	[Consecutivo] [int] NOT NULL,
	[CodigoRegional] [int] NOT NULL,
	[AnoVigencia] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_SecuenciaNumeroProceso] PRIMARY KEY CLUSTERED 
(
	[IdSecuenciaProceso] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[SecuenciaNumeroContrato]    Script Date: 07/26/2013 16:57:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[SecuenciaNumeroContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[SecuenciaNumeroContrato](
	[IdSecuenciaContrato] [int] IDENTITY(1,1) NOT NULL,
	[Consecutivo] [int] NOT NULL,
	[CodigoRegional] [int] NOT NULL,
	[AnoVigencia] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_SecuenciaNumeroContrato] PRIMARY KEY CLUSTERED 
(
	[IdSecuenciaContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[RegimenContratacion]    Script Date: 07/26/2013 16:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[RegimenContratacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[RegimenContratacion](
	[IdRegimenContratacion] [int] IDENTITY(1,1) NOT NULL,
	[NombreRegimenContratacion] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RegimenContratacion] PRIMARY KEY CLUSTERED 
(
	[IdRegimenContratacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreRegimenContratacion] UNIQUE NONCLUSTERED 
(
	[NombreRegimenContratacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[UnidadMedida]    Script Date: 07/26/2013 16:57:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[UnidadMedida]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[UnidadMedida](
	[IdNumeroContrato] [int] IDENTITY(1,1) NOT NULL,
	[NumeroContrato] [nvarchar](50) NULL,
	[FechaInicioEjecuciónContrato] [datetime] NOT NULL,
	[FechaTerminacionInicialContrato] [datetime] NOT NULL,
	[FechaFinalTerminacionContrato] [datetime] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_UnidadMedida] PRIMARY KEY CLUSTERED 
(
	[IdNumeroContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[TipoSupvInterventor]    Script Date: 07/26/2013 16:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoSupvInterventor]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[TipoSupvInterventor](
	[IdTipoSupvInterventor] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoSupvInterventor] PRIMARY KEY CLUSTERED 
(
	[IdTipoSupvInterventor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoSupvInterventor] UNIQUE NONCLUSTERED 
(
	[Nombre] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[TipoObligacion]    Script Date: 07/26/2013 16:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoObligacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[TipoObligacion](
	[IdTipoObligacion] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoObligacion] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoObligacion] PRIMARY KEY CLUSTERED 
(
	[IdTipoObligacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[TipoGarantia]    Script Date: 07/26/2013 16:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoGarantia]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[TipoGarantia](
	[IdTipoGarantia] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoGarantia] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoGarantia] PRIMARY KEY CLUSTERED 
(
	[IdTipoGarantia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoGarantia] UNIQUE NONCLUSTERED 
(
	[NombreTipoGarantia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[TipoClausula]    Script Date: 07/26/2013 16:57:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoClausula]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[TipoClausula](
	[IdTipoClausula] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoClausula] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoClausula] PRIMARY KEY CLUSTERED 
(
	[IdTipoClausula] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoClausula] UNIQUE NONCLUSTERED 
(
	[NombreTipoClausula] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[TipoAmparo]    Script Date: 07/26/2013 16:57:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoAmparo]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[TipoAmparo](
	[IdTipoAmparo] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoAmparo] [nvarchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdTipoGarantia] [int] NOT NULL,
 CONSTRAINT [PK_TipoAmparo] PRIMARY KEY CLUSTERED 
(
	[IdTipoAmparo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoAmparo] UNIQUE NONCLUSTERED 
(
	[NombreTipoAmparo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[TipoContrato]    Script Date: 07/26/2013 16:57:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[TipoContrato](
	[IdTipoContrato] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoContrato] [nvarchar](128) NOT NULL,
	[IdCategoriaContrato] [int] NOT NULL,
	[ActaInicio] [bit] NOT NULL,
	[AporteCofinaciacion] [bit] NOT NULL,
	[RecursoFinanciero] [bit] NOT NULL,
	[RegimenContrato] [int] NOT NULL,
	[DescripcionTipoContrato] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoContrato] PRIMARY KEY CLUSTERED 
(
	[IdTipoContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoContrato] UNIQUE NONCLUSTERED 
(
	[NombreTipoContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[Obligacion]    Script Date: 07/26/2013 16:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[Obligacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[Obligacion](
	[IdObligacion] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoObligacion] [int] NOT NULL,
	[IdTipoContrato] [int] NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Obligacion] PRIMARY KEY CLUSTERED 
(
	[IdObligacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [Contrato].[ClausulaContrato]    Script Date: 07/26/2013 16:56:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[ClausulaContrato](
	[IdClausulaContrato] [int] IDENTITY(1,1) NOT NULL,
	[NombreClausulaContrato] [nvarchar](128) NOT NULL,
	[IdTipoClausula] [int] NOT NULL,
	[Contenido] [nvarchar](128) NULL,
	[IdTipoContrato] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ClausulaContrato] PRIMARY KEY CLUSTERED 
(
	[IdClausulaContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreClausulaContrato] UNIQUE NONCLUSTERED 
(
	[NombreClausulaContrato] ASC,
	[IdTipoClausula] ASC,
	[IdTipoContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  ForeignKey [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]    Script Date: 07/26/2013 16:56:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]') AND parent_object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]'))
ALTER TABLE [Contrato].[ClausulaContrato]  WITH CHECK ADD  CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU] FOREIGN KEY([IdTipoClausula])
REFERENCES [Contrato].[TipoClausula] ([IdTipoClausula])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]') AND parent_object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]'))
ALTER TABLE [Contrato].[ClausulaContrato] CHECK CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU]
GO
/****** Object:  ForeignKey [FK_CLAUSULA_FK_CLAUSU_TIPOCONT]    Script Date: 07/26/2013 16:56:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_CLAUSULA_FK_CLAUSU_TIPOCONT]') AND parent_object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]'))
ALTER TABLE [Contrato].[ClausulaContrato]  WITH CHECK ADD  CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCONT] FOREIGN KEY([IdTipoContrato])
REFERENCES [Contrato].[TipoContrato] ([IdTipoContrato])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_CLAUSULA_FK_CLAUSU_TIPOCONT]') AND parent_object_id = OBJECT_ID(N'[Contrato].[ClausulaContrato]'))
ALTER TABLE [Contrato].[ClausulaContrato] CHECK CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCONT]
GO
/****** Object:  ForeignKey [FK_OBLIGACI_FK_OBLIGA_TIPOCONT]    Script Date: 07/26/2013 16:56:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_OBLIGACI_FK_OBLIGA_TIPOCONT]') AND parent_object_id = OBJECT_ID(N'[Contrato].[Obligacion]'))
ALTER TABLE [Contrato].[Obligacion]  WITH CHECK ADD  CONSTRAINT [FK_OBLIGACI_FK_OBLIGA_TIPOCONT] FOREIGN KEY([IdTipoContrato])
REFERENCES [Contrato].[TipoContrato] ([IdTipoContrato])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_OBLIGACI_FK_OBLIGA_TIPOCONT]') AND parent_object_id = OBJECT_ID(N'[Contrato].[Obligacion]'))
ALTER TABLE [Contrato].[Obligacion] CHECK CONSTRAINT [FK_OBLIGACI_FK_OBLIGA_TIPOCONT]
GO
/****** Object:  ForeignKey [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]    Script Date: 07/26/2013 16:57:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoAmparo]'))
ALTER TABLE [Contrato].[TipoAmparo]  WITH CHECK ADD  CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA] FOREIGN KEY([IdTipoGarantia])
REFERENCES [Contrato].[TipoGarantia] ([IdTipoGarantia])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoAmparo]'))
ALTER TABLE [Contrato].[TipoAmparo] CHECK CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA]
GO
/****** Object:  ForeignKey [FK_TIPOCONT_FK_TIPO_C_CATEGORI]    Script Date: 07/26/2013 16:57:37 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOCONT_FK_TIPO_C_CATEGORI]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoContrato]'))
ALTER TABLE [Contrato].[TipoContrato]  WITH CHECK ADD  CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI] FOREIGN KEY([IdCategoriaContrato])
REFERENCES [Contrato].[CategoriaContrato] ([IdCategoriaContrato])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_TIPOCONT_FK_TIPO_C_CATEGORI]') AND parent_object_id = OBJECT_ID(N'[Contrato].[TipoContrato]'))
ALTER TABLE [Contrato].[TipoContrato] CHECK CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI]
GO
