USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Conratos_Valor_Garantia]    Script Date: 08/07/2013 21:43:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Conratos_Valor_Garantia]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Conratos_Valor_Garantia]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Conratos_Valor_Garantia]    Script Date: 08/07/2013 21:43:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ares\Andr�s Morales
-- Create date:  07/08/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que calcula el valor total de una garant�a
--EXEC dbo.usp_RubOnline_Conratos_Valor_Garantia 31,'NUEVE'
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Conratos_Valor_Garantia]
@idContrato int,
@NUMGARANTIA NVARCHAR(128)
AS
BEGIN
DECLARE
@VALOR NUMERIC(18,3),
@VALORTOTAL NUMERIC(18,3)
SELECT  @VALOR = SUM(Contrato.AmparosAsociados.ValorAsegurado)
FROM 
Contrato.AmparosAsociados,
Contrato.Garantia
where
Contrato.Garantia.IDGarantia = Contrato.AmparosAsociados.IdGarantia
AND Contrato.Garantia.IDContrato = @idContrato
AND Contrato.Garantia.NumGarantia = @NUMGARANTIA

UPDATE Contrato.Garantia SET Contrato.Garantia.Valor = @VALOR
WHERE Contrato.Garantia.IDContrato = @idContrato
AND Contrato.Garantia.NumGarantia = @NUMGARANTIA

UPDATE Contrato.Garantia SET Contrato.Garantia.Valortotal = @VALOR
WHERE Contrato.Garantia.IDContrato = @idContrato
AND Contrato.Garantia.NumGarantia = @NUMGARANTIA
END

GO


