USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]    Script Date: 08/05/2013 23:21:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]    Script Date: 08/05/2013 23:21:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


   
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que consulta Obligacion
   -- Modificado por Jonathan Acosta - Consulta por estado del contrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]
	@Descripcion nvarchar(128)= NULL ,@IdTipoObligacion INT = NULL,@IdTipoContrato INT = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT 
 [Contrato].[Obligacion].IdTipoContrato, 
 [Contrato].[Obligacion].IdObligacion, 
 [Contrato].[Obligacion].IdTipoObligacion, 
 [Contrato].[Obligacion].Descripcion, 
 [Contrato].[Obligacion].Estado, 
 [Contrato].[Obligacion].UsuarioCrea, 
 [Contrato].[Obligacion].FechaCrea, 
 [Contrato].[Obligacion].UsuarioModifica,
  [Contrato].[Obligacion].FechaModifica 
 FROM [Contrato].[Obligacion] , [Contrato].[TipoContrato]
 WHERE 
 [Contrato].[Obligacion].IdTipoObligacion = CASE WHEN @IdTipoObligacion IS NULL THEN [Contrato].[Obligacion].IdTipoObligacion ELSE @IdTipoObligacion END 
 AND [Contrato].[Obligacion].Descripcion = CASE WHEN @Descripcion IS NULL THEN [Contrato].[Obligacion].Descripcion ELSE @Descripcion END 
 AND [Contrato].[Obligacion].IdTipoContrato = CASE WHEN @IdTipoContrato IS NULL THEN [Contrato].[Obligacion].IdTipoContrato ELSE @IdTipoContrato END 
 AND ([Contrato].[Obligacion].Estado = @Estado or @Estado is null)
 AND [Contrato].[Obligacion].IdTipoContrato = [Contrato].[TipoContrato].IdTipoContrato
 ORDER BY  [Contrato].[TipoContrato].DescripcionTipoContrato , [Contrato].[Obligacion].Descripcion ASC
END


GO

