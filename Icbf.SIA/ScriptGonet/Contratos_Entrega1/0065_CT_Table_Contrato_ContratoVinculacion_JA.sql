USE SIA
GO


/****** Object:  Table [Contrato].[ContratoVinculacion]    Script Date: 08/01/2013 10:57:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ContratoVinculacion]') AND type in (N'U'))
DROP TABLE [Contrato].[ContratoVinculacion]
GO
-- =============================================
-- Author:              
-- Create date:         
-- Description:         Crea la entidad [Contrato].[ContratoVinculacion]
-- =============================================
CREATE TABLE [Contrato].[ContratoVinculacion](
	[IdContrato] [int] NULL,
	[IdContratoAsociado] [int] NULL
) ON [PRIMARY]

GO


