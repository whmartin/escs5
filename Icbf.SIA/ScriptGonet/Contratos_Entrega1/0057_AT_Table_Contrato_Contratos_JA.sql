USE [SIA]
GO

-- =============================================
-- Author:              
-- Create date:         
-- Description:        Agrega varias columnas a la entidad Contrato.Contrato
-- =============================================

/* Para evitar posibles problemas de p�rdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del dise�ador de base de datos.*/
IF NOT EXISTS (

	SELECT
		NAME
	FROM SYSCOLUMNS
	WHERE ID = OBJECT_ID('Contrato.Contrato') 
		AND NAME in ('Consecutivo', 'AfectaPlanCompras', 'IdSolicitante', 'IdProducto', 'FechaLiquidacion', 'NumeroDocumentoVigenciaFutura')

	)
BEGIN

	ALTER TABLE Contrato.Contrato ADD
		Consecutivo varchar(50) NULL,
		AfectaPlanCompras bit NULL,
		IdSolicitante int NULL,
		IdProducto int NULL,
		FechaLiquidacion datetime NULL,
		NumeroDocumentoVigenciaFutura int NULL
	
END
