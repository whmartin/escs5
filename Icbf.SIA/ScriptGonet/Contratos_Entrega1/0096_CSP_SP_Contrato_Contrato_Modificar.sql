USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]    Script Date: 08/06/2013 00:01:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]
GO

USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]    Script Date: 08/06/2013 00:01:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]
@IdContrato		int,
@FechaRegistro	datetime,
@NumeroProceso	nvarchar(50),
@NumeroContrato	nvarchar(50),
@FechaAdjudicacion	datetime,
@IdModalidad	int,
@IdCategoriaContrato	int,
@IdTipoContrato	int,
@IdCodigoModalidad	int,
@IdModalidadAcademica	int,
@IdCodigoProfesion	int,
@IdNombreProfesion	int,
@IdRegionalContrato	int,
@RequiereActa	bit,
@ManejaAportes	bit,
@ManejaRecursos	bit,
@ManejaVigenciasFuturas	bit,
@IdRegimenContratacion	int,
@CodigoRegional	int,
@NombreSolicitante	nvarchar(50),
@DependenciaSolicitante	nvarchar(50),
@CargoSolicitante	nvarchar(150),
@ObjetoContrato	nvarchar(300),
@AlcanceObjetoContrato	nvarchar(128),
@ValorInicialContrato	numeric(18, 3),
@ValorTotalAdiciones	numeric(18, 3),
@ValorFinalContrato	numeric(18, 3),
@ValorAportesICBF	numeric(18, 3),
@ValorAportesOperador	numeric(18, 3),
@ValorTotalReduccion	numeric(18, 3),
@JustificacionAdicionSuperior50porc	nvarchar(300),
@FechaSuscripcion	datetime,
@FechaInicioEjecucion	datetime,
@FechaFinalizacionInicial	datetime,
@PlazoInicial	int,
@FechaInicialTerminacion	datetime,
@FechaFinalTerminacion	datetime,
@FechaProyectadaLiquidacion	datetime,
@FechaAnulacion	datetime,
@Prorrogas	int,
@PlazoTotal	int,
@FechaFirmaActaInicio	datetime,
@VigenciaFiscalInicial	int,
@VigenciaFiscalFinal	int,
@IdUnidadEjecucion	int,
@IdLugarEjecucion	int,
@DatosAdicionales	nvarchar(128),
@IdEstadoContrato	int,
@IdTipoDocumentoContratista	nvarchar(3),
@IdentificacionContratista	nvarchar(50),
@NombreContratista	nvarchar(128),
@IdFormaPago	int,
@IdTipoEntidad	int,
@UsuarioModifica NVARCHAR(250),
@ClaseContrato	nchar(1),
@Consecutivo	varchar(50),
@AfectaPlanCompras	bit,
@IdSolicitante	int,
@IdProducto	int,
@FechaLiquidacion	datetime,
@NumeroDocumentoVigenciaFutura	int,
@RequiereGarantia bit = null
AS
BEGIN
	UPDATE Contrato.Contrato 
	SET FechaRegistro = @FechaRegistro,
	NumeroProceso = @NumeroProceso,
	NumeroContrato = @NumeroContrato,
	FechaAdjudicacion = @FechaAdjudicacion,
	IdModalidad = @IdModalidad,
	IdCategoriaContrato = @IdCategoriaContrato,
	IdTipoContrato = @IdTipoContrato,
	IdCodigoModalidad = @IdCodigoModalidad,
	IdModalidadAcademica = @IdModalidadAcademica,
	IdCodigoProfesion = @IdCodigoProfesion,
	IdNombreProfesion = @IdNombreProfesion,
	IdRegionalContrato = @IdRegionalContrato,
	RequiereActa = @RequiereActa,
	ManejaAportes = @ManejaAportes,
	ManejaRecursos = @ManejaRecursos,
	ManejaVigenciasFuturas = @ManejaVigenciasFuturas,
	IdRegimenContratacion = @IdRegimenContratacion,
	CodigoRegional = @CodigoRegional,
	NombreSolicitante = @NombreSolicitante,
	DependenciaSolicitante = @DependenciaSolicitante,
	CargoSolicitante = @CargoSolicitante,
	ObjetoContrato = @ObjetoContrato,
	AlcanceObjetoContrato = @AlcanceObjetoContrato,
	ValorInicialContrato = @ValorInicialContrato,
	ValorTotalAdiciones = @ValorTotalAdiciones,
	ValorFinalContrato = @ValorFinalContrato,
	ValorAportesICBF = @ValorAportesICBF,
	ValorAportesOperador = @ValorAportesOperador,
	ValorTotalReduccion = @ValorTotalReduccion,
	JustificacionAdicionSuperior50porc = @JustificacionAdicionSuperior50porc,
	FechaSuscripcion = @FechaSuscripcion,
	FechaInicioEjecucion = @FechaInicioEjecucion,
	FechaFinalizacionInicial = @FechaFinalizacionInicial,
	PlazoInicial = @PlazoInicial,
	FechaInicialTerminacion = @FechaInicialTerminacion,
	FechaFinalTerminacion = @FechaFinalTerminacion,
	FechaProyectadaLiquidacion =@FechaProyectadaLiquidacion,
	FechaAnulacion = @FechaAnulacion,
	Prorrogas = @Prorrogas,
	PlazoTotal = @PlazoTotal,
	FechaFirmaActaInicio = @FechaFirmaActaInicio,
	VigenciaFiscalInicial = @VigenciaFiscalInicial,
	VigenciaFiscalFinal = @VigenciaFiscalFinal,
	IdUnidadEjecucion = @IdUnidadEjecucion,
	IdLugarEjecucion = @IdLugarEjecucion,
	DatosAdicionales = @DatosAdicionales,
	IdEstadoContrato = @IdEstadoContrato,
	IdTipoDocumentoContratista = @IdTipoDocumentoContratista,
	IdentificacionContratista = @IdentificacionContratista,
	NombreContratista = @NombreContratista,
	IdFormaPago = @IdFormaPago,
	IdTipoEntidad = @IdTipoEntidad,
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE(),
	ClaseContrato = @ClaseContrato,
	Consecutivo = @Consecutivo,
	AfectaPlanCompras = @AfectaPlanCompras,
	IdSolicitante = @IdSolicitante,
	IdProducto = @IdProducto,
	FechaLiquidacion = @FechaLiquidacion,
	NumeroDocumentoVigenciaFutura = @NumeroDocumentoVigenciaFutura,
	@RequiereGarantia = RequiereGarantia
	WHERE IdContrato = @IdContrato
END
GO

