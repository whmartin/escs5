USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Eliminar]    Script Date: 07/31/2013 00:57:01 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Eliminar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Eliminar]
GO
-- =============================================
-- Author:       Juan Carlos Valverde Sámano      
-- Create date:   17/01/2014      
-- Description:         Crea el procedimiento usp_RubOnline_Contrato_RelacionarContratistas_Eliminar
--Que elimina una RelaciónContratista
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Eliminar]
@IdContratistaContrato INT
AS
BEGIN
DELETE Contrato.RelacionarContratistas
WHERE IdContratistaContrato = @IdContratistaContrato
END
GO