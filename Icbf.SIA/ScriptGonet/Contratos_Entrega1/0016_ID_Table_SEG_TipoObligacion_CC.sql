USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:12
   -- Description:         Inserta la tabla para ser visualizada en la aplicación
   -- =============================================
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Contratos/TipoObligacion' and NombrePrograma = 'Tipo Obligación' )
begin
      declare @idpadre int
      select @idpadre = IdPrograma from SEG.Programa where NombrePrograma = 'Contrato'
      insert INTO SEG.Programa 
      SELECT IdModulo,'Tipo Obligación','Contratos/TipoObligacion',1,1,'Administrador',GETDATE(),'','',1,1,NULL,NULL,NULL
      from SEG.Modulo
      where NombreModulo = 'CONTRATOS'    

      INSERT INTO SEG.Permiso 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      from SEG.Programa
      where CodigoPrograma = 'Contratos/TipoObligacion'
      and NombrePrograma = 'Tipo Obligación' 
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
