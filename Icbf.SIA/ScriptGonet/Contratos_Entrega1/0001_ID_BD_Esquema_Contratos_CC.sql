USE [SIA]
GO
-- =============================================
-- Author:              @ReS\Cesar Casanova
-- Create date:         26/06/2013 08:12
-- Description:         Crea el Esquema Contratos en la base de datos
-- =============================================
if NOT exists (SELECT
                        SCHEMA_NAME
                  FROM information_schema.schemata
                  WHERE SCHEMA_NAME = 'Contrato')
BEGIN
      EXEC ('CREATE SCHEMA [Contrato]')
END
GO
