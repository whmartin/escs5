USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         30/06/2013 11:46am
   -- Description:         Inserta en la tabla para ser visualizada en la aplicaci�n
   --						y el permiso al usuario administrador
   -- =============================================
IF NOT EXISTS(SELECT IDPROGRAMA FROM SEG.PROGRAMA 
WHERE
	CODIGOPROGRAMA = 'Contratos/RegistrarContratosMarco' and 
	NOMBREPROGRAMA = 'Contratos Marco' )
BEGIN
      DECLARE @idpadre int
     
      SELECT @idpadre = IdPrograma 
	  FROM SEG.Programa 
	  WHERE 
	  NombrePrograma = 'Contrato'
---	  
      INSERT INTO SEG.Programa 
      SELECT 
      IdModulo,'Contratos Marco','Contratos/RegistrarContratosMarco',1,1,'Administrador',GETDATE(),'','',1,1,NULL,NULL,NULL
      FROM SEG.Modulo
      WHERE 
      NOMBREMODULO = 'CONTRATOS'    
---
      INSERT INTO SEG.PERMISO 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      FROM SEG.Programa
      WHERE 
      CODIGOPROGRAMA = 'Contratos/RegistrarContratosMarco' and 
      NOMBREPROGRAMA = 'Contratos Marco' 
END
ELSE
BEGIN
      PRINT 'El VALOR A INSERTAR YA EXISTE EN LA BASE DE DATOS'
END