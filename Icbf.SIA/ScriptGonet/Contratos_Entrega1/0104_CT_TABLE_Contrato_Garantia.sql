USE [SIA]
GO

/****** Object:  Table [Contrato].[Garantia]    Script Date: 08/08/2013 21:07:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[Garantia]') AND type in (N'U'))
DROP TABLE [Contrato].[Garantia]
GO

USE [SIA]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:         Crea la entidad [Contrato].[Garantia]
-- =============================================
/****** Object:  Table [Contrato].[Garantia]    Script Date: 08/08/2013 21:07:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Contrato].[Garantia](
	[IDGarantia] [int] IDENTITY(1,1) NOT NULL,
	[IDContrato] [int] NOT NULL,
	[IDTipoGarantia] [int] NOT NULL,
	[IDterceroaseguradora] [int] NULL,
	[IDtercerosucursal] [int] NULL,
	[IDcontratistaContato] [int] NULL,
	[IDtipobeneficiario] [int] NULL,
	[NITICBF] [nvarchar](50) NULL,
	[DescBeneficiario] [nvarchar](200) NULL,
	[NumGarantia] [nvarchar](128) NOT NULL,
	[FechaExpedicion] [datetime] NOT NULL,
	[FechaVencInicial] [datetime] NOT NULL,
	[FechaRecibo] [datetime] NOT NULL,
	[FechaDevolucion] [datetime] NOT NULL,
	[MotivoDevolucion] [nvarchar](128) NOT NULL,
	[FechaAprobacion] [datetime] NOT NULL,
	[FechaCertificacionPago] [datetime] NOT NULL,
	[Valor] [numeric](18, 3) NULL,
	[Valortotal] [numeric](18, 3) NULL,
	[Estado] [bit] NOT NULL,
	[Anexo] [bit] NOT NULL,
	[ObservacionAnexo] [nvarchar](256) NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_Garantia] PRIMARY KEY CLUSTERED 
(
	[IDGarantia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

