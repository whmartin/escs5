USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]    Script Date: 08/01/2013 23:26:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausulas_Consultar
SE ADICIONA EL ORDEN POR NOMBRE TIPO CLAUSULA
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]
	@NombreTipoClausula NVARCHAR(50) = NULL,
	@Descripcion NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdTipoClausula, NombreTipoClausula, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[TipoClausula] 
 WHERE NombreTipoClausula = CASE WHEN @NombreTipoClausula IS NULL THEN NombreTipoClausula ELSE @NombreTipoClausula END
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END
 AND (Estado = @Estado OR @Estado IS NULL)
 ORDER BY NombreTipoClausula
END

GO


