USE [SIA]
GO

/****** Object:  Index [UI_LUGAR_EJECUCION]    Script Date: 07/31/2013 18:06:07 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Contrato].[LugarEjecucion]') AND name = N'UI_LUGAR_EJECUCION')
DROP INDEX [UI_LUGAR_EJECUCION] ON [Contrato].[LugarEjecucion] WITH ( ONLINE = OFF )
GO

USE [SIA]
GO
-- =============================================
-- Author:             Juan Carlos Valverde S�mano 
-- Create date:         17/01/2014
-- Description:         Crea el INDEX UI_LUGAR_EJECUCION ON [Contrato].[LugarEjecucion] 
-- =============================================
/****** Object:  Index [UI_LUGAR_EJECUCION]    Script Date: 07/31/2013 18:06:09 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UI_LUGAR_EJECUCION] ON [Contrato].[LugarEjecucion] 
(
	[IdContratoLugarEjecucion] ASC,
	[IDDepartamento] ASC,
	[IDMunicipio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


