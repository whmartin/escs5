USE [SIA]
GO

IF EXISTS (SELECT
	*
FROM sys.tables where  name = 'GestionarClausulasContrato')
BEGIN

	TRUNCATE TABLE [Contrato].[GestionarClausulasContrato]

END
GO

/****** Object:  Index [UQ_GESTIONCALUSLACONTRATO]    Script Date: 07/30/2013 14:19:09 ******/
IF EXISTS (SELECT
	*
FROM sys.indexes
WHERE OBJECT_ID = OBJECT_ID(N'[Contrato].[GestionarClausulasContrato]') AND name = N'UQ_GESTIONCALUSLACONTRATO')
DROP INDEX [UQ_GESTIONCALUSLACONTRATO] ON [Contrato].[GestionarClausulasContrato] WITH (ONLINE = OFF)
GO

USE [SIA]
GO
-- =============================================
-- Author: Juan Carlos Valverde S�mano          
-- Create date:   17/01/2014       
-- Description:  Realiza un reset a la entidad [Contrato].[GestionarClausulasContrato]
-- =============================================
/****** Object:  Index [UQ_GESTIONCALUSLACONTRATO]    Script Date: 07/30/2013 14:19:14 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_GESTIONCALUSLACONTRATO] ON [Contrato].[GestionarClausulasContrato]
(
[IdContrato] ASC,
[OrdenNumero] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO