USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:12
   -- Description:         Inserta la tabla para ser visualizada en la aplicaci�n
   -- =============================================
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Contratos/ModalidadSeleccion' and NombrePrograma = 'Modalidad Selecci�n' )
begin
      declare @idpadre int
      select @idpadre = IdPrograma from SEG.Programa where NombrePrograma = 'Contrato'
      insert INTO SEG.Programa 
      SELECT IdModulo,'Modalidad Selecci�n','Contratos/ModalidadSeleccion',1,1,'Administrador',GETDATE(),'','',1,1,NULL,NULL,NULL
      from SEG.Modulo
      where NombreModulo = 'CONTRATOS'    

      INSERT INTO SEG.Permiso 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      from SEG.Programa
      where CodigoPrograma = 'Contratos/ModalidadSeleccion'
      and NombrePrograma = 'Modalidad Selecci�n' 
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
