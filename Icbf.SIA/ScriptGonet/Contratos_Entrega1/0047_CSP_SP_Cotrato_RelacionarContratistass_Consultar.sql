USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistass_Consultar]    Script Date: 07/31/2013 00:54:31 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarContratistass_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistass_Consultar]
GO
-- =============================================
-- Author:              @ReS\Andr�s Morales
-- Create date:         30/06/2013 11:46am
-- Description:         Consulta todos los contratistas
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistass_Consultar]
@IdContratistaContrato INT = NULL, @IdContrato INT = NULL, @NumeroIdentificacion BIGINT = NULL, @ClaseEntidad NVARCHAR = NULL, @PorcentajeParticipacion INT = NULL, @NumeroIdentificacionRepresentanteLegal BIGINT = NULL
AS
BEGIN
SELECT
	IdContratistaContrato,
	IdContrato,
	NumeroIdentificacion,
	ClaseEntidad,
	PorcentajeParticipacion,
	NumeroIdentificacionRepresentanteLegal,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[RelacionarContratistas]
WHERE IdContratistaContrato =
	CASE
		WHEN @IdContratistaContrato IS NULL THEN IdContratistaContrato ELSE @IdContratistaContrato
	END AND IdContrato =
	CASE
		WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato
	END AND NumeroIdentificacion =
	CASE
		WHEN @NumeroIdentificacion IS NULL THEN NumeroIdentificacion ELSE @NumeroIdentificacion
	END AND ClaseEntidad =
	CASE
		WHEN @ClaseEntidad IS NULL THEN ClaseEntidad ELSE @ClaseEntidad
	END AND PorcentajeParticipacion =
	CASE
		WHEN @PorcentajeParticipacion IS NULL THEN PorcentajeParticipacion ELSE @PorcentajeParticipacion
	END AND NumeroIdentificacionRepresentanteLegal =
	CASE
		WHEN @NumeroIdentificacionRepresentanteLegal IS NULL THEN NumeroIdentificacionRepresentanteLegal ELSE @NumeroIdentificacionRepresentanteLegal
	END
END
GO