USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]    Script Date: 08/13/2013 20:30:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]
GO

   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado consula las clausuas de los contratos
   -- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]
	@IdGestionClausula INT = NULL,
	@IdContrato NVARCHAR(3) = NULL,
	@NombreClausula NVARCHAR(10) = NULL,
	@TipoClausula INT = NULL,
	@Orden NVARCHAR(128) = NULL,
	@Ordennumero NVARCHAR(128) = NULL,
	@DescripcionClausula NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdGestionClausula, IdContrato, NombreClausula, TipoClausula, Orden, DescripcionClausula, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[GestionarClausulasContrato] 
 WHERE 
 IdGestionClausula = 
 CASE WHEN @IdGestionClausula IS NULL THEN IdGestionClausula 
 ELSE @IdGestionClausula END AND 
 IdContrato = 
 CASE WHEN @IdContrato IS NULL THEN IdContrato 
 ELSE @IdContrato END AND 
 NombreClausula = 
 CASE WHEN @NombreClausula IS NULL THEN NombreClausula 
 ELSE @NombreClausula END AND 
 TipoClausula = 
 CASE WHEN @TipoClausula IS NULL THEN TipoClausula 
 ELSE @TipoClausula END AND 
 Orden = 
 CASE WHEN @Orden IS NULL THEN Orden 
 ELSE @Orden END AND 
 DescripcionClausula = 
 CASE WHEN @DescripcionClausula IS NULL THEN DescripcionClausula 
 ELSE @DescripcionClausula END
 AND (Ordennumero = @Ordennumero OR @Ordennumero IS NULL)
 ORDER BY 
 IdContrato,
 [Contrato].[GestionarClausulasContrato].OrdenNumero
END


GO


