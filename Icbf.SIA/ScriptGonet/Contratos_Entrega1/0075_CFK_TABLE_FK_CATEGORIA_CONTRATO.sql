USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_Contrato_CategoriaContrato]') AND parent_object_id = OBJECT_ID(N'[Contrato].[Contrato]'))
ALTER TABLE [Contrato].[Contrato] DROP CONSTRAINT [FK_Contrato_CategoriaContrato]
GO

   -- =============================================
   -- Author:              @ReS\Andrés Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea la llave foranea con la tabla categoría
   -- =============================================

DELETE Contrato.GestionarClausulasContrato
go

delete Contrato.ClausulaContrato
GO

delete Contrato.TipoClausula   
go

delete Contrato.TipoContrato
go

DELETE [Contrato].[Contrato]
go

delete [Contrato].[CategoriaContrato]
GO


USE [SIA]
GO

ALTER TABLE [Contrato].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_CategoriaContrato] FOREIGN KEY([IdCategoriaContrato])
REFERENCES [Contrato].[CategoriaContrato] ([IdCategoriaContrato])
GO

ALTER TABLE [Contrato].[Contrato] CHECK CONSTRAINT [FK_Contrato_CategoriaContrato]
GO

