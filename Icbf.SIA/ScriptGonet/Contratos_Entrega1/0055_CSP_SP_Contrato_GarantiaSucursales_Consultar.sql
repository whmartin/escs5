USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GarantiaSucursal_consultar]    Script Date: 07/31/2013 18:54:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GarantiaSucursal_consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GarantiaSucursal_consultar]
GO

USE [SIA]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Andr�s Morales
Permite : Crear el procedimiento de consulta de Sucursales Terceros
***********************************************/

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GarantiaSucursal_consultar]    Script Date: 07/31/2013 18:54:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GarantiaSucursal_consultar]
@IDTERCERO INT
AS

SELECT
      DIV.Departamento.NombreDepartamento,
      DIV.Municipio.NombreMunicipio,
      Proveedor.Sucursal.Nombre,
      Proveedor.Sucursal.Direccion,
      Proveedor.Sucursal.Correo,
      Proveedor.Sucursal.Indicativo,
      Proveedor.Sucursal.Telefono,
      Proveedor.Sucursal.Extension,
      Proveedor.Sucursal.Celular
FROM Oferente.TERCERO
INNER JOIN Proveedor.EntidadProvOferente
      ON Oferente.TERCERO.IDTERCERO = Proveedor.EntidadProvOferente.IdTercero
INNER JOIN Proveedor.Sucursal
      ON Proveedor.EntidadProvOferente.IdEntidad = Proveedor.Sucursal.IdEntidad
INNER JOIN DIV.Departamento
      ON Proveedor.Sucursal.Departamento = DIV.Departamento.IdDepartamento
INNER JOIN DIV.Municipio
      ON Proveedor.Sucursal.Municipio = DIV.Municipio.IdMunicipio
WHERE Proveedor.EntidadProvOferente.IdTercero = @IDTERCERO
--AND Oferente.TERCERO.RAZONSOCIAL LIKE '%ASEGURADORA%'

GO


