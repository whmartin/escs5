USE [SIA]
GO

TRUNCATE TABLE [Contrato].[SupervisorIntervContrato]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_SupervisorIntervContrato_TipoSupvInterventor]') AND parent_object_id = OBJECT_ID(N'[Contrato].[SupervisorIntervContrato]'))
ALTER TABLE [Contrato].[SupervisorIntervContrato] DROP CONSTRAINT [FK_SupervisorIntervContrato_TipoSupvInterventor]
GO
   -- =============================================
   -- Author:              @ReS\Andrés Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea la llave foranea con la tabla tiposupervisor
   -- =============================================

TRUNCATE TABLE [Contrato].[SupervisorIntervContrato]
GO

USE [SIA]
GO

ALTER TABLE [Contrato].[SupervisorIntervContrato]  WITH CHECK ADD  CONSTRAINT [FK_SupervisorIntervContrato_TipoSupvInterventor] FOREIGN KEY([IDTipoSupvInterventor])
REFERENCES [Contrato].[TipoSupvInterventor] ([IdTipoSupvInterventor])
GO

ALTER TABLE [Contrato].[SupervisorIntervContrato] CHECK CONSTRAINT [FK_SupervisorIntervContrato_TipoSupvInterventor]
GO

