
USE [SIA]
GO
   -- ==========================================================================================
   -- Author:                  @ReS\Andr�s Morales
   -- Create date:         03/07/2013 10:00   
   -- Description:          Inserta las tablas b�sicas para ser consultadas
   -- ==========================================================================================
TRUNCATE TABLE [Contrato].[TablaParametrica]

-- Categoria Contrato
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('00'
           ,'Categor�a Contrato'
           ,'Contratos/CategoriaContrato'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO
--Tipo Cl�usula
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('01'
           ,'Tipo Cl�usula'
           ,'Contratos/TipoClausula'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO
--- Regimen Contrataci�n
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('02'
           ,'Regimen Contrataci�n'
           ,'Contratos/RegimenContratacion'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO

--- Tipo Contrato
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('03'
           ,'Tipo Contrato'
           ,'Contratos/TipoContrato'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO

--- Cl�usula Contrato
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('04'
           ,'Cl�usula Contrato'
           ,'Contratos/ClausulaContrato'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO

--- Forma Pago
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('05'
           ,'Forma Pago'
           ,'Contratos/FormaPago'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO
--- Modalidad Selecci�n
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('06'
           ,'Modalidad Selecc��n'
           ,'Contratos/ModalidadSeleccion'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO

--- Tipo Obligaci�n
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('07'
           ,'Tipo Obligaci�n'
           ,'Contratos/TipoObligacion'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO
--- Tipo Supervisor Interventor
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('08'
           ,'Tipo Supervisor/Interventor'
           ,'Contratos/TipoSupvInterventor'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO

--- Objeto Contrato
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('09'
           ,'Objeto Contrato'
           ,'Contratos/ObjetoContrato'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO

--- Obligaci�n
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('10'
           ,'Obligaci�n'
           ,'Contratos/Obligacion'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO
--- Tipo Garantia
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('11'
           ,'Tipo Garant�a'
           ,'Contratos/TipoGarantia'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO

--- Tipo Amparo
INSERT INTO [Contrato].[TablaParametrica]
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea])
     VALUES
           ('12'
           ,'Tipo Amparo'
           ,'Contratos/TipoAmparo'
           ,'True'
           ,'administrador'
           ,'2013-06-10 16:43:00.000')
GO

SELECT * FROM [Contrato].[TablaParametrica]