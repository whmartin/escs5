USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Consulta_ContratistaContrato]    Script Date: 08/01/2013 22:31:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Consulta_ContratistaContrato]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Consulta_ContratistaContrato]
GO

USE [SIA]
GO

-- =============================================
-- Author:		@Res\Andr�s Morales
-- Create date:  08/01/2013 11:48:15 AM
-- Description:	Procedimiento almacenado Consulta los Contratista asociados a un contrato
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Consulta_ContratistaContrato]
@IDCONTRATO INT
AS

	SELECT 
	Contrato.RelacionarContratistas.IdContratistaContrato ,
	Contrato.RelacionarContratistas.NumeroIdentificacion,
	Oferente.TERCERO.RAZONSOCIAL,
	CASE Contrato.RelacionarContratistas.ClaseEntidad
		WHEN 1 THEN 'Contratista'
		WHEN 2 THEN 'Consorcio/Uni�n Temporal'
	END ENTIDAD
	FROM
	Contrato.RelacionarContratistas,
	Oferente.TERCERO
	where
	Oferente.TERCERO.NUMEROIDENTIFICACION = Contrato.RelacionarContratistas.NUMEROIDENTIFICACION
	AND IDCONTRATO = @IDCONTRATO


GO


