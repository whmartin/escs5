USE [SIA]
GO

TRUNCATE TABLE Contrato.GestionarObligacion
GO

Delete Contrato.GestionarClausulasContrato
GO

-- =============================================
-- Author:		Ares\Andres Morales
-- Create date:  13/08/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que crea el constraint a gestionar clasulas contrato
-- EXEC [usp_RubOnline_Contrato_Garantias_Consultar]
-- =============================================

/****** Object:  Index [UQ_CONTRATOGESTIONCLAUSULA]    Script Date: 08/12/2013 14:27:26 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Contrato].[GestionarClausulasContrato]') AND name = N'UQ_CONTRATOGESTIONCLAUSULA')
DROP INDEX [UQ_CONTRATOGESTIONCLAUSULA] ON [Contrato].[GestionarClausulasContrato] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [UQ_CONTRATOGESTIONCLAUSULA]    Script Date: 08/12/2013 14:27:29 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_CONTRATOGESTIONCLAUSULA] ON [Contrato].[GestionarClausulasContrato] 
(
	[TipoClausula] ASC,
	[IdContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO