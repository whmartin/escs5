USE [SIA]
GO

   -- =============================================
   -- Author:              @ReS\Cesar Casanova
   -- Description:         Crear la tabla de relacionar contratistas
   -- =============================================

/****** Object:  Table [Contrato].[RelacionarContratistas]    Script Date: 08/12/2013 20:11:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[RelacionarContratistas]') AND type in (N'U'))
DROP TABLE [Contrato].[RelacionarContratistas]
GO

CREATE TABLE [Contrato].[RelacionarContratistas](
	[IdContratistaContrato] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [int] NOT NULL,
	[NumeroIdentificacion] [bigint] NOT NULL,
	[ClaseEntidad] [nvarchar](1) NOT NULL,
	[PorcentajeParticipacion] [int] NOT NULL,
	[NumeroIdentificacionRepresentanteLegal] [bigint] NOT NULL,
	[EstadoIntegrante] [bit] NULL,	
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RelacionarContratistas] PRIMARY KEY CLUSTERED 
(
	[IdContratistaContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


