USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_Contrato_FormaPago]') AND parent_object_id = OBJECT_ID(N'[Contrato].[Contrato]'))
ALTER TABLE [Contrato].[Contrato] DROP CONSTRAINT [FK_Contrato_FormaPago]
GO

   -- =============================================
   -- Author:              @ReS\Andrés Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea la llave foranea con la tabla formapago
   -- =============================================


TRUNCATE TABLE [Contrato].[Contrato]
GO
USE [SIA]
GO

ALTER TABLE [Contrato].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_FormaPago] FOREIGN KEY([IdFormaPago])
REFERENCES [Contrato].[FormaPago] ([IdFormapago])
GO

ALTER TABLE [Contrato].[Contrato] CHECK CONSTRAINT [FK_Contrato_FormaPago]
GO


