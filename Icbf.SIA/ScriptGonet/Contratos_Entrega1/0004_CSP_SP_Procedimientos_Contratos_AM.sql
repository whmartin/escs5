USE [SIA]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Andres Morales
El dia : 26/07/2013
Permite : Script consolidado que crea por primera vez
		  los procedimientos almacenados corespondientes 
		  a los casos de uso
001,002,003,004,005,006,007,008,009,010,011,012,013,
014,015,016,017,018,019,020,021,022,023,024,025,026,
027,028,029,031,033
***********************************************/


/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]    Script Date: 07/26/2013 17:05:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]    Script Date: 07/26/2013 17:05:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]    Script Date: 07/26/2013 17:05:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]    Script Date: 07/26/2013 17:05:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]    Script Date: 07/26/2013 17:05:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContratos_Consultar]    Script Date: 07/26/2013 17:05:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContratos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]    Script Date: 07/26/2013 17:06:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]    Script Date: 07/26/2013 17:06:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]    Script Date: 07/26/2013 17:06:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]    Script Date: 07/26/2013 17:06:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]    Script Date: 07/26/2013 17:06:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]    Script Date: 07/26/2013 17:06:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]    Script Date: 07/26/2013 17:06:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]    Script Date: 07/26/2013 17:06:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]    Script Date: 07/26/2013 17:06:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]    Script Date: 07/26/2013 17:06:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]    Script Date: 07/26/2013 17:06:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]    Script Date: 07/26/2013 17:06:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]    Script Date: 07/26/2013 17:06:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]    Script Date: 07/26/2013 17:06:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]    Script Date: 07/26/2013 17:06:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar]    Script Date: 07/26/2013 17:05:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]    Script Date: 07/26/2013 17:05:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]    Script Date: 07/26/2013 17:05:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Insertar]    Script Date: 07/26/2013 17:05:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]    Script Date: 07/26/2013 17:05:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]    Script Date: 07/26/2013 17:05:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]    Script Date: 07/26/2013 17:05:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]    Script Date: 07/26/2013 17:05:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]    Script Date: 07/26/2013 17:05:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]    Script Date: 07/26/2013 17:05:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]    Script Date: 07/26/2013 17:05:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]    Script Date: 07/26/2013 17:05:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]    Script Date: 07/26/2013 17:05:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]    Script Date: 07/26/2013 17:05:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Insertar]    Script Date: 07/26/2013 17:05:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]    Script Date: 07/26/2013 17:05:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantias_Consultar]    Script Date: 07/26/2013 17:05:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantias_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantias_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]    Script Date: 07/26/2013 17:05:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]    Script Date: 07/26/2013 17:05:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]    Script Date: 07/26/2013 17:05:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]    Script Date: 07/26/2013 17:05:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]    Script Date: 07/26/2013 17:05:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]    Script Date: 07/26/2013 17:05:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]    Script Date: 07/26/2013 17:05:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]    Script Date: 07/26/2013 17:05:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]    Script Date: 07/26/2013 17:05:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]    Script Date: 07/26/2013 17:05:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Consultar]    Script Date: 07/26/2013 17:05:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]    Script Date: 07/26/2013 17:05:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]    Script Date: 07/26/2013 17:05:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]    Script Date: 07/26/2013 17:05:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]    Script Date: 07/26/2013 17:05:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]    Script Date: 07/26/2013 17:05:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]    Script Date: 07/26/2013 17:05:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]    Script Date: 07/26/2013 17:05:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]    Script Date: 07/26/2013 17:05:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]    Script Date: 07/26/2013 17:05:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]    Script Date: 07/26/2013 17:05:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]    Script Date: 07/26/2013 17:05:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]    Script Date: 07/26/2013 17:05:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]    Script Date: 07/26/2013 17:05:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]    Script Date: 07/26/2013 17:05:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]    Script Date: 07/26/2013 17:05:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]    Script Date: 07/26/2013 17:06:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]    Script Date: 07/26/2013 17:06:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]    Script Date: 07/26/2013 17:06:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]    Script Date: 07/26/2013 17:06:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]    Script Date: 07/26/2013 17:06:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]    Script Date: 07/26/2013 17:06:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]    Script Date: 07/26/2013 17:06:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]    Script Date: 07/26/2013 17:06:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Modificar]    Script Date: 07/26/2013 17:06:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantia_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]    Script Date: 07/26/2013 17:06:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]    Script Date: 07/26/2013 17:06:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]    Script Date: 07/26/2013 17:06:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]    Script Date: 07/26/2013 17:06:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Modificar]    Script Date: 07/26/2013 17:06:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacion_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]    Script Date: 07/26/2013 17:06:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]    Script Date: 07/26/2013 17:06:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]    Script Date: 07/26/2013 17:06:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]    Script Date: 07/26/2013 17:06:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]    Script Date: 07/26/2013 17:06:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]    Script Date: 07/26/2013 17:06:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]    Script Date: 07/26/2013 17:06:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]    Script Date: 07/26/2013 17:06:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]    Script Date: 07/26/2013 17:06:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]    Script Date: 07/26/2013 17:06:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]    Script Date: 07/26/2013 17:06:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]    Script Date: 07/26/2013 17:06:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]    Script Date: 07/26/2013 17:06:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]    Script Date: 07/26/2013 17:06:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]    Script Date: 07/26/2013 17:06:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]    Script Date: 07/26/2013 17:06:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]    Script Date: 07/26/2013 17:06:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]    Script Date: 07/26/2013 17:06:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]    Script Date: 07/26/2013 17:06:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]    Script Date: 07/26/2013 17:06:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]    Script Date: 07/26/2013 17:06:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]    Script Date: 07/26/2013 17:06:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]    Script Date: 07/26/2013 17:06:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]    Script Date: 07/26/2013 17:06:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]    Script Date: 07/26/2013 17:05:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]    Script Date: 07/26/2013 17:05:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]    Script Date: 07/26/2013 17:05:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]    Script Date: 07/26/2013 17:06:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]    Script Date: 07/26/2013 17:06:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]    Script Date: 07/26/2013 17:06:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]    Script Date: 07/26/2013 17:06:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]    Script Date: 07/26/2013 17:06:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]    Script Date: 07/26/2013 17:06:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]    Script Date: 07/26/2013 17:06:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]    Script Date: 07/26/2013 17:06:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]    Script Date: 07/26/2013 17:05:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]    Script Date: 07/26/2013 17:05:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]    Script Date: 07/26/2013 17:05:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]    Script Date: 07/26/2013 17:05:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]    Script Date: 07/26/2013 17:05:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]    Script Date: 07/26/2013 17:05:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]    Script Date: 07/26/2013 17:05:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]    Script Date: 07/26/2013 17:05:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]    Script Date: 07/26/2013 17:05:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]    Script Date: 07/26/2013 17:05:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]    Script Date: 07/26/2013 17:05:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]    Script Date: 07/26/2013 17:05:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]    Script Date: 07/26/2013 17:05:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]    Script Date: 07/26/2013 17:05:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]    Script Date: 07/26/2013 17:05:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]    Script Date: 07/26/2013 17:05:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]    Script Date: 07/26/2013 17:05:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]    Script Date: 07/26/2013 17:05:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]    Script Date: 07/26/2013 17:05:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]    Script Date: 07/26/2013 17:05:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que actualiza un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]
		@TipoSupervisorInterventor INT,	@NumeroContrato NVARCHAR(50),	@NumeroIdentificacion INT,	@NombreRazonSocialSupervisorInterventor NVARCHAR(50),	@NumeroIdentificacionDirectorInterventoria INT,	@NombreRazonSocialDirectorInterventoria NVARCHAR(50), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ConsultarSupervisorInterventor SET NumeroContrato = @NumeroContrato, NumeroIdentificacion = @NumeroIdentificacion, NombreRazonSocialSupervisorInterventor = @NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria = @NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria = @NombreRazonSocialDirectorInterventoria, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE TipoSupervisorInterventor = @TipoSupervisorInterventor
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]    Script Date: 07/26/2013 17:05:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que guarda un nuevo ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]
		@TipoSupervisorInterventor INT OUTPUT, 	@NumeroContrato NVARCHAR(50),	@NumeroIdentificacion INT,	@NombreRazonSocialSupervisorInterventor NVARCHAR(50),	@NumeroIdentificacionDirectorInterventoria INT,	@NombreRazonSocialDirectorInterventoria NVARCHAR(50), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ConsultarSupervisorInterventor(NumeroContrato, NumeroIdentificacion, NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroContrato, @NumeroIdentificacion, @NombreRazonSocialSupervisorInterventor, @NumeroIdentificacionDirectorInterventoria, @NombreRazonSocialDirectorInterventoria, @UsuarioCrea, GETDATE())
	SELECT @TipoSupervisorInterventor = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]    Script Date: 07/26/2013 17:05:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que elimina un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]
	@TipoSupervisorInterventor INT
AS
BEGIN
	DELETE Contrato.ConsultarSupervisorInterventor WHERE TipoSupervisorInterventor = @TipoSupervisorInterventor
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]    Script Date: 07/26/2013 17:05:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que consulta un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]
	@TipoSupervisorInterventor INT
AS
BEGIN
 SELECT TipoSupervisorInterventor, NumeroContrato, NumeroIdentificacion, NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ConsultarSupervisorInterventor] WHERE  TipoSupervisorInterventor = @TipoSupervisorInterventor
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]    Script Date: 07/26/2013 17:05:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el sp_usp_RubOnline_Contrato_CategoriaContratos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContratos_Consultar]
	@NombreCategoriaContrato NVARCHAR(50) = NULL,
	@Estado BIT = NULL
AS
BEGIN
 SELECT IdCategoriaContrato, NombreCategoriaContrato, Descripcion, Estado, 
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[CategoriaContrato] 
 WHERE NombreCategoriaContrato = CASE WHEN @NombreCategoriaContrato IS NULL THEN NombreCategoriaContrato ELSE @NombreCategoriaContrato END
 and (Estado = @Estado or @Estado is null)
 ORDER BY NombreCategoriaContrato
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]    Script Date: 07/26/2013 17:05:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]
		@IdCategoriaContrato INT,	@NombreCategoriaContrato NVARCHAR(50),	@Descripcion NVARCHAR(100),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.CategoriaContrato SET NombreCategoriaContrato = @NombreCategoriaContrato, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdCategoriaContrato = @IdCategoriaContrato
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]    Script Date: 07/26/2013 17:05:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]
		@IdCategoriaContrato INT OUTPUT, 	@NombreCategoriaContrato NVARCHAR(50),	@Descripcion NVARCHAR(100),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.CategoriaContrato(NombreCategoriaContrato, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreCategoriaContrato, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdCategoriaContrato = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]    Script Date: 07/26/2013 17:05:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]
	@IdCategoriaContrato INT
AS
BEGIN
	DELETE Contrato.CategoriaContrato WHERE IdCategoriaContrato = @IdCategoriaContrato
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]    Script Date: 07/26/2013 17:05:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]
	@IdCategoriaContrato INT
AS
BEGIN
 SELECT IdCategoriaContrato, NombreCategoriaContrato, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[CategoriaContrato] WHERE  IdCategoriaContrato = @IdCategoriaContrato
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]    Script Date: 07/26/2013 17:05:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContratos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]
	@IdAporte INT = NULL,@IdContrato NVARCHAR(3) = NULL,@TipoAportante NVARCHAR(11) = NULL,@NumeroIdenticacionContratista bigint = NULL,@TipoAporte NVARCHAR(10) = NULL,@ValorAporte INT = NULL,@DescripcionAporteEspecie NVARCHAR(128) = NULL,@FechaRP DATETIME = NULL,@NumeroRP INT = NULL
AS
BEGIN
 SELECT IdAporte, IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AporteContrato] WHERE IdAporte = CASE WHEN @IdAporte IS NULL THEN IdAporte ELSE @IdAporte END AND IdContrato = CASE WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato END AND TipoAportante = CASE WHEN @TipoAportante IS NULL THEN TipoAportante ELSE @TipoAportante END AND NumeroIdenticacionContratista = CASE WHEN @NumeroIdenticacionContratista IS NULL THEN NumeroIdenticacionContratista ELSE @NumeroIdenticacionContratista END AND TipoAporte = CASE WHEN @TipoAporte IS NULL THEN TipoAporte ELSE @TipoAporte END AND ValorAporte = CASE WHEN @ValorAporte IS NULL THEN ValorAporte ELSE @ValorAporte END AND DescripcionAporteEspecie = CASE WHEN @DescripcionAporteEspecie IS NULL THEN DescripcionAporteEspecie ELSE @DescripcionAporteEspecie END AND FechaRP = CASE WHEN @FechaRP IS NULL THEN FechaRP ELSE @FechaRP END AND NumeroRP = CASE WHEN @NumeroRP IS NULL THEN NumeroRP ELSE @NumeroRP END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]    Script Date: 07/26/2013 17:05:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]
		@IdAporte INT,	@IdContrato NVARCHAR(3),	@TipoAportante NVARCHAR(11),	@NumeroIdenticacionContratista bigint,	@TipoAporte NVARCHAR(10),	@ValorAporte INT,	@DescripcionAporteEspecie NVARCHAR(128),	@FechaRP DATETIME,	@NumeroRP INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.AporteContrato SET IdContrato = @IdContrato, TipoAportante = @TipoAportante, NumeroIdenticacionContratista = @NumeroIdenticacionContratista, TipoAporte = @TipoAporte, ValorAporte = @ValorAporte, DescripcionAporteEspecie = @DescripcionAporteEspecie, FechaRP = @FechaRP, NumeroRP = @NumeroRP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdAporte = @IdAporte
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]    Script Date: 07/26/2013 17:05:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Insertar
***********************************************/


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]
		@IdAporte INT OUTPUT, 	@IdContrato NVARCHAR(3),	@TipoAportante NVARCHAR(11),	@NumeroIdenticacionContratista bigint,	@TipoAporte NVARCHAR(10),	@ValorAporte INT,	@DescripcionAporteEspecie NVARCHAR(128),	@FechaRP DATETIME,	@NumeroRP INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.AporteContrato(IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea)
					  VALUES(@IdContrato, @TipoAportante, @NumeroIdenticacionContratista, @TipoAporte, @ValorAporte, @DescripcionAporteEspecie, @FechaRP, @NumeroRP, @UsuarioCrea, GETDATE())
	SELECT @IdAporte = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]    Script Date: 07/26/2013 17:05:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Eliminar
***********************************************/
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]
	@IdAporte INT
AS
BEGIN
	DELETE Contrato.AporteContrato WHERE IdAporte = @IdAporte
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]    Script Date: 07/26/2013 17:05:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]
	@IdAporte INT
AS
BEGIN
 SELECT IdAporte, IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AporteContrato] WHERE  IdAporte = @IdAporte
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]    Script Date: 07/26/2013 17:05:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociadoss_Consultar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]
	@IdGarantia INT = NULL,@FechaVigenciaDesde DATETIME = NULL,@VigenciaHasta DATETIME = NULL
AS
BEGIN
 SELECT IdAmparo, IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AmparosAsociados] WHERE IdGarantia = CASE WHEN @IdGarantia IS NULL THEN IdGarantia ELSE @IdGarantia END AND FechaVigenciaDesde = CASE WHEN @FechaVigenciaDesde IS NULL THEN FechaVigenciaDesde ELSE @FechaVigenciaDesde END AND VigenciaHasta = CASE WHEN @VigenciaHasta IS NULL THEN VigenciaHasta ELSE @VigenciaHasta END
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]    Script Date: 07/26/2013 17:05:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]
		@IdAmparo INT,	@IdGarantia INT,	@IdTipoAmparo INT,	@FechaVigenciaDesde DATETIME,	@VigenciaHasta DATETIME,	@ValorParaCalculoAsegurado NUMERIC(18,3),	@IdTipoCalculo INT,	@ValorAsegurado NUMERIC(18,3), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.AmparosAsociados SET IdGarantia = @IdGarantia, IdTipoAmparo = @IdTipoAmparo, FechaVigenciaDesde = @FechaVigenciaDesde, VigenciaHasta = @VigenciaHasta, ValorParaCalculoAsegurado = @ValorParaCalculoAsegurado, IdTipoCalculo = @IdTipoCalculo, ValorAsegurado = @ValorAsegurado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdAmparo = @IdAmparo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]    Script Date: 07/26/2013 17:05:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]
		@IdAmparo INT OUTPUT, 	@IdGarantia INT,	@IdTipoAmparo INT,	@FechaVigenciaDesde DATETIME,	@VigenciaHasta DATETIME,	@ValorParaCalculoAsegurado NUMERIC(18,3),	@IdTipoCalculo INT,	@ValorAsegurado NUMERIC(18,3), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.AmparosAsociados(IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea)
					  VALUES(@IdGarantia, @IdTipoAmparo, @FechaVigenciaDesde, @VigenciaHasta, @ValorParaCalculoAsegurado, @IdTipoCalculo, @ValorAsegurado, @UsuarioCrea, GETDATE())
	SELECT @IdAmparo = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]    Script Date: 07/26/2013 17:05:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]
	@IdAmparo INT
AS
BEGIN
	DELETE Contrato.AmparosAsociados WHERE IdAmparo = @IdAmparo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]    Script Date: 07/26/2013 17:05:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]
	@IdAmparo INT
AS
BEGIN
 SELECT IdAmparo, IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AmparosAsociados] WHERE  IdAmparo = @IdAmparo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]    Script Date: 07/26/2013 17:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContratos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]
	@IdObjetoContratoContractual INT = NULL,@ObjetoContractual NVARCHAR(500) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdObjetoContratoContractual, ObjetoContractual, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ObjetoContrato] WHERE IdObjetoContratoContractual = CASE WHEN @IdObjetoContratoContractual IS NULL THEN IdObjetoContratoContractual ELSE @IdObjetoContratoContractual END AND ObjetoContractual = CASE WHEN @ObjetoContractual IS NULL THEN ObjetoContractual ELSE @ObjetoContractual END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]    Script Date: 07/26/2013 17:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]
		@IdObjetoContratoContractual INT,	@ObjetoContractual NVARCHAR(500),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM Contrato.ObjetoContrato WHERE ObjetoContractual = @ObjetoContractual AND IdObjetoContratoContractual NOT IN (@IdObjetoContratoContractual))
	BEGIN
		UPDATE Contrato.ObjetoContrato SET ObjetoContractual = @ObjetoContractual, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdObjetoContratoContractual = @IdObjetoContratoContractual
	END
	ELSE
	BEGIN
		RAISERROR (''Registro duplicado, verifique por favor.'', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]    Script Date: 07/26/2013 17:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContrato_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]
		@IdObjetoContratoContractual INT OUTPUT, 	@ObjetoContractual NVARCHAR(500),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	if not EXISTS (SELECT 1 FROM Contrato.ObjetoContrato WHERE ObjetoContractual = @ObjetoContractual)
	BEGIN
	INSERT INTO Contrato.ObjetoContrato(ObjetoContractual, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@ObjetoContractual, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdObjetoContratoContractual = @@IDENTITY 		
	END
	ELSE
	BEGIN
		RAISERROR (''Registro duplicado, verifique por favor.'', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]    Script Date: 07/26/2013 17:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContrato_Eliminar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]
	@IdObjetoContratoContractual INT
AS
BEGIN
	DELETE Contrato.ObjetoContrato WHERE IdObjetoContratoContractual = @IdObjetoContratoContractual
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]    Script Date: 07/26/2013 17:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContrato_Consultar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]
	@IdObjetoContratoContractual INT
AS
BEGIN
 SELECT IdObjetoContratoContractual, ObjetoContractual, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[ObjetoContrato] 
 WHERE  IdObjetoContratoContractual = @IdObjetoContratoContractual
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]    Script Date: 07/26/2013 17:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 10:29
   -- Description:          Procedimiento almacenado que consulta ModalidadSeleccion
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccions_Consultar]
	@Nombre NVARCHAR(128) = NULL,@Sigla NVARCHAR(3) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdModalidad, Nombre, Sigla, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ModalidadSeleccion] WHERE Nombre = CASE WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre END AND Sigla = CASE WHEN @Sigla IS NULL THEN Sigla ELSE @Sigla END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]    Script Date: 07/26/2013 17:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]
		@IdModalidad INT,	@Nombre NVARCHAR(128),	@Sigla NVARCHAR(3),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ModalidadSeleccion SET Nombre = @Nombre, Sigla = @Sigla, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdModalidad = @IdModalidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]    Script Date: 07/26/2013 17:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]
		@IdModalidad INT OUTPUT, 	@Nombre NVARCHAR(128),	@Sigla NVARCHAR(3),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ModalidadSeleccion(Nombre, Sigla, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Nombre, @Sigla, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdModalidad = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]    Script Date: 07/26/2013 17:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]
	@IdModalidad INT
AS
BEGIN
	DELETE Contrato.ModalidadSeleccion WHERE IdModalidad = @IdModalidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]    Script Date: 07/26/2013 17:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]
	@IdModalidad INT
AS
BEGIN
 SELECT IdModalidad, Nombre, Sigla, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ModalidadSeleccion] WHERE  IdModalidad = @IdModalidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]    Script Date: 07/26/2013 17:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]
@IdContratoLugarEjecucion	INT
AS
BEGIN
 SELECT IdContratoLugarEjecucion, IDDepartamento, IDMunicipio, 
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[LugarEjecucion] 
 WHERE 
 IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]    Script Date: 07/26/2013 17:06:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que consulta un(a) TablaParametrica
-- =============================================

create PROCEDURE [dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]
	@CodigoTablaParametrica NVARCHAR(128) = NULL,
	@NombreTablaParametrica NVARCHAR(128) = NULL,
	@Estado					BIT = NULL
AS
BEGIN
	SELECT
		IdTablaParametrica,
		CodigoTablaParametrica,
		NombreTablaParametrica,
		Url,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TablaParametrica]
	WHERE CodigoTablaParametrica = CASE WHEN @CodigoTablaParametrica IS NULL 
										THEN CodigoTablaParametrica 
										ELSE @CodigoTablaParametrica
								   END
	AND NombreTablaParametrica LIKE ''%'' + CASE WHEN @NombreTablaParametrica IS NULL 
											   THEN NombreTablaParametrica 
											   ELSE @NombreTablaParametrica
										  END + ''%''
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado
		END
	ORDER BY NombreTablaParametrica
END--FIN PP

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]    Script Date: 07/26/2013 17:06:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/20/2013 10:09:07 PM
-- Description:	Procedimiento almacenado que consulta un(a) SecuenciaNumeroProceso
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]
	@NumeroProceso NVARCHAR(25) output, @CodigoRegional INT ,@Sigla nvarchar(3), @AnoVigencia INT, @UsuarioCrea NVARCHAR(250) 
AS
BEGIN
	DECLARE @Consecutivo INT
	DECLARE @AnoVigenciaTransformado NVARCHAR(2)
	DECLARE @DelimitadorSecuencia NVARCHAR(1)

	SET @DelimitadorSecuencia = ''.''
	/*Validar esta creada un consecutivo por CodigoRegional y AnoVigencia*/
	if not exists(SELECT * FROM [Contrato].[SecuenciaNumeroProceso] WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia)
	BEGIN
			SET @Consecutivo = 1	
	END
	ELSE
	BEGIN
			SELECT @Consecutivo = MAX(Consecutivo) + 1 
			FROM [Contrato].[SecuenciaNumeroProceso] 
			WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia
	END
	
	/*
		Armar numero contrato
	*/
		
	SET @AnoVigenciaTransformado = CASE WHEN LEN(CONVERT(NVARCHAR(4),@AnoVigencia)) = 4 THEN SUBSTRING(CONVERT(NVARCHAR(4),@AnoVigencia),3,2) ELSE CONVERT(NVARCHAR(4),@AnoVigencia) END
	
	SELECT @NumeroProceso = right( ''00'' + cast( @CodigoRegional AS varchar(2)), 2 ) + @DelimitadorSecuencia + @Sigla + @DelimitadorSecuencia + @AnoVigenciaTransformado + @DelimitadorSecuencia + right( ''00000'' + cast( @Consecutivo AS varchar(5)), 5 )   

	print @NumeroProceso

	/*Registra el la secuencia*/
	 IF @NumeroProceso IS NOT NULL
	 BEGIN
		INSERT INTO Contrato.SecuenciaNumeroProceso(Consecutivo, CodigoRegional, AnoVigencia, UsuarioCrea, FechaCrea)
		VALUES(@Consecutivo, @CodigoRegional, @AnoVigencia, @UsuarioCrea, GETDATE())
	END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]    Script Date: 07/26/2013 17:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]
	@NumeroContrato NVARCHAR(11) output, @CodigoRegional INT ,@AnoVigencia INT, @UsuarioCrea NVARCHAR(250) 
AS
BEGIN
	DECLARE @Consecutivo INT
	/*Validar esta creada un consecutivo por CodigoRegional y AnoVigencia*/
	if not exists(SELECT * FROM [Contrato].[SecuenciaNumeroContrato] WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia)
	BEGIN
			SET @Consecutivo = 1	
	END
	ELSE
	BEGIN
			SELECT @Consecutivo = MAX(Consecutivo) + 1 
			FROM [Contrato].[SecuenciaNumeroContrato] 
			WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia
	END
	/*Registra el la secuencia*/
	INSERT INTO Contrato.SecuenciaNumeroContrato(Consecutivo, CodigoRegional, AnoVigencia, UsuarioCrea, FechaCrea)
					  VALUES(@Consecutivo, @CodigoRegional, @AnoVigencia, @UsuarioCrea, GETDATE())
	/*
		Armar numero contrato
	*/
	SELECT @NumeroContrato = right( ''00'' + cast( @CodigoRegional AS varchar(2)), 2 ) + right( ''00000'' + cast( @Consecutivo AS varchar(5)), 5 ) + cast( @AnoVigencia AS varchar(4))  

	print @NumeroContrato
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]    Script Date: 07/26/2013 17:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]
	@OrigenTipoSupervisor bit = NULL,
	@IDTipoSupvInterventor INT = NULL,
	@IDTerceroExterno INT = NULL,
	@TipoVinculacion bit = NULL,
	@FechaInicia DATETIME = NULL,
	@FechaFinaliza DATETIME = NULL,
	@Estado bit = NULL,
	@FechaModificaInterventor DATETIME = NULL
AS
BEGIN
 SELECT IDSupervisorInterv, IDContratoSupervisa, OrigenTipoSupervisor, IDTipoSupvInterventor, 
 IDTerceroExterno, IDFuncionarioInterno, IDContratoInterventoria, 
 TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, 
 FechaCrea, UsuarioCrea, FechaModifica, UsuarioModifica,IDTerceroInterventoria 
 FROM [Contrato].[SupervisorIntervContrato] 
 WHERE OrigenTipoSupervisor = CASE WHEN @OrigenTipoSupervisor IS NULL THEN OrigenTipoSupervisor ELSE @OrigenTipoSupervisor END AND IDTipoSupvInterventor = CASE WHEN @IDTipoSupvInterventor IS NULL THEN IDTipoSupvInterventor ELSE @IDTipoSupvInterventor END AND IDTerceroExterno = CASE WHEN @IDTerceroExterno IS NULL THEN IDTerceroExterno ELSE @IDTerceroExterno END AND TipoVinculacion = CASE WHEN @TipoVinculacion IS NULL THEN TipoVinculacion ELSE @TipoVinculacion END AND FechaInicia = CASE WHEN @FechaInicia IS NULL THEN FechaInicia ELSE @FechaInicia END AND FechaFinaliza = CASE WHEN @FechaFinaliza IS NULL THEN FechaFinaliza ELSE @FechaFinaliza END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END AND FechaModificaInterventor = CASE WHEN @FechaModificaInterventor IS NULL THEN FechaModificaInterventor ELSE @FechaModificaInterventor END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]    Script Date: 07/26/2013 17:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que actualiza un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]
@IDSupervisorInterv INT, 	
@IDContratoSupervisa INT,	
@OrigenTipoSupervisor bit,	
@IDTipoSupvInterventor INT,	
@IDTerceroExterno INT,	
@IDFuncionarioInterno INT,	
@IDContratoInterventoria INT,
@IDTerceroInterventoria int,	
@TipoVinculacion bit,	
@FechaInicia DATETIME,	
@FechaFinaliza DATETIME,	
@Estado bit,	
@FechaModificaInterventor DATETIME,	
@UsuarioModifica nvarchar(128)
AS
BEGIN
	UPDATE Contrato.SupervisorIntervContrato 
	SET IDContratoSupervisa = @IDContratoSupervisa, 
	OrigenTipoSupervisor = @OrigenTipoSupervisor, 
	IDTipoSupvInterventor = @IDTipoSupvInterventor, 
	IDTerceroExterno = @IDTerceroExterno, 
	IDFuncionarioInterno = @IDFuncionarioInterno, 
	IDContratoInterventoria = @IDContratoInterventoria,
	IDTerceroInterventoria = @IDTerceroInterventoria,
	TipoVinculacion = @TipoVinculacion, 
	FechaInicia = @FechaInicia, 
	FechaFinaliza = @FechaFinaliza, 
	Estado = @Estado, 
	FechaModificaInterventor = @FechaModificaInterventor, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IDSupervisorInterv = @IDSupervisorInterv
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]    Script Date: 07/26/2013 17:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]
		@IDSupervisorInterv INT OUTPUT, 	
		@IDContratoSupervisa INT,	
		@OrigenTipoSupervisor bit,	
		@IDTipoSupvInterventor INT,	
		@IDTerceroExterno INT,	
		@IDFuncionarioInterno INT,	
		@IDContratoInterventoria INT,
		@IDTerceroInterventoria	int,
		@TipoVinculacion bit,	
		@FechaInicia DATETIME,	
		@FechaFinaliza DATETIME,	
		@Estado bit,	
		@FechaModificaInterventor DATETIME,	
		@UsuarioCrea nvarchar(128),
		@UsuarioModifica nvarchar(128)
AS
BEGIN
	INSERT INTO Contrato.SupervisorIntervContrato(IDContratoSupervisa, OrigenTipoSupervisor, IDTipoSupvInterventor, IDTerceroExterno, IDFuncionarioInterno, IDContratoInterventoria, TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, UsuarioCrea, FechaCrea, IDTerceroInterventoria,UsuarioModifica, FechaModifica)
	VALUES(@IDContratoSupervisa, @OrigenTipoSupervisor, @IDTipoSupvInterventor, @IDTerceroExterno, @IDFuncionarioInterno, @IDContratoInterventoria, @TipoVinculacion, @FechaInicia, @FechaFinaliza, @Estado, @FechaModificaInterventor, @UsuarioCrea, GETDATE(), @IDTerceroInterventoria,@UsuarioModifica,GETDATE())
	SELECT @IDSupervisorInterv = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]    Script Date: 07/26/2013 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]
	@IDSupervisorInterv INT
AS
BEGIN
	DELETE Contrato.SupervisorIntervContrato 
	WHERE IDSupervisorInterv = @IDSupervisorInterv
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]    Script Date: 07/26/2013 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]
	@IDSupervisorInterv INT
AS
BEGIN
 SELECT IDSupervisorInterv, IDContratoSupervisa, OrigenTipoSupervisor, 
 IDTipoSupvInterventor, IDTerceroExterno, IDFuncionarioInterno, 
 IDContratoInterventoria, TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, 
 FechaCrea, UsuarioCrea, FechaModifica, UsuarioModifica, IDTerceroInterventoria
 FROM [Contrato].[SupervisorIntervContrato] 
 WHERE  IDSupervisorInterv = @IDSupervisorInterv
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]    Script Date: 07/26/2013 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacions_Consultar
Modificado por Jonathan Acosta
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]
	@NombreRegimenContratacion NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdRegimenContratacion, NombreRegimenContratacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[RegimenContratacion] WHERE NombreRegimenContratacion = CASE WHEN @NombreRegimenContratacion IS NULL THEN NombreRegimenContratacion ELSE @NombreRegimenContratacion END
 AND (Estado = @Estado OR @Estado IS NULL)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]    Script Date: 07/26/2013 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]
		@IdRegimenContratacion INT,	@NombreRegimenContratacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.RegimenContratacion SET NombreRegimenContratacion = @NombreRegimenContratacion, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRegimenContratacion = @IdRegimenContratacion
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]    Script Date: 07/26/2013 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]
		@IdRegimenContratacion INT OUTPUT, 	@NombreRegimenContratacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.RegimenContratacion(NombreRegimenContratacion, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreRegimenContratacion, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdRegimenContratacion = @@IDENTITY 		
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]    Script Date: 07/26/2013 17:06:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]
	@IdRegimenContratacion INT
AS
BEGIN
	DELETE Contrato.RegimenContratacion WHERE IdRegimenContratacion = @IdRegimenContratacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]    Script Date: 07/26/2013 17:06:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]
	@IdRegimenContratacion INT
AS
BEGIN
 SELECT IdRegimenContratacion, NombreRegimenContratacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[RegimenContratacion] WHERE  IdRegimenContratacion = @IdRegimenContratacion
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]    Script Date: 07/26/2013 17:06:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedidas_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]
	@IdNumeroContrato INT = NULL,
	@NumeroContrato NVARCHAR(50) = NULL,
	@FechaInicioEjecuciónContrato DATETIME = NULL,
	@FechaTerminacionInicialContrato DATETIME = NULL,
	@FechaFinalTerminacionContrato DATETIME = NULL
AS
BEGIN
 SELECT IdNumeroContrato,NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[UnidadMedida] 
 WHERE 
 IdNumeroContrato = CASE WHEN @IdNumeroContrato IS NULL THEN IdNumeroContrato 
 ELSE  @IdNumeroContrato END 
 AND FechaInicioEjecuciónContrato = CASE WHEN @FechaInicioEjecuciónContrato IS NULL THEN FechaInicioEjecuciónContrato ELSE @FechaInicioEjecuciónContrato END 
 AND FechaTerminacionInicialContrato = CASE WHEN @FechaTerminacionInicialContrato IS NULL THEN FechaTerminacionInicialContrato ELSE @FechaTerminacionInicialContrato END 
 AND FechaFinalTerminacionContrato = CASE  WHEN @FechaFinalTerminacionContrato IS NULL THEN FechaFinalTerminacionContrato ELSE @FechaFinalTerminacionContrato END
 AND NumeroContrato = CASE WHEN @NumeroContrato = '''' THEN NumeroContrato ELSE  @NumeroContrato END 
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]    Script Date: 07/26/2013 17:06:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]
		@IdNumeroContrato INT,	
		@NumeroContrato NVARCHAR(50),
		@FechaInicioEjecuciónContrato DATETIME,	@FechaTerminacionInicialContrato DATETIME,	@FechaFinalTerminacionContrato DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.UnidadMedida 
	SET NumeroContrato = @NumeroContrato,
	FechaInicioEjecuciónContrato = @FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato = @FechaTerminacionInicialContrato, FechaFinalTerminacionContrato = @FechaFinalTerminacionContrato, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNumeroContrato = @IdNumeroContrato
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]    Script Date: 07/26/2013 17:06:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]
		@IdNumeroContrato INT OUTPUT, 
		@NumeroContrato	NVARCHAR(50),
		@FechaInicioEjecuciónContrato DATETIME,	
		@FechaTerminacionInicialContrato DATETIME,	
		@FechaFinalTerminacionContrato DATETIME, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.UnidadMedida(NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroContrato, @FechaInicioEjecuciónContrato, @FechaTerminacionInicialContrato, @FechaFinalTerminacionContrato, @UsuarioCrea, GETDATE())
	SELECT @IdNumeroContrato = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]    Script Date: 07/26/2013 17:06:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]
	@IdNumeroContrato INT
AS
BEGIN
	DELETE Contrato.UnidadMedida WHERE IdNumeroContrato = @IdNumeroContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]    Script Date: 07/26/2013 17:06:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]
	@IdNumeroContrato INT
AS
BEGIN
 SELECT IdNumeroContrato,NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[UnidadMedida] WHERE  IdNumeroContrato = @IdNumeroContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]    Script Date: 07/26/2013 17:06:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]
	@Nombre NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdTipoSupvInterventor, Nombre, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoSupvInterventor] WHERE Nombre = CASE WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre END
 AND (Estado = @Estado OR @Estado IS NULL)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]    Script Date: 07/26/2013 17:06:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]
		@IdTipoSupvInterventor INT,	@Nombre NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoSupvInterventor SET Nombre = @Nombre, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoSupvInterventor = @IdTipoSupvInterventor
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]    Script Date: 07/26/2013 17:06:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]
		@IdTipoSupvInterventor INT OUTPUT, 	@Nombre NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoSupvInterventor(Nombre, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Nombre, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoSupvInterventor = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]    Script Date: 07/26/2013 17:06:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]
	@IdTipoSupvInterventor INT
AS
BEGIN
	DELETE Contrato.TipoSupvInterventor WHERE IdTipoSupvInterventor = @IdTipoSupvInterventor
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]    Script Date: 07/26/2013 17:06:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]
	@IdTipoSupvInterventor INT
AS
BEGIN
 SELECT IdTipoSupvInterventor, Nombre, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoSupvInterventor] WHERE  IdTipoSupvInterventor = @IdTipoSupvInterventor
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]    Script Date: 07/26/2013 17:06:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacions_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]
	@NombreTipoObligacion NVARCHAR(128) = NULL,
	@Estado bit = null
AS
BEGIN
 SELECT IdTipoObligacion, NombreTipoObligacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoObligacion] WHERE NombreTipoObligacion = CASE WHEN @NombreTipoObligacion IS NULL THEN NombreTipoObligacion ELSE @NombreTipoObligacion END
 AND (Estado = @Estado OR @Estado IS NULL)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Modificar]    Script Date: 07/26/2013 17:06:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacion_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Modificar]
		@IdTipoObligacion INT,	@NombreTipoObligacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoObligacion SET NombreTipoObligacion = @NombreTipoObligacion, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoObligacion = @IdTipoObligacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]    Script Date: 07/26/2013 17:06:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]
		@IdTipoObligacion INT OUTPUT, 	@NombreTipoObligacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoObligacion(NombreTipoObligacion, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoObligacion, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoObligacion = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]    Script Date: 07/26/2013 17:06:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]
	@IdTipoObligacion INT
AS
BEGIN
	DELETE Contrato.TipoObligacion WHERE IdTipoObligacion = @IdTipoObligacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]    Script Date: 07/26/2013 17:06:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]
	@IdTipoObligacion INT
AS
BEGIN
 SELECT IdTipoObligacion, NombreTipoObligacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoObligacion] WHERE  IdTipoObligacion = @IdTipoObligacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]    Script Date: 07/26/2013 17:06:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantias_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]
	@NombreTipoGarantia NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdTipoGarantia, NombreTipoGarantia, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoGarantia] WHERE NombreTipoGarantia = CASE WHEN @NombreTipoGarantia IS NULL THEN NombreTipoGarantia ELSE @NombreTipoGarantia END
 AND (Estado = @Estado OR @Estado IS NULL)
 ORDER BY NombreTipoGarantia
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Modificar]    Script Date: 07/26/2013 17:06:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantia_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Modificar]
		@IdTipoGarantia INT,	@NombreTipoGarantia NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoGarantia SET NombreTipoGarantia = @NombreTipoGarantia, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoGarantia = @IdTipoGarantia
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]    Script Date: 07/26/2013 17:06:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]
		@IdTipoGarantia INT OUTPUT, 	@NombreTipoGarantia NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoGarantia(NombreTipoGarantia, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoGarantia, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoGarantia = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]    Script Date: 07/26/2013 17:06:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]
	@IdTipoGarantia INT
AS
BEGIN
	DELETE Contrato.TipoGarantia WHERE IdTipoGarantia = @IdTipoGarantia
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]    Script Date: 07/26/2013 17:06:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]
	@IdTipoGarantia INT
AS
BEGIN
 SELECT IdTipoGarantia, NombreTipoGarantia, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoGarantia] WHERE  IdTipoGarantia = @IdTipoGarantia
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]    Script Date: 07/26/2013 17:06:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausulas_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]
	@NombreTipoClausula NVARCHAR(50) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdTipoClausula, NombreTipoClausula, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[TipoClausula] 
 WHERE NombreTipoClausula = CASE WHEN @NombreTipoClausula IS NULL THEN NombreTipoClausula ELSE @NombreTipoClausula END
 AND (Estado = @Estado OR @Estado IS NULL)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]    Script Date: 07/26/2013 17:06:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]
		@IdTipoClausula INT,	@NombreTipoClausula NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoClausula SET NombreTipoClausula = @NombreTipoClausula, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoClausula = @IdTipoClausula
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]    Script Date: 07/26/2013 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]
		@IdTipoClausula INT OUTPUT, 	@NombreTipoClausula NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoClausula(NombreTipoClausula, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoClausula, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoClausula = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]    Script Date: 07/26/2013 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]
	@IdTipoClausula INT
AS
BEGIN
	DELETE Contrato.TipoClausula WHERE IdTipoClausula = @IdTipoClausula
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]    Script Date: 07/26/2013 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]
	@IdTipoClausula INT
AS
BEGIN
 SELECT IdTipoClausula, NombreTipoClausula, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoClausula] WHERE  IdTipoClausula = @IdTipoClausula
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]    Script Date: 07/26/2013 17:05:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que actualiza un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]
		@IdContratoLugarEjecucion INT,	
		@IDContrato INT,	
		@Nacional BIT, 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE 
		Contrato.LugarEjecucionContrato 
	SET IDContrato = @IDContrato, 
		Nacional = @Nacional, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]    Script Date: 07/26/2013 17:05:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que guarda un nuevo LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]
		@IdContratoLugarEjecucion INT OUTPUT, 	
		@IDContrato INT,	
		@Nacional BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.LugarEjecucionContrato(IDContrato,  Nacional, UsuarioCrea, FechaCrea)
					  VALUES(@IDContrato,  @Nacional, @UsuarioCrea, GETDATE())
	SELECT @IdContratoLugarEjecucion = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]    Script Date: 07/26/2013 17:05:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que elimina un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
	DELETE Contrato.LugarEjecucionContrato WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]    Script Date: 07/26/2013 17:05:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
 SELECT 
	IdContratoLugarEjecucion, IDContrato, 
	Nacional, UsuarioCrea, FechaCrea, 
	UsuarioModifica, FechaModifica 
	FROM [Contrato].[LugarEjecucionContrato] 
	WHERE  IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]    Script Date: 07/26/2013 17:05:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que guarda un nuevo LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]
		@IdContratoLugarEjecucion INT, 	
		@IDDepartamento INT,	
		@IDMunicipio INT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.LugarEjecucion(IdContratoLugarEjecucion,IDDepartamento, IDMunicipio, UsuarioCrea, FechaCrea)
	VALUES(@IdContratoLugarEjecucion,@IDDepartamento, @IDMunicipio, @UsuarioCrea, GETDATE())
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]    Script Date: 07/26/2013 17:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que elimina un(a) LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
	DELETE Contrato.LugarEjecucion WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]    Script Date: 07/26/2013 17:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]
	@NumeroCDP INT = NULL,@FechaExpedicionCDP DATETIME = NULL
AS
BEGIN
 SELECT IdInformacionPresupuestal, NumeroCDP, ValorCDP, FechaExpedicionCDP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestal] WHERE NumeroCDP = CASE WHEN @NumeroCDP IS NULL THEN NumeroCDP ELSE @NumeroCDP END AND FechaExpedicionCDP = CASE WHEN @FechaExpedicionCDP IS NULL THEN FechaExpedicionCDP ELSE @FechaExpedicionCDP END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]    Script Date: 07/26/2013 17:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]
	@FechaSolicitudRP DATETIME = NULL,@NumeroRP INT = NULL
AS
BEGIN
 SELECT IdInformacionPresupuestalRP, FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestalRP] WHERE FechaSolicitudRP = CASE WHEN @FechaSolicitudRP IS NULL THEN FechaSolicitudRP ELSE @FechaSolicitudRP END AND NumeroRP = CASE WHEN @NumeroRP IS NULL THEN NumeroRP ELSE @NumeroRP END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]    Script Date: 07/26/2013 17:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]
		@IdInformacionPresupuestalRP INT,	@FechaSolicitudRP DATETIME,	@NumeroRP INT,	@ValorRP INT,	@FechaExpedicionRP DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.InformacionPresupuestalRP SET FechaSolicitudRP = @FechaSolicitudRP, NumeroRP = @NumeroRP, ValorRP = @ValorRP, FechaExpedicionRP = @FechaExpedicionRP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]    Script Date: 07/26/2013 17:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]
		@IdInformacionPresupuestalRP INT OUTPUT, 	@FechaSolicitudRP DATETIME,	@NumeroRP INT,	@ValorRP INT,	@FechaExpedicionRP DATETIME, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.InformacionPresupuestalRP(FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea)
					  VALUES(@FechaSolicitudRP, @NumeroRP, @ValorRP, @FechaExpedicionRP, @UsuarioCrea, GETDATE())
	SELECT @IdInformacionPresupuestalRP = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]    Script Date: 07/26/2013 17:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que elimina un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]
	@IdInformacionPresupuestalRP INT
AS
BEGIN
	DELETE Contrato.InformacionPresupuestalRP WHERE IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]    Script Date: 07/26/2013 17:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]
	@IdInformacionPresupuestalRP INT
AS
BEGIN
 SELECT IdInformacionPresupuestalRP, FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestalRP] WHERE  IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]    Script Date: 07/26/2013 17:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]
		@IdInformacionPresupuestal INT,	@NumeroCDP INT,	@ValorCDP NUMERIC(18,3),	@FechaExpedicionCDP DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.InformacionPresupuestal SET NumeroCDP = @NumeroCDP, ValorCDP = @ValorCDP, FechaExpedicionCDP = @FechaExpedicionCDP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdInformacionPresupuestal = @IdInformacionPresupuestal
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]    Script Date: 07/26/2013 17:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]
		@IdInformacionPresupuestal INT OUTPUT, 	@NumeroCDP INT,	@ValorCDP NUMERIC(18,3),	@FechaExpedicionCDP DATETIME, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.InformacionPresupuestal(NumeroCDP, ValorCDP, FechaExpedicionCDP, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroCDP, @ValorCDP, @FechaExpedicionCDP, @UsuarioCrea, GETDATE())
	SELECT @IdInformacionPresupuestal = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]    Script Date: 07/26/2013 17:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que elimina un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]
	@IdInformacionPresupuestal INT
AS
BEGIN
	DELETE Contrato.InformacionPresupuestal WHERE IdInformacionPresupuestal = @IdInformacionPresupuestal
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Consultar]    Script Date: 07/26/2013 17:05:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Consultar]
	@IdInformacionPresupuestal INT
AS
BEGIN
 SELECT IdInformacionPresupuestal, NumeroCDP, ValorCDP, FechaExpedicionCDP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestal] WHERE  IdInformacionPresupuestal = @IdInformacionPresupuestal
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]    Script Date: 07/26/2013 17:05:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacions_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]
	@IdGestionObligacion INT = NULL,@IdGestionClausula INT = NULL,@NombreObligacion NVARCHAR(10) = NULL,@IdTipoObligacion INT = NULL,@Orden NVARCHAR(128) = NULL,@DescripcionObligacion NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdGestionObligacion, IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarObligacion] WHERE IdGestionObligacion = CASE WHEN @IdGestionObligacion IS NULL THEN IdGestionObligacion ELSE @IdGestionObligacion END AND IdGestionClausula = CASE WHEN @IdGestionClausula IS NULL THEN IdGestionClausula ELSE @IdGestionClausula END AND NombreObligacion = CASE WHEN @NombreObligacion IS NULL THEN NombreObligacion ELSE @NombreObligacion END AND IdTipoObligacion = CASE WHEN @IdTipoObligacion IS NULL THEN IdTipoObligacion ELSE @IdTipoObligacion END AND Orden = CASE WHEN @Orden IS NULL THEN Orden ELSE @Orden END AND DescripcionObligacion = CASE WHEN @DescripcionObligacion IS NULL THEN DescripcionObligacion ELSE @DescripcionObligacion END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]    Script Date: 07/26/2013 17:05:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]
		@IdGestionObligacion INT,	@IdGestionClausula INT,	@NombreObligacion NVARCHAR(10),	@IdTipoObligacion INT,	@Orden NVARCHAR(128),	@DescripcionObligacion NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.GestionarObligacion SET IdGestionClausula = @IdGestionClausula, NombreObligacion = @NombreObligacion, IdTipoObligacion = @IdTipoObligacion, Orden = @Orden, DescripcionObligacion = @DescripcionObligacion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdGestionObligacion = @IdGestionObligacion
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]    Script Date: 07/26/2013 17:05:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]
		@IdGestionObligacion INT OUTPUT, 	@IdGestionClausula INT,	@NombreObligacion NVARCHAR(10),	@IdTipoObligacion INT,	@Orden NVARCHAR(128),	@DescripcionObligacion NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.GestionarObligacion(IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea)
					  VALUES(@IdGestionClausula, @NombreObligacion, @IdTipoObligacion, @Orden, @DescripcionObligacion, @UsuarioCrea, GETDATE())
	SELECT @IdGestionObligacion = @@IDENTITY 		
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]    Script Date: 07/26/2013 17:05:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]
	@IdGestionObligacion INT
AS
BEGIN
	DELETE Contrato.GestionarObligacion WHERE IdGestionObligacion = @IdGestionObligacion
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]    Script Date: 07/26/2013 17:05:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]
	@IdGestionObligacion INT
AS
BEGIN
 SELECT IdGestionObligacion, IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarObligacion] WHERE  IdGestionObligacion = @IdGestionObligacion
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]    Script Date: 07/26/2013 17:05:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado consula las clausuas de los contratos
   -- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar]
	@IdGestionClausula INT = NULL,@IdContrato NVARCHAR(3) = NULL,@NombreClausula NVARCHAR(10) = NULL,
	@TipoClausula INT = NULL,@Orden NVARCHAR(128) = NULL,@DescripcionClausula NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdGestionClausula, IdContrato, NombreClausula, TipoClausula, Orden, DescripcionClausula, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[GestionarClausulasContrato] 
 WHERE 
 IdGestionClausula = 
 CASE WHEN @IdGestionClausula IS NULL THEN IdGestionClausula 
 ELSE @IdGestionClausula END AND 
 IdContrato = 
 CASE WHEN @IdContrato IS NULL THEN IdContrato 
 ELSE @IdContrato END AND 
 NombreClausula = 
 CASE WHEN @NombreClausula IS NULL THEN NombreClausula 
 ELSE @NombreClausula END AND 
 TipoClausula = 
 CASE WHEN @TipoClausula IS NULL THEN TipoClausula 
 ELSE @TipoClausula END AND 
 Orden = 
 CASE WHEN @Orden IS NULL THEN Orden 
 ELSE @Orden END AND 
 DescripcionClausula = 
 CASE WHEN @DescripcionClausula IS NULL THEN DescripcionClausula 
 ELSE @DescripcionClausula END
 ORDER BY 
 IdContrato,
 [Contrato].[GestionarClausulasContrato].OrdenNumero
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]    Script Date: 07/26/2013 17:05:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]
		@IdGestionClausula INT,	@IdContrato NVARCHAR(3),	@NombreClausula NVARCHAR(10),	@TipoClausula INT,	@Orden NVARCHAR(128), @OrdenNumero INT,	@DescripcionClausula NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.GestionarClausulasContrato SET IdContrato = @IdContrato, NombreClausula = @NombreClausula, TipoClausula = @TipoClausula, Orden = @Orden, OrdenNumero = @OrdenNumero, DescripcionClausula = @DescripcionClausula, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdGestionClausula = @IdGestionClausula
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]    Script Date: 07/26/2013 17:05:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]
		@IdGestionClausula INT OUTPUT, 	@IdContrato NVARCHAR(3),	@NombreClausula NVARCHAR(10),	@TipoClausula INT,	@Orden NVARCHAR(128), @OrdenNumero INT, @DescripcionClausula NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.GestionarClausulasContrato(IdContrato, NombreClausula, TipoClausula, Orden, OrdenNumero, DescripcionClausula, UsuarioCrea, FechaCrea)
				VALUES(@IdContrato, @NombreClausula, @TipoClausula, @Orden,  @OrdenNumero, @DescripcionClausula, @UsuarioCrea, GETDATE())
	SELECT @IdGestionClausula = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]    Script Date: 07/26/2013 17:05:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]
	@IdGestionClausula INT
AS
BEGIN
	DELETE Contrato.GestionarClausulasContrato WHERE IdGestionClausula = @IdGestionClausula
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]    Script Date: 07/26/2013 17:05:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]
	@IdGestionClausula INT
AS
BEGIN
 SELECT IdGestionClausula, IdContrato, NombreClausula, TipoClausula, Orden, OrdenNumero, DescripcionClausula, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarClausulasContrato] WHERE  IdGestionClausula = @IdGestionClausula
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantias_Consultar]    Script Date: 07/26/2013 17:05:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantias_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Ares\Jonathan Acosta
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantias_Consultar]
	@IDTipoGarantia INT = NULL,
	@NumGarantia NVARCHAR(128) = NULL,
	@NumContrato NUMERIC(25,0) = NULL,
	@Estado		bit = null
AS
BEGIN
 SELECT IDGarantia, garantia.IDContrato, IDTipoGarantia, NumGarantia, 
 FechaExpedicion, FechaVencInicial, FechaRecibo, FechaDevolucion, 
 MotivoDevolucion, FechaAprobacion, FechaCertificacionPago, Valor, 
 Estado, Anexo, ObservacionAnexo, garantia.UsuarioCrea, garantia.FechaCrea, garantia.UsuarioModifica, 
 garantia.FechaModifica 
 FROM [Contrato].[Garantia] as garantia inner join Contrato.Contrato as contrato 
 on (contrato.IdContrato = garantia.IDContrato)
 WHERE IDTipoGarantia = CASE WHEN @IDTipoGarantia IS NULL THEN IDTipoGarantia ELSE @IDTipoGarantia END 
 AND NumGarantia = CASE WHEN @NumGarantia IS NULL THEN NumGarantia ELSE @NumGarantia END
 AND contrato.NumeroContrato = CASE WHEN @NumContrato IS NULL THEN NumeroContrato ELSE @NumContrato END
 AND garantia.Estado = CASE WHEN @Estado IS NULL THEN garantia.Estado ELSE @Estado END
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]    Script Date: 07/26/2013 17:05:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Ares\Andrés Morales
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Modificar]
@IDGarantia INT, 	
@IDContrato INT,	
@IDTipoGarantia INT,	
@NumGarantia NVARCHAR(128),	
@FechaExpedicion DATETIME,	
@FechaVencInicial DATETIME,	
@FechaRecibo DATETIME,	
@FechaDevolucion DATETIME,	
@MotivoDevolucion NVARCHAR(128),	
@FechaAprobacion DATETIME,	
@FechaCertificacionPago DATETIME,	
@Valor NUMERIC(18,0),
@Estado BIT,	
@Anexo BIT,	
@ObservacionAnexo NVARCHAR(256), 
@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.Garantia 
	SET IDContrato = @IDContrato, 
	IDTipoGarantia = @IDTipoGarantia, 
	NumGarantia = @NumGarantia, 
	FechaExpedicion = @FechaExpedicion, 
	FechaVencInicial = @FechaVencInicial, 
	FechaRecibo = @FechaRecibo, 
	FechaDevolucion = @FechaDevolucion, 
	MotivoDevolucion = @MotivoDevolucion, 
	FechaAprobacion = @FechaAprobacion, 
	FechaCertificacionPago = @FechaCertificacionPago, 
	Valor = @Valor, 
	Estado = @Estado, 
	Anexo = @Anexo, 
	ObservacionAnexo = @ObservacionAnexo, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IDGarantia = @IDGarantia
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Insertar]    Script Date: 07/26/2013 17:05:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Insertar]
@IDGarantia INT OUTPUT, 	
@IDContrato INT,	
@IDTipoGarantia INT,	
@NumGarantia NVARCHAR(128),	
@FechaExpedicion DATETIME,	
@FechaVencInicial DATETIME,	
@FechaRecibo DATETIME,	
@FechaDevolucion DATETIME,	
@MotivoDevolucion NVARCHAR(128),	
@FechaAprobacion DATETIME,	
@FechaCertificacionPago DATETIME,	
@Valor NUMERIC(18,0),
@Estado BIT,	
@Anexo BIT,	
@ObservacionAnexo NVARCHAR(256), 
@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.Garantia(IDContrato, IDTipoGarantia, NumGarantia, FechaExpedicion, FechaVencInicial, FechaRecibo, FechaDevolucion, MotivoDevolucion, FechaAprobacion, FechaCertificacionPago, Valor, Estado, Anexo, ObservacionAnexo, UsuarioCrea, FechaCrea)
					  VALUES(@IDContrato, @IDTipoGarantia, @NumGarantia, @FechaExpedicion, @FechaVencInicial, @FechaRecibo, @FechaDevolucion, @MotivoDevolucion, @FechaAprobacion, @FechaCertificacionPago, @Valor, @Estado, @Anexo, @ObservacionAnexo, @UsuarioCrea, GETDATE())
	SELECT @IDGarantia = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]    Script Date: 07/26/2013 17:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Ares\Henry
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que elimina un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]
	@IDGarantia INT
AS
BEGIN
	DELETE Contrato.Garantia 
	WHERE IDGarantia = @IDGarantia
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]    Script Date: 07/26/2013 17:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Garantia_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Consultar]
	@IDGarantia INT
AS
BEGIN
 SELECT IDGarantia, IDContrato, IDTipoGarantia, NumGarantia, 
 FechaExpedicion, FechaVencInicial, FechaRecibo, FechaDevolucion, 
 MotivoDevolucion, FechaAprobacion, FechaCertificacionPago, Valor, 
 Estado, Anexo, ObservacionAnexo, UsuarioCrea, FechaCrea, UsuarioModifica, 
 FechaModifica 
 FROM [Contrato].[Garantia] 
 WHERE  IDGarantia = @IDGarantia
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]    Script Date: 07/26/2013 17:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPagos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]
	@NombreFormaPago NVARCHAR(50) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdFormapago, NombreFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[FormaPago] WHERE NombreFormaPago = CASE WHEN @NombreFormaPago IS NULL THEN NombreFormaPago ELSE @NombreFormaPago END
 AND (Estado = @Estado OR @Estado IS NULL)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]    Script Date: 07/26/2013 17:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]
		@IdFormapago INT,	@NombreFormaPago NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.FormaPago SET NombreFormaPago = @NombreFormaPago, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdFormapago = @IdFormapago
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]    Script Date: 07/26/2013 17:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]
		@IdFormapago INT OUTPUT, 	@NombreFormaPago NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.FormaPago(NombreFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreFormaPago, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdFormapago = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]    Script Date: 07/26/2013 17:05:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]
	@IdFormapago INT
AS
BEGIN
	DELETE Contrato.FormaPago WHERE IdFormapago = @IdFormapago
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]    Script Date: 07/26/2013 17:05:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]
	@IdFormapago INT
AS
BEGIN
 SELECT IdFormapago, NombreFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[FormaPago] WHERE  IdFormapago = @IdFormapago
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]    Script Date: 07/26/2013 17:05:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/20/2013 10:09:07 PM
-- Description:	Procedimiento almacenado que consulta un(a) Estado de un Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]
	@EstadoContrato NVARCHAR(128) output, @NumeroContrato NUMERIC(25) 
AS
BEGIN
	DECLARE @EstadoActual NVARCHAR(128)
	SET @EstadoActual = ''DESCONOCIDO''

	-- SUSCRITO
		SELECT @EstadoActual = ''SUSCRITO''
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND FechaSuscripcion IS NOT NULL

	-- EJECUCION
		SELECT @EstadoActual = ''EJECUCION''
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() BETWEEN FechaInicioEjecucion AND FechaFinalTerminacion

		-- SUSPENDIDO
		/*SELECT @EstadoActual = ''SUSPENDIDO''
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato*/

	-- TERMINADO
		SELECT @EstadoActual = ''TERMINADO''
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() > FechaFinalTerminacion 

	-- LIQUIDADO
		SELECT @EstadoActual = ''LIQUIDADO''
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() >= FechaProyectadaLiquidacion 

		-- ANULADO
		/*SELECT @EstadoActual = ''ANULADO''
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND FechaSuscripcion IS NOT NULL	*/

	-- PERDIDA COMPETENCIA
		SELECT @EstadoActual = ''PERDIDA''
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() > FechaFinalTerminacion AND DATEDIFF(m,FechaFinalTerminacion,GETDATE()) >= 30
	

	SELECT @EstadoContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]    Script Date: 07/26/2013 17:05:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contratos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que consulta un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar]
@FechaRegistro DATETIME = NULL,
@NumeroProceso NVARCHAR (50) = NULL,
@NumeroContrato nvarchar (50) = NULL,
@IdModalidad INT = NULL,
@IdCategoriaContrato INT = NULL,
@IdTipoContrato INT = NULL
AS
BEGIN

SELECT
	CONVERT(date, FechaRegistro),
	IdContrato,
	FechaRegistro,
	NumeroProceso,
	NumeroContrato,
	FechaAdjudicacion,
	IdModalidad,
	IdCategoriaContrato,
	IdTipoContrato,
	IdCodigoModalidad,
	IdModalidadAcademica,
	IdCodigoProfesion,
	IdNombreProfesion,
	IdRegionalContrato,
	RequiereActa,
	ManejaAportes,
	ManejaRecursos,
	ManejaVigenciasFuturas,
	IdRegimenContratacion,
	CodigoRegional,
	NombreSolicitante,
	DependenciaSolicitante,
	CargoSolicitante,
	ObjetoContrato,
	AlcanceObjetoContrato,
	ValorInicialContrato,
	ValorTotalAdiciones,
	ValorFinalContrato,
	ValorAportesICBF,
	ValorAportesOperador,
	ValorTotalReduccion,
	JustificacionAdicionSuperior50porc,
	FechaSuscripcion,
	FechaInicioEjecucion,
	FechaFinalizacionInicial,
	PlazoInicial,
	FechaInicialTerminacion,
	FechaFinalTerminacion,
	FechaProyectadaLiquidacion,
	FechaAnulacion,
	Prorrogas,
	PlazoTotal,
	FechaFirmaActaInicio,
	VigenciaFiscalInicial,
	VigenciaFiscalFinal,
	IdUnidadEjecucion,
	IdLugarEjecucion,
	DatosAdicionales,
	IdEstadoContrato,
	IdTipoDocumentoContratista,
	IdentificacionContratista,
	NombreContratista,
	IdFormaPago,
	IdTipoEntidad,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[Contrato]
WHERE (NumeroProceso = @NumeroProceso OR @NumeroProceso IS NULL)
AND (NumeroContrato = @NumeroContrato OR @NumeroContrato IS NULL)
AND (IdModalidad = @IdModalidad OR @IdModalidad IS NULL)
AND (IdCategoriaContrato = @IdCategoriaContrato OR @IdCategoriaContrato IS NULL)
AND (IdTipoContrato = @IdTipoContrato OR @IdTipoContrato IS NULL)
AND FechaRegistro =
--AND (CONVERT(date, FechaRegistro) = CONVERT(date, @FechaRegistro))
	CASE
		WHEN @FechaRegistro IS NULL THEN FechaRegistro ELSE @FechaRegistro
	END
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]    Script Date: 07/26/2013 17:05:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Modificar]
@IdContrato		int,
@FechaRegistro	datetime,
@NumeroProceso	nvarchar(50),
@NumeroContrato	nvarchar(50),
@FechaAdjudicacion	datetime,
@IdModalidad	int,
@IdCategoriaContrato	int,
@IdTipoContrato	int,
@IdCodigoModalidad	int,
@IdModalidadAcademica	int,
@IdCodigoProfesion	int,
@IdNombreProfesion	int,
@IdRegionalContrato	int,
@RequiereActa	bit,
@ManejaAportes	bit,
@ManejaRecursos	bit,
@ManejaVigenciasFuturas	bit,
@IdRegimenContratacion	int,
@CodigoRegional	int,
@NombreSolicitante	nvarchar(50),
@DependenciaSolicitante	nvarchar(50),
@CargoSolicitante	nvarchar(150),
@ObjetoContrato	nvarchar(300),
@AlcanceObjetoContrato	nvarchar(128),
@ValorInicialContrato	numeric(18, 3),
@ValorTotalAdiciones	numeric(18, 3),
@ValorFinalContrato	numeric(18, 3),
@ValorAportesICBF	numeric(18, 3),
@ValorAportesOperador	numeric(18, 3),
@ValorTotalReduccion	numeric(18, 3),
@JustificacionAdicionSuperior50porc	nvarchar(300),
@FechaSuscripcion	datetime,
@FechaInicioEjecucion	datetime,
@FechaFinalizacionInicial	datetime,
@PlazoInicial	int,
@FechaInicialTerminacion	datetime,
@FechaFinalTerminacion	datetime,
@FechaProyectadaLiquidacion	datetime,
@FechaAnulacion	datetime,
@Prorrogas	int,
@PlazoTotal	int,
@FechaFirmaActaInicio	datetime,
@VigenciaFiscalInicial	int,
@VigenciaFiscalFinal	int,
@IdUnidadEjecucion	int,
@IdLugarEjecucion	int,
@DatosAdicionales	nvarchar(128),
@IdEstadoContrato	int,
@IdTipoDocumentoContratista	nvarchar(3),
@IdentificacionContratista	nvarchar(50),
@NombreContratista	nvarchar(128),
@IdFormaPago	int,
@IdTipoEntidad	int,
@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.Contrato 
	SET FechaRegistro = @FechaRegistro,
	NumeroProceso = @NumeroProceso,
	NumeroContrato = @NumeroContrato,
	FechaAdjudicacion = @FechaAdjudicacion,
	IdModalidad = @IdModalidad,
	IdCategoriaContrato = @IdCategoriaContrato,
	IdTipoContrato = @IdTipoContrato,
	IdCodigoModalidad = @IdCodigoModalidad,
	IdModalidadAcademica = @IdModalidadAcademica,
	IdCodigoProfesion = @IdCodigoProfesion,
	IdNombreProfesion = @IdNombreProfesion,
	IdRegionalContrato = @IdRegionalContrato,
	RequiereActa = @RequiereActa,
	ManejaAportes = @ManejaAportes,
	ManejaRecursos = @ManejaRecursos,
	ManejaVigenciasFuturas = @ManejaVigenciasFuturas,
	IdRegimenContratacion = @IdRegimenContratacion,
	CodigoRegional = @CodigoRegional,
	NombreSolicitante = @NombreSolicitante,
	DependenciaSolicitante = @DependenciaSolicitante,
	CargoSolicitante = @CargoSolicitante,
	ObjetoContrato = @ObjetoContrato,
	AlcanceObjetoContrato = @AlcanceObjetoContrato,
	ValorInicialContrato = @ValorInicialContrato,
	ValorTotalAdiciones = @ValorTotalAdiciones,
	ValorFinalContrato = @ValorFinalContrato,
	ValorAportesICBF = @ValorAportesICBF,
	ValorAportesOperador = @ValorAportesOperador,
	ValorTotalReduccion = @ValorTotalReduccion,
	JustificacionAdicionSuperior50porc = @JustificacionAdicionSuperior50porc,
	FechaSuscripcion = @FechaSuscripcion,
	FechaInicioEjecucion = @FechaInicioEjecucion,
	FechaFinalizacionInicial = @FechaFinalizacionInicial,
	PlazoInicial = @PlazoInicial,
	FechaInicialTerminacion = @FechaInicialTerminacion,
	FechaFinalTerminacion = @FechaFinalTerminacion,
	FechaProyectadaLiquidacion =@FechaProyectadaLiquidacion,
	FechaAnulacion = @FechaAnulacion,
	Prorrogas = @Prorrogas,
	PlazoTotal = @PlazoTotal,
	FechaFirmaActaInicio = @FechaFirmaActaInicio,
	VigenciaFiscalInicial = @VigenciaFiscalInicial,
	VigenciaFiscalFinal = @VigenciaFiscalFinal,
	IdUnidadEjecucion = @IdUnidadEjecucion,
	IdLugarEjecucion = @IdLugarEjecucion,
	DatosAdicionales = @DatosAdicionales,
	IdEstadoContrato = @IdEstadoContrato,
	IdTipoDocumentoContratista = @IdTipoDocumentoContratista,
	IdentificacionContratista = @IdentificacionContratista,
	NombreContratista = @NombreContratista,
	IdFormaPago = @IdFormaPago,
	IdTipoEntidad = @IdTipoEntidad,
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdContrato = @IdContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Insertar]    Script Date: 07/26/2013 17:05:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Insertar]
@IdContrato	int	output,
@FechaRegistro	datetime,
@NumeroProceso	nvarchar(50),
@NumeroContrato	nvarchar(50),
@FechaAdjudicacion	datetime,
@IdModalidad	int,
@IdCategoriaContrato	int,
@IdTipoContrato	int,
@IdCodigoModalidad	int,
@IdModalidadAcademica	int,
@IdCodigoProfesion	int,
@IdNombreProfesion	int,
@IdRegionalContrato	int,
@RequiereActa	bit,
@ManejaAportes	bit,
@ManejaRecursos	bit,
@ManejaVigenciasFuturas	bit,
@IdRegimenContratacion	int,
@CodigoRegional	int,
@NombreSolicitante	nvarchar(50),
@DependenciaSolicitante	nvarchar(50),
@CargoSolicitante	nvarchar(150),
@ObjetoContrato	nvarchar(300),
@AlcanceObjetoContrato	nvarchar(128),
@ValorInicialContrato	numeric(18, 3),
@ValorTotalAdiciones	numeric(18, 3),
@ValorFinalContrato	numeric(18, 3),
@ValorAportesICBF	numeric(18, 3),
@ValorAportesOperador	numeric(18, 3),
@ValorTotalReduccion	numeric(18, 3),
@JustificacionAdicionSuperior50porc	nvarchar(300),
@FechaSuscripcion	datetime,
@FechaInicioEjecucion	datetime,
@FechaFinalizacionInicial	datetime,
@PlazoInicial	int,
@FechaInicialTerminacion	datetime,
@FechaFinalTerminacion	datetime,
@FechaProyectadaLiquidacion	datetime,
@FechaAnulacion	datetime,
@Prorrogas	int,
@PlazoTotal	int,
@FechaFirmaActaInicio	datetime,
@VigenciaFiscalInicial	int,
@VigenciaFiscalFinal	int,
@IdUnidadEjecucion	int,
@IdLugarEjecucion	int,
@DatosAdicionales	nvarchar(128),
@IdEstadoContrato	int,
@IdTipoDocumentoContratista	nvarchar(3),
@IdentificacionContratista	nvarchar(50),
@NombreContratista	nvarchar(128),
@IdFormaPago	int,
@IdTipoEntidad	int,
@UsuarioCrea	nvarchar(250)
	
		
		
AS
BEGIN
	INSERT INTO Contrato.Contrato(
	FechaRegistro,
	NumeroProceso,
	NumeroContrato,
	FechaAdjudicacion,
	IdModalidad,
	IdCategoriaContrato,
	IdTipoContrato,
	IdCodigoModalidad,
	IdModalidadAcademica,
	IdCodigoProfesion,
	IdNombreProfesion,
	IdRegionalContrato,
	RequiereActa,
	ManejaAportes,
	ManejaRecursos,
	ManejaVigenciasFuturas,
	IdRegimenContratacion,
	CodigoRegional,
	NombreSolicitante,
	DependenciaSolicitante,
	CargoSolicitante,
	ObjetoContrato,
	AlcanceObjetoContrato,
	ValorInicialContrato,
	ValorTotalAdiciones,
	ValorFinalContrato,
	ValorAportesICBF,
	ValorAportesOperador,
	ValorTotalReduccion,
	JustificacionAdicionSuperior50porc,
	FechaSuscripcion,
	FechaInicioEjecucion,
	FechaFinalizacionInicial,
	PlazoInicial,
	FechaInicialTerminacion,
	FechaFinalTerminacion,
	FechaProyectadaLiquidacion,
	FechaAnulacion,
	Prorrogas,
	PlazoTotal,
	FechaFirmaActaInicio,
	VigenciaFiscalInicial,
	VigenciaFiscalFinal,
	IdUnidadEjecucion,
	IdLugarEjecucion,
	DatosAdicionales,
	IdEstadoContrato,
	IdTipoDocumentoContratista,
	IdentificacionContratista,
	NombreContratista,
	IdFormaPago,
	IdTipoEntidad,
	UsuarioCrea,
	FechaCrea)
VALUES(
	@FechaRegistro,
	@NumeroProceso,
	@NumeroContrato,
	@FechaAdjudicacion,
	@IdModalidad,
	@IdCategoriaContrato,
	@IdTipoContrato,
	@IdCodigoModalidad,
	@IdModalidadAcademica,
	@IdCodigoProfesion,
	@IdNombreProfesion,
	@IdRegionalContrato,
	@RequiereActa,
	@ManejaAportes,
	@ManejaRecursos,
	@ManejaVigenciasFuturas,
	@IdRegimenContratacion,
	@CodigoRegional,
	@NombreSolicitante,
	@DependenciaSolicitante,
	@CargoSolicitante,
	@ObjetoContrato,
	@AlcanceObjetoContrato,
	@ValorInicialContrato,
	@ValorTotalAdiciones,
	@ValorFinalContrato,
	@ValorAportesICBF,
	@ValorAportesOperador,
	@ValorTotalReduccion,
	@JustificacionAdicionSuperior50porc,
	@FechaSuscripcion,
	@FechaInicioEjecucion,
	@FechaFinalizacionInicial,
	@PlazoInicial,
	@FechaInicialTerminacion,
	@FechaFinalTerminacion,
	@FechaProyectadaLiquidacion,
	@FechaAnulacion,
	@Prorrogas,
	@PlazoTotal,
	@FechaFirmaActaInicio,
	@VigenciaFiscalInicial,
	@VigenciaFiscalFinal,
	@IdUnidadEjecucion,
	@IdLugarEjecucion,
	@DatosAdicionales,
	@IdEstadoContrato,
	@IdTipoDocumentoContratista,
	@IdentificacionContratista,
	@NombreContratista,
	@IdFormaPago,
	@IdTipoEntidad,
	@UsuarioCrea,
	GETDATE())
	SELECT @IdContrato = @@IDENTITY 		
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]    Script Date: 07/26/2013 17:05:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que elimina un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]
	@IdContrato INT
AS
BEGIN
	DELETE Contrato.Contrato WHERE IdContrato = @IdContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]    Script Date: 07/26/2013 17:05:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que consulta un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Consultar]
	@IdContrato INT
AS
BEGIN
 SELECT IdContrato,
FechaRegistro,
NumeroProceso,
NumeroContrato,
FechaAdjudicacion,
IdModalidad,
IdCategoriaContrato,
IdTipoContrato,
IdCodigoModalidad,
IdModalidadAcademica,
IdCodigoProfesion,
IdNombreProfesion,
IdRegionalContrato,
RequiereActa,
ManejaAportes,
ManejaRecursos,
ManejaVigenciasFuturas,
IdRegimenContratacion,
CodigoRegional,
NombreSolicitante,
DependenciaSolicitante,
CargoSolicitante,
ObjetoContrato,
AlcanceObjetoContrato,
ValorInicialContrato,
ValorTotalAdiciones,
ValorFinalContrato,
ValorAportesICBF,
ValorAportesOperador,
ValorTotalReduccion,
JustificacionAdicionSuperior50porc,
FechaSuscripcion,
FechaInicioEjecucion,
FechaFinalizacionInicial,
PlazoInicial,
FechaInicialTerminacion,
FechaFinalTerminacion,
FechaProyectadaLiquidacion,
FechaAnulacion,
Prorrogas,
PlazoTotal,
FechaFirmaActaInicio,
VigenciaFiscalInicial,
VigenciaFiscalFinal,
IdUnidadEjecucion,
IdLugarEjecucion,
DatosAdicionales,
IdEstadoContrato,
IdTipoDocumentoContratista,
IdentificacionContratista,
NombreContratista,
IdFormaPago,
IdTipoEntidad,
UsuarioCrea,
FechaCrea,
UsuarioModifica,
FechaModifica
 FROM [Contrato].[Contrato] 
 WHERE  IdContrato = @IdContrato
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar]    Script Date: 07/26/2013 17:05:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que consulta un(a) ConsultarSupervisorInterventor
-- Ajste en el formato de las fechas
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar]
	@TipoSupervisorInterventor INT = NULL,
	@NumeroContrato NVARCHAR(50) = NULL,
	@NumeroIdentificacion varchar(128) = NULL,
	@NombreRazonSocialSupervisorInterventor NVARCHAR(128) = NULL,
	@NumeroIdentificacionDirectorInterventoria varchar(128) = NULL,
	@NombreRazonSocialDirectorInterventoria NVARCHAR(128) = NULL
AS
BEGIN
 SELECT 
  SUPCON.IDSUPERVISORINTERV,
 TIPCON.NOMBRE AS TIPOSUPERVISORINTERVENTOR, 
 CONVERT(VARCHAR(10),CON.NUMEROCONTRATO,103) AS NUMEROCONTRATO, 
 CONVERT(VARCHAR(10),CON.FECHASUSCRIPCION,103) AS FECHASUSCRIPCION,
 CONVERT(VARCHAR(10),CON.FECHAFINALTERMINACION,103) AS FECHAFINALTERMINACION,
 CONINVERT.NUMEROCONTRATO as NUMEROCONTRATOINTER,
 CONVERT(VARCHAR(10),SUPCON.FECHAINICIA,103) AS FECHAINICIA,
 CONVERT(VARCHAR(10),SUPCON.FECHAFINALIZA,103) AS FECHAFINALIZA,
 CONVERT(VARCHAR(10),SUPCON.FECHAMODIFICAINTERVENTOR,103) AS FECHAMODIFICAINTERVENTOR,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN (SELECT GLOBAL.TIPOSDOCUMENTOS.NOMTIPODOCUMENTO FROM GLOBAL.TIPOSDOCUMENTOS WHERE IDTIPODOCUMENTO = TERCERO.IDTIPODOCIDENTIFICA)END AS TIPOIDENTIFICACIONSUPINV,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.NUMEROIDENTIFICACION END NUMEROIDENTIFICACION, 
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.RAZONSOCIAL END NOMBRERAZONSOCIALSUPERVISORINTERVENTOR,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.PRIMERAPELLIDO END PRIMERAPELLIDO,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.SEGUNDOAPELLIDO END SEGUNDOAPELLIDO,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.PRIMERNOMBRE END PRIMERNOMBRE,
 CASE WHEN SUPCON.IDTERCEROEXTERNO IS NOT NULL THEN TERCERO.SEGUNDONOMBRE END SEGUNDONOMBRE,
 DIRINTER.NUMEROIDENTIFICACION NUMEROIDENTIFICACIONDIRECTORINTERVENTORIA, 
 DIRINTER.PRIMERAPELLIDO AS PRIMERAPELLIDODIRECT,
 DIRINTER.SEGUNDOAPELLIDO AS SEGUNDOAPELLIDODIRECT,
 DIRINTER.PRIMERNOMBRE AS PRIMERNOMBREDIRECT,
 DIRINTER.SEGUNDONOMBRE AS SEGUNDONOMBREDIRECT,
 DIRINTER.RAZONSOCIAL NOMBRERAZONSOCIALDIRECTORINTERVENTORIA, 
 SUPCON.USUARIOCREA, 
 SUPCON.FECHACREA, 
 SUPCON.USUARIOMODIFICA, 
 SUPCON.FECHAMODIFICA 
 FROM [Contrato].[SupervisorIntervContrato] SUPCON INNER JOIN Contrato.TipoSupvInterventor TIPCON 
 ON (TIPCON.IdTipoSupvInterventor = SUPCON.IDTipoSupvInterventor) INNER JOIN Contrato.Contrato CON
 ON (CON.IdContrato = SUPCON.IDContratoSupervisa) LEFT outer JOIN Oferente.TERCERO TERCERO
 ON (TERCERO.IDTERCERO = SUPCON.IDTerceroExterno) LEFT outer JOIN Oferente.TERCERO DIRINTER
 ON (DIRINTER.IDTERCERO = SUPCON.IDTERCEROINTERVENTORIA) LEFT OUTER JOIN CONTRATO.CONTRATO CONINVERT
 ON (CONINVERT.IDCONTRATO = SUPCON.IDCONTRATOINTERVENTORIA)
 WHERE SUPCON.IdTipoSupvInterventor = CASE WHEN @TipoSupervisorInterventor IS NULL THEN SUPCON.IdTipoSupvInterventor ELSE @TipoSupervisorInterventor END 
 AND CON.NumeroContrato = CASE WHEN @NumeroContrato IS NULL THEN CON.NumeroContrato ELSE @NumeroContrato END 
 AND TERCERO.NUMEROIDENTIFICACION = CASE WHEN @NumeroIdentificacion IS NULL THEN TERCERO.NUMEROIDENTIFICACION ELSE @NumeroIdentificacion END
 AND ((TERCERO.RAZONSOCIAL LIKE ''%'' + CASE WHEN @NombreRazonSocialSupervisorInterventor IS NULL THEN TERCERO.RAZONSOCIAL ELSE @NombreRazonSocialSupervisorInterventor END + ''%'')
	OR (TERCERO.PRIMERNOMBRE + '' '' +TERCERO.SEGUNDONOMBRE + '' '' + TERCERO.PRIMERAPELLIDO + '' '' + TERCERO.SEGUNDOAPELLIDO LIKE ''%'' + CASE WHEN @NombreRazonSocialSupervisorInterventor IS NULL THEN TERCERO.PRIMERNOMBRE + '' '' +TERCERO.SEGUNDONOMBRE + '' '' + TERCERO.PRIMERAPELLIDO + '' '' + TERCERO.SEGUNDOAPELLIDO ELSE @NombreRazonSocialSupervisorInterventor END + ''%''))
 AND (DIRINTER.NUMEROIDENTIFICACION = @NumeroIdentificacionDirectorInterventoria OR @NumeroIdentificacionDirectorInterventoria IS NULL)
 AND ((DIRINTER.RAZONSOCIAL LIKE ''%'' + @NombreRazonSocialDirectorInterventoria OR @NombreRazonSocialDirectorInterventoria IS NULL)
	OR (DIRINTER.PRIMERNOMBRE + '' '' +DIRINTER.SEGUNDONOMBRE + '' '' + DIRINTER.PRIMERAPELLIDO + '' '' + DIRINTER.SEGUNDOAPELLIDO LIKE ''%'' + @NombreRazonSocialDirectorInterventoria OR @NombreRazonSocialDirectorInterventoria IS NULL))
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]    Script Date: 07/26/2013 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Consultar los tipos de amparos
Modificado por Jonathan Acosta
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparos_Consultar]
	@NombreTipoAmparo NVARCHAR(50) = NULL,@IdTipoGarantia INT = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdTipoAmparo, NombreTipoAmparo, IdTipoGarantia, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[TipoAmparo] 
 WHERE NombreTipoAmparo = CASE WHEN @NombreTipoAmparo IS NULL THEN NombreTipoAmparo ELSE @NombreTipoAmparo END AND IdTipoGarantia = CASE WHEN @IdTipoGarantia IS NULL THEN IdTipoGarantia ELSE @IdTipoGarantia END
 AND (Estado = @Estado OR @Estado IS NULL)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]    Script Date: 07/26/2013 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]
		@IdTipoAmparo INT,	@NombreTipoAmparo NVARCHAR(50),	@IdTipoGarantia INT,	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoAmparo SET NombreTipoAmparo = @NombreTipoAmparo, IdTipoGarantia = @IdTipoGarantia, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoAmparo = @IdTipoAmparo
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]    Script Date: 07/26/2013 17:06:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]
		@IdTipoAmparo INT OUTPUT, 	@NombreTipoAmparo NVARCHAR(50),	@IdTipoGarantia INT,	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoAmparo(NombreTipoAmparo, IdTipoGarantia, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoAmparo, @IdTipoGarantia, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoAmparo = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]    Script Date: 07/26/2013 17:06:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]
	@IdTipoAmparo INT
AS
BEGIN
	DELETE Contrato.TipoAmparo WHERE IdTipoAmparo = @IdTipoAmparo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]    Script Date: 07/26/2013 17:06:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]
	@IdTipoAmparo INT
AS
BEGIN
 SELECT IdTipoAmparo, NombreTipoAmparo, IdTipoGarantia, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoAmparo] WHERE  IdTipoAmparo = @IdTipoAmparo
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]    Script Date: 07/26/2013 17:06:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 10:01
   -- Description:          Procedimiento almacenado que consulta un(a) TipoContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContratos_Consultar]
	@NombreTipoContrato NVARCHAR(128) = NULL,@IdCategoriaContrato INT = NULL,
	@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoContrato, NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion, 
 RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, 
 FechaModifica 
 FROM [Contrato].[TipoContrato] 
 WHERE NombreTipoContrato = CASE WHEN @NombreTipoContrato IS NULL THEN NombreTipoContrato ELSE @NombreTipoContrato END 
 AND IdCategoriaContrato = CASE WHEN @IdCategoriaContrato IS NULL THEN IdCategoriaContrato ELSE @IdCategoriaContrato END 
 AND (Estado = @Estado or @Estado is null)
 ORDER BY NombreTipoContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]    Script Date: 07/26/2013 17:06:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]
		@IdTipoContrato INT,	@NombreTipoContrato NVARCHAR(128),	@IdCategoriaContrato INT,	@ActaInicio BIT,	@AporteCofinaciacion BIT,	@RecursoFinanciero BIT,	@RegimenContrato INT,	@DescripcionTipoContrato NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoContrato SET NombreTipoContrato = @NombreTipoContrato, IdCategoriaContrato = @IdCategoriaContrato, ActaInicio = @ActaInicio, AporteCofinaciacion = @AporteCofinaciacion, RecursoFinanciero = @RecursoFinanciero, RegimenContrato = @RegimenContrato, DescripcionTipoContrato = @DescripcionTipoContrato, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoContrato = @IdTipoContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]    Script Date: 07/26/2013 17:06:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]
		@IdTipoContrato INT OUTPUT, 	@NombreTipoContrato NVARCHAR(128),	@IdCategoriaContrato INT,	@ActaInicio BIT,	@AporteCofinaciacion BIT,	@RecursoFinanciero BIT,	@RegimenContrato INT,	@DescripcionTipoContrato NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoContrato(NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion, RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoContrato, @IdCategoriaContrato, @ActaInicio, @AporteCofinaciacion, @RecursoFinanciero, @RegimenContrato, @DescripcionTipoContrato, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoContrato = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]    Script Date: 07/26/2013 17:06:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]
	@IdTipoContrato INT
AS
BEGIN
	DELETE Contrato.TipoContrato WHERE IdTipoContrato = @IdTipoContrato
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]    Script Date: 07/26/2013 17:06:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]
	@IdTipoContrato INT
AS
BEGIN
 SELECT IdTipoContrato, NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion, RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoContrato] WHERE  IdTipoContrato = @IdTipoContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]    Script Date: 07/26/2013 17:06:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
   
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que consulta Obligacion
   -- Modificado por Jonathan Acosta - Consulta por estado del contrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacions_Consultar]
	@IdTipoObligacion INT = NULL,@IdTipoContrato INT = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdObligacion, IdTipoObligacion, IdTipoContrato, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[Obligacion] WHERE IdTipoObligacion = CASE WHEN @IdTipoObligacion IS NULL THEN IdTipoObligacion ELSE @IdTipoObligacion END AND IdTipoContrato = CASE WHEN @IdTipoContrato IS NULL THEN IdTipoContrato ELSE @IdTipoContrato END 
 AND (Estado = @Estado or @Estado is null)
 ORDER BY IdTipoContrato, Descripcion 
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]    Script Date: 07/26/2013 17:06:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que actualiza Obligacion
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]
		@IdObligacion INT,	@IdTipoObligacion INT,	@IdTipoContrato INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.Obligacion SET IdTipoObligacion = @IdTipoObligacion, IdTipoContrato = @IdTipoContrato, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdObligacion = @IdObligacion
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]    Script Date: 07/26/2013 17:06:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que inserta Obligacion
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]
		@IdObligacion INT OUTPUT, 	@IdTipoObligacion INT,	@IdTipoContrato INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.Obligacion(IdTipoObligacion, IdTipoContrato, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoObligacion, @IdTipoContrato, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdObligacion = @@IDENTITY 		
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]    Script Date: 07/26/2013 17:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_Obligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]
	@IdObligacion INT
AS
BEGIN
	DELETE Contrato.Obligacion WHERE IdObligacion = @IdObligacion
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]    Script Date: 07/26/2013 17:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_Obligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]
	@IdObligacion INT
AS
BEGIN
 SELECT IdObligacion, IdTipoObligacion, IdTipoContrato, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[Obligacion] WHERE  IdObligacion = @IdObligacion
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_LugarEjecucionContratos_Consultar]    Script Date: 07/26/2013 17:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_LugarEjecucionContratos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContratos_Consultar]
@NUMEROCONTRATO NVARCHAR(50) = NULL,
@IDDEPARTAMENTO	INT = NULL,
@IDMUNICIPIO	INT = NULL
	
AS
BEGIN
 SELECT LUGAREJECUCIONCTR.IDCONTRATOLUGAREJECUCION, LUGAREJECUCIONCTR.IDCONTRATO, CONTR.NUMEROCONTRATO, NACIONAL, 
 LUGAREJECUCION.IDDEPARTAMENTO, DEPTO.NOMBREDEPARTAMENTO,LUGAREJECUCION.IDMUNICIPIO,MUNPIO.NOMBREMUNICIPIO
 FROM [CONTRATO].[LUGAREJECUCIONCONTRATO] LUGAREJECUCIONCTR INNER JOIN CONTRATO.CONTRATO CONTR 
 ON (CONTR.IDCONTRATO = LUGAREJECUCIONCTR.IDCONTRATO) LEFT JOIN CONTRATO.LUGAREJECUCION LUGAREJECUCION
 ON (LUGAREJECUCION.IDCONTRATOLUGAREJECUCION = LUGAREJECUCIONCTR.IDCONTRATOLUGAREJECUCION) LEFT JOIN DIV.DEPARTAMENTO DEPTO
 ON (DEPTO.IDDEPARTAMENTO = LUGAREJECUCION.IDDEPARTAMENTO) LEFT JOIN DIV.MUNICIPIO MUNPIO
 ON (MUNPIO.IDMUNICIPIO = LUGAREJECUCION.IDMUNICIPIO)
 WHERE 
 (CONTR.NUMEROCONTRATO = @NUMEROCONTRATO OR @NUMEROCONTRATO IS NULL)
 AND (LUGAREJECUCION.IDDEPARTAMENTO = @IDDEPARTAMENTO OR @IDDEPARTAMENTO IS NULL)
 AND (LUGAREJECUCION.IDMUNICIPIO = @IDMUNICIPIO OR @IDMUNICIPIO IS NULL)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]    Script Date: 07/26/2013 17:05:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ClausulaContratos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContratos_Consultar]
	@NombreClausulaContrato NVARCHAR(128) = NULL,@IdTipoClausula INT = NULL,@IdTipoContrato INT = NULL,
	@Estado	bit = NULL
AS
BEGIN
 SELECT IdClausulaContrato, NombreClausulaContrato, IdTipoClausula, Contenido, IdTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ClausulaContrato] WHERE NombreClausulaContrato = CASE WHEN @NombreClausulaContrato IS NULL THEN NombreClausulaContrato ELSE @NombreClausulaContrato END AND IdTipoClausula = CASE WHEN @IdTipoClausula IS NULL THEN IdTipoClausula ELSE @IdTipoClausula END AND IdTipoContrato = CASE WHEN @IdTipoContrato IS NULL THEN IdTipoContrato ELSE @IdTipoContrato END
 AND (Estado = @Estado OR @Estado IS NULL)
 ORDER BY NombreClausulaContrato
END 
 

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]    Script Date: 07/26/2013 17:05:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que modifica un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]
		@IdClausulaContrato INT,	@NombreClausulaContrato NVARCHAR(128),	@IdTipoClausula INT,	@Contenido NVARCHAR(128),	@IdTipoContrato INT,	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ClausulaContrato SET NombreClausulaContrato = @NombreClausulaContrato, IdTipoClausula = @IdTipoClausula, Contenido = @Contenido, IdTipoContrato = @IdTipoContrato, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdClausulaContrato = @IdClausulaContrato
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]    Script Date: 07/26/2013 17:05:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que inserta un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]
		@IdClausulaContrato INT OUTPUT, 	@NombreClausulaContrato NVARCHAR(128),	@IdTipoClausula INT,	@Contenido NVARCHAR(128),	@IdTipoContrato INT,	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ClausulaContrato(NombreClausulaContrato, IdTipoClausula, Contenido, IdTipoContrato, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreClausulaContrato, @IdTipoClausula, @Contenido, @IdTipoContrato, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdClausulaContrato = @@IDENTITY 		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]    Script Date: 07/26/2013 17:05:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'	
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ClausulaContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]
	@IdClausulaContrato INT
	AS
	BEGIN
		DELETE Contrato.ClausulaContrato WHERE IdClausulaContrato = @IdClausulaContrato
	END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]    Script Date: 07/26/2013 17:05:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que consulta un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]
	@IdClausulaContrato INT
AS
BEGIN
 SELECT IdClausulaContrato, NombreClausulaContrato, IdTipoClausula, Contenido, IdTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ClausulaContrato] WHERE  IdClausulaContrato = @IdClausulaContrato
END


' 
END
GO
