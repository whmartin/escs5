USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]    Script Date: 08/01/2013 21:44:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPagos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]
	@NombreFormaPago NVARCHAR(50) = NULL,
	@Descripcion NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN
 SELECT IdFormapago, NombreFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[FormaPago] 
 WHERE NombreFormaPago = CASE WHEN @NombreFormaPago IS NULL THEN NombreFormaPago ELSE @NombreFormaPago END
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END
 AND (Estado = @Estado OR @Estado IS NULL)
END

GO


