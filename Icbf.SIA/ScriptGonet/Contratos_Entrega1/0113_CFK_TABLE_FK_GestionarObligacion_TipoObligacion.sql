USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Contrato].[FK_GestionarObligacion_TipoObligacion]') AND parent_object_id = OBJECT_ID(N'[Contrato].[GestionarObligacion]'))
ALTER TABLE [Contrato].[GestionarObligacion] DROP CONSTRAINT [FK_GestionarObligacion_TipoObligacion]
GO


USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea la realaci�n con la tabla [TipoObligacion] - [GestionarObligacion]
   -- =============================================
ALTER TABLE [Contrato].[GestionarObligacion]  WITH CHECK ADD  CONSTRAINT [FK_GestionarObligacion_TipoObligacion] FOREIGN KEY([IdTipoObligacion])
REFERENCES [Contrato].[TipoObligacion] ([IdTipoObligacion])
GO

ALTER TABLE [Contrato].[GestionarObligacion] CHECK CONSTRAINT [FK_GestionarObligacion_TipoObligacion]
GO


