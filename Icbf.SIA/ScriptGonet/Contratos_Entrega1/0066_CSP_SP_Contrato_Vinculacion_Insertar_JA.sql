USE [SIA]
GO

/****** Object:  StoredProcedure [AUDITA].[usp_RubOnline_Contrato_ContratoVinculacion_Insertar]    Script Date: 08/05/2013 09:22:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AUDITA].[usp_RubOnline_Contrato_ContratoVinculacion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [AUDITA].[usp_RubOnline_Contrato_ContratoVinculacion_Insertar]
GO


-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/27/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Contrato vinculacion
-- =============================================
CREATE PROCEDURE [AUDITA].[usp_RubOnline_Contrato_ContratoVinculacion_Insertar]
@IdContrato	INT,
@IdContratoAsociado	INT
AS
BEGIN
	SET NOCOUNT ON;
INSERT INTO Contrato.ContratoVinculacion(IdContrato,IdContratoAsociado)
VALUES(@IdContrato,@IdContratoAsociado)
END

GO

