
USE [SIA]
GO

/****** Object:  Index [UQ_NombreTipoAmparo]    Script Date: 08/06/2013 19:30:08 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Contrato].[TipoAmparo]') AND name = N'UQ_NombreTipoAmparo')
ALTER TABLE [Contrato].[TipoAmparo] DROP CONSTRAINT [UQ_NombreTipoAmparo]
GO

TRUNCATE TABLE [Contrato].[TipoAmparo]
GO


USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Crea el indice �nico en la tabla [TipoAmparo]
   -- =============================================
/****** Object:  Index [UQ_NombreTipoAmparo]    Script Date: 08/06/2013 19:30:10 ******/
ALTER TABLE [Contrato].[TipoAmparo] ADD  CONSTRAINT [UQ_NombreTipoAmparo] UNIQUE NONCLUSTERED 
(
	[NombreTipoAmparo] ASC,
	[idTipoGarantia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO