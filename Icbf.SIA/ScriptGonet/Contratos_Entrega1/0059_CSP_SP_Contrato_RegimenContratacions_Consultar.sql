USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]    Script Date: 08/01/2013 21:36:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacions_Consultar
Modificado por Jonathan Acosta
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]
	@NombreRegimenContratacion NVARCHAR(128) = NULL,
	@Descripcion NVARCHAR(128) = NULL,
	@Estado	bit = null
AS
BEGIN

 SELECT IdRegimenContratacion, NombreRegimenContratacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[RegimenContratacion] 
 WHERE NombreRegimenContratacion = CASE WHEN @NombreRegimenContratacion IS NULL THEN NombreRegimenContratacion ELSE @NombreRegimenContratacion END
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END
 AND (Estado = @Estado OR @Estado IS NULL)
 
END
GO


