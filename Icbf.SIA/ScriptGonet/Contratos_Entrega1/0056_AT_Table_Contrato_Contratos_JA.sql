USE [SIA]
GO
-- =============================================
-- Author:              
-- Create date:         
-- Description:         Agrega la columna ClaseContrato a la entidad Contrato.Contrato
-- =============================================

/* Para evitar posibles problemas de p�rdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del dise�ador de base de datos.*/
IF NOT EXISTS (SELECT
	NAME
FROM SYSCOLUMNS
WHERE ID = OBJECT_ID('Contrato.Contrato') AND NAME = ('ClaseContrato'))
BEGIN


	ALTER TABLE Contrato.Contrato ADD
	ClaseContrato nchar (1) NULL

END