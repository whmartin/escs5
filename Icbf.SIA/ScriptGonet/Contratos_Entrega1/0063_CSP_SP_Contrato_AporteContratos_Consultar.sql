USE [SIA]
GO
-- =============================================
-- Author:              
-- Create date:         
-- Description: Crea el procedimiento usp_RubOnline_Contrato_AporteContratos_Consultar
-- =============================================
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]    Script Date: 08/01/2013 23:13:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]
GO

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContratos_Consultar]
	@IdAporte INT = NULL,@IdContrato NVARCHAR(3) = NULL,@TipoAportante NVARCHAR(11) = NULL,@NumeroIdenticacionContratista bigint = NULL,@TipoAporte NVARCHAR(10) = NULL,@ValorAporte INT = NULL,@DescripcionAporteEspecie NVARCHAR(128) = NULL,@FechaRP DATETIME = NULL,@NumeroRP INT = NULL
AS
BEGIN
 SELECT IdAporte, IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AporteContrato] WHERE IdAporte = CASE WHEN @IdAporte IS NULL THEN IdAporte ELSE @IdAporte END AND IdContrato = CASE WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato END AND TipoAportante = CASE WHEN @TipoAportante IS NULL THEN TipoAportante ELSE @TipoAportante END AND NumeroIdenticacionContratista = CASE WHEN @NumeroIdenticacionContratista IS NULL THEN NumeroIdenticacionContratista ELSE @NumeroIdenticacionContratista END AND TipoAporte = CASE WHEN @TipoAporte IS NULL THEN TipoAporte ELSE @TipoAporte END AND ValorAporte = CASE WHEN @ValorAporte IS NULL THEN ValorAporte ELSE @ValorAporte END AND DescripcionAporteEspecie = CASE WHEN @DescripcionAporteEspecie IS NULL THEN DescripcionAporteEspecie ELSE @DescripcionAporteEspecie END AND FechaRP = CASE WHEN @FechaRP IS NULL THEN FechaRP ELSE @FechaRP END AND NumeroRP = CASE WHEN @NumeroRP IS NULL THEN NumeroRP ELSE @NumeroRP END
 ORDER BY IdAporte,NumeroIdenticacionContratista, TipoAporte 
END
GO


