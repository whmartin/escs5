USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]    Script Date: 08/12/2013 20:12:25 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]
GO
-- =============================================
-- Author:          Juan Carlos Valverde S�mano    
-- Create date:         17/01/2014
-- Description:      Realiza un consulta de un registro en espec�fico de la entidad [Contrato].[RelacionarContratistas]
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]
@IdContratistaContrato INT
AS
BEGIN
SELECT
	IdContratistaContrato,
	IdContrato,
	NumeroIdentificacion,
	ClaseEntidad,
	PorcentajeParticipacion,
	EstadoIntegrante,
	NumeroIdentificacionRepresentanteLegal,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[RelacionarContratistas]
WHERE IdContratistaContrato = @IdContratistaContrato
END

GO