USE [SIA]
GO

/****** Object:  Table [Proveedor].[TipoDocumento]    Script Date: 08/07/2013 19:00:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[TipoDocumento]') AND type in (N'U'))
DROP TABLE [Contrato].[TipoDocumento]
GO

USE [SIA]
GO
-- =============================================
-- Author: Juan Carlos Valverde S�mano	        
-- Create date: 17]/01/2014
-- Description:Crea la entidad [Contrato].[TipoDocumento]
-- =============================================
/****** Object:  Table [Proveedor].[TipoDocumento]    Script Date: 08/07/2013 19:00:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Contrato].[TipoDocumento](
	[IdTipoDocumento] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoDocumento] [nvarchar](128) NOT NULL,
	[Descripcion] [nvarchar](128) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoDocumento] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


