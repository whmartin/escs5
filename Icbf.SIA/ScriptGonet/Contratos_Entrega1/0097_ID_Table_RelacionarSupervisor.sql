USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Andr�s Morales
   -- Create date:         06/08/2013 11:46am
   -- Description:         Inserta en la tabla para ser visualizada en la aplicaci�n
   --						y el permiso al usuario administrador
   -- =============================================
IF NOT EXISTS(SELECT IDPROGRAMA FROM SEG.PROGRAMA 
WHERE
	CODIGOPROGRAMA = 'Contratos/RelacionarSupervisorInterventor' and 
	NOMBREPROGRAMA = 'Relacionar Supervisor' )
BEGIN
      DECLARE @idpadre int
     
      SELECT @idpadre = IdPrograma 
	  FROM SEG.Programa 
	  WHERE 
	  NombrePrograma = 'Contrato'
---	  
      INSERT INTO SEG.Programa 
      SELECT 
      IdModulo,'Relacionar Supervisor','Contratos/RelacionarSupervisorInterventor',1,1,'Administrador',GETDATE(),'','',1,1,NULL,NULL,NULL
      FROM SEG.Modulo
      WHERE 
      NOMBREMODULO = 'CONTRATOS'    
---
      INSERT INTO SEG.PERMISO 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      FROM SEG.Programa
      WHERE 
      CODIGOPROGRAMA = 'Contratos/RelacionarSupervisorInterventor' and 
      NOMBREPROGRAMA = 'Relacionar Supervisor' 
END
ELSE
BEGIN
      PRINT 'El VALOR A INSERTAR YA EXISTE EN LA BASE DE DATOS'
END