USE [SIA]
GO

-- =============================================
-- Author:              @ReS\Andrés Morales
-- Create date:         06/08/2013 11:46am
-- Description:         Crea la columna requieregarantía en la tabla contrato
-- =============================================

IF NOT EXISTS (SELECT
	NAME
FROM SYSCOLUMNS
WHERE ID = OBJECT_ID('Contrato.Contrato')
AND NAME = ('RequiereGarantia'))
BEGIN

	ALTER TABLE Contrato.Contrato ADD
	RequiereGarantia bit NULL

END




