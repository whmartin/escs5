USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]    Script Date: 07/31/2013 00:52:40 ******/
IF EXISTS (SELECT
	*
FROM sys.objects
WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el procedimiento de consulta de contratistas
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]
@IdContratistaContrato INT
AS
BEGIN
SELECT
	IdContratistaContrato,
	IdContrato,
	NumeroIdentificacion,
	ClaseEntidad,
	PorcentajeParticipacion,
	NumeroIdentificacionRepresentanteLegal,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[RelacionarContratistas]
WHERE IdContratistaContrato = @IdContratistaContrato
END
GO