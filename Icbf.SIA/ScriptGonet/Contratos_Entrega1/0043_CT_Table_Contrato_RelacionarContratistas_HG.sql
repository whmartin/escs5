USE [SIA]
GO

/****** Object:  Table [Contrato].[RelacionarContratistas]    Script Date: 07/30/2013 21:17:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[RelacionarContratistas]') AND type in (N'U'))
DROP TABLE [Contrato].[RelacionarContratistas]
GO
/***********************************************
Creado Por: @ReS Soluciones.
			Henry Gomez
Permite : Crea la tabla para almacenar la información de los contratistas de un contrato
***********************************************/


CREATE TABLE [Contrato].[RelacionarContratistas](
	[IdContratistaContrato] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [int] NOT NULL,
	[NumeroIdentificacion] [bigint] NOT NULL,
	[ClaseEntidad] [nvarchar](1) NOT NULL,
	[PorcentajeParticipacion] [int] NOT NULL,
	[NumeroIdentificacionRepresentanteLegal] [bigint] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RelacionarContratistas] PRIMARY KEY CLUSTERED 
(
	[IdContratistaContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


