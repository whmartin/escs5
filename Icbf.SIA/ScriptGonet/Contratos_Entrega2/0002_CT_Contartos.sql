USE [SIA]
GO


/**************************************
-- Author:	    gonet
-- Create date: 2014-07-09
-- Description: Crear Tabla inicial de contratos
**************************************/

--/****** Object:  Table [CONTRATO].[Contrato]    Script Date: 03/07/2014 11:32:49 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[Contrato]') AND type in (N'U'))
--DROP TABLE [CONTRATO].[Contrato]
--GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[Contrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[Contrato](
	[IdContrato] [int] IDENTITY(1,1) NOT NULL,
	[IdNumeroProceso] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[IdRegionalContrato] [int] NULL,
	[IdTipoContrato] [int] NULL,
	[IdEstadoContrato] [int] NULL,
	[ObjetoDelContrato] [nvarchar](800) NULL,
	[FechaInicioEjecucion] [datetime] NULL,
	[FechaFinalTerminacionContrato] [datetime] NULL,
	[FechaFinalizacionIniciaContrato] [datetime] NULL,
	[FechaSuscripcionContrato] [datetime] NULL,
	[NumeroContrato] [nvarchar](50) NULL,
	[ValorInicialContrato] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Contrato] PRIMARY KEY CLUSTERED 
(
	[IdContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

