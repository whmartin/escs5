USE [SIA]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ProveedoresContratos]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla ProveedoresContratos a crear'
RETURN
END
CREATE TABLE [CONTRATO].[ProveedoresContratos](
 [IdProveedoresContratos] [INT]  IDENTITY(1,1) NOT NULL,
 [IdProveedores] [INT]  NOT NULL,
 [IdContrato] [INT]  NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_ProveedoresContratos] PRIMARY KEY CLUSTERED 
(
 	[IdProveedoresContratos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ProveedoresContratos') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Almacena datos sobre contratos asignados a proveedores', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ProveedoresContratos;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ProveedoresContratos') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdProveedoresContratos' AND [object_id] = OBJECT_ID('CONTRATO.ProveedoresContratos')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ProveedoresContratos,
@level2type = N'COLUMN', @level2name = IdProveedoresContratos;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ProveedoresContratos') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdProveedores' AND [object_id] = OBJECT_ID('CONTRATO.ProveedoresContratos')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Id Proveedores', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ProveedoresContratos,
@level2type = N'COLUMN', @level2name = IdProveedores;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ProveedoresContratos') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdContrato' AND [object_id] = OBJECT_ID('CONTRATO.ProveedoresContratos')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Id Contrato', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ProveedoresContratos,
@level2type = N'COLUMN', @level2name = IdContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ProveedoresContratos') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('CONTRATO.ProveedoresContratos')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ProveedoresContratos,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ProveedoresContratos') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('CONTRATO.ProveedoresContratos')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ProveedoresContratos,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ProveedoresContratos') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('CONTRATO.ProveedoresContratos')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ProveedoresContratos,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ProveedoresContratos') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('CONTRATO.ProveedoresContratos')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ProveedoresContratos,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
