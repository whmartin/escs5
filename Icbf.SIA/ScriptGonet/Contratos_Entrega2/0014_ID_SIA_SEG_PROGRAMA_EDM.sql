USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  12/05/2014 9:00 AM
-- Description:	Insertar Tabla programa para visualizar en el men� de Contratos la opci�n de listas param�tricas
-- =============================================
IF not exists(SELECT * from [SEG].[Programa] where [NombrePrograma] = N'Listas param�tricas' and [CodigoPrograma] = N'Contratos/TablaParametrica')
Begin
	IF exists(SELECT * from SEG.Modulo where [NombreModulo] = 'Contratos')
	Begin

		declare @IdModulo int = null

			select @IdModulo = IdModulo from SEG.Modulo where [NombreModulo] = 'Contratos'

				INSERT into [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], 
				[UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
				VALUES (@IdModulo, N'Listas param�tricas', N'Contratos/TablaParametrica', 2, 1, N'Administrador', GETDATE(), NULL, 
				NULL, 1, 1, NULL)

	end
End