USE [SIA]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar]    Script Date: 06/06/2014 04:17:33 p.m. ******/
DROP PROCEDURE [dbo].[usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar]    Script Date: 06/06/2014 04:17:33 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Obtiene las modalidades academicas registradas en Kactus
-- =============================================
CREATE PROCEDURE [dbo].[usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar] 
	@Nombre VARCHAR(100) = NULL, @Id VARCHAR(30) = NULL, @Estado BIT = NULL
AS
BEGIN
	
	SELECT '' AS Id, '' AS Codigo, '' AS Descripcion

END

GO


