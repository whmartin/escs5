USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  22/08/2014 
-- Description:	Insertar Tabla programa para visualizar en el men� de tablas param�tricas la opci�n de Objeto Contrato
-- =============================================
IF not exists(SELECT * from [SEG].[Programa] where [NombrePrograma] = 'Objeto Contrato')
Begin
INSERT INTO [SEG].[Programa]
           ([IdModulo]
           ,[NombrePrograma]
           ,[CodigoPrograma]
           ,[Posicion]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           ,[VisibleMenu]
           ,[generaLog])
     VALUES
           (3
           ,'Objeto Contrato'
           ,'Contratos/ObjetoContrato'
           ,1
           ,1
           ,'Administrador'
           ,GETDATE()
           ,0
           ,1)
End


