USE [SIA]
GO

/********************************
2014-05-22
Creamos el SYNONYM RegionalesPCI 
jorge Vizcaino
********************************/

/****** Object:  Synonym [Ppto].[RegionalesPCI]    Script Date: 22/05/2014 11:45:23 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'RegionalesPCI' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[RegionalesPCI]
GO

/****** Object:  Synonym [Ppto].[RegionalesPCI]    Script Date: 22/05/2014 11:45:23 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'RegionalesPCI' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[RegionalesPCI] FOR [NMF].[Ppto].[RegionalesPCI]
GO


