USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_EstadosGarantias_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_EstadosGarantias_Insertar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/25/2014 5:10:11 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_EstadosGarantias_Insertar]
		@IDEstadosGarantias INT OUTPUT, 	@CodigoEstadoGarantia NVARCHAR(8),	@DescripcionEstadoGarantia NVARCHAR(80), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.EstadosGarantias(CodigoEstadoGarantia, DescripcionEstadoGarantia, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoEstadoGarantia, @DescripcionEstadoGarantia, @UsuarioCrea, GETDATE())
	SELECT @IDEstadosGarantias = @@IDENTITY 		
END
