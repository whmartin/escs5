USE [SIA]
GO
-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/27/2014 6:43:39 PM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ConsecutivoContratoRegionales]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla ConsecutivoContratoRegionales a crear'
RETURN
END
CREATE TABLE [CONTRATO].[ConsecutivoContratoRegionales](
 [IDConsecutivoContratoRegional] [INT]  IDENTITY(1,1) NOT NULL,
 [IdRegional] [INT]  NOT NULL,
 [IdVigencia] [INT]  NOT NULL,
 [Consecutivo] [NUMERIC] (30) NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_ConsecutivoContratoRegionales] PRIMARY KEY CLUSTERED 
(
 	[IDConsecutivoContratoRegional] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [CONTRATO].[ConsecutivoContratoRegionales]
ADD CONSTRAINT FK_ConsecutivoContratoRegionales_IdRegional
FOREIGN KEY (IdRegional) REFERENCES [DIV].[Regional](IdRegional)

ALTER TABLE [CONTRATO].[ConsecutivoContratoRegionales]
ADD CONSTRAINT FK_ConsecutivoContratoRegionales_IdVigencia
FOREIGN KEY (IdVigencia) REFERENCES [Global].[Vigencia](IdVigencia)

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Almacena datos sobre ConsecutivoContratoRegionales', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDConsecutivoContratoRegional' AND [object_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales,
@level2type = N'COLUMN', @level2name = IDConsecutivoContratoRegional;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdRegional' AND [object_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Regional', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales,
@level2type = N'COLUMN', @level2name = IdRegional;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdVigencia' AND [object_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Vigencia', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales,
@level2type = N'COLUMN', @level2name = IdVigencia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Consecutivo' AND [object_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Consecutivo', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales,
@level2type = N'COLUMN', @level2name = Consecutivo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('CONTRATO.ConsecutivoContratoRegionales')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = ConsecutivoContratoRegionales,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
