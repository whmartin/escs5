USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Proveedores_Contratoss_Consultar]    Script Date: 03/07/2014 03:37:01 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ProveedoresContratos_formaPago_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_formaPago_Consultar]
GO

-- =============================================
-- Author:		Gonet / Efrain Diaz Mejia
-- Create date:  03/07/2014 15:40
-- Description:	Procedimiento almacenado que consulta un(a) Proveedores_Contratos por forma de pago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_formaPago_Consultar]
	@IdProveedores INT = NULL, 
	@IdFormaPago INT = NULL,
	@IdContrato INT = NULL
	
AS
BEGIN
	SELECT
		IdProveedoresContratos,
		IdProveedores,
		IdContrato,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica,
		IdFormaPago
	FROM [CONTRATO].[ProveedoresContratos]
	WHERE IdProveedores =
		CASE
			WHEN @IdProveedores IS NULL THEN IdProveedores ELSE @IdProveedores
		END AND IdFormaPago =
		CASE
			WHEN @IdFormaPago IS NULL THEN IdFormaPago ELSE @IdFormaPago
		END AND IdContrato <>
		CASE
			WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato
		END
END




