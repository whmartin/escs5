USE [SIA]
/***********************************************
Creado Por: Carlos Andr�s C�rdenas
El dia : 2013-08-26
Modificado Por: Abraham Rivero Dom�nguez
El d�a : 2014-06-10
Permite : Crear el programa Consultar supervisor de cada contrato 
y asignarle permisos al administrador
***********************************************/
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Contratos/ConsultarSuperInterContrato' )
begin
      
	    

      insert INTO [SEG].[Programa]([IdModulo],
					[NombrePrograma],
					[CodigoPrograma],
					[Posicion],
					[Estado],
					[UsuarioCreacion],
					[FechaCreacion],
					[UsuarioModificacion],[FechaModificacion],[VisibleMenu],[generaLog],[Padre])
      SELECT IdModulo,'Consulta supervisor Contrato','Contratos/ConsultarSuperInterContrato',1,1,
      'Administrador',GETDATE(),NULL,NULL,1,1,NULL
      from SEG.Modulo
      where NombreModulo = 'Contratos'    
      
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
