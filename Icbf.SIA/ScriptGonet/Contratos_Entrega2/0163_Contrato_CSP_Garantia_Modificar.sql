USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_Garantia_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Modificar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/4/2014 9:59:59 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Modificar]
		@IDGarantia INT, 	
		@IDTipoGarantia INT,	
		@NumeroGarantia NVARCHAR(80),	
		@FechaAprobacionGarantia DATETIME,	
		@FechaCertificacionGarantia DATETIME,	
		@FechaDevolucion DATETIME,	
		@MotivoDevolucion NVARCHAR(800),	
		@IDUsuarioAprobacion INT,	
		@IDUsuarioDevolucion INT,	
		@IdContrato INT,	
		@BeneficiarioICBF BIT,
		@BeneficiarioOTROS BIT,		
		@DescripcionBeneficiarios NVARCHAR(400),	
		@FechaInicioGarantia DATETIME,	
		@FechaExpedicionGarantia DATETIME,	
		@FechaVencimientoInicialGarantia DATETIME,	
		@FechaVencimientoFinalGarantia DATETIME,	
		@FechaReciboGarantia DATETIME,	
		@ValorGarantia NVARCHAR(50),	
		@Anexos BIT,	
		@ObservacionesAnexos NVARCHAR(400),	
		@EntidadProvOferenteAseguradora INT, 
		@UsuarioModifica NVARCHAR(250),
		@Estado BIT,
		@IDSucursalAseguradoraContrato INT,
		@IDEstadosGarantias INT
AS
BEGIN
	IF EXISTS(SELECT * FROM Contrato.Garantia WHERE NumeroGarantia=@NumeroGarantia AND IDGarantia <> @IDGarantia)
	BEGIN
		RAISERROR('Ya existe un registro con este(a) Número Garantía',16,1)
	    RETURN
	END
	UPDATE Contrato.Garantia SET 
	IDTipoGarantia = @IDTipoGarantia, 
	NumeroGarantia = @NumeroGarantia, 
	FechaAprobacionGarantia = @FechaAprobacionGarantia, 
	FechaCertificacionGarantia = @FechaCertificacionGarantia, 
	FechaDevolucion = @FechaDevolucion, 
	MotivoDevolucion = @MotivoDevolucion, 
	IDUsuarioAprobacion = @IDUsuarioAprobacion, 
	IDUsuarioDevolucion = @IDUsuarioDevolucion, 
	IdContrato = @IdContrato,
	BeneficiarioICBF = @BeneficiarioICBF, 
	BeneficiarioOTROS = @BeneficiarioOTROS,
	DescripcionBeneficiarios = @DescripcionBeneficiarios, 
	FechaInicioGarantia = @FechaInicioGarantia, 
	FechaExpedicionGarantia = @FechaExpedicionGarantia, 
	FechaVencimientoInicialGarantia = @FechaVencimientoInicialGarantia, 
	FechaVencimientoFinalGarantia = @FechaVencimientoFinalGarantia, 
	FechaReciboGarantia = @FechaReciboGarantia, 
	ValorGarantia = @ValorGarantia, 
	Anexos = @Anexos, 
	ObservacionesAnexos = @ObservacionesAnexos, 
	EntidadProvOferenteAseguradora = @EntidadProvOferenteAseguradora, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE(),
	Estado =  @Estado,
	IDSucursalAseguradoraContrato = @IDSucursalAseguradoraContrato,
	IDEstadosGarantias = @IDEstadosGarantias
	WHERE IDGarantia = @IDGarantia
END
