USE [SIA]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contratos_Adheridos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contratos_Adheridos_Consultar]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz mejia
-- Create date:  30/07/2014 
-- Description:	Procedimiento almacenado que consulta  Contratos Adheridos
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contratos_Adheridos_Consultar]
	@IdContrato INT
AS
BEGIN

	 select idcontrato, numeroContrato, valorfinalcontrato from contrato.contrato
		where idcontrato in (select idcontrato from contrato.contrato
		where fk_idcontrato= @IdContrato and convenioAdhesion =1)
		
	  
END