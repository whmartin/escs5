USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AmparosGarantiass_Consultar]    Script Date: 01/07/2014 04:17:14 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AmparosGarantiass_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantiass_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AmparosGarantiass_Consultar]    Script Date: 01/07/2014 04:17:14 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  5/30/2014 2:33:59 PM
-- Description:	Procedimiento almacenado que consulta un(a) AmparosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantiass_Consultar]
	@FechaVigenciaDesde DATETIME = NULL,
	@FechaVigenciaHasta DATETIME = NULL,
	@ValorCalculoAsegurado NUMERIC(8,2) = NULL,
	@ValorAsegurado NUMERIC(32,8) = NULL,
	@IDGarantia INT = NULL
AS
BEGIN
 SELECT ampg.IDAmparosGarantias, ampg.IDGarantia, ampg.IdTipoAmparo,tiam.NombreTipoAmparo,ampg.FechaVigenciaDesde, ampg.FechaVigenciaHasta, 
 ampg.IDUnidadCalculo, uncal.Descripcion as NombreUnidadCalculo, ampg.ValorCalculoAsegurado, ampg.ValorAsegurado, ampg.UsuarioCrea, ampg.FechaCrea, ampg.UsuarioModifica,
 ampg.FechaModifica 
 FROM [Contrato].[AmparosGarantias] ampg
 INNER JOIN CONTRATO.TipoAmparo tiam ON ampg.IdTipoAmparo = tiam.IdTipoAmparo
 INNER JOIN CONTRATO.UnidadCalculo uncal ON ampg.IDUnidadCalculo = uncal.IDUnidadCalculo
 WHERE ampg.FechaVigenciaDesde = CASE WHEN @FechaVigenciaDesde IS NULL THEN ampg.FechaVigenciaDesde ELSE @FechaVigenciaDesde END 
 AND ampg.FechaVigenciaHasta = CASE WHEN @FechaVigenciaHasta IS NULL THEN ampg.FechaVigenciaHasta ELSE @FechaVigenciaHasta END 
 AND ampg.ValorCalculoAsegurado = CASE WHEN @ValorCalculoAsegurado IS NULL THEN ampg.ValorCalculoAsegurado ELSE @ValorCalculoAsegurado END 
 AND ampg.ValorAsegurado = CASE WHEN @ValorAsegurado IS NULL THEN ampg.ValorAsegurado ELSE @ValorAsegurado END
 AND ampg.IDGarantia = CASE WHEN @IDGarantia IS NULL THEN ampg.IDGarantia ELSE @IDGarantia END
END



