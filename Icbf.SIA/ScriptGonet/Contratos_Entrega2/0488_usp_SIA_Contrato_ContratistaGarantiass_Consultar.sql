USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ContratistaGarantiass_Consultar]    Script Date: 16/07/2014 03:19:34 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ContratistaGarantiass_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantiass_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ContratistaGarantiass_Consultar]    Script Date: 16/07/2014 03:19:34 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  6/20/2014 2:25:34 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContratistaGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantiass_Consultar]
	@IDGarantia INT = NULL,@IDEntidadProvOferente INT = NULL
AS
BEGIN
 SELECT DISTINCT * FROM (
  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador,EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor, EP.IdTipoSector, EP.TipoEntOfProv, T.DIGITOVERIFICACION,
		contgar.IDGarantia, contgar.IDEntidadProvOferente, contgar.IDContratista_Garantias, contgar.UsuarioCrea, contgar.FechaCrea, contgar.UsuarioModifica, contgar.FechaModifica
  FROM [Proveedor].[EntidadProvOferente] EP 
     INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
	 INNER JOIN PROVEEDOR.EntidadProvOferente Entprovof ON Entprovof.IdTercero = T.IDTERCERO
	 INNER JOIN [Contrato].[ContratistaGarantias] contgar ON contgar.IDEntidadProvOferente = Entprovof.IdEntidad
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
     WHERE T.IdTipoPersona=1
UNION 
SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, t.RAZONSOCIAL as Razonsocila,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador,EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor, EP.IdTipoSector, EP.TipoEntOfProv, T.DIGITOVERIFICACION,
		contgar.IDGarantia, contgar.IDEntidadProvOferente, contgar.IDContratista_Garantias, contgar.UsuarioCrea, contgar.FechaCrea, contgar.UsuarioModifica, contgar.FechaModifica
  FROM [Proveedor].[EntidadProvOferente] EP 
       INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
	 INNER JOIN PROVEEDOR.EntidadProvOferente Entprovof ON Entprovof.IdTercero = T.IDTERCERO
	 INNER JOIN [Contrato].[ContratistaGarantias] contgar ON contgar.IDEntidadProvOferente = Entprovof.IdEntidad
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
    WHERE T.IdTipoPersona=2 OR T.IdTipoPersona=3 OR T.IdTipoPersona=4
    ) DT

  WHERE IDGarantia = CASE WHEN @IDGarantia IS NULL THEN IDGarantia ELSE @IDGarantia END 
  AND IDEntidadProvOferente = CASE WHEN @IDEntidadProvOferente IS NULL THEN IDEntidadProvOferente ELSE @IDEntidadProvOferente END
END
