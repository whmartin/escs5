USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasContratoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratoss_Consultar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date: 06/25/2014 14:40:00
-- Description:	Procedimiento almacenado que consulta un(a) PlanComprasContratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratoss_Consultar]
	@NumeroConsecutivoPlanCompras	INT = NULL,
	@Vigencia						INT = NULL,
	@IdContrato						INT = NULL,
	@IDPlanDeCompras				INT = NULL,
	@CodigoRegional					NVARCHAR(16) = NULL
AS
BEGIN
	
	SELECT @CodigoRegional = R.CodigoRegional
	FROM SEG.Usuario U
		INNER JOIN DIV.Regional R ON U.IdRegional = R.IdRegional
	WHERE U.IdUsuario = @CodigoRegional

	--SELECT @CodigoRegional = R.CodigoRegional
	--FROM DIV.Regional R
	--WHERE R.IdRegional = @CodigoRegional

	SELECT PCC.IDPlanDeComprasContratos,
		PCC.Vigencia,
		PCC.IdContrato,
		PCC.IDPlanDeCompras,
		PCC.CodigoRegional,
		PCC.UsuarioCrea,
		PCC.FechaCrea,
		PCC.UsuarioModifica,
		PCC.FechaModifica
	FROM CONTRATO.PlanDeComprasContratos PCC
	WHERE PCC.IDPlanDeCompras = COALESCE(@NumeroConsecutivoPlanCompras,PCC.IDPlanDeCompras)
		AND PCC.Vigencia = COALESCE(@Vigencia,PCC.Vigencia)
		AND PCC.IdContrato = COALESCE(@IdContrato,PCC.IdContrato)
		AND PCC.IDPlanDeCompras = COALESCE(@IDPlanDeCompras,PCC.IDPlanDeCompras)
		AND PCC.CodigoRegional = COALESCE(@CodigoRegional,PCC.CodigoRegional)
END

