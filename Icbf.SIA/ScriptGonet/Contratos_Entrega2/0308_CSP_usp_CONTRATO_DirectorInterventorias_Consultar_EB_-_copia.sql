USE [SIA]
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_DirectorInterventorias_Consultar]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_DirectorInterventorias_Consultar]
GO

-- =============================================
-- Author:		
-- Create date:  7/1/2014 11:19:06 AM
-- Description:	Procedimiento almacenado que consulta un(a) DirectorInterventoria
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_DirectorInterventorias_Consultar]
    @IdTipoIdentificacion INT = NULL ,
    @NumeroIdentificacion NVARCHAR(24) = NULL ,
    @PrimerNombre NVARCHAR(80) = NULL ,
    @SegundoNombre NVARCHAR(80) = NULL ,
    @PrimerApellido NVARCHAR(80) = NULL ,
    @SegundoApellido NVARCHAR(80) = NULL ,
    @Celular NVARCHAR(24) = NULL ,
	@Telefono NVARCHAR(24) = NULL ,
    @CorreoElectronico NVARCHAR(80) = NULL
AS 
    BEGIN
        SELECT  IDDirectorInterventoria ,
                IdTipoIdentificacion ,
                NumeroIdentificacion ,
                PrimerNombre ,
                SegundoNombre ,
                PrimerApellido ,
                SegundoApellido ,
                Celular ,
                Telefono ,
                CorreoElectronico ,
                UsuarioCrea ,
                FechaCrea ,
                UsuarioModifica ,
                FechaModifica
        FROM    [CONTRATO].[DirectorInterventoria]
        WHERE   IdTipoIdentificacion = CASE WHEN @IdTipoIdentificacion IS NULL
                                            THEN IdTipoIdentificacion
                                            ELSE @IdTipoIdentificacion
                                       END
                AND NumeroIdentificacion = CASE WHEN @NumeroIdentificacion IS NULL
                                                THEN NumeroIdentificacion
                                                ELSE @NumeroIdentificacion
                                           END
                AND PrimerNombre = CASE WHEN @PrimerNombre IS NULL
                                        THEN PrimerNombre
                                        ELSE @PrimerNombre
                                   END
                AND SegundoNombre = CASE WHEN @SegundoNombre IS NULL
                                         THEN SegundoNombre
                                         ELSE @SegundoNombre
                                    END
                AND PrimerApellido = CASE WHEN @PrimerApellido IS NULL
                                          THEN PrimerApellido
                                          ELSE @PrimerApellido
                                     END
                AND SegundoApellido = CASE WHEN @SegundoApellido IS NULL
                                           THEN SegundoApellido
                                           ELSE @SegundoApellido
                                      END
                AND Celular = CASE WHEN @Celular IS NULL THEN Celular
                                   ELSE @Celular
                              END
                AND Telefono = CASE WHEN @Telefono IS NULL THEN Telefono
                                    ELSE @Telefono
                               END
                AND CorreoElectronico = CASE WHEN @CorreoElectronico IS NULL
                                             THEN CorreoElectronico
                                             ELSE @CorreoElectronico
                                        END
    END

