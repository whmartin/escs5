USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Alterar tabla CONTRATO.Contrato
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[Contrato]') AND type in (N'U'))
BEGIN

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdRegionalContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdRegionalContrato] [int] NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdTipoContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdTipoContrato] [int] NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdEstadoContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdEstadoContrato] [int] NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'ObjetoDelContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [ObjetoDelContrato] [nvarchar](800) NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'FechaInicioEjecucion')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [FechaInicioEjecucion] [datetime] NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'FechaFinalTerminacionContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [FechaFinalTerminacionContrato] [datetime] NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'FechaFinalizacionIniciaContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [FechaFinalizacionIniciaContrato] [datetime] NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'FechaSuscripcionContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [FechaSuscripcionContrato] [datetime] NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'NumeroContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [NumeroContrato] [nvarchar](50) NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'ValorInicialContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [ValorInicialContrato] [numeric](18, 0) NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdVigenciaInicial')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdVigenciaInicial] [int] NULL

		ALTER TABLE CONTRATO.Contrato
		WITH CHECK ADD  CONSTRAINT [FK_Global_Vigencia_Inicial] FOREIGN KEY (IdVigenciaInicial)
		REFERENCES Global.Vigencia(IdVigencia)
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdVigenciaFinal')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdVigenciaFinal] [int] NULL

		ALTER TABLE CONTRATO.Contrato
		WITH CHECK ADD  CONSTRAINT [FK_Global_Vigencia_Final] FOREIGN KEY (IdVigenciaFinal)
		REFERENCES Global.Vigencia(IdVigencia)
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdModalidadAcademica')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdModalidadAcademica] [nvarchar](30) NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdProfesion')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdProfesion] [nvarchar](30) NULL
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdModalidadSeleccion')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdModalidadSeleccion] [int] NULL

		ALTER TABLE CONTRATO.Contrato
		WITH CHECK ADD  CONSTRAINT [FK_Contrato_ModalidadSeleccion] FOREIGN KEY (IdModalidadSeleccion)
		REFERENCES CONTRATO.ModalidadSeleccion(IdModalidad)
	END

	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdCategoriaContrato')
	BEGIN 
		ALTER TABLE CONTRATO.Contrato
		ADD [IdCategoriaContrato] [int] NULL

		ALTER TABLE CONTRATO.Contrato
		WITH CHECK ADD  CONSTRAINT [FK_Contrato_CategoriaContrato] FOREIGN KEY (IdCategoriaContrato)
		REFERENCES CONTRATO.CategoriaContrato(IdCategoriaContrato)
	END

END






