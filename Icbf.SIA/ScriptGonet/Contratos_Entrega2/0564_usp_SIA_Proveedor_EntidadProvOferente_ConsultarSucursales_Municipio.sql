USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Proveedor_EntidadProvOferente_ConsultarSucursales_Municipio]    Script Date: 06/08/2014 04:42:30 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Proveedor_EntidadProvOferente_ConsultarSucursales_Municipio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Proveedor_EntidadProvOferente_ConsultarSucursales_Municipio]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Proveedor_EntidadProvOferente_ConsultarSucursales_Municipio]    Script Date: 06/08/2014 04:42:30 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Carlos Cardenas
-- Create date:  18/06/2014 11:33:55 AM
-- Description:	Procedimiento almacenado que consulta las sucursales asociadas a un tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_EntidadProvOferente_ConsultarSucursales_Municipio]
	@IdTercero INT = NULL,
	@IdDepartamento INT = NULL,
	@IdMunicipio INT = NULL
AS
BEGIN
 SELECT DISTINCT
	 -- Proveedor.Sucursal.IdSucursal,
	   --DIV.Departamento.IdDepartamento,
      --DIV.Departamento.NombreDepartamento
	  DIV.Municipio.IdMunicipio,
     DIV.Municipio.NombreMunicipio
   --   Proveedor.Sucursal.Nombre,
   --   Proveedor.Sucursal.Direccion,
   --   Proveedor.Sucursal.Correo,
   --   Proveedor.Sucursal.Indicativo,
   --   Proveedor.Sucursal.Telefono,
   --   Proveedor.Sucursal.Extension,
   --   Proveedor.Sucursal.Celular
FROM Oferente.TERCERO
INNER JOIN Proveedor.EntidadProvOferente
      ON Oferente.TERCERO.IDTERCERO = Proveedor.EntidadProvOferente.IdTercero
INNER JOIN Proveedor.Sucursal
      ON Proveedor.EntidadProvOferente.IdEntidad = Proveedor.Sucursal.IdEntidad
INNER JOIN DIV.Departamento 
	  ON PROVEEDOR.Sucursal.Departamento = DIV.Departamento.IdDepartamento 
INNER JOIN DIV.Municipio 
	  ON PROVEEDOR.Sucursal.Municipio = DIV.Municipio.IdMunicipio 
  
WHERE Proveedor.EntidadProvOferente.IdTercero = CASE WHEN @IdTercero IS NULL THEN Proveedor.EntidadProvOferente.IdTercero ELSE @IdTercero END 
	   AND DIV.Departamento.IdDepartamento =  CASE WHEN @IdDepartamento IS NULL THEN DIV.Departamento.IdDepartamento ELSE @IdDepartamento END 	 
	   AND DIV.Municipio.IdMunicipio =  CASE WHEN @IdMunicipio IS NULL THEN DIV.Municipio.IdMunicipio ELSE @IdMunicipio END 	 
END
