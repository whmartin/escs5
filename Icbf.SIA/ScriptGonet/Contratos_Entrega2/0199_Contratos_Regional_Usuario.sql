USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_Contratos_Regional_Usuario]    Script Date: 13/06/2014 09:56:45 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Contratos_Regional_Usuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Contratos_Regional_Usuario]
GO

/****** Object:  StoredProcedure [dbo].[usp_Contratos_Regional_Usuario]    Script Date: 13/06/2014 09:56:45 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet\Carlos Cardenas
-- Description:	Procedimiento almacenado que consulta las regionales asociadas al usuario

-- =============================================
CREATE PROCEDURE [dbo].[usp_Contratos_Regional_Usuario]
		@IdUsuario INT
	AS
	BEGIN
	
		 DECLARE @IdTipoUsuario INT
		 DECLARE @IdRegionalUsuario INT
		 --DECLARE @IdEntidadContratistaUsuario INT
		 DECLARE @IdRegional INT = NULL
		-- DECLARE @IdEntidadContratista INT = NULL

		 SELECT @IdRegionalUsuario=IdRegional, @IdTipoUsuario=IdTipoUsuario FROM SEG.Usuario WHERE IdUsuario=@IdUsuario --@IdEntidadContratistaUsuario=IdEntidadContratista,
		 IF @IdTipoUsuario=2 BEGIN SET @IdRegional=@IdRegionalUsuario END
		-- IF @IdTipoUsuario=3 BEGIN SET @IdEntidadContratista=@IdEntidadContratistaUsuario END
		 
		 --IF @IdEntidadContratista IS NOT NULL
		 --BEGIN
		 --	select distinct reg.idRegional, reg.NombreRegional
		 --	from div.Regional reg 
		 --	join con.Contrato cont on reg.IdRegional=cont.IdRegional
		 --	where cont.IdEntidadContratista=@IdEntidadContratista
			--order by reg.NombreRegional desc
		 --END
		 --ELSE
		 --BEGIN
			IF @IdRegional IS NOT NULL
			BEGIN
				select reg.idRegional, reg.NombreRegional
				from div.Regional reg 
				where reg.IdRegional=@IdRegional
				order by reg.NombreRegional desc
			END
			ELSE
			BEGIN
				select reg.idRegional, reg.NombreRegional
				from div.Regional reg
				order by reg.NombreRegional desc
			END
		 --END
	END

