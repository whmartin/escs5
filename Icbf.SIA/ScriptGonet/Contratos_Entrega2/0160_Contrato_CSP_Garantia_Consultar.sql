USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_Garantia_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/4/2014 9:59:59 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Consultar]
	@IDGarantia INT
AS
BEGIN
 SELECT IDGarantia, IDTipoGarantia, NumeroGarantia, FechaAprobacionGarantia, 
 FechaCertificacionGarantia, FechaDevolucion, MotivoDevolucion, IDUsuarioAprobacion, IDUsuarioDevolucion,
  IdContrato, BeneficiarioICBF,BeneficiarioOTROS, DescripcionBeneficiarios, FechaInicioGarantia, FechaExpedicionGarantia, 
  FechaVencimientoInicialGarantia, FechaVencimientoFinalGarantia, FechaReciboGarantia, ValorGarantia, 
  Anexos, ObservacionesAnexos, EntidadProvOferenteAseguradora, UsuarioCrea, FechaCrea, UsuarioModifica, 
  FechaModifica,Estado, IDSucursalAseguradoraContrato,IDEstadosGarantias FROM [Contrato].[Garantia] WHERE  IDGarantia = @IDGarantia
END

