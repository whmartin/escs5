USE [SIA]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Eliminacion Tabla [CONTRATO].[TipoSupvInterventor]
-- =============================================

/****** Object:  Table [CONTRATO].[TipoSupvInterventor]    Script Date: 23/05/2014 15:37:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[TipoSupvInterventor]') AND type in (N'U'))
DROP TABLE [CONTRATO].[TipoSupvInterventor]
GO