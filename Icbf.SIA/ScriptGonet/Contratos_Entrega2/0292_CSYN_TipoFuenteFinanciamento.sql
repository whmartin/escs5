USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion sinonimo [BaseSIF].[TipoFuenteFinanciamento]
-- =============================================

/****** Object:  Synonym [BaseSIF].[TipoFuenteFinanciamento]    Script Date: 01/07/2014 16:35:59 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoFuenteFinanciamento' AND schema_id = SCHEMA_ID(N'BaseSIF'))
DROP SYNONYM [BaseSIF].[TipoFuenteFinanciamento]
GO

/****** Object:  Synonym [BaseSIF].[TipoFuenteFinanciamento]    Script Date: 01/07/2014 16:35:59 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoFuenteFinanciamento' AND schema_id = SCHEMA_ID(N'BaseSIF'))
CREATE SYNONYM [BaseSIF].[TipoFuenteFinanciamento] FOR [NMF].[BaseSIF].[TipoFuenteFinanciamento]
GO


