USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]    Script Date: 09/11/2014 09:50:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]    Script Date: 09/11/2014 09:50:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  6/20/2014 2:25:33 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ContratistaGarantias
-- =============================================
-- =============================================
-- Author:		Emilio Calapi�a
-- Create date:  11/09/2014 09:52 AM
-- Description:	Se ajusta para que cuando exista el contratista devuelva su id para evitar errores de null en
-- la aplicaci�n
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]
		@IDContratista_Garantias INT OUTPUT, 	@IDGarantia INT,	@IDEntidadProvOferente INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN

	IF NOT EXISTS (Select IDContratista_Garantias from Contrato.ContratistaGarantias where IDGarantia = @IDGarantia AND IDEntidadProvOferente = @IDEntidadProvOferente)
	BEGIN
		INSERT INTO Contrato.ContratistaGarantias(IDGarantia, IDEntidadProvOferente, UsuarioCrea, FechaCrea)
						  VALUES(@IDGarantia, @IDEntidadProvOferente, @UsuarioCrea, GETDATE())
		SELECT @IDContratista_Garantias = @@IDENTITY
	END	
	ELSE
	BEGIN
		Select @IDContratista_Garantias=IDContratista_Garantias from Contrato.ContratistaGarantias where IDGarantia = @IDGarantia AND IDEntidadProvOferente = @IDEntidadProvOferente
	END
END

GO


