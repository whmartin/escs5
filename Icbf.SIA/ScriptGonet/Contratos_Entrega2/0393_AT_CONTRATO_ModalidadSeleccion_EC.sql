USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Crear tabla [CONTRATO].[ModalidadSeleccion]
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ModalidadSeleccion]') AND type in (N'U'))
BEGIN

	IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.ModalidadSeleccion') AND NAME = ('CodigoModalidad'))
	BEGIN
		ALTER TABLE CONTRATO.ModalidadSeleccion
		ADD CodigoModalidad nvarchar(30) NULL;
	END

END
ELSE
BEGIN

	CREATE TABLE [CONTRATO].[ModalidadSeleccion](
	[IdModalidad] [int] IDENTITY(1,1) NOT NULL,
	[CodigoModalidad] [nvarchar](30) NULL,
	[Nombre] [nvarchar](128) NOT NULL,
	[Sigla] [nvarchar](5) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	 CONSTRAINT [PK_ModalidadSeleccion] PRIMARY KEY CLUSTERED 
	(
		[IdModalidad] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	 CONSTRAINT [UQ_NombreModalidadSeleccion] UNIQUE NONCLUSTERED 
	(
		[Nombre] ASC,
		[Sigla] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key {0} Column' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'ModalidadSeleccion', @level2type=N'COLUMN',@level2name=N'IdModalidad'

END
