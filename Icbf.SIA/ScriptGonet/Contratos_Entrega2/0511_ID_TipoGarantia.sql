use SIA
Go

/***************************************************
2014-07-24
Gonet-jorge vizcaino
Crecion de los tipo de garantias
***************************************************/

IF not exists (select 1 from CONTRATO.TipoGarantia where NombreTipoGarantia = 'Contrato de Seguro (P�liza)')
BEGIN
insert into  CONTRATO.TipoGarantia (NombreTipoGarantia,
									Estado,
									UsuarioCrea,
									FechaCrea,
									UsuarioModifica,
									FechaModifica) VALUES ('Contrato de Seguro (P�liza)',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
end

IF not exists (select 1 from CONTRATO.TipoGarantia where NombreTipoGarantia = 'Patrimonio Aut�nomo')
BEGIN
insert into  CONTRATO.TipoGarantia (NombreTipoGarantia,
									Estado,
									UsuarioCrea,
									FechaCrea,
									UsuarioModifica,
									FechaModifica) VALUES ('Patrimonio Aut�nomo',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
end

IF not exists (select 1 from CONTRATO.TipoGarantia where NombreTipoGarantia = 'Garantia Bancaria')
BEGIN
insert into  CONTRATO.TipoGarantia (NombreTipoGarantia,
									Estado,
									UsuarioCrea,
									FechaCrea,
									UsuarioModifica,
									FechaModifica) VALUES ('Garantia Bancaria',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
end

IF not exists (select 1 from CONTRATO.TipoGarantia where NombreTipoGarantia = 'Endoso de Garantia en Titulos Valores')
BEGIN
insert into  CONTRATO.TipoGarantia (NombreTipoGarantia,
									Estado,
									UsuarioCrea,
									FechaCrea,
									UsuarioModifica,
									FechaModifica) VALUES ('Endoso de Garantia en Titulos Valores',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
end



