USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Crear tabla [CONTRATO].[TipoContrato]
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[TipoContrato]') AND type in (N'U'))
BEGIN

	IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.TipoContrato') AND NAME = ('CodigoTipoContrato'))
	BEGIN
		ALTER TABLE CONTRATO.TipoContrato
		ADD CodigoTipoContrato nvarchar(30) NULL;
	END

	

END
ELSE
BEGIN

	CREATE TABLE [CONTRATO].[TipoContrato](
	[IdTipoContrato] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoContrato] [nvarchar](30) NULL,
	[NombreTipoContrato] [nvarchar](128) NOT NULL,
	[IdCategoriaContrato] [int] NOT NULL,
	[ActaInicio] [bit] NOT NULL,
	[AporteCofinaciacion] [bit] NOT NULL,
	[RecursoFinanciero] [bit] NOT NULL,
	[RegimenContrato] [int] NOT NULL,
	[DescripcionTipoContrato] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
		CONSTRAINT [PK_TipoContrato] PRIMARY KEY CLUSTERED 
	(
		[IdTipoContrato] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [UQ_NombreTipoContrato] UNIQUE NONCLUSTERED 
	(
		[NombreTipoContrato] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [CONTRATO].[TipoContrato]  WITH CHECK ADD  CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI] FOREIGN KEY([IdCategoriaContrato])
	REFERENCES [CONTRATO].[CategoriaContrato] ([IdCategoriaContrato])


	ALTER TABLE [CONTRATO].[TipoContrato] CHECK CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key {0} Column' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'TipoContrato', @level2type=N'COLUMN',@level2name=N'IdTipoContrato'


	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to CategoriaContrato table' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'TipoContrato', @level2type=N'COLUMN',@level2name=N'IdCategoriaContrato'



END
