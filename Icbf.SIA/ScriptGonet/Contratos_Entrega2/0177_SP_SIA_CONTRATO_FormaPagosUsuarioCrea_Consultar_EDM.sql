USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_FormaPagos_Consultar]    Script Date: 06/06/2014 05:30:00 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_FormaPagosUsuarioCrea_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPagosUsuarioCrea_Consultar]
GO


-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  5/29/2014 7:56:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) FormaPago por el usuario crea
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPagosUsuarioCrea_Consultar]
	@IdProveedores INT = NULL,@IdMedioPago INT = NULL,@IdEntidadFinanciera INT = NULL,
	@TipoCuentaBancaria INT = NULL,@NumeroCuentaBancaria NVARCHAR(80) = NULL, @UsuarioCrea varchar(250)
AS
BEGIN
 SELECT FP.IdFormaPago, FP.IdProveedores, FP.IdMedioPago, 
 FP.IdEntidadFinanciera, FP.TipoCuentaBancaria, FP.NumeroCuentaBancaria, 
 FP.UsuarioCrea, FP.FechaCrea, FP.UsuarioModifica, FP.FechaModifica
 ,TCEF.NombreTipoCta
 ,EF.NombreEntFin
 ,TMP.DescTipoMedioPago
 FROM [CONTRATO].[FormaPago] FP
 LEFT OUTER JOIN [BaseSIF].[TipoCuentaEntFin] TCEF on TCEF.IdTipoCta = FP.TipoCuentaBancaria
 LEFT OUTER JOIN [BaseSIF].[EntidadFinanciera] EF on EF.IdentidadFinanciera = FP.IdEntidadFinanciera
 INNER JOIN [Ppto].[TipoMedioPago]  TMP on TMP.IdTipoMedioPago = FP.IdMedioPago
  WHERE IdProveedores = CASE WHEN @IdProveedores IS NULL THEN IdProveedores ELSE @IdProveedores END 
 AND FP.IdMedioPago = CASE WHEN @IdMedioPago IS NULL THEN FP.IdMedioPago ELSE @IdMedioPago END 
 AND FP.IdEntidadFinanciera = CASE WHEN @IdEntidadFinanciera IS NULL THEN FP.IdEntidadFinanciera ELSE @IdEntidadFinanciera END 
 AND FP.TipoCuentaBancaria = CASE WHEN @TipoCuentaBancaria IS NULL THEN FP.TipoCuentaBancaria ELSE @TipoCuentaBancaria END 
 AND FP.NumeroCuentaBancaria = CASE WHEN @NumeroCuentaBancaria IS NULL THEN FP.NumeroCuentaBancaria ELSE @NumeroCuentaBancaria END
 AND FP.UsuarioCrea = CASE WHEN @UsuarioCrea IS NULL THEN FP.UsuarioCrea ELSE @UsuarioCrea END 
END




