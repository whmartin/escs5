USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Modificar]
GO
-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/14/2014 4:20:50 PM
-- Description:	Procedimiento almacenado que actualiza un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Modificar]
		@IdLugarEjecucionContratos INT,	@IdContrato INT,	@IdDepartamento INT,	@IdRegional INT,	@NivelNacional BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE CONTRATO.LugarEjecucionContrato SET IdContrato = @IdContrato, IdDepartamento = @IdDepartamento, IdRegional = @IdRegional, NivelNacional = @NivelNacional, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdLugarEjecucionContratos = @IdLugarEjecucionContratos
END
