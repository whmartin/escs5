USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]    Script Date: 25/07/2014 10:34:46 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date: 07/07/2014 13:24:00
-- Description:	Procedimiento almacenado que guarda un nuevo DetallePlanComprasContratos
-- =============================================
ALTER PROCEDURE [dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]
	@NumeroConsecutivoPlanCompras	INT,	
	@Vigencia						INT,
	@IdContrato						INT,	
	@IdUsuario						INT,
	@UsuarioCrea					NVARCHAR(225),
	@ProductoPlanCompraContratos	ProductoPlanCompraContratos READONLY,
	@RubrosPlanCompraContratos		RubrosPlanCompraContratos READONLY
AS
BEGIN
	DECLARE @CodRegional				NVARCHAR(16)
			,@IDPlanDeComprasContratos	INT

	SELECT @CodRegional = R.CodigoRegional
	FROM DIV.Regional R
		INNER JOIN SEG.Usuario U ON R.IdRegional = U.IdRegional
	WHERE U.IdUsuario = @IdUsuario

	IF EXISTS(SELECT * FROM Contrato.PlanDeComprasContratos WHERE IDPlanDeCompras = @NumeroConsecutivoPlanCompras)
	BEGIN
		RAISERROR('Ya existe un registro con este número consecutivo plan compras',16,1)
	    RETURN
	END

	INSERT INTO Contrato.PlanDeComprasContratos(IdContrato, IDPlanDeCompras, Vigencia, CodigoRegional, UsuarioCrea, FechaCrea)
	VALUES(@IdContrato, @NumeroConsecutivoPlanCompras, @Vigencia, @CodRegional, @UsuarioCrea, GETDATE())

	SELECT @IDPlanDeComprasContratos = SCOPE_IDENTITY();

	INSERT INTO CONTRATO.ProductoPlanCompraContratos(IDProducto, CantidadCupos, IDPlanDeComprasContratos, UsuarioCrea, FechaCrea)
	SELECT CodigoProducto
		  ,CONVERT(NUMERIC(18,2),CantidadCupos)
		  ,@IDPlanDeComprasContratos
		  ,@UsuarioCrea
		  ,GETDATE()
	FROM @ProductoPlanCompraContratos
		
	INSERT INTO CONTRATO.RubroPlanComprasContrato(ValorRubroPresupuestal, IDPlanDeComprasContratos, IDRubro, UsuarioCrea, FechaCrea)
	SELECT ValorRubroPresupuestal
		  ,@IDPlanDeComprasContratos
		  ,CodigoRubro
		  ,@UsuarioCrea
		  ,GETDATE()
	FROM @RubrosPlanCompraContratos

END
