USE [SIA]
GO


IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_DIV_RegionalPCI_sConsultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_DIV_RegionalPCI_sConsultar]
GO

-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Description:	Procedimiento almacenado que consulta las Regionales PCI
-- Modificacion: 2014/05/23  1046
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_RegionalPCI_sConsultar]
	@CodigoRegional NVARCHAR(2) = NULL,@NombreRegional NVARCHAR(256) = NULL
AS
BEGIN
	SELECT
		R.IdRegional,
		RPCI.CodRegionalICBF CodigoRegional,
		(RPCI.CodRegionalICBF + ' - ' + UPPER(RPCI.DescripcionPCI)) AS NombreRegional,
		RPCI.CodRegionalPCI,
		RPCI.UsuarioCrea,
		RPCI.FechaCrea,
		RPCI.UsuarioModifica,
		RPCI.FechaModifica
	FROM [Ppto].[RegionalesPCI] RPCI
	INNER JOIN [DIV].[Regional] R
		ON R.CodigoRegional COLLATE SQL_Latin1_General_CP1_CI_AS = RPCI.CodRegionalICBF COLLATE SQL_Latin1_General_CP1_CI_AS
	WHERE R.IdRegional =
		CASE
			WHEN @CodigoRegional IS NULL THEN R.IdRegional ELSE @CodigoRegional
		END
	AND DescripcionPCI =
		CASE
			WHEN @NombreRegional IS NULL THEN DescripcionPCI ELSE @NombreRegional
		END
	ORDER BY NombreRegional
END




