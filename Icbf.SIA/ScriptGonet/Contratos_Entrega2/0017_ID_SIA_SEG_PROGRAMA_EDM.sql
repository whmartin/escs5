USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  12/05/2014 12:20 AM
-- Description:	Insertar Tabla programa para visualizar en el men� de Contratos la opci�n de listas param�tricas
-- =============================================
IF not exists(SELECT * from [SEG].[Programa] where [NombrePrograma] = N'N�mero de proceso' and [CodigoPrograma] = N'Contratos/NumeroProcesos')
Begin
	IF exists(SELECT * from SEG.Modulo where [NombreModulo] = 'Contratos')
	Begin

		declare @IdModulo int = null

			select @IdModulo = IdModulo from SEG.Modulo where [NombreModulo] = 'Contratos'

				INSERT [SEG].[Programa] ([IdModulo], [NombrePrograma], [CodigoPrograma], [Posicion], [Estado], 
				[UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion], [VisibleMenu], [generaLog], [Padre]) 
				VALUES (@IdModulo, N'N�mero de proceso', N'Contratos/NumeroProcesos', 2, 1, N'Administrador', GETDATE(), NULL, 
				NULL, 0, 1, NULL)

	end
End