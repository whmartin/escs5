USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/27/2014 6:43:39 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ConsecutivoContratoRegionales
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar]
		@IDConsecutivoContratoRegional INT OUTPUT, 	@IdRegional INT,	@IdVigencia INT,	@Consecutivo NUMERIC(30), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO CONTRATO.ConsecutivoContratoRegionales(IdRegional, IdVigencia, Consecutivo, UsuarioCrea, FechaCrea)
					  VALUES(@IdRegional, @IdVigencia, @Consecutivo, @UsuarioCrea, GETDATE())
	SELECT @IDConsecutivoContratoRegional = @@IDENTITY 		
END
