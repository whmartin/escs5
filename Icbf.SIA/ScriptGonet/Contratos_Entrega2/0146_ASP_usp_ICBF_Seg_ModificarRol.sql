USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ModificarRol]    Script Date: 03/06/2014 10:43:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ModificarRol]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ICBF_Seg_ModificarRol]
GO

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que modifica información de un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ModificarRol]
		@pIdRol INT
		,@ProviderKey VARCHAR(125)
		, @Nombre NVARCHAR(20)
		, @Descripcion NVARCHAR(200)
		, @Estado BIT
		, @UsuarioModificacion NVARCHAR(250)
		, @EsAdministrador BIT = 0
AS
BEGIN
	UPDATE SEG.Rol
	SET providerKey = @ProviderKey
		, [Nombre]=@Nombre
		, [Descripcion]=@Descripcion
		, [Estado]=@Estado
		, [UsuarioModificacion]=@UsuarioModificacion
		, FechaModificacion=GETDATE()
		, EsAdministrador = @EsAdministrador
	WHERE IdRol=@pIdRol
END

