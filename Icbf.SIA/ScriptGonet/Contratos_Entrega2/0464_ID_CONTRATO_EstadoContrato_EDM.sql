USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  14/07/2014 10:38 
-- Description:	Insert tabla [CONTRATO].[EstadoContrato].
-- =============================================
IF not exists(SELECT [CodEstado] from [CONTRATO].[EstadoContrato] where [Descripcion] = 'Contrato con RP')

Begin

INSERT INTO [CONTRATO].[EstadoContrato]
           ([CodEstado]
           ,[Descripcion]
           ,[Inactivo]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('CRP'
           ,'Contrato con CDP'
           ,1
           ,'Administrador'
           ,GetDate()
           ,null
           ,null)
END


