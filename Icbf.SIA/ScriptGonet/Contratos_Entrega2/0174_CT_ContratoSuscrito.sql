use SIA

-- =============================================
-- Author:		Gonet
-- Create date: 2014-07-09
-- Description:	Alterar tabla [Contrato].[ContratoSuscrito]
-- =============================================

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ContratoSuscrito]') AND type in (N'U'))
BEGIN


CREATE TABLE [Contrato].[ContratoSuscrito]
(
 [IDContratoSuscrito] Int NOT NULL,
 [FechaSuscripcion] Datetime NOT NULL,
 [IdContrato] Int NOT NULL,
 [IDUsuario] Int NOT NULL,
 [Consecutivo] Nvarchar(80) NOT NULL,
 [UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL
)
end
go




-- Create indexes for table Contrato.ContratoSuscrito

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[IX_Contrato_IDContrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ContratoSuscrito]'))
CREATE UNIQUE INDEX [IX_Contrato_IDContrato] ON [Contrato].[ContratoSuscrito] ([IdContrato])
go

-- Add keys for table Contrato.ContratoSuscrito

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[PK_ContratoSuscrito_ID]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ContratoSuscrito]'))
ALTER TABLE [Contrato].[ContratoSuscrito] ADD CONSTRAINT [PK_ContratoSuscrito_ID] PRIMARY KEY ([IDContratoSuscrito])
go