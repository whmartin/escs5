USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuario]    Script Date: 23/05/2014 10:35:09 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ConsultarUsuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuario]
GO


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta planes de acci�n
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 

-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Description:	Se acreg� la columna IdRegional a la consulta
-- Modificacion: 2014/05/23 

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuario]
	@IdUsuario int
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
      ,SEG.Usuario.IdTipoUsuario
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
	  ,SEG.Usuario.IdRegional
  FROM SEG.Usuario 
  WHERE
	IdUsuario = @IdUsuario
END



