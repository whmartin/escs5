USE [SIA]
GO

-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  25/06/2014 14:54 PM
-- Description: Tabla de registro de documentos asociados a un contrato suscrito
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ArchivoContrato_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ArchivosContratos]'))
ALTER TABLE [CONTRATO].[ArchivosContratos] DROP CONSTRAINT [FK_ArchivoContrato_Contrato]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ArchivoContrato_Archivo]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ArchivosContratos]'))
ALTER TABLE [CONTRATO].[ArchivosContratos] DROP CONSTRAINT [FK_ArchivoContrato_Archivo]
GO

/****** Object:  Table [CONTRATO].[ArchivosContratos]    Script Date: 08/07/2014 9:59:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ArchivosContratos]') AND type in (N'U'))
DROP TABLE [CONTRATO].[ArchivosContratos]
GO

/****** Object:  Table [CONTRATO].[ArchivosContratos]    Script Date: 08/07/2014 9:59:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ArchivosContratos]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[ArchivosContratos](
	[IdArchivoContrato] [int] IDENTITY(1,1) NOT NULL,
	[IdArchivo] [numeric](18, 0) NULL,
	[IdContrato] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [ArchivoContrato] PRIMARY KEY CLUSTERED 
(
	[IdArchivoContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ArchivoContrato_Archivo]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ArchivosContratos]'))
ALTER TABLE [CONTRATO].[ArchivosContratos]  WITH CHECK ADD  CONSTRAINT [FK_ArchivoContrato_Archivo] FOREIGN KEY([IdArchivo])
REFERENCES [Estructura].[Archivo] ([IdArchivo])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ArchivoContrato_Archivo]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ArchivosContratos]'))
ALTER TABLE [CONTRATO].[ArchivosContratos] CHECK CONSTRAINT [FK_ArchivoContrato_Archivo]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ArchivoContrato_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ArchivosContratos]'))
ALTER TABLE [CONTRATO].[ArchivosContratos]  WITH CHECK ADD  CONSTRAINT [FK_ArchivoContrato_Contrato] FOREIGN KEY([IdContrato])
REFERENCES [CONTRATO].[Contrato] ([IdContrato])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ArchivoContrato_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ArchivosContratos]'))
ALTER TABLE [CONTRATO].[ArchivosContratos] CHECK CONSTRAINT [FK_ArchivoContrato_Contrato]
GO


