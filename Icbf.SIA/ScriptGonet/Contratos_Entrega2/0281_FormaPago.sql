USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion tabla [CONTRATO].[TipoFormaPago]
-- =============================================

/****** Object:  Table [CONTRATO].[TipoFormaPago]    Script Date: 23/05/2014 15:44:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[TipoFormaPago]') AND type in (N'U'))
DROP TABLE [CONTRATO].[TipoFormaPago]
GO

/****** Object:  Table [CONTRATO].[TipoFormaPago]    Script Date: 23/05/2014 15:44:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[TipoFormaPago]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[TipoFormaPago](
	[IdTipoFormaPago] [int] IDENTITY(1,1) NOT NULL,
	[CodigoTipoFormaPago] [varchar](8) NOT NULL,
	[NombreTipoFormaPago] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](128) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_TipoFormaPago] PRIMARY KEY CLUSTERED 
(
	[IdTipoFormaPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NombreTipoFormaPago] UNIQUE NONCLUSTERED 
(
	[NombreTipoFormaPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


