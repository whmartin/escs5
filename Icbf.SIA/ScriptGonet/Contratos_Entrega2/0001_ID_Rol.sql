use SIA

/**************************************
-- Author:	    2014-07-09
-- Create date: gonet
-- Description: Crear Rol Administrador
**************************************/
IF not exists (select 1 from SEG.Rol where Nombre = 'Administrador')
BEGIN
	insert INTO SEG.Rol (providerKey,
	Nombre,
	Descripcion,
	Estado,
	UsuarioCreacion,
	FechaCreacion,
	UsuarioModificacion,
	FechaModificacion)
	values ('','Administrador','Administrador',1,'Administrador',getdate(),null,null)

end

