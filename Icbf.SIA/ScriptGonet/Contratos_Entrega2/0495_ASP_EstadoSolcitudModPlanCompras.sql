use SIA
Go
/*********************************************
2014-07-17
inserta los estado del plan de compras 
*********************************************/

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', NULL,NULL))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaModifica'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaCrea'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'Activo'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Activo'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'Descripcion'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Descripcion'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'CodEstado'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'CodEstado'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'IdEstadoSolicitud'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdEstadoSolicitud'

GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdEstadoSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] DROP CONSTRAINT [FK_SolicitudModPlanCompras_IdEstadoSolicitud]
GO


/****** Object:  Table [CONTRATO].[EstadoSolcitudModPlanCompras]    Script Date: 17/07/2014 16:33:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[EstadoSolcitudModPlanCompras]') AND type in (N'U'))
DROP TABLE [CONTRATO].[EstadoSolcitudModPlanCompras]
GO

/****** Object:  Table [CONTRATO].[EstadoSolcitudModPlanCompras]    Script Date: 17/07/2014 16:33:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[EstadoSolcitudModPlanCompras]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[EstadoSolcitudModPlanCompras](
	[IdEstadoSolicitud] int identity(1,1) NOT NULL,
	[CodEstado] [nvarchar](24) NOT NULL,
	[Descripcion] [nvarchar](160) NOT NULL,
	[Activo] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoSolcitudModPlanCompras] PRIMARY KEY CLUSTERED 
(
	[IdEstadoSolicitud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'IdEstadoSolicitud'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdEstadoSolicitud'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'CodEstado'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CodEstado' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'CodEstado'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'Descripcion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id IdTercero' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'Activo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Activo' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Activo'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificación del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Almacena datos sobre Estado de solicitud de plan de compras' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras'
GO




IF not exists (select 1 from [CONTRATO].[EstadoSolcitudModPlanCompras] where CodEstado = '001' )
BEGIN
	INSERT into [CONTRATO].[EstadoSolcitudModPlanCompras] 
	(CodEstado,
		Descripcion,
		Activo,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica)
	values ('001','Enviado',1,'Administrador',getdate(),NULL,NULL)
END

IF not exists (select 1 from [CONTRATO].[EstadoSolcitudModPlanCompras] where CodEstado = '002' )
BEGIN
	INSERT into [CONTRATO].[EstadoSolcitudModPlanCompras] 
		(CodEstado,
		Descripcion,
		Activo,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica)

	values ('002','Aprobado',1,'Administrador',getdate(),NULL,NULL)
END

IF not exists (select 1 from [CONTRATO].[EstadoSolcitudModPlanCompras] where CodEstado = '003' )
BEGIN
	INSERT into [CONTRATO].[EstadoSolcitudModPlanCompras] 
		(CodEstado,
		Descripcion,
		Activo,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica)

	values ('003','No aprobado',1,'Administrador',getdate(),NULL,NULL)
END

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID(' [CONTRATO].[SolicitudModPlanCompras]') AND NAME = ('IdEstadoSolicitud'))
BEGIN
	ALTER table  [CONTRATO].[SolicitudModPlanCompras] alter column IdEstadoSolicitud int 
END


IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdEstadoSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
	ALTER TABLE [CONTRATO].[SolicitudModPlanCompras]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudModPlanCompras_IdEstadoSolicitud] FOREIGN KEY([IdEstadoSolicitud])
	REFERENCES [CONTRATO].[EstadoSolcitudModPlanCompras] ([IdEstadoSolicitud])
GO
