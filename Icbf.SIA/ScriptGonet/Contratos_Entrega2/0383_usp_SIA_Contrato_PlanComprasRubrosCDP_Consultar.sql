USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Consultar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) PlanComprasRubrosCDP
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Consultar]
	@IDRubroPlanComprasContrato INT
AS
BEGIN
	SELECT RPC.IDRubroPlanComprasContrato,
		 RPC.ValorRubroPresupuestal,
		 RPC.IDPlanDeComprasContratos,
		 RPC.IDRubro,
		 PCC.IDPlanDeCompras NumeroConsecutivoPlanCompras,
		 RPC.UsuarioCrea,
		 RPC.FechaCrea,
		 RPC.UsuarioModifica,
		 RPC.FechaModifica
	FROM Contrato.RubroPlanComprasContrato RPC
		INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON RPC.IDPlanDeComprasContratos = PCC.IDPlanDeComprasContratos
	WHERE RPC.IDRubroPlanComprasContrato = COALESCE(@IDRubroPlanComprasContrato,RPC.IDRubroPlanComprasContrato)
END
