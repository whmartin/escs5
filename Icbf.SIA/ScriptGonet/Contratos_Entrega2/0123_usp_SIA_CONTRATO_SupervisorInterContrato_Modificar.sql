USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Modificar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Modificar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/30/2014 12:23:44 PM
-- Description:	Procedimiento almacenado que actualiza un(a) SupervisorInterContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Modificar]
		@IDSupervisorIntervContrato INT,	@FechaInicio DATETIME,	@Inactivo BIT,	@Identificacion NVARCHAR(80),	@TipoIdentificacion NVARCHAR(4),	@IDTipoSuperInter INT,	@IdNumeroContratoInterventoria INT,	@IDProveedoresInterventor INT,	@IDEmpleadosSupervisor INT,	@IdContrato INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
UPDATE CONTRATO.SupervisorInterContrato
SET	FechaInicio = @FechaInicio,
	Inactivo = @Inactivo,
	Identificacion = @Identificacion,
	TipoIdentificacion = @TipoIdentificacion,
	IDTipoSuperInter = @IDTipoSuperInter,
	IdNumeroContratoInterventoria = @IdNumeroContratoInterventoria,
	IDProveedoresInterventor = @IDProveedoresInterventor,
	IDEmpleadosSupervisor = @IDEmpleadosSupervisor,
	IdContrato = @IdContrato,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IDSupervisorIntervContrato = @IDSupervisorIntervContrato
END
