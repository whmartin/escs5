USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_PlanDeComprasContratoss_Consultar]    Script Date: 25/06/2014 12:25:38 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_PlanDeComprasContratoss_Consultar')
BEGIN
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_PlanDeComprasContratoss_Consultar]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_PlanDeComprasContratoss_Consultar]    Script Date: 25/06/2014 12:25:38 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/23/2014 3:26:50 PM
-- Description:	Procedimiento almacenado que consulta un(a) PlanDeComprasContratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_PlanDeComprasContratoss_Consultar]
	@IdContrato INT = NULL,@IDPlanDeCompras INT = NULL
AS
BEGIN
 SELECT IDPlanDeComprasContratos, IdContrato, IDPlanDeCompras, Vigencia, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [CONTRATO].[PlanDeComprasContratos] 
 WHERE IdContrato = CASE WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato END AND 
 IDPlanDeCompras = CASE WHEN @IDPlanDeCompras IS NULL THEN IDPlanDeCompras ELSE @IDPlanDeCompras END
END

GO


