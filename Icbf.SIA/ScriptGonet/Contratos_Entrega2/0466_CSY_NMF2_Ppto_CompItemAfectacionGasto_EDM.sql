USE [SIA]
GO
-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  14/07/2014 15:38 
-- Description:	Sinonimo de la tabla [Ppto].[CompItemAfectacionGasto] de la base de datos NMF
-- =============================================
/****** Object:  Synonym [Ppto].[CompItemAfectacionGasto]    Script Date: 14/07/2014 03:37:04 p.m. ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CompItemAfectacionGasto' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[CompItemAfectacionGasto]
GO

/****** Object:  Synonym [Ppto].[CompItemAfectacionGasto]    Script Date: 14/07/2014 03:37:04 p.m. ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CompItemAfectacionGasto' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[CompItemAfectacionGasto] FOR [NMF].[Ppto].[CompItemAfectacionGasto]
GO


