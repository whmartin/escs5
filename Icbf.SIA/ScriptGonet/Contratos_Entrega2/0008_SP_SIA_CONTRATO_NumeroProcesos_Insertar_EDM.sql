USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Insertar]    Script Date: 28/05/2014 12:26:52 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_NumeroProcesos_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Insertar]
GO

-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  5/7/2014 5:55:13 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NumeroProcesos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Insertar]
		@IdNumeroProceso INT OUTPUT, 	@NumeroProceso INT,	@Inactivo BIT,	@NumeroProcesoGenerado NVARCHAR(30),	@IdModalidadSeleccion INT,	@IdRegional INT,	@IdVigencia INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO CONTRATO.NumeroProcesos(NumeroProceso, Inactivo, NumeroProcesoGenerado, IdModalidadSeleccion, IdRegional, IdVigencia, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroProceso, @Inactivo, @NumeroProcesoGenerado, @IdModalidadSeleccion, @IdRegional, @IdVigencia, @UsuarioCrea, GETDATE())
	SELECT @IdNumeroProceso = @@IDENTITY 		
END



