USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Modificar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Modificar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Proveedores_Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Modificar]
		@IdProveedoresContratos INT,	@IdProveedores INT,	@IdContrato INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE CONTRATO.ProveedoresContratos
	SET	IdProveedores = @IdProveedores,
		IdContrato = @IdContrato,
		UsuarioModifica = @UsuarioModifica,
		FechaModifica = GETDATE()
	WHERE IdProveedoresContratos = @IdProveedoresContratos
END
