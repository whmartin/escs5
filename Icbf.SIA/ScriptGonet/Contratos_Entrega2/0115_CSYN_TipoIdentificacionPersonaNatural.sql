USE [SIA]
GO

/********************************
2014-05-22
Creamos el SYNONYM TipoIdentificacionPersonaNatural 
jorge Vizcaino
********************************/

/****** Object:  Synonym [Oferente].[TipoIdentificacionPersonaNatural]    Script Date: 22/05/2014 11:45:23 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoIdentificacionPersonaNatural' AND schema_id = SCHEMA_ID(N'Oferente'))
DROP SYNONYM [Oferente].[TipoIdentificacionPersonaNatural]
GO

/****** Object:  Synonym [Oferente].[TipoIdentificacionPersonaNatural]    Script Date: 22/05/2014 11:45:23 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoIdentificacionPersonaNatural' AND schema_id = SCHEMA_ID(N'Oferente'))
CREATE SYNONYM [Oferente].[TipoIdentificacionPersonaNatural] FOR [Oferentes].[Oferente].[TipoIdentificacionPersonaNatural]
GO
