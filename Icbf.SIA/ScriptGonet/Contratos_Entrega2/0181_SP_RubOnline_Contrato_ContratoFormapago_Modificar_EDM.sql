USE [SIA]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ContratoFormapago_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ContratoFormapago_Modificar]
GO

-- =============================================
-- Author:		Gonet\ Efrain Diaz Mejia
-- Create date:  09/06/2014 11:02 AM
-- Description:	Procedimiento almacenado que actualiza la forma de pago de un contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ContratoFormapago_Modificar]

@IdContrato	int,
@IdFormaPago int

AS
BEGIN
UPDATE [CONTRATO].[Contrato]
   SET 
      [IdFormaPago] = @IdFormaPago
      
   WHERE [IdContrato] = @IdContrato

END


