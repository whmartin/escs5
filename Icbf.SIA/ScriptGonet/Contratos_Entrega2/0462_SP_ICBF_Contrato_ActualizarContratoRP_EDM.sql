USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_ICBF_Contrato_SuscribirContrato]    Script Date: 11/07/2014 06:13:28 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Contrato_ActualizarContratoRP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ICBF_Contrato_ActualizarContratoRP]
GO


-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que Suscribe un contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Contrato_ActualizarContratoRP]
		@IdContrato INT,
		@UsuarioModifica Nvarchar(250),
		@IdEstadoContrato int
					
AS
Begin
UPDATE [CONTRATO].[Contrato]
   SET 
	  [UsuarioModifica] = @UsuarioModifica
	  ,[FechaModifica] = GetDate()
	  ,[IdEstadoContrato] = @IdEstadoContrato
 WHERE [IdContrato] = @IdContrato

End




