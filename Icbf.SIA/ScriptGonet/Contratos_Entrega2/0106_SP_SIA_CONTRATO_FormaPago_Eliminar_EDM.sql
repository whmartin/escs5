USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_FormaPago_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPago_Eliminar]
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/28/2014 3:00:58 PM
-- Description:	Procedimiento almacenado que elimina un(a) FormaPago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPago_Eliminar]
	@IdFormaPago INT
AS
BEGIN
	DELETE CONTRATO.FormaPago WHERE IdFormaPago = @IdFormaPago
END
