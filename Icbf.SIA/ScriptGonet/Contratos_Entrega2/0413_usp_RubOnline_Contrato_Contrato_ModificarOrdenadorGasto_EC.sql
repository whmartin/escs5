USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]    Script Date: 01/07/2014 10:26:24 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_Contrato_ModificarOrdenadorGasto')
BEGIN
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_ModificarOrdenadorGasto]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_ModificarOrdenadorGasto]    Script Date: 01/07/2014 10:26:24 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 24/06/2014
-- Description:	Asocia la informacion del ordenador gasto al contrato
-- =============================================
CREATE PROCEDURE usp_RubOnline_Contrato_Contrato_ModificarOrdenadorGasto
	@IdContrato INT,
	@IdRegionalEmpOrdG INT = NULL,
	@IdDependenciaEmpOrdG INT = NULL,
	@IdCargoEmpOrdG INT = NULL,
	@IdEmpleadoOrdenadorGasto INT = NULL
AS
BEGIN
	
	UPDATE CONTRATO.Contrato
	SET IdEmpleadoOrdenadorGasto = @IdEmpleadoOrdenadorGasto,
	IdRegionalOrdenador = @IdRegionalEmpOrdG,
	IdDependenciaOrdenador = @IdDependenciaEmpOrdG,
	IdCargoOrdenador = @IdCargoEmpOrdG
	WHERE IdContrato = @IdContrato

END
GO
