USE [SIA]

/***********************************************
Creado Por: Eduardo Isaac Ballesteros
El dia : 2014-07-10 08:00:00
Permite : Inserta los siguientes nuevos estados de contrato: Ejecución, Suspendido, Terminado, PerdidaCompetencia
***********************************************/

--Estado Ejecución
IF NOT EXISTS (
		SELECT *
		FROM [CONTRATO].[EstadoContrato]
		WHERE CodEstado = 'EJE'
		)
BEGIN
	INSERT INTO [CONTRATO].[EstadoContrato] (
		[CodEstado]
		,[Descripcion]
		,[Inactivo]
		,[UsuarioCrea]
		,[FechaCrea]
		,[UsuarioModifica]
		,[FechaModifica]
		)
	VALUES (
		'EJE'
		,'Ejecución'		
		,1
		,'Administrador'
		,GETDATE()
		,NULL
		,NULL
		)
END



--Estado Suspendido
IF NOT EXISTS (
		SELECT *
		FROM [CONTRATO].[EstadoContrato]
		WHERE CodEstado = 'SUP'
		)
BEGIN
	INSERT INTO [CONTRATO].[EstadoContrato] (
		[CodEstado]
		,[Descripcion]
		,[Inactivo]
		,[UsuarioCrea]
		,[FechaCrea]
		,[UsuarioModifica]
		,[FechaModifica]
		)
	VALUES (
		'SUP'
		,'Suspendido'		
		,1
		,'Administrador'
		,GETDATE()
		,NULL
		,NULL
		)
END



--Estado Terminado
IF NOT EXISTS (
		SELECT *
		FROM [CONTRATO].[EstadoContrato]
		WHERE CodEstado = 'TER'
		)
BEGIN
	INSERT INTO [CONTRATO].[EstadoContrato] (
		[CodEstado]
		,[Descripcion]
		,[Inactivo]
		,[UsuarioCrea]
		,[FechaCrea]
		,[UsuarioModifica]
		,[FechaModifica]
		)
	VALUES (
		'TER'
		,'Terminado'		
		,1
		,'Administrador'
		,GETDATE()
		,NULL
		,NULL
		)
END



--Estado Perdida Competencia
IF NOT EXISTS (
		SELECT *
		FROM [CONTRATO].[EstadoContrato]
		WHERE CodEstado = 'PEC'
		)
BEGIN
	INSERT INTO [CONTRATO].[EstadoContrato] (
		[CodEstado]
		,[Descripcion]
		,[Inactivo]
		,[UsuarioCrea]
		,[FechaCrea]
		,[UsuarioModifica]
		,[FechaModifica]
		)
	VALUES (
		'PEC'
		,'Perdida Competencia'		
		,1
		,'Administrador'
		,GETDATE()
		,NULL
		,NULL
		)
END


-- Estado Contrato con RP
IF NOT EXISTS (
		SELECT *
		FROM [CONTRATO].[EstadoContrato]
		WHERE CodEstado = 'CRP'
		)
BEGIN
	INSERT INTO [CONTRATO].[EstadoContrato] (
		[CodEstado]
		,[Descripcion]
		,[Inactivo]
		,[UsuarioCrea]
		,[FechaCrea]
		,[UsuarioModifica]
		,[FechaModifica]
		)
	VALUES (
		'CRP'
		,'Contrato con RP'		
		,1
		,'Administrador'
		,GETDATE()
		,NULL
		,NULL
		)
END


