USE [SIA]
GO
-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 10:24:57 AM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ArchivosGarantias]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla ArchivosGarantias a crear'
RETURN
END
CREATE TABLE [Contrato].[ArchivosGarantias](
 [IDArchivosGarantias] [INT]  IDENTITY(1,1) NOT NULL,
 [IDArchivo] [NUMERIC] (18,0) NOT NULL,
 [IDGarantia] [INT]  NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_ArchivosGarantias] PRIMARY KEY CLUSTERED 
(
 	[IDArchivosGarantias] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.ArchivosGarantias') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Hace referencia a los archivos anexos a la garantia.', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ArchivosGarantias;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.ArchivosGarantias') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDArchivosGarantias' AND [object_id] = OBJECT_ID('Contrato.ArchivosGarantias')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ArchivosGarantias,
@level2type = N'COLUMN', @level2name = IDArchivosGarantias;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.ArchivosGarantias') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDArchivo' AND [object_id] = OBJECT_ID('Contrato.ArchivosGarantias')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'ID Archivo', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ArchivosGarantias,
@level2type = N'COLUMN', @level2name = IDArchivo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.ArchivosGarantias') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDGarantia' AND [object_id] = OBJECT_ID('Contrato.ArchivosGarantias')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'ID Garantia', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ArchivosGarantias,
@level2type = N'COLUMN', @level2name = IDGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.ArchivosGarantias') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('Contrato.ArchivosGarantias')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ArchivosGarantias,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.ArchivosGarantias') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('Contrato.ArchivosGarantias')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ArchivosGarantias,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.ArchivosGarantias') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('Contrato.ArchivosGarantias')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ArchivosGarantias,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.ArchivosGarantias') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('Contrato.ArchivosGarantias')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ArchivosGarantias,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
