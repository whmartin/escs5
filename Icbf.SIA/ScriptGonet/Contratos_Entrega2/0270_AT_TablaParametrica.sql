use sia

-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Create date:  22/05/2014 2:30 PM
-- Description:	Cambio nombre tabla parametrica
-- =============================================

update CONTRATO.TablaParametrica SET NombreTablaParametrica = 'Administrar  consecutivo'
where NombreTablaParametrica = 'Administrar consecutivo de contrato'