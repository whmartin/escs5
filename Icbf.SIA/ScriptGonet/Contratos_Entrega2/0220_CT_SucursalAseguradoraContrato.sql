USE [SIA]
GO
-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 1:59:52 PM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[SucursalAseguradoraContrato]') AND type in (N'U'))
BEGIN
--Print 'Ya Existe La tabla SucursalAseguradoraContrato a crear'
RETURN
END
CREATE TABLE [Contrato].[SucursalAseguradoraContrato](
 [IDSucursalAseguradoraContrato] [INT]  IDENTITY(1,1) NOT NULL,
 [IDDepartamento] [INT]  NOT NULL,
 [IDMunicipio] [INT]  NOT NULL,
 [Nombre] [NVARCHAR] (160) NOT NULL,
 [IDZona] [INT] ,
 [DireccionNotificacion] [NVARCHAR] (80) NOT NULL,
 [CorreoElectronico] [NVARCHAR] (80) NOT NULL,
 [Indicativo] [NVARCHAR] (3) NOT NULL,
 [Telefono] [NVARCHAR] (40) NOT NULL,
 [Extension] [NVARCHAR] (8) NOT NULL,
 [Celular] [NVARCHAR] (40) NOT NULL,
 [IDEntidadProvOferente] [INT]  NOT NULL,
 [CodigoSucursal] [INT]  ,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_SucursalAseguradoraContrato] PRIMARY KEY CLUSTERED 
(
 	[IDSucursalAseguradoraContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Hace referencia a las sucursales relacionadas a la garantia de un contrato.', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDSucursalAseguradoraContrato' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = IDSucursalAseguradoraContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDDepartamento' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'ID Departamento', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = IDDepartamento;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDMunicipio' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'ID Municipio', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = IDMunicipio;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Nombre' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Nombre', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = Nombre;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDZona' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'ID Zona', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = IDZona;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'DireccionNotificacion' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Direccion Notificacion', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = DireccionNotificacion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'CorreoElectronico' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Correo Electronico', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = CorreoElectronico;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Indicativo' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Indicativo', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = Indicativo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Telefono' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Telefono', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = Telefono;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Extension' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Extension', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = Extension;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Celular' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Celular', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = Celular;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDEntidadProvOferente' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'ID Entidad Prov Oferente', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = IDEntidadProvOferente;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'CodigoSucursal' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Codigo Sucursal', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = CodigoSucursal;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('Contrato.SucursalAseguradoraContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = SucursalAseguradoraContrato,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
