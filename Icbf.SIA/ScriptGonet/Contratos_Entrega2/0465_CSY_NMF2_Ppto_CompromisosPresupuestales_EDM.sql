USE [SIA]
GO
-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  14/07/2014 15:38 
-- Description:	Sinonimo de la tabla [Ppto].[CompItemAfectacionGasto] de la base de datos NMF
-- =============================================

/****** Object:  Synonym [Ppto].[CompromisosPresupuestales]    Script Date: 14/07/2014 03:35:15 p.m. ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CompromisosPresupuestales' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[CompromisosPresupuestales]
GO

/****** Object:  Synonym [Ppto].[CompromisosPresupuestales]    Script Date: 14/07/2014 03:35:15 p.m. ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CompromisosPresupuestales' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[CompromisosPresupuestales] FOR [NMF].[Ppto].[CompromisosPresupuestales]
GO


