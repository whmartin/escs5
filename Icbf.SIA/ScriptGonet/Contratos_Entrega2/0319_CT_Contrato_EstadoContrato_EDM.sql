USE [SIA]
GO
-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  6/12/2014 4:52:44 PM
-- Description:	Script cRea tabla  [Contrato].[EstadoContrato]
-- =============================================

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[EstadoContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [Contrato].[EstadoContrato]
(
 [IDEstadoContrato] Int NOT NULL,
 [CodEstado] Char(4) NOT NULL,
 [Descripcion] Nvarchar(80) NOT NULL,
  CONSTRAINT [PK_EsatdoContrato] PRIMARY KEY CLUSTERED 
(
	[IDEstadoContrato] ASC
)
)
END