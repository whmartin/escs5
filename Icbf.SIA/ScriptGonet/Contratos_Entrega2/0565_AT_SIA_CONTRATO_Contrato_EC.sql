USE [SIA]
GO

IF  EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'AlcanceObjetoDelContrato')
BEGIN 
	ALTER TABLE CONTRATO.Contrato 
	ALTER COLUMN AlcanceObjetoDelContrato nvarchar(4000) null;
END