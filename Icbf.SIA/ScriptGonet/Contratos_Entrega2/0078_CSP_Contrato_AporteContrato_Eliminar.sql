USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AporteContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Eliminar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  5/29/2014 2:03:01 PM
-- Description:	Procedimiento almacenado que elimina un(a) AporteContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Eliminar]
	@IdAporteContrato INT
AS
BEGIN
	DELETE Contrato.AporteContrato WHERE IdAporteContrato = @IdAporteContrato
END
