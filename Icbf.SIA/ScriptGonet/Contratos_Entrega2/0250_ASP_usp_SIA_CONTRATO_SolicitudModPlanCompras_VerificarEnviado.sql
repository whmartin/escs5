USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado]    Script Date: 25/06/2014 14:55:18 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado]
GO


-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/22/2014 1:36:08 PM
-- Description:	Procedimiento almacenado que consulta un(a) SolicitudModPlanCompras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado]
	@CodEstado nvarchar(24),
	@NumeroConsecPlanCompras INT,
	@Vigencia varchar(8)
AS
BEGIN
	SELECT SolPC.IdSolicitudModPlanCompra,
		plcc.IDPlanDeCompras,
		EdoSol.Descripcion AS EstadoSolicitud
	FROM [CONTRATO].[SolicitudModPlanCompras] SolPC
	Inner JOIN CONTRATO.PlanDeComprasContratos plcc
		ON SolPC.IdPlanDeCompras = plcc.IdPlanDeCompras	
	INNER JOIN [CONTRATO].[EstadoSolcitudModPlanCompras] EdoSol
		ON SolPC.IdEstadoSolicitud = EdoSol.IdEstadoSolicitud
	WHERE EdoSol.CodEstado = @CodEstado
	AND plcc.IDPlanDeCompras = @NumeroConsecPlanCompras
	AND plcc.Vigencia = @Vigencia

END



