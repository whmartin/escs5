USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contratos_FormaPago_Consultar]    Script Date: 12/08/2014 11:04:52 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Contratos_FormaPago_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contratos_FormaPago_Consultar]
GO


-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  08/11/2014 4:52:44 PM
-- Description:	Procedimiento almacenado que consulta un por el id de la forma de pago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contratos_FormaPago_Consultar]
	
	@IdFormapago INT,
	@IDProveedoresContratos INT
AS
BEGIN

	 SELECT 
	 IDFormaPago
  FROM [CONTRATO].[ProveedoresContratos]
	 WHERE IdFormaPago = @IdFormapago
	 AND IDProveedoresContratos <> @IDProveedoresContratos

END




