USE [SIA]
GO

IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_VigenciaFuturass_Consultar]')
			AND type IN (
				N'P'
				,N'PC'
				)
		)
	DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturass_Consultar]
GO

-- =============================================
-- Author: Eduardo Isaac Ballesteros
-- Create date:  7/4/2014 9:50:33 AM
-- Description:	Procedimiento almacenado que consulta un(a) VigenciaFuturas
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturass_Consultar] @NumeroRadicado NVARCHAR(80) = NULL
	,@FechaExpedicion DATETIME = NULL
	,@IdContrato INT = NULL
	,@ValorVigenciaFutura NUMERIC(30, 2) = NULL
AS
BEGIN
	SELECT IDVigenciaFuturas
		,NumeroRadicado
		,FechaExpedicion
		,IdContrato
		,ValorVigenciaFutura
		,UsuarioCrea
		,FechaCrea
		,UsuarioModifica
		,FechaModifica
	FROM [CONTRATO].[VigenciaFuturas]
	WHERE NumeroRadicado = CASE 
			WHEN @NumeroRadicado IS NULL
				THEN NumeroRadicado
			ELSE @NumeroRadicado
			END
		AND FechaExpedicion = CASE 
			WHEN @FechaExpedicion IS NULL
				THEN FechaExpedicion
			ELSE @FechaExpedicion
			END
		AND IdContrato = CASE 
			WHEN @IdContrato IS NULL
				THEN IdContrato
			ELSE @IdContrato
			END
		AND ValorVigenciaFutura = CASE 
			WHEN @ValorVigenciaFutura IS NULL
				THEN ValorVigenciaFutura
			ELSE @ValorVigenciaFutura
			END
END

