USE [SIA]
GO

/****** Object:  Synonym [Ppto].[CompItemAfectacionGasto]    Script Date: 22/07/2014 15:58:15 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CompItemAfectacionGasto' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[CompItemAfectacionGasto]
GO

/****** Object:  Synonym [Ppto].[CompItemAfectacionGasto]    Script Date: 22/07/2014 15:58:15 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CompItemAfectacionGasto' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[CompItemAfectacionGasto] FOR [NMF].[Ppto].[CompItemAfectacionGasto]
GO


