USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Interventors_Consultar]    Script Date: 02/06/2014 10:51:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Interventor_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Interventor_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Interventors_Consultar]    Script Date: 02/06/2014 10:51:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero
-- Create date: 2014-06-03
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Interventor_Consultar]
	@IDSupervisorIntervContrato INT
	
AS
BEGIN
	SELECT DISTINCT DT.IdEntidad, DT.IdTercero, DT.NombreTipoPersona, DT.CodDocumento, DT.NumeroIdentificacion, DT.NombreRazonsocial 
  ,DT.IdTipoPersona, DT.IDTIPODOCIDENTIFICA, DT.PRIMERNOMBRE, DT.SEGUNDONOMBRE, DT.PRIMERAPELLIDO, DT.SEGUNDOAPELLIDO, DT.RAZONSOCIAL
  , DT.CORREOELECTRONICO, DT.IDSupervisorIntervContrato, DT.NumeroTelefono, DT.DireccionComercial
  FROM (
  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, 
  ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as NombreRazonsocial,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador,EP.UsuarioCrea, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor
	,T.PRIMERNOMBRE, ISNULL(T.SEGUNDONOMBRE,'') AS SEGUNDONOMBRE, T.PRIMERAPELLIDO, ISNULL(T.SEGUNDOAPELLIDO,'') AS SEGUNDOAPELLIDO, '' AS RAZONSOCIAL
	, T.CORREOELECTRONICO, SI.IDSupervisorIntervContrato, TelT.NumeroTelefono, IAE.DireccionComercial
  FROM [CONTRATO].[SupervisorInterContrato] SI
	 INNER JOIN	[Proveedor].[EntidadProvOferente] EP ON SI.IDProveedoresInterventor = EP.IdEntidad
     INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
     LEFT JOIN [Oferente].[TelTerceros] TelT ON T.IDTERCERO = TelT.IdTercero
     WHERE T.IdTipoPersona=1
UNION 
 SELECT  EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, 
 ( T.RAZONSOCIAL ) as NombreRazonsocial,
		E.Descripcion AS Estado ,EP.IdEstado ,EP.ObserValidador,EP.UsuarioCrea, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor
		,''	AS PRIMERNOMBRE, '' AS SEGUNDONOMBRE, '' AS PRIMERAPELLIDO, '' as SEGUNDOAPELLIDO, T.RAZONSOCIAL
		, T.CORREOELECTRONICO, SI.IDSupervisorIntervContrato, TelT.NumeroTelefono, IAE.DireccionComercial
  FROM [CONTRATO].[SupervisorInterContrato] SI
	INNER JOIN [Proveedor].[EntidadProvOferente] EP ON SI.IDProveedoresInterventor = EP.IdEntidad
    INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
    LEFT JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
    INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona
    INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
    INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
    INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
    LEFT JOIN [Oferente].[TelTerceros] TelT ON T.IDTERCERO = TelT.IdTercero
    WHERE T.IdTipoPersona=2
    ) DT
	WHERE DT.IDSupervisorIntervContrato = @IDSupervisorIntervContrato
END

GO


