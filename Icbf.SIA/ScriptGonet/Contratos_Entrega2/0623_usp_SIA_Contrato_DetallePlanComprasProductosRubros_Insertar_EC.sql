USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]    Script Date: 09/08/2014 16:35:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]
GO

/***************************************************/
/* Autor: Emilio Calapi�a					       */
/* Fecha: 09/09/2014 10:11 a.m.                    */
/* Descripci�n: Se agrega columna IdPagosDetalle  */
/***************************************************/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'RubrosPlanCompraContratos' AND ss.name = N'dbo')
	DROP TYPE dbo.RubrosPlanCompraContratos
GO

CREATE TYPE dbo.RubrosPlanCompraContratos AS TABLE
(
 CodigoRubro					NVARCHAR(256) NOT NULL, --CODIGO Rubro Pacco
 ValorRubroPresupuestal			NUMERIC(30,2) NULL, --Valor Rubro Pacco
 IdPagosDetalle				    NVARCHAR(150) NULL
)


/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]    Script Date: 09/08/2014 16:35:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jos� Ignacio De Los Reyes
-- Create date: 07/07/2014 13:24:00
-- Description:	Procedimiento almacenado que guarda un nuevo DetallePlanComprasContratos
-- =============================================
-- =============================================
-- Author:		Emilio Calapi�a
-- Create date: 09/09/2014 10:11:00
-- Description:	Se modifico el tipo tabla definido por el usuario dbo.RubrosPlanCompraContratos, 
-- se agrego la columna IdPagosDetalle
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]
	@NumeroConsecutivoPlanCompras	INT,	
	@Vigencia						INT,
	@IdContrato						INT,	
	@IdUsuario						INT,
	@UsuarioCrea					NVARCHAR(225),
	@ProductoPlanCompraContratos	ProductoPlanCompraContratos READONLY,
	@RubrosPlanCompraContratos		RubrosPlanCompraContratos READONLY
AS
BEGIN
	DECLARE @CodRegional				NVARCHAR(16)
			,@IDPlanDeComprasContratos	INT

	SELECT @CodRegional = R.CodigoRegional
	FROM DIV.Regional R
		INNER JOIN SEG.Usuario U ON R.IdRegional = U.IdRegional
	WHERE U.IdUsuario = @IdUsuario
	
	/*Codigo insertado por Cesar Ortiz para obtener el id de la regional a partir del contrato cuando no lo puede establecer con el usuario*/
	/*-------------------------------------------------*/
	IF (@CodRegional IS NULL)
	BEGIN
		SELECT @CodRegional = R.CodigoRegional
		FROM DIV.Regional R
			INNER JOIN CONTRATO.Contrato C ON R.IdRegional = C.IdRegionalContrato
		WHERE C.IdContrato = @IdContrato
	END
	/*-------------------------------------------------*/

	IF EXISTS(SELECT * FROM Contrato.PlanDeComprasContratos WHERE IDPlanDeCompras = @NumeroConsecutivoPlanCompras)
	BEGIN
		RAISERROR('Ya existe un registro con este n�mero consecutivo plan compras',16,1)
	    RETURN
	END

	INSERT INTO Contrato.PlanDeComprasContratos(IdContrato, IDPlanDeCompras, Vigencia, CodigoRegional, UsuarioCrea, FechaCrea)
	VALUES(@IdContrato, @NumeroConsecutivoPlanCompras, @Vigencia, @CodRegional, @UsuarioCrea, GETDATE())

	SELECT @IDPlanDeComprasContratos = SCOPE_IDENTITY();

	INSERT INTO CONTRATO.ProductoPlanCompraContratos(IDProducto, CantidadCupos, IDPlanDeComprasContratos, UsuarioCrea, FechaCrea, IdDetalleObjeto)
	SELECT CodigoProducto
		  ,CONVERT(NUMERIC(18,2),CantidadCupos)
		  ,@IDPlanDeComprasContratos
		  ,@UsuarioCrea
		  ,GETDATE()
		  ,IdDetalleObjeto
	FROM @ProductoPlanCompraContratos
		
	INSERT INTO CONTRATO.RubroPlanComprasContrato(ValorRubroPresupuestal, IDPlanDeComprasContratos, IDRubro, UsuarioCrea, FechaCrea, IdPagosDetalle)
	SELECT ValorRubroPresupuestal
		  ,@IDPlanDeComprasContratos
		  ,CodigoRubro
		  ,@UsuarioCrea
		  ,GETDATE()
		  ,IdPagosDetalle
	FROM @RubrosPlanCompraContratos

END

GO


