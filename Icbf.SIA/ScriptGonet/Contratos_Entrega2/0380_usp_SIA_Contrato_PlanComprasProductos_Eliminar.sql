USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasProductos_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Eliminar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:32 PM
-- Description:	Procedimiento almacenado que elimina un(a) PlanComprasProductos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Eliminar]
	@IdProductoPlanCompraContrato INT
AS
BEGIN
	DELETE Contrato.ProductoPlanCompraContratos 
	WHERE IDProductoPlanCompraContrato = @IdProductoPlanCompraContrato
END
