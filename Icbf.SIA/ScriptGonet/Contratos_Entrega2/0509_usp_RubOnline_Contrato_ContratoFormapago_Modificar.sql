USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ContratoFormapago_Modificar]    Script Date: 23/07/2014 02:47:02 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ContratoFormapago_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ContratoFormapago_Modificar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ContratoFormapago_Modificar]    Script Date: 23/07/2014 02:47:02 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Gonet\ Efrain Diaz Mejia
-- Create date:  09/06/2014 11:02 AM
-- Description:	Procedimiento almacenado que actualiza la forma de pago de un contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ContratoFormapago_Modificar]
		@IdProveedoresContratos int,
		@IdFormaPago INT,
		@UsuarioModifica Nvarchar(250)
					
AS
Begin
	
	UPDATE [CONTRATO].[ProveedoresContratos]
	   SET [IDFormaPago] = @IdFormaPago
		  ,[UsuarioModifica] = @UsuarioModifica
		  ,[FechaModifica] = Getdate()
      
	 WHERE IdProveedoresContratos = @IdProveedoresContratos

END

