USE [SIA]
GO
-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  11/6/2014 8:59:59 AM
-- =============================================

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ValorFinalContrato'))
BEGIN
ALTER TABLE CONTRATO.Contrato
ADD ValorFinalContrato Numeric (18,0)
END
GO

IF EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.AporteContrato') AND NAME = ('ValorAporte'))
BEGIN
ALTER TABLE CONTRATO.AporteContrato
ALTER COLUMN ValorAporte Numeric (30,2)
END
GO

