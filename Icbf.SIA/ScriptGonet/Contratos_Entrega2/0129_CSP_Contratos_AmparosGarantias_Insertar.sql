USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AmparosGarantias_Insertar]    Script Date: 09/06/2014 11:08:57 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AmparosGarantias_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantias_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AmparosGarantias_Insertar]    Script Date: 09/06/2014 11:08:57 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  5/30/2014 2:33:59 PM
-- Description:	Procedimiento almacenado que guarda un nuevo AmparosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantias_Insertar]
		@IDAmparosGarantias INT OUTPUT, 	@IDGarantia INT,	@IdTipoAmparo INT,	@FechaVigenciaDesde DATETIME,	@FechaVigenciaHasta DATETIME,	@IDUnidadCalculo INT,	@ValorCalculoAsegurado NUMERIC(8,2),	@ValorAsegurado NUMERIC(32,8), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
IF EXISTS( SELECT * FROM CONTRATO.AmparosGarantias ampg
		WHERE ampg.IDGarantia = @IDGarantia
		AND ampg.IdTipoAmparo= @IdTipoAmparo
		)
	BEGIN
		RAISERROR('Ya existe la garantia asociada al tipo de amparo seleccionado',16,1)
	    RETURN
	END
	INSERT INTO Contrato.AmparosGarantias(IDGarantia, IdTipoAmparo, FechaVigenciaDesde, FechaVigenciaHasta, IDUnidadCalculo, ValorCalculoAsegurado, ValorAsegurado, UsuarioCrea, FechaCrea)
					  VALUES(@IDGarantia, @IdTipoAmparo, @FechaVigenciaDesde, @FechaVigenciaHasta, @IDUnidadCalculo, @ValorCalculoAsegurado, @ValorAsegurado, @UsuarioCrea, GETDATE())
	SELECT @IDAmparosGarantias = @@IDENTITY 		
END


