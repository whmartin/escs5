USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_Contrato_Obtener')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Obtener]    Script Date: 29/07/2014 04:39:39 p.m. ******/
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Obtener]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Obtener]    Script Date: 29/07/2014 04:39:39 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 22/06/2014
-- Description:	
-- =============================================
-- [dbo].[usp_RubOnline_Contrato_Contrato_Obtener] 1
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Obtener] 
	@IdContrato INT
AS
BEGIN
	
	SELECT C.IdContrato, C.IdContratoAsociado, C.ConvenioMarco, C.ConvenioAdhesion, C.FK_IdContrato, C.NumeroContrato, C.IdEstadoContrato,
	C.IdCategoriaContrato, C.IdTipoContrato, C.IdModalidadAcademica, C.IdProfesion, C.IdModalidadSeleccion, C.IdNumeroProceso, 
	NP.NumeroProcesoGenerado AS NumeroProceso, C.FechaAdjudicacionDelProceso, 
	C.ActaDeInicio, C.ManejaAporte, C.ManejaRecurso, C.ConsecutivoPlanCompasAsociado, C.IdRegimenContratacion, 
	C.IdEmpleadoSolicitante, C.IdRegionalSolicitante, C.IdDependenciaSolicitante, C.IdCargoSolicitante,
	C.IdEmpleadoOrdenadorGasto, C.IdRegionalOrdenador, C.IdDependenciaOrdenador, C.IdCargoOrdenador,
	C.ObjetoDelContrato, C.AlcanceObjetoDelContrato, 
	(CASE WHEN C.ValorInicialContrato IS NULL THEN NULL ELSE CAST(C.ValorInicialContrato AS DECIMAL(30, 2)) END) AS ValorInicialContrato, 
	C.FechaInicioEjecucion, C.FechaFinalizacionIniciaContrato, C.FechaFinalTerminacionContrato, C.FechaSuscripcionContrato, 
	C.ManejaVigenciasFuturas, C.IdVigenciaInicial, C.IdVigenciaFinal, C.IdFormaPago, C.DatosAdicionaleslugarEjecucion, 
	C.FechaCrea, C.FechaModifica, C.UsuarioModifica, C.UsuarioCrea
	FROM Contrato.Contrato C
	LEFT OUTER JOIN CONTRATO.NumeroProcesos NP ON C.IdNumeroProceso = NP.IdNumeroProceso
	WHERE IdContrato = @IdContrato

END



GO


