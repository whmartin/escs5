USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_ContratosCDP_Obtener')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]    Script Date: 11/07/2014 06:02:05 p.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]    Script Date: 11/07/2014 06:02:05 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 01/07/2014
-- Description:	 M�todo de consulta CDPs asociados a un contrato
-- basado en el sp usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener] 67
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]
	@IdContrato INT
AS
BEGIN
	
	SELECT CCDP.[IdContratosCDP],
	CCDP.[IdCDP],
	CCDP.[IdContrato],
	RPCI.[DescripcionPCI] AS Regional,
	'' /*AI.Descripcion*/ AS Area,
	IECDP.[CDP] AS NumeroCDP,
	IECDP.[FechaSolicitudCDP] AS FechaCDP,
	IECDP.[ValorActualCDP] AS ValorCDP,
	CGRPG.DescripcionRubro AS RubroPresupuestal,
	rtrim(ltrim(TFF.CodTipoFte)) + '-' + rtrim(ltrim(TFF.DescTipoFuente)) AS TipoFuenteFinanciamiento,
	rtrim(ltrim(TRFP.CodTipoRecursoFinPptal)) +'-'+ rtrim(ltrim(TRFP.DescTipoRecurso)) AS RecursoPresupuestal,
	rtrim(ltrim(DASIIF.CodDepAfecSIIF)) + '-' + rtrim(ltrim(DASIIF.Descripcion)) AS DependenciaAfectacionGastos,
	'' AS TipoDocumentoSoporte,
	rtrim(ltrim(TSF.CodSitFondos)) + '-' + rtrim(ltrim(TSF.DescTipoSitFondos)) AS TipoSituacionFondos,
	0 AS ConsecutivoPlanCompras,
	CCDP.[UsuarioCrea], CCDP.[FechaCrea], CCDP.[UsuarioModifica], CCDP.[FechaModifica]
	FROM [CONTRATO].[ContratosCDP] CCDP
	INNER JOIN Ppto.InfoETLCDP IECDP ON IECDP.[IdEtlCDP] = CCDP.[IdCDP] --Error en el sinonimo
	INNER JOIN [Ppto].[RegionalesPCI] RPCI on RPCI.IdRegionalPCI = IECDP.IdRegionalPCI
	INNER JOIN Ppto.CatGralRubrosPptalGasto CGRPG on CGRPG.IdCatalogo = IECDP.IdCatRubro
	INNER JOIN BaseSIF.TipoFuenteFinanciamento TFF on TFF.IdTipoFte = IECDP.IdTipoFte
	INNER JOIN BaseSIF.TipoRecursoFinPptal TRFP on TRFP.IdTipoRecursoFinPptal = IECDP.IdTipoRecursoFinPptal
	INNER JOIN Ppto.DependenciasAfectacionSIIF DASIIF on DASIIF.IdDepAfecSIIF = IECDP.IdDepAfecSIIF
	INNER JOIN BaseSIF.TipoSitFondos TSF On TSF.IdTipoSitFondos = IECDP.IdTipoSitFondos
	--INNER JOIN /*[NMF].[Ppto].[AreasxRubro]*/ Ppto.AreasxRubro APR on APR.IdVigencia = IECDP.IdVigencia and APR.IdCatalogo = IECDP.IdCatRubro
	--INNER JOIN /*[NMF].[Ppto].[AreasInternas]*/ Ppto.AreasInternas AI On AI.IdAreasInternas = APR.IdAreasInternas
	WHERE CCDP.[IdContrato] = @IdContrato

END



GO


