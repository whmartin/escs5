USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_RPContratos_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[RPContratos]'))
ALTER TABLE [CONTRATO].[RPContratos] DROP CONSTRAINT [FK_RPContratos_Contrato]
GO

/****** Object:  Table [CONTRATO].[RPContratos]    Script Date: 09/07/2014 06:05:55 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[RPContratos]') AND type in (N'U'))
DROP TABLE [CONTRATO].[RPContratos]
GO

/****** Object:  Table [CONTRATO].[RPContratos]    Script Date: 09/07/2014 06:05:55 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[RPContratos]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[RPContratos](
	[IdRPContratos] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [int] NOT NULL,
	[IdRP] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_RPContratos] PRIMARY KEY CLUSTERED 
(
	[IdRPContratos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_RPContratos_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[RPContratos]'))
ALTER TABLE [CONTRATO].[RPContratos]  WITH CHECK ADD  CONSTRAINT [FK_RPContratos_Contrato] FOREIGN KEY([IdContrato])
REFERENCES [CONTRATO].[Contrato] ([IdContrato])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_RPContratos_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[RPContratos]'))
ALTER TABLE [CONTRATO].[RPContratos] CHECK CONSTRAINT [FK_RPContratos_Contrato]
GO


