USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  26/06/2014 17:06 PM
-- Description:	Actualiza tabla [CONTRATO].[TablaParametrica] para mostrar opci�n men�.
-- =============================================

IF exists(SELECT [IdTablaParametrica] FROM [CONTRATO].[TablaParametrica] where url = 'Contratos/NumeroProcesos')

Begin
	
	declare @IdTabla int = null

	select @IdTabla = [IdTablaParametrica] FROM [CONTRATO].[TablaParametrica] where url = 'Contratos/NumeroProcesos'
	
	If @IdTabla is not null
	begin
	   UPDATE [CONTRATO].[TablaParametrica]
	   SET [NombreTablaParametrica] = N'Administrar N�mero de Proceso'
	   WHERE [IdTablaParametrica] = @IdTabla
	END
		
 
	
END


