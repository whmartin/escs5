USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ModificarUsuario]    Script Date: 26/05/2014 14:30:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ModificarUsuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Seg_ModificarUsuario]
GO


-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  16/10/2012
-- Description:	Procedimiento almacenado que modifica información de un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ModificarUsuario]
		  @IdUsuario INT
		, @IdTipoPersona INT
		, @IdTipoDocumento INT
		, @NumeroDocumento NVARCHAR(17)
		, @DV NVARCHAR(1)
		, @PrimerNombre NVARCHAR(150)
		, @SegundoNombre NVARCHAR(150)
		, @PrimerApellido NVARCHAR(150)
		, @SegundoApellido NVARCHAR(150)
		, @RazonSocial NVARCHAR(150)
		, @CorreoElectronico NVARCHAR(150)
		, @TelefonoContacto NVARCHAR(10)
		, @Estado BIT
		, @UsuarioModificacion NVARCHAR(250)
		, @IdTipoUsuario int
		, @IdRegional int
AS
BEGIN
	UPDATE SEG.Usuario
		SET [IdTipoDocumento] = @IdTipoDocumento
		   ,[NumeroDocumento] = @NumeroDocumento
		   ,[PrimerNombre] = @PrimerNombre
		   ,[SegundoNombre] = @SegundoNombre
		   ,[PrimerApellido] = @PrimerApellido
		   ,[SegundoApellido] = @SegundoApellido
		   ,[CorreoElectronico] = @CorreoElectronico
		   ,[Estado] = @Estado
		   ,[UsuarioModificacion] = @UsuarioModificacion
		   ,[FechaModificacion] = GETDATE()
		   ,[IdTipoPersona] = @IdTipoPersona
		   ,[DV] = @DV
		   ,[RazonSocial] = @RazonSocial
		   ,[TelefonoContacto] = @TelefonoContacto
		   ,IdTipoUsuario = @IdTipoUsuario
		   ,IdRegional = @IdRegional
	WHERE [IdUsuario] = @IdUsuario
END
