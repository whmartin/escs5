USE SIA

/********************************
2014-05-22
Creamos el schema Tesoreria 
jorge Vizcaino
********************************/
GO

/****** Object:  Schema [Tesoreria]    Script Date: 22/05/2014 11:44:25 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'Tesoreria')
DROP SCHEMA [Tesoreria]
GO

/****** Object:  Schema [Tesoreria]    Script Date: 22/05/2014 11:44:25 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Tesoreria')
EXEC sys.sp_executesql N'CREATE SCHEMA [Tesoreria]'

GO


