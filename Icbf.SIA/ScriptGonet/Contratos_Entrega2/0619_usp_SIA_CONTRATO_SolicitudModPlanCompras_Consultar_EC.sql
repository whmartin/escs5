USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]    Script Date: 09/08/2014 16:21:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]    Script Date: 09/08/2014 16:21:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==========================================================================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  22/05/2014 1:36:08 PM
-- Description:	Procedimiento almacenado que consulta un(a) SolicitudModPlanCompras
-- ==========================================================================================
-- ==========================================================================================
-- Author:		Wilmer Alvarez
-- Create date:  22/07/2014 12:36 PM
-- Description:	Se separa con espacio el nombre del usuario
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]
@IdSolicitudModPlanCompra int
as
begin

		select	SolPC.IdSolicitudModPlanCompra,
				SolPC.Justificacion,
				SolPC.NumeroSolicitud,
				SolPC.FechaSolicitud,
				SolPC.IdEstadoSolicitud,
				SolPC.IdUsuarioSolicitud,
				SolPC.IdPlanDeCompras,
				SolPC.UsuarioCrea,
				SolPC.FechaCrea,
				SolPC.UsuarioModifica,
				SolPC.FechaModifica,
				PC.IdContrato,
				cco.NumeroContrato,
				PC.IDPlanDeCompras NumeroConsecPlanDeCompras,
				Usu.PrimerNombre + ' ' + Usu.SegundoNombre + ' ' + Usu.PrimerApellido + ' ' + Usu.SegundoApellido UsuarioSolicitud,
				PC.Vigencia,
				EdoSol.Descripcion EstadoSolicitud
		from	CONTRATO.SolicitudModPlanCompras SolPC inner join 
				CONTRATO.PlanDeComprasContratos PC on SolPC.IdPlanDeCompras = PC.IDPlanDeComprasContratos inner join 
				SEG.Usuario Usu on SolPC.IdUsuarioSolicitud = Usu.IdUsuario inner join 
				CONTRATO.EstadoSolcitudModPlanCompras EdoSol on SolPC.IdEstadoSolicitud = EdoSol.IdEstadoSolicitud inner join 
				CONTRATO.Contrato cco on PC.IdContrato = cco.IdContrato
		where	SolPC.IdSolicitudModPlanCompra = @IdSolicitudModPlanCompra
end

GO


