USE [SIA]
GO
-- =============================================
-- Author: Eduardo Isaac Ballesteros Muñoz
-- Create date: 7/1/2014 11:59:06 AM
-- Description: Creación de la FK FK_DirectorInterventoria_SupervisorInterContrato
-- =============================================
IF EXISTS (
		SELECT *
		FROM DBO.SYSOBJECTS
		WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_DirectorInterventoria_SupervisorInterContrato]')
			AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1
		)

ALTER TABLE [CONTRATO].[SupervisorInterContrato] DROP CONSTRAINT FK_DirectorInterventoria_SupervisorInterContrato

GO

ALTER TABLE [CONTRATO].[SupervisorInterContrato]
ADD CONSTRAINT [FK_DirectorInterventoria_SupervisorInterContrato]
FOREIGN KEY ([IDDirectorInterventoria])
REFERENCES [CONTRATO].[DirectorInterventoria] ([IDDirectorInterventoria])