
USE [SIA]
GO

-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date: 22/08/2014
-- Description:	Insertar opción de Tipo Obligacion en la tabla TablaParametrica
-- =============================================
IF not exists(SELECT IdTablaParametrica from Contrato.TablaParametrica where NombreTablaParametrica = 'Tipo Obligación')
Begin
INSERT INTO Contrato.TablaParametrica
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           )
     VALUES
           ('03'
           ,'Tipo Obligación'
           ,'Contratos/TipoObligacion'
           ,1
           ,'Administrador'
           ,GETDATE()
           )
End