USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Jose de los reyes
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion Tabla [CONTRATO].[EstadoContrato]
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_EstadoContrato]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_EstadoContrato]
END


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[EstadoContrato]') AND type in (N'U'))
BEGIN
/****** Object:  Table [CONTRATO].[EstadoContrato]    Script Date: 24/06/2014 06:41:36 p.m. ******/
DROP TABLE [CONTRATO].[EstadoContrato]

END
GO

/****** Object:  Table [CONTRATO].[EstadoContrato]    Script Date: 24/06/2014 06:41:36 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CONTRATO].[EstadoContrato](
	[IDEstadoContrato] [int] IDENTITY(1,1) NOT NULL,
	[CodEstado] [char](4) NOT NULL,
	[Descripcion] [nvarchar](80) NOT NULL,
	[Inactivo] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EsatdoContrato] PRIMARY KEY CLUSTERED 
(
	[IDEstadoContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


