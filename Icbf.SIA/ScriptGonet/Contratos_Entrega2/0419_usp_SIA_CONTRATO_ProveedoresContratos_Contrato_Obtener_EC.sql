USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]    Script Date: 30/06/2014 07:44:52 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener')
BEGIN
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]    Script Date: 30/06/2014 07:44:52 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 30/06/2014
-- Description:	Obtiene la informacion de contratistas relacionados con un contrato, 
-- basado en usp_SIA_CONTRATO_Proveedores_Contratistas_Consultar
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]
	@IdContrato INT, 
	@IntegrantesUnionTemporal BIT = 0
AS
BEGIN
	
	IF @IntegrantesUnionTemporal = 0
	BEGIN

		SELECT PC.IdProveedoresContratos, IdProveedores AS IdTercero, TP.NombreTipoPersona AS TipoPersona, 
		TD.CodDocumento AS TipoIdentificacion, T.NUMEROIDENTIFICACION AS NumeroIdentificacion, 
		(CASE TP.CodigoTipoPersona  
			WHEN '001' THEN (T.PRIMERNOMBRE + (CASE ISNULL(T.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDONOMBRE + ' ' END) + T.PRIMERAPELLIDO + (CASE ISNULL(T.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDOAPELLIDO END))
			ELSE T.RAZONSOCIAL END) AS InformacionContratista, 
		'' AS IdentificacionRepLegal, '' AS Representante, NULL AS PorcentajeParticipacion, NULL AS NumeroIdentificacionIntegrante
		FROM CONTRATO.ProveedoresContratos PC
		INNER JOIN Oferente.TERCERO T ON PC.IdProveedores = T.IdTercero
		INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona = TP.IdTipoPersona 
		INNER JOIN Global.TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento 
		--INNER JOIN Oferente.EntidadProvOferente EPO ON EPO.IDTERCERO = PC.IdProveedores
		--INNER JOIN Proveedor.InfoAdminEntidad IAE ON IAE.IdEntidad = EPO.IdEntidad
		WHERE IdContrato = @IdContrato

	END
	ELSE
	BEGIN
		PRINT 'FALTA CONSULTA'
		-- CONSULTA TEMPORAL INTEGRANTES UNION TEMPORAL
		--SELECT PC.IdProveedoresContratos, IdProveedores AS IdTercero, TP.NombreTipoPersona AS TipoPersona, 
		--TD.CodDocumento AS TipoIdentificacion, T.NUMEROIDENTIFICACION AS NumeroIdentificacion, 
		--(CASE TP.CodigoTipoPersona  
		--	WHEN '001' THEN (T.PRIMERNOMBRE + (CASE ISNULL(T.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDONOMBRE + ' ' END) + T.PRIMERAPELLIDO + (CASE ISNULL(T.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDOAPELLIDO END))
		--	ELSE T.RAZONSOCIAL END) AS InformacionContratista, 
		--'' AS IdentificacionRepLegal, '' AS Representante, NULL AS PorcentajeParticipacion, NULL AS NumeroIdentificacionIntegrante
		--FROM CONTRATO.ProveedoresContratos PC
		--INNER JOIN Oferente.TERCERO T ON PC.IdProveedores = T.IdTercero
		--INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona = TP.IdTipoPersona 
		--INNER JOIN Global.TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento 
		----INNER JOIN Oferente.EntidadProvOferente EPO ON EPO.IDTERCERO = PC.IdProveedores
		----INNER JOIN Proveedor.InfoAdminEntidad IAE ON IAE.IdEntidad = EPO.IdEntidad
		--WHERE IdContrato = @IdContrato

	END



END

GO


