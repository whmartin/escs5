use SIA
GO

-- ==========================================================================================
-- Author:		Emilio Calapi�a
-- Create date:  2014/10/28 11:03
-- Description:	Creaci�n de la opci�n de desbloqueo
-- ==========================================================================================
declare @IdPrograma int

if not exists(	select	IdPrograma
				from	SEG.Programa
				where	CodigoPrograma = 'Seguridad/DesbloquearUsuario')
begin
		DECLARE @IdModuloSeguridad INT
		select top 1 @IdModuloSeguridad=IdModulo from SEG.Modulo where NombreModulo = 'Seguridad'

		insert into SEG.Programa
				(	IdModulo, NombrePrograma, CodigoPrograma, Posicion, VisibleMenu, GeneraLog, 
					Padre, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion)
		values	(	@IdModuloSeguridad, 'Desbloquear usuario', 'Seguridad/DesbloquearUsuario',  4, 1, 1, 
					null, 1, 'administrador', getdate(), null, null)

end

select	@IdPrograma = IdPrograma
from	SEG.Programa
where	CodigoPrograma = 'Seguridad/DesbloquearUsuario'

if not exists(	select	IdPrograma
				from	SEG.Permiso
				where	IdPrograma = @IdPrograma
						and IdRol = 1)
begin

	insert into SEG.Permiso
			(	IdPrograma, IdRol, Insertar, Modificar, Eliminar, Consultar, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion)
	values	(	@IdPrograma, 1, 1, 1, 0, 1, 'administrador', getdate(), null, null)
end