USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]    Script Date: 04/06/2014 12:06:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]
	@IdTipoSupvInterventor INT
AS
BEGIN
	DELETE Contrato.[TipoSuperInter] WHERE IDTipoSuperInter = @IdTipoSupvInterventor
END



