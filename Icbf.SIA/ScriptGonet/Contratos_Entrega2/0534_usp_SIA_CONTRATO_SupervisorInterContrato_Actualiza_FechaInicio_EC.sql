USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_SupervisorInterContrato_Actualiza_FechaInicio')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Actualiza_FechaInicio]    Script Date: 29/07/2014 11:01:43 a.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Actualiza_FechaInicio]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Actualiza_FechaInicio]    Script Date: 29/07/2014 11:01:43 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 29/08/2014
-- Description:	Permite modificar la fecha de inicio de un supervisor/interventor registrado
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Actualiza_FechaInicio] 
	@IDSupervisorIntervContrato INT,
	@FechaInicio DATETIME,
	@UsuarioModifica VARCHAR(250)
AS
BEGIN
	
	UPDATE CONTRATO.SupervisorInterContrato
	SET	FechaInicio = @FechaInicio,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
	WHERE IDSupervisorIntervContrato = @IDSupervisorIntervContrato

END

GO


