USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Eliminar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/27/2014 6:43:39 PM
-- Description:	Procedimiento almacenado que elimina un(a) ConsecutivoContratoRegionales
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Eliminar]
	@IDConsecutivoContratoRegional INT
AS
BEGIN
	DELETE CONTRATO.ConsecutivoContratoRegionales WHERE IDConsecutivoContratoRegional = @IDConsecutivoContratoRegional
END
