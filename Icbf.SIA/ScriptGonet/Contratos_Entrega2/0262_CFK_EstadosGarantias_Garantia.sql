USE [SIA]
GO
-- =============================================
-- Author:		Gonet\Carlos Andr�s C�rdenas P.
-- Create date:  20/06/2014 10:10:00 AM
-- Description:	Creacion de llave for�nea FK_EstadosGarantias_Garantia
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Ppto].[FK_EstadosGarantias_Garantia]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [Contrato].[Garantia] DROP CONSTRAINT FK_EstadosGarantias_Garantia
END

ALTER TABLE [Contrato].[Garantia]
ADD CONSTRAINT FK_EstadosGarantias_Garantia FOREIGN KEY(IDEstadosGarantias)REFERENCES  [CONTRATO].[EstadosGarantias](IDEstadosGarantias)

