USE [SIA]
GO

/********************************
2014-05-22
Creamos el SYNONYM TipoMedioPago 
jorge Vizcaino
********************************/

/****** Object:  Synonym [BaseSIF].[TipoMedioPago]    Script Date: 22/05/2014 11:45:23 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoMedioPago' AND schema_id = SCHEMA_ID(N'BaseSIF'))
DROP SYNONYM [BaseSIF].[TipoMedioPago]
GO

/****** Object:  Synonym [BaseSIF].[TipoMedioPago]    Script Date: 22/05/2014 11:45:23 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoMedioPago' AND schema_id = SCHEMA_ID(N'BaseSIF'))
CREATE SYNONYM [BaseSIF].[TipoMedioPago] FOR [NMF].[BaseSIF].[TipoMedioPago]
GO


