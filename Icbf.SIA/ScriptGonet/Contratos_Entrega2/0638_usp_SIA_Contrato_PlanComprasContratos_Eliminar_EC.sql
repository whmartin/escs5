USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar]    Script Date: 09/17/2014 17:51:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar]    Script Date: 09/17/2014 17:51:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Jos� Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:27 PM
-- Description:	Procedimiento almacenado que elimina un(a) PlanComprasContratos
-- =============================================
-- [dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar] 123
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar]
	@IDPlanDeComprasContratos INT
AS
BEGIN

	--DELETE FROM CONTRATO.SolicitudModPlanCompras WHERE IdPlanDeCompras = @IDPlanDeComprasContratos
	
	DELETE PPC
	FROM CONTRATO.ProductoPlanCompraContratos PPC
		INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON PPC.IDPlanDeComprasContratos = PCC.IDPlanDeComprasContratos
	WHERE PCC.IDPlanDeComprasContratos = @IDPlanDeComprasContratos

	DELETE RPC
	FROM CONTRATO.RubroPlanComprasContrato RPC
		INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON RPC.IDPlanDeComprasContratos = PCC.IDPlanDeComprasContratos
	WHERE PCC.IDPlanDeComprasContratos = @IDPlanDeComprasContratos

	DELETE Contrato.PlanDeComprasContratos 
	WHERE IDPlanDeComprasContratos = @IDPlanDeComprasContratos
END


GO


