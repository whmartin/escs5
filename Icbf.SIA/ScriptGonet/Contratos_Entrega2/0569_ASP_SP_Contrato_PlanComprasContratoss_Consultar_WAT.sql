USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasContratoss_Consultar]    Script Date: 08/08/2014 11:11:48 a.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratoss_Consultar]
GO

-- ==========================================================================================
-- Author:		Jos� Ignacio De Los Reyes
-- Create date: 06/25/2014 14:40:00
-- Description:	Procedimiento almacenado que consulta un(a) PlanComprasContratos
-- ==========================================================================================
-- ==========================================================================================
-- Author:		Wilmer Alvarez
-- Create date: 08/08/2014 11:00:00
-- Description:	Se retiran el parametro redundante y se cambia el nombre de @CodigoRegional
--				por @usuario par no prestarse a confuci�n
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratoss_Consultar]
@Vigencia						int = null,
@IdContrato						int = null,
@IDPlanDeCompras				int = null,
@Usuario						int = null
as
begin
		declare @CodigoRegional	nvarchar(16) = null

		select	@CodigoRegional = R.CodigoRegional
		from	SEG.Usuario U inner join 
				DIV.Regional R on U.IdRegional = R.IdRegional
		where	U.IdUsuario = @Usuario

		select	pcc.IDPlanDeComprasContratos,
				pcc.Vigencia,
				pcc.IdContrato,
				pcc.IDPlanDeCompras,
				pcc.CodigoRegional,
				pcc.UsuarioCrea,
				pcc.FechaCrea,
				pcc.UsuarioModifica,
				pcc.FechaModifica
		from	CONTRATO.PlanDeComprasContratos pcc
		where	pcc.Vigencia = isnull(@Vigencia, pcc.Vigencia)
				and pcc.IdContrato = isnull(@IdContrato, pcc.IdContrato)
				and pcc.IDPlanDeCompras = isnull(@IDPlanDeCompras, pcc.IDPlanDeCompras)
				and pcc.CodigoRegional = isnull(@CodigoRegional, pcc.CodigoRegional)
end
go


