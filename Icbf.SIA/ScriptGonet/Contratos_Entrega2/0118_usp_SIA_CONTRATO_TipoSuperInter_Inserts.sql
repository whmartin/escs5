USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 2014-05-28
-- Description:	Insertar data  TipoSuperInter
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[TipoSuperInter]') AND type in (N'U'))
BEGIN
	if NOT EXISTS (select 1 from CONTRATO.TipoSuperInter where Codigo in ('01') )
	begin
		INSERT INTO CONTRATO.TipoSuperInter(Codigo, Descripcion, UsuarioCrea, FechaCrea)
						  VALUES('01', 'Interno', 'Administrador', GETDATE())
		INSERT INTO CONTRATO.TipoSuperInter(Codigo, Descripcion, UsuarioCrea, FechaCrea)
						  VALUES('02', 'Externo', 'Administrador', GETDATE())
					  end

RETURN
END



