USE [SIA]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ==============================================================================================================
-- Author:		 Jos� Ignacio De Los Reyes
-- Create date:  23/10/2013 14:22:00 PM
-- Permite : Crear el modulo "Cargar Archivo Maestro COmpromisos" en Oferentes y se le asignan los permisos a administrador
			 Para Solucionar Caso de Uso 
-- ==============================================================================================================*/

BEGIN TRY
	BEGIN TRAN T1

		IF OBJECT_ID('tempdb..#TMP_DATOSMENU') IS NOT NULL
		BEGIN
			DROP TABLE #TMP_DATOSMENU
		END

		CREATE TABLE #TMP_DATOSMENU(
			ID				INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
			DESCRIP			NVARCHAR(100),
			POSICION		INT,
			PADRE			INT,
			CODIPROGRAMA	nvarchar(500),
			VisibleMenu		INT
		)

		INSERT INTO #TMP_DATOSMENU
		VALUES('Plan Compras Contratos',0,0,'Contrato/PlanComprasContratos',0)

		--INSERT INTO #TMP_DATOSMENU
		--VALUES('Lupas Relacionar Plan Compras',1,1,'Contrato/PlanComprasContratos',0)

		--INSERT INTO #TMP_DATOSMENU
		--VALUES('Cargue Archivo Maestro',1,1,'Ppto/EstructuraArchivoRegistrarCompromisoMaestro',0)

		--INSERT INTO #TMP_DATOSMENU
		--VALUES('Cargue Archivo Detalle1',1,1,'Ppto/EstructuraArchivoDetalle1ItemAfectacionGastos',0)

		--INSERT INTO #TMP_DATOSMENU
		--VALUES('Cargue Archivo Detalle2',1,1,'Ppto/EstructuraArchivoDetalle2PlanPagos',0)
		
		DECLARE @CANT_REG				INT,
				@INDEX					INT,
				@NombrePrograma			NVARCHAR(100),
				@NombreProgramaPadre	NVARCHAR(100),
				@Posicion				INT,
				@IdModulo				INT,
				@IdPadreTmp				INT,
				@CODIPROGRAMA			nvarchar(500),
				@VisibleMenu			INT

		SELECT @CANT_REG = COUNT(ISNULL(ID,0)) FROM #TMP_DATOSMENU
		SET @INDEX = 1

		SELECT @IdModulo = IdModulo
		FROM SEG.Modulo
		WHERE NombreModulo = 'Contratos'
		
		WHILE (@CANT_REG >= @INDEX)
		BEGIN
			
			SELECT @NombrePrograma = DESCRIP,
				   @Posicion = POSICION,
				   @IdPadreTmp = PADRE,
				   @CODIPROGRAMA = ISNULL(CODIPROGRAMA,''),
				   @VisibleMenu = VisibleMenu
			FROM #TMP_DATOSMENU
			WHERE ID = @INDEX
			
			SELECT @NombreProgramaPadre = DESCRIP
			FROM #TMP_DATOSMENU
			WHERE ID = (SELECT PADRE 
						FROM #TMP_DATOSMENU 
						WHERE ID = @INDEX)
							
			IF NOT EXISTS(SELECT idprograma 
						  FROM SEG.Programa
						  WHERE CodigoPrograma = @CODIPROGRAMA
							AND NombrePrograma = @NombrePrograma
							AND IdModulo = @IdModulo)
			BEGIN
				  DECLARE @idpadre INT
			      
				  SELECT @idpadre = IdPrograma 
				  FROM SEG.Programa 
				  WHERE NombrePrograma = @NombreProgramaPadre
					AND IdModulo = @IdModulo			
			      
				  INSERT INTO SEG.Programa 
				  SELECT IdModulo,@NombrePrograma,@CODIPROGRAMA,@Posicion,1,'Administrador',GETDATE(),'','',@VisibleMenu,1,@idpadre,0,NULL
				  FROM SEG.Modulo
				  WHERE NombreModulo = 'Contratos'
			                  
				  INSERT INTO SEG.Permiso 
				  SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
				  FROM SEG.Programa
				  WHERE CodigoPrograma = @CODIPROGRAMA
					AND NombrePrograma = @NombrePrograma
					AND IdModulo = @IdModulo
			END
			ELSE
			BEGIN
				  PRINT 'YA EXISTE EL MODULO A CREAR'
			END
			
			SET @INDEX = (@INDEX + 1)
		END
					
	IF @@TRANCOUNT > 0
	BEGIN
		COMMIT TRAN T1
	END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRAN T1
	END
	
	DECLARE @msg nvarchar(max), @errSeverity int, @errNro int

	SET @msg = ''
	SET @msg = @msg + ' '+ CONVERT(nvarchar(max),ERROR_MESSAGE()) + CHAR(13)+ CHAR(10) + ' '
	SET @errSeverity = ERROR_SEVERITY()
	SET @errNro = ERROR_NUMBER()

	RAISERROR(@msg , @errSeverity , 1)
END CATCH



