USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ListaProveedores_Contratos_Consultar]    Script Date: 28/05/2014 11:23:34 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ListaProveedores_Contratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ListaProveedores_Contratos_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ListaProveedores_Contratos_Consultar]    Script Date: 28/05/2014 11:23:34 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  28/05/2014 02:53:55 PM
-- Description:	Procedimiento almacenado que consulta la lista de proveedores asociados a un contrato en especifico
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ListaProveedores_Contratos_Consultar]
	@IdContrato INT = NULL
AS
BEGIN
  SELECT DISTINCT * FROM (
	  SELECT T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, T.IdTercero, ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as Razonsocila,
			PC.UsuarioCrea
	  FROM 
		 oferente.TERCERO T
		 INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
		 INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		 INNER JOIN contrato.ProveedoresContratos PC ON T.IdTercero = PC.IdProveedores
		 WHERE T.IdTipoPersona=1 AND PC.IdContrato = @IdContrato
	   
	   UNION 
	   
	   SELECT T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, T.IdTercero, ( T.RAZONSOCIAL ) as Razonsocila,
			PC.UsuarioCrea
	  FROM 
		 oferente.TERCERO T
		 INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
		 INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		 INNER JOIN contrato.ProveedoresContratos PC ON T.IdTercero = PC.IdProveedores
		 WHERE T.IdTipoPersona=2 AND PC.IdContrato = @IdContrato
		  ) DT

   
END


