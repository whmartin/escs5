USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Consultar]    Script Date: 01/07/2014 16:03:57 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoFormaPago_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoFormaPago_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Consultar]
	@IdTipoFormaPago INT
AS
BEGIN
	SELECT
		IdTipoFormaPago,
		NombreTipoFormaPago,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoFormaPago]
	WHERE IdTipoFormaPago = @IdTipoFormaPago
END
