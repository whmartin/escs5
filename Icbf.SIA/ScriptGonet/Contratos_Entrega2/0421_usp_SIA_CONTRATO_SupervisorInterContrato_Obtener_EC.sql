USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Obtener]    Script Date: 01/07/2014 01:47:48 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_SupervisorInterContrato_Obtener')
BEGIN
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Obtener]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Obtener]    Script Date: 01/07/2014 01:47:48 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 01/07/2014
-- Description:	
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Obtener] @IdContrato = 8
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Obtener]
	@IdContrato INT, 
	@DirectoresInterventoria BIT = 0
AS
BEGIN
	
	IF @DirectoresInterventoria = 0 
	BEGIN
		SELECT SIC.IDSupervisorIntervContrato AS IDSupervisorIntervContrato, SI.Descripcion AS EtQSupervisorInterventor, 
		TSI.Descripcion AS EtQInternoExterno, SIC.FechaInicio AS FechaInicio, 
		SIC.TipoIdentificacion AS TipoIdentificacion, SIC.Identificacion AS Identificacion, 
		'' AS TipoVinculacionContractual, 
		Evw.nom_emp1 + (CASE ISNULL(Evw.nom_emp2, '') WHEN '' THEN ' ' ELSE ' ' + Evw.nom_emp2 + ' ' END) + Evw.ape_emp1 + (CASE ISNULL(Evw.ape_emp2, '') WHEN '' THEN ' ' ELSE ' ' + Evw.ape_emp2 END) AS NombreCompleto, 
		Evw.regional AS Regional, Evw.nom_depe AS Dependencia, Evw.desc_car AS Cargo, Evw.direccion AS Direccion, 
		Evw.telefono AS Telefono, C.NumeroContrato AS IdNumeroContratoInterventoria, '' AS Celular, Evw.eee_mail AS CorreoElectronico,
		SIC.[UsuarioCrea], SIC.[FechaCrea], SIC.[UsuarioModifica], SIC.[FechaModifica]
		FROM [CONTRATO].[SupervisorInterContrato] SIC 
		INNER JOIN [CONTRATO].[TipoSuperInter] TSI ON TSI.IDTipoSuperInter = SIC.IDTipoSuperInter
		INNER JOIN [CONTRATO].[TipoSupervisorInterventor] SI ON SI.Codigo = TSI.Codigo
		INNER JOIN [KACTUS].[KPRODII].[dbo].[Da_Emple] Evw ON Evw.num_iden = SIC.IDEmpleadosSupervisor
		LEFT OUTER JOIN [CONTRATO].[Contrato] C ON C.IdContrato = SIC.IdNumeroContratoInterventoria
		WHERE SIC.IdContrato = @IdContrato
		UNION
		SELECT SIC.IDSupervisorIntervContrato AS IDSupervisorIntervContrato, 
		SI.Descripcion AS EtQSupervisorInterventor, 
		TSI.Descripcion AS EtQInternoExterno, SIC.FechaInicio AS FechaInicio, 
		SIC.TipoIdentificacion AS TipoIdentificacion, SIC.Identificacion AS Identificacion,
		'' AS TipoVinculacionContractual, 
		(CASE TP.CodigoTipoPersona  
			WHEN '001' THEN (T.PRIMERNOMBRE + (CASE ISNULL(T.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDONOMBRE + ' ' END) + T.PRIMERAPELLIDO + (CASE ISNULL(T.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDOAPELLIDO END))
			ELSE T.RAZONSOCIAL END) COLLATE DATABASE_DEFAULT AS NombreCompleto,
		'' AS Regional, '' AS Dependencia, '' AS Cargo, '' AS Direccion, 
		'' AS Telefono, C.NumeroContrato AS IdNumeroContratoInterventoria, 
		'' AS Celular, T.CORREOELECTRONICO COLLATE DATABASE_DEFAULT AS CorreoElectronico, 
		SIC.[UsuarioCrea], SIC.[FechaCrea], SIC.[UsuarioModifica], SIC.[FechaModifica]
		FROM [CONTRATO].[SupervisorInterContrato] SIC 
		INNER JOIN [CONTRATO].[TipoSuperInter] TSI ON TSI.IDTipoSuperInter = SIC.IDTipoSuperInter
		INNER JOIN [CONTRATO].[TipoSupervisorInterventor] SI ON SI.Codigo = TSI.Codigo
		INNER JOIN [PROVEEDOR].[EntidadProvOferente] EPO ON EPO.IdEntidad = SIC.IDProveedoresInterventor
		INNER JOIN [Oferente].[TERCERO] T ON T.[IDTERCERO] = EPO.IdTercero 
		INNER JOIN [Oferente].[TipoPersona] TP ON TP.IdTipoPersona=T.IdTipoPersona 
		LEFT OUTER JOIN [CONTRATO].[Contrato] C ON C.IdContrato = SIC.IdNumeroContratoInterventoria
		WHERE SIC.IdContrato = @IdContrato
	END
	ELSE
	BEGIN
		SELECT 0 AS IDSupervisorIntervContrato, NULL AS EtQSupervisorInterventor, NULL AS EtQInternoExterno, NULL AS FechaInicio, 
		td.[NomTipoDocumento] AS TipoIdentificacion, DI.[NumeroIdentificacion] AS Identificacion, '' AS TipoVinculacionContractual, 
		DI.[PrimerNombre] + (CASE ISNULL(DI.[SegundoNombre], '') WHEN '' THEN ' ' ELSE ' ' + DI.[SegundoNombre] + ' ' END) + DI.[PrimerApellido] + (CASE ISNULL(DI.[SegundoApellido], '') WHEN '' THEN ' ' ELSE ' ' + DI.[SegundoApellido] END) AS NombreCompleto, 
		NULL AS Regional, 
		NULL AS Dependencia, NULL AS Cargo, NULL AS Direccion, [Telefono] AS Telefono, NULL AS IdNumeroContratoInterventoria,
		[Celular] AS Celular, [CorreoElectronico] AS CorreoElectronico,
		DI.[UsuarioCrea], DI.[FechaCrea], DI.[UsuarioModifica], DI.[FechaModifica]
		FROM [CONTRATO].[SupervisorInterContrato] SIC
		INNER JOIN [CONTRATO].[DirectorInterventoria] DI ON DI.[IDDirectorInterventoria] = SIC.[IDDirectorInterventoria]
		INNER JOIN [Global].[TiposDocumentos] td ON td.[IdTipoDocumento] = DI.[IdTipoIdentificacion]
		WHERE SIC.IdContrato = @IdContrato
	END

END

GO


