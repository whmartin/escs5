USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_EstadosGarantias_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_EstadosGarantias_Modificar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/25/2014 5:10:11 PM
-- Description:	Procedimiento almacenado que actualiza un(a) EstadosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_EstadosGarantias_Modificar]
		@IDEstadosGarantias INT,	@CodigoEstadoGarantia NVARCHAR(8),	@DescripcionEstadoGarantia NVARCHAR(80), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.EstadosGarantias SET CodigoEstadoGarantia = @CodigoEstadoGarantia, DescripcionEstadoGarantia = @DescripcionEstadoGarantia, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IDEstadosGarantias = @IDEstadosGarantias
END
