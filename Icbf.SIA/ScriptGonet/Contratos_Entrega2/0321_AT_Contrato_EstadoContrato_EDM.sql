USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  18/06/2014 11:15
-- Description:	Insertar campo [UsuarioCrea] de la tabla Contrato.EstadoContrato
-- =============================================

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'EstadoContrato' 
           AND  COLUMN_NAME = 'UsuarioCrea')

	ALTER TABLE Contrato.EstadoContrato
	ADD
	   [UsuarioCrea] [nvarchar](250) NOT NULL
	   	
GO