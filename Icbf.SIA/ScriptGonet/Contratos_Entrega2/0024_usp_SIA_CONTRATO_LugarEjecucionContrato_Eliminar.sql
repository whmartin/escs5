USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Eliminar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Eliminar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/14/2014 4:20:50 PM
-- Description:	Procedimiento almacenado que elimina un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Eliminar]
	@IdLugarEjecucionContratos INT
AS
BEGIN
	DELETE CONTRATO.LugarEjecucionContrato
	WHERE IdLugarEjecucionContratos = @IdLugarEjecucionContratos
END
