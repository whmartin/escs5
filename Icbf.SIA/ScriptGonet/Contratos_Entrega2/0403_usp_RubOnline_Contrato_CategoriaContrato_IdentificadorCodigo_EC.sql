USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_IdentificadorCodigo]    Script Date: 10/06/2014 03:22:42 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_CategoriaContrato_IdentificadorCodigo')
BEGIN
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_IdentificadorCodigo]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_CategoriaContrato_IdentificadorCodigo]    Script Date: 10/06/2014 03:22:42 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 10/06/2014
-- Description:	Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos) de la categoria contrato la 
-- informaci�n del mismo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_IdentificadorCodigo]
	@IdCategoriaContrato INT = NULL,
	@CodigoCategoriaContrato NVARCHAR(5) = NULL
AS
BEGIN
	
	SELECT IdCategoriaContrato, CodigoCategoriaContrato, NombreCategoriaContrato, Descripcion, Estado, UsuarioCrea, 
      FechaCrea, UsuarioModifica, FechaModifica
	FROM CONTRATO.CategoriaContrato
	WHERE IdCategoriaContrato = ISNULL(@IdCategoriaContrato, IdCategoriaContrato) AND
	CodigoCategoriaContrato = ISNULL(@CodigoCategoriaContrato, CodigoCategoriaContrato)

END

GO


