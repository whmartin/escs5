
USE [SIA]
GO

-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date: 21/08/2014
-- Description:	Insertar opci�n de Clausula Contrato en la tabla TablaParametrica
-- =============================================
IF not exists(SELECT IdTablaParametrica from Contrato.TablaParametrica where NombreTablaParametrica = 'Clausula Contrato')
Begin
INSERT INTO Contrato.TablaParametrica
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           )
     VALUES
           ('03'
           ,'Clausula Contrato'
           ,'Contratos/ClausulaContrato'
           ,1
           ,'Administrador'
           ,GETDATE()
           )
End