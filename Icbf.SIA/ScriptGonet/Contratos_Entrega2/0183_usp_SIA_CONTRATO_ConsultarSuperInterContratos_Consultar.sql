USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContratos_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  6/6/2014 4:39:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) ConsultarSuperInterContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContratos_Consultar]
	@CodSupervisorInterventor nvarchar(4) = NULL,
	@NumeroContrato NVARCHAR(25) = NULL,
	@NumeroIdentificacion NVARCHAR(100) = NULL,
	@RazonSocial NVARCHAR(100) = NULL,
	@NombreSuperInterv NVARCHAR(100) = NULL,
	@NumIdentifDirInterventoria NVARCHAR(100) = NULL,
	@NombreDirInterventoria NVARCHAR(50) = NULL
	
AS
BEGIN
 

 IF (EXISTS(SELECT TSI.codigo FROM [CONTRATO].[SupervisorInterContrato] SI
     INNER JOIN [CONTRATO].[TipoSuperInter] TSI ON SI.IDTipoSuperInter = TSI.IDTipoSuperInter 
	 WHERE TSI.codigo = @CodSupervisorInterventor AND TSI.codigo = '02'))--Externo (Interventor)
	 BEGIN
SELECT DISTINCT DT.IdEntidad, DT.IdTercero, DT.NombreTipoPersona, DT.CodDocumento, DT.NumeroIdentificacion, DT.NombreRazonsocial 
  ,DT.IdTipoPersona, DT.IDTIPODOCIDENTIFICA
  , DT.CORREOELECTRONICO, DT.IDSupervisorIntervContrato, DT.NumeroTelefono, DT.DireccionComercial
  ,DT.SupervisorInterventor, DT.NumeroContrato, DT.TipoContrato, DT.RegionalContrato
  ,DT.FechaSuscripcionContrato, DT.FechaFinalTerminacionContrato, DT.FechaInicio, DT.FechaFinalizacionSuperInterv, DT.FechaModifSuperInter
  ,DT.NumeroContratoInterventoria, DT.CodigoSuperInter
  , DT.UsuarioCrea, DT.FechaCrea, DT.UsuarioModifica, DT.FechaModifica
  ,DT.PRIMERNOMBRE, DT.SEGUNDONOMBRE, DT.PRIMERAPELLIDO, DT.SEGUNDOAPELLIDO, DT.RAZONSOCIAL
  FROM (
  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, 
  ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as NombreRazonsocial,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor
	,T.PRIMERNOMBRE, ISNULL(T.SEGUNDONOMBRE,'') AS SEGUNDONOMBRE, T.PRIMERAPELLIDO, ISNULL(T.SEGUNDOAPELLIDO,'') AS SEGUNDOAPELLIDO, '' AS RAZONSOCIAL
	, T.CORREOELECTRONICO, SI.IDSupervisorIntervContrato, TelT.NumeroTelefono, IAE.DireccionComercial
	, TSI.codigo AS CodigoSuperInter, CASE WHEN TSI.codigo = '01' THEN 'Supervisor' WHEN TSI.codigo = '02' THEN 'Interventor' ELSE '' END AS SupervisorInterventor 
	, C.NumeroContrato, TC.NombreTipoContrato AS TipoContrato, Reg.NombreRegional AS RegionalContrato
	, C.FechaSuscripcionContrato, C.FechaFinalTerminacionContrato
	, SI.FechaInicio, NULL AS FechaFinalizacionSuperInterv, SI.FechaModifica AS FechaModifSuperInter
	, CI.NumeroContrato AS NumeroContratoInterventoria
	, SI.UsuarioCrea, SI.FechaCrea, SI.UsuarioModifica, SI.FechaModifica
  FROM [CONTRATO].[SupervisorInterContrato] SI
     INNER JOIN [CONTRATO].[TipoSuperInter] TSI ON SI.IDTipoSuperInter = TSI.IDTipoSuperInter
	 LEFT JOIN [CONTRATO].[Contrato] C ON SI.IdContrato = C.IdContrato
	 LEFT JOIN [CONTRATO].[TipoContrato] TC ON C.IdTipoContrato = TC.IdTipoContrato
	 LEFT JOIN [DIV].[REgional] Reg ON C.IdRegionalContrato = Reg.IdRegional
	 LEFT JOIN [CONTRATO].[Contrato] CI ON SI.IdNumeroContratoInterventoria = CI.IdContrato
	 INNER JOIN	[Proveedor].[EntidadProvOferente] EP ON SI.IDProveedoresInterventor = EP.IdEntidad
     INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
     LEFT JOIN [Oferente].[TelTerceros] TelT ON T.IDTERCERO = TelT.IdTercero
     WHERE T.IdTipoPersona=1
UNION 
 SELECT  EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, 
 ( T.RAZONSOCIAL ) as NombreRazonsocial,
		E.Descripcion AS Estado ,EP.IdEstado ,EP.ObserValidador, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor
		,''	AS PRIMERNOMBRE, '' AS SEGUNDONOMBRE, '' AS PRIMERAPELLIDO, '' as SEGUNDOAPELLIDO, T.RAZONSOCIAL
		, T.CORREOELECTRONICO, SI.IDSupervisorIntervContrato, TelT.NumeroTelefono, IAE.DireccionComercial
		, TSI.codigo AS CodigoSuperInter, CASE WHEN TSI.codigo = '01' THEN 'Supervisor' WHEN TSI.codigo = '02' THEN 'Interventor' ELSE '' END AS SupervisorInterventor
		, C.NumeroContrato, TC.NombreTipoContrato AS TipoContrato, Reg.NombreRegional AS RegionalContrato
	, C.FechaSuscripcionContrato, C.FechaFinalTerminacionContrato
	, SI.FechaInicio, NULL AS FechaFinalizacionSuperInterv, SI.FechaModifica AS FechaModifSuperInter
	, CI.NumeroContrato AS NumeroContratoInterventoria
	, SI.UsuarioCrea, SI.FechaCrea, SI.UsuarioModifica, SI.FechaModifica
  FROM [CONTRATO].[SupervisorInterContrato] SI
	INNER JOIN [CONTRATO].[TipoSuperInter] TSI ON SI.IDTipoSuperInter = TSI.IDTipoSuperInter
	LEFT JOIN [CONTRATO].[Contrato] C ON SI.IdContrato = C.IdContrato
	 LEFT JOIN [CONTRATO].[TipoContrato] TC ON C.IdTipoContrato = TC.IdTipoContrato
	 LEFT JOIN [DIV].[REgional] Reg ON C.IdRegionalContrato = Reg.IdRegional
	 LEFT JOIN [CONTRATO].[Contrato] CI ON SI.IdNumeroContratoInterventoria = CI.IdContrato
	INNER JOIN [Proveedor].[EntidadProvOferente] EP ON SI.IDProveedoresInterventor = EP.IdEntidad
    INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
    LEFT JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
    INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona
    INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
    INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
    INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
    LEFT JOIN [Oferente].[TelTerceros] TelT ON T.IDTERCERO = TelT.IdTercero
    WHERE T.IdTipoPersona=2
    ) DT
	WHERE (@CodSupervisorInterventor IS NULL OR DT.CodigoSuperInter = @CodSupervisorInterventor)
	AND (@NumeroContrato IS NULL OR DT.NumeroContrato = @NumeroContrato)
	AND (@NumeroIdentificacion IS NULL OR DT.NumeroIdentificacion = @NumeroIdentificacion)
	AND (@RazonSocial IS NULL OR DT.RAZONSOCIAL LIKE '%' + @RazonSocial + '%')
	AND (@NombreSuperInterv IS NULL OR (DT.NombreRazonsocial like CASE WHEN DT.IdTipoPersona = 1 THEN  '%' + @NombreSuperInterv + '%' ELSE '' END ))
	
	END

ELSE IF (EXISTS(SELECT TSI.codigo FROM [CONTRATO].[SupervisorInterContrato] SI
     INNER JOIN [CONTRATO].[TipoSuperInter] TSI ON SI.IDTipoSuperInter = TSI.IDTipoSuperInter 
	 WHERE TSI.codigo = @CodSupervisorInterventor AND TSI.codigo = '01' ))--Interno (Supervisor)
BEGIN
SELECT  *

FROM
(select 
 0 AS IdEntidad, 0 AS IdTercero, 'Natural' AS NombreTipoPersona, Em.desc_ide AS CodDocumento, Em.num_iden AS NumeroIdentificacion
 , (Em.nom_emp1 + ' ' + Em.nom_emp2 + ' ' + Em.ape_emp1 + ' ' + Em.ape_emp2) AS NombreRazonsocial 
  ,1 AS IdTipoPersona, 1 AS IDTIPODOCIDENTIFICA
  ,  ISNULL(Em.eee_mail,'') AS CORREOELECTRONICO, SI.IDSupervisorIntervContrato AS IDSupervisorIntervContrato, Em.telefono AS NumeroTelefono, Em.direccion AS DireccionComercial
  ,'Supervisor' AS SupervisorInterventor, C.NumeroContrato AS NumeroContrato, TC.NombreTipoContrato AS TipoContrato, Reg.NombreRegional AS RegionalContrato
  , C.FechaSuscripcionContrato AS FechaSuscripcionContrato, C.FechaFinalTerminacionContrato AS FechaFinalTerminacionContrato, SI.FechaInicio AS FechaInicio, NULL AS FechaFinalizacionSuperInterv, NULL AS FechaModifSuperInter
  ,CI.NumeroContrato AS NumeroContratoInterventoria, '01' AS CodigoSuperInter
  , 'Administrador' AS UsuarioCrea, GETDATE() AS FechaCrea, NULL AS UsuarioModifica, NULL AS FechaModifica
  ,Em.nom_emp1 AS PRIMERNOMBRE, Em.nom_emp2 AS SEGUNDONOMBRE, Em.ape_emp1 AS PRIMERAPELLIDO, Em.ape_emp2 AS SEGUNDOAPELLIDO, '' AS RAZONSOCIAL
--select
-- Em.ID_Ident, desc_ide AS TipoIdentificacion, Em.num_iden AS NumeroIdentificacion, Em.desc_vin AS TipoVinculacion
--,(Em.nom_emp1 + ' ' + Em.nom_emp2 + ' ' + Em.ape_emp1 + ' ' + Em.ape_emp2) AS NombreCompleto
--,Em.nom_emp1 AS PrimerNombre, Em.nom_emp2 AS SegundoNombre, Em.ape_emp1 AS PrimerApellido, Em.ape_emp2 AS SegundoApellido
--,Em.ID_regio, Em.regional, Em.nom_depe AS Dependencia, Em.direccion, telefono, ISNULL(Em.eee_mail,'') AS CorreoE
--,Em.Desc_car AS Cargo, Em.num_iden AS IdEmpleado 
 from [KACTUS].[KPRODII].[dbo].[Da_Emple] Em 
 INNER JOIN [CONTRATO].[SupervisorInterContrato] SI ON SI.IDEmpleadosSupervisor = Em.num_iden
 LEFT JOIN [CONTRATO].[Contrato] C ON SI.IdContrato = C.IdContrato
	 LEFT JOIN [CONTRATO].[TipoContrato] TC ON C.IdTipoContrato = TC.IdTipoContrato
	 LEFT JOIN [DIV].[REgional] Reg ON C.IdRegionalContrato = Reg.IdRegional
	 LEFT JOIN [CONTRATO].[Contrato] CI ON SI.IdNumeroContratoInterventoria = CI.IdContrato) DT
	 WHERE (@CodSupervisorInterventor IS NULL OR DT.CodigoSuperInter = @CodSupervisorInterventor)
	AND (@NumeroContrato IS NULL OR DT.NumeroContrato = @NumeroContrato)
	AND (@NumeroIdentificacion IS NULL OR DT.NumeroIdentificacion = @NumeroIdentificacion)
	AND (@RazonSocial IS NULL OR DT.RAZONSOCIAL LIKE '%' + @RazonSocial + '%')
	AND (@NombreSuperInterv IS NULL OR (DT.NombreRazonsocial like CASE WHEN IdTipoPersona = 1 THEN  '%' + @NombreSuperInterv + '%' ELSE '' END ))
	
END
END
