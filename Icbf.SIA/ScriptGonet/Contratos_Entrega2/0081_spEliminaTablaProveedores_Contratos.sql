USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Eliminacion Tabla [CONTRATO].[Proveedores_Contratos]
-- =============================================

/****** Object:  Table [CONTRATO].[Proveedores_Contratos]    Script Date: 05/27/2014 11:37:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[Proveedores_Contratos]') AND type in (N'U'))
DROP TABLE [CONTRATO].[Proveedores_Contratos]
GO

