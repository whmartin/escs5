USE [SIA]
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Eliminar]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Eliminar]
GO

-- =============================================
-- Author:		
-- Create date:  7/1/2014 14:01:01
-- Description:	Procedimiento almacenado que elimina un(a) DirectorInterventoria
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Eliminar]
    @IDDirectorInterventoria INT
AS 
    BEGIN
        DELETE  CONTRATO.DirectorInterventoria
        WHERE   IDDirectorInterventoria = @IDDirectorInterventoria
    END

