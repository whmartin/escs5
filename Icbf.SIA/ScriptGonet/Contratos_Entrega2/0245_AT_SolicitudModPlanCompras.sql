use sia

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/22/2014 1:36:08 PM
-- Description:	Alterar tabla CONTRATO.SolicitudModPlanCompras
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].FK_SolicitudModPlanCompras_IdVigencia') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN	
	ALTER table CONTRATO.SolicitudModPlanCompras  drop constraint FK_SolicitudModPlanCompras_IdVigencia
END


IF EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.SolicitudModPlanCompras') AND NAME = ('IdVigencia'))
BEGIN
	ALTER table CONTRATO.SolicitudModPlanCompras drop column IdVigencia 
END
