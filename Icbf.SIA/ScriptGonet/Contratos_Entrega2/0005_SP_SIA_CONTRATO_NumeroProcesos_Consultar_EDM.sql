USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_NumeroProcesos_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Consultar]
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/7/2014 5:55:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) NumeroProcesos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Consultar]
	@IdNumeroProceso INT
AS
BEGIN
	SELECT
		IdNumeroProceso,
		NumeroProceso,
		Inactivo,
		NumeroProcesoGenerado,
		IdModalidadSeleccion,
		IdRegional,
		IdVigencia,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].[NumeroProcesos]
	WHERE IdNumeroProceso = @IdNumeroProceso
END
