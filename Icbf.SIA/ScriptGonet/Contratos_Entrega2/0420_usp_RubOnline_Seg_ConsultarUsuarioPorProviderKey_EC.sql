USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]    Script Date: 01/07/2014 08:58:01 a.m. ******/
DROP PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]    Script Date: 01/07/2014 08:58:01 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 03-10-2012
-- Description:	Procedimiento almacenado que consulta un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
--				 2014/07/01 - Emilio Calapi�a - Se agregan columnas IdRegional, IdTipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey]
	@ProviderKey varchar(125)
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
	  ,SEG.Usuario.IdRegional
	  ,SEG.Usuario.IdTipoUsuario
  FROM SEG.Usuario 
  WHERE
	providerKey = @ProviderKey
END



GO


