USE [SIA]
GO

IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Modificar]')
			AND type IN (
				N'P'
				,N'PC'
				)
		)
	DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Modificar]
GO

-- =============================================
-- Author:	Eduardo Isaac Ballesteros
-- Create date:  7/4/2014 9:12:33 AM
-- Description:	Procedimiento almacenado que actualiza un(a) VigenciaFuturas
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Modificar] @IDVigenciaFuturas INT
	,@NumeroRadicado NVARCHAR(80)
	,@FechaExpedicion DATETIME
	,@IdContrato INT
	,@ValorVigenciaFutura NUMERIC(30, 2)
	,@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE CONTRATO.VigenciaFuturas
	SET NumeroRadicado = @NumeroRadicado
		,FechaExpedicion = @FechaExpedicion
		,IdContrato = @IdContrato
		,ValorVigenciaFutura = @ValorVigenciaFutura
		,UsuarioModifica = @UsuarioModifica
		,FechaModifica = GETDATE()
	WHERE IDVigenciaFuturas = @IDVigenciaFuturas
END

