USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContratoAsociado_IdentificadorCodigo]    Script Date: 22/06/2014 03:49:25 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_TipoContratoAsociado_IdentificadorCodigo')
BEGIN
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContratoAsociado_IdentificadorCodigo]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContratoAsociado_IdentificadorCodigo]    Script Date: 22/06/2014 03:49:25 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 22/06/2014
-- Description:	Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos) del
-- TipoContratoAsociado la informaci�n del mismo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContratoAsociado_IdentificadorCodigo]
	@IdTipoContratoAsociado INT = NULL,
	@CodigoTipoContratoAsociado VARCHAR(30) = NULL
AS
BEGIN
	
	SELECT IdContratoAsociado, CodContratoAsociado, Descripcion, Inactivo
	FROM CONTRATO.TipoContratoAsociado
	WHERE IdContratoAsociado = ISNULL(@IdTipoContratoAsociado, IdContratoAsociado) AND
	CodContratoAsociado = ISNULL(@CodigoTipoContratoAsociado, CodContratoAsociado)

END

GO


