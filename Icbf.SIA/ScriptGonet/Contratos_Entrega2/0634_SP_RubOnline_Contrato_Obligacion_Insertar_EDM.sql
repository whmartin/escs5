USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]    Script Date: 16/09/2014 03:24:02 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]
GO


   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que inserta Obligacion
   -- =============================================
   -- =============================================
-- Author:		Gonet\Efrain Diaz
-- Create date:  15/09/2014 
-- Description:	se agrego procedimiento para controlar los registros repetidos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]
		@IdObligacion INT OUTPUT, 	
		@IdTipoObligacion INT,	
		@IdTipoContrato INT,	
		@Descripcion nvarchar(MAX),	
		@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN

	IF NOT EXISTS (SELECT IdObligacion FROM Contrato.Obligacion WHERE IdTipoObligacion = @IdTipoObligacion 
	and IdTipoContrato = @IdTipoContrato and Descripcion = @Descripcion)
		BEGIN
			INSERT INTO Contrato.Obligacion(IdTipoObligacion, IdTipoContrato, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoObligacion, @IdTipoContrato, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
			SELECT @IdObligacion = @@IDENTITY 	
		END
		ELSE
		BEGIN
			RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
						16, -- Severity.
						1, -- State.
						2627);
	END


		
END





