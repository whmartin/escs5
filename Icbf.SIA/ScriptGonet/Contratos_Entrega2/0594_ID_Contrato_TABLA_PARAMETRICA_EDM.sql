
USE [SIA]
GO

-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date: 22/08/2014
-- Description:	Insertar opci�n de Objeto Contrato en la tabla TablaParametrica
-- =============================================
IF not exists(SELECT IdTablaParametrica from Contrato.TablaParametrica where NombreTablaParametrica = 'Objeto Contrato')
Begin
INSERT INTO Contrato.TablaParametrica
           ([CodigoTablaParametrica]
           ,[NombreTablaParametrica]
           ,[Url]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           )
     VALUES
           ('03'
           ,'Objeto Contrato'
           ,'Contratos/ObjetoContrato'
           ,1
           ,'Administrador'
           ,GETDATE()
           )
End