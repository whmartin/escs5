USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contratos_Consultar_lupaPlanCompras]    Script Date: 15/07/2014 13:23:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contratos_Consultar_lupaPlanCompras]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar_lupaPlanCompras]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar_lupaPlanCompras] 
	@IdContrato INT = NULL,
	@NumeroContrato nvarchar (50) = NULL
AS
BEGIN
	SELECT IdContrato, NumeroContrato FROM [CONTRATO].[Contrato]
	WHERE (@IdContrato IS NULL OR IdContrato = @IdContrato)
	AND (@NumeroContrato IS NULL OR NumeroContrato = @NumeroContrato)
END

