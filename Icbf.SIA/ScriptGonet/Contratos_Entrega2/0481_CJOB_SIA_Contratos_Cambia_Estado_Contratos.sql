USE [msdb]
GO


-- =============================================
-- Author: Eduardo Isaac Ballesteros Mu�oz		
-- Create date:  07/10/2014 09:00
-- Description: SIA CU_026_CONT-ESTADOCONTRATO  Creacion de job programacion todos los dias 7:00pm
-- =============================================

DECLARE @pjob_id varchar(250)

SELECT @pjob_id=job_id FROM msdb.dbo.sysjobs_view WHERE name = N'SIA_Contratos_Cambia_Estado_Contratos'

/****** Object:  Job [SIA_Contratos_Cambia_Estado_Contratos]    Script Date: 16/07/2014 11:05:27 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'SIA_Contratos_Cambia_Estado_Contratos')
EXEC msdb.dbo.sp_delete_job @job_id=@pjob_id, @delete_unused_schedule=1
GO

/****** Object:  Job [SIA_Contratos_Cambia_Estado_Contratos]    Script Date: 16/07/2014 11:05:27 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 16/07/2014 11:05:27 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
select @jobId = job_id from msdb.dbo.sysjobs where (name = N'SIA_Contratos_Cambia_Estado_Contratos')
if (@jobId is NULL)
BEGIN
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'SIA_Contratos_Cambia_Estado_Contratos', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job ejecutado para cambiar el estado del contrato automaticamente', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'Prueba', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END
/****** Object:  Step [SP_usp_SIA_CONTRATO_Contrato_Modificar_Estado]    Script Date: 16/07/2014 11:05:28 ******/
IF NOT EXISTS (SELECT * FROM msdb.dbo.sysjobsteps WHERE job_id = @jobId and step_id = 1)
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SP_usp_SIA_CONTRATO_Contrato_Modificar_Estado', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec usp_SIA_CONTRATO_Contrato_Modificar_Estado', 
		@database_name=N'SIA', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'prog_7PM', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20140716, 
		@active_end_date=99991231, 
		@active_start_time=190000, 
		@active_end_time=235959, 
		@schedule_uid=N'e253c776-096d-444f-9dd4-0486dfc17531'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


