use SIA

-- =============================================
-- Author:		Gonet
-- Create date: 2014-07-09
-- Description:	Creacion tabla [CONTRATO].[PlanDeComprasContratos]
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].FK_SolicitudModPlanCompras_IdPlanDeCompras') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN	
	alter table [CONTRATO].[SolicitudModPlanCompras] drop constraint FK_SolicitudModPlanCompras_IdPlanDeCompras
END

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[PlanDeCompras]') AND type in (N'U'))
DROP TABLE [CONTRATO].[PlanDeCompras]
GO


/****** Object:  Table [CONTRATO].[PlanDeComprasContratos]    Script Date: 25/06/2014 10:15:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[PlanDeComprasContratos]') AND type in (N'U'))
DROP TABLE [CONTRATO].[PlanDeComprasContratos]
GO


CREATE TABLE [CONTRATO].[PlanDeComprasContratos]
(
 [IDPlanDeComprasContratos] Int identity(1,1) NOT NULL,
 [IdContrato] Int NOT NULL,
 [IDPlanDeCompras] Int NULL,
 [Vigencia] Varchar(8) NOT NULL,
 [UsuarioCrea] [nvarchar](250) NOT NULL,
[FechaCrea] [datetime] NOT NULL,
[UsuarioModifica] [nvarchar](250) NULL,
[FechaModifica] [datetime] NULL,
)

ALTER TABLE [CONTRATO].[PlanDeComprasContratos] ADD CONSTRAINT [PK_PlanDeComprasContratos_IDPlanDeComprasContratos] PRIMARY KEY ([IDPlanDeComprasContratos])
go

TRUNCATE table [CONTRATO].[SolicitudModPlanCompras]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdPlanDeCompras]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudModPlanCompras_IdPlanDeCompras] FOREIGN KEY([IdPlanDeCompras])
REFERENCES [CONTRATO].PlanDeComprasContratos (IDPlanDeComprasContratos)
GO



IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_PlanDeComprasContratos_IdContrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[PlanDeComprasContratos]'))
ALTER TABLE [CONTRATO].[PlanDeComprasContratos] ADD  CONSTRAINT [FK_PlanDeComprasContratos_IdContrato] FOREIGN KEY([IdContrato])
REFERENCES [CONTRATO].Contrato ([IdContrato])
GO


