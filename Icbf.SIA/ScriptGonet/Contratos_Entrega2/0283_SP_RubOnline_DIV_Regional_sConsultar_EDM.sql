USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Regional_sConsultar]    Script Date: 01/07/2014 02:18:52 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_DIV_Regional_sConsultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_sConsultar]
GO


-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/10/2012 11:52:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Regional
-- =============================================
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  12/15/2012 12:15:00 PM
-- Description:	Procedimiento almacenado que consulta un(a) Regional Ordenado por Nombre DESC
-- =============================================
-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  27/06/2014 16:20
-- Description:	Se agreg� el campo concatenado de codigo regional con nombre de regional
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_sConsultar]
	@CodigoRegional NVARCHAR(2) = NULL,@NombreRegional NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdRegional, CodigoRegional, upper(NombreRegional) as NombreRegional, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica,
 rtrim(ltrim(CodigoRegional)) +'-'+ rtrim(ltrim(upper(NombreRegional))) as CodigoNombreRegional
 FROM [DIV].[Regional] WHERE CodigoRegional = CASE WHEN @CodigoRegional IS NULL THEN CodigoRegional ELSE @CodigoRegional END 
 AND NombreRegional = CASE WHEN @NombreRegional IS NULL THEN NombreRegional ELSE @NombreRegional END ORDER BY NombreRegional

END


