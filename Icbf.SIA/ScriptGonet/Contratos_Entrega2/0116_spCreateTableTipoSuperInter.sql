USE [SIA]
GO
-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/30/2014 12:06:42 PM
-- Creacion de table TipoSuperInter
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[TipoSuperInter]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla TipoSuperInter a crear'
RETURN
END
CREATE TABLE [CONTRATO].[TipoSuperInter](
 [IDTipoSuperInter] [INT]  IDENTITY(1,1) NOT NULL,
 [Codigo] [NVARCHAR] (4) NOT NULL,
 [Descripcion] [NVARCHAR] (100) NOT NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_TipoSuperInter] PRIMARY KEY CLUSTERED 
(
 	[IDTipoSuperInter] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.TipoSuperInter') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Almacena datos sobre TipoSuperInter', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = TipoSuperInter;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.TipoSuperInter') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDTipoSuperInter' AND [object_id] = OBJECT_ID('CONTRATO.TipoSuperInter')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = TipoSuperInter,
@level2type = N'COLUMN', @level2name = IDTipoSuperInter;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.TipoSuperInter') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Codigo' AND [object_id] = OBJECT_ID('CONTRATO.TipoSuperInter')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'CodigoTipoSuperInter', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = TipoSuperInter,
@level2type = N'COLUMN', @level2name = Codigo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.TipoSuperInter') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Descripcion' AND [object_id] = OBJECT_ID('CONTRATO.TipoSuperInter')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'DescripcionTipoSuperInter', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = TipoSuperInter,
@level2type = N'COLUMN', @level2name = Descripcion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.TipoSuperInter') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('CONTRATO.TipoSuperInter')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = TipoSuperInter,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.TipoSuperInter') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('CONTRATO.TipoSuperInter')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = TipoSuperInter,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.TipoSuperInter') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('CONTRATO.TipoSuperInter')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = TipoSuperInter,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.TipoSuperInter') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('CONTRATO.TipoSuperInter')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = TipoSuperInter,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
