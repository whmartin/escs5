USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_Contratos_SuscritosUsuario]    Script Date: 09/07/2014 03:36:34 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Contratos_SuscritosUsuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Contratos_SuscritosUsuario]
GO

/****** Object:  StoredProcedure [dbo].[usp_Contratos_SuscritosUsuario]    Script Date: 09/07/2014 03:36:34 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet\Carlos Cardenas
-- Description:	Procedimiento almacenado que consulta los contratos suscrito dependiendo el usuario

-- =============================================
CREATE PROCEDURE [dbo].[usp_Contratos_SuscritosUsuario]
		@FechaRegistroDesde DATETIME = NULL,
		@FechaRegistroHasta DATETIME = NULL,
		@NumeroContratoConvenio VARCHAR (50) = NULL,
		@VigenciaFiscalInicial VARCHAR = NULL,
		@RegionalContrato INT = NULL,
		@CategoriaContrato INT = NULL,
		@TipoContrato INT = NULL,
		@IdUsuario INT = NULL
	AS
	BEGIN
	
		 DECLARE @IdTipoUsuario INT
		 DECLARE @IdRegionalUsuario INT
		 DECLARE @IdRegional INT = NULL
		 DECLARE @IdVigenciaInicial INT

		 SELECT @IdRegionalUsuario=IdRegional,@IdTipoUsuario=IdTipoUsuario FROM SEG.Usuario WHERE IdUsuario=@IdUsuario
		 IF @IdTipoUsuario=2 BEGIN SET @IdRegional=@IdRegionalUsuario END

		 Select @IdVigenciaInicial=IdVigencia from global.Vigencia where AcnoVigencia = @VigenciaFiscalInicial
		
		IF @IdRegional IS NOT NULL
		BEGIN
			select cont.IdContrato, cont.FechaCrea,cont.NumeroContrato,cont.IdVigenciaInicial,vig.AcnoVigencia,
			cont.IdRegionalContrato,reg.NombreRegional as Regional,
			cont.IdCategoriaContrato, catc.NombreCategoriaContrato,
			cont.IdTipoContrato,TiCon.NombreTipoContrato
			from div.Regional reg 
			INNER JOIN CONTRATO.Contrato cont ON cont.IdRegionalContrato = reg.IdRegional
			INNER JOIN Global.Vigencia vig ON cont.IdVigenciaInicial = vig.IdVigencia
			INNER JOIN CONTRATO.CategoriaContrato catc ON cont.IdCategoriaContrato = catc.IdCategoriaContrato
			INNER JOIN CONTRATO.TipoContrato TiCon ON Cont.IdTipoContrato = TiCon.IdTipoContrato
			where reg.IdRegional=@IdRegional
			AND cont.Suscrito = 1
			AND ( cont.FechaCrea BETWEEN @FechaRegistroDesde AND @FechaRegistroHasta OR @FechaRegistroDesde IS NULL OR @FechaRegistroHasta IS NULL )
			AND cont.NumeroContrato = CASE WHEN @NumeroContratoConvenio IS NULL THEN cont.NumeroContrato ELSE @NumeroContratoConvenio END
			AND cont.IdVigenciaInicial = CASE WHEN @IdVigenciaInicial IS NULL THEN cont.IdVigenciaInicial ELSE @IdVigenciaInicial END 
			AND cont.IdRegionalContrato = CASE WHEN @RegionalContrato IS NULL THEN cont.IdRegionalContrato ELSE @RegionalContrato END
			AND cont.IdCategoriaContrato = CASE WHEN @CategoriaContrato IS NULL THEN cont.IdCategoriaContrato ELSE @CategoriaContrato END 
			AND cont.IdTipoContrato = CASE WHEN @TipoContrato IS NULL THEN cont.IdTipoContrato ELSE @TipoContrato END 
		END
		ELSE
		BEGIN
			select cont.IdContrato, cont.FechaCrea,cont.NumeroContrato,cont.IdVigenciaInicial,vig.AcnoVigencia,
			cont.IdRegionalContrato,reg.NombreRegional as Regional,
			cont.IdCategoriaContrato, catc.NombreCategoriaContrato,
			cont.IdTipoContrato,TiCon.NombreTipoContrato
			from div.Regional reg 
			INNER JOIN CONTRATO.Contrato cont ON cont.IdRegionalContrato = reg.IdRegional
			INNER JOIN Global.Vigencia vig ON cont.IdVigenciaInicial = vig.IdVigencia
			INNER JOIN CONTRATO.CategoriaContrato catc ON cont.IdCategoriaContrato = catc.IdCategoriaContrato
			INNER JOIN CONTRATO.TipoContrato TiCon ON Cont.IdTipoContrato = TiCon.IdTipoContrato
			WHERE cont.Suscrito = 1
			AND ( cont.FechaCrea BETWEEN @FechaRegistroDesde AND @FechaRegistroHasta OR @FechaRegistroDesde IS NULL OR @FechaRegistroHasta IS NULL )
			AND cont.NumeroContrato = CASE WHEN @NumeroContratoConvenio IS NULL THEN cont.NumeroContrato ELSE @NumeroContratoConvenio END
			AND cont.IdVigenciaInicial = CASE WHEN @IdVigenciaInicial IS NULL THEN cont.IdVigenciaInicial ELSE @IdVigenciaInicial END 
			AND cont.IdRegionalContrato = CASE WHEN @RegionalContrato IS NULL THEN cont.IdRegionalContrato ELSE @RegionalContrato END
			AND cont.IdCategoriaContrato = CASE WHEN @CategoriaContrato IS NULL THEN cont.IdCategoriaContrato ELSE @CategoriaContrato END 
			AND cont.IdTipoContrato = CASE WHEN @TipoContrato IS NULL THEN cont.IdTipoContrato ELSE @TipoContrato END 
	
		END

		IF @@ROWCOUNT >= 500
		BEGIN
			RAISERROR('La consulta arroja demasiados resultados,por favor refine su consulta',16,1)
			RETURN
		END
	END



