USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Da_Emple_Consultar_Empleado]    Script Date: 24/06/2014 05:23:58 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_KACTUS_KPRODII_Da_Emple_Consultar_Empleado')
BEGIN
DROP PROCEDURE [dbo].[usp_KACTUS_KPRODII_Da_Emple_Consultar_Empleado]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Da_Emple_Consultar_Empleado]    Script Date: 24/06/2014 05:23:58 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 24/06/2014
-- Description:	
-- =============================================
-- usp_KACTUS_KPRODII_Da_Emple_Consultar_Empleado '396079', '1', '10180', '312418'
CREATE PROCEDURE [dbo].[usp_KACTUS_KPRODII_Da_Emple_Consultar_Empleado]
	@NumeroIdentificacion INT,
	@IdRegional INT = NULL,
	@IdDependencia INT = NULL,
	@IdCargo INT = NULL
AS
BEGIN
	
	SELECT desc_ide AS TipoIdentificacion, num_iden AS NumeroIdentificacion, 
	desc_vin AS TipoVinculacionContractual, 
	nom_emp1 + (CASE ISNULL(nom_emp2, '') WHEN '' THEN ' ' ELSE ' ' + nom_emp2 + ' ' END) + ape_emp1 + (CASE ISNULL(ape_emp2, '') WHEN '' THEN ' ' ELSE ' ' + ape_emp2 END) AS NombreEmpleado, 
	regional AS Regional, nom_depe AS Dependencia,  
	desc_car AS Cargo,
	NULL AS IdTipoIdentificacion, NULL AS IdTipoVinculacionContractual, 
	ID_regio AS IdRegional, ID_depen AS IdDependencia, ID_Carg AS IdCargo,
	nom_emp1 AS PrimerNombre, ISNULL(nom_emp2, '') AS SegundoNombre, ape_emp1 AS PrimerApellido, 
	ISNULL(ape_emp2, '') AS SegundoApellido, direccion AS Direccion, telefono AS Telefono
	FROM [KACTUS].[KPRODII].[dbo].[Da_Emple]
	WHERE num_iden = ISNULL(@NumeroIdentificacion, num_iden) AND
	ID_regio = ISNULL(@IdRegional, ID_regio) AND
	ID_depen = ISNULL(@IdDependencia, ID_depen) AND
	ID_Carg = ISNULL(@IdCargo, ID_Carg)

END

GO


