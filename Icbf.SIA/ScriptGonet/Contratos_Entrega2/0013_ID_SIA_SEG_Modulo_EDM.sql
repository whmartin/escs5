
USE [SIA] 
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  12/05/2014 08:11:56 am
-- Description:	Insertar Tabla Modulo para visualizar en el men� la opci�n de contratos
-- =============================================
IF not exists(select * from [SEG].[Modulo] where [NombreModulo] = 'Contratos')
Begin

		INSERT [SEG].[Modulo] ( [NombreModulo], [Posicion], [Estado], [UsuarioCreacion], [FechaCreacion], 
		[UsuarioModificacion], [FechaModificacion])
		 VALUES ( N'Contratos', 0, 1, N'Administrador', GETDATE(), NULL, NULL)

 end

 