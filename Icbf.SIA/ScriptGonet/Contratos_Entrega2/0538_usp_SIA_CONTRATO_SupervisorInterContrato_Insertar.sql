USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Insertar]    Script Date: 29/07/2014 05:47:13 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Insertar]    Script Date: 29/07/2014 05:47:13 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Eduardo Isaac Ballesteros Mu�oz
-- Create date: 7/1/2014 14:20:06 AM
-- Description:	Procedimiento almacenado que guarda un nuevo SupervisorInterContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Insertar] @IDSupervisorIntervContrato INT OUTPUT
	,@FechaInicio DATETIME
	,@Inactivo BIT
	,@Identificacion NVARCHAR(80)
	,@TipoIdentificacion NVARCHAR(4)
	,@IDTipoSuperInter INT
	,@IdNumeroContratoInterventoria INT = NULL
	,@IDProveedoresInterventor INT = NULL
	,@IDEmpleadosSupervisor INT = NULL
	,@IDDirectorInterventoria INT = NULL
	,@IdContrato INT 
	,@UsuarioCrea NVARCHAR(250)
AS
BEGIN

	IF NOT EXISTS(select * from CONTRATO.SupervisorInterContrato where Identificacion=@Identificacion AND IdContrato=@IdContrato)
	BEGIN

	INSERT INTO CONTRATO.SupervisorInterContrato (
		FechaInicio
		,Inactivo
		,Identificacion
		,TipoIdentificacion
		,IDTipoSuperInter
		,IdNumeroContratoInterventoria
		,IDProveedoresInterventor
		,IDEmpleadosSupervisor
		,IdContrato
		,IDDirectorInterventoria
		,UsuarioCrea
		,FechaCrea
		)
	VALUES (
		@FechaInicio
		,@Inactivo
		,@Identificacion
		,@TipoIdentificacion
		,@IDTipoSuperInter
		,@IdNumeroContratoInterventoria
		,@IDProveedoresInterventor
		,@IDEmpleadosSupervisor
		,@IdContrato
		,@IDDirectorInterventoria
		,@UsuarioCrea
		,GETDATE()
		)

	SELECT @IDSupervisorIntervContrato = @@IDENTITY
	END
	ELSE
	BEGIN
		RAISERROR('Ya existe el tipo de persona asociado al contrato',16,1)
	    RETURN
	END
END

