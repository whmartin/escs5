USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Eliminar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) PlanComprasRubrosCDP
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Eliminar]
	@IDRubroPlanComprasContrato INT	
AS
BEGIN
	DELETE Contrato.RubroPlanComprasContrato 
	WHERE IDRubroPlanComprasContrato = @IDRubroPlanComprasContrato
END
