USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]    Script Date: 22/09/2014 02:38:22 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoFormaPago_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Eliminar]
GO

/***********************************************
Creado Por: Gonet/Efrain Diaz
Fecha: 22/09/2014			
Permite : Crear el usp_RubOnline_Contrato_TipoFormaPago_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Eliminar]
	@IdTipoFormaPago INT
AS
BEGIN
	DELETE [Contrato].[TipoFormaPago] WHERE IdTipoFormaPago = @IdTipoFormaPago
END



