USE SIA

-- =============================================
-- Author:		Gonet
-- Create date: 2014-07-09
-- Description:	Alterar tabla [Contrato].[TipoSuperInter]
-- =============================================
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('[Contrato].[TipoSuperInter]') AND NAME = ('Estado'))
BEGIN
	alter table [Contrato].[TipoSuperInter] add Estado bit 
END
ELSE
BEGIN
	PRINT 'YA EXISTE EL CAMPO Estado'
END
go
UPDATE [Contrato].[TipoSuperInter] SET Estado = 0


alter table [Contrato].[TipoSuperInter] alter column Estado bit not null


IF not EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Contrato].DF_Contrato_TipoSuperInter_Estado'))
BEGIN	
	alter table [Contrato].[TipoSuperInter] add constraint DF_Contrato_TipoSuperInter_Estado default(0) for Estado
END

