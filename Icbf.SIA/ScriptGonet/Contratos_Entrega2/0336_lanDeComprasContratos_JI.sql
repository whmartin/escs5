USE SIA
GO
-- =============================================
-- Author:		Jos� Ignacio De Los Reyes
-- Create date: 06/25/2014 17:28:00
-- Description:	Se agrega campo "CodigoRegional" en la tabla "CONTRATO.PlanDeComprasContratos" para Relacionar las regionales
-- =============================================
IF NOT EXISTS(SELECT T.name 
			  FROM sys.tables T 
				INNER JOIN sys.columns C ON T.object_id = C.object_id 
				INNER JOIN sys.schemas S ON T.schema_id = S.schema_id
			  WHERE C.name = 'CodigoRegional' 
				AND T.name = 'PlanDeComprasContratos' 
				AND S.name = 'CONTRATO')
BEGIN
	ALTER TABLE CONTRATO.PlanDeComprasContratos ADD CodigoRegional NVARCHAR(16) NULL

END--FIN IF NOS EXISTS
GO

UPDATE CONTRATO.PlanDeComprasContratos
SET CodigoRegional = '08'

GO
IF EXISTS(SELECT T.name 
		  FROM sys.tables T 
			INNER JOIN sys.columns C ON T.object_id = C.object_id 
			INNER JOIN sys.schemas S ON T.schema_id = S.schema_id
		  WHERE C.name = 'CodigoRegional' 
			AND T.name = 'PlanDeComprasContratos' 
			AND S.name = 'CONTRATO')
BEGIN
	ALTER TABLE CONTRATO.PlanDeComprasContratos ALTER COLUMN CodigoRegional NVARCHAR(16) NOT NULL
END--FIN IF NOS EXISTS
GO

