USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_AporteContrato_ExisteContratoAporte]    Script Date: 28/05/2014 03:59:33 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_AporteContrato_ExisteContratoAporte]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_AporteContrato_ExisteContratoAporte]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_AporteContrato_ExisteContratoAporte]    Script Date: 28/05/2014 03:59:33 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  28/05/2014 04:00:14 PM
-- Description:	Procedimiento almacenado que Verifica la existencia de un contratista asociado a un tipo y valor de aporte
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_AporteContrato_ExisteContratoAporte]
	 @IdContrato INT,
	 @AporteEnDinero BIT,
	 @ValorAporte NUMERIC(30,2)
AS
BEGIN
 SELECT [IdAporteContrato] FROM [CONTRATO].[AporteContrato]
 WHERE [IdContrato] = @IdContrato
 AND [AporteEnDinero] = @AporteEnDinero
 AND [ValorAporte] = @ValorAporte
END


