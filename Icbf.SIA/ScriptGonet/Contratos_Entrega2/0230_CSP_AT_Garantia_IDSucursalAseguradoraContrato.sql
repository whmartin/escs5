USE [SIA]
GO
-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  11/6/2014 8:59:59 AM
-- =============================================

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Garantia') AND NAME = ('IDSucursalAseguradoraContrato'))
BEGIN
ALTER TABLE CONTRATO.Garantia
ADD IDSucursalAseguradoraContrato INT
END
GO

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Garantia') AND NAME = ('BeneficiarioOTROS'))
BEGIN
ALTER TABLE CONTRATO.Garantia
ADD BeneficiarioOTROS BIT NULL
END
GO