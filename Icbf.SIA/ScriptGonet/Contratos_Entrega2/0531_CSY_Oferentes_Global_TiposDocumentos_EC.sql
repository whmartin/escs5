USE [SIA]
GO
-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date:  28/07/2014 15:38 
-- Description:	Sinonimo de la tabla Global.TiposDocumentos de la base de datos Oferentes
-- =============================================

/****** Object:  Synonym [Ppto].[CompromisosPresupuestales]    Script Date: 14/07/2014 03:35:15 p.m. ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GlobalTiposDocumentos' AND schema_id = SCHEMA_ID(N'Oferente'))
DROP SYNONYM [Oferente].[GlobalTiposDocumentos]
GO

/****** Object:  Synonym [Ppto].[CompromisosPresupuestales]    Script Date: 14/07/2014 03:35:15 p.m. ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GlobalTiposDocumentos' AND schema_id = SCHEMA_ID(N'Oferente'))
CREATE SYNONYM [Oferente].[GlobalTiposDocumentos] FOR [Oferentes].[Global].[TiposDocumentos]
GO

