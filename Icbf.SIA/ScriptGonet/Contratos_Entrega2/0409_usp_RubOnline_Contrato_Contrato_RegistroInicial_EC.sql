USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_RegistroInicial]    Script Date: 22/06/2014 04:27:48 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_Contrato_RegistroInicial')
BEGIN
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_RegistroInicial]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_RegistroInicial]    Script Date: 22/06/2014 04:27:48 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 22/06/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_RegistroInicial]
	@IdContrato INT OUTPUT,
	@UsuarioCrea NVARCHAR(250),
	@IdRegionalContrato INT
AS
BEGIN

	INSERT INTO CONTRATO.Contrato
		(UsuarioCrea, IdRegionalContrato, FechaCrea)
	VALUES
		(@UsuarioCrea, @IdRegionalContrato, GETDATE())

	SELECT @IdContrato = SCOPE_IDENTITY()

END

GO


