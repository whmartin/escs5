USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AmparosGarantias_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantias_Eliminar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  5/30/2014 2:33:59 PM
-- Description:	Procedimiento almacenado que elimina un(a) AmparosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantias_Eliminar]
	@IDAmparosGarantias INT
AS
BEGIN
	DELETE Contrato.AmparosGarantias WHERE IDAmparosGarantias = @IDAmparosGarantias
END
