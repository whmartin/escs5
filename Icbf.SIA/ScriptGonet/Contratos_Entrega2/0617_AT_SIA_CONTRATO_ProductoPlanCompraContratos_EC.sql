USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapiña
-- Create date: 08/07/2014
-- Description: agrega la columna iddetalleobjeto a la tabla productoplancompracontratos
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ProductoPlanCompraContratos]') AND type in (N'U'))
BEGIN
	
	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ProductoPlanCompraContratos' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdDetalleObjeto')
	BEGIN 
		ALTER TABLE CONTRATO.ProductoPlanCompraContratos
		ADD IdDetalleObjeto NVARCHAR(150) NULL;
	END
	

END