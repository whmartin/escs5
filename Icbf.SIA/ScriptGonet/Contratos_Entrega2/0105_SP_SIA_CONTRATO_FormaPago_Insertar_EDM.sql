USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_FormaPago_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPago_Insertar]
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/28/2014 3:00:58 PM
-- Description:	Procedimiento almacenado que guarda un nuevo FormaPago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPago_Insertar]
		@IdFormaPago INT OUTPUT, 	@IdProveedores INT,	@IdMedioPago INT,	@IdEntidadFinanciera INT,	@TipoCuentaBancaria INT,	@NumeroCuentaBancaria NVARCHAR(80), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO CONTRATO.FormaPago(IdProveedores, IdMedioPago, IdEntidadFinanciera, TipoCuentaBancaria, NumeroCuentaBancaria, UsuarioCrea, FechaCrea)
					  VALUES(@IdProveedores, @IdMedioPago, @IdEntidadFinanciera, @TipoCuentaBancaria, @NumeroCuentaBancaria, @UsuarioCrea, GETDATE())
	SELECT @IdFormaPago = @@IDENTITY 		
END
