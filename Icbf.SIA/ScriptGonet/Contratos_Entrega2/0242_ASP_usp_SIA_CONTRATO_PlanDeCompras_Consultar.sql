USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_PlanDeCompras_Consultar]    Script Date: 25/06/2014 11:27:50 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_PlanDeCompras_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_PlanDeCompras_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/22/2014 1:34:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) PlanDeCompras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_PlanDeCompras_Consultar]
	@IdPlanDeCompras INT,
	@Idvigencia varchar(8)
AS
BEGIN
	SELECT IDPlanDeComprasContratos as  IdPlanDeCompras,
		IDPlanDeCompras as  NumeroConsecutivo,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].PlanDeComprasContratos
	WHERE IdPlanDeCompras = @IdPlanDeCompras
	and  Vigencia = @Idvigencia
END
