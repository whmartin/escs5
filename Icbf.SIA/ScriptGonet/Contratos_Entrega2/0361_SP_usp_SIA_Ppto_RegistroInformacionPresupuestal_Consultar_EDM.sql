USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]    Script Date: 03/07/2014 05:45:47 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]
GO

-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Create date:  22/05/2014 2:30 PM
-- Description:	Procedimiento almacenado que Muestra la información relacionada con el certificado de disponibilidad presupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]
	@NumeroCDP INT = null,
	@VigenciaFiscal INT = null,
	@RegionalICBF INT = null,
	@Area INT = null,
	@ValorTotalDesde INT = null,
	@ValorTotalHasta INT = null,
	@FechaCDPDesde DATETIME = null,
	@FechaCDPHasta DATETIME = null

AS
BEGIN
		select top 501  IECDP.[IdEtlCDP]
      ,IECDP.[IdLogProceso]
      ,IECDP.[IdRegionalPCI]
      ,IECDP.[CDP] NumeroCDP
      ,IECDP.[Fecha]
      ,IECDP.[EstadoCDP]
      ,IECDP.[IdVigencia]
      ,IECDP.[TipoCDP]
      ,IECDP.[Descripcion]
      ,IECDP.[CodSolicitudCDP]
      ,IECDP.[FechaSolicitudCDP] FechaCDP
      ,IECDP.[CodAutorizacionByS]
      ,IECDP.[ModContratacion]
      ,IECDP.[TipoContrato]
      ,IECDP.[ValorInicialCDP]
      ,IECDP.[ValorTotalOperacionCDP]
      ,IECDP.[ValorActualCDP] ValorCDP
      ,IECDP.[Saldo]
      ,IECDP.[ListaItemAfectaGasto]
      ,IECDP.[ItemAfectaGasto]
      ,IECDP.[IdDepAfecSIIF]
      ,IECDP.[IdCatRubro]
      ,IECDP.[IdTipoFte]
      ,IECDP.[IdTipoRecursoFinPptal]
      ,IECDP.[IdTipoSitFondos]
      ,IECDP.[ValorInicial]
      ,IECDP.[ValorTotalOperacion]
      ,IECDP.[ValorActual]
      ,IECDP.[SaldoNoComprometido]
      ,IECDP.[SaldoBloqueado]
      ,IECDP.[ListaItemOperacion]
      ,IECDP.[FechaOperacion] 
      ,IECDP.[ValorOperacion]
      ,IECDP.[FechaCrea]
      ,IECDP.[UsuarioCrea]
      ,IECDP.[FechaModifica]
      ,IECDP.[UsuarioModifica]
	  ,V.[AcnoVigencia] as VigenciaFiscal
	  ,IECDP.IdRegionalPCI RegionalICBF
	  ,RPCI.DescripcionPCI NombreRegionalICBF
	  ,'' Area
	  ,rtrim(ltrim(DASIIF.CodDepAfecSIIF)) + '-' + rtrim(ltrim(DASIIF.Descripcion)) as DependenciaAfectacionGastos
	  ,rtrim(ltrim(TFF.CodTipoFte))+'-'+ rtrim(ltrim(TFF.DescTipoFuente)) as TipoFuenteFinanciamiento
	  ,CGRPG.RubroSIIF PosicionCatalogoGastos
	  ,CGRPG.DescripcionRubro RubroPresupuestal
	  ,rtrim(ltrim(TRFP.CodTipoRecursoFinPptal)) +'-'+ rtrim(ltrim(TRFP.DescTipoRecurso)) as RecursoPresupuestal
	  ,'' TipoDocumentoSoporte
	  ,rtrim(ltrim(TSF.CodSitFondos)) +'-'+ rtrim(ltrim(TSF.DescTipoSitFondos)) as TipoSituacionFondos
		from Ppto.InfoETLCDP IECDP
		--Inner Join Ppto.AreasxRubro APR on APR.IdVigencia = IECDP.IdVigencia and APR.IdCatalogo = IECDP.IdCatRubro
		--Inner Join Ppto.AreasInternas AI On AI.IdAreasInternas = APR.IdAreasInternas
		Inner Join Global.Vigencia V on V.IdVigencia = IECDP.IdVigencia
		inner join Ppto.RegionalesPCI RPCI on RPCI.IdRegionalPCI = IECDP.IdRegionalPCI
		inner join DIV.Regional R on RPCI.CodRegionalIcbf COLLATE DATABASE_DEFAULT  = R.CodigoRegional COLLATE DATABASE_DEFAULT 
		Inner join Ppto.DependenciasAfectacionSIIF DASIIF on DASIIF.IdDepAfecSIIF = IECDP.IdDepAfecSIIF
		Inner Join BaseSIF.TipoFuenteFinanciamento TFF on TFF.IdTipoFte = IECDP.IdTipoFte
		Inner Join BaseSIF.TipoRecursoFinPptal TRFP on TRFP.IdTipoRecursoFinPptal = IECDP.IdTipoRecursoFinPptal
		Inner Join Ppto.CatGralRubrosPptalGasto CGRPG on CGRPG.IdCatalogo = IECDP.IdCatRubro
		Inner join BaseSIF.TipoSitFondos TSF On TSF.IdTipoSitFondos = IECDP.IdTipoSitFondos
		Where isnull(IECDP.CDP,0) = CASE WHEN @NumeroCDP IS NULL THEN isnull(IECDP.CDP,0) ELSE @NumeroCDP END 
		And isnull(IECDP.IdVigencia,0) = CASE WHEN @VigenciaFiscal IS NULL THEN isnull(IECDP.IdVigencia,0) ELSE @VigenciaFiscal END 
		And isnull(R.IdRegional,0) = CASE WHEN @RegionalICBF IS NULL THEN isnull(R.IdRegional,0) ELSE @RegionalICBF END 
		--and isnull(IECDP.IdRegionalPCI,0) = COALESCE(@RegionalICBF, isnull(IECDP.IdRegionalPCI,0))
		And isnull(IECDP.ValorActualCDP,0) >= CASE WHEN @ValorTotalDesde IS NULL THEN isnull(IECDP.ValorActualCDP,0) ELSE @ValorTotalDesde END 
		And isnull(IECDP.ValorActualCDP,0) <= CASE WHEN @ValorTotalHasta IS NULL THEN  isnull(IECDP.ValorActualCDP,0) ELSE @ValorTotalHasta END 
	    And isnull(IECDP.FechaSolicitudCDP,'') >= CASE WHEN @FechaCDPDesde IS NULL THEN isnull(IECDP.FechaSolicitudCDP,'') ELSE @FechaCDPDesde END 
	    And isnull(IECDP.FechaSolicitudCDP,'') <= CASE WHEN @FechaCDPHasta IS NULL THEN isnull(IECDP.FechaSolicitudCDP,'') ELSE @FechaCDPHasta END 
 
 IF @@ROWCOUNT >= 500
  BEGIN
   RAISERROR('La consulta arroja demasiados resultados,por favor refine su consulta',16,1)
   RETURN
  END


END
