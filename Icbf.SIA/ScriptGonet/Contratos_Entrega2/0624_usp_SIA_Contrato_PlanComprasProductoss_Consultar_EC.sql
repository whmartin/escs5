USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasProductoss_Consultar]    Script Date: 09/09/2014 10:44:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasProductoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductoss_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasProductoss_Consultar]    Script Date: 09/09/2014 10:44:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:32 PM
-- Description:	Procedimiento almacenado que consulta un(a) PlanComprasProductos
-- =============================================
-- =============================================
-- Author:		Emilio Calapiña
-- Create date:  09/09/2014 14:06
-- Description:	Se agrego columna IdDetalleObjeto
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductoss_Consultar]
	@NumeroConsecutivoPlanCompras	INT = NULL,
	@IDPlanDeComprasContratos		INT = NULL,
	@IDProductoPlanCompraContrato	INT = NULL,
	@IDProducto						NVARCHAR(128) = NULL,
	@CantidadCupos					NUMERIC(18,2) = NULL
AS
BEGIN
	SELECT PCC.IDPlanDeCompras NumeroConsecutivoPlanCompras,
		PPC.IDProductoPlanCompraContrato,
		PPC.IDProducto CodigoProducto,
		PPC.CantidadCupos,
		PCC.IDPlanDeComprasContratos,
		PPC.UsuarioCrea,
		PPC.FechaCrea,
		PPC.UsuarioModifica,
		PPC.FechaModifica,
		PPC.ValorProducto , PPC.Tiempo , PPC.UnidadTiempo ,PPC.IdUnidadTiempo, PPC.IdDetalleObjeto
	FROM [Contrato].[ProductoPlanCompraContratos] PPC
		INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON PPC.IDPlanDeComprasContratos = PCC.IDPlanDeComprasContratos
	WHERE PPC.IDPlanDeComprasContratos = COALESCE(@IDPlanDeComprasContratos,PPC.IDPlanDeComprasContratos)
		AND PPC.IDProductoPlanCompraContrato = COALESCE(@IDProductoPlanCompraContrato,PPC.IDProductoPlanCompraContrato)
		AND PPC.IDProducto = COALESCE(@IDProducto,PPC.IDProducto)
		AND PPC.CantidadCupos = COALESCE(@CantidadCupos,PPC.CantidadCupos)
		AND PCC.IDPlanDeCompras = COALESCE(@NumeroConsecutivoPlanCompras,PCC.IDPlanDeCompras)
END--FIN PP



GO

