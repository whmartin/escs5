USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]    Script Date: 04/06/2014 12:07:16 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]
		@IdTipoSupvInterventor INT,	@Nombre NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoSuperInter
	SET	Descripcion = @Nombre,
		Estado = @Estado,
		UsuarioModifica = @UsuarioModifica,
		FechaModifica = GETDATE()
	WHERE IDTipoSuperInter = @IdTipoSupvInterventor
END


