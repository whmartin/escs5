USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AporteContrato_Insertar]    Script Date: 16/09/2014 11:24:04 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AporteContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AporteContrato_Insertar]    Script Date: 16/09/2014 11:24:04 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  5/29/2014 2:03:01 PM
-- Description:	Procedimiento almacenado que guarda un nuevo AporteContrato
-- Actualizacion: se filtra que la condicion de no repetir la combinaci�n debe ser por cada contratista
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Insertar]
		@IdAporteContrato INT OUTPUT, 	@AportanteICBF BIT,	@NumeroIdentificacionICBF NVARCHAR(56),	@ValorAporte NUMERIC(30,2),	@DescripcionAporte NVARCHAR(896),	@AporteEnDinero BIT,	@IdContrato INT,	@IDEntidadProvOferente INT,	@FechaRP DATETIME,	@NumeroRP NVARCHAR(30),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	IF EXISTS(SELECT * FROM Contrato.AporteContrato 
			  WHERE  IdContrato = @IdContrato AND
					 AporteEnDinero = @AporteEnDinero AND
					 ValorAporte = @ValorAporte AND
					 NumeroIdentificacionICBF =@NumeroIdentificacionICBF)
	BEGIN
		RAISERROR('Ya existe un registro con esta combinaci�n',16,1)
	    RETURN
	END
	INSERT INTO Contrato.AporteContrato(AportanteICBF, NumeroIdentificacionICBF, ValorAporte, DescripcionAporte, AporteEnDinero, IdContrato, IDEntidadProvOferente, FechaRP, NumeroRP, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@AportanteICBF, @NumeroIdentificacionICBF, @ValorAporte, @DescripcionAporte, @AporteEnDinero, @IdContrato, @IDEntidadProvOferente, @FechaRP, @NumeroRP, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdAporteContrato = @@IDENTITY 		
END

GO


