
USE [SIA]
GO
/*Cesar Andres Ortiz Lajud
05/08/2014 
Cambio de valor Hasta del registro identificado con el numero 8, es muy grande para el tipo de variable 
*/
UPDATE [CONTRATO].[ValoresContrato]
   SET [Hasta] = 999999999
 WHERE IdValoresContrato = 8
GO


