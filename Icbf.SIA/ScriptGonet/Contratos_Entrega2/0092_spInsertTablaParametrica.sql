USE [SIA]

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Crear permiso administra consecutivo [CONTRATO].[TablaParametrica]
-- =============================================


IF(NOT EXISTS(select * from [CONTRATO].[TablaParametrica] where NombreTablaParametrica = 'Administrar consecutivo de contrato'))
BEGIN
insert into [CONTRATO].[TablaParametrica]
(CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea)VALUES
('02', 'Administrar consecutivo de contrato', 'Contratos/ConsecutivoContratoRegionales', 1, 'Administrador', GETDATE())
END
ELSE
	print 'Ya existe Administrar consecutivo de contrato en tabla paramétrica'