USE [SIA]
GO
-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:32 PM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[ProductoPlanCompraContratos]') AND type in (N'U'))
BEGIN
	DROP table [Contrato].[ProductoPlanCompraContratos]
--Print 'Ya Existe La tabla ProductoPlanCompraContratos a crear'
--RETURN
END

-- Table ProductoPlanCompraContratos

CREATE TABLE [Contrato].[ProductoPlanCompraContratos]
(
 IDProductoPlanCompraContrato	INT IDENTITY(1,1) NOT NULL,
 IDProducto						NVARCHAR(128) NOT NULL,
 CantidadCupos					NUMERIC(18,2) NULL,
 IDPlanDeComprasContratos		INT NULL,
 UsuarioCrea					NVARCHAR(250)  NOT NULL,
 FechaCrea						DATETIME NOT NULL,
 UsuarioModifica				NVARCHAR(250) NULL,
 FechaModifica					DATETIME NULL
)
go
-- Add keys for table ProductoPlanCompraContratos
ALTER TABLE Contrato.ProductoPlanCompraContratos
ADD CONSTRAINT PK_Contrato_ProductoPlanCompraContratos
PRIMARY KEY(IDProductoPlanCompraContrato)
GO
ALTER TABLE Contrato.ProductoPlanCompraContratos
ADD CONSTRAINT FK_ProductoPlanCompraContratos_PlanDeComprasContratos
FOREIGN KEY(IDPlanDeComprasContratos)
REFERENCES CONTRATO.PlanDeComprasContratos(IDPlanDeComprasContratos)
GO

