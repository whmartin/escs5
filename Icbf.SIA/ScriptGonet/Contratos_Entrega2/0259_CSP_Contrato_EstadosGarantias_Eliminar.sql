USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_EstadosGarantias_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_EstadosGarantias_Eliminar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/25/2014 5:10:11 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_EstadosGarantias_Eliminar]
	@IDEstadosGarantias INT
AS
BEGIN
	DELETE Contrato.EstadosGarantias WHERE IDEstadosGarantias = @IDEstadosGarantias
END
