use SIA
/********************************************
2014-06-03
Crear campo para administrar los roles administrador
*********************************************/
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('SEG.Rol') AND NAME = ('EsAdministrador'))
BEGIN
	alter table SEG.Rol add EsAdministrador bit  
END
ELSE
BEGIN
	PRINT 'YA EXISTE EL CAMPO EsAdministrador'
END
go
UPDATE SEG.Rol SET EsAdministrador = 0


alter table SEG.Rol alter column EsAdministrador bit not null


IF not EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[SEG].DF_SEG_Rol_EsAdministrador'))
BEGIN	
	alter table SEG.Rol add constraint DF_SEG_Rol_EsAdministrador default(0) for EsAdministrador
END

