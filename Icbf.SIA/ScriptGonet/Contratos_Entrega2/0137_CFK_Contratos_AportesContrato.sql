USE [SIA]
GO
-- =============================================
-- Author:		Gonet\Carlos Andr�s C�rdenas P.
-- Create date:  30/05/2014 05:10:00 PM
-- Description:	Creacion de llave for�nea FK_Contratos_AportesContrato
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Ppto].[FK_Contratos_AportesContrato]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [Contrato].[AporteContrato] DROP CONSTRAINT FK_Contratos_AportesContrato
END

ALTER TABLE [Contrato].[AporteContrato]
ADD CONSTRAINT FK_Contratos_AportesContrato FOREIGN KEY(IdContrato)REFERENCES  [Contrato].[Contrato](IdContrato)

