USE [SIA]
GO

IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Insertar]')
			AND type IN (
				N'P'
				,N'PC'
				)
		)
	DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Insertar]
GO

-- =============================================
-- Author:	Eduardo Isaac Ballesteros	
-- Create date:  7/4/2014 9:10:33 AM
-- Description:	Procedimiento almacenado que guarda un nuevo VigenciaFuturas
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Insertar] @IDVigenciaFuturas INT OUTPUT
	,@NumeroRadicado NVARCHAR(80)
	,@FechaExpedicion DATETIME
	,@IdContrato INT
	,@ValorVigenciaFutura NUMERIC(30, 2)
	,@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO CONTRATO.VigenciaFuturas (
		NumeroRadicado
		,FechaExpedicion
		,IdContrato
		,ValorVigenciaFutura
		,UsuarioCrea
		,FechaCrea
		)
	VALUES (
		@NumeroRadicado
		,@FechaExpedicion
		,@IdContrato
		,@ValorVigenciaFutura
		,@UsuarioCrea
		,GETDATE()
		)

	SELECT @IDVigenciaFuturas = @@IDENTITY
END

