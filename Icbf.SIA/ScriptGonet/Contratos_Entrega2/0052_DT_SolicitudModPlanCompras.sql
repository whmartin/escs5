use SIA

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Creacion table [CONTRATO].[EstadoSolcitudModPlanCompras]
-- =============================================

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', NULL,NULL))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaModifica'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaCrea'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'Activo'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Activo'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'Descripcion'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Descripcion'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'CodEstado'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'CodEstado'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'IdEstadoSolicitud'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdEstadoSolicitud'

GO

/****** Object:  Table [CONTRATO].[EstadoSolcitudModPlanCompras]    Script Date: 08/07/2014 14:54:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[EstadoSolcitudModPlanCompras]') AND type in (N'U'))
DROP TABLE [CONTRATO].[EstadoSolcitudModPlanCompras]
GO

/****** Object:  Table [CONTRATO].[EstadoSolcitudModPlanCompras]    Script Date: 08/07/2014 14:54:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[EstadoSolcitudModPlanCompras]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[EstadoSolcitudModPlanCompras](
	[IdEstadoSolicitud] [nvarchar](1) NOT NULL,
	[CodEstado] [nvarchar](24) NOT NULL,
	[Descripcion] [nvarchar](160) NOT NULL,
	[Activo] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EstadoSolcitudModPlanCompras] PRIMARY KEY CLUSTERED 
(
	[IdEstadoSolicitud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'IdEstadoSolicitud'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdEstadoSolicitud'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'CodEstado'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CodEstado' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'CodEstado'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'Descripcion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id IdTercero' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'Activo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Activo' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Activo'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creaci�n del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificaci�n del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'EstadoSolcitudModPlanCompras', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Almacena datos sobre Estado de solicitud de plan de compras' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'EstadoSolcitudModPlanCompras'
GO



/**********************************************************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdVigencia]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] DROP CONSTRAINT [FK_SolicitudModPlanCompras_IdVigencia]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdUsuarioSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] DROP CONSTRAINT [FK_SolicitudModPlanCompras_IdUsuarioSolicitud]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdPlanDeCompras]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] DROP CONSTRAINT [FK_SolicitudModPlanCompras_IdPlanDeCompras]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdEstadoSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] DROP CONSTRAINT [FK_SolicitudModPlanCompras_IdEstadoSolicitud]
GO

/****** Object:  Table [CONTRATO].[SolicitudModPlanCompras]    Script Date: 23/05/2014 15:18:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]') AND type in (N'U'))
DROP TABLE [CONTRATO].[SolicitudModPlanCompras]
GO


USE [SIA]
GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', NULL,NULL))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaModifica'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaCrea'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'IdPlanDeCompras'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdPlanDeCompras'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'IdUsuarioSolicitud'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdUsuarioSolicitud'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'IdEstadoSolicitud'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdEstadoSolicitud'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'FechaSolicitud'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaSolicitud'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'NumeroSolicitud'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'NumeroSolicitud'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'Justificacion'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Justificacion'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'IdSolicitudModPlanCompra'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdSolicitudModPlanCompra'

GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdUsuarioSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] DROP CONSTRAINT [FK_SolicitudModPlanCompras_IdUsuarioSolicitud]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdPlanDeCompras]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] DROP CONSTRAINT [FK_SolicitudModPlanCompras_IdPlanDeCompras]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdEstadoSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] DROP CONSTRAINT [FK_SolicitudModPlanCompras_IdEstadoSolicitud]
GO

/****** Object:  Table [CONTRATO].[SolicitudModPlanCompras]    Script Date: 08/07/2014 9:15:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]') AND type in (N'U'))
DROP TABLE [CONTRATO].[SolicitudModPlanCompras]
GO

/****** Object:  Table [CONTRATO].[SolicitudModPlanCompras]    Script Date: 08/07/2014 9:15:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[SolicitudModPlanCompras](
	[IdSolicitudModPlanCompra] [int] IDENTITY(1,1) NOT NULL,
	[Justificacion] [nvarchar](896) NOT NULL,
	[NumeroSolicitud] [int] NOT NULL,
	[FechaSolicitud] [datetime] NOT NULL,
	[IdEstadoSolicitud] [nvarchar](1) NOT NULL,
	[IdUsuarioSolicitud] [int] NOT NULL,
	[IdPlanDeCompras] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_SolicitudModPlanCompras] PRIMARY KEY CLUSTERED 
(
	[IdSolicitudModPlanCompra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdEstadoSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudModPlanCompras_IdEstadoSolicitud] FOREIGN KEY([IdEstadoSolicitud])
REFERENCES [CONTRATO].[EstadoSolcitudModPlanCompras] ([IdEstadoSolicitud])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdEstadoSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] CHECK CONSTRAINT [FK_SolicitudModPlanCompras_IdEstadoSolicitud]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdUsuarioSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudModPlanCompras_IdUsuarioSolicitud] FOREIGN KEY([IdUsuarioSolicitud])
REFERENCES [SEG].[Usuario] ([IdUsuario])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SolicitudModPlanCompras_IdUsuarioSolicitud]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SolicitudModPlanCompras]'))
ALTER TABLE [CONTRATO].[SolicitudModPlanCompras] CHECK CONSTRAINT [FK_SolicitudModPlanCompras_IdUsuarioSolicitud]
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'IdSolicitudModPlanCompra'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdSolicitudModPlanCompra'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'Justificacion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Justificacion' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'Justificacion'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'NumeroSolicitud'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'N�mero de la solicitud' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'NumeroSolicitud'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'FechaSolicitud'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha y hora de la solicitud' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaSolicitud'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'IdEstadoSolicitud'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id EstadoSolicitud' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdEstadoSolicitud'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'IdUsuarioSolicitud'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IdUsuarioSolicitud' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdUsuarioSolicitud'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'IdPlanDeCompras'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id Plan de compras' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'IdPlanDeCompras'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creaci�n del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificaci�n del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'SolicitudModPlanCompras', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Almacena datos sobre plan de compras' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'SolicitudModPlanCompras'
GO


