USE [SIA]
GO

/**************************************
-- Create date:	    2014-07-09
-- Author: gonet
-- Creacion de la tabla [CONTRATO].[NumeroProcesos]
**************************************/




IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
AND column_name = 'IdNumeroProceso')
BEGIN 
	ALTER TABLE CONTRATO.Contrato
	ADD IdNumeroProceso [int] NULL
END


IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', NULL,NULL))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'FechaModifica'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'FechaCrea'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'IdVigencia'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'IdVigencia'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'IdRegional'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'IdRegional'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'IdModalidadSeleccion'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'IdModalidadSeleccion'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'NumeroProcesoGenerado'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'NumeroProcesoGenerado'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'Inactivo'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'Inactivo'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'NumeroProceso'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'NumeroProceso'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'IdNumeroProceso'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'IdNumeroProceso'

GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_Vigencia]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos] DROP CONSTRAINT [FK_NumeroProcesos_Vigencia]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_Regional]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos] DROP CONSTRAINT [FK_NumeroProcesos_Regional]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_NumeroProceso]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_NumeroProceso]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_ModalidadSeleccion]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos] DROP CONSTRAINT [FK_NumeroProcesos_ModalidadSeleccion]
GO

/****** Object:  Table [CONTRATO].[NumeroProcesos]    Script Date: 26/05/2014 04:05:20 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]') AND type in (N'U'))
DROP TABLE [CONTRATO].[NumeroProcesos]
GO

/****** Object:  Table [CONTRATO].[NumeroProcesos]    Script Date: 26/05/2014 04:05:20 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[NumeroProcesos](
	[IdNumeroProceso] [int] IDENTITY(1,1) NOT NULL,
	[NumeroProceso] [int] NOT NULL,
	[Inactivo] [bit] NOT NULL,
	[NumeroProcesoGenerado] [nvarchar](30) NULL,
	[IdModalidadSeleccion] [int] NOT NULL,
	[IdRegional] [int] NOT NULL,
	[IdVigencia] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_NumeroProcesos] PRIMARY KEY CLUSTERED 
(
	[IdNumeroProceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_ModalidadSeleccion]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos]  WITH CHECK ADD  CONSTRAINT [FK_NumeroProcesos_ModalidadSeleccion] FOREIGN KEY([IdModalidadSeleccion])
REFERENCES [CONTRATO].[ModalidadSeleccion] ([IdModalidad])
--ON UPDATE CASCADE
--ON DELETE CASCADE
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_ModalidadSeleccion]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos] CHECK CONSTRAINT [FK_NumeroProcesos_ModalidadSeleccion]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_Regional]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos]  WITH CHECK ADD  CONSTRAINT [FK_NumeroProcesos_Regional] FOREIGN KEY([IdRegional])
REFERENCES [DIV].[Regional] ([IdRegional])
--ON UPDATE CASCADE
--ON DELETE CASCADE
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_Regional]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos] CHECK CONSTRAINT [FK_NumeroProcesos_Regional]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_Vigencia]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos]  WITH CHECK ADD  CONSTRAINT [FK_NumeroProcesos_Vigencia] FOREIGN KEY([IdVigencia])
REFERENCES [Global].[Vigencia] ([IdVigencia])
--ON UPDATE CASCADE
--ON DELETE CASCADE
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_NumeroProcesos_Vigencia]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[NumeroProcesos]'))
ALTER TABLE [CONTRATO].[NumeroProcesos] CHECK CONSTRAINT [FK_NumeroProcesos_Vigencia]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_NumeroProceso]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_NumeroProceso] FOREIGN KEY([IdNumeroProceso])
REFERENCES [CONTRATO].[NumeroProcesos] ([IdNumeroProceso])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_NumeroProceso]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_NumeroProceso]
GO


IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'IdNumeroProceso'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id de la tabla número de proceso.' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'IdNumeroProceso'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'NumeroProceso'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el número del proceso.' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'NumeroProceso'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'Inactivo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corresponde al estado del registro.' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'Inactivo'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'NumeroProcesoGenerado'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corresponde al Numero de proceso generado con el código de la regional, el consecutivo, la sigla de la modalidad y la vigencia.' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'NumeroProcesoGenerado'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'IdModalidadSeleccion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corresponde al código de la modalidad de selección' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'IdModalidadSeleccion'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'IdRegional'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corresponde al id de la regional' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'IdRegional'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'IdVigencia'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corresponde al id de la vigencia' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'IdVigencia'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'UsuarioCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'FechaCrea'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'UsuarioModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', N'COLUMN',N'FechaModifica'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificación del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'NumeroProcesos', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Guarda la información relacionada con el número de proceso' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'NumeroProcesos'
GO


