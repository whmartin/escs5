USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Regional_sConsultar]    Script Date: 09/05/2014 02:49:20 p.m. ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_DIV_Regional_sConsultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_sConsultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Regional_sConsultar]    Script Date: 09/05/2014 02:49:20 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		GONET/Efrain Diaz Mejia
-- Create date:  09/05/2014 17:52:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Regional Ordenado por Nombre 
-- =============================================
-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/10/2012 11:52:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Regional
-- =============================================
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  12/15/2012 12:15:00 PM
-- Description:	Procedimiento almacenado que consulta un(a) Regional Ordenado por Nombre DESC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_sConsultar]
	@CodigoRegional NVARCHAR(2) = NULL,@NombreRegional NVARCHAR(256) = NULL
AS
BEGIN
	SELECT
		IdRegional,
		CodigoRegional,
		UPPER(NombreRegional) AS NombreRegional,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [DIV].[Regional]
	WHERE CodigoRegional =
		CASE
			WHEN @CodigoRegional IS NULL THEN CodigoRegional ELSE @CodigoRegional
		END
	AND NombreRegional =
		CASE
			WHEN @NombreRegional IS NULL THEN NombreRegional ELSE @NombreRegional
		END
	ORDER BY NombreRegional

END

