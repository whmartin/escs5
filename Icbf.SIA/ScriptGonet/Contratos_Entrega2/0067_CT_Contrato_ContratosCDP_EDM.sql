USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Creacion de tabla [CONTRATO].[ContratosCDP]
-- =============================================


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ContratosCDP_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ContratosCDP]'))
ALTER TABLE [CONTRATO].[ContratosCDP] DROP CONSTRAINT [FK_ContratosCDP_Contrato]
GO

/****** Object:  Table [CONTRATO].[ContratosCDP]    Script Date: 26/05/2014 09:26:44 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ContratosCDP]') AND type in (N'U'))
DROP TABLE [CONTRATO].[ContratosCDP]
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ContratosCDP]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[ContratosCDP](
	[IdContratosCDP] [int] IDENTITY(1,1) NOT NULL,
	[IdCDP] [int] NOT NULL,
	[IdContrato] [int] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_ContratosCDP] PRIMARY KEY CLUSTERED 
(
	[IdContratosCDP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ContratosCDP_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ContratosCDP]'))
ALTER TABLE [CONTRATO].[ContratosCDP]  WITH CHECK ADD  CONSTRAINT [FK_ContratosCDP_Contrato] FOREIGN KEY([IdContrato])
REFERENCES [CONTRATO].[Contrato] ([IdContrato])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ContratosCDP_Contrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ContratosCDP]'))
ALTER TABLE [CONTRATO].[ContratosCDP] CHECK CONSTRAINT [FK_ContratosCDP_Contrato]
GO




