USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ArchivosContratos_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ArchivosContratos_Insertar]
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  25/06/2014 15:03
-- Description:	Procedimiento almacenado que guarda un nuevo registro de Archivos Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ArchivosContratos_Insertar]
		@IdArchivoContrato INT OUTPUT, 	
		@IdArchivo numeric(18,0), 
		@IdContrato INT,	
		@UsuarioCrea NVARCHAR(250)
		
AS
BEGIN
	INSERT INTO [CONTRATO].[ArchivosContratos]
           ([IdArchivo]
           ,[IdContrato]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           (@IdArchivo
           ,@IdContrato
           ,@UsuarioCrea
           ,Getdate()
           ,null
           ,null)

	SELECT @IdArchivoContrato = @@IDENTITY  		
END


GO



