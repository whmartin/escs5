USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ValoresPlanComprasContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ValoresPlanComprasContratos_Consultar]
GO

-- =============================================
-- Author:		Jos� Ignacio De Los Reyes
-- Create date: 06/27/2014 14:40:00
-- Description:	Procedimiento almacenado que consulta un(a) PlanComprasContratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ValoresPlanComprasContratos_Consultar]
	@IdValoresContrato	INT = NULL
AS
BEGIN
	SELECT IdValoresContrato
		  ,Desde
		  ,Hasta
		  ,UsuarioCrea
		  ,FechaCrea
		  ,UsuarioModifica
		  ,FechaModifica
	FROM CONTRATO.ValoresContrato
	WHERE IdValoresContrato = COALESCE(@IdValoresContrato, IdValoresContrato)
END--FIN PP