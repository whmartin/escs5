USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ContratistaGarantias_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 2:25:34 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContratistaGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Consultar]
	@IDContratista_Garantias INT
AS
BEGIN
 SELECT IDContratista_Garantias, IDGarantia, IDEntidadProvOferente, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ContratistaGarantias] WHERE  IDContratista_Garantias = @IDContratista_Garantias
END
