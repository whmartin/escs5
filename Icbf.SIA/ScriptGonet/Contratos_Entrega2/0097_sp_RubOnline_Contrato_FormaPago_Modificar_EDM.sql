USE [SIA]
GO

-- =============================================
-- Author:		Abraham
-- Create date: 2014-05-28
-- Description:	Eliminacion usp_RubOnline_Contrato_FormaPago_Modificar
-- =============================================

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]    Script Date: 28/05/2014 12:02:43 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]
GO





