USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion]    Script Date: 08/07/2014 05:55:56 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Actualizar_NumeroContrato]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].usp_RubOnline_Contrato_Actualizar_NumeroContrato
GO


-- =============================================
-- Author:		Gonet/Efrain Diaz
-- Create date: 08/07/2014
-- Description:	Modifica el n�mero de contrato de la tabla contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Actualizar_NumeroContrato]
	@IdContrato INT = NULL,
	@NumeroContrato Nvarchar(50) = NULL,
	@UsuarioModifica NVARCHAR(250) = NULL
AS
BEGIN
	
	UPDATE Contrato.Contrato  
	SET
	NumeroContrato = @NumeroContrato
	,UsuarioModifica = @UsuarioModifica
	,FechaModifica = GETDATE()
	WHERE IdContrato = @IdContrato

END




