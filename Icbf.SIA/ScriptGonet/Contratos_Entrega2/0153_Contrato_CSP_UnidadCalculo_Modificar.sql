USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_UnidadCalculo_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculo_Modificar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/3/2014 3:19:34 PM
-- Description:	Procedimiento almacenado que actualiza un(a) UnidadCalculo
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculo_Modificar]
		@IDUnidadCalculo INT,	@CodUnidadCalculo NVARCHAR(8),	@Descripcion NVARCHAR(80),	@Inactivo BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	IF EXISTS(SELECT * FROM Contrato.UnidadCalculo WHERE CodUnidadCalculo=@CodUnidadCalculo AND IDUnidadCalculo <> @IDUnidadCalculo)
	BEGIN
		RAISERROR('Ya existe un registro con este(a) Código Unidad Calculo',16,1)
	    RETURN
	END
	IF EXISTS(SELECT * FROM Contrato.UnidadCalculo WHERE Descripcion=@Descripcion AND IDUnidadCalculo <> @IDUnidadCalculo)
	BEGIN
		RAISERROR('Ya existe un registro con este(a) Descripción',16,1)
	    RETURN
	END
	UPDATE Contrato.UnidadCalculo SET CodUnidadCalculo = @CodUnidadCalculo, Descripcion = @Descripcion, Inactivo = @Inactivo, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IDUnidadCalculo = @IDUnidadCalculo
END
