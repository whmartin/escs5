USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_TipoIdentificacions_Consultar]    Script Date: 02/06/2014 15:11:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_KACTUS_KPRODII_TipoIdentificacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_KACTUS_KPRODII_TipoIdentificacions_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_TipoIdentificacions_Consultar]    Script Date: 02/06/2014 15:11:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero
-- Create date: 2014-06-02
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_KACTUS_KPRODII_TipoIdentificacions_Consultar] 
	
AS
BEGIN
	Select DISTINCT ID_Ident, desc_ide from [KACTUS].[KPRODII].[dbo].[Da_Emple]
END

GO


