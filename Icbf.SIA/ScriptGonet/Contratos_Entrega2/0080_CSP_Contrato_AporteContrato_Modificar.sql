USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AporteContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Modificar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  5/29/2014 2:03:01 PM
-- Description:	Procedimiento almacenado que actualiza un(a) AporteContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Modificar]
		@IdAporteContrato INT,	@AportanteICBF BIT,	@NumeroIdentificacionICBF NVARCHAR(56),	@ValorAporte NUMERIC(30,2),	@DescripcionAporte NVARCHAR(896),	@AporteEnDinero BIT,	@IdContrato INT,	@IDEntidadProvOferente INT,	@FechaRP DATETIME,	@NumeroRP NVARCHAR(30),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.AporteContrato SET AportanteICBF = @AportanteICBF, NumeroIdentificacionICBF = @NumeroIdentificacionICBF, ValorAporte = @ValorAporte, DescripcionAporte = @DescripcionAporte, AporteEnDinero = @AporteEnDinero, IdContrato = @IdContrato, IDEntidadProvOferente = @IDEntidadProvOferente, FechaRP = @FechaRP, NumeroRP = @NumeroRP, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdAporteContrato = @IdAporteContrato
END
