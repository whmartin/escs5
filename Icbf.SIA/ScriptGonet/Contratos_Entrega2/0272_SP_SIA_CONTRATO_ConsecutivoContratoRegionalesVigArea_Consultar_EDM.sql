USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionalesVigArea_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionalesVigArea_Consultar]
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  26/06/2014 4:52:44 PM
-- Description:	Procedimiento almacenado que consulta un(a) Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionalesVigArea_Consultar]
	@IdVigencia INT = NULL,
	@IDRegional INT = NULL
AS
BEGIN

SELECT [IDConsecutivoContratoRegional]
      ,[IdRegional]
      ,[IdVigencia]
      ,[Consecutivo]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
  FROM [CONTRATO].[ConsecutivoContratoRegionales]
   WHERE IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END 
   AND IdRegional = CASE WHEN @IDRegional IS NULL THEN IdRegional ELSE @IDRegional END 

END