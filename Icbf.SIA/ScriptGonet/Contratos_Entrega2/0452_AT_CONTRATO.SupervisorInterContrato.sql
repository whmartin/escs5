USE [SIA]
GO
-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  11/07/2014 8:59:59 AM
-- Descripcion: se habilita para que el campo TipoIdentificacion acepte valores NULL
-- =============================================

IF EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND NAME = ('TipoIdentificacion'))
BEGIN
ALTER TABLE CONTRATO.SupervisorInterContrato
ALTER COLUMN TipoIdentificacion varchar(50)  NULL
END
GO