USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AporteContratos_Consultar]    Script Date: 01/07/2014 03:52:43 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AporteContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AporteContratos_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AporteContratos_Consultar]    Script Date: 01/07/2014 03:52:43 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  5/29/2014 2:03:01 PM
-- Description:	Procedimiento almacenado que consulta un(a) AporteContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AporteContratos_Consultar]
	@AportanteICBF BIT = NULL,@NumeroIdentificacionICBF NVARCHAR(56) = NULL,@ValorAporte NUMERIC(30,2) = NULL,@DescripcionAporte NVARCHAR(896) = NULL,@AporteEnDinero BIT = NULL,@IdContrato INT = NULL,@IDEntidadProvOferente INT = NULL,@FechaRP DATETIME = NULL,@NumeroRP NVARCHAR(30) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdAporteContrato, AportanteICBF, NumeroIdentificacionICBF, ValorAporte, DescripcionAporte, AporteEnDinero, IdContrato, 
 IDEntidadProvOferente, FechaRP, NumeroRP, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[AporteContrato] 
 WHERE IdContrato = CASE WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato END 
 --AND AportanteICBF = CASE WHEN @AportanteICBF IS NULL THEN AportanteICBF ELSE @AportanteICBF END 
 --AND NumeroIdentificacionICBF = CASE WHEN @NumeroIdentificacionICBF IS NULL THEN NumeroIdentificacionICBF ELSE @NumeroIdentificacionICBF END 
 --AND ValorAporte = CASE WHEN @ValorAporte IS NULL THEN ValorAporte ELSE @ValorAporte END 
 --AND DescripcionAporte = CASE WHEN @DescripcionAporte IS NULL THEN DescripcionAporte ELSE @DescripcionAporte END 
 --AND AporteEnDinero = CASE WHEN @AporteEnDinero IS NULL THEN AporteEnDinero ELSE @AporteEnDinero END 
 --AND IDEntidadProvOferente = CASE WHEN @IDEntidadProvOferente IS NULL THEN IDEntidadProvOferente ELSE @IDEntidadProvOferente END 
 --AND FechaRP = CASE WHEN @FechaRP IS NULL THEN FechaRP ELSE @FechaRP END AND NumeroRP = CASE WHEN @NumeroRP IS NULL THEN NumeroRP ELSE @NumeroRP END 
 --AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END



