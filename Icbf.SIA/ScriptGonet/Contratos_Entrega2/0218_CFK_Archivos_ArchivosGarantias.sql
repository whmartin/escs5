USE [SIA]
GO
-- =============================================
-- Author:		Gonet\Carlos Andr�s C�rdenas P.
-- Create date:  20/06/2014 10:10:00 AM
-- Description:	Creacion de llave for�nea FK_Archivos_ArchivosGarantias
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Ppto].[FK_Archivos_ArchivosGarantias]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [Contrato].[ArchivosGarantias] DROP CONSTRAINT FK_Archivos_ArchivosGarantias
END

ALTER TABLE [Contrato].[ArchivosGarantias]
ADD CONSTRAINT FK_Archivos_ArchivosGarantias FOREIGN KEY(IDArchivo)REFERENCES  [Estructura].[Archivo](IdArchivo)

