USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Garantia_ExisteNumeroGarantia]    Script Date: 17/06/2014 09:17:07 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Garantia_ExisteNumeroGarantia]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Garantia_ExisteNumeroGarantia]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Garantia_ExisteNumeroGarantia]    Script Date: 17/06/2014 09:17:07 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  17/06/2014 09:17:14 AM
-- Description:	Procedimiento almacenado que verifica la existencia de un numero de garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Garantia_ExisteNumeroGarantia]
	 @NumeroGarantia NVARCHAR(80)
AS
BEGIN
 SELECT IDGarantia FROM CONTRATO.Garantia
 WHERE NumeroGarantia = @NumeroGarantia
END