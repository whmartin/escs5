USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/30/2014 12:23:44 PM
-- Description:	Procedimiento almacenado que consulta un(a) SupervisorInterContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Consultar]
	@IDSupervisorIntervContrato INT
AS
BEGIN
SELECT
	IDSupervisorIntervContrato,
	FechaInicio,
	Inactivo,
	Identificacion,
	TipoIdentificacion,
	IDTipoSuperInter,
	IdNumeroContratoInterventoria,
	IDProveedoresInterventor,
	IDEmpleadosSupervisor,
	IdContrato,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [CONTRATO].[SupervisorInterContrato]
WHERE IDSupervisorIntervContrato = @IDSupervisorIntervContrato
END
