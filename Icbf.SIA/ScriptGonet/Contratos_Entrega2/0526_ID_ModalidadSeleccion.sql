use SIA
GO

/***************************************************
2014-07-24
Gonet-jorge vizcaino
Crecion de ModalidadSeleccion
***************************************************/

ALTER table CONTRATO.ModalidadSeleccion alter column sigla nvarchar(8)

IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'CONT_DIR')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('CONT_DIR','CONTRATACI�N DIRECTA','CD',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	 
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'CM')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('CM','CONSURSO DE MERITOS','CM',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'CMA')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('CMA','CONCURSO DE M�RITOS ABIERTO','CMA',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'LP')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('LP','LICITACION PUBLICA','LP',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'MC')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('MC','M�NIMA CUANT�A','MC',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

 
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'SA-MC')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('SA-MC','SELECCI�N ABREVIADA DE MENOR CUANT�A','SA-MC',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'SA-SIE')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('SA-SIE','SELECCI�N ABREVIADA SUBASTA INVERSA ELECTR�NICA','SA-SIE',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'SA-SIP')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('SA-SIP','SELECCI�N ABREVIADA SUBASTA INVERSA PRESENCIAL','SA-SIP',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end


	
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'CDA')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('CDA','CONTRATACI�N DIRECTA DE APORTE','CDA',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	 
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'CPA')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('CPA','CONVOCATORIA P�BLICA DE APORTE','CPA',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	 
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'CPA-LH')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('CPA-LH',' CONVOCATORIA PUBLICA DE APORTE CON LISTA DE HABILITACION','CPA-LH',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

	 
IF not exists (select 1 from CONTRATO.ModalidadSeleccion where CodigoModalidad = 'CPALO')
BEGIN
	insert into  CONTRATO.ModalidadSeleccion (CodigoModalidad,
										Nombre,
										Sigla,
										Estado,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica) 
	values ('CPALO','CONVOCATORIA P�BLICA DE APORTE CON LISTA DE OFERENTES','CPALO',1,'jorge.vizcaino',GETDATE(),NULL,NULL)
	
end

