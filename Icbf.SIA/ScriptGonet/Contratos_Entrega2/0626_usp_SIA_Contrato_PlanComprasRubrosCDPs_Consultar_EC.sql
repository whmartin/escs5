USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDPs_Consultar]    Script Date: 09/09/2014 11:38:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasRubrosCDPs_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDPs_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDPs_Consultar]    Script Date: 09/09/2014 11:38:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) PlanComprasRubrosCDP
-- =============================================
-- =============================================
-- Author:		Emilio Calapi�a
-- Create date:  09/09/2014 11:39:36 AM
-- Description:	Se agrega columna IdPagosDetalle al select
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDPs_Consultar] --725
	@NumeroConsecutivoPlanCompras	INT = NULL,
	@IDRubroPlanComprasContrato		INT = NULL,
	@ValorRubro						NUMERIC(32,2) = NULL
AS
BEGIN
	SELECT RPC.IDRubroPlanComprasContrato,
		 RPC.ValorRubroPresupuestal,
		 RPC.IDPlanDeComprasContratos,
		 RPC.IDRubro,
		 PCC.IDPlanDeCompras NumeroConsecutivoPlanCompras,
		 RPC.UsuarioCrea,
		 RPC.FechaCrea,
		 RPC.UsuarioModifica,
		 RPC.FechaModifica,
		 RPC.IdPagosDetalle
	FROM Contrato.RubroPlanComprasContrato RPC
		INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON RPC.IDPlanDeComprasContratos = PCC.IDPlanDeComprasContratos
	WHERE RPC.IDRubroPlanComprasContrato = COALESCE(@IDRubroPlanComprasContrato,RPC.IDRubroPlanComprasContrato)
		AND RPC.ValorRubroPresupuestal = COALESCE(@ValorRubro,RPC.ValorRubroPresupuestal)
		AND PCC.IDPlanDeCompras = COALESCE(@NumeroConsecutivoPlanCompras,PCC.IDPlanDeCompras)
END

GO


