USE SIA
GO

-- =============================================
-- Author:		Gonet/Jose de los reyes
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion tipo tabla RubrosPlanCompraContratos
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'ProductoPlanCompraContratos' AND ss.name = N'dbo')
	DROP TYPE dbo.ProductoPlanCompraContratos
GO

CREATE TYPE dbo.ProductoPlanCompraContratos AS TABLE
(
 CodigoProducto					NUMERIC(18,0) NOT NULL, --CODIGO Producto Pacco
 CantidadCupos					NUMERIC(18,2) NULL --Cantidad de Cupos Pacco
)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'RubrosPlanCompraContratos' AND ss.name = N'dbo')
	DROP TYPE dbo.RubrosPlanCompraContratos
GO

CREATE TYPE dbo.RubrosPlanCompraContratos AS TABLE
(
 CodigoRubro					NVARCHAR(256) NOT NULL, --CODIGO Rubro Pacco
 ValorRubroPresupuestal			NUMERIC(30,2) NULL --Valor Rubro Pacco
)
