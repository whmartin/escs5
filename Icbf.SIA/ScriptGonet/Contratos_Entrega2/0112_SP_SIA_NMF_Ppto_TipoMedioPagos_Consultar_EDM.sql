USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_NMF_Ppto_TipoMedioPagos_Consultar]    Script Date: 29/05/2014 09:12:05 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_NMF_Ppto_TipoMedioPagos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_NMF_Ppto_TipoMedioPagos_Consultar]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  29/05/2014 10:16
-- Description:	Procedimiento almacenado que consulta un(a) TipoMedioPago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_NMF_Ppto_TipoMedioPagos_Consultar]
	@CodMedioPago NVARCHAR(256) = NULL,@DescTipoMedioPago NVARCHAR(256) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoMedioPago, CodMedioPago, DescTipoMedioPago, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Ppto].[TipoMedioPago] 
 WHERE CodMedioPago = CASE WHEN @CodMedioPago IS NULL THEN CodMedioPago ELSE @CodMedioPago END 
 AND DescTipoMedioPago LIKE CASE WHEN @DescTipoMedioPago IS NULL THEN DescTipoMedioPago ELSE @DescTipoMedioPago + '%' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END



