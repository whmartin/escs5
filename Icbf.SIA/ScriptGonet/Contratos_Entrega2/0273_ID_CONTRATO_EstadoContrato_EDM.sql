USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  27/06/2014 09:26 
-- Description:	Insert tabla [CONTRATO].[EstadoContrato] para mostrar opci�n men�.
-- =============================================

/****** Object:  Table [CONTRATO].[EstadoContrato]    Script Date: 08/07/2014 10:17:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[EstadoContrato]') AND type in (N'U'))
DROP TABLE [CONTRATO].[EstadoContrato]
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[EstadoContrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[EstadoContrato](
	[IDEstadoContrato] [int] IDENTITY(1,1) NOT NULL,
	[CodEstado] [char](4) NOT NULL,
	[Descripcion] [nvarchar](80) NOT NULL,
	[Inactivo] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_EsatdoContrato] PRIMARY KEY CLUSTERED 
(
	[IDEstadoContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO




IF not exists(SELECT [CodEstado] from [CONTRATO].[EstadoContrato] where [Descripcion] = 'Suscrito')

Begin

INSERT INTO [CONTRATO].[EstadoContrato]
           ([CodEstado]
           ,[Descripcion]
           ,[Inactivo]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('SUS'
           ,'Suscrito'
           ,1
           ,'Administrador'
           ,GetDate()
           ,null
           ,null)
END



