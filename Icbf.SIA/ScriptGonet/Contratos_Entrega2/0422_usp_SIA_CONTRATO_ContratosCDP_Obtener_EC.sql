USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]    Script Date: 01/07/2014 10:26:24 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_ContratosCDP_Obtener')
BEGIN
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]    Script Date: 01/07/2014 10:26:24 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 01/07/2014
-- Description:	 M�todo de consulta CDPs asociados a un contrato
-- basado en el sp usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosCDP_Obtener]
	@IdContrato INT
AS
BEGIN
	
	SELECT CCDP.[IdContratosCDP],
	CCDP.[IdCDP],
	CCDP.[IdContrato],
	RPCI.[DescripcionPCI] AS Regional,
	AI.Descripcion AS Area,
	IECDP.[CDP] AS NumeroCDP,
	IECDP.[FechaSolicitudCDP] AS FechaCDP,
	IECDP.[ValorActualCDP] AS ValorCDP,
	'' AS RubroPresupuestal,
	'' AS TipoFuenteFinanciamiento,
	'' AS RecursoPresupuestal,
	'' AS DependenciaAfectacionGastos,
	'' AS TipoDocumentoSoporte,
	'' AS TipoSituacionFondos,
	'' AS ConsecutivoPlanCompras,
	CCDP.[UsuarioCrea], CCDP.[FechaCrea], CCDP.[UsuarioModifica], CCDP.[FechaModifica]
	FROM [CONTRATO].[ContratosCDP] CCDP
	INNER JOIN [NMF].[Ppto].[InfoETLCDP] /*Ppto.InfoETLCDP*/ IECDP ON IECDP.[IdEtlCDP] = CCDP.[IdCDP] --Error en el sinonimo
	INNER JOIN [NMF].[Ppto].[RegionalesPCI] /*[Ppto].[RegionalesPCI]*/ RPCI on RPCI.IdRegionalPCI = IECDP.IdRegionalPCI
	INNER JOIN [NMF].[Ppto].[AreasxRubro] /*Ppto.AreasxRubro*/ APR on APR.IdVigencia = IECDP.IdVigencia and APR.IdCatalogo = IECDP.IdCatRubro
	INNER JOIN [NMF].[Ppto].[AreasInternas] /*Ppto.AreasInternas*/ AI On AI.IdAreasInternas = APR.IdAreasInternas
	WHERE CCDP.[IdContrato] = @IdContrato

END

GO


