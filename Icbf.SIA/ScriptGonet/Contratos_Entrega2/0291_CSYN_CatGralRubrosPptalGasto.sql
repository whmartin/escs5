USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion sinonimo [Ppto].[CatGralRubrosPptalGasto]
-- =============================================


/****** Object:  Synonym [Ppto].[CatGralRubrosPptalGasto]    Script Date: 01/07/2014 16:35:59 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CatGralRubrosPptalGasto' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[CatGralRubrosPptalGasto]
GO

/****** Object:  Synonym [Ppto].[CatGralRubrosPptalGasto]    Script Date: 01/07/2014 16:35:59 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CatGralRubrosPptalGasto' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[CatGralRubrosPptalGasto] FOR [NMF].[Ppto].[CatGralRubrosPptalGasto]
GO


