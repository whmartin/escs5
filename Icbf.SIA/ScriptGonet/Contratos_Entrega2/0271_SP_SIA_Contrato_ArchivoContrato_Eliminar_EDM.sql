USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_OAC_OAC_DocVisitasReclamoArchivos_Eliminar]    Script Date: 26/06/2014 09:47:47 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ArchivoContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ArchivoContrato_Eliminar]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  26/06/2014 3:19:08 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocVisitasReclamoArchivos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ArchivoContrato_Eliminar]
	@IdArchivoContrato INT
AS
BEGIN
	DELETE [CONTRATO].[ArchivosContratos] WHERE [IdArchivoContrato] = @IdArchivoContrato
END



