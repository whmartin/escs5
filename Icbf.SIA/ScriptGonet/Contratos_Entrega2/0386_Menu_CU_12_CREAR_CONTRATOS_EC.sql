USE [SIA]
GO 
-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date:  12/05/2014 11:30:14 PM
-- Description:	Insert tabla [SEG].[Programa] para mostrar opci�n men�.
-- =============================================

IF not exists(SELECT [IdModulo] FROM [SEG].[Programa] WHERE [NombrePrograma] = 'Crear Contrato')
BEGIN
    DECLARE @IdProgramaPadre INT = null
	DECLARE @IdModulo INT = null
	SELECT @IdModulo = IdModulo FROM [SEG].[Modulo] WHERE [NombreModulo] = 'Contratos'
	
	IF @IdModulo is not null
	BEGIN
        
		INSERT INTO [SEG].[Programa]
			   ([IdModulo]
			   ,[NombrePrograma]
			   ,[CodigoPrograma]
			   ,[Posicion]
			   ,[Estado]
			   ,[UsuarioCreacion]
			   ,[FechaCreacion]
			   ,[UsuarioModificacion]
			   ,[FechaModificacion]
			   ,[VisibleMenu]
			   ,[generaLog]
			   ,[Padre])
		 VALUES
			   (@IdModulo
			   ,'Crear Contrato'
			   ,'Contratos/Contratos'
			   ,0
			   ,1
			   ,'Administrador'
			   ,GETDATE()
			   ,NULL
			   ,NULL
			   ,1
			   ,1
			   ,NULL)		   
			   
	END

END