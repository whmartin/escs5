USE [SIA]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Eliminacion Tabla [CONTRATO].[SupervisorIntervContrato]
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SupervisorIntervContrato_TipoSupvInterventor]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SupervisorIntervContrato]'))
ALTER TABLE [CONTRATO].[SupervisorIntervContrato] DROP CONSTRAINT [FK_SupervisorIntervContrato_TipoSupvInterventor]
GO

/****** Object:  Table [CONTRATO].[SupervisorIntervContrato]    Script Date: 23/05/2014 15:40:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[SupervisorIntervContrato]') AND type in (N'U'))
DROP TABLE [CONTRATO].[SupervisorIntervContrato]
GO
