USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Municipio_sConsultarPorIdsDep]    Script Date: 05/20/2014 15:24:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_DIV_Municipio_sConsultarPorIdsDep]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_DIV_Municipio_sConsultarPorIdsDep]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_DIV_Municipio_sConsultarPorIdsDep]    Script Date: 05/20/2014 15:24:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Abraham Rivero
-- Create date:  05/13/2014 5:03:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) Municipio por idsDepartamento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Municipio_sConsultarPorIdsDep]
	@IdsDepartamento nvarchar(MAX) = NULL
AS
BEGIN
	

 SELECT IdMunicipio, 
		IdDepartamento, 
		CodigoMunicipio, 
		NombreMunicipio, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Municipio] 
	WHERE (@IdsDepartamento IS NULL OR IdDepartamento IN (select * from dbo.fnSplitString(@IdsDepartamento,',')))  
	      
	ORDER BY NombreMunicipio ASC      
END



GO


