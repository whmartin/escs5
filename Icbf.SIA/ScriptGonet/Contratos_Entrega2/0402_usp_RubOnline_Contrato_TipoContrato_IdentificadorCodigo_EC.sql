USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo]    Script Date: 10/06/2014 11:06:11 a.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo')
BEGIN
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo]    Script Date: 10/06/2014 11:06:11 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 10/06/2014
-- Description:	Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos) del tipo de contrato la 
-- informaci�n del mismo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo]
	@IdTipoContrato INT = NULL,
	@CodigoTipoContrato NVARCHAR(30) = NULL
AS
BEGIN
	
	SELECT IdTipoContrato, CodigoTipoContrato, NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion,
    RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica
	FROM CONTRATO.TipoContrato 
	WHERE IdTipoContrato = ISNULL(@IdTipoContrato, IdTipoContrato) AND
	CodigoTipoContrato = ISNULL(@CodigoTipoContrato, CodigoTipoContrato)

END

GO


