USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 15/08/2014 10:32:37 a.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 15/08/2014 10:32:37 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 02/06/2014
-- Description:	
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal] 1108, 150, 1111
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
	@IdContratoConvMarco INT,
	@ValorInicialContConv DECIMAL, 
	@IdContrato INT

AS
BEGIN
	
	DECLARE @ValorFinalContratoConvMarco DECIMAL, @ValorInicialContratoConvMarco DECIMAL, @SumValorFinalContratos DECIMAL
	SELECT @ValorInicialContratoConvMarco=ValorInicialContrato 
	FROM CONTRATO.Contrato WHERE IdContrato = @IdContratoConvMarco

	SET @ValorFinalContratoConvMarco = @ValorInicialContratoConvMarco; --SE ESTABLECE ASI YA QUE EN FASE I el vlr inicial es igual al final, cuando se empiece a calcular el valor final con adiciones y reducciones esta linea se deberia eliminar
	print cast(@ValorFinalContratoConvMarco as varchar)

	DECLARE @IdTipoContratoAsociado INT
	SELECT @IdTipoContratoAsociado = [IdContratoAsociado] FROM [SIA].[CONTRATO].[TipoContratoAsociado] WHERE [CodContratoAsociado] = 'CM'

	SELECT @SumValorFinalContratos=ISNULL(SUM(ValorFinalContrato), 0) 
	FROM CONTRATO.Contrato WHERE FK_IdContrato = @IdContratoConvMarco AND IdContrato <> @IdContrato AND
	IdContratoAsociado = @IdTipoContratoAsociado
	print cast(@SumValorFinalContratos as varchar)


	IF ((@ValorFinalContratoConvMarco) >= (@SumValorFinalContratos + @ValorInicialContConv))
	BEGIN

		SELECT @IdContratoConvMarco AS IdContrato
		RETURN 

	END


END



GO


