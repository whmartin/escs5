USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Insert data [CONTRATO].[TipoContratoAsociado]
-- =============================================

SET IDENTITY_INSERT [CONTRATO].[TipoContratoAsociado] ON 
IF NOT EXISTS (SELECT * FROM [CONTRATO].[TipoContratoAsociado] WHERE CodContratoAsociado = 'CM' AND Descripcion = 'Convenio marco')
	INSERT [CONTRATO].[TipoContratoAsociado] ([IdContratoAsociado], [CodContratoAsociado], [Descripcion], [Inactivo]) VALUES (1, N'CM', N'Convenio marco', 1)
IF NOT EXISTS (SELECT * FROM [CONTRATO].[TipoContratoAsociado] WHERE CodContratoAsociado = 'CCA' AND Descripcion = 'Contrato/Convenio Adhesión')
	INSERT [CONTRATO].[TipoContratoAsociado] ([IdContratoAsociado], [CodContratoAsociado], [Descripcion], [Inactivo]) VALUES (2, N'CCA', N'Contrato/Convenio Adhesión', 1)
SET IDENTITY_INSERT [CONTRATO].[TipoContratoAsociado] OFF
