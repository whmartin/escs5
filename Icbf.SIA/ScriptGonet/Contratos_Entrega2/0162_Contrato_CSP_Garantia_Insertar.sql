USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_Garantia_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Insertar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/4/2014 9:59:59 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Insertar]
		@IDGarantia INT OUTPUT, 	
		@IDTipoGarantia INT,	
		@NumeroGarantia NVARCHAR(80),	
		@FechaAprobacionGarantia DATETIME,	
		@FechaCertificacionGarantia DATETIME,	
		@FechaDevolucion DATETIME,	
		@MotivoDevolucion NVARCHAR(800),	
		@IDUsuarioAprobacion INT,	
		@IDUsuarioDevolucion INT,	
		@IdContrato INT,	
		@BeneficiarioICBF BIT,
		@BeneficiarioOTROS BIT,		
		@DescripcionBeneficiarios NVARCHAR(400),	
		@FechaInicioGarantia DATETIME,	
		@FechaExpedicionGarantia DATETIME,	
		@FechaVencimientoInicialGarantia DATETIME,	
		@FechaVencimientoFinalGarantia DATETIME,	
		@FechaReciboGarantia DATETIME,	
		@ValorGarantia NVARCHAR(50),	
		@Anexos BIT,	
		@ObservacionesAnexos NVARCHAR(400),	
		@EntidadProvOferenteAseguradora INT, 
		@UsuarioCrea NVARCHAR(250),
		@Estado BIT,
		@IDSucursalAseguradoraContrato INT,
		@IDEstadosGarantias INT
AS
BEGIN
	IF EXISTS(SELECT * FROM Contrato.Garantia WHERE NumeroGarantia=@NumeroGarantia)
	BEGIN
		RAISERROR('Ya existe un registro con este(a) Número Garantía',16,1)
	    RETURN
	END
	INSERT INTO Contrato.Garantia(IDTipoGarantia, NumeroGarantia, FechaAprobacionGarantia, 
	FechaCertificacionGarantia, FechaDevolucion, MotivoDevolucion, IDUsuarioAprobacion, IDUsuarioDevolucion,
	IdContrato, BeneficiarioICBF,BeneficiarioOTROS, DescripcionBeneficiarios, FechaInicioGarantia, FechaExpedicionGarantia, 
	FechaVencimientoInicialGarantia, FechaVencimientoFinalGarantia, FechaReciboGarantia, ValorGarantia,
	Anexos, ObservacionesAnexos, EntidadProvOferenteAseguradora, UsuarioCrea, FechaCrea, Estado,IDSucursalAseguradoraContrato,
	IDEstadosGarantias)
	VALUES(@IDTipoGarantia, @NumeroGarantia,@FechaAprobacionGarantia,
	 @FechaCertificacionGarantia, @FechaDevolucion, @MotivoDevolucion, @IDUsuarioAprobacion, @IDUsuarioDevolucion,
	  @IdContrato, @BeneficiarioICBF,@BeneficiarioOTROS, @DescripcionBeneficiarios, @FechaInicioGarantia, @FechaExpedicionGarantia, 
	  @FechaVencimientoInicialGarantia, @FechaVencimientoFinalGarantia, @FechaReciboGarantia, @ValorGarantia, 
	  @Anexos, @ObservacionesAnexos, @EntidadProvOferenteAseguradora, @UsuarioCrea, GETDATE(),@Estado,@IDSucursalAseguradoraContrato,
	  @IDEstadosGarantias)
	SELECT @IDGarantia = @@IDENTITY 		
END
