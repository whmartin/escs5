USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Crear tabla [CONTRATO].[TipoContratoAsociado]
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_TipoContratoAsociado]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_TipoContratoAsociado]
END


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ContratoAsociado]') AND type in (N'U'))
BEGIN

/****** Object:  Table [CONTRATO].[ContratoAsociado]    Script Date: 09/06/2014 04:28:06 p.m. ******/
DROP TABLE [CONTRATO].[ContratoAsociado]

END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[TipoContratoAsociado]') AND type in (N'U'))
BEGIN
/****** Object:  Table [CONTRATO].[TipoContratoAsociado]    Script Date: 07/07/2014 09:17:50 a.m. ******/
DROP TABLE [CONTRATO].[TipoContratoAsociado]

END
GO

/****** Object:  Table [CONTRATO].[TipoContratoAsociado]    Script Date: 07/07/2014 09:17:50 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CONTRATO].[TipoContratoAsociado](
	[IdContratoAsociado] [int] IDENTITY(1,1) NOT NULL,
	[CodContratoAsociado] [varchar](8) NULL,
	[Descripcion] [nvarchar](50) NULL,
	[Inactivo] [bit] NULL,
 CONSTRAINT [PK_ContratoAsociado] PRIMARY KEY CLUSTERED 
(
	[IdContratoAsociado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO