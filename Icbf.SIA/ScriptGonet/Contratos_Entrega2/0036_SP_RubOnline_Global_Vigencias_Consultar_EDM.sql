USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]    Script Date: 20/05/2014 09:55:19 a.m. ******/
IF  EXISTS (SELECT	* FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Global_Vigencias_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Global_Vigencias_Consultar]
GO


-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Create date:  20/05/2014
-- Description:	Procedimiento almacenado que consulta las vigencias 
-- =============================================
create PROCEDURE [dbo].[usp_RubOnline_Global_Vigencias_Consultar]
	@Activo bit = NULL
AS
BEGIN
	SELECT
		IdVigencia,
		AcnoVigencia,
		Activo,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Global].[Vigencia]
	WHERE Activo =
		CASE
			WHEN @Activo IS NULL THEN Activo ELSE @Activo
		END
	ORDER BY AcnoVigencia DESC
END



