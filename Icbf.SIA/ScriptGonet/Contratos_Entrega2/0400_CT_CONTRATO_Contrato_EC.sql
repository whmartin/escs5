USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	agragar campos a  tabla  [CONTRATO].[Contrato]
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Global_Vigencia_Inicial]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN	
	ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Global_Vigencia_Inicial]
END



IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Global_Vigencia_Final]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Global_Vigencia_Final]
END
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_TipoContratoAsociado]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_TipoContratoAsociado]
END
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_TipoContrato]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_TipoContrato]
END
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_RegimenContratacion]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_RegimenContratacion]
END
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_NumeroProceso]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_NumeroProceso]
END
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_ModalidadSeleccion]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_ModalidadSeleccion]
END
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_EstadoContrato]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_EstadoContrato]
END
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_CONTRATO_Contrato_TipoFormaPago]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_CONTRATO_Contrato_TipoFormaPago]
END
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].[FK_Contrato_CategoriaContrato]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_CategoriaContrato]
END
GO


IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].DF_Contrato_Suscrito'))
BEGIN	
	ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [DF_Contrato_Suscrito]
END

/******************************************************Inicio creacion de campo********************************************************/

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdEmpleadoSolicitante'))
BEGIN
	alter table CONTRATO.Contrato add IdEmpleadoSolicitante Int NULL
END



IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdRegionalSolicitante'))
BEGIN
	alter table CONTRATO.Contrato add IdRegionalSolicitante Int NULL
END

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdDependenciaSolicitante'))
BEGIN
	alter table CONTRATO.Contrato add IdDependenciaSolicitante Int NULL
END

--	[] [int] NULL,
--	[] [int] NULL,
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdCargoSolicitante'))
BEGIN
	alter table CONTRATO.Contrato add IdCargoSolicitante Int NULL
END
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdEstadoContrato'))
BEGIN
	alter table CONTRATO.Contrato add IdEstadoContrato Int NULL
END
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdUsuario'))
BEGIN
	alter table CONTRATO.Contrato add IdUsuario Int NULL
END
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdNumeroProceso'))
BEGIN
	alter table CONTRATO.Contrato add IdNumeroProceso Int NULL
END
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdModalidadSeleccion'))
BEGIN
	alter table CONTRATO.Contrato add IdModalidadSeleccion Int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('FechaAdjudicacionDelProceso'))
BEGIN
	alter table CONTRATO.Contrato add FechaAdjudicacionDelProceso datetime NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdCategoriaContrato'))
BEGIN
	alter table CONTRATO.Contrato add IdCategoriaContrato int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdTipoContrato'))
BEGIN
	alter table CONTRATO.Contrato add IdTipoContrato int NULL
END
--	[] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdModalidadAcademica'))
BEGIN
	alter table CONTRATO.Contrato add IdModalidadAcademica [nvarchar](30)  NULL
END
--	[] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdProfesion'))
BEGIN
	alter table CONTRATO.Contrato add IdProfesion  [nvarchar](30) NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ActaDeInicio'))
BEGIN
	alter table CONTRATO.Contrato add ActaDeInicio bit NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ManejaAporte'))
BEGIN
	alter table CONTRATO.Contrato add ManejaAporte bit NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ManejaRecurso'))
BEGIN
	alter table CONTRATO.Contrato add ManejaRecurso bit NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdRegimenContratacion'))
BEGIN
	alter table CONTRATO.Contrato add IdRegimenContratacion int NULL
END
--	[]  NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ObjetoDelContrato'))
BEGIN
	alter table CONTRATO.Contrato add ObjetoDelContrato [nvarchar](800) NULL
END
--	[]  NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('AlcanceObjetoDelContrato'))
BEGIN
	alter table CONTRATO.Contrato add AlcanceObjetoDelContrato [nvarchar](800) NULL
END
--	[]  NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ValorInicialContrato'))
BEGIN
	alter table CONTRATO.Contrato add ValorInicialContrato [numeric](30,8) NULL
END
--	[]  NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ValorFinalContrato'))
BEGIN
	alter table CONTRATO.Contrato add ValorFinalContrato [numeric](30, 8) NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('FechaInicioEjecucion'))
BEGIN
	alter table CONTRATO.Contrato add FechaInicioEjecucion datetime NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('FechaFinalizacionIniciaContrato'))
BEGIN
	alter table CONTRATO.Contrato add FechaFinalizacionIniciaContrato datetime NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('FechaFinalTerminacionContrato'))
BEGIN
	alter table CONTRATO.Contrato add FechaFinalTerminacionContrato datetime NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ManejaVigenciasFuturas'))
BEGIN
	alter table CONTRATO.Contrato add ManejaVigenciasFuturas bit NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdVigenciaInicial'))
BEGIN
	alter table CONTRATO.Contrato add IdVigenciaInicial int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdVigenciaFinal'))
BEGIN
	alter table CONTRATO.Contrato add IdVigenciaFinal int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdFormaPago'))
BEGIN
	alter table CONTRATO.Contrato add IdFormaPago int NULL
END
--	[] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('DatosAdicionalesLugarEjecucion'))
BEGIN
	alter table CONTRATO.Contrato add DatosAdicionalesLugarEjecucion  [nvarchar](800) NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdEmpleadoOrdenadorGasto'))
BEGIN
	alter table CONTRATO.Contrato add IdEmpleadoOrdenadorGasto int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdRegionalOrdenador'))
BEGIN
	alter table CONTRATO.Contrato add IdRegionalOrdenador int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdDependenciaOrdenador'))
BEGIN
	alter table CONTRATO.Contrato add IdDependenciaOrdenador int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdCargoOrdenador'))
BEGIN
	alter table CONTRATO.Contrato add IdCargoOrdenador int NULL
END
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdContratoAsociado'))
BEGIN
	alter table CONTRATO.Contrato add IdContratoAsociado Int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ConvenioMarco'))
BEGIN
	alter table CONTRATO.Contrato add ConvenioMarco bit NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ConvenioAdhesion'))
BEGIN
	alter table CONTRATO.Contrato add ConvenioAdhesion bit NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('FK_IdContrato'))
BEGIN
	alter table CONTRATO.Contrato add FK_IdContrato int NULL
END
--	[]  NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('NumeroContrato'))
BEGIN
	alter table CONTRATO.Contrato add NumeroContrato [nvarchar](50) NULL
END
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdRegionalContrato'))
BEGIN
	alter table CONTRATO.Contrato add IdRegionalContrato Int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('FechaSuscripcionContrato'))
BEGIN
	alter table CONTRATO.Contrato add FechaSuscripcionContrato datetime NULL
END
--	[]  NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ConsecutivoSuscrito'))
BEGIN
	alter table CONTRATO.Contrato add ConsecutivoSuscrito [nvarchar](30) NULL
END
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('IdUsuarioSuscribe'))
BEGIN
	alter table CONTRATO.Contrato add IdUsuarioSuscribe Int NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('Suscrito'))
BEGIN
	alter table CONTRATO.Contrato add Suscrito bit NULL
END
--	[] [int] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('ConsecutivoPlanCompasAsociado'))
BEGIN
	alter table CONTRATO.Contrato add ConsecutivoPlanCompasAsociado Int NULL
END
--	[]  NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('UsuarioCrea'))
BEGIN
	alter table CONTRATO.Contrato add UsuarioCrea [nvarchar](250) NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('FechaCrea'))
BEGIN
	alter table CONTRATO.Contrato add FechaCrea datetime NULL
END
--	[]  NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('UsuarioModifica'))
BEGIN
	alter table CONTRATO.Contrato add UsuarioModifica [nvarchar](250) NULL
END
--	[] [] NULL,
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('FechaModifica'))
BEGIN
	alter table CONTRATO.Contrato add FechaModifica datetime NULL
END

/******************************************************Fin creacion de campo********************************************************/

ALTER TABLE [CONTRATO].[Contrato] ADD  CONSTRAINT [DF_Contrato_Suscrito]  DEFAULT ((0)) FOR [Suscrito]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_CategoriaContrato] FOREIGN KEY([IdCategoriaContrato])
REFERENCES [CONTRATO].[CategoriaContrato] ([IdCategoriaContrato])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_CategoriaContrato]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_CONTRATO_Contrato_TipoFormaPago] FOREIGN KEY([IdFormaPago])
REFERENCES [CONTRATO].[TipoFormaPago] ([IdTipoFormaPago])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_CONTRATO_Contrato_TipoFormaPago]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_EstadoContrato] FOREIGN KEY([IdEstadoContrato])
REFERENCES [CONTRATO].[EstadoContrato] ([IDEstadoContrato])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_EstadoContrato]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_ModalidadSeleccion] FOREIGN KEY([IdModalidadSeleccion])
REFERENCES [CONTRATO].[ModalidadSeleccion] ([IdModalidad])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_ModalidadSeleccion]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_NumeroProceso] FOREIGN KEY([IdNumeroProceso])
REFERENCES [CONTRATO].[NumeroProcesos] ([IdNumeroProceso])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_NumeroProceso]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_RegimenContratacion] FOREIGN KEY([IdRegimenContratacion])
REFERENCES [CONTRATO].[RegimenContratacion] ([IdRegimenContratacion])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_RegimenContratacion]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_TipoContrato] FOREIGN KEY([IdTipoContrato])
REFERENCES [CONTRATO].[TipoContrato] ([IdTipoContrato])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_TipoContrato]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_TipoContratoAsociado] FOREIGN KEY([IdContratoAsociado])
REFERENCES [CONTRATO].[TipoContratoAsociado] ([IdContratoAsociado])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_TipoContratoAsociado]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Global_Vigencia_Final] FOREIGN KEY([IdVigenciaFinal])
REFERENCES [Global].[Vigencia] ([IdVigencia])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Global_Vigencia_Final]
GO

ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Global_Vigencia_Inicial] FOREIGN KEY([IdVigenciaInicial])
REFERENCES [Global].[Vigencia] ([IdVigencia])
GO

ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Global_Vigencia_Inicial]
GO


