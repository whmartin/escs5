USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/14/2014 4:20:50 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Consultar]
	@IdLugarEjecucionContratos INT
AS
BEGIN
	SELECT
		IdLugarEjecucionContratos,
		IdContrato,
		IdDepartamento,
		IdRegional,
		NivelNacional,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].[LugarEjecucionContrato]
	WHERE IdLugarEjecucionContratos = @IdLugarEjecucionContratos
END
