USE [SIA]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_RPContratos_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_RPContratos_Insertar]
GO


-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  11/07/2014 14:40:13 
-- Description:	Procedimiento almacenado que guarda un nuevo NumeroProcesos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_RPContratos_Insertar]
		@IdRPContratos INT OUTPUT, 	
		@IdContrato INT,
		@IdRP INT,
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO [CONTRATO].[RPContratos]
           ([IdContrato]
           ,[IdRP]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           (
           @IdContrato
		   ,@IdRP
           ,@UsuarioCrea
           ,getdate()
           ,null
           ,null)
		   SELECT @IdRPContratos = @@IDENTITY

END



