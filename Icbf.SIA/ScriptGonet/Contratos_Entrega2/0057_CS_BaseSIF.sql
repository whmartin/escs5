USE SIA

/********************************
2014-05-22
Creamos el schema BaseSIF 
jorge Vizcaino
********************************/
GO

--/****** Object:  Schema [BaseSIF]    Script Date: 22/05/2014 11:44:25 ******/
--IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'BaseSIF')
--DROP SCHEMA [BaseSIF]
--GO

/****** Object:  Schema [BaseSIF]    Script Date: 22/05/2014 11:44:25 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'BaseSIF')
EXEC sys.sp_executesql N'CREATE SCHEMA [BaseSIF]'

GO


