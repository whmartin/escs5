USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_KACTUS_KPRODII_Da_Emple_DependenciaSolicitante_Consultar]    Script Date: 22/07/2014 10:05:55 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_KACTUS_KPRODII_Da_Emple_DependenciaSolicitante_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_KACTUS_KPRODII_Da_Emple_DependenciaSolicitante_Consultar]
GO
-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Create date:  17/07/2014 17:53
-- Description:	Procedimiento almacenado que Muestra la información relacionada con el certificado de disponibilidad presupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_KACTUS_KPRODII_Da_Emple_DependenciaSolicitante_Consultar]
	@IdDependenciaSolicitante INT = Null,
	@DependenciaSolicitante nVarchar(256) = Null

AS
BEGIN
	  SELECT distinct
      ID_depen IdDependenciaSolicitante
      ,nom_depe NombreDependenciaSolicitante
      FROM [KACTUS].[KPRODII].[dbo].[Da_Emple]
		Where isnull(ID_depen,0) = 
		CASE WHEN @IdDependenciaSolicitante IS NULL 
		THEN isnull(ID_depen,0) ELSE @IdDependenciaSolicitante END 
		And isnull(nom_depe,'') like
		CASE WHEN @DependenciaSolicitante IS NULL 
		THEN isnull(nom_depe,'') ELSE @DependenciaSolicitante + '%' END 

END




