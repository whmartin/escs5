USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionaless_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionaless_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/27/2014 6:43:39 PM
-- Description:	Procedimiento almacenado que consulta un(a) ConsecutivoContratoRegionales
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionaless_Consultar]
	@IdRegional INT = NULL,@IdVigencia INT = NULL,@Consecutivo NUMERIC(30) = NULL
AS
BEGIN
	SELECT
		MAX(CCR.Consecutivo) AS Consecutivo,
		MIN(CAST(CCR.IDConsecutivoContratoRegional AS nvarchar(50))) AS IDConsecutivoContratoRegional,
		CCR.IdRegional,
		CCR.IdVigencia,
		MIN(CCR.UsuarioCrea) AS UsuarioCrea,
		MIN(CAST(CCR.FechaCrea AS nvarchar(50))) AS FechaCrea,
		MIN(CCR.UsuarioModifica) AS UsuarioModifica,
		MIN(CAST(CCR.FechaModifica AS nvarchar(50))) AS FechaModifica,
		Reg.NombreRegional AS Regional,
		Vig.AcnoVigencia AS Vigencia
	FROM [CONTRATO].[ConsecutivoContratoRegionales] CCR
	INNER JOIN [DIV].[Regional] Reg
		ON CCR.IdRegional = Reg.IdRegional
	INNER JOIN [Global].[Vigencia] Vig
		ON CCR.IdVigencia = Vig.IdVigencia
	WHERE CCR.IdRegional =
		CASE
			WHEN @IdRegional IS NULL THEN CCR.IdRegional ELSE @IdRegional
		END
	AND CCR.IdVigencia =
		CASE
			WHEN @IdVigencia IS NULL THEN CCR.IdVigencia ELSE @IdVigencia
		END
	AND CCR.Consecutivo =
		CASE
			WHEN @Consecutivo IS NULL THEN CCR.Consecutivo ELSE @Consecutivo
		END
	GROUP BY	CCR.IdRegional,
				CCR.IdVigencia,
				Reg.NombreRegional,
				Vig.AcnoVigencia
END
