USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Modificar]    Script Date: 25/06/2014 14:35:33 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Modificar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Modificar]
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/22/2014 1:36:08 PM
-- Description:	Procedimiento almacenado que actualiza un(a) SolicitudModPlanCompras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Modificar]
		@IdSolicitudModPlanCompra INT,	
		@Justificacion NVARCHAR(896),	
		@NumeroSolicitud INT,	
		@FechaSolicitud DATETIME,	
		@IdEstadoSolicitud NVARCHAR(1),	
		@IdUsuarioSolicitud INT,	
		@IdPlanDeCompras INT,	
		--@IdVigencia INT,
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE CONTRATO.SolicitudModPlanCompras
	SET	Justificacion = @Justificacion,
		NumeroSolicitud = @NumeroSolicitud,
		FechaSolicitud = @FechaSolicitud,
		IdEstadoSolicitud = @IdEstadoSolicitud,
		IdUsuarioSolicitud = @IdUsuarioSolicitud,
		IdPlanDeCompras = @IdPlanDeCompras,
		--IdVigencia = @IdVigencia,
		UsuarioModifica = @UsuarioModifica,
		FechaModifica = GETDATE()
	WHERE IdSolicitudModPlanCompra = @IdSolicitudModPlanCompra
END

