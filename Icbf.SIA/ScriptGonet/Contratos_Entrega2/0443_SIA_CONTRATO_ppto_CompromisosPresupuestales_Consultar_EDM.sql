USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]    Script Date: 10/07/2014 02:23:10 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ppto_CompromisosPresupuestales_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ppto_CompromisosPresupuestales_Consultar]
GO

-- =============================================
-- Author:		Gonet / Efrain Diaz Mejia
-- Create date:  10/07/2014 14:32:20
-- Description: procedimiento almacenado que consulta la informacion de los RP 
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ppto_CompromisosPresupuestales_Consultar]
	@IdRP INT = NULL
	,@IdRegional INT = NULL
	,@IdVigencia INT = NULL
	,@ValorTotalDesde numeric(30, 8) = null
	,@ValorTotalHasta numeric(30, 8) = null
	,@FechaRPDesde DATETIME = null
	,@FechaRPHasta DATETIME = null
AS
BEGIN
SELECT top 501 
      CP.IdCompromiso IdRP
      ,CP.[UsuarioCrea]
      ,CP.[FechaCrea]
      ,CP.[UsuarioModifica]
      ,CP.[FechaModifica]
	  ,IAG.ValorInicialItem ValorRP
	  ,R.CodigoRegional CodigoRegional
	  ,CP.FechaRegCompromiso FechaRP
	  ,rtrim(ltrim(R.CodigoRegional)) +' - '+ rtrim(ltrim(R.NombreRegional)) NombreRegional
  FROM [Ppto].[CompromisosPresupuestales] CP
  inner join [Ppto].[CompItemAfectacionGasto] IAG ON IAG.IdCompromiso = CP.IdCompromiso  
  inner join Ppto.RegionalesPCI RPCI on RPCI.IdRegionalPCI = CP.IdRegionalPCI
  inner join DIV.Regional R on RPCI.CodRegionalIcbf = R.CodigoRegional
  Where isnull(@IdRP,0) = CASE WHEN @IdRP IS NULL THEN isnull(@IdRP,0) ELSE @IdRP END 
		And isnull(CP.IdVigencia,0) = CASE WHEN @IdVigencia IS NULL THEN isnull(CP.IdVigencia,0) ELSE @IdVigencia END 
		And isnull(R.IdRegional,0) = CASE WHEN @IdRegional IS NULL THEN isnull(R.IdRegional,0) ELSE @IdRegional END 
		And isnull(IAG.ValorInicialItem,0) >= CASE WHEN @ValorTotalDesde IS NULL THEN isnull(IAG.ValorInicialItem,0) ELSE @ValorTotalDesde END 
		And isnull(IAG.ValorInicialItem,0) <= CASE WHEN @ValorTotalHasta IS NULL THEN  isnull(IAG.ValorInicialItem,0) ELSE @ValorTotalHasta END 
	    And isnull(CP.FechaRegCompromiso,'') >= CASE WHEN @FechaRPDesde IS NULL THEN isnull(CP.FechaRegCompromiso,'') ELSE @FechaRPDesde END 
	    And isnull(CP.FechaRegCompromiso,'') <= CASE WHEN @FechaRPHasta IS NULL THEN isnull(CP.FechaRegCompromiso,'') ELSE @FechaRPHasta END 
 
 IF @@ROWCOUNT >= 500
  BEGIN
   RAISERROR('La consulta arroja demasiados resultados,por favor refine su consulta',16,1)
   RETURN
  END
END






