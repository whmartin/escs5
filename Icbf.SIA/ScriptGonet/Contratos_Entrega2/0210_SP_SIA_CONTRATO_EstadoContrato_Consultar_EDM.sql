USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_EstadoContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_EstadoContrato_Consultar]
GO

-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  6/18/2014 4:52:44 PM
-- Description:	Procedimiento almacenado que consulta un(a) estado de Contrato
-- =============================================

CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_EstadoContrato_Consultar]
	@IdEstadoContrato INT = NULL,
	@CodigoEstadoContrato NVARCHAR(4) = NULL,
	@Descripcion NVARCHAR(80) = NULL,
	@Inactivo BIT = null
	as

	begin
	SELECT	 [IDEstadoContrato]
      ,[CodEstado]
      ,[Descripcion]
      ,[Inactivo]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
	FROM [CONTRATO].[EstadoContrato]
	  WHERE IDEstadoContrato = CASE WHEN @IdEstadoContrato IS NULL THEN IDEstadoContrato ELSE @IdEstadoContrato END 
		 AND CodEstado = CASE WHEN @CodigoEstadoContrato IS NULL THEN CodEstado ELSE @CodigoEstadoContrato END 
		 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
		 AND Inactivo = CASE WHEN @Inactivo IS NULL THEN Inactivo ELSE @Inactivo END 
	end
	
	GO



