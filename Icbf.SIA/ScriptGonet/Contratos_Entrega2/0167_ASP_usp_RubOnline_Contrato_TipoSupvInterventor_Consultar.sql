USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]    Script Date: 04/06/2014 12:05:39 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]
	@IdTipoSupvInterventor INT
AS
BEGIN
	SELECT IDTipoSuperInter as	IdTipoSupvInterventor,
	  Descripcion as	Nombre,
		1 Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoSuperInter]
	WHERE IDTipoSuperInter = @IdTipoSupvInterventor
END

