USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Insertar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 1:59:51 PM
-- Description:	Procedimiento almacenado que guarda un nuevo SucursalAseguradoraContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Insertar]
		@IDSucursalAseguradoraContrato INT OUTPUT, 	@IDDepartamento INT,	@IDMunicipio INT,	@Nombre NVARCHAR(160),	@IDZona INT,	@DireccionNotificacion NVARCHAR(80),	@CorreoElectronico NVARCHAR(80),	@Indicativo NVARCHAR(3),	@Telefono NVARCHAR(40),	@Extension NVARCHAR(8),	@Celular NVARCHAR(40),	@IDEntidadProvOferente INT,	@CodigoSucursal INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.SucursalAseguradoraContrato(IDDepartamento, IDMunicipio, Nombre, IDZona, DireccionNotificacion, CorreoElectronico, Indicativo, Telefono, Extension, Celular, IDEntidadProvOferente, CodigoSucursal, UsuarioCrea, FechaCrea)
					  VALUES(@IDDepartamento, @IDMunicipio, @Nombre, @IDZona, @DireccionNotificacion, @CorreoElectronico, @Indicativo, @Telefono, @Extension, @Celular, @IDEntidadProvOferente, @CodigoSucursal, @UsuarioCrea, GETDATE())
	SELECT @IDSucursalAseguradoraContrato = @@IDENTITY 		
END
