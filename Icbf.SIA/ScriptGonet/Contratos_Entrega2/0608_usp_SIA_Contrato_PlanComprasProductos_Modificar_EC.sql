USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasProductos_Modificar]    Script Date: 08/28/2014 11:19:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasProductos_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Modificar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasProductos_Modificar]    Script Date: 08/28/2014 11:19:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:32 PM
-- Description:	Procedimiento almacenado que actualiza un(a) PlanComprasProductos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Modificar]
		@IDProductoPlanCompraContrato	INT,
		@IDPlanDeComprasContratos		INT,
		@CodigoProducto					NVARCHAR(30),
		@CantidadCupos					NUMERIC(15,2),
		@ValorProducto					NUMERIC(30,2), 
		@Tiempo							NUMERIC(30,2),
		@IdUnidadTiempo					INT,
		@UnidadTiempo					NVARCHAR(250),
		@UsuarioModifica				NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ProductoPlanCompraContratos 
	SET CantidadCupos = @CantidadCupos
	   ,ValorProducto = @ValorProducto
	   ,Tiempo = @Tiempo
	   ,IdUnidadTiempo = @IdUnidadTiempo
	   ,UnidadTiempo = @UnidadTiempo
	   ,UsuarioModifica = @UsuarioModifica
	   ,FechaModifica = GETDATE() 
	WHERE IDProducto = @CodigoProducto
		and IDProductoPlanCompraContrato = @IDProductoPlanCompraContrato
		and IDPlanDeComprasContratos = @IDPlanDeComprasContratos
END

GO


