USE [SIA]
GO

-- =============================================
-- Author:		Abraham
-- Create date: 2014-05-28
-- Description:	Creacion synonyms TipoMedioPago
-- =============================================

/****** Object:  Synonym [BaseSIF].[TipoMedioPago]    Script Date: 29/05/2014 03:46:26 p.m. ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoMedioPago' AND schema_id = SCHEMA_ID(N'ppto'))
DROP SYNONYM [ppto].[TipoMedioPago]
GO

/****** Object:  Synonym [BaseSIF].[TipoMedioPago]    Script Date: 29/05/2014 03:46:26 p.m. ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoMedioPago' AND schema_id = SCHEMA_ID(N'ppto'))
CREATE SYNONYM [ppto].[TipoMedioPago] FOR [NMF].[ppto].[TipoMedioPago]
GO


