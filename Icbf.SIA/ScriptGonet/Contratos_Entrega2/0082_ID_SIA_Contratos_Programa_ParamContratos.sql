USE [SIA]
/***********************************************
Creado Por: Carlos Andr�s C�rdenas
El dia : 2013-08-26
Modificado Por: Abraham Rivero Dom�nguez
El d�a : 2014-05-27
Permite : Crear el programa Parametrizaci�n Contable en inversiones en el m�dulo de Parametrizador 
y asignarle permisos al administrador
***********************************************/
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Contratos/ConsecutivoContratoRegionales' )
begin
      
	    

      insert INTO [SEG].[Programa]([IdModulo],
					[NombrePrograma],
					[CodigoPrograma],
					[Posicion],
					[Estado],
					[UsuarioCreacion],
					[FechaCreacion],
					[UsuarioModificacion],[FechaModificacion],[VisibleMenu],[generaLog],[Padre])
      SELECT IdModulo,'Administrar consecutivo de contrato','Contratos/ConsecutivoContratoRegionales',1,1,
      'Administrador',GETDATE(),NULL,NULL,0,1,NULL
      from SEG.Modulo
      where NombreModulo = 'Contratos'    
      
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
