USE SIA
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Creacion de data TipoUsuario
-- =============================================

if not exists(select 1 from SEG.TipoUsuario)
BEGIN 
INSERT INTO [SEG].[TipoUsuario]
           ([CodigoTipoUsuario]
           ,[NombreTipoUsuario]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES ('01','Sede Nacional','A','Cargue.Inicial',getdate(),NULL,NULL)

INSERT INTO [SEG].[TipoUsuario]
           ([CodigoTipoUsuario]
           ,[NombreTipoUsuario]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES ('02','Regional','A','Cargue.Inicial',getdate(),NULL,NULL)

INSERT INTO [SEG].[TipoUsuario]
           ([CodigoTipoUsuario]
           ,[NombreTipoUsuario]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES ('03','Entidad Contratista','A','Cargue.Inicial',getdate(),NULL,NULL)
end

GO


