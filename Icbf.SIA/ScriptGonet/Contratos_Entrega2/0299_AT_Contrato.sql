use sia

-- =============================================
-- Author:		Gonet
-- Create date: 2014-07-09
-- Description:	Alterar tabla CONTRATO.Contrato
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].DF__Contrato__Suscri__60C822F7'))
BEGIN	
	alter table CONTRATO.Contrato DROP constraint DF__Contrato__Suscri__60C822F7

	alter table CONTRATO.Contrato add constraint DF_Contrato_Suscrito DEFAULT (0) FOR Suscrito

END


