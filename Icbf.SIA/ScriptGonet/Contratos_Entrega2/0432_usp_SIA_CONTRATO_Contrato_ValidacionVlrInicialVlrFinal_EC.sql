USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 09/07/2014 11:06:58 a.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 09/07/2014 11:06:58 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 02/06/2014
-- Description:	
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal] 
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
	@IdContratoConvMarco INT,
	@ValorInicialContConv DECIMAL, 
	@IdContrato INT

AS
BEGIN
	
	DECLARE @ValorFinalContratoConvMarco DECIMAL, @SumValorFinalContratos DECIMAL
	SELECT @ValorFinalContratoConvMarco=ValorFinalContrato 
	FROM CONTRATO.Contrato WHERE IdContrato = @IdContratoConvMarco


	SELECT @SumValorFinalContratos=ISNULL(SUM(ValorFinalContrato), 0) 
	FROM CONTRATO.Contrato WHERE FK_IdContrato = @IdContratoConvMarco AND IdContrato <> @IdContrato


	IF (@ValorFinalContratoConvMarco >= (@ValorInicialContConv + @SumValorFinalContratos))
	BEGIN

		SELECT @IdContratoConvMarco AS IdContrato
		RETURN 

	END


END


GO


