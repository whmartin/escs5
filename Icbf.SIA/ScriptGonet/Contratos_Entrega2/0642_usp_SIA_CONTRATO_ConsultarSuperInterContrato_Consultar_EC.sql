USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContrato_Consultar]    Script Date: 09/22/2014 18:11:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContrato_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContrato_Consultar]    Script Date: 09/22/2014 18:11:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  6/6/2014 4:39:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) ConsultarSuperInterContrato
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContrato_Consultar] @IDSupervisorIntervContrato=2224
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsultarSuperInterContrato_Consultar]
	@IDSupervisorIntervContrato INT
AS
BEGIN
 

	 IF (EXISTS(SELECT TSI.codigo FROM [CONTRATO].[SupervisorInterContrato] SI
     INNER JOIN [CONTRATO].[TipoSupervisorInterventor] TSI ON SI.IDTipoSuperInter = TSI.IDTipoSupervisorInterventor 
	 WHERE IDSupervisorIntervContrato = @IDSupervisorIntervContrato AND TSI.codigo = '02'))--Externo (Interventor)
	 BEGIN
		 PRINT 'INTERVENTOR'
		SELECT DISTINCT DT.IdEntidad, DT.IdTercero, DT.NombreTipoPersona
		, DT.CodDocumento, DT.NumeroIdentificacion
		, DT.NombreRazonsocial , DT.IdTipoPersona, DT.IDTIPODOCIDENTIFICA
		, DT.CORREOELECTRONICO, DT.IDSupervisorIntervContrato, DT.DireccionComercial
		, DT.SupervisorInterventor, DT.NumeroContrato, DT.TipoContrato, DT.RegionalContrato, DT.ObjetoDelContrato
	  ,DT.FechaSuscripcionContrato, DT.FechaFinalTerminacionContrato, DT.FechaInicio, DT.FechaFinalizacionSuperInterv, DT.FechaModifSuperInter
	  ,DT.NumeroContratoInterventoria, DT.CodigoSuperInter, DT.IdEstadoContrato, DT.NombreEstado AS EstadoContrato, Inactivo AS InactivoSuperInter
	  , DT.UsuarioCrea, DT.FechaCrea, DT.UsuarioModifica, DT.FechaModifica,DT.IDDirectorInterventoria,
	  DT.NumeroIdentificacionDirector , DT.Telefono, DT.Celular, DT.NombreDirInterventoria,DT.CodigoSuperInter
	  
	  FROM (
	  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, 
	  ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as NombreRazonsocial,
			E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor
		
		, T.CORREOELECTRONICO, SI.IDSupervisorIntervContrato, IAE.DireccionComercial
		, TSI.codigo AS CodigoSuperInter, CASE WHEN TSI.codigo = '01' THEN 'Supervisor' WHEN TSI.codigo = '02' THEN 'Interventor' ELSE '' END AS SupervisorInterventor 
		, C.NumeroContrato, C.IdEstadoContrato, C.ObjetoDelContrato, TC.NombreTipoContrato AS TipoContrato, Reg.NombreRegional AS RegionalContrato
		, C.FechaSuscripcionContrato, C.FechaFinalTerminacionContrato
		, SI.FechaInicio, NULL AS FechaFinalizacionSuperInterv, SI.FechaModifica AS FechaModifSuperInter
		, CI.NumeroContrato AS NumeroContratoInterventoria
		, SI.UsuarioCrea, SI.FechaCrea, SI.UsuarioModifica, SI.FechaModifica, SI.Inactivo, estcon.Descripcion as NombreEstado
		, SI.IDDirectorInterventoria, dirinter.NumeroIdentificacion AS NumeroIdentificacionDirector, dirinter.Telefono, dirinter.Celular
		,( dirinter.PrimerNombre +' '+ISNULL(dirinter.SegundoNombre,'') +' ' + dirinter.PrimerApellido +' '+ ISNULL(dirinter.SegundoApellido,'') ) as NombreDirInterventoria

	  FROM [CONTRATO].[SupervisorInterContrato] SI
		 INNER JOIN [CONTRATO].[TipoSuperInter] TSI ON SI.IDTipoSuperInter = TSI.IDTipoSuperInter
		 LEFT JOIN [CONTRATO].[Contrato] C ON SI.IdContrato = C.IdContrato
		 LEFT JOIN [CONTRATO].[TipoContrato] TC ON C.IdTipoContrato = TC.IdTipoContrato
		 LEFT JOIN [DIV].[REgional] Reg ON C.IdRegionalContrato = Reg.IdRegional
		 LEFT JOIN [CONTRATO].[Contrato] CI ON SI.IdNumeroContratoInterventoria = CI.IdContrato
		 INNER JOIN	[Proveedor].[EntidadProvOferente] EP ON SI.IDProveedoresInterventor = EP.IdEntidad
		 INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
		 INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
		 INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
		 INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		 INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
		 INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
		 INNER JOIN CONTRATO.EstadoContrato estcon ON C.IdEstadoContrato = estcon.IDEstadoContrato
		 LEFT JOIN CONTRATO.DirectorInterventoria dirinter ON dirinter.IDDirectorInterventoria = SI.IDDirectorInterventoria
		 WHERE T.IdTipoPersona=1
	UNION 
	 SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, 
	  /*( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') )*/ T.RAZONSOCIAL as NombreRazonsocial,
			E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor
		
		, T.CORREOELECTRONICO, SI.IDSupervisorIntervContrato, IAE.DireccionComercial
		, TSI.codigo AS CodigoSuperInter, CASE WHEN TSI.codigo = '01' THEN 'Supervisor' WHEN TSI.codigo = '02' THEN 'Interventor' ELSE '' END AS SupervisorInterventor 
		, C.NumeroContrato, C.IdEstadoContrato, C.ObjetoDelContrato, TC.NombreTipoContrato AS TipoContrato, Reg.NombreRegional AS RegionalContrato
		, C.FechaSuscripcionContrato, C.FechaFinalTerminacionContrato
		, SI.FechaInicio, NULL AS FechaFinalizacionSuperInterv, SI.FechaModifica AS FechaModifSuperInter
		, CI.NumeroContrato AS NumeroContratoInterventoria
		, SI.UsuarioCrea, SI.FechaCrea, SI.UsuarioModifica, SI.FechaModifica, SI.Inactivo, estcon.Descripcion as NombreEstado
		, SI.IDDirectorInterventoria, dirinter.NumeroIdentificacion AS NumeroIdentificacionDirector, dirinter.Telefono, dirinter.Celular
		,( dirinter.PrimerNombre +' '+ISNULL(dirinter.SegundoNombre,'') +' ' + dirinter.PrimerApellido +' '+ ISNULL(dirinter.SegundoApellido,'') ) as NombreDirInterventoria
	  FROM [CONTRATO].[SupervisorInterContrato] SI
		INNER JOIN [CONTRATO].[TipoSuperInter] TSI ON SI.IDTipoSuperInter = TSI.IDTipoSuperInter
		LEFT JOIN [CONTRATO].[Contrato] C ON SI.IdContrato = C.IdContrato
		 LEFT JOIN [CONTRATO].[TipoContrato] TC ON C.IdTipoContrato = TC.IdTipoContrato
		 LEFT JOIN [DIV].[REgional] Reg ON C.IdRegionalContrato = Reg.IdRegional
		 LEFT JOIN [CONTRATO].[Contrato] CI ON SI.IdNumeroContratoInterventoria = CI.IdContrato
		INNER JOIN [Proveedor].[EntidadProvOferente] EP ON SI.IDProveedoresInterventor = EP.IdEntidad
		INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
		LEFT JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
		INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona
		INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
		INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
		 INNER JOIN CONTRATO.EstadoContrato estcon ON C.IdEstadoContrato = estcon.IDEstadoContrato
		 LEFT JOIN CONTRATO.DirectorInterventoria dirinter ON dirinter.IDDirectorInterventoria = SI.IDDirectorInterventoria
		WHERE T.IdTipoPersona=2
		) DT
			WHERE DT.IDSupervisorIntervContrato = @IDSupervisorIntervContrato
	
	END
	ELSE IF (EXISTS(SELECT TSI.codigo FROM [CONTRATO].[SupervisorInterContrato] SI
     INNER JOIN [CONTRATO].[TipoSupervisorInterventor] TSI ON SI.IDTipoSuperInter = TSI.IDTipoSupervisorInterventor 
	 WHERE IDSupervisorIntervContrato = @IDSupervisorIntervContrato AND TSI.codigo = '01'))--Interno (Supervisor)
		BEGIN
			PRINT 'SUPERVISOR'
			SELECT  *

	FROM
	(select DISTINCT
	-- 0 AS IdEntidad, 0 AS IdTercero, 
	'Natural' AS NombreTipoPersona, Em.desc_ide AS CodDocumento, Em.num_iden AS NumeroIdentificacion
	, (Em.nom_emp1 + ' ' + Em.nom_emp2 + ' ' + Em.ape_emp1 + ' ' + Em.ape_emp2) AS NombreRazonsocial 
	,1 AS IdTipoPersona, 1 AS IDTIPODOCIDENTIFICA
	,  ISNULL(Em.eee_mail,'') AS CORREOELECTRONICO, SI.IDSupervisorIntervContrato AS IDSupervisorIntervContrato
	, Em.telefono AS NumeroTelefono, Em.direccion AS DireccionComercial
	,'Supervisor' AS SupervisorInterventor, C.NumeroContrato AS NumeroContrato, TC.NombreTipoContrato AS TipoContrato
	, Reg.NombreRegional AS RegionalContrato, C.ObjetoDelContrato,SI.Inactivo AS InactivoSuperInter
	, C.FechaSuscripcionContrato AS FechaSuscripcionContrato, C.FechaFinalTerminacionContrato AS FechaFinalTerminacionContrato, SI.FechaInicio AS FechaInicio, NULL AS FechaFinalizacionSuperInterv, NULL AS FechaModifSuperInter
	, CI.NumeroContrato AS NumeroContratoInterventoria, '01' AS CodigoSuperInter
	, 'Administrador' AS UsuarioCrea, GETDATE() AS FechaCrea, NULL AS UsuarioModifica, NULL AS FechaModifica
	,Em.nom_emp1 AS PRIMERNOMBRE, Em.nom_emp2 AS SEGUNDONOMBRE, Em.ape_emp1 AS PRIMERAPELLIDO, Em.ape_emp2 AS SEGUNDOAPELLIDO, '' AS RAZONSOCIAL, estcon.Descripcion as EstadoContrato
	--select
	-- Em.ID_Ident, desc_ide AS TipoIdentificacion, Em.num_iden AS NumeroIdentificacion, Em.desc_vin AS TipoVinculacion
	--,(Em.nom_emp1 + ' ' + Em.nom_emp2 + ' ' + Em.ape_emp1 + ' ' + Em.ape_emp2) AS NombreCompleto
	--,Em.nom_emp1 AS PrimerNombre, Em.nom_emp2 AS SegundoNombre, Em.ape_emp1 AS PrimerApellido, Em.ape_emp2 AS SegundoApellido
	--,Em.ID_regio, Em.regional, Em.nom_depe AS Dependencia, Em.direccion, telefono, ISNULL(Em.eee_mail,'') AS CorreoE
	--,Em.Desc_car AS Cargo, Em.num_iden AS IdEmpleado 
	 from [KACTUS].[KPRODII].[dbo].[Da_Emple] Em 
	 INNER JOIN [CONTRATO].[SupervisorInterContrato] SI ON SI.IDEmpleadosSupervisor = Em.num_iden
	 LEFT JOIN [CONTRATO].[Contrato] C ON SI.IdContrato = C.IdContrato
		 LEFT JOIN [CONTRATO].[TipoContrato] TC ON C.IdTipoContrato = TC.IdTipoContrato
		 LEFT JOIN [DIV].[REgional] Reg ON C.IdRegionalContrato = Reg.IdRegional
		 LEFT JOIN [CONTRATO].[Contrato] CI ON SI.IdNumeroContratoInterventoria = CI.IdContrato
		 INNER JOIN CONTRATO.EstadoContrato estcon ON C.IdEstadoContrato = estcon.IDEstadoContrato
		 ) DT
					 WHERE DT.IDSupervisorIntervContrato = @IDSupervisorIntervContrato
			END	
		END


GO


