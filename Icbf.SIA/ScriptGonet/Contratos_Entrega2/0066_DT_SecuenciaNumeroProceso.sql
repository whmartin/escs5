use SIA

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Eliminacion table [CONTRATO].[SecuenciaNumeroProceso]
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[SecuenciaNumeroProceso]') AND type in (N'U'))
DROP TABLE [CONTRATO].[SecuenciaNumeroProceso]
GO
