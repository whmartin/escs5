USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Estructura_Archivo_Insertar]    Script Date: 25/06/2014 08:09:12 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Estructura_Archivo_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Insertar]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date: 25/06/2014
-- Description: Procedimiento almacenado para insertar un resgistro en la tabla Archivos
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Insertar]
		    @IdArchivo NUMERIC(18,0) OUTPUT
		, 	@IdUsuario INT
		,	@IdFormatoArchivo INT
		,	@FechaRegistro DATETIME
		,	@NombreArchivo NVARCHAR(128)
		,	@ServidorFTP NVARCHAR(256)=NULL
		,	@Estado NVARCHAR(1)
		,	@ResumenCarga NVARCHAR(256)
		,   @UsuarioCrea NVARCHAR(250)
		,	@NombreArchivoOri NVARCHAR(256)
AS
BEGIN
	INSERT INTO Estructura.Archivo(IdUsuario
	                             , IdFormatoArchivo
	                             , FechaRegistro
	                             , NombreArchivo
	                             , ServidorFTP
	                             , Estado
	                             , ResumenCarga
	                             , UsuarioCrea
	                             , FechaCrea
	                             , NombreArchivoOri
	                             )
					  VALUES(@IdUsuario
					       , @IdFormatoArchivo
					       , @FechaRegistro
					       , @NombreArchivo
					       , @ServidorFTP
					       , @Estado
					       , @ResumenCarga
					       , @UsuarioCrea
					       , GETDATE()
					       , @NombreArchivoOri
					       )
	SELECT @IdArchivo = @@IDENTITY 		
END









