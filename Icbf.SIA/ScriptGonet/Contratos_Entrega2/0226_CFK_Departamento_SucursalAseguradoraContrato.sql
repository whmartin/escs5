USE [SIA]
GO
-- =============================================
-- Author:		Gonet\Carlos Andr�s C�rdenas P.
-- Create date:  20/06/2014 10:10:00 AM
-- Description:	Creacion de llave for�nea FK_Departamento_SucursalAseguradoraContrato
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Contrato].[FK_Departamento_SucursalAseguradoraContrato]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [Contrato].[SucursalAseguradoraContrato] DROP CONSTRAINT FK_Departamento_SucursalAseguradoraContrato
END

ALTER TABLE [Contrato].[SucursalAseguradoraContrato]
ADD CONSTRAINT FK_Departamento_SucursalAseguradoraContrato FOREIGN KEY(IDDepartamento)REFERENCES  [DIV].[Departamento](IdDepartamento)

