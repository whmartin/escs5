USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]    Script Date: 04/06/2014 12:06:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]
		@IdTipoSupvInterventor INT OUTPUT, 	@Nombre NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	declare @maxid int;

	select @maxid = MAX(IDTipoSuperInter) + 1 from CONTRATO.TipoSuperInter

	INSERT INTO Contrato.TipoSuperInter(Descripcion, Estado, UsuarioCrea, FechaCrea,Codigo)
					  VALUES(@Nombre, @Estado, @UsuarioCrea, GETDATE(),@maxid)
	SELECT @IdTipoSupvInterventor = @@IDENTITY 		
END


