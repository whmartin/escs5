USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]    Script Date: 05/14/2014 16:50:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]    Script Date: 05/14/2014 16:50:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/14/2014 4:20:50 PM
-- Description:	Procedimiento almacenado que guarda un nuevo LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]
		@IdLugarEjecucionContratos INT OUTPUT, 	
		@IdContrato INT,	
		--@IdDepartamento int,	
		@IdsRegionales nvarchar(MAX),	
		@NivelNacional BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN

	if (@NivelNacional IS NULL OR @NivelNacional = 0)
	begin

		IF CHARINDEX(',', @IdsRegionales) > 0
		BEGIN
			insert into CONTRATO.LugarEjecucionContrato(IdContrato, IdDepartamento, IdRegional, NivelNacional, UsuarioCrea, FechaCrea)
			select @IdContrato, IdDepartamento, IdMunicipio, 0, @UsuarioCrea, GETDATE()
			FROM DIV.Municipio where IdMunicipio IN (Select splitdata from fnSplitString(@IdsRegionales, ','))
			
			SELECT @IdLugarEjecucionContratos = 1000 
		END
		ELSE
		BEGIN
			DECLARE @IdDep int 
			select @IdDep = IdDepartamento FROM DIV.Municipio where IdMunicipio = CAST(@IdsRegionales AS INT)
			INSERT INTO CONTRATO.LugarEjecucionContrato(IdContrato, IdDepartamento, IdRegional, NivelNacional, UsuarioCrea, FechaCrea)
					  VALUES(@IdContrato, @IdDep, CAST(@IdsRegionales AS INT), 0, @UsuarioCrea, GETDATE())
			
			SELECT @IdLugarEjecucionContratos = @@IDENTITY 
			
		END
	END
	ELSE
	BEGIN
		INSERT INTO CONTRATO.LugarEjecucionContrato(IdContrato, IdDepartamento, IdRegional, NivelNacional, UsuarioCrea, FechaCrea)
					  VALUES(@IdContrato, NULL, NULL, @NivelNacional, @UsuarioCrea, GETDATE())
		
		SELECT @IdLugarEjecucionContratos = @@IDENTITY 
	END
			
END

GO


