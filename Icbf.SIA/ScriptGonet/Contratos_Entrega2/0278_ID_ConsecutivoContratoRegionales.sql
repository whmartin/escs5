use SIA

-- =============================================
-- Author:		Gonet
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion de vigencias iniciales 
-- =============================================

TRUNCATE table CONTRATO.ConsecutivoContratoRegionales

DECLARE @vigencia2014 int,
		@vigencia2013 int

IF Not Exists(SELECT 1 from global.vigencia where AcnoVigencia = '2015')
BEGIN
	INSERT INTO  global.vigencia VALUES ('2015',1,'Administrador',GETDATE(),NULL,NULL)
END

IF Not Exists(SELECT 1 from global.vigencia where AcnoVigencia = '2014')
BEGIN
	INSERT INTO  global.vigencia VALUES ('2014',1,'Administrador',GETDATE(),NULL,NULL)
END

select  IdVigencia
from  global.vigencia
where AcnoVigencia = '2015'


select @vigencia2013 = IdVigencia
from  global.vigencia
where AcnoVigencia = '2014'

INSERT INTO CONTRATO.ConsecutivoContratoRegionales
select IdRegional,@vigencia2013,1,'Administrador',GETDATE(),null,null
from DIV.Regional

INSERT INTO CONTRATO.ConsecutivoContratoRegionales
select IdRegional,@vigencia2014,1,'Administrador',GETDATE(),null,null
from DIV.Regional

