USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_Contrato_AportesContrato_Obtener')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AportesContrato_Obtener]    Script Date: 30/07/2014 09:50:21 a.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AportesContrato_Obtener]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AportesContrato_Obtener]    Script Date: 30/07/2014 09:50:21 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 29/06/2014
-- Description:	Obtiene la informaci�n de los aportes asociados a un contrato seleccionados a partir de los filtros de entrada
-- (P�gina Registro Contratos)
-- =============================================
-- [dbo].[usp_SIA_Contrato_AportesContrato_Obtener] @AportanteICBF= 0, @IdContrato = 4, @IDEntidadProvOferente = 45951
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AportesContrato_Obtener]
	@AportanteICBF BIT = NULL, 
	@IdContrato INT = NULL,
	@IDEntidadProvOferente INT = NULL,
	@Estado BIT = NULL
AS
BEGIN

	SELECT IdAporteContrato, AportanteICBF, NumeroIdentificacionICBF, ValorAporte, DescripcionAporte, AporteEnDinero, 
	(CASE AporteEnDinero WHEN 0 THEN 'ESPECIE' ELSE 'DINERO' END) AS TipoAporte,
	IdContrato, IDEntidadProvOferente, T.NUMEROIDENTIFICACION AS NumeroIdentificacionContratista,
	(CASE TP.CodigoTipoPersona  
		WHEN '001' THEN (T.PRIMERNOMBRE + (CASE ISNULL(T.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDONOMBRE + ' ' END) + T.PRIMERAPELLIDO + (CASE ISNULL(T.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDOAPELLIDO END))
		ELSE T.RAZONSOCIAL END) AS InformacionAportante, 
	FechaRP, NumeroRP, AP.Estado, AP.UsuarioCrea, AP.FechaCrea, AP.UsuarioModifica, AP.FechaModifica 
	FROM Contrato.AporteContrato AP
	LEFT OUTER JOIN PROVEEDOR.EntidadProvOferente EPO ON EPO.IdEntidad = AP.IDEntidadProvOferente
	LEFT OUTER JOIN Oferente.TERCERO T ON T.IDTERCERO = EPO.IdTercero 
	LEFT OUTER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona = TP.IdTipoPersona  
	WHERE AportanteICBF = ISNULL(@AportanteICBF, AportanteICBF) 
	AND IdContrato = ISNULL(@IdContrato, IdContrato) 
	AND ISNULL(IDEntidadProvOferente, '') = ISNULL(@IDEntidadProvOferente, ISNULL(IDEntidadProvOferente, ''))
	AND AP.Estado = ISNULL(@Estado, AP.Estado)

END


GO


