USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_Contrato_Actualizacion')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion]    Script Date: 12/08/2014 09:12:42 a.m. ******/
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion]    Script Date: 12/08/2014 09:12:42 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 22/06/2014
-- Description:	
-- =============================================
-- [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion] 4
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion] 
	@IdContrato INT = NULL,
	@IdEstadoContrato INT = NULL, 
	@IdTipoContratoAsociado INT = NULL,
	@ConvenioMarco BIT = NULL, 
	@ConvenioAdhesion BIT = NULL,
	@FK_IdContrato INT = NULL,
	@IdCategoriaContrato INT = NULL,
	@IdTipoContrato INT = NULL,
	@IdModalidadAcademica VARCHAR(30) = NULL,
	@IdProfesion VARCHAR(30) = NULL, 
	@IdModalidadSeleccion INT = NULL,
	@IdNumeroProceso INT = NULL,
	@FechaAdjudicacionProceso DATETIME = NULL,
	@ActaDeInicio BIT = NULL, 
	@ManejaAporte BIT = NULL, 
	@ManejaRecurso BIT = NULL,
	@IdRegimenContratacion INT = NULL,
	@IdConsecutivoPlanCompras INT = NULL,
	@ObjetoDelContrato VARCHAR(500) = NULL,
	@AlcanceObjetoDelContrato VARCHAR(4000) = NULL,
	@ValorInicialContrato NUMERIC(30,2) = NULL,
	@FechaInicioEjecucion DATETIME = NULL,
	@FechaFinalizacionIniciaContrato DATETIME = NULL,
	@FechaFinalTerminacionContrato DATETIME = NULL,
	@ManejaVigenciaFuturas BIT = NULL,
	@IdVigenciaInicial INT = NULL,
	@IdVigenciaFinal INT = NULL,
	@IdFormaPago INT = NULL,
	@DatosAdicionaleslugarEjecucion VARCHAR(800) = NULL, 
	@UsuarioModifica NVARCHAR(250) = NULL
AS
BEGIN
	DECLARE @ConvenioMarcoOLD BIT, @FK_IdContratoOLD INT, @SumValorFinalContratos DECIMAL(30, 2)
	SELECT @ConvenioMarcoOLD=ConvenioMarco, @FK_IdContratoOLD=FK_IdContrato 
	FROM Contrato.Contrato WHERE IdContrato = @IdContrato

	-- Actualizacion del valor final del contrato marco desde el hijo
	IF @ConvenioMarco = 1
	BEGIN
		
		IF @FK_IdContrato <> @FK_IdContratoOLD AND @FK_IdContratoOLD IS NOT NULL
		BEGIN
			-- Si se des-asocia de un contrato se recalcula el valor de dicho contrato sin el valor final de 
			-- este contrato que se esta actualizando
			SELECT @SumValorFinalContratos=ISNULL(SUM(ValorFinalContrato), 0) 
			FROM CONTRATO.Contrato WHERE FK_IdContrato = @FK_IdContratoOLD AND IdContrato <> @IdContrato

			-- Se actualiza el valor final del contrato des-asociado sumando los valores de los contratos
			-- asociados al valor inicial de dicho contrato
			UPDATE CONTRATO.Contrato
			SET ValorFinalContrato = @SumValorFinalContratos + ValorInicialContrato
			WHERE IdContrato = @FK_IdContratoOLD
		END
		
		IF @FK_IdContrato <> 0 AND @FK_IdContrato IS NOT NULL 
		BEGIN
			-- Se calcula el valor final de contrato con la sumatoria de los valores finales de los contratos
			-- que figuran como asociados diferentes del contrato actual
			SELECT @SumValorFinalContratos=ISNULL(SUM(ValorFinalContrato), 0) 
			FROM CONTRATO.Contrato WHERE FK_IdContrato = @FK_IdContrato AND IdContrato <> @IdContrato

			-- Se actualiza el valor final del contrato asociado con la sumatoria del paso anterior, mas el valor
			-- inicial del contrato que se esta actualizando, mas el valor inicial de contraro de dicho contrato
			UPDATE CONTRATO.Contrato
			SET ValorFinalContrato = @SumValorFinalContratos + ISNULL(@ValorInicialContrato, 0) + ValorInicialContrato
			WHERE IdContrato = @FK_IdContrato
		END

	END
	ELSE
	BEGIN

		IF @FK_IdContratoOLD IS NOT NULL
		BEGIN
			-- Si se des-asocia de un contrato se recalcula el valor de dicho contrato sin el valor final de 
			-- este contrato que se esta actualizando
			SELECT @SumValorFinalContratos=ISNULL(SUM(ValorFinalContrato), 0) 
			FROM CONTRATO.Contrato WHERE FK_IdContrato = @FK_IdContratoOLD AND IdContrato <> @IdContrato

			-- Se actualiza el valor final del contrato des-asociado sumando los valores de los contratos
			-- asociados al valor inicial de dicho contrato
			UPDATE CONTRATO.Contrato
			SET ValorFinalContrato = @SumValorFinalContratos + ValorInicialContrato
			WHERE IdContrato = @FK_IdContratoOLD
		END

	END

	-- Obtiene la sumatoria de los contratos asociados a este contrato -- NO APLICA YA QUE LOS CONTRATOS ASOCIADOS DEBEN ESTAR SUSCRITOS
	--SELECT @SumValorFinalContratos=ISNULL(SUM(ValorFinalContrato), 0) 
	--FROM CONTRATO.Contrato WHERE FK_IdContrato = @IdContrato 


	UPDATE Contrato.Contrato  
	SET
	IdContratoAsociado = @IdTipoContratoAsociado,
	IdEstadoContrato = @IdEstadoContrato,
	ConvenioMarco = @ConvenioMarco, 
	ConvenioAdhesion = @ConvenioAdhesion,
	FK_IdContrato = @FK_IdContrato,
	IdCategoriaContrato = @IdCategoriaContrato,
	IdTipoContrato = @IdTipoContrato,
	IdModalidadAcademica = @IdModalidadAcademica,
	IdProfesion = @IdProfesion, 
	IdModalidadSeleccion = @IdModalidadSeleccion,
	IdNumeroProceso = @IdNumeroProceso,
	FechaAdjudicacionDelProceso = @FechaAdjudicacionProceso,
	ActaDeInicio = @ActaDeInicio, 
	ManejaAporte = @ManejaAporte, 
	ManejaRecurso = @ManejaRecurso,
	ConsecutivoPlanCompasAsociado = @IdConsecutivoPlanCompras,
	IdRegimenContratacion = @IdRegimenContratacion,
	ObjetoDelContrato = @ObjetoDelContrato,
	AlcanceObjetoDelContrato = @AlcanceObjetoDelContrato,
	ValorInicialContrato = @ValorInicialContrato,
	ValorFinalContrato = @ValorInicialContrato, /* + @SumValorFinalContratos*/ -- Sumatoria de los contratos asociados(Si los tiene), mas el valor inicial
	FechaInicioEjecucion = @FechaInicioEjecucion,
	FechaFinalizacionIniciaContrato = @FechaFinalizacionIniciaContrato,
	FechaFinalTerminacionContrato = @FechaFinalTerminacionContrato,
	ManejaVigenciasFuturas = @ManejaVigenciaFuturas,
	IdVigenciaInicial = @IdVigenciaInicial,
	IdVigenciaFinal = @IdVigenciaFinal,
	IdFormaPago = @IdFormaPago,
	DatosAdicionaleslugarEjecucion = @DatosAdicionaleslugarEjecucion
	,UsuarioModifica = @UsuarioModifica
	,FechaModifica = GETDATE()
	WHERE IdContrato = @IdContrato


	IF @ManejaAporte = 0
	BEGIN
		DELETE FROM [CONTRATO].[AporteContrato] WHERE [IdContrato] = @IdContrato
	END

	IF @ManejaRecurso = 0
	BEGIN 
		DELETE FROM [CONTRATO].[ProductoPlanCompraContratos] 
		WHERE [IDPlanDeComprasContratos] IN (SELECT [IDPlanDeComprasContratos] FROM [CONTRATO].[PlanDeComprasContratos] WHERE [IdContrato] = @IdContrato)

		DELETE FROM [CONTRATO].[RubroPlanComprasContrato] 
		WHERE [IDPlanDeComprasContratos] IN (SELECT [IDPlanDeComprasContratos] FROM [CONTRATO].[PlanDeComprasContratos] WHERE [IdContrato] = @IdContrato)

		DELETE FROM [CONTRATO].[SolicitudModPlanCompras]
		WHERE [IdPlanDeCompras] IN (SELECT [IDPlanDeComprasContratos] FROM [CONTRATO].[PlanDeComprasContratos] WHERE [IdContrato] = @IdContrato)

		DELETE FROM [CONTRATO].[PlanDeComprasContratos] WHERE [IdContrato] = @IdContrato

		DELETE FROM [CONTRATO].[ContratosCDP] WHERE [IdContrato] = @IdContrato
	END

	IF @ManejaVigenciaFuturas = 0
	BEGIN
		DELETE FROM [CONTRATO].[VigenciaFuturas] WHERE [IdContrato] = @IdContrato
	END

END





GO


