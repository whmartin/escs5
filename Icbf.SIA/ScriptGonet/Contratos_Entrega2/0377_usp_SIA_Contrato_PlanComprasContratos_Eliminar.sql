USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:27 PM
-- Description:	Procedimiento almacenado que elimina un(a) PlanComprasContratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratos_Eliminar]
	@IDPlanDeComprasContratos INT
AS
BEGIN
	DELETE PPC
	FROM CONTRATO.ProductoPlanCompraContratos PPC
		INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON PPC.IDPlanDeComprasContratos = PCC.IDPlanDeComprasContratos
	WHERE PCC.IDPlanDeComprasContratos = @IDPlanDeComprasContratos

	DELETE RPC
	FROM CONTRATO.RubroPlanComprasContrato RPC
		INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON RPC.IDPlanDeComprasContratos = PCC.IDPlanDeComprasContratos
	WHERE PCC.IDPlanDeComprasContratos = @IDPlanDeComprasContratos

	DELETE Contrato.PlanDeComprasContratos 
	WHERE IDPlanDeComprasContratos = @IDPlanDeComprasContratos
END
