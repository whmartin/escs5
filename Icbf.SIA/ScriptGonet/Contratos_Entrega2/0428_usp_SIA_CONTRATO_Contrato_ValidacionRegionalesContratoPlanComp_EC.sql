USE [SIA]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_Contrato_ValidacionRegionalesContratoPlanComp')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionRegionalesContratoPlanComp]    Script Date: 08/07/2014 07:29:18 p.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionRegionalesContratoPlanComp]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionRegionalesContratoPlanComp]    Script Date: 08/07/2014 07:29:18 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 08/07/2014
-- Description:	Valida si la regional del contrato convenio coincide con la regional de
-- los registros de plan de compras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionRegionalesContratoPlanComp]
	@IdContratoConvMarco INT,
	@CodigoRegionalContConv NVARCHAR(16)
AS
BEGIN
	
	IF NOT EXISTS(SELECT * FROM CONTRATO.PlanDeComprasContratos WHERE IdContrato = @IdContratoConvMarco AND
				   CodigoRegional <> @CodigoRegionalContConv)
	BEGIN
		
		SELECT @IdContratoConvMarco AS IdContrato
		RETURN 

	END

END

GO


