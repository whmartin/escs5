USE [SIA]
GO
-- =============================================
-- Author:		Gonet\Carlos Andr�s C�rdenas P.
-- Create date:  03/06/2014 04:10:00 PM
-- Description:	Creacion de llave for�nea FK_TipoAmparo_AmparosGarantias
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Contrato].[FK_TipoAmparo_AmparosGarantias]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [Contrato].[AmparosGarantias] DROP CONSTRAINT FK_TipoAmparo_AmparosGarantias
END

ALTER TABLE [Contrato].[AmparosGarantias]
ADD CONSTRAINT FK_TipoAmparo_AmparosGarantias FOREIGN KEY(IdTipoAmparo)REFERENCES  [Contrato].[TipoAmparo](IdTipoAmparo)

