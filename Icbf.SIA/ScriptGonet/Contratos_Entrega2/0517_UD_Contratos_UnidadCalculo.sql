USE [SIA]
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  25/07/2014 9:38:34 AM
-- Description:	Actualiza la informacion relacionada con la tabla Unidad Calculo
-- =============================================

IF EXISTS (Select CodUnidadCalculo from CONTRATO.UnidadCalculo where CodUnidadCalculo = 01)
BEGIN
	UPDATE [Contrato].[UnidadCalculo] set Descripcion = 'Salarios M�nimos' where CodUnidadCalculo = 01
END
	

