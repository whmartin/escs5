USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  15/07/2014 10:38 
-- Description:	Insert tabla [CONTRATO].[EstadoContrato].
-- =============================================
IF exists(SELECT [CodEstado] from [CONTRATO].[EstadoContrato] where [Descripcion] = 'Contrato con RP')

Begin

	  declare @IdEstadoContrato int = null;

	  select @IdEstadoContrato = IDEstadoContrato from [CONTRATO].[EstadoContrato] where [Descripcion] = 'Contrato con RP'
	  if @IdEstadoContrato is not null
	  begin 
	  
	  update [SIA].[CONTRATO].[EstadoContrato] 
	  set [Descripcion] =  'Contrato con RP'
	  Where IDEstadoContrato = @IdEstadoContrato

	  end

END