use SIA

-- =============================================
-- Author:		Gonet/Jose de los reyes
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion tipo tabla RubrosPlanCompraContratos
-- =============================================

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND NAME = ('IDDirectorInterventoria'))
BEGIN	
	ALTER table CONTRATO.SupervisorInterContrato add IDDirectorInterventoria int  
END
ELSE
BEGIN
	PRINT 'YA EXISTE EL CAMPO IDDirectorInterventoria'
END


IF not EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'CONTRATO.SupervisorInterContrato') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN	
	ALTER table  CONTRATO.SupervisorInterContrato  add constraint FK_CONTRATO_SupervisorInterContrato_IDDirectorInterventoria
	foreign key (IDDirectorInterventoria)
	references CONTRATO.DirectorInterventoria (IDDirectorInterventoria)
END
ELSE
BEGIN
	Select 'Ya existe la relacion '
END
