USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_Garantia_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Eliminar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/4/2014 9:59:59 AM
-- Description:	Procedimiento almacenado que elimina un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Eliminar]
	@IDGarantia INT
AS
BEGIN
	DECLARE @Estado Varchar
	SELECT @Estado = esgar.DescripcionEstadoGarantia from CONTRATO.Garantia gar
	INNER JOIN CONTRATO.EstadosGarantias esgar ON gar.IDEstadosGarantias = esgar.IDEstadosGarantias
	WHERE IDGarantia = @IDGarantia

	if(@Estado = 'Aprobada')
	BEGIN
		DELETE CONTRATO.AmparosGarantias WHERE IDGarantia = @IDGarantia
		DELETE Contrato.Garantia WHERE IDGarantia = @IDGarantia
	END
END
