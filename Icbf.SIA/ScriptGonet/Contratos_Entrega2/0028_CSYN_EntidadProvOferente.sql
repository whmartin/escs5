USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  2014-07-09
-- Description:	Creacion synonyms [Oferente].[EntidadProvOferente]
-- =============================================

/****** Object:  Synonym [Oferente].[EntidadProvOferente]    Script Date: 16/05/2014 18:08:11 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'EntidadProvOferente' AND schema_id = SCHEMA_ID(N'Oferente'))
DROP SYNONYM [Oferente].[EntidadProvOferente]
GO

/****** Object:  Synonym [Oferente].[EntidadProvOferente]    Script Date: 16/05/2014 18:08:11 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'EntidadProvOferente' AND schema_id = SCHEMA_ID(N'Oferente'))
CREATE SYNONYM [Oferente].[EntidadProvOferente] FOR [Oferentes].[Oferente].[EntidadProvOferente]
GO


