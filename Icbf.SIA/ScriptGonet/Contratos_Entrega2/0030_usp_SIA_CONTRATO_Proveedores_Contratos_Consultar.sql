USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Procedimiento almacenado que consulta un(a) Proveedores_Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Consultar]
	@IdProveedoresContratos INT
AS
BEGIN
	SELECT
		IdProveedoresContratos,
		IdProveedores,
		IdContrato,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].[ProveedoresContratos]
	WHERE IdProveedoresContratos = @IdProveedoresContratos
END
