USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_TiposVinculacionContractual_Consultar]    Script Date: 28/05/2014 04:50:05 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_TiposVinculacionContractual_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_TiposVinculacionContractual_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_TiposVinculacionContractual_Consultar]    Script Date: 28/05/2014 04:50:05 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jorge Emilio Calapina
-- Create date: 28/05/2014
-- Description:	Consulta los tipos de vinculación contractual que coincidan con
-- los filtros dados
-- =============================================
-- usp_SIA_CONTRATO_TiposVinculacionContractual_Consultar
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_TiposVinculacionContractual_Consultar] 
	@IdTipoVinCont VARCHAR(15) = NULL,
	@TipoVincCont VARCHAR(50) = NULL
AS
BEGIN
	
	SELECT cod_tnom AS IdTipoVinculacionContractual, nom_tnom AS TipoVinculacionContractual
	FROM [KACTUS].[KPRODII].[dbo].[nm_tnomi]
	WHERE cast(cod_tnom as varchar(15)) = ISNULL(@IdTipoVinCont, cast(cod_tnom as varchar(15))) 
	AND cast(nom_tnom as varchar(50)) = ISNULL(@TipoVincCont, cast(nom_tnom as varchar(50)))
	ORDER BY nom_tnom ASC

END

GO


