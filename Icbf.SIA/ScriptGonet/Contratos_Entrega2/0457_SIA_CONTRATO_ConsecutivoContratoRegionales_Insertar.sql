USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar]    Script Date: 14/07/2014 02:42:53 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar]    Script Date: 14/07/2014 02:42:53 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/27/2014 6:43:39 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ConsecutivoContratoRegionales

-- Actualizaci�n: se valida que el consecutivo a ingresar no sea menor al ya establecido a un contrato suscrito
-- Autor: Carlos Andr�s C�rdenas
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar]
		@IDConsecutivoContratoRegional INT OUTPUT, 	@IdRegional INT,	@IdVigencia INT,	@Consecutivo NUMERIC(30), @UsuarioCrea NVARCHAR(250)



AS
BEGIN
	DECLARE @ValMaxContrato NUMERIC(30)

	SELECT @ValMaxContrato = MAX(ConsecutivoSuscrito)  from CONTRATO.Contrato 
	WHERE IdRegionalContrato = @IdRegional
	AND IdVigenciaInicial = @IdVigencia
	group by IdVigenciaInicial,IdRegionalContrato

	IF(@Consecutivo > @ValMaxContrato)
	BEGIN
		INSERT INTO CONTRATO.ConsecutivoContratoRegionales(IdRegional, IdVigencia, Consecutivo, UsuarioCrea, FechaCrea)
						  VALUES(@IdRegional, @IdVigencia, @Consecutivo, @UsuarioCrea, GETDATE())
		SELECT @IDConsecutivoContratoRegional = @@IDENTITY 
	END
	ELSE
	BEGIN
		RAISERROR('El n�mero digitado no puede ser menor al de un contrato suscrito',16,1)
	    RETURN
	END

			
END



