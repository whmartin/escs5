USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_TipoVinculacions_Consultar]    Script Date: 02/06/2014 15:12:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_KACTUS_KPRODII_TipoVinculacions_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_KACTUS_KPRODII_TipoVinculacions_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_TipoVinculacions_Consultar]    Script Date: 02/06/2014 15:12:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_KACTUS_KPRODII_TipoVinculacions_Consultar] 
	
AS
BEGIN
	select cod_tnom, Nom_tnom FROM [KACTUS].[KPRODII].[dbo].[nm_tnomi]
END

GO


