USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AmparosGarantias_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantias_Modificar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  5/30/2014 2:33:59 PM
-- Description:	Procedimiento almacenado que actualiza un(a) AmparosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantias_Modificar]
		@IDAmparosGarantias INT,	@IDGarantia INT,	@IdTipoAmparo INT,	@FechaVigenciaDesde DATETIME,	@FechaVigenciaHasta DATETIME,	@IDUnidadCalculo INT,	@ValorCalculoAsegurado NUMERIC(8,2),	@ValorAsegurado NUMERIC(32,8), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.AmparosGarantias SET IDGarantia = @IDGarantia, IdTipoAmparo = @IdTipoAmparo, FechaVigenciaDesde = @FechaVigenciaDesde, FechaVigenciaHasta = @FechaVigenciaHasta, IDUnidadCalculo = @IDUnidadCalculo, ValorCalculoAsegurado = @ValorCalculoAsegurado, ValorAsegurado = @ValorAsegurado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IDAmparosGarantias = @IDAmparosGarantias
END
