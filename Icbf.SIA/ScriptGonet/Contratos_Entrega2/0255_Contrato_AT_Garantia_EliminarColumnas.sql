USE SIA
GO

--Autor: Carlos Andres Cardenas
--Fecha: 25/06/2014 16:32 
-- Descripcion: se borra los campos aprobada, devuelta, 
--				se cambia por una tabla estados


IF EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Garantia') AND NAME = ('Aprobada'))
BEGIN
ALTER TABLE [CONTRATO].[Garantia] DROP COLUMN [Aprobada];
END
GO

IF EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Garantia') AND NAME = ('Devuelta'))
BEGIN
ALTER TABLE [CONTRATO].[Garantia] DROP COLUMN [Devuelta];
END
GO

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Garantia') AND NAME = ('IDEstadosGarantias'))
BEGIN
ALTER TABLE CONTRATO.Garantia
ADD IDEstadosGarantias INT 
END
GO