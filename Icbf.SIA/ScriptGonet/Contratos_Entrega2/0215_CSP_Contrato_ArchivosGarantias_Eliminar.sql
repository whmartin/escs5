USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ArchivosGarantias_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantias_Eliminar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 10:24:57 AM
-- Description:	Procedimiento almacenado que elimina un(a) ArchivosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantias_Eliminar]
	@IDArchivosGarantias INT
AS
BEGIN
	DELETE Contrato.ArchivosGarantias WHERE IDArchivosGarantias = @IDArchivosGarantias
END
