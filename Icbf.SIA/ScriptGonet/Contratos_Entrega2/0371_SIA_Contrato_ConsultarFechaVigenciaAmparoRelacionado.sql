USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]    Script Date: 08/07/2014 02:22:32 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  5/29/2014 2:03:01 PM
-- Description:	Procedimiento almacenado que consulta un(a) AporteContrato
-- =============================================
ALTER PROCEDURE [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]
AS
BEGIN
 SELECT min(FechaVigenciaDesde) as FechaVigenciaDesde, max(FechaVigenciaHasta) as FechaVigenciaHasta from CONTRATO.AmparosGarantias  
END
