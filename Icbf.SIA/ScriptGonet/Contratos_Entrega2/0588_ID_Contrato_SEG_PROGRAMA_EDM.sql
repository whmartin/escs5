USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  21/08/2014 
-- Description:	Insertar Tabla programa para visualizar en el men� de tablas param�tricas la opci�n de Clausula Contrato
-- =============================================
IF not exists(SELECT * from [SEG].[Programa] where [NombrePrograma] = 'Clausula Contrato')
Begin
INSERT INTO [SEG].[Programa]
           ([IdModulo]
           ,[NombrePrograma]
           ,[CodigoPrograma]
           ,[Posicion]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           ,[VisibleMenu]
           ,[generaLog])
     VALUES
           (3
           ,'Clausula Contrato'
           ,'Contratos/ClausulaContrato'
           ,1
           ,1
           ,'Administrador'
           ,GETDATE()
           ,0
           ,1)
End


