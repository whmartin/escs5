USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarRolesNombre]    Script Date: 09/06/2014 08:37:52 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_ConsultarRolesNombre]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarRolesNombre]
GO


/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarRoles]    Script Date: 06/06/2014 12:10:27 p.m. ******/
-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado para la consulta de roles
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarRolesNombre]
	@Nombre NVARCHAR(20)
	, @Estado BIT
AS
BEGIN
	IF @Estado IS NULL
	BEGIN
		SELECT [IdRol]
			,[providerKey]
			,[Nombre]
			,[Descripcion]
			,[Estado]
		FROM SEG.Rol
		WHERE [Nombre] = @Nombre
		AND [Estado] = @Estado
		AND EsAdministrador = 1
	END
	ELSE
	BEGIN
		SELECT 
			[IdRol]
			,[providerKey]
			,[Nombre]
			,[Descripcion]
			,[Estado]
		FROM SEG.Rol
		WHERE [Nombre] = @Nombre
			AND [Estado] = @Estado
			AND EsAdministrador = 1
	END
END




