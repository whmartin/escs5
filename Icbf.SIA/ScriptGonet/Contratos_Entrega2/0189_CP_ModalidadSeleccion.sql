USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:12
   -- Description:         Inserta la tabla para ser visualizada en la aplicaci�n
   -- =============================================
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Contratos/ModalidadSeleccion' and NombrePrograma = 'Modalidad Selecci�n' )
begin
      declare @idpadre int
      select @idpadre = IdPrograma from SEG.Programa where NombrePrograma = 'Contrato'
      insert INTO SEG.Programa  (IdModulo,
								NombrePrograma,
								CodigoPrograma,
								Posicion,
								Estado,
								UsuarioCreacion,
								FechaCreacion,
								UsuarioModificacion,
								FechaModificacion,
								VisibleMenu,
								generaLog,
								Padre)
      SELECT IdModulo,'Modalidad Selecci�n','Contratos/ModalidadSeleccion',1,1,'Administrador',GETDATE(),'','',0,1,NULL
      from SEG.Modulo
      where NombreModulo = 'CONTRATOS'    

      INSERT INTO SEG.Permiso 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      from SEG.Programa
      where CodigoPrograma = 'Contratos/ModalidadSeleccion'
      and NombrePrograma = 'Modalidad Selecci�n' 

	    	INSERT INTO CONTRATO.TablaParametrica ( [CodigoTablaParametrica], [NombreTablaParametrica], 
		[Url], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
		VALUES ( N'07', N'Modalidad Selecci�n', N'Contratos/ModalidadSeleccion', 1, N'administrador', GETDATE(), NULL, NULL)
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
