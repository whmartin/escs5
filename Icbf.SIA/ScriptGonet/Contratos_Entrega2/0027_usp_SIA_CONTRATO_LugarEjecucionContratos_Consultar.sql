USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_LugarEjecucionContratos_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContratos_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/14/2014 4:20:50 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContratos_Consultar]
	@IdContrato INT = NULL,@IdDepartamento INT = NULL,@IdRegional INT = NULL,@NivelNacional BIT = NULL
AS
BEGIN
	SELECT
		IdLugarEjecucionContratos,
		IdContrato,
		IdDepartamento,
		IdRegional,
		NivelNacional,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].[LugarEjecucionContrato]
	WHERE IdContrato =
		CASE
			WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato
		END AND IdDepartamento =
		CASE
			WHEN @IdDepartamento IS NULL THEN IdDepartamento ELSE @IdDepartamento
		END AND IdRegional =
		CASE
			WHEN @IdRegional IS NULL THEN IdRegional ELSE @IdRegional
		END AND NivelNacional =
		CASE
			WHEN @NivelNacional IS NULL THEN NivelNacional ELSE @NivelNacional
		END
END
