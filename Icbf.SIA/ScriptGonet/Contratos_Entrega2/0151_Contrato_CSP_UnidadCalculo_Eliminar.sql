USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_UnidadCalculo_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculo_Eliminar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/3/2014 3:19:34 PM
-- Description:	Procedimiento almacenado que elimina un(a) UnidadCalculo
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculo_Eliminar]
	@IDUnidadCalculo INT
AS
BEGIN
	DELETE Contrato.UnidadCalculo WHERE IDUnidadCalculo = @IDUnidadCalculo
END
