use SIA 

-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  09/06/2014 11:16
-- Description:	Insertar constraint foreign key en la tabla [Contrato].[Contrato] haciendo referencia a [Contrato].[FormaPago]
-- =============================================

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_FormaPago]') 
AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_FormaPago] FOREIGN KEY([IdFormaPago])
REFERENCES [CONTRATO].[FormaPago] ([IdFormaPago])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_FormaPago]') 
AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_FormaPago]
GO