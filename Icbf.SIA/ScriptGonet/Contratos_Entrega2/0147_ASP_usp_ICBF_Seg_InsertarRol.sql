USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_InsertarRol]    Script Date: 03/06/2014 12:03:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Seg_InsertarRol]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ICBF_Seg_InsertarRol]
GO

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que guarda información de un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarRol]
		  @IdRol INT OUTPUT
		, @ProviderKey VARCHAR(125)
		, @Nombre NVARCHAR(20)
		, @Descripcion NVARCHAR(200)
		, @Estado BIT
		, @UsuarioCreacion NVARCHAR(250)
		, @EsAdministrador BIT = 0
AS
BEGIN
	INSERT INTO SEG.Rol([providerKey], [Nombre], [Descripcion], [Estado], [UsuarioCreacion], FechaCreacion,EsAdministrador)
					  VALUES(@ProviderKey, @Nombre, @Descripcion, @Estado, @UsuarioCreacion, GETDATE(),@EsAdministrador)
	SELECT @IdRol = @@IDENTITY 					  
END
