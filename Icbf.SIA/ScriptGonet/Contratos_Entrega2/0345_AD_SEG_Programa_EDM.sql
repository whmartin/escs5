USE [SIA]
GO
-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  26/06/2014 17:06 PM
-- Description:	Actualiza tabla [SEG].[Programa] para mostrar opci�n men�.
-- =============================================

IF exists(SELECT [IdModulo] from [SEG].[Programa] where [CodigoPrograma] = 'Contratos/NumeroProcesos')

Begin
	declare @IdPrograma int = null

	select @IdPrograma = IdPrograma from [SEG].[Programa] where [CodigoPrograma] = 'Contratos/NumeroProcesos'
	
	If @IdPrograma is not null
	begin
	     UPDATE [SEG].[Programa]
		 SET [NombrePrograma] = N'Administrar N�mero de Proceso'
         WHERE  [IdPrograma] = @IdPrograma
	END
END