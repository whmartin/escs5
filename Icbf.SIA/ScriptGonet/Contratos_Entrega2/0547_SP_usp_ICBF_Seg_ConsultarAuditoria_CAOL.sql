USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ConsultarAuditoria]    Script Date: 31/07/2014 09:38:44 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  la auditoria
-- =============================================
ALTER PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarAuditoria]
	@pNombrePrograma NVARCHAR(200),
	@pIdRegistro NUMERIC(18,0)
AS
BEGIN

SELECT
	A.fecha Fecha,
	U.PrimerNombre + ' ' + U.PrimerApellido 'NombreUsuario',
	A.operacion Operacion,
	A.parametrosOperacion ParametrosOperacion,
	A.tabla Tabla,
	A.direccionIp DireccionIp,
	A.navegador Navegador
FROM
	AUDITA.LogSIA A
LEFT OUTER JOIN
	SEG.Usuario U ON A.usuario = U.IdUsuario
WHERE
	A.programa = @pNombrePrograma
AND
	A.idRegistro = 	@pIdRegistro


END
