USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].usp_SIA_CONTRATO_ContratosCDP_Insertar') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].usp_SIA_CONTRATO_ContratosCDP_Insertar
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  26/05/2014 15:55:13 
-- Description:	Procedimiento almacenado que guarda un nuevo NumeroProcesos
-- =============================================
CREATE PROCEDURE [dbo].usp_SIA_CONTRATO_ContratosCDP_Insertar
		@IdContratosCDP INT OUTPUT, 	
		@IdCDP INT,	
		@IdContrato INT,
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
INSERT INTO [CONTRATO].[ContratosCDP]
           ([IdCDP]
           ,[IdContrato]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           (@IdCDP
           ,@IdContrato
           ,@UsuarioCrea
           ,getdate()
           ,null
           ,null)
		   SELECT @IdContratosCDP = @@IDENTITY

END
