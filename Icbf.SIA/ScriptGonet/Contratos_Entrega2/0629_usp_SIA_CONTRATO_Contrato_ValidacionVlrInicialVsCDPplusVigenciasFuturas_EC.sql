USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVsCDPplusVigenciasFuturas]    Script Date: 09/10/2014 09:45:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVsCDPplusVigenciasFuturas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVsCDPplusVigenciasFuturas]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVsCDPplusVigenciasFuturas]    Script Date: 09/10/2014 09:45:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================
-- Author:      Gonet/Emilio Calapi�a
-- Create date: 02/06/2014
-- Description: Validacion de valor inicial contra la sumatoria de cdp con vigencia futura a registrar
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVsCDPplusVigenciasFuturas] 3419, 55555555555555555555
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVsCDPplusVigenciasFuturas] 
    @IdContrato INT,
    @ValorVigenciaFutura NUMERIC(30, 2) --DECIMAL
AS
BEGIN
    DECLARE @ValorInicialContrato DECIMAL 
    SELECT @ValorInicialContrato=ISNULL(ValorInicialContrato, 0) FROM CONTRATO.Contrato WHERE IdContrato = @IdContrato
     print 1
    DECLARE @ValorTotalVigenciasFuturas NUMERIC(30, 2)
    SET @ValorTotalVigenciasFuturas = 0
    IF EXISTS(SELECT * FROM CONTRATO.VigenciaFuturas WHERE IdContrato = @IdContrato)
    BEGIN
		SELECT @ValorTotalVigenciasFuturas=SUM(ValorVigenciaFutura) FROM CONTRATO.VigenciaFuturas WHERE IdContrato = @IdContrato
	END
     print 2
    DECLARE @ValorTotalCDPs DECIMAL
    SET @ValorTotalCDPs = 0
    IF EXISTS(SELECT *
		FROM [CONTRATO].[ContratosCDP] CCDP
		INNER JOIN Ppto.InfoETLCDP IECDP ON IECDP.[IdEtlCDP] = CCDP.[IdCDP] --Error en el sinonimo
		INNER JOIN [Ppto].[RegionalesPCI] RPCI on RPCI.IdRegionalPCI = IECDP.IdRegionalPCI
		INNER JOIN Ppto.CatGralRubrosPptalGasto CGRPG on CGRPG.IdCatalogo = IECDP.IdCatRubro
		INNER JOIN BaseSIF.TipoFuenteFinanciamento TFF on TFF.IdTipoFte = IECDP.IdTipoFte
		INNER JOIN BaseSIF.TipoRecursoFinPptal TRFP on TRFP.IdTipoRecursoFinPptal = IECDP.IdTipoRecursoFinPptal
		INNER JOIN Ppto.DependenciasAfectacionSIIF DASIIF on DASIIF.IdDepAfecSIIF = IECDP.IdDepAfecSIIF
		INNER JOIN BaseSIF.TipoSitFondos TSF On TSF.IdTipoSitFondos = IECDP.IdTipoSitFondos
		WHERE CCDP.[IdContrato] = @IdContrato)
	BEGIN
		SELECT @ValorTotalCDPs=SUM(IECDP.ValorActualCDP)
		FROM [CONTRATO].[ContratosCDP] CCDP
		INNER JOIN Ppto.InfoETLCDP IECDP ON IECDP.[IdEtlCDP] = CCDP.[IdCDP] --Error en el sinonimo
		INNER JOIN [Ppto].[RegionalesPCI] RPCI on RPCI.IdRegionalPCI = IECDP.IdRegionalPCI
		INNER JOIN Ppto.CatGralRubrosPptalGasto CGRPG on CGRPG.IdCatalogo = IECDP.IdCatRubro
		INNER JOIN BaseSIF.TipoFuenteFinanciamento TFF on TFF.IdTipoFte = IECDP.IdTipoFte
		INNER JOIN BaseSIF.TipoRecursoFinPptal TRFP on TRFP.IdTipoRecursoFinPptal = IECDP.IdTipoRecursoFinPptal
		INNER JOIN Ppto.DependenciasAfectacionSIIF DASIIF on DASIIF.IdDepAfecSIIF = IECDP.IdDepAfecSIIF
		INNER JOIN BaseSIF.TipoSitFondos TSF On TSF.IdTipoSitFondos = IECDP.IdTipoSitFondos
		WHERE CCDP.[IdContrato] = @IdContrato
	END
     
    IF ((@ValorInicialContrato) > (@ValorTotalCDPs + @ValorTotalVigenciasFuturas + @ValorVigenciaFutura))
    BEGIN
 
        SELECT @IdContrato AS IdContrato
        RETURN
 
    END
     
END
 



GO


