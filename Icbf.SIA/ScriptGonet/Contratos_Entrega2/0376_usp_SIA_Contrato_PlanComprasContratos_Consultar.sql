USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratos_Consultar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:27 PM
-- Description:	Procedimiento almacenado que consulta un(a) PlanComprasContratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasContratos_Consultar]
	@IdProductoPlanCompraContrato INT
AS
BEGIN
	SELECT PCC.IDPlanDeComprasContratos,
		PCC.Vigencia,
		PCC.IdContrato,
		PCC.IDPlanDeCompras,
		PCC.CodigoRegional,
		PCC.UsuarioCrea,
		PCC.FechaCrea,
		PCC.UsuarioModifica,
		PCC.FechaModifica
	FROM [Contrato].[PlanDeComprasContratos] PCC
	WHERE PCC.IDPlanDeComprasContratos = @IdProductoPlanCompraContrato
END
