USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Procedimiento almacenado que elimina un(a) Proveedores_Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar]
	@IdProveedoresContratos INT
AS
BEGIN
	DELETE CONTRATO.ProveedoresContratos
	WHERE IdProveedoresContratos = @IdProveedoresContratos
END
