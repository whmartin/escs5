USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar]    Script Date: 09/25/2014 15:18:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar]    Script Date: 09/25/2014 15:18:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Procedimiento almacenado que elimina un(a) Proveedores_Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar]
	@IdProveedoresContratos INT,
	@IdContrato INT = NULL
AS
BEGIN

	IF @IdContrato IS NOT NULL
	BEGIN
		DELETE FROM CONTRATO.AporteContrato
		WHERE IdContrato = @IdContrato AND 
		IDEntidadProvOferente = (SELECT IDEntidadProvOferente FROM CONTRATO.ProveedoresContratos
		WHERE IdProveedoresContratos = @IdProveedoresContratos)
	END

	DELETE CONTRATO.ProveedoresContratos
	WHERE IdProveedoresContratos = @IdProveedoresContratos
END

GO


