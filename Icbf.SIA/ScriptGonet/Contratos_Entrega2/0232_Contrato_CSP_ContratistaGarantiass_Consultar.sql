USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ContratistaGarantiass_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantiass_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 2:25:34 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContratistaGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantiass_Consultar]
	@IDGarantia INT = NULL,@IDEntidadProvOferente INT = NULL
AS
BEGIN
 SELECT IDContratista_Garantias, IDGarantia, IDEntidadProvOferente, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ContratistaGarantias] WHERE IDGarantia = CASE WHEN @IDGarantia IS NULL THEN IDGarantia ELSE @IDGarantia END AND IDEntidadProvOferente = CASE WHEN @IDEntidadProvOferente IS NULL THEN IDEntidadProvOferente ELSE @IDEntidadProvOferente END
END
