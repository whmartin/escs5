USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Eliminacion table [CONTRATO].[LugarEjecucionContrato]
-- =============================================

/****** Object:  Table [CONTRATO].[LugarEjecucionContrato]    Script Date: 23/05/2014 16:35:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[LugarEjecucionContrato]') AND type in (N'U'))
DROP TABLE [CONTRATO].[LugarEjecucionContrato]
GO
