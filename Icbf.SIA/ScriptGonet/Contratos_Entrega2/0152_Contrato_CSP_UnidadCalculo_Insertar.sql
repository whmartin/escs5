USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_UnidadCalculo_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculo_Insertar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/3/2014 3:19:34 PM
-- Description:	Procedimiento almacenado que guarda un nuevo UnidadCalculo
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculo_Insertar]
		@IDUnidadCalculo INT OUTPUT, 	@CodUnidadCalculo NVARCHAR(8),	@Descripcion NVARCHAR(80),	@Inactivo BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	IF EXISTS(SELECT * FROM Contrato.UnidadCalculo WHERE CodUnidadCalculo=@CodUnidadCalculo)
	BEGIN
		RAISERROR('Ya existe un registro con este(a) Código Unidad Calculo',16,1)
	    RETURN
	END
	IF EXISTS(SELECT * FROM Contrato.UnidadCalculo WHERE Descripcion=@Descripcion)
	BEGIN
		RAISERROR('Ya existe un registro con este(a) Descripción',16,1)
	    RETURN
	END
	INSERT INTO Contrato.UnidadCalculo(CodUnidadCalculo, Descripcion, Inactivo, UsuarioCrea, FechaCrea)
					  VALUES(@CodUnidadCalculo, @Descripcion, @Inactivo, @UsuarioCrea, GETDATE())
	SELECT @IDUnidadCalculo = @@IDENTITY 		
END
