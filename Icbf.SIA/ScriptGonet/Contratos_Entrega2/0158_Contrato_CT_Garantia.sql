USE [SIA]
GO
-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/4/2014 9:59:59 AM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[Garantia]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla Garantia a crear'
RETURN
END
CREATE TABLE [Contrato].[Garantia](
 [IDGarantia] [INT]  IDENTITY(1,1) NOT NULL,
 [IDTipoGarantia] [INT]  NOT NULL,
 [NumeroGarantia] [NVARCHAR] (80) ,
 [Aprobada] [BIT]  ,
 [Devuelta] [BIT]  ,
 [FechaAprobacionGarantia] [DATETIME]  ,
 [FechaCertificacionGarantia] [DATETIME]  ,
 [FechaDevolucion] [DATETIME]  ,
 [MotivoDevolucion] [NVARCHAR] (800) NULL,
 [IDUsuarioAprobacion] [INT]  ,
 [IDUsuarioDevolucion] [INT]  ,
 [IdContrato] [INT]  ,
 [BeneficiarioICBF] [BIT]  ,
 [DescripcionBeneficiarios] [NVARCHAR] (400) NOT NULL,
 [FechaInicioGarantia] [DATETIME]  ,
 [FechaExpedicionGarantia] [DATETIME]  ,
 [FechaVencimientoInicialGarantia] [DATETIME]  ,
 [FechaVencimientoFinalGarantia] [DATETIME]  ,
 [FechaReciboGarantia] [DATETIME]  ,
 [ValorGarantia] [NVARCHAR] (50) ,
 [Anexos] [BIT]  ,
 [ObservacionesAnexos] [NVARCHAR] (400)  NULL,
 [EntidadProvOferenteAseguradora] [INT]  NULL,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 Estado bit,
 IDSucursalAseguradoraContrato int,
 IDEstadosGarantias int,
 BeneficiarioOTROS bit
 CONSTRAINT [PK_Garantia] PRIMARY KEY CLUSTERED 
(
 	[IDGarantia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Hace referencia a la garantia del contrato.', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = IDGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDTipoGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'ID Tipo Garantia', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = IDTipoGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'NumeroGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Numero Identificacion ICBF', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = NumeroGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Aprobada' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'si el valor esta en 1 la garantia esta aprobada', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = Aprobada;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Devuelta' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'si el valor esta en 1 la garantia esta devuelta ', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = Devuelta;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaAprobacionGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha Aprobacion Garantia', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaAprobacionGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCertificacionGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha Certificacion Garantia', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaCertificacionGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaDevolucion' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha Devolucion de la garantía', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaDevolucion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'MotivoDevolucion' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Motivo Devolucion de la garantía', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = MotivoDevolucion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDUsuarioAprobacion' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'usuario que aprueba la garantia', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = IDUsuarioAprobacion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDUsuarioDevolucion' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'usuario devolucion', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = IDUsuarioDevolucion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdContrato' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Id Contrato', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = IdContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'BeneficiarioICBF' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Beneficiario y/o Asegurado y/o Afianzado ICBF  1,Otros 0', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = BeneficiarioICBF;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'DescripcionBeneficiarios' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Descripción de los Beneficiarios y/o Asegurados y/o Afianzado', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = DescripcionBeneficiarios;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaInicioGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de Inicio Garantía', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaInicioGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaExpedicionGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de Expedición Garantía', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaExpedicionGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaVencimientoInicialGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de Vencimiento Inicial Garantía', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaVencimientoInicialGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaVencimientoFinalGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de Vencimiento Final Garantía', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaVencimientoFinalGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaReciboGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de Recibo Garantía', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaReciboGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'ValorGarantia' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Valor Garantia', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = ValorGarantia;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Anexos' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Anexos', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = Anexos;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'ObservacionesAnexos' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Observaciones Anexos', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = ObservacionesAnexos;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'EntidadProvOferenteAseguradora' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Entidad Prov Oferente Aseguradora', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = EntidadProvOferenteAseguradora;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.Garantia') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('Contrato.Garantia')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = Garantia,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
