USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  2014-07-09
-- Description:	Creacion tabla contratos.lugarEjecucion
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[LugarEjecucionContrato_IdDepartamento_FK]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[LugarEjecucionContrato]'))
ALTER TABLE [CONTRATO].[LugarEjecucionContrato] DROP CONSTRAINT [LugarEjecucionContrato_IdDepartamento_FK]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[LugarEjecucionContrato_IdMunicipio_FK]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[LugarEjecucionContrato]'))
ALTER TABLE [CONTRATO].[LugarEjecucionContrato] DROP CONSTRAINT [LugarEjecucionContrato_IdMunicipio_FK]
GO

/****** Object:  Table [CONTRATO].[LugarEjecucionContrato]    Script Date: 05/16/2014 16:11:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[LugarEjecucionContrato]') AND type in (N'U'))
DROP TABLE [CONTRATO].[LugarEjecucionContrato]
GO

/****** Object:  Table [CONTRATO].[LugarEjecucionContrato]    Script Date: 05/16/2014 16:11:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CONTRATO].[LugarEjecucionContrato](
	[IdLugarEjecucionContratos] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [int] NOT NULL,
	[IdDepartamento] [int] NULL,
	[IdRegional] [int] NULL,
	[NivelNacional] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_LugarEjecucionContrato] PRIMARY KEY CLUSTERED 
(
	[IdLugarEjecucionContratos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [CONTRATO].[LugarEjecucionContrato]  WITH CHECK ADD  CONSTRAINT [LugarEjecucionContrato_IdDepartamento_FK] FOREIGN KEY([IdDepartamento])
REFERENCES [DIV].[Departamento] ([IdDepartamento])
GO

ALTER TABLE [CONTRATO].[LugarEjecucionContrato] CHECK CONSTRAINT [LugarEjecucionContrato_IdDepartamento_FK]
GO

ALTER TABLE [CONTRATO].[LugarEjecucionContrato]  WITH CHECK ADD  CONSTRAINT [LugarEjecucionContrato_IdMunicipio_FK] FOREIGN KEY([IdRegional])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO

ALTER TABLE [CONTRATO].[LugarEjecucionContrato] CHECK CONSTRAINT [LugarEjecucionContrato_IdMunicipio_FK]
GO


IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Almacena datos sobre Lugar de ejecución de contratos', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdLugarEjecucionContratos' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = IdLugarEjecucionContratos;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdContrato' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Id Contrato', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = IdContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdDepartamento' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Id Departamento', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = IdDepartamento;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdRegional' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Id Regional', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = IdRegional;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'NivelNacional' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'NivelNacional', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = NivelNacional;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = LugarEjecucionContrato,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO