USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_FormaPagosUsuarioCrea_Consultar]    Script Date: 09/06/2014 08:49:28 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ContratoUsuarioCrea_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratoUsuarioCrea_Consultar]
GO



-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  09/06/2014 14:56
-- Description:	Procedimiento almacenado que consulta un(a) contrato FormaPago por el usuario crea
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratoUsuarioCrea_Consultar]
	@UsuarioCrea varchar(250) = NULL,
	@IdContrato int = null
AS
BEGIN
SELECT  [IdContrato]
      ,[IdRegionalContrato]
      ,[IdNumeroProceso]
      ,[IdTipoContrato]
      ,[IdEstadoContrato]
      ,[ObjetoDelContrato]
      ,[FechaInicioEjecucion]
      ,[FechaFinalTerminacionContrato]
      ,[FechaFinalizacionIniciaContrato]
      ,[FechaSuscripcionContrato]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
      ,[NumeroContrato]
      ,[ValorInicialContrato]
      ,[IdFormaPago]
  FROM [CONTRATO].[Contrato]
  WHERE UsuarioCrea = CASE WHEN @UsuarioCrea IS NULL THEN UsuarioCrea ELSE @UsuarioCrea END 
  AND IdContrato = CASE WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato END
END




