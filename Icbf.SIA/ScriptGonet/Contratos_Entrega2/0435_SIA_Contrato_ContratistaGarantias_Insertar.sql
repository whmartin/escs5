USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]    Script Date: 09/07/2014 02:34:45 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]    Script Date: 09/07/2014 02:34:45 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  6/20/2014 2:25:33 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ContratistaGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]
		@IDContratista_Garantias INT OUTPUT, 	@IDGarantia INT,	@IDEntidadProvOferente INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN

	IF NOT EXISTS (Select IDContratista_Garantias from Contrato.ContratistaGarantias where IDGarantia = @IDGarantia AND IDEntidadProvOferente = @IDEntidadProvOferente)
	BEGIN
		INSERT INTO Contrato.ContratistaGarantias(IDGarantia, IDEntidadProvOferente, UsuarioCrea, FechaCrea)
						  VALUES(@IDGarantia, @IDEntidadProvOferente, @UsuarioCrea, GETDATE())
		SELECT @IDContratista_Garantias = @@IDENTITY
	END	
END

