USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_OAC_OAC_DocVisitasReclamoArchivoss_Consultar]    Script Date: 25/06/2014 02:50:49 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ArchivosContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ArchivosContratos_Consultar]
GO

-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  25/06/2014 14:54 PM
-- Description: Procedimiento almacenado de documentos asociados a un contrato suscrito
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ArchivosContratos_Consultar]
@IdContrato INT = NULL
AS
BEGIN
	SELECT AC.[IdArchivoContrato]
      ,AC.[IdArchivo]
      ,AC.[IdContrato]
      ,AC.[UsuarioCrea]
      ,AC.[FechaCrea]
      ,AC.[UsuarioModifica]
      ,AC.[FechaModifica]
	  ,A.[IdUsuario]
      ,A.[IdFormatoArchivo]
      ,A.[FechaRegistro]
      ,A.[NombreArchivo]
      ,A.[Estado]
      ,A.[ResumenCarga]
      ,A.[UsuarioCrea]
      ,A.[FechaCrea]
      ,A.[UsuarioModifica]
      ,A.[FechaModifica]
      ,A.[ServidorFTP]
      ,A.[NombreArchivoOri]
  FROM [CONTRATO].[ArchivosContratos] AC
  Inner join [Estructura].[Archivo] A on A.[IdArchivo] = AC.[IdArchivo]
WHERE AC.[IdContrato] =
	CASE
		WHEN @IdContrato IS NULL THEN AC.[IdContrato] ELSE @IdContrato
	END
END