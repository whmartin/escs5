USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar]    Script Date: 13/06/2014 09:40:06 a.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar')
BEGIN
DROP PROCEDURE [dbo].[usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar]    Script Date: 13/06/2014 09:40:06 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Obtiene las modalidades academicas registradas en Kactus
-- =============================================
CREATE PROCEDURE [dbo].[usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar] 
	@Nombre VARCHAR(100) = NULL, @Id VARCHAR(30) = NULL, @Estado BIT = NULL
AS
BEGIN

	SELECT NULL AS Id, ID_Modal AS Codigo, Des_Moda AS Descripcion FROM [KACTUS].[KPRODII].[dbo].[Mo_Acade]
	WHERE Des_Moda = ISNULL(@Nombre, Des_Moda)

END



GO


