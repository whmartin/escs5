USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Insert data [CONTRATO].[EstadoContrato] 
-- =============================================

IF NOT EXISTS(SELECT * FROM [CONTRATO].[EstadoContrato] WHERE [CodEstado] = 'REE' AND [Descripcion] = 'En Registro')
BEGIN
SET IDENTITY_INSERT [CONTRATO].[EstadoContrato] ON 
INSERT [CONTRATO].[EstadoContrato] ([IDEstadoContrato], [CodEstado], [Descripcion], [Inactivo], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'REE ', N'En Registro', 1, N'Administrador', CAST(0x0000A35300000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [CONTRATO].[EstadoContrato] OFF
END

IF NOT EXISTS(SELECT * FROM [CONTRATO].[EstadoContrato] WHERE [CodEstado] = 'REC' AND [Descripcion] = 'Registrado')
BEGIN
SET IDENTITY_INSERT [CONTRATO].[EstadoContrato] ON 
INSERT [CONTRATO].[EstadoContrato] ([IDEstadoContrato], [CodEstado], [Descripcion], [Inactivo], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'REC ', N'Registrado', 1, N'Administrador', CAST(0x0000A35300000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [CONTRATO].[EstadoContrato] OFF
END