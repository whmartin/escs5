USE [SIA]
GO

/********************************
2014-05-22
Creamos el SYNONYM AreasInternas 
jorge Vizcaino
********************************/

/****** Object:  Synonym [Ppto].[AreasInternas]    Script Date: 22/05/2014 11:45:23 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'AreasInternas' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[AreasInternas]
GO

/****** Object:  Synonym [Ppto].[AreasInternas]    Script Date: 22/05/2014 11:45:23 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'AreasInternas' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[AreasInternas] FOR [NMF].[Ppto].[AreasInternas]
GO


