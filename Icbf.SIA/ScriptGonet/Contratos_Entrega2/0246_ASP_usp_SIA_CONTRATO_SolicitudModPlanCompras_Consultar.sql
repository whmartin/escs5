USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]    Script Date: 25/06/2014 14:31:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]
GO


-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/22/2014 1:36:08 PM
-- Description:	Procedimiento almacenado que consulta un(a) SolicitudModPlanCompras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]
	@IdSolicitudModPlanCompra INT
AS
BEGIN
  
	 SELECT SolPC.IdSolicitudModPlanCompra, SolPC.Justificacion, SolPC.NumeroSolicitud, SolPC.FechaSolicitud, SolPC.IdEstadoSolicitud, 
	 SolPC.IdUsuarioSolicitud, SolPC.IdPlanDeCompras, SolPC.IdVigencia, SolPC.UsuarioCrea, SolPC.FechaCrea, SolPC.UsuarioModifica, SolPC.FechaModifica 
	 ,0 AS IdContrato
	 ,0 AS NumeroContrato
	 ,PC.NumeroConsecutivo AS NumeroConsecPlanDeCompras
	 ,(Usu.PrimerNombre +'' + Usu.SegundoNombre +'' + Usu.PrimerApellido +'' + Usu.SegundoApellido) AS UsuarioSolicitud
	 ,PC.Vigencia AS Vigencia
	 ,EdoSol.Descripcion AS EstadoSolicitud
	 FROM [CONTRATO].[SolicitudModPlanCompras] SolPC 
	 INNER JOIN [CONTRATO].[PlanDeCompras] PC ON SolPC.IdPlanDeCompras = PC.IdPlanDeCompras
	 INNER JOIN [SEG].[Usuario] Usu ON SolPC.IdUsuarioSolicitud = Usu.IdUsuario
	 --INNER JOIN [Global].[Vigencia] Vig ON SolPC.IdVigencia = Vig.IdVigencia
	 INNER JOIN [CONTRATO].[EstadoSolcitudModPlanCompras] EdoSol ON SolPC.IdEstadoSolicitud = EdoSol.IdEstadoSolicitud
 
	 WHERE SolPC.IdSolicitudModPlanCompra = @IdSolicitudModPlanCompra
END

