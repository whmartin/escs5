USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]    Script Date: 04/08/2014 08:50:02 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]
GO


-- =============================================
-- Author:		Gonet / Efrain Diaz Mejia
-- Create date:  09/07/2014 18:20
-- Description: procedimiento almacenado que consulta la informacion de los RP asociado a un contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]
	@IdContrato INT = NULL,
	@IdRP INT = NULL
AS
BEGIN
SELECT RPC.[IdRPContratos]
      ,RPC.[IdContrato]
      ,RPC.[IdRP]
      ,RPC.[UsuarioCrea]
      ,RPC.[FechaCrea]
      ,RPC.[UsuarioModifica]
      ,RPC.[FechaModifica]
	  ,IAG.ValorInicialItem ValorRP
	  ,R.CodigoRegional CodigoRegional
	  ,CP.FechaRegCompromiso FechaExpedicionRP
	  ,rtrim(ltrim(R.CodigoRegional)) +' - '+ rtrim(ltrim(R.NombreRegional)) NombreRegional
  FROM [CONTRATO].[RPContratos] RPC
  inner join [Ppto].[CompromisosPresupuestales] CP on RPC.IdRP = CP.IdCompromiso
  inner join [Ppto].[CompItemAfectacionGasto] IAG ON IAG.IdCompromiso = CP.IdCompromiso  
  inner join Ppto.RegionalesPCI RPCI on RPCI.IdRegionalPCI = CP.IdRegionalPCI
  inner join DIV.Regional R on RPCI.CodRegionalIcbf collate DATABASE_DEFAULT = R.CodigoRegional collate DATABASE_DEFAULT
  WHERE RPC.IdRP =
		CASE
			WHEN @IdRP IS NULL THEN RPC.IdRP ELSE @IdRP
		END AND RPC.IdContrato =
		CASE
			WHEN @IdContrato IS NULL THEN RPC.IdContrato ELSE @IdContrato
		END
END





