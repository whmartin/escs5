USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_ConsultarValidar]    Script Date: 05/28/2014 11:36:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_ConsultarValidar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_ConsultarValidar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_ConsultarValidar]    Script Date: 05/28/2014 11:36:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham
-- Create date: 2014-05-28
-- Description:	Consulta consecutivo de contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_ConsultarValidar] 
	@Consecutivo NUMERIC(30,0),
	@IdRegional INT,
	@IdVigencia INT
AS
BEGIN
	SELECT * FROM [CONTRATO].[ConsecutivoContratoRegionales]
	where IdRegional = @IdRegional
	AND IdVigencia = @IdVigencia
	AND Consecutivo = @Consecutivo
END

GO


