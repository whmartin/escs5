USE [SIA]
GO

/***************************************************/
/* Autor: Emilio Calapi�a					       */
/* Fecha: 09/09/2014 09:49 a.m.                    */
/* Descripci�n: Se agrega columna IdPagos Detalle  */
/***************************************************/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[RubroPlanComprasContrato]') AND type in (N'U'))
BEGIN
	
	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'RubroPlanComprasContrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdPagosDetalle')
	BEGIN 
		ALTER TABLE CONTRATO.RubroPlanComprasContrato
		ADD IdPagosDetalle NVARCHAR(150) NULL;
	END
	

END