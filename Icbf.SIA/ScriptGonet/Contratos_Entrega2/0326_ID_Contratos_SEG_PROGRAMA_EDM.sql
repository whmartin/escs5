USE [SIA]
GO
-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  20/06/2014 15:06 PM
-- Description:	Insert tabla [SEG].[Programa] para mostrar opci�n men�.
-- =============================================

IF not exists(SELECT [IdModulo] from [SEG].[Programa] where [NombrePrograma] = 'Suscripci�n Contrato')

Begin
        
INSERT INTO [SEG].[Programa]
           ([IdModulo]
           ,[NombrePrograma]
           ,[CodigoPrograma]
           ,[Posicion]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           ,[UsuarioModificacion]
           ,[FechaModificacion]
           ,[VisibleMenu]
           ,[generaLog]
           ,[Padre])
     VALUES
           (3
           ,'Suscripci�n Contrato'
           ,'Contratos/SuscripcionContrato'
           ,6
           ,1
           ,'Administrador'
           ,GETDATE()
           ,NULL
           ,NULL
           ,1
           ,1
           ,NULL
           )
END



