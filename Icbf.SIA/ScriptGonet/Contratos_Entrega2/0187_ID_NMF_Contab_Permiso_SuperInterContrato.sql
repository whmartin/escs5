USE [SIA]
/***********************************************
Creado Por: Carlos Andr�s C�rdenas
El dia : 2013-08-21
Modificado por: Abraham Rivero Dom�nguez
El dia: 2013-06-10
Permite : Crear el permiso al programa Contratos 
***********************************************/
IF EXISTS (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Contratos/ConsultarSuperInterContrato')
BEGIN

	IF NOT EXISTS (SELECT 1 FROM [SEG].[Permiso]
					WHERE [IdRol] = (SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'Administrador')
					AND [IdPrograma] = (SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Contratos/ConsultarSuperInterContrato'))
	BEGIN
		INSERT INTO [SEG].[Permiso]
			   ([IdPrograma]
			   ,[IdRol]
			   ,[Insertar]
			   ,[Modificar]
			   ,[Eliminar]
			   ,[Consultar]
			   ,[UsuarioCreacion]
			   ,[FechaCreacion])           
		 VALUES
			   ((SELECT  [IdPrograma] FROM [SEG].[Programa] WHERE [CodigoPrograma] = 'Contratos/ConsultarSuperInterContrato')
			   ,(SELECT  [IdRol]  FROM [SEG].[Rol] WHERE Nombre = 'Administrador')
			   ,1
			   ,1
			   ,1
			   ,1
			   ,'Administrador'
			   ,GETDATE()) 
	END
END      
GO
