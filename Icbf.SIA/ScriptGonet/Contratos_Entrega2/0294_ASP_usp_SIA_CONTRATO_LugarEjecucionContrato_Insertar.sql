USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]    Script Date: 01/07/2014 17:10:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/14/2014 4:20:50 PM
-- Description:	Procedimiento almacenado que guarda un nuevo LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar]
		@IdLugarEjecucionContratos INT OUTPUT, 	
		@IdContrato INT,	
		--@IdDepartamento int,	
		@IdsRegionales nvarchar(MAX),	
		@NivelNacional BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN

	if (@NivelNacional IS NULL OR @NivelNacional = 0)
	begin

		delete CONTRATO.LugarEjecucionContrato where IdContrato = @IdContrato and NivelNacional = 1-- cuando es nivel nacional limpiados todos los municipio que se han agregados

		IF CHARINDEX(',', @IdsRegionales) > 0
		BEGIN
			insert into CONTRATO.LugarEjecucionContrato(IdContrato, IdDepartamento, IdRegional, NivelNacional, UsuarioCrea, FechaCrea)
			select @IdContrato, IdDepartamento, IdMunicipio, 0, @UsuarioCrea, GETDATE()
			FROM DIV.Municipio 
			where IdMunicipio IN (Select splitdata from fnSplitString(@IdsRegionales, ','))
			And	  IdMunicipio not in (select IdRegional from  CONTRATO.LugarEjecucionContrato where IdContrato = @IdContrato)
			
			SELECT @IdLugarEjecucionContratos = 1000 
		END
		ELSE
		BEGIN
			DECLARE @IdDep int 
			select @IdDep = IdDepartamento FROM DIV.Municipio where IdMunicipio = CAST(@IdsRegionales AS INT)
			IF Not Exists(select 1 from CONTRATO.LugarEjecucionContrato where IdContrato = @IdContrato AND IdRegional = CAST(@IdsRegionales AS INT))
			BEGIN
				INSERT INTO CONTRATO.LugarEjecucionContrato(IdContrato, IdDepartamento, IdRegional, NivelNacional, UsuarioCrea, FechaCrea)
						  VALUES(@IdContrato, @IdDep, CAST(@IdsRegionales AS INT), 0, @UsuarioCrea, GETDATE())
			
				SELECT @IdLugarEjecucionContratos = @@IDENTITY 
			END
			Else
			BEGIN
				RAISERROR('Ya se encuentra asociado el municipio al contrato',16,1)
				RETURN
			END
		END
	END
	ELSE
	BEGIN
		
		delete CONTRATO.LugarEjecucionContrato where IdContrato = @IdContrato -- cuando es nivel nacional limpiados todos los municipio que se han agregados

		INSERT INTO CONTRATO.LugarEjecucionContrato(IdContrato, IdDepartamento, IdRegional, NivelNacional, UsuarioCrea, FechaCrea)
					  VALUES(@IdContrato, NULL, NULL, @NivelNacional, @UsuarioCrea, GETDATE())
		
		SELECT @IdLugarEjecucionContratos = @@IDENTITY 
	END
			
END
