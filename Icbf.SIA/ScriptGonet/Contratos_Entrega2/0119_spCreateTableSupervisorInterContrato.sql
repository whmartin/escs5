USE [SIA]
GO
-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/30/2014 12:23:44 PM
-- Descripcion: Creacion tabla [CONTRATO].[SupervisorInterContrato]
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[SupervisorInterContrato]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla SupervisorInterContrato a crear'
RETURN
END
CREATE TABLE [CONTRATO].[SupervisorInterContrato](
 [IDSupervisorIntervContrato] [INT]  IDENTITY(1,1) NOT NULL,
 [FechaInicio] [DATETIME]  NOT NULL,
 [Inactivo] [BIT]  NOT NULL,
 [Identificacion] [NVARCHAR] (80) NOT NULL,
 [TipoIdentificacion] [NVARCHAR] (50) NOT NULL,
 [IDTipoSuperInter] [INT]  NOT NULL,
 [IdNumeroContratoInterventoria] [INT]  ,
 [IDProveedoresInterventor] [INT]  ,
 [IDEmpleadosSupervisor] [INT]  ,
 [IdContrato] [INT]  ,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_SupervisorInterContrato] PRIMARY KEY CLUSTERED 
(
 	[IDSupervisorIntervContrato] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_TipoSuperInter_IDTipoSuperInter]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SupervisorInterContrato]'))
ALTER TABLE [CONTRATO].[SupervisorInterContrato]
ADD CONSTRAINT FK_TipoSuperInter_IDTipoSuperInter
FOREIGN KEY (IDTipoSuperInter) REFERENCES [CONTRATO].[TipoSuperInter](IDTipoSuperInter)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_ContratoInterventoria_IdContrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SupervisorInterContrato]'))
ALTER TABLE [CONTRATO].[SupervisorInterContrato]
ADD CONSTRAINT FK_ContratoInterventoria_IdContrato
FOREIGN KEY (IdNumeroContratoInterventoria) REFERENCES [CONTRATO].[Contrato](IdContrato)
go

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_EntidadProvOferente_IdEntidad]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SupervisorInterContrato]'))
ALTER TABLE [CONTRATO].[SupervisorInterContrato]
ADD CONSTRAINT FK_EntidadProvOferente_IdEntidad
FOREIGN KEY (IDProveedoresInterventor) REFERENCES [PROVEEDOR].[EntidadProvOferente](IdEntidad)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_IdContrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SupervisorInterContrato]'))
ALTER TABLE [CONTRATO].[SupervisorInterContrato]
ADD CONSTRAINT FK_Contrato_IdContrato
FOREIGN KEY (IdContrato) REFERENCES [CONTRATO].[Contrato](IdContrato)
GO



IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Almacena datos sobre SupervisorInterContrato', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDSupervisorIntervContrato' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = IDSupervisorIntervContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaInicio' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha Inicio', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = FechaInicio;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Inactivo' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Inactivo', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = Inactivo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Identificacion' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Identificacion', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = Identificacion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'TipoIdentificacion' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'TipoIdentificacion', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = TipoIdentificacion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDTipoSuperInter' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'IDTipoSuperInter', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = IDTipoSuperInter;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdNumeroContratoInterventoria' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'IdNumeroContratoInterventoria', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = IdNumeroContratoInterventoria;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDProveedoresInterventor' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'IDProveedoresInterventor', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = IDProveedoresInterventor;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDEmpleadosSupervisor' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'IDEmpleadosSupervisor', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = IDEmpleadosSupervisor;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdContrato' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'IdContrato', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = IdContrato;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('CONTRATO.SupervisorInterContrato')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = SupervisorInterContrato,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
