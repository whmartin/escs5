use sia

-- =============================================
-- Author:		Gonet
-- Create date: 2014-07-09
-- Description:	Alterar tabla CONTRATO.ProveedoresContratos
-- =============================================

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.ProveedoresContratos') AND NAME = ('IDFormaPago'))
BEGIN
	alter table CONTRATO.ProveedoresContratos add [IDFormaPago] Int NULL

	alter table CONTRATO.ProveedoresContratos add constraint FK_CONTRATO_ProveedoresContratos_FormaPago
	foreign key (IDFormaPago)
	references CONTRATO.FormaPago (IDFormaPago)
END
ELSE
BEGIN
	PRINT 'YA EXISTE EL CAMPO IDFormaPago'
END


