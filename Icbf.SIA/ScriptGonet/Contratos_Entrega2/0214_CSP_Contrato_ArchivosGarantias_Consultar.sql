USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ArchivosGarantias_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantias_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 10:24:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantias_Consultar]
	@IDArchivosGarantias INT
AS
BEGIN
 SELECT IDArchivosGarantias, IDArchivo, IDGarantia, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ArchivosGarantias] WHERE  IDArchivosGarantias = @IDArchivosGarantias
END
