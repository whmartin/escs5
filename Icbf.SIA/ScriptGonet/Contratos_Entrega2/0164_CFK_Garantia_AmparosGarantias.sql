USE [SIA]
GO
-- =============================================
-- Author:		Gonet\Carlos Andr�s C�rdenas P.
-- Create date:  03/06/2014 04:15:00 PM
-- Description:	Creacion de llave for�nea FK_Garantia_AmparosGarantias
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Contrato].[FK_Garantia_AmparosGarantias]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [Contrato].[AmparosGarantias] DROP CONSTRAINT FK_Garantia_AmparosGarantias
END

ALTER TABLE [Contrato].[AmparosGarantias]
ADD CONSTRAINT FK_Garantia_AmparosGarantias FOREIGN KEY(IDGarantia)
REFERENCES  [Contrato].[Garantia](IDGarantia)

