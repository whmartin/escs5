USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date: 18/06/2014 11:15
-- Description:	Insertar campo [FechaModifica] de la tabla Contrato.EstadoContrato
-- =============================================

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'EstadoContrato' 
           AND  COLUMN_NAME = 'FechaModifica')

	ALTER TABLE Contrato.EstadoContrato
	ADD
	   [FechaModifica] [datetime] NULL
	   	
GO