USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Insert data [CONTRATO].[CategoriaContrato]
-- =============================================

IF NOT EXISTS(SELECT * FROM [CONTRATO].[CategoriaContrato] WHERE [CodigoCategoriaContrato] = 'CTR' AND [NombreCategoriaContrato] = 'CONTRATO')
BEGIN
SET IDENTITY_INSERT [CONTRATO].[CategoriaContrato] ON 
INSERT [CONTRATO].[CategoriaContrato] ([IdCategoriaContrato], [CodigoCategoriaContrato], [NombreCategoriaContrato], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'CTR', N'CONTRATO', N'Contrato', 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [CONTRATO].[CategoriaContrato] OFF
END

IF NOT EXISTS(SELECT * FROM [CONTRATO].[CategoriaContrato] WHERE [CodigoCategoriaContrato] = 'CVN' AND [NombreCategoriaContrato] = 'CONVENIO')
BEGIN
SET IDENTITY_INSERT [CONTRATO].[CategoriaContrato] ON 
INSERT [CONTRATO].[CategoriaContrato] ([IdCategoriaContrato], [CodigoCategoriaContrato], [NombreCategoriaContrato], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'CVN', N'CONVENIO', N'Convenio', 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [CONTRATO].[CategoriaContrato] OFF
END