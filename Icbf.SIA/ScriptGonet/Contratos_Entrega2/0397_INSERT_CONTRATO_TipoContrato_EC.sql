USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Insert data [CONTRATO].[TipoContrato]
-- =============================================

DECLARE @IDCATEGORIA INT

SET @IDCATEGORIA = 0
SELECT @IDCATEGORIA=[IdCategoriaContrato] FROM [CONTRATO].[CategoriaContrato] WHERE [CodigoCategoriaContrato] = 'CTR'

IF @IDCATEGORIA <> 0
BEGIN
	SET IDENTITY_INSERT [CONTRATO].[TipoContrato] ON 
	IF NOT EXISTS (SELECT * FROM [CONTRATO].[TipoContrato] WHERE [CodigoTipoContrato] = 'PREST_SERV_APO_GEST' AND [NombreTipoContrato] = 'Prestación de servicios de apoyo a la gestión')
	INSERT [CONTRATO].[TipoContrato] ([IdTipoContrato], [CodigoTipoContrato], [NombreTipoContrato], [IdCategoriaContrato], [ActaInicio], [AporteCofinaciacion], [RecursoFinanciero], [RegimenContrato], [DescripcionTipoContrato], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (9, N'PREST_SERV_APO_GEST', N'Prestación de servicios de apoyo a la gestión', @IDCATEGORIA, 0, 0, 0, 1, NULL, 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
	IF NOT EXISTS (SELECT * FROM [CONTRATO].[TipoContrato] WHERE [CodigoTipoContrato] = 'PREST_SERV_PROF' AND [NombreTipoContrato] = 'Prestación de servicios profesionales')
	INSERT [CONTRATO].[TipoContrato] ([IdTipoContrato], [CodigoTipoContrato], [NombreTipoContrato], [IdCategoriaContrato], [ActaInicio], [AporteCofinaciacion], [RecursoFinanciero], [RegimenContrato], [DescripcionTipoContrato], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (10, N'PREST_SERV_PROF', N'Prestación de servicios profesionales', @IDCATEGORIA, 0, 0, 0, 1, NULL, 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
	IF NOT EXISTS (SELECT * FROM [CONTRATO].[TipoContrato] WHERE [CodigoTipoContrato] = 'APORTE' AND [NombreTipoContrato] = 'Aporte Contrato')
	INSERT [CONTRATO].[TipoContrato] ([IdTipoContrato], [CodigoTipoContrato], [NombreTipoContrato], [IdCategoriaContrato], [ActaInicio], [AporteCofinaciacion], [RecursoFinanciero], [RegimenContrato], [DescripcionTipoContrato], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (12, N'APORTE', N'Aporte Contrato', @IDCATEGORIA, 0, 0, 0, 1, NULL, 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
	SET IDENTITY_INSERT [CONTRATO].[TipoContrato] OFF
END


SET @IDCATEGORIA = 0
SELECT @IDCATEGORIA=[IdCategoriaContrato] FROM [CONTRATO].[CategoriaContrato] WHERE [CodigoCategoriaContrato] = 'CVN'

IF @IDCATEGORIA <> 0
BEGIN
	--SET IDENTITY_INSERT [CONTRATO].[TipoContrato] ON
	IF NOT EXISTS (SELECT * FROM [CONTRATO].[TipoContrato] WHERE [CodigoTipoContrato] = 'MC_INT_ADMIN' AND [NombreTipoContrato] = 'Marco Interadministrativo')
	INSERT [CONTRATO].[TipoContrato] ([CodigoTipoContrato], [NombreTipoContrato], [IdCategoriaContrato], [ActaInicio], [AporteCofinaciacion], [RecursoFinanciero], [RegimenContrato], [DescripcionTipoContrato], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (N'MC_INT_ADMIN', N'Marco Interadministrativo', @IDCATEGORIA, 0, 0, 0, 1, NULL, 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
	IF NOT EXISTS (SELECT * FROM [CONTRATO].[TipoContrato] WHERE [CodigoTipoContrato] = 'APORTE' AND [NombreTipoContrato] = 'Aporte Convenio')
	INSERT [CONTRATO].[TipoContrato] ([CodigoTipoContrato], [NombreTipoContrato], [IdCategoriaContrato], [ActaInicio], [AporteCofinaciacion], [RecursoFinanciero], [RegimenContrato], [DescripcionTipoContrato], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (N'APORTE', N'Aporte Convenio', @IDCATEGORIA, 0, 0, 0, 1, NULL, 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
	--SET IDENTITY_INSERT [CONTRATO].[TipoContrato] OFF
END 
