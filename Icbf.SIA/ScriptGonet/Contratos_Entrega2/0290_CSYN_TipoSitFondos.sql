USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion sinonimo [BaseSIF].[TipoSitFondos]
-- =============================================

/****** Object:  Synonym [BaseSIF].[TipoSitFondos]    Script Date: 01/07/2014 16:35:59 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoSitFondos' AND schema_id = SCHEMA_ID(N'BaseSIF'))
DROP SYNONYM [BaseSIF].[TipoSitFondos]
GO

/****** Object:  Synonym [BaseSIF].[TipoSitFondos]    Script Date: 01/07/2014 16:35:59 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoSitFondos' AND schema_id = SCHEMA_ID(N'BaseSIF'))
CREATE SYNONYM [BaseSIF].[TipoSitFondos] FOR [NMF].[BaseSIF].[TipoSitFondos]
GO