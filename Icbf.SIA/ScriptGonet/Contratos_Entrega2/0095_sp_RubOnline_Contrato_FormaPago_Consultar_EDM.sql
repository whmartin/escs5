USE [SIA]
GO

-- =============================================
-- Author:		Abraham
-- Create date: 2014-05-28
-- Description:	Eliminacion usp_RubOnline_Contrato_FormaPago_Consultar
-- =============================================
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]    Script Date: 28/05/2014 11:50:13 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]
GO






