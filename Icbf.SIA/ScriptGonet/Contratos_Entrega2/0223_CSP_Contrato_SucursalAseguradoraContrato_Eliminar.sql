USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Eliminar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 1:59:52 PM
-- Description:	Procedimiento almacenado que elimina un(a) SucursalAseguradoraContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Eliminar]
	@IDSucursalAseguradoraContrato INT
AS
BEGIN
	DELETE Contrato.SucursalAseguradoraContrato WHERE IDSucursalAseguradoraContrato = @IDSucursalAseguradoraContrato
END
