USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanComprass_Consultar]    Script Date: 10/17/2014 16:07:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanComprass_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanComprass_Consultar]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanComprass_Consultar]    Script Date: 10/17/2014 16:07:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==========================================================================================
-- Author:			Abraham Rivero Dom�nguez
-- Create date:		22/05/2014 1:36:08 PM
-- Description:		Procedimiento almacenado que consulta un(a) SolicitudModPlanCompras
-- ==========================================================================================
-- ==========================================================================================
-- Author:			Wilmer Alvarez
-- Create date:		22/07/2014 10:30 AM
-- Description:		Se adiciona el filtro por vigencia
-- ==========================================================================================
CREATE procedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanComprass_Consultar]
	@Vigencia varchar(4) = null,
	@NumeroConsecutivoPlanCompras int = null,
	@IdContrato int = null,
	@NumeroSolicitud int = null
as
begin

		select	SolPC.IdSolicitudModPlanCompra,
				SolPC.Justificacion,
				SolPC.NumeroSolicitud,
				SolPC.FechaSolicitud,
				SolPC.IdEstadoSolicitud,
				SolPC.IdUsuarioSolicitud,
				SolPC.IdPlanDeCompras,
				SolPC.UsuarioCrea,
				SolPC.FechaCrea,
				SolPC.UsuarioModifica,
				SolPC.FechaModifica,
				PC.IdContrato,
				PC.IDPlanDeCompras NumeroConsecPlanDeCompras,
				Usu.PrimerNombre + ' ' + Usu.SegundoNombre + ' ' + Usu.PrimerApellido + ' ' + Usu.SegundoApellido UsuarioSolicitud,
				PC.Vigencia,
				EdoSol.Descripcion EstadoSolicitud
		from	[CONTRATO].[SolicitudModPlanCompras] SolPC inner join 
				[CONTRATO].PlanDeComprasContratos PC on SolPC.IdPlanDeCompras = PC.IDPlanDeComprasContratos inner join 
				[SEG].[Usuario] Usu on SolPC.IdUsuarioSolicitud = Usu.IdUsuario inner join 
				[CONTRATO].[EstadoSolcitudModPlanCompras] EdoSol on SolPC.IdEstadoSolicitud = EdoSol.IdEstadoSolicitud
				INNER JOIN CONTRATO.Contrato C ON PC.IdContrato = C.IdContrato
		where	PC.IDPlanDeCompras = isnull(@NumeroConsecutivoPlanCompras, PC.IDPlanDeCompras)
				and PC.IdContrato = isnull(@IdContrato, PC.IdContrato) 
				and SolPC.IdSolicitudModPlanCompra = isnull(@NumeroSolicitud, SolPC.IdSolicitudModPlanCompra)
				and PC.Vigencia = isnull(@Vigencia,PC.Vigencia)
				AND C.IdEstadoContrato IS NOT NULL
end


GO


