USE [SIA]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/30/2014 12:23:44 PM
-- Descripcion: insert data CONTRATO.SupervisorInterventor
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[SupervisorInterventor]') AND type in (N'U'))
BEGIN
	if NOT EXISTS (select 1 from CONTRATO.TipoSuperInter where Codigo in ('01') )
	begin
	INSERT INTO CONTRATO.SupervisorInterventor(Codigo, Descripcion, UsuarioCrea, FechaCrea)
					  VALUES('01', 'Supervisor', 'Administrador', GETDATE())
	INSERT INTO CONTRATO.SupervisorInterventor(Codigo, Descripcion, UsuarioCrea, FechaCrea)
					  VALUES('02', 'Interventor', 'Administrador', GETDATE())
	end

RETURN
END



