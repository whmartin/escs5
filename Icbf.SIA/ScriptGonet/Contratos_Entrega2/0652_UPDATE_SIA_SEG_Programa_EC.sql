-- ====================================================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 24/10/2014
-- Description:	Modificaci�n de nombre de opci�n en el men� de contratos
-- solicitado por ICBF
-- ====================================================================
USE SIA

DECLARE @idModuloContratos INT
SELECT @idModuloContratos=IdModulo FROM SEG.Modulo WHERE NombreModulo = 'Contratos'

DECLARE @idPrograma INT
select @idPrograma=IdPrograma from SEG.Programa where NombrePrograma = 'Consulta supervisor Contrato'

update SEG.Programa
set NombrePrograma = 'Consultar Supervisor/Interventor Contrato'
where IdPrograma=@idPrograma


