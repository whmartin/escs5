USE [SIA]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_ModalidadSeleccion_IdentificadorCodigo')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 07/07/2014 10:43:28 a.m. ******/
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_IdentificadorCodigo]

END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 07/07/2014 10:43:28 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 10/06/2014
-- Description: Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos) de la modalidad de
-- selecci�n la informaci�n del mismo
-- =============================================
CREATE PROCEDURE usp_RubOnline_Contrato_ModalidadSeleccion_IdentificadorCodigo
	@IdModalidad INT = NULL,
	@CodigoModalidad NVARCHAR(30) = NULL
AS
BEGIN
	
	SELECT IdModalidad, CodigoModalidad, Nombre, Sigla, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica
	FROM CONTRATO.ModalidadSeleccion
	WHERE IdModalidad = ISNULL(@IdModalidad, IdModalidad) AND
	CodigoModalidad = ISNULL(@CodigoModalidad, CodigoModalidad)

END
GO
