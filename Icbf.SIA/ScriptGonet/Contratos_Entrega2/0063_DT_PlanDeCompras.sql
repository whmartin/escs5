USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Eliminacion table [CONTRATO].[PlanDeCompras]
-- =============================================

/****** Object:  Table [CONTRATO].[PlanDeCompras]    Script Date: 23/05/2014 16:46:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[PlanDeCompras]') AND type in (N'U'))
DROP TABLE [CONTRATO].[PlanDeCompras]
GO
