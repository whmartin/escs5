use SIA
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  09/06/2014 11:15
-- Description:	Insertar campos IdFormaPago [Contrato].[Contrato] 
-- =============================================

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'Contrato' 
           AND  COLUMN_NAME = 'IdFormaPago')

	ALTER TABLE [Contrato].[Contrato] 
	ADD
	   IdFormaPago int null
	
GO




