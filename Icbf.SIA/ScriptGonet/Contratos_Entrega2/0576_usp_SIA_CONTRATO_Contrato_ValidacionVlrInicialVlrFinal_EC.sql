USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 12/08/2014 11:04:30 a.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 12/08/2014 11:04:30 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 02/06/2014
-- Description:	
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal] 1108, 150, 1111
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
	@IdContratoConvMarco INT,
	@ValorInicialContConv DECIMAL, 
	@IdContrato INT

AS
BEGIN
	
	DECLARE @ValorFinalContratoConvMarco DECIMAL, @ValorInicialContratoConvMarco DECIMAL, @SumValorFinalContratos DECIMAL
	SELECT @ValorInicialContratoConvMarco=ValorInicialContrato 
	FROM CONTRATO.Contrato WHERE IdContrato = @IdContratoConvMarco

	SET @ValorFinalContratoConvMarco = @ValorInicialContratoConvMarco;
	print cast(@ValorFinalContratoConvMarco as varchar)

	SELECT @SumValorFinalContratos=ISNULL(SUM(ValorFinalContrato), 0) 
	FROM CONTRATO.Contrato WHERE FK_IdContrato = @IdContratoConvMarco AND IdContrato <> @IdContrato
	print cast(@SumValorFinalContratos as varchar)


	IF ((@ValorFinalContratoConvMarco + @SumValorFinalContratos) >= (@SumValorFinalContratos + @ValorInicialContConv))
	BEGIN

		SELECT @IdContratoConvMarco AS IdContrato
		RETURN 

	END


END


GO


