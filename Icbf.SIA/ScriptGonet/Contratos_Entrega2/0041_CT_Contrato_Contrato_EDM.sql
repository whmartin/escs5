USE [SIA]
GO


-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/7/2014 5:55:13 PM
-- Description:	modificacion table [CONTRATO].[Contrato]
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_NumeroProceso]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_NumeroProceso]
GO

/****** Object:  Table [CONTRATO].[Contrato]    Script Date: 26/05/2014 04:21:44 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[Contrato]') AND type in (N'U'))
DROP TABLE [CONTRATO].[Contrato]
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[Contrato]') AND type in (N'U'))
BEGIN
CREATE TABLE [CONTRATO].[Contrato](
	[IdContrato] [int] IDENTITY(1,1) NOT NULL,
	[IdNumeroProceso] [int] NULL,
	[UsuarioCrea] [nvarchar](250) NULL,
	[FechaCrea] [datetime] NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	[NumeroContrato] [nvarchar](50) NULL,
 CONSTRAINT [PK_Contrato] PRIMARY KEY CLUSTERED 
(
	[IdContrato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_NumeroProceso]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_NumeroProceso] FOREIGN KEY([IdNumeroProceso])
REFERENCES [CONTRATO].[NumeroProcesos] ([IdNumeroProceso])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_NumeroProceso]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato] CHECK CONSTRAINT [FK_Contrato_NumeroProceso]
GO


