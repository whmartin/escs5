USE SIA
GO

-- =============================================
-- Author:		Carlos Cardenas
-- Create date:  13/06/2014 6:41: PM
-- Description: crea columna suscritoen la tabla contrato
-- =============================================

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Contrato') AND NAME = ('Suscrito'))
BEGIN

ALTER TABLE CONTRATO.Contrato ADD Suscrito BIT 

alter table CONTRATO.Contrato add constraint DF_Contrato_Suscrito DEFAULT (0) FOR Suscrito

END
GO

