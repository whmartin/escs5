USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Eliminacion sp usp_RubOnline_Contrato_FormaPago_Eliminar
-- =============================================
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]    Script Date: 28/05/2014 12:01:00 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]
GO






