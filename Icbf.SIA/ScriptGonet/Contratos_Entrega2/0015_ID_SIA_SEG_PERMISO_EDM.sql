USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  12/05/2014 10:00 PM
-- Description:	Insertar Tabla permiso para visualizar en el men� de SIa la opci�n de Tablas Pram�tricas
-- =============================================
IF not exists (SELECT * FROM [SEG].[Programa] PROG
INNER JOIN SEG.Permiso PRM ON PRM.IdPrograma = PROG.IdPrograma
WHERE PROG.CodigoPrograma = 'Contratos/TablaParametrica')
begin
	declare @IdPrograma int = null

	select @IdPrograma = IdPrograma from [SEG].[Programa] where CodigoPrograma = 'Contratos/TablaParametrica'
	
	If @IdPrograma is not null
	begin

		INSERT [SEG].[Permiso] 
		([IdPrograma], [IdRol], [Insertar], [Modificar], [Eliminar], [Consultar], 
		[UsuarioCreacion], [FechaCreacion], [UsuarioModificacion], [FechaModificacion])
		VALUES (@IdPrograma, 1, 1, 1, 1, 1, N'Administrador', GETDATE(), NULL, NULL)
	
	End	   
END