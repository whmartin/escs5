USE [SIA]
GO

/********************************
2014-05-22
Creamos el SYNONYM InfoETLCDP 
jorge Vizcaino
********************************/

/****** Object:  Synonym [Ppto].[InfoETLCDP]    Script Date: 22/05/2014 11:45:23 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'InfoETLCDP' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[InfoETLCDP]
GO

/****** Object:  Synonym [Ppto].[InfoETLCDP]    Script Date: 22/05/2014 11:45:23 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'InfoETLCDP' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[InfoETLCDP] FOR [NMF].[Ppto].[InfoETLCDP]
GO


