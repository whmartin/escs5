USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_NumeroProcesoss_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesoss_Consultar]
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/7/2014 5:55:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) NumeroProcesos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesoss_Consultar]
	@NumeroProceso INT = NULL,@Inactivo BIT = NULL,@NumeroProcesoGenerado NVARCHAR(30) = NULL,@IdModalidadSeleccion INT = NULL,@IdRegional INT = NULL,@IdVigencia INT = NULL
AS
BEGIN
	SELECT
		IdNumeroProceso,
		NumeroProceso,
		Inactivo,
		NumeroProcesoGenerado,
		IdModalidadSeleccion,
		IdRegional,
		IdVigencia,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].[NumeroProcesos]
	WHERE NumeroProceso =
		CASE
			WHEN @NumeroProceso IS NULL THEN NumeroProceso ELSE @NumeroProceso
		END
	AND Inactivo =
		CASE
			WHEN @Inactivo IS NULL THEN Inactivo ELSE @Inactivo
		END
	AND NumeroProcesoGenerado LIKE CASE
		WHEN @NumeroProcesoGenerado IS NULL THEN NumeroProcesoGenerado ELSE @NumeroProcesoGenerado + '%'
	END
	AND IdModalidadSeleccion =
		CASE
			WHEN @IdModalidadSeleccion IS NULL THEN IdModalidadSeleccion ELSE @IdModalidadSeleccion
		END
	AND IdRegional =
		CASE
			WHEN @IdRegional IS NULL THEN IdRegional ELSE @IdRegional
		END
	AND IdVigencia =
		CASE
			WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia
		END
END
