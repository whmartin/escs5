USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion]    Script Date: 22/06/2014 04:59:35 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_Contrato_Actualizacion')
BEGIN
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion]    Script Date: 22/06/2014 04:59:35 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 22/06/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Actualizacion] 
	@IdContrato INT = NULL,
	@IdEstadoContrato INT = NULL, 
	@IdTipoContratoAsociado INT = NULL,
	@ConvenioMarco BIT = NULL, 
	@ConvenioAdhesion BIT = NULL,
	@FK_IdContrato INT = NULL,
	@IdCategoriaContrato INT = NULL,
	@IdTipoContrato INT = NULL,
	@IdModalidadAcademica VARCHAR(30) = NULL,
	@IdProfesion VARCHAR(30) = NULL, 
	@IdModalidadSeleccion INT = NULL,
	@IdNumeroProceso INT = NULL,
	@FechaAdjudicacionProceso DATETIME = NULL,
	@ActaDeInicio BIT = NULL, 
	@ManejaAporte BIT = NULL, 
	@ManejaRecurso BIT = NULL,
	@IdRegimenContratacion INT = NULL,
	@IdConsecutivoPlanCompras INT = NULL,
	@ObjetoDelContrato VARCHAR(500) = NULL,
	@AlcanceObjetoDelContrato VARCHAR(4000) = NULL,
	@ValorInicialContrato DECIMAL(27, 2) = NULL,
	@FechaInicioEjecucion DATETIME = NULL,
	@FechaFinalizacionIniciaContrato DATETIME = NULL,
	@FechaFinalTerminacionContrato DATETIME = NULL,
	@ManejaVigenciaFuturas BIT = NULL,
	@IdVigenciaInicial INT = NULL,
	@IdVigenciaFinal INT = NULL,
	@IdFormaPago INT = NULL,
	@DatosAdicionaleslugarEjecucion VARCHAR(800) = NULL, 
	@UsuarioModifica NVARCHAR(250) = NULL
AS
BEGIN
	
	UPDATE Contrato.Contrato  
	SET
	IdContratoAsociado = @IdTipoContratoAsociado,
	IdEstadoContrato = @IdEstadoContrato,
	ConvenioMarco = @ConvenioMarco, 
	ConvenioAdhesion = @ConvenioAdhesion,
	FK_IdContrato = @FK_IdContrato,
	IdCategoriaContrato = @IdCategoriaContrato,
	IdTipoContrato = @IdTipoContrato,
	IdModalidadAcademica = @IdModalidadAcademica,
	IdProfesion = @IdProfesion, 
	IdModalidadSeleccion = @IdModalidadSeleccion,
	IdNumeroProceso = @IdNumeroProceso,
	FechaAdjudicacionDelProceso = @FechaAdjudicacionProceso,
	ActaDeInicio = @ActaDeInicio, 
	ManejaAporte = @ManejaAporte, 
	ManejaRecurso = @ManejaRecurso,
	ConsecutivoPlanCompasAsociado = @IdConsecutivoPlanCompras,
	IdRegimenContratacion = @IdRegimenContratacion,
	ObjetoDelContrato = @ObjetoDelContrato,
	AlcanceObjetoDelContrato = @AlcanceObjetoDelContrato,
	ValorInicialContrato = @ValorInicialContrato,
	ValorFinalContrato = @ValorInicialContrato,
	FechaInicioEjecucion = @FechaInicioEjecucion,
	FechaFinalizacionIniciaContrato = @FechaFinalizacionIniciaContrato,
	FechaFinalTerminacionContrato = @FechaFinalTerminacionContrato,
	ManejaVigenciasFuturas = @ManejaVigenciaFuturas,
	IdVigenciaInicial = @IdVigenciaInicial,
	IdVigenciaFinal = @IdVigenciaFinal,
	IdFormaPago = @IdFormaPago,
	DatosAdicionaleslugarEjecucion = @DatosAdicionaleslugarEjecucion
	,UsuarioModifica = @UsuarioModifica
	,FechaModifica = GETDATE()
	WHERE IdContrato = @IdContrato

END

GO


