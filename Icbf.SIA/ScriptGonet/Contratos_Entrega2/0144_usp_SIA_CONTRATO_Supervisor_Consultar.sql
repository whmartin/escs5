use SIA

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Supervisor_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Supervisor_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero
-- Create date: 2014-06-03
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Supervisor_Consultar]
	
	@IDSupervisorIntervContrato INT
	
AS
BEGIN
	SELECT
		Em.ID_Ident,
		desc_ide AS TipoIdentificacion,
		Em.num_iden AS NumeroIdentificacion,
		Em.desc_vin AS TipoVinculacion,
		(Em.nom_emp1 + ' ' + Em.nom_emp2 + ' ' + Em.ape_emp1 + ' ' + Em.ape_emp2) AS NombreCompleto,
		Em.nom_emp1 AS PrimerNombre,
		Em.nom_emp2 AS SegundoNombre,
		Em.ape_emp1 AS PrimerApellido,
		Em.ape_emp2 AS SegundoApellido,
		Em.ID_regio,
		Em.regional,
		Em.nom_depe AS Dependencia,
		Em.direccion,
		Em.telefono,
		ISNULL(Em.eee_mail, '') AS CorreoE,
		Em.Desc_car AS Cargo,
		Em.num_iden AS IdEmpleado
	FROM [KACTUS].[KPRODII].[dbo].[Da_Emple] Em
	INNER JOIN [CONTRATO].[SupervisorInterContrato] SI
		ON SI.IDEmpleadosSupervisor = Em.num_iden
	WHERE ID_Ident = @IDSupervisorIntervContrato
END
GO