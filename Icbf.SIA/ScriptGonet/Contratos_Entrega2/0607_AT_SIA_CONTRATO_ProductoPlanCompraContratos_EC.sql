USE [SIA]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[ProductoPlanCompraContratos]') AND type in (N'U'))
BEGIN
	
	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ProductoPlanCompraContratos' AND table_schema = 'CONTRATO' 
	AND column_name = 'ValorProducto')
	BEGIN 
		ALTER TABLE CONTRATO.ProductoPlanCompraContratos
		ADD ValorProducto NUMERIC(30, 2) NULL;
	END
	
	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ProductoPlanCompraContratos' AND table_schema = 'CONTRATO' 
	AND column_name = 'Tiempo')
	BEGIN 
		ALTER TABLE CONTRATO.ProductoPlanCompraContratos
		ADD Tiempo NUMERIC(30, 2) NULL;
	END
	
	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ProductoPlanCompraContratos' AND table_schema = 'CONTRATO' 
	AND column_name = 'UnidadTiempo')
	BEGIN 
		ALTER TABLE CONTRATO.ProductoPlanCompraContratos
		ADD UnidadTiempo VARCHAR(250) NULL;
	END
	ELSE
	BEGIN
		ALTER TABLE CONTRATO.ProductoPlanCompraContratos
		ALTER COLUMN UnidadTiempo VARCHAR(250) null;
	END
	
	IF NOT EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ProductoPlanCompraContratos' AND table_schema = 'CONTRATO' 
	AND column_name = 'IdUnidadTiempo')
	BEGIN 
		
		ALTER TABLE CONTRATO.ProductoPlanCompraContratos
		ADD IdUnidadTiempo INT NULL;
	END

END