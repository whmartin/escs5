USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Modificar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 1:59:52 PM
-- Description:	Procedimiento almacenado que actualiza un(a) SucursalAseguradoraContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Modificar]
		@IDSucursalAseguradoraContrato INT,	@IDDepartamento INT,	@IDMunicipio INT,	@Nombre NVARCHAR(160),	@IDZona INT,	@DireccionNotificacion NVARCHAR(80),	@CorreoElectronico NVARCHAR(80),	@Indicativo NVARCHAR(3),	@Telefono NVARCHAR(40),	@Extension NVARCHAR(8),	@Celular NVARCHAR(40),	@IDEntidadProvOferente INT,	@CodigoSucursal INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.SucursalAseguradoraContrato SET IDDepartamento = @IDDepartamento, IDMunicipio = @IDMunicipio, Nombre = @Nombre, IDZona = @IDZona, DireccionNotificacion = @DireccionNotificacion, CorreoElectronico = @CorreoElectronico, Indicativo = @Indicativo, Telefono = @Telefono, Extension = @Extension, Celular = @Celular, IDEntidadProvOferente = @IDEntidadProvOferente, CodigoSucursal = @CodigoSucursal, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IDSucursalAseguradoraContrato = @IDSucursalAseguradoraContrato
END
