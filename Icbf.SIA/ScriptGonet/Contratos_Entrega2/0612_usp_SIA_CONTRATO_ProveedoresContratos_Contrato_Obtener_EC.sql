USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]    Script Date: 09/01/2014 10:03:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]    Script Date: 09/01/2014 10:03:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Gonet/EfrainDiazMejia
-- Create date: 01/08/2014
-- Description:	Se agreg� el campo 
-- =============================================
-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 30/06/2014
-- Description:	Obtiene la informacion de contratistas relacionados con un contrato, 
-- basado en usp_SIA_CONTRATO_Proveedores_Contratistas_Consultar
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener] 12
-- [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener] 64
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener]
	@IdContrato INT, 
	@IntegrantesUnionTemporal INT = 0
AS
BEGIN
	
	IF @IntegrantesUnionTemporal = 0
	BEGIN

		SELECT PC.IdProveedoresContratos, T.IDTERCERO /*IdProveedores*/ AS IdTercero, TP.NombreTipoPersona AS TipoPersona, 
		TD.CodDocumento AS TipoIdentificacion, T.NUMEROIDENTIFICACION AS NumeroIdentificacion, 
		(CASE TP.CodigoTipoPersona  
			WHEN '001' THEN (T.PRIMERNOMBRE + (CASE ISNULL(T.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDONOMBRE + ' ' END) + T.PRIMERAPELLIDO + (CASE ISNULL(T.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDOAPELLIDO END))
			ELSE T.RAZONSOCIAL END) AS InformacionContratista, 
		Trep.NUMEROIDENTIFICACION AS IdentificacionRepLegal, 
		--.Trep.RAZONSOCIAL AS Representante, 
		--(CASE TPrep.CodigoTipoPersona  
		--	WHEN '001' THEN (Trep.PRIMERNOMBRE + (CASE ISNULL(Trep.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + Trep.SEGUNDONOMBRE + ' ' END) + Trep.PRIMERAPELLIDO + (CASE ISNULL(Trep.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + Trep.SEGUNDOAPELLIDO END))
		--	ELSE Trep.RAZONSOCIAL END) AS Representante,
		(Trep.PRIMERNOMBRE + (CASE ISNULL(Trep.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + Trep.SEGUNDONOMBRE + ' ' END) + Trep.PRIMERAPELLIDO + (CASE ISNULL(Trep.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + Trep.SEGUNDOAPELLIDO END)) AS Representante,
		NULL AS PorcentajeParticipacion, NULL AS NumeroIdentificacionIntegrante, OTD.NomTipoDocumento AS TipoDocumentoRepresentante,
		EPO.IdEntidad
		FROM CONTRATO.ProveedoresContratos PC
		INNER JOIN [PROVEEDOR].[EntidadProvOferente] EPO ON EPO.IDENTIDAD = PC.IdProveedores
		INNER JOIN Oferente.TERCERO T ON EPO.IDTERCERO = T.IdTercero
		INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona = TP.IdTipoPersona 
		INNER JOIN Global.TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento 
		INNER JOIN Proveedor.InfoAdminEntidad IAE ON IAE.IdEntidad = EPO.IdEntidad
		LEFT JOIN Oferente.Tercero Trep ON Trep.IdTercero = IAE.IdRepLegal
		LEFT JOIN Oferente.GlobalTiposDocumentos OTD ON OTD.IdTipoDocumento = Trep.IDTIPODOCIDENTIFICA
		LEFT JOIN Oferente.TipoPersona TPrep ON Trep.IdTipoPersona = TPrep.IdTipoPersona 
		WHERE IdContrato = @IdContrato
		GROUP BY PC.IdProveedoresContratos, T.IDTERCERO , TP.NombreTipoPersona , 
		TD.CodDocumento , T.NUMEROIDENTIFICACION , 
		(CASE TP.CodigoTipoPersona  
			WHEN '001' THEN (T.PRIMERNOMBRE + (CASE ISNULL(T.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDONOMBRE + ' ' END) + T.PRIMERAPELLIDO + (CASE ISNULL(T.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDOAPELLIDO END))
			ELSE T.RAZONSOCIAL END), 
		Trep.NUMEROIDENTIFICACION, 
		--.Trep.RAZONSOCIAL AS Representante, 
		--(CASE TPrep.CodigoTipoPersona  
		--	WHEN '001' THEN (Trep.PRIMERNOMBRE + (CASE ISNULL(Trep.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + Trep.SEGUNDONOMBRE + ' ' END) + Trep.PRIMERAPELLIDO + (CASE ISNULL(Trep.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + Trep.SEGUNDOAPELLIDO END))
		--	ELSE Trep.RAZONSOCIAL END),
		(Trep.PRIMERNOMBRE + (CASE ISNULL(Trep.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + Trep.SEGUNDONOMBRE + ' ' END) + Trep.PRIMERAPELLIDO + (CASE ISNULL(Trep.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + Trep.SEGUNDOAPELLIDO END)),
		OTD.NomTipoDocumento,
		EPO.IdEntidad

	END
	ELSE
	BEGIN
		PRINT 'FALTA CONSULTA'
		-- CONSULTA TEMPORAL INTEGRANTES UNION TEMPORAL

		SELECT DISTINCT NULL AS IdProveedoresContratos
		,NULL AS IdTercero
		,PER.NombreTipoPersona AS TipoPersona
		,DOCS.CodDocumento AS TipoIdentificacion
		,(SELECT NUMEROIDENTIFICACION FROM Oferente.TERCERO WHERE IDTERCERO=TCont.IdTercero/*@IdTercero*/) AS NumeroIdentificacion
		,T.NUMEROIDENTIFICACION AS NumeroIdentificacionIntegrante
		,CASE WHEN T.IdTipoPersona=1 THEN (T.PRIMERNOMBRE+' '+ T.SEGUNDONOMBRE+' '+T.PRIMERAPELLIDO+' '+T.SEGUNDOAPELLIDO) ELSE T.RazonSocial END AS InformacionContratista
		,I.PorcentajeParticipacion AS PorcentajeParticipacion,
		NULL AS TipoDocumentoRepresentante, NULL AS IdentificacionRepLegal, NULL AS Representante, NULL AS IdEntidad
		FROM CONTRATO.ProveedoresContratos PC
		INNER JOIN [PROVEEDOR].[EntidadProvOferente] EPO ON EPO.IDENTIDAD = PC.IdProveedores
		INNER JOIN Oferente.TERCERO TCont ON EPO.IDTERCERO = TCont.IdTercero
		INNER JOIN Oferente.TipoPersona TP ON TCont.IdTipoPersona = TP.IdTipoPersona 
		INNER JOIN PROVEEDOR.EntidadProvOferente PROV ON PROV.IdTercero = TCont.IdTercero 
		INNER JOIN PROVEEDOR.Integrantes I ON PROV.IdEntidad=I.IdEntidad
		INNER JOIN Oferente.TERCERO T ON I.NumeroIdentificacion=T.NUMEROIDENTIFICACION
		INNER JOIN Oferente.TipoPersona PER ON T.IdTipoPersona=PER.IdTipoPersona
		INNER JOIN Oferente.GlobalTiposDocumentos DOCS ON T.IDTIPODOCIDENTIFICA=DOCS.IdTipoDocumento
		WHERE PC.IdContrato = @IdContrato/*2238*/ AND TP.CodigoTipoPersona IN ('003', '004')
		ORDER BY NumeroIdentificacion, NumeroIdentificacionIntegrante

	END



END


GO


