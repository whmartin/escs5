USE [SIA]
/***********************************************
Creado Por: Carlos Andr�s C�rdenas
El dia : 2013-08-26
Modificado Por: Abraham Rivero Dom�nguez
El d�a : 2013-10-21
Permite : Crear el programa Parametrizaci�n Contable en inversiones en el m�dulo de Parametrizador 
y asignarle permisos al administrador
***********************************************/
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Contratos/SolicitudModPlanCompras' )
begin
      
	    

      insert INTO [SEG].[Programa]([IdModulo],
					[NombrePrograma],
					[CodigoPrograma],
					[Posicion],
					[Estado],
					[UsuarioCreacion],
					[FechaCreacion],
					[UsuarioModificacion],[FechaModificacion],[VisibleMenu],[generaLog],[Padre])
      SELECT IdModulo,'Solicitud de modificaci�n contractual','Contratos/SolicitudModPlanCompras',1,1,
      'Administrador',GETDATE(),NULL,NULL,1,1,NULL
      from SEG.Modulo
      where NombreModulo = 'Contratos'    
      
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
