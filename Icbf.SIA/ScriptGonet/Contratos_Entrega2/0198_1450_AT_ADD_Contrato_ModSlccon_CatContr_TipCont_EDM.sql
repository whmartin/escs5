USE [SIA]
GO

-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  07/07/2014 14:45
-- Description:	Insertar campos en las tablas [CONTRATO].[ModalidadSeleccion], [CONTRATO].[CategoriaContrato], [CONTRATO].[TipoContrato]
-- =============================================

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'ModalidadSeleccion' 
           AND  COLUMN_NAME = 'CodigoModalidad')

	ALTER TABLE [CONTRATO].[ModalidadSeleccion]
	ADD
	   [CodigoModalidad] [nvarchar](30) NULL
	
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'CategoriaContrato' 
           AND  COLUMN_NAME = 'CodigoCategoriaContrato')

	ALTER TABLE [CONTRATO].[CategoriaContrato]
	ADD
	   [CodigoCategoriaContrato] [nvarchar](30) NULL
	
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'TipoContrato' 
           AND  COLUMN_NAME = 'CodigoTipoContrato')

	ALTER TABLE [CONTRATO].[TipoContrato]
	ADD
	  [CodigoTipoContrato] [nvarchar](30) NULL
	
GO
