USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar]    Script Date: 15/07/2014 05:06:25 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar]    Script Date: 15/07/2014 05:06:25 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/27/2014 6:43:39 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ConsecutivoContratoRegionales
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar]
		@IDConsecutivoContratoRegional INT,	@IdRegional INT,	@IdVigencia INT,	@Consecutivo NUMERIC(30), @UsuarioModifica NVARCHAR(250)
AS
BEGIN

DECLARE @ValMaxContrato NUMERIC(30)

	SELECT @ValMaxContrato = MAX(ISNULL(ConsecutivoSuscrito,0))  from CONTRATO.Contrato 
	WHERE IdRegionalContrato = @IdRegional
	AND IdVigenciaInicial = @IdVigencia
	group by IdVigenciaInicial,IdRegionalContrato

	IF(@Consecutivo <= @ValMaxContrato)
	BEGIN
		RAISERROR('El n�mero digitado no puede ser menor al de un contrato suscrito',16,1)
	    RETURN
	END
	ELSE
	BEGIN
		UPDATE CONTRATO.ConsecutivoContratoRegionales 
		SET IdRegional = @IdRegional, IdVigencia = @IdVigencia, Consecutivo = @Consecutivo, 
		UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE()
		WHERE IDConsecutivoContratoRegional = @IDConsecutivoContratoRegional
	END
END



