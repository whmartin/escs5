USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contratoss_Consultar]    Script Date: 11/07/2014 12:00:59 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Contratoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contratoss_Consultar]
GO

-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  6/16/2014 4:52:44 PM
-- Description:	Procedimiento almacenado que consulta un(a) Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contratoss_Consultar]
	@FechaRegistroSistemaDesde DATETIME = NULL,
	@FechaRegistroSistemaHasta DATETIME = NULL,
	@IdContrato INT = NULL,
	@NumeroContrato NVARCHAR(30) = NULL,
	@NumeroProceso NVARCHAR(30) = NULL,
	@VigenciaFiscalinicial INT = NULL,
	@IDRegional INT = NULL,
	@IDModalidadSeleccion INT = NULL,
	@IDCategoriaContrato INT = NULL,
	@IDTipoContrato INT = NULL,
	@IDEstadoContraro INT = NULL
AS
BEGIN

 SELECT distinct C.IdContrato, C.NumeroContrato, 
  C.IdNumeroProceso, C.IdVigenciaInicial, C.IdRegionalContrato, 
  C.IdModalidadSeleccion, C.IdCategoriaContrato, 
 C.IdTipoContrato, C.IdEstadoContrato, C.UsuarioCrea, 
 C.FechaCrea, C.UsuarioModifica, C.FechaModifica,
 Ltrim(rtrim(R.CodigoRegional)) +' - '+Ltrim(rtrim(R.NombreRegional)) as Regional,
 Ltrim(rtrim(MS.CodigoModalidad)) + ' - ' + Ltrim(rtrim(MS.Nombre)) as ModalidadSeleccion,
 Ltrim(rtrim(CC.CodigoCategoriaContrato)) + ' - ' + Ltrim(rtrim(CC.NombreCategoriaContrato)) AS CategoriaContrato,
 Ltrim(rtrim(TC.CodigoTipoContrato)) + ' - ' + Ltrim(rtrim(TC.NombreTipoContrato)) AS TipoContrato,
 Ltrim(rtrim(EC.Descripcion)) AS EstadoContrato,
 V.AcnoVigencia As Vigencia,
 NP.NumeroProcesoGenerado As numeroProceso,
 C.FechaAdjudicacionDelProceso
 FROM CONTRATO.Contrato C
 left outer join DIV.Regional R on C.IDRegionalContrato = R.IdRegional
 left outer join CONTRATO.ModalidadSeleccion MS on MS.IdModalidad = C.IdModalidadSeleccion
 left outer join CONTRATO.CategoriaContrato CC on CC.IdCategoriaContrato = C.IdCategoriaContrato
 left outer join CONTRATO.TipoContrato TC on TC.IdTipoContrato = C.IdTipoContrato
 left outer join Contrato.EstadoContrato EC on EC.IDEstadoContrato = C.IDEstadoContrato
 left outer join Global.Vigencia V on V.IdVigencia = C.IdVigenciaInicial
 left outer join CONTRATO.NumeroProcesos NP on NP.IdNumeroProceso = C.IdNumeroProceso
 WHERE C.FechaCrea >= CASE WHEN @FechaRegistroSistemaDesde IS NULL THEN C.FechaCrea ELSE @FechaRegistroSistemaDesde END 
 AND C.FechaCrea <= CASE WHEN @FechaRegistroSistemaHasta IS NULL THEN C.FechaCrea ELSE @FechaRegistroSistemaHasta END 
 AND ISNULL(C.IdContrato, '') = CASE WHEN @IdContrato IS NULL THEN ISNULL(C.IdContrato, '') ELSE @IdContrato END 
 AND ISNULL(C.NumeroContrato, '') = CASE WHEN @NumeroContrato IS NULL THEN ISNULL(C.NumeroContrato, '') ELSE @NumeroContrato END 
 AND ISNULL(C.IdNumeroProceso, '') = CASE WHEN @NumeroProceso IS NULL THEN ISNULL(C.IdNumeroProceso, '') ELSE @NumeroProceso END 
 AND ISNULL(C.IdVigenciaInicial, '') = CASE WHEN @VigenciaFiscalinicial IS NULL THEN ISNULL(C.IdVigenciaInicial, '') ELSE @VigenciaFiscalinicial END 
 AND ISNULL(C.IDRegionalContrato, '') = CASE WHEN @IDRegional IS NULL THEN ISNULL(C.IDRegionalContrato, '') ELSE @IDRegional END 
 AND ISNULL(C.IDModalidadSeleccion, '') = CASE WHEN @IDModalidadSeleccion IS NULL THEN ISNULL(C.IDModalidadSeleccion, '') ELSE @IDModalidadSeleccion END 
 AND ISNULL(C.IDCategoriaContrato, '') = CASE WHEN @IDCategoriaContrato IS NULL THEN ISNULL(C.IDCategoriaContrato, '') ELSE @IDCategoriaContrato END 
 AND ISNULL(C.IDTipoContrato, '') = CASE WHEN @IDTipoContrato IS NULL THEN ISNULL(C.IDTipoContrato, '') ELSE @IDTipoContrato END 
 AND ISNULL(C.IDEstadoContrato, '') = CASE WHEN @IDEstadoContraro IS NULL THEN ISNULL(C.IDEstadoContrato, '') ELSE @IDEstadoContraro END
END

