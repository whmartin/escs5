USE SIA
GO

-- =============================================
-- Author:		Gonet/Emilio Calapiña
-- Create date: 08/07/2014
-- Description:	crea un tipo producto plan compra contratos
-- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'ProductoPlanCompraContratos' AND ss.name = N'dbo')
	DROP TYPE dbo.ProductoPlanCompraContratos
GO

CREATE TYPE dbo.ProductoPlanCompraContratos AS TABLE
(
 CodigoProducto					NUMERIC(18,0) NOT NULL, --CODIGO Producto Pacco
 CantidadCupos					NUMERIC(18,2) NULL, --Cantidad de Cupos Pacco
 IdDetalleObjeto				NVARCHAR(150) NULL --IdDetalleObjeto Pacco 
)
GO
