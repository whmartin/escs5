USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]    Script Date: 09/05/2014 06:06:57 p.m. ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Global_Vigencia_sConsultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]    Script Date: 09/05/2014 06:06:57 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		GONET/Efrain Diaz Mejia
-- Create date:  09/05/2014 18:20:03
-- Description:	Procedimiento almacenado que consulta un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]
	@AcnoVigencia INT = NULL,@Activo NVARCHAR(1) = NULL
AS
BEGIN
	SELECT
		IdVigencia,
		AcnoVigencia,
		Activo,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Global].[Vigencia]
	WHERE AcnoVigencia =
		CASE
			WHEN @AcnoVigencia IS NULL THEN AcnoVigencia ELSE @AcnoVigencia
		END
	AND Activo =
		CASE
			WHEN @Activo IS NULL THEN Activo ELSE @Activo
		END
END
