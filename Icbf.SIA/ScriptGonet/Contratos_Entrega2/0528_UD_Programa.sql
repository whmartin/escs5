use SIA
GO

/***********************************
2014-07-24
Ordernar menu
***********************************/
UPDATE SEG.Programa SET Posicion = 6
where CodigoPrograma = 'Contratos/TablaParametrica'


UPDATE SEG.Programa SET Posicion = 5
where CodigoPrograma = 'Contratos/Contratos'


UPDATE SEG.Programa SET Posicion = 4
where CodigoPrograma = 'Contratos/SuscripcionContrato'


UPDATE SEG.Programa SET Posicion = 3
where CodigoPrograma = 'Contratos/AsociarRpContrato'

UPDATE SEG.Programa SET Posicion = 2
where CodigoPrograma = 'Contratos/ConsultarSuperInterContrato'



UPDATE SEG.Programa SET Posicion = 1
where CodigoPrograma = 'Contratos/SolicitudModPlanCompras'