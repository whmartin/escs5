USE [SIA]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_InfoGarantia_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_InfoGarantia_Consultar]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz mejia
-- Create date:  31/07/2014 
-- Description:	Procedimiento almacenado que consulta  la información de las garantias
-- =============================================

CREATE PROCEDURE [dbo].[usp_SIA_Contrato_InfoGarantia_Consultar]
	@IdContrato INT
AS
BEGIN

	 SELECT 
	 G.IdContrato,
	 G.IDGarantia, 
	 G.IDTipoGarantia, 
	 TG.NombreTipoGarantia,
	 G.NumeroGarantia, 
	 G.FechaInicioGarantia, 
	 G.ValorGarantia,
	 G.UsuarioCrea, 
	 G.FechaCrea, 
	 G.UsuarioModifica, 
     G.FechaModifica,
	 G.Estado, 
     G.IDEstadosGarantias,
	 EG.CodigoEstadoGarantia,
     EG.DescripcionEstadoGarantia,
	 G.FechaAprobacionGarantia,
	 G.FechaCertificacionGarantia,
	 G.IdSucursalAseguradoraContrato

  FROM Contrato.Garantia G
  INNER JOIN CONTRATO.TipoGarantia TG on TG.IdTipoGarantia = G.IDTipoGarantia
  left outer JOIN CONTRATO.EstadosGarantias EG on EG.IDEstadosGarantias = G.IDEstadosGarantias
  Where  G.IdContrato = @IdContrato
	
	select * FROM Contrato.Garantia
END