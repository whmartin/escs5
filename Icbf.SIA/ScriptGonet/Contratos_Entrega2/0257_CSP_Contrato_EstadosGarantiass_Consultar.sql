USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_EstadosGarantiass_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_EstadosGarantiass_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/25/2014 5:10:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_EstadosGarantiass_Consultar]
	@CodigoEstadoGarantia NVARCHAR(8) = NULL,@DescripcionEstadoGarantia NVARCHAR(80) = NULL
AS
BEGIN
 SELECT IDEstadosGarantias, CodigoEstadoGarantia, DescripcionEstadoGarantia, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[EstadosGarantias] WHERE CodigoEstadoGarantia = CASE WHEN @CodigoEstadoGarantia IS NULL THEN CodigoEstadoGarantia ELSE @CodigoEstadoGarantia END AND DescripcionEstadoGarantia = CASE WHEN @DescripcionEstadoGarantia IS NULL THEN DescripcionEstadoGarantia ELSE @DescripcionEstadoGarantia END
END
