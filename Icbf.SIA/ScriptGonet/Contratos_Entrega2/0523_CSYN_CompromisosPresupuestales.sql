USE [SIA]
GO

/****** Object:  Synonym [Ppto].[CompromisosPresupuestales]    Script Date: 22/07/2014 15:43:53 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CompromisosPresupuestales' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[CompromisosPresupuestales]
GO

/****** Object:  Synonym [Ppto].[CompromisosPresupuestales]    Script Date: 22/07/2014 15:43:53 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CompromisosPresupuestales' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[CompromisosPresupuestales] FOR [NMF].[Ppto].[CompromisosPresupuestales]
GO


