USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Insertar]    Script Date: 01/07/2014 16:01:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoFormaPago_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Insertar]
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoFormaPago_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Insertar]
		@IdTipoFormaPago INT OUTPUT, 	@NombreTipoFormaPago NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN

	INSERT INTO Contrato.TipoFormaPago(NombreTipoFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea,CodigoTipoFormaPago)
					  VALUES(@NombreTipoFormaPago, @Descripcion, @Estado, @UsuarioCrea, GETDATE(),SUBSTRING(@NombreTipoFormaPago,0,4))
	SELECT @IdTipoFormaPago = @@IDENTITY 		
END


