USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Contrato_Profesiones_Consultar]    Script Date: 13/06/2014 10:06:16 a.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_KACTUS_KPRODII_Contrato_Profesiones_Consultar')
BEGIN
DROP PROCEDURE [dbo].[usp_KACTUS_KPRODII_Contrato_Profesiones_Consultar]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Contrato_Profesiones_Consultar]    Script Date: 13/06/2014 10:06:16 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 13/06/2014
-- Description:	Obtiene las profesiones por categoria registradas en Kactus
-- =============================================
CREATE PROCEDURE [dbo].[usp_KACTUS_KPRODII_Contrato_Profesiones_Consultar] 
	@ModalidadAcademica VARCHAR(30), @Descripcion VARCHAR(100) = NULL, @Codigo VARCHAR(30) = NULL, @Estado BIT = NULL
AS
BEGIN
	
	SELECT ID_Profe AS Codigo, Des_Prof AS Descripcion FROM [KACTUS].[KPRODII].[dbo].[Da_Profe]
	WHERE ID_Profe = ISNULL(@Codigo, ID_Profe)
	AND Des_Prof = ISNULL(@Descripcion, Des_Prof)
	AND ID_Modal = ISNULL(@ModalidadAcademica, ID_Modal)
	ORDER BY Descripcion ASC

END

GO


