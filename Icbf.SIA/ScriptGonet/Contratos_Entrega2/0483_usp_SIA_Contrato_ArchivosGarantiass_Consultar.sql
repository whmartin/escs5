USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ArchivosGarantiass_Consultar]    Script Date: 16/07/2014 11:53:09 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ArchivosGarantiass_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantiass_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ArchivosGarantiass_Consultar]    Script Date: 16/07/2014 11:53:09 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  6/20/2014 10:24:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantiass_Consultar]
	@IDArchivo INT = NULL,@IDGarantia INT = NULL
AS
BEGIN
 SELECT archgar.IDArchivosGarantias, archgar.IDArchivo,A.NombreArchivo,A.NombreArchivoOri, 
 archgar.IDGarantia, archgar.UsuarioCrea, archgar.FechaCrea,
 archgar.UsuarioModifica, archgar.FechaModifica 
 FROM [Contrato].[ArchivosGarantias] archgar
 INNER JOIN Estructura.Archivo A ON archgar.IDArchivo = A.IdArchivo
 WHERE archgar.IDArchivo = CASE WHEN @IDArchivo IS NULL THEN archgar.IDArchivo ELSE @IDArchivo END 
 AND archgar.IDGarantia = CASE WHEN @IDGarantia IS NULL THEN archgar.IDGarantia ELSE @IDGarantia END
END


