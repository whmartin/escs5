use SIA

-- =============================================
-- Author:		Gonet
-- Create date:  2014-07-09
-- Description:	Limpieza de las tabla archivos
-- =============================================


IF EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('Estructura.Archivo') AND NAME = ('nombretabla'))
BEGIN
	alter table Estructura.Archivo drop column nombretabla  
END


IF EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('Estructura.Archivo') AND NAME = ('IdTabla'))
BEGIN
	alter table Estructura.Archivo drop column IdTabla
END

DELETE FROM Estructura.ColumnaDepurador
DELETE from Estructura.Columna
DELETE FROM Estructura.FormatoArchivo
DELETE FROM Estructura.TipoEstructura
DELETE FROM Estructura.Modalidad

IF NOT exists(select 1 from Estructura.Modalidad)
BEGIN 
	INSERT into Estructura.Modalidad (NombreModalidad,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica,
										GrupoModalidad) 
									VALUES ('Archivos PDF','Administrador',GETDATE(),NULL,NULL,NULL)

		INSERT into Estructura.Modalidad (NombreModalidad,
										UsuarioCrea,
										FechaCrea,
										UsuarioModifica,
										FechaModifica,
										GrupoModalidad) 
									VALUES ('Archivos JPG','Administrador',GETDATE(),NULL,NULL,NULL)
END

IF NOT exists(select 1 from Estructura.TipoEstructura)
BEGIN 
	INSERT into Estructura.TipoEstructura (NombreEstructura,
											UsuarioCrea,
											FechaCrea,
											UsuarioModifica,
											FechaModifica,
											GrupoEstructura) 
									VALUES ('Contratos','Administrador',GETDATE(),NULL,NULL,NULL)
END

IF NOT exists(select 1 from Estructura.FormatoArchivo)
BEGIN 

	DECLARE @modalidadpdf int,
			@modalidadjpg int


	select @modalidadpdf = IdModalidad from Estructura.Modalidad
	where NombreModalidad = 'Archivos PDF'

	select @modalidadjpg = IdModalidad from Estructura.Modalidad
	where NombreModalidad = 'Archivos JPG'

	INSERT into Estructura.FormatoArchivo (IdTipoEstructura,
											IdModalidad,
											TablaTemporal,
											Separador,
											Extension,
											Estado,
											FechaCrea,
											UsuarioCrea,
											UsuarioModifica,
											FechaModifica,
											ValidarCampos) 

	select TOP 1 IdTipoEstructura,@modalidadpdf,'General.archivoPDF',':','PDF',1,GETDATE(),'Administrador',NULL,NULL,0 from Estructura.TipoEstructura


	INSERT into Estructura.FormatoArchivo (IdTipoEstructura,
											IdModalidad,
											TablaTemporal,
											Separador,
											Extension,
											Estado,
											FechaCrea,
											UsuarioCrea,
											UsuarioModifica,
											FechaModifica,
											ValidarCampos) 

	select TOP 1 IdTipoEstructura,@modalidadjpg,'General.archivoJPG',':','JPG',1,GETDATE(),'Administrador',NULL,NULL,0 from Estructura.TipoEstructura
								
END

