USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Insertar]    Script Date: 03/07/2014 07:18:45 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_FormaPago_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_FormaPago_Insertar]
GO
-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  03/07/2014 19:16
-- Description:	Procedimiento almacenado que guarda un nuevo Proveedores_Contratos con forma de pago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_FormaPago_Insertar]
		@IdProveedoresContratos INT OUTPUT, 	
		@IdProveedores INT,	
		@IdContrato INT, 
		@UsuarioCrea NVARCHAR(250),
		@IdFormaPago Int

AS
BEGIN
	INSERT INTO CONTRATO.ProveedoresContratos (IdProveedores, IdContrato, UsuarioCrea, FechaCrea, IdFormaPago)
		VALUES (@IdProveedores, @IdContrato, @UsuarioCrea, GETDATE(), @IdFormaPago)
	SELECT
		@IdProveedoresContratos = @@IDENTITY
 		
END



