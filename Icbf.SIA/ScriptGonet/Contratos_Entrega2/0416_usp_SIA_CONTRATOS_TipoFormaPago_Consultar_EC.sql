USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATOS_TipoFormaPago_Consultar]    Script Date: 29/06/2014 01:07:17 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATOS_TipoFormaPago_Consultar')
BEGIN
DROP PROCEDURE [dbo].[usp_SIA_CONTRATOS_TipoFormaPago_Consultar]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATOS_TipoFormaPago_Consultar]    Script Date: 29/06/2014 01:07:17 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 29/06/2014
-- Description:	Metodo que permite consultar la entidad TipoFormaPago filtrando los resultados por los filtros
-- codigoTipoFormaPago, nombreTipoFormaPago, descripcion, estado
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATOS_TipoFormaPago_Consultar]
	@CodigoTipoFormaPago VARCHAR(8) = NULL,
	@NombreTipoFormaPago VARCHAR(50) = NULL,
	@Descripcion VARCHAR(128) = NULL,
	@Estado BIT = NULL
AS
BEGIN
	
	SELECT [IdTipoFormaPago], [CodigoTipoFormaPago], [NombreTipoFormaPago], [Descripcion],
	[Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]
	FROM [CONTRATO].[TipoFormaPago]
	WHERE [CodigoTipoFormaPago] = ISNULL(@CodigoTipoFormaPago, [CodigoTipoFormaPago]) AND
	[NombreTipoFormaPago] = ISNULL(@NombreTipoFormaPago, [NombreTipoFormaPago]) AND
	[Descripcion] = ISNULL(@Descripcion, [Descripcion]) AND
	[Estado] = ISNULL(@Estado, [Estado])

END

GO


