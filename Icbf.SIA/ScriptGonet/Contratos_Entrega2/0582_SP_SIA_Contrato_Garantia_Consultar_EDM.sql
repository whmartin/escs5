USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_Garantia_Consultar]    Script Date: 13/08/2014 11:58:23 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_Garantia_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Consultar]
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  6/4/2014 9:59:59 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
-- =============================================
-- Author:		Efra�n Diaz mejia
-- Create date:  13/08/2014 
-- Description:	Se agregaron los camos zona, DescripcionEstadoGarantia, CodDescripcionEstadoGarantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Consultar]
	@IDGarantia INT
AS
BEGIN
 SELECT G.IDGarantia, G.IDTipoGarantia, G.NumeroGarantia, G.FechaAprobacionGarantia, 
 G.FechaCertificacionGarantia, G.FechaDevolucion, G.MotivoDevolucion, G.IDUsuarioAprobacion, G.IDUsuarioDevolucion,
  G.IdContrato, G.BeneficiarioICBF, G.BeneficiarioOTROS, G.DescripcionBeneficiarios, G.FechaInicioGarantia, G.FechaExpedicionGarantia, 
  G.FechaVencimientoInicialGarantia, G.FechaVencimientoFinalGarantia, G.FechaReciboGarantia, G.ValorGarantia, 
  G.Anexos, G.ObservacionesAnexos, G.EntidadProvOferenteAseguradora, G.UsuarioCrea, G.FechaCrea, G.UsuarioModifica, 
  G.FechaModifica, G.Estado, G.IDSucursalAseguradoraContrato, G.IDEstadosGarantias,
  rtrim(ltrim(EG.CodigoEstadoGarantia)) +'-'+ rtrim(ltrim(EG.DescripcionEstadoGarantia)) as CodDescripcionEstadoGarantia,
  EG.DescripcionEstadoGarantia,
  Case SAC.Idzona 
  When 1 Then 'Urbana' 
  When 2 Then 'Rural'
  else 'N/A' end as Zona
    
   FROM Contrato.Garantia G
   Inner join CONTRATO.EstadosGarantias EG ON G.IdEstadosGarantias = EG.IDEstadosGarantias 
   inner join CONTRATO.SucursalAseguradoraContrato SAC ON SAC.IDSucursalAseguradoraContrato = G.IDSucursalAseguradoraContrato
   WHERE  IDGarantia = @IDGarantia
END
