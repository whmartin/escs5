USE [SIA]
GO

UPDATE [CONTRATO].[TipoContrato] 
SET [NombreTipoContrato] = UPPER([NombreTipoContrato])
WHERE [CodigoTipoContrato] = 'PREST_SERV_APO_GEST' AND [NombreTipoContrato] = 'Prestación de servicios de apoyo a la gestión'

UPDATE [CONTRATO].[TipoContrato] 
SET [NombreTipoContrato] = UPPER([NombreTipoContrato])
WHERE [CodigoTipoContrato] = 'PREST_SERV_PROF' AND [NombreTipoContrato] = 'Prestación de servicios profesionales'

UPDATE [CONTRATO].[TipoContrato]
SET [NombreTipoContrato] = UPPER([NombreTipoContrato])
WHERE [CodigoTipoContrato] = 'APORTE' AND [NombreTipoContrato] = 'Aporte Contrato'

UPDATE [CONTRATO].[TipoContrato] 
SET [NombreTipoContrato] = UPPER([NombreTipoContrato])
WHERE [CodigoTipoContrato] = 'MC_INT_ADMIN' AND [NombreTipoContrato] = 'Marco Interadministrativo'

UPDATE [CONTRATO].[TipoContrato] 
SET [NombreTipoContrato] = UPPER([NombreTipoContrato])
WHERE [CodigoTipoContrato] = 'APORTE' AND [NombreTipoContrato] = 'Aporte Convenio'