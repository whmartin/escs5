USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Modificar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:36 PM
-- Description:	Procedimiento almacenado que actualiza un(a) PlanComprasRubrosCDP
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubrosCDP_Modificar]
		@IDRubroPlanComprasContrato INT,
		@IDPlanDeComprasContratos	INT,
		@CodigoRubro				NVARCHAR(256),
		@ValorRubro					NUMERIC(32,2),
		@UsuarioModifica			NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.RubroPlanComprasContrato 
	SET ValorRubroPresupuestal = @ValorRubro
		,UsuarioModifica = @UsuarioModifica
		,FechaModifica = GETDATE() 
	WHERE IDRubro = @CodigoRubro
		AND IDRubroPlanComprasContrato = @IDRubroPlanComprasContrato
		AND IDPlanDeComprasContratos = @IDPlanDeComprasContratos
END
GO


