USE [SIA]
GO
   -- =============================================
   -- Author:              @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:12
   -- Description:         Inserta la tabla para ser visualizada en la aplicación
   -- =============================================
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Contratos/TipoContrato' and NombrePrograma = 'Tipo Contrato' )
begin
      declare @idpadre int
      select @idpadre = IdPrograma from SEG.Programa where NombrePrograma = 'Contrato'
      insert INTO SEG.Programa  (IdModulo,
								NombrePrograma,
								CodigoPrograma,
								Posicion,
								Estado,
								UsuarioCreacion,
								FechaCreacion,
								UsuarioModificacion,
								FechaModificacion,
								VisibleMenu,
								generaLog,
								Padre)
      SELECT IdModulo,'Tipo Contrato','Contratos/TipoContrato',1,1,'Administrador',GETDATE(),'','',0,1,NULL
      from SEG.Modulo
      where NombreModulo = 'CONTRATOS'    

      INSERT INTO SEG.Permiso 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      from SEG.Programa
      where CodigoPrograma = 'Contratos/TipoContrato'
      and NombrePrograma = 'Tipo Contrato' 


	  INSERT INTO CONTRATO.TablaParametrica ( [CodigoTablaParametrica], [NombreTablaParametrica], 
		[Url], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
		VALUES ( N'09', N'Tipo Contrato', N'Contratos/TipoContrato', 1, N'administrador', GETDATE(), NULL, NULL)
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
