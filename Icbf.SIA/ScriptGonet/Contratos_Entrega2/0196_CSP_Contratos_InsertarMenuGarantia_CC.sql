USE SIA
GO

-- ====================================================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date: 12/06/2014
-- Description:	SE CREA PARA REALIZAR LA INSERICION DEL MODULO GARANTIAS EN CONTRATOS
-- ====================================================================
IF NOT EXISTS(SELECT idprograma FROM SEG.Programa WHERE CodigoPrograma = 'Contratos/Garantia' AND NombrePrograma = 'Registrar Garant�as')
BEGIN
       
      DECLARE @rolid INT

        INSERT INTO SEG.Programa (IdModulo,
                                               NombrePrograma,
                                               CodigoPrograma,
                                               Posicion,
                                               Estado,
                                               UsuarioCreacion,
                                               FechaCreacion,
                                               UsuarioModificacion,
                                               FechaModificacion,
                                               VisibleMenu,
                                               generaLog,
                                               Padre)
        SELECT IdModulo,'Registrar Garant�as','Contratos/Garantia',1,1,'Administrador',GETDATE(),'','',1,1,NULL  
        FROM SEG.Modulo
        WHERE NombreModulo = 'Contratos'
               
               
     
       
      select @rolid = IdRol FROM SEG.Rol where Nombre = 'ADMINISTRADOR'
      
         
        INSERT INTO SEG.Permiso (IdPrograma,
                                               IdRol,
                                               Insertar,
                                               Modificar,
                                               Eliminar,
                                               Consultar,
                                               UsuarioCreacion,
                                               FechaCreacion,
                                               UsuarioModificacion,
                                               FechaModificacion)
        SELECT idprograma,@rolid,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
        FROM SEG.Programa
        WHERE CodigoPrograma = 'Contratos/Garantia'
            AND NombrePrograma = 'Registrar Garant�as'     
      
END
ELSE
BEGIN
        PRINT 'YA EXISTE EL MODULO A CREAR'
END