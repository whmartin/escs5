use sia

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Creacion tabla [CONTRATO].[AporteContrato]
-- =============================================

/****** Object:  Table [CONTRATO].[AporteContrato]    Script Date: 23/05/2014 15:31:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[AporteContrato]') AND type in (N'U'))
DROP TABLE [CONTRATO].[AporteContrato]
GO
