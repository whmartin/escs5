USE [SIA]
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Contrato_Modificar_Estado]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_Modificar_Estado]
GO

-- =============================================
-- Author: Eduardo Isaac Ballesteros Mu�oz		
-- Create date:  07/10/2014 09:00
-- Description: SIA CU_026_CONT-ESTADOCONTRATO 
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_Modificar_Estado]
AS
BEGIN
	SET NOCOUNT ON;

	--******************************************************************************************************************************************************** 
	--Datos globales
	--Estado Contrato Ejecuci�n
	DECLARE @EstadoContratoEjecucion INT

	SET @EstadoContratoEjecucion = (
			SELECT IdEstadoContrato
			FROM CONTRATO.EstadoContrato
			WHERE CodEstado = 'EJE'
			)

	--Estado Contrato Suspendido
	DECLARE @EstadoContratoSuspendido INT

	SET @EstadoContratoSuspendido = (
			SELECT IdEstadoContrato
			FROM CONTRATO.EstadoContrato
			WHERE CodEstado = 'SUP'
			)

	--Estado Contrato Terminado
	DECLARE @EstadoContratoTerminado INT

	SET @EstadoContratoTerminado = (
			SELECT IdEstadoContrato
			FROM CONTRATO.EstadoContrato
			WHERE CodEstado = 'TER'
			)

	--Estado Contrato Perdida Competencia
	DECLARE @EstadoContratoPerdidaCompetencia INT

	SET @EstadoContratoPerdidaCompetencia = (
			SELECT IdEstadoContrato
			FROM CONTRATO.EstadoContrato
			WHERE CodEstado = 'PEC'
			)

	--Estado Contrato Con RP
	DECLARE @EstadoContratoERP INT

	SET @EstadoContratoERP = (
			SELECT IdEstadoContrato
			FROM CONTRATO.EstadoContrato
			WHERE CodEstado = 'CRP'
			)

	--Estado Garant�a Aprobada
	DECLARE @EstadoGarantiaAprobada INT

	SET @EstadoGarantiaAprobada = (
			SELECT IDEstadosGarantias
			FROM CONTRATO.EstadosGarantias
			WHERE CodigoEstadoGarantia = '001'
			)

	--**************************************************************************************************************************************************************** 
	--	El contrato debe colocarse en estado TERMINADO si la fecha de terminaci�n del contrato "FechaFinalInicioContrato"
	--	es igual a la fecha del sistema m�s un d�a
	UPDATE [CONTRATO].[Contrato]
	SET [IdEstadoContrato] = @EstadoContratoTerminado
		,[UsuarioModifica] = 'job_EstadosContratos'
		,[FechaModifica] = GETDATE()
	WHERE CAST([FechaFinalizacionIniciaContrato] AS DATE) = CAST(GETDATE() + 1 AS DATE)

	--**************************************************************************************************************************************************************** 
	--	El contrato debe colocarse en estado PERDIDA DE COMPETENCIA,
	--	si el contrato se encuentra en estado TERMINADO y
	--	Fecha del sistema - fecha de terminaci�n del contrato "FechaFinalInicioContrato" >= 2 a�os y 6 meses
	UPDATE [CONTRATO].[Contrato]
	SET [IdEstadoContrato] = @EstadoContratoPerdidaCompetencia
		,[UsuarioModifica] = 'job_EstadosContratos'
		,[FechaModifica] = GETDATE()
	WHERE [IdEstadoContrato] = @EstadoContratoTerminado
		AND DATEDIFF(DAY, CAST([FechaFinalizacionIniciaContrato] AS DATE), CAST(GETDATE() AS DATE)) >= DATEDIFF(DAY, CAST(GETDATE() AS DATE), (DATEADD(MONTH, 6, DATEADD(YEAR, 2, CAST(GETDATE() AS DATE)))))

	--**************************************************************************************************************************************************************** 
	--	El contrato debe colocarse en estado EJECUCI�N si:

	UPDATE [CONTRATO].[Contrato]
	SET [IdEstadoContrato] = @EstadoContratoEjecucion
		,[FechaInicioEjecucion] = (
			SELECT MAX(FECHA)
			FROM (
				SELECT CONTRATO.AporteContrato.IdContrato
					,CONTRATO.AporteContrato.FechaRP AS FECHA
				FROM CONTRATO.AporteContrato
				WHERE CONTRATO.AporteContrato.IdContrato = CONTRATO.Contrato.IdContrato
				
				UNION
				
				SELECT CONTRATO.Garantia.IdContrato
					,CONTRATO.Garantia.FechaAprobacionGarantia AS FECHA
				FROM CONTRATO.Garantia
				WHERE Contrato.Garantia.IdContrato = CONTRATO.Contrato.IdContrato
				) AS FECHAS
			)
		,[UsuarioModifica] = 'job_EstadosContratos'
		,[FechaModifica] = GETDATE()
	WHERE
		--	Se validan que las Garant�as se encuentren todas en estado Aprobado
		(
			(
				(
					SELECT COUNT(IDGarantia)
					FROM CONTRATO.Garantia
					WHERE CONTRATO.Garantia.IdContrato = CONTRATO.Contrato.IdContrato
					) <> 0
				)
			AND (
				(
					SELECT COUNT(IDGarantia)
					FROM CONTRATO.Garantia
					WHERE CONTRATO.Garantia.IDEstadosGarantias = @EstadoGarantiaAprobada
						AND CONTRATO.Garantia.IdContrato = CONTRATO.Contrato.IdContrato
					) = (
					SELECT COUNT(IDGarantia)
					FROM CONTRATO.Garantia
					WHERE CONTRATO.Garantia.IdContrato = CONTRATO.Contrato.IdContrato
					)
				)
			)
		--	Se valida que el contrato este en estado "Contrato con RP"
		AND ([CONTRATO].[Contrato].IdEstadoContrato = @EstadoContratoERP)
		--	No se encuentra een estado "SUSPENDIDO". Se debe actualizar la fecha de inicio de ejecuci�n a la Fecha mayor entre 
		--	la mayor Fecha Aprobaci�n garant�a de las garant�as y Fecha Expedici�n RP		
		AND ([CONTRATO].[Contrato].IdEstadoContrato <> @EstadoContratoSuspendido)
END