USE [SIA]
GO


UPDATE [CONTRATO].[ModalidadSeleccion] 
SET [Nombre] = UPPER([Nombre])
WHERE [CodigoModalidad] = 'CONT_DIR' AND [Nombre] = 'Contratación Directa'


UPDATE [CONTRATO].[RegimenContratacion] 
SET [NombreRegimenContratacion] = UPPER([NombreRegimenContratacion])
WHERE [NombreRegimenContratacion] = 'Régimen' AND [Descripcion] = 'Régimen'







