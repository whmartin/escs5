USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_NMF_BaseSIF_EntidadFinancieras_Consultar]    Script Date: 29/05/2014 09:07:56 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_NMF_BaseSIF_EntidadFinancieras_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_NMF_BaseSIF_EntidadFinancieras_Consultar]
GO


-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  29/05/2014 10:16
-- Description:	Procedimiento almacenado que consulta Entidades Financieras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_NMF_BaseSIF_EntidadFinancieras_Consultar]
    @CodigoEntFin NVARCHAR(50) = NULL ,
    @NombreEntFin NVARCHAR(256) = NULL ,
    @Estado BIT = NULL ,
    @EsPagoPorOrdenDePago BIT = NULL ,
    @EsPagoPorOrdenBancaria BIT = NULL
AS 
    BEGIN
        SELECT  IdEntidadFinanciera ,
                CodigoEntFin ,
                CodigoEntFin + ' - ' + NombreEntFin AS NombreEntFin ,
                Estado ,
                UsuarioCrea ,
                FechaCrea ,
                UsuarioModifica ,
                FechaModifica ,
                NombreEntFin AS NombreEntFin2 ,
                EsPagoPorOrdenBancaria ,
                EsPagoPorOrdenDePago
        FROM    [BaseSIF].[EntidadFinanciera]
        WHERE   CodigoEntFin = CASE WHEN @CodigoEntFin IS NULL
                                    THEN CodigoEntFin
                                    ELSE @CodigoEntFin
                               END
                AND NombreEntFin LIKE CASE WHEN @NombreEntFin IS NULL
                                           THEN NombreEntFin
                                           ELSE @NombreEntFin + '%'
                                      END
                AND Estado = CASE WHEN @Estado IS NULL THEN Estado
                                  ELSE @Estado
                             END
				AND
				(
					EsPagoPorOrdenBancaria = @EsPagoPorOrdenBancaria
					OR @EsPagoPorOrdenBancaria IS NULL
				) --columna permite null
                --AND EsPagoPorOrdenBancaria = CASE WHEN @EsPagoPorOrdenBancaria IS NULL
                --                                  THEN EsPagoPorOrdenBancaria
                --                                  ELSE @EsPagoPorOrdenBancaria
                --                            END
				AND
				(
					EsPagoPorOrdenDePago = @EsPagoPorOrdenDePago
					OR @EsPagoPorOrdenDePago IS NULL
				) --columna permite null
                --AND EsPagoPorOrdenDePago = CASE WHEN @EsPagoPorOrdenDePago IS NULL
                --                                THEN EsPagoPorOrdenDePago
                --                                ELSE @EsPagoPorOrdenDePago
                --                           END
    END




