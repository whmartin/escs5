USE SIA
GO


UPDATE [CONTRATO].[TipoContratoAsociado] 
SET Descripcion = UPPER(Descripcion)
WHERE CodContratoAsociado = 'CM' AND Descripcion = 'Convenio marco'

UPDATE [CONTRATO].[TipoContratoAsociado] 
SET Descripcion = UPPER(Descripcion)
WHERE CodContratoAsociado = 'CCA' AND Descripcion = 'Contrato/Convenio Adhesi�n'