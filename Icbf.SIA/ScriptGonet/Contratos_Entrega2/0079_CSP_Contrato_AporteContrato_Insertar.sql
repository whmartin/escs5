USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AporteContrato_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Insertar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  5/29/2014 2:03:01 PM
-- Description:	Procedimiento almacenado que guarda un nuevo AporteContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Insertar]
		@IdAporteContrato INT OUTPUT, 	@AportanteICBF BIT,	@NumeroIdentificacionICBF NVARCHAR(56),	@ValorAporte NUMERIC(30,2),	@DescripcionAporte NVARCHAR(896),	@AporteEnDinero BIT,	@IdContrato INT,	@IDEntidadProvOferente INT,	@FechaRP DATETIME,	@NumeroRP NVARCHAR(30),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	IF EXISTS(SELECT * FROM Contrato.AporteContrato 
			  WHERE  IdContrato = @IdContrato AND
					 AporteEnDinero = @AporteEnDinero AND
					 ValorAporte = @ValorAporte AND
					 IDEntidadProvOferente = @IDEntidadProvOferente)
	BEGIN
		RAISERROR('Ya existe un registro con esta combinación',16,1)
	    RETURN
	END
	INSERT INTO Contrato.AporteContrato(AportanteICBF, NumeroIdentificacionICBF, ValorAporte, DescripcionAporte, AporteEnDinero, IdContrato, IDEntidadProvOferente, FechaRP, NumeroRP, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@AportanteICBF, @NumeroIdentificacionICBF, @ValorAporte, @DescripcionAporte, @AporteEnDinero, @IdContrato, @IDEntidadProvOferente, @FechaRP, @NumeroRP, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdAporteContrato = @@IDENTITY 		
END
