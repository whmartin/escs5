USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_TipoSupervisorInterventors_ConsultarID]    Script Date: 11/07/2014 16:09:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_TipoSupervisorInterventors_ConsultarID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_TipoSupervisorInterventors_ConsultarID]
GO


-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/30/2014 3:57:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) SupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_TipoSupervisorInterventors_ConsultarID]
	@pIDTipoSupervisorInterventor int
AS
BEGIN
	SELECT
		IDTipoSupervisorInterventor,
		Codigo,
		Descripcion,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].[TipoSupervisorInterventor]
	WHERE IDTipoSupervisorInterventor = @pIDTipoSupervisorInterventor
END


