USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_UnidadCalculos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculos_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/3/2014 3:19:34 PM
-- Description:	Procedimiento almacenado que consulta un(a) UnidadCalculo
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculos_Consultar]
	@CodUnidadCalculo NVARCHAR(8) = NULL,@Descripcion NVARCHAR(80) = NULL,@Inactivo BIT = NULL
AS
BEGIN
 SELECT IDUnidadCalculo, CodUnidadCalculo, Descripcion, Inactivo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[UnidadCalculo] WHERE CodUnidadCalculo = CASE WHEN @CodUnidadCalculo IS NULL THEN CodUnidadCalculo ELSE @CodUnidadCalculo END AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END AND Inactivo = CASE WHEN @Inactivo IS NULL THEN Inactivo ELSE @Inactivo END
END
