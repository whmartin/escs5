USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]    Script Date: 25/06/2014 09:52:55 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]    Script Date: 25/06/2014 09:52:55 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  5/29/2014 2:03:01 PM
-- Description:	Procedimiento almacenado que consulta un(a) AporteContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]
AS
BEGIN
 SELECT min(FechaVigenciaDesde), max(FechaVigenciaHasta) from CONTRATO.AmparosGarantias  
END


