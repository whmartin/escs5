USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  22/08/2014 
-- Description:	Insertar Tabla Permiso para visualizar en el men� de tablas param�tricas la opci�n de Objeto Contrato
-- =============================================
IF not exists (SELECT * FROM [SEG].[Programa] PROG
inner JOIN SEG.Permiso PRM ON PRM.IdPrograma = PROG.IdPrograma
WHERE PROG.NombrePrograma LIKE '%Objeto Contrato%')
begin
	declare @IdPrograma int = null

	select @IdPrograma = IdPrograma from [SEG].[Programa] where [NombrePrograma] = 'Objeto Contrato'
	
	If @IdPrograma is not null
	begin
	
		INSERT INTO SEG.Permiso
					   ([IdPrograma]
					   ,[IdRol]
					   ,[Insertar]
					   ,[Modificar]
					   ,[Eliminar]
					   ,[Consultar]
					   ,[UsuarioCreacion]
					   ,[FechaCreacion]
					   ,[UsuarioModificacion]
					   ,[FechaModificacion]
					   )
				 VALUES
					   (@IdPrograma
					   ,1
					   ,1
					   ,1
					   ,1
					   ,1
					   ,'Administrador'
					   ,GETDATE()
					   ,NULL
					   ,NULL
					   )
	End	   
END