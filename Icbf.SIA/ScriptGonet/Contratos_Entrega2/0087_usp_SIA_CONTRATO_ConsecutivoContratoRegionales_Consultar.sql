USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/27/2014 6:43:39 PM
-- Description:	Procedimiento almacenado que consulta un(a) ConsecutivoContratoRegionales
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Consultar]
	@IDConsecutivoContratoRegional INT
AS
BEGIN
	SELECT
		IDConsecutivoContratoRegional,
		IdRegional,
		IdVigencia,
		Consecutivo,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].[ConsecutivoContratoRegionales]
	WHERE IDConsecutivoContratoRegional = @IDConsecutivoContratoRegional
END
