USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]    Script Date: 17/09/2014 10:11:40 a. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Modificar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]
		@IdModalidad INT,	@Nombre NVARCHAR(128),	@Sigla NVARCHAR(5),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN


	IF NOT EXISTS (SELECT Nombre FROM Contrato.ModalidadSeleccion WHERE Nombre = @Nombre 
	and Sigla = @Sigla and IdModalidad <> @IdModalidad)
	BEGIN
		IF NOT EXISTS (SELECT Nombre FROM Contrato.ModalidadSeleccion WHERE Nombre = @Nombre 
		and IdModalidad <> @IdModalidad)
		BEGIN

			IF NOT EXISTS (SELECT Nombre FROM Contrato.ModalidadSeleccion WHERE 
			Sigla = @Sigla and IdModalidad <> @IdModalidad)
			BEGIN
					UPDATE Contrato.ModalidadSeleccion SET Nombre = @Nombre, Sigla = @Sigla, 
					Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
					WHERE IdModalidad = @IdModalidad
			END
			ELSE
			BEGIN
				RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
							16, -- Severity.
							1, -- State.
							2627);
			END
				
		END
		ELSE
		BEGIN
			RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
						16, -- Severity.
						1, -- State.
						2627);
		END
			
	END
	ELSE
	BEGIN
		RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
					16, -- Severity.
					1, -- State.
					2627);
	END

	
END



