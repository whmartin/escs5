USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_Garantias_Consultar]    Script Date: 26/06/2014 04:57:53 p.m. ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_Garantias_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_Garantias_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_Garantias_Consultar]    Script Date: 26/06/2014 04:57:53 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  6/4/2014 9:59:59 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_Garantias_Consultar]
		@IDTipoGarantia INT= NULL,	
		@NumeroGarantia NVARCHAR(80)= NULL,	
		@FechaAprobacionGarantia DATETIME= NULL,	
		@FechaCertificacionGarantia DATETIME= NULL,	
		@FechaDevolucion DATETIME= NULL,	
		@MotivoDevolucion NVARCHAR(800)= NULL,	
		@IDUsuarioAprobacion INT= NULL,	
		@IDUsuarioDevolucion INT= NULL,	
		@IdContrato INT= NULL,	
		@BeneficiarioICBF BIT= NULL,
		@BeneficiarioOTROS BIT = NULL,	
		@DescripcionBeneficiarios NVARCHAR(400)= NULL,	
		@FechaInicioGarantia DATETIME= NULL,	
		@FechaExpedicionGarantia DATETIME= NULL,	
		@FechaVencimientoInicialGarantia DATETIME= NULL,	
		@FechaVencimientoFinalGarantia DATETIME= NULL,	
		@FechaReciboGarantia DATETIME= NULL,	
		@ValorGarantia NVARCHAR(50)= NULL,	
		@Anexos BIT= NULL,	
		@ObservacionesAnexos NVARCHAR(400)= NULL,	
		@EntidadProvOferenteAseguradora INT= NULL, 
		@UsuarioCrea NVARCHAR(250)= NULL,
		@Estado BIT= NULL,
		@IDSucursalAseguradoraContrato INT= NULL,
		@IDEstadosGarantias INT= NULL
AS
BEGIN
	SELECT
		IDGarantia,
		IDTipoGarantia,
		NumeroGarantia,
		FechaAprobacionGarantia,
		FechaCertificacionGarantia,
		FechaDevolucion,
		MotivoDevolucion,
		IDUsuarioAprobacion,
		IDUsuarioDevolucion,
		IdContrato,
		BeneficiarioICBF,
		BeneficiarioOTROS,
		DescripcionBeneficiarios,
		FechaInicioGarantia,
		FechaExpedicionGarantia,
		FechaVencimientoInicialGarantia,
		FechaVencimientoFinalGarantia,
		FechaReciboGarantia,
		ValorGarantia,
		Anexos,
		ObservacionesAnexos,
		EntidadProvOferenteAseguradora,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica,
		Estado,
		IDSucursalAseguradoraContrato,
		IDEstadosGarantias
	FROM [Contrato].[Garantia]
	WHERE IdContrato =
		CASE
			WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato
		END
 
 --WHERE  IDTipoGarantia = CASE WHEN @IDTipoGarantia IS NULL THEN IDTipoGarantia ELSE @IDTipoGarantia END 
 --AND NumeroGarantia = CASE WHEN @NumeroGarantia IS NULL THEN NumeroGarantia ELSE @NumeroGarantia END 
 --AND FechaAprobacionGarantia = CASE WHEN @FechaAprobacionGarantia IS NULL THEN FechaAprobacionGarantia ELSE @FechaAprobacionGarantia END 
 --AND FechaCertificacionGarantia = CASE WHEN @FechaCertificacionGarantia IS NULL THEN FechaCertificacionGarantia ELSE @FechaCertificacionGarantia END 
 --AND FechaDevolucion = CASE WHEN @FechaDevolucion IS NULL THEN FechaDevolucion ELSE @FechaDevolucion END 
 --AND MotivoDevolucion = CASE WHEN @MotivoDevolucion IS NULL THEN MotivoDevolucion ELSE @MotivoDevolucion END 
 --AND IDUsuarioAprobacion = CASE WHEN @IDUsuarioAprobacion IS NULL THEN IDUsuarioAprobacion ELSE @IDUsuarioAprobacion END 
 --AND IDUsuarioDevolucion = CASE WHEN @IDUsuarioDevolucion IS NULL THEN IDUsuarioDevolucion ELSE @IDUsuarioDevolucion END 
 --AND IdContrato = CASE WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato END 
 --AND BeneficiarioICBF = CASE WHEN @BeneficiarioICBF IS NULL THEN BeneficiarioICBF ELSE @BeneficiarioICBF END 
 --AND DescripcionBeneficiarios = CASE WHEN @DescripcionBeneficiarios IS NULL THEN DescripcionBeneficiarios ELSE @DescripcionBeneficiarios END 
 --AND FechaInicioGarantia = CASE WHEN @FechaInicioGarantia IS NULL THEN FechaInicioGarantia ELSE @FechaInicioGarantia END 
 --AND FechaExpedicionGarantia = CASE WHEN @FechaExpedicionGarantia IS NULL THEN FechaExpedicionGarantia ELSE @FechaExpedicionGarantia END 
 --AND FechaVencimientoInicialGarantia = CASE WHEN @FechaVencimientoInicialGarantia IS NULL THEN FechaVencimientoInicialGarantia ELSE @FechaVencimientoInicialGarantia END 
 --AND FechaVencimientoFinalGarantia = CASE WHEN @FechaVencimientoFinalGarantia IS NULL THEN FechaVencimientoFinalGarantia ELSE @FechaVencimientoFinalGarantia END 
 --AND FechaReciboGarantia = CASE WHEN @FechaReciboGarantia IS NULL THEN FechaReciboGarantia ELSE @FechaReciboGarantia END 
 --AND ValorGarantia = CASE WHEN @ValorGarantia IS NULL THEN ValorGarantia ELSE @ValorGarantia END 
 --AND Anexos = CASE WHEN @Anexos IS NULL THEN Anexos ELSE @Anexos END 
 --AND ObservacionesAnexos = CASE WHEN @ObservacionesAnexos IS NULL THEN ObservacionesAnexos ELSE @ObservacionesAnexos END 
 --AND EntidadProvOferenteAseguradora = CASE WHEN @EntidadProvOferenteAseguradora IS NULL THEN EntidadProvOferenteAseguradora ELSE @EntidadProvOferenteAseguradora END
 --AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
 --AND IDSucursalAseguradoraContrato = CASE WHEN @IDSucursalAseguradoraContrato IS NULL THEN IDSucursalAseguradoraContrato ELSE @IDSucursalAseguradoraContrato END
 --AND IDEstadosGarantias = CASE WHEN @IDEstadosGarantias IS NULL THEN IDEstadosGarantias ELSE @IDEstadosGarantias END
END



