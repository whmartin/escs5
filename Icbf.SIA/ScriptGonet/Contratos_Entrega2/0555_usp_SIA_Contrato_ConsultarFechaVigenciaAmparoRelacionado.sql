USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]    Script Date: 04/08/2014 11:07:10 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]    Script Date: 04/08/2014 11:07:10 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  5/29/2014 2:03:01 PM
-- Description:	Procedimiento almacenado que consulta un(a) AporteContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado]
@IdGarantia INT
AS
BEGIN
 SELECT min(FechaVigenciaDesde) as FechaVigenciaDesde, max(FechaVigenciaHasta) as FechaVigenciaHasta from CONTRATO.AmparosGarantias  
 WHERE IDGarantia = @IdGarantia
END


