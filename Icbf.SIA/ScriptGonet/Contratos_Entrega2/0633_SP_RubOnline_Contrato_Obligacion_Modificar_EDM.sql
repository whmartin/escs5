USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]    Script Date: 16/09/2014 02:50:29 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]
GO


   -- =============================================
   -- Author:              @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que actualiza Obligacion
   -- =============================================
   -- =============================================
	-- Author:		Gonet\Efrain Diaz
	-- Create date:  15/09/2014 
	-- Description:	se agrego procedimiento para controlar los registros repetidos
	-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]
		@IdObligacion INT,	@IdTipoObligacion INT,	@IdTipoContrato INT,	
		@Descripcion nvarchar(MAX),	
		@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	

	IF NOT EXISTS (SELECT IdObligacion FROM Contrato.Obligacion WHERE IdTipoObligacion = @IdTipoObligacion 
	and IdTipoContrato = @IdTipoContrato and Descripcion = @Descripcion and IdObligacion <> @IdObligacion)
	BEGIN
			UPDATE Contrato.Obligacion 
			SET IdTipoObligacion = @IdTipoObligacion, IdTipoContrato = @IdTipoContrato, 
			Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdObligacion = @IdObligacion
	END
	ELSE
	BEGIN
		RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
					16, -- Severity.
					1, -- State.
					2627);
	END

END




