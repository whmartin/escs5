USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  21/06/2014 
-- Description:	modificar campo campo Descripcion de la tabla Contrato.TipoSuperInter
-- =============================================

IF EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'TipoSuperInter' 
           AND  COLUMN_NAME = 'Descripcion')

	ALTER TABLE CONTRATO.TipoSuperInter
	Alter column 
	   [Descripcion] [nvarchar](128) NOT NULL