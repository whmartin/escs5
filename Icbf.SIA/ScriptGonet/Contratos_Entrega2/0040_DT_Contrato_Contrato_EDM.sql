USE [SIA]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Eliminacion tabla [CONTRATO].[Contrato]
-- =============================================

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'Contrato', N'COLUMN',N'IdFormaPago'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'Contrato', @level2type=N'COLUMN',@level2name=N'IdFormaPago'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'Contrato', N'COLUMN',N'IdCategoriaContrato'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'Contrato', @level2type=N'COLUMN',@level2name=N'IdCategoriaContrato'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'Contrato', N'COLUMN',N'IdModalidad'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'Contrato', @level2type=N'COLUMN',@level2name=N'IdModalidad'

GO

IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'CONTRATO', N'TABLE',N'Contrato', N'COLUMN',N'IdContrato'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'Contrato', @level2type=N'COLUMN',@level2name=N'IdContrato'

GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_ModalidadSeleccion]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_ModalidadSeleccion]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_FormaPago]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_FormaPago]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Contrato_CategoriaContrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Contrato]'))
ALTER TABLE [CONTRATO].[Contrato] DROP CONSTRAINT [FK_Contrato_CategoriaContrato]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_PlanDeComprasContratos_IdContrato]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[PlanDeComprasContratos]'))
ALTER TABLE [CONTRATO].[PlanDeComprasContratos] DROP CONSTRAINT [FK_PlanDeComprasContratos_IdContrato]
GO

/****** Object:  Table [CONTRATO].[Contrato]    Script Date: 20/05/2014 05:39:45 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[Contrato]') AND type in (N'U'))
DROP TABLE [CONTRATO].[Contrato]
GO

