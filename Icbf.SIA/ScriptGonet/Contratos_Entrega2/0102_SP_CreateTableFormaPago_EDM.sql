USE [SIA]
GO
-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/28/2014 3:00:58 PM
-- Descripcion: Creacion tabla forma de pago
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[FormaPago]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla FormaPago a crear'
RETURN
END
CREATE TABLE [CONTRATO].[FormaPago](
 [IdFormaPago] [INT]  IDENTITY(1,1) NOT NULL,
 [IdProveedores] [INT]  NOT NULL,
 [IdMedioPago] [INT]  NOT NULL,
 [IdEntidadFinanciera] [INT]  ,
 [TipoCuentaBancaria] [INT]  ,
 [NumeroCuentaBancaria] [NVARCHAR] (80) ,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_FormaPago] PRIMARY KEY CLUSTERED 
(
 	[IdFormaPago] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Guarda la información relacionada con la forma de pago', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdFormaPago' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id de la forma de pago.', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = IdFormaPago;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdProveedores' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Corresponde al Id del proveedor asociado a la forma de pago.', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = IdProveedores;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdMedioPago' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el medio de pago.', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = IdMedioPago;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdEntidadFinanciera' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Corresponde a la entidad financiera.', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = IdEntidadFinanciera;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'TipoCuentaBancaria' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Corresponde al tipo de cuenta bancaria.', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = TipoCuentaBancaria;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'NumeroCuentaBancaria' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Corresponde al número de cuenta bancaria.', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = NumeroCuentaBancaria;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.FormaPago') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('CONTRATO.FormaPago')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = CONTRATO, 
@level1type = N'TABLE',  @level1name = FormaPago,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
