USE [SIA]
GO

IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Eliminar]')
			AND type IN (
				N'P'
				,N'PC'
				)
		)
	DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Eliminar]
GO

-- =============================================
-- Author:	Eduardo Isaac Ballesteros
-- Create date:  7/4/2014 09:22:33 AM
-- Description:	Procedimiento almacenado que elimina un(a) VigenciaFuturas
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Eliminar] @IDVigenciaFuturas INT
AS
BEGIN
	DELETE CONTRATO.VigenciaFuturas
	WHERE IDVigenciaFuturas = @IDVigenciaFuturas
END

