USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]    Script Date: 17/09/2014 10:32:13 a. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Modificar
***********************************************/

-- =============================================
	-- Author:		Gonet\Efrain Diaz
	-- Create date:  15/09/2014 
	-- Description:	se agrego procedimiento para controlar los registros repetidos
	-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]
		@IdModalidad INT OUTPUT, 	@Nombre NVARCHAR(128),	@Sigla NVARCHAR(5),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN


	IF NOT EXISTS (SELECT Nombre FROM Contrato.ModalidadSeleccion WHERE Nombre = @Nombre 
	and Sigla = @Sigla)
	BEGIN
		IF NOT EXISTS (SELECT Nombre FROM Contrato.ModalidadSeleccion WHERE Nombre = @Nombre )
		BEGIN

			IF NOT EXISTS (SELECT Nombre FROM Contrato.ModalidadSeleccion WHERE 
			Sigla = @Sigla)
			BEGIN
					INSERT INTO Contrato.ModalidadSeleccion(Nombre, Sigla, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Nombre, @Sigla, @Estado, @UsuarioCrea, GETDATE())
					SELECT @IdModalidad = @@IDENTITY 
			END
			ELSE
			BEGIN
				RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
							16, -- Severity.
							1, -- State.
							2627);
			END
				
		END
		ELSE
		BEGIN
			RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
						16, -- Severity.
						1, -- State.
						2627);
		END
			
	END
	ELSE
	BEGIN
		RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
					16, -- Severity.
					1, -- State.
					2627);
	END

			
END



