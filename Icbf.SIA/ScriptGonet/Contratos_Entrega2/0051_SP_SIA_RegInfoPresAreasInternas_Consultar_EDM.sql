USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_RegInfoPresAreasInternas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_RegInfoPresAreasInternas_Consultar]
GO

-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Create date:  22/05/2014 2:30 PM
-- Description:	Procedimiento almacenado que Muestra la información relacionada con el certificado de disponibilidad presupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_RegInfoPresAreasInternas_Consultar]
	
	@RegionalICBF INT = null
	
AS
BEGIN

        select distinct AI.codigo, AI.descripcion, AI.estado, AI.UsuarioCrea, AI.FechaCrea, AI.UsuarioModifica, AI.FechaModifica 
		from Ppto.AreasInternas AI
		left outer Join Ppto.AreasxRubro APR on APR.IdAreasInternas = AI.IdAreasInternas
		left outer Join Ppto.InfoETLCDP IECDP on APR.IdVigencia = IECDP.IdVigencia and APR.IdCatalogo = IECDP.IdCatRubro
		
		Where IECDP.IdRegionalPCI = CASE WHEN @RegionalICBF IS NULL THEN IECDP.IdRegionalPCI ELSE @RegionalICBF END 
		And AI.Estado = 1
		
END

GO