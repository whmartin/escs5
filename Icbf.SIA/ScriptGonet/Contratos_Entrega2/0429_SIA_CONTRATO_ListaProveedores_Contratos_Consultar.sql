USE [SIA]
GO
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ListaProveedores_Contratos_Consultar]    Script Date: 09/07/2014 11:11:39 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  28/05/2014 02:53:55 PM
-- Description:	Procedimiento almacenado que consulta la lista de proveedores asociados a un contrato en especifico
-- =============================================
ALTER PROCEDURE [dbo].[usp_SIA_CONTRATO_ListaProveedores_Contratos_Consultar]
	@IdContrato INT = NULL
AS
BEGIN
  SELECT DISTINCT * FROM (
	  SELECT T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, T.IdTercero, entprof.IdEntidad ,( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as Razonsocila,
			PC.UsuarioCrea
	  FROM 
		 PROVEEDOR.EntidadProvOferente entprof
		 INNER JOIN CONTRATO.ProveedoresContratos PC ON entprof.IdEntidad = PC.IdProveedores
		 INNER JOIN oferente.TERCERO T ON T.IDTERCERO = entprof.IdTercero
		 INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
		 INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		 WHERE T.IdTipoPersona=1 AND PC.IdContrato = @IdContrato
	   
	   UNION 
	   
	   SELECT T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, T.IdTercero,entprof.IdEntidad, ( T.RAZONSOCIAL ) as Razonsocila,
			PC.UsuarioCrea
	  FROM 
		  PROVEEDOR.EntidadProvOferente entprof
		 INNER JOIN CONTRATO.ProveedoresContratos PC ON entprof.IdEntidad = PC.IdProveedores
		 INNER JOIN oferente.TERCERO T ON T.IDTERCERO = entprof.IdTercero
		 INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
		 INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		 WHERE T.IdTipoPersona=2 AND PC.IdContrato = @IdContrato
		  ) DT

   
END