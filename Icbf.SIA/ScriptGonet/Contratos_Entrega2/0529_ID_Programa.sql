use SIA
GO

/*****************************************
20140724 
Cambiar menu a parametrica
******************************************/

update SEG.Programa SET VisibleMenu = 0
where CodigoPrograma = 'Contratos/TipoClausula'

update SEG.Programa SET VisibleMenu = 0
where CodigoPrograma = 'Contratos/RegimenContratacion'

IF not exists (select 1 from CONTRATO.TablaParametrica where Url = 'Contratos/TipoClausula')
BEGIN
	insert INTO CONTRATO.TablaParametrica
	SELECT '11',NombrePrograma,CodigoPrograma,1,'Administrador',GETDATE(),NULL,NULL
	from SEG.Programa
	where CodigoPrograma = 'Contratos/TipoClausula'
end

IF not exists (select 1 from CONTRATO.TablaParametrica where Url = 'Contratos/RegimenContratacion')
BEGIN
	insert INTO CONTRATO.TablaParametrica
	SELECT '12',NombrePrograma,CodigoPrograma,1,'Administrador',GETDATE(),NULL,NULL
	from SEG.Programa
	where CodigoPrograma = 'Contratos/RegimenContratacion'
end
