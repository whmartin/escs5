USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SupervisorInterventors_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterventors_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/30/2014 3:57:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) SupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterventors_Consultar]
	@Codigo NVARCHAR(4) = NULL,@Descripcion NVARCHAR(100) = NULL
AS
BEGIN
SELECT
	IDSupervisorInterventor,
	Codigo,
	Descripcion,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [CONTRATO].[SupervisorInterventor]
WHERE Codigo =
	CASE
		WHEN @Codigo IS NULL THEN Codigo ELSE @Codigo
	END AND Descripcion =
	CASE
		WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
	END
END
