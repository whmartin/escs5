USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[LugarEjecucionContrato]') AND type in (N'U'))
BEGIN
IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = 0)
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'FechaModifica'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'FechaCrea'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'NivelNacional' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'NivelNacional'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdRegional' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'IdRegional'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdDepartamento' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'IdDepartamento'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdContrato' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'IdContrato'


IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IdLugarEjecucionContratos' AND [object_id] = OBJECT_ID('CONTRATO.LugarEjecucionContrato')))
	EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'IdLugarEjecucionContratos'


IF OBJECT_ID('LugarEjecucionContrato_IdMunicipio_FK', 'F') IS NOT NULL 
BEGIN
ALTER TABLE [CONTRATO].[LugarEjecucionContrato] DROP CONSTRAINT [LugarEjecucionContrato_IdMunicipio_FK]
END


IF OBJECT_ID('LugarEjecucionContrato_IdDepartamento_FK', 'F') IS NOT NULL 
BEGIN
ALTER TABLE [CONTRATO].[LugarEjecucionContrato] DROP CONSTRAINT [LugarEjecucionContrato_IdDepartamento_FK]
END


--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[LugarEjecucionContrato]') AND type in (N'U'))
--BEGIN
/****** Object:  Table [CONTRATO].[LugarEjecucionContrato]    Script Date: 15/07/2014 09:12:19 a.m. ******/
DROP TABLE [CONTRATO].[LugarEjecucionContrato]
END
GO

/****** Object:  Table [CONTRATO].[LugarEjecucionContrato]    Script Date: 15/07/2014 09:12:19 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CONTRATO].[LugarEjecucionContrato](
	[IdLugarEjecucionContratos] [int] IDENTITY(1,1) NOT NULL,
	[IdContrato] [int] NOT NULL,
	[IdDepartamento] [int] NULL,
	[IdRegional] [int] NULL,
	[NivelNacional] [bit] NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
 CONSTRAINT [PK_LugarEjecucionContrato] PRIMARY KEY CLUSTERED 
(
	[IdLugarEjecucionContratos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [CONTRATO].[LugarEjecucionContrato]  WITH CHECK ADD  CONSTRAINT [LugarEjecucionContrato_IdDepartamento_FK] FOREIGN KEY([IdDepartamento])
REFERENCES [DIV].[Departamento] ([IdDepartamento])
GO

ALTER TABLE [CONTRATO].[LugarEjecucionContrato] CHECK CONSTRAINT [LugarEjecucionContrato_IdDepartamento_FK]
GO

ALTER TABLE [CONTRATO].[LugarEjecucionContrato]  WITH CHECK ADD  CONSTRAINT [LugarEjecucionContrato_IdMunicipio_FK] FOREIGN KEY([IdRegional])
REFERENCES [DIV].[Municipio] ([IdMunicipio])
GO

ALTER TABLE [CONTRATO].[LugarEjecucionContrato] CHECK CONSTRAINT [LugarEjecucionContrato_IdMunicipio_FK]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es el id' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'IdLugarEjecucionContratos'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id Contrato' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'IdContrato'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id Departamento' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'IdDepartamento'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id Regional' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'IdRegional'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NivelNacional' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'NivelNacional'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario creador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'UsuarioCrea'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'FechaCrea'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario modificador del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'UsuarioModifica'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de modificación del registro' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato', @level2type=N'COLUMN',@level2name=N'FechaModifica'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Almacena datos sobre Lugar de ejecución de contratos' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'LugarEjecucionContrato'
GO


