USE [SIA]
GO

-- Autor: Carlos Andres Cardenas
-- Fecha: 2014-06-25 1755 
-- Descripcion: Se inserta los valores estados de la garantia

DELETE FROM [CONTRATO].[EstadosGarantias]
GO
SET IDENTITY_INSERT [CONTRATO].[EstadosGarantias] ON 

INSERT [CONTRATO].[EstadosGarantias] ([IDEstadosGarantias], [CodigoEstadoGarantia], [DescripcionEstadoGarantia], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'001', N'Aprobada', N'Administrador', CAST(0x0000A35500000000 AS DateTime), NULL, NULL)
INSERT [CONTRATO].[EstadosGarantias] ([IDEstadosGarantias], [CodigoEstadoGarantia], [DescripcionEstadoGarantia], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'002', N'Devuelta', N'Administrador', CAST(0x0000A35000000000 AS DateTime), NULL, NULL)
INSERT [CONTRATO].[EstadosGarantias] ([IDEstadosGarantias], [CodigoEstadoGarantia], [DescripcionEstadoGarantia], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'003', N'Pendiente', N'Administrador', CAST(0x0000A35000000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [CONTRATO].[EstadosGarantias] OFF
