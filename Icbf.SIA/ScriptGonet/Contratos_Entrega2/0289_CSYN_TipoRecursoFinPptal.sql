USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  22/05/2014 2:30 PM
-- Description:	Creacion sinonimo [BaseSIF].[TipoRecursoFinPptal]
-- =============================================

/****** Object:  Synonym [BaseSIF].[TipoRecursoFinPptal]    Script Date: 01/07/2014 16:35:59 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoRecursoFinPptal' AND schema_id = SCHEMA_ID(N'BaseSIF'))
DROP SYNONYM [BaseSIF].[TipoRecursoFinPptal]
GO

/****** Object:  Synonym [BaseSIF].[TipoRecursoFinPptal]    Script Date: 01/07/2014 16:35:59 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoRecursoFinPptal' AND schema_id = SCHEMA_ID(N'BaseSIF'))
CREATE SYNONYM [BaseSIF].[TipoRecursoFinPptal] FOR [NMF].[BaseSIF].[TipoRecursoFinPptal]
GO