USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Eliminar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Eliminar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/30/2014 12:23:44 PM
-- Description:	Procedimiento almacenado que elimina un(a) SupervisorInterContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Eliminar]
	@IDSupervisorIntervContrato INT
AS
BEGIN
DELETE CONTRATO.SupervisorInterContrato
WHERE IDSupervisorIntervContrato = @IDSupervisorIntervContrato
END
