USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Insert data [CONTRATO].[ModalidadSeleccion]
-- =============================================

IF NOT EXISTS (SELECT * FROM [CONTRATO].[ModalidadSeleccion] WHERE [CodigoModalidad] = 'CONT_DIR' AND [Nombre] = 'Contratación Directa')
BEGIN
SET IDENTITY_INSERT [CONTRATO].[ModalidadSeleccion] ON 
INSERT [CONTRATO].[ModalidadSeleccion] ([IdModalidad], [CodigoModalidad], [Nombre], [Sigla], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (3, N'CONT_DIR', N'Contratación Directa', N'CD', 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [CONTRATO].[ModalidadSeleccion] OFF
END 
