USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener]    Script Date: 09/08/2014 23:05:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener]    Script Date: 09/08/2014 23:05:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 08/07/2014
-- Description:	Obtiene los productos asociados a un plan de compras para ser utilizado en la pagina de registro
-- de contratos
-- =============================================
-- [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener] 1
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener] 
	@IDPlanDeComprasContratos INT
AS
BEGIN
	
	SELECT PPC.IDProductoPlanCompraContrato,
		PPC.IDProducto CodigoProducto,
		PPC.CantidadCupos,
		PPC.ValorProducto,
		PPC.Tiempo,
		PPC.UnidadTiempo,
		PPC.IdUnidadTiempo,
		PPC.IDPlanDeComprasContratos, 
		PPC.UsuarioCrea,
		PPC.FechaCrea,
		PPC.UsuarioModifica,
		PPC.FechaModifica,
		PCC.IDPlanDeCompras AS NumeroConsecutivo,
		PPC.IdDetalleObjeto 
	FROM Contrato.ProductoPlanCompraContratos PPC
	INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON PCC.IDPlanDeComprasContratos = PPC.IDPlanDeComprasContratos
	WHERE PPC.IDPlanDeComprasContratos = @IDPlanDeComprasContratos

END




GO


