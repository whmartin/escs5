USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[CategoriaContrato]') AND type in (N'U'))
BEGIN

	IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.CategoriaContrato') AND NAME = ('CodigoCategoriaContrato'))
	BEGIN
		ALTER TABLE CONTRATO.CategoriaContrato
		ADD CodigoCategoriaContrato nvarchar(30) NULL;
	END


END
ELSE
BEGIN

	CREATE TABLE [CONTRATO].[CategoriaContrato](
	[IdCategoriaContrato] [int] IDENTITY(1,1) NOT NULL,
	[CodigoCategoriaContrato] [nvarchar](30) NULL,
	[NombreCategoriaContrato] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](100) NULL,
	[Estado] [bit] NOT NULL,
	[UsuarioCrea] [nvarchar](250) NOT NULL,
	[FechaCrea] [datetime] NOT NULL,
	[UsuarioModifica] [nvarchar](250) NULL,
	[FechaModifica] [datetime] NULL,
	 CONSTRAINT [PK_CategoriaContrato] PRIMARY KEY CLUSTERED 
	(
		[IdCategoriaContrato] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	 CONSTRAINT [UQ_NombreCategoriaContrato] UNIQUE NONCLUSTERED 
	(
		[NombreCategoriaContrato] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key {0} Column' , @level0type=N'SCHEMA',@level0name=N'CONTRATO', @level1type=N'TABLE',@level1name=N'CategoriaContrato', @level2type=N'COLUMN',@level2name=N'IdCategoriaContrato'

END


