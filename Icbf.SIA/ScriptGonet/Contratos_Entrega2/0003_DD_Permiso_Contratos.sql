use SIA


/**************************************
-- Create date:	    2014-07-09
-- Author: gonet
-- Description: Limpiamos el menu de contratos
**************************************/



DELETE
from SEG.Permiso 
where IdPrograma IN (SELECT tempp.IdPrograma
from [SEG].[Programa] tempp
where tempp.CodigoPrograma LIKE '%Contrato%')

DELETE
from [SEG].[Programa]
where CodigoPrograma LIKE '%Contrato%'