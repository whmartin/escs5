USE [SIA]
GO
  

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ContratosCDP_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosCDP_Consultar]
GO

-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date: 26/05/2014 11:55:13 
-- Description:	Procedimiento almacenado que consulta Contratos relacionados al n�mero de CDP
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosCDP_Consultar]
	@IdCDP INT = NULL,
	@IdContrato INT = NULL

AS
BEGIN

SELECT [IdContratosCDP]
      ,[IdCDP]
      ,[IdContrato]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
  FROM [CONTRATO].[ContratosCDP]
 WHERE IdCDP = CASE WHEN @IdCDP IS NULL THEN IdCDP ELSE @IdCDP END 
 AND IdContrato = CASE WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato END 
 
END