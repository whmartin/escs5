USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 2:25:33 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ContratistaGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Insertar]
		@IDContratista_Garantias INT OUTPUT, 	@IDGarantia INT,	@IDEntidadProvOferente INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ContratistaGarantias(IDGarantia, IDEntidadProvOferente, UsuarioCrea, FechaCrea)
					  VALUES(@IDGarantia, @IDEntidadProvOferente, @UsuarioCrea, GETDATE())
	SELECT @IDContratista_Garantias = @@IDENTITY 		
END
