USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_Obtener]    Script Date: 30/07/2014 01:38:41 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_Contrato_Obtener]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Obtener]
GO


-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia

-- Create date: 30/07/2014
-- Description:	Se agregaron los campos ValorTotalCDP, ValorTotalAportesICBF, ValorAportesContratista 
-- =============================================
-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 22/06/2014
-- Description:	
-- =============================================
-- [dbo].[usp_RubOnline_Contrato_Contrato_Obtener] 1
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Obtener] 
	@IdContrato INT
AS
BEGIN

	Declare @ValorTotalCDP as numeric(32,8)
	Declare @ValorTotalAportesICBF numeric(32,8)
	Declare @ValorTotalAportesContratista numeric(32,8)
	Declare @ValorTotalContratosAdheridos numeric(32,8)

	SELECT 	@ValorTotalCDP=SUM(IECDP.ValorActualCDP) 
	FROM [CONTRATO].[ContratosCDP] CCDP
	INNER JOIN Ppto.InfoETLCDP IECDP ON IECDP.[IdEtlCDP] = CCDP.[IdCDP] 
	WHERE CCDP.[IdContrato] = @IdContrato

	SELECT @ValorTotalAportesICBF = SUM(AP.ValorAporte)
	FROM Contrato.AporteContrato AP
	WHERE AP.IdContrato = @IdContrato
	And AportanteICBF = 1

	SELECT @ValorTotalAportesContratista = SUM(AP.ValorAporte)
	FROM Contrato.AporteContrato AP
	WHERE AP.IdContrato = @IdContrato
	And AportanteICBF = 0

	select @ValorTotalContratosAdheridos = SUM(valorfinalcontrato) from contrato.contrato
	where idcontrato in (select idcontrato from contrato.contrato
	where fk_idcontrato= @IdContrato and convenioAdhesion =1)
	
	SELECT C.IdContrato, C.IdContratoAsociado, C.ConvenioMarco, C.ConvenioAdhesion, C.FK_IdContrato, C.NumeroContrato, C.IdEstadoContrato,
	C.IdCategoriaContrato, C.IdTipoContrato, C.IdModalidadAcademica, C.IdProfesion, C.IdModalidadSeleccion, C.IdNumeroProceso, 
	NP.NumeroProcesoGenerado AS NumeroProceso, C.FechaAdjudicacionDelProceso, 
	C.ActaDeInicio, C.ManejaAporte, C.ManejaRecurso, C.ConsecutivoPlanCompasAsociado, C.IdRegimenContratacion, 
	C.IdEmpleadoSolicitante, C.IdRegionalSolicitante, C.IdDependenciaSolicitante, C.IdCargoSolicitante,
	C.IdEmpleadoOrdenadorGasto, C.IdRegionalOrdenador, C.IdDependenciaOrdenador, C.IdCargoOrdenador,
	C.ObjetoDelContrato, C.AlcanceObjetoDelContrato, 
	(CASE WHEN C.ValorInicialContrato IS NULL THEN NULL ELSE CAST(C.ValorInicialContrato AS DECIMAL(30, 2)) END) AS ValorInicialContrato, 
	C.FechaInicioEjecucion, C.FechaFinalizacionIniciaContrato, C.FechaFinalTerminacionContrato, C.FechaSuscripcionContrato, 
	C.ManejaVigenciasFuturas, C.IdVigenciaInicial, C.IdVigenciaFinal, C.IdFormaPago, C.DatosAdicionaleslugarEjecucion, 
	C.FechaCrea, C.FechaModifica, C.UsuarioModifica, C.UsuarioCrea, @ValorTotalCDP as ValorTotalCDP, @ValorTotalAportesICBF as ValorTotalAportesICBF,  @ValorTotalAportesContratista as ValorAportesContratista,
	@ValorTotalContratosAdheridos as ValorTotalContratosAdheridos
	FROM Contrato.Contrato C
	LEFT OUTER JOIN CONTRATO.NumeroProcesos NP ON C.IdNumeroProceso = NP.IdNumeroProceso
	WHERE IdContrato = @IdContrato

END




