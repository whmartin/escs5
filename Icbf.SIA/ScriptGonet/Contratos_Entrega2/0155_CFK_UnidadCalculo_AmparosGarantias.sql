USE [SIA]
GO
-- =============================================
-- Author:		Gonet\Carlos Andr�s C�rdenas P.
-- Create date:  30/05/2014 05:10:00 PM
-- Description:	Creacion de llave for�nea FK_UnidadCalculo_AmparosGarantias
-- =============================================
IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Contrato].[FK_UnidadCalculo_AmparosGarantias]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [Contrato].[AmparosGarantias] DROP CONSTRAINT FK_UnidadCalculo_AmparosGarantias
END

ALTER TABLE [Contrato].[AmparosGarantias]
ADD CONSTRAINT FK_UnidadCalculo_AmparosGarantias FOREIGN KEY(IDUnidadCalculo)REFERENCES  [Contrato].[UnidadCalculo](IDUnidadCalculo)

