USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_AportesContrato_Obtener]    Script Date: 08/08/2014 02:40:05 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AportesContrato_Obtener]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AportesContrato_Obtener]
GO




-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 29/06/2014
-- Description:	Obtiene la informaci�n de los aportes asociados a un contrato seleccionados a partir de los filtros de entrada
-- (P�gina Registro Contratos)
-- =============================================
-- [dbo].[usp_SIA_Contrato_AportesContrato_Obtener] @AportanteICBF= 0, @IdContrato = 4, @IDEntidadProvOferente = 45951
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AportesContrato_Obtener]
	@AportanteICBF BIT = NULL, 
	@IdContrato INT = NULL,
	@IDEntidadProvOferente INT = NULL,
	@Estado BIT = NULL
AS
BEGIN

	SELECT IdAporteContrato, AportanteICBF, NumeroIdentificacionICBF, ValorAporte, DescripcionAporte, AporteEnDinero, 
	(CASE AporteEnDinero WHEN 0 THEN 'ESPECIE' ELSE 'DINERO' END) AS TipoAporte,
	IdContrato, IDEntidadProvOferente, T.NUMEROIDENTIFICACION AS NumeroIdentificacionContratista,
	(CASE TP.CodigoTipoPersona  
		WHEN '001' THEN (T.PRIMERNOMBRE + (CASE ISNULL(T.SEGUNDONOMBRE, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDONOMBRE + ' ' END) + T.PRIMERAPELLIDO + (CASE ISNULL(T.SEGUNDOAPELLIDO, '') WHEN '' THEN ' ' ELSE ' ' + T.SEGUNDOAPELLIDO END))
		ELSE T.RAZONSOCIAL END) AS InformacionAportante, 
	FechaRP, NumeroRP, AP.Estado, AP.UsuarioCrea, AP.FechaCrea, AP.UsuarioModifica, AP.FechaModifica,
	TD.NomTipoDocumento,
	TD.CodDocumento,
	TD.IdTipoDocumento
	FROM Contrato.AporteContrato AP
	LEFT OUTER JOIN PROVEEDOR.EntidadProvOferente EPO ON EPO.IdEntidad = AP.IDEntidadProvOferente
	LEFT OUTER JOIN Oferente.TERCERO T ON T.IDTERCERO = EPO.IdTercero 
	LEFT OUTER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona = TP.IdTipoPersona
	Left outer JOIN Global.TiposDocumentos TD  ON 
				 T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento 
	WHERE AportanteICBF = ISNULL(@AportanteICBF, AportanteICBF) 
	AND IdContrato = ISNULL(@IdContrato, IdContrato) 
	AND ISNULL(IDEntidadProvOferente, '') = ISNULL(@IDEntidadProvOferente, ISNULL(IDEntidadProvOferente, ''))
	AND AP.Estado = ISNULL(@Estado, AP.Estado)

END


