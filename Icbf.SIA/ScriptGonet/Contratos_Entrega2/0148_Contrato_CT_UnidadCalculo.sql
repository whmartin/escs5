USE [SIA]
GO
-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/3/2014 3:19:34 PM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[UnidadCalculo]') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla UnidadCalculo a crear'
RETURN
END
CREATE TABLE [Contrato].[UnidadCalculo](
 [IDUnidadCalculo] [INT]  IDENTITY(1,1) NOT NULL,
 [CodUnidadCalculo] [NVARCHAR] (8) NOT NULL,
 [Descripcion] [NVARCHAR] (80) NOT NULL,
 [Inactivo] [BIT]  ,
 [UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
 [FechaCrea]   [DATETIME]  NOT NULL,
 [UsuarioModifica] [NVARCHAR] (250),
 [FechaModifica]   [DATETIME]  ,
 CONSTRAINT [PK_UnidadCalculo] PRIMARY KEY CLUSTERED 
(
 	[IDUnidadCalculo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Hace referencia a la unidad de calculo posibles valores salario minimo, porcentaje', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'IDUnidadCalculo' AND [object_id] = OBJECT_ID('Contrato.UnidadCalculo')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Es el id', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo,
@level2type = N'COLUMN', @level2name = IDUnidadCalculo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'CodUnidadCalculo' AND [object_id] = OBJECT_ID('Contrato.UnidadCalculo')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Código Unidad Calculo', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo,
@level2type = N'COLUMN', @level2name = CodUnidadCalculo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Descripcion' AND [object_id] = OBJECT_ID('Contrato.UnidadCalculo')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Descripción', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo,
@level2type = N'COLUMN', @level2name = Descripcion;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'Inactivo' AND [object_id] = OBJECT_ID('Contrato.UnidadCalculo')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Inactivo', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo,
@level2type = N'COLUMN', @level2name = Inactivo;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioCrea' AND [object_id] = OBJECT_ID('Contrato.UnidadCalculo')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario creador del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo,
@level2type = N'COLUMN', @level2name = UsuarioCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaCrea' AND [object_id] = OBJECT_ID('Contrato.UnidadCalculo')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de creación del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo,
@level2type = N'COLUMN', @level2name = FechaCrea;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'UsuarioModifica' AND [object_id] = OBJECT_ID('Contrato.UnidadCalculo')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Usuario modificador del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo,
@level2type = N'COLUMN', @level2name = UsuarioModifica;
END

IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('Contrato.UnidadCalculo') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'FechaModifica' AND [object_id] = OBJECT_ID('Contrato.UnidadCalculo')))
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Fecha de modificación del registro', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = UnidadCalculo,
@level2type = N'COLUMN', @level2name = FechaModifica;
END

GO
