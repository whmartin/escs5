use SIA
GO
-- =============================================
-- Author:		Gonet
-- Create date: 25/08/2014
-- Description: Alter table [CONTRATO].[TipoObligacion]
-- =============================================


IF not EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE NAME = N'UQ_NombreTipoObligacion')
BEGIN	
	
ALTER TABLE [CONTRATO].[TipoObligacion]
ADD
 CONSTRAINT [UQ_NombreTipoObligacion] UNIQUE NONCLUSTERED 
(
	[NombreTipoObligacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END


