USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Eliminacion table [CONTRATO].[AmparosAsociados]
-- =============================================

/****** Object:  Table [CONTRATO].[AmparosAsociados]    Script Date: 23/05/2014 16:50:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[AmparosAsociados]') AND type in (N'U'))
DROP TABLE [CONTRATO].[AmparosAsociados]
GO
