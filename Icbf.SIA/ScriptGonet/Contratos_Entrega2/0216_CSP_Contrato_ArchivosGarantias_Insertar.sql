USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ArchivosGarantias_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantias_Insertar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 10:24:56 AM
-- Description:	Procedimiento almacenado que guarda un nuevo ArchivosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantias_Insertar]
		@IDArchivosGarantias INT OUTPUT, 	@IDArchivo INT,	@IDGarantia INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ArchivosGarantias(IDArchivo, IDGarantia, UsuarioCrea, FechaCrea)
					  VALUES(@IDArchivo, @IDGarantia, @UsuarioCrea, GETDATE())
	SELECT @IDArchivosGarantias = @@IDENTITY 		
END
