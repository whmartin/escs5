USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Modificar]    Script Date: 01/07/2014 16:03:02 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoFormaPago_Modificar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Modificar]
GO

/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoFormaPago_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPago_Modificar]
		@IdTipoFormaPago INT,	@NombreTipoFormaPago NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoFormaPago
	SET	NombreTipoFormaPago = @NombreTipoFormaPago,
		Descripcion = @Descripcion,
		Estado = @Estado,
		UsuarioModifica = @UsuarioModifica,
		FechaModifica = GETDATE()
	WHERE IdTipoFormaPago = @IdTipoFormaPago
END

