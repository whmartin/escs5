USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_TipoSuperInters_Consultar]    Script Date: 17/09/2014 05:21:50 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_TipoSuperInters_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_TipoSuperInters_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_TipoSuperInters_Consultar]    Script Date: 17/09/2014 05:21:50 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/30/2014 12:06:42 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSuperInter
--Actualizacion: Se adiciona en la consulta el campo estado
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_TipoSuperInters_Consultar]
	@Codigo NVARCHAR(4) = NULL,@Descripcion NVARCHAR(100) = NULL
AS
BEGIN
SELECT
	IDTipoSuperInter,
	Codigo,
	Descripcion,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica,
	Estado
FROM [CONTRATO].[TipoSuperInter]
WHERE Codigo =
	CASE
		WHEN @Codigo IS NULL THEN Codigo ELSE @Codigo
	END AND Descripcion =
	CASE
		WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
	END
END


