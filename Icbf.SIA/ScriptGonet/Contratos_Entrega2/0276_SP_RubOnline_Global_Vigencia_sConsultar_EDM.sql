USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]    Script Date: 27/06/2014 04:12:56 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Global_Vigencia_sConsultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]
GO
-- =============================================
-- Author:		GONET/Efrain Diaz Mejia
-- Create date:  09/05/2014 18:20:03
-- Description:	Procedimiento almacenado que consulta un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]
	@AcnoVigencia INT = NULL,@Activo NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Global].[Vigencia] WHERE AcnoVigencia = CASE WHEN @AcnoVigencia IS NULL THEN AcnoVigencia ELSE @AcnoVigencia END
 AND Activo = CASE WHEN @Activo IS NULL THEN Activo ELSE @Activo END
 Order By AcnoVigencia
END



