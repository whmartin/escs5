USE [SIA]
GO

/********************************
2014-05-22
Creamos el SYNONYM AreasxRubro 
jorge Vizcaino
********************************/

/****** Object:  Synonym [Ppto].[AreasxRubro]    Script Date: 22/05/2014 11:45:23 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'AreasxRubro' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[AreasxRubro]
GO

/****** Object:  Synonym [Ppto].[AreasxRubro]    Script Date: 22/05/2014 11:45:23 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'AreasxRubro' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[AreasxRubro] FOR [NMF].[Ppto].[AreasxRubro]
GO


