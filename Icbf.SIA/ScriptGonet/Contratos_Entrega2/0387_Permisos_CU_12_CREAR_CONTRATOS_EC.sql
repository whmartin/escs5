USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Emilio Calapi�a
-- Create date:  12/05/2014 11:30:14 PM
-- Description:	Insertar Tabla permiso para visualizar en el men�
-- =============================================
IF not exists (SELECT * FROM [SEG].[Programa] PROG
INNER JOIN SEG.Permiso PRM ON PRM.IdPrograma = PROG.IdPrograma
WHERE PROG.[CodigoPrograma] = 'Contratos/Contratos'
AND PRM.IdRol = 1)
begin
	declare @IdPrograma int = null

	select @IdPrograma = IdPrograma from [SEG].[Programa] where [CodigoPrograma] = 'Contratos/Contratos'
	
	If @IdPrograma is not null
	begin
	
		INSERT INTO SEG.Permiso
					   ([IdPrograma]
					   ,[IdRol]
					   ,[Insertar]
					   ,[Modificar]
					   ,[Eliminar]
					   ,[Consultar]
					   ,[UsuarioCreacion]
					   ,[FechaCreacion]
					   ,[UsuarioModificacion]
					   ,[FechaModificacion]
					   )
				 VALUES
					   (@IdPrograma
					   ,1
					   ,1
					   ,1
					   ,1
					   ,1
					   ,'Administrador'
					   ,GETDATE()
					   ,NULL
					   ,NULL
					   )
	End	   
	

END