USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar]    Script Date: 20/08/2014 12:10:25 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar]
GO

-- ==========================================================================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/22/2014 1:36:08 PM
-- Description:	Procedimiento almacenado que guarda un nuevo SolicitudModPlanCompras
-- ==========================================================================================
-- ==========================================================================================
-- Author:		Wilmer Alvarez Tirado
-- Create date: 20/08/2014 11:00
-- Description:	Se retira el par�metro fecha solicitud
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar]
@IdSolicitudModPlanCompra int output, 	
@Justificacion nvarchar(896),	
@NumeroSolicitud int,	
@IdEstadoSolicitud nvarchar(1),	
@IdUsuarioSolicitud int,	
@IdPlanDeCompras int,		
@UsuarioCrea nvarchar(250)
as
begin
		insert into CONTRATO.SolicitudModPlanCompras
				(	Justificacion, 
					NumeroSolicitud, 
					FechaSolicitud, 
					IdEstadoSolicitud, 
					IdUsuarioSolicitud, 
					IdPlanDeCompras, 
					UsuarioCrea, 
					FechaCrea)
		values	(	@Justificacion, 
					@NumeroSolicitud, 
					getdate(), 
					@IdEstadoSolicitud, 
					@IdUsuarioSolicitud, 
					@IdPlanDeCompras, 
					@UsuarioCrea, 
					getdate())

		select @IdSolicitudModPlanCompra = scope_identity()
end
go