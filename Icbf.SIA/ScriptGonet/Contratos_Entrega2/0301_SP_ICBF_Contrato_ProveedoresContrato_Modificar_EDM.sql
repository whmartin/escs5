USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_ICBF_Seg_ModificarModulo]    Script Date: 26/06/2014 12:37:12 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ICBF_Contrato_ProveedoresContrato_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ICBF_Contrato_ProveedoresContrato_Modificar]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  03/07/2014 08:47:00
-- Description:	Procedimiento almacenado que Suscribe un contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Contrato_ProveedoresContrato_Modificar]
		@IdProveedoresContratos int,
		@IdProveedores int,
		@IdFormaPago INT,
		@UsuarioModifica Nvarchar(250)
					
AS
Begin
	
	UPDATE [CONTRATO].[ProveedoresContratos]
	   SET [IDFormaPago] = @IdFormaPago
		  ,[UsuarioModifica] = @UsuarioModifica
		  ,[FechaModifica] = Getdate()
      
	 WHERE IdProveedoresContratos = @IdProveedoresContratos
	 AND IdProveedores = @IdProveedores

End