USE [SIA]
GO
-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  28/01/2014 11:32:56 PM
-- Description:	Insertar Tabla permiso para visualizar en el men� de OAC la opci�n de acuerdos
-- =============================================
IF not exists (SELECT * FROM [SEG].[Programa] PROG
INNER JOIN SEG.Permiso PRM ON PRM.IdPrograma = PROG.IdPrograma
WHERE PROG.NombrePrograma LIKE '%Asociar RP a un Contrato%')
begin
	declare @IdPrograma int = null

	select @IdPrograma = IdPrograma from [SEG].[Programa] where [NombrePrograma] = 'Asociar RP a un Contrato'
	
	If @IdPrograma is not null
	begin
	
		INSERT INTO SEG.Permiso
					   ([IdPrograma]
					   ,[IdRol]
					   ,[Insertar]
					   ,[Modificar]
					   ,[Eliminar]
					   ,[Consultar]
					   ,[UsuarioCreacion]
					   ,[FechaCreacion]
					   ,[UsuarioModificacion]
					   ,[FechaModificacion]
					   )
				 VALUES
					   (@IdPrograma
					   ,1
					   ,1
					   ,1
					   ,1
					   ,1
					   ,'Administrador'
					   ,GETDATE()
					   ,NULL
					   ,NULL
					   )
	End	   
END