USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Insertar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Insertar]
GO


-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Proveedores_Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_Insertar]
		@IdProveedoresContratos INT OUTPUT, 	@IdProveedores INT,	@IdContrato INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO CONTRATO.ProveedoresContratos (IdProveedores, IdContrato, UsuarioCrea, FechaCrea)
		VALUES (@IdProveedores, @IdContrato, @UsuarioCrea, GETDATE())
	SELECT
		@IdProveedoresContratos = @@IDENTITY
 		
END
