USE [SIA]
GO

IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Consultar]')
			AND type IN (
				N'P'
				,N'PC'
				)
		)
	DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Consultar]
GO

-- =============================================
-- Author: Eduardo Isaac Ballesteros	
-- Create date:  7/4/2014 9:40:33 AM
-- Description:	Procedimiento almacenado que consulta un(a) VigenciaFuturas
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_VigenciaFuturas_Consultar] @IDVigenciaFuturas INT
AS
BEGIN
	SELECT IDVigenciaFuturas
		,NumeroRadicado
		,FechaExpedicion
		,IdContrato
		,ValorVigenciaFutura
		,UsuarioCrea
		,FechaCrea
		,UsuarioModifica
		,FechaModifica
	FROM [CONTRATO].[VigenciaFuturas]
	WHERE IDVigenciaFuturas = @IDVigenciaFuturas
END

