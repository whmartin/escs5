USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Consultar]    Script Date: 21/05/2014 03:11:14 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_NumeroProcesosPorContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesosPorContrato_Consultar]
GO

-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  5/7/2014 5:55:13 PM
-- Description:	Procedimiento almacenado que consulta NumeroProcesos por contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesosPorContrato_Consultar]
	@IdNumeroProceso INT
AS
BEGIN
 SELECT  CON.IdContrato, NP.NumeroProceso
 FROM [CONTRATO].[Contrato] CON
 inner join  [CONTRATO].[NumeroProcesos] NP on NP.IdNumeroProceso = CON.IdNumeroProceso
 WHERE  NP.IdNumeroProceso = @IdNumeroProceso
END


