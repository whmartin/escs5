USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Regionals_Consultar]    Script Date: 02/06/2014 15:09:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_KACTUS_KPRODII_Regionals_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_KACTUS_KPRODII_Regionals_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_KACTUS_KPRODII_Regionals_Consultar]    Script Date: 02/06/2014 15:09:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet
-- Create date: 2014-07-09
-- Description:	Seleccionar registros de kactus
-- =============================================
CREATE PROCEDURE [dbo].[usp_KACTUS_KPRODII_Regionals_Consultar] 
	
AS
BEGIN
	SELECT DISTINCT ID_regio, regional from [KACTUS].[KPRODII].[dbo].[Da_Emple]
END

GO


