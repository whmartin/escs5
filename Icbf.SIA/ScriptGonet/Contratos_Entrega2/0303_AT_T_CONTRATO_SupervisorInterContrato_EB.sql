USE [SIA]
GO
-- =============================================
-- Author: Eduardo Isaac Ballesteros Muñoz
-- Create date: 7/1/2014 11:39:06 AM
-- Description: Adición del campo IDDirectorInterventoria para la tabla CONTRATO.SupervisorInterContrato
-- =============================================
IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.SupervisorInterContrato') AND NAME = ('IDDirectorInterventoria'))
BEGIN
	ALTER TABLE CONTRATO.SupervisorInterContrato ADD IDDirectorInterventoria INT NULL
END
ELSE
BEGIN
 PRINT 'YA EXISTE EL CAMPO IDDirectorInterventoria'
END
