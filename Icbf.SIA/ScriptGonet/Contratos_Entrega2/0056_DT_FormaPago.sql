USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Eliminacion Tabla [CONTRATO].[FormaPago]
-- =============================================

/****** Object:  Table [CONTRATO].[FormaPago]    Script Date: 23/05/2014 15:44:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[FormaPago]') AND type in (N'U'))
DROP TABLE [CONTRATO].[FormaPago]
GO

