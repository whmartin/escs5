USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contratoss_Consultar]    Script Date: 11/08/2014 05:49:46 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Contratoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contratoss_Consultar]
GO

-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  6/16/2014 4:52:44 PM
-- Description:	Procedimiento almacenado que consulta un(a) Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contratoss_Consultar]
	@FechaRegistroSistemaDesde DATETIME = NULL,
	@FechaRegistroSistemaHasta DATETIME = NULL,
	@IdContrato INT = NULL,
	@NumeroContrato NVARCHAR(30) = NULL,
	@NumeroProceso NVARCHAR(30) = NULL,
	@VigenciaFiscalinicial INT = NULL,
	@IDRegional INT = NULL,
	@IDModalidadSeleccion INT = NULL,
	@IDCategoriaContrato INT = NULL,
	@IDTipoContrato INT = NULL,
	@IDEstadoContraro INT = NULL,
	@IdDependenciaSolicitante INT = Null,
	@ManejaRecurso Bit = null
AS
BEGIN

 SELECT distinct top 501  C.IdContrato, C.NumeroContrato, 
  C.IdNumeroProceso, C.IdVigenciaInicial, C.IdRegionalContrato, 
  C.IdModalidadSeleccion, C.IdCategoriaContrato, 
 C.IdTipoContrato, C.IdEstadoContrato, C.UsuarioCrea, 
 C.FechaCrea, C.UsuarioModifica, C.FechaModifica,
 Ltrim(rtrim(R.CodigoRegional)) +' - '+Ltrim(rtrim(R.NombreRegional)) as Regional,
 Ltrim(rtrim(MS.CodigoModalidad)) + ' - ' + Ltrim(rtrim(MS.Nombre)) as ModalidadSeleccion,
 Ltrim(rtrim(CC.CodigoCategoriaContrato)) + ' - ' + Ltrim(rtrim(CC.NombreCategoriaContrato)) AS CategoriaContrato,
 Ltrim(rtrim(TC.CodigoTipoContrato)) + ' - ' + Ltrim(rtrim(TC.NombreTipoContrato)) AS TipoContrato,
 Ltrim(rtrim(EC.Descripcion)) AS EstadoContrato,
 V.AcnoVigencia As Vigencia,
 NP.NumeroProcesoGenerado As numeroProceso,
 C.FechaAdjudicacionDelProceso,
 C.ValorInicialContrato,
 C.ValorFinalContrato,
 C.IdDependenciaSolicitante,
 EMP.nom_depe DependenciaSolicitante,
 C.ObjetoDelContrato,
 C.AlcanceObjetoDelContrato,
 C.ValorInicialContrato,
 ''ValorTotalAdiciones,
 ''ValorTotalReducciones,
 ''ValorContratosAdheridos,
 C.ValorFinalContrato,
 --AC.ValorAporte
 '' valorAportesICBF,
 ''ValorAportesContratista,
 C.FechaInicioEjecucion,
 ''FechaFirmaActaInicio,
 C.FechaFinalizacionIniciaContrato,
 ''PlazoInicialEjecucion,
 C.FechaSuscripcionContrato,
 ''Prorrogas,
 ''PlazoTotalEjecucion,
 C.[FechaFinalTerminacionContrato],
 ''FechaProyectadaLiquidacion,
 ''FechaLiquidacion,
 C.[ManejaVigenciasFuturas],
 VF.AcnoVigencia VigenciaFiscalFinal,
 ''FechaAnulacion,
 ''FormaPago,
 ''NivelNacional,
 ''DatosAdicionalesLugarEjecucuion,
 ''ValorTotalContratosAsociadosConvenioMacro
 FROM CONTRATO.Contrato C
 left outer join DIV.Regional R on C.IDRegionalContrato = R.IdRegional
 left outer join CONTRATO.ModalidadSeleccion MS on MS.IdModalidad = C.IdModalidadSeleccion
 left outer join CONTRATO.CategoriaContrato CC on CC.IdCategoriaContrato = C.IdCategoriaContrato
 left outer join CONTRATO.TipoContrato TC on TC.IdTipoContrato = C.IdTipoContrato
 left outer join Contrato.EstadoContrato EC on EC.IDEstadoContrato = C.IDEstadoContrato
 left outer join Global.Vigencia V on V.IdVigencia = C.IdVigenciaInicial
 left outer join Global.Vigencia VF on VF.IdVigencia = C.IdVigenciaFinal
 left outer join CONTRATO.NumeroProcesos NP on NP.IdNumeroProceso = C.IdNumeroProceso
 left outer join KACTUS.KPRODII.dbo.Da_Emple EMP on EMP.ID_depen = C.IdDependenciaSolicitante
 --left outer join CONTRATO.AporteContrato AC on AC.IdContrato = C.IdContrato
 WHERE C.FechaCrea >= CASE WHEN @FechaRegistroSistemaDesde IS NULL THEN C.FechaCrea ELSE @FechaRegistroSistemaDesde END 
 AND C.FechaCrea <= CASE WHEN @FechaRegistroSistemaHasta IS NULL THEN C.FechaCrea ELSE @FechaRegistroSistemaHasta END 
 AND ISNULL(C.IdContrato, 0) = CASE WHEN @IdContrato IS NULL THEN ISNULL(C.IdContrato, 0) ELSE @IdContrato END 
 AND ISNULL(C.NumeroContrato, '') = CASE WHEN @NumeroContrato IS NULL THEN ISNULL(C.NumeroContrato, '') ELSE @NumeroContrato END 
 AND ISNULL(C.IdNumeroProceso, '') = CASE WHEN @NumeroProceso IS NULL THEN ISNULL(C.IdNumeroProceso, '') ELSE @NumeroProceso END 
 AND ISNULL(C.IdVigenciaInicial, 0) = CASE WHEN @VigenciaFiscalinicial IS NULL THEN ISNULL(C.IdVigenciaInicial, 0) ELSE @VigenciaFiscalinicial END 
 AND ISNULL(C.IDRegionalContrato, 0) = CASE WHEN @IDRegional IS NULL THEN ISNULL(C.IDRegionalContrato, 0) ELSE @IDRegional END 
 AND ISNULL(C.IDModalidadSeleccion, 0) = CASE WHEN @IDModalidadSeleccion IS NULL THEN ISNULL(C.IDModalidadSeleccion, 0) ELSE @IDModalidadSeleccion END 
 AND ISNULL(C.IDCategoriaContrato, 0) = CASE WHEN @IDCategoriaContrato IS NULL THEN ISNULL(C.IDCategoriaContrato, 0) ELSE @IDCategoriaContrato END 
 AND ISNULL(C.IDTipoContrato, 0) = CASE WHEN @IDTipoContrato IS NULL THEN ISNULL(C.IDTipoContrato, 0) ELSE @IDTipoContrato END 
 AND ISNULL(C.IDEstadoContrato, 0) = CASE WHEN @IDEstadoContraro IS NULL THEN ISNULL(C.IDEstadoContrato, 0) ELSE @IDEstadoContraro END
 AND ISNULL(C.IdDependenciaSolicitante, 0) = CASE WHEN @IdDependenciaSolicitante IS NULL THEN ISNULL(C.IdDependenciaSolicitante, 0) ELSE @IdDependenciaSolicitante END 
 AND ISNULL(C.ManejaRecurso, 0) = CASE WHEN @ManejaRecurso IS NULL THEN ISNULL(C.ManejaRecurso, 0) ELSE @ManejaRecurso END 
END



