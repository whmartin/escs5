USE [SIA]
GO

IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Insertar]')
			AND type IN (
				N'P'
				,N'PC'
				)
		)
	DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Insertar]
GO

-- =============================================
-- Author: Eduardo Isaac Ballesteros Mu�oz
-- Create date: 7/1/2014 14:20:06 AM
-- Description:	Procedimiento almacenado que guarda un nuevo SupervisorInterContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContrato_Insertar] @IDSupervisorIntervContrato INT OUTPUT
	,@FechaInicio DATETIME
	,@Inactivo BIT
	,@Identificacion NVARCHAR(80)
	,@TipoIdentificacion NVARCHAR(4)
	,@IDTipoSuperInter INT
	,@IdNumeroContratoInterventoria INT = NULL
	,@IDProveedoresInterventor INT = NULL
	,@IDEmpleadosSupervisor INT = NULL
	,@IDDirectorInterventoria INT = NULL
	,@IdContrato INT = NULL
	,@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO CONTRATO.SupervisorInterContrato (
		FechaInicio
		,Inactivo
		,Identificacion
		,TipoIdentificacion
		,IDTipoSuperInter
		,IdNumeroContratoInterventoria
		,IDProveedoresInterventor
		,IDEmpleadosSupervisor
		,IdContrato
		,IDDirectorInterventoria
		,UsuarioCrea
		,FechaCrea
		)
	VALUES (
		@FechaInicio
		,@Inactivo
		,@Identificacion
		,@TipoIdentificacion
		,@IDTipoSuperInter
		,@IdNumeroContratoInterventoria
		,@IDProveedoresInterventor
		,@IDEmpleadosSupervisor
		,@IdContrato
		,@IDDirectorInterventoria
		,@UsuarioCrea
		,GETDATE()
		)

	SELECT @IDSupervisorIntervContrato = @@IDENTITY
END
