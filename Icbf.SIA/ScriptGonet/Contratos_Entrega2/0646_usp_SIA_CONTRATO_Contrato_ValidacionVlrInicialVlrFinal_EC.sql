USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 09/24/2014 12:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 09/24/2014 12:05:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 02/06/2014
-- Description:	
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal] 42, 600000000, 56
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
	@IdContratoConvMarco INT,
	@ValorInicialContConv DECIMAL, 
	@IdContrato INT

AS
BEGIN
	
	DECLARE @ValorFinalContratoConvMarco DECIMAL, @ValorInicialContratoConvMarco DECIMAL, @SumValorFinalContratos DECIMAL
	SELECT @ValorInicialContratoConvMarco=ValorInicialContrato 
	FROM CONTRATO.Contrato WHERE IdContrato = @IdContratoConvMarco

	SET @ValorFinalContratoConvMarco = @ValorInicialContratoConvMarco;
	print cast(@ValorFinalContratoConvMarco as varchar)

	SELECT @SumValorFinalContratos=SUM(ISNULL(ValorFinalContrato, 0))  
	FROM CONTRATO.Contrato WHERE FK_IdContrato = @IdContratoConvMarco AND IdContrato <> @IdContrato
	print cast(@SumValorFinalContratos as varchar)

	-- Se quita de la ecuacion sumValorFinalContratos por correccion ICBF 14/09/2014 
	IF ((@ValorFinalContratoConvMarco /*+ @SumValorFinalContratos*/) >= (@SumValorFinalContratos + @ValorInicialContConv))
	BEGIN

		SELECT @IdContratoConvMarco AS IdContrato
		RETURN 

	END


END



GO


