USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_EmpleadosICBFlupa_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].usp_SIA_CONTRATO_EmpleadosICBFlupa_Consultar
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_EmpleadosICBFlupa_Consultar]    Script Date: 24/06/2014 11:29:08 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jorge Emilio Calapina
-- Create date: 27/05/2014
-- Description:	Consulta los empleados que coincidan con los filtros dados
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_EmpleadosICBFlupa_Consultar] 
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_EmpleadosICBFlupa_Consultar] 
	@IdTipoIdentificacion VARCHAR(10) = NULL, @NumeroIdentificacion VARCHAR(11) = NULL, @IdTipoVinculacionContractual VARCHAR(50) = NULL, 
	@IdRegional VARCHAR(50) = NULL, @PrimerNombre VARCHAR(50) = NULL, @SegundoNombre VARCHAR(50) = NULL, @PrimerApellido VARCHAR(50) = NULL, 
	@SegundoApellido VARCHAR(50) = NULL
AS
BEGIN
	IF @IdTipoIdentificacion IS NOT NULL
	BEGIN
		IF @IdTipoIdentificacion = 'CC'
		BEGIN
			SET @IdTipoIdentificacion = 'C'
		END
		ELSE IF @IdTipoIdentificacion = 'CE'
		BEGIN
			SET @IdTipoIdentificacion = 'CE'
		END
	END


	SELECT desc_ide AS TipoIdentificacion, num_iden AS NumeroIdentificacion, 
	desc_vin AS TipoVinculacionContractual, 
	nom_emp1 + (CASE ISNULL(nom_emp2, '') WHEN '' THEN ' ' ELSE ' ' + nom_emp2 + ' ' END) + ape_emp1 + (CASE ISNULL(ape_emp2, '') WHEN '' THEN ' ' ELSE ' ' + ape_emp2 END) AS NombreEmpleado, 
	regional AS Regional, nom_depe AS Dependencia,  
	desc_car AS Cargo,
	NULL AS IdTipoIdentificacion, NULL AS IdTipoVinculacionContractual, 
	ID_regio AS IdRegional, ID_depen AS IdDependencia, ID_Carg AS IdCargo,
	nom_emp1 AS PrimerNombre, ISNULL(nom_emp2, '') AS SegundoNombre, ape_emp1 AS PrimerApellido, 
	ISNULL(ape_emp2, '') AS SegundoApellido, direccion AS Direccion, telefono AS Telefono
	FROM [KACTUS].[KPRODII].[dbo].[Da_Emple]
	WHERE RTRIM(ID_ident) = ISNULL(@IdTipoIdentificacion, ID_ident)
	AND num_iden = ISNULL(@NumeroIdentificacion, num_iden)
	AND CAST(ID_vincu AS VARCHAR(50)) = ISNULL(@IdTipoVinculacionContractual, CAST(ID_vincu AS VARCHAR(50))) 
	AND ID_regio = ISNULL(@IdRegional, ID_regio)
	AND nom_emp1 = ISNULL(@PrimerNombre, nom_emp1)
	AND nom_emp2 = ISNULL(@SegundoNombre, nom_emp2)
	AND ape_emp1 = ISNULL(@PrimerApellido, ape_emp1)
	AND ape_emp2 = ISNULL(@SegundoApellido, ape_emp2)
	

END


GO


