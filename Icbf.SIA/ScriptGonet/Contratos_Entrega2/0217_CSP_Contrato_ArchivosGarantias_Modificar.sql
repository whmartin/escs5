USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ArchivosGarantias_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantias_Modificar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 10:24:57 AM
-- Description:	Procedimiento almacenado que actualiza un(a) ArchivosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ArchivosGarantias_Modificar]
		@IDArchivosGarantias INT,	@IDArchivo INT,	@IDGarantia INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ArchivosGarantias SET IDArchivo = @IDArchivo, IDGarantia = @IDGarantia, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IDArchivosGarantias = @IDArchivosGarantias
END
