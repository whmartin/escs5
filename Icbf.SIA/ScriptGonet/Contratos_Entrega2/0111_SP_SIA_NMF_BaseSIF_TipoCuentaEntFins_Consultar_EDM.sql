USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_NMF_BaseSIF_TipoCuentaEntFins_Consultar]    Script Date: 29/05/2014 09:09:12 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_NMF_BaseSIF_TipoCuentaEntFins_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_NMF_BaseSIF_TipoCuentaEntFins_Consultar]
GO


-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  29/05/2014 10:16
-- Description:	Procedimiento almacenado que consulta un(a) TipoCuentaEntFin
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_NMF_BaseSIF_TipoCuentaEntFins_Consultar]
	@CodTipoCta NVARCHAR(50) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCta, CodTipoCta, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [BaseSIF].[TipoCuentaEntFin] 
 WHERE CodTipoCta = CASE WHEN @CodTipoCta IS NULL THEN CodTipoCta ELSE @CodTipoCta END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END



