USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_Contrato_PlanComprasProductos_Obtener')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener]    Script Date: 09/07/2014 12:41:58 p.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener]    Script Date: 09/07/2014 12:41:58 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 08/07/2014
-- Description:	Obtiene los productos asociados a un plan de compras para ser utilizado en la pagina de registro
-- de contratos
-- =============================================
-- [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener] 1
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Obtener] 
	@IDPlanDeComprasContratos INT
AS
BEGIN
	
	SELECT PPC.IDProductoPlanCompraContrato,
		PPC.IDProducto CodigoProducto,
		PPC.CantidadCupos,
		PPC.IDPlanDeComprasContratos, 
		PPC.UsuarioCrea,
		PPC.FechaCrea,
		PPC.UsuarioModifica,
		PPC.FechaModifica,
		PCC.IDPlanDeCompras AS NumeroConsecutivo
	FROM Contrato.ProductoPlanCompraContratos PPC
	INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON PCC.IDPlanDeComprasContratos = PPC.IDPlanDeComprasContratos
	WHERE PPC.IDPlanDeComprasContratos = @IDPlanDeComprasContratos

END


GO


