USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ContratistaGarantias_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Eliminar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 2:25:34 PM
-- Description:	Procedimiento almacenado que elimina un(a) ContratistaGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Eliminar]
	@IDContratista_Garantias INT
AS
BEGIN
	DELETE Contrato.ContratistaGarantias WHERE IDContratista_Garantias = @IDContratista_Garantias
END
