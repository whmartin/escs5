USE SIA
GO
-- =============================================
-- Author:		Jos� Ignacio De Los Reyes
-- Create date: 06/27/2014 14:11:00 PM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Contrato.ValoresContrato') AND type in (N'U'))
BEGIN
Print 'Ya Existe La tabla ValoresContrato a crear'
RETURN
END
CREATE TABLE Contrato.ValoresContrato(
 IdValoresContrato INT IDENTITY(1,1)  ,
 Desde Numeric(18,2),
 Hasta Numeric(18,2),
 UsuarioCrea NVARCHAR (250)  NOT NULL,
 FechaCrea   DATETIME  NOT NULL,
 UsuarioModifica NVARCHAR (250),
 FechaModifica   DATETIME 
 CONSTRAINT [PK_ValoresContrato] PRIMARY KEY CLUSTERED 
(
	IdValoresContrato
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT NULL FROM SYS.EXTENDED_PROPERTIES WHERE major_id = OBJECT_ID('Contrato.ValoresContrato') AND name = N'MS_Description' AND minor_id = 0)
BEGIN
EXEC sys.sp_addextendedproperty
@name = N'MS_Description', 
@value = N'Valores Contratos', 
@level0type = N'SCHEMA', @level0name = Contrato, 
@level1type = N'TABLE',  @level1name = ValoresContrato;
END
GO

DELETE Contrato.ValoresContrato

DBCC CHECKIDENT('Contrato.ValoresContrato',RESEED,0)

INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (1,1000000, GETDATE(),'Administrador')
INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (1000000,5000000, GETDATE(),'Administrador')
INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (5000000,10000000, GETDATE(),'Administrador')
INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (10000000,20000000, GETDATE(),'Administrador')
INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (20000000,30000000, GETDATE(),'Administrador')
INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (30000000,70000000, GETDATE(),'Administrador')
INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (70000000,200000000, GETDATE(),'Administrador')
INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (200000000,500000000, GETDATE(),'Administrador')
INSERT INTO Contrato.ValoresContrato(Desde,Hasta,FechaCrea,UsuarioCrea) VALUES (500000000,9999999999999, GETDATE(),'Administrador')


