USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_NumeroProcesosPorContrato_Consultar]    Script Date: 27/06/2014 03:07:39 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_NumeroProcesosPorContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesosPorContrato_Consultar]
GO

-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  27/06/2014 15:13
-- Description:	Procedimiento almacenado que consulta NumeroProcesos por contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesosPorContrato_Consultar]
	@IdNumeroProceso INT
AS
BEGIN
 SELECT NP.[IdNumeroProceso]
      ,NP.[NumeroProceso]
      ,NP.[Inactivo]
      ,NP.[NumeroProcesoGenerado]
      ,NP.[IdModalidadSeleccion]
      ,NP.[IdRegional]
      ,NP.[IdVigencia]
      ,NP.[UsuarioCrea]
      ,NP.[FechaCrea]
      ,NP.[UsuarioModifica]
      ,NP.[FechaModifica],
   CON.IdContrato, NP.NumeroProceso
 FROM [CONTRATO].[Contrato] CON
 inner join  [CONTRATO].[NumeroProcesos] NP on NP.IdNumeroProceso = CON.IdNumeroProceso
 WHERE  NP.IdNumeroProceso = @IdNumeroProceso
END




