USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  27/06/2014 09:26 
-- Description:	Insert tabla [CONTRATO].[EstadoContrato] para mostrar opci�n men�.
-- =============================================
IF not exists(SELECT [CodEstado] from [CONTRATO].[EstadoContrato] where [Descripcion] = 'Suscrito')

Begin

INSERT INTO [CONTRATO].[EstadoContrato]
           ([CodEstado]
           ,[Descripcion]
           ,[Inactivo]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
     VALUES
           ('SUS'
           ,'Suscrito'
           ,1
           ,'Administrador'
           ,GetDate()
           ,null
           ,null)
END


