USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_UnidadCalculo_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculo_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/3/2014 3:19:34 PM
-- Description:	Procedimiento almacenado que consulta un(a) UnidadCalculo
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_UnidadCalculo_Consultar]
	@IDUnidadCalculo INT
AS
BEGIN
 SELECT IDUnidadCalculo, CodUnidadCalculo, Descripcion, Inactivo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[UnidadCalculo] WHERE  IDUnidadCalculo = @IDUnidadCalculo
END
