USE [SIA]
GO
-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:36 PM
-- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contrato].[RubroPlanComprasContrato]') AND type in (N'U'))
BEGIN
	drop table Contrato.RubroPlanComprasContrato
END
GO

CREATE TABLE Contrato.RubroPlanComprasContrato
(
	 IDRubroPlanComprasContrato INT IDENTITY(1,1) NOT NULL,
	 ValorRubroPresupuestal		NUMERIC(30,2) NULL,
	 IDPlanDeComprasContratos	INT NULL,
	 IDRubro					NVARCHAR(256) NULL,
	 UsuarioCrea				NVARCHAR (250)  NOT NULL,
	 FechaCrea					DATETIME  NOT NULL,
	 UsuarioModifica			NVARCHAR (250),
	 FechaModifica				DATETIME
)
go

-- Add keys for table RubroPlanComprasContrato
ALTER TABLE Contrato.RubroPlanComprasContrato
ADD CONSTRAINT PK_Contrato_RubroPlanComprasContrato
PRIMARY KEY(IDRubroPlanComprasContrato)
GO
ALTER TABLE Contrato.RubroPlanComprasContrato
ADD CONSTRAINT FK_RubroPlanComprasContrato_PlanDeComprasContratos
FOREIGN KEY(IDPlanDeComprasContratos)
REFERENCES CONTRATO.PlanDeComprasContratos(IDPlanDeComprasContratos)
GO


