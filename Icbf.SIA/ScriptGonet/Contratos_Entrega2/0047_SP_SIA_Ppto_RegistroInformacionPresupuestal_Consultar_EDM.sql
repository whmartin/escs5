USE [SIA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]
GO

-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Create date:  22/05/2014 2:30 PM
-- Description:	Procedimiento almacenado que Muestra la información relacionada con el certificado de disponibilidad presupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]
	@NumeroCDP INT = null,
	@VigenciaFiscal INT = null,
	@RegionalICBF INT = null,
	@Area INT = null,
	@ValorTotalDesde INT = null,
	@ValorTotalHasta INT = null,
	@FechaCDPDesde DATETIME = null,
	@FechaCDPHasta DATETIME = null

AS
BEGIN
		select  * 
		from Ppto.InfoETLCDP IECDP
		Inner Join Ppto.AreasxRubro APR on APR.IdVigencia = IECDP.IdVigencia and APR.IdCatalogo = IECDP.IdCatRubro
		Inner Join Ppto.AreasInternas AI On AI.IdAreasInternas = APR.IdAreasInternas
		Where IECDP.CDP = CASE WHEN @NumeroCDP IS NULL THEN IECDP.CDP ELSE @NumeroCDP END 
		And IECDP.IdVigencia = CASE WHEN @VigenciaFiscal IS NULL THEN IECDP.IdVigencia ELSE @VigenciaFiscal END 
		And IECDP.IdVigencia = CASE WHEN @VigenciaFiscal IS NULL THEN IECDP.IdVigencia ELSE @VigenciaFiscal END 
		And IECDP.IdRegionalPCI = CASE WHEN @RegionalICBF IS NULL THEN IECDP.IdRegionalPCI ELSE @RegionalICBF END 
		And IECDP.ValorActualCDP >= CASE WHEN @ValorTotalDesde IS NULL THEN IECDP.ValorActualCDP ELSE @ValorTotalDesde END 
		And IECDP.ValorActualCDP <= CASE WHEN @FechaCDPHasta IS NULL THEN IECDP.ValorActualCDP ELSE @FechaCDPHasta END 
		And IECDP.FechaSolicitudCDP >= CASE WHEN @FechaCDPDesde IS NULL THEN IECDP.FechaSolicitudCDP ELSE @FechaCDPDesde END 
		And IECDP.FechaSolicitudCDP <= CASE WHEN @FechaCDPHasta IS NULL THEN IECDP.FechaSolicitudCDP ELSE @FechaCDPHasta END 



		

  
END

GO