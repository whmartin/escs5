USE [SIA]
GO
-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  11/6/2014 8:59:59 AM
-- =============================================

IF NOT EXISTS(SELECT NAME FROM SYSCOLUMNS WHERE ID = OBJECT_ID('CONTRATO.Garantia') AND NAME = ('Estado'))
BEGIN
ALTER TABLE CONTRATO.Garantia
ADD Estado BIT NULL
END
GO