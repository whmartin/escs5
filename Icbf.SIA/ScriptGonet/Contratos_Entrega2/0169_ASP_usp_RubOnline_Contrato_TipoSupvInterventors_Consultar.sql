USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]    Script Date: 04/06/2014 12:07:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventors_Consultar]

	@Nombre NVARCHAR (128) = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT IDTipoSuperInter as IdTipoSupvInterventor,
		Descripcion as Nombre,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoSuperInter]
	WHERE Descripcion LIKE '%' + 
		CASE
			WHEN @Nombre IS NULL THEN Descripcion ELSE @Nombre
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)

END




