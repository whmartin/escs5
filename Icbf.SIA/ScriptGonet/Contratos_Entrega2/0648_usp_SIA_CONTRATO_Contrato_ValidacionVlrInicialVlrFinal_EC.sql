USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 10/17/2014 15:56:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]    Script Date: 10/17/2014 15:56:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 02/06/2014
-- Description:	Realiza la validacion del valor inicial sobre el valor final del cont conv marco
-- =============================================
-- [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal] 1, 100000000000000000001.00, 4
-- [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal] 1, 100000000000000000000.00, 4
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal]
	@IdContratoConvMarco INT,
	@ValorInicialContConv DECIMAL(30, 2), 
	@IdContrato INT

AS
BEGIN
	PRINT '1'
	DECLARE @ValorFinalContratoConvMarco DECIMAL(30, 2), @ValorInicialContratoConvMarco DECIMAL(30, 2), @SumValorFinalContratos DECIMAL(30, 2)
	SELECT @ValorInicialContratoConvMarco=ValorInicialContrato 
	FROM CONTRATO.Contrato WHERE IdContrato = @IdContratoConvMarco

	SET @ValorFinalContratoConvMarco = @ValorInicialContratoConvMarco;
	print cast(@ValorFinalContratoConvMarco as varchar)

	SELECT @SumValorFinalContratos=SUM(ISNULL(ValorFinalContrato, 0))  
	FROM CONTRATO.Contrato WHERE FK_IdContrato = @IdContratoConvMarco AND IdContrato <> @IdContrato
	print cast(@SumValorFinalContratos as varchar)

	PRINT ISNULL(@ValorFinalContratoConvMarco, 0)
	PRINT ISNULL(@SumValorFinalContratos, 0)
	PRINT ISNULL(@ValorInicialContConv, 0)
	-- Se quita de la ecuacion sumValorFinalContratos por correccion ICBF 14/09/2014 
	IF ((ISNULL(@ValorFinalContratoConvMarco, 0) /*+ @SumValorFinalContratos*/) >= (ISNULL(@SumValorFinalContratos, 0) + ISNULL(@ValorInicialContConv, 0)))
	BEGIN

		SELECT @IdContratoConvMarco AS IdContrato
		RETURN 

	END


END




GO


