USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ContratistaGarantias_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Modificar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 2:25:34 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ContratistaGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ContratistaGarantias_Modificar]
		@IDContratista_Garantias INT,	@IDGarantia INT,	@IDEntidadProvOferente INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ContratistaGarantias SET IDGarantia = @IDGarantia, IDEntidadProvOferente = @IDEntidadProvOferente, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IDContratista_Garantias = @IDContratista_Garantias
END
