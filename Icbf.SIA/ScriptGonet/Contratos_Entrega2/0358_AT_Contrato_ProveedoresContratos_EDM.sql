USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date:  22/05/2014 2:30 PM
-- Description:	Alterar tabla [CONTRATO].[ProveedoresContratos]
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_CONTRATO_ProveedoresContratos_FormaPago]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ProveedoresContratos]'))
ALTER TABLE [CONTRATO].[ProveedoresContratos] DROP CONSTRAINT [FK_CONTRATO_ProveedoresContratos_FormaPago]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_CONTRATO_ProveedoresContratos_FormaPago]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ProveedoresContratos]'))
ALTER TABLE [CONTRATO].[ProveedoresContratos]  WITH CHECK ADD  CONSTRAINT [FK_CONTRATO_ProveedoresContratos_FormaPago] FOREIGN KEY([IDFormaPago])
REFERENCES [CONTRATO].[FormaPago] ([IdFormaPago])
ON DELETE set null
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_CONTRATO_ProveedoresContratos_FormaPago]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[ProveedoresContratos]'))
ALTER TABLE [CONTRATO].[ProveedoresContratos] CHECK CONSTRAINT [FK_CONTRATO_ProveedoresContratos_FormaPago]
GO