USE [SIA]
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 06/06/2014
-- Description:	Insert data [CONTRATO].[RegimenContratacion]
-- =============================================


IF NOT EXISTS(SELECT * FROM [CONTRATO].[RegimenContratacion] WHERE [NombreRegimenContratacion] = 'Régimen 1' AND [Descripcion] = 'Régimen 1')
BEGIN
--	SET IDENTITY_INSERT [CONTRATO].[RegimenContratacion] ON 
	INSERT [CONTRATO].[RegimenContratacion] ([NombreRegimenContratacion], [Descripcion], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES ( N'Régimen 1', N'Régimen 1', 1, N'Administrador', CAST(0x0000A34500000000 AS DateTime), NULL, NULL)
--	SET IDENTITY_INSERT [CONTRATO].[RegimenContratacion] OFF
END
