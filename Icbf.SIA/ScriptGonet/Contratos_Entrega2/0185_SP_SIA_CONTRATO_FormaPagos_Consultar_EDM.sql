USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_FormaPagos_Consultar]    Script Date: 10/06/2014 11:41:18 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_FormaPagos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPagos_Consultar]
GO


-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  5/29/2014 7:56:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) FormaPago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPagos_Consultar]
	@IdProveedores INT = NULL,@IdMedioPago INT = NULL,@IdEntidadFinanciera INT = NULL,
	@TipoCuentaBancaria INT = NULL,@NumeroCuentaBancaria NVARCHAR(80) = NULL
AS
BEGIN
 SELECT FP.IdFormaPago, FP.IdProveedores, FP.IdMedioPago, 
 FP.IdEntidadFinanciera, FP.TipoCuentaBancaria, FP.NumeroCuentaBancaria, 
 FP.UsuarioCrea, FP.FechaCrea, FP.UsuarioModifica, FP.FechaModifica
 ,TCEF.NombreTipoCta
 ,EF.NombreEntFin
 ,TMP.DescTipoMedioPago
 FROM [CONTRATO].[FormaPago] FP
 LEFT OUTER JOIN [BaseSIF].[TipoCuentaEntFin] TCEF on TCEF.IdTipoCta = FP.TipoCuentaBancaria
 LEFT OUTER JOIN [BaseSIF].[EntidadFinanciera] EF on EF.IdentidadFinanciera = FP.IdEntidadFinanciera
 INNER JOIN [Ppto].[TipoMedioPago]  TMP on TMP.IdTipoMedioPago = FP.IdMedioPago
  WHERE isnull(FP.IdProveedores,0) = CASE WHEN @IdProveedores IS NULL THEN isnull(FP.IdProveedores,0) ELSE @IdProveedores END 
 AND isnull(FP.IdMedioPago,0) = CASE WHEN @IdMedioPago IS NULL THEN isnull(FP.IdMedioPago,0) ELSE @IdMedioPago END 
 AND isnull(FP.IdEntidadFinanciera,0) = CASE WHEN @IdEntidadFinanciera IS NULL THEN isnull(FP.IdEntidadFinanciera,0) ELSE @IdEntidadFinanciera END 
 AND isnull(FP.TipoCuentaBancaria,0) = CASE WHEN @TipoCuentaBancaria IS NULL THEN isnull(FP.TipoCuentaBancaria,0) ELSE @TipoCuentaBancaria END 
 AND isnull(FP.NumeroCuentaBancaria,0) = CASE WHEN @NumeroCuentaBancaria IS NULL THEN isnull(FP.NumeroCuentaBancaria,0) ELSE @NumeroCuentaBancaria END
END




