USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Proveedores_Contratoss_Consultar]    Script Date: 03/07/2014 09:52:19 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratoss_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratoss_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Procedimiento almacenado que consulta un(a) Proveedores_Contratos
-- =============================================
-- =============================================
-- Author:		Gonet / Efrain Diaz Mejia
-- Create date:  03/07/2014 11:20
-- Description:	Se agrag� el campo de id forma de pago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratoss_Consultar]
	@IdProveedores INT = NULL,@IdContrato INT = NULL
AS
BEGIN
	SELECT
		IdProveedoresContratos,
		IdProveedores,
		IdContrato,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica,
		IdFormaPago
	FROM [CONTRATO].[ProveedoresContratos]
	WHERE IdProveedores =
		CASE
			WHEN @IdProveedores IS NULL THEN IdProveedores ELSE @IdProveedores
		END AND IdContrato =
		CASE
			WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato
		END
END



