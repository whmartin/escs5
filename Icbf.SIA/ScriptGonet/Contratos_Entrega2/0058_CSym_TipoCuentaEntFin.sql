USE [SIA]
GO

/********************************
2014-05-22
Creamos el SYNONYM TipoCuentaEntFin 
jorge Vizcaino
********************************/

/****** Object:  Synonym [BaseSIF].[TipoCuentaEntFin]    Script Date: 22/05/2014 11:45:23 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoCuentaEntFin' AND schema_id = SCHEMA_ID(N'BaseSIF'))
DROP SYNONYM [BaseSIF].[TipoCuentaEntFin]
GO

/****** Object:  Synonym [BaseSIF].[TipoCuentaEntFin]    Script Date: 22/05/2014 11:45:23 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TipoCuentaEntFin' AND schema_id = SCHEMA_ID(N'BaseSIF'))
CREATE SYNONYM [BaseSIF].[TipoCuentaEntFin] FOR [NMF].[BaseSIF].[TipoCuentaEntFin]
GO


