use sia

-- =============================================
-- Author:		Gonet
-- Create date: 2014-07-09
-- Description:	Alterar tabla CONTRATO.TipoFormaPago
-- =============================================

IF not EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].FK_CONTRATO_Contrato_TipoFormaPago') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN	
	alter table CONTRATO.Contrato DROP constraint FK_Contrato_FormaPago

	alter table CONTRATO.Contrato add constraint FK_CONTRATO_Contrato_TipoFormaPago
	foreign key (IDFormaPago)
	references CONTRATO.TipoFormaPago (IdTipoFormaPago)
END



