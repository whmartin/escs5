USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 1:59:52 PM
-- Description:	Procedimiento almacenado que consulta un(a) SucursalAseguradoraContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContrato_Consultar]
	@IDSucursalAseguradoraContrato INT
AS
BEGIN
 SELECT IDSucursalAseguradoraContrato, IDDepartamento, IDMunicipio, Nombre, IDZona, DireccionNotificacion, CorreoElectronico, Indicativo, Telefono, Extension, Celular, IDEntidadProvOferente, CodigoSucursal, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[SucursalAseguradoraContrato] WHERE  IDSucursalAseguradoraContrato = @IDSucursalAseguradoraContrato
END
