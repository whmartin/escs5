USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_NMF_Oferente_Tercero_Consultar]    Script Date: 10/06/2014 05:29:48 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_NMF_Oferente_Tercero_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_NMF_Oferente_Tercero_Consultar]
GO



-- =============================================
-- Author:		Gonet/Efrain Diaz Mejia
-- Create date:  29/05/2014 5:33:49 PM
-- Description:	Procedimiento almacenado que consulta un(a) Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_NMF_Oferente_Tercero_Consultar]
@IdTercero INT
AS
BEGIN
SELECT T.[IDTERCERO]
      ,T.[IDTIPODOCIDENTIFICA]
      ,T.[IDESTADOTERCERO]
      ,T.[IdTipoPersona]
      ,T.[NUMEROIDENTIFICACION]
      ,T.[DIGITOVERIFICACION]
      ,T.[CORREOELECTRONICO]
      ,T.[PRIMERNOMBRE]
      ,T.[SEGUNDONOMBRE]
      ,T.[PRIMERAPELLIDO]
      ,T.[SEGUNDOAPELLIDO]
      ,T.[RAZONSOCIAL]
      ,T.[FECHAEXPEDICIONID]
      ,T.[FECHANACIMIENTO]
      ,T.[SEXO]
      ,T.[FECHACREA]
      ,T.[USUARIOCREA]
      ,T.[FECHAMODIFICA]
      ,T.[USUARIOMODIFICA]
	  ,TD.NomTipoDocumento as CodigoTipoIdentificacionPersonaNatural
	  ,TP.NombreTipoPersona
FROM [Oferente].[Tercero] T
inner join [Oferente].[TipoPersona] TP on TP.IdTipoPersona = T.IdTipoPersona
inner join [Global].[TiposDocumentos] TD on TD.IdTipoDocumento = T.IDTIPODOCIDENTIFICA
WHERE IdTercero = @IdTercero
     
END
