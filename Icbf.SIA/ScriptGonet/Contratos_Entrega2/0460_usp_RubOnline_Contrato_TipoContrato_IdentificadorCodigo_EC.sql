USE [SIA]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo]    Script Date: 14/07/2014 02:19:50 p.m. ******/
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo]    Script Date: 14/07/2014 02:19:50 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 10/06/2014
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo]
	@IdTipoContrato INT = NULL,
	@CodigoTipoContrato NVARCHAR(30) = NULL, 
	@IdCategoriaContrato INT = NULL
AS
BEGIN
	
	SELECT IdTipoContrato, CodigoTipoContrato, NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion,
    RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica
	FROM CONTRATO.TipoContrato 
	WHERE IdTipoContrato = ISNULL(@IdTipoContrato, IdTipoContrato) AND
	CodigoTipoContrato = ISNULL(@CodigoTipoContrato, CodigoTipoContrato) AND
	IdCategoriaContrato = ISNULL(@IdCategoriaContrato, IdCategoriaContrato) 

END


GO


