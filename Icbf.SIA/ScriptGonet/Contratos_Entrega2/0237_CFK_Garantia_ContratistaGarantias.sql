USE [SIA]
GO
-- =============================================
-- Author:		Gonet\Carlos Andr�s C�rdenas P.
-- Create date:  20/06/2014 10:10:00 AM
-- Description:	Creacion de llave for�nea FK_Garantia_ContratistaGarantias
-- =============================================

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Ppto].[FK_Garantia_ContratistaGarantias]') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN
ALTER TABLE [Contrato].[ContratistaGarantias] DROP CONSTRAINT FK_Garantia_ContratistaGarantias
END

ALTER TABLE [Contrato].[ContratistaGarantias]
ADD CONSTRAINT FK_Garantia_ContratistaGarantias FOREIGN KEY(IDGarantia)REFERENCES  [CONTRATO].[Garantia](IDGarantia)

