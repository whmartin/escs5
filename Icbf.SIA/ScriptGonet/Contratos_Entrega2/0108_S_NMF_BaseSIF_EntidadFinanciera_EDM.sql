USE [SIA]
GO

-- =============================================
-- Author:		Abraham
-- Create date: 2014-05-28
-- Description:	Creacion synonyms EntidadFinanciera
-- =============================================

/****** Object:  Synonym [BaseSIF].[TipoMedioPago]    Script Date: 29/05/2014 09:05:52 a.m. ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'EntidadFinanciera' AND schema_id = SCHEMA_ID(N'BaseSIF'))
DROP SYNONYM [BaseSIF].[EntidadFinanciera]
GO

/****** Object:  Synonym [BaseSIF].[TipoMedioPago]    Script Date: 29/05/2014 09:05:52 a.m. ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'EntidadFinanciera' AND schema_id = SCHEMA_ID(N'BaseSIF'))
CREATE SYNONYM [BaseSIF].[EntidadFinanciera] FOR [NMF].[BaseSIF].[EntidadFinanciera]
GO
