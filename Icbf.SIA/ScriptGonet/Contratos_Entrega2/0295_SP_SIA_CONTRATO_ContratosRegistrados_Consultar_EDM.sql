USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ContratosRegistrados_Consultar]    Script Date: 01/07/2014 07:38:01 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ContratosRegistrados_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosRegistrados_Consultar]
GO

-- =============================================
-- Author:		Efra�n D�az Mej�a
-- Create date:  6/16/2014 4:52:44 PM
-- Description:	Procedimiento almacenado que consulta un(a) Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosRegistrados_Consultar]
	@FechaRegistroSistema DATETIME = NULL,
	@IdContrato INT = NULL,
	@NumeroContrato NVARCHAR(30) = NULL,
	@NumeroProceso NVARCHAR(30) = NULL,
	@VigenciaFiscalinicial INT = NULL,
	@IDRegional INT = NULL,
	@IDModalidadSeleccion INT = NULL,
	@IDCategoriaContrato INT = NULL,
	@IDTipoContrato INT = NULL,
	@IDEstadoContraro INT = NULL,
	@UsuarioCrea Nvarchar(250) = NULL,
	@FechaInicioEjecucion DATETIME = NULL,
	@FechaFinalizaInicioContrato  DATETIME = NULL
AS
BEGIN

 SELECT distinct C.IdContrato, C.NumeroContrato, 
  C.IdNumeroProceso, C.IdVigenciaInicial, C.IdRegionalContrato, 
  C.IdModalidadSeleccion, C.IdCategoriaContrato, 
 C.IdTipoContrato, C.IdEstadoContrato, C.UsuarioCrea, 
 C.FechaCrea, C.UsuarioModifica, C.FechaModifica,
 Ltrim(rtrim(R.CodigoRegional)) +' - '+Ltrim(rtrim(R.NombreRegional)) as Regional,
 Ltrim(rtrim(MS.CodigoModalidad)) + ' - ' + Ltrim(rtrim(MS.Nombre)) as ModalidadSeleccion,
 Ltrim(rtrim(CC.CodigoCategoriaContrato)) + ' - ' + Ltrim(rtrim(CC.NombreCategoriaContrato)) AS CategoriaContrato,
 Ltrim(rtrim(TC.CodigoTipoContrato)) + ' - ' + Ltrim(rtrim(TC.NombreTipoContrato)) AS TipoContrato,
 Ltrim(rtrim(EC.Descripcion)) AS EstadoContrato,
 V.AcnoVigencia As Vigencia,
 NP.NumeroProcesoGenerado As numeroProceso,
 C.FechaInicioEjecucion,
 C.FechaFinalizacionIniciaContrato
 FROM CONTRATO.Contrato C
 left outer join DIV.Regional R on C.IDRegionalContrato = R.IdRegional
 left outer join CONTRATO.ModalidadSeleccion MS on MS.IdModalidad = C.IdModalidadSeleccion
 left outer join CONTRATO.CategoriaContrato CC on CC.IdCategoriaContrato = C.IdCategoriaContrato
 left outer join CONTRATO.TipoContrato TC on TC.IdTipoContrato = C.IdTipoContrato
 left outer join Contrato.EstadoContrato EC on EC.IDEstadoContrato = C.IDEstadoContrato
 left outer join Global.Vigencia V on V.IdVigencia = C.IdVigenciaInicial
 left outer join CONTRATO.NumeroProcesos NP on NP.IdNumeroProceso = C.IdNumeroProceso
 WHERE isnull(convert(varchar(10),C.FechaCrea, 103),'') = CASE WHEN convert(varchar(10),@FechaRegistroSistema,103) IS NULL 
 THEN isnull(convert(varchar(10),C.FechaCrea, 103),'') ELSE convert(varchar(10),@FechaRegistroSistema,103) END 
 AND isnull(C.IdContrato,'') = CASE WHEN @IdContrato IS NULL THEN isnull(C.IdContrato,'') ELSE @IdContrato END 
 AND isnull(C.NumeroContrato,'') = CASE WHEN @NumeroContrato IS NULL THEN isnull(C.NumeroContrato,'') ELSE @NumeroContrato END 
 AND isnull(C.IdNumeroProceso,'') = CASE WHEN @NumeroProceso IS NULL THEN isnull(C.IdNumeroProceso,'') ELSE @NumeroProceso END 
 AND isnull(C.IdVigenciaInicial,'') = CASE WHEN @VigenciaFiscalinicial IS NULL THEN isnull(C.IdVigenciaInicial,'') ELSE @VigenciaFiscalinicial END 
 AND isnull(C.IDRegionalContrato,'') = CASE WHEN @IDRegional IS NULL THEN isnull(C.IDRegionalContrato,'') ELSE @IDRegional END 
 AND isnull(C.IDModalidadSeleccion,'') = CASE WHEN @IDModalidadSeleccion IS NULL THEN isnull(C.IDModalidadSeleccion,'') ELSE @IDModalidadSeleccion END 
 AND isnull(C.IDCategoriaContrato,'') = CASE WHEN @IDCategoriaContrato IS NULL THEN isnull(C.IDCategoriaContrato,'') ELSE @IDCategoriaContrato END 
 AND isnull(C.IDTipoContrato,'') = CASE WHEN @IDTipoContrato IS NULL THEN isnull(C.IDTipoContrato,'') ELSE @IDTipoContrato END 
 AND isnull(C.IDEstadoContrato,'') = CASE WHEN @IDEstadoContraro IS NULL THEN isnull(C.IDEstadoContrato,'') ELSE @IDEstadoContraro END
 AND isnull(C.UsuarioCrea,'') = CASE WHEN @UsuarioCrea IS NULL THEN isnull(C.UsuarioCrea,'') ELSE @UsuarioCrea END 
 AND isnull(convert(varchar(10),C.FechaInicioEjecucion, 103),'') = CASE WHEN convert(varchar(10),@FechaInicioEjecucion,103) IS NULL 
 THEN isnull(convert(varchar(10),C.FechaInicioEjecucion, 103),'') ELSE convert(varchar(10),@FechaInicioEjecucion,103) END 
 AND isnull(convert(varchar(10),C.FechaFinalizacionIniciaContrato, 103),'') = CASE WHEN convert(varchar(10),@FechaFinalizaInicioContrato,103) IS NULL 
 THEN isnull(convert(varchar(10),C.FechaFinalizacionIniciaContrato, 103),'') ELSE convert(varchar(10),@FechaFinalizaInicioContrato,103) END 
END




