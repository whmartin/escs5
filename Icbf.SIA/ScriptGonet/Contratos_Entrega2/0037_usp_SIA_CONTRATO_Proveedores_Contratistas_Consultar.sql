USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratistas_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratistas_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/20/2014 12:40:01 PM
-- Description:	Procedimiento almacenado que consulta un(a) Proveedores_Contratista
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratistas_Consultar]
	@IdEntidad INT = NULL,@IdTercero INT = NULL,@NombreTipoPersona NVARCHAR(256) = NULL,@CodDocumento NVARCHAR(256) = NULL,@NumeroIdentificacion NVARCHAR(256) = NULL,@Razonsocial NVARCHAR(256) = NULL,@NumeroidentificacionRepLegal NVARCHAR(256) = NULL,@RazonsocialRepLegal NVARCHAR(256) = NULL
AS
BEGIN
 SELECT DISTINCT DT.IdEntidad, DT.IdTercero, DT.NombreTipoPersona, DT.CodDocumento, DT.NumeroIdentificacion, DT.Razonsocial 
  ,TerRep.Numeroidentificacion as NumeroidentificacionRepLegal
  ,( TerRep.PRIMERNOMBRE +' '+ISNULL(TerRep.SEGUNDONOMBRE,'') +' ' + TerRep.PRIMERAPELLIDO +' '+ ISNULL(TerRep.SEGUNDOAPELLIDO,'') ) as RazonsocialRepLegal
  ,DT.UsuarioCrea, DT.FechaCrea, DT.UsuarioModifica, DT.FechaModifica
  FROM (
  SELECT EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') ) as Razonsocial,
		E.Descripcion AS Estado ,EP.IdEstado,EP.ObserValidador, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor
		,IAE.IdRepLegal --, TerRep.NUMEROIDENTIFICACION AS NumeroIdentificacionRepLegal,  TerRep.PRIMERNOMBRE  as RazonsocilaRepLegal
		,PC.UsuarioCrea, PC.FechaCrea, PC.UsuarioModifica, PC.FechaModifica
  FROM [Proveedor].[EntidadProvOferente] EP 
     INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
     INNER JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
     INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona  
     INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
     INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
     INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
     INNER JOIN CONTRATO.ProveedoresContratos PC ON PC.IdProveedores = EP.IdTercero
     
     --LEFT JOIN Oferente.Tercero TerRep ON IAE.IdRepLegal = TerRep.IDTERCERO
     WHERE T.IdTipoPersona=1
UNION 
 SELECT  EP.IdEntidad,EP.ConsecutivoInterno,T.IdTipoPersona,Tp.NombreTipoPersona,TD.CodDocumento,T.IDTIPODOCIDENTIFICA,T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, ( T.RAZONSOCIAL ) as Razonsocial,
		E.Descripcion AS Estado ,EP.IdEstado ,EP.ObserValidador, EdoProveedor.Descripcion AS EstadoProveedor, EP.IdEstadoProveedor
		,IAE.IdRepLegal --, TerRep.NUMEROIDENTIFICACION AS NumeroIdentificacionRepLegal, ( TerRep.RAZONSOCIAL ) as RazonsocilaRepLegal
		,PC.UsuarioCrea, PC.FechaCrea, PC.UsuarioModifica, PC.FechaModifica
  FROM [Proveedor].[EntidadProvOferente] EP 
    INNER JOIN oferente.TERCERO T ON EP.IdTercero=T.IDTERCERO
    LEFT JOIN Proveedor.InfoAdminEntidad IAE ON EP.IdEntidad=IAE.IdEntidad
    INNER JOIN Oferente.TipoPersona TP ON T.IdTipoPersona=TP.IdTipoPersona
    INNER JOIN [Global].TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
    INNER JOIN Proveedor.EstadoDatosBasicos E ON EP.IdEstado=E.IdEstadoDatosBasicos
    INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
    INNER JOIN CONTRATO.ProveedoresContratos PC ON PC.IdProveedores = EP.IdTercero
    --LEFT JOIN Oferente.Tercero TerRep ON IAE.IdRepLegal = TerRep.IDTERCERO
    WHERE T.IdTipoPersona=2
    ) DT
    LEFT JOIN Oferente.Tercero TerRep ON DT.IdRepLegal = TerRep.IDTERCERO
 WHERE DT.IdEntidad = CASE WHEN @IdEntidad IS NULL THEN DT.IdEntidad ELSE @IdEntidad END 
 AND DT.IdTercero = CASE WHEN @IdTercero IS NULL THEN DT.IdTercero ELSE @IdTercero END 
 AND DT.NombreTipoPersona = CASE WHEN @NombreTipoPersona IS NULL THEN DT.NombreTipoPersona ELSE @NombreTipoPersona END 
 AND DT.CodDocumento = CASE WHEN @CodDocumento IS NULL THEN DT.CodDocumento ELSE @CodDocumento END 
 AND DT.NumeroIdentificacion = CASE WHEN @NumeroIdentificacion IS NULL THEN DT.NumeroIdentificacion ELSE @NumeroIdentificacion END 
 AND DT.Razonsocial = CASE WHEN @Razonsocial IS NULL THEN DT.Razonsocial ELSE @Razonsocial END 
 --AND TerRep.NumeroidentificacionRepLegal = CASE WHEN @NumeroidentificacionRepLegal IS NULL THEN TerRep.NumeroidentificacionRepLegal ELSE @NumeroidentificacionRepLegal END 
 --AND RazonsocialRepLegal = CASE WHEN @RazonsocialRepLegal IS NULL THEN RazonsocialRepLegal ELSE @RazonsocialRepLegal END
END
