USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]    Script Date: 25/06/2014 18:44:28 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]
GO


-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/22/2014 1:36:08 PM
-- Description:	Procedimiento almacenado que consulta un(a) SolicitudModPlanCompras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar]
	@IdSolicitudModPlanCompra INT
AS
BEGIN

	SELECT
		SolPC.IdSolicitudModPlanCompra,
		SolPC.Justificacion,
		SolPC.NumeroSolicitud,
		SolPC.FechaSolicitud,
		SolPC.IdEstadoSolicitud,
		SolPC.IdUsuarioSolicitud,
		SolPC.IdPlanDeCompras,
		PC.Vigencia,
		SolPC.UsuarioCrea,
		SolPC.FechaCrea,
		SolPC.UsuarioModifica,
		SolPC.FechaModifica,
		PC.IdContrato AS IdContrato,
		cco.NumeroContrato AS NumeroContrato,
		PC.IDPlanDeComprasContratos AS NumeroConsecPlanDeCompras,
		(Usu.PrimerNombre + '' + Usu.SegundoNombre + '' + Usu.PrimerApellido + '' + Usu.SegundoApellido) AS UsuarioSolicitud,
		PC.Vigencia AS Vigencia,
		EdoSol.Descripcion AS EstadoSolicitud
	FROM [CONTRATO].[SolicitudModPlanCompras] SolPC
	INNER JOIN [CONTRATO].PlanDeComprasContratos PC
		ON SolPC.IdPlanDeCompras = PC.IDPlanDeComprasContratos
	INNER JOIN [SEG].[Usuario] Usu
		ON SolPC.IdUsuarioSolicitud = Usu.IdUsuario
	--INNER JOIN [Global].[Vigencia] Vig ON SolPC.IdVigencia = Vig.IdVigencia
	INNER JOIN [CONTRATO].[EstadoSolcitudModPlanCompras] EdoSol
		ON SolPC.IdEstadoSolicitud = EdoSol.IdEstadoSolicitud
	Inner JOIN CONTRATO.Contrato cco
		ON PC.IdContrato = cco.IdContrato

	WHERE SolPC.IdSolicitudModPlanCompra = @IdSolicitudModPlanCompra
END
