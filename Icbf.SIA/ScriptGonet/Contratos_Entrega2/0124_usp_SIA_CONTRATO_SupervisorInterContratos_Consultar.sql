USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SupervisorInterContratos_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContratos_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/30/2014 12:23:44 PM
-- Description:	Procedimiento almacenado que consulta un(a) SupervisorInterContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SupervisorInterContratos_Consultar]
	@FechaInicio DATETIME = NULL,@Inactivo BIT = NULL,@Identificacion NVARCHAR(80) = NULL,@TipoIdentificacion NVARCHAR(4) = NULL,@IDTipoSuperInter INT = NULL,@IdNumeroContratoInterventoria INT = NULL,@IDProveedoresInterventor INT = NULL,@IDEmpleadosSupervisor INT = NULL,@IdContrato INT = NULL
AS
BEGIN
SELECT
	IDSupervisorIntervContrato,
	FechaInicio,
	Inactivo,
	Identificacion,
	TipoIdentificacion,
	IDTipoSuperInter,
	IdNumeroContratoInterventoria,
	IDProveedoresInterventor,
	IDEmpleadosSupervisor,
	IdContrato,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [CONTRATO].[SupervisorInterContrato]
WHERE FechaInicio =
	CASE
		WHEN @FechaInicio IS NULL THEN FechaInicio ELSE @FechaInicio
	END AND Inactivo =
	CASE
		WHEN @Inactivo IS NULL THEN Inactivo ELSE @Inactivo
	END AND Identificacion =
	CASE
		WHEN @Identificacion IS NULL THEN Identificacion ELSE @Identificacion
	END AND TipoIdentificacion =
	CASE
		WHEN @TipoIdentificacion IS NULL THEN TipoIdentificacion ELSE @TipoIdentificacion
	END AND IDTipoSuperInter =
	CASE
		WHEN @IDTipoSuperInter IS NULL THEN IDTipoSuperInter ELSE @IDTipoSuperInter
	END AND IdNumeroContratoInterventoria =
	CASE
		WHEN @IdNumeroContratoInterventoria IS NULL THEN IdNumeroContratoInterventoria ELSE @IdNumeroContratoInterventoria
	END AND IDProveedoresInterventor =
	CASE
		WHEN @IDProveedoresInterventor IS NULL THEN IDProveedoresInterventor ELSE @IDProveedoresInterventor
	END AND IDEmpleadosSupervisor =
	CASE
		WHEN @IDEmpleadosSupervisor IS NULL THEN IDEmpleadosSupervisor ELSE @IDEmpleadosSupervisor
	END AND IdContrato =
	CASE
		WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato
	END
END
