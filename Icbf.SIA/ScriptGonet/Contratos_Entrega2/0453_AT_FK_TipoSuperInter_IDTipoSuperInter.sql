use SIA

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[CONTRATO].FK_TipoSuperInter_IDTipoSuperInter') AND OBJECTPROPERTY(ID, N'ISFOREIGNKEY') = 1)
BEGIN	
	ALTER table CONTRATO.SupervisorInterContrato drop constraint FK_TipoSuperInter_IDTipoSuperInter

	ALTER table  CONTRATO.SupervisorInterContrato add constraint FK_TipoSuperInter_IDTipoSuperInter 
	foreign key (IDTipoSuperInter)
	references CONTRATO.TipoSupervisorInterventor (IDTipoSupervisorInterventor)
END
ELSE
BEGIN
	ALTER table  CONTRATO.SupervisorInterContrato add constraint FK_TipoSuperInter_IDTipoSuperInter 
	foreign key (IDTipoSuperInter)
	references CONTRATO.TipoSupervisorInterventor (IDTipoSupervisorInterventor)
END
