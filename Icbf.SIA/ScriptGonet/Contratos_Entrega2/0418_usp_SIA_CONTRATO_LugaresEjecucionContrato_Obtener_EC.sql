USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_LugaresEjecucionContrato_Obtener]    Script Date: 29/06/2014 07:31:44 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_LugaresEjecucionContrato_Obtener')
BEGIN
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_LugaresEjecucionContrato_Obtener]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_LugaresEjecucionContrato_Obtener]    Script Date: 29/06/2014 07:31:44 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 29/06/2014
-- Description:	Obtiene los lugares de ejecuci�n asociados a un contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_LugaresEjecucionContrato_Obtener]
	@IdContrato INT
AS
BEGIN

	IF NOT EXISTS(SELECT * FROM CONTRATO.LugarEjecucionContrato WHERE IdContrato = @IdContrato AND NivelNacional = 1)
	BEGIN
		SELECT LEC.IdLugarEjecucionContratos, LEC.IdContrato, LEC.IdDepartamento, 
		LEC.IdRegional AS IdMunicipio, 	LEC.IdRegional, 
		LEC.NivelNacional, D.NombreDepartamento AS Departamento, M.NombreMunicipio AS Municipio,
		LEC.UsuarioCrea, LEC.FechaCrea, LEC.UsuarioModifica, LEC.FechaModifica 
		FROM CONTRATO.LugarEjecucionContrato LEC
		INNER JOIN DIV.Departamento D ON D.IdDepartamento = LEC.IdDepartamento
		INNER JOIN DIV.Municipio M ON M.IdMunicipio = LEC.IdRegional
		WHERE IdContrato = @IdContrato
		ORDER BY NombreDepartamento, NombreMunicipio ASC
	END

END

GO


