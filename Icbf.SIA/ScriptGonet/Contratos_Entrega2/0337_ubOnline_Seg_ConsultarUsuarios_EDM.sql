USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Seg_ConsultarUsuarios]    Script Date: 25/06/2014 07:03:28 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Seg_ConsultarUsuarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarios]
GO



-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 03-10-2012
-- Description:	Procedimiento almacenado que consulta un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarios]
	@IdPerfil int,
	@NumeroDocumento NVARCHAR(250),
	@PrimerNombre NVARCHAR(250),
	@PrimerApellido NVARCHAR(250),
	@Estado BIT = NULL
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,Global.TiposDocumentos.NomTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
	  ,SEG.Usuario.idRegional
  FROM SEG.Usuario INNER JOIN [Global].[TiposDocumentos] ON SEG.Usuario.IdTipoDocumento=[Global].[TiposDocumentos].[IdTipoDocumento]
WHERE
	SEG.Usuario.NumeroDocumento = CASE WHEN @NumeroDocumento = '' THEN SEG.Usuario.NumeroDocumento ELSE @NumeroDocumento END
AND
	SEG.Usuario.PrimerNombre LIKE 	 CASE WHEN @PrimerNombre = '' THEN SEG.Usuario.PrimerNombre ELSE '%'+@PrimerNombre+'%' END
AND
	SEG.Usuario.PrimerApellido LIKE 	CASE WHEN @PrimerApellido = '' THEN SEG.Usuario.PrimerApellido ELSE '%'+@PrimerApellido+'%' END
AND 
	SEG.Usuario.Estado = CASE WHEN @Estado	IS NULL THEN SEG.Usuario.Estado ELSE @Estado END

END





