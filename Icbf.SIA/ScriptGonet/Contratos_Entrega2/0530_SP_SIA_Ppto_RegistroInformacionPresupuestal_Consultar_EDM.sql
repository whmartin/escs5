USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]    Script Date: 25/07/2014 10:13:43 a.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]
GO

-- =============================================
-- Author:		GONET\Efrain Diaz Mejia
-- Create date:  22/05/2014 2:30 PM
-- Description:	Procedimiento almacenado que Muestra la información relacionada con el certificado de disponibilidad presupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar]
	@NumeroCDP nvarchar(128) = null,
	@VigenciaFiscal INT = null,
	@RegionalICBF INT = null,
	@Area INT = null,
	@ValorTotalDesde numeric(30, 8) = null,
	@ValorTotalHasta numeric(30, 8) = null,
	@FechaCDPDesde DATETIME = null,
	@FechaCDPHasta DATETIME = null,
	@NumeroRegistros int

AS
BEGIN
	  select top (@NumeroRegistros)
	  IECDP.idetlcdp,
	  V.[AcnoVigencia] as VigenciaFiscal
	  ,IECDP.IdRegionalPCI RegionalICBF
	  --,rtrim(ltrim(IECDP.IdRegionalPCI)) + '-' + rtrim(ltrim(RPCI.DescripcionPCI) as NombreRegionalICBF
	  ,rtrim(ltrim(R.CodigoRegional)) +'-'+ rtrim(ltrim(upper(R.NombreRegional))) as NombreRegionalICBF
	  ,'' Area
	  ,IECDP.[CDP] NumeroCDP
	  ,IECDP.[FechaSolicitudCDP] FechaCDP
	  ,IECDP.[ValorActualCDP] ValorCDP
	  ,upper(rtrim(ltrim(DASIIF.CodDepAfecSIIF))) + '-' + upper(rtrim(ltrim(DASIIF.Descripcion))) as DependenciaAfectacionGastos
	  ,upper(rtrim(ltrim(TFF.CodTipoFte)))+'-'+ upper(rtrim(ltrim(TFF.DescTipoFuente))) as TipoFuenteFinanciamiento
	  ,CGRPG.RubroSIIF PosicionCatalogoGastos
	  ,CGRPG.DescripcionRubro RubroPresupuestal
	  ,upper(rtrim(ltrim(TRFP.CodTipoRecursoFinPptal))) +'-'+ upper(rtrim(ltrim(TRFP.DescTipoRecurso))) as RecursoPresupuestal
	  ,'' TipoDocumentoSoporte
	  ,upper(rtrim(ltrim(TSF.CodSitFondos))) +'-'+ upper(rtrim(ltrim(TSF.DescTipoSitFondos))) as TipoSituacionFondos
	  ,IECDP.FechaCrea
      ,IECDP.UsuarioCrea
      ,IECDP.FechaModifica
      ,IECDP.UsuarioModifica
		from Ppto.InfoETLCDP IECDP
		--Inner Join Ppto.AreasxRubro APR on APR.IdVigencia = IECDP.IdVigencia and APR.IdCatalogo = IECDP.IdCatRubro
		--Inner Join Ppto.AreasInternas AI On AI.IdAreasInternas = APR.IdAreasInternas
		Inner Join Global.Vigencia V on V.IdVigencia = IECDP.IdVigencia
		inner join Ppto.RegionalesPCI RPCI on RPCI.IdRegionalPCI = IECDP.IdRegionalPCI 
		inner join DIV.Regional R on RPCI.CodRegionalIcbf collate DATABASE_DEFAULT  = R.CodigoRegional collate DATABASE_DEFAULT
		Inner join Ppto.DependenciasAfectacionSIIF DASIIF on DASIIF.IdDepAfecSIIF = IECDP.IdDepAfecSIIF
		Inner Join BaseSIF.TipoFuenteFinanciamento TFF on TFF.IdTipoFte = IECDP.IdTipoFte
		Inner Join BaseSIF.TipoRecursoFinPptal TRFP on TRFP.IdTipoRecursoFinPptal = IECDP.IdTipoRecursoFinPptal
		Inner Join Ppto.CatGralRubrosPptalGasto CGRPG on CGRPG.IdCatalogo = IECDP.IdCatRubro
		Inner join BaseSIF.TipoSitFondos TSF On TSF.IdTipoSitFondos = IECDP.IdTipoSitFondos
		Where isnull(IECDP.CDP,'') = CASE WHEN @NumeroCDP IS NULL THEN isnull(IECDP.CDP,'') ELSE @NumeroCDP END 
		And isnull(IECDP.IdVigencia,0) = CASE WHEN @VigenciaFiscal IS NULL THEN isnull(IECDP.IdVigencia,0) ELSE @VigenciaFiscal END 
		And isnull(R.IdRegional,0) = CASE WHEN @RegionalICBF IS NULL THEN isnull(R.IdRegional,0) ELSE @RegionalICBF END 
		--and isnull(IECDP.IdRegionalPCI,0) = COALESCE(@RegionalICBF, isnull(IECDP.IdRegionalPCI,0))
		And isnull(IECDP.ValorActualCDP,0) >= CASE WHEN @ValorTotalDesde IS NULL THEN isnull(IECDP.ValorActualCDP,0) ELSE @ValorTotalDesde END 
		And isnull(IECDP.ValorActualCDP,0) <= CASE WHEN @ValorTotalHasta IS NULL THEN  isnull(IECDP.ValorActualCDP,0) ELSE @ValorTotalHasta END 
	    And isnull(IECDP.FechaSolicitudCDP,'') >= CASE WHEN @FechaCDPDesde IS NULL THEN isnull(IECDP.FechaSolicitudCDP,'') ELSE convert(datetime,@FechaCDPDesde,103) END 
	    And isnull(IECDP.FechaSolicitudCDP,'') <= CASE WHEN @FechaCDPHasta IS NULL THEN isnull(IECDP.FechaSolicitudCDP,'') ELSE convert(datetime,@FechaCDPHasta,103) END 
 
 IF @@ROWCOUNT >= 500
  BEGIN
   RAISERROR('La consulta arroja demasiados resultados,por favor refine su consulta',16,1)
   RETURN
  END


END


