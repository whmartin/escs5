
USE [SIA]
GO

-- =============================================
-- Author:		GoNet/Efrain Diaz Mejia
-- Create date:  12/05/2014 11:00 AM
-- Description:	Insertar opci�n de N�mero de proceso en la tabla TablaParametrica
-- =============================================
IF not exists(SELECT IdTablaParametrica from CONTRATO.TablaParametrica where NombreTablaParametrica = N'N�mero de proceso' and  Url = N'Contratos/NumeroProcesos')
Begin
		INSERT INTO CONTRATO.TablaParametrica ( [CodigoTablaParametrica], [NombreTablaParametrica], 
		[Url], [Estado], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) 
		VALUES ( N'01', N'N�mero de proceso', N'Contratos/NumeroProcesos', 1, N'administrador', GETDATE(), NULL, NULL)

End