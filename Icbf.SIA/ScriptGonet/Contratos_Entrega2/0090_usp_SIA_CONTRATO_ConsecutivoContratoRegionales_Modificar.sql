USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar]
GO

-- =============================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  5/27/2014 6:43:39 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ConsecutivoContratoRegionales
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar]
		@IDConsecutivoContratoRegional INT,	@IdRegional INT,	@IdVigencia INT,	@Consecutivo NUMERIC(30), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE CONTRATO.ConsecutivoContratoRegionales SET IdRegional = @IdRegional, IdVigencia = @IdVigencia, Consecutivo = @Consecutivo, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IDConsecutivoContratoRegional = @IDConsecutivoContratoRegional
END
