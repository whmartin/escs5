USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_FormaPago_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPago_Modificar]
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/28/2014 3:00:58 PM
-- Description:	Procedimiento almacenado que actualiza un(a) FormaPago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPago_Modificar]
		@IdFormaPago INT,	@IdProveedores INT,	@IdMedioPago INT,	@IdEntidadFinanciera INT,	@TipoCuentaBancaria INT,	@NumeroCuentaBancaria NVARCHAR(80), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE CONTRATO.FormaPago SET IdProveedores = @IdProveedores, IdMedioPago = @IdMedioPago, IdEntidadFinanciera = @IdEntidadFinanciera, TipoCuentaBancaria = @TipoCuentaBancaria, NumeroCuentaBancaria = @NumeroCuentaBancaria, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdFormaPago = @IdFormaPago
END
