USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AporteContrato_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  5/29/2014 2:03:02 PM
-- Description:	Procedimiento almacenado que consulta un(a) AporteContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AporteContrato_Consultar]
	@IdAporteContrato INT
AS
BEGIN
 SELECT IdAporteContrato, AportanteICBF, NumeroIdentificacionICBF, ValorAporte, DescripcionAporte, AporteEnDinero, IdContrato, IDEntidadProvOferente, FechaRP, NumeroRP, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AporteContrato] WHERE  IdAporteContrato = @IdAporteContrato
END
