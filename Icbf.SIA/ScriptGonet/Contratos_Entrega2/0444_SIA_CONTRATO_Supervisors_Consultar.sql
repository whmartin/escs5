USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Supervisors_Consultar]    Script Date: 10/07/2014 05:25:32 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Supervisors_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Supervisors_Consultar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Supervisors_Consultar]    Script Date: 10/07/2014 05:25:32 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abraham Rivero
-- Create date: 2014-06-02
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Supervisors_Consultar]
	@IdTipoidentificacion NVARCHAR(4) = NULL,
	@NumeroIdentificacion NVARCHAR(50) = NULL,
	@TipoVinculacion NVARCHAR(100) = NULL,
	@IdRegional NVARCHAR(4) = NULL,
	@PrimerNombre NVARCHAR(100) = NULL,
	@SegundoNombre NVARCHAR(100) = NULL,
	@PrimerApellido NVARCHAR(100) = NULL,
	@SegundoApellido NVARCHAR(100) = NULL
AS
BEGIN

IF @IdTipoIdentificacion IS NOT NULL
	BEGIN
		IF @IdTipoIdentificacion = 'CC'
		BEGIN
			SET @IdTipoIdentificacion = 'C'
		END
		ELSE IF @IdTipoIdentificacion = 'CE'
		BEGIN
			SET @IdTipoIdentificacion = 'CE'
		END
	END

	IF((ISNUMERIC(@NumeroIdentificacion) = 1 ) or (@NumeroIdentificacion IS NULL))-- se colcoa validacion ya que no existen @NumeroIdentificacion que no sean numericos 
			BEGIN 

		SELECT 
			ID_Ident,
			desc_ide AS TipoIdentificacion,
			num_iden AS NumeroIdentificacion,
			desc_vin AS TipoVinculacion,
			(nom_emp1 + ' ' + nom_emp2 + ' ' + ape_emp1 + ' ' + ape_emp2) AS NombreCompleto,
			nom_emp1 AS PrimerNombre,
			nom_emp2 AS SegundoNombre,
			ape_emp1 AS PrimerApellido,
			ape_emp2 AS SegundoApellido,
			ID_regio,
			regional,
			nom_depe AS Dependencia,
			direccion,
			telefono,
			ISNULL(eee_mail, '') AS CorreoE,
			Desc_car AS Cargo,
			num_iden AS IdEmpleado
		FROM [KACTUS].[KPRODII].[dbo].[Da_Emple]
		WHERE (@IdTipoidentificacion IS NULL OR ID_Ident = @IdTipoidentificacion)
		AND (@NumeroIdentificacion IS NULL OR num_iden = @NumeroIdentificacion)
		AND (@TipoVinculacion IS NULL OR desc_vin = @TipoVinculacion)
		AND (@IdRegional IS NULL OR ID_regio = @IdRegional)
		AND (@PrimerNombre IS NULL OR nom_emp1 = @PrimerNombre)
		AND (@SegundoNombre IS NULL OR nom_emp2 = @SegundoNombre)
		AND (@PrimerApellido IS NULL OR ape_emp1 = @PrimerApellido)
		AND (@SegundoApellido IS NULL OR ape_emp2 = @SegundoApellido)
	END

END

