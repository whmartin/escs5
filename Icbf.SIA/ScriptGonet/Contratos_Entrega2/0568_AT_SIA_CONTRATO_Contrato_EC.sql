USE [SIA]
GO

IF  EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'ValorInicialContrato')
BEGIN 
	ALTER TABLE CONTRATO.Contrato 
	ALTER COLUMN ValorInicialContrato numeric(30,2) null;
END

IF  EXISTS(select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Contrato' AND table_schema = 'CONTRATO' 
	AND column_name = 'ValorFinalContrato')
BEGIN 
	ALTER TABLE CONTRATO.Contrato 
	ALTER COLUMN ValorFinalContrato numeric(30,2) null;
END