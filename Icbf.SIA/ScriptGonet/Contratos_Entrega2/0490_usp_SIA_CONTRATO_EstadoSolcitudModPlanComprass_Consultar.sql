USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_EstadoSolcitudModPlanComprass_Consultar]    Script Date: 17/07/2014 9:18:25 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_EstadoSolcitudModPlanComprass_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_EstadoSolcitudModPlanComprass_Consultar]
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/22/2014 12:33:44 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoSolcitudModPlanCompras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_EstadoSolcitudModPlanComprass_Consultar]
	@CodEstado NVARCHAR(24) = NULL,@Descripcion NVARCHAR(160) = NULL,@Activo BIT = NULL
AS
BEGIN
	SELECT
		IdEstadoSolicitud,
		CodEstado,
		Descripcion,
		Activo,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [CONTRATO].[EstadoSolcitudModPlanCompras]
	WHERE CodEstado =
		CASE
			WHEN @CodEstado IS NULL THEN CodEstado ELSE @CodEstado
		END AND Descripcion =
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END AND Activo =
		CASE
			WHEN @Activo IS NULL THEN Activo ELSE @Activo
		END
END
