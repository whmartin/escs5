USE [Oferentes]
GO

-- Autor: Carlos Andr�s C�rdenas
-- Fecha: 24/07/2014
-- Descripcion: Inserta el salario minimo del a�o 2014

IF NOT EXISTS (SELECT * from [Oferente].[SalarioMinimo] where A�o = 2014)
BEGIN
INSERT INTO [Oferente].[SalarioMinimo] (A�o,Valor,Estado,UsuarioCrea,FechaCrea) VALUES (2014,616000,1,'Administrador','2014-07-24 11:53:00')
END

