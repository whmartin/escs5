USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasRubros_Obtener]    Script Date: 09/09/2014 11:22:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasRubros_Obtener]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubros_Obtener]
GO


/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_PlanComprasRubros_Obtener]    Script Date: 09/09/2014 11:22:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 08/07/2014
-- Description:	Obtiene los rubros asociados a un plan de compras para ser utilizado en la pagina de registro
-- de contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasRubros_Obtener]
	@IDPlanDeComprasContratos INT
AS
BEGIN
	
	SELECT RPC.IDRubroPlanComprasContrato,
		 RPC.ValorRubroPresupuestal,
		 RPC.IDPlanDeComprasContratos,
		 RPC.IDRubro,
		 '' as RecursoPresupuestal,
		 RPC.UsuarioCrea,
		 RPC.FechaCrea,
		 RPC.UsuarioModifica,
		 RPC.FechaModifica, 
		 PCC.IDPlanDeCompras AS NumeroConsecutivo,
		 RPC.IdPagosDetalle 
	FROM Contrato.RubroPlanComprasContrato RPC 
	INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON PCC.IDPlanDeComprasContratos = RPC.IDPlanDeComprasContratos
	WHERE RPC.IDPlanDeComprasContratos = @IDPlanDeComprasContratos

END


GO


