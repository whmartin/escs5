USE SIA
GO

--Autor: Carlos Andres Cardenas
--fecha: 03/06/2014 15:41
--Descripcion: se inserta los valores salario minimo, porcentaje

IF NOT EXISTS (select * from Contrato.UnidadCalculo where Descripcion = 'Salario M�nimo')
INSERT INTO Contrato.UnidadCalculo (CodUnidadCalculo,Descripcion,UsuarioCrea,FechaCrea,Inactivo) VALUES ('01','Salario M�nimo','Administrador',GETDATE(),0)
GO

IF NOT EXISTS (select * from Contrato.UnidadCalculo where Descripcion = 'Porcentaje')
INSERT INTO Contrato.UnidadCalculo (CodUnidadCalculo,Descripcion,UsuarioCrea,FechaCrea,Inactivo) VALUES ('02','Porcentaje','Administrador',GETDATE(),0)
GO