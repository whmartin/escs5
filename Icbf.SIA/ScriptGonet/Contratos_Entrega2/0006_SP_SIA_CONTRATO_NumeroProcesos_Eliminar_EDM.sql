USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_NumeroProcesos_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Eliminar]
GO


-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/7/2014 5:55:13 PM
-- Description:	Procedimiento almacenado que elimina un(a) NumeroProcesos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Eliminar]
	@IdNumeroProceso INT
AS
BEGIN
	DELETE CONTRATO.NumeroProcesos WHERE IdNumeroProceso = @IdNumeroProceso
END
