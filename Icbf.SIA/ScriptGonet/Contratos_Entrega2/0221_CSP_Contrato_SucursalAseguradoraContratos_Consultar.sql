USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_SucursalAseguradoraContratos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContratos_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  6/20/2014 1:59:52 PM
-- Description:	Procedimiento almacenado que consulta un(a) SucursalAseguradoraContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_SucursalAseguradoraContratos_Consultar]
	@IDDepartamento INT = NULL,@IDMunicipio INT = NULL,@Nombre NVARCHAR(160) = NULL,@IDZona INT = NULL,@DireccionNotificacion NVARCHAR(80) = NULL,@CorreoElectronico NVARCHAR(80) = NULL,@Indicativo NVARCHAR(3) = NULL,@Telefono NVARCHAR(40) = NULL,@Extension NVARCHAR(8) = NULL,@Celular NVARCHAR(40) = NULL,@IDEntidadProvOferente INT = NULL,@CodigoSucursal INT = NULL
AS
BEGIN
 SELECT IDSucursalAseguradoraContrato, IDDepartamento, IDMunicipio, Nombre, IDZona, DireccionNotificacion, CorreoElectronico, Indicativo, Telefono, Extension, Celular, IDEntidadProvOferente, CodigoSucursal, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[SucursalAseguradoraContrato] WHERE IDDepartamento = CASE WHEN @IDDepartamento IS NULL THEN IDDepartamento ELSE @IDDepartamento END AND IDMunicipio = CASE WHEN @IDMunicipio IS NULL THEN IDMunicipio ELSE @IDMunicipio END AND Nombre = CASE WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre END AND IDZona = CASE WHEN @IDZona IS NULL THEN IDZona ELSE @IDZona END AND DireccionNotificacion = CASE WHEN @DireccionNotificacion IS NULL THEN DireccionNotificacion ELSE @DireccionNotificacion END AND CorreoElectronico = CASE WHEN @CorreoElectronico IS NULL THEN CorreoElectronico ELSE @CorreoElectronico END AND Indicativo = CASE WHEN @Indicativo IS NULL THEN Indicativo ELSE @Indicativo END AND Telefono = CASE WHEN @Telefono IS NULL THEN Telefono ELSE @Telefono END AND Extension = CASE WHEN @Extension IS NULL THEN Extension ELSE @Extension END AND Celular = CASE WHEN @Celular IS NULL THEN Celular ELSE @Celular END AND IDEntidadProvOferente = CASE WHEN @IDEntidadProvOferente IS NULL THEN IDEntidadProvOferente ELSE @IDEntidadProvOferente END AND CodigoSucursal = CASE WHEN @CodigoSucursal IS NULL THEN CodigoSucursal ELSE @CodigoSucursal END
END
