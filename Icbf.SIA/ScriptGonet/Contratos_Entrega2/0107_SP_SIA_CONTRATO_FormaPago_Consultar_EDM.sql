USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_FormaPago_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPago_Consultar]
GO

-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/28/2014 3:00:58 PM
-- Description:	Procedimiento almacenado que consulta un(a) FormaPago
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_FormaPago_Consultar]
	@IdFormaPago INT
AS
BEGIN
 SELECT IdFormaPago, IdProveedores, IdMedioPago, IdEntidadFinanciera, TipoCuentaBancaria, NumeroCuentaBancaria, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [CONTRATO].[FormaPago] WHERE  IdFormaPago = @IdFormaPago
END
