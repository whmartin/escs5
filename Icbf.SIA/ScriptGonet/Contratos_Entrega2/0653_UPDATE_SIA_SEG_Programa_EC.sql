-- ====================================================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 27/10/2014
-- Description:	Modificaci�n de nombre de opci�n en el men� de contratos
-- solicitado por ICBF: Solicitud Modificaci�n Plan de Compras
-- ====================================================================
USE SIA

DECLARE @idModuloContratos INT
SELECT @idModuloContratos=IdModulo FROM SEG.Modulo WHERE NombreModulo = 'Contratos'

DECLARE @idPrograma INT
select @idPrograma=IdPrograma from SEG.Programa where CodigoPrograma = 'Contratos/SolicitudModPlanCompras'

update SEG.Programa
set NombrePrograma = 'Solicitud Modificaci�n Plan de Compras'
where IdPrograma=@idPrograma