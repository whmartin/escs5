USE [SIA]
GO

IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_NumeroProcesos_Modificar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Modificar]
GO


-- =============================================
-- Author:		Efraín Díaz Mejía
-- Create date:  5/7/2014 5:55:13 PM
-- Description:	Procedimiento almacenado que actualiza un(a) NumeroProcesos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_NumeroProcesos_Modificar]
		@IdNumeroProceso INT,	@NumeroProceso INT,	@Inactivo BIT,	@NumeroProcesoGenerado NVARCHAR(30),	@IdModalidadSeleccion INT,	@IdRegional INT,	@IdVigencia INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE CONTRATO.NumeroProcesos
	SET	NumeroProceso = @NumeroProceso,
		Inactivo = @Inactivo,
		NumeroProcesoGenerado = @NumeroProcesoGenerado,
		IdModalidadSeleccion = @IdModalidadSeleccion,
		IdRegional = @IdRegional,
		IdVigencia = @IdVigencia,
		UsuarioModifica = @UsuarioModifica,
		FechaModifica = GETDATE()
	WHERE IdNumeroProceso = @IdNumeroProceso
END
