USE [SIA]
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Consultar]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Consultar]
GO

-- =============================================
-- Author: Eduardo Isaac Ballesteros Mu�oz
-- Create date:  7/1/2014 13:59:06
-- Description:	Procedimiento almacenado que consulta un(a) DirectorInterventoria
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Consultar]
    @IDDirectorInterventoria INT
AS 
    BEGIN
        SELECT  IDDirectorInterventoria ,
                IdTipoIdentificacion ,
                NumeroIdentificacion ,
                PrimerNombre ,
                SegundoNombre ,
                PrimerApellido ,
                SegundoApellido ,
                Celular ,
                Telefono ,
                CorreoElectronico ,
                UsuarioCrea ,
                FechaCrea ,
                UsuarioModifica ,
                FechaModifica
        FROM    [CONTRATO].[DirectorInterventoria]
        WHERE   IDDirectorInterventoria = @IDDirectorInterventoria
    END

