USE [SIA]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_SIA_CONTRATO_ContratosCDP_Eliminar')
BEGIN
/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ContratosCDP_Eliminar]    Script Date: 08/07/2014 04:29:26 p.m. ******/
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosCDP_Eliminar]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_ContratosCDP_Eliminar]    Script Date: 08/07/2014 04:29:26 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapi�a
-- Create date: 08/07/2014
-- Description:	M�todo de eliminar para la entidad RegistroInformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_ContratosCDP_Eliminar]
	@IdContratosCDP INT
AS
BEGIN
	
	DELETE FROM [CONTRATO].[ContratosCDP]
	WHERE [IdContratosCDP] = @IdContratosCDP

END

GO


