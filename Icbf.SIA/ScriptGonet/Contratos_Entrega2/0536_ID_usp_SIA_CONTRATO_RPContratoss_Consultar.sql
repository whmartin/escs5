USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]    Script Date: 29/07/2014 14:21:28 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]
GO


-- =============================================
-- Author:		Gonet / Efrain Diaz Mejia
-- Create date:  09/07/2014 18:20
-- Description: procedimiento almacenado que consulta la informacion de los RP asociado a un contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_RPContratoss_Consultar]
	@IdContrato INT = NULL,
	@IdRP INT = NULL
AS
BEGIN
	SELECT
		RPC.[IdRPContratos],
		RPC.[IdContrato],
		RPC.[IdRP],
		RPC.[UsuarioCrea],
		RPC.[FechaCrea],
		RPC.[UsuarioModifica],
		RPC.[FechaModifica],
		IAG.ValorInicialItem ValorRP,
		R.CodigoRegional CodigoRegional,
		RTRIM(LTRIM(R.CodigoRegional)) + ' - ' + RTRIM(LTRIM(R.NombreRegional)) NombreRegional
	FROM [CONTRATO].[RPContratos] RPC
	INNER JOIN [Ppto].[CompromisosPresupuestales] CP
		ON RPC.IdRP = CP.IdCompromiso
	INNER JOIN [Ppto].[CompItemAfectacionGasto] IAG
		ON IAG.IdCompromiso = CP.IdCompromiso
	INNER JOIN Ppto.RegionalesPCI RPCI
		ON RPCI.IdRegionalPCI  = CP.IdRegionalPCI 
	INNER JOIN DIV.Regional R
		ON RPCI.CodRegionalIcbf collate database_default = R.CodigoRegional collate database_default
	WHERE RPC.IdRP =
		CASE
			WHEN @IdRP IS NULL THEN RPC.IdRP ELSE @IdRP
		END AND RPC.IdContrato =
		CASE
			WHEN @IdContrato IS NULL THEN RPC.IdContrato ELSE @IdContrato
		END
END



