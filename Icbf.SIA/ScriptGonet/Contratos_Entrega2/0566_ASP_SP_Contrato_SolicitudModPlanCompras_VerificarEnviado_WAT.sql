USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado]    Script Date: 01/08/2014 05:08:37 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado]
GO

-- ===========================================================================================
-- Author:		Abraham Rivero Domínguez
-- Create date:  22/05/2014 1:36:08 PM
-- Description:	Procedimiento almacenado que consulta un(a) SolicitudModPlanCompras
-- ===========================================================================================
-- ===========================================================================================
-- Author:			Wilmer Alvarez Tirado
-- Create date:		01/08/2014 17:00
-- Description:		Se corrige el cruce de plan de compras
-- ===========================================================================================
create procedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado]
@CodEstado nvarchar(24),
@NumeroConsecPlanCompras INT,
@Vigencia varchar(8)
as
begin
		select	smc.IdSolicitudModPlanCompra,
				pcc.IDPlanDeCompras,
				esc.Descripcion AS EstadoSolicitud
		from	CONTRATO.SolicitudModPlanCompras smc inner join 
				CONTRATO.PlanDeComprasContratos pcc on smc.IdPlanDeCompras = pcc.IDPlanDeComprasContratos inner join 
				CONTRATO.EstadoSolcitudModPlanCompras esc on smc.IdEstadoSolicitud = esc.IdEstadoSolicitud
		where	esc.CodEstado = @CodEstado
				and pcc.IDPlanDeCompras = @NumeroConsecPlanCompras
				and pcc.Vigencia = @Vigencia
end
go