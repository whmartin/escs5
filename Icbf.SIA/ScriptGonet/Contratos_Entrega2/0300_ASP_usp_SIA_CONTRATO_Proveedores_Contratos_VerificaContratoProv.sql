USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_VerificaContratoProv]    Script Date: 03/07/2014 10:13:22 ******/
IF  EXISTS (SELECT
	*
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_VerificaContratoProv]') AND type IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_VerificaContratoProv]
GO


-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/19/2014 9:16:14 AM
-- Description:	Procedimiento almacenado que consulta un(a) Proveedores_Contratos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_Proveedores_Contratos_VerificaContratoProv]
	 @IdProveedores INT,	@IdContrato INT
AS
BEGIN
	SELECT IdProveedoresContratos
	FROM [CONTRATO].[ProveedoresContratos]
	WHERE IdProveedores = @IdProveedores
	and IdContrato = @IdContrato

  
END


