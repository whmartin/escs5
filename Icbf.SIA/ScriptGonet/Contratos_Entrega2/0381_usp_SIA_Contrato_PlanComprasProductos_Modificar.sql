USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasProductos_Modificar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Modificar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:32 PM
-- Description:	Procedimiento almacenado que actualiza un(a) PlanComprasProductos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Modificar]
		@IDProductoPlanCompraContrato	INT,
		@IDPlanDeComprasContratos		INT,
		@CodigoProducto					NVARCHAR(30),
		@CantidadCupos					NUMERIC(15,2),
		@UsuarioModifica				NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ProductoPlanCompraContratos 
	SET CantidadCupos = @CantidadCupos
	   ,UsuarioModifica = @UsuarioModifica
	   ,FechaModifica = GETDATE() 
	WHERE IDProducto = @CodigoProducto
		and IDProductoPlanCompraContrato = @IDProductoPlanCompraContrato
		and IDPlanDeComprasContratos = @IDPlanDeComprasContratos
END
