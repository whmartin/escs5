USE [SIA]
GO
-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:27 PM
-- =============================================
IF NOT  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Contrato.PlanDeComprasContratos') AND type in (N'U'))
BEGIN

CREATE TABLE Contrato.PlanDeComprasContratos
(
	 IDPlanDeComprasContratos 	Int NOT NULL,
	 IdContrato 				Int NOT NULL,
	 IDPlanDeCompras 			Int NULL,
	 Vigencia 					Varchar(8) NOT NULL,
	 UsuarioCrea				NVARCHAR (250)  NOT NULL,
	 FechaCrea					DATETIME  NOT NULL,
	 UsuarioModifica			NVARCHAR (250),
	 FechaModifica				DATETIME
)
--go

-- Add keys for table PlanDeComprasContratos
ALTER TABLE Contrato.PlanDeComprasContratos
ADD CONSTRAINT PK_Contrato_PlanDeComprasContratos
PRIMARY KEY(IDPlanDeComprasContratos)
--GO
ALTER TABLE Contrato.PlanDeComprasContratos
ADD CONSTRAINT FK_PlanDeComprasContratos_Contrato
FOREIGN KEY(IdContrato)
REFERENCES CONTRATO.Contrato(IdContrato)
END
--GO

GO
