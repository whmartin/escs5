USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Eliminacion table [CONTRATO].[SupervisorIntervContrato]
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_SupervisorIntervContrato_TipoSupvInterventor]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[SupervisorIntervContrato]'))
ALTER TABLE [CONTRATO].[SupervisorIntervContrato] DROP CONSTRAINT [FK_SupervisorIntervContrato_TipoSupvInterventor]
GO

/****** Object:  Table [CONTRATO].[SupervisorIntervContrato]    Script Date: 23/05/2014 16:05:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[SupervisorIntervContrato]') AND type in (N'U'))
DROP TABLE [CONTRATO].[SupervisorIntervContrato]
GO

