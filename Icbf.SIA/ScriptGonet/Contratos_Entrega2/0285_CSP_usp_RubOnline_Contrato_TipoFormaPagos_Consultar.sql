USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_TipoFormaPagos_Consultar]    Script Date: 01/07/2014 16:04:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RubOnline_Contrato_TipoFormaPagos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPagos_Consultar]
GO


/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoFormaPagos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoFormaPagos_Consultar]

	@NombreTipoFormaPago NVARCHAR (50) = NULL,
	@Descripcion NVARCHAR (128) = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdTipoFormaPago,
		NombreTipoFormaPago,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoFormaPago]
	WHERE NombreTipoFormaPago  LIKE '%' + 
		CASE
			WHEN @NombreTipoFormaPago IS NULL THEN NombreTipoFormaPago ELSE @NombreTipoFormaPago
		END + '%'
	AND Descripcion  LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreTipoFormaPago

END

