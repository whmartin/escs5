USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_Contratos_SuscritosUsuario]    Script Date: 04/08/2014 02:51:03 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Contratos_SuscritosUsuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Contratos_SuscritosUsuario]
GO

/****** Object:  StoredProcedure [dbo].[usp_Contratos_SuscritosUsuario]    Script Date: 04/08/2014 02:51:03 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet\Carlos Cardenas
-- Description:	Procedimiento almacenado que consulta los contratos suscrito dependiendo el usuario

-- =============================================
CREATE PROCEDURE [dbo].[usp_Contratos_SuscritosUsuario]
		@FechaRegistroDesde DATETIME = NULL,
		@FechaRegistroHasta DATETIME = NULL,
		@NumeroContratoConvenio VARCHAR (50) = NULL,
		@VigenciaFiscalInicial VARCHAR = NULL,
		@RegionalContrato INT = NULL,
		@CategoriaContrato INT = NULL,
		@TipoContrato INT = NULL,
		@IdUsuario INT = NULL
	AS
	BEGIN
	
		 DECLARE @IdTipoUsuario INT
		 DECLARE @IdRegionalUsuario INT
		 DECLARE @IdRegional INT = NULL
		 DECLARE @IdVigenciaInicial INT
		 DECLARE @EstadoSuscrito INT

		 SELECT @IdRegionalUsuario=IdRegional,@IdTipoUsuario=IdTipoUsuario FROM SEG.Usuario WHERE IdUsuario=@IdUsuario
		 IF @IdTipoUsuario=2 BEGIN SET @IdRegional=@IdRegionalUsuario END

		 Select @IdVigenciaInicial=IdVigencia from global.Vigencia where AcnoVigencia = @VigenciaFiscalInicial

		 Select @EstadoSuscrito = IDEstadoContrato from CONTRATO.EstadoContrato where CodEstado = 'SUS'
		
		IF @IdRegional IS NOT NULL
		BEGIN
			select DISTINCT cont.IdContrato, cont.FechaCrea,cont.NumeroContrato,cont.IdVigenciaInicial,V.AcnoVigencia,
			cont.IdRegionalContrato,R.NombreRegional as Regional,
			cont.IdCategoriaContrato, CC.NombreCategoriaContrato,
			cont.IdTipoContrato,TC.NombreTipoContrato
			FROM CONTRATO.Contrato cont
			 left outer join DIV.Regional R on cont.IDRegionalContrato = R.IdRegional
			 left outer join CONTRATO.ModalidadSeleccion MS on MS.IdModalidad = cont.IdModalidadSeleccion
			 left outer join CONTRATO.CategoriaContrato CC on CC.IdCategoriaContrato = cont.IdCategoriaContrato
			 left outer join CONTRATO.TipoContrato TC on TC.IdTipoContrato = cont.IdTipoContrato
			 left outer join Contrato.EstadoContrato EC on EC.IDEstadoContrato = cont.IDEstadoContrato
			 left outer join Global.Vigencia V on V.IdVigencia = cont.IdVigenciaInicial
			 left outer join Global.Vigencia VF on VF.IdVigencia = cont.IdVigenciaFinal
			 left outer join CONTRATO.NumeroProcesos NP on NP.IdNumeroProceso = cont.IdNumeroProceso
			 left outer join KACTUS.KPRODII.dbo.Da_Emple EMP on EMP.ID_depen = cont.IdDependenciaSolicitante
			WHERE Cont.IdEstadoContrato = @EstadoSuscrito --cont.Suscrito = 1
			AND cont.FechaCrea >= CASE WHEN @FechaRegistroDesde IS NULL THEN cont.FechaCrea ELSE @FechaRegistroDesde END 
			AND cont.FechaCrea <= CASE WHEN @FechaRegistroHasta IS NULL THEN cont.FechaCrea ELSE @FechaRegistroHasta END 
			AND ISNULL(cont.NumeroContrato, '') = CASE WHEN @NumeroContratoConvenio IS NULL THEN ISNULL(cont.NumeroContrato, '') ELSE @NumeroContratoConvenio END 
			AND ISNULL(cont.IdVigenciaInicial, 0) = CASE WHEN @IdVigenciaInicial IS NULL THEN ISNULL(cont.IdVigenciaInicial, 0) ELSE @IdVigenciaInicial END 
			AND ISNULL(cont.IDRegionalContrato, 0) = CASE WHEN @RegionalContrato IS NULL THEN ISNULL(cont.IDRegionalContrato, 0) ELSE @RegionalContrato END 
		    AND ISNULL(cont.IDCategoriaContrato, 0) = CASE WHEN @CategoriaContrato IS NULL THEN ISNULL(cont.IDCategoriaContrato, 0) ELSE @CategoriaContrato END 
		    AND ISNULL(cont.IDTipoContrato, 0) = CASE WHEN @TipoContrato IS NULL THEN ISNULL(cont.IDTipoContrato, 0) ELSE @TipoContrato END 
			AND R.IdRegional = @IdRegional 
		END
		ELSE
		BEGIN
			select DISTINCT cont.IdContrato, cont.FechaCrea,cont.NumeroContrato,cont.IdVigenciaInicial,V.AcnoVigencia,
			cont.IdRegionalContrato,R.NombreRegional as Regional,
			cont.IdCategoriaContrato, CC.NombreCategoriaContrato,
			cont.IdTipoContrato,TC.NombreTipoContrato
			FROM CONTRATO.Contrato cont
			 left outer join DIV.Regional R on cont.IDRegionalContrato = R.IdRegional
			 left outer join CONTRATO.ModalidadSeleccion MS on MS.IdModalidad = cont.IdModalidadSeleccion
			 left outer join CONTRATO.CategoriaContrato CC on CC.IdCategoriaContrato = cont.IdCategoriaContrato
			 left outer join CONTRATO.TipoContrato TC on TC.IdTipoContrato = cont.IdTipoContrato
			 left outer join Contrato.EstadoContrato EC on EC.IDEstadoContrato = cont.IDEstadoContrato
			 left outer join Global.Vigencia V on V.IdVigencia = cont.IdVigenciaInicial
			 left outer join Global.Vigencia VF on VF.IdVigencia = cont.IdVigenciaFinal
			 left outer join CONTRATO.NumeroProcesos NP on NP.IdNumeroProceso = cont.IdNumeroProceso
			 left outer join KACTUS.KPRODII.dbo.Da_Emple EMP on EMP.ID_depen = cont.IdDependenciaSolicitante
			WHERE Cont.IdEstadoContrato = @EstadoSuscrito --cont.Suscrito = 1
			AND cont.FechaCrea >= CASE WHEN @FechaRegistroDesde IS NULL THEN cont.FechaCrea ELSE @FechaRegistroDesde END 
			AND cont.FechaCrea <= CASE WHEN @FechaRegistroHasta IS NULL THEN cont.FechaCrea ELSE @FechaRegistroHasta END 
			AND ISNULL(cont.NumeroContrato, '') = CASE WHEN @NumeroContratoConvenio IS NULL THEN ISNULL(cont.NumeroContrato, '') ELSE @NumeroContratoConvenio END 
			AND ISNULL(cont.IdVigenciaInicial, 0) = CASE WHEN @IdVigenciaInicial IS NULL THEN ISNULL(cont.IdVigenciaInicial, 0) ELSE @IdVigenciaInicial END 
			AND ISNULL(cont.IDRegionalContrato, 0) = CASE WHEN @RegionalContrato IS NULL THEN ISNULL(cont.IDRegionalContrato, 0) ELSE @RegionalContrato END 
		    AND ISNULL(cont.IDCategoriaContrato, 0) = CASE WHEN @CategoriaContrato IS NULL THEN ISNULL(cont.IDCategoriaContrato, 0) ELSE @CategoriaContrato END 
		    AND ISNULL(cont.IDTipoContrato, 0) = CASE WHEN @TipoContrato IS NULL THEN ISNULL(cont.IDTipoContrato, 0) ELSE @TipoContrato END 
 
		END

		IF @@ROWCOUNT >= 500
		BEGIN
			RAISERROR('La consulta arroja demasiados resultados,por favor refine su consulta',16,1)
			RETURN
		END
	END