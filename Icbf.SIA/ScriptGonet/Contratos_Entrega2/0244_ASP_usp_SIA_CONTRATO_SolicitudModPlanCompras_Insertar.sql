USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar]    Script Date: 25/06/2014 12:18:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar]
GO

-- =============================================
-- Author:		Abraham Rivero Dom�nguez
-- Create date:  5/22/2014 1:36:08 PM
-- Description:	Procedimiento almacenado que guarda un nuevo SolicitudModPlanCompras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar]
		@IdSolicitudModPlanCompra INT OUTPUT, 	
		@Justificacion NVARCHAR(896),	
		@NumeroSolicitud INT,	
		@FechaSolicitud DATETIME,	
		@IdEstadoSolicitud NVARCHAR(1),	
		@IdUsuarioSolicitud INT,	
		@IdPlanDeCompras INT,		
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO CONTRATO.SolicitudModPlanCompras(Justificacion, NumeroSolicitud, FechaSolicitud, IdEstadoSolicitud, IdUsuarioSolicitud, IdPlanDeCompras, UsuarioCrea, FechaCrea)
					  VALUES(@Justificacion, @NumeroSolicitud, @FechaSolicitud, @IdEstadoSolicitud, @IdUsuarioSolicitud, @IdPlanDeCompras, @UsuarioCrea, GETDATE())
	SELECT @IdSolicitudModPlanCompra = @@IDENTITY 		
END

