USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_PlanComprasProductos_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Consultar]
GO

-- =============================================
-- Author:		José Ignacio De Los Reyes
-- Create date:  6/24/2014 2:38:32 PM
-- Description:	Procedimiento almacenado que consulta un(a) PlanComprasProductos
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_PlanComprasProductos_Consultar]	
	@IdProductoPlanCompraContrato INT
AS
BEGIN
	SELECT PCC.IDPlanDeCompras NumeroConsecutivoPlanCompras,
		PPC.IDProductoPlanCompraContrato,
		PPC.IDProducto CodigoProducto,
		PPC.CantidadCupos,
		PCC.IDPlanDeComprasContratos,
		PPC.UsuarioCrea,
		PPC.FechaCrea,
		PPC.UsuarioModifica,
		PPC.FechaModifica
	FROM [Contrato].[ProductoPlanCompraContratos] PPC
		INNER JOIN CONTRATO.PlanDeComprasContratos PCC ON PPC.IDPlanDeComprasContratos = PCC.IDPlanDeComprasContratos
	WHERE PPC.IDProductoPlanCompraContrato = COALESCE(@IDProductoPlanCompraContrato,PPC.IDProductoPlanCompraContrato)
END
