USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 26/05/2014 11:55:13 
-- Description:	Eliminacion table [CONTRATO].[Garantia]
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[CONTRATO].[FK_Garantia_RelacionarContratistas]') AND parent_object_id = OBJECT_ID(N'[CONTRATO].[Garantia]'))
ALTER TABLE [CONTRATO].[Garantia] DROP CONSTRAINT [FK_Garantia_RelacionarContratistas]
GO

/****** Object:  Table [CONTRATO].[Garantia]    Script Date: 23/05/2014 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CONTRATO].[Garantia]') AND type in (N'U'))
DROP TABLE [CONTRATO].[Garantia]
GO

