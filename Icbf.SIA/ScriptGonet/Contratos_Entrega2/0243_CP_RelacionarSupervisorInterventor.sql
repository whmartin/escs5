USE [SIA]
GO
   -- =============================================
   -- Author:              Jorge Vizcaino
   -- Create date:         15/06/2013 08:12
   -- Description:         Inserta la tabla para ser visualizada en la aplicación
   -- =============================================
IF not exists(select idprograma from SEG.Programa where CodigoPrograma = 'Contratos/RelacionarSupervisorInterventor' and NombrePrograma = 'Asociar Supervisor/Interventorr' )
begin
      declare @idpadre int
      select @idpadre = IdPrograma from SEG.Programa where NombrePrograma = 'Contrato'
      insert INTO SEG.Programa  (IdModulo,
								NombrePrograma,
								CodigoPrograma,
								Posicion,
								Estado,
								UsuarioCreacion,
								FechaCreacion,
								UsuarioModificacion,
								FechaModificacion,
								VisibleMenu,
								generaLog,
								Padre)
      SELECT IdModulo,'Asociar Supervisor/Interventorr','Contratos/RelacionarSupervisorInterventor',1,1,'Administrador',GETDATE(),'','',0,1,NULL
      from SEG.Modulo
      where NombreModulo = 'CONTRATOS'    

      INSERT INTO SEG.Permiso 
      SELECT idprograma,1,1,1,1,1,'Administrador',GETDATE(),NULL,NULL
      from SEG.Programa
      where CodigoPrograma = 'Contratos/RelacionarSupervisorInterventor'
      and NombrePrograma = 'Asociar Supervisor/Interventorr'
end
else
begin
      print 'YA EXISTE EL MODULO A CREAR'
end
