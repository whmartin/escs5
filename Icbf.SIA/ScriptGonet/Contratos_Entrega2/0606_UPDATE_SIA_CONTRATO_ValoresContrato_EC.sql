USE [SIA]
GO

IF EXISTS(SELECT * FROM [CONTRATO].[ValoresContrato] WHERE 
			Desde = 500000000.00)
BEGIN
	UPDATE [CONTRATO].[ValoresContrato]
	SET Hasta = 9999999999.00, 
	UsuarioModifica = 'Administrador', 
	FechaModifica = GETDATE()
	WHERE Desde = 500000000.00
END
ELSE
BEGIN
	INSERT INTO [CONTRATO].[ValoresContrato]
		(Desde, Hasta, UsuarioCrea, FechaCrea)
	VALUES
		(500000000.00, 9999999999.00, 'Administrador', GETDATE())
END