USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_ValoresContrato]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_ValoresContrato]
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  6/4/2014 11:59:59 AM
-- Description:	Procedimiento almacenado que consulta el n�mero del convenio 
-- asi como valor inicial del contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_ValoresContrato]
	@IdContrato INT
AS
BEGIN
	select con.IdContrato,con.NumeroContrato, con.ValorInicialContrato,con.UsuarioCrea,con.FechaCrea,con.FechaSuscripcionContrato as FechaSuscripcion
	from CONTRATO.Contrato con
	INNER JOIN CONTRATO.Garantia gar ON con.IdContrato = gar.IdContrato
	--LEFT JOIN contrato.ContratoSuscrito consus ON con.IdContrato = consus.IdContrato
	where
	con.Suscrito = 1 AND
	con.IdContrato = @IdContrato
END