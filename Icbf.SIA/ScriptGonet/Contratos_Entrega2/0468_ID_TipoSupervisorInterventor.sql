USE [SIA]
GO

DELETE FROM [CONTRATO].[TipoSupervisorInterventor]
GO
SET IDENTITY_INSERT [CONTRATO].[TipoSupervisorInterventor] ON 

-- Autor: Carlos Andrés Cárdenas
-- Fecha: 15/07/2014
-- Descripción: se inserta los valores en la tabla TipoSupervisorInterventor

INSERT [CONTRATO].[TipoSupervisorInterventor] ([IDTipoSupervisorInterventor], [Codigo], [Descripcion], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (1, N'01', N'Supervisor', N'Administrador', CAST(0x0000A34500C9D37D AS DateTime), NULL, NULL)
INSERT [CONTRATO].[TipoSupervisorInterventor] ([IDTipoSupervisorInterventor], [Codigo], [Descripcion], [UsuarioCrea], [FechaCrea], [UsuarioModifica], [FechaModifica]) VALUES (2, N'02', N'Interventor', N'Administrador', CAST(0x0000A34500C9D380 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [CONTRATO].[TipoSupervisorInterventor] OFF
