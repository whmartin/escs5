USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_ModificarSolicitante]    Script Date: 24/06/2014 02:01:45 p.m. ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_RubOnline_Contrato_Contrato_ModificarSolicitante')
BEGIN
DROP PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_ModificarSolicitante]
END
GO

/****** Object:  StoredProcedure [dbo].[usp_RubOnline_Contrato_Contrato_ModificarSolicitante]    Script Date: 24/06/2014 02:01:45 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gonet/Emilio Calapina
-- Create date: 24/06/2014
-- Description:	Asocia la informacion del solicitante al contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_ModificarSolicitante]
	@IdContrato INT,
	@IdRegionalEmpSol INT = NULL, 
	@IdDependenciaEmpSol INT = NULL, 
	@IdCargoEmpSol INT = NULL, 
	@IdEmpleadoSolicitante INT = NULL
AS
BEGIN
	
	UPDATE CONTRATO.Contrato
	SET IdEmpleadoSolicitante = @IdEmpleadoSolicitante,
	IdRegionalSolicitante = @IdRegionalEmpSol,
	IdDependenciaSolicitante = @IdDependenciaEmpSol,
	IdCargoSolicitante = @IdCargoEmpSol
	WHERE IdContrato = @IdContrato

END

GO


