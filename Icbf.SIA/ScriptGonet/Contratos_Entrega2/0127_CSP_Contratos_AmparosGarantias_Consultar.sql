USE [SIA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_AmparosGarantias_Consultar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantias_Consultar]
GO

-- =============================================
-- Author:		Carlos Andrés Cárdenas
-- Create date:  5/30/2014 2:33:59 PM
-- Description:	Procedimiento almacenado que consulta un(a) AmparosGarantias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_AmparosGarantias_Consultar]
	@IDAmparosGarantias INT
AS
BEGIN
 SELECT IDAmparosGarantias, IDGarantia, IdTipoAmparo, FechaVigenciaDesde, FechaVigenciaHasta, IDUnidadCalculo, ValorCalculoAsegurado, ValorAsegurado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AmparosGarantias] WHERE  IDAmparosGarantias = @IDAmparosGarantias
END
