USE [SIA]
GO

-- =============================================
-- Author:		Gonet
-- Create date: 2014-07-09
-- Description:	Crear Sinonimo [Ppto].[DependenciasAfectacionSIIF]
-- =============================================

/****** Object:  Synonym [Ppto].[Ppto.DependenciasAfectacionSIIF]    Script Date: 01/07/2014 16:30:47 ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'DependenciasAfectacionSIIF' AND schema_id = SCHEMA_ID(N'Ppto'))
DROP SYNONYM [Ppto].[DependenciasAfectacionSIIF]
GO

/****** Object:  Synonym [Ppto].[Ppto.DependenciasAfectacionSIIF]    Script Date: 01/07/2014 16:30:47 ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'DependenciasAfectacionSIIF' AND schema_id = SCHEMA_ID(N'Ppto'))
CREATE SYNONYM [Ppto].[DependenciasAfectacionSIIF] FOR [NMF].[Ppto].[DependenciasAfectacionSIIF]
GO


