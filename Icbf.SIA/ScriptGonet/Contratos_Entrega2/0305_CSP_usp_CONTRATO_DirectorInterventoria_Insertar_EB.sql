USE [SIA]
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Insertar]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Insertar]
GO

-- =============================================
-- Author: Eduardo Isaac Ballesteros Mu�oz
-- Create date: 7/1/2014 12:59:06 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DirectorInterventoria
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_CONTRATO_DirectorInterventoria_Insertar]
    @IDDirectorInterventoria INT OUTPUT ,
    @IdTipoIdentificacion INT ,
    @NumeroIdentificacion NVARCHAR(24) ,
    @PrimerNombre NVARCHAR(80) ,
    @SegundoNombre NVARCHAR(80) ,
    @PrimerApellido NVARCHAR(80) ,
    @SegundoApellido NVARCHAR(80) ,
    @Celular NVARCHAR(24) ,
    @Telefono NVARCHAR(24) ,
    @CorreoElectronico NVARCHAR(80) ,
    @UsuarioCrea NVARCHAR(250)
AS 
    BEGIN
        INSERT  INTO CONTRATO.DirectorInterventoria
                ( IdTipoIdentificacion ,
                  NumeroIdentificacion ,
                  PrimerNombre ,
                  SegundoNombre ,
                  PrimerApellido ,
                  SegundoApellido ,
                  Celular ,
                  Telefono ,
                  CorreoElectronico ,
                  UsuarioCrea ,
                  FechaCrea
		        )
        VALUES  ( @IdTipoIdentificacion ,
                  @NumeroIdentificacion ,
                  @PrimerNombre ,
                  @SegundoNombre ,
                  @PrimerApellido ,
                  @SegundoApellido ,
                  @Celular ,
                  @Telefono ,
                  @CorreoElectronico ,
                  @UsuarioCrea ,
                  GETDATE()
                )

        SELECT  @IDDirectorInterventoria = @@IDENTITY
    END

