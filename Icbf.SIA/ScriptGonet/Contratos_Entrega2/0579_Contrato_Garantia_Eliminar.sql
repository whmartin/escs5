USE [SIA]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_Garantia_Eliminar]    Script Date: 12/08/2014 03:13:06 p.m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIA_Contrato_Garantia_Eliminar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Eliminar]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIA_Contrato_Garantia_Eliminar]    Script Date: 12/08/2014 03:13:06 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Andr�s C�rdenas
-- Create date:  6/4/2014 9:59:59 AM
-- Description:	Procedimiento almacenado que elimina un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Contrato_Garantia_Eliminar]
	@IDGarantia INT
AS
BEGIN
	DECLARE @Estado Varchar
	SELECT @Estado = esgar.DescripcionEstadoGarantia from CONTRATO.Garantia gar
	INNER JOIN CONTRATO.EstadosGarantias esgar ON gar.IDEstadosGarantias = esgar.IDEstadosGarantias
	WHERE IDGarantia = @IDGarantia

	if(@Estado <> 'Aprobada')
	BEGIN
		DELETE CONTRATO.AmparosGarantias WHERE IDGarantia = @IDGarantia
		DELETE Contrato.Garantia WHERE IDGarantia = @IDGarantia
	END
	ELSE
	BEGIN
		RAISERROR('La garant�a no pudo ser eliminada ya que se encuentra en estado de Aprobada',16,1)
	    RETURN
	END

END


