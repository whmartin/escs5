﻿//-----------------------------------------------------------------------
// <copyright file="MostrencosService.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase MostrencosService.</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.SIA.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icbf.Mostrencos.Business;
    using Icbf.Mostrencos.Entity;
    using Icbf.SIA.Entity;
    using Icbf.Utilities.Exceptions;
    using Icbf.Seguridad.Entity;

    /// <summary>
    /// Clase MostrencosService
    /// </summary>
    public class MostrencosService
    {
        private AsignacionAbogadoBLL vAsignacionAbogadoBLL;
        private RegistroDenunciaBLL vRegistroDenunciaBLL;
        private TipoIdentificacionBLL vTipoIdentificacionBLL;
        private RegistrosDocumentosBienDenunciadoBLL vRegistrosDocumentosBienDenunciadoBLL;
        private RegistroCalidadDenuncianteBLL vRegistroCalidadDenuncianteBLL;
        private RegistroCalidadDenuncianteCitacionBLL vRegistroCalidadDenuncianteCitacionBLL;
        private RegistrarProtocolizacionBLL vRegistrarProtocolizacionBLL;
        private RegionalesBLL vRegionalesBLL;
        private TipoDocumentoBienDenunciadoBLL vTipoDocumentoBienDenunciadoBLL;
        private HistoricoEstadosDenunciaBienBLL vHistoricoEstadosDenunciaBienBLL;
        private TiposDocumentosGlobalBLL vTiposDocumentosGlobalBLL;
        private DenunciaBienBLL vDenunciaBienBLL;
        private AbogadosBLL vAbogadosBLL;
        private RegionalBLL vRegionalBLL;
        private EstadoTerceroBLL vEstadoTerceroBLL;
        private TerceroBLL vTerceroBLL;
        private EstadoDenunciaBLL vEstadoDenunciaBLL;
        private TrasladoDenunciaBLL vTrasladoDenunciaBLL;
        private HistoricoAsignacionDenunciasBLL vHistoricoAsignacionDenunciasBLL;
        private CausantesBLL vCausantesBLL;
        private DocumentosSolicitadosDenunciaBienBLL vDocumentosSolicitadosDenunciaBienBLL;
        private EstadoDocumentoBLL vEstadoDocumentoBLL;
        private HistoricoDocumentosSolicitadosDenunciaBienBLL vHistoricoDocumentosSolicitadosDenunciaBienBLL;
        private RolBLL vRolBLL;
        private ApoderadosBLL vApoderadosBLL;
        private FiltrosMostrencosBLL vfiltrosMostrencosBLL;
        private ContratoParticipacionEconomicaBLL vContratoDenunciaBienBLL;
        private VigenciasBLL vVigenciaBLL;
        private DenunciaMuebleTituloBLL vDenunciaMuebleTituloBLL;
        private TituloValorBLL vTituloValorBLL;
        private MuebleInmuebleBLL vMuebleInmuebleBLL;
        private DocumentosSolicitadosYRecibidosBLL vDocumentosSolicitadosYRecibidosBLL;
        private TipoTituloBLL vTipoTituloBLL;
        private ClaseEntradaBLL vClaseEntradaBLL;
        private EstadoFisicoBienBLL vEstadoFisicoBienBLL;
        private ClaseBienBLL vClaseBienBLL;
        private EntidadBancariaBLL vEntidadBancariaBLL;
        private EntidadEmisoraBLL vEntidadEmisoraBLL;
        private MarcaBienBLL vMarcaBienBLL;
        private SubTipoBienBLL vSubTipoBienBLL;
        private DestinacionEconomicaBLL vDestinacionEconomicaBLL;
        private GN_VDIPBLL vGN_VDIPBLL;
        private TipoBienBLL vTipoBienBLL;
        private DespachoJudicialBLL vDespachoJudicialBLL;
        private MedioComunicacionBLL vMedioComunicacionBLL;
        private InformeDenuncianteBLL vInformeDenuncianteBLL;
        private TramiteNotarialBLL vTramiteNotarialBLL;
        private TramiteJudicialBLL vTramiteJudicialBLL;
        private TipoDocumentoSoporteDenunciaBLL vTipoDocumentoSoporteDenunciaBLL;
        private AnulacionDenunciaBLL vAnulacionDenunciaBLL;
        private NotariasBLL vNotariasBLL;
        private TerceroBLL terceroBLL;
        private ContinuarOficioBLL vContinuarOficioBLL;
        private PagosBLL vPagosBLL;
        private FaseDenunciaBLL vFaseDenunciaBLL;
        private FeriadosBLL vFeriadosBLL;
        private DesicionAuto desicionAuto;
        private DecisionAutoInadmitida decisionAutoInadmitida;
        private DecisionRecurso decisionRecurso;
        private InformacionVentaBLL vInformacionVentaBLL;
        private ConfiguracionTiemposEjecucionProcesoBLL vConfiguracionTiemposEjecucionProcesoBLL;
        private AccionBLL vAccionBLL;
        private ActuacionBLL vActuacionBLL;

        public MostrencosService()
        {
            this.vAsignacionAbogadoBLL = new AsignacionAbogadoBLL();
            this.vRegistroDenunciaBLL = new RegistroDenunciaBLL();
            this.vTipoIdentificacionBLL = new TipoIdentificacionBLL();
            this.vRegistrosDocumentosBienDenunciadoBLL = new RegistrosDocumentosBienDenunciadoBLL();
            this.vRegistroCalidadDenuncianteBLL = new RegistroCalidadDenuncianteBLL();
            this.vRegistroCalidadDenuncianteCitacionBLL = new RegistroCalidadDenuncianteCitacionBLL();
            this.vRegistrarProtocolizacionBLL = new RegistrarProtocolizacionBLL();
            this.vRegionalesBLL = new RegionalesBLL();
            this.vTiposDocumentosGlobalBLL = new TiposDocumentosGlobalBLL();
            this.vHistoricoEstadosDenunciaBienBLL = new HistoricoEstadosDenunciaBienBLL();
            this.vDenunciaBienBLL = new DenunciaBienBLL();
            this.vRegionalBLL = new Mostrencos.Business.RegionalBLL();
            this.vEstadoTerceroBLL = new EstadoTerceroBLL();
            this.vTerceroBLL = new TerceroBLL();
            this.vEstadoDenunciaBLL = new EstadoDenunciaBLL();
            this.vAbogadosBLL = new AbogadosBLL();
            this.vTrasladoDenunciaBLL = new TrasladoDenunciaBLL();
            this.vHistoricoAsignacionDenunciasBLL = new HistoricoAsignacionDenunciasBLL();
            this.vTipoDocumentoBienDenunciadoBLL = new TipoDocumentoBienDenunciadoBLL();
            this.vCausantesBLL = new CausantesBLL();
            this.vDocumentosSolicitadosDenunciaBienBLL = new DocumentosSolicitadosDenunciaBienBLL();
            this.vEstadoDocumentoBLL = new EstadoDocumentoBLL();
            this.vHistoricoDocumentosSolicitadosDenunciaBienBLL = new HistoricoDocumentosSolicitadosDenunciaBienBLL();
            this.vApoderadosBLL = new ApoderadosBLL();
            this.vRolBLL = new RolBLL();
            this.vTituloValorBLL = new TituloValorBLL();
            this.vMuebleInmuebleBLL = new MuebleInmuebleBLL();
            this.vfiltrosMostrencosBLL = new FiltrosMostrencosBLL();
            this.vContratoDenunciaBienBLL = new ContratoParticipacionEconomicaBLL();
            this.vVigenciaBLL = new VigenciasBLL();
            this.vDenunciaMuebleTituloBLL = new DenunciaMuebleTituloBLL();
            this.vDocumentosSolicitadosYRecibidosBLL = new DocumentosSolicitadosYRecibidosBLL();
            this.vTipoTituloBLL = new TipoTituloBLL();
            this.vClaseEntradaBLL = new ClaseEntradaBLL();
            this.vEstadoFisicoBienBLL = new EstadoFisicoBienBLL();
            this.vClaseBienBLL = new ClaseBienBLL();
            this.vEntidadBancariaBLL = new EntidadBancariaBLL();
            this.vEntidadEmisoraBLL = new EntidadEmisoraBLL();
            this.vMarcaBienBLL = new MarcaBienBLL();
            this.vSubTipoBienBLL = new SubTipoBienBLL();
            this.vDestinacionEconomicaBLL = new DestinacionEconomicaBLL();
            this.vGN_VDIPBLL = new GN_VDIPBLL();
            this.vTipoBienBLL = new TipoBienBLL();
            this.vMedioComunicacionBLL = new MedioComunicacionBLL();
            this.vDespachoJudicialBLL = new DespachoJudicialBLL();
            this.vTramiteNotarialBLL = new TramiteNotarialBLL();
            this.vTramiteJudicialBLL = new TramiteJudicialBLL();
            this.vTipoDocumentoSoporteDenunciaBLL = new TipoDocumentoSoporteDenunciaBLL();
            this.vInformeDenuncianteBLL = new InformeDenuncianteBLL();
            this.vAnulacionDenunciaBLL = new AnulacionDenunciaBLL();
            this.vNotariasBLL = new NotariasBLL();
            this.terceroBLL = new TerceroBLL();
            this.vContinuarOficioBLL = new ContinuarOficioBLL();
            this.vPagosBLL = new PagosBLL();
            this.vFaseDenunciaBLL = new FaseDenunciaBLL();
            this.vFeriadosBLL = new FeriadosBLL();
            this.desicionAuto = new DesicionAuto();
            this.decisionAutoInadmitida = new DecisionAutoInadmitida();
            this.decisionRecurso = new DecisionRecurso();
            this.vInformacionVentaBLL = new InformacionVentaBLL();
            this.vConfiguracionTiemposEjecucionProcesoBLL = new ConfiguracionTiemposEjecucionProcesoBLL();
            this.vAccionBLL = new AccionBLL();
            this.vActuacionBLL = new ActuacionBLL();
        }

        #region Asignar Abogado

        /// <summary>
        /// Método de consulta para la descripción de la regional del usuario que inicó sesión
        /// </summary>
        /// <param name="pIdRegional"></param>
        /// <returns>Retorna el nombre de la regional</returns>
        public string CosultarRegionalUsuario(int pIdRegional)
        {
            try
            {
                return vAsignacionAbogadoBLL.CosultarRegionalUsuario(pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad Abogado
        /// </summary>
        /// <param name="pAsignacionAbogado"></param>
        /// <returns>Devuelve un indicador con el resultado de la inserción 0,1 ó 2 Dónde 0 es Error, 1 es afectacion de datos incorrecta y 2 es satisfatorio</returns>
        public int InsertarAsignacionAbogado(AsignacionAbogado pAsignacionAbogado)
        {
            try
            {
                return vAsignacionAbogadoBLL.InsertarAsignacionAbogado(pAsignacionAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método pra la consulta abogados por los parámetros de region, Nombre abogado ó profesión del abogado
        /// </summary>
        /// <param name="pAbogado"></param>
        /// <returns>Retorna una lista de los datos de la denuncia, nombre y regional de abogados.</returns>
        public List<AsignacionAbogado> ConsultarAbogados(AsignacionAbogado pAbogado)
        {
            try
            {
                return vAsignacionAbogadoBLL.ConsultarAbogados(pAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método pra la consulta de denuncias en estado REGISTADO(sin asignar) y por la regional del usuario con incio de sesión
        /// </summary>
        /// <param name="pConsultaAbogado"></param>
        /// <returns>Retorna una lista de los datos del abogado, tales como: correro, nombre, identificación ...</returns>
        public List<ConsultaAbogadoXDenuncia> ConsultarAsignacionAbogadosXDenuncia(ConsultaAbogadoXDenuncia pConsultaAbogado)
        {
            try
            {
                return vAsignacionAbogadoBLL.ConsultarAsignacionAbogadosXDenuncia(pConsultaAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ReporteDenuncias

        public List<FasesDenuncia> ConsultarFasesDenuncia()
        {
            try
            {
                return new FaseDenunciaBLL().ConsultarFasesDenuncia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().InsertarReporteDenuncias(pReporteDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().ModificarReporteDenuncias(pReporteDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().EliminarReporteDenuncias(pReporteDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ReporteDenuncias ConsultarReporteDenuncias(int pIdReporteDenuncias)
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().ConsultarReporteDenuncias(pIdReporteDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarReporteDenunciass(ReporteDenunciaFiltros pFiltros)
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().ConsultarReporteDenunciass(pFiltros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarTipoPersonaAsociacion()
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().ConsultarTipoPersonaAsociacion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarTipoDeIdentificacion()
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().ConsultarTipoDeIdentificacion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarEstadoDenuncia()
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().ConsultarEstadoDenuncia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarDepartamentoUbicacion()
        {
            try
            {
                return new Icbf.Mostrencos.Business.ReporteDenunciasBLL().ConsultarDepartamentoUbicacion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion ReporteDenuncias

        #region Registro Denuncia
        public int InsertarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                return vRegistroDenunciaBLL.InsertarRegistroDenuncia(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                return vRegistroDenunciaBLL.ModificarRegistroDenuncia(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                return vRegistroDenunciaBLL.EliminarRegistroDenuncia(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public RegistroDenuncia ConsultarRegistroDenuncia(int pIdDenunciaBien)
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarRegistroDenuncia(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public RegistroDenuncia ConsultarRegistroDenunciante(int pIdTipoDocIdentifica, String pNumeroIdentificacion)
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarRegistroDenunciante(pIdTipoDocIdentifica, pNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<RegistroDenuncia> ConsultarRegistroDenuncias(int? pIdTipoDocIdentifica, String pNumeroIdentificacion, String pDenunciante, String pRadicadoDenuncia)
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarRegistroDenuncias(pIdTipoDocIdentifica, pNumeroIdentificacion, pDenunciante, pRadicadoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public RegistroDenuncia ConsultarDocumentosBasicos(int pIdDenuncia, int pTipoDocumento)
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarDocumentosBasicos(pIdDenuncia, pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<DocumentosSolicitadosDenunciaBien> ConsultarOtrosDocumentos(int pIdDenuncia, int pTipoDocumento)
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarOtrosDocumentos(pIdDenuncia, pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ConsultarConsecutivoDenuncia()
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarConsecutivoDenuncia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ConsultarAnioUltimoRegistro()
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarAnioUltimoRegistro();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método para consultar el correo del enlace de área.
        /// </summary>
        /// <param name="pIdRegional">Parametro de consulta por id de regional</param>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoCoordinadorJuridico(int? pIdRegional, string pProviderKey)
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarCorreoCoordinadorJuridico(pIdRegional, pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar el correo del enlace de área.
        /// </summary>
        /// <param name="pIdRegional">Parametro de consulta por id de regional</param>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarNombreCoordinadorJuridico(string pCorreoCoordinadorJuridico)
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarNombreCoordinadorJuridico(pCorreoCoordinadorJuridico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar el correo del enlace de área.
        /// </summary>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoAdministrador(string pProviderKey)
        {
            try
            {
                return vRegistroDenunciaBLL.ConsultarCorreoAdministrador(pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TipoDocumento
        public List<TipoIdentificacion> ConsultarTipoIdentificacion()
        {
            try
            {
                return vTipoIdentificacionBLL.ConsultarTipoIdentificacion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumentosGlobal> ConsultarTiposIdentificacionGlobal(string pTiposDocumentos)
        {
            try
            {
                return vTiposDocumentosGlobalBLL.ConsultarTiposIdentificacionGlobal(pTiposDocumentos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Registrar Documentos Bien Denunciado

        /// <summary>
        /// Elimina los tipos de documentos Otros
        /// </summary>
        /// <param name="pRegistroDenuncia">Parametro que envía el IDDenunica y TipoDocumento</param>
        /// <returns>correos</returns>
        public int EliminarDenunciaDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pRegistroDenuncia)
        {
            try
            {
                return this.vRegistrosDocumentosBienDenunciadoBLL.EliminarDenunciaDocumentosSolicitadosDenunciaBien(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroDenuncia> ConsultarRegistroDocumentosBienDenunciado(int pIdRegional, int pEstado, string pRegistradoDesde, string pRegistradoHasta, string pRadicadoDenuncia, int pIdUsuario)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.ConsultarRegistroDocumentosBienDenunciado(pIdRegional, pEstado, pRegistradoDesde, pRegistradoHasta, pRadicadoDenuncia, pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Abogados ConsultarAbogadosPorIdUsuario(int pIdUsuario)
        {
            try
            {
                return vAbogadosBLL.ConsultarAbogadosPorIdUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Mostrencos.Entity.Regional ConsultarRegionalDelAbogado(int pIdRegional)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.ConsultarRegionalDelAbogado(pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroCalidadDenunciante> ConsultarCalidadDenunciante(int pIdDenuncia)
        {
            try
            {
                return vRegistroCalidadDenuncianteBLL.ConsultarCalidadDenunciante(pIdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DenunciaDatosEntidad> BuscarDatosEntidadXNombre(string pNombreRazonSocial, int pIdDenunciaBien)
        {
            try
            {
                return vRegistroCalidadDenuncianteBLL.BuscarDatosEntidadXNombre(pNombreRazonSocial, pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RegistroCalidadDenuncianteCitacion ConsultarCalidadDenuncianteCitacion(int IdReconocimientoCalidadDenuncianteDetalle)
        {
            try
            {
                return vRegistroCalidadDenuncianteCitacionBLL.ConsultarCalidadDenunciante(IdReconocimientoCalidadDenuncianteDetalle);
            }
            catch (UserInterfaceException exc)
            {
                throw new UserInterfaceException(exc.Message);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int SetCalidadDenuncianteCitacion(RegistroCalidadDenuncianteCitacion vRegistroCalidad)
        {
            try
            {
                return vRegistroCalidadDenuncianteCitacionBLL.SetCalidadDenuncianteCitacion(vRegistroCalidad);
            }
            catch (UserInterfaceException exc)
            {
                throw new UserInterfaceException(exc.Message);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarCalidadDenunciante(RegistroCalidadDenunciante vregistroCalidadDenunciante)
        {
            try
            {
                return vRegistroCalidadDenuncianteBLL.InsertarCalidadDenunciante(vregistroCalidadDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarCalidadDenunciante(RegistroCalidadDenunciante vregistroCalidadDenunciante)
        {
            try
            {
                return vRegistroCalidadDenuncianteBLL.ModificarCalidadDenunciante(vregistroCalidadDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Protocolizacion

        public RegistroDenuncia ConsultarDenunciaBienXId(int pIdDenuncia)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.ConsultarDenunciaBienXId(pIdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosXIdDenuncia(int pIdDenuncia, int pIdEstado)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.ConsultarDocumentosXIdDenuncia(pIdDenuncia, pIdEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public InfoAdjudicacion ConsultarDatosInfoAdjudicacion(int pIdDenuncia)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ConsultarDatosInfoAdjudicacion(pIdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public InfoEscrituracion ConsultarDatosInfoEscrituracion(int pIdDenuncia)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ConsultarDatosInfoEscrituracion(pIdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarTipoTramite(int pIdDenuncia)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ConsultarTipoTramite(pIdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<InfoMuebleProtocolizacion> ConsultarInfoMuebleXIdBienDenuncia(int IdDenunciaBien)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ConsultarInfoMuebleXIdBienDenuncia(IdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ResultFiltroTitulo> ConsultarTituloXIdDenuncia(int vInIntIdDenunciaBien)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ConsultarTituloXIdDenuncia(vInIntIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarTramiteJudicialxIdDenunciaBien(TramiteJudicial vDatoTramiteJudicial)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ActualizarTramiteJudicialxIdDenunciaBien(vDatoTramiteJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarEstadoBienxIdDenuncia(string vEstadoBien, int vIdDenunciaBien, DateTime? vFechaAdjudica, int vIdMuebleInmueble)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ActualizarEstadoBienxIdDenuncia(vEstadoBien, vIdDenunciaBien, vFechaAdjudica, vIdMuebleInmueble);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarTituloBienxIdDenuncia(string vEstadoBien, int vIdDenunciaBien, DateTime? vFechaAdjudica, int pIdTitulo)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ActualizarTituloBienxIdDenuncia(vEstadoBien, vIdDenunciaBien, vFechaAdjudica, pIdTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarFechaComunicacionTitulo(int vIdDenunciaBien, DateTime vFechaAComunicacion)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ActualizarFechaComunicacionTitulo(vIdDenunciaBien, vFechaAComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarFechaComunicacionNotarial(int vIdDenunciaBien, DateTime vFechaAComunicacion)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ActualizarFechaComunicacionNotarial(vIdDenunciaBien, vFechaAComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarTramiteNotarialxIdDenuncia(int vIdDenunciaBien, DateTime vFechaEscritura, decimal vNumeroEscritura,
            decimal vNumeroRegistro, DateTime vFechaIngresoICBF, DateTime vFechaInstrumentosP, DateTime vFechaComunicacion)
        {
            try
            {
                return vRegistrarProtocolizacionBLL.ActualizarTramiteNotarialxIdDenuncia(vIdDenunciaBien, vFechaEscritura, vNumeroEscritura,
                    vNumeroRegistro, vFechaIngresoICBF, vFechaInstrumentosP, vFechaComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoBienDenunciados()
        {
            try
            {
                List<TipoDocumentoBienDenunciado> vLstDocumentos = vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciados();
                return vLstDocumentos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosSolicitadosDenunciaBienObtieneId(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien, ref int IdDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.InsertarDocumentosSolicitadosDenunciaBienObtieneId(pDocumentosSolicitadosDenunciaBien, ref IdDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.InsertarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosSolicitadosDenunciaBienOtros(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.InsertarDocumentosSolicitadosDenunciaBienOtros(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.ModificarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarEstadoDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.ActualizarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDocumentosSolicitadosDenunciaBienRecibidos(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.ModificarDocumentosSolicitadosDenunciaBienRecibidos(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDenunciaDocumentosSolicitadosDenunciaBien(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoBLL.ModificarDenunciaDocumentosSolicitadosDenunciaBien(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Regional

        public Regionales ConsultarRegionalPorId(String idRegional)
        {
            try
            {
                return vRegionalesBLL.ConsultarRegionalesPorID(idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Regionales> ConsultarRegionales()
        {
            try
            {
                return vRegionalesBLL.ConsultarRegionales();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region HistoricoEstadosDenunciaBien
        /// <summary>
        /// consulta los históricos de los estados de una denuncia por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de la denuncia consultada </returns>
        public List<HistoricoEstadosDenunciaBien> HistoricoEstadosDenunciaBienConsultar(int pIdDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienBLL.HistoricoEstadosDenunciaBienConsultar(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarHistoricoEstadosDenunciaBien(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return vHistoricoEstadosDenunciaBienBLL.InsertarHistoricoEstadosDenunciaBien(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarHistoricoEstadosDenunciaXContratoParticipacion(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return vHistoricoEstadosDenunciaBienBLL.InsertarHistoricoEstadosDenunciaXContratoParticipacion(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Hisotrico de la denuncia
        /// </summary>
        /// <param name="pHistoricoEstadosDenunciaBien">Parámetro historico de la denuncia</param>
        /// <returns>Retorna excepción</returns>
        public int InsertarHistoricoEstadosDenunciaBienGeneral(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return vHistoricoEstadosDenunciaBienBLL.InsertarHistoricoEstadosDenunciaBienGeneral(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarHistoricoEstadosDenunciaXContratoParticipacionDocumentacionRecibida(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return vHistoricoEstadosDenunciaBienBLL.InsertarHistoricoEstadosDenunciaXContratoParticipacionDocumentacionRecibida(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public HistoricoEstadosDenunciaBien ConsultarHistoricoEstadosDenunciaBien(int pIdHistoricoEstadoDenunciaBien)
        {
            try
            {
                return vHistoricoEstadosDenunciaBienBLL.ConsultarHistoricoEstadosDenunciaBien(pIdHistoricoEstadoDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBiens()
        {
            try
            {
                return vHistoricoEstadosDenunciaBienBLL.ConsultarHistoricoEstadosDenunciaBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBienByID(int pIdDenunciaBien)
        {
            try
            {
                return vHistoricoEstadosDenunciaBienBLL.ConsultarHistoricoEstadosDenunciaBienByID(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Trasladar Denuncia

        /// <summary>
        /// Consulta todos los estados denuncia que estén activos
        /// </summary>
        /// <returns>Lista de estados denuncia</returns>
        public List<EstadoDenuncia> ConsultarEstadosDenuncia()
        {
            try
            {
                return new Icbf.Mostrencos.Business.EstadoDenunciaBLL().ConsultarEstadoDenuncia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una denuncia bien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">IdDenunciaBien que se va a consultar</param>
        /// <returns>Una variable de tipo DenunciaBien con la información encontrada</returns>
        public DenunciaBien ConsultaCompletaDenunciaBienPorId(int pIdDenuncia)
        {
            try
            {
                DenunciaBien vDenunciaBien = vDenunciaBienBLL.ConsultarDenunciaBienXIdDenunciaBien(pIdDenuncia);
                vDenunciaBien = ConsultarDatosDenunciaBien(vDenunciaBien);
                return vDenunciaBien;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los estados denuncia por el IdEstadoDenuncia
        /// </summary>
        /// <param name="pIdEstadoDenuncia">Id del estado denuncia</param>
        /// <returns>Estado denuncia consultado</returns>
        public EstadoDenuncia ConsultarEstadoDenunciaPorId(int pIdEstadoDenuncia)
        {
            try
            {
                return vEstadoDenunciaBLL.ConsultarEstadoDenunciaPorId(pIdEstadoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// onsulta toda la informacion de una denuncia
        /// </summary>
        /// <param name="pDenunciaBien">denuncia a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public DenunciaBien ConsultarDatosDenunciaBien(DenunciaBien pDenunciaBien)
        {
            try
            {
                pDenunciaBien.Abogados = vAbogadosBLL.ConsultarAbogadoXId(pDenunciaBien.IdAbogado);
                pDenunciaBien.EstadoDenuncia = vEstadoDenunciaBLL.ConsultarEstadoDenunciaPorId(pDenunciaBien.IdEstadoDenuncia);
                pDenunciaBien.Regional = vRegionalBLL.ConsultarRegionalXId(pDenunciaBien.IdRegionalUbicacion);
                pDenunciaBien.Tercero = vTerceroBLL.ConsultarTerceroXId(pDenunciaBien.IdTercero);
                pDenunciaBien.Tercero.TiposDocumentosGlobal = vTiposDocumentosGlobalBLL.ConsultarTiposIdentificacionGlobalPorId(pDenunciaBien.Tercero.IdTipoDocIdentifica);
                pDenunciaBien.ListaHistoricoEstadosDenunciaBien = vHistoricoEstadosDenunciaBienBLL.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pDenunciaBien.IdDenunciaBien);
                pDenunciaBien.ListaTrasladoDenuncia = vTrasladoDenunciaBLL.ConsultarTrasladoDenunciaPorIdDenunciaBien(pDenunciaBien.IdDenunciaBien);
                if (pDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                {
                    if (!string.IsNullOrEmpty(pDenunciaBien.Tercero.PrimerNombre))
                    {
                        pDenunciaBien.NombreMostrar = pDenunciaBien.Tercero.PrimerNombre;
                    }

                    if (!string.IsNullOrEmpty(pDenunciaBien.Tercero.SegundoNombre))
                    {
                        pDenunciaBien.NombreMostrar += " " + pDenunciaBien.Tercero.SegundoNombre;
                    }

                    if (!string.IsNullOrEmpty(pDenunciaBien.Tercero.PrimerApellido))
                    {
                        pDenunciaBien.NombreMostrar += " " + pDenunciaBien.Tercero.PrimerApellido;
                    }

                    if (!string.IsNullOrEmpty(pDenunciaBien.Tercero.SegundoApellido))
                    {
                        pDenunciaBien.NombreMostrar += " " + pDenunciaBien.Tercero.SegundoApellido;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(pDenunciaBien.Tercero.RazonSocial))
                    {
                        pDenunciaBien.NombreMostrar = pDenunciaBien.Tercero.RazonSocial;
                        pDenunciaBien.Tercero.NumeroIdentificacion = pDenunciaBien.Tercero.NumeroIdentificacion + '-' + pDenunciaBien.Tercero.DigitoVerificacion;
                    }
                }

                if (pDenunciaBien.ListaHistoricoEstadosDenunciaBien.Count() == 0)
                {
                    pDenunciaBien.FechaEstado = (pDenunciaBien.FechaAsignacionAbogado != null) ? Convert.ToDateTime(pDenunciaBien.FechaAsignacionAbogado) : Convert.ToDateTime(pDenunciaBien.FechaRadicadoDenuncia);
                }
                else
                {
                    pDenunciaBien.FechaEstado = pDenunciaBien.ListaHistoricoEstadosDenunciaBien.OrderBy(p => p.FechaCrea).LastOrDefault().FechaCrea;
                }

                if (string.IsNullOrEmpty(pDenunciaBien.Abogados.NombreAbogado))
                {
                    pDenunciaBien.Abogados.FechaAsignacionAbogado = null;
                }

                return pDenunciaBien;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// onsulta toda la informacion de una denuncia
        /// </summary>
        /// <param name="pDenunciaBien">denuncia a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public DenunciaBien ConsultarDatosDenunciaBienDecision(DenunciaBien pDenunciaBien)
        {
            try
            {
                pDenunciaBien.desicionAuto = vTramiteJudicialBLL.consultarDesicionAutoId(pDenunciaBien.IdDenunciaBien);
                pDenunciaBien.decisionAutoInadmitida = vTramiteJudicialBLL.consultarDecisionAutoInadmitidaId((pDenunciaBien.IdDenunciaBien));
                pDenunciaBien.decisionRecurso = vTramiteJudicialBLL.consultarDecisionRecursoId((pDenunciaBien.IdDenunciaBien));

                return pDenunciaBien;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una denuncia bien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">IdDenunciaBien que se va a consultar</param>
        /// <returns>Una variable de tipo DenunciaBien con la información encontrada</returns>
        public DenunciaBien ConsultarDenunciaBienXIdDenunciaBien(int pIdDenuncia)
        {
            try
            {
                return vDenunciaBienBLL.ConsultarDenunciaBienXIdDenunciaBien(pIdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los traslados de las denuncias por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo TrasladoDenuncia con la información consultada </returns>
        public List<TrasladoDenuncia> ConsultarTrasladoDenunciaPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return vTrasladoDenunciaBLL.ConsultarTrasladoDenunciaPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los traslados denuncia por varios parámetros
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia que contiene la información a consultar </param>
        /// <returns>lista de tipo TrasladoDenuncia con la información resultante de la consulta</returns>
        public List<TrasladoDenuncia> ConsultarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia)
        {
            try
            {
                return vTrasladoDenunciaBLL.ConsultarTrasladoDenuncia(pTrasladoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo traslado de la denuncia
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia con la información a insertar</param>
        /// <returns>id del TrasladoDenuncia que se inserto</returns>
        public int InsertarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia, HistoricoEstadosDenunciaBien pHistorico)
        {
            try
            {
                return vTrasladoDenunciaBLL.InsertarTrasladoDenuncia(pTrasladoDenuncia, pHistorico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta el ultimo numero consecutivo de los traslados
        /// </summary>
        /// <returns>ultimo numero consecutivo de los traslados</returns>
        public int ConsultarUltimoNumerotraslado()
        {
            try
            {
                return vTrasladoDenunciaBLL.ConsultarUltimoNumerotraslado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta el TrasladoDenuncia por el idTrasladoDenuncia
        /// </summary>
        /// <param name="pIdTrasladoDenuncia">idTrasladoDenuncia a consultar</param>
        /// <returns>variable de tipo TrasladoDenuncia con la información consultada</returns>
        public TrasladoDenuncia ConsultarTrasladoDenunciaPorId(int pIdTrasladoDenuncia)
        {
            try
            {
                return vTrasladoDenunciaBLL.ConsultarTrasladoDenunciaPorId(pIdTrasladoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información del abogado por el id de la regional a la que esta asignado
        /// </summary>
        /// <param name="pIdRegional">id de la regional que se va a consultar</param>
        /// <returns>Lista de tipo Abogados con la información consultada</returns>
        public Usuario ConsultarUsuarioCoordinadorPorIdRegional(int pIdRegional)
        {
            try
            {
                List<Abogados> vListaAbogados = vAbogadosBLL.ConsultarAbogadoXIdRegional(pIdRegional);
                List<Usuario> vListaUsuario = new List<Usuario>();
                foreach (Abogados item in vListaAbogados)
                {
                    vListaUsuario.Add(new SIAService().ConsultarUsuario(item.IdUsuario));
                }
                int vlista = vListaUsuario.Count();
                return vListaUsuario.FirstOrDefault();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los abogados por el IdAbogado
        /// </summary>
        /// <param name="pIdAbogado">Id del abogado a consultar</param>
        /// <returns>Lista de tipo Abogados con el resultado de la consulta</returns>
        public Abogados ConsultarAbogadoXId(int pIdAbogado)
        {
            try
            {
                return vAbogadosBLL.ConsultarAbogadoXId(pIdAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarDatosUsuario(int pIdUsuario)
        {
            try
            {
                return new SIAService().ConsultarDatosUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuarioPorIdUsuario(int pIdUsuario)
        {
            try
            {
                return new SIAService().ConsultarUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// edita un TrasladoDenuncia, cambia el estado de la denuncia  e inserta en el historial de estados de la denuncia 
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia con la información que se va aguardar</param>
        /// <param name="pDenunciabien">variable de tipo DenunciaBien con la información que se va aguardar</param>
        /// <param name="pHistorico">variable de tipo HistóricoEstadosDenunciaBien con la información que se va aguardar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia, DenunciaBien pDenunciabien, HistoricoEstadosDenunciaBien pHistorico)
        {
            try
            {
                return this.vTrasladoDenunciaBLL.EditarTrasladoDenuncia(pTrasladoDenuncia, pDenunciabien, pHistorico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// edita un TrasladoDenuncia
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo v con la información que se va aguardar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia)
        {
            try
            {
                return vTrasladoDenunciaBLL.EditarTrasladoDenuncia(pTrasladoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        // <summary>
        /// Edita la información de una denuncia
        /// </summary>
        /// <param name="pDenunciaBien">variable de tipo DenunciaBien que contiene la información que se va a almacenar </param>
        /// <returns>1 si se actualizó correctamente la denuncia </returns>
        public int EditarDenunciaBien(DenunciaBien pDenunciaBien)
        {
            try
            {
                return vDenunciaBienBLL.EditarDenunciaBien(pDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Actualizar campos IdFase, IdActuacion y IdAccion
        public int EditarDenunciaBienFase(int vDenunciaBien)
        {
            try
            {
                return vDenunciaBienBLL.EditarDenunciaBienFase(vDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EditarDocumentacionCompleta(int vDenunciaBien, bool vDocumentacion)
        {
            try
            {
                return vDenunciaBienBLL.EditarDocumentacionCompleta(vDenunciaBien, vDocumentacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualizar el Estado de la Denuncia Bien a Terminada
        /// </summary>
        /// <param name="vDenunciaBien">Id de la denuncia</param>
        /// <returns>Resultado de la operación</returns>
        public int EditarDenunciaBienEstado(int vDenunciaBien)
        {
            try
            {
                return vDenunciaBienBLL.EditarDenunciaBienEstado(vDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta las denuncias bien que coincidan con los parámetros de búsqueda
        /// </summary>
        /// <param name="pIdRegional">Id de la regional de la denuncia</param>
        /// <param name="pEstado">Estado de la denuncia</param>
        /// <param name="pRegistradoDesde">Fecha desde del registro de la denuncia</param>
        /// <param name="pRegistradoHasta">Fecha hasta del registro de la denuncia</param>
        /// <param name="pRadicadoDenuncia">radicado de la denuncia</param>
        /// <param name="pIdAbogado">id del abogado que esta asignado a la denuncia</param>
        /// <returns>Lista de denuncias que cumplen con los criterios de la búsqueda</returns>
        public List<DenunciaBien> ConsultarDenunciaBien(int pIdRegional, int pEstado, string pRegistradoDesde, string pRegistradoHasta, string pRadicadoDenuncia, int pIdAbogado)
        {
            try
            {
                List<DenunciaBien> vListaDenunciaBien = vDenunciaBienBLL.ConsultarDenunciaBien(pIdRegional, pEstado, pRegistradoDesde, pRegistradoHasta, pRadicadoDenuncia, pIdAbogado);
                for (int i = 0; i < vListaDenunciaBien.Count(); i++)
                {
                    vListaDenunciaBien[i] = ConsultarDatosDenunciaBien(vListaDenunciaBien[i]);
                }

                return vListaDenunciaBien;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Reasignar Abogado

        /// <summary>
        /// inserta un nuevo histórico asignación denuncia
        /// </summary>
        /// <param name="pHistoricoAsignacionDenuncias">variable de tipo Histórico Asignación Denuncias que contiene la información a insertar</param>
        /// <returns>id del Histórico Asignación Denuncias insertado</returns>
        public int InsertarHistoricoAsignacionDenuncias(HistoricoAsignacionDenuncias pHistoricoAsignacionDenuncias)
        {
            try
            {
                return vHistoricoAsignacionDenunciasBLL.InsertarHistoricoAsignacionDenuncias(pHistoricoAsignacionDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Histórico Asignación Denuncias por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de asignación de abogados</returns>
        public List<HistoricoAsignacionDenuncias> ConsultarHistoricoAsignacionDenunciasPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return vHistoricoAsignacionDenunciasBLL.ConsultarHistoricoAsignacionDenunciasPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo abogado
        /// </summary>
        /// <param name="pAbogados">Variable de tipoAbogados que contiene la información que se va a insertar</param>
        /// <returns> id del abogado insertado</returns>
        public int InsetrarAbogados(Abogados pAbogados)
        {
            try
            {
                return vAbogadosBLL.InsetrarAbogados(pAbogados);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los abogados de la base de datos SIA
        /// </summary>
        /// <param name="pConsulta">variable con la información a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public List<AsignacionAbogado> ConsultarAbogadosSoloEnSIA(AsignacionAbogado pConsulta)
        {
            try
            {
                return this.vAbogadosBLL.ConsultarAbogadosSoloEnSIA(pConsulta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Documentos de Soporte de la Denuncia
        /// <summary>
        /// Inserta un nuevo registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Variable de tipo TipoDocumentoBienDenunciado con los datos a insertar</param>
        /// <returns>IdTipoDocumentoBienDenunciado del registro insertado</returns>
        public int InsertarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoBLL.InsertarTipoDocumentoBienDenunciado(pTipoDocumentoBienDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// edita un registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Variable de tipo TipoDocumentoBienDenunciado con los datos a insertar</param>
        /// <returns>el numero de registros editados</returns>
        public int EditarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoBLL.EditarTipoDocumentoBienDenunciado(pTipoDocumentoBienDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los TipoDocumentoBienDenunciado por el id
        /// </summary>
        /// <param name="pIdTipoDocumentoBienDenunciado">id del TipoDocumentoBienDenunciado</param>
        /// <returns>Variable de tipo TipoDocumentoBienDenunciado con los datos consultados</returns>
        public TipoDocumentoBienDenunciado ConsultarTipoDocumentoBienDenunciadoPorId(int pIdTipoDocumentoBienDenunciado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciadoPorId(pIdTipoDocumentoBienDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los TipoDocumentoBienDenunciado que coincidan con los parámetros de entrada
        /// </summary>
        /// <param name="pNombre">Nombre del documento</param>
        /// <param name="pEstado">Estado del documento</param>
        /// <returns>Lista de resultados de la consulta</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoBienDenunciado(string pNombre, string pEstado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciado(pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los TipoDocumentoBienDenunciado que coincidan con los parámetros de entrada
        /// </summary>        
        /// <returns>Lista de resultados de la consulta</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoBienDenunciadoActivos()
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciado(string.Empty, "ACTIVO");
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Id del registro que se va a eliminar</param>
        /// <returns>1 si se ejecuta correctamente</returns>
        public int EliminarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoBLL.EliminarTipoDocumentoBienDenunciado(pTipoDocumentoBienDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Registrar la Información del Causante
        /// <summary>
        /// Inserta la información de un causante
        /// </summary>
        /// <param name="pCausante">Variable de tipo causante que contiene la información que se va a insertar</param>
        /// <returns>Id del causante insertado</returns>
        public int InsertarCausante(Causantes pCausante)
        {
            try
            {
                return this.vCausantesBLL.InsertarCausante(pCausante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de un causante por el id causante
        /// </summary>
        /// <param name="pIdCaudante">Id causante que se va a consultar</param>
        /// <returns>Variable de tipo causante que contiene los datos de la consulta</returns>
        public Causantes ConsultarCausantesPorIdCaudante(int pIdCaudante)
        {
            try
            {
                return this.vCausantesBLL.ConsultarCausantesPorIdCaudante(pIdCaudante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de un causante por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo Causantes con la información resultante de la consulta</returns>
        public List<Causantes> ConsultarCausantesPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vCausantesBLL.ConsultarCausantesPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un causante
        /// </summary>
        /// <param name="pCausante">Variable de tipo causante que contiene la información que se va a insertar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarCausante(Causantes pCausante)
        {
            try
            {
                return this.vCausantesBLL.EditarCausante(pCausante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la informacion del causante
        /// </summary>
        /// <param name="pCausante">Causante a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public Causantes ConsultarInfirmacionCausantes(Causantes pCausante)
        {
            try
            {
                pCausante.Tercero = this.vTerceroBLL.ConsultarTerceroXId(pCausante.IdTercero);
                pCausante.Tercero.TiposDocumentosGlobal = this.vTiposDocumentosGlobalBLL.ConsultarTiposIdentificacionGlobalPorId(pCausante.Tercero.IdTipoDocIdentifica);
                if (!string.IsNullOrEmpty(pCausante.Tercero.PrimerNombre))
                {
                    pCausante.NombreMostrar = pCausante.Tercero.PrimerNombre;
                }

                if (!string.IsNullOrEmpty(pCausante.Tercero.SegundoNombre))
                {
                    pCausante.NombreMostrar += " " + pCausante.Tercero.SegundoNombre;
                }

                if (!string.IsNullOrEmpty(pCausante.Tercero.PrimerApellido))
                {
                    pCausante.NombreMostrar += " " + pCausante.Tercero.PrimerApellido;
                }

                if (!string.IsNullOrEmpty(pCausante.Tercero.SegundoApellido))
                {
                    pCausante.NombreMostrar += " " + pCausante.Tercero.SegundoApellido;
                }
                return pCausante;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un registro en la tabla DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int InsertarDocumentoSolicitadoDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienBLL.InsertarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un registro de la tabla  DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EditarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienBLL.EditarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un registro de la tabla  DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EditarDocumentosSolicitadosYRecibidosDenunciaBien(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienBLL.EditarDocumentosSolicitadosYRecibidosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pIdDocumentosSolicitadosDenunciaBien">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public DocumentosSolicitadosDenunciaBien ConsultarDocumentosSolicitadosDenunciaBienPorId(int pIdDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienBLL.ConsultarDocumentosSolicitadosDenunciaBienPorId(pIdDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdCausante
        /// </summary>
        /// <param name="pIdCausante">id del registro a consulta</param>
        /// <returns>lista de tipo DocumentosSolicitadosDenunciaBien con la información consultada</returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdCausante(int pIdCausante)
        {
            try
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = this.vDocumentosSolicitadosDenunciaBienBLL.ConsultarDocumentosSolicitadosDenunciaBienPorIdCausante(pIdCausante);
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                {
                    item.TipoDocumentoBienDenunciado = this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciadoPorId(item.IdTipoDocumentoBienDenunciado);
                    item.EstadoDocumento = this.vEstadoDocumentoBLL.ConsultarEstadoDocumentoPorId(Convert.ToInt32(item.IdEstadoDocumento));
                    item.EsNuevo = false;
                }

                return vListaDocumentos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta los datos de la documentación recibida del causante
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int InsertarDocumentoRecibidoCausante(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienBLL.EditarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina la información de documentación recibida del causante
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EliminarDocumentoRecibidoCausante(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienBLL.EditarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un tercero por el tipo de identificación , el número de identificación y el nombre del tipo de persona
        /// </summary>
        /// <param name="pIdTipoIdentificacion"> id del tipo de identificación</param>
        /// <param name="pNumeroIdentificacion">Número de identificación</param>
        /// <param name="pNombreTipoPersona">Nombre del tipo de persona</param>
        /// <returns>Variable de tipo Tercero con la información resultante de la consulta</returns>
        public Tercero ConsultarTercero(int pIdTipoIdentificacion, string pNumeroIdentificacion, string pNombreTipoPersona)
        {
            try
            {
                return this.vTerceroBLL.ConsultarTercero(pIdTipoIdentificacion, pNumeroIdentificacion, pNombreTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un tercero por el id tercero
        /// </summary>
        /// <param name="pIdTercero">id del tercero que se va a consultar</param>
        /// <returns>Variable de tipo Tercero con la información consultada</returns>
        public Tercero ConsultarTerceroXId(int pIdTercero)
        {
            try
            {
                return this.vTerceroBLL.ConsultarTerceroXId(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los tercero
        /// </summary>
        /// <returns>Variable de tipo Tercero con la información consultada</returns>
        public List<Tercero> ConsultarTerceroTodos()
        {
            try
            {
                return this.vTerceroBLL.ConsultarTerceroTodos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Valida si ya existe un Causante con el mismo id tercero y id denuncia bien
        /// </summary>
        /// <param name="pIdTercero">Id del tercero</param>
        /// <param name="pIdDenunciaBien"> id de la denuncia bien</param>
        /// <returns>true si existe o false si no existe</returns>
        public bool ValidarDuplicidadCausante(int pIdTercero, int pIdDenunciaBien)
        {
            try
            {
                return this.vCausantesBLL.ValidarDuplicidad(pIdTercero, pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el estado documento por el nombre del estado
        /// </summary>
        /// <param name="pNombreEstadoDocumento">nombre a consultar</param>
        /// <returns>variable de tipo EstadoDocumento con la información resultante de la consulta</returns>
        public EstadoDocumento ConsultarEstadoDocumentoPorNombre(string pNombreEstadoDocumento)
        {
            try
            {
                return this.vEstadoDocumentoBLL.ConsultarEstadoDocumentoPorNombre(pNombreEstadoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el EstadoDocumento por el id del estado
        /// </summary>
        /// <param name="pIdEstadoDocumento">Id del estado que se va a consultar</param>
        /// <returns>variable de tipo EstadoDocumento con la información resultante de la consulta</returns>
        public EstadoDocumento ConsultarEstadoDocumentoPorId(int pIdEstadoDocumento)
        {
            try
            {
                return this.vEstadoDocumentoBLL.ConsultarEstadoDocumentoPorId(pIdEstadoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de los históricos de documentos solicitados filtrados por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>lista de tipo  HistóricoDocumentosSolicitadosDenunciaBien con la información resultante de la consulta</returns>
        public List<HistoricoDocumentosSolicitadosDenunciaBien> ConsultarHistoricoDocumentosDenunciaPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                List<HistoricoDocumentosSolicitadosDenunciaBien> vListaHistorico = this.vHistoricoDocumentosSolicitadosDenunciaBienBLL.ConsultarHistoricoDocumentosDenunciaPorIdDenunciaBien(pIdDenunciaBien);
                foreach (HistoricoDocumentosSolicitadosDenunciaBien item in vListaHistorico)
                {
                    item.TipoDocumentoBienDenunciado = this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciadoPorId(item.IdTipoDocumentoBienDenunciado);
                    item.EstadoDocumento = this.vEstadoDocumentoBLL.ConsultarEstadoDocumentoPorId(Convert.ToInt32(item.IdEstadoDocumento));
                }
                return vListaHistorico;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo registro en la tabla HistóricoDocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pHistorico"> variable de tipo HistóricoDocumentosSolicitadosDenunciaBien con la información a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int InsertarHistoricoDocumentosSolicitadosDenunciaBien(HistoricoDocumentosSolicitadosDenunciaBien pHistorico)
        {
            try
            {
                return this.vHistoricoDocumentosSolicitadosDenunciaBienBLL.InsertarHistoricoDocumentosSolicitadosDenunciaBien(pHistorico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los históricos de los estados de una denuncia por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de la denuncia consultada </returns>
        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienBLL.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los roles por el nombre de programa y el nombre de la función
        /// </summary>
        /// <param name="pNombrePrograma">Nombre del programa</param>
        /// <param name="pNombreFuncion">Nombre de la función</param>
        /// <returns>lista de roles</returns>
        public List<Rol> ConsultarRolesPorProgramaFuncion(string pNombrePrograma, string pNombreFuncion)
        {
            try
            {
                return vRolBLL.ConsultarRolesPorProgramaFuncion(pNombrePrograma, pNombreFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Registrar la Información del Apoderado
        /// <summary>
        /// Inserta la información de un Apoderado
        /// </summary>
        /// <param name="pApoderado">Variable de tipo Apoderado que contiene la información que se va a insertar</param>
        /// <returns>Id del Apoderado insertado</returns>
        public int InsertarApoderado(Apoderados pApoderado)
        {
            try
            {
                return this.vApoderadosBLL.InsertarApoderado(pApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de un Apoderado por el id Apoderado
        /// </summary>
        /// <param name="pIdApoderado">Id Apoderado que se va a consultar</param>
        /// <returns>Variable de tipo Apoderado que contiene los datos de la consulta</returns>
        public Apoderados ConsultarApoderadosPorIdApoderado(int pIdApoderado)
        {
            try
            {
                return this.vApoderadosBLL.ConsultarApoderadosPorIdApoderado(pIdApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de un Apoderado por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo Apoderados con la información resultante de la consulta</returns>
        public List<Apoderados> ConsultarApoderadosPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vApoderadosBLL.ConsultarApoderadosPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un Apoderado
        /// </summary>
        /// <param name="pApoderado">Variable de tipo Apoderado que contiene la información que se va a insertar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarApoderado(Apoderados pApoderado)
        {
            try
            {
                return this.vApoderadosBLL.EditarApoderado(pApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la informacion del Apoderado
        /// </summary>
        /// <param name="pApoderado">Apoderado a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public Apoderados ConsultarInfirmacionApoderados(Apoderados pApoderado)
        {
            try
            {
                if (pApoderado.IdTercero != null)
                {
                    pApoderado.Tercero = this.vTerceroBLL.ConsultarTerceroXId(Convert.ToInt32(pApoderado.IdTercero));
                    pApoderado.Tercero.TiposDocumentosGlobal = this.vTiposDocumentosGlobalBLL.ConsultarTiposIdentificacionGlobalPorId(pApoderado.Tercero.IdTipoDocIdentifica);
                    if (!string.IsNullOrEmpty(pApoderado.Tercero.PrimerNombre))
                    {
                        pApoderado.NombreMostrar = pApoderado.Tercero.PrimerNombre;
                    }

                    if (pApoderado.Tercero.NombreTipoPersona != "NATURAL")
                    {
                        pApoderado.NombreMostrar = pApoderado.Tercero.RazonSocial;
                    }

                    if (!string.IsNullOrEmpty(pApoderado.Tercero.SegundoNombre))
                    {
                        pApoderado.NombreMostrar += " " + pApoderado.Tercero.SegundoNombre;
                    }

                    if (!string.IsNullOrEmpty(pApoderado.Tercero.PrimerApellido))
                    {
                        pApoderado.NombreMostrar += " " + pApoderado.Tercero.PrimerApellido;
                    }

                    if (!string.IsNullOrEmpty(pApoderado.Tercero.SegundoApellido))
                    {
                        pApoderado.NombreMostrar += " " + pApoderado.Tercero.SegundoApellido;
                    }
                }
                return pApoderado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = this.vDocumentosSolicitadosDenunciaBienBLL.ConsultarDocumentosSolicitadosDenunciaBienPorIdDenunciaBien(pIdDenunciaBien);
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                {
                    item.TipoDocumentoBienDenunciado = this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciadoPorId(item.IdTipoDocumentoBienDenunciado);
                    item.EstadoDocumento = this.vEstadoDocumentoBLL.ConsultarEstadoDocumentoPorId(Convert.ToInt32(item.IdEstadoDocumento));
                    item.EsNuevo = false;
                }

                return vListaDocumentos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdCalidadDenunciante">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(int pIdCalidadDenunciante)
        {
            try
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = this.vDocumentosSolicitadosDenunciaBienBLL.ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(pIdCalidadDenunciante);

                vListaDocumentos.ForEach(item =>
                {
                    item.TipoDocumentoBienDenunciado = this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciadoPorId(item.IdTipoDocumentoBienDenunciado);
                    item.EstadoDocumento = this.vEstadoDocumentoBLL.ConsultarEstadoDocumentoPorId(Convert.ToInt32(item.IdEstadoDocumento));
                    item.EsNuevo = false;
                });

                return vListaDocumentos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdApoderado
        /// </summary>
        /// <param name="pIdApoderado">id del registro a consulta</param>
        /// <returns>lista de tipo DocumentosSolicitadosDenunciaBien con la información consultada</returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(int pIdApoderado)
        {
            try
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = this.vDocumentosSolicitadosDenunciaBienBLL.ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(pIdApoderado);
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                {
                    item.TipoDocumentoBienDenunciado = this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoBienDenunciadoPorId(item.IdTipoDocumentoBienDenunciado);
                    item.EstadoDocumento = this.vEstadoDocumentoBLL.ConsultarEstadoDocumentoPorId(Convert.ToInt32(item.IdEstadoDocumento));
                    item.EsNuevo = false;
                }

                return vListaDocumentos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta los datos de la documentación recibida del Apoderado
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int InsertarDocumentoRecibidoApoderado(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienBLL.EditarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina la información de documentación recibida del Apoderado
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EliminarDocumentoRecibidoApoderado(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienBLL.EditarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Valida si ya existe un Apoderado con el mismo id tercero y id denuncia bien
        /// </summary>
        /// <param name="pIdTercero">Id del tercero</param>
        /// <param name="pIdDenunciaBien"> id de la denuncia bien</param>
        /// <returns>true si existe o false si no existe</returns>
        public bool ValidarDuplicidadApoderado(int pIdTercero, int pIdDenunciaBien, int pIdApoderado)
        {
            try
            {
                return this.vApoderadosBLL.ValidarDuplicidad(pIdTercero, pIdDenunciaBien, pIdApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Registar Bienes Denunciados
        /// <summary>
        /// consulta los MuebleInmueble por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de MuebleInmueble</returns>
        public List<MuebleInmueble> ConsultarMuebleInmueblePorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                List<MuebleInmueble> vLista = this.vMuebleInmuebleBLL.ConsultarMuebleInmueblePorIdDenunciaBien(pIdDenunciaBien);
                foreach (MuebleInmueble item in vLista)
                {
                    item.NombreTipoBien = item.IdTipoBien.Equals("2") ? "MUEBLE" : "INMUEBLE";
                    item.NombreSubTipoBien = this.ConsultarSubTipoBien(item.IdTipoBien).Where(p => p.IdSubTipoBien == item.IdSubTipoBien).FirstOrDefault().NombreSubTipoBien;
                    item.NombreClaseBien = this.ConsultarClaseBien(item.IdSubTipoBien).Where(p => p.IdClaseBien == item.IdClaseBien).FirstOrDefault().NombreClaseBien;
                }
                return vLista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// consulta los MuebleInmueble por el Id
        /// </summary>
        /// <param name="pIdMuebleInmueble">id que se va a consultar</param>
        /// <returns>MuebleInmueble</returns>
        public MuebleInmueble ConsultarMuebleInmueblePorId(int pIdMuebleInmueble)
        {
            try
            {
                return this.vMuebleInmuebleBLL.ConsultarMuebleInmueblePorId(pIdMuebleInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public List<TituloValor> ConsultarTituloValorPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                List<TituloValor> vListTituloValor = this.vTituloValorBLL.ConsultarTituloValorPorIdDenunciaBien(pIdDenunciaBien);
                foreach (TituloValor item in vListTituloValor)
                {
                    item.TipoTitulo = this.ConsultarTipoTituloPorIdTipoTitulo(item.IdTipoTitulo);
                }

                return vListTituloValor;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public TituloValor ConsultarTituloValorPorIdTituloValor(int pIdTituloValor)
        {
            try
            {
                return this.vTituloValorBLL.ConsultarTituloValorPorIdTituloValor(pIdTituloValor);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// consulta los TipoTitulo por el IdTipoTitulo
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>TipoTitulo</returns>
        public TipoTitulo ConsultarTipoTituloPorIdTipoTitulo(int pIdTipoTitulo)
        {
            try
            {
                return this.vTipoTituloBLL.ConsultarTipoTituloPorIdTipoTitulo(pIdTipoTitulo);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// consulta los TipoTitulo 
        /// </summary>
        /// <returns>lISTA DE TipoTitulo</returns>
        public List<TipoTitulo> ConsultarTipoTitulo()
        {
            try
            {
                return this.vTipoTituloBLL.ConsultarTipoTitulo();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// consulta los ClaseEntrada
        /// </summary>
        /// <returns>LISTA DE TipoBien</returns>
        public List<ClaseEntrada> ConsultarClaseEntrada()
        {
            try
            {
                return this.vClaseEntradaBLL.ConsultarClaseEntrada();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// consulta los ClaseBien
        /// </summary>
        /// <returns>LISTA DE ClaseBien</returns>
        public List<ClaseBien> ConsultarClaseBien(string pIdSubTipoBien)
        {
            try
            {
                return this.vClaseBienBLL.ConsultarClaseBien(pIdSubTipoBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los EntidadBancaria
        /// </summary>
        /// <returns>LISTA DE EntidadBancaria</returns>
        public List<EntidadBancaria> ConsultarEntidadBancaria()
        {
            try
            {
                return this.vEntidadBancariaBLL.ConsultarEntidadBancaria();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los tipos de cuentas
        /// </summary>
        /// <returns>LISTA DE TipoCuenta</returns>
        public List<TipoCuenta> ConsultarTipoCuenta()
        {
            try
            {
                return this.vEntidadBancariaBLL.ConsultarTipoCuenta();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los EntidadEmisora
        /// </summary>
        /// <returns>LISTA DE EntidadEmisora</returns>
        public List<EntidadEmisora> ConsultarEntidadEmisora()
        {
            try
            {
                return this.vEntidadEmisoraBLL.ConsultarEntidadEmisora();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los EstadoFisicoBien
        /// </summary>
        /// <returns>LISTA DE EstadoFisicoBien</returns>
        public List<EstadoFisicoBien> ConsultarEstadoFisicoBien()
        {
            try
            {
                return this.vEstadoFisicoBienBLL.ConsultarEstadoFisicoBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los MarcaBien
        /// </summary>
        /// <returns>LISTA DE MarcaBien</returns>
        public List<MarcaBien> ConsultMarcaBien()
        {
            try
            {
                return this.vMarcaBienBLL.ConsultarMarcaBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los SubTipoBien
        /// </summary>
        /// <returns>LISTA DE SubTipoBien</returns>
        public List<SubTipoBien> ConsultarSubTipoBien(string pIdTipoBien)
        {
            try
            {
                return this.vSubTipoBienBLL.ConsultarSubTipoBien(pIdTipoBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los DestinacionEconomica
        /// </summary>
        /// <returns>LISTA DE DestinacionEconomica</returns>
        public List<DestinacionEconomica> ConsultarDestinacionEconomica()
        {
            try
            {
                return this.vDestinacionEconomicaBLL.ConsultarDestinacionEconomica();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la informacion que se va a cargar en los campos ocultos
        /// </summary>
        /// <param name="pIdSubTipoBien">Valos que se va a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public string ConsultarCamposOcultos(string pIdSubTipoBien)
        {
            try
            {
                return this.vMuebleInmuebleBLL.ConsultarCamposOcultos(pIdSubTipoBien);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// valida si el bien denunciado ya existe
        /// </summary>
        /// <param name="pIddenunciaBien">id de la denuncia</param>
        /// <param name="pBienDenunciado">tipo del bien denunciado</param>
        /// <param name="pDato">identificador del bien denunciado</param>
        /// <param name="pIdBienDenunciado">Id del bien denunciado</param>
        /// <param name="pIdBienDenunciado">id del bien denunciado</param>
        /// <returns>thue si existe o false en caso contrario</returns>
        public bool ValidarExistenciaBienDenunciado(int pIddenunciaBien, string pBienDenunciado, string pDato, int pIdBienDenunciado)
        {
            try
            {
                return this.vMuebleInmuebleBLL.ValidarExistenciaBienDenunciado(pIddenunciaBien, pBienDenunciado, pDato, pIdBienDenunciado);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Inserta la información de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Variable de tipo MuebleInmueble que contiene la información que se va a insertar</param>
        /// <returns>Id del MuebleInmueble insertado</returns>
        public int InsertarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                return this.vMuebleInmuebleBLL.InsertarMuebleInmueble(pMuebleInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Edita la información de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Variable de tipo MuebleInmueble que contiene la información que se va a insertar</param>
        /// <returns>numero de registros modificados</returns>
        public int EditarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                return this.vMuebleInmuebleBLL.EditarMuebleInmueble(pMuebleInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// valida si el bien denunciado ya existe en otras regionales
        /// </summary>
        /// <param name="pIddenunciaBien">id de la denuncia</param>
        /// <param name="pBienDenunciado">tipo del bien denunciado</param>
        /// <param name="pDato">identificador del bien denunciado</param>
        /// <returns>thue si existe o false en caso contrario</returns>
        public string ValidarExistenciaBienDenunciadoOtrasRegionales(int pIddenunciaBien, string pBienDenunciado, string pDato)
        {
            try
            {
                return this.vMuebleInmuebleBLL.ValidarExistenciaBienDenunciadoOtrasRegionales(pIddenunciaBien, pBienDenunciado, pDato);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Inserta la información de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Variable de tipo TituloValor que contiene la información que se va a insertar</param>
        /// <returns>Id del TituloValor insertado</returns>
        public int InsertarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                return this.vTituloValorBLL.InsertarTituloValor(pTituloValor);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Edita la información de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Variable de tipo TituloValor que contiene la información que se va a insertar</param>
        /// <returns>Numero de filas afectadas</returns>
        public int EditarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                return this.vTituloValorBLL.EditarTituloValor(pTituloValor);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Consulta los departamentos de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        public List<GN_VDIP> ConsultarDepartamentos()
        {
            try
            {
                return this.vGN_VDIPBLL.ConsultarDepartamentos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los municipios de un departamento de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        /// <param name="pCodMunicipio">id del departamento</param>
        public List<GN_VDIP> ConsultarMunicipios(string pCodDepto)
        {
            try
            {
                return this.vGN_VDIPBLL.ConsultarMunicipios(pCodDepto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los departamentos de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        public List<GN_VDIP> ConsultarDepartamentosSeven()
        {
            try
            {
                return this.vGN_VDIPBLL.ConsultarDepartamentosSeven();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los municipios de un departamento de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        /// <param name="pCodMunicipio">id del departamento</param>
        public List<GN_VDIP> ConsultarMunicipiosSeven(string pCodDepto)
        {
            try
            {
                return this.vGN_VDIPBLL.ConsultarMunicipiosSeven(pCodDepto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        //Consultar Contratos X Denuncia
        public List<ContratoParticipacionEconomica> ConsultarContratoXDenuncia(int IdDenuncia)
        {
            try
            {
                List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = vContratoDenunciaBienBLL.ConsultarContratoXDenuncia(IdDenuncia);
                return vListaContratoDenunciaBien;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Información de Contrato
        public List<ContratoParticipacionEconomica> ConsultarInformacionContrato(int IdDenuncia, int IdContrato)
        {
            try
            {
                List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = vContratoDenunciaBienBLL.ConsultarInformacionContrato(IdDenuncia, IdContrato);

                return vListaContratoDenunciaBien;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Denuncia Mueble-Titulo
        public List<DenunciaMuebleTitulo> ConsultarValorSumaDenuncia(int IdDenunciaBien)
        {
            try
            {
                List<DenunciaMuebleTitulo> vListaDenunciaMuebleTitulo = vDenunciaMuebleTituloBLL.ConsultarValorSumaDenuncia(IdDenunciaBien);

                return vListaDenunciaMuebleTitulo;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Denuncia Mueble-Titulo por IdDenuncia
        public DenunciaMuebleTitulo ConsultarValorSumaDenunciaPorIdDenuncia(int IdDenunciaBien)
        {
            try
            {
                DenunciaMuebleTitulo vListaDenunciaMuebleTitulo = vDenunciaMuebleTituloBLL.ConsultarValorSumaDenunciaPorIdDenuncia(IdDenunciaBien);

                return vListaDenunciaMuebleTitulo;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Tipo Bien

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<TipoBien> ConsultarTipoBienTodos()
        {
            try
            {
                return vTipoBienBLL.ConsultarTipoBienTodos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<string> ConsultarSubTipoBien(int tipoBien)
        {
            try
            {
                return vTipoBienBLL.ConsultarSubTipoBien(tipoBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<string> ConsultarMarcaBien()
        {
            try
            {
                return vTipoBienBLL.ConsultarMarcaBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<string> ConsultarClaseBien()
        {
            try
            {
                return vTipoBienBLL.ConsultarClaseBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Filtros

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<ResultFiltroMueble> filtroMueble(FiltroMueble vfiltroMueble)
        {
            try
            {
                return this.vfiltrosMostrencosBLL.filtroMueble(vfiltroMueble);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<ResultFiltroMueble> BusquedaMueble(string vInStringRadicado)
        {
            try
            {
                return this.vfiltrosMostrencosBLL.BusquedaMueble(vInStringRadicado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<ResultFiltroTitulo> BusquedaTitulo(string vInStringRadicado)
        {
            try
            {
                return this.vfiltrosMostrencosBLL.BusquedaTitulo(vInStringRadicado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<ResultFiltroTitulo> filtroTitulo(FiltroTitulo vfiltroTitulo)
        {
            try
            {
                return this.vfiltrosMostrencosBLL.filtroTitulo(vfiltroTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<ResultFiltroDenunciante> filtroDenunciante(FiltroDenunciante vfiltroDenunciante)
        {
            try
            {
                return vfiltrosMostrencosBLL.filtroDenunciante(vfiltroDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener una lista de los Causantes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<ResultFiltroDenunciante> filtroCausante(FiltroDenunciante vfiltroDenunciante)
        {
            try
            {
                return vfiltrosMostrencosBLL.filtroCausante(vfiltroDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Tipo Titulo

        public List<TipoTitulo> ConsultarTipoTituloTodos()
        {
            try
            {
                return vTipoTituloBLL.ConsultarTipoTituloTodos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoTitulo> ConsultarTipoTitulo(string pNombre, bool? pEstado)
        {
            try
            {
                return vTipoTituloBLL.ConsultarTipoTitulo(pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TipoTitulo ConsultarTipoTituloPorId(int pIdTipoTitulo)
        {
            try
            {
                return vTipoTituloBLL.ConsultarTipoTituloPorId(pIdTipoTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int IngresarTipoTitulo(TipoTitulo pTipoTitulo)
        {
            try
            {
                return vTipoTituloBLL.IngresarTipoTitulo(pTipoTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarTipoTitulo(TipoTitulo pTipoTitulo)
        {
            try
            {
                return vTipoTituloBLL.ActualizarTipoTitulo(pTipoTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Despacho Judicial


        public List<DespachoJudicial> ConsultarDespachoJudicial(string pNombre, int pDepartamento, int pMunicipio, bool? pEstado)
        {
            try
            {
                return vDespachoJudicialBLL.ConsultarDespachoJudicial(pNombre, pDepartamento, pMunicipio, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DespachoJudicial> ConsultarDespachoJudicialTodos()
        {
            try
            {
                return vDespachoJudicialBLL.ConsultarDespachoJudicialTodos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int IngresarDespachoJudicial(DespachoJudicial pDespachoJudicial)

        {
            try
            {
                return vDespachoJudicialBLL.IngresarDespachoJudicial(pDespachoJudicial);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DespachoJudicial ConsultarDespachoJudicialPorId(int pIdDespacho)
        {
            try
            {
                return vDespachoJudicialBLL.ConsultarDespachoJudicialPorId(pIdDespacho);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarDespachoJudicial(DespachoJudicial pDespachoJudicial)
        {
            try
            {
                return vDespachoJudicialBLL.ActualizarDespachoJudicial(pDespachoJudicial);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Medio Comunicación

        public List<MedioComunicacion> ConsultarMedioComunicacion(string pNombre, bool? pEstado)
        {
            try
            {
                return vMedioComunicacionBLL.ConsultarMedioComunicacion(pNombre, pEstado);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public MedioComunicacion ConsultarMedioComunicacionPorId(int pIdMedioComunicacion)
        {
            try
            {
                return vMedioComunicacionBLL.ConsultarMedioComunicacionPorId(pIdMedioComunicacion);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int IngresarMedioComunicacion(MedioComunicacion pMedioComunicacion)
        {
            try
            {
                return vMedioComunicacionBLL.IngresarMedioComunicacion(pMedioComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int ActualizarMedioComunicacion(MedioComunicacion pMedioComunicacion)
        {
            try
            {
                return vMedioComunicacionBLL.ActualizarMedioComunicacion(pMedioComunicacion);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Contrato Participación Económica
        //Consultar Existencia Contratos X Denuncia
        public List<ContratoParticipacionEconomica> ConsultarExistenciaContratoXDenuncia(int IdDenuncia, int IdContrato)
        {
            try
            {
                List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = vContratoDenunciaBienBLL.ConsultarExistenciaContratoXDenuncia(IdDenuncia, IdContrato);

                return vListaContratoDenunciaBien;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Contratos X Denuncia Multiples Parámetros
        public List<ContratoParticipacionEconomica> ConsultarContratoXDenunciaMultiplesParametros(int IdVigencia, int idRegional, string NumeroContrato)
        {
            try
            {
                List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = vContratoDenunciaBienBLL.ConsultarContratoXDenunciaMultiplesParametros(IdVigencia, idRegional, NumeroContrato);

                return vListaContratoDenunciaBien;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarContratoParticipacionEconomica(ContratoParticipacionEconomica pContratoParticipacionEconomica)
        {
            try
            {
                return this.vContratoDenunciaBienBLL.InsertarContratoParticipacionEconomica(pContratoParticipacionEconomica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar All Regionales
        public List<Regionales> ConsultarRegionalesAll()
        {
            try
            {
                List<Regionales> vListaContratoRegional = vRegionalesBLL.ConsultarRegionalAll();

                return vListaContratoRegional;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar All Vigencias
        public List<Vigencias> ConsultarVigenciasAll()
        {
            try
            {
                List<Vigencias> vListaContratoVigencias = vVigenciaBLL.ConsultarVigenciasAll();

                return vListaContratoVigencias;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Documentos Solicitados

        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosDenunciaBienPorId(int IdDenuncia)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosBLL.ConsultarDocumentosSolicitadosYRecibidosDenunciaBienPorId(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(int IdContratoParticipacionEconomica)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosBLL.ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(IdContratoParticipacionEconomica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Documentación Solicitada y Recibida por IdDenuncia y IdOrdenPago
        /// </summary>
        /// <param name="IdDenuncia">Parámetro IdDenuncia</param>
        /// /// <param name="IdOrdenPago">Parámetro IdOrdenPago</param>
        /// <returns>Retorna Lista de documentación </returns>
        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosPorIdDenuncia(int IdDenuncia)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosBLL.ConsultarDocumentosSolicitadosYRecibidosPorIdDenuncia(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Insertar Documentación
        public int InsertarDocumentosSolicitadosDenunciaBienContratos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                return vDocumentosSolicitadosYRecibidosBLL.InsertarDocumentosSolicitadosDenunciaBienContratos(pDocumentosSolicitadosYRecibidos);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Insertar Documentación por relación de pago
        public int InsertarDocumentosSolicitadosDenunciaBienRelacionDePagos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                return vDocumentosSolicitadosYRecibidosBLL.InsertarDocumentosSolicitadosDenunciaBienRelacionDePagos(pDocumentosSolicitadosYRecibidos);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Actualizar Documentación
        public int ActualizarDocumentosSolicitadosDenunciaBienContratos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                return vDocumentosSolicitadosYRecibidosBLL.ActualizarDocumentosSolicitadosDenunciaBienContratos(pDocumentosSolicitadosYRecibidos);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Eliminar Documentación
        public int EliminarDocumentosSolicitadosDenunciaBienContratos(int vIdDocumento)
        {
            try
            {
                return vDocumentosSolicitadosYRecibidosBLL.EliminarDocumentosSolicitadosDenunciaBienContratos(vIdDocumento);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Informe Denunciante
        //Consultar Datos Informe Denunciante
        public List<InformeDenunciante> ConsultarDatosInformeDenunciante(int IdDenuncia, string FechaInicio, string FechaFinal)
        {
            try
            {
                List<InformeDenunciante> vListaInformeDenunciante = vInformeDenuncianteBLL.ConsultarDatosInformeDenunciante(IdDenuncia, FechaInicio, FechaFinal);

                return vListaInformeDenunciante;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Datos Informe Denunciante Por ID
        public List<InformeDenunciante> ConsultarDatosInformeDenuncianteXID(int IdInformeDenunciante)
        {
            try
            {
                List<InformeDenunciante> vListaInformeDenunciante = vInformeDenuncianteBLL.ConsultarDatosInformeDenuncianteXID(IdInformeDenunciante);

                return vListaInformeDenunciante;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Datos Informe Denunciante Por ID
        public List<InformeDenunciante> ConsultarDatosInformeDenuncianteIdDenuncia(int IdDenuncia)
        {
            try
            {
                List<InformeDenunciante> vListaInformeDenunciante = vInformeDenuncianteBLL.ConsultarDatosInformeDenuncianteIdDenuncia(IdDenuncia);

                return vListaInformeDenunciante;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Validar Datos Informe Denunciante Por ID
        public List<InformeDenunciante> ValidarExistenciaDatosInformeDenunciante(int IdDenunciaBien, int IdDocumentoInforme, string FechaInforme)
        {
            try
            {
                List<InformeDenunciante> vListaInformeDenunciante = vInformeDenuncianteBLL.ValidarExistenciaDatosInformeDenunciante(IdDenunciaBien, IdDocumentoInforme, FechaInforme);

                return vListaInformeDenunciante;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Anular Denuncia

        /// <summary>
        /// Consultar el historico de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien"></param>
        /// <returns></returns>
        public List<AnulacionDenuncia> ConsultarHistoricoAnulacionPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vAnulacionDenunciaBLL.ConsultarHistoricoAnulacionDenunciaPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una solicitud de aprobacion de una denuncia
        /// </summary>
        /// <param name="pAnulacionDenuncia"></param>
        /// <returns></returns>
        public int InsertarSolicitudAnulacion(AnulacionDenuncia pAnulacionDenuncia)
        {
            try
            {
                return this.vAnulacionDenunciaBLL.InsertarSolicitudAnulacion(pAnulacionDenuncia);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Insetar aprobacion de la solicitudde una anulacion
        /// </summary>
        /// <param name="pAnulacionDenuncia"></param>
        /// <returns></returns>
        public int InsertarAprobacionAnulacion(AnulacionDenuncia pAnulacionDenuncia)
        {
            try
            {
                return this.vAnulacionDenunciaBLL.InsertarAprobacionAnulacion(pAnulacionDenuncia);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Consultar Nombre y correo del abogado solicitante
        /// </summary>
        /// <param name="vIdDenunciaBien"></param>
        /// <returns></returns>
        public AnulacionDenuncia ConsultarAbogadoSolicitudAnulacion(int vIdDenunciaBien)
        {
            try
            {
                return this.vAnulacionDenunciaBLL.ConsultarAbogadoSolicitudAnulacion(vIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        //Insertar Informe Denunciante
        public int InsertarInformeDenunciante(InformeDenunciante pInformeDenunciante)
        {
            try
            {
                return this.vInformeDenuncianteBLL.InsertarInformeDenunciante(pInformeDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Actualizar Informe Denunciante
        public int ActualizarInformeDenunciante(InformeDenunciante pInformeDenunciante)
        {
            try
            {
                return this.vInformeDenuncianteBLL.ActualizarInformeDenunciante(pInformeDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public bool validarTramiteNotarial(int IdDenuncia, string Notaria)
        {
            try
            {
                return vTramiteNotarialBLL.validarTramiteNotarial(IdDenuncia, Notaria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int InsertarTramiteNotarial(TramiteNotarial vTramiteNotarial)
        {
            try
            {
                return vTramiteNotarialBLL.InsertarTramiteNotarial(vTramiteNotarial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int InsertarTramiteNotarialAceptado(TramiteNotarialAceptado vTramiteNotarialAceptado)
        {
            try
            {
                return vTramiteNotarialBLL.InsertarTramiteNotarialAceptado(vTramiteNotarialAceptado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int InsertarTramiteJudicial(TramiteJudicial vTramiteJudicial)
        {
            try
            {
                return vTramiteJudicialBLL.InsertarTramiteJudicial(vTramiteJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int InsertarDesicionAuto(int IdDenunciaBien, int InDesicionAuto)
        {
            try
            {
                return vTramiteJudicialBLL.InsertarDesicionAuto(IdDenunciaBien, InDesicionAuto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int InsertarTipoTramite(int IdDenunciaBien, int TipoTramite)
        {
            try
            {
                return vTramiteJudicialBLL.InsertarTipoTramite(IdDenunciaBien, TipoTramite);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int InsertarDesicionAutoInadmitida(int IdDenunciaBien, int IdInadmitida)
        {
            try
            {
                return vTramiteJudicialBLL.InsertarDesicionAutoInadmitida(IdDenunciaBien, IdInadmitida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int InsertarDesicionRecurso(int IdDenunciaBien, int IdRecurso, bool presenta)
        {
            try
            {
                return vTramiteJudicialBLL.InsertarDesicionRecurso(IdDenunciaBien, IdRecurso, presenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int UpdateTramiteNotarial(TramiteNotarial vTramiteNotarial)
        {
            try
            {
                return vTramiteNotarialBLL.UpdateTramiteNotarial(vTramiteNotarial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public int UpdateTramiteJudicial(TramiteJudicial vTramiteJudicial)
        {
            try
            {
                return vTramiteJudicialBLL.UpdateTramiteJudicial(vTramiteJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Insertar Tramite Notarial
        /// </summary>
        /// <returns>Retorna bool</returns>
        public bool validarTramiteJudicial(int IdDenuncia)
        {
            try
            {
                return vTramiteJudicialBLL.validarTramiteJudicial(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Tramite Judicial por ID Denuncia
        /// </summary>
        /// <returns>Retorna bool</returns>
        public TramiteJudicial ConsultaTramiteJudicial(int IdDenuncia)
        {
            try
            {
                return vTramiteJudicialBLL.ConsultarTramiteJudicial(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta para Tramite Notarial por ID Denuncia
        /// </summary>
        /// <returns>Retorna bool</returns>
        public TramiteNotarial ConsultarTramiteNotarial(int IdDenuncia)
        {
            try
            {
                return vTramiteNotarialBLL.ConsultarTramiteNotarial(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para Tramite Notarial por ID Denuncia
        /// </summary>
        /// <returns>Retorna bool</returns>
        public TramiteNotarialAceptado ConsultarTramiteNotarialAceptado(int IdDenuncia)
        {
            try
            {
                return vTramiteNotarialBLL.ConsultarTramiteNotarialAceptado(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta para obtener Todas las Notarias
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public List<TramiteNotarial> ObtenerNotarias()
        {
            try
            {
                return vTramiteNotarialBLL.ObtenerNotarias();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Elimianr Informe Denunciante
        public int EliminarInformeDenunciante(int IdInformeDenunciante)
        {
            try
            {
                return this.vInformeDenuncianteBLL.EliminarInformeDenunciante(IdInformeDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consulta del tercero por Tipo Juridico y Razon Social
        public List<Tercero> ConsultarJuridicoTercerosPorNombre(string nombre)
        {
            try
            {
                List<Tercero> vListaJuridicoTerceros = terceroBLL.ConsultarJuridicoTercerosPorNombre(nombre);
                return vListaJuridicoTerceros;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consulta el tipo de Entidad por IdTercero
        public Tercero ConsultarTipoEntidadPorIdTercero(int IdTercero)
        {
            try
            {
                Tercero vTipoEntidadTerceros = terceroBLL.ConsultarTipoEntidadPorIdTercero(IdTercero);
                return vTipoEntidadTerceros;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Tipo Documento Soporte Denuncia
        //Consultar Datos Tipo Documento Soporte
        public List<TipoDocumentoSoporteDenuncia> ListarTipoDocumentoSporteDenuncia()
        {
            try
            {
                List<TipoDocumentoSoporteDenuncia> vListaTipoDocumentoSoporteDenuncia = vTipoDocumentoSoporteDenunciaBLL.ListarTipoDocumentoSporteDenuncia();
                return vListaTipoDocumentoSoporteDenuncia;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TipoDocumentoSoporteDenuncia ConsultarTipoDocumentoSoporteDenunciadoPorId(int pIdTipoDocumentoSoporteDenunciado)
        {
            try
            {
                return this.vTipoDocumentoSoporteDenunciaBLL.ConsultarTipoDocumentoSoporteDenunciadoPorId(pIdTipoDocumentoSoporteDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public AnulacionDenuncia ConsultarCoordinadorJuridicoSolicitudAnulacion(int vIdDenunciaBien, string vkeys)
        {
            try
            {
                return this.vAnulacionDenunciaBLL.ConsultarCoordinadorJuridicoSolicitudAnulacion(vIdDenunciaBien, vkeys);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        #endregion

        #region Notarias

        /// <summary>
        /// Consulta Todas las Notarias
        /// </summary>
        /// <returns> List<Notarias> con la informacion solicitada</returns>
        public List<Notarias> ConsultarNotarias()
        {
            try
            {
                return vNotariasBLL.ConsultarNotarias();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion


        #region Continuar de Oficio

        public List<ContinuarOficio> ConsultarContinuarOficios(int? pIdDenunciaBien, int? pIdContinuarOficio)
        {
            try
            {
                return vContinuarOficioBLL.ConsultarContinuarOficios(pIdDenunciaBien, pIdContinuarOficio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarContinuarOficio(ContinuarOficio pContinuarOficio)
        {
            try
            {
                return vContinuarOficioBLL.InsertarContinuarOficio(pContinuarOficio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarContinuarOficio(ContinuarOficio pContinuarOficio)
        {
            try
            {
                return vContinuarOficioBLL.ModificarContinuarOficio(pContinuarOficio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region SiNo

        /// <summary>
        /// Método de consulta para obtener una lista de los Tipos de Bienes
        /// </summary>
        /// <returns>Retorna List de TipoBien</returns>
        public void Sino(bool ValorSiNo, int vIdDenunciaBien)
        {
            try
            {
                this.vfiltrosMostrencosBLL.Sino(ValorSiNo, vIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region OrdenesPago

        /// <summary>
        /// consulta los ClaseBien
        /// </summary>
        /// <returns>LISTA DE OrdenesPago</returns>
        public List<OrdenesPago> ConsultarOrdenesPagoDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vPagosBLL.ConsultarOrdenesPagoDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de pagos por IdDenuncia y IdOrdenPago 
        /// </summary>
        /// <param name="IdOrdenPago">id de la Orden de pago</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarOrdenesPagoParticipacionPorIdOrdenPago(int IdOrdenPago)
        {
            try
            {
                return this.vPagosBLL.ConsultarOrdenesPagoParticipacionPorIdOrdenPago(IdOrdenPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los registros de la tabla Ordenes Pago por IdDenuncia
        /// </summary>
        /// <param name="IdDenuncia">id de la Orden de la denuncia</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarOrdenesPagoParticipacionPorIdDenuncia(int IdDenuncia)
        {
            try
            {
                return this.vPagosBLL.ConsultarOrdenesPagoParticipacionPorIdDenuncia(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Existencia de Orden de pago por número de comprobante
        /// </summary>
        /// <param name="IdDenunciaBien">Parámetro IdDenuncia</param>
        /// <param name="NoComprobante">Parámetro número de comprobante</param>
        /// <returns>Lista de Ordenes de pago</returns>
        public List<OrdenesPago> ConsultarExistenciaOrdenDePagoXComprobantePago(int IdDenunciaBien, decimal NoComprobantePago)
        {
            try
            {
                return this.vPagosBLL.ConsultarExistenciaOrdenDePagoXComprobantePago(IdDenunciaBien, NoComprobantePago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de pagos por IdDenuncia y número de resolución
        /// </summary>
        /// <param name="IdDenunciaBien">id de la denuncia</param>
        /// /// <param name="NumeroDeResolucion">Número de resolución de pago</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarExistenciaOrdenDePagoXNumeroDeResolucion(int IdDenunciaBien, decimal NumeroDeResolucion)
        {
            try
            {
                return this.vPagosBLL.ConsultarExistenciaOrdenDePagoXNumeroDeResolucion(IdDenunciaBien, NumeroDeResolucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo registro en la tabla OrdenesPago
        /// </summary>
        /// <param name="pOrdenDePago"> variable de tipo OrdenesPago con la información a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int InsertarOrdenesDePago(OrdenesPago pOrdenDePago)
        {
            try
            {
                return this.vPagosBLL.InsertarOrdenesDePago(pOrdenDePago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza un nuevo registro en la tabla OrdenesPago
        /// </summary>
        /// <param name="pOrdenDePago"> variable de tipo OrdenesPago con la información a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int ActualizarOrdenesDePago(OrdenesPago pOrdenDePago)
        {
            try
            {
                return this.vPagosBLL.ActualizarOrdenesDePago(pOrdenDePago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Feriados
        public List<Feriados> ObtenerFeriados()
        {
            try
            {
                return vFeriadosBLL.ObtenerFeriados();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #endregion

        #region Registrar Bienes Denunciados

        /// <summary>
        /// consulta los MuebleInmueble por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de MuebleInmueble</returns>
        public List<MuebleInmueble> ConsultarMuebleInmueble(int pIdDenunciaBien)
        {
            try
            {
                List<MuebleInmueble> vLista = this.vMuebleInmuebleBLL.ConsultarMuebleInmueble(pIdDenunciaBien);
                foreach (MuebleInmueble item in vLista)
                {
                    item.NombreTipoBien = item.IdTipoBien.Equals("2") ? "MUEBLE" : "INMUEBLE";
                    item.NombreSubTipoBien = this.ConsultarSubTipoBien(item.IdTipoBien).Where(p => p.IdSubTipoBien == item.IdSubTipoBien).FirstOrDefault().NombreSubTipoBien;
                    item.NombreClaseBien = this.ConsultarClaseBien(item.IdSubTipoBien).Where(p => p.IdClaseBien == item.IdClaseBien).FirstOrDefault().NombreClaseBien;
                }
                return vLista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public List<TituloValor> ConsultarTituloValor(int pIdDenunciaBien)
        {
            try
            {
                List<TituloValor> vListTituloValor = this.vTituloValorBLL.ConsultarTituloValor(pIdDenunciaBien);
                foreach (TituloValor item in vListTituloValor)
                {
                    item.TipoTitulo = this.ConsultarTipoTituloPorIdTipoTitulo(item.IdTipoTitulo);
                }

                return vListTituloValor;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region RegistrarDestinacionBienes

        /// <summary>
        /// Método para consultar la Información de Venta por Id
        /// </summary>
        /// <param name="pIdInformacionVenta">Id a consultar</param>
        /// <returns>Resultado de la consulta</returns>
        public InformacionVenta ConsultarInformacionVentaId(int pIdInformacionVenta)
        {
            try
            {
                return vInformacionVentaBLL.ConsultarInformacionVentaId(pIdInformacionVenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Insertar la información de la venta
        /// </summary>
        /// <param name="pInformacionVenta">Entidad a Insertar</param>
        /// <returns>Resultado de la operación</returns>
        public int InsertarInformacionVenta(InformacionVenta pInformacionVenta)
        {
            try
            {
                return vInformacionVentaBLL.InsertarInformacionVenta(pInformacionVenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar la información de la venta
        /// </summary>
        /// <param name="pInformacionVenta">Entidad a Modificar</param>
        /// <returns>Resultado de la operación</returns>
        public int ModificarInformacionVenta(InformacionVenta pInformacionVenta)
        {
            try
            {
                return vInformacionVentaBLL.ModificarInformacionVenta(pInformacionVenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita los campos UsoBien y IdInformacionVenta de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Entidad a modificar</param>
        /// <returns>Resultado de la Operación</returns>
        public int ModificarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                return this.vMuebleInmuebleBLL.ModificarMuebleInmueble(pMuebleInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Método que inserta el Historico
        /// </summary>
        /// <param name="pHistoricoEstadosDenunciaBien">Entidad pHistoricoEstadosDenunciaBien</param>
        /// <returns>Resultado de la Operación</returns>
        public int InsertarHistoricoEstadosDenuncia(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return vHistoricoEstadosDenunciaBienBLL.InsertarHistoricoEstadosDenuncia(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita los campos UsoBien y IdInformacionVenta de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Entidad a modificar</param>
        /// <returns>Resultado de la Operación</returns>
        public int ModificarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                return this.vTituloValorBLL.ModificarTituloValor(pTituloValor);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Método que valida si la denuncia ya ejecuto la Fse de "Protocolización" y la acción Comunicación Administrativa y Financiera
        /// </summary>
        /// <param name="pIdDenunciaBien">Id de la Denuncia</param>
        /// <returns>Resultadod e la operación (Bool)</returns>
        public List<HistoricoEstadosDenunciaBien> ValidaFaseAccion(int pIdDenunciaBien, int pFase, int? pAccion)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienBLL.ValidaFaseAccion(pIdDenunciaBien, pFase, pAccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta los tipos de documento Activos
        /// </summary>
        /// <returns>Lista de Tipos de Documento</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoSoporte()
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoBLL.ConsultarTipoDocumentoSoporte();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ConfiguracionTiemposEjecucionProceso

        /// <summary>
        /// Método que inserta ConfiguracionTiemposEjecucionProceso
        /// </summary>
        /// <param name="pConfiguracionTiemposEjecucionProceso">Entidad ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Resultado de la operación</returns>
        public int InsertarConfiguracionTiemposEjecucionProceso(ConfiguracionTiemposEjecucionProceso pConfiguracionTiemposEjecucionProceso)
        {
            try
            {
                return vConfiguracionTiemposEjecucionProcesoBLL.InsertarConfiguracionTiemposEjecucionProceso(pConfiguracionTiemposEjecucionProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que actualiza ConfiguracionTiemposEjecucionProceso
        /// </summary>
        /// <param name="pConfiguracionTiemposEjecucionProceso">Entidad ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Resultado de la operación</returns>
        public int ModificarConfiguracionTiemposEjecucionProceso(ConfiguracionTiemposEjecucionProceso pConfiguracionTiemposEjecucionProceso)
        {
            try
            {
                return vConfiguracionTiemposEjecucionProcesoBLL.ModificarConfiguracionTiemposEjecucionProceso(pConfiguracionTiemposEjecucionProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta la ConfiguracionTiemposEjecucionProceso x Id
        /// </summary>
        /// <param name="pIdConfiguracionTiemposEjecucionProceso">Id de ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Entidad ConfiguracionTiemposEjecucionProceso</returns>
        public ConfiguracionTiemposEjecucionProceso ConsultarConfiguracionTiemposEjecucionProceso(int pIdConfiguracionTiemposEjecucionProceso)
        {
            try
            {
                return vConfiguracionTiemposEjecucionProcesoBLL.ConsultarConfiguracionTiemposEjecucionProceso(pIdConfiguracionTiemposEjecucionProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta  los registros de ConfiguracionTiemposEjecucionProceso x Filtros
        /// </summary>
        /// <param name="pIdFase">Id de la Fase</param>
        /// <param name="pIdActuacion">Id de la actuación</param>
        /// <param name="pIdAccion">Id de la Acción</param>
        /// <param name="pEstado">Id del estado</param>
        /// <returns>Lista de Resultados</returns>
        public List<ConfiguracionTiemposEjecucionProceso> ConsultarListaConfiguracionTiemposEjecucionProceso(string pFase, string pActuacion, string pAccion, byte? pEstado)
        {
            try
            {
                return vConfiguracionTiemposEjecucionProcesoBLL.ConsultarListaConfiguracionTiemposEjecucionProceso(pFase, pActuacion, pAccion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Accion

        /// <summary>
        /// Evento para Cargar la Lista de opciones de Acciones x IdActuacion
        /// </summary>
        /// <param name="sender">The DropDownList</param>
        /// <param name="e">The SelectedIndexChanged</param>
        public List<Accion> ConsultarAccionesActivasPorIdFase(int pIdActuacion)
        {
            try
            {
                return vAccionBLL.ConsultarAccionesActivasPorIdFase(pIdActuacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Actuación

        /// <summary>
        /// Evento para Cargar la Lista de opciones de Actuaciones x IdFase
        /// </summary>
        /// <param name="sender">The DropDownList</param>
        /// <param name="e">The SelectedIndexChanged</param>
        public List<Actuacion> ConsultarActuacionesActivasPorIdFase(int pIdFase)
        {
            try
            {
                return vActuacionBLL.ConsultarActuacionesActivasPorIdFase(pIdFase);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Fase

        /// <summary>
        /// Método que consulta las fases en estado Activo
        /// </summary>
        /// <returns>Lista de Fases</returns>
        public List<FasesDenuncia> ConsultarFasesActivas()
        {
            try
            {
                return new FaseDenunciaBLL().ConsultarFasesActivas();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #endregion

        #region ReportesEstadisticos

        /// <summary>
        /// Método que consulta los Estados de la Denuncia para Repoorte
        /// </summary>
        /// <returns>Lista Filtrada de Estados</returns>
        public List<EstadoDenuncia> ConsultarEstadosDenunciasReportes()
        {
            try
            {
                return vEstadoDenunciaBLL.ConsultarEstadosDenunciasReportes();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}