﻿using System;
using Icbf.Utilities.Exceptions;
using System.Collections.Generic;
using Icbf.SIA.Business.Concursales;
using Icbf.SIA.Entity.Concursales;

namespace Icbf.SIA.Service
{
    public class ConcursalesService
    {
        #region Parametricas
         
        public List<Clasificador> ConsultarParameticasConcursales(Boolean? vEstado, string vNombre)
        {
            try
            {
                return new  ParametricasBLL().ConsultarParameticasConcursales(vEstado, vNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParameticasConcursalesDetalle(Boolean? vEstado, string vNombre, int vIdClasificador)
        {
            try
            {
                return new ParametricasBLL().ConsultarParameticasConcursalesDetalle(vEstado, vNombre, vIdClasificador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParameticasNaturalezaTipoProceso(Boolean? vEstado, string vNombre)
        {
            try
            {
                return new ParametricasBLL().ConsultarParameticasNaturalezaTipoProceso(vEstado, vNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        public List<Valor> ConsultarParametricaPorPadre(int vIdPadre)
        {
            try
            {
                return new ParametricasBLL().ConsultarParametricaPorPadre(vIdPadre);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParametricaPorCodigo(string vCodigo)
        {
            try
            {
                return new ParametricasBLL().ConsultarParametricaPorCodigo(vCodigo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Valor ConsultarParametricaPorCodigo(string vCodigo, string vNombre)
        {
            try
            {
                return new ParametricasBLL().ConsultarParametricaPorCodigo(vCodigo, vNombre);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Clasificador ConsultarClasificador(int vIdClasificador)
        {
            try
            {
                return new ParametricasBLL().ConsultarClasificador(vIdClasificador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Valor ConsultarValorParametrica(int vIdValor)
        {
            try
            {
                return new ParametricasBLL().ConsultarValorParametrica(vIdValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarValorParametrica(Valor vValor)
        {
            try
            {
                return new ParametricasBLL().ModificarValorParametrica(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarValorParametrica(Valor vValor)
        {
            try
            {
                return new ParametricasBLL().InsertarValorParametrica(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ElimianrValorParametrica(Valor vValor)
        {
            try
            {
                return new ParametricasBLL().EliminarValorParametrica(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        #endregion

        #region Solicitudes Registro

        public List<CarteraActiva> ConsultarCarteraActiva(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            try
            {
                return new SolicitudesBLL().ConsultarCarteraActiva(vIdentificacion, vIdTipoDocumento, vIDRegional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CarteraAgrupada> ConsultarCarteraActivaAgrupada(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            try
            {
                return new SolicitudesBLL().ConsultarCarteraActivaAgrupada(vIdentificacion, vIdTipoDocumento, vIDRegional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CarteraActiva ConsultarCarteraActivaPorId(int vId)
        {
            try
            {
                return new SolicitudesBLL().ConsultarCarteraActivaPorId(vId);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ExisteCarteraActivaAsociada(int vIdCarteraActiva, string vResolucion)
        {
            try
            {
                return new SolicitudesBLL().ExisteCarteraActivaAsociada(vIdCarteraActiva, vResolucion);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string IniciarProcesoConcursal(CarteraActiva item)
        {
            try
            {
                return new SolicitudesBLL().IniciarProcesoConcursal(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public KeyValuePair<int, bool> ValidarCarteraTercero(string identificacion)
        {
            try
            {
                return new SolicitudesBLL().ValidarCarteraTercero(identificacion);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Tipos de Proceso por Naturaleza

        public List<TipoProcesoNaturaleza> ConsultarTiposdeProcesosporNaturaleza(string vTipoProceso, string vNaturaleza, Boolean? vEstado)
        {
            try
            {
                return new TipoProcesoNaturalezaBLL().ConsultarTiposdeProcesosporNaturaleza(vTipoProceso, vNaturaleza, vEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public TipoProcesoNaturaleza ConsultarTipoProcesoNaturaleza(int vIdTipoProcesoNaturaleza)
        {
            try
            {
                return new TipoProcesoNaturalezaBLL().ConsultarTipoProcesoNaturaleza(vIdTipoProcesoNaturaleza);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }                
        public int ModificarTipoProcesoNaturaleza(TipoProcesoNaturaleza vIdTipoProcesoNaturaleza)
        {
            try
            {
                return new TipoProcesoNaturalezaBLL().ModificarTipoProcesoNaturaleza(vIdTipoProcesoNaturaleza);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarTipoProcesoNaturaleza(TipoProcesoNaturaleza vIdTipoProcesoNaturaleza)
        {
            try
            {
                return new TipoProcesoNaturalezaBLL().InsertarTipoProcesoNaturaleza(vIdTipoProcesoNaturaleza);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTipoProcesoNaturaleza(TipoProcesoNaturaleza vIdTipoProcesoNaturaleza)
        {
            try
            {
                return new TipoProcesoNaturalezaBLL().EliminarTipoProcesoNaturaleza(vIdTipoProcesoNaturaleza);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Documentos Proceso Concursal

        public int InsertarDocumentoProcesoConcursal(ProcesoConcursalDocumento item)
        {
            try
            {
                return new SolicitudesBLL().InsertarDocumentosProcesoConcursal(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarDocumentoProcesoConcursal(int idDocumento)
        {
            try
            {
                return new SolicitudesBLL().EliminarDocumentoProcesoConcursal(idDocumento);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursalDocumento> ObtenerDocumentosPorIdProceso(int idProceso)
        {
            try
            {
                return new ProcesoConcursalBLL().ObtenerDocumentosPorIdProceso(idProceso);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Proceso Concursal

        public List<ProcesoConcursal> ObtenerProcesosConcursales(int idRegional, string numeroIdentificacion, int?idEstado, string usuarioAsignado , string consecutivo)
        {
            try
            {
                return new ProcesoConcursalBLL().ObtenerProcesosConcursales(idRegional, numeroIdentificacion, idEstado, usuarioAsignado, consecutivo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesMultipleEstado(int idRegional, string numeroIdentificacion, string usuarioAsignado, string[] estados, string consecutivo)
        {
            try
            {
                return new ProcesoConcursalBLL().ObtenerProcesosConcursalesMultipleEstado(idRegional, numeroIdentificacion, usuarioAsignado, estados, consecutivo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesOmitirEstados(int idRegional, string numeroIdentificacion, string usuarioAsignado, string[] estadosOmitir, string consecutivo, int? vIdEstado = null)
        {
            try
            {
                return new ProcesoConcursalBLL().ObtenerProcesosConcursalesOmitirEstados(idRegional, numeroIdentificacion, usuarioAsignado,estadosOmitir, consecutivo, vIdEstado);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesGestionNovedades(int idRegional, string numeroIdentificacion, int? idEstado, string usuarioAsignado, string consecutivo)
        {
            try
            {
                return new ProcesoConcursalBLL().ObtenerProcesosConcursalesGestionNovedades(idRegional, numeroIdentificacion, idEstado, usuarioAsignado, consecutivo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ProcesoConcursal ObtenerProcesosConcursalPorId(int idProceso)
        {
            try
            {
                return new ProcesoConcursalBLL().ObtenerProcesosConcursalPorId(idProceso);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AsignarUsuarioProceso(int id, string usuario, string usuarioModifica)
        {
            try
            {
                return new ProcesoConcursalBLL().AsignarUsuarioProceso(id,usuario,usuarioModifica);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Proceso Concursal Estado

        public ProcesoConcursalEstado ConsultarEstadoPorCodigo(string codigo)
        {
            try
            {
                return new ProcesoConcursalEstadoBLL().ConsultarEstadoPorCodigo(codigo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursalEstado> ConsultarEstadosOmitir(string [] codigosEstadosOmitidos)
        {
            try
            {
                return new ProcesoConcursalEstadoBLL().ConsultarEstadosOmitir(codigosEstadosOmitidos);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Novedades Proceso Concursal

        public int IngresarNovedad(ProcesoConcursalNovedad item)
        {
            try
            {
               return new ProcesoConcursalBLL().IngresarNovedad(item);
            }
            catch(Exception ex)
            {

                throw;
            }
        }

        public int IngresarGestionNovedad(ProcesoConcursalGestionNovedad item)
        {
            try
            {
                return new ProcesoConcursalBLL().IngresarGestionNovedad(item);
            }
            catch(Exception ex)
            {

                throw;
            }
        }

        #endregion

        #region Trazabilidad Proceso Concursal

        public List<ProcesoConcursalGestion> ObtenerGestionProcesoPorIdProceso(int idProceso)
        {
            try
            {
                return new ProcesoConcursalBLL().ObtenerGestionProcesoPorIdProceso(idProceso);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}
