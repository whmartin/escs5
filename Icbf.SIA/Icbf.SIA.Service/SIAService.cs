﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Business;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.Business.Concursales;
using System.IO;


namespace Icbf.SIA.Service
{
    public class SIAService
    {
        private UsuarioBLL vUsuarioBLL;
        private UtilidadTextoBLL ManejoTexto;
        private UtilidadesArchivoBLL FtpArchivo;
        private DepartamentoBLL vDepartamento;
        private MunicipioBLL vMunicipio;
        private RepresentanteLegalBLL vRepresentanteLegalBLL;
        private VigenciaBLL vVigenciaBLL;
        private TipoUsuarioBLL vTipoUsuarioBLL;
        private RegionalBLL vRegionalTipoContrato;
        private RegionalBLL vRegionalBLL;
        private ReporteBLL vReporteBLL;
        private CategoriaEmpleadoBLL vCategoriaEmpleadoBLL;
        private CategoriaValoresBLL vCategoriaValoresBLL;
        private ProyeccionPresupuestosBLL vProyeccionPresupuestosBLL;
        private ComplementosNMFBll vComplementosNMFBll;
        private CupoAreasBLL vCupoAreasBLL;
        private RubrosCuposBLL vRubrosCuposBLL;
        private ObjetoBLL vObjetoBLL;
        private ProyeccionCategoriaBLL vProyeccionCategoriaBLL;
        private GlobalAreasBLL vGlobalAreasBLL;
        private CargaArchivoBLL vCargaArchivoBLL;
        private ParametricasBLL vConcursalesBLL;
        private AdicionProyeccionBLL vAdicionProyeccionBLL;
        private HistoricoAdicionProyeccionBLL vHistoricoAdicionProyeccionBLL;
        private HistoricoAdicionCandidatoBLL vHistoricoAdicionCandidatoBLL;
        private HistoricoProrrogaCandidatoBLL vHistoricoProrrogaCandidatoBLL;
        private AdicionProrrogaCandidatoBLL vAdicionProrrogaCandidatoBLL;


        public SIAService()
        {
            vUsuarioBLL = new UsuarioBLL();
            ManejoTexto = new UtilidadTextoBLL();
            FtpArchivo = new UtilidadesArchivoBLL();
            vDepartamento = new DepartamentoBLL();
            vMunicipio = new MunicipioBLL();
            vRepresentanteLegalBLL = new RepresentanteLegalBLL();
            vVigenciaBLL = new VigenciaBLL();
            vRegionalBLL = new RegionalBLL();
            vTipoUsuarioBLL = new TipoUsuarioBLL();
            vRegionalTipoContrato = new RegionalBLL();
            vReporteBLL = new ReporteBLL();
            vCategoriaEmpleadoBLL = new CategoriaEmpleadoBLL();
            vCategoriaValoresBLL = new CategoriaValoresBLL();
            vProyeccionPresupuestosBLL = new ProyeccionPresupuestosBLL();
            vComplementosNMFBll = new ComplementosNMFBll();
            vCupoAreasBLL = new CupoAreasBLL();
            vRubrosCuposBLL = new RubrosCuposBLL();
            vObjetoBLL = new ObjetoBLL();
            vProyeccionCategoriaBLL = new ProyeccionCategoriaBLL();
            vGlobalAreasBLL = new GlobalAreasBLL();
            vCargaArchivoBLL = new CargaArchivoBLL();
            vAdicionProyeccionBLL = new AdicionProyeccionBLL();
            vHistoricoAdicionProyeccionBLL =new HistoricoAdicionProyeccionBLL();
            vHistoricoAdicionCandidatoBLL= new HistoricoAdicionCandidatoBLL();
            vHistoricoProrrogaCandidatoBLL = new HistoricoProrrogaCandidatoBLL();
            vAdicionProrrogaCandidatoBLL = new AdicionProrrogaCandidatoBLL();


        }
    #region Usuario
    /// <summary>
    /// Método de inserción para la entidad Usuario
    /// </summary>
    /// <param name="pUsuario"></param>
    /// <returns></returns>
    public int InsertarUsuario(Usuario pUsuario)
        {
            try
            {
                return vUsuarioBLL.InsertarUsuario(pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad Usuario interno
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int InsertarUsuarioInterno(Usuario pUsuario)
        {
            try
            {
                return vUsuarioBLL.InsertarUsuarioInterno(pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Usuario
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int ModificarUsuario(Usuario pUsuario)
        {
            try
            {
                return vUsuarioBLL.ModificarUsuario(pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarDatosUsuario(int pIdUsuario)
        {
            try
            {
                return vUsuarioBLL.ConsultarDatosUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de modificación del nombreUsuario que es el correo electronico de acceso
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <param name="pcorreoErrado"></param>
        /// <param name="pcorreoNuevo"></param>
        /// <returns></returns>
        public int modificarCorreoUsuario(Usuario pIdUsuario)
        {
            try
            {
                return vUsuarioBLL.modificarCorreoUsuario(pIdUsuario);
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la Razon Social de un Usuario
        /// </summary>
        /// <param name="pRazonSocial"></param>
        /// <param name="pproviderKey"></param>
        /// <returns></returns>
        public bool ModificarRazonSocialDeUsuario(string pRazonSocial, string pproviderKey)
        {
            try
            {
                return vUsuarioBLL.ModificarRazonSocialDeUsuario(pRazonSocial, pproviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id y po Numero de Documento para la entidad Usuario
        /// </summary>
        /// <param name="pTipoDocumento"></param>
        /// <param name="pNumeroDocumento"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(int pTipoDocumento, string pNumeroDocumento)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuario(pTipoDocumento, pNumeroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por Nombre de Usuario para la entidad Usuario
        /// </summary>
        /// <param name="pNombreUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(string pNombreUsuario)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuario(pNombreUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(int pIdUsuario)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuarioPorproviderKey(string pProviderKey)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuarioPorproviderKey(pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para la entidad Usuario
        /// </summary>
        /// <returns></returns>
        public List<Usuario> ConsultarTodosUsuarios()
        {
            try
            {
                return vUsuarioBLL.ConsultarTodosUsuarios();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Usuario
        /// </summary>
        /// <param name="pNumeroDocumento"></param>
        /// <param name="pPrimerNombre"></param>
        /// <param name="pPrimerApellido"></param>
        /// <param name="pPerfil"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<Usuario> ConsultarUsuarios(String pNumeroDocumento, String pPrimerNombre, String pPrimerApellido, int? pPerfil, Boolean? pEstado)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuarios(pNumeroDocumento, pPrimerNombre, pPrimerApellido, pPerfil, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por NumeroIdentificacion para la entidad Usuario
        /// </summary>
        /// <param name="pNumeroDocumento"></param>
        /// /// <param name="pNombre"></param>
        public List<Usuario> ConsultarUsuarioss(string pNumeroDocumento, string pNombre)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuarioss(pNumeroDocumento,pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        public Usuario ConsultarUsuarioPorId(int pIdUsuario)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuarioPorId(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region TipoDocumento
        /// <summary>
        /// Método de consulta por id para la entidad TipoDocumento
        /// </summary>
        /// <param name="pIdTipoDocumento"></param>
        /// <returns></returns>
        public TipoDocumento ConsultarTipoDocumento(int pIdTipoDocumento)
        {
            try
            {
                TipoDocumentoBLL vTipoDocumentoBLL = new TipoDocumentoBLL();
                return vTipoDocumentoBLL.ConsultarTipoDocumento(pIdTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por codigo y nombre para la entidad TipoDocumento
        /// </summary>
        /// <returns></returns>
        public List<TipoDocumento> ConsultarTiposDocumento(String pCodigoDocumento, String pNombreDocumento)
        {
            try
            {
                TipoDocumentoBLL vTipoDocumentoBLL = new TipoDocumentoBLL();
                return vTipoDocumentoBLL.ConsultarTiposDocumento(pCodigoDocumento, pNombreDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TipoDocumento> ConsultarTiposDocumentoNMF(String pCodigoDocumento, String pNombreDocumento)
        {
            try
            {
                TipoDocumentoBLL vTipoDocumentoBLL = new TipoDocumentoBLL();
                return vTipoDocumentoBLL.ConsultarTiposDocumento(pCodigoDocumento, pNombreDocumento);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoDocumento
        /// </summary>
        /// <returns></returns>
        public List<TipoDocumento> ConsultarTodosTipoDocumento()
        {
            try
            {
                TipoDocumentoBLL vTipoDocumentoBLL = new TipoDocumentoBLL();
                return vTipoDocumentoBLL.ConsultarTodosTipoDocumento();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region FormatoArchivo
        /// <summary>
        /// Método de consulta por filtros para la entidad FormatoArchivo
        /// </summary>
        /// <param name="pTablaTemporal"></param>
        /// <param name="pExt"></param>
        /// <returns></returns>
        public List<FormatoArchivo> ConsultarFormatoArchivoTemporarExt(string pTablaTemporal, string pExt)
        {
            try
            {
                FormatoArchivoBLL vFArchivoBLL = new FormatoArchivoBLL();
                return vFArchivoBLL.ConsultarFormatoAchivoByTemporarExt(pTablaTemporal, pExt);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Departamento
        /// <summary>
        /// Método de consulta por id para la entidad Departamento
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public Departamento ConsultarDepartamento(int pIdDepartamento)
        {
            try
            {
                return vDepartamento.ConsultarDepartamento(pIdDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Departamento
        /// </summary>
        /// <param name="pIdPais"></param>
        /// <param name="pCodigoDepartamento"></param>
        /// <param name="pNombreDepartamento"></param>
        /// <returns></returns>
        public List<Departamento> ConsultarDepartamentos(int? pIdPais, String pCodigoDepartamento, String pNombreDepartamento)
        {
            try
            {
                return vDepartamento.ConsultarDepartamentos(pIdPais, pCodigoDepartamento, pNombreDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Municipio
        /// <summary>
        /// Método de consulta por id para la entidad Municipio
        /// </summary>
        /// <param name="pIdMunicipio"></param>
        /// <returns></returns>
        public Municipio ConsultarMunicipio(int pIdMunicipio)
        {
            try
            {
                return vMunicipio.ConsultarMunicipio(pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Municipio
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pCodigoMunicipio"></param>
        /// <param name="pNombreMunicipio"></param>
        /// <returns></returns>
        public List<Municipio> ConsultarMunicipios(int? pIdDepartamento, String pCodigoMunicipio, String pNombreMunicipio)
        {
            try
            {
                return vMunicipio.ConsultarMunicipios(pIdDepartamento, pCodigoMunicipio, pNombreMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Municipio
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pCodigoMunicipio"></param>
        /// <param name="pNombreMunicipio"></param>
        /// <returns></returns>
        public List<Municipio> ConsultarMunicipiosPorIdsDep(string pIdsDepartamento)
        {
            try
            {
                return vMunicipio.ConsultarMunicipiosPorIdsDep(pIdsDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Utilidades de texto        
        public string QuitarEspacios(string Texto)
        {
            return ManejoTexto.QuitarEspacios(Texto);
        }
        #endregion

        #region Utilidad de FTP
        /// <summary>
        /// Método que descarga un Archivo del FTP
        /// </summary>
        /// <param name="NombreFTP"></param>
        /// <param name="NombreArchivo"></param>
        /// <returns></returns>
        public MemoryStream DescargarArchivoFtp(string NombreFTP, string NombreArchivo)
        {
            return FtpArchivo.DescargarArchivoFtp(NombreFTP, NombreArchivo);
        }

        /// <summary>
        /// Método que Sube un archivo al FTP
        /// </summary>
        /// <param name="pFileStream"></param>
        /// <param name="NombreFTP"></param>
        /// <param name="Nombre"></param>
        /// <returns></returns>
        public Boolean SubirArchivoFtp(Stream pFileStream, String NombreFTP, String Nombre)
        {
            return FtpArchivo.SubirArchivoFtp(pFileStream, NombreFTP, Nombre);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public bool CrearDirectorioFTP(string directorio)
        {
            return FtpArchivo.CrearDirectorioFTP(directorio);
        }

        #endregion

        #region RepresentanteLegal
        /// <summary>
        /// Método de inserción para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int InsertarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                return vRepresentanteLegalBLL.InsertarRepresentanteLegal(pRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int ModificarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                return vRepresentanteLegalBLL.ModificarRepresentanteLegal(pRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int EliminarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                return vRepresentanteLegalBLL.EliminarRepresentanteLegal(pRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pIdRepresentanteLegal"></param>
        /// <returns></returns>
        public RepresentanteLegal ConsultarRepresentanteLegal(int pIdRepresentanteLegal)
        {
            try
            {
                return vRepresentanteLegalBLL.ConsultarRepresentanteLegal(pIdRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pIdTipoDocumento"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdMunicipio"></param>
        /// <param name="pIdentificacion"></param>
        /// <param name="pPrimerNombre"></param>
        /// <param name="pSegundoNombre"></param>
        /// <param name="pPrimerApellido"></param>
        /// <param name="pSegundoApellido"></param>
        /// <returns></returns>
        public List<RepresentanteLegal> ConsultarRepresentanteLegals(int? pIdTipoDocumento, int? pIdDepartamento, int? pIdMunicipio, String pIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                return vRepresentanteLegalBLL.ConsultarRepresentanteLegals(pIdTipoDocumento, pIdDepartamento, pIdMunicipio, pIdentificacion, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Vigencia
        /// <summary>
        /// Método de consulta por id para la entidad Vigencia
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <returns></returns>
        public Vigencia ConsultarVigencia(int pIdVigencia)
        {
            try
            {
                return vVigenciaBLL.ConsultarVigencia(pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Vigencia
        /// </summary>
        /// <param name="pActivo"></param>
        /// <returns></returns>
        public List<Vigencia> ConsultarVigenciasSinAnnoActual(String pActivo)
        {
            try
            {
                return vVigenciaBLL.ConsultarVigenciasSinAnnoActual(pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Vigencia
        /// </summary>
        /// <param name="pActivo">Valor booleano que representa el estado del registro activo o inactivo</param>
        /// <returns>Lista de la instancia Vigencia</returns>
        public List<Vigencia> ConsultarVigencias(bool pActivo)
        {
            try
            {
                return vVigenciaBLL.ConsultarVigencias(pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public bool ConsultarVigenciasExistenteAnio(int pIdContrato, int pAnio)
        {
            try
            {
                return vVigenciaBLL.ConsultarVigenciasExistenteAnio(pIdContrato, pAnio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Vigencia> ConsultarVigenciasConFutura(bool pActivo)
        {
            try
            {
                var itemsActivos = vVigenciaBLL.ConsultarVigencias(pActivo);
                int mayor = itemsActivos.Max(e => e.AcnoVigencia);
                itemsActivos.Add(new Vigencia() { AcnoVigencia = mayor + 1, IdVigencia = int.MaxValue });
                itemsActivos = itemsActivos.OrderByDescending(e => e.AcnoVigencia).ToList();
                return itemsActivos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Regional

        public List<Regional> ConsultarRegionalPCIs(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                return vRegionalBLL.ConsultarRegionals(pCodigoRegional, pNombreRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Regional> ConsultarRegionalPCIsNMF(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                return vRegionalBLL.ConsultarRegionalsNMF(pCodigoRegional, pNombreRegional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Emilio Calapiña
        /// Obtiene la información de la regional cuyo identificador único de tabla coincida
        /// con la entrada dada
        /// </summary>
        /// <param name="pIdRegional">Entero con el identificador de la regional</param>
        /// <returns>Entidad con la información de la regional</returns>
        public Regional ConsultarRegional(int? pIdRegional)
        {
            try
            {
                return vRegionalBLL.ConsultarRegional(pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Regional> ConsultarRegionalsUsuario(int pIdUsuario)
        {
            try
            {
                return vRegionalBLL.ConsultarRegionalsUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region AreasInternas


        #endregion
        #region TipoUsuario
        /*Tipo de Usuario*/
        /// <summary>
        /// Gonet 
        /// Permite insertar un tipo de usuario
        ///     -Genera auditoria
        /// 20-Feb-2014
        /// </summary>
        /// <param name="pTipoUsuario">Instancia con la información del tipo a insertar</param>
        /// <returns>Identificador de base de datos de la información insertada</returns>
        public int InsertarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                return vTipoUsuarioBLL.InsertarTipoUsuario(pTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Permite modificar un tipo de usuario
        ///     -Genera auditoria
        /// 20-Feb-2014
        /// </summary>
        /// <param name="pTipoUsuario">Instancia con la información a modificar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int ModificarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                return vTipoUsuarioBLL.ModificarTipoUsuario(pTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Permite eliminar un tipo de usuario
        ///     -Genera auditoria
        /// 20-Feb-2014
        /// </summary>
        /// <param name="pIdTipoUsuario">Instancia con la información a eliminar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int EliminarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                return vTipoUsuarioBLL.EliminarTipoUsuario(pIdTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Consulta y recupera la información de tipo de usuario a partir del identificador de base de datos
        /// 20-Feb-2014
        /// </summary>
        /// <param name="pIdTipoUsuario">Entero con el identificador del tipo</param>
        /// <returns>Instancia que contiene la información recuperada de la base de datos</returns>
        public TipoUsuario ConsultarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                return vTipoUsuarioBLL.ConsultarTipoUsuario(pIdTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consulta y recupera la información de los tipos de usuario a partir de los filtros dados
        /// 20-Feb-2014
        /// </summary>
        /// <param name="pCodigoTipoUsuario">Cadena de texto con el codigo de tipo de usuario</param>
        /// <param name="pNombreTipoUsuario">Cadena de texto con el nombre de tipo de usuario</param>
        /// <param name="pEstado">Cadena de texto con el estado</param>
        /// <returns>Lista con las instancias que contiene la información, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<TipoUsuario> ConsultarTipoUsuarios(String pCodigoTipoUsuario, String pNombreTipoUsuario, String pEstado)
        {
            try
            {
                return vTipoUsuarioBLL.ConsultarTipoUsuarios(pCodigoTipoUsuario, pNombreTipoUsuario, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Consultar Regional
        public List<Regional> ConsultarRegionals(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                return vRegionalTipoContrato.ConsultarRegionals(pCodigoRegional, pNombreRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        //Implementacion reportes dinamicos
        #region Consultar reportes
        //Tabla de reportes para transversal de reportes
        public int InsertarReporte(Reporte pReporte)
        {
            try
            {

                return vReporteBLL.InsertarReporte(pReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarReporte(Reporte pReporte)
        {
            try
            {

                return vReporteBLL.ModificarReporte(pReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarReporte(Reporte pReporte)
        {
            try
            {

                return vReporteBLL.EliminarReporte(pReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Reporte ConsultarReporte(int pIdReporte)
        {
            try
            {

                return vReporteBLL.ConsultarReporte(pIdReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Reporte> ConsultarReportes(String pNombreReporte, String pServidor)
        {
            try
            {

                return vReporteBLL.ConsultarReportes(pNombreReporte, pServidor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        //Implementacion reportes dinamicos
        #region Categoria Empleados
        /// <summary>
        /// Método de inserción para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int InsertarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                return vCategoriaEmpleadoBLL.InsertarCategoriaEmpleado(pCategoriaEmpleado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int ModificarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                return vCategoriaEmpleadoBLL.ModificarCategoriaEmpleado(pCategoriaEmpleado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int EliminarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                return vCategoriaEmpleadoBLL.EliminarCategoriaEmpleado(pCategoriaEmpleado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pIdCategoria"></param>
        public CategoriaEmpleado ConsultarCategoriaEmpleado(int pIdCategoria)
        {
            try
            {
                return vCategoriaEmpleadoBLL.ConsultarCategoriaEmpleado(pIdCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pActivo"></param>
        public List<CategoriaEmpleado> ConsultarCategoriaEmpleados(String pCodigo, Boolean? pActivo, String pDescripcion, String pCodigoProducto)
        {
            try
            {
                return vCategoriaEmpleadoBLL.ConsultarCategoriaEmpleados(pCodigo, pActivo, pDescripcion, pCodigoProducto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<CategoriaEmpleado> ConsultarCategoriaEmpleadoAsignada(int? pIdProyeccionPresupuestos, String pCodEstadoProceso)
        {
            try
            {
                return vCategoriaEmpleadoBLL.ConsultarCategoriaEmpleadoAsignada(pIdProyeccionPresupuestos, pCodEstadoProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region CategoriaValores
        public int InsertarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                return vCategoriaValoresBLL.InsertarCategoriaValores(pCategoriaValores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CategoriaValores
        /// </summary>
        /// <param name="pCategoriaValores"></param>
        public int ModificarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                return vCategoriaValoresBLL.ModificarCategoriaValores(pCategoriaValores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CategoriaValores
        /// </summary>
        /// <param name="pCategoriaValores"></param>
        public int EliminarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                return vCategoriaValoresBLL.EliminarCategoriaValores(pCategoriaValores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CategoriaValores
        /// </summary>
        /// <param name="pIdCategoriaValor"></param>
        public CategoriaValores ConsultarCategoriaValores(int pIdCategoriaValor)
        {
            try
            {
                return vCategoriaValoresBLL.ConsultarCategoriaValores(pIdCategoriaValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CategoriaValores
        /// </summary>
        /// <param name="pNivel"></param>
        /// <param name="pIdVigencia"></param>
        /// <param name="pActivo"></param>
        public List<CategoriaValores> ConsultarCategoriaValoress(int? pNivel, int? pIdVigencia, Boolean? pActivo, int? IdCategoriaEmpleados)
        {
            try
            {
                return vCategoriaValoresBLL.ConsultarCategoriaValoress(pNivel, pIdVigencia, pActivo, IdCategoriaEmpleados);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CategoriaValores> ConsultarCategoriaValoresAsignada(int? pIdProyeccionPresupuestos, int? pIdCategoriaEmpleado)
        {
            try
            {
                return vCategoriaValoresBLL.ConsultarCategoriaValoresAsignada(pIdProyeccionPresupuestos, pIdCategoriaEmpleado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region Proyeccion Presupuestos
        public int InsertarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosBLL.InsertarProyeccionPresupuestos(pProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int AprobarProyeccionCategorias(List<ProyeccionCategoria> pProyeccionCategoria, string pUsuarioModifica)
        {
            try
            {
                return vProyeccionCategoriaBLL.AprobarProyeccionCategorias(pProyeccionCategoria, pUsuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                return vProyeccionCategoriaBLL.AprobarProyeccionCategoria(pProyeccionCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosBLL.AprobarProyeccionPresupuestos(pProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionPresupuestos(List<ProyeccionPresupuestos> pProyeccionPresupuestos, string pUsuarioModifica)
        {
            try
            {
                return vProyeccionPresupuestosBLL.AprobarProyeccionPresupuestos(pProyeccionPresupuestos, pUsuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pProyeccionPresupuestos"></param>
        public int ModificarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosBLL.ModificarProyeccionPresupuestos(pProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pProyeccionPresupuestos"></param>
        public int EliminarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosBLL.EliminarProyeccionPresupuestos(pProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdProyeccionPresupuestos"></param>
        public ProyeccionPresupuestos ConsultarProyeccionPresupuestos(int pIdProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosBLL.ConsultarProyeccionPresupuestos(pIdProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <param name="pIdArea"></param>
        /// <param name="pValorCupo"></param>
        /// <param name="pTotalCupos"></param>
        /// <param name="pActivo"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestoss(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vProyeccionPresupuestosBLL.ConsultarProyeccionPresupuestoss(pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <param name="pIdArea"></param>
        /// <param name="pValorCupo"></param>
        /// <param name="pTotalCupos"></param>
        /// <param name="pActivo"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestoCuposAre(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vProyeccionPresupuestosBLL.ConsultarProyeccionPresupuestoCuposAre(pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestosAdiciones(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
    , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vProyeccionPresupuestosBLL.ConsultarProyeccionPresupuestosAdiciones(pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoAproPresupuestos> ConsultarHistoricoProyeccionPresupuestos(int pIdProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosBLL.ConsultarHistoricoProyeccionPresupuestos(pIdProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoAproPresupuestos> ConsultarHistoricoProyeccionRechazados(int pIdProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosBLL.ConsultarHistoricoProyeccionRechazados(pIdProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        #endregion

        #region ComplementosNMF
        public List<TipoRecursoFinPptal> ConsultarTipoRecursoFinPptal()
        {
            try
            {
                return vComplementosNMFBll.ConsultarTipoRecursoFinPptal();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TipoRecursoFinPptal ConsultarRecurso(int? pIdRecurso)
        {
            try
            {
                return vComplementosNMFBll.ConsultarRecurso(pIdRecurso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CatGralRubrosPptalGasto> ConsultarCatGralRubrosPptalGasto(int? pIdAreasInt)
        {
            try
            {
                return vComplementosNMFBll.ConsultarCatGralRubrosPptalGasto(pIdAreasInt);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<AreasInternasNMF> ConsultarAreas(int? pIdAreasInt)
        {
            try
            {
                return vComplementosNMFBll.ConsultarAreas(pIdAreasInt);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region Cupo Areas
        public int InsertarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasBLL.InsertarCupoAreas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoCupoAreas(List<CupoAreas> pCupoAreasEstado, string pUsuarioModifica, int pIdProyeccionPresupuestos)
        {
            try
            {
                return vCupoAreasBLL.CambiarEstadoCupoAreas(pCupoAreasEstado, pUsuarioModifica, pIdProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int ModificarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasBLL.ModificarCupoAreas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int EliminarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasBLL.EliminarCupoAreas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CupoAreas
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        public CupoAreas ConsultarCupoAreas(int pIdCupoArea)
        {
            try
            {
                return vCupoAreasBLL.ConsultarCupoAreas(pIdCupoArea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CupoAreas
        /// </summary>
        /// <param name="pConsecutivoInterno"></param>
        /// <param name="pIdProveedor"></param>
        /// <param name="pFechaIngresoICBF"></param>
        /// <param name="pIdCategoriaValores"></param>
        /// <param name="pHonorarioBase"></param>
        /// <param name="pIvaHonorario"></param>
        /// <param name="pPorcentajeIVA"></param>
        /// <param name="pTiempoProyectadoMeses"></param>
        /// <param name="pTiempoProyectadoDias"></param>
        /// <param name="pAprobado"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        public List<CupoAreas> ConsultarCupoAreass(String pConsecutivoInterno, int? pIdProveedor, DateTime? pFechaIngresoICBF
            , int? pIdCategoriaValores, int? pIdProyeccionPresupuestos, String pIdEstadoProceso, string pNombreProveedor = null)
        {
            try
            {
                return vCupoAreasBLL.ConsultarCupoAreass(pConsecutivoInterno, pIdProveedor, pFechaIngresoICBF
            , pIdCategoriaValores, pIdProyeccionPresupuestos, pIdEstadoProceso, pNombreProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CupoAreas ConsultarCupoAreasCategoria(int pIdProyeccionPresupuestos, int pIdCategoriaValores)
        {
            try
            {
                return vCupoAreasBLL.ConsultarCupoAreasCategoria(pIdProyeccionPresupuestos, pIdCategoriaValores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region RubrosCupo
        public int InsertarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                return vRubrosCuposBLL.InsertarRubrosCupos(pRubrosCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RubrosCupos
        /// </summary>
        /// <param name="pRubrosCupos"></param>
        public int ModificarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                return vRubrosCuposBLL.ModificarRubrosCupos(pRubrosCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RubrosCupos
        /// </summary>
        /// <param name="pRubrosCupos"></param>
        public int EliminarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                return vRubrosCuposBLL.EliminarRubrosCupos(pRubrosCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad RubrosCupos
        /// </summary>
        /// <param name="pIdRubroCupos"></param>
        public RubrosCupos ConsultarRubrosCupos(int pIdRubroCupos)
        {
            try
            {
                return vRubrosCuposBLL.ConsultarRubrosCupos(pIdRubroCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RubrosCupos
        /// </summary>
        /// <param name="pCodigoRubro"></param>
        /// <param name="pDescripcionRubro"></param>
        /// <param name="pEstado"></param>
        public List<RubrosCupos> ConsultarRubrosCuposs(String pCodigoRubro, String pDescripcionRubro, Boolean? pEstado)
        {
            try
            {
                return vRubrosCuposBLL.ConsultarRubrosCuposs(pCodigoRubro, pDescripcionRubro, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region Objeto
        /// <summary>
        /// Método de inserción para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int InsertarObjeto(Objeto pObjeto)
        {
            try
            {
                return vObjetoBLL.InsertarObjeto(pObjeto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int ModificarObjeto(Objeto pObjeto)
        {
            try
            {
                return vObjetoBLL.ModificarObjeto(pObjeto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int EliminarObjeto(Objeto pObjeto)
        {
            try
            {
                return vObjetoBLL.EliminarObjeto(pObjeto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Objeto
        /// </summary>
        /// <param name="pIdObjetoCupos"></param>
        public Objeto ConsultarObjeto(int pIdObjetoCupos)
        {
            try
            {
                return vObjetoBLL.ConsultarObjeto(pIdObjetoCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Objeto
        /// </summary>
        /// <param name="pTitulo"></param>
        /// <param name="pDescripcionObjeto"></param>
        /// <param name="pEstado"></param>
        public List<Objeto> ConsultarObjetos(String pTitulo, String pDescripcionObjeto, Boolean? pEstado)
        {
            try
            {
                return vObjetoBLL.ConsultarObjetos(pTitulo, pDescripcionObjeto, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Proyeccion Categoria
        public int InsertarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                return vProyeccionCategoriaBLL.InsertarProyeccionCategoria(pProyeccionCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pProyeccionCategoria"></param>
        public int ModificarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                return vProyeccionCategoriaBLL.ModificarProyeccionCategoria(pProyeccionCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pProyeccionCategoria"></param>
        public int EliminarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                return vProyeccionCategoriaBLL.EliminarProyeccionCategoria(pProyeccionCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pIDProyeccionCategoria"></param>
        public ProyeccionCategoria ConsultarProyeccionCategoria(int? pIDProyeccionCategoria, int? pIdProyeccion)
        {
            try
            {
                return vProyeccionCategoriaBLL.ConsultarProyeccionCategoria(pIDProyeccionCategoria, pIdProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pCantidad"></param>
        public List<ProyeccionCategoria> ConsultarProyeccionCategorias(int? pIdProyeccionNecesidad, String pCodEstadoProceso)
        {
            try
            {
                return vProyeccionCategoriaBLL.ConsultarProyeccionCategorias(pIdProyeccionNecesidad, pCodEstadoProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region GlobalAreas
        public List<GlobalAreas> ConsultarGlobalArea(int? pIdAreasInt, int? pIdRegionalInt)
        {
            try
            {
                return vGlobalAreasBLL.ConsultarGlobalArea(pIdAreasInt, pIdRegionalInt);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<GlobalAreas> ConsultarAreasRegionales(int? pIdAreasInt, String pIdRegionalesStr)
        {
            try
            {
                return vGlobalAreasBLL.ConsultarAreasRegionales(pIdAreasInt, pIdRegionalesStr);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        #endregion

        public List<DescripcionErrorCarga> CargarDatosArchivo(String xml)
        {
            try
            {
                return vCargaArchivoBLL.CargarDatosArchivo(xml);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        #region AdicionProyeccion

        public int InsertarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                return vAdicionProyeccionBLL.InsertarAdicionProyeccion(pAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pAdicionProyeccion"></param>
        public int ModificarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                return vAdicionProyeccionBLL.ModificarAdicionProyeccion(pAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pAdicionProyeccion"></param>
        public int EliminarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                return vAdicionProyeccionBLL.EliminarAdicionProyeccion(pAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pIdAdicionProyeccion"></param>
        public AdicionProyeccion ConsultarAdicionProyeccion(int pIdAdicionProyeccion)
        {
            try
            {
                return vAdicionProyeccionBLL.ConsultarAdicionProyeccion(pIdAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pIdProyeccionPresupuestos"></param>
        /// <param name="pValorAdicionado"></param>
        /// <param name="pAprobado"></param>
        public AdicionProyeccion ConsultarAdicionProyeccions(int? pIdProyeccionPresupuestos, Decimal? pValorAdicionado, Boolean? pAprobado)
        {
            try
            {
                return vAdicionProyeccionBLL.ConsultarAdicionProyeccions(pIdProyeccionPresupuestos, pValorAdicionado, pAprobado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<ProyeccionAdiciones> ConsultarProyeccionAdiciones(int? pIdProyeccionPresupuestos, int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vAdicionProyeccionBLL.ConsultarProyeccionAdiciones(pIdProyeccionPresupuestos, pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProyeccionAdiciones> ConsultarProyeccionAdicionesIndividual(int? pIdProyeccionPresupuestos, int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
         , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vAdicionProyeccionBLL.ConsultarProyeccionAdicionesIndividual(pIdProyeccionPresupuestos, pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Histrico Adición Proyeccion

         public int InsertarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                return vHistoricoAdicionProyeccionBLL.InsertarHistoricoAdicionProyeccion(pHistoricoAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pHistoricoAdicionProyeccion"></param>
        public int ModificarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                return vHistoricoAdicionProyeccionBLL.ModificarHistoricoAdicionProyeccion(pHistoricoAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pHistoricoAdicionProyeccion"></param>
        public int EliminarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                return vHistoricoAdicionProyeccionBLL.EliminarHistoricoAdicionProyeccion(pHistoricoAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pIdHistoricoAdicionProyeccion"></param>
        public HistoricoAdicionProyeccion ConsultarHistoricoAdicionProyeccion(int pIdHistoricoAdicionProyeccion)
        {
            try
            {
                return vHistoricoAdicionProyeccionBLL.ConsultarHistoricoAdicionProyeccion(pIdHistoricoAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pIdAdicionProyeccion"></param>
        /// <param name="pRazonRechazo"></param>
        /// <param name="pRechazado"></param>
        public List<HistoricoAdicionProyeccion> ConsultarHistoricoAdicionProyeccions(int? pIdAdicionProyeccion, String pRazonRechazo, Boolean? pRechazado)
        {
            try
            {
                return vHistoricoAdicionProyeccionBLL.ConsultarHistoricoAdicionProyeccions(pIdAdicionProyeccion, pRazonRechazo, pRechazado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Historico Adicion Candidato
        public int InsertarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                return vHistoricoAdicionCandidatoBLL.InsertarHistoricoAdicionCandidato(pHistoricoAdicionCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pHistoricoAdicionCandidato"></param>
        public int ModificarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                return vHistoricoAdicionCandidatoBLL.ModificarHistoricoAdicionCandidato(pHistoricoAdicionCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pHistoricoAdicionCandidato"></param>
        public int EliminarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                return vHistoricoAdicionCandidatoBLL.EliminarHistoricoAdicionCandidato(pHistoricoAdicionCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pIdHistoricoAdicionCandidato"></param>
        public HistoricoAdicionCandidato ConsultarHistoricoAdicionCandidato(int pIdHistoricoAdicionCandidato)
        {
            try
            {
                return vHistoricoAdicionCandidatoBLL.ConsultarHistoricoAdicionCandidato(pIdHistoricoAdicionCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pValorAdicion"></param>
        /// <param name="pValorAdicionAnterior"></param>
        public List<HistoricoAdicionCandidato> ConsultarHistoricoAdicionCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, Decimal? pValorAdicion, Decimal? pValorAdicionAnterior)
        {
            try
            {
                return vHistoricoAdicionCandidatoBLL.ConsultarHistoricoAdicionCandidatos(pIdCupoArea, pIdProyeccionPresuspuesto, pValorAdicion, pValorAdicionAnterior);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region HistoricoProrrogasCandidato

        public int InsertarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                return vHistoricoProrrogaCandidatoBLL.InsertarHistoricoProrrogaCandidato(pHistoricoProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pHistoricoProrrogaCandidato"></param>
        public int ModificarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                return vHistoricoProrrogaCandidatoBLL.ModificarHistoricoProrrogaCandidato(pHistoricoProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pHistoricoProrrogaCandidato"></param>
        public int EliminarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                return vHistoricoProrrogaCandidatoBLL.EliminarHistoricoProrrogaCandidato(pHistoricoProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pIdHistoricoProrrogaCandidato"></param>
        public HistoricoProrrogaCandidato ConsultarHistoricoProrrogaCandidato(int pIdHistoricoProrrogaCandidato)
        {
            try
            {
                return vHistoricoProrrogaCandidatoBLL.ConsultarHistoricoProrrogaCandidato(pIdHistoricoProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pFechaIncioAnterior"></param>
        /// <param name="pFechaIncioNueva"></param>
        /// <param name="pFechaFinAnterior"></param>
        /// <param name="pFechaFinNueva"></param>
        public List<HistoricoProrrogaCandidato> ConsultarHistoricoProrrogaCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, DateTime? pFechaIncioAnterior, DateTime? pFechaIncioNueva, DateTime? pFechaFinAnterior, DateTime? pFechaFinNueva)
        {
            try
            {
                return vHistoricoProrrogaCandidatoBLL.ConsultarHistoricoProrrogaCandidatos(pIdCupoArea, pIdProyeccionPresuspuesto, pFechaIncioAnterior, pFechaIncioNueva, pFechaFinAnterior, pFechaFinNueva);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Adicion Y Prorrogas
        public int InsertarAdicionYProrrogas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasBLL.InsertarCupoAreas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de modificación para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int ModificarAdicionYProrrogas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasBLL.ModificarAdicionYProrrogas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<AdicionesYProrrogas> ConsultarAdicionYProrrogas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasBLL.ConsultarAdicionYProrrogas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion


        #region AdicionYProrrogas
        public int InsertarAdicionProrrogaCandidato(AdicionProrrogaCandidato pAdicionProrrogaCandidato)
        {
            try
            {
                return vAdicionProrrogaCandidatoBLL.InsertarAdicionProrrogaCandidato(pAdicionProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pAdicionProrrogaCandidato"></param>
        public int ModificarAdicionProrrogaCandidato(AdicionProrrogaCandidato pAdicionProrrogaCandidato)
        {
            try
            {
                return vAdicionProrrogaCandidatoBLL.ModificarAdicionProrrogaCandidato(pAdicionProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pAdicionProrrogaCandidato"></param>
        public int EliminarAdicionProrrogaCandidato(int pAdicionProrrogaCandidato)
        {
            try
            {
                return vAdicionProrrogaCandidatoBLL.EliminarAdicionProrrogaCandidato(pAdicionProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pIdAdicionProrrogaCandidato"></param>
        public AdicionProrrogaCandidato ConsultarAdicionProrrogaCandidato(int pIdAdicionProrrogaCandidato)
        {
            try
            {
                return vAdicionProrrogaCandidatoBLL.ConsultarAdicionProrrogaCandidato(pIdAdicionProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pFechaIncioProyeccion"></param>
        /// <param name="pFechaFinProyeccion"></param>
        /// <param name="pValorAdicionado"></param>
        public List<AdicionProrrogaCandidato> ConsultarAdicionProrrogaCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, DateTime? pFechaIncioProyeccion, DateTime? pFechaFinProyeccion, Decimal? pValorAdicionado)
        {
            try
            {
                return vAdicionProrrogaCandidatoBLL.ConsultarAdicionProrrogaCandidatos(pIdCupoArea, pIdProyeccionPresuspuesto, pFechaIncioProyeccion, pFechaFinProyeccion, pValorAdicionado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

    }
}