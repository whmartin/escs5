﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class InfoAdjudicacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTramiteJudicial { get; set; }
        public int IdTipoTramite { get; set; }
        public int IdDenunciaBien { get; set; }
        public int IdDespachoJudicial { get; set; }
        public int IdSentidoSentencia { get; set; }
        public string TipoTramite { get; set; }
        public DateTime FechaRadicacion { get; set; }
        public string DespachoJudicialAsignado { get; set; }
        public string Sentencia { get; set; }
        public DateTime FechaSentencia { get; set; }
        public string SentidoSentencia { get; set; }
        public DateTime FechaConstanciaEjecutoria { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string NombreJuzgado { get; set; }
        public string NombreDepartamento { get; set; }
        public string NombreMunicipio { get; set; }
    }
}
