﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class ContinuarOficio : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdContinuarOficio { get; set; }
        public int IdDenunciaBien { get; set; }
        public decimal NumeroResolucion { get; set; }
        public string Observaciones { get; set; }
        public int IdMotivoContinuarOficio { get; set; }
        public string NombreMotivoContinuarOficio { get; set; }
        /// <summary>
        /// Obtiene o establece IdUsuarioCrea
        /// </summary>
        public int IdUsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

    }
}
