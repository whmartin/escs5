using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class ReporteDenuncias : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public ReporteDenuncias()
        {
        }

        public int IdReporteDenuncias
        {
            get;
            set;
        }

        public int IdTipoPersonaAsociacion
        {
            get;
            set;
        }

        public String NombreTipoPersonaAsociacion
        {
            get;
            set;
        }

        public int IdTipoIdentificacion
        {
            get;
            set;
        }

        public String TipoIdentificacion
        {
            get;
            set;
        }

        public String NumeroIdentificacion
        {
            get;
            set;
        }

        public int NumeroDeIdentificacion
        {
            get;
            set;
        }

        public int IdDepartamentoUbicacion
        {
            get;
            set;
        }

        public String DepartamentoUbicacion
        {
            get;
            set;
        }

        public int Estado
        {
            get;
            set;
        }

        public String NombreEstado
        {
            get;
            set;
        }

        public DateTime RegistradoDesde
        {
            get;
            set;
        }

        public DateTime RegistradoHasta
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }

        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }

        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}
