﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad AnulacionDenuncia con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class AnulacionDenuncia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdAnulacionDenuncia
        /// </summary>
        public int IdSolicitudAnulacion { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece MotivoAnulacion
        /// </summary>
        public string MotivoAnulacion { get; set; }

        /// <summary>
        /// Obtiene o establece IdMotivoAnulacion
        /// </summary>
        public int IdMotivoAnulacion { get; set; }

        /// <summary>
        /// Obtiene o establece DescripcionAnulacion
        /// </summary>
        public string DescripcionAnulacion { get; set; }

        /// <summary>
        /// Obtiene o establece AnulacionAprobada
        /// </summary>
        public string AnulacionAprobada { get; set; }

        /// <summary>
        /// Obtiene o establece IdAnulacionAprobada
        /// </summary>
        public int IdAnulacionAprobada { get; set; }

        /// <summary>
        /// Obtiene o establece Observaciones
        /// </summary>
        public string Observacion { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRespuestaAnulacion
        /// </summary>
        public DateTime? FechaSolicitud { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRespuestaAnulacion
        /// </summary>
        public DateTime? FechaRespuestaSolicitud { get; set; }

        /// <summary>
        /// Obtiene o establece IdUsuarioCrea
        /// </summary>
        public int IdUsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        #region Correo

        /// <summary>
        /// Obtiene o establece Para
        /// </summary>
        public string Para { get; set; }

        /// <summary>
        /// Obtiene o establece Asunto
        /// </summary>
        public string Asunto { get; set; }

        /// <summary>
        /// Obtiene o establece Mensaje
        /// </summary>
        public string Mensaje { get; set; }

        /// <summary>
        /// Obtiene o establece NombreEnviar
        /// </summary>
        public string NombreaEnviar { get; set; }

        #endregion

    }
}
