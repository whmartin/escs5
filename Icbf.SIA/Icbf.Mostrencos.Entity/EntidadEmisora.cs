﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad EntidadEmisora con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class EntidadEmisora : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdEntidadEmisora
        /// </summary>
        public string IdEntidadEmisora { get; set; }

        /// <summary>
        /// Obtiene o establece NombreEntidadEmisora
        /// </summary>
        public string NombreEntidadEmisora { get; set; }
    }
}
