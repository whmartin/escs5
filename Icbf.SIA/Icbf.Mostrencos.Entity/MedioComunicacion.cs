using System;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad MedioComunicacion con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class MedioComunicacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdMedioComunicacion
        /// </summary>
        public int IdMedioComunicacion { get; set; }

        /// <summary>
        /// Obtiene o establece Nombre
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Obtiene o establece Estado
        /// </summary>
        public bool? Estado { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime? FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime? FechaModifica { get; set; }

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="MedioComunicacion"/>.
        /// </summary>
        public MedioComunicacion()
        {
        }
    }
}