﻿//-----------------------------------------------------------------------
// <copyright file="TiposDocumentosGlobal.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TiposDocumentosGlobal.</summary>
// <author>INGENIAN SOFTWARE[Darío Beltán]</author>
// <date>18/03/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad TiposDocumentosGlobal con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TiposDocumentosGlobal : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TiposDocumentosGlobal"/>.
        /// </summary>
        public TiposDocumentosGlobal()
        {
        }

        /// <summary>
        /// Obtiene o establece IdTipoDocumento
        /// </summary>
        public int IdTipoDocumento
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece Código Documento
        /// </summary>
        public string CodDocumento
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece NombreTipoDocumento
        /// </summary>
        public string NomTipoDocumento
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece CODSIIF
        /// </summary>
        public string codSiif
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece ValorSIRECI
        /// </summary>
        public string ValorSIRECI { get; set; }
    }
}
