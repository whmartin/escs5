﻿//-----------------------------------------------------------------------
// <copyright file="resultFiltroDenunciante.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase resultFiltroDenunciante.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad Regional con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class ResultFiltroDenunciante : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Obtiene o establece radicadoDenuncia
        /// </summary>
        public string radicadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece TipoIdentificacion
        /// </summary>
        public string TipoIdentificacion { get; set; }

        /// <summary>
        /// Obtiene o establece RazonSocial
        /// </summary>
        public string RazonSocial { get; set; }

        /// <summary>
        /// Obtiene o establece NroRegDefuncion
        /// </summary>
        public long NroRegDefuncion { get; set; }

        /// <summary>
        /// Obtiene o establece fechaDefuncion
        /// </summary>
        public DateTime fechaDefuncion { get; set; }

        /// <summary>
        /// Obtiene o establece fechaRadicadoDenuncia
        /// </summary>
        public DateTime fechaRadicadoDenuncia { get; set; }

    }
}