﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad MarcaBien con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class MarcaBien : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdMarcaBien
        /// </summary>
        public string IdMarcaBien { get; set; }

        /// <summary>
        /// Obtiene o establece NombreMarcaBien
        /// </summary>
        public string NombreMarcaBien { get; set; }
    }
}
