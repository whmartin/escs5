﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Tercero con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class Tercero : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="Tercero"/>.
        /// </summary>
        public Tercero()
        {
            this.Estadotercero = new EstadoTercero();
            this.TiposDocumentosGlobal = new TiposDocumentosGlobal();
        }

        /// <summary>
        /// Obtiene o establece IdTercero
        /// </summary>
        [Key]
        public int IdTercero { get; set; }

        /// <summary>
        /// Obtiene o establece IdEstadoTercero
        /// </summary>
        public int IdEstadoTercero { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoPersona
        /// </summary>
        public int IdTipoPersona { get; set; }

        /// <summary>
        /// Obtiene o establece Clave de usuario del proveedor
        /// </summary>
        public string ProviderUserKey { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroIdentificación
        /// </summary>
        public string NumeroIdentificacion { get; set; }

        /// <summary>
        /// Obtiene o establece DigitoVerificación
        /// </summary>
        public int DigitoVerificacion { get; set; }

        /// <summary>
        /// Obtiene o establece Correo Electrónico
        /// </summary>
        public string CorreoElectronico { get; set; }

        /// <summary>
        /// Obtiene o establece PrimerNombre
        /// </summary>
        public string PrimerNombre { get; set; }

        /// <summary>
        /// Obtiene o establece SegundoNombre
        /// </summary>
        public string SegundoNombre { get; set; }

        /// <summary>
        /// Obtiene o establece PrimerApellido
        /// </summary>
        public string PrimerApellido { get; set; }

        /// <summary>
        /// Obtiene o establece SegundoApellido
        /// </summary>
        public string SegundoApellido { get; set; }

        /// <summary>
        /// Obtiene o establece RazónSocial
        /// </summary>
        public string RazonSocial { get; set; }

        /// <summary>
        /// Obtiene o establece Fecha Expedición
        /// </summary>
        public DateTime FechaExpedicion { get; set; }

        /// <summary>
        /// Obtiene o establece FechaNacimiento
        /// </summary>
        public DateTime FechaNacimiento { get; set; }

        /// <summary>
        /// Obtiene o establece Sexo
        /// </summary>
        public string Sexo { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si EsFundación
        /// </summary>
        public bool EsFundacion { get; set; }

        /// <summary>
        /// Obtiene o establece ConsecutivoInterno
        /// </summary>
        public string ConsecutivoInterno { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si CreadoPorInterno
        /// </summary>
        public bool CreadoPorInterno { get; set; }

        /// <summary>
        /// Obtiene o establece IdTerceroEnOferentes
        /// </summary>
        public int IdTerceroEnOferentes { get; set; }

        /// <summary>
        /// Obtiene o establece Indicativo
        /// </summary>
        public string Indicativo { get; set; }

        /// <summary>
        /// Obtiene o establece Teléfono
        /// </summary>
        public string Telefono { get; set; }

        /// <summary>
        /// Obtiene o establece Extensión
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Obtiene o establece Celular
        /// </summary>
        public string Celular { get; set; }

        /// <summary>
        /// Obtiene o establece IdMunicipio
        /// </summary>
        public string IdMunicipio { get; set; }

        /// <summary>
        /// Obtiene el nombre del Municipio
        /// </summary>
        public string Municipio { get; set; }

        /// <summary>
        /// Obtiene el nombre del Departamento
        /// </summary>
        public string Departamento { get; set; }

        /// <summary>
        /// Obtiene o establece IdZona
        /// </summary>
        public int IdZona { get; set; }

        /// <summary>
        /// Obtiene o establece Dirección
        /// </summary>
        public string Direccion { get; set; }

        /// <summary>
        /// Obtiene o establece ClaseActividad
        /// </summary>
        public string ClaseActividad { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoDocumentoIdentifica
        /// </summary>
        public int IdTipoDocIdentifica { get; set; }

        /// <summary>
        /// Obtiene o establece Estado Tercero
        /// </summary>
        public EstadoTercero Estadotercero { get; set; }

        /// <summary>
        /// Obtiene o establece TiposDocumentosGlobal
        /// </summary>
        public TiposDocumentosGlobal TiposDocumentosGlobal { get; set; }

        /// <summary>
        /// Obtiene o establece NombreMunicipio
        /// </summary>
        public string NombreMunicipio { get; set; }

        /// <summary>
        /// Obtiene o establece NombreTipoPersona
        /// </summary>
        public string NombreTipoPersona { get; set; }

        /// <summary>
        /// Obtiene o establece NombreDepartamento
        /// </summary>
        public string NombreDepartamento { get; set; }

        /// <summary>
        /// Obtiene o establece el Tipo de Sector de la Entidad
        /// </summary>
        public int IdTipoSectorEntidad { get; set; }

        /// <summary>
        /// Obtiene o establece la Entidad
        /// </summary>
        public string Entidad { get; set; }
    }
}
