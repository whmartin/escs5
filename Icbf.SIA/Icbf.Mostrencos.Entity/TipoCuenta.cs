﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad TipoCuenta con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TipoCuenta : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdTipoCta
        /// </summary>
        public string IdTipoCta { get; set; }

        /// <summary>
        /// Obtiene o establece NombreTipoCta
        /// </summary>
        public string NombreTipoCta { get; set; }
    }
}
