//-----------------------------------------------------------------------
// <copyright file="RegistroDenuncia.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistroDenuncia.</summary>
// <author>INGENIAN SOFTWARE[Darío Beltán]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad RegistroDenuncia con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class RegistroDenuncia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegistroDenuncia"/> class.
        /// </summary>
        public RegistroDenuncia()
        {
            DocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();
            this.Abogado = new AsignacionAbogado();
            this.ListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();
        }

        /// <summary>
        /// Gets or sets IdDenunciaBien. Propiedad PK autonumérica
        /// </summary>
        /// <value>El IdDenunciaBien</value>
        public int IdDenunciaBien
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdTercero
        /// </summary>
        public int? IdTercero
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IDTIPODOCIDENTIFICA
        /// </summary>
        public int IDTIPODOCIDENTIFICA
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NUMEROIDENTIFICACION
        /// </summary>
        public string NUMEROIDENTIFICACION
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CodigoTipoIdentificacionPersonaNatural
        /// </summary>
        public string CodigoTipoIdentificacionPersonaNatural
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DIGITOVERIFICACION
        /// </summary>
        public int DIGITOVERIFICACION
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdTipoPersona
        /// </summary>
        public int IdTipoPersona
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreTipoPersona
        /// </summary>
        public string NombreTipoPersona
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PRIMERNOMBRE
        /// </summary>
        public string PRIMERNOMBRE
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SEGUNDONOMBRE
        /// </summary>
        public string SEGUNDONOMBRE
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PRIMERAPELLIDO
        /// </summary>
        public string PRIMERAPELLIDO
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SEGUNDOAPELLIDO
        /// </summary>
        public string SEGUNDOAPELLIDO
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdDepartamento
        /// </summary>
        public int IdDepartamento
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreDepartamento
        /// </summary>
        public string NombreDepartamento
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdMunicipio
        /// </summary>
        public int IdMunicipio
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreMunicipio
        /// </summary>
        public string NombreMunicipio
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreRegional
        /// </summary>
        public string NombreRegional
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdZona
        /// </summary>
        public string IdZona
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Direccion
        /// </summary>
        public string Direccion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Indicativo
        /// </summary>
        public string Indicativo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Telefono
        /// </summary>
        public string Telefono
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Celular
        /// </summary>
        public string Celular
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CORREOELECTRONICO
        /// </summary>
        public string CORREOELECTRONICO
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Acepto
        /// </summary>
        /// <value>El Acepto</value>
        public bool Acepto
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DescripcionDenuncia
        /// </summary>
        public string DescripcionDenuncia
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdRegionalUbicacion
        /// </summary>
        public int IdRegionalUbicacion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreArchivo
        /// </summary>
        public string NombreArchivo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ObservacionesDocumentacionSolicitada
        /// </summary>
        public string ObservacionesDocumentacionSolicitada
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DenunciaRecibidaCorrespondenciaPorInterna
        /// </summary>
        public string DenunciaRecibidaCorrespondenciaPorInterna
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RadicadoCorrespondencia
        /// </summary>
        public string RadicadoCorrespondencia
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaRadicadoCorrespondencia
        /// </summary>
        public DateTime FechaRadicadoCorrespondencia
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Fase
        /// </summary>
        public int IdFase
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets HoraRadicadoCorrespondencia
        /// </summary>
        public string HoraRadicadoCorrespondencia
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RadicadoDenuncia
        /// </summary>
        public string RadicadoDenuncia
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdEstado
        /// </summary>
        public int IdEstado
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreEstadoDenuncia
        /// </summary>
        public string NombreEstadoDenuncia
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RazonSocial
        /// </summary>
        public string RazonSocial
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreTipoIdentificacion
        /// </summary>
        public string NombreTipoIdentificacion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CodigoTipoIdentificacion
        /// </summary>
        public string CodigoTipoIdentificacion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaRadicadoDenuncia
        /// </summary>
        public DateTime FechaRadicadoDenuncia
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreRegionalUbicacion
        /// </summary>
        public string NombreRegionalUbicacion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Abogado
        /// </summary>
        public AsignacionAbogado Abogado
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DocumentosSolicitadosDenunciaBien
        /// </summary>
        public DocumentosSolicitadosDenunciaBien DocumentosSolicitadosDenunciaBien
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece ListaHistóricoEstadosDenunciaBien
        /// </summary>
        public List<HistoricoEstadosDenunciaBien> ListaHistoricoEstadosDenunciaBien { get; set; }

        /// <summary>
        /// Gets or sets NombreMostrar
        /// </summary>
        public string NombreMostrar
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ClaseDenuncia
        /// </summary>
        public string ClaseDenuncia
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ConsecutivoDenuncia
        /// </summary>
        public int ConsecutivoDenuncia
        {
            get;
            set;
        }

        public int AnioRadicado
        {
            get;
            set;
        }

        public int IdTipoDocumento
        {
            get;
            set;
        }

        public string CodDocumento
        {
            get;
            set;
        }
        public int IdAbogado
        {
            get;
            set;
        }
        public int IdEstadoDenuncia
        {
            get;
            set;
        }

        public bool Existe { get; set; }

        public string TipoTramite { get; set; }

        public bool? DocumentacionCompleta { get; set; }
    }
}
