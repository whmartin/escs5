﻿namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad EntidadEmisora con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class DecisionAutoInadmitida : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdEntidadEmisora
        /// </summary>
        public string IdDecisionAutoInadmitida { get; set; }

        /// <summary>
        /// Obtiene o establece NombreEntidadEmisora
        /// </summary>
        public string NombreDecisionAutoInadmitida { get; set; }

        public bool Estado { get; set; }

        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }
    }
}
