﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class DenunciaDatosEntidad
    {
        public int IdDenunciaBien { get; set; }
        public string NombreEntidad { get; set; }
        public string Departamento { get; set; }
        public string Municipio { get; set; }
        public string Direccion { get; set; }
    }
}
