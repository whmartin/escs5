﻿


namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Apoderados con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class Apoderados : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="Causantes"/>.
        /// </summary>
        public Apoderados()
        {
            this.Tercero = new Tercero();
            this.DenunciaBuen = new DenunciaBien();
        }
        /// <summary>
        /// Obtiene o establece IdCausante
        /// </summary>
        public int IdApoderado { get; set; }

        /// <summary>
        /// Obtiene o establece IdTercero
        /// </summary>
        public int? IdTercero { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece ExistePoder
        /// </summary>
        public string ExisteApoderado { get; set; }

        /// <summary>
        /// Obtiene o establece DenuncianteEsApoderado
        /// </summary>
        public string DenuncianteEsApoderado { get; set; }

        /// <summary>
        /// Obtiene o establece FechaInicioPoder
        /// </summary>
        public DateTime? FechaInicioPoder { get; set; }

        /// <summary>
        /// Obtiene o establece FechaFinPoder
        /// </summary>
        public DateTime? FechaFinPoder { get; set; }

        /// <summary>
        /// Obtiene o establece Observaciones
        /// </summary>
        public string Observaciones { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoApoderado
        /// </summary>
        public string EstadoApoderado { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroTarjetaProfecional
        /// </summary>
        public string NumeroTarjetaProfesional { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece Tercero
        /// </summary>
        public Tercero Tercero { get; set; }

        /// <summary>
        /// Obtiene o establece DenunciaBuen
        /// </summary>
        public DenunciaBien DenunciaBuen { get; set; }

        /// <summary>
        /// Obtiene o establece NombreMostrar
        /// </summary>
        public string NombreMostrar { get; set; }

        /// <summary>
        /// Obtiene o establece IdUsuarioCrea
        /// </summary>
        public int IdUsuarioCrea { get; set; }
    }
}
