﻿//-----------------------------------------------------------------------
// <copyright file="resultFiltroDenunciante.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase resultFiltroDenunciante.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad Regional con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class FiltroMueble : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece tipoBien
        /// </summary>
        public string RadicadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece tipoBien
        /// </summary>
        public string tipoBien { get; set; }
        /// <summary>
        /// Obtiene o establece subTipoBien
        /// </summary>
        public string subTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece claseBien
        /// </summary>
        public string claseBien { get; set; }

        /// <summary>
        /// Obtiene o establece nroIdenticacionBien
        /// </summary>
        public string nroIdenticacionBien { get; set; }

        /// <summary>
        /// Obtiene o establece matriculaInmobiliaria
        /// </summary>
        public string matriculaInmobiliaria { get; set; }

        /// <summary>
        /// Obtiene o establece cedulaCatastral
        /// </summary>
        public long cedulaCatastral { get; set; }

        /// <summary>
        /// Obtiene o establece marcaBien
        /// </summary>
        public string marcaBien { get; set; }

    }
}