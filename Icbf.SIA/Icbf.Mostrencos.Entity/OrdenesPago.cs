﻿//-----------------------------------------------------------------------
// <copyright file="OrdenesPago.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase OrdenesPago.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>22/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;

namespace Icbf.Mostrencos.Entity
{
    [Serializable]
    public class OrdenesPago : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase ModalidadPago
        /// </summary>
        public OrdenesPago()
        {
            this.IdModalidadPago = new ModalidadPago();
        }

        /// <summary>
        /// Obtiene o establece IdOrdenesPago
        /// </summary>
        [Key]
        public int IdOrdenesPago
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece IdModalidadPago de la entidad ModalidadPago
        /// </summary>
        public ModalidadPago IdModalidadPago
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece FechaSolicitudLiquidacion
        /// </summary>
        public DateTime? FechaSolicitudLiquidacion
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece NumeroResolucion
        /// </summary>
        public decimal NumeroResolucion
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece FechaResolucionPago
        /// </summary>
        public DateTime? FechaResolucionPago
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece ValorAPagar
        /// </summary>
        public decimal ValorAPagar
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece NumeroComprobantePago
        /// </summary>
        public decimal NumeroComprobantePago
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece ValorPagado
        /// </summary>
        public decimal ValorPagado
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime? FechaCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime? FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece un valor que indica si es nuevo el archivo solicitado
        /// </summary>
        public bool EsNuevo { get; set; }

        /// <summary>
        /// Obtiene o establece Estado
        /// </summary>
        public bool Estado
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece un valor que indica si Nombre del Estado del archivo solicitado
        /// </summary>
        public String NombreEstadoDocumento
        {
            get;
            set;
        }
    }
}
