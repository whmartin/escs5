﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad EntidadBancaria con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class EntidadBancaria : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdEntidadBancaria
        /// </summary>
        public string IdEntidadBancaria { get; set; }

        /// <summary>
        /// Obtiene o establece NombreEntidadBancaria
        /// </summary>
        public string NombreEntidadBancaria { get; set; }
    }
}
