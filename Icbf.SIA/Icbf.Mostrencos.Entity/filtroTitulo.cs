﻿//-----------------------------------------------------------------------
// <copyright file="resultFiltroDenunciante.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase resultFiltroDenunciante.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad Regional con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class FiltroTitulo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece RadicadoDenuncia
        /// </summary>
        public string RadicadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece tipoBien
        /// </summary>
        public string tipoTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece nroIdenticacionBien
        /// </summary>
        public string nroCuentaBancaria { get; set; }

        /// <summary>
        /// Obtiene o establece nroTituloValor
        /// </summary>
        public string nroTituloValor { get; set; }

        /// <summary>
        /// Obtiene o establece entidadEmisora
        /// </summary>
        public string entidadEmisora { get; set; }

    }
}
