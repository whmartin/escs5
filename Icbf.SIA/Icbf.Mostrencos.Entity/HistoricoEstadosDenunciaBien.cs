using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    [Serializable]
    public class HistoricoEstadosDenunciaBien : Icbf.Seguridad.Entity.EntityAuditoria
    {

        [Key]
        public int IdHistoricoEstadoDenunciaBien
        {
            get;
            set;
        }
        public int IdDenunciaBien
        {
            get;
            set;
        }
        public int IdEstadoDenuncia
        {
            get;
            set;
        }
        public string NombreEstadoDenuncia
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public string NombreEstado
        {
            get;
            set;
        }
        public string Responsable
        {
            get;
            set;
        }

        public int IdFase { get; set; }
        public int IdAccion { get; set; }
        public int IdActuacion { get; set; }
        public string Fase { get; set; }
        public string Accion { get; set; }
        public string Actuacion { get; set; }
        public int IdUsuarioCrea { get; set; }
        public HistoricoEstadosDenunciaBien()
        {
        }
    }
}
