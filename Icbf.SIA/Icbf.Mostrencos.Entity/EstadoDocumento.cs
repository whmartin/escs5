﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad EstadoDocumento con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class EstadoDocumento
    {
        /// <summary>
        /// Obtiene o establece IdEstadoDocumento
        /// </summary>
        public int IdEstadoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece CódigoEstadoDocumento
        /// </summary>
        public string CodigoEstadoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece NombreEstadoDocumento
        /// </summary>
        public string NombreEstadoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si el Estado es activo o inactivo
        /// </summary>
        public bool Estado { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }
    }
}
