﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad TipoBien con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TipoBien : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TipoBien"/>.
        /// </summary>
        public TipoBien()
        {
            //this.EstadoDenuncia = new EstadoDenuncia();
            //this.Abogados = new Abogados();
            //this.Regional = new Regional();
            //this.Tercero = new Tercero();
            //this.ListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();
            //this.ListaTrasladoDenuncia = new List<TrasladoDenuncia>();
        }

		/// <summary>
        /// Obtiene o establece IdTipoBien
        /// </summary>
        public string IdTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece NombreTipoBien
        /// </summary>
        public string NombreTipoBien { get; set; }
		
        /// <summary>
        /// Obtiene o establece tipoBien
        /// </summary>
        public int idTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece idTipoProducto
        /// </summary>
        public int idTipoProducto { get; set; }

        /// <summary>
        /// Obtiene o establece tipoProducto
        /// </summary>
        public string tipoProducto { get; set; }

        /// <summary>
        /// Obtiene o establece un codProducto
        /// </summary>
        public long codProducto { get; set; }

        /// <summary>
        /// Obtiene o establece Producto
        /// </summary>
        public string Producto { get; set; }

    }
}
