

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Abogados con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class GN_VDIP
    {
        /// <summary>
        /// Obtiene o establece CODIGP_PAIS
        /// </summary>
        public string CODIGP_PAIS { get; set; }

        /// <summary>
        /// Obtiene o establece NOMBRE_PAIS
        /// </summary>
        public string NOMBRE_PAIS { get; set; }

        /// <summary>
        /// Obtiene o establece CODIGO_DEPARTAMENTO
        /// </summary>
        public string CODIGO_DEPARTAMENTO { get; set; }

        /// <summary>
        /// Obtiene o establece NOMBRE_DEPARTAMENTO
        /// </summary>
        public string NOMBRE_DEPARTAMENTO { get; set; }

        /// <summary>
        /// Obtiene o establece CODIGO_MUNICIPIO
        /// </summary>
        public string CODIGO_MUNICIPIO { get; set; }

        /// <summary>
        /// Obtiene o establece NOMBRE_MUNICIPIO
        /// </summary>
        public string NOMBRE_MUNICIPIO { get; set; }
    }
}
