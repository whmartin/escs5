﻿//-----------------------------------------------------------------------
// <copyright file="resultFiltroDenunciante.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase resultFiltroDenunciante.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad Regional con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class FiltroDenunciante : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece tipoBien
        /// </summary>
        public string RadicadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece NroIdentificacion
        /// </summary>
        public string TipoIdentificacion { get; set; }
        /// <summary>
        /// Obtiene o establece NroIdentificacion
        /// </summary>
        public string NroIdentificacion { get; set; }

        /// <summary>
        /// Obtiene o establece PrimerNombre
        /// </summary>
        public string PrimerNombre { get; set; }

        /// <summary>
        /// Obtiene o establece SegundoNombre
        /// </summary>
        public string SegundoNombre { get; set; }

        /// <summary>
        /// Obtiene o establece PrimerApellido
        /// </summary>
        public string PrimerApellido { get; set; }

        /// <summary>
        /// Obtiene o establece SegundoApellido
        /// </summary>
        public string SegundoApellido { get; set; }

        /// <summary>
        /// Obtiene o establece RazonSocial
        /// </summary>
        public string RazonSocial { get; set; }

        /// <summary>
        /// Obtiene o establece RegistroDefuncion
        /// </summary>
        public string RegistroDefuncion { get; set; }

        /// <summary>
        /// Obtiene o establece RegistroDefuncion, true para denunciante y false para Causante
        /// </summary>
        public Boolean Denuncia { get; set; }

    }
}