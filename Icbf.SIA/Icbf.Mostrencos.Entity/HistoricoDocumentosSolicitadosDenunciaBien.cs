﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad HistóricoDocumentosSolicitadosDenunciaBien con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class HistoricoDocumentosSolicitadosDenunciaBien : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="HistoricoDocumentosSolicitadosDenunciaBien"/>.
        /// </summary>
        public HistoricoDocumentosSolicitadosDenunciaBien()
        {
            this.TipoDocumentoBienDenunciado = new TipoDocumentoBienDenunciado();
            this.EstadoDocumento = new EstadoDocumento();
        }

        /// <summary>
        /// Obtiene o establece  IdHistóricoDocumentosSolicitadosDenunciaBien
        /// </summary>
        public int IdHistoricoDocumentosSolicitadosDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece  IdDocumentosSolicitadosDenunciaBien
        /// </summary>
        public int IdDocumentosSolicitadosDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece  IdTipoDocumentoBienDenunciado
        /// </summary>
        public int IdTipoDocumentoBienDenunciado { get; set; }

        /// <summary>
        /// Obtiene o establece  IdTipoDocumentoSoporteDenuncia
        /// </summary>
        public int IdTipoDocumentoSoporteDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece  FechaSolicitud
        /// </summary>
        public DateTime FechaSolicitud { get; set; }

        /// <summary>
        /// Obtiene o establece  IdEstadoDocumento
        /// </summary>
        public int? IdEstadoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece  FechaRecibido
        /// </summary>
        public DateTime? FechaRecibido { get; set; }

        /// <summary>
        /// Obtiene o establece  NombreArchivo
        /// </summary>
        public string NombreArchivo { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece  TipoDocumentoBienDenunciado
        /// </summary>
        public TipoDocumentoBienDenunciado TipoDocumentoBienDenunciado { get; set; }

        /// <summary>
        /// Obtiene o establece  EstadoDocumento
        /// </summary>
        public EstadoDocumento EstadoDocumento { get; set; }
    }
}
