﻿//-----------------------------------------------------------------------
// <copyright file="Actuacion.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Actuacion.</summary>
// <author>Ingenian Software</author>
// <date>10/10/2018</date>
//-----------------------------------------------------------------------

using System;

namespace Icbf.Mostrencos.Entity
{
    public class Actuacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Actuacion"/> class.
        /// </summary>
        public Actuacion()
        {

        }

        /// <summary>
        /// Gets or sets IdActuacion
        /// </summary>
        public int IdActuacion { get; set; }

        /// <summary>
        /// Gets or sets Nombre
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets Fecha Modifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Gets or sets Estado
        /// </summary>
        public byte Estado
        {
            get;
            set;
        }
    }
}
