﻿//-----------------------------------------------------------------------
// <copyright file="RegistroDenuncia.cs" company="ICBF"> 
// Copyright (c) 2018 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistroDenuncia.</summary>
// <author>INGENIAN SOFTWARE[Pablo Gómez]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad RegistroDenuncia con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class RegistroCalidadDenunciante : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Gets or sets IdTercero
        /// </summary>
        public int NumeroResolucion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdDenunciaBien. Propiedad PK autonumérica
        /// </summary>
        /// <value>El IdDenunciaBien</value>
        public string ReconocimientoCalidad
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaRadicadoCorrespondencia
        /// </summary>
        public DateTime FechaResolucion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HoraRadicadoCorrespondencia
        /// </summary>
        public string Observaciones
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdDenunciaBien. Propiedad PK autonumérica
        /// </summary>
        /// <value>El IdReconocimientoCalidadDenuncianteDetalle</value>
        public int IdReconocimientoCalidadDenunciante
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdDenunciaBien. Propiedad PK autonumérica
        /// </summary>
        /// <value>El IdReconocimientoCalidadDenuncianteDetalle</value>
        public string NombreDetalle
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdDenunciaBien. Propiedad PK autonumérica
        /// </summary>
        /// <value>El IdDenunciaBien</value>
        public int IdDenunciaBien
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea
        {
            get;
            set;
        }
    }
}

