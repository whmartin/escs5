﻿using System;

namespace Icbf.Mostrencos.Entity
{
    [Serializable]
    public class TipoDocumentoSoporteDenuncia
    {
        public int IdTipoDocumentoSoporteDenuncia { get; set; }
        public string NombreDocumento { get; set; }
        public string DescripcionDocumento { get; set; }
        public string Estado { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
    }
}
