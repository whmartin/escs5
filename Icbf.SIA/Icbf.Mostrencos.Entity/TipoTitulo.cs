﻿//-----------------------------------------------------------------------
// <copyright file="TipoTitulo.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoTitulo.</summary>
// <author>INGENIAN</author>
// <date>02/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad TipoTitulo con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TipoTitulo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TipoTitulo"/>.
        /// </summary>
        public TipoTitulo()
        {

        }

        public int IdTipoTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece Nombre
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Obtiene o establece NombreTipoTitulo
        /// </summary>
        public string NombreTipoTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece Nombre
        /// </summary>
        public bool? Estado { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

    }
}