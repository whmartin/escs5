﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad SubTipoBien con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class SubTipoBien : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdTipoBien
        /// </summary>
        public string IdTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdSubTipoBien
        /// </summary>
        public string IdSubTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece NombreSubTipoBien
        /// </summary>
        public string NombreSubTipoBien { get; set; }
    }
}

