//-----------------------------------------------------------------------
// <copyright file="DocumentosSolicitadosDenunciaBien.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase DocumentosSolicitadosCausantes.</summary>
// <author>INGENIAN</author>
// <date>01/10/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad DocumentosSolicitadosDenunciaBien con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class DocumentosSolicitadosDenunciaBien : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DocumentosSolicitadosDenunciaBien"/>.
        /// </summary>
        public DocumentosSolicitadosDenunciaBien()
        {
            this.Documentos = new List<DocumentosSolicitadosDenunciaBien>();
            this.OtrosDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
            this.EstadoDocumento = new EstadoDocumento();
            this.TipoDocumentoBienDenunciado = new TipoDocumentoBienDenunciado();
            this.TipoDocumentoSoporteDenuncia = new TipoDocumentoSoporteDenuncia();
            this.Tercero = new Tercero();
        }

        /// <summary>
        /// Obtiene o establece IdDocumentosSolicitadosDenunciaBien
        /// </summary>
        public int IdDocumentosSolicitadosDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdCausante
        /// </summary>
        public int? IdCausante { get; set; }

        /// <summary>
        /// Obtiene o establece IdApoderado
        /// </summary>
        public int? IdApoderado { get; set; }

        /// <summary>
        /// Id calidad del denunciante
        /// </summary>
        public int? IdCalidadDenunciante { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoDocumentoBienDenunciado
        /// </summary>
        public int IdTipoDocumentoBienDenunciado { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoDocumentoSoporteDenuncia
        /// </summary>
        public int IdTipoDocumentoSoporteDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece TipoDocumento
        /// </summary>
        public string TipoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece FechaSolicitud
        /// </summary>
        public DateTime FechaSolicitud { get; set; }

        /// <summary>
        /// Obtiene o establece ObservacionesDocumentaciónSolicitada
        /// </summary>
        public string ObservacionesDocumentacionSolicitada { get; set; }

        /// <summary>
        /// Obtiene o establece IdEstadoDocumento
        /// </summary>
        public int? IdEstadoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRecibido
        /// </summary>
        public DateTime? FechaRecibido { get; set; }

        /// <summary>
        /// Obtiene o establece NombreArchivo
        /// </summary>
        public string NombreArchivo { get; set; }

        /// <summary>
        /// Obtiene o establece RutaArchivo
        /// </summary>
        public string RutaArchivo { get; set; }

        /// <summary>
        /// Obtiene o establece DocumentaciónCompleta
        /// </summary>
        public string DocumentacionCompleta { get; set; }

        /// <summary>
        /// Obtiene o establece ObservacionesDocumentaciónRecibida
        /// </summary>
        public string ObservacionesDocumentacionRecibida { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime? FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece NombreTipoDocumento
        /// </summary>
        public string NombreTipoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRecibidoGrilla
        /// </summary>
        public string FechaRecibidoGrilla { get; set; }

        /// <summary>
        /// Obtiene o establece FechaSolicitudGrilla
        /// </summary>
        public string FechaSolicitudGrilla { get; set; }

        /// <summary>
        /// Obtiene o establece FechaOficioGrilla
        /// </summary>
        public string FechaOficioGrilla { get; set; }


        /// <summary>
        /// Obtiene o establece ObservacionesDocumento
        /// </summary>
        public string ObservacionesDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece NombreEstado
        /// </summary>
        public string NombreEstado { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si ExisteArchivo
        /// </summary>
        public bool ExisteArchivo { get; set; }

        /// <summary>
        /// Obtiene o establece OtrosDocumentos
        /// </summary>
        public List<DocumentosSolicitadosDenunciaBien> OtrosDocumentos { get; set; }

        /// <summary>
        /// Obtiene o establece Documentos
        /// </summary>
        public List<DocumentosSolicitadosDenunciaBien> Documentos { get; set; }

        /// <summary>
        /// Obtiene o establece byts
        /// </summary>
        public byte[] bytes { get; set; }

        /// <summary>
        /// Obtiene o establece TipoDocumentoBienDenunciado
        /// </summary>
        public TipoDocumentoBienDenunciado TipoDocumentoBienDenunciado { get; set; }

        /// <summary>
        /// Obtiene o establece TipoDocumentoBienDenunciado
        /// </summary>
        public TipoDocumentoSoporteDenuncia TipoDocumentoSoporteDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoDocumento
        /// </summary>
        public EstadoDocumento EstadoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si ss nuevo el archivo solicitado
        /// </summary>
        public bool EsNuevo { get; set; }

        public Tercero Tercero { get; set; }

        public DateTime? FechaOficio { get; set; }

        public string AsuntoOficio { get; set; }

        public string ResumenOficio { get; set; }

    }
}
