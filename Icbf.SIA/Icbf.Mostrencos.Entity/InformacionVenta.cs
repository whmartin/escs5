﻿//-----------------------------------------------------------------------
// <copyright file="InformacionVenta.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase InformacionVenta.</summary>
// <author>Ingenian Software</author>
// <date>02/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad InformacionVenta con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class InformacionVenta : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InformacionVenta"/> class.
        /// </summary>
        public InformacionVenta()
        {
        }

        /// <summary>
        /// Gets or sets IdInformacionVenta.
        /// </summary>
        public int IdInformacionVenta { get; set; }

        /// <summary>
        /// Gets or sets IdTipoDocumentoVenta.
        /// </summary>
        public int? IdTipoDocumentoVenta { get; set; }

        /// <summary>
        /// Gets or sets NumeroDocumentoVenta.
        /// </summary>
        public int? NumeroDocumentoVenta { get; set; }

        /// <summary>
        /// Gets or sets ValorVenta.
        /// </summary>
        public decimal? ValorVenta { get; set; }

        /// <summary>
        /// Gets or sets FechaVenta.
        /// </summary>
        public DateTime FechaVenta { get; set; }

        /// <summary>
        /// Gets or sets FechaSolicitudAvaluo.
        /// </summary>
        public DateTime FechaSolicitudAvaluo { get; set; }

        /// <summary>
        /// Gets or sets FechaAvaluo.
        /// </summary>
        public DateTime FechaAvaluo { get; set; }

        /// <summary>
        /// Gets or sets ValorAvaluo.
        /// </summary>
        public decimal? ValorAvaluo { get; set; }

        /// <summary>
        /// Gets or sets ProfesionalrealizaAvaluo.
        /// </summary>
        public string ProfesionalrealizaAvaluo { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea.
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea.
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica.
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets FechaModifica.
        /// </summary>
        public DateTime? FechaModifica { get; set; }

        /// <summary>
        /// Gets or sets NombreTipoDocumento.
        /// </summary>
        public string NombreTipoDocumento { get; set; }
    }
}
