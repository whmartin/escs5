﻿//-----------------------------------------------------------------------
// <copyright file="Notarias.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Notarias.</summary>
// <author>INGENIAN</author>
// <date>28/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad Notarias con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class Notarias : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece CodNotaria
        /// </summary>
        public string CodNotaria{ get; set; }

        /// <summary>
        /// Obtiene o establece NombreNotaria
        /// </summary>
        public string NombreNotaria { get; set; }

        /// <summary>
        /// Obtiene o establece DepartamentoNotaria
        /// </summary>
        public string DepartamentoNotaria { get; set; }

        /// <summary>
        /// Obtiene o establece CiudadNotaria
        /// </summary>
        public string CiudadNotaria { get; set; }

    }
}
