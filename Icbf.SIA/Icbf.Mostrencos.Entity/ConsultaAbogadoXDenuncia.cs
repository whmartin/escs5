﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class ConsultaAbogadoXDenuncia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdDenuncia { get; set; }

        public int IdRegional { get; set; }

        public String Regional { get; set; }

        public int IdEstadoDenuncia { get; set; }

        public String EstadoDenuncia { get; set; }

        public DateTime? FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }

        public String NumeroDenuncia { get; set; }

        public String TipoIdentificacion { get; set; }

        public String NumeroIdentificacion { get; set; }

        public String NombreORazonSocial { get; set; }

        public String NombreTercero { get; set; }

        public String AbogadoAsignado { get; set; }

        public DateTime? FechaAsignacionAbogado { get; set; }

        public DateTime? FechaRadicadoDenuncia { get; set; }
    }
}
