﻿//-----------------------------------------------------------------------
// <copyright file="TipoDocumentoBienDenunciado.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoDocumentoBienDenunciado.</summary>
// <author>INGENIAN SOFTWARE[Darío Beltán]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad TipoDocumentoBienDenunciado con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TipoDocumentoBienDenunciado : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public TipoDocumentoBienDenunciado()
        {
            this.EstadoDocumento = new EstadoDocumento();
        }

        /// <summary>
        /// Gets or sets IdTipoDocumentoBienDenunciado
        /// </summary>
        public int IdTipoDocumentoBienDenunciado
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DescripcionDocumento
        /// </summary>
        public string DescripcionDocumento
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreTipoDocumento
        /// </summary>
        public string NombreTipoDocumento
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Estado
        /// </summary>
        /// <value>The Estado</value>
        public string Estado
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets EstadoDocumento
        /// </summary>
        public EstadoDocumento EstadoDocumento { get; set; }
    }
}
