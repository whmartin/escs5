﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class EnumeradoresDenuncia
    {

        public enum EnumClaseDenuncia
        {
            Vacante = 1,
            Mostrenco = 2,
            VocacionHereditaria = 3
        }

        public enum EnumModalidadPago
        {
            Parcial = 2,
            Total = 3
        }
    }
}
