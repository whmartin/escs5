﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    [Serializable]
    public class InfoMuebleProtocolizacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdMuebleInmueble { get; set; }
        public string TipoBien { get; set; }
        public string NombreTipoBien { get; set; }
        public string SubTipoBien { get; set; }
        public string NombreSubTipoBien { get; set; }
        public string ClaseBien { get; set; }
        public string NombreClaseBien { get; set; }
        public string EstadoBien { get; set; }
        public string DescripcionBien { get; set; }
        public DateTime? FechaAdjudicado { get; set; }
        public string FechaEscritura { get; set; }
        public string NumeroEscritura { get; set; }
        public string Notaria { get; set; }
        public string NumeroRegistro { get; set; }
        public DateTime? FechaIngresoICBF { get; set; }
        public DateTime? FechaInstrumentosPublicos { get; set; }
        public DateTime? FechaComunicacion { get; set; }
        public bool Modificado { get; set; }
    }
}
