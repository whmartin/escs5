﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class DenunciaTituloValor
    {
        public long IdBienDenunciaTipoValor { get; set; }
        public long IdBienDenuncia { get; set; }
        public int IdTipoCuentaBancaria { get; set; }
        public int IdEntidadBancaria { get; set; }
        public int IdDepartamento { get; set; }
        public string IdMunicipio { get; set; }
        public int IdTipoTitulo { get; set; }
        public string IdTipoIdentificacionEntidadEmisora { get; set; }
        public int IdEntidadEmisora { get; set; }
        public string IdClaseEntrada { get; set; }
        public string ClaseEntrada { get; set; }
        public int IdCodigoProducto { get; set; }
        public string CodigoProducto { get; set; }
        public float NumeroCuentaBancaria { get; set; }
        public float ValorEfectivoEstimado { get; set; }
        public float NumeroTituloValor { get; set; }
        public float NumeroIdentificacionBien { get; set; }
        public float CantidadTitulos { get; set; }
        public float ValorUnitarioTitulo { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public DateTime FechaRecibido { get; set; }
        public DateTime FechaVenta { get; set; }
        public float CantidadVendida { get; set; }
        public string DescripcionTitulo { get; set; }
        public DateTime FechaAdjudicado { get; set; }
        public int Estado { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
    }
}
