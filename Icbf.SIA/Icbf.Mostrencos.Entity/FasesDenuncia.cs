﻿//-----------------------------------------------------------------------
// <copyright file="TipoIdentificacion.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoIdentificacion.</summary>
// <author>INGENIAN SOFTWARE[Iván Gómez]</author>
// <date>30/04/2018</date>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class FasesDenuncia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoIdentificacion"/> class.
        /// </summary>
        public FasesDenuncia()
        {

        }

        /// <summary>
        /// Gets or sets IdFase
        /// </summary>
        public int IdFase
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreFase
        /// </summary>
        public string NombreFase
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Fecha Modifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Estado
        /// </summary>
        public byte Estado
        {
            get;
            set;
        }
    }
}
