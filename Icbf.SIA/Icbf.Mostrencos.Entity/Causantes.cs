﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Causantes con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class Causantes : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="Causantes"/>.
        /// </summary>
        public Causantes()
        {
            this.Tercero = new Tercero();
            this.DenunciaBuen = new DenunciaBien();
        }

        /// <summary>
        /// Obtiene o establece IdCausante
        /// </summary>
        public int IdCausante { get; set; }

        /// <summary>
        /// Obtiene o establece IdTercero
        /// </summary>
        public int IdTercero { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroRegistroDefunción
        /// </summary>
        public string NumeroRegistroDefuncion { get; set; }

        /// <summary>
        /// Obtiene o establece FechaDefunción
        /// </summary>
        public DateTime FechaDefuncion { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece Tercero
        /// </summary>
        public Tercero Tercero { get; set; }

        /// <summary>
        /// Obtiene o establece DenunciaBuen
        /// </summary>
        public DenunciaBien DenunciaBuen { get; set; }

        /// <summary>
        /// Obtiene o establece NombreMostrar
        /// </summary>
        public string NombreMostrar { get; set; }

        /// <summary>
        /// Obtiene o establece IdUsuarioCrea
        /// </summary>
        public int IdUsuarioCrea { get; set; }
    }
}
