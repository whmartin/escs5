﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class Vigencias : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdVigencia
        {
            get;
            set;
        }
        public string AcnoVigencia
        {
            get;
            set;
        }
        public String Activo
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public string acno
        {
            get;
            set;
        }
    }
}
