
using System;
using Icbf.SIA.Entity;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad DespachoJudicial con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>

    [Serializable]
    public class DespachoJudicial : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdDespachoJudicial
        /// </summary>
        public int IdDespachoJudicial { get; set; }

        /// <summary>
        /// Obtiene o establece Departamento
        /// </summary>
        public Departamento Departamento { get; set; }

        /// <summary>
        /// Obtiene o establece Municipio
        /// </summary>
        public Municipio Municipio { get; set; }

        /// <summary>
        /// Obtiene o establece Nombre
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Obtiene o establece Descripcion
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Obtiene o establece Estado
        /// </summary>
        public bool? Estado { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime? FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime? FechaModifica { get; set; }

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DespachoJudicial"/>.
        /// </summary>
        public DespachoJudicial()
        {
        }
    }
}
