﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class DenunciaMueble
    {
        public long IdBienDenunciaMueble { get; set; }
        public long IdBienDenuncia { get; set; }
        public int IdSubTipoBien { get; set; }
        public string SubTipoBien { get; set; }
        public int IdClaseBien { get; set; }
        public string ClaseBien { get; set; }
        public int IdDepartamento { get; set; }
        public string DepartamentoSeven { get; set; }
        public int IdMunicipio { get; set; }
        public string MunicipioSeven { get; set; }
        public int IdClaseEntrada { get; set; }
        public string ClaseEntrada { get; set; }
        public int IdEstadoFisicoBien { get; set; }
        public string EstadoFisicoBien { get; set; }
        public int IdPorcentajePertenenciaBien { get; set; }
        public string PorcentajePertenenciaBien { get; set; }
        public int IdDestinacionEconomica { get; set; }
        public string DestinacionEconomica { get; set; }
        public int IdMarcaBien { get; set; }
        public string MarcaBien { get; set; }
        public int IdTipoParametro { get; set; }
        public string TipoParametro { get; set; }
        public int IdTipoBien { get; set; }
        public string TipoBien { get; set; }
        public int IdCodigoParametro { get; set; }
        public string CodigoParametro { get; set; }
        public float ValorEstimadoComercial { get; set; }
        public string MatriculaInmobiliaria { get; set; }
        public float CedulaCatastral { get; set; }
        public string DireccionInmueble { get; set; }
        public string NumeroIdentificacionMueble { get; set; }
        public int Estado { get; set; }
        public string DescripcionBien { get; set; }
        public DateTime FechaVenta { get; set; }
        public DateTime FechaAdjudicado { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
    }
}
