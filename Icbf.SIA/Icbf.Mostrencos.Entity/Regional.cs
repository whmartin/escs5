﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Regional con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class Regional : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdRegional
        /// </summary>
        public int IdRegional { get; set; }

        /// <summary>
        /// Obtiene o establece NombreRegional
        /// </summary>
        public string NombreRegional { get; set; }

        /// <summary>
        /// Obtiene o establece CódigoRegional
        /// </summary>
        public string CodigoRegional { get; set; }

        /// <summary>
        /// Obtiene o establece Estado
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Obtiene o establece Código PCI
        /// </summary>
        public string CodPCI { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }
    }
}
