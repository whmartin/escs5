﻿
namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad DenunciaBien con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class DenunciaBien : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DenunciaBien"/>.
        /// </summary>
        public DenunciaBien()
        {
            this.EstadoDenuncia = new EstadoDenuncia();
            this.Abogados = new Abogados();
            this.Regional = new Regional();
            this.Tercero = new Tercero();
            this.ListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();
            this.ListaTrasladoDenuncia = new List<TrasladoDenuncia>();
            this.desicionAuto = new DesicionAuto();
            this.decisionAutoInadmitida = new DecisionAutoInadmitida();
            this.decisionRecurso = new DecisionRecurso();
        }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        [Key]
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece RadicadoDenuncia
        /// </summary>
        public string RadicadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece IdTercero
        /// </summary>
        public int IdTercero { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si Acepto
        /// </summary>
        public bool Acepto { get; set; }

        /// <summary>
        /// Obtiene o establece DescripciónDenuncia
        /// </summary>
        public string DescripcionDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece IdRegionalUbicación
        /// </summary>
        public int IdRegionalUbicacion { get; set; }

        /// <summary>
        /// Obtiene o establece DenunciaRecibidaCorrespondenciaPorInterna
        /// </summary>
        public string DenunciaRecibidaCorrespondenciaPorInterna { get; set; }

        /// <summary>
        /// Obtiene o establece RadicadoCorrespondencia
        /// </summary>
        public string RadicadoCorrespondencia { get; set; }

        /// <summary>
        /// Obtiene o establece InterposicionRecurso
        /// </summary>
        public bool InterposicionRecurso { get; set; }


        /// <summary>
        /// Obtiene o establece ExisteDenuncia
        /// </summary>
        public bool ExisteDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRadicadoCorrespondencia
        /// </summary>
        public DateTime? FechaRadicadoCorrespondencia { get; set; }

        /// <summary>
        /// Obtiene o establece HoraRadicadoCorrespondencia
        /// </summary>
        public DateTime? HoraRadicadoCorrespondencia { get; set; }

        /// <summary>
        /// Obtiene o establece IdEstadoDenuncia
        /// </summary>
        public int IdEstadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRadicadoDenuncia
        /// </summary>
        public DateTime? FechaRadicadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece IdAbogado
        /// </summary>
        public int IdAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece FechaAsignaciónAbogado
        /// </summary>
        public DateTime? FechaAsignacionAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece IdClaseDenuncia
        /// </summary>
        public int IdClaseDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece Abogados
        /// </summary>
        public Abogados Abogados { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoDenuncia
        /// </summary>
        public EstadoDenuncia EstadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece Regional
        /// </summary>
        public Regional Regional { get; set; }

        /// <summary>
        /// Obtiene o establece Tercero
        /// </summary>
        public Tercero Tercero { get; set; }

        /// <summary>
        /// Obtiene o establece ListaHistóricoEstadosDenunciaBien
        /// </summary>
        public List<HistoricoEstadosDenunciaBien> ListaHistoricoEstadosDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece ListaTrasladoDenuncia
        /// </summary>
        public List<TrasladoDenuncia> ListaTrasladoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece NombreMostrar
        /// </summary>
        public string NombreMostrar { get; set; }

        /// <summary>
        /// Obtiene o establece FechaEstado
        /// </summary>
        public DateTime FechaEstado { get; set; }
        public DesicionAuto desicionAuto { get; set; }
        public DecisionAutoInadmitida decisionAutoInadmitida { get; set; }
        public DecisionRecurso decisionRecurso { get; set; }
    }
}
