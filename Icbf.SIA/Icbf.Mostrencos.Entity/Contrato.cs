﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class Contrato
    {
        public int IdContrato { get; set; }
        public int IdNumeroProceso { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string NumeroContrato { get; set; }
        public float ValorFinalContrato { get; set; }
        public int IdRegionalContrato { get; set; }
        public int IdTipoContrato { get; set; }
        public int IdEstadoContrato { get; set; }
        public string ObjetoDelContrato { get; set; }
        public DateTime FechaInicioEjecucion { get; set; }
        public DateTime FechaFinalTerminacionContrato { get; set; }
        public DateTime FechaFinalizacionIniciaContrato { get; set; }
        public DateTime FechaSuscripcionContrato { get; set; }
        public float ValorInicialContrato { get; set; }
        public int IdVigenciaInicial { get; set; }
        public int IdVigenciaFinal { get; set; }
        public string IdModalidadAcademica { get; set; }
        public string IdProfesion { get; set; }
        public int IdModalidadSeleccion { get; set; }
        public int IdCategoriaContrato { get; set; }
        public int IdFormaPago { get; set; }
        public Boolean Suscrito { get; set; }
        public int IdEmpleadoSolicitante { get; set; }
        public int IdRegionalSolicitante { get; set; }
        public int IdDependenciaSolicitante { get; set; }
        public int IdCargoSolicitante { get; set; }
        public int IdUsuario { get; set; }
        public DateTime FechaAdjudicacionDelProceso { get; set; }
        public Boolean ActaDeInicio { get; set; }
        public Boolean ManejaAporte { get; set; }
        public Boolean ManejaRecurso { get; set; }
        public int IdRegimenContratacion { get; set; }
        public string AlcanceObjetoDelContrato { get; set; }
        public Boolean ManejaVigenciasFuturas { get; set; }
        public string DatosAdicionalesLugarEjecucion { get; set; }
        public int IdEmpleadoOrdenadorGasto { get; set; }
        public int IdRegionalOrdenador { get; set; }
        public int IdDependenciaOrdenador { get; set; }
        public int IdCargoOrdenador { get; set; }
        public int IdContratoAsociado { get; set; }
        public Boolean ConvenioMarco { get; set; }
        public Boolean ConvenioAdhesion { get; set; }
        public string ConsecutivoSuscrito { get; set; }
        public int IdUsuarioSuscribe { get; set; }
        public DateTime FechaLiquidacion { get; set; }
        public DateTime FechaTerminacionAnticipada { get; set; }
        public DateTime FechaActaInicio { get; set; }
        public DateTime FechaAnulacion { get; set; }
        public string UsuarioAsignado { get; set; }
        public int RequiereGarantia { get; set; }
        public int AportesEspecieICBF { get; set; }
        public double ValorAnticipo { get; set; }
        public double ValorPagoAnticipo { get; set; }
        public double ValoraPagoAnticipo { get; set; }
        public int IdTipoPago { get; set; }
        public double PorcentajeAnticipo { get; set; }
        public string NumeroContratoMigrado { get; set; }
        public int EsContSinValor { get; set; }
        public int IdCodigoSECOP { get; set; }
        public int IdRol { get; set; }
        public int EsFechaFinalCalculada { get; set; }
        public int DiasFechaFinalCalculada { get; set; }
        public int MesesFechaFinalCalculada { get; set; }
        public string VinculoSECOP { get; set; }
        public int IdSolicitudContrato { get; set; }
        public string NombreOrdenadorGasto { get; set; }
        public string RegionalOrdenadorGasto { get; set; }
        public string CargoOrdenadorGasto { get; set; }
        public string DependenciaOrdenadorGasto { get; set; }
        public string NombreSolicitante { get; set; }
        public string RegionalSolicitante { get; set; }
        public string CargoSolicitante { get; set; }
        public string DependenciaSolicitante { get; set; }
        public int FK_IdContrato { get; set; }
        public int ConsecutivoPlanCompasAsociado { get; set; }
        public int IdArea { get; set; }
        public int IdGeneracionArchivoObl { get; set; }
    }
}
