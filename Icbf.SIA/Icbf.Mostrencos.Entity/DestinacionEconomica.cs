﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad DestinacionEconomica con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class DestinacionEconomica : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdDestinacionEconomica
        /// </summary>
        public string IdDestinacionEconomica { get; set; }

        /// <summary>
        /// Obtiene o establece NombreDestinacionEconomica
        /// </summary>
        public string NombreDestinacionEconomica { get; set; }
    }
}
