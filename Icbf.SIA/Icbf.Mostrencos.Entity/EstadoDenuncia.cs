﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad EstadoDenuncia con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class EstadoDenuncia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdEstadoDenuncia
        /// </summary>
        public int IdEstadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece CódigoEstadoDenuncia
        /// </summary>
        public string CodigoEstadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece NombreEstadoDenuncia
        /// </summary>
        public string NombreEstadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si el Estado esta activo o no
        /// </summary>
        public bool Estado { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }
    }
}
