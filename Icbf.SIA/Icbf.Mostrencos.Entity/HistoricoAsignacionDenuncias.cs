﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Histórico Asignación Denuncias con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class HistoricoAsignacionDenuncias : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="HistoricoAsignacionDenuncias"/>.
        /// </summary>
        public HistoricoAsignacionDenuncias()
        {
            this.Abogados = new Abogados();
            this.DenunciaBien = new DenunciaBien();
        }

        /// <summary>
        /// Obtiene o establece IdHistóricoAsignaciónDenuncias
        /// </summary>
        public int IdHistoricoAsignacionDenuncias { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdAbogado
        /// </summary>
        public int IdAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece FechaAsignación
        /// </summary>
        public DateTime FechaAsignacion { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece Abogados
        /// </summary>
        public Abogados Abogados { get; set; }

        /// <summary>
        /// Obtiene o establece DenunciaBien
        /// </summary>
        public DenunciaBien DenunciaBien { get; set; }
    }
}
