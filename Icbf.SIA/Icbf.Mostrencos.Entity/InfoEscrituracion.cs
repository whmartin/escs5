﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class InfoEscrituracion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTramiteNotarial { get; set; }
        public int IdTipoTramite { get; set; }
        public int IdDecisionTramiteNotarialRechazado { get; set; }
        public int IdDecisionTramiteNotarial { get; set; }
        public int IdDenunciaBien { get; set; }
        public int IdNotaria { get; set; }
        public string TipoTramite { get; set; }
        public string Notaria { get; set; }
        public string DecisionTramiteNotarial { get; set; }
        public string DecisionTramiteNotarialRechazado { get; set; }
        public DateTime FechaActaTramiteNotarial { get; set; }
        public DateTime FechaEscritura { get; set; }
        public decimal NumeroEscritura { get; set; }
        public decimal NumeroRegistro { get; set; }
        public DateTime FechaIngresoICBF { get; set; }
        public DateTime FechaInstrumentosPublicos { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public DateTime FechaComunicacionSecretariaHacienda { get; set; }

        public DateTime FechaSolemnizacion { get; set; }
    }
}
