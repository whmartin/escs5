﻿
namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad EstadoFisicoBien con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class EstadoFisicoBien : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdEstadoFisicoBien
        /// </summary>
        public string IdEstadoFisicoBien { get; set; }

        /// <summary>
        /// Obtiene o establece NombreEstadoFisicoBien
        /// </summary>
        public string NombreEstadoFisicoBien { get; set; }
    }
}
