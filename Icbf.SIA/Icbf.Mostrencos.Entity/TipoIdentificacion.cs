//-----------------------------------------------------------------------
// <copyright file="TipoIdentificacion.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoIdentificacion.</summary>
// <author>INGENIAN SOFTWARE[Dar�o Belt�n]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad TipoIdentificacion con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    public class TipoIdentificacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoIdentificacion"/> class.
        /// </summary>
        public TipoIdentificacion()
        {
        }

        /// <summary>
        /// Gets or sets IdTipoIdentificacionPersonaNatural
        /// </summary>
        public int IdTipoIdentificacionPersonaNatural
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CodigoTipoIdentificacionPersonaNatural
        /// </summary>
        public string CodigoTipoIdentificacionPersonaNatural
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NombreTipoIdentificacionPersonaNatural
        /// </summary>
        public string NombreTipoIdentificacionPersonaNatural
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Estado
        /// </summary>
        /// <value>The Estado</value>
        public bool Estado
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}
