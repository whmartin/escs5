﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad TrasladoDenuncia con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TrasladoDenuncia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TrasladoDenuncia"/>.
        /// </summary>
        public TrasladoDenuncia()
        {
            this.EstadoDenuncia = new EstadoDenuncia();
            this.RegionalDestino = new Regional();
            this.RegionalOrigen = new Regional();
        }

        /// <summary>
        /// Obtiene o establece IdTrasladoDenuncia
        /// </summary>
        [Key]
        public int IdTrasladoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdEstadoTraslado
        /// </summary>
        public int IdEstadoTraslado { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroTraslado
        /// </summary>
        public int NumeroTraslado { get; set; }

        /// <summary>
        /// Obtiene o establece FechaTraslado
        /// </summary>
        public DateTime FechaTraslado { get; set; }

        /// <summary>
        /// Obtiene o establece MotivoTraslado
        /// </summary>
        public string MotivoTraslado { get; set; }

        /// <summary>
        /// Obtiene o establece IdRegionalOrigen
        /// </summary>
        public int IdRegionalOrigen { get; set; }

        /// <summary>
        /// Obtiene o establece IdRegionalTraslada
        /// </summary>
        public int IdRegionalTraslada { get; set; }

        /// <summary>
        /// Obtiene o establece DescripciónMotivo
        /// </summary>
        public string DescripcionMotivo { get; set; }

        /// <summary>
        /// Obtiene o establece SolicitudTrasladoAprobadoOrigen
        /// </summary>
        public string SolicitudTrasladoAprobadoOrigen { get; set; }

        /// <summary>
        /// Obtiene o establece SolicitudTrasladoAprobadoDestino
        /// </summary>
        public string SolicitudTrasladoAprobadoDestino { get; set; }

        /// <summary>
        /// Obtiene o establece ObservacionesOrigen
        /// </summary>
        public string ObservacionesOrigen { get; set; }

        /// <summary>
        /// Obtiene o establece ObservacionesDestino
        /// </summary>
        public string ObservacionesDestino { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoDenuncia
        /// </summary>
        public EstadoDenuncia EstadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece RegionalDestino
        /// </summary>
        public Regional RegionalDestino { get; set; }

        /// <summary>
        /// Obtiene o establece RegionalOrigen
        /// </summary>
        public Regional RegionalOrigen { get; set; }
    }
}
