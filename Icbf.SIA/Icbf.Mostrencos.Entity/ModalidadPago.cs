﻿//-----------------------------------------------------------------------
// <copyright file="ModalidadPago.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ModalidadPago.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>22/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;

namespace Icbf.Mostrencos.Entity
{
    [Serializable]
    public class ModalidadPago : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdModalidadPago
        /// </summary>
        [Key]
        public int IdModalidadPago
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece NombreModalidadPago
        /// </summary>
        public string NombreModalidadPago
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece Estado
        /// </summary>
        public bool Estado
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime? FechaCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime? FechaModifica
        {
            get;
            set;
        }
    }
}
