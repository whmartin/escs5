﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using System.Data.Common;
using System.Data;

namespace Icbf.Mostrencos.Entity
{
    public class ContratoParticipacionEconomica : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdContratoParticipacionEconomica { get; set; }
        public int IdContrato { get; set; }
        public int IdDenunciaBien { get; set; }
        public int Vigencia { get; set; }
        public int IDRegional { get; set; }
        public string NombreRegional { get; set; }
        public string NumeroContrato { get; set; }
        public DateTime FechaSuscripcion { get; set; }
        public DateTime FechaInicioEjecucion { get; set; }
        public int FechaInicialEjecucionDias { get; set; }
        public int FechaInicialEjecucionMeses { get; set; }
        public int FechaInicialEjecucionAnhios { get; set; }
        public DateTime FechaTerminacionInicial { get; set; }
        public DateTime FechaTerminacionFinal { get; set; }
        public int IDEstadoContrato { get; set; }
        public string EstadoContrato { get; set; }

        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }

    }
}
