﻿
namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Abogados con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TituloValor : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TituloValor"/>.
        /// </summary>
        public TituloValor()
        {
            this.TipoTitulo = new TipoTitulo();
        }

        /// <summary>
        /// Obtiene o establece IdTituloValor
        /// </summary>
        public int IdTituloValor { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoTitulo
        /// </summary>
        public int IdTipoTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoBien
        /// </summary>
        public string EstadoBien { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroCuentaBancaria
        /// </summary>
        public string NumeroCuentaBancaria { get; set; }

        /// <summary>
        /// Obtiene o establece EntidadBancaria
        /// </summary>
        public string EntidadBancaria { get; set; }

        /// <summary>
        /// Obtiene o establece TipoCuentaBancaria
        /// </summary>
        public string TipoCuentaBancaria { get; set; }

        /// <summary>
        /// Obtiene o establece IdDepartamento
        /// </summary>
        public string IdDepartamento { get; set; }

        /// <summary>
        /// Obtiene o establece IdMunicipio
        /// </summary>
        public string IdMunicipio { get; set; }

        /// <summary>
        /// Obtiene o establece ValirEfectivoEstimado
        /// </summary>
        public decimal ValorEfectivoEstimado { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroTituloValor
        /// </summary>
        public string NumeroTituloValor { get; set; }

        /// <summary>
        /// Obtiene o establece IdEntidadEmisora
        /// </summary>
        public int IdEntidadEmisora { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoIdentificacionEntidadEmisora
        /// </summary>
        public int IdTipoIdentificacionEntidadEmisora { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroIdentificacionBien
        /// </summary>
        public string NumeroIdentificacionBien { get; set; }

        /// <summary>
        /// Obtiene o establece CantidadTitulos
        /// </summary>
        public string CantidadTitulos { get; set; }

        /// <summary>
        /// Obtiene o establece CantidadVendida
        /// </summary>
        public string CantidadVendida { get; set; }

        /// <summary>
        /// Obtiene o establece ValorUnidadTitulo
        /// </summary>
        public decimal ValorUnidadTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece FechaVencimiento
        /// </summary>
        public DateTime? FechaVencimiento { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRecibido
        /// </summary>
        public DateTime? FechaRecibido { get; set; }

        /// <summary>
        /// Obtiene o establece FechaVenta
        /// </summary>
        public DateTime? FechaVenta { get; set; }

        /// <summary>
        /// Obtiene o establece ClaseEntrada
        /// </summary>
        public string ClaseEntrada { get; set; }

        /// <summary>
        /// Obtiene o establece FechaAdjudicado
        /// </summary>
        public DateTime? FechaAdjudicado { get; set; }

        /// <summary>
        /// Obtiene o establece DescripcionTitulo
        /// </summary>
        public string DescripcionTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece CodigoProducto
        /// </summary>
        public string CodigoProducto { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece TipoTitulo
        /// </summary>
        public TipoTitulo TipoTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece IdUsuarioCrea
        /// </summary>
        public int IdUsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsoBien
        /// </summary>
        public int? UsoBien { get; set; }

        /// <summary>
        /// Obtiene o establece  NombreUsoBien
        /// </summary>
        public string NombreUsoBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdInformacionVenta
        /// </summary>
        public int? IdInformacionVenta { get; set; }
    }
}
