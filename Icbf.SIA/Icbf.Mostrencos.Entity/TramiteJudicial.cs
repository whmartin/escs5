﻿//-----------------------------------------------------------------------
// <copyright file="TramiteJudicial.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TramiteJudicial.</summary>
// <author>INGENIAN</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad TramiteNotarial con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TramiteJudicial : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdTramiteJudicial
        /// </summary>
        public int IdTramiteJudicial { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoTramite
        /// </summary>
        public int IdTipoTramite { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
	    public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdDespachoJudicial
        /// </summary>
	    public int IdDespachoJudicial { get; set; }

        /// <summary>
        /// Obtiene o establece IdSentidoSentencia
        /// </summary>
        public int IdSentidoSentencia { get; set; }

        /// <summary>
        /// Obtiene o establece TipoTramite
        /// </summary>
        public string TipoTramite { get; set; }

        /// <summary>
        /// Obtiene o establece TipoTramite
        /// </summary>
        public string TipoProceso { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRadicacion
        /// </summary>
        public DateTime FechaRadicacion { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRadicacion
        /// </summary>
        public DateTime? FechaComunicacion { get; set; }

        /// <summary>
        /// Obtiene o establece DespachoJudicialAsignado
        /// </summary>
        public string DespachoJudicialAsignado { get; set; }

        /// <summary>
        /// Obtiene o establece Sentencia
        /// </summary>
        public string Sentencia { get; set; }

        /// <summary>
        /// Obtiene o establece FechaSentencia
        /// </summary>
        public DateTime FechaSentencia { get; set; }

        /// <summary>
        /// Obtiene o establece SentidoSentencia
        /// </summary>
        public string SentidoSentencia { get; set; }

        /// <summary>
        /// Obtiene o establece FechaConstanciaEjecutoria
        /// </summary>
        public DateTime FechaConstanciaEjecutoria { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
	    public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
	    public DateTime FechaModifica { get; set; }

    }
}