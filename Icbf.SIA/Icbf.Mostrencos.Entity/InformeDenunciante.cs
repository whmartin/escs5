﻿using System;

namespace Icbf.Mostrencos.Entity
{
    public class InformeDenunciante : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdInformeDenunciante { get; set; }
        public int IdDocumentoInforme { get; set; }
        public int IdDenunciaBien { get; set; }
        public DateTime FechaInforme { get; set; }
        public string DescripcionInforme { get; set; }
        public string DocumentoInforme { get; set; }
        public string NombreArchivo { get; set; }
        public string UsuarioCrea { get; set; }

        public int idUsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string RutaArchivo { get; set; }
    }
}
