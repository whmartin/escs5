using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    [Serializable]
    public class AsignacionAbogado : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdDenuncia
        {
            get;
            set;
        }
        public int IdAbogado
        {
            get;
            set;
        }
        public String CodigoAbogado
        {
            get;
            set;
        }
        public String NombreAbogado
        {
            get;
            set;
        }
        public int IdRegionalAbogado
        {
            get;
            set;
        }
        public String NombreRegionalAbogado
        {
            get;
            set;
        }
        public DateTime FechaAsignacionAbogado
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String Identificacion { get; set; }

        public String Profesion { get; set; }

        public int IdUsuario { get; set; }

        public int IdUsuarioCrea { get; set; }

        public string TipoFuncionario { get; set; }

        public string Correo { get; set; }

        public AsignacionAbogado()
        {
        }
    }
}
