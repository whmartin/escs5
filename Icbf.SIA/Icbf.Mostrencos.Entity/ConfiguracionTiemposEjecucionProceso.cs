﻿//-----------------------------------------------------------------------
// <copyright file="ConfiguracionTiemposEjecucionProceso.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ConfiguracionTiemposEjecucionProceso.</summary>
// <author>Ingenian Software</author>
// <date>09/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad ConfiguracionTiemposEjecucionProceso con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class ConfiguracionTiemposEjecucionProceso : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfiguracionTiemposEjecucionProceso"/> class.
        /// </summary>
        public ConfiguracionTiemposEjecucionProceso()
        {
        }

        /// <summary>
        /// Gets or sets IdConfiguracionTiemposEjecucionProceso.
        /// </summary>
        public int IdConfiguracionTiemposEjecucionProceso { get; set; }

        /// <summary>
        /// Gets or sets IdFase.
        /// </summary>
        public int IdFase { get; set; }

        /// <summary>
        /// Gets or sets IdActuacion.
        /// </summary>
        public int IdActuacion { get; set; }

        /// <summary>
        /// Gets or sets IdAccion.
        /// </summary>
        public int IdAccion { get; set; }

        /// <summary>
        /// Gets or sets Estado.
        /// </summary>
        public byte Estado { get; set; }

        /// <summary>
        /// Gets or sets CalculoDiasEjecucion.
        /// </summary>
        public byte CalculoDiasEjecucion { get; set; }

        /// <summary>
        /// Gets or sets TiempoEjecucioFaseActuacionAccion.
        /// </summary>
        public int TiempoEjecucioFaseActuacionAccion { get; set; }

        /// <summary>
        /// Gets or sets CalculoDiasAlerta.
        /// </summary>
        public byte? CalculoDiasAlerta { get; set; }

        /// <summary>
        /// Gets or sets TiempoEjecucionAlerta.
        /// </summary>
        public int? TiempoEjecucionAlerta { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea.
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea.
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica.
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets FechaModifica.
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Gets or sets Fase.
        /// </summary>
        public string Fase { get; set; }

        /// <summary>
        /// Gets or sets Actuacion.
        /// </summary>
        public string Actuacion { get; set; }

        /// <summary>
        /// Gets or sets Accion.
        /// </summary>
        public string Accion { get; set; }
    }
}
