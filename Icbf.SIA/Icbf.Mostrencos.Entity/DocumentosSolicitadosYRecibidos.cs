﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    [Serializable]
    public class DocumentosSolicitadosYRecibidos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase ModalidadPago
        /// </summary>
        public DocumentosSolicitadosYRecibidos()
        {
            this.OrdenesPago = new OrdenesPago();
        }

        public int IdDocumentosSolicitadosDenunciaBien { get; set; }
        public int IdDenunciaBien { get; set; }
        public int IdCausante { get; set; }
        public int IdApoderado { get; set; }
        public int IdContratoParticipacionEcnomica { get; set; }
        public int IdTipoDocumentoBienDenunciado { get; set; }
        public int IdTipoDocumentoSoporteDenuncia { get; set; }
        public string NombreTipoDocumento { get; set; }
        public string RutaArchivo { get; set; }
        public string DocumentacionCompleta { get; set; }
        public string ObservacionesDocumento { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string Estado { get; set; }
        public int TipoDocumento { get; set; }
        public string NomTipoDocumento { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public string ObservacionesDocumentacionSolicitada { get; set; }
        public int IdEstadoDocumento { get; set; }
        public string NombreEstadoDocumento { get; set; }
        public DateTime? FechaRecibido { get; set; }
        public string NombreArchivo { get; set; }
        public string ObservacionesDocumentacionRecibida { get; set; }
        public OrdenesPago OrdenesPago { get; set; }

        /// <summary>
        /// Obtiene o establece byts
        /// </summary>
        public byte[] bytes { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoDocumento
        /// </summary>
        public EstadoDocumento EstadoDocumento { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si ss nuevo el archivo solicitado
        /// </summary>
        public bool EsNuevo { get; set; }

        /// <summary>
        /// Obtiene o establece TipoDocumentoBienDenunciado
        /// </summary>
        public TipoDocumentoBienDenunciado TipoDocumentoBienDenunciado { get; set; }
    }
}
