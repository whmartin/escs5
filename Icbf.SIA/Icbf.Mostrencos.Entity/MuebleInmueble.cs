﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Abogados con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class MuebleInmueble : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdMuebleInmueble
        /// </summary>
        public int IdMuebleInmueble { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece TipoBien
        /// </summary>
        public string IdTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece SubTipoBien
        /// </summary>
        public string IdSubTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece ClaseBien
        /// </summary>
        public string IdClaseBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdDepartamento
        /// </summary>
        public string IdDepartamento { get; set; }

        /// <summary>
        /// Obtiene o establece IdMunicipio
        /// </summary>
        public string IdMunicipio { get; set; }

        /// <summary>
        /// Obtiene o establece ClaseEntrada
        /// </summary>
        public string ClaseEntrada { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoFisicoBien
        /// </summary>
        public string EstadoFisicoBien { get; set; }

        /// <summary>
        /// Obtiene o establece ValorEstimadoComercial
        /// </summary>
        public decimal ValorEstimadoComercial { get; set; }

        /// <summary>
        /// Obtiene o establece DestinacionEconomica
        /// </summary>
        public string DestinacionEconomica { get; set; }

        /// <summary>
        /// Obtiene o establece PorcentajePertenenciaBien
        /// </summary>
        public string PorcentajePertenenciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece MatriculaInmobiliaria
        /// </summary>
        public string MatriculaInmobiliaria { get; set; }

        /// <summary>
        /// Obtiene o establece CedulaCatastral
        /// </summary>
        public string CedulaCatastral { get; set; }

        /// <summary>
        /// Obtiene o establece DireccionInmueble
        /// </summary>
        public string DireccionInmueble { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroIdentificacionInmueble
        /// </summary>
        public string NumeroIdentificacionInmueble { get; set; }

        /// <summary>
        /// Obtiene o establece MarcaBien
        /// </summary>
        public string MarcaBien { get; set; }

        /// <summary>
        /// Obtiene o establece FechaVenta
        /// </summary>
        public DateTime? FechaVenta { get; set; }

        /// <summary>
        /// Obtiene o establece FechaVenta
        /// </summary>
        public DateTime? FechaAdjudicado { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRegistro
        /// </summary>
        public DateTime? FechaRegistro { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoBien
        /// </summary>
        public string EstadoBien { get; set; }

        /// <summary>
        /// Obtiene o establece DescripcionBien
        /// </summary>
        public string DescripcionBien { get; set; }

        /// <summary>
        /// Obtiene o establece TipoParametro
        /// </summary>
        public string TipoParametro { get; set; }

        /// <summary>
        /// Obtiene o establece CodigoParametros
        /// </summary>
        public string CodigoParametros { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece IdUsuarioCrea
        /// </summary>
        public int IdUsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  NombreTipoBien
        /// </summary>
        public string NombreTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece  NombreSubTipoBien
        /// </summary>
        public string NombreSubTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece  NombreClaseBien
        /// </summary>
        public string NombreClaseBien { get; set; }

        /// <summary>
        /// Obtiene o establece UsoBien esta "Venta - 1" o "Uso Propio - 0"
        /// </summary>
        public int? UsoBien { get; set; }

        /// <summary>
        /// Obtiene o establece  NombreUsoBien
        /// </summary>
        public string NombreUsoBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdInformacionVenta
        /// </summary>
        public int? IdInformacionVenta { get; set; }
    }
}
