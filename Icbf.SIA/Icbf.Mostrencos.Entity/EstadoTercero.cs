﻿
namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad EstadoTercero con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class EstadoTercero : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="EstadoTercero"/>.
        /// </summary>
        public EstadoTercero()
        {
            this.ListaTerceros = new List<Tercero>();
        }

        /// <summary>
        /// Obtiene o establece IdEstadoTercero
        /// </summary>
        [Key]
        public int IdEstadoTercero { get; set; }

        /// <summary>
        /// Obtiene o establece Código Estado Tercero
        /// </summary>
        public string CodigoEstadotercero { get; set; }

        /// <summary>
        /// Obtiene o establece DescripciónEstado
        /// </summary>
        public string DescripcionEstado { get; set; }

        /// <summary>
        /// Obtiene o establece Estado
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Obtiene o establece Código PCI
        /// </summary>
        public string CodPCI { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece ListaTerceros
        /// </summary>
        public List<Tercero> ListaTerceros { get; set; }        
    }
}
