﻿//-----------------------------------------------------------------------
// <copyright file="TramiteNotarial.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TramiteNotarial.</summary>
// <author>INGENIAN</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad TramiteNotarial con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TramiteNotarial : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdTramiteNotarial
        /// </summary>
        public int IdTramiteNotarial { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoTramite
        /// </summary>
        public int IdTipoTramite { get; set; }

        /// <summary>
        /// Obtiene o establece IdDecisionTramiteNotarialRechazado
        /// </summary>
        public int IdDecisionTramiteNotarialRechazado { get; set; }

        /// <summary>
        /// Obtiene o establece IdDecisionTramiteNotarial
        /// </summary>
        public int IdDecisionTramiteNotarial { get; set; }

        /// <summary>
        /// Obtiene o establece IdDenunciaBien
        /// </summary>
        public int IdDenunciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece IdNotaria
        /// </summary>
        public string IdNotaria { get; set; }

        /// <summary>
        /// Obtiene o establece TipoTramite
        /// </summary>
	    public string TipoTramite { get; set; }

        /// <summary>
        /// Obtiene o establece Notaria
        /// </summary>
        public string Notaria { get; set; }

        /// <summary>
        /// Obtiene o establece DecisionTramiteNotarial
        /// </summary>
        public string DecisionTramiteNotarial { get; set; }

        /// <summary>
        /// Obtiene o establece DecisionTramiteNotarialRechazado
        /// </summary>
        public string DecisionTramiteNotarialRechazado { get; set; }

        /// <summary>
        /// Obtiene o establece FechaActaTramiteNotarial
        /// </summary>
        public DateTime FechaActaTramiteNotarial { get; set; }

        /// <summary>
        /// Obtiene o establece FechaEscritura
        /// </summary>
        public DateTime FechaEscritura { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroEscritura
        /// </summary>
        public decimal NumeroEscritura { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroRegistro
        /// </summary>
        public decimal NumeroRegistro { get; set; }

        /// <summary>
        /// Obtiene o establece FechaIngresoICBF
        /// </summary>
        public DateTime FechaIngresoICBF { get; set; }

        /// <summary>
        /// Obtiene o establece FechaInstrumentosPublicos
        /// </summary>
        public DateTime FechaInstrumentosPublicos { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }


        /// <summary>
        /// Obtiene o establece FechaComunicacion
        /// </summary>
        public DateTime FechaComunicacion { get; set; }
    }
}