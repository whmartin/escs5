﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    //Entidad que obtiene el valor de la denuncia entre las entidades DenunciaMueble y DenunciaTitulo
    public class DenunciaMuebleTitulo
    {
        public int IdBienDenuncia { get; set; }
        public decimal ValorDenuncia { get; set; }
    }
}
