﻿//-----------------------------------------------------------------------
// <copyright file="resultFiltroDenunciante.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase resultFiltroDenunciante.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad Regional con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class ResultFiltroTitulo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdTituloValor
        /// </summary>
        public int IdTituloValor { get; set; }

        /// <summary>
        /// Obtiene o establece radicadoDenuncia
        /// </summary>
        public string radicadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece NombRazonSocial
        /// </summary>
        public string NombRazonSocial { get; set; }

        /// <summary>
        /// Obtiene o establece fechaRadicado
        /// </summary>
        public DateTime fechaRadicado { get; set; }

        /// <summary>
        /// Obtiene o establece tipoTitulo
        /// </summary>
        public string tipoTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece cuentaBancaria
        /// </summary>
        public string cuentaBancaria { get; set; }

        /// <summary>
        /// Obtiene o establece nroTituloValor
        /// </summary>
        public string nroTituloValor { get; set; }

        /// <summary>
        /// Obtiene o establece observaciones
        /// </summary>
        public string observaciones { get; set; }

        /// <summary>
        /// Obtiene o establece nombreTitulo
        /// </summary>
        public string nombreTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece ValorEfectivoEstimado
        /// </summary>
        public string ValorEfectivoEstimado { get; set; }

        /// <summary>
        /// Obtiene o establece NumeroIdentificacionBien
        /// </summary>
        public string NumeroIdentificacionBien { get; set; }

        /// <summary>
        /// Obtiene o establece CantidadTitulos
        /// </summary>
        public string CantidadTitulos { get; set; }

        /// <summary>
        /// Obtiene o establece ValorUnitarioTitulo
        /// </summary>
        public string ValorUnitarioTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece FechaVencimiento
        /// </summary>
        public DateTime FechaVencimiento { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRecibido
        /// </summary>
        public DateTime FechaRecibido { get; set; }

        /// <summary>
        /// Obtiene o establece FechaVenta
        /// </summary>
        public DateTime FechaVenta { get; set; }

        /// <summary>
        /// Obtiene o establece CantidadVendida
        /// </summary>
        public string CantidadVendida { get; set; }

        /// <summary>
        /// Obtiene o establece ClaseEntrada
        /// </summary>
        public string ClaseEntrada { get; set; }

        /// <summary>
        /// Obtiene o establece DescripcionTitulo
        /// </summary>
        public string DescripcionTitulo { get; set; }

        /// <summary>
        /// Obtiene o establece FechaAdjudicado
        /// </summary>
        public DateTime FechaAdjudicado { get; set; }

        /// <summary>
        /// Obtiene o establece estado
        /// </summary>
        public string estado { get; set; }

        /// <summary>
        /// Obtiene o establece EntidadBancaria
        /// </summary>
        public string EntidadBancaria { get; set; }

        /// <summary>
        /// Obtiene o establece TipoCuentaBancaria
        /// </summary>
        public string TipoCuentaBancaria { get; set; }

        /// <summary>
        /// Obtiene o establece Departamento
        /// </summary>
        public string Departamento { get; set; }

        /// <summary>
        /// Obtiene o establece Municipio
        /// </summary>
        public string Municipio { get; set; }

        /// <summary>
        /// Obtiene o establece EntidadEmisora
        /// </summary>
        public string EntidadEmisora { get; set; }

        /// <summary>
        /// Obtiene o establece TipoIdentificacionEntidadEmisora
        /// </summary>
        public string TipoIdentificacionEntidadEmisora { get; set; }

        /// <summary>
        /// Obtiene o establece FechaDenuncia
        /// </summary>
        public DateTime FechaDenuncia { get; set; }

        public DateTime? FechaAdjudicados { get; set; }

        public bool Modificado { get; set; }
    }
}