﻿//-----------------------------------------------------------------------
// <copyright file="resultFiltroDenunciante.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase resultFiltroDenunciante.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad Regional con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class ResultFiltroMueble : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Obtiene o establece radicadoDenuncia
        /// </summary>
        public string radicadoDenuncia { get; set; }

        /// <summary>
        /// Obtiene o establece NombRazonSocial
        /// </summary>
        public string NombRazonSocial { get; set; }

        /// <summary>
        /// Obtiene o establece fechaRadicado
        /// </summary>
        public DateTime fechaRadicado { get; set; }

        /// <summary>
        /// Obtiene o establece fechaDenuncia
        /// </summary>
        public DateTime FechaRadicadoCorrespondencia { get; set; }

        /// <summary>
        /// Obtiene o establece tipoBien
        /// </summary>
        public string tipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece subTipoBien
        /// </summary>
        public string subTipoBien { get; set; }

        /// <summary>
        /// Obtiene o establece ClaseBien
        /// </summary>
        public string ClaseBien { get; set; }

        /// <summary>
        /// Obtiene o establece matriculaInmobiliaria
        /// </summary>
        public string matriculaInmobiliaria { get; set; }

        /// <summary>
        /// Obtiene o establece cedulaCatastral
        /// </summary>
        public string cedulaCatastral { get; set; }

        /// <summary>
        /// Obtiene o establece nroIdentifica
        /// </summary>
        public string nroIdentifica { get; set; }

        /// <summary>
        /// Obtiene o establece observaciones
        /// </summary>
        public string observaciones { get; set; }

        /// <summary>
        /// Obtiene o establece DepartamentoSeven
        /// </summary>
        public string DepartamentoSeven { get; set; }

        /// <summary>
        /// Obtiene o establece MunicipioSeven
        /// </summary>
        public string MunicipioSeven { get; set; }

        /// <summary>
        /// Obtiene o establece EstadoFisicoBien
        /// </summary>
        public string EstadoFisicoBien { get; set; }

        /// <summary>
        /// Obtiene o establece ValorEstimadoComercial
        /// </summary>
        public string ValorEstimadoComercial { get; set; }

        /// <summary>
        /// Obtiene o establece PorcentajePertenenciaBien
        /// </summary>
        public string PorcentajePertenenciaBien { get; set; }

        /// <summary>
        /// Obtiene o establece DestinacionEconomica
        /// </summary>
        public string DestinacionEconomica { get; set; }

        /// <summary>
        /// Obtiene o establece DireccionInmueble
        /// </summary>
        public string DireccionInmueble { get; set; }

        /// <summary>
        /// Obtiene o establece MarcaBien
        /// </summary>
        public string MarcaBien { get; set; }

        /// <summary>
        /// Obtiene o establece FechaRegistro
        /// </summary>
        public DateTime FechaRegistro { get; set; }

        /// <summary>
        /// Obtiene o establece DescripcionBien
        /// </summary>
        public string DescripcionBien { get; set; }

        /// <summary>
        /// Obtiene o establece FechaVenta
        /// </summary>
        public DateTime FechaVenta { get; set; }

        /// <summary>
        /// Obtiene o establece ClaseEntrada
        /// </summary>
        public string ClaseEntrada { get; set; }

        /// <summary>
        /// Obtiene o establece FechaAdjudicado
        /// </summary>
        public DateTime FechaAdjudicado { get; set; }

        /// <summary>
        /// Obtiene o establece estado
        /// </summary>
        public string estado { get; set; }

        /// <summary>
        /// Obtiene o establece IdMuebleInmueble
        /// </summary>
        public int IdMuebleInmueble { get; set; }

        /// <summary>
        /// Obtiene o establece FechaDenuncia
        /// </summary>
        public DateTime FechaDenuncia { get; set; }
    }
}