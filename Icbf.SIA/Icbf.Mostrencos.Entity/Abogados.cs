﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad Abogados con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class Abogados : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="Abogados"/>.
        /// </summary>
        public Abogados()
        {
            this.ListaDenuncias = new List<DenunciaBien>();
        }

        /// <summary>
        /// Obtiene o establece IdAbogado
        /// </summary>
        [Key]
        public int IdAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece Identificación
        /// </summary>
        public long Identificacion { get; set; }

        /// <summary>
        /// Obtiene o establece NombreAbogado
        /// </summary>
        public string NombreAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece IdRegionalAbogado
        /// </summary>
        public int IdRegionalAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece NombreRegionalAbogado
        /// </summary>
        public string NombreRegionalAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece FechaAsignaciónAbogado
        /// </summary>
        public DateTime? FechaAsignacionAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece TipoFuncionarioAbogado
        /// </summary>
        public string TipoFuncionarioAbogado { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si Estado es activo o no
        /// </summary>
        public bool Estado { get; set; }

        /// <summary>
        /// Obtiene o establece IdUsuario
        /// </summary>
        public int IdUsuario { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece  UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Obtiene o establece vListaDenuncias
        /// </summary>
        public List<DenunciaBien> ListaDenuncias { get; set; }        
    }
}
