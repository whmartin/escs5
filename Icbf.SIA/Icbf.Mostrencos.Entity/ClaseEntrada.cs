﻿

namespace Icbf.Mostrencos.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Clase Entidad ClaseEntrada con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class ClaseEntrada : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdClaseEntrada
        /// </summary>
        public string IdClaseEntrada { get; set; }

        /// <summary>
        /// Obtiene o establece NombreClaseEntrada
        /// </summary>
        public string NombreClaseEntrada { get; set; }
    }
}
