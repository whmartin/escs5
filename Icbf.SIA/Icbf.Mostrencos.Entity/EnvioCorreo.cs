﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class EnvioCorreo
    {
        public int IdDenunciaBien { get; set; }
        public int IdRegionalUbicacion { get; set; }
        public string NombreRegional { get; set; }
        public string NombreEstadoDenuncia { get; set; }
        public string Fase { get; set; }
        public string Accion { get; set; }
        public string Actuacion { get; set; }
        public DateTime FechaInforme { get; set; }
        public DateTime FechaCrea { get; set; }
        public string RadicadoDenuncia { get; set; }
        public string CorreoAbogado { get; set; }
        public string CorreoDenunciante { get; set; }
        public string CorreoCoordinador { get; set; }
    }
}
