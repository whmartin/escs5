﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase para transportar filtros del Reporte de Denuncias
    /// </summary>
    public class ReporteDenunciaFiltros
    {
        /// <summary>
        /// 
        /// </summary>
        public string TipoPersonaAsociacion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? TipoIdentificacion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NumeroIdentificacion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DepartamentoUbicacion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Estado { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime RegistradoDesde { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime RegistradoHasta { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ClaseDenuncia { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? Fase { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DecisionReconociemiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? TipoTramite { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? DecisionTramiteNot { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? ResultadoDesicionTramiteNot { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? TerminacionAnormalTramiteNot { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DecisionAuto { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? ResultadoDecisionAuto { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SentidoSentencia { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? ModalidadPago { get; set; }
    }
}
