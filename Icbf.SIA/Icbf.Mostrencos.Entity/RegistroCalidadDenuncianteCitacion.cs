﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    /// <summary>
    /// Clase Entidad RegistroDenuncia con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class RegistroCalidadDenuncianteCitacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Gets or sets IdDenunciaBien. Propiedad PK autonumérica
        /// </summary>
        /// <value>El IdDenunciaBien</value>
        public string ReconocimientoCalidadDenunciante
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdTercero
        /// </summary>
        public int NumeroResolucion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdResolucion
        /// </summary>
        public int IdResolucion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FechaRadicadoCorrespondencia
        /// </summary>
        public DateTime FechaResolucion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HoraRadicadoCorrespondencia
        /// </summary>
        public string Observaciones
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HoraRadicadoCorrespondencia
        /// </summary>
        public string SeNotifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HoraRadicadoCorrespondencia
        /// </summary>
        public DateTime FechaNotificacion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HoraRadicadoCorrespondencia
        /// </summary>
        public DateTime FechaAviso
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HoraRadicadoCorrespondencia
        /// </summary>
        public string ObservacionCitacion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IdDenunciaBien. Propiedad PK autonumérica
        /// </summary>
        /// <value>El IdReconocimientoCalidadDenuncianteDetalle</value>
        public string NombreDetalle
        {
            get;
            set;
        }
    }
}
