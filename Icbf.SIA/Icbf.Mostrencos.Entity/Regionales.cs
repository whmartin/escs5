using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Entity
{
    public class Regionales : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRegional
        {
            get;
            set;
        }
        public String CodigoRegional
        {
            get;
            set;
        }
        public String NombreRegional
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Regionales()
        {
        }
    }
}
