﻿//-----------------------------------------------------------------------
// <copyright file="TramiteNotarial.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TramiteNotarial.</summary>
// <author>INGENIAN</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad TramiteNotarial con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class TramiteNotarialAceptado : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece IdTramiteNotarial
        /// </summary>
        public int IdTramiteNotarialAceptado { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoTramite
        /// </summary>
        public int IdTipoTramiteNotarial { get; set; }

        /// <summary>
        /// Obtiene o establece IdTipoTramite
        /// </summary>
        public int IdTramiteNotarial { get; set; }

        /// <summary>
        /// Obtiene o establece IdDecisionTramiteNotarialRechazado
        /// </summary>
        public int IdTerminacionAnormalTramiteNotarial { get; set; }

        /// <summary>
        /// Obtiene o establece FechaActaTramiteNotarial
        /// </summary>
        public DateTime FechaEdictoEmplazatorio { get; set; }

        /// <summary>
        /// Obtiene o establece FechaEscritura
        /// </summary>
        public DateTime FechaComunicacionScretariaHacienda { get; set; }

        /// <summary>
        /// Obtiene o establece FechaEscritura
        /// </summary>
        public DateTime FechaSolemnizacion { get; set; }
        /// <summary>
        /// Obtiene o establece FechaEscritura
        /// </summary>
        public DateTime FechaPublicacionEdicto { get; set; }
        /// <summary>
        /// Obtiene o establece FechaEscritura
        /// </summary>
        public DateTime FechaComunicacionDian { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string TerminacionAnormalTramiteNotarial { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Obtiene o establece FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Obtiene o establece UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Obtiene o establece FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }
    }
}