﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using EnvioCorreoService;
using Quartz;

/// <summary>
/// Descripción breve de EmailJob
/// </summary>
public class EmailJob : IJob
{
    protected Service1 service_event = new Service1();

    private string URI = ConfigurationManager.AppSettings["URI"];
    private string URL = ConfigurationManager.AppSettings["URL"];

    /// <summary>
    /// Método que ejecuta el envío de correo.
    /// </summary>
    /// <param name="context"></param>
    public void Execute(IJobExecutionContext context)
    {
        this.CallWebApiCorreo();
    }

    /// <summary>
    /// Método que llama la Web Api de Mostrencos para enviar los correos
    /// </summary>
    public void CallWebApiCorreo()
    {
        HttpClient client = new HttpClient();
        client.BaseAddress = new Uri(URI);

        // Add an Accept header for JSON format.
        client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

        //HttpResponseMessage response = client.GetAsync("api/enviocorreo/enviarcorreos").Result;
        HttpResponseMessage response = client.GetAsync(URL).Result;

        if (response.IsSuccessStatusCode)
        {
            service_event.EventLog.WriteEntry("Ejecución exitosa del servicio");
        }
        else
        {
            service_event.EventLog.WriteEntry("Ocurrió un error en la ejecución del servicio");
        }
    }
}