﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace EnvioCorreoService
{
    public partial class Service1 : ServiceBase
    {
        /// <summary>
        /// Indica la cantidad de minutos de la ejecución del servicio
        /// </summary>
        private int MinutosEjecucion = int.Parse(ConfigurationManager.AppSettings["MinutosEjecucion"]);

        /// <summary>
        /// Intervalo de ejecución
        /// </summary>
        private Timer IntervaloEjecucion = new Timer();

        public Service1()
        {
            InitializeComponent();

            this.CanStop = true;
            this.CanPauseAndContinue = true;

            //Setup logging
            this.AutoLog = false;

            ((ISupportInitialize)this.EventLog).BeginInit();
            if (!System.Diagnostics.EventLog.SourceExists(this.ServiceName))
            {
                System.Diagnostics.EventLog.CreateEventSource(this.ServiceName, "Application");
            }
        ((ISupportInitialize)this.EventLog).EndInit();

            this.EventLog.Source = this.ServiceName;
            this.EventLog.Log = "Application";
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                this.EventLog.WriteEntry("Iniciando servicio");
                JobScheduler.Start();
            }
            catch (Exception)
            {
                this.EventLog.WriteEntry("Excepción ejecutando el servicio");
            }
        }

        public void OnStartTest()
        {
            JobScheduler.Start();
        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("Deteniendo Servicio");
        }


        //protected override void OnStart(string[] args)
        //{
        //    this.IntervaloEjecucion.Elapsed += new ElapsedEventHandler(this.IntervaloEjecucion_Transcurrido);
        //    this.IntervaloEjecucion.Interval = 1000 * 60 * this.MinutosEjecucion;
        //    this.IntervaloEjecucion.Enabled = true;
        //    this.IntervaloEjecucion.Start();
        //}


        //protected override void OnStop()
        //{
        //    this.IntervaloEjecucion.Enabled = false;
        //    this.IntervaloEjecucion.Stop();
        //    this.IntervaloEjecucion.Dispose();
        //}

        ///// <summary>
        ///// Método que implementa la ejecución de la depuración
        ///// </summary>
        ///// <param name="sender">Objeto sender</param>
        ///// <param name="e">ElapsedEventArgs e</param>
        //private void IntervaloEjecucion_Transcurrido(object sender, ElapsedEventArgs e)
        //{
        //    JobScheduler.Start();
        //}
    }
}
