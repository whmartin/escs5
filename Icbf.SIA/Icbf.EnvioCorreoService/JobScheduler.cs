﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;

/// <summary>
/// Scheduler que permite ejecutar la tarea de llamado al WebApi de acuerdo a las fechas programadas.
/// </summary>
public class JobScheduler
{
    public static void Start()
    {
        IJobDetail emailJob = JobBuilder.Create<EmailJob>()
              .WithIdentity("job1")
              .Build();

        ITrigger trigger1 = TriggerBuilder.Create()
            .WithIdentity("trigger1")
            .WithCronSchedule(ConfigurationManager.AppSettings["CONFIGENVIO"])
            .ForJob(emailJob)
            .Build();

        ITrigger trigger2 = TriggerBuilder.Create()
            .WithIdentity("trigger2")
            .WithCronSchedule(ConfigurationManager.AppSettings["CONFIGENVIOTIEMPO"])
            .ForJob(emailJob)
            .Build();
        
        ISchedulerFactory sf = new StdSchedulerFactory();
        IScheduler sc = sf.GetScheduler();

        sc.AddJob(emailJob, true, true);
        sc.ScheduleJob(trigger1);
        sc.ScheduleJob(trigger2);
        sc.Start();
    }
}