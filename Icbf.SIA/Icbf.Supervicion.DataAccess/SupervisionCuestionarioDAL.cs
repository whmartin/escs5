using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionCuestionarioDAL : GeneralDAL
    {
        public SupervisionCuestionarioDAL()
        {
        }
        public int InsertarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Cuestionarios_PoblarDatos"))
                {
                    int vResultado;
                    String MensajeErr = "";
                    System.Data.SqlClient.SqlParameter tvpParam = new System.Data.SqlClient.SqlParameter();
                    tvpParam.SqlDbType = SqlDbType.Structured;
                    tvpParam.TypeName = "dbo.IdPreguntasSupervision";
                    tvpParam.ParameterName = "@t_Preguntas";
                    tvpParam.Value = pSupervisionCuestionario.Preguntas;

                    if (pSupervisionCuestionario.IdCuestionario == 0)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdCuestionarioRef", DbType.Int32, DBNull.Value);//Se envia cuando se modfica el cuestionario 
                    }
                    else { vDataBase.AddInParameter(vDbCommand, "@IdCuestionarioRef", DbType.Int32, pSupervisionCuestionario.IdCuestionario); }

                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionCuestionario.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaServicio", DbType.Int32, pSupervisionCuestionario.IdVigenciaServicio);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadServicio", DbType.Int32, pSupervisionCuestionario.IdModalidadServicio);
                    if (pSupervisionCuestionario.IdSubComponente == -1)
                    { vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, DBNull.Value); }
                    else
                    { vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pSupervisionCuestionario.IdSubComponente); }

                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pSupervisionCuestionario.IdComponente);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionCuestionario.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionCuestionario.EstadoPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionCuestionario.UsuarioCrea);
                    vDbCommand.Parameters.Add(tvpParam);
                    vDataBase.AddOutParameter(vDbCommand, "@IdCuestionario", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@MensajeError", DbType.String, 500);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionCuestionario.IdCuestionario = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCuestionario").ToString());
                    MensajeErr = vDataBase.GetParameterValue(vDbCommand, "@MensajeError").ToString();
                    GenerarLogAuditoria(pSupervisionCuestionario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionCuestionario_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCuestionario", DbType.Int32, pSupervisionCuestionario.IdCuestionario);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionCuestionario.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionCuestionario.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaServicio", DbType.Int32, pSupervisionCuestionario.IdVigenciaServicio);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadServicio", DbType.Int32, pSupervisionCuestionario.IdModalidadServicio);
                    vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pSupervisionCuestionario.IdSubComponente);
                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pSupervisionCuestionario.IdComponente);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionCuestionario.EstadoPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionCuestionario.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionCuestionario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionCuestionario_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCuestionario", DbType.Int32, pSupervisionCuestionario.IdCuestionario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionCuestionario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public SupervisionCuestionario ConsultarSupervisionCuestionario(int pIdCuestionario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Cuestionarios_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCuestionario", DbType.Int32, pIdCuestionario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionCuestionario vSupervisionCuestionario = new SupervisionCuestionario();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionCuestionario.IdCuestionario = vDataReaderResults["IdCuestionario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCuestionario"].ToString()) : vSupervisionCuestionario.IdCuestionario;
                            vSupervisionCuestionario.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionCuestionario.Nombre;
                            vSupervisionCuestionario.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionCuestionario.IdDireccion;
                            vSupervisionCuestionario.IdVigenciaServicio = vDataReaderResults["IdVigenciaServicio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaServicio"].ToString()) : vSupervisionCuestionario.IdVigenciaServicio;
                            vSupervisionCuestionario.IdModalidadServicio = vDataReaderResults["IdModalidadServicio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadServicio"].ToString()) : vSupervisionCuestionario.IdModalidadServicio;
                            vSupervisionCuestionario.IdSubComponente = vDataReaderResults["IdSubComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubComponente"].ToString()) : vSupervisionCuestionario.IdSubComponente;
                            vSupervisionCuestionario.IdComponente = vDataReaderResults["IdComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComponente"].ToString()) : vSupervisionCuestionario.IdComponente;
                            vSupervisionCuestionario.EstadoPregunta = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionCuestionario.EstadoPregunta;
                            vSupervisionCuestionario.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionCuestionario.UsuarioCrea;
                            vSupervisionCuestionario.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionCuestionario.FechaCrea;
                            vSupervisionCuestionario.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionCuestionario.UsuarioModifica;
                            vSupervisionCuestionario.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionCuestionario.FechaModifica;
                        }
                        return vSupervisionCuestionario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCuestionario> ConsultarSupervisionCuestionarios(String pNombre, int? pIdDireccion, int? pIdVigenciaServicio, int? pIdModalidadServicio, int? pIdSubComponente, int? pIdComponente, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Cuestionarios_Consultar"))
                {
                    if (pNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pIdDireccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pIdDireccion);
                    if (pIdVigenciaServicio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigenciaServicio", DbType.Int32, pIdVigenciaServicio);
                    if (pIdModalidadServicio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadServicio", DbType.Int32, pIdModalidadServicio);
                    if (pIdSubComponente != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pIdSubComponente);
                    if (pIdComponente != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pIdComponente);

                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionCuestionario> vListaSupervisionCuestionario = new List<SupervisionCuestionario>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionCuestionario vSupervisionCuestionario = new SupervisionCuestionario();
                            vSupervisionCuestionario.IdCuestionario = vDataReaderResults["IdCuestionario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCuestionario"].ToString()) : vSupervisionCuestionario.IdCuestionario;
                            vSupervisionCuestionario.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionCuestionario.Nombre;
                            vSupervisionCuestionario.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionCuestionario.IdDireccion;
                            vSupervisionCuestionario.Direccion = vDataReaderResults["NombreDireccion"] != DBNull.Value ? vDataReaderResults["NombreDireccion"].ToString() : vSupervisionCuestionario.Direccion;
                            vSupervisionCuestionario.IdVigenciaServicio = vDataReaderResults["IdVigenciaServicio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaServicio"].ToString()) : vSupervisionCuestionario.IdVigenciaServicio;
                            vSupervisionCuestionario.IdModalidadServicio = vDataReaderResults["IdModalidadServicio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadServicio"].ToString()) : vSupervisionCuestionario.IdModalidadServicio;
                            vSupervisionCuestionario.IdSubComponente = vDataReaderResults["IdSubComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubComponente"].ToString()) : vSupervisionCuestionario.IdSubComponente;
                            vSupervisionCuestionario.IdComponente = vDataReaderResults["IdComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComponente"].ToString()) : vSupervisionCuestionario.IdComponente;
                            vSupervisionCuestionario.EstadoPregunta = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionCuestionario.EstadoPregunta;
                            vSupervisionCuestionario.EstadoPreguntaStr = vDataReaderResults["NombreEstado"] != DBNull.Value ? vDataReaderResults["NombreEstado"].ToString() : vSupervisionCuestionario.EstadoPreguntaStr;
                            vSupervisionCuestionario.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionCuestionario.UsuarioCrea;
                            vSupervisionCuestionario.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionCuestionario.FechaCrea;
                            vSupervisionCuestionario.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionCuestionario.UsuarioModifica;
                            vSupervisionCuestionario.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionCuestionario.FechaModifica;

                            vListaSupervisionCuestionario.Add(vSupervisionCuestionario);
                        }
                        return vListaSupervisionCuestionario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCuestionario> ConsultarSupervisionCuestionarioPreguntas(int? pIdCuestionario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_CuestionariosPreguntas_Consultar"))
                {
                    if (pIdCuestionario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCuestionario", DbType.String, pIdCuestionario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionCuestionario> vListaSupervisionCuestionario = new List<SupervisionCuestionario>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionCuestionario vSupervisionCuestionario = new SupervisionCuestionario();
                            vSupervisionCuestionario.IdCuestionario = vDataReaderResults["IdCuestionario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCuestionario"].ToString()) : vSupervisionCuestionario.IdCuestionario;
                            vSupervisionCuestionario.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"]) : vSupervisionCuestionario.IdPregunta;
                            vSupervisionCuestionario.NombrePregunta = vDataReaderResults["NombrePregunta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePregunta"].ToString()) : vSupervisionCuestionario.NombrePregunta;
                            vSupervisionCuestionario.EstadoPregunta = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionCuestionario.EstadoPregunta;
                            vSupervisionCuestionario.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionCuestionario.UsuarioModifica;
                            vSupervisionCuestionario.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionCuestionario.FechaModifica;
                            vListaSupervisionCuestionario.Add(vSupervisionCuestionario);
                        }
                        return vListaSupervisionCuestionario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
