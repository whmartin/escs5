using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionGruposApoyoIntegrantesDAL : GeneralDAL
    {
        public SupervisionGruposApoyoIntegrantesDAL()
        {
        }
        public int InsertarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoIntegrantes_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdIntegranteGrupo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyoIntegrantes.IdGrupo);
                    //vDataBase.AddInParameter(vDbCommand, "@Grupo", DbType.String, pSupervisionGruposApoyoIntegrantes.Grupo);
                    //vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionGruposApoyoIntegrantes.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pSupervisionGruposApoyoIntegrantes.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Nombres", DbType.String, pSupervisionGruposApoyoIntegrantes.Nombres);
                    vDataBase.AddInParameter(vDbCommand, "@Apellidos", DbType.String, pSupervisionGruposApoyoIntegrantes.Apellidos);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionGruposApoyoIntegrantes.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionGruposApoyoIntegrantes.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdIntegranteGrupo").ToString());
                    //pSupervisionGruposApoyoIntegrantes.IdGrupo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdGrupo").ToString());
                    GenerarLogAuditoria(pSupervisionGruposApoyoIntegrantes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionGruposApoyoIntegrantes_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdIntegranteGrupo", DbType.Int32, pSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyoIntegrantes.IdGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@Grupo", DbType.String, pSupervisionGruposApoyoIntegrantes.Grupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionGruposApoyoIntegrantes.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pSupervisionGruposApoyoIntegrantes.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Nombres", DbType.String, pSupervisionGruposApoyoIntegrantes.Nombres);
                    vDataBase.AddInParameter(vDbCommand, "@Apellidos", DbType.String, pSupervisionGruposApoyoIntegrantes.Apellidos);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionGruposApoyoIntegrantes.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionGruposApoyoIntegrantes.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionGruposApoyoIntegrantes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionGruposApoyoIntegrantes_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdIntegranteGrupo", DbType.Int32, pSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyoIntegrantes.IdGrupo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionGruposApoyoIntegrantes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public SupervisionGruposApoyoIntegrantes ConsultarSupervisionGruposApoyoIntegrantes(int pIdIntegranteGrupo, int pIdGrupo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoIntegrantes_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdIntegranteGrupo", DbType.Int32, pIdIntegranteGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pIdGrupo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionGruposApoyoIntegrantes vSupervisionGruposApoyoIntegrantes = new SupervisionGruposApoyoIntegrantes();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo = vDataReaderResults["IdIntegranteGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdIntegranteGrupo"].ToString()) : vSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo;
                            vSupervisionGruposApoyoIntegrantes.IdGrupo = vDataReaderResults["IdGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGrupo"].ToString()) : vSupervisionGruposApoyoIntegrantes.IdGrupo;
                            vSupervisionGruposApoyoIntegrantes.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Identificacion"].ToString()) : vSupervisionGruposApoyoIntegrantes.Identificacion;
                            vSupervisionGruposApoyoIntegrantes.Nombres = vDataReaderResults["Nombres"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombres"].ToString()) : vSupervisionGruposApoyoIntegrantes.Nombres;
                            vSupervisionGruposApoyoIntegrantes.Apellidos = vDataReaderResults["Apellidos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Apellidos"].ToString()) : vSupervisionGruposApoyoIntegrantes.Apellidos;
                            vSupervisionGruposApoyoIntegrantes.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionGruposApoyoIntegrantes.Estado;
                            vSupervisionGruposApoyoIntegrantes.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionGruposApoyoIntegrantes.UsuarioCrea;
                            vSupervisionGruposApoyoIntegrantes.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionGruposApoyoIntegrantes.FechaCrea;
                            vSupervisionGruposApoyoIntegrantes.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionGruposApoyoIntegrantes.UsuarioModifica;
                            vSupervisionGruposApoyoIntegrantes.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionGruposApoyoIntegrantes.FechaModifica;
                        }
                        return vSupervisionGruposApoyoIntegrantes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ConsultarSupervisionGruposApoyoIntegrantes(String pNombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoIntegrantes_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int IdIntegranteGrupo = 0;
                        while (vDataReaderResults.Read())
                        {
                            IdIntegranteGrupo = vDataReaderResults["IdIntegranteGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdIntegranteGrupo"].ToString()) : IdIntegranteGrupo;
                        }
                        return IdIntegranteGrupo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposApoyoIntegrantes> ConsultarSupervisionGruposApoyoIntegrantess(int? pIdIntegranteGrupo,int? pIdGrupo,int? pIdentificacion,String pNombres, String pApellidos, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoIntegrantes_Consultar"))
                {
                    if(pIdIntegranteGrupo != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdIntegranteGrupo", DbType.Int32, pIdIntegranteGrupo);
                    if (pIdGrupo != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pIdGrupo);
                    if (pIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pIdentificacion);
                    if(pNombres != null)
                         vDataBase.AddInParameter(vDbCommand, "@Nombres", DbType.String, pNombres);
                    if(pApellidos != null)
                         vDataBase.AddInParameter(vDbCommand, "@Apellidos", DbType.String, pApellidos);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionGruposApoyoIntegrantes> vListaSupervisionGruposApoyoIntegrantes = new List<SupervisionGruposApoyoIntegrantes>();
                        while (vDataReaderResults.Read())
                        {
                                SupervisionGruposApoyoIntegrantes vSupervisionGruposApoyoIntegrantes = new SupervisionGruposApoyoIntegrantes();
                            vSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo = vDataReaderResults["IdIntegranteGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdIntegranteGrupo"].ToString()) : vSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo;
                            vSupervisionGruposApoyoIntegrantes.IdGrupo = vDataReaderResults["IdGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGrupo"].ToString()) : vSupervisionGruposApoyoIntegrantes.IdGrupo;
                            vSupervisionGruposApoyoIntegrantes.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Identificacion"].ToString()) : vSupervisionGruposApoyoIntegrantes.Identificacion;
                            vSupervisionGruposApoyoIntegrantes.Nombres = vDataReaderResults["Nombres"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombres"].ToString()) : vSupervisionGruposApoyoIntegrantes.Nombres;
                            vSupervisionGruposApoyoIntegrantes.Apellidos = vDataReaderResults["Apellidos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Apellidos"].ToString()) : vSupervisionGruposApoyoIntegrantes.Apellidos;
                            vSupervisionGruposApoyoIntegrantes.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionGruposApoyoIntegrantes.Estado;
                            vSupervisionGruposApoyoIntegrantes.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionGruposApoyoIntegrantes.UsuarioCrea;
                            vSupervisionGruposApoyoIntegrantes.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionGruposApoyoIntegrantes.FechaCrea;
                            vSupervisionGruposApoyoIntegrantes.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionGruposApoyoIntegrantes.UsuarioModifica;
                            vSupervisionGruposApoyoIntegrantes.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionGruposApoyoIntegrantes.FechaModifica;
                                vListaSupervisionGruposApoyoIntegrantes.Add(vSupervisionGruposApoyoIntegrantes);
                        }
                        return vListaSupervisionGruposApoyoIntegrantes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region "Consultar Integrantes Oferentes"

        public Database ObtenerInstanciaOferentes()
        {
            //return DatabaseFactory.CreateDatabase("DataBaseConnectionString");
            return Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase("OferentesConnectionString");
        }

        public List<SupervisionGruposLupaPersona> ConsultarPersonaLupa(String pIdTipoDocumento, Int64? pNumeroIdentificacion, String pNombre1, String pNombre2, String pApellido1, String pApellido2)
        {
            try
            {
                Database vDataBase = ObtenerInstanciaOferentes();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_SPCP_SUP_ServicioConsultarTercero"))
                {
                    if (pNumeroIdentificacion != null) { vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, pNumeroIdentificacion); }
                    if (pNombre1 != null && pNombre1 != "") { vDataBase.AddInParameter(vDbCommand, "@PRIMERNOMBRE", DbType.String, pNombre1); }
                    if (pNombre2 != null && pNombre2 != "") { vDataBase.AddInParameter(vDbCommand, "@SEGUNDONOMBRE", DbType.String, pNombre2); }
                    if (pApellido1 != null && pApellido1 != "") { vDataBase.AddInParameter(vDbCommand, "@PRIMERAPELLIDO", DbType.String, pApellido1); }
                    if (pApellido2 != null && pApellido2 != "") { vDataBase.AddInParameter(vDbCommand, "@SEGUNDOAPELLIDO", DbType.String, pApellido2); }
                    if (pIdTipoDocumento != null && pIdTipoDocumento != "") { vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCUMENTO", DbType.Int16, pIdTipoDocumento); }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionGruposLupaPersona> vListaSupervisonIntegrantesPerson = new List<SupervisionGruposLupaPersona>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionGruposLupaPersona vSupervisionIntegrantesPersona = new SupervisionGruposLupaPersona();
                            vSupervisionIntegrantesPersona.TipoPersona = vDataReaderResults["TIPO_PERSONA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TIPO_PERSONA"].ToString()) : vSupervisionIntegrantesPersona.TipoPersona;
                            vSupervisionIntegrantesPersona.TipoIdentificacion = vDataReaderResults["TIPO_IDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TIPO_IDENTIFICACION"].ToString()) : vSupervisionIntegrantesPersona.TipoIdentificacion;
                            vSupervisionIntegrantesPersona.Identificacion = vDataReaderResults["IDENTIFICACION"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IDENTIFICACION"]) : vSupervisionIntegrantesPersona.Identificacion;
                            vSupervisionIntegrantesPersona.NombreTercero = vDataReaderResults["TERCERO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TERCERO"].ToString()) : vSupervisionIntegrantesPersona.NombreTercero;
                            vSupervisionIntegrantesPersona.Nombre1 = vDataReaderResults["NOMBRE1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBRE1"].ToString()) : vSupervisionIntegrantesPersona.TipoIdentificacion;
                            vSupervisionIntegrantesPersona.Nombre2 = vDataReaderResults["NOMBRE2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBRE2"].ToString()) : vSupervisionIntegrantesPersona.TipoIdentificacion;
                            vSupervisionIntegrantesPersona.Apellido1 = vDataReaderResults["APELLIDO1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["APELLIDO1"].ToString()) : vSupervisionIntegrantesPersona.TipoIdentificacion;
                            vSupervisionIntegrantesPersona.Apellido2 = vDataReaderResults["APELLIDO2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["APELLIDO2"].ToString()) : vSupervisionIntegrantesPersona.TipoIdentificacion;

                            vListaSupervisonIntegrantesPerson.Add(vSupervisionIntegrantesPersona);
                        }
                        return vListaSupervisonIntegrantesPerson;
                    }


                }

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
        #endregion
    }
}
