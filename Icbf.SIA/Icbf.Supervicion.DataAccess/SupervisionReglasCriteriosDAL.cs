﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionReglasCriteriosDAL : GeneralDAL
    {

        public List<SupervisionReglasCriterios> ConsultarSupervisionCriterios(int? pIdCriterioRespuesta, int? pIdRespuesta, int? pTipoconf, int? pEstado, int? pTipoConsulta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_CriteriosRespuestas_Consultar"))
                {

                    if (pIdCriterioRespuesta != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCriterioRespuesta", DbType.Int32, pIdCriterioRespuesta);
                    if (pIdRespuesta != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRespuesta", DbType.Int32, pIdRespuesta);
                    if (pTipoconf != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoConfiguracion", DbType.Int32, pTipoconf);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    if (pTipoConsulta != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoConsulta", DbType.Int32, pTipoConsulta);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionReglasCriterios> vListaSupervisionCriterios = new List<SupervisionReglasCriterios>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionReglasCriterios vSupervisionCriterios = new SupervisionReglasCriterios();
                            vSupervisionCriterios.IdCriterioRespuesta = vDataReaderResults["IdCriterioRespuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCriterioRespuesta"].ToString()) : vSupervisionCriterios.IdCriterioRespuesta;
                            vSupervisionCriterios.IdRespuesta = vDataReaderResults["IdRespuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRespuesta"].ToString()) : vSupervisionCriterios.IdRespuesta;
                            vSupervisionCriterios.IdCriterio = vDataReaderResults["IdCriterio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCriterio"].ToString()) : vSupervisionCriterios.IdCriterio;
                            vSupervisionCriterios.TipoConfiguracion = vDataReaderResults["TipoConfiguracion"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["TipoConfiguracion"].ToString()) : vSupervisionCriterios.TipoConfiguracion;
                            vSupervisionCriterios.ValorMinimo = vDataReaderResults["ValorMinimo"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["ValorMinimo"].ToString()) : vSupervisionCriterios.ValorMinimo;
                            vSupervisionCriterios.ValorMaximo = vDataReaderResults["ValorMaximo"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["ValorMaximo"].ToString()) : vSupervisionCriterios.ValorMaximo;
                            vSupervisionCriterios.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionCriterios.Estado;
                            vSupervisionCriterios.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionCriterios.UsuarioCrea;
                            vSupervisionCriterios.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionCriterios.FechaCrea;
                            vSupervisionCriterios.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionCriterios.UsuarioModifica;
                            vSupervisionCriterios.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionCriterios.FechaModifica;
                            vListaSupervisionCriterios.Add(vSupervisionCriterios);
                        }
                        return vListaSupervisionCriterios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public SupervisionReglasCriterios InsertarSupervisionReglasCriterios(SupervisionReglasCriterios pObjSupReglasCriterios, DataTable dtbCriterios)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Criterios_PoblarDatos"))
                {


                    vDataBase.AddInParameter(vDbCommand, "@IdRespuesta", DbType.Int32, pObjSupReglasCriterios.IdRespuesta);
                    vDataBase.AddOutParameter(vDbCommand, "@IdCriterioRespuesta", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@TipoConfiguracion", DbType.Int16, pObjSupReglasCriterios.TipoConfiguracion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int16, pObjSupReglasCriterios.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pObjSupReglasCriterios.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorMinimo", DbType.Int32, pObjSupReglasCriterios.ValorMinimo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorMaximo", DbType.Int32, pObjSupReglasCriterios.ValorMaximo);

                    System.Data.SqlClient.SqlParameter tvpParam = new System.Data.SqlClient.SqlParameter();
                    tvpParam.SqlDbType = SqlDbType.Structured;
                    tvpParam.TypeName = "dbo.IdPreguntasSupervision";
                    tvpParam.ParameterName = "@t_Criterios";
                    tvpParam.Value = dtbCriterios;
                    vDbCommand.Parameters.Add(tvpParam);

                    vDataBase.AddOutParameter(vDbCommand, "@MensajeError", DbType.String, 500);


                    //if (pIdPregunta != null)
                    //    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pIdPregunta);
                    //if (pValor != null)
                    //    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pValor);
                    //if (pEstado != null)
                    //    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);


                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pObjSupReglasCriterios.IdCriterioRespuesta = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCriterioRespuesta").ToString());
                    pObjSupReglasCriterios.MensajeError = vDataBase.GetParameterValue(vDbCommand, "@MensajeError").ToString();

                    //GenerarLogAuditoria(pDiagnosticoSeguimiento, vDbCommand);
                    return pObjSupReglasCriterios;

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }


        public SupervisionPreguntas ConsultarDatosFiltrosSupervisionCriterios(int? pIdRespuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Respuestas_Consultar_Conf"))
                {

                    if (pIdRespuesta != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRespuesta", DbType.Int32, pIdRespuesta);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionPreguntas vSupervisionCriterios = new SupervisionPreguntas();
                        while (vDataReaderResults.Read())
                        {

                            vSupervisionCriterios.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionCriterios.IdPregunta;
                            vSupervisionCriterios.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionCriterios.IdDireccion;
                            vSupervisionCriterios.Aplicable = vDataReaderResults["Aplicable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Aplicable"].ToString()) : vSupervisionCriterios.Aplicable;
                        }
                        return vSupervisionCriterios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
