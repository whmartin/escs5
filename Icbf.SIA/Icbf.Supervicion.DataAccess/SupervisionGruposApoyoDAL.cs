using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionGruposApoyoDAL : GeneralDAL
    {
        public SupervisionGruposApoyoDAL()
        {
        }
        public int InsertarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdGrupo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionGruposApoyo.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pSupervisionGruposApoyo.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionGruposApoyo.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionGruposApoyo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionGruposApoyo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionGruposApoyo.IdGrupo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdGrupo").ToString());
                    GenerarLogAuditoria(pSupervisionGruposApoyo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionGruposApoyo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyo.IdGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionGruposApoyo.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pSupervisionGruposApoyo.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionGruposApoyo.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionGruposApoyo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionGruposApoyo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionGruposApoyo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionGruposApoyo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyo.IdGrupo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionGruposApoyo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public SupervisionGruposApoyo ConsultarSupervisionGruposApoyo(int pIdGrupo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pIdGrupo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionGruposApoyo vSupervisionGruposApoyo = new SupervisionGruposApoyo();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionGruposApoyo.IdGrupo = vDataReaderResults["IdGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGrupo"].ToString()) : vSupervisionGruposApoyo.IdGrupo;
                            vSupervisionGruposApoyo.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionGruposApoyo.IdDireccion;
                            vSupervisionGruposApoyo.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vSupervisionGruposApoyo.IdRegional;
                            vSupervisionGruposApoyo.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionGruposApoyo.Nombre;
                            vSupervisionGruposApoyo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionGruposApoyo.Estado;
                            vSupervisionGruposApoyo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionGruposApoyo.UsuarioCrea;
                            vSupervisionGruposApoyo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionGruposApoyo.FechaCrea;
                            vSupervisionGruposApoyo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionGruposApoyo.UsuarioModifica;
                            vSupervisionGruposApoyo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionGruposApoyo.FechaModifica;
                        }
                        return vSupervisionGruposApoyo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposApoyo> ConsultarSupervisionGruposApoyos(int? pIdDireccion, int? pIdRegional, String pNombre, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyo_Consultar"))
                {
                    if(pIdDireccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pIdDireccion);
                    if(pIdRegional != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if(pNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionGruposApoyo> vListaSupervisionGruposApoyo = new List<SupervisionGruposApoyo>();
                        while (vDataReaderResults.Read())
                        {
                                SupervisionGruposApoyo vSupervisionGruposApoyo = new SupervisionGruposApoyo();
                            vSupervisionGruposApoyo.IdGrupo = vDataReaderResults["IdGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGrupo"].ToString()) : vSupervisionGruposApoyo.IdGrupo;
                            vSupervisionGruposApoyo.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionGruposApoyo.IdDireccion;
                            vSupervisionGruposApoyo.Direccion = vDataReaderResults["NombreDireccion"] != DBNull.Value ? vDataReaderResults["NombreDireccion"].ToString() : vSupervisionGruposApoyo.Direccion;
                            vSupervisionGruposApoyo.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vSupervisionGruposApoyo.IdRegional;
                            vSupervisionGruposApoyo.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionGruposApoyo.Nombre;
                            vSupervisionGruposApoyo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionGruposApoyo.Estado;
                            vSupervisionGruposApoyo.EstadoStr = vDataReaderResults["NombreEstado"] != DBNull.Value ? vDataReaderResults["NombreEstado"].ToString() : vSupervisionGruposApoyo.EstadoStr;
                            vSupervisionGruposApoyo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionGruposApoyo.UsuarioCrea;
                            vSupervisionGruposApoyo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionGruposApoyo.FechaCrea;
                            vSupervisionGruposApoyo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionGruposApoyo.UsuarioModifica;
                            vSupervisionGruposApoyo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionGruposApoyo.FechaModifica;
                            vListaSupervisionGruposApoyo.Add(vSupervisionGruposApoyo);
                        }
                        return vListaSupervisionGruposApoyo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionGruposApoyo(string Nombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, Nombre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int IdGrupo = 0;
                        while (vDataReaderResults.Read())
                        {
                            IdGrupo = vDataReaderResults["IdGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGrupo"].ToString()) : IdGrupo;
                        }
                        return IdGrupo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
