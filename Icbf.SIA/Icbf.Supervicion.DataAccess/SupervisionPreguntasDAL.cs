using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionPreguntasDAL : GeneralDAL
    {
        public SupervisionPreguntasDAL()
        {
        }
        public int InsertarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Preguntas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPregunta", DbType.Int32, pSupervisionPreguntas.IdPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionPreguntas.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionPreguntas.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pSupervisionPreguntas.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRespuesta", DbType.Int32, pSupervisionPreguntas.IdTipoRespuesta);
                    vDataBase.AddInParameter(vDbCommand, "@Aplicable", DbType.Int32, pSupervisionPreguntas.Aplicable);
                    vDataBase.AddInParameter(vDbCommand, "@Responsable", DbType.Int32, pSupervisionPreguntas.Responsable);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionPreguntas.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionPreguntas.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionPreguntas.IdPregunta = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPregunta").ToString());
                    GenerarLogAuditoria(pSupervisionPreguntas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int VerficaExisteSupervisionregunta(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                Database vDatabase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDatabase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Preguntas_Consultar"))
                {
                    int vResultado = 1;
                    vDatabase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionPreguntas.Nombre);

                    return vResultado;
                }
            }
            catch (Exception ex)
            {
                
                throw new GenericException(ex);
            }
        }


        public int ModificarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Preguntas_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pSupervisionPreguntas.IdPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionPreguntas.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionPreguntas.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pSupervisionPreguntas.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRespuesta", DbType.Int32, pSupervisionPreguntas.IdTipoRespuesta);
                    vDataBase.AddInParameter(vDbCommand, "@Aplicable", DbType.Int32, pSupervisionPreguntas.Aplicable);
                    vDataBase.AddInParameter(vDbCommand, "@Responsable", DbType.Int32, pSupervisionPreguntas.Responsable);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionPreguntas.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionPreguntas.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionPreguntas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionPreguntas_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pSupervisionPreguntas.IdPregunta);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionPreguntas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public SupervisionPreguntas ConsultarSupervisionPreguntas(int pIdPregunta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Preguntas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pIdPregunta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionPreguntas vSupervisionPreguntas = new SupervisionPreguntas();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionPreguntas.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionPreguntas.IdPregunta;
                            vSupervisionPreguntas.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionPreguntas.IdDireccion;
                            vSupervisionPreguntas.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionPreguntas.Nombre;
                            vSupervisionPreguntas.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vSupervisionPreguntas.Descripcion;
                            vSupervisionPreguntas.IdTipoRespuesta = vDataReaderResults["IdTipoRespuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRespuesta"].ToString()) : vSupervisionPreguntas.IdTipoRespuesta;
                            vSupervisionPreguntas.Aplicable = vDataReaderResults["Aplicable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Aplicable"].ToString()) : vSupervisionPreguntas.Aplicable;
                            vSupervisionPreguntas.Responsable = vDataReaderResults["Responsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Responsable"].ToString()) : vSupervisionPreguntas.Responsable;
                            vSupervisionPreguntas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionPreguntas.Estado;
                            vSupervisionPreguntas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionPreguntas.UsuarioCrea;
                            vSupervisionPreguntas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionPreguntas.FechaCrea;
                            vSupervisionPreguntas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionPreguntas.UsuarioModifica;
                            vSupervisionPreguntas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionPreguntas.FechaModifica;
                        }
                        return vSupervisionPreguntas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionPreguntas(string NombrePregunta, int? Aplicable)
        {
            try
            {
                int IdPregunta = 0;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Preguntas_Consultar"))
                {
                    if (NombrePregunta != "")
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, NombrePregunta);
                    }
                    if (Aplicable != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Aplicable", DbType.Int16, Aplicable);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionPreguntas vSupervisionPreguntas = new SupervisionPreguntas();
                        while (vDataReaderResults.Read())
                        {
                            IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionPreguntas.IdPregunta;

                        }
                        return IdPregunta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<SupervisionPreguntas> ConsultarSupervisionPreguntass(int? IdPregunta, int? pIdDireccion, String pNombre, String pDescripcion, int? pIdTipoRespuesta, int? pAplicable, int? pResponsable, int? pEstado, String UsuarioCrea)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Preguntas_Consultar"))
                {
                    if (IdPregunta != null)
                        vDataBase.AddOutParameter(vDbCommand, "@IdPregunta", DbType.Int32, 0);
                    if (pIdDireccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pIdDireccion);
                    if (pIdTipoRespuesta != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoRespuesta", DbType.Int32, pIdTipoRespuesta);
                    if (pAplicable != null)
                        vDataBase.AddInParameter(vDbCommand, "@Aplicable", DbType.Int32, pAplicable);
                    if (pResponsable != null)
                        vDataBase.AddInParameter(vDbCommand, "@Responsable", DbType.Int32, pResponsable);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    if (UsuarioCrea != null && UsuarioCrea.Trim() != "")
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, UsuarioCrea);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionPreguntas> vListaSupervisionPreguntas = new List<SupervisionPreguntas>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionPreguntas vSupervisionPreguntas = new SupervisionPreguntas();
                            vSupervisionPreguntas.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionPreguntas.IdPregunta;
                            vSupervisionPreguntas.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionPreguntas.IdDireccion;
                            vSupervisionPreguntas.Direccion = vDataReaderResults["NombreDireccion"] != DBNull.Value ? vDataReaderResults["NombreDireccion"].ToString() : vSupervisionPreguntas.Direccion;
                            vSupervisionPreguntas.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionPreguntas.Nombre;
                            vSupervisionPreguntas.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vSupervisionPreguntas.Descripcion;
                            vSupervisionPreguntas.IdTipoRespuesta = vDataReaderResults["IdTipoRespuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRespuesta"].ToString()) : vSupervisionPreguntas.IdTipoRespuesta;
                            vSupervisionPreguntas.NombreTipoRespuesta = vDataReaderResults["NombreTipoRespuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoRespuesta"].ToString()) : vSupervisionPreguntas.NombreTipoRespuesta;
                            vSupervisionPreguntas.Aplicable = vDataReaderResults["Aplicable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Aplicable"].ToString()) : vSupervisionPreguntas.Aplicable;
                            vSupervisionPreguntas.AplicableStr = vDataReaderResults["DescripcionAplicable"] != DBNull.Value ? vDataReaderResults["DescripcionAplicable"].ToString() : vSupervisionPreguntas.AplicableStr;
                            vSupervisionPreguntas.Responsable = vDataReaderResults["Responsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Responsable"].ToString()) : vSupervisionPreguntas.Responsable;
                            vSupervisionPreguntas.NombreResponsable = vDataReaderResults["NombreResponsable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreResponsable"].ToString()) : vSupervisionPreguntas.NombreResponsable;
                            vSupervisionPreguntas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionPreguntas.Estado;
                            vSupervisionPreguntas.EstadoStr = vDataReaderResults["NombreEstado"] != DBNull.Value ? vDataReaderResults["NombreEstado"].ToString() : vSupervisionPreguntas.EstadoStr;
                            vSupervisionPreguntas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionPreguntas.UsuarioCrea;
                            vSupervisionPreguntas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionPreguntas.FechaCrea;
                            vSupervisionPreguntas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionPreguntas.UsuarioModifica;
                            vSupervisionPreguntas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionPreguntas.FechaModifica;

                            vListaSupervisionPreguntas.Add(vSupervisionPreguntas);
                        }
                        return vListaSupervisionPreguntas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionPreguntas> ConsultarTipoRespuesta()
        {
            try
            {
                Database vDatabase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDatabase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_TipoRespuestas_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDatabase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionPreguntas> vListaSupervicionsPreguntas = new List<SupervisionPreguntas>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionPreguntas vSupervisionPreguntas = new SupervisionPreguntas();
                            vSupervisionPreguntas.IdTipoRespuesta = vDataReaderResults["IdTipoRespuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRespuesta"].ToString()) : vSupervisionPreguntas.IdTipoRespuesta;
                            vSupervisionPreguntas.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vSupervisionPreguntas.Descripcion;
                            vListaSupervicionsPreguntas.Add(vSupervisionPreguntas);
                        }
                        return vListaSupervicionsPreguntas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
