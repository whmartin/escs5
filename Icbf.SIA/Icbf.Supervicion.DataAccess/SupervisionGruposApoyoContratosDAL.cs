using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionGruposApoyoContratosDAL : GeneralDAL
    {
        public SupervisionGruposApoyoContratosDAL()
        {
        }

        public int InsertarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoContratos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContratosGrupo", DbType.Int32, 18);

                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyoContratos.IdGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionGruposApoyoContratos.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pSupervisionGruposApoyoContratos.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pSupervisionGruposApoyoContratos.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.Int32, pSupervisionGruposApoyoContratos.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumentoEntidad", DbType.Int32, pSupervisionGruposApoyoContratos.NumeroDocumentoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEntidadContratista", DbType.Int32, pSupervisionGruposApoyoContratos.NombreEntidadContratista);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionEntidadContratista", DbType.Int32, pSupervisionGruposApoyoContratos.DireccionEntidadContratista);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumentoRepLegal", DbType.Int32, pSupervisionGruposApoyoContratos.NumeroDocumentoRepLegal);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRepLegal", DbType.Int32, pSupervisionGruposApoyoContratos.NombreRepLegal);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumentoSupervisor", DbType.Int32, pSupervisionGruposApoyoContratos.NumeroDocumentoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSupervisor", DbType.Int32, pSupervisionGruposApoyoContratos.NombreSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionGruposApoyoContratos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.Int32, pSupervisionGruposApoyoContratos.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionGruposApoyoContratos.IdContratosGrupo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContratosGrupo").ToString());
                    GenerarLogAuditoria(pSupervisionGruposApoyoContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos, DataTable pDtbContratos)
        {


            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoContratos_PoblarDatos"))
                {
                    int vResultado;
                    System.Data.SqlClient.SqlParameter tvpParam = new System.Data.SqlClient.SqlParameter();
                    tvpParam.SqlDbType = SqlDbType.Structured;
                    tvpParam.TypeName = "dbo.ContratosSupervision";
                    tvpParam.ParameterName = "@t_Contratos";
                    tvpParam.Value = pDtbContratos;

                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyoContratos.IdGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionGruposApoyoContratos.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pSupervisionGruposApoyoContratos.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int16, pSupervisionGruposApoyoContratos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionGruposApoyoContratos.UsuarioCrea);
                    vDbCommand.Parameters.Add(tvpParam);
                    //Tabla con los datos de contratos
                    vDataBase.AddOutParameter(vDbCommand, "@MensajeError", DbType.String, 500);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionGruposApoyoContratos.MensajeError = vDataBase.GetParameterValue(vDbCommand, "@MensajeError").ToString();
                    GenerarLogAuditoria(pSupervisionGruposApoyoContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionGruposApoyoContratos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratosGrupo", DbType.Int32, pSupervisionGruposApoyoContratos.IdContratosGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyoContratos.IdGrupo);
                    //vDataBase.AddInParameter(vDbCommand, "@Grupo", DbType.String, pSupervisionGruposApoyoContratos.Grupo);
                    //vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pSupervisionGruposApoyoContratos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pSupervisionGruposApoyoContratos.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionGruposApoyoContratos.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionGruposApoyoContratos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionGruposApoyoContratos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionGruposApoyoContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionGruposApoyoContratos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratosGrupo", DbType.Int32, pSupervisionGruposApoyoContratos.IdContratosGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pSupervisionGruposApoyoContratos.IdGrupo);
                    //vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pSupervisionGruposApoyoContratos.IdContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionGruposApoyoContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionGruposApoyoContratos ConsultarSupervisionGruposApoyoContratos(int pIdContratosGrupo, int pIdGrupo, int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoContratos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratosGrupo", DbType.Int32, pIdContratosGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdGrupo", DbType.Int32, pIdGrupo);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionGruposApoyoContratos vSupervisionGruposApoyoContratos = new SupervisionGruposApoyoContratos();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionGruposApoyoContratos.IdContratosGrupo = vDataReaderResults["IdContratosGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosGrupo"].ToString()) : vSupervisionGruposApoyoContratos.IdContratosGrupo;
                            vSupervisionGruposApoyoContratos.IdGrupo = vDataReaderResults["IdGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGrupo"].ToString()) : vSupervisionGruposApoyoContratos.IdGrupo;
                            //vSupervisionGruposApoyoContratos.Grupo = vDataReaderResults["Grupo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Grupo"].ToString()) : vSupervisionGruposApoyoContratos.Grupo;
                            //vSupervisionGruposApoyoContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSupervisionGruposApoyoContratos.IdContrato;
                            vSupervisionGruposApoyoContratos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vSupervisionGruposApoyoContratos.IdRegional;
                            vSupervisionGruposApoyoContratos.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionGruposApoyoContratos.Nombre;
                            vSupervisionGruposApoyoContratos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionGruposApoyoContratos.Estado;
                            vSupervisionGruposApoyoContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionGruposApoyoContratos.UsuarioCrea;
                            vSupervisionGruposApoyoContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionGruposApoyoContratos.FechaCrea;
                            vSupervisionGruposApoyoContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionGruposApoyoContratos.UsuarioModifica;
                            vSupervisionGruposApoyoContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionGruposApoyoContratos.FechaModifica;
                        }
                        return vSupervisionGruposApoyoContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionGruposApoyoContratos(String pNombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoContratos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int IdContrato = 0;
                        while (vDataReaderResults.Read())
                        {
                            IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : IdContrato;
                        }
                        return IdContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposApoyoContratos> ConsultarSupervisionGruposApoyoContratoss(int? pIdContratosGrupo, int? pIdGrupo, int? pIdDireccion, int? pIdRegional, int? pVigencia, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_GruposApoyoContratos_Consultar"))
                {
                    if (pIdContratosGrupo != null)
                        vDataBase.AddInParameter(vDbCommand, "@idcontratosgrupo ", DbType.Int32, pIdContratosGrupo);
                    if (pIdGrupo != null)
                        vDataBase.AddInParameter(vDbCommand, "@idgrupo", DbType.Int32, pIdGrupo);
                    if (pIdDireccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@iddireccion", DbType.Int32, pIdDireccion);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@idregional", DbType.Int32, pIdRegional);
                    if (pVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@vigencia", DbType.Int32, pVigencia);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@estado", DbType.Int16, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionGruposApoyoContratos> vListaSupervisionGruposApoyoContratos = new List<SupervisionGruposApoyoContratos>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionGruposApoyoContratos vSupervisionGruposApoyoContratos = new SupervisionGruposApoyoContratos();
                            vSupervisionGruposApoyoContratos.IdContratosGrupo = vDataReaderResults["IdContratosGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosGrupo"].ToString()) : vSupervisionGruposApoyoContratos.IdContratosGrupo;
                            vSupervisionGruposApoyoContratos.IdGrupo = vDataReaderResults["IdGrupo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGrupo"].ToString()) : vSupervisionGruposApoyoContratos.IdGrupo;
                            vSupervisionGruposApoyoContratos.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionGruposApoyoContratos.IdDireccion;
                            vSupervisionGruposApoyoContratos.NombreDireccionICBF = vDataReaderResults["NombreDireccion"] != DBNull.Value ? vDataReaderResults["NombreDireccion"].ToString() : vSupervisionGruposApoyoContratos.NombreDireccionICBF;
                            vSupervisionGruposApoyoContratos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vSupervisionGruposApoyoContratos.IdRegional;
                            vSupervisionGruposApoyoContratos.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vSupervisionGruposApoyoContratos.Vigencia;
                            vSupervisionGruposApoyoContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? vDataReaderResults["NumeroContrato"].ToString() : vSupervisionGruposApoyoContratos.NumeroContrato;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoEntidad = vDataReaderResults["NumeroDocumentoEntidad"] != DBNull.Value ? vDataReaderResults["NumeroDocumentoEntidad"].ToString() : vSupervisionGruposApoyoContratos.NumeroDocumentoEntidad;
                            vSupervisionGruposApoyoContratos.NombreEntidadContratista = vDataReaderResults["NombreEntidadContratista"] != DBNull.Value ? vDataReaderResults["NombreEntidadContratista"].ToString() : vSupervisionGruposApoyoContratos.NombreEntidadContratista;
                            vSupervisionGruposApoyoContratos.DireccionEntidadContratista = vDataReaderResults["DireccionEntidadContratista"] != DBNull.Value ? vDataReaderResults["DireccionEntidadContratista"].ToString() : vSupervisionGruposApoyoContratos.DireccionEntidadContratista;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoRepLegal = vDataReaderResults["NumeroDocumentoRepLegal"] != DBNull.Value ? vDataReaderResults["NumeroDocumentoRepLegal"].ToString() : vSupervisionGruposApoyoContratos.NumeroDocumentoRepLegal;
                            vSupervisionGruposApoyoContratos.NombreRepLegal = vDataReaderResults["NombreRepLegal"] != DBNull.Value ? vDataReaderResults["NombreRepLegal"].ToString() : vSupervisionGruposApoyoContratos.NombreRepLegal;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoSupervisor = vDataReaderResults["NumeroDocumentoSupervisor"] != DBNull.Value ? vDataReaderResults["NumeroDocumentoSupervisor"].ToString() : vSupervisionGruposApoyoContratos.NumeroDocumentoSupervisor;
                            vSupervisionGruposApoyoContratos.NombreSupervisor = vDataReaderResults["NombreSupervisor"] != DBNull.Value ? vDataReaderResults["NombreSupervisor"].ToString() : vSupervisionGruposApoyoContratos.NombreSupervisor;
                            vSupervisionGruposApoyoContratos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionGruposApoyoContratos.Estado;
                            vSupervisionGruposApoyoContratos.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? vDataReaderResults["NombreEstado"].ToString() : vSupervisionGruposApoyoContratos.NombreEstado;
                            vSupervisionGruposApoyoContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionGruposApoyoContratos.UsuarioCrea;
                            vSupervisionGruposApoyoContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionGruposApoyoContratos.FechaCrea;
                            vSupervisionGruposApoyoContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionGruposApoyoContratos.UsuarioModifica;
                            vSupervisionGruposApoyoContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionGruposApoyoContratos.FechaModifica;
                            vListaSupervisionGruposApoyoContratos.Add(vSupervisionGruposApoyoContratos);
                        }
                        return vListaSupervisionGruposApoyoContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #region "Contratos cuentame"

        public Database ObtenerInstanciaCuentame()
        {
            //return DatabaseFactory.CreateDatabase("DataBaseConnectionString");
            return Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase("CuentameConnectionString");
        }


        public List<SupervisionGruposApoyoContratos> ConsultarContratosCuentame(Int32? pVigencia, string pIdRegional, Int16 IdDireccion)
        {
            try
            {
                Database vDataBase = ObtenerInstanciaCuentame();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_SIA_ConsultarContratos"))
                {
                    if (pVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pVigencia);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, pIdRegional);
                    if (IdDireccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDireccionIcbf", DbType.Int16, IdDireccion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionGruposApoyoContratos> vListaSupervisionGruposApoyoContratos = new List<SupervisionGruposApoyoContratos>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionGruposApoyoContratos vSupervisionGruposApoyoContratos = new SupervisionGruposApoyoContratos();
                            vSupervisionGruposApoyoContratos.IdContrato = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["idContrato"].ToString()) : vSupervisionGruposApoyoContratos.IdContrato;
                            vSupervisionGruposApoyoContratos.NumeroContrato = vDataReaderResults["numeroContrato"] != DBNull.Value ? vDataReaderResults["numeroContrato"].ToString() : vSupervisionGruposApoyoContratos.NumeroContrato;
                            vSupervisionGruposApoyoContratos.FechaInicioContrato = vDataReaderResults["fechaInicioContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["fechaInicioContrato"].ToString()) : vSupervisionGruposApoyoContratos.FechaInicioContrato;
                            vSupervisionGruposApoyoContratos.FechaFinContrato = vDataReaderResults["fechaFinalizacionContratoN"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["fechaFinalizacionContratoN"].ToString()) : vSupervisionGruposApoyoContratos.FechaFinContrato;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoEntidad = vDataReaderResults["numDocEntidad"] != DBNull.Value ? vDataReaderResults["numDocEntidad"].ToString() : vSupervisionGruposApoyoContratos.NumeroDocumentoEntidad;
                            vSupervisionGruposApoyoContratos.NombreEntidadContratista = vDataReaderResults["nombEntidad"] != DBNull.Value ? vDataReaderResults["nombEntidad"].ToString() : vSupervisionGruposApoyoContratos.NombreEntidadContratista;
                            vSupervisionGruposApoyoContratos.DireccionEntidadContratista = vDataReaderResults["dirEntidad"] != DBNull.Value ? vDataReaderResults["dirEntidad"].ToString() : vSupervisionGruposApoyoContratos.DireccionEntidadContratista;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoRepLegal = vDataReaderResults["IdentRepLegal"] != DBNull.Value ? vDataReaderResults["IdentRepLegal"].ToString() : vSupervisionGruposApoyoContratos.NumeroDocumentoRepLegal;
                            vSupervisionGruposApoyoContratos.NombreRepLegal = vDataReaderResults["nombRepLegal"] != DBNull.Value ? vDataReaderResults["nombRepLegal"].ToString() : vSupervisionGruposApoyoContratos.NombreRepLegal;
                            vSupervisionGruposApoyoContratos.NombreSupervisor = vDataReaderResults["nombSupervisor"] != DBNull.Value ? vDataReaderResults["nombSupervisor"].ToString() : vSupervisionGruposApoyoContratos.NombreSupervisor;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoSupervisor = vDataReaderResults["IdentSupervisor"] != DBNull.Value ? vDataReaderResults["IdentSupervisor"].ToString() : vSupervisionGruposApoyoContratos.NombreSupervisor; vListaSupervisionGruposApoyoContratos.Add(vSupervisionGruposApoyoContratos);
                        }
                        return vListaSupervisionGruposApoyoContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion


        #region "Contratos SIM"

        public Database ObtenerInstanciaSim()
        {
            //return DatabaseFactory.CreateDatabase("DataBaseConnectionString");
            return Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase("SimConnectionString");
        }

        public List<SupervisionGruposApoyoContratos> ConsultarContratosSim(int? pVigencia, string pIdRegional)
        {
            try
            {//Revisar parametros de regional que est� pegado
                Database vDataBase = ObtenerInstanciaSim();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("Usp_SupervisionContratos_Consultar"))
                {
                    vDbCommand.CommandTimeout = 60;
                    if (pVigencia != null)
                    { vDataBase.AddInParameter(vDbCommand, "@vigencia ", DbType.Int32, pVigencia); }

                    if (pIdRegional != null)
                    { vDataBase.AddInParameter(vDbCommand, "@regional", DbType.String, pIdRegional); }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionGruposApoyoContratos> vListaSupervisionGruposApoyoContratos = new List<SupervisionGruposApoyoContratos>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionGruposApoyoContratos vSupervisionGruposApoyoContratos = new SupervisionGruposApoyoContratos();
                            vSupervisionGruposApoyoContratos.IdContrato = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["idContrato"].ToString()) : vSupervisionGruposApoyoContratos.IdContrato;
                            vSupervisionGruposApoyoContratos.NumeroContrato = vDataReaderResults["numeroContrato"] != DBNull.Value ? vDataReaderResults["numeroContrato"].ToString() : vSupervisionGruposApoyoContratos.NumeroContrato;
                            vSupervisionGruposApoyoContratos.FechaInicioContrato = vDataReaderResults["fechaInicioContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["fechaInicioContrato"].ToString()) : vSupervisionGruposApoyoContratos.FechaInicioContrato;
                            vSupervisionGruposApoyoContratos.FechaFinContrato = vDataReaderResults["fechaFinalizacionContratoN"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["fechaFinalizacionContratoN"].ToString()) : vSupervisionGruposApoyoContratos.FechaFinContrato;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoEntidad = vDataReaderResults["numDocEntidad"] != DBNull.Value ? vDataReaderResults["numDocEntidad"].ToString() : vSupervisionGruposApoyoContratos.NumeroDocumentoEntidad;
                            vSupervisionGruposApoyoContratos.NombreEntidadContratista = vDataReaderResults["nombEntidad"] != DBNull.Value ? vDataReaderResults["nombEntidad"].ToString() : vSupervisionGruposApoyoContratos.NombreEntidadContratista;
                            vSupervisionGruposApoyoContratos.DireccionEntidadContratista = vDataReaderResults["dirEntidad"] != DBNull.Value ? vDataReaderResults["dirEntidad"].ToString() : vSupervisionGruposApoyoContratos.DireccionEntidadContratista;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoRepLegal = vDataReaderResults["IdentRepLegal"] != DBNull.Value ? vDataReaderResults["IdentRepLegal"].ToString() : vSupervisionGruposApoyoContratos.NumeroDocumentoRepLegal;
                            vSupervisionGruposApoyoContratos.NombreRepLegal = vDataReaderResults["nombRepLegal"] != DBNull.Value ? vDataReaderResults["nombRepLegal"].ToString() : vSupervisionGruposApoyoContratos.NombreRepLegal;
                            vSupervisionGruposApoyoContratos.NombreSupervisor = vDataReaderResults["nombSupervisor"] != DBNull.Value ? vDataReaderResults["nombSupervisor"].ToString() : vSupervisionGruposApoyoContratos.NombreSupervisor;
                            vSupervisionGruposApoyoContratos.NumeroDocumentoSupervisor = vDataReaderResults["IdentSupervisor"] != DBNull.Value ? vDataReaderResults["IdentSupervisor"].ToString() : vSupervisionGruposApoyoContratos.NombreSupervisor;

                            vListaSupervisionGruposApoyoContratos.Add(vSupervisionGruposApoyoContratos);

                        }
                        return vListaSupervisionGruposApoyoContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

    }
}
