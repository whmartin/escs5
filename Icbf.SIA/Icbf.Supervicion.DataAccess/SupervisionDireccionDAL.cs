using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionDireccionDAL : GeneralDAL
    {
        public SupervisionDireccionDAL()
        {
        }
        //prueba
        public List<SupervisionDireccion> ConsultarSupervisionDireccions(int? pIdDireccionesICBF, String pCodigoDireccion,String pNombreDireccion ,int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Direcciones_Consultar"))
                {
                    if (pIdDireccionesICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDireccionesICBF", DbType.Int32, pIdDireccionesICBF);
                    if (pCodigoDireccion != null && pCodigoDireccion !="")
                        vDataBase.AddInParameter(vDbCommand, "@CodigoDireccion", DbType.String, pCodigoDireccion);
                    if (pNombreDireccion != null && pNombreDireccion != "")
                        vDataBase.AddInParameter(vDbCommand,"@NombreDireccion",DbType.String,pNombreDireccion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionDireccion> vSupervisionDireccions = new List<SupervisionDireccion>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionDireccion vSupervisionDireccion = new SupervisionDireccion();
                            vSupervisionDireccion.IdDireccionesICBF = vDataReaderResults["IdDireccionesICBF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionesICBF"].ToString()) : vSupervisionDireccion.IdDireccionesICBF;
                            vSupervisionDireccion.CodigoDireccion = vDataReaderResults["CodigoDireccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDireccion"].ToString()) : vSupervisionDireccion.CodigoDireccion;
                            vSupervisionDireccion.NombreDireccion = vDataReaderResults["NombreDireccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDireccion"].ToString()) : vSupervisionDireccion.NombreDireccion;
                            vSupervisionDireccion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionDireccion.Estado;
                            vSupervisionDireccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionDireccion.UsuarioCrea;
                            vSupervisionDireccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionDireccion.FechaCrea;
                            vSupervisionDireccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionDireccion.UsuarioModifica;
                            vSupervisionDireccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionDireccion.FechaModifica;
                            if (vSupervisionDireccion.Estado == 1)
                            {
                                vSupervisionDireccion.EstadoStr = "Activo";
                            }
                            else
                            {
                                vSupervisionDireccion.EstadoStr = "Desactivado";
                            }

                                vSupervisionDireccions.Add(vSupervisionDireccion);
                        }
                        return vSupervisionDireccions;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
