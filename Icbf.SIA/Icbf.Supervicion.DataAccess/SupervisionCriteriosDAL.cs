using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionCriteriosDAL : GeneralDAL
    {
        public SupervisionCriteriosDAL()
        {
        }
        public int InsertarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("dbo.usp_SIA_Contratos_Supervision_Criterios_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCriterio", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pSupervisionCriterios.IdPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pSupervisionCriterios.Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionCriterios.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionCriterios.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionCriterios.IdCriterio = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCriterio").ToString());
                    GenerarLogAuditoria(pSupervisionCriterios, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Criterios_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCriterio", DbType.Int32, pSupervisionCriterios.IdCriterio);
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pSupervisionCriterios.Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionCriterios.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionCriterios.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionCriterios, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionCriterios_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCriterio", DbType.Int32, pSupervisionCriterios.IdCriterio);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionCriterios, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public SupervisionCriterios ConsultarSupervisionCriterios(int pIdCriterio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Criterios_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCriterio", DbType.Int32, pIdCriterio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionCriterios vSupervisionCriterios = new SupervisionCriterios();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionCriterios.IdCriterio = vDataReaderResults["IdCriterio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCriterio"].ToString()) : vSupervisionCriterios.IdCriterio;
                            vSupervisionCriterios.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionCriterios.IdPregunta;
                            vSupervisionCriterios.Valor = vDataReaderResults["Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Valor"].ToString()) : vSupervisionCriterios.Valor;
                            vSupervisionCriterios.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionCriterios.Estado;
                            vSupervisionCriterios.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionCriterios.UsuarioCrea;
                            vSupervisionCriterios.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionCriterios.FechaCrea;
                            vSupervisionCriterios.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionCriterios.UsuarioModifica;
                            vSupervisionCriterios.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionCriterios.FechaModifica;
                        }
                        return vSupervisionCriterios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionCriterios(string Valor)
        {
            try
            {
                int IdPregunta = 0;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Criterios_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, Valor);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionCriterios vSupervisionCriterios = new SupervisionCriterios();
                        while (vDataReaderResults.Read())
                        {
                            IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionCriterios.IdPregunta;
                        }
                        return IdPregunta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCriterios> ConsultarSupervisionCriterioss(int? pIdPregunta, String pValor, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Criterios_Consultar"))
                {
                    if(pIdPregunta != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pIdPregunta);
                    if(pValor != null)
                         vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pValor);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionCriterios> vListaSupervisionCriterios = new List<SupervisionCriterios>();
                        while (vDataReaderResults.Read())
                        {
                                SupervisionCriterios vSupervisionCriterios = new SupervisionCriterios();
                            vSupervisionCriterios.IdCriterio = vDataReaderResults["IdCriterio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCriterio"].ToString()) : vSupervisionCriterios.IdCriterio;
                            vSupervisionCriterios.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionCriterios.IdPregunta;
                            vSupervisionCriterios.Valor = vDataReaderResults["Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Valor"].ToString()) : vSupervisionCriterios.Valor;
                            vSupervisionCriterios.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionCriterios.Estado;
                            vSupervisionCriterios.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionCriterios.UsuarioCrea;
                            vSupervisionCriterios.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionCriterios.FechaCrea;
                            vSupervisionCriterios.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionCriterios.UsuarioModifica;
                            vSupervisionCriterios.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionCriterios.FechaModifica;
                            if (vSupervisionCriterios.Estado==1)
                            {
                                vSupervisionCriterios.EstadoStr = "Activo";
                            }
                            else
                            {
                                vSupervisionCriterios.EstadoStr = "Desactivado";
                            }

                                vListaSupervisionCriterios.Add(vSupervisionCriterios);
                        }
                        return vListaSupervisionCriterios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
