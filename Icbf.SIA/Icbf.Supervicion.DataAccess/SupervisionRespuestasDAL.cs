using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionRespuestasDAL : GeneralDAL
    {
        public SupervisionRespuestasDAL()
        {
        }
        public int InsertarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Respuestas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRespuesta", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pSupervisionRespuestas.IdPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pSupervisionRespuestas.Valor);
                    vDataBase.AddInParameter(vDbCommand, "@VulneraDerecho", DbType.Boolean, pSupervisionRespuestas.VulneraDerecho);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionRespuestas.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionRespuestas.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionRespuestas.IdRespuesta = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRespuesta").ToString());
                    GenerarLogAuditoria(pSupervisionRespuestas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Respuestas_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRespuesta", DbType.Int32, pSupervisionRespuestas.IdRespuesta);
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pSupervisionRespuestas.IdPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pSupervisionRespuestas.Valor);
                    vDataBase.AddInParameter(vDbCommand, "@VulneraDerecho", DbType.Boolean, pSupervisionRespuestas.VulneraDerecho);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionRespuestas.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionRespuestas.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionRespuestas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionRespuestas_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRespuesta", DbType.Int32, pSupervisionRespuestas.IdRespuesta);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionRespuestas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public SupervisionRespuestas ConsultarSupervisionRespuestas(int pIdRespuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Respuestas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRespuesta", DbType.Int32, pIdRespuesta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionRespuestas vSupervisionRespuestas = new SupervisionRespuestas();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionRespuestas.IdRespuesta = vDataReaderResults["IdRespuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRespuesta"].ToString()) : vSupervisionRespuestas.IdRespuesta;
                            vSupervisionRespuestas.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionRespuestas.IdPregunta;
                            vSupervisionRespuestas.Valor = vDataReaderResults["Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Valor"].ToString()) : vSupervisionRespuestas.Valor;
                            vSupervisionRespuestas.VulneraDerecho = vDataReaderResults["VulneraDerecho"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VulneraDerecho"].ToString()) : vSupervisionRespuestas.VulneraDerecho;
                            vSupervisionRespuestas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionRespuestas.Estado;
                            vSupervisionRespuestas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionRespuestas.UsuarioCrea;
                            vSupervisionRespuestas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionRespuestas.FechaCrea;
                            vSupervisionRespuestas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionRespuestas.UsuarioModifica;
                            vSupervisionRespuestas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionRespuestas.FechaModifica;
                        }
                        return vSupervisionRespuestas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionRespuestas(string pValor)
        {
            try
            {
                int IdRespuesta = 0;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Respuestas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pValor);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            IdRespuesta = vDataReaderResults["IdRespuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRespuesta"].ToString()) : IdRespuesta;
                        }
                        return IdRespuesta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionRespuestas> ConsultarSupervisionRespuestass(int? pIdPregunta, String pValor, Boolean? pVulneraDerecho, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Respuestas_Consultar"))
                {
                    if (pIdPregunta != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pIdPregunta);
                    if (pValor != null || pValor!="")
                        vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pValor);
                    if (pVulneraDerecho != null)
                        vDataBase.AddInParameter(vDbCommand, "@VulneraDerecho", DbType.Boolean, pVulneraDerecho);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionRespuestas> vListaSupervisionRespuestas = new List<SupervisionRespuestas>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionRespuestas vSupervisionRespuestas = new SupervisionRespuestas();
                            vSupervisionRespuestas.IdRespuesta = vDataReaderResults["IdRespuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRespuesta"].ToString()) : vSupervisionRespuestas.IdRespuesta;
                            vSupervisionRespuestas.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vSupervisionRespuestas.IdPregunta;
                            vSupervisionRespuestas.Valor = vDataReaderResults["Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Valor"].ToString()) : vSupervisionRespuestas.Valor;
                            vSupervisionRespuestas.VulneraDerecho = vDataReaderResults["VulneraDerecho"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VulneraDerecho"]) : vSupervisionRespuestas.VulneraDerecho;
                            vSupervisionRespuestas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionRespuestas.Estado;
                            vSupervisionRespuestas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionRespuestas.UsuarioCrea;
                            vSupervisionRespuestas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionRespuestas.FechaCrea;
                            vSupervisionRespuestas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionRespuestas.UsuarioModifica;
                            vSupervisionRespuestas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionRespuestas.FechaModifica;
                            if (vSupervisionRespuestas.Estado == 1)
                            {
                                vSupervisionRespuestas.EstadoStr = "Activo";
                            }
                            else
                            {
                                vSupervisionRespuestas.EstadoStr = "Desactivado";
                            }

                            if (vSupervisionRespuestas.VulneraDerecho == true)
                            {
                                vSupervisionRespuestas.VulneraDerechoStr = "Si";
                            }
                            else
                            {
                                vSupervisionRespuestas.VulneraDerechoStr = "No";
                            }

                            vListaSupervisionRespuestas.Add(vSupervisionRespuestas);
                        }
                        return vListaSupervisionRespuestas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
