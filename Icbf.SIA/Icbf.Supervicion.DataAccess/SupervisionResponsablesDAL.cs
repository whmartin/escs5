using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionResponsablesDAL : GeneralDAL
    {
        public SupervisionResponsablesDAL()
        {
        }
        public int InsertarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Responsables_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdResponsable", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionResponsables.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionResponsables.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionResponsables.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionResponsables.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionResponsables.IdResponsable = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdResponsable").ToString());
                    GenerarLogAuditoria(pSupervisionResponsables, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Responsables_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsable", DbType.Int32, pSupervisionResponsables.IdResponsable);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionResponsables.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionResponsables.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionResponsables.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionResponsables.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionResponsables, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionResponsables_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsable", DbType.Int32, pSupervisionResponsables.IdResponsable);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionResponsables, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public SupervisionResponsables ConsultarSupervisionResponsables(int pIdResponsable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Responsables_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsable", DbType.Int32, pIdResponsable);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionResponsables vSupervisionResponsables = new SupervisionResponsables();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionResponsables.IdResponsable = vDataReaderResults["IdResponsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsable"].ToString()) : vSupervisionResponsables.IdResponsable;
                            vSupervisionResponsables.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionResponsables.IdDireccion;
                            vSupervisionResponsables.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionResponsables.Nombre;
                            vSupervisionResponsables.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionResponsables.Estado;
                            vSupervisionResponsables.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionResponsables.UsuarioCrea;
                            vSupervisionResponsables.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionResponsables.FechaCrea;
                            vSupervisionResponsables.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionResponsables.UsuarioModifica;
                            vSupervisionResponsables.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionResponsables.FechaModifica;
                        }
                        return vSupervisionResponsables;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ConsultarSupervisionResponsables(string Nombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Responsables_Consultar"))
                {
                    int IdResponsable = 0;
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, Nombre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            IdResponsable = vDataReaderResults["IdResponsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsable"].ToString()) : IdResponsable;
                        }
                        return IdResponsable;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionResponsables> ConsultarSupervisionResponsabless(int? pIdDireccion, String pNombre, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Responsables_Consultar"))
                {
                    if (pIdDireccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pIdDireccion);
                    if (pNombre != null && pNombre.Trim() != "")
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionResponsables> vListaSupervisionResponsables = new List<SupervisionResponsables>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionResponsables vSupervisionResponsables = new SupervisionResponsables();
                            vSupervisionResponsables.IdResponsable = vDataReaderResults["IdResponsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsable"].ToString()) : vSupervisionResponsables.IdResponsable;
                            vSupervisionResponsables.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionResponsables.IdDireccion;
                            vSupervisionResponsables.Direccion = vDataReaderResults["NombreDireccion"] != DBNull.Value ? vDataReaderResults["NombreDireccion"].ToString() : vSupervisionResponsables.Direccion;
                            vSupervisionResponsables.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionResponsables.Nombre;
                            vSupervisionResponsables.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionResponsables.Estado;
                            vSupervisionResponsables.EstadoStr = vDataReaderResults["NombreEstado"] != DBNull.Value ? vDataReaderResults["NombreEstado"].ToString() : vSupervisionResponsables.EstadoStr;
                            vSupervisionResponsables.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionResponsables.UsuarioCrea;
                            vSupervisionResponsables.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionResponsables.FechaCrea;
                            vSupervisionResponsables.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionResponsables.UsuarioModifica;
                            vSupervisionResponsables.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionResponsables.FechaModifica;
                            vListaSupervisionResponsables.Add(vSupervisionResponsables);
                        }
                        return vListaSupervisionResponsables;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
