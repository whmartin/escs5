using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.DataAccess
{
    public class SupervisionSubcomponentesDAL : GeneralDAL
    {
        public SupervisionSubcomponentesDAL()
        {
        }
        public int InsertarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Subcomponentes_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSubcomponente", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pSupervisionSubcomponentes.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionSubcomponentes.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionSubcomponentes.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisionSubcomponentes.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisionSubcomponentes.IdSubcomponente = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSubcomponente").ToString());
                    GenerarLogAuditoria(pSupervisionSubcomponentes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("dbo.usp_SIA_Contratos_Supervision_Subcomponentes_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSubcomponente", DbType.Int32, pSupervisionSubcomponentes.IdSubcomponente);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSupervisionSubcomponentes.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSupervisionSubcomponentes.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisionSubcomponentes.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionSubcomponentes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_SUP_SupervisionSubcomponentes_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSubcomponente", DbType.Int32, pSupervisionSubcomponentes.IdSubcomponente);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisionSubcomponentes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public SupervisionSubcomponentes ConsultarSupervisionSubcomponentes(int pIdSubcomponente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Subcomponentes_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSubcomponente", DbType.Int32, pIdSubcomponente);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionSubcomponentes vSupervisionSubcomponentes = new SupervisionSubcomponentes();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisionSubcomponentes.IdSubcomponente = vDataReaderResults["IdSubcomponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubcomponente"].ToString()) : vSupervisionSubcomponentes.IdSubcomponente;
                            vSupervisionSubcomponentes.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionSubcomponentes.IdDireccion;
                            vSupervisionSubcomponentes.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionSubcomponentes.Nombre;
                            vSupervisionSubcomponentes.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionSubcomponentes.Estado;
                            vSupervisionSubcomponentes.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionSubcomponentes.UsuarioCrea;
                            vSupervisionSubcomponentes.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionSubcomponentes.FechaCrea;
                            vSupervisionSubcomponentes.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionSubcomponentes.UsuarioModifica;
                            vSupervisionSubcomponentes.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionSubcomponentes.FechaModifica;
                        }
                        return vSupervisionSubcomponentes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionSubcomponentes(string Nombre)
        {
            try
            {
                int IdSubcomponente = 0;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Subcomponentes_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, Nombre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisionSubcomponentes vSupervisionSubcomponentes = new SupervisionSubcomponentes();
                        while (vDataReaderResults.Read())
                        {
                            IdSubcomponente = vDataReaderResults["IdSubcomponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubcomponente"].ToString()) : IdSubcomponente;
                        }
                        return IdSubcomponente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionSubcomponentes> ConsultarSupervisionSubcomponentess(int? pIdDireccion, String pNombre, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Subcomponentes_Consultar"))
                {
                    if (pIdDireccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pIdDireccion);
                    if (pNombre != null && pNombre != "")
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionSubcomponentes> vListaSupervisionSubcomponentes = new List<SupervisionSubcomponentes>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionSubcomponentes vSupervisionSubcomponentes = new SupervisionSubcomponentes();
                            vSupervisionSubcomponentes.IdSubcomponente = vDataReaderResults["IdSubcomponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubcomponente"].ToString()) : vSupervisionSubcomponentes.IdSubcomponente;
                            vSupervisionSubcomponentes.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vSupervisionSubcomponentes.IdDireccion;
                            vSupervisionSubcomponentes.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSupervisionSubcomponentes.Nombre;
                            vSupervisionSubcomponentes.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSupervisionSubcomponentes.Estado;
                            vSupervisionSubcomponentes.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisionSubcomponentes.UsuarioCrea;
                            vSupervisionSubcomponentes.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisionSubcomponentes.FechaCrea;
                            vSupervisionSubcomponentes.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisionSubcomponentes.UsuarioModifica;
                            vSupervisionSubcomponentes.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisionSubcomponentes.FechaModifica;
                            vSupervisionSubcomponentes.Direccion = vDataReaderResults["NombreDireccion"] != DBNull.Value ? vDataReaderResults["NombreDireccion"].ToString() : vSupervisionSubcomponentes.Nombre;
                            vSupervisionSubcomponentes.EstadoStr = vDataReaderResults["NombreEstado"] != DBNull.Value ? vDataReaderResults["NombreEstado"].ToString() : vSupervisionSubcomponentes.EstadoStr;

                            vListaSupervisionSubcomponentes.Add(vSupervisionSubcomponentes);
                        }
                        return vListaSupervisionSubcomponentes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionComponentes> ConsultarSupervisionComponentes(int? pIdComponente, int? pCodComponente, String pNombreComponente, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Supervision_Componentes_Consultar"))
                {
                    if (pIdComponente != null) { vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int16, pIdComponente); }
                    if (pCodComponente != null) { vDataBase.AddInParameter(vDbCommand, "@CodComponente", DbType.Int32, pCodComponente); }
                    if (pNombreComponente != null && pNombreComponente != "") { vDataBase.AddInParameter(vDbCommand, "@NombreComponente", DbType.String, pNombreComponente); }
                    if (pEstado != null) { vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado); }
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisionComponentes> vListaSupervisionComponentes = new List<SupervisionComponentes>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisionComponentes vSupervicionComponentes = new SupervisionComponentes();
                            vSupervicionComponentes.IdComponente = vDataReaderResults["IdComponente"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["IdComponente"].ToString()) : vSupervicionComponentes.IdComponente;
                            vSupervicionComponentes.CodComponente = vDataReaderResults["CodComponente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodComponente"].ToString()) : vSupervicionComponentes.CodComponente;
                            vSupervicionComponentes.NombreComponente = vDataReaderResults["NombreComponente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreComponente"].ToString()) : vSupervicionComponentes.NombreComponente;
                            vSupervicionComponentes.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Estado"].ToString()) : vSupervicionComponentes.Estado;
                            vSupervicionComponentes.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervicionComponentes.UsuarioCrea;
                            vSupervicionComponentes.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervicionComponentes.FechaCrea;
                            vSupervicionComponentes.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervicionComponentes.UsuarioModifica;
                            vSupervicionComponentes.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervicionComponentes.FechaModifica;

                            vListaSupervisionComponentes.Add(vSupervicionComponentes);
                        }
                        return vListaSupervisionComponentes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
    }
}
