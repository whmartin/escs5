using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.DataAccess;
using Icbf.SIGUV.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.Business
{
    public class VehiculosBLL
    {
        private VehiculosDAL vVehiculosDAL;
        public VehiculosBLL()
        {
            vVehiculosDAL = new VehiculosDAL();
        }
        public int InsertarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                return vVehiculosDAL.InsertarVehiculos(pVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                return vVehiculosDAL.ModificarVehiculos(pVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                return vVehiculosDAL.EliminarVehiculos(pVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Vehiculos ConsultarVehiculos(int pIdVehiculo)
        {
            try
            {
                return vVehiculosDAL.ConsultarVehiculos(pIdVehiculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Vehiculos> ConsultarVehiculoss(int? pPropio, int? pRelevo, int? pIdClase, String pPlaca, int? pCilindraje, int? pModelo, int? pCapacidad, int? pEstado)
        {
            try
            {
                return vVehiculosDAL.ConsultarVehiculoss(pPropio, pRelevo, pIdClase, pPlaca, pCilindraje, pModelo, pCapacidad, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
