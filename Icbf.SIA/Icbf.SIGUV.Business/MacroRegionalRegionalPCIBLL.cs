using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.DataAccess;
using Icbf.SIGUV.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.Business
{
    public class MacroRegionalRegionalPCIBLL
    {
        private MacroRegionalRegionalPCIDAL vMacroRegionalRegionalPCIDAL;
        public MacroRegionalRegionalPCIBLL()
        {
            vMacroRegionalRegionalPCIDAL = new MacroRegionalRegionalPCIDAL();
        }
        public int InsertarMacroRegionalRegionalPCI(MacroRegionalRegionalPCI pMacroRegionalRegionalPCI)
        {
            try
            {
                return vMacroRegionalRegionalPCIDAL.InsertarMacroRegionalRegionalPCI(pMacroRegionalRegionalPCI);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarMacroRegionalRegionalPCI(MacroRegionalRegionalPCI pMacroRegionalRegionalPCI)
        {
            try
            {
                return vMacroRegionalRegionalPCIDAL.ModificarMacroRegionalRegionalPCI(pMacroRegionalRegionalPCI);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarMacroRegionalRegionalPCI(MacroRegionalRegionalPCI pMacroRegionalRegionalPCI)
        {
            try
            {
                return vMacroRegionalRegionalPCIDAL.EliminarMacroRegionalRegionalPCI(pMacroRegionalRegionalPCI);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public MacroRegionalRegionalPCI ConsultarMacroRegionalRegionalPCI(int pIdMacroRegionalRegionalPCI)
        {
            try
            {
                return vMacroRegionalRegionalPCIDAL.ConsultarMacroRegionalRegionalPCI(pIdMacroRegionalRegionalPCI);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<MacroRegionalRegionalPCI> ConsultarMacroRegionalRegionalPCIs(int? pIdMacroRegional, int? pIdRegionalPCI)
        {
            try
            {
                return vMacroRegionalRegionalPCIDAL.ConsultarMacroRegionalRegionalPCIs(pIdMacroRegional, pIdRegionalPCI);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
