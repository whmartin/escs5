using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.DataAccess;
using Icbf.SIGUV.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.Business
{
    public class RelevoBLL
    {
        private RelevoDAL vRelevoDAL;
        public RelevoBLL()
        {
            vRelevoDAL = new RelevoDAL();
        }
        public int InsertarRelevo(Relevo pRelevo)
        {
            try
            {
                return vRelevoDAL.InsertarRelevo(pRelevo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarRelevo(Relevo pRelevo)
        {
            try
            {
                return vRelevoDAL.ModificarRelevo(pRelevo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarRelevo(Relevo pRelevo)
        {
            try
            {
                return vRelevoDAL.EliminarRelevo(pRelevo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Relevo ConsultarRelevo(int pIdRelevo)
        {
            try
            {
                return vRelevoDAL.ConsultarRelevo(pIdRelevo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Relevo> ConsultarRelevos(int? pIdContratoSiguv, int? pIdVehiculoPrincipal, int? pIdVehiculoReemplazo, DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                return vRelevoDAL.ConsultarRelevos(pIdContratoSiguv, pIdVehiculoPrincipal, pIdVehiculoReemplazo, pFechaInicio, pFechaFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
