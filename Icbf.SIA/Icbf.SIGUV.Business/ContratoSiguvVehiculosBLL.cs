using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.DataAccess;
using Icbf.SIGUV.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.Business
{
    public class ContratoSiguvVehiculosBLL
    {
        private ContratoSiguvVehiculosDAL vContratoSiguvVehiculosDAL;
        public ContratoSiguvVehiculosBLL()
        {
            vContratoSiguvVehiculosDAL = new ContratoSiguvVehiculosDAL();
        }
        public int InsertarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                return vContratoSiguvVehiculosDAL.InsertarContratoSiguvVehiculos(pContratoSiguvVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                return vContratoSiguvVehiculosDAL.ModificarContratoSiguvVehiculos(pContratoSiguvVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                return vContratoSiguvVehiculosDAL.EliminarContratoSiguvVehiculos(pContratoSiguvVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ContratoSiguvVehiculos ConsultarContratoSiguvVehiculos(int pIdContratoSiguvVehiculos)
        {
            try
            {
                return vContratoSiguvVehiculosDAL.ConsultarContratoSiguvVehiculos(pIdContratoSiguvVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoSiguvVehiculos> ConsultarContratoSiguvVehiculoss(int? pIdContratoSiguv, int? pIdVehiculo, int? pIdRegionalPCI, int? pIdCentroZonal)
        {
            try
            {
                return vContratoSiguvVehiculosDAL.ConsultarContratoSiguvVehiculoss(pIdContratoSiguv, pIdVehiculo, pIdRegionalPCI, pIdCentroZonal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
