using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.DataAccess;
using Icbf.SIGUV.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.Business
{
    public class ContratoSiguvBLL
    {
        private ContratoSiguvDAL vContratoSiguvDAL;
        public ContratoSiguvBLL()
        {
            vContratoSiguvDAL = new ContratoSiguvDAL();
        }
        public int InsertarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                return vContratoSiguvDAL.InsertarContratoSiguv(pContratoSiguv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                return vContratoSiguvDAL.ModificarContratoSiguv(pContratoSiguv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                return vContratoSiguvDAL.EliminarContratoSiguv(pContratoSiguv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ContratoSiguv ConsultarContratoSiguv(int pIdContratoSiguv)
        {
            try
            {
                return vContratoSiguvDAL.ConsultarContratoSiguv(pIdContratoSiguv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoSiguv> ConsultarContratoSiguvs(int? pIdRegionalPCI, int? pIdMacroRegional, int? pIdContrato)
        {
            try
            {
                return vContratoSiguvDAL.ConsultarContratoSiguvs(pIdRegionalPCI, pIdMacroRegional, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
