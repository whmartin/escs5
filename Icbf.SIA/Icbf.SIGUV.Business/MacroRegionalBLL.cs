using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.DataAccess;
using Icbf.SIGUV.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.Business
{
    public class MacroRegionalBLL
    {
        private MacroRegionalDAL vMacroRegionalDAL;
        public MacroRegionalBLL()
        {
            vMacroRegionalDAL = new MacroRegionalDAL();
        }
        public int InsertarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                return vMacroRegionalDAL.InsertarMacroRegional(pMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                return vMacroRegionalDAL.ModificarMacroRegional(pMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                return vMacroRegionalDAL.EliminarMacroRegional(pMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public MacroRegional ConsultarMacroRegional(int pIdMacroRegional)
        {
            try
            {
                return vMacroRegionalDAL.ConsultarMacroRegional(pIdMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<MacroRegional> ConsultarMacroRegionals(String pNombre, int? pEstado)
        {
            try
            {
                return vMacroRegionalDAL.ConsultarMacroRegionals(pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<MacroRegional> ConsultarMacroRegionalMacroRegionalPCI(int pIdMacroRegional)
        {
            try
            {
                return vMacroRegionalDAL.ConsultarMacroRegionalMacroRegionalPCI(pIdMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
