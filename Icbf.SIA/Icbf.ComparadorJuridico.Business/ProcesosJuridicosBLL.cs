using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    public class ProcesosJuridicosBLL
    {
        private ProcesosJuridicosDAL vProcesosJuridicosDAL;
        public ProcesosJuridicosBLL()
        {
            vProcesosJuridicosDAL = new ProcesosJuridicosDAL();
        }
        public int InsertarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicosDAL.InsertarProcesosJuridicos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicosDAL.ModificarProcesosJuridicos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicosDAL.EliminarProcesosJuridicos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ProcesosJuridicos ConsultarProcesosJuridicos(int pIdProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicosDAL.ConsultarProcesosJuridicos(pIdProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesosJuridicos> ConsultarProcesosJuridicoss(int? pIdRegional, int? pIdVigencia, int? pIdProceso, 
            int? pIdTercero, string pIdPlanContable, string pNumeroProceso, string pValorProceso, int? pEstado,
            int? pIdNaturaleza, int? pIdFase, int? pIdSubFase)
        {
            try
            {
                return vProcesosJuridicosDAL.ConsultarProcesosJuridicoss(pIdRegional, pIdVigencia, pIdProceso, pIdTercero, 
                    pIdPlanContable, pNumeroProceso, pValorProceso, pEstado, pIdNaturaleza, pIdFase, pIdSubFase);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar el correo del contador.
        /// </summary>
        /// <param name="pIdRegional">Parametro de consulta por id de regional</param>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoContador(int? pIdRegional, string pProviderKey)
        {
            try
            {
                return vProcesosJuridicosDAL.ConsultarCorreoContador(pIdRegional, pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar el correo del administrador.
        /// </summary>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoAdministrador(string pProviderKey)
        {
            try
            {
                return vProcesosJuridicosDAL.ConsultarCorreoAdministrador(pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta el CodigoCuenta y Descripcion del Plan Contable
        /// </summary>
        /// <param name="pIdNaturalezaProceso"></param>
        /// <param name="pIdFaseProceso"></param>
        /// <param name="pIdSubFaseProceso"></param>
        /// <param name="pIdProceso"></param>
        /// <returns></returns>
        public ProcesosJuridicos ConsultarProcesosJuridicosPlanContable(int pIdNaturalezaProceso,
            int pIdFaseProceso, int pIdSubFaseProceso, int pIdProceso)
        {
            try
            {
                return vProcesosJuridicosDAL.ConsultarProcesosJuridicosPlanContable(pIdNaturalezaProceso,
                    pIdFaseProceso, pIdSubFaseProceso, pIdProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta si existe registros con la misma informaci�n
        /// </summary>
        /// <param name="pIdProceso">Id del Proceso</param>
        /// <param name="pIdNaturalezaProceso">Descripci�n de la Naturaleza del proceso</param>
        /// <param name="pIdFaseProceso">Descripci�n de la Fase del proceso</param>
        /// <param name="pIdSubFaseProceso">Descripci�n de la SubFase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarCombinacion(int pIdProceso, int? pIdNaturalezaProceso, int? pIdFaseProceso, int? pIdSubFaseProceso, int? pIdProcesosJuridicos)
        {
            try
            {
                return this.vProcesosJuridicosDAL.ConsultarCombinacion(pIdProceso, pIdNaturalezaProceso, pIdFaseProceso, pIdSubFaseProceso, pIdProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
