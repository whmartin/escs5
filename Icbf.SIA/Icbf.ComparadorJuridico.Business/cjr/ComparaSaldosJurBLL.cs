﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Icbf.ComparadorJuridico.Entity;
using System.Data.OleDb;


namespace Icbf.ComparadorJuridico.Business.cjr
{
    public class ComparaSaldosJurBLL
    {

        public DataTable GetTblComparaSaldosJur(int parIdAno, int parIdMes, int parRegional, string parCuenta)
        {
            Icbf.ComparadorJuridico.DataAccess.cjr.ComparaSaldosJurDAL oComparaSaldosJur = new Icbf.ComparadorJuridico.DataAccess.cjr.ComparaSaldosJurDAL();
            return oComparaSaldosJur.GetTblComparaSaldosJur(parIdAno, parIdMes, parRegional, parCuenta);
        }
    }
}
