﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Icbf.ComparadorJuridico.Entity;
using System.Data.OleDb;

namespace Icbf.ComparadorJuridico.Business.cjr
{
    public class CabSIIFSaldosBLL
    {
        public string strMensaje;
        public Icbf.ComparadorJuridico.Entity.cjr.CabSIIFSaldos oCabSIIFSaldos;

        public int procesarArchivo(int parIdAno, int parIdMes, string parStrArchivo, int parIdUsuario)
        {
            int NroReg = 0;
            int IdCabProc = 0;
            strMensaje = "";
            DataTable dtTable;
            dtTable = leerArchivo(parStrArchivo);

            if (dtTable.Columns.Contains("INSP_str_Identificacion_1") && dtTable.Columns.Contains("Codigo") &&
                dtTable.Columns.Contains("Descripcion") && dtTable.Columns.Contains("Saldo_Final"))
            {

                oCabSIIFSaldos = new Icbf.ComparadorJuridico.Entity.cjr.CabSIIFSaldos();
                oCabSIIFSaldos.csfIdCabSaldosSIIF = 0;
                oCabSIIFSaldos.csfNomArh = System.IO.Path.GetFileName(parStrArchivo);
                oCabSIIFSaldos.csfIdAno = parIdAno;
                oCabSIIFSaldos.csfIdMes = parIdMes;
                oCabSIIFSaldos.csfIdEstado = 1;
                oCabSIIFSaldos.csfAudIdUsuario = parIdUsuario;

                Icbf.ComparadorJuridico.DataAccess.cjr.CabSIIFSaldosDAL oCabSIIFSaldosDAL = new Icbf.ComparadorJuridico.DataAccess.cjr.CabSIIFSaldosDAL();
                IdCabProc = oCabSIIFSaldosDAL.Guarda (oCabSIIFSaldos);

                Icbf.ComparadorJuridico.DataAccess.cjr.dtsSIIFSaldos.dtSIIFSaldosDataTable dtSaldoSIIF = new Icbf.ComparadorJuridico.DataAccess.cjr.dtsSIIFSaldos.dtSIIFSaldosDataTable();

                Decimal Saldo_Inicial;
                Decimal Movimientos_Debito;
                Decimal Movimientos_Credito;
                Decimal Saldo_Final;

                foreach (DataRow drFila in dtTable.Rows)
                {
                    if (drFila["Codigo"].ToString().Length > 0)
                    {
                        Saldo_Inicial = 0;
                        Movimientos_Debito = 0;
                        Movimientos_Credito = 0;
                        Saldo_Final = 0;

                        if (decimal.TryParse(drFila["Saldo_Inicial"].ToString(), out Saldo_Inicial))
                        {
                            Saldo_Inicial = decimal.Parse(drFila["Saldo_Inicial"].ToString());
                        }
                        else
                        {
                            Saldo_Inicial = 0;
                        }

                        if (decimal.TryParse(drFila["Movimientos_Debito"].ToString(), out Movimientos_Debito))
                        {
                            Movimientos_Debito = decimal.Parse(drFila["Movimientos_Debito"].ToString());
                        }
                        else
                        {
                            Movimientos_Debito = 0;
                        }

                        if (decimal.TryParse(drFila["Movimientos_Credito"].ToString(), out Movimientos_Credito))
                        {
                            Movimientos_Credito = decimal.Parse(drFila["Movimientos_Credito"].ToString());
                        }
                        else
                        {
                            Movimientos_Credito = 0;
                        }

                        if (decimal.TryParse(drFila["Saldo_Final"].ToString(), out Saldo_Final))
                        {
                            Saldo_Final = decimal.Parse(drFila["Saldo_Final"].ToString());
                        }
                        else
                        {
                            Saldo_Final = 0;
                        }

                        dtSaldoSIIF.AdddtSIIFSaldosRow(IdCabProc, drFila["INSP_str_Identificacion_1"].ToString(), drFila["Codigo"].ToString(),
                                drFila["Descripcion"].ToString(), Saldo_Inicial, Movimientos_Debito, Movimientos_Credito, Saldo_Final);
                    }
                }
                if (dtSaldoSIIF.Rows.Count > 0)
                {
                    oCabSIIFSaldosDAL.GuardaDet(dtSaldoSIIF);

                    // Actualiza en la tabla definitiva desde donde se va a hacer la comparacion
                    oCabSIIFSaldosDAL.ActualizaCarga(IdCabProc);
                }



                NroReg = dtTable.Rows.Count;

            }
            else
            {
                strMensaje = "Estructura No Valida";
                NroReg = 0;
            }
            return NroReg;
        }

        public System.Data.DataTable leerArchivo(string parStrArchivo)
        {
            String strConn = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
             @"Data Source=" + parStrArchivo + @" ;Extended Properties='Excel 12.0;HDR=YES;IMEX=1;MAXSCANROWS=0;'";

            OleDbConnection Conn = new OleDbConnection(strConn);
            Conn.Open();
            string sTblname = Conn.GetSchema("Tables").Rows[0]["TABLE_NAME"].ToString();
            Conn.Close();

            OleDbCommand Comando = new OleDbCommand();
            Comando.CommandText = String.Format("SELECT * FROM [{0}]", sTblname);

            DataSet da = new DataSet();
            OleDbDataAdapter objda = new OleDbDataAdapter(Comando.CommandText, strConn);
            objda.Fill(da);
            return da.Tables[0];
        }


        public DataTable ConsSIIFSaldos(int parAno, int parMes)
        {
            Icbf.ComparadorJuridico.DataAccess.cjr.CabSIIFSaldosDAL oCabSIIFSaldosDAL = new Icbf.ComparadorJuridico.DataAccess.cjr.CabSIIFSaldosDAL();
            return oCabSIIFSaldosDAL.ConsSIIFSaldos(parAno, parMes);
        }

        public DataTable ConsDetSIIFSaldos(int parIdCabSaldoSIIF)
        {
            Icbf.ComparadorJuridico.DataAccess.cjr.CabSIIFSaldosDAL oCabSIIFSaldosDAL = new Icbf.ComparadorJuridico.DataAccess.cjr.CabSIIFSaldosDAL();
            //DataTable dtDAtos;
            //dtDAtos = oCabSIIFSaldosDAL.ConsDetSIIFSaldos(parIdCabSaldoSIIF);
            return oCabSIIFSaldosDAL.ConsDetSIIFSaldos(parIdCabSaldoSIIF);
            
        }

    }
}
