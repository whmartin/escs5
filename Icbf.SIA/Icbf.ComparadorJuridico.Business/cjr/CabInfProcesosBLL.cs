﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Icbf.ComparadorJuridico.Entity;
using System.Data.OleDb;

namespace Icbf.ComparadorJuridico.Business
{
    public class CabInfProcesosBLL
    {
        
        public string strMensaje;
        public Icbf.ComparadorJuridico.Entity.cjr.CabInfProcesos oCabInfProceso;

        public int procesarArchivo(int parIdAno, int parIdMes, string parStrArchivo, int parIdUsuario)
        {
            int NroReg = 0;
            int IdCabProc = 0;
            strMensaje = "";
            DataTable  dtTable;
            dtTable = leerArchivo(parStrArchivo);

            if (dtTable.Columns.Contains("CUENTA") && dtTable.Columns.Contains("NOMCUENTA") && dtTable.Columns.Contains("REGIONAL") &&
                dtTable.Columns.Contains("SALDOJURIDICA") && dtTable.Columns.Contains("SALDO6DIGITOS"))
            {
                oCabInfProceso = new  Icbf.ComparadorJuridico.Entity.cjr.CabInfProcesos();
                oCabInfProceso.cipIdCabInfProcesos = 0;
                oCabInfProceso.cipNomArch = System.IO.Path.GetFileName(parStrArchivo);
                oCabInfProceso.cipIdAno = parIdAno;
                oCabInfProceso.cipIdMes = parIdMes;
                oCabInfProceso.cipIdEstado =1;
                oCabInfProceso.cipAudIdUsuario = parIdUsuario;
                
                Icbf.ComparadorJuridico.DataAccess.cjr.CabInfProcesosDAL oCabInfProcesosDAL = new Icbf.ComparadorJuridico.DataAccess.cjr.CabInfProcesosDAL();
                IdCabProc = oCabInfProcesosDAL.Guarda(oCabInfProceso);

                Icbf.ComparadorJuridico.DataAccess.cjr.dtsInfProcesos.dtInfProcesosDataTable dtInfPeocesos = new Icbf.ComparadorJuridico.DataAccess.cjr.dtsInfProcesos.dtInfProcesosDataTable();

                Decimal lSaldoJur;
                Decimal lSaldo6Dig;

                foreach (DataRow drFila in dtTable.Rows)                    
                {
                    if (drFila["CUENTA"].ToString().Length > 0)
                    {
                        lSaldoJur = 0;
                        lSaldo6Dig = 0;
                        if (drFila["SALDOJURIDICA"] is decimal)
                        {
                            lSaldoJur = decimal.Parse(drFila["SALDOJURIDICA"].ToString());
                        }
                        else
                        {
                            lSaldoJur = 0;
                        }

                        if (drFila["SALDO6DIGITOS"] is decimal)
                        {
                            lSaldo6Dig = decimal.Parse(drFila["SALDO6DIGITOS"].ToString());
                        }
                        else
                        {
                            lSaldo6Dig = 0;
                        }
                            
                        dtInfPeocesos.AdddtInfProcesosRow(IdCabProc, drFila["CUENTA"].ToString(), drFila["NOMCUENTA"].ToString(),
                                drFila["REGIONAL"].ToString(), lSaldoJur, lSaldo6Dig);
                    }                   
                }
                if (dtInfPeocesos.Rows.Count >0)
                {
                    oCabInfProcesosDAL.GuardaDet(dtInfPeocesos);

                    // Actualiza en la tabla definitiva desde donde se va a hacer la comparacion
                    oCabInfProcesosDAL.ActualizaCarga(IdCabProc);
                }

                

                NroReg = dtTable.Rows.Count;

            }
            else 
            {
                strMensaje = "Estructura No Valida";
                NroReg = 0;
            }
            return NroReg;
        }

        public System.Data.DataTable leerArchivo(string parStrArchivo)
        {
            String strConn = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
             @"Data Source=" + parStrArchivo + @" ;Extended Properties='Excel 12.0;HDR=YES;IMEX=1;MAXSCANROWS=0;'";

            OleDbConnection Conn = new OleDbConnection(strConn);
            Conn.Open();
            string sTblname = Conn.GetSchema("Tables").Rows[0]["TABLE_NAME"].ToString();
            Conn.Close();

            OleDbCommand Comando = new OleDbCommand();
            Comando.CommandText = String.Format("SELECT * FROM [{0}]", sTblname);

            DataSet da = new DataSet();
            OleDbDataAdapter objda = new OleDbDataAdapter(Comando.CommandText, strConn);
            objda.Fill(da);
            return da.Tables[0];
        }

        public DataTable ConsInfProcesoXAnoMes(int parAno, int parMes)
        {
            Icbf.ComparadorJuridico.DataAccess.cjr.CabInfProcesosDAL oCabInfProcesosDAL = new Icbf.ComparadorJuridico.DataAccess.cjr.CabInfProcesosDAL();
            return oCabInfProcesosDAL.ConsInfProcesoXAnoMes(parAno, parMes);       
        }

        public DataTable DetTmpInfProcesosConsulta(int parIdCabInfProcesos)
        {
            Icbf.ComparadorJuridico.DataAccess.cjr.CabInfProcesosDAL oCabInfProcesosDAL = new Icbf.ComparadorJuridico.DataAccess.cjr.CabInfProcesosDAL();
            return oCabInfProcesosDAL.DetTmpInfProcesosConsulta(parIdCabInfProcesos);       
        
        }
    }
}
