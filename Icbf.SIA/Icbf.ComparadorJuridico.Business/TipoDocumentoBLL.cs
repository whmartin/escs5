using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;

namespace Icbf.ComparadorJuridico.Business
{
    public class TipoDocumentoBLL
    {
        private TipoDocumentoDAL vTipoDocumentoDAL;
        public TipoDocumentoBLL()
        {
            vTipoDocumentoDAL = new TipoDocumentoDAL();
        }
        public int InsertarTipoDocumento(TipoDocumento pTipoDocumento)
        {
            try
            {
                return vTipoDocumentoDAL.InsertarTipoDocumento(pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTipoDocumento(TipoDocumento pTipoDocumento)
        {
            try
            {
                return vTipoDocumentoDAL.ModificarTipoDocumento(pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTipoDocumento(TipoDocumento pTipoDocumento)
        {
            try
            {
                return vTipoDocumentoDAL.EliminarTipoDocumento(pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public TipoDocumento ConsultarTipoDocumento(int pIdTipoDocumento)
        {
            try
            {
                return vTipoDocumentoDAL.ConsultarTipoDocumento(pIdTipoDocumento);  
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TipoDocumento> ConsultarTodosTipoDocumento()
        {
            try
            {
                return vTipoDocumentoDAL.ConsultarTodosTipoDocumento();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
