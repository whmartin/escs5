﻿//-----------------------------------------------------------------------
// <copyright file="NaturalezaProcesoBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase NaturalezaProcesoBLL.</summary>
// <author>Ingenian Software</author>
// <date>18/01/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo NaturalezaProcesoBLL
    /// </summary>
    public class NaturalezaProcesoBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private NaturalezaProcesoDAL vNaturalezaProcesoDAL;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public NaturalezaProcesoBLL()
        {
            this.vNaturalezaProcesoDAL = new NaturalezaProcesoDAL();
        }

        /// <summary>
        /// Consulta los datos de la Naturaleza del Proceso
        /// </summary>
        /// <param name="pCodigoNaturaleza">Código de la Naturaleza</param>
        /// <param name="pDescripcionNaturaleza">Descripción de la Naturaleza</param>        
        /// <param name="pEstado">Estado de la Naturaleza</param>
        /// <returns>Lista de Registros de Naturaleza del Proceso</returns>
        public List<NaturalezaProceso> ConsultarNaturalezaProceso(int? pIdNaturalezaProceso, string pCodigoNaturaleza, string pDescripcionNaturaleza, int? pEstado)
        {
            try
            {
                return this.vNaturalezaProcesoDAL.ConsultarNaturalezaProceso(pIdNaturalezaProceso, pCodigoNaturaleza, pDescripcionNaturaleza, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de Naturaleza Proceso
        /// </summary>
        /// <param name="pIdNaturaleza">Id de la Naturaleza del proceso</param>
        /// <param name="pDescripcionNaturalezaProceso">Nombre de la naturaleza del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarNaturalezaProcesoNombre(int? pIdNaturaleza, string pDescripcionNaturalezaProceso)
        {
            try
            {
                return this.vNaturalezaProcesoDAL.ConsultarNaturalezaProcesoNombre(pIdNaturaleza, pDescripcionNaturalezaProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de Naturaleza del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarNaturalezaproceso(NaturalezaProceso pNaturalezaProceso)
        {
            try
            {
                return this.vNaturalezaProcesoDAL.InsertarNaturalezaproceso(pNaturalezaProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Método para la modificación de Naturaleza del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarNaturalezaproceso(NaturalezaProceso pNaturalezaProceso)
        {
            try
            {
                return this.vNaturalezaProcesoDAL.ModificarNaturalezaproceso(pNaturalezaProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la Naturaleza
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoNaturaleza()
        {
            try
            {
                return this.vNaturalezaProcesoDAL.ObtenerCodigoNaturaleza();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la Naturaleza del Proceso Activos y concatenados por codigo y descripción
        /// </summary>
        /// <returns>Lista de Registros de Naturaleza del Proceso</returns>
        public List<NaturalezaProceso> ConsultarNaturalezaProcesoConcatenado()
        {
            try
            {
                return this.vNaturalezaProcesoDAL.ConsultarNaturalezaProcesoConcatenado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
