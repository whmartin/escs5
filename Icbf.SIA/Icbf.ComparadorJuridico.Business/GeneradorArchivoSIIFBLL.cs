using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  GeneradorArchivoSIIF
    /// </summary>
    public class GeneradorArchivoSIIFBLL
    {
        private readonly GeneradorArchivoSIIFDAL _vGeneradorArchivoSiifdal;
        public GeneradorArchivoSIIFBLL()
        {
            _vGeneradorArchivoSiifdal = new GeneradorArchivoSIIFDAL();
        }
        ///// <summary>
        ///// Método de inserción para la entidad GeneradorArchivoSIIF
        ///// </summary>
        ///// <param name="pGeneradorArchivoSIIF"></param>
        //public int InsertarGeneradorArchivoSIIF(GeneradorArchivoSIIF pGeneradorArchivoSIIF)
        //{
        //    try
        //    {
        //        return vGeneradorArchivoSIIFDAL.InsertarGeneradorArchivoSIIF(pGeneradorArchivoSIIF);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //         throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de modificación para la entidad GeneradorArchivoSIIF
        ///// </summary>
        ///// <param name="pGeneradorArchivoSIIF"></param>
        //public int ModificarGeneradorArchivoSIIF(GeneradorArchivoSIIF pGeneradorArchivoSIIF)
        //{
        //    try
        //    {
        //        return vGeneradorArchivoSIIFDAL.ModificarGeneradorArchivoSIIF(pGeneradorArchivoSIIF);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //         throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de eliminación para la entidad GeneradorArchivoSIIF
        ///// </summary>
        ///// <param name="pGeneradorArchivoSIIF"></param>
        //public int EliminarGeneradorArchivoSIIF(GeneradorArchivoSIIF pGeneradorArchivoSIIF)
        //{
        //    try
        //    {
        //        return vGeneradorArchivoSIIFDAL.EliminarGeneradorArchivoSIIF(pGeneradorArchivoSIIF);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //         throw new GenericException(ex);
        //    }
        //}

        ///// <summary>
        ///// Método de consulta por id para la entidad GeneradorArchivoSIIF
        ///// </summary>
        ///// <param name="pIdGeneradorArchivoSIIF"></param>
        //public GeneradorArchivoSIIF ConsultarGeneradorArchivoSIIF(int pIdGeneradorArchivoSIIF)
        //{
        //    try
        //    {
        //        return vGeneradorArchivoSIIFDAL.ConsultarGeneradorArchivoSIIF(pIdGeneradorArchivoSIIF);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Método de consulta por filtros para la entidad GeneradorArchivoSIIF
        /// </summary>
        /// <param name="pTipoArchivo"></param>
        /// <param name="pFechaGeneracion"></param>
        /// <param name="pNombreArchivo"></param>
        /// <param name="pIdRegional"></param>
        /// <param name="pFechaInicio"></param>
        /// <param name="pFechaFin"></param>
        public List<GeneradorArchivoSIIF> ConsultarGeneradorArchivoSIIFs(String pTipoArchivo, DateTime? pFechaGeneracion, String pNombreArchivo, int? pIdRegional, DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                return _vGeneradorArchivoSiifdal.ConsultarGeneradorArchivoSIIFs(pTipoArchivo, pFechaGeneracion, pNombreArchivo, pIdRegional, pFechaInicio, pFechaFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public DataSet ConsultarRegistrosAGenerarSiiF(DateTime pFechaInicialMov, DateTime pFechaFinMov, int pIdRegional)
        {
            try
            {
                return _vGeneradorArchivoSiifdal.ConsultarRegistrosAGenerarSiiF(pFechaInicialMov, pFechaFinMov, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataSet GeneracionArchivoJuridica(DateTime pFechaInicialMov, DateTime pFechaFinMov, int pIdRegional, string usuario)
        {
            try
            {
                return _vGeneradorArchivoSiifdal.GeneracionArchivoJuridica(pFechaInicialMov, pFechaFinMov, pIdRegional, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
