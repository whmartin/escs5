﻿//-----------------------------------------------------------------------
// <copyright file="ProcesosJuridicosDocumentosBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProcesosJuridicosDocumentosBLL.</summary>
// <author>Ingenian Software</author>
// <date>06/03/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo ProcesosJuridicosDocumentosBLL
    /// </summary>
    public class ProcesosJuridicosDocumentosBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private ProcesosJuridicosDocumentosDAL vProcesosJuridicosDocumentosDAL;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProcesosJuridicosDocumentosBLL()
        {
            this.vProcesosJuridicosDocumentosDAL = new ProcesosJuridicosDocumentosDAL();
        }

        /// <summary>
        /// Elimina los documentos Asociados al Proceso Juridico
        /// </summary>
        /// <param name="pProcesosJuridicos">Parametro que envía el IdProcesosJuridicos</param>
        /// <returns>Resultado de la Operación</returns>
        public int EliminarProcesosJuridicosDocumentos(ProcesosJuridicosDocumentos pProcesosJuridicos)
        {
            try
            {
                return this.vProcesosJuridicosDocumentosDAL.EliminarProcesosJuridicosDocumentos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta los documentos Asociados al Proceso Juridico
        /// </summary>
        /// <param name="pProcesosJuridicos">Parametro de tipo Entidad ProcesosJuridicosDocumentos</param>
        /// <returns>Resultado de la Operación</returns>
        public int InsertarProcesosJuridicosDocumentos(ProcesosJuridicosDocumentos pProcesosJuridicos)
        {
            try
            {
                return this.vProcesosJuridicosDocumentosDAL.InsertarProcesosJuridicosDocumentos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta los Documentos x IdProcesoJuridico
        /// </summary>
        /// <param name="pIdProcesoJuridico"></param>
        /// <returns>Lista de Documentos x Proceso Juridico</returns>
        public List<ProcesosJuridicosDocumentos> ConsultaDocumentosProcesosJuridicos(int pIdProcesoJuridico)
        {
            try
            {
                return this.vProcesosJuridicosDocumentosDAL.ConsultaDocumentosProcesosJuridicos(pIdProcesoJuridico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
