using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    public class PlanContableBLL
    {
        private PlanContableDAL vPlanContableDAL;
        public PlanContableBLL()
        {
            vPlanContableDAL = new PlanContableDAL();
        }
        public int InsertarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                return vPlanContableDAL.InsertarPlanContable(pPlanContable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                return vPlanContableDAL.ModificarPlanContable(pPlanContable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                return vPlanContableDAL.EliminarPlanContable(pPlanContable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public PlanContable ConsultarPlanContable(int pIdPlanContable)
        {
            try
            {
                return vPlanContableDAL.ConsultarPlanContable(pIdPlanContable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlanContable> ConsultarPlanContables(int? pCodigoCuenta, String pNombreCuenta, int? pCodigoSubcuenta, String pNombreSubcuenta, int? pCodigoTercerNivelCuenta, String pNombreTercerNivelCuenta)
        {
            try
            {
                return vPlanContableDAL.ConsultarPlanContables(pCodigoCuenta, pNombreCuenta, pCodigoSubcuenta, pNombreSubcuenta, pCodigoTercerNivelCuenta, pNombreTercerNivelCuenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
