namespace Icbf.ComparadorJuridico.Business
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using Entity;
    using Utilities.Exceptions;

    public class TipoProcesoBLL
    {
        private TipoProcesoDAL vTipoProcesoDAL;
        public TipoProcesoBLL()
        {
            vTipoProcesoDAL = new TipoProcesoDAL();
        }

        public List<TipoProceso> ConsultarTodosTipoProceso()
        {
            try
            {
                return vTipoProcesoDAL.ConsultarTodosTipoProceso();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
