using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    public class VigenciaBLL
    {
        private VigenciaDAL vVigenciaDAL;
        public VigenciaBLL()
        {
            vVigenciaDAL = new VigenciaDAL();
        }        

        public Vigencia ConsultarVigencia(int pIdVigencia)
        {
            try
            {
                return vVigenciaDAL.ConsultarVigencia(pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Vigencia> ConsultarVigencias(int? pVigencia, int? pIdMes, String pMes)
        {
            try
            {
                return vVigenciaDAL.ConsultarVigencias(pVigencia, pIdMes, pMes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta todas las vigencias
        /// </summary>
        /// <returns>Lista de Vigencias</returns>
        public List<Vigencia> ConsultarVigenciass()
        {
            try
            {
                return vVigenciaDAL.ConsultarVigenciass();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
