﻿//-----------------------------------------------------------------------
// <copyright file="SubFaseProcesoBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase SubFaseProcesoBLL.</summary>
// <author>Ingenian Software</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo SubFaseProcesoBLL
    /// </summary>
    public class SubFaseProcesoBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private SubFaseProcesoDAL vSubFaseProcesoDAL;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public SubFaseProcesoBLL()
        {
            this.vSubFaseProcesoDAL = new SubFaseProcesoDAL();
        }

        /// <summary>
        /// Consulta los datos de la SubFase del Proceso
        /// </summary>
        /// <param name="pIdSubFaseProceso">Id de la SubFase</param>
        /// <param name="pCodigoSubFaseProceso">Código de la SubFase</param>
        /// <param name="pDescripcionSubFaseProceso">Descripción de la SubFase</param>
        /// <param name="pEstado">Estado de la SubFase</param>
        /// <returns>Lista de Registros de SubFase del Proceso</returns>
        public List<SubFaseProceso> ConsultarSubFaseProceso(int? pIdSubFaseProceso, string pCodigoSubFaseProceso, string pDescripcionSubFaseProceso, int pEstado)
        {
            try
            {
                return this.vSubFaseProcesoDAL.ConsultarSubFaseProceso(pIdSubFaseProceso, pCodigoSubFaseProceso, pDescripcionSubFaseProceso, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de SubFase Proceso
        /// </summary>
        /// <param name="pIdSubFaseProceso">Id de la SubFase del proceso</param>
        /// <param name="pDescripcionSubFaseProceso">Nombre de la SubFase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarSubFaseProcesoNombre(int? pIdSubFaseProceso, string pDescripcionSubFaseProceso)
        {
            try
            {
                return this.vSubFaseProcesoDAL.ConsultarSubFaseProcesoNombre(pIdSubFaseProceso, pDescripcionSubFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de SubFase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarSubFaseproceso(SubFaseProceso pSubFaseProceso)
        {
            try
            {
                return this.vSubFaseProcesoDAL.InsertarSubFaseproceso(pSubFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la modificación de SubFase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarSubFaseproceso(SubFaseProceso pSubFaseProceso)
        {
            try
            {
                return this.vSubFaseProcesoDAL.ModificarSubFaseproceso(pSubFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la SubFase
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoSubFase()
        {
            try
            {
                return this.vSubFaseProcesoDAL.ObtenerCodigoSubFase();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la SubFase del Proceso Contactenado Código-Descripción
        /// </summary>
        /// <returns>Lista de Registros de SubFase del Proceso</returns>
        public List<SubFaseProceso> ConsultarSubFaseProcesoConcatenado()
        {
            try
            {
                return this.vSubFaseProcesoDAL.ConsultarSubFaseProcesoConcatenado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
