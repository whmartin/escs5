﻿//-----------------------------------------------------------------------
// <copyright file="PlanContableCJRBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase PlanContableCJRBLL.</summary>
// <author>Ingenian Software</author>
// <date>10/04/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo PlanContableCJRBLL
    /// </summary>
    public class PlanContableCJRBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private PlanContableCJRDAL vPlanContableCJRDAL;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public PlanContableCJRBLL()
        {
            this.vPlanContableCJRDAL = new PlanContableCJRDAL();
        }

        /// <summary>
        /// Consulta los datos del Plan Contable Concatenados
        /// </summary>
        /// <returns>Lista de Registros de Plan Contable Concatenados</returns>
        public List<PlanContableCJR> ConsultarPlanContableConcatenado()
        {
            try
            {
                return this.vPlanContableCJRDAL.ConsultarPlanContableConcatenado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
