﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    public class TipoUsuarioBLL
    {
        private TipoUsuarioDAL vTipoUsuarioDAL;
        public TipoUsuarioBLL()
        {
            vTipoUsuarioDAL = new TipoUsuarioDAL();
        }
        public int InsertarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                return vTipoUsuarioDAL.InsertarTipoUsuario(pTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                return vTipoUsuarioDAL.ModificarTipoUsuario(pTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                return vTipoUsuarioDAL.EliminarTipoUsuario(pIdTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TipoUsuario ConsultarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                return vTipoUsuarioDAL.ConsultarTipoUsuario(pIdTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoUsuario> ConsultarTipoUsuarios(String pCodigoTipoUsuario, String pNombreTipoUsuario, String pEstado)
        {
            try
            {
                return vTipoUsuarioDAL.ConsultarTipoUsuarios(pCodigoTipoUsuario, pNombreTipoUsuario, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
