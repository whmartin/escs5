﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;

namespace Icbf.ComparadorJuridico.Business
{
    public class CierreVigenciaBLL
    {
        private CierreVigenciaDAL vCierreVigenciaDAL;
        public CierreVigenciaBLL()
        {
            vCierreVigenciaDAL = new CierreVigenciaDAL();
        }

        public string CerrarVigencia(string NombreUsuario)
        {
            try
            {
                return vCierreVigenciaDAL.CerrarVigencia(NombreUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
