using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    public class TerceroBLL
    {
        private TerceroDAL vTerceroDAL;
        public TerceroBLL()
        {
            vTerceroDAL = new TerceroDAL();
        }
        public int InsertarTercero(Tercero pTercero)
        {
            try
            {
                return vTerceroDAL.InsertarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTercero(Tercero pTercero)
        {
            try
            {
                return vTerceroDAL.ModificarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTercero(Tercero pTercero)
        {
            try
            {
                return vTerceroDAL.EliminarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                return vTerceroDAL.ConsultarTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Tercero> ConsultarTerceros(int? pIdTipoDocumento, String pNumeroDocumento, String pPrimerNombreTercero, String pSegundoNombreTercero, String pPrimerApellidoTercero, String pSegundoApellidoTercero, String pRazonSocial)
        {
            try
            {
                return vTerceroDAL.ConsultarTerceros(pIdTipoDocumento, pNumeroDocumento, pPrimerNombreTercero, pSegundoNombreTercero, pPrimerApellidoTercero, pSegundoApellidoTercero, pRazonSocial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
