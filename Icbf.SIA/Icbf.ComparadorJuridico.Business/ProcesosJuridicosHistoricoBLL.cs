﻿//-----------------------------------------------------------------------
// <copyright file="ProcesosJuridicosHistoricoBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProcesosJuridicosHistoricoBLL.</summary>
// <author>Ingenian Software</author>
// <date>21/03/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo ProcesosJuridicosHistoricoBLL
    /// </summary>
    public class ProcesosJuridicosHistoricoBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private ProcesosJuridicosHistoricoDAL vProcesosJuridicosHistoricoDAL;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProcesosJuridicosHistoricoBLL()
        {
            this.vProcesosJuridicosHistoricoDAL = new ProcesosJuridicosHistoricoDAL();
        }

        /// <summary>
        /// Método que consulta el historico de un Proceso Juridico
        /// </summary>
        /// <param name="pIdProcesosJuridicos">Id del dProceso Juridico</param>
        /// <returns>Lista de Registros Historicos del Proceso Jurídico</returns>
        public List<ProcesosJuridicosHistorico> ConsultarProcesosJuridicosDetalle(int pIdProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicosHistoricoDAL.ConsultarProcesosJuridicosDetalle(pIdProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta el historico de un Proceso Juridico
        /// </summary>
        /// <param name="pIdProcesosJuridicos">Id del Historico</param>
        /// <returns>Lista de Registros Historicos</returns>
        public ProcesosJuridicosHistorico ConsultarProcesosJuridicosHistorico(int pIdHistorico)
        {
            try
            {
                return vProcesosJuridicosHistoricoDAL.ConsultarProcesosJuridicosHistorico(pIdHistorico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
