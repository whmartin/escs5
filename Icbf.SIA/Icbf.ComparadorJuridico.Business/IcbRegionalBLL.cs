using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    public class IcbRegionalBLL
    {
        private IcbRegionalDAL vIcbRegionalDAL;
        public IcbRegionalBLL()
        {
            vIcbRegionalDAL = new IcbRegionalDAL();
        }
        public int InsertarIcbRegional(IcbRegional pIcbRegional)
        {
            try
            {
                return vIcbRegionalDAL.InsertarIcbRegional(pIcbRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarIcbRegional(IcbRegional pIcbRegional)
        {
            try
            {
                return vIcbRegionalDAL.ModificarIcbRegional(pIcbRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarIcbRegional(IcbRegional pIcbRegional)
        {
            try
            {
                return vIcbRegionalDAL.EliminarIcbRegional(pIcbRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public IcbRegional ConsultarIcbRegional(int pRegIdRegional)
        {
            try
            {
                return vIcbRegionalDAL.ConsultarIcbRegional(pRegIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<IcbRegional> ConsultarIcbRegionals(String pRegNombre, String pRegCodReg, String pRegCnnDb)
        {
            try
            {
                return vIcbRegionalDAL.ConsultarIcbRegionals(pRegNombre, pRegCodReg, pRegCnnDb);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
