﻿//-----------------------------------------------------------------------
// <copyright file="FaseProcesoBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase FaseProcesoBLL.</summary>
// <author>Ingenian Software</author>
// <date>13/02/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo FaseProcesoBLL
    /// </summary>
    public class FaseProcesoBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private FaseProcesoDAL vFaseProcesoDAL;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public FaseProcesoBLL()
        {
            this.vFaseProcesoDAL = new FaseProcesoDAL();
        }

        /// <summary>
        /// Consulta los datos de la Fase del Proceso
        /// </summary>
        /// <param name="pIdFaseProceso">Id de la Fase</param>
        /// <param name="pCodigoFaseProceso">Código de la Fase</param>
        /// <param name="pDescripcionFaseProceso">Descripción de la Fase</param>
        /// <param name="pEstado">Estado de la Fase</param>
        /// <returns>Lista de Registros de Fase del Proceso</returns>
        public List<FaseProceso> ConsultarFaseProceso(int? pIdFaseProceso, string pCodigoFaseProceso, string pDescripcionFaseProceso, int pEstado)
        {
            try
            {
                return this.vFaseProcesoDAL.ConsultarFaseProceso(pIdFaseProceso, pCodigoFaseProceso, pDescripcionFaseProceso, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de Fase Proceso
        /// </summary>
        /// <param name="pIdFaseProceso">Id de la Fase del proceso</param>
        /// <param name="pDescripcionFaseProceso">Nombre de la Fase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarFaseProcesoNombre(int? pIdFaseProceso, string pDescripcionFaseProceso)
        {
            try
            {
                return this.vFaseProcesoDAL.ConsultarFaseProcesoNombre(pIdFaseProceso, pDescripcionFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de Fase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarFaseproceso(FaseProceso pFaseProceso)
        {
            try
            {
                return this.vFaseProcesoDAL.InsertarFaseproceso(pFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la modificación de Fase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarFaseproceso(FaseProceso pFaseProceso)
        {
            try
            {
                return this.vFaseProcesoDAL.ModificarFaseproceso(pFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la Fase
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoFase()
        {
            try
            {
                return this.vFaseProcesoDAL.ObtenerCodigoFase();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la Fase del Proceso Contatenado Código-Descripción
        /// </summary>
        /// <returns>Lista de Registros de Fase del Proceso</returns>
        public List<FaseProceso> ConsultarFaseProcesoConcatenado()
        {
            try
            {
                return this.vFaseProcesoDAL.ConsultarFaseProcesoConcatenado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
