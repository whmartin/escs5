using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Net.Mail;

namespace Icbf.ComparadorJuridico.Business
{
    public class UsuarioBLL
    {
        private UsuarioDAL vUsuarioDAL;


        public UsuarioBLL()
        {
            vUsuarioDAL = new UsuarioDAL();

        }
        public int InsertarUsuario(Usuario pUsuario)
        {
            try
            {
                MembershipUser oMu = Membership.GetUser(pUsuario.NombreUsuario);
                if (oMu != null)
                {
                    return -1;
                }
                else
                {
                    if (!Roles.RoleExists(pUsuario.Rol))
                    {
                        throw new UserInterfaceException("EL Rol seleccionado no existe, verifique por favor.");
                    }
                    oMu = Membership.CreateUser(pUsuario.NombreUsuario, pUsuario.Contrasena, pUsuario.CorreoElectronico);
                    Roles.AddUserToRole(pUsuario.NombreUsuario, pUsuario.Rol);
                    pUsuario.Providerkey = oMu.ProviderUserKey.ToString();
                    return vUsuarioDAL.InsertarUsuario(pUsuario);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (Roles.RoleExists(pUsuario.Rol))
                    if (Roles.FindUsersInRole(pUsuario.Rol, pUsuario.NombreUsuario).Length > 0)
                        Roles.RemoveUserFromRole(pUsuario.NombreUsuario, pUsuario.Rol);
                Membership.DeleteUser(pUsuario.NombreUsuario);
                if (pUsuario.IdUsuario != 0)
                    vUsuarioDAL.EliminarUsuario(pUsuario);
                throw new GenericException(ex);
            }
        }
        

        public int ModificarUsuario(Usuario pUsuario)
        {
            try
            {
                MembershipUser oMu = Membership.GetUser(pUsuario.NombreUsuario);
                if (oMu == null)
                {
                    return -1;
                }
                else
                {
                    if (pUsuario.Contrasena.Trim() != "")
                    {
                        oMu.ChangePassword(oMu.GetPassword(), pUsuario.Contrasena);
                    }
                    if (!Roles.RoleExists(pUsuario.Rol))
                    {
                        throw new UserInterfaceException("EL Rol seleccionado no existe, verifique por favor.");
                    }
                    oMu.Email = pUsuario.CorreoElectronico;
                    oMu.IsApproved = pUsuario.Estado;
                    Membership.UpdateUser(oMu);
                    if (Roles.GetRolesForUser(oMu.UserName).Length > 0)
                        Roles.RemoveUserFromRole(pUsuario.NombreUsuario, Roles.GetRolesForUser(oMu.UserName)[0]);
                    Roles.AddUserToRole(pUsuario.NombreUsuario, pUsuario.Rol);
                    return vUsuarioDAL.ModificarUsuario(pUsuario);

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarUsuario(Usuario pUsuario)
        {
            try
            {
                return vUsuarioDAL.EliminarUsuario(pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuario(string pNombreUsuario)
        {
            try
            {
                Usuario oUsuario;
                MembershipUser oMu = Membership.GetUser(pNombreUsuario);
                if (oMu != null)
                {
                    oUsuario = vUsuarioDAL.ConsultarUsuario(oMu.ProviderUserKey.ToString());

                    oUsuario.NombreUsuario = oMu.UserName;
                    oUsuario.CorreoElectronico = oMu.Email;
                    oUsuario.Contrasena = oMu.GetPassword();
                    oUsuario.Rol = Roles.GetRolesForUser(pNombreUsuario)[0];
                    return oUsuario;
                }
                else
                    return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuario(int pTipoDocumento, string pNumeroDocumento)
        {
            try
            {
                Usuario oUsuario;
                oUsuario = vUsuarioDAL.ConsultarUsuario(pTipoDocumento, pNumeroDocumento);
                if (oUsuario.IdUsuario != 0)
                {
                    return oUsuario;
                }
                else if (oUsuario.NombreUsuario != null)
                {
                    MembershipUser oMu = Membership.GetUser(oUsuario.NombreUsuario);
                    if (oMu != null)
                    {
                        oUsuario.NombreUsuario = oMu.UserName;
                        oUsuario.CorreoElectronico = oMu.Email;
                        oUsuario.Contrasena = oMu.GetPassword();
                        oUsuario.Rol = Roles.GetRolesForUser(pNumeroDocumento)[0];
                    }
                    return oUsuario;
                }
                return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Usuario> ConsultarUsuarios(String pNumeroDocumento, String pPrimerNombre, String pPrimerApellido, int pPerfil, Boolean? pEstado, Int32? pTipoUsuario)
        {
            try
            {
                List<Usuario> vListaUsuarios = vUsuarioDAL.ConsultarUsuarios(pNumeroDocumento, pPrimerNombre, pPrimerApellido, pPerfil, pEstado, pTipoUsuario);
                foreach (Usuario vUsuario in vListaUsuarios)
                {
                    MembershipUser oMu = Membership.GetUser(new Guid(vUsuario.Providerkey));
                    if (oMu != null)
                    {
                        vUsuario.NombreUsuario = oMu.UserName;
                        vUsuario.CorreoElectronico = oMu.Email;
                        //vUsuario.Contrasena = oMu.GetPassword();
                        vUsuario.Rol = "";
                        if (Roles.GetRolesForUser(oMu.UserName).Length > 0)
                        {
                            vUsuario.Rol = Roles.GetRolesForUser(oMu.UserName)[0];
                        }                        
                        
                    }
                }
                return vListaUsuarios;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuario(int pIdUsuario)
        {
            try
            {
                Usuario oUsuario = vUsuarioDAL.ConsultarUsuario(pIdUsuario);
                MembershipUser oMu = Membership.GetUser(new Guid(oUsuario.Providerkey));
                if (oMu != null)
                {
                    oUsuario.NombreUsuario = oMu.UserName;
                    oUsuario.CorreoElectronico = oMu.Email;
                    oUsuario.Contrasena = oMu.GetPassword();
                    oUsuario.Rol = "";
                    if (Roles.GetRolesForUser(oMu.UserName).Length > 0)
                    {
                        oUsuario.Rol = Roles.GetRolesForUser(oMu.UserName)[0];
                    }
                    
                    return oUsuario;
                }
                else
                    return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuarioSinContrasena(int pIdUsuario)
        {
            try
            {
                Usuario oUsuario = vUsuarioDAL.ConsultarUsuario(pIdUsuario);
                MembershipUser oMu = Membership.GetUser(new Guid(oUsuario.Providerkey));
                if (oMu != null)
                {
                    oUsuario.NombreUsuario = oMu.UserName;
                    oUsuario.CorreoElectronico = oMu.Email;
                    oUsuario.Contrasena = "";
                    if (Roles.GetRolesForUser(oMu.UserName).Length > 0)
                        oUsuario.Rol = Roles.GetRolesForUser(oMu.UserName)[0];
                    else
                        oUsuario.Rol = "";
                    return oUsuario;
                }
                else
                    return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Usuario> ConsultarTodosUsuarios()
        {
            try
            {
                List<Usuario> oListUsuario = new List<Usuario>();
                MembershipUserCollection oMuC = Membership.GetAllUsers();
                if (oMuC != null)
                {
                    foreach (MembershipUser oMu in oMuC)
                    {
                        Usuario oUsuario = vUsuarioDAL.ConsultarUsuario(oMu.ProviderUserKey.ToString());
                        oUsuario.NombreUsuario = oMu.UserName;
                        oUsuario.CorreoElectronico = oMu.Email;
                        //oUsuario.Contrasena = oMu.GetPassword();
                        oUsuario.Rol = Roles.GetRolesForUser(oMu.UserName)[0];
                        oListUsuario.Add(oUsuario);
                    }
                    return oListUsuario;
                }
                else
                    return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        private string GenerarContrasena()
        {
            string sClave = "";
            int PasswordLength = 7;
            string _allowedChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789";
            Byte[] randomBytes = new Byte[PasswordLength];
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            Regex rx = new Regex(@"(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$");

            while (!rx.IsMatch(sClave))
            {
                for (int i = 0; i < PasswordLength; i++)
                {
                    Random randomObj = new Random();
                    randomObj.NextBytes(randomBytes);
                    chars[i] = _allowedChars[(int)randomBytes[i] % allowedCharCount];
                }
                sClave = new String(chars);
            }

            return sClave;
        }
        
    }
}
