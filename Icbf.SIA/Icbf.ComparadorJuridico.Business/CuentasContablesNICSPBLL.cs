﻿//-----------------------------------------------------------------------
// <copyright file="CuentasContablesNICSPBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase CuentasContablesNICSPBLL.</summary>
// <author>Ingenian Software</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.DataAccess;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using ICBF.MasterData.ClientObjectModel.NMF;

namespace Icbf.ComparadorJuridico.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo CuentasContablesNICSPBLL
    /// </summary>
    public class CuentasContablesNICSPBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private CuentasContablesNICSPDAL vCuentasContablesNICSPDAL;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public CuentasContablesNICSPBLL()
        {
            this.vCuentasContablesNICSPDAL = new CuentasContablesNICSPDAL();
        }

        /// <summary>
        /// Método que retorna una lista de cuentas contables NICSP
        /// </summary>
        /// <param name="pIdCuentasContablesNICSP">Id cuenta contable NISP</param>
        /// <param name="pIdTipoProceso">Id tipo proceso</param>
        /// <param name="pIdFaseProceso">Id fase del proceso</param>
        /// <param name="pIdSubFaseProceso">Id sub fase del proceso</param>
        /// <param name="pIdNaturalezaProceso">Id naturaleza del proceso</param>
        /// <param name="pIdCodCtaConSiifConIntArea">Id cuentacontable inter area</param>
        /// <param name="pIdCodCtaConSiifDebito">id cuenta contable debito</param>
        /// <param name="pIdCodCtaConSiifCredito">Id cuenta contable credito</param>
        /// <param name="pEstado">Estado</param>
        /// <returns>Una lista de las cuentas contables NICSP</returns>
        public IEnumerable<CuentasContablesNICSP> ConsultarCuentasContablesNICSP(
           int? pIdCuentasContablesNICSP,
           int? pIdTipoProceso,
           int? pIdFaseProceso,
           int? pIdSubFaseProceso,
           int? pIdNaturalezaProceso,
           int? pIdCodCtaConSiifConIntArea,
           int? pIdCodCtaConSiifDebito,
           int? pIdCodCtaConSiifCredito,
           bool? pEstado)
        {
            try
            {
                return vCuentasContablesNICSPDAL.ConsultarCuentasContablesNICSP(pIdCuentasContablesNICSP, pIdTipoProceso, pIdFaseProceso, pIdSubFaseProceso, pIdNaturalezaProceso, pIdCodCtaConSiifConIntArea, pIdCodCtaConSiifDebito, pIdCodCtaConSiifCredito, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método que retorna una lista de cuentas contables NICSP
        /// </summary>
        /// <param name="pIdCuentasContablesNICSP">Id cuenta contable NISP</param>
        /// <param name="pIdTipoProceso">Id tipo proceso</param>
        /// <param name="pIdFaseProceso">Id fase del proceso</param>
        /// <param name="pIdSubFaseProceso">Id sub fase del proceso</param>
        /// <param name="pIdNaturalezaProceso">Id naturaleza del proceso</param>
        /// <param name="pIdCodCtaConSiifConIntArea">Id cuentacontable inter area</param>
        /// <param name="pIdCodCtaConSiifDebito">id cuenta contable debito</param>
        /// <param name="pIdCodCtaConSiifCredito">Id cuenta contable credito</param>
        /// <param name="pEstado">Estado</param>
        /// <returns>Una lista de las cuentas contables NICSP</returns>
        public IEnumerable<CuentasContablesNICSPResumen> ConsultarCuentasContablesNICSPResumen(
           int? pIdCuentasContablesNICSP,
           int? pIdTipoProceso,
           int? pIdFaseProceso,
           int? pIdSubFaseProceso,
           int? pIdNaturalezaProceso,
           int? pIdCodCtaConSiifConIntArea,
           int? pIdCodCtaConSiifDebito,
           int? pIdCodCtaConSiifCredito,
           bool? pEstado)
        {
            try
            {
                return vCuentasContablesNICSPDAL.ConsultarCuentasContablesNICSPResumen(
                    pIdCuentasContablesNICSP,
                    pIdTipoProceso,
                    pIdFaseProceso,
                    pIdSubFaseProceso,
                    pIdNaturalezaProceso,
                    pIdCodCtaConSiifConIntArea,
                    pIdCodCtaConSiifDebito,
                    pIdCodCtaConSiifCredito,
                    pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para guardar la cuenta contable NICSP
        /// </summary>
        /// <param name="pCuentasContablesNICSP">Objeto que contiene la información a guardar</param>
        /// <returns>Resultado de la operación</returns>
        public int GuardarCuentasContablesNICSP(CuentasContablesNICSP pCuentasContablesNICSP)
        {
            try
            {
                return vCuentasContablesNICSPDAL.GuardarCuentasContablesNICSP(pCuentasContablesNICSP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consulta de cuentas contables de la base de datos de NMF
        /// </summary>
        /// <param name="pCodCtaContSiif">Código cuenta contable</param>
        /// <param name="pDescCtaContSiif">Descripción cuenta contable</param>
        /// <returns>Una lista con las cuentas contables que coincidan con los filtros seleccionados</returns>
        public List<TCON01Resumido> ConsultarTCON01(string pCodCtaContSiif = "", string pDescCtaContSiif = "")
        {
            try
            {
                TCONCOM com = new TCONCOM();

                List<TCON01Resumido> lista = com.ConsultarTCON01<TCON01Resumido>(pCodCtaContSiif, pDescCtaContSiif, true);
                return lista;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para validar si existe una cuenta parametrizada
        /// </summary>
        /// <param name="pIdCuentasContablesNICSP">Identificador de cuentas contables NICSP</param>
        /// <param name="pIdTipoProceso">Id tipo de proceso</param>
        /// <param name="pIdCodCtaConSiif">Id cuenta contable debito/credito</param>
        /// <returns>True si existe ó False si no existe</returns>
        public bool ValidaContablesNICSP(
           int pIdCuentasContablesNICSP,
           int pIdTipoProceso,
           int pIdCodCtaConSiifDebito,
           int pIdCodCtaConSiifCredito)
        {
            try
            {
                return vCuentasContablesNICSPDAL.ValidaContablesNICSP(pIdCuentasContablesNICSP, pIdTipoProceso, pIdCodCtaConSiifDebito, pIdCodCtaConSiifCredito);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
