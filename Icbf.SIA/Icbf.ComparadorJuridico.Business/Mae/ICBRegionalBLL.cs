﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace Icbf.ComparadorJuridico.Business.Mae
{
   public  class ICBRegionalBLL
    {

       public DataTable ConsRegional()
       { 
        Icbf.ComparadorJuridico.DataAccess.Mae.ICBRegionalDAL oRegional = new Icbf.ComparadorJuridico.DataAccess.Mae.ICBRegionalDAL();
           return oRegional.ConsRegional();

       }
    }
}
