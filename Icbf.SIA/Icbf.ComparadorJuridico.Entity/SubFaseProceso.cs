﻿//-----------------------------------------------------------------------
// <copyright file="SubFaseProceso.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la entidad SubFaseProceso.</summary>
// <author>Ingenian Software</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

using System;

namespace Icbf.ComparadorJuridico.Entity
{
    /// <summary>
    /// Clase de la entidad Planilla
    /// </summary>
    [Serializable]

    /// <summary>
    /// Clase de la entidad SubFaseProceso
    /// </summary>
    public class SubFaseProceso : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Gets or sets IdSubFaseProceso
        /// </summary>
        public int IdSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or sets CodigoSubFaseProceso
        /// </summary>
        public string CodigoSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or sets DescripcionSubFaseProceso
        /// </summary>
        public string DescripcionSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or sets Estado
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Constructor de la clase.
        /// </summary>
        public SubFaseProceso()
        {
        }
    }
}
