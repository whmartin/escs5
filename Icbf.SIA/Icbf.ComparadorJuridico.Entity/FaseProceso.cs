﻿//-----------------------------------------------------------------------
// <copyright file="FaseProceso.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la entidad FaseProceso.</summary>
// <author>Ingenian Software</author>
// <date>13/02/2018</date>
//-----------------------------------------------------------------------

using System;

namespace Icbf.ComparadorJuridico.Entity
{
    /// <summary>
    /// Clase de la entidad Planilla
    /// </summary>
    [Serializable]

    /// <summary>
    /// Clase de la entidad FaseProceso
    /// </summary>
    public class FaseProceso : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Gets or sets IdFaseProceso
        /// </summary>
        public int IdFaseProceso { get; set; }

        /// <summary>
        /// Gets or sets CodigoFaseProceso
        /// </summary>
        public string CodigoFaseProceso { get; set; }

        /// <summary>
        /// Gets or sets DescripcionFaseProceso
        /// </summary>
        public string DescripcionFaseProceso { get; set; }

        /// <summary>
        /// Gets or sets Estado
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Constructor de la clase.
        /// </summary>
        public FaseProceso()
        {
        }
    }
}
