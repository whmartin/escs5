﻿namespace Icbf.ComparadorJuridico.Entity
{
    using System;

    [Serializable]
    public class CuentasContablesNICSPResumen
    {
        /// <summary>
        /// Gets or sets IdCuentasContablesNICSP
        /// </summary>
        public int IdCuentasContablesNICSP { get; set; }

        public string TipoProceso { get; set; }

        public string Naturaleza { get; set; }

        public string Fase { get; set; }

        public string SubFase { get; set; }

        public string CuentaDebito { get; set; }

        public string CuentaCredito { get; set; }

        public string CuentaInterAreas{ get; set; }
        public int IdFaseProceso { get; set; }
        public int IdSubFaseProceso { get; set; }
        public int IdNaturalezaProceso { get; set; }
    }
}
