using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    /// <summary>
    /// Este Modulo Genera el archivo para ser cargado en SIIF Nacion
    /// </summary>
    public class GeneradorArchivoSIIF : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdGeneradorArchivoSIIF
        {
            get;
            set;
        }
        public String TipoArchivo
        {
            get;
            set;
        }
        public DateTime FechaGeneracion
        {
            get;
            set;
        }
        public String NombreArchivo
        {
            get;
            set;
        }
        public int IdRegional
        {
            get;
            set;
        }
        public DateTime FechaInicio
        {
            get;
            set;
        }
        public DateTime FechaFin
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public GeneradorArchivoSIIF()
        {
        }
    }
}
