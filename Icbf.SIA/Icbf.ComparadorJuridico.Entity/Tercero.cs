using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class Tercero : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTercero
        {
            get;
            set;
        }
        public int IdTipoDocumento
        {
            get;
            set;
        }
        
        public String TipoDocumento { get; set; }

        public String TerceroConcatenado { get; set; }

        public String NumeroDocumento
        {
            get;
            set;
        }
        public String PrimerNombreTercero
        {
            get;
            set;
        }
        public String SegundoNombreTercero
        {
            get;
            set;
        }
        public String PrimerApellidoTercero
        {
            get;
            set;
        }
        public String SegundoApellidoTercero
        {
            get;
            set;
        }
        public String RazonSocial
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Tercero()
        {
        }
    }
}
