using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class TipoDocumento : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTipoDocumento
        {
            get;
            set;
        }
        public String Codigo
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }
        public TipoDocumento()
        {
        }
    }
}
