using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class Vigencia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdVigencia
        {
            get;
            set;
        }
        public string VigenciaConcatenada { get; set; }
        public int Ano
        {
            get;
            set;
        }
        public int IdMes
        {
            get;
            set;
        }
        public String Mes
        {
            get;
            set;
        }
        
        public String FechaInicio { get; set; }

        public String FechaFin { get; set; }
        
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Gets Or Sets de VigenciaActiva
        /// </summary>
        public bool VigenciaActiva { get; set; }

        public Vigencia()
        {
        }
    }
}
