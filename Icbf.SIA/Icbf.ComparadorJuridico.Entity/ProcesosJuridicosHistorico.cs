//-----------------------------------------------------------------------
// <copyright file="ProcesosJuridicosHistorico.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProcesosJuridicosHistorico.</summary>
// <author>Ingenian Software</author>
// <date>21/03/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    /// <summary>
    /// Clase Entidad ProcesosJuridicosHistorico con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class ProcesosJuridicosHistorico : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Gets or Sets IdHistorico
        /// </summary>
        public int IdHistorico { get; set; }

        /// <summary>
        /// Gets or Sets FechaLog
        /// </summary>
        public DateTime FechaLog { get; set; }

        /// <summary>
        /// Gets or Sets IdProcesosJuridicos
        /// </summary>
        public int IdProcesosJuridicos { get; set; }

        /// <summary>
        /// Gets or Sets IdRegional
        /// </summary>
        public int IdRegional { get; set; }

        /// <summary>
        /// Gets or Sets IdVigencia
        /// </summary>
        public int IdVigencia { get; set; }

        /// <summary>
        /// Gets or Sets IdProceso
        /// </summary>
        public int IdProceso { get; set; }

        /// <summary>
        /// Gets or Sets IdTercero
        /// </summary>
        public int IdTercero { get; set; }

        /// <summary>
        /// Gets or Sets IdPlanContable
        /// </summary>
        public int IdPlanContable { get; set; }

        /// <summary>
        /// Gets or Sets NumeroProceso
        /// </summary>
        public String NumeroProceso { get; set; }

        /// <summary>
        /// Gets or Sets ValorProceso
        /// </summary>
        public String ValorProceso { get; set; }

        /// <summary>
        /// Gets or Sets Estado
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Gets or Sets UsuarioCrea
        /// </summary>
        public String UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or Sets UsuarioModifica
        /// </summary>
        public String UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or Sets FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or Sets FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Listado de terceros asgnados al proceso separados por coma(,)
        /// </summary>
        public String ListTerceros { get; set; }

        /// <summary>
        /// Gets or Sets Regional
        /// </summary>
        public String Regional { get; set; }

        /// <summary>
        /// Gets or Sets VigenciaConcatenada
        /// </summary>
        public String VigenciaConcatenada { get; set; }

        /// <summary>
        /// Gets or Sets NombreTipoProceso
        /// </summary>
        public String NombreTipoProceso { get; set; }

        /// <summary>
        /// Gets or Sets TerceroConcatenado
        /// </summary>
        public String TerceroConcatenado { get; set; }

        /// <summary>
        /// Gets or Sets ConcatenadoPlanContable
        /// </summary>
        public String ConcatenadoPlanContable { get; set; }

        /// <summary>
        /// Gets or Sets Dado_de_Baja
        /// </summary>
        public String Dado_de_Baja { get; set; }

        /// <summary>
        /// Gets or Sets IdNaturalezaProceso
        /// </summary>
        public int? IdNaturalezaProceso { get; set; }

        /// <summary>
        /// Gets or Sets IdFaseProceso
        /// </summary>
        public int? IdFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets IdSubFaseProceso
        /// </summary>
        public int? IdSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets Juzgado
        /// </summary>
        public string Juzgado { get; set; }

        /// <summary>
        /// Gets or Sets Apoderado
        /// </summary>
        public string Apoderado { get; set; }

        /// <summary>
        /// Gets or Sets Juzgado
        /// </summary>
        public DateTime FechaAdmisionJuzgado { get; set; }

        /// <summary>
        /// Gets or Sets Juzgado
        /// </summary>
        public DateTime FechaAdmisionICBF { get; set; }

        /// <summary>
        /// Gets or Sets Observaciones
        /// </summary>
        public string Observaciones { get; set; }

        /// <summary>
        /// Gets or Sets DescripcionNaturalezaProceso
        /// </summary>
        public string DescripcionNaturalezaProceso { get; set; }

        /// <summary>
        /// Gets or Sets DescripcionFaseProceso
        /// </summary>
        public string DescripcionFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets DescripcionSubFaseProceso
        /// </summary>
        public string DescripcionSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets DescripcionPlanContable
        /// </summary>
        public string DescripcionPlanContable { get; set; }

        /// <summary>
        /// Gets or Sets Descripcion
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProcesosJuridicosHistorico()
        {
        }
    }
}
