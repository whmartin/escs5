﻿//-----------------------------------------------------------------------
// <copyright file="ProcesosJuridicosDocumentos.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProcesosJuridicosDocumentos.</summary>
// <author>Ingenian Software</author>
// <date>26/02/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    /// <summary>
    /// Clase Entidad DocumentosProcesosJuridicos con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class ProcesosJuridicosDocumentos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProcesosJuridicosDocumentos()
        {
            this.Documentos = new List<ProcesosJuridicosDocumentos>();
        }

        /// <summary>
        /// Gets or sets IdDocumentosProcesosJuridicos
        /// </summary>
        public int IdProcesosJuridicosDocumentos { get; set; }

        /// <summary>
        /// Gets or sets IdProcesosJuridicos
        /// </summary>
        public int IdProcesosJuridicos { get; set; }

        /// <summary>
        /// Gets or sets IdEstadoDocumento
        /// </summary>
        //public int IdEstadoDocumento { get; set; }

        /// <summary>
        /// Gets or sets NombreArchivo
        /// </summary>
        public string NombreArchivo { get; set; }

        /// <summary>
        /// Gets or sets RutaArchivo
        /// </summary>
        public string RutaArchivo { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Gets or sets byts
        /// </summary>
        public byte[] bytes { get; set; }

        /// <summary>
        /// Obtiene o establece un valor que indica si ExisteArchivo
        /// </summary>
        public bool ExisteArchivo { get; set; }

        /// <summary>
        /// Obtiene o establece OtrosDocumentos
        /// </summary>
        public List<ProcesosJuridicosDocumentos> Documentos { get; set; }
    }
}
