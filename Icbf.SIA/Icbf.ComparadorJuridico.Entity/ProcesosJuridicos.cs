using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class ProcesosJuridicos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdProcesosJuridicos
        {
            get;
            set;
        }
        public int IdRegional
        {
            get;
            set;
        }
        public int IdVigencia
        {
            get;
            set;
        }
        public int IdProceso
        {
            get;
            set;
        }
        public int IdTercero
        {
            get;
            set;
        }
        public int IdPlanContable
        {
            get;
            set;
        }
        public string NumeroProceso
        {
            get;
            set;
        }
        public string ValorProceso
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Listado de terceros asgnados al proceso separados por coma(,)
        /// </summary>
        public String ListTerceros { get; set; }

        public String Regional { get; set; }
        public String VigenciaConcatenada { get; set; }
        public String NombreTipoProceso { get; set; }
        public String TerceroConcatenado { get; set; }
        public String ConcatenadoPlanContable { get; set; }
        public String Dado_de_Baja { get; set; }

        /// <summary>
        /// Gets or Sets IdNaturaleza
        /// </summary>
        public int? IdNaturalezaProceso { get; set; }

        /// <summary>
        /// Gets or Sets IdFaseProceso
        /// </summary>
        public int? IdFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets IdSubFaseProceso
        /// </summary>
        public int? IdSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets Juzgado
        /// </summary>
        public string Juzgado { get; set; }

        /// <summary>
        /// Gets or Sets Apoderado
        /// </summary>
        public string Apoderado { get; set; }

        /// <summary>
        /// Gets or Sets Juzgado
        /// </summary>
        public DateTime FechaAdmisionJuzgado { get; set; }

        /// <summary>
        /// Gets or Sets Juzgado
        /// </summary>
        public DateTime FechaAdmisionICBF { get; set; }

        /// <summary>
        /// Gets or Sets DescripcionNaturalezaProceso
        /// </summary>
        public string DescripcionNaturalezaProceso { get; set; }

        /// <summary>
        /// Gets or Sets DescripcionFaseProceso
        /// </summary>
        public string DescripcionFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets DescripcionSubFaseProceso
        /// </summary>
        public string DescripcionSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets Observaciones
        /// </summary>
        public string Observaciones { get; set; }

        public int NumeroProcesos { get; set; }

        public string Descripcion { get; set; }

        public string CodigoCuenta { get; set; }

        public ProcesosJuridicos()
        {
        }
    }
}
