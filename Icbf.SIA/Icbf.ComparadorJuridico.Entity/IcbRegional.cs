using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class IcbRegional : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int RegIdRegional
        {
            get;
            set;
        }
        public String RegNombre
        {
            get;
            set;
        }
        public String RegCodReg
        {
            get;
            set;
        }
        public String RegCnnDb
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public IcbRegional()
        {
        }
    }
}
