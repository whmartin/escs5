using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class PlanContable : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdPlanContable
        {
            get;
            set;
        }
        
        public String ConcatenadoPlanContable { get; set; }

        public int CodigoCuenta
        {
            get;
            set;
        }
        public String NombreCuenta
        {
            get;
            set;
        }
        public int CodigoSubcuenta
        {
            get;
            set;
        }
        public String NombreSubcuenta
        {
            get;
            set;
        }
        public int CodigoTercerNivelCuenta
        {
            get;
            set;
        }
        public String NombreTercerNivelCuenta
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public PlanContable()
        {
        }
    }
}
