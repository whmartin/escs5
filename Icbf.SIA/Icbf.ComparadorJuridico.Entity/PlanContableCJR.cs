﻿//-----------------------------------------------------------------------
// <copyright file="PlanContableCJR.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase PlanContableCJR.</summary>
// <author>Ingenian Software</author>
// <date>10/04/2018</date>
//-----------------------------------------------------------------------

using System;

namespace Icbf.ComparadorJuridico.Entity
{
    /// <summary>
    /// Clase Entidad PlanContableCJR con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    public class PlanContableCJR : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Gets or Sets IdPlanContable
        /// </summary>
        public int IdPlanContable { get; set; }

        /// <summary>
        /// Gets or Sets CodigoCuenta
        /// </summary>
        public int CodigoCuenta { get; set; }

        /// <summary>
        /// Gets or Sets NombreCuenta
        /// </summary>
        public string NombreCuenta { get; set; }

        /// <summary>
        /// Gets or Sets Descripcion
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or Sets IdProceso
        /// </summary>
        public int IdProceso { get; set; }

        /// <summary>
        /// Gets or Sets IdFaseProceso
        /// </summary>
        public int IdFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets IdSubFaseProceso
        /// </summary>
        public int IdSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or Sets IdNaturalezaProceso
        /// </summary>
        public int IdNaturalezaProceso { get; set; }

        /// <summary>
        /// Gets or Sets Estado
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Gets or Sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or Sets FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or Sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or Sets FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Gets or Sets PlanContable
        /// </summary>
        public string PlanContable { get; set; }
    }
}
