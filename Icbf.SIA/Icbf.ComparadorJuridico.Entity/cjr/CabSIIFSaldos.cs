﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity.cjr
{
    public class CabSIIFSaldos : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public int csfIdCabSaldosSIIF
        {
            get;
            set;
        }
        public string csfNomArh
        {
            get;
            set;
        }
        public decimal csfIdAno
        {
            get;
            set;
        }
        public decimal csfIdMes
        {
            get;
            set;
        }
        public decimal csfIdEstado
        {
            get;
            set;
        }
        public DateTime  csfFchCarga
        {
            get;
            set;
        }
        public int csfAudIdUsuario
        {
            get;
            set;
        }
    }
}
