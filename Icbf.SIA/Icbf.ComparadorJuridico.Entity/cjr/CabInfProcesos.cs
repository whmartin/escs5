﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity.cjr
{
    public class CabInfProcesos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int cipIdCabInfProcesos
        {   get;
            set;
        }
        public int cipIdAno
        {
            get;
            set;
        }
        public int cipIdMes
        {
            get;
            set;
        }
        public string cipNomArch
        {
            get;
            set;
        }
        public int cipIdEstado
        {
            get;
            set;
        }
        public DateTime  cipFchCarga
        {
            get;
            set;
        }
        public int cipAudIdUsuario
        {
            get;
            set;
        }
    }
}
