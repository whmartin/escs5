﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class Usuario : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdUsuario
        {
            set;
            get;
        }
        public int TipoUsuario
        {
            set;
            get;
        }
        public String Providerkey
        {
            set;
            get;
        }
        public int IdTipoDocumento
        {
            set;
            get;
        }
        public String TipoDocumento
        {
            set;
            get;
        }
        public String NumeroDocumento
        {
            set;
            get;
        }
        public String PrimerNombre
        {
            set;
            get;
        }
        public String SegundoNombre
        {
            set;
            get;
        }
        public String PrimerApellido
        {
            set;
            get;
        }
        public String SegundoApellido
        {
            set;
            get;
        }
        public String TelefonoContacto
        {
            set;
            get;
        }
        public String CorreoElectronico
        {
            set;
            get;
        }
        public String NombreUsuario
        {
            set;
            get;
        }
        public String Contrasena
        {
            get;
            set;
        }
        public String Rol
        {
            get;
            set;
        }
        public int IdTipoUsuario 
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCreacion
        {
            set;
            get;
        }
        public String UsuarioModificacion
        {
            set;
            get;
        }
        public DateTime FechaCreacion
        {
            set;
            get;
        }
        public DateTime FechaModificacion
        {
            set;
            get;
        }

        //Agregados AGV
        public int IdRegional
        {
            get;
            set;
        }

        public String NombreRegional
        {
            get;
            set;
        }

        public Usuario()
        {
        }
    }
}
