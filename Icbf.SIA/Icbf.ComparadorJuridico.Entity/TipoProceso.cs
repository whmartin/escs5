﻿namespace Icbf.ComparadorJuridico.Entity
{
    /// <summary>
    /// Clase TipoProceso
    /// </summary>
    public class TipoProceso
    {
        /// <summary>
        /// IdTipoProceso
        /// </summary>
        public int IdTipoProceso{ get; set; }

        /// <summary>
        /// NombreTipoProcesol
        /// </summary>
        public string NombreTipoProcesol { get; set; }
    }
}
