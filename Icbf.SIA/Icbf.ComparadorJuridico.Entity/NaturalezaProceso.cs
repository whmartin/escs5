﻿//-----------------------------------------------------------------------
// <copyright file="NaturalezaProceso.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la entidad NaturalezaProceso.</summary>
// <author>Ingenian Software</author>
// <date>18/01/2018</date>
//-----------------------------------------------------------------------

using System;

namespace Icbf.ComparadorJuridico.Entity
{
    /// <summary>
    /// Clase de la entidad NaturalezaProceso
    /// </summary>
    public class NaturalezaProceso : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Gets or sets IdNaturalezaProceso
        /// </summary>
        public int IdNaturalezaProceso { get; set; }

        /// <summary>
        /// Gets or sets CodigoNaturalezaProceso
        /// </summary>
        public string CodigoNaturalezaProceso { get; set; }

        /// <summary>
        /// Gets or sets DescripcionNaturalezaProceso
        /// </summary>
        public string DescripcionNaturalezaProceso { get; set; }
        
        /// <summary>
        /// Gets or sets CodigoContable
        /// </summary>
        ///public string CodigoContable { get; set; }

        /// <summary>
        /// Gets or sets Estado
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Constructor de la clase.
        /// </summary>
        public NaturalezaProceso()
        {
        }
    }
}
