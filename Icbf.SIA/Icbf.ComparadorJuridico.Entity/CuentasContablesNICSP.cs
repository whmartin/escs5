﻿
namespace Icbf.ComparadorJuridico.Entity
{
    using System;

    /// <summary>
    /// Clase de la entidad Planilla
    /// </summary>
    [Serializable]

    /// <summary>
    /// Clase de la entidad SubFaseProceso
    /// </summary>
    public class CuentasContablesNICSP : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Gets or sets IdCuentasContablesNICSP
        /// </summary>
        public int IdCuentasContablesNICSP { get; set; }

        /// <summary>
        /// Gets or sets IdTipoProceso
        /// </summary>
        public int IdTipoProceso { get; set; }

        /// <summary>
        /// Gets or sets IdFaseProceso
        /// </summary>
        public int IdFaseProceso { get; set; }

        /// <summary>
        /// Gets or sets IdSubFaseProceso
        /// </summary>
        public int IdSubFaseProceso { get; set; }

        /// <summary>
        /// Gets or sets IdNaturalezaProceso
        /// </summary>
        public int IdNaturalezaProceso { get; set; }

        /// <summary>
        /// Gets or sets IdCodCtaConSiifConIntArea
        /// </summary>
        public int IdCodCtaConSiifConIntArea { get; set; }

        public string CodCtaContSiifIntArea { get; set; }

        public string DescCtaContSiifIntArea { get; set; }

        /// <summary>
        /// Gets or sets IdCodCtaConSiifDebito
        /// </summary>
        public int IdCodCtaConSiifDebito { get; set; }

        public string CodCtaContSiifDebito { get; set; }

        public string DescCtaContSiifDebito { get; set; }


        /// <summary>
        /// Gets or sets IdCodCtaConSiifCredito
        /// </summary>
        public int IdCodCtaConSiifCredito { get; set; }

        public string CodCtaContSiifCredito { get; set; }

        public string DescCtaContSiifCredito { get; set; }


        /// <summary>
        /// Gets or sets Estado
        /// </summary>
        public bool Estado { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea
        /// </summary>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea
        /// </summary>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica
        /// </summary>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets FechaModifica
        /// </summary>
        public DateTime FechaModifica { get; set; }

        /// <summary>
        /// Constructor de la clase.
        /// </summary>
        public CuentasContablesNICSP()
        {
        }
    }
}
