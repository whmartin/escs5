﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class TipoUsuario : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTipoUsuario
        {
            get;
            set;
        }
        public String CodigoTipoUsuario
        {
            get;
            set;
        }
        public String NombreTipoUsuario
        {
            get;
            set;
        }
        public String Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public TipoUsuario()
        {
        }
    }
}
