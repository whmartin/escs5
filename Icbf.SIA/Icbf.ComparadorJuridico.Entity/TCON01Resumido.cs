﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.ComparadorJuridico.Entity
{
    public class TCON01Resumido
    {
        /// <summary>
        /// Id cuenta contable
        /// </summary>
        public int IdCodCtaConSiif { get; set; }

        /// <summary>
        /// Código cuenta contable
        /// </summary>
        public string CodCtaContSiif { get; set; }

        /// <summary>
        /// Descripción cuenta contable
        /// </summary>
        public string DescCtaContSiif { get; set; }

        /// <summary>
        /// Campo de estado
        /// </summary>
        public bool Estado { get; set; }

        /// <summary>
        /// Campo de inactivo
        /// </summary>
        public bool Inactivo { get; set; }
    }
}
