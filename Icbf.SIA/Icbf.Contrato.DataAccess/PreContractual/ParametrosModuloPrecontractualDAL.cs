﻿using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess.PreContractual
{
    public class ParametrosModuloPrecontractualDAL : GeneralDAL
    {

        public ParametrosModuloPrecontractual ConsultarParametro(string Nombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_ParametrosModulo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, Nombre);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ParametrosModuloPrecontractual vSolicitudContrato = new ParametrosModuloPrecontractual();

                        while(vDataReaderResults.Read())
                        {
                            vSolicitudContrato.IdParametro = vDataReaderResults["IdParametro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdParametro"].ToString()) : vSolicitudContrato.IdParametro;
                            vSolicitudContrato.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSolicitudContrato.Nombre;
                            vSolicitudContrato.Valor = vDataReaderResults["Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Valor"].ToString()) : vSolicitudContrato.Valor;
                        }

                        return vSolicitudContrato;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
