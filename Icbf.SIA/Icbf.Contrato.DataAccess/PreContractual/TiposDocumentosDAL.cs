﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;

using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity.PreContractual;

namespace Icbf.Contrato.DataAccess.PreContractual
{
    public class TiposDocumentosDAL : GeneralDAL
    { 
        public int InsertarTipoDocumento(TiposDocumento pTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_TipoDocumento_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTipoDocumento.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoDocumento.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoDocumento.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, pTipoDocumento.ModalidadesSeleccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoDocumento.IdTipoDocumento = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoDocumento").ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTipoDocumento(TiposDocumento pTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_TiposDocumento_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTipoDocumento.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTipoDocumento.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoDocumento.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoDocumento.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoDocumento.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, pTipoDocumento.ModalidadesSeleccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TiposDocumento ConsultarTipoDocumentoPorId(int pIdTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_TiposDocumento_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pIdTipoDocumento);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiposDocumento vTipoDocumento = null;

                        while (vDataReaderResults.Read())
                        {
                            vTipoDocumento = new TiposDocumento();
                            vTipoDocumento.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumento.IdTipoDocumento;
                            vTipoDocumento.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vTipoDocumento.Nombre;
                            vTipoDocumento.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"]) : vTipoDocumento.Descripcion;
                            vTipoDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumento.UsuarioCrea;
                            vTipoDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumento.FechaCrea;
                            vTipoDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumento.UsuarioModifica;
                            vTipoDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumento.FechaModifica;
                            vTipoDocumento.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoDocumento.Estado;
                            vTipoDocumento.ModalidadesSeleccion = vDataReaderResults["ModalidadesSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadesSeleccion"].ToString()) : vTipoDocumento.ModalidadesSeleccion;
                        }

                        return vTipoDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTiposDocumentos(int? pIdModalidadSeleccion, string pNombre, bool ? estado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_TiposDocumento_Consultar"))
                {
                    if (pIdModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion.Value);
                    if (!string.IsNullOrEmpty(pNombre))
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if(estado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, estado.Value);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiposDocumento> vListaTiposDocumentos = new List<TiposDocumento>();

                        while (vDataReaderResults.Read())
                        {
                            TiposDocumento vTipoDocumento = new TiposDocumento();
                            vTipoDocumento.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumento.IdTipoDocumento;
                            vTipoDocumento.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vTipoDocumento.Nombre;
                            vTipoDocumento.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"]) : vTipoDocumento.Descripcion;
                            vTipoDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumento.UsuarioCrea;
                            vTipoDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumento.FechaCrea;
                            vTipoDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumento.UsuarioModifica;
                            vTipoDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumento.FechaModifica;
                            vTipoDocumento.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoDocumento.Estado;
                            vTipoDocumento.ModalidadesSeleccion = vDataReaderResults["ModalidadesSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadesSeleccion"].ToString()) : vTipoDocumento.UsuarioModifica;
                            vListaTiposDocumentos.Add(vTipoDocumento);
                            vTipoDocumento.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoDocumento.Estado;
                        }

                        return vListaTiposDocumentos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTipoDocumentoPorActividad(int idActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_TiposDocumento_ConsultarActividad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, idActividad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiposDocumento> vListaTiposDocumentos = new List<TiposDocumento>();

                        while (vDataReaderResults.Read())
                        {
                            TiposDocumento vTipoDocumento = new TiposDocumento();
                            vTipoDocumento.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumento.IdTipoDocumento;
                            vTipoDocumento.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vTipoDocumento.Nombre;
                            vTipoDocumento.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"]) : vTipoDocumento.Descripcion;
                            vTipoDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumento.UsuarioCrea;
                            vTipoDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumento.FechaCrea;
                            vTipoDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumento.UsuarioModifica;
                            vTipoDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumento.FechaModifica;
                            vTipoDocumento.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoDocumento.Estado;
                            vListaTiposDocumentos.Add(vTipoDocumento);
                        }

                        return vListaTiposDocumentos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTipoDocumentoPosiblesPorActividad(int idActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_TiposDocumento_ConsultarPosiblesPorActividad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, idActividad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiposDocumento> vListaTiposDocumentos = new List<TiposDocumento>();

                        while (vDataReaderResults.Read())
                        {
                            TiposDocumento vTipoDocumento = new TiposDocumento();
                            vTipoDocumento.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumento.IdTipoDocumento;
                            vTipoDocumento.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vTipoDocumento.Nombre;
                            vTipoDocumento.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"]) : vTipoDocumento.Descripcion;
                            vTipoDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumento.UsuarioCrea;
                            vTipoDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumento.FechaCrea;
                            vTipoDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumento.UsuarioModifica;
                            vTipoDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumento.FechaModifica;
                            vTipoDocumento.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoDocumento.Estado;
                            vListaTiposDocumentos.Add(vTipoDocumento);
                        }

                        return vListaTiposDocumentos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarModalidadSeleccionPorIdTipoDocumento(int pIdTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_TiposDocumentos_ConsultarModalidades"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pIdTipoDocumento);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Dictionary<int, string> vListaModalidades = new Dictionary<int, string>();
                        int key;
                        string value;
                        while (vDataReaderResults.Read())
                        {
                            key = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : 0;
                            value = vDataReaderResults["Modalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Modalidad"].ToString()) : string.Empty;
                            vListaModalidades.Add(key, value);
                        }

                        return vListaModalidades;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
