﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity.PreContractual;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Icbf.Utilities.Exceptions;
using System.Data.Common;
using System.Data;

namespace Icbf.Contrato.DataAccess.PreContractual
{
    public class DesarrolloActividadDAL : GeneralDAL
    {
        public List<SolicitudContratoActividadesDesarrollo> ConsultarDesarrolloActividadPorId(int idSolicitud)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarActividadesSolicitud"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, idSolicitud);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoActividadesDesarrollo> vListaActividaes = new List<SolicitudContratoActividadesDesarrollo>();

                        while(vDataReaderResults.Read())
                        {
                            SolicitudContratoActividadesDesarrollo vActividades = new SolicitudContratoActividadesDesarrollo();
                            vActividades.IdActivdad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActivdad;
                            vActividades.IdDesarrolloActividad = vDataReaderResults["IdDesarrolloActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDesarrolloActividad"].ToString()) : vActividades.IdDesarrolloActividad;
                            vActividades.IdSolicitud = vDataReaderResults["IdSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitud"].ToString()) : vActividades.IdSolicitud;
                            vActividades.FechaInicial = vDataReaderResults["FechaInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicial"].ToString()) : vActividades.FechaInicial;
                            vActividades.FechaFinalizacion = vDataReaderResults["FechaFinalizacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacion"].ToString()) : vActividades.FechaInicial;
                            vActividades.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"]) : vActividades.Observaciones;
                            vActividades.Finalizada = vDataReaderResults["Finalizada"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizada"].ToString()) : vActividades.Finalizada;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            vActividades.AplicaPublicacion = vDataReaderResults["AplicaPublicacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AplicaPublicacion"].ToString()) : vActividades.AplicaPublicacion;
                            vActividades.FechaPublicacion = vDataReaderResults["FechaPublicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacion"].ToString()) : vActividades.FechaPublicacion;
                            vActividades.EsPadre = vDataReaderResults["EsPadre"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPadre"].ToString()) : vActividades.EsPadre;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vActividades.Nombre;
                            vActividades.Cantidad = vDataReaderResults["Cantidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cantidad"].ToString()) : vActividades.Cantidad;
                            vListaActividaes.Add(vActividades);
                        }

                        return vListaActividaes;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<SolicitudContratoActividadesDesarrollo> ConsultarDesarrolloActividadPorIdActividad(int idSolicitud, int idActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarSubActividadesSolicitud"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, idSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, idActividad);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoActividadesDesarrollo> vListaActividaes = new List<SolicitudContratoActividadesDesarrollo>();

                        while(vDataReaderResults.Read())
                        {
                            SolicitudContratoActividadesDesarrollo vActividades = new SolicitudContratoActividadesDesarrollo();
                            vActividades.IdActivdad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActivdad;
                            vActividades.IdDesarrolloActividad = vDataReaderResults["IdDesarrolloActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDesarrolloActividad"].ToString()) : vActividades.IdDesarrolloActividad;
                            vActividades.IdSolicitud = vDataReaderResults["IdSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitud"].ToString()) : vActividades.IdSolicitud;
                            vActividades.FechaInicial = vDataReaderResults["FechaInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicial"].ToString()) : vActividades.FechaInicial;
                            vActividades.FechaFinalizacion = vDataReaderResults["FechaFinalizacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacion"].ToString()) : vActividades.FechaInicial;
                            vActividades.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"]) : vActividades.Observaciones;
                            vActividades.Finalizada = vDataReaderResults["Finalizada"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizada"].ToString()) : vActividades.Finalizada;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            vActividades.AplicaPublicacion = vDataReaderResults["AplicaPublicacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AplicaPublicacion"].ToString()) : vActividades.AplicaPublicacion;
                            vActividades.FechaPublicacion = vDataReaderResults["FechaPublicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacion"].ToString()) : vActividades.FechaPublicacion;
                            vActividades.EsPadre = vDataReaderResults["EsPadre"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPadre"].ToString()) : vActividades.EsPadre;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vActividades.Nombre;

                            vListaActividaes.Add(vActividades);
                        }

                        return vListaActividaes;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public SolicitudContratoActividadesDesarrolloDetalle ConsultarDesarrolloActividadPorId(int idSolicitudContrato, int idActividad, int ? idActividadPadre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarDesarrolloActividadPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, idSolicitudContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, idActividad);

                    if(idActividadPadre.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdActividadPadre", DbType.Int32, idActividadPadre);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SolicitudContratoActividadesDesarrolloDetalle vActividades = null;

                        while(vDataReaderResults.Read())
                        {
                            vActividades = new SolicitudContratoActividadesDesarrolloDetalle();
                            vActividades.IdActivdad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActivdad;
                            vActividades.IdDesarrolloActividad = vDataReaderResults["IdDesarrolloActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDesarrolloActividad"].ToString()) : vActividades.IdDesarrolloActividad;
                            vActividades.IdSolicitud = vDataReaderResults["IdSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitud"].ToString()) : vActividades.IdSolicitud;
                            vActividades.FechaInicial = vDataReaderResults["FechaInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicial"].ToString()) : vActividades.FechaInicial;
                            vActividades.FechaFinalizacion = vDataReaderResults["FechaFinalizacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacion"].ToString()) : vActividades.FechaInicial;
                            vActividades.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"]) : vActividades.Observaciones;
                            vActividades.Finalizada = vDataReaderResults["Finalizada"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizada"].ToString()) : vActividades.Finalizada;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            vActividades.AplicaPublicacion = vDataReaderResults["AplicaPublicacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AplicaPublicacion"].ToString()) : vActividades.AplicaPublicacion;
                            vActividades.FechaPublicacion = vDataReaderResults["FechaPublicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacion"].ToString()) : vActividades.FechaPublicacion;
                            vActividades.NombreActividad = vDataReaderResults["NombreActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreActividad"]) : vActividades.NombreActividad;
                            vActividades.NombreActividadPadre = vDataReaderResults["ActividadPadre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ActividadPadre"]) : vActividades.NombreActividad;
                            vActividades.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vActividades.IdModalidadSeleccion;
                            vActividades.NumeroRadico = vDataReaderResults["NumeroRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicado"]) : vActividades.NumeroRadico;
                            vActividades.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoSolicitud"]) : vActividades.NumeroRadico;

                        }
                        return vActividades;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarDesarrolloActividad(SolicitudContratoActividadesDesarrolloDetalle item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarDesarrolloInsertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDesarrolloActividad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, item.IdSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, item.IdActivdad);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicial", DbType.DateTime, item.FechaInicial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinal", DbType.DateTime, item.FechaFinalizacion);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, item.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@AplicaPublicacion", DbType.Boolean, item.AplicaPublicacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacion", DbType.DateTime, item.FechaPublicacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, item.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    item.IdDesarrolloActividad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDesarrolloActividad").ToString());
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDesarrolloActividad(SolicitudContratoActividadesDesarrolloDetalle item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarDesarrolloActualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDesarrolloActividad", DbType.Int32, item.IdDesarrolloActividad);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicial", DbType.DateTime, item.FechaInicial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinal", DbType.DateTime, item.FechaFinalizacion);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, item.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@AplicaPublicacion", DbType.Boolean, item.AplicaPublicacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacion", DbType.DateTime, item.FechaPublicacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, item.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int FinalizarGestionActividad(int idDesarrolloActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_FinalizarGestion"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDesarrolloActividad", DbType.Int32, idDesarrolloActividad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarDesarrolloActividadDocumentos(SolicitudContratoActividadesDesarrolloDocumentos item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_DesarrolloActividadDocumento_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumento", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDesarrolloActividad", DbType.Int32, item.IdDesarrolloActividad);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, item.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, item.RutaArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, item.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@NombreOriginal", DbType.String, item.NombreOriginal);

                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    item.IdDocumento = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumento").ToString());
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSDesarrolloActividadDocumentos(int idDesarrolloActividadDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_DesarrolloActividadDocumento_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, idDesarrolloActividadDocumento);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<SolicitudContratoActividadesDesarrolloDocumentos> ConsultarDocumentosPorIdDesarrolloActividad(int idDesarrolloActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_DesarrolloActividadDocumentos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDesarrolloActividad", DbType.Int32, idDesarrolloActividad);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoActividadesDesarrolloDocumentos> vListaSolicitudContratos = new List<SolicitudContratoActividadesDesarrolloDocumentos>();

                        while(vDataReaderResults.Read())
                        {
                            SolicitudContratoActividadesDesarrolloDocumentos vSolicitudContrato = new SolicitudContratoActividadesDesarrolloDocumentos();
                            vSolicitudContrato.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vSolicitudContrato.IdDocumento;
                            vSolicitudContrato.IdDesarrolloActividad = vDataReaderResults["IdDesarrolloActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDesarrolloActividad"].ToString()) : vSolicitudContrato.IdDesarrolloActividad;
                            vSolicitudContrato.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vSolicitudContrato.NombreArchivo;
                            vSolicitudContrato.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vSolicitudContrato.RutaArchivo;
                            vSolicitudContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudContrato.UsuarioCrea;
                            vSolicitudContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudContrato.FechaCrea;
                            vSolicitudContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudContrato.UsuarioModifica;
                            vSolicitudContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudContrato.FechaModifica;
                            vSolicitudContrato.NombreOriginal = vDataReaderResults["NombreOriginal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreOriginal"].ToString()) : vSolicitudContrato.NombreOriginal;

                            vListaSolicitudContratos.Add(vSolicitudContrato);
                        }

                        return vListaSolicitudContratos;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
