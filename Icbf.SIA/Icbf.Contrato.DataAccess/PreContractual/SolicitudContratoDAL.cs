﻿using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess.PreContractual
{
    public class SolicitudContratoDAL : GeneralDAL
    {
        public int InsertarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSolicitud", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand,  "@IdRegional", DbType.Int32, item.IdRegional); 
                    vDataBase.AddInParameter(vDbCommand,  "@IdModalidadSeleccion", DbType.Int32, item.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand,  "@IdCategoriaContrato", DbType.Int32, item.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand,  "@IdTipoContrato", DbType.Int32, item.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand,  "@EsContratoConvenio", DbType.Boolean, item.EsContratoMarco);
                    vDataBase.AddInParameter(vDbCommand,  "@EsContratoAdhesion", DbType.Boolean, item.EsContratoConvenioAdhesion);

                    if(! string.IsNullOrEmpty(item.IdModalidadAcademica))
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadAcademica", DbType.String, item.IdModalidadAcademica);

                    if (! string.IsNullOrEmpty(item.IdProfesion))
                        vDataBase.AddInParameter(vDbCommand, "@IdProfesion", DbType.String, item.IdProfesion);

                    vDataBase.AddInParameter(vDbCommand, "@RequiereGarantia", DbType.Boolean, item.RequiereGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaVigenciasFuturas", DbType.Boolean, item.ManejaVigenciasFuturas);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereActaInicio", DbType.Boolean, item.RequiereActaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaRecursosICBF", DbType.Boolean, item.ManejaRecursosICBF);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaRecursosEspecieICBF", DbType.Boolean, item.ManejaRecursosEspecieICBF);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaCofinanciacion", DbType.Boolean, item.ManejaCofinanciacion);

                    vDataBase.AddInParameter(vDbCommand,  "@UsuarioCrea", DbType.String, item.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaInicial", DbType.Int32, item.IdVigenciaInicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFinal", DbType.Int32, item.IdVigenciaFinal);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    item.IdSolicitud = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSolicitud").ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    
        public int ModificarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitud", DbType.Int32,item.IdSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, item.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, item.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, item.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, item.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@EsContratoConvenio", DbType.Boolean, item.EsContratoMarco);
                    vDataBase.AddInParameter(vDbCommand, "@EsContratoAdhesion", DbType.Boolean, item.EsContratoConvenioAdhesion);

                    if ( ! string.IsNullOrEmpty(item.IdModalidadAcademica))
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadAcademica", DbType.String, item.IdModalidadAcademica);

                    if (!  string.IsNullOrEmpty(item.IdProfesion))
                        vDataBase.AddInParameter(vDbCommand, "@IdProfesion", DbType.String, item.IdProfesion);

                    vDataBase.AddInParameter(vDbCommand, "@RequiereGarantia", DbType.Boolean, item.RequiereGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaVigenciasFuturas", DbType.Boolean, item.ManejaVigenciasFuturas);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereActaInicio", DbType.Boolean, item.RequiereActaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaRecursosICBF", DbType.Boolean, item.ManejaRecursosICBF);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaRecursosEspecieICBF", DbType.Boolean, item.ManejaRecursosEspecieICBF);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaCofinanciacion", DbType.Boolean, item.ManejaCofinanciacion);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, item.UsuarioCrea);

                    if (!string.IsNullOrEmpty(item.UsuarioRevision))
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioRevision", DbType.String, item.UsuarioRevision);
                    
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, item.FechaInicioContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalizacion", DbType.DateTime, item.FechaFinalizaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaInicial", DbType.Int32, item.IdVigenciaInicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFinal", DbType.Int32, item.IdVigenciaFinal);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitud", DbType.Int32, item.IdEstadoSolicitud);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolicitudContrato ConsultarPorId(int idSolicitudContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarSolicitud"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, idSolicitudContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SolicitudContrato vSolicitudContrato = null;

                        while (vDataReaderResults.Read())
                        {
                            vSolicitudContrato = new SolicitudContrato();
                            vSolicitudContrato.IdSolicitud = vDataReaderResults["IdSolicitudContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudContrato"].ToString()) : vSolicitudContrato.IdSolicitud;
                            vSolicitudContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSolicitudContrato.IdContrato;
                            vSolicitudContrato.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vSolicitudContrato.IdEstadoSolicitud;
                            vSolicitudContrato.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoSolicitud"].ToString()) : vSolicitudContrato.EstadoSolicitud;
                            vSolicitudContrato.IdRegional = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vSolicitudContrato.IdRegional;
                            vSolicitudContrato.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vSolicitudContrato.Regional;
                            vSolicitudContrato.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vSolicitudContrato.IdModalidadSeleccion;
                            vSolicitudContrato.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"]) : vSolicitudContrato.ModalidadSeleccion;
                            vSolicitudContrato.NumeroRadicado = vDataReaderResults["NumeroRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicado"]) : vSolicitudContrato.NumeroRadicado;
                            vSolicitudContrato.FechaMaximaTramite = vDataReaderResults["FechaMaximaTramite"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaMaximaTramite"]) : vSolicitudContrato.FechaMaximaTramite;
                            vSolicitudContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudContrato.UsuarioCrea;
                            vSolicitudContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudContrato.FechaCrea;
                            vSolicitudContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudContrato.UsuarioModifica;
                            vSolicitudContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudContrato.FechaModifica;
                            vSolicitudContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vSolicitudContrato.IdCategoriaContrato;
                            vSolicitudContrato.CategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vSolicitudContrato.CategoriaContrato;
                            vSolicitudContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vSolicitudContrato.IdTipoContrato;
                            vSolicitudContrato.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"]) : vSolicitudContrato.TipoContrato;
                            vSolicitudContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"]) : vSolicitudContrato.CodigoTipoContrato;

                            vSolicitudContrato.IdModalidadAcademica = vDataReaderResults["IdModalidadAcademica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidadAcademica"]) : vSolicitudContrato.IdModalidadAcademica;
                            vSolicitudContrato.IdProfesion = vDataReaderResults["IdProfesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdProfesion"]) : vSolicitudContrato.IdProfesion;
                            vSolicitudContrato.RequiereActaInicio = vDataReaderResults["ActaDeInicio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ActaDeInicio"]) : vSolicitudContrato.RequiereActaInicio;
                            vSolicitudContrato.ManejaCofinanciacion = vDataReaderResults["ManejaAporte"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaAporte"]) : vSolicitudContrato.ManejaCofinanciacion;
                            vSolicitudContrato.ManejaRecursosICBF = vDataReaderResults["ManejaRecurso"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaRecurso"]) : vSolicitudContrato.ManejaRecursosICBF;
                            vSolicitudContrato.RequiereGarantia = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"]) : vSolicitudContrato.RequiereGarantia;
                            vSolicitudContrato.ManejaRecursosEspecieICBF = vDataReaderResults["AportesEspecieICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportesEspecieICBF"]) : vSolicitudContrato.ManejaRecursosEspecieICBF;
                            vSolicitudContrato.ManejaVigenciasFuturas = vDataReaderResults["ManejaVigenciasFuturas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaVigenciasFuturas"]) : vSolicitudContrato.ManejaVigenciasFuturas;
                            vSolicitudContrato.EsContratoConvenioAdhesion = vDataReaderResults["ConvenioAdhesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvenioAdhesion"]) : vSolicitudContrato.EsContratoConvenioAdhesion;
                            vSolicitudContrato.EsContratoMarco = vDataReaderResults["ConvenioMarco"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvenioMarco"]) : vSolicitudContrato.EsContratoMarco;

                            vSolicitudContrato.IdentificacionSolicitante = vDataReaderResults["IdEmpleadoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleadoSolicitante"]) : vSolicitudContrato.IdentificacionSolicitante;
                            vSolicitudContrato.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSolicitante"]) : vSolicitudContrato.NombreSolicitante;                           
                            vSolicitudContrato.RegionalSolicitante = vDataReaderResults["RegionalSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegionalSolicitante"]) : vSolicitudContrato.RegionalSolicitante;                           
                            vSolicitudContrato.IdRegionalSolicitante = vDataReaderResults["IdRegionalSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalSolicitante"]) : vSolicitudContrato.IdRegionalSolicitante;
                            vSolicitudContrato.IdCargoSolicitante = vDataReaderResults["IdCargoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoSolicitante"]) : vSolicitudContrato.IdCargoSolicitante;
                            vSolicitudContrato.CargoSolicitante = vDataReaderResults["CargoSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSolicitante"]) : vSolicitudContrato.CargoSolicitante;                           
                            vSolicitudContrato.IdDependenciaSolicitante = vDataReaderResults["IdDependenciaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaSolicitante"]) : vSolicitudContrato.IdDependenciaSolicitante;
                            vSolicitudContrato.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"]) : vSolicitudContrato.DependenciaSolicitante;                           
                            
                            vSolicitudContrato.IdentificacionOrdenadorGasto = vDataReaderResults["IdEmpleadoOrdenadorGasto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleadoOrdenadorGasto"]) : vSolicitudContrato.IdentificacionOrdenadorGasto;
                            vSolicitudContrato.NombreOrdenadorGasto = vDataReaderResults["NombreOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreOrdenadorGasto"]) : vSolicitudContrato.NombreOrdenadorGasto;                           
                            vSolicitudContrato.IdRegionalOrdenadorGasto= vDataReaderResults["IdRegionalOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalOrdenador"]) : vSolicitudContrato.IdRegionalOrdenadorGasto;
                            vSolicitudContrato.RegionalOrdenadorGasto = vDataReaderResults["RegionalOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegionalOrdenadorGasto"]) : vSolicitudContrato.RegionalOrdenadorGasto;                                                       
                            vSolicitudContrato.IdCargoOrdenadorGasto= vDataReaderResults["IdCargoOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoOrdenador"]) : vSolicitudContrato.IdCargoOrdenadorGasto;
                            vSolicitudContrato.CargoOrdenadorGasto = vDataReaderResults["CargoOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoOrdenadorGasto"]) : vSolicitudContrato.CargoOrdenadorGasto;                                                       
                            vSolicitudContrato.IdDependenciaOrdenadorGasto= vDataReaderResults["IdDependenciaOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaOrdenador"]) : vSolicitudContrato.IdDependenciaOrdenadorGasto;
                            vSolicitudContrato.DependenciaOrdenadorGasto = vDataReaderResults["DependenciaOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaOrdenadorGasto"]) : vSolicitudContrato.DependenciaOrdenadorGasto;

                            vSolicitudContrato.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"]) : vSolicitudContrato.IdVigenciaInicial;
                            vSolicitudContrato.FechaInicioContrato = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"]) : vSolicitudContrato.FechaInicioContrato;
                            vSolicitudContrato.IdVigenciaFinal = vDataReaderResults["IdVigenciaFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaFinal"]) : vSolicitudContrato.IdVigenciaFinal;
                            vSolicitudContrato.FechaFinalizaContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"]) : vSolicitudContrato.FechaFinalizaContrato;

                            vSolicitudContrato.ValorContrato = vDataReaderResults["ValorContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorContrato"]) : vSolicitudContrato.ValorContrato;
                            vSolicitudContrato.ValorContratoFinal = vDataReaderResults["ValorContratoFinal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorContratoFinal"]) : vSolicitudContrato.ValorContratoFinal;
                            vSolicitudContrato.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Objeto"]) : vSolicitudContrato.Objeto;
                            vSolicitudContrato.Alcance = vDataReaderResults["Alcance"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Alcance"]) : vSolicitudContrato.Alcance;
                            vSolicitudContrato.IdPlanCompras = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"]) : vSolicitudContrato.IdPlanCompras;

                            vSolicitudContrato.UsuarioArea = vDataReaderResults["UsuarioArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioArea"]) : vSolicitudContrato.UsuarioArea;
                            vSolicitudContrato.UsuarioRevision = vDataReaderResults["UsuarioRevision"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioRevision"]) : vSolicitudContrato.UsuarioRevision;
                            vSolicitudContrato.CodigoEstadoSolicitud = vDataReaderResults["CodigoEstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoSolicitud"]) : vSolicitudContrato.CodigoEstadoSolicitud;

                            vSolicitudContrato.VigenciaFinal = vDataReaderResults["VigenciaFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFinal"]) : vSolicitudContrato.VigenciaFinal;
                            vSolicitudContrato.VigenciaInicial = vDataReaderResults["VigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaInicial"]) : vSolicitudContrato.VigenciaInicial;


                        }
                        return vSolicitudContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContrato> ConsultarSolicitudContrato
            (
             DateTime? vFechaRegistroSistemaDesde, DateTime? vFechaRegistroSistemaHasta, int? vIdSolicitudContrato, int? vVigenciaFiscalinicial, 
             int? vIDRegional, int? vIDModalidadSeleccion, int? vIDCategoriaContrato, int? vIDTipoContrato, int? vIDEstadoSolicitud, string usuarioArea, string usuarioRevision
            )
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarSolicitudes"))
                {
                    if (vFechaRegistroSistemaDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaDesde", DbType.DateTime, vFechaRegistroSistemaDesde.Value);
                    if (vFechaRegistroSistemaHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaHasta", DbType.Int32, vFechaRegistroSistemaHasta.Value);
                    if (vIdSolicitudContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, vIdSolicitudContrato);
                    if (vVigenciaFiscalinicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, vVigenciaFiscalinicial.Value);
                    if (vIDRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, vIDRegional.Value);
                    if (vIDModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDModalidadSeleccion", DbType.Int32, vIDModalidadSeleccion.Value);
                    if (vIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, vIDCategoriaContrato.Value);
                    if (vIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, vIDTipoContrato.Value);
                    if (vIDEstadoSolicitud != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEstadoSolicitud", DbType.Int32, vIDEstadoSolicitud.Value);
                    if (usuarioArea != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioArea", DbType.String, usuarioArea);
                    if (usuarioRevision != null)
                        vDataBase.AddInParameter(vDbCommand, "@usuarioRevision", DbType.String, usuarioRevision);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContrato> vListaSolicitudContratos  = new List<SolicitudContrato>();

                        while (vDataReaderResults.Read())
                        {
                            SolicitudContrato vSolicitudContrato = new SolicitudContrato();
                            vSolicitudContrato.IdSolicitud = vDataReaderResults["IdSolicitudContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudContrato"].ToString()) : vSolicitudContrato.IdSolicitud;
                            vSolicitudContrato.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vSolicitudContrato.IdEstadoSolicitud;
                            vSolicitudContrato.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoSolicitud"].ToString()) : vSolicitudContrato.EstadoSolicitud;
                            vSolicitudContrato.IdRegional = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vSolicitudContrato.IdRegional;
                            vSolicitudContrato.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vSolicitudContrato.Regional;
                            vSolicitudContrato.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vSolicitudContrato.IdModalidadSeleccion;
                            vSolicitudContrato.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"]) : vSolicitudContrato.ModalidadSeleccion;
                            vSolicitudContrato.NumeroRadicado = vDataReaderResults["NumeroRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicado"]) : vSolicitudContrato.NumeroRadicado;
                            vSolicitudContrato.FechaMaximaTramite = vDataReaderResults["FechaMaximaTramite"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaMaximaTramite"]) : vSolicitudContrato.FechaMaximaTramite;
                            vSolicitudContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudContrato.UsuarioCrea;
                            vSolicitudContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudContrato.FechaCrea;
                            vSolicitudContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudContrato.UsuarioModifica;
                            vSolicitudContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudContrato.FechaModifica;
                            vSolicitudContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vSolicitudContrato.IdCategoriaContrato;
                            vSolicitudContrato.CategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vSolicitudContrato.CategoriaContrato;
                            vSolicitudContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vSolicitudContrato.IdTipoContrato;
                            vSolicitudContrato.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"]) : vSolicitudContrato.TipoContrato;
                            vSolicitudContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudContrato.FechaModifica;
                            vSolicitudContrato.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vSolicitudContrato.DependenciaSolicitante;
                            vSolicitudContrato.UsuarioRevision = vDataReaderResults["UsuarioRevision"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioRevision"].ToString()) : vSolicitudContrato.UsuarioRevision;
                            vSolicitudContrato.CodigoEstadoSolicitud = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vSolicitudContrato.CodigoEstadoSolicitud;

                            vListaSolicitudContratos.Add(vSolicitudContrato);
                        }

                        return vListaSolicitudContratos ;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoEstado> ConsultarEstados()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarEstados"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoEstado> vListaSolicitudContratoEstado = new List<SolicitudContratoEstado>();

                        SolicitudContratoEstado vSolicitudContrato = null;

                        while (vDataReaderResults.Read())
                        {
                            vSolicitudContrato = new SolicitudContratoEstado();
                            vSolicitudContrato.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vSolicitudContrato.IdEstadoSolicitud;
                            vSolicitudContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudContrato.UsuarioCrea;
                            vSolicitudContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudContrato.FechaCrea;
                            vSolicitudContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudContrato.UsuarioModifica;
                            vSolicitudContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudContrato.FechaModifica;
                            vSolicitudContrato.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSolicitudContrato.Nombre;
                            vSolicitudContrato.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vSolicitudContrato.Codigo;
                            vSolicitudContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vSolicitudContrato.Estado;
                            vListaSolicitudContratoEstado.Add(vSolicitudContrato);
                        }

                        return vListaSolicitudContratoEstado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoGestion> ConsultarGestionSolicitud(int idSolicitudContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ConsultarSolicitudGestion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, idSolicitudContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoGestion> vListaSolicitudContratos = new List<SolicitudContratoGestion>();

                        while (vDataReaderResults.Read())
                        {
                            SolicitudContratoGestion vSolicitudContrato = new SolicitudContratoGestion();
                            vSolicitudContrato.IdSolicitudContratoGestion = vDataReaderResults["IdSolicitudContratoGestion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudContratoGestion"].ToString()) : vSolicitudContrato.IdSolicitudContratoGestion;
                            vSolicitudContrato.IdSolicitudContrato = vDataReaderResults["IdSolicitudContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudContrato"].ToString()) : vSolicitudContrato.IdSolicitudContrato;
                            vSolicitudContrato.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoSolicitud"].ToString()) : vSolicitudContrato.EstadoSolicitud;
                            vSolicitudContrato.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vSolicitudContrato.Observaciones;
                            vSolicitudContrato.Usuario = vDataReaderResults["Usuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Usuario"].ToString()) : vSolicitudContrato.Usuario;
                            vSolicitudContrato.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vSolicitudContrato.FechaRegistro;
                            vListaSolicitudContratos.Add(vSolicitudContrato);
                        }

                        return vListaSolicitudContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoSolicitud(SolicitudContratoCambioEstado solCambioEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_CambiarEstado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idSolicitudContrato", DbType.Int32, solCambioEstado.IdSolicitudContrato);
                    vDataBase.AddInParameter(vDbCommand, "@codigo", DbType.String, solCambioEstado.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@usuario", DbType.String, solCambioEstado.Usuario);

                    if (solCambioEstado.Fecha.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@fecha", DbType.DateTime, solCambioEstado.Fecha.Value);

                    if (!string.IsNullOrEmpty(solCambioEstado.Observaciones))
                        vDataBase.AddInParameter(vDbCommand, "@observaciones", DbType.String, solCambioEstado.Observaciones);

                    if (!string.IsNullOrEmpty(solCambioEstado.NumeroRadicado))
                        vDataBase.AddInParameter(vDbCommand, "@numeroRadicado", DbType.String, solCambioEstado.NumeroRadicado);

                    if (!string.IsNullOrEmpty(solCambioEstado.IdsDocumentosAprobados))
                        vDataBase.AddInParameter(vDbCommand, "@IdsDocumentosAprobados", DbType.String, solCambioEstado.IdsDocumentosAprobados);


                    int vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<string,string> ValidarSolicitudContrato(int idSolicitud)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContrato_ValidarRegistro"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, idSolicitud);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Dictionary<string, string> vListaValidacion = new Dictionary<string, string>();

                        string key, value = string.Empty;    

                        while (vDataReaderResults.Read())
                        {
                            key = vDataReaderResults["Titulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Titulo"].ToString()) : string.Empty;
                            value = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) :string.Empty;

                            if(key != string.Empty) 
                                vListaValidacion.Add(key,value);
                        }

                        return vListaValidacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoDocumentos> ConsultarDocumentosPorIdSolicitud(int idSolicitud)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContratoDocumentos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, idSolicitud);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoDocumentos> vListaSolicitudContratos = new List<SolicitudContratoDocumentos>();

                        while (vDataReaderResults.Read())
                        {
                            SolicitudContratoDocumentos vSolicitudContrato = new SolicitudContratoDocumentos();
                            vSolicitudContrato.IdSolicitudContratoDocumento = vDataReaderResults["IdSolicitudContratoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudContratoDocumento"].ToString()) : vSolicitudContrato.IdSolicitudContratoDocumento;
                            vSolicitudContrato.IdSolicitudContrato = vDataReaderResults["IdSolicitudContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudContrato"].ToString()) : vSolicitudContrato.IdSolicitudContrato;
                            vSolicitudContrato.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vSolicitudContrato.IdTipoDocumento;
                            vSolicitudContrato.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vSolicitudContrato.NombreArchivo;
                            vSolicitudContrato.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vSolicitudContrato.RutaArchivo;
                            vSolicitudContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudContrato.UsuarioCrea;
                            vSolicitudContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudContrato.FechaCrea;
                            vSolicitudContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudContrato.UsuarioModifica;
                            vSolicitudContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudContrato.FechaModifica;
                            vSolicitudContrato.TipoDocumento = vDataReaderResults["TipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumento"].ToString()) : vSolicitudContrato.TipoDocumento;
                            vSolicitudContrato.Aprobado = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Aprobado"].ToString()) : vSolicitudContrato.Aprobado;
                       
                            vListaSolicitudContratos.Add(vSolicitudContrato);
                        }

                        return vListaSolicitudContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarDocumentosFaltantesPorCrear(int idSolicitud, int idModalidadSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContratoDocumento_ConsultarFaltantes"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContrato", DbType.Int32, idSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, idModalidadSeleccion);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Dictionary<int, string> vLista = new Dictionary<int, string>();

                        int key;
                        string value = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            key = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : 0;
                            value = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : string.Empty;
                            vLista.Add(key, value);
                        }

                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarSolicitudContratoDocumentos(SolicitudContratoDocumentos item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContratoDocumentos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSolicitudDocumento", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand,  "@IdSolicitudContrato", DbType.Int32, item.IdSolicitudContrato);
                    vDataBase.AddInParameter(vDbCommand,  "@IdTipoDocumento", DbType.Int32, item.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand,  "@NombreArchivo", DbType.String, item.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand,  "@RutaArchivo", DbType.String, item.RutaArchivo);
                    vDataBase.AddInParameter(vDbCommand,  "@UsuarioCrea", DbType.String, item.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    item.IdSolicitudContratoDocumento = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSolicitudDocumento").ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSolicitudContratoDocumentos(int idSolicitudDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_SolicitudContratoDocumento_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudDocumento", DbType.Int32, idSolicitudDocumento);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
