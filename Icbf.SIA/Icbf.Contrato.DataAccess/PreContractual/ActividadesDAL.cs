﻿using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq; 
using System.Text;

namespace Icbf.Contrato.DataAccess.PreContractual
{
    public class ActividadesDAL : GeneralDAL
    {
        public int InsertarActividad(SolicitudContratoActividad  pActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdActividad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pActividad.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pActividad.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pActividad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pActividad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@Espadre", DbType.Boolean, pActividad.EsPadre);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, pActividad.ModalidadesSeleccion);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pActividad.IdActividad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdActividad").ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividad(SolicitudContratoActividad pActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, pActividad.IdActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pActividad.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pActividad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pActividad.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pActividad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, pActividad.ModalidadesSeleccion);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividadDocumentos(int idActividad, string tiposDocumentos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_ModificarDocumentos"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, idActividad);
                    vDataBase.AddInParameter(vDbCommand, "@TiposDocumentos", DbType.String, tiposDocumentos);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividadSubActividadDocumentos(int idActividad, string tiposDocumentos, string subActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_ModificarSubActividadesDocumentos"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, idActividad);
                    if (!string.IsNullOrEmpty(tiposDocumentos))
                    vDataBase.AddInParameter(vDbCommand, "@TiposDocumentos", DbType.String, tiposDocumentos);
                    if(! string.IsNullOrEmpty(subActividades))
                    vDataBase.AddInParameter(vDbCommand, "@SubActividades", DbType.String, subActividades);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarActividad(SolicitudContratoActividad  pActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, pActividad.IdActividad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolicitudContratoActividad ConsultarActividadPorId(int pIdSolicitudContratoActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudContratoActividades", DbType.Int32, pIdSolicitudContratoActividades);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SolicitudContratoActividad vActividades = null;

                        while (vDataReaderResults.Read())
                        {
                            vActividades = new SolicitudContratoActividad();
                            vActividades.IdActividad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActividad;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vActividades.Nombre;
                            vActividades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"]) : vActividades.Descripcion;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            vActividades.ModalidadesSeleccion = vDataReaderResults["ModalidadesSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadesSeleccion"].ToString()) : vActividades.ModalidadesSeleccion;
                            vActividades.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vActividades.Estado;
                        }

                        return vActividades;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarActividades(int? pIdModalidadSeleccion, String pNombre, int? pTipoDocumento, bool? pEstado, bool esPadre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_Consultar"))
                {
                    if (pIdModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion.Value);
                    if (!string.IsNullOrEmpty(pNombre))
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTipoDocumento.Value);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado.Value);

                    vDataBase.AddInParameter(vDbCommand, "@Espadre", DbType.Boolean, esPadre);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoActividad> vListaActividaes = new List<SolicitudContratoActividad>();

                        while (vDataReaderResults.Read())
                        {
                            SolicitudContratoActividad vActividades = new SolicitudContratoActividad();
                            vActividades.IdActividad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActividad;
                            vActividades.EsPadre = vDataReaderResults["EsPadre"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPadre"].ToString()) : vActividades.EsPadre;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vActividades.Nombre;
                            vActividades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"]) : vActividades.Descripcion;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            vActividades.ModalidadesSeleccion = vDataReaderResults["ModalidadesSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadesSeleccion"].ToString()) : vActividades.ModalidadesSeleccion;
                            vActividades.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vActividades.Estado;
                            vListaActividaes.Add(vActividades);
                        }

                        return vListaActividaes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarSubActividadesPorActividad(int pIdActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_ConsultarSubActiviades"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, pIdActividad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoActividad> vListaActividaes = new List<SolicitudContratoActividad>();

                        while (vDataReaderResults.Read())
                        {
                            SolicitudContratoActividad vActividades = new SolicitudContratoActividad();
                            vActividades.IdActividad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActividad;
                            vActividades.EsPadre = vDataReaderResults["EsPadre"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPadre"].ToString()) : vActividades.EsPadre;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vActividades.Nombre;
                            vActividades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"]) : vActividades.Descripcion;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            vListaActividaes.Add(vActividades);
                        }

                        return vListaActividaes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarSubActividadesPosiblesPorActividad(int pIdActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_ConsultarPosiblesSubActiviades"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, pIdActividad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudContratoActividad> vListaActividaes = new List<SolicitudContratoActividad>();

                        while (vDataReaderResults.Read())
                        {
                            SolicitudContratoActividad vActividades = new SolicitudContratoActividad();
                            vActividades.IdActividad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActividad;
                            vActividades.EsPadre = vDataReaderResults["EsPadre"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPadre"].ToString()) : vActividades.EsPadre;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vActividades.Nombre;
                            vActividades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"]) : vActividades.Descripcion;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            vListaActividaes.Add(vActividades);
                        }

                        return vListaActividaes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int,string> ConsultarModalidadSeleccionPorIdActividad(int pIdActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_PreContractual_Actividades_ConsultarModalidades"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, pIdActividad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Dictionary<int, string> vListaModalidades = new Dictionary<int, string>();
                        int key;
                        string value;
                        while (vDataReaderResults.Read())
                        {
                            key = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : 0;
                            value = vDataReaderResults["Modalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Modalidad"].ToString()) : string.Empty;
                            vListaModalidades.Add(key, value);
                        }

                        return vListaModalidades;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
