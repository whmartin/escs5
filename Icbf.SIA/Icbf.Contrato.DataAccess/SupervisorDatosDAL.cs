﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class SupervisorDatosDAL : GeneralDAL
    {
        public SupervisorDatosDAL()
        { }
        
        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterventor
        /// </summary>
        public List<SupervisorTipoIdentificacion> ConsultarSupervisorTipoIdentificacions()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_KACTUS_KPRODII_TipoIdentificacions_Consultar"))
                {
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisorTipoIdentificacion> vListaSupervisorTipoIdentificacion = new List<SupervisorTipoIdentificacion>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisorTipoIdentificacion vSupervisorTipoIdentificacion = new SupervisorTipoIdentificacion();

                            vSupervisorTipoIdentificacion.ID_Ident = vDataReaderResults["ID_Ident"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ID_Ident"].ToString()) : vSupervisorTipoIdentificacion.ID_Ident;
                            vSupervisorTipoIdentificacion.desc_ide = vDataReaderResults["desc_ide"] != DBNull.Value ? Convert.ToString(vDataReaderResults["desc_ide"].ToString()) : vSupervisorTipoIdentificacion.desc_ide;
                            vListaSupervisorTipoIdentificacion.Add(vSupervisorTipoIdentificacion);
                        }
                        return vListaSupervisorTipoIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorTipoVinculacion
        /// </summary>
        public List<SupervisorTipoVinculacion> ConsultarSupervisorTipoVinculacions()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_KACTUS_KPRODII_TipoVinculacions_Consultar"))
                {

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisorTipoVinculacion> vListaSupervisorTipoVinculacion = new List<SupervisorTipoVinculacion>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisorTipoVinculacion vSupervisorTipoVinculacion = new SupervisorTipoVinculacion();

                            vSupervisorTipoVinculacion.ID_vincu = vDataReaderResults["cod_tnom"] != DBNull.Value ? Convert.ToString(vDataReaderResults["cod_tnom"].ToString()) : vSupervisorTipoVinculacion.ID_vincu;
                            vSupervisorTipoVinculacion.desc_vin = vDataReaderResults["Nom_tnom"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nom_tnom"].ToString()) : vSupervisorTipoVinculacion.desc_vin;
                            vListaSupervisorTipoVinculacion.Add(vSupervisorTipoVinculacion);
                        }
                        return vListaSupervisorTipoVinculacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorRegional
        /// </summary>
        public List<SupervisorRegional> ConsultarSupervisorRegionals()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_KACTUS_KPRODII_Regionals_Consultar"))
                {

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisorRegional> vListaSupervisorRegional = new List<SupervisorRegional>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisorRegional vSupervisorRegional = new SupervisorRegional();

                            vSupervisorRegional.ID_regio = vDataReaderResults["ID_regio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ID_regio"].ToString()) : vSupervisorRegional.ID_regio;
                            vSupervisorRegional.regional = vDataReaderResults["regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["regional"].ToString()) : vSupervisorRegional.regional;
                            vListaSupervisorRegional.Add(vSupervisorRegional);
                        }
                        return vListaSupervisorRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
