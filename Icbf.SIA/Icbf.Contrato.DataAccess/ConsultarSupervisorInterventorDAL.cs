using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad SupervisorInterventor
    /// </summary>
    public class ConsultarSupervisorInterventorDAL : GeneralDAL
    {
        public ConsultarSupervisorInterventorDAL()
        {
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad ConsultarSupervisorInterventor
        /// </summary>
        /// <param name="pTipoSupervisorInterventor"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pNombreRazonSocialSupervisorInterventor"></param>
        /// <param name="pNumeroIdentificacionDirectorInterventoria"></param>
        /// <param name="pNombreRazonSocialDirectorInterventoria"></param>
        /// <returns></returns>
        public DataTable ConsultarConsultarSupervisorInterventors(int? pTipoSupervisorInterventor, String pNumeroContrato, String pNumeroIdentificacion, String pNombreRazonSocialSupervisorInterventor, String pNumeroIdentificacionDirectorInterventoria, String pNombreRazonSocialDirectorInterventoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ConsultarSupervisorInterventors_Consultar"))
                {
                    if(pTipoSupervisorInterventor != null)
                         vDataBase.AddInParameter(vDbCommand, "@TipoSupervisorInterventor", DbType.Int32, pTipoSupervisorInterventor);
                    if(pNumeroContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if(pNumeroIdentificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    if(pNombreRazonSocialSupervisorInterventor != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreRazonSocialSupervisorInterventor", DbType.String, pNombreRazonSocialSupervisorInterventor);
                    if(pNumeroIdentificacionDirectorInterventoria != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionDirectorInterventoria", DbType.String, pNumeroIdentificacionDirectorInterventoria);
                    if(pNombreRazonSocialDirectorInterventoria != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreRazonSocialDirectorInterventoria", DbType.String, pNombreRazonSocialDirectorInterventoria);
                    
                        return vDataBase.ExecuteDataSet(vDbCommand).Tables[0];
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
