using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.DataAccess;

namespace Icbf.contrato.DataAccess
{
    public class TipoModificacionDAL : GeneralDAL
    {
        public TipoModificacionDAL()
        {
        }
        
        public int InsertarTipoModificacion(TipoModificacion pTipoModificacionBasica, string idsAsociados)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand,  "@Descripcion", DbType.String, pTipoModificacionBasica.Descripcion);
                    vDataBase.AddInParameter(vDbCommand,  "@RequiereModificacion", DbType.Boolean, pTipoModificacionBasica.RequiereModificacion);
                    vDataBase.AddInParameter(vDbCommand,  "@Estado", DbType.Boolean, pTipoModificacionBasica.Estado);
                    vDataBase.AddInParameter(vDbCommand,  "@UsuarioCrea", DbType.String, pTipoModificacionBasica.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand,  "@IdsAsociados", DbType.String, idsAsociados);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoModificacionBasica.IdTipoModificacionBasica = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoModificacion").ToString());
                    GenerarLogAuditoria(pTipoModificacionBasica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        
        public int ModificarTipoModificacion(TipoModificacion pTipoModificacionBasica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, pTipoModificacionBasica.IdTipoModificacionBasica);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereModificacion", DbType.Int32, pTipoModificacionBasica.RequiereModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pTipoModificacionBasica.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoModificacionBasica.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoModificacionBasica.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@IdsAsociados", DbType.String, pTipoModificacionBasica.IdsAsociados);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoModificacionBasica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TipoModificacion ConsultarTipoModificacion(int pIdTipoModificacionBasica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, pIdTipoModificacionBasica);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoModificacion vTipoModificacionBasica = new TipoModificacion();
                        while (vDataReaderResults.Read())
                        {
                            vTipoModificacionBasica.Codigo = vDataReaderResults["codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["codigo"].ToString()) : vTipoModificacionBasica.Codigo;
                            vTipoModificacionBasica.IdTipoModificacionBasica = vDataReaderResults["IdTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModificacionContractual"].ToString()) : vTipoModificacionBasica.IdTipoModificacionBasica;
                            vTipoModificacionBasica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoModificacionBasica.Descripcion;
                            vTipoModificacionBasica.RequiereModificacion = vDataReaderResults["RequiereModificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereModificacion"].ToString()) : vTipoModificacionBasica.RequiereModificacion;
                            vTipoModificacionBasica.EsPorDefecto = vDataReaderResults["EsPorDefecto"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPorDefecto"].ToString()) : vTipoModificacionBasica.EsPorDefecto;                
                            vTipoModificacionBasica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoModificacionBasica.Estado;
                            vTipoModificacionBasica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoModificacionBasica.UsuarioCrea;
                            vTipoModificacionBasica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoModificacionBasica.FechaCrea;
                            vTipoModificacionBasica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoModificacionBasica.UsuarioModifica;
                            vTipoModificacionBasica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoModificacionBasica.FechaModifica;
                            vTipoModificacionBasica.GeneraNuevaGarantia = vDataReaderResults["GeneraNuevaGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["GeneraNuevaGarantia"].ToString()) : vTipoModificacionBasica.GeneraNuevaGarantia;
                        }
                        return vTipoModificacionBasica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ConsultarExistenciaCombinacion(string idsAsociados, int idTipoModificacion)
        {
            bool result = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificacion_ConsultarExistenciaCombinacion"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdsAsociados", DbType.String, idsAsociados);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, idTipoModificacion);
                    vDataBase.AddOutParameter(vDbCommand, "@existeCombinacion", DbType.Boolean,2);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    result = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@existeCombinacion").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public List<TipoModificacion> ConsultarTipoModificacion(int? pIdTipoModificacionBasica, String pDescripcion, Boolean? pRequiereModificacion, Boolean? pEstado, string codigo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificaciones_Consultar"))
                {
                    if(pIdTipoModificacionBasica != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacionBasica", DbType.Int32, pIdTipoModificacionBasica);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (codigo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, codigo);
                    if(pRequiereModificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@RequiereModificacion", DbType.Int32, pRequiereModificacion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoModificacion> vListaTipoModificacionBasica = new List<TipoModificacion>();

                        while (vDataReaderResults.Read())
                        {
                            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
                            vTipoModificacionBasica.IdTipoModificacionBasica = vDataReaderResults["IdTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModificacionContractual"].ToString()) : vTipoModificacionBasica.IdTipoModificacionBasica;
                            vTipoModificacionBasica.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vTipoModificacionBasica.Codigo;
                            vTipoModificacionBasica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoModificacionBasica.Descripcion;
                            vTipoModificacionBasica.RequiereModificacion = vDataReaderResults["RequiereModificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereModificacion"].ToString()) : vTipoModificacionBasica.RequiereModificacion;
                            vTipoModificacionBasica.EsPorDefecto = vDataReaderResults["EsPorDefecto"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPorDefecto"].ToString()) : vTipoModificacionBasica.EsPorDefecto;
                            vTipoModificacionBasica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoModificacionBasica.Estado;
                            vTipoModificacionBasica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoModificacionBasica.UsuarioCrea;
                            vTipoModificacionBasica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoModificacionBasica.FechaCrea;
                            vTipoModificacionBasica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoModificacionBasica.UsuarioModifica;
                            vTipoModificacionBasica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoModificacionBasica.FechaModifica;
                            vTipoModificacionBasica.GeneraNuevaGarantia = vDataReaderResults["GeneraNuevaGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["GeneraNuevaGarantia"].ToString()) : vTipoModificacionBasica.GeneraNuevaGarantia;
                            vListaTipoModificacionBasica.Add(vTipoModificacionBasica);
                        }
                        return vListaTipoModificacionBasica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoModificacion> ConsultarTipoModificacionPorDefecto(bool todos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificaciones_ConsultarPorDefecto"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@obtenerTodas", DbType.Boolean, todos);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoModificacion> vListaTipoModificacionBasica = new List<TipoModificacion>();
                        while (vDataReaderResults.Read())
                        {
                            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
                            vTipoModificacionBasica.IdTipoModificacionBasica = vDataReaderResults["IdTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModificacionContractual"].ToString()) : vTipoModificacionBasica.IdTipoModificacionBasica;
                            vTipoModificacionBasica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoModificacionBasica.Descripcion;
                            vTipoModificacionBasica.RequiereModificacion = vDataReaderResults["RequiereModificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereModificacion"].ToString()) : vTipoModificacionBasica.RequiereModificacion;
                            vTipoModificacionBasica.EsPorDefecto = vDataReaderResults["EsPorDefecto"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPorDefecto"].ToString()) : vTipoModificacionBasica.EsPorDefecto;
                            vTipoModificacionBasica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoModificacionBasica.Estado;
                            vTipoModificacionBasica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoModificacionBasica.UsuarioCrea;
                            vTipoModificacionBasica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoModificacionBasica.FechaCrea;
                            vTipoModificacionBasica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoModificacionBasica.UsuarioModifica;
                            vTipoModificacionBasica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoModificacionBasica.FechaModifica;
                            vListaTipoModificacionBasica.Add(vTipoModificacionBasica);
                        }
                        return vListaTipoModificacionBasica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoModificacion> ConsultarTipoModificacionHijos(int pIdTipoModificacionBasica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificaciones_ConsultarHijos"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacionBasica", DbType.Int32, pIdTipoModificacionBasica);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoModificacion> vListaTipoModificacionBasica = new List<TipoModificacion>();
                        while (vDataReaderResults.Read())
                        {
                            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
                            vTipoModificacionBasica.IdTipoModificacionBasica = vDataReaderResults["IdTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModificacionContractual"].ToString()) : vTipoModificacionBasica.IdTipoModificacionBasica;
                            vTipoModificacionBasica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoModificacionBasica.Descripcion;
                            vTipoModificacionBasica.RequiereModificacion = vDataReaderResults["RequiereModificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereModificacion"].ToString()) : vTipoModificacionBasica.RequiereModificacion;
                            vTipoModificacionBasica.EsPorDefecto = vDataReaderResults["EsPorDefecto"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPorDefecto"].ToString()) : vTipoModificacionBasica.EsPorDefecto;
                            vTipoModificacionBasica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoModificacionBasica.Estado;
                            vTipoModificacionBasica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoModificacionBasica.UsuarioCrea;
                            vTipoModificacionBasica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoModificacionBasica.FechaCrea;
                            vTipoModificacionBasica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoModificacionBasica.UsuarioModifica;
                            vTipoModificacionBasica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoModificacionBasica.FechaModifica;
                            vListaTipoModificacionBasica.Add(vTipoModificacionBasica);
                        }
                        return vListaTipoModificacionBasica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ConsultarPuedeEliminar(int pIdTipoModificacion)
        {
            bool result = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificacion_PuedeEliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, pIdTipoModificacion);
                    vDataBase.AddOutParameter(vDbCommand, "@puedeEliminar", DbType.Boolean, 2);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    result = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@puedeEliminar").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public int EliminarModificacion(int pIdTipoModificacion)
        {
            int result = 0;

            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificacion_Eliminar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, pIdTipoModificacion);
                    result = vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }
    }
}
