using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Tipo cl�usula
    /// </summary>
    public class TipoClausulaDAL : GeneralDAL
    {
        public TipoClausulaDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int InsertarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoClausula_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoClausula", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoClausula", DbType.String, pTipoClausula.NombreTipoClausula);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoClausula.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoClausula.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoClausula.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoClausula.IdTipoClausula = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoClausula").ToString());
                    GenerarLogAuditoria(pTipoClausula, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int ModificarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoClausula_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoClausula", DbType.Int32, pTipoClausula.IdTipoClausula);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoClausula", DbType.String, pTipoClausula.NombreTipoClausula);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoClausula.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoClausula.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoClausula.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoClausula, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int EliminarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoClausula_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoClausula", DbType.Int32, pTipoClausula.IdTipoClausula);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoClausula, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad TipoClausula
        /// </summary>
        /// <param name="pIdTipoClausula"></param>
        /// <returns></returns>
        public TipoClausula ConsultarTipoClausula(int pIdTipoClausula)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoClausula_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoClausula", DbType.Int32, pIdTipoClausula);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoClausula vTipoClausula = new TipoClausula();
                        while (vDataReaderResults.Read())
                        {
                            vTipoClausula.IdTipoClausula = vDataReaderResults["IdTipoClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoClausula"].ToString()) : vTipoClausula.IdTipoClausula;
                            vTipoClausula.NombreTipoClausula = vDataReaderResults["NombreTipoClausula"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoClausula"].ToString()) : vTipoClausula.NombreTipoClausula;
                            vTipoClausula.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoClausula.Descripcion;
                            vTipoClausula.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoClausula.Estado;
                            vTipoClausula.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoClausula.UsuarioCrea;
                            vTipoClausula.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoClausula.FechaCrea;
                            vTipoClausula.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoClausula.UsuarioModifica;
                            vTipoClausula.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoClausula.FechaModifica;
                        }
                        return vTipoClausula;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoClausula
        /// </summary>
        /// <param name="pNombreTipoClausula"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoClausula> ConsultarTipoClausulas(String pNombreTipoClausula, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoClausulas_Consultar"))
                {
                    if(pNombreTipoClausula != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoClausula", DbType.String, pNombreTipoClausula);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoClausula> vListaTipoClausula = new List<TipoClausula>();
                        while (vDataReaderResults.Read())
                        {
                            TipoClausula vTipoClausula = new TipoClausula();
                            vTipoClausula.IdTipoClausula = vDataReaderResults["IdTipoClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoClausula"].ToString()) : vTipoClausula.IdTipoClausula;
                            vTipoClausula.NombreTipoClausula = vDataReaderResults["NombreTipoClausula"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoClausula"].ToString()) : vTipoClausula.NombreTipoClausula;
                            vTipoClausula.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoClausula.Descripcion;
                            vTipoClausula.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoClausula.Estado;
                            if (vTipoClausula.Estado)
                            {
                                vTipoClausula.EstadoString = "Activo";
                            } else
                            {
                                vTipoClausula.EstadoString = "Inactivo";
                            }
                            vTipoClausula.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoClausula.UsuarioCrea;
                            vTipoClausula.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoClausula.FechaCrea;
                            vTipoClausula.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoClausula.UsuarioModifica;
                            vTipoClausula.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoClausula.FechaModifica;
                            vListaTipoClausula.Add(vTipoClausula);
                        }
                        return vListaTipoClausula;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
