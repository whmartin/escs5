﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class ImposicionMultaDAL : GeneralDAL
    {
        public int InsertarImposicionMulta(ImposicionMulta pImposicionMulta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_ImposicionMultas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdImposicionMulta", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pImposicionMulta.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@Razones", DbType.String, pImposicionMulta.Razones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pImposicionMulta.UsuarioCrea);                   

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pImposicionMulta.IdImposicionMulta = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdImposicionMulta").ToString());
                    GenerarLogAuditoria(pImposicionMulta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarImposicionMulta(ImposicionMulta pImposicionMulta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_ImposicionMultas_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdImposicionMulta", DbType.Int32, pImposicionMulta.IdImposicionMulta);
                    vDataBase.AddInParameter(vDbCommand, "@Razones", DbType.String, pImposicionMulta.Razones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pImposicionMulta.UsuarioModifica);

                    vDataBase.AddInParameter(vDbCommand, "@FechaEnvioProcesoSancionatorio", DbType.DateTime, pImposicionMulta.FechaEnvioProcesoSancionatorio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRadicadoCitacion", DbType.String, pImposicionMulta.NumeroRadicadoCitacion);

                    vDataBase.AddInParameter(vDbCommand, "@FechaCelebracionAudiencia", DbType.DateTime, pImposicionMulta.FechaCelebracionAudiencia);
                    vDataBase.AddInParameter(vDbCommand, "@InformeEjecutivo", DbType.String, pImposicionMulta.InformeEjecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoAudiencia", DbType.String, pImposicionMulta.EstadoAudiencia);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoEstado", DbType.String, pImposicionMulta.MotivoEstado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroResolucion", DbType.String, pImposicionMulta.NumeroResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaResolucion", DbType.DateTime, pImposicionMulta.FechaResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@Resuleve", DbType.String, pImposicionMulta.Resuleve);
                    vDataBase.AddInParameter(vDbCommand, "@TipoResuelve", DbType.String, pImposicionMulta.TipoResuelve);
                    vDataBase.AddInParameter(vDbCommand, "@InterponeRecurso", DbType.Boolean, pImposicionMulta.InterponeRecurso);
                    vDataBase.AddInParameter(vDbCommand, "@ValorSancion", DbType.Decimal, pImposicionMulta.ValorSancion);
                    vDataBase.AddInParameter(vDbCommand, "@DesicionRecurso", DbType.String, pImposicionMulta.DesicionRecurso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEjecutoriaResolucion", DbType.DateTime, pImposicionMulta.FechaEjecutoriaResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacionSECOP", DbType.DateTime, pImposicionMulta.FechaPublicacionSECOP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEnvioCamaraComercio", DbType.DateTime, pImposicionMulta.FechaEnvioCamaraComercio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaComunicacionProcuraduria", DbType.DateTime, pImposicionMulta.FechaComunicacionProcuraduria);
                    vDataBase.AddInParameter(vDbCommand, "@ValorSancionModificacion", DbType.Decimal, pImposicionMulta.ValorSancionModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@TipoSancion", DbType.String, pImposicionMulta.TipoSancion);
                    if (pImposicionMulta.Aprobado.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@Aprobado", DbType.Boolean, pImposicionMulta.Aprobado.Value);
 
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pImposicionMulta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMulta(int IdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_TerminacionAnticipada_ConsultarPorContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ImposicionMulta> vLista = new List<ImposicionMulta>();
                        while (vDataReaderResults.Read())
                        {
                            ImposicionMulta vAdiciones = new ImposicionMulta();
                            vAdiciones.IdImposicionMulta = vDataReaderResults["IdImposicionMulta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdImposicionMulta"].ToString()) : vAdiciones.IdImposicionMulta;
                            vAdiciones.Razones = vDataReaderResults["Razones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razones"].ToString()) : vAdiciones.Razones;
                            vAdiciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vAdiciones.IDDetalleConsModContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;
                            vLista.Add(vAdiciones);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMultaId(int idImposicionMulta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_ImposicionMultas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdImposicionMulta", DbType.Int32, idImposicionMulta);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ImposicionMulta> vLista = new List<ImposicionMulta>();
                        while (vDataReaderResults.Read())
                        {
                            ImposicionMulta vAdiciones = new ImposicionMulta();
                            vAdiciones.IdImposicionMulta = vDataReaderResults["IdImposicionMulta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdImposicionMulta"].ToString()) : vAdiciones.IdImposicionMulta;
                            vAdiciones.Razones = vDataReaderResults["Razones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razones"].ToString()) : vAdiciones.Razones;
                            vAdiciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vAdiciones.IDDetalleConsModContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;

                            vAdiciones.FechaEnvioProcesoSancionatorio = vDataReaderResults["FechaEnvioProcesoSancionatorio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioProcesoSancionatorio"].ToString()) : vAdiciones.FechaEnvioProcesoSancionatorio;
                            vAdiciones.NumeroRadicadoCitacion = vDataReaderResults["NumeroRadicadoCitacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicadoCitacion"].ToString()) : vAdiciones.NumeroRadicadoCitacion;
                            vAdiciones.FechaCelebracionAudiencia = vDataReaderResults["FechaCelebracionAudiencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCelebracionAudiencia"].ToString()) : vAdiciones.FechaCelebracionAudiencia;
                            vAdiciones.InformeEjecutivo = vDataReaderResults["InformeEjecutivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformeEjecutivo"].ToString()) : vAdiciones.InformeEjecutivo;
                            vAdiciones.EstadoAudiencia = vDataReaderResults["EstadoAudiencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoAudiencia"].ToString()) : vAdiciones.EstadoAudiencia;
                            vAdiciones.MotivoEstado = vDataReaderResults["MotivoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoEstado"].ToString()) : vAdiciones.MotivoEstado;
                            vAdiciones.NumeroResolucion = vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroResolucion"].ToString()) : vAdiciones.NumeroResolucion;
                            vAdiciones.FechaResolucion = vDataReaderResults["FechaResolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaResolucion"].ToString()) : vAdiciones.FechaResolucion;
                            vAdiciones.Resuleve = vDataReaderResults["Resuleve"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Resuleve"].ToString()) : vAdiciones.Resuleve;
                            vAdiciones.TipoResuelve = vDataReaderResults["Resuleve"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoResuelve"].ToString()) : vAdiciones.TipoResuelve;
                            vAdiciones.ValorSancion = vDataReaderResults["ValorSancion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorSancion"].ToString()) : vAdiciones.ValorSancion;
                            vAdiciones.InterponeRecurso = vDataReaderResults["InterponeRecurso"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["InterponeRecurso"].ToString()) : vAdiciones.InterponeRecurso;
                            vAdiciones.DesicionRecurso = vDataReaderResults["DesicionRecurso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DesicionRecurso"].ToString()) : vAdiciones.DesicionRecurso;
                            vAdiciones.FechaEjecutoriaResolucion = vDataReaderResults["FechaEjecutoriaResolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEjecutoriaResolucion"].ToString()) : vAdiciones.FechaEjecutoriaResolucion;
                            vAdiciones.FechaPublicacionSECOP = vDataReaderResults["FechaPublicacionSECOP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacionSECOP"].ToString()) : vAdiciones.FechaPublicacionSECOP;
                            vAdiciones.FechaEnvioCamaraComercio = vDataReaderResults["FechaEnvioCamaraComercio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioCamaraComercio"].ToString()) : vAdiciones.FechaEnvioCamaraComercio;
                            vAdiciones.FechaComunicacionProcuraduria = vDataReaderResults["FechaComunicacionProcuraduria"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaComunicacionProcuraduria"].ToString()) : vAdiciones.FechaComunicacionProcuraduria;
                            vAdiciones.ValorSancionModificacion = vDataReaderResults["ValorSancionModificacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorSancionModificacion"].ToString()) : vAdiciones.ValorSancionModificacion;
                            vAdiciones.TipoSancion = vDataReaderResults["TipoSancion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSancion"].ToString()) : vAdiciones.TipoSancion;
                            vAdiciones.Aprobado = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Aprobado"].ToString()) : vAdiciones.Aprobado;
                                                                   
                            vLista.Add(vAdiciones);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMultaIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_ImposicionMultas_ConsultarPorDetalleConsModContractual"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDetConsModContractual", DbType.Int32, IdDetConsModContractual);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ImposicionMulta> vLista = new List<ImposicionMulta>();
                        while (vDataReaderResults.Read())
                        {
                            ImposicionMulta vAdiciones = new ImposicionMulta();
                            vAdiciones.IdImposicionMulta = vDataReaderResults["IdImposicionMulta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdImposicionMulta"].ToString()) : vAdiciones.IdImposicionMulta;
                            vAdiciones.Razones = vDataReaderResults["Razones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razones"].ToString()) : vAdiciones.Razones;
                            vAdiciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vAdiciones.IDDetalleConsModContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;
                            vLista.Add(vAdiciones);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMultaHistorico> ConsultarHistoricoProcesoImpMultas(int idImposicion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_ImposicionMultas_ConsultarHistorico"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdImposicion", DbType.Int32, idImposicion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ImposicionMultaHistorico> vLista = new List<ImposicionMultaHistorico>();
                        while (vDataReaderResults.Read())
                        {
                            ImposicionMultaHistorico vImpMultas = new ImposicionMultaHistorico();
                            vImpMultas.IdHistoricoProcesoImpMultas = vDataReaderResults["IdImposicionMultaHistorico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdImposicionMultaHistorico"].ToString()) : vImpMultas.IdHistoricoProcesoImpMultas;
                            vImpMultas.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : vImpMultas.Fecha;
                            vImpMultas.Motivo = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vImpMultas.Motivo;
                            vImpMultas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vImpMultas.UsuarioCrea;
                            vImpMultas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vImpMultas.FechaCrea;
                            vLista.Add(vImpMultas);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
