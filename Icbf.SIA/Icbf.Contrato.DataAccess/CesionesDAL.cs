using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class CesionesDAL : GeneralDAL
    {
        public CesionesDAL()
        {
        }
        public int InsertarCesiones(Cesiones pCesiones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCesion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCesion", DbType.DateTime, pCesiones.FechaCesion);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pCesiones.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pCesiones.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pCesiones.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCesiones.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCesion", DbType.Decimal, pCesiones.ValorCesion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorEjecutado", DbType.Decimal, pCesiones.ValorEjecutado);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCesiones.IdCesion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCesion").ToString());
                    GenerarLogAuditoria(pCesiones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarCesiones(Cesiones pCesiones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, pCesiones.IdCesion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCesiones.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCesion", DbType.Decimal, pCesiones.ValorCesion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorEjecutado", DbType.Decimal, pCesiones.ValorEjecutado);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCesiones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarCesiones(Cesiones pCesiones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, pCesiones.IdCesion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCesiones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Cesiones ConsultarCesiones(int pIdCesion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, pIdCesion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Cesiones vCesiones = new Cesiones();
                        while (vDataReaderResults.Read())
                        {
                            vCesiones.IdCesion = vDataReaderResults["IdCesion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCesion"].ToString()) : vCesiones.IdCesion;
                            vCesiones.FechaCesion = vDataReaderResults["FechaCesion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCesion"].ToString()) : vCesiones.FechaCesion;
                            vCesiones.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vCesiones.Justificacion;
                            vCesiones.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vCesiones.Estado;
                            vCesiones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vCesiones.IDDetalleConsModContractual;
                            vCesiones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCesiones.UsuarioCrea;
                            vCesiones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCesiones.FechaCrea;
                            vCesiones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCesiones.UsuarioModifica;
                            vCesiones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCesiones.FechaModifica;
                            vCesiones.ValorCesion = vDataReaderResults["ValorCesion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCesion"].ToString()) : vCesiones.ValorCesion;
                            vCesiones.ValorEjecutado = vDataReaderResults["ValorEjecutado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEjecutado"].ToString()) : vCesiones.ValorEjecutado;
                        }
                        return vCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Cesiones ConsultarCesionesContrato(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_CesionesContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Cesiones vCesiones = new Cesiones();
                        while (vDataReaderResults.Read())
                        {
                            vCesiones.IdCesion = vDataReaderResults["IdCesion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCesion"].ToString()) : vCesiones.IdCesion;
                            vCesiones.FechaCesion = vDataReaderResults["FechaCesion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCesion"].ToString()) : vCesiones.FechaCesion;
                            vCesiones.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vCesiones.Justificacion;
                            vCesiones.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vCesiones.Estado;
                            vCesiones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vCesiones.IDDetalleConsModContractual;
                            vCesiones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCesiones.UsuarioCrea;
                            vCesiones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCesiones.FechaCrea;
                            vCesiones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCesiones.UsuarioModifica;
                            vCesiones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCesiones.FechaModifica;
                            vCesiones.ValorCesion = vDataReaderResults["ValorCesion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCesion"].ToString()) : vCesiones.ValorCesion;
                            vCesiones.ValorEjecutado = vDataReaderResults["ValorEjecutado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEjecutado"].ToString()) : vCesiones.ValorEjecutado;
                        }
                        return vCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarInfoCesionesSuscritas(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Cesiones_InfoCesionesSuscritas"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Cesiones> vListaCesiones = new List<Cesiones>();
                        while (vDataReaderResults.Read())
                        {
                            Cesiones vCesiones = new Cesiones();
                            vCesiones.IdCesion = vDataReaderResults["IdCesion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCesion"].ToString()) : vCesiones.IdCesion;
                            vCesiones.Estado = vDataReaderResults["IdConsModContractualesEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractualesEstado"].ToString()) : vCesiones.Estado;
                            vCesiones.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vCesiones.IDCosModContractual;
                            vCesiones.ValorCesion = vDataReaderResults["ValorCesion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCesion"].ToString()) : vCesiones.ValorCesion;
                            vCesiones.ValorEjecutado = vDataReaderResults["ValorEjecutado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEjecutado"].ToString()) : vCesiones.ValorEjecutado;
                            vListaCesiones.Add(vCesiones);
                        }
                        return vListaCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarInfoCesionesSuscritasReporte(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Cesiones_InfoCesionesSuscritasReporte"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Cesiones> vListaCesiones = new List<Cesiones>();
                        while (vDataReaderResults.Read())
                        {
                            Cesiones vCesiones = new Cesiones();
                           
                            vCesiones.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vCesiones.IdRP;
                            vCesiones.FechaSuscripcion = vDataReaderResults["FechaSubscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSubscripcion"].ToString()) : vCesiones.FechaSuscripcion;
                            vCesiones.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vCesiones.IdRP;
                            vCesiones.RepresentanteLegal = vDataReaderResults["NOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBRE"].ToString()) : vCesiones.RepresentanteLegal;
                            vCesiones.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vCesiones.NumeroIdentificacion;
                            vListaCesiones.Add(vCesiones);
                        }
                        return vListaCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarInfoCesiones(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Cesiones_InfoCesiones"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Cesiones> vListaCesiones = new List<Cesiones>();
                        while (vDataReaderResults.Read())
                        {
                            Cesiones vCesiones = new Cesiones();
                            vCesiones.IdCesion = vDataReaderResults["IdCesion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCesion"].ToString()) : vCesiones.IdCesion;
                            vCesiones.Estado = vDataReaderResults["IdConsModContractualesEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractualesEstado"].ToString()) : vCesiones.Estado;
                            vCesiones.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vCesiones.IDCosModContractual;
                            vCesiones.ValorCesion = vDataReaderResults["ValorCesion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCesion"].ToString()) : vCesiones.ValorCesion;
                            vCesiones.ValorEjecutado = vDataReaderResults["ValorEjecutado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEjecutado"].ToString()) : vCesiones.ValorEjecutado;
                            vListaCesiones.Add(vCesiones);
                        }
                        return vListaCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarCesioness(String pFechaCesion, String pJustificacion, int? pEstado, int? pIDDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesioness_Consultar"))
                {
                    if(pFechaCesion != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaCesion", DbType.String, pFechaCesion);
                    if(pJustificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pJustificacion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    if(pIDDetalleConsModContractual != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pIDDetalleConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Cesiones> vListaCesiones = new List<Cesiones>();
                        while (vDataReaderResults.Read())
                        {
                                Cesiones vCesiones = new Cesiones();
                            vCesiones.IdCesion = vDataReaderResults["IdCesion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCesion"].ToString()) : vCesiones.IdCesion;
                            vCesiones.FechaCesion = vDataReaderResults["FechaCesion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCesion"].ToString()) : vCesiones.FechaCesion;
                            vCesiones.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vCesiones.Justificacion;
                            vCesiones.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vCesiones.Estado;
                            vCesiones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vCesiones.IDDetalleConsModContractual;
                            vCesiones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCesiones.UsuarioCrea;
                            vCesiones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCesiones.FechaCrea;
                            vCesiones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCesiones.UsuarioModifica;
                            vCesiones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCesiones.FechaModifica;
                            vCesiones.ValorCesion = vDataReaderResults["ValorCesion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCesion"].ToString()) : vCesiones.ValorCesion;
                            vCesiones.ValorEjecutado = vDataReaderResults["ValorEjecutado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEjecutado"].ToString()) : vCesiones.ValorEjecutado;
                               
                            vListaCesiones.Add(vCesiones);
                        }
                        return vListaCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarCesionessContrato(int pContrato, int? pIntegrantesUnionTemporal, int? pIdCesion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_ConsultarContrato"))
                {                    

                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato);
                    if (pIntegrantesUnionTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IntegrantesUnionTemporal", DbType.Int32, pIntegrantesUnionTemporal);
                    if (pIdCesion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, pIdCesion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Cesiones> vListaCesiones = new List<Cesiones>();
                        while (vDataReaderResults.Read())
                        {
                            Cesiones vCesiones = new Cesiones();
                            vCesiones.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vCesiones.IdProveedoresContratos;
                            vCesiones.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vCesiones.IdTercero;
                            vCesiones.TipoPersonaNombre = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vCesiones.TipoPersonaNombre;
                            vCesiones.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vCesiones.TipoIdentificacion;
                            vCesiones.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vCesiones.NumeroIdentificacion;
                            vCesiones.Proveedor = vDataReaderResults["InformacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionContratista"].ToString()) : vCesiones.Proveedor;
                            vCesiones.TipoDocumentoRepresentanteLegal = vDataReaderResults["TipoDocumentoRepresentante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoRepresentante"].ToString()) : vCesiones.TipoDocumentoRepresentanteLegal;
                            vCesiones.NumeroIDRepresentanteLegal = vDataReaderResults["IdentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepLegal"].ToString()) : vCesiones.NumeroIDRepresentanteLegal;
                            vCesiones.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vCesiones.RepresentanteLegal;
                            vCesiones.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vCesiones.PorcentajeParticipacion;
                            vCesiones.NumeroIdentificacionIntegrante = vDataReaderResults["NumeroIdentificacionIntegrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionIntegrante"].ToString()) : vCesiones.NumeroIdentificacionIntegrante;
                            vCesiones.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vCesiones.IdEntidad;
                            vListaCesiones.Add(vCesiones);
                        }
                        return vListaCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarContratisasActuales(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_ConsultarContratistasActuales"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato);

                    if (pIntegrantesUnionTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IntegrantesUnionTemporal", DbType.Int32, pIntegrantesUnionTemporal);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Cesiones> vListaCesiones = new List<Cesiones>();
                        while (vDataReaderResults.Read())
                        {
                            Cesiones vCesiones = new Cesiones();
                            vCesiones.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vCesiones.IdProveedoresContratos;
                            vCesiones.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vCesiones.IdTercero;
                            vCesiones.TipoPersonaNombre = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vCesiones.TipoPersonaNombre;
                            vCesiones.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vCesiones.TipoIdentificacion;
                            vCesiones.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vCesiones.NumeroIdentificacion;
                            vCesiones.Proveedor = vDataReaderResults["InformacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionContratista"].ToString()) : vCesiones.Proveedor;
                            vCesiones.TipoDocumentoRepresentanteLegal = vDataReaderResults["TipoDocumentoRepresentante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoRepresentante"].ToString()) : vCesiones.TipoDocumentoRepresentanteLegal;
                            vCesiones.NumeroIDRepresentanteLegal = vDataReaderResults["IdentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepLegal"].ToString()) : vCesiones.NumeroIDRepresentanteLegal;
                            vCesiones.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vCesiones.RepresentanteLegal;
                            vCesiones.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vCesiones.PorcentajeParticipacion;
                            vCesiones.NumeroIdentificacionIntegrante = vDataReaderResults["NumeroIdentificacionIntegrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionIntegrante"].ToString()) : vCesiones.NumeroIdentificacionIntegrante;
                            vCesiones.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vCesiones.IdEntidad;
                            vListaCesiones.Add(vCesiones);
                        }
                        return vListaCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarContratisasCesion(int pContrato, int idcesion, int? pIntegrantesUnionTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_ConsultarContratistasCesion"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, idcesion);

                    if (pIntegrantesUnionTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IntegrantesUnionTemporal", DbType.Int32, pIntegrantesUnionTemporal);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Cesiones> vListaCesiones = new List<Cesiones>();
                        while (vDataReaderResults.Read())
                        {
                            Cesiones vCesiones = new Cesiones();
                            vCesiones.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vCesiones.IdProveedoresContratos;
                            vCesiones.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vCesiones.IdTercero;
                            vCesiones.TipoPersonaNombre = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vCesiones.TipoPersonaNombre;
                            vCesiones.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vCesiones.TipoIdentificacion;
                            vCesiones.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vCesiones.NumeroIdentificacion;
                            vCesiones.Proveedor = vDataReaderResults["InformacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionContratista"].ToString()) : vCesiones.Proveedor;
                            vCesiones.TipoDocumentoRepresentanteLegal = vDataReaderResults["TipoDocumentoRepresentante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoRepresentante"].ToString()) : vCesiones.TipoDocumentoRepresentanteLegal;
                            vCesiones.NumeroIDRepresentanteLegal = vDataReaderResults["IdentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepLegal"].ToString()) : vCesiones.NumeroIDRepresentanteLegal;
                            vCesiones.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vCesiones.RepresentanteLegal;
                            vCesiones.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vCesiones.PorcentajeParticipacion;
                            vCesiones.NumeroIdentificacionIntegrante = vDataReaderResults["NumeroIdentificacionIntegrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionIntegrante"].ToString()) : vCesiones.NumeroIdentificacionIntegrante;
                            vCesiones.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vCesiones.IdEntidad;
                            vListaCesiones.Add(vCesiones);
                        }
                        return vListaCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarExisteCesionario(int pContrato, int idcesion, int idProveedorePadre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_ExisteCedente"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, idcesion);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedorPadre", DbType.Int32, idProveedorePadre);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Cesiones> vListaCesiones = new List<Cesiones>();
                        while (vDataReaderResults.Read())
                        {
                            Cesiones vCesiones = new Cesiones();
                            vCesiones.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vCesiones.IdProveedoresContratos;
                            vCesiones.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vCesiones.IdTercero;
                            vCesiones.TipoPersonaNombre = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vCesiones.TipoPersonaNombre;
                            vCesiones.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vCesiones.TipoIdentificacion;
                            vCesiones.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vCesiones.NumeroIdentificacion;
                            vCesiones.Proveedor = vDataReaderResults["InformacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionContratista"].ToString()) : vCesiones.Proveedor;
                            vCesiones.TipoDocumentoRepresentanteLegal = vDataReaderResults["TipoDocumentoRepresentante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoRepresentante"].ToString()) : vCesiones.TipoDocumentoRepresentanteLegal;
                            vCesiones.NumeroIDRepresentanteLegal = vDataReaderResults["IdentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepLegal"].ToString()) : vCesiones.NumeroIDRepresentanteLegal;
                            vCesiones.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vCesiones.RepresentanteLegal;
                            vCesiones.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vCesiones.PorcentajeParticipacion;
                            vCesiones.NumeroIdentificacionIntegrante = vDataReaderResults["NumeroIdentificacionIntegrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionIntegrante"].ToString()) : vCesiones.NumeroIdentificacionIntegrante;
                            vCesiones.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vCesiones.IdEntidad;
                            vListaCesiones.Add(vCesiones);
                        }
                        return vListaCesiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
