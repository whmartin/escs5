﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad  Tipo Contrato
    /// </summary>
    public class DependenciaSolicitanteDAL : GeneralDAL
    {
        public DependenciaSolicitanteDAL()
        {
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad DependenciaSolicitante
        /// </summary>
        /// <param name="pIdDependenciaSolicitante"></param>
        /// <param name="pNombreDependenciaSolicitante"></param>
        /// <returns></returns>
        public List<DependenciaSolicitante> ConsultarDependenciaSolicitante(int? pIdDependenciaSolicitante
            , String pNombreDependenciaSolicitante,string pCodigoRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_KACTUS_KPRODII_Da_Emple_DependenciaSolicitante_Consultar"))
                {
                    if (pIdDependenciaSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "	@IdDependenciaSolicitante", DbType.Int32, pIdDependenciaSolicitante);
                    if (pNombreDependenciaSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DependenciaSolicitante", DbType.Int32, pNombreDependenciaSolicitante);
                    if (pCodigoRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoReonal", DbType.String, pCodigoRegional);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DependenciaSolicitante> vListaDependenciaSolicitante = new List<DependenciaSolicitante>();
                        while (vDataReaderResults.Read())
                        {
                            DependenciaSolicitante vDependenciaSolicitante = new DependenciaSolicitante();
                            vDependenciaSolicitante.IdDependenciaSolicitante = vDataReaderResults["IdDependenciaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaSolicitante"].ToString()) : vDependenciaSolicitante.IdDependenciaSolicitante;
                            vDependenciaSolicitante.NombreDependenciaSolicitante = vDataReaderResults["NombreDependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDependenciaSolicitante"].ToString()) : vDependenciaSolicitante.NombreDependenciaSolicitante;

                            vListaDependenciaSolicitante.Add(vDependenciaSolicitante);
                        }
                        return vListaDependenciaSolicitante;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
