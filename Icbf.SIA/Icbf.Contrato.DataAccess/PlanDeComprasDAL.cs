﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad PlanDeCompras
    /// </summary>
    public class PlanDeComprasDAL : GeneralDAL
    {
        public PlanDeComprasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int InsertarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeCompras_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPlanDeCompras", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivo", DbType.Int32, pPlanDeCompras.NumeroConsecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanDeCompras.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPlanDeCompras.IdPlanDeCompras = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPlanDeCompras").ToString());
                    GenerarLogAuditoria(pPlanDeCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int ModificarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeCompras_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanDeCompras", DbType.Int32, pPlanDeCompras.IdPlanDeCompras);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivo", DbType.Int32, pPlanDeCompras.NumeroConsecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlanDeCompras.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanDeCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int EliminarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeCompras_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanDeCompras", DbType.Int32, pPlanDeCompras.IdPlanDeCompras);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanDeCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pIdPlanDeCompras"></param>
        public PlanDeCompras ConsultarPlanDeCompras(int pIdPlanDeCompras,string pVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeCompras_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanDeCompras", DbType.Int32, pIdPlanDeCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Idvigencia", DbType.String, pVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        PlanDeCompras vPlanDeCompras = new PlanDeCompras();
                        while (vDataReaderResults.Read())
                        {
                            vPlanDeCompras.IdPlanDeCompras = vDataReaderResults["IdPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanDeCompras"].ToString()) : vPlanDeCompras.IdPlanDeCompras;
                            vPlanDeCompras.NumeroConsecutivo = vDataReaderResults["NumeroConsecutivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivo"].ToString()) : vPlanDeCompras.NumeroConsecutivo;
                            vPlanDeCompras.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanDeCompras.UsuarioCrea;
                            vPlanDeCompras.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanDeCompras.FechaCrea;
                            vPlanDeCompras.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanDeCompras.UsuarioModifica;
                            vPlanDeCompras.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanDeCompras.FechaModifica;
                            vPlanDeCompras.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vPlanDeCompras.IdContrato;
                            
                        }
                        return vPlanDeCompras;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pNumeroConsecutivo"></param>
        public List<PlanDeCompras> ConsultarPlanDeComprass(int? pNumeroConsecutivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeComprass_Consultar"))
                {
                    if (pNumeroConsecutivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivo", DbType.Int32, pNumeroConsecutivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<PlanDeCompras> vListaPlanDeCompras = new List<PlanDeCompras>();
                        while (vDataReaderResults.Read())
                        {
                            PlanDeCompras vPlanDeCompras = new PlanDeCompras();
                            vPlanDeCompras.IdPlanDeCompras = vDataReaderResults["IdPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanDeCompras"].ToString()) : vPlanDeCompras.IdPlanDeCompras;
                            vPlanDeCompras.NumeroConsecutivo = vDataReaderResults["NumeroConsecutivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivo"].ToString()) : vPlanDeCompras.NumeroConsecutivo;
                            vPlanDeCompras.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanDeCompras.UsuarioCrea;
                            vPlanDeCompras.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanDeCompras.FechaCrea;
                            vPlanDeCompras.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanDeCompras.UsuarioModifica;
                            vPlanDeCompras.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanDeCompras.FechaModifica;
                            vListaPlanDeCompras.Add(vPlanDeCompras);
                        }
                        return vListaPlanDeCompras;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
