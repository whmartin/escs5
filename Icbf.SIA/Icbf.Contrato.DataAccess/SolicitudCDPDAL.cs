﻿using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess
{
    public class SolicitudCDPDAL : GeneralDAL
    {
        public SolicitudCDPDAL(){}

        public int InsertarLoteSolicitudesCDP(SolicitudCDP item, string nombreArchivo)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_SolicitudCDP_InsertarLote"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, nombreArchivo);

                    var pdatosSolicitudesCDP = new SqlParameter("@SolicitudesCDP", SqlDbType.Structured)
                    {
                        Value = item.SolicitidesCDP
                    };

                    vDbCommand.Parameters.Add(pdatosSolicitudesCDP);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TrazabilidadSolicitudCDP> ObtenerTrazabilidad()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_SolicitudCDP_ConsultarCargue"))
                {

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TrazabilidadSolicitudCDP> vListaAdiciones = new List<TrazabilidadSolicitudCDP>();
                        while (vDataReaderResults.Read())
                        {
                            TrazabilidadSolicitudCDP vCargue = new TrazabilidadSolicitudCDP();
                            vCargue.IdCargue = vDataReaderResults["IdSolicitudCDPCargue"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudCDPCargue"].ToString()) : vCargue.IdCargue;
                            vCargue.Fecha = vDataReaderResults["FechaCargue"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCargue"].ToString()) : vCargue.Fecha;
                            vCargue.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vCargue.NombreArchivo;

                            vListaAdiciones.Add(vCargue);
                        }
                        return vListaAdiciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
