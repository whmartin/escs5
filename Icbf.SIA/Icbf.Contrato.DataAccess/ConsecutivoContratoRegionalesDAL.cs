﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad ConsecutivoContratoRegionales
    /// </summary>
    public class ConsecutivoContratoRegionalesDAL : GeneralDAL
    {
        public ConsecutivoContratoRegionalesDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int InsertarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDConsecutivoContratoRegional", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pConsecutivoContratoRegionales.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pConsecutivoContratoRegionales.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.Decimal, pConsecutivoContratoRegionales.Consecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pConsecutivoContratoRegionales.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pConsecutivoContratoRegionales.IDConsecutivoContratoRegional = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDConsecutivoContratoRegional").ToString());
                    GenerarLogAuditoria(pConsecutivoContratoRegionales, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int ModificarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDConsecutivoContratoRegional", DbType.Int32, pConsecutivoContratoRegionales.IDConsecutivoContratoRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pConsecutivoContratoRegionales.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pConsecutivoContratoRegionales.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.Decimal, pConsecutivoContratoRegionales.Consecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pConsecutivoContratoRegionales.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pConsecutivoContratoRegionales, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int EliminarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDConsecutivoContratoRegional", DbType.Int32, pConsecutivoContratoRegionales.IDConsecutivoContratoRegional);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pConsecutivoContratoRegionales, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public ConsecutivoContratoRegionales ConsultarConsecutivoContratoRegionales(int pIDConsecutivoContratoRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsecutivoContratoRegionales_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDConsecutivoContratoRegional", DbType.Int32, pIDConsecutivoContratoRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ConsecutivoContratoRegionales vConsecutivoContratoRegionales = new ConsecutivoContratoRegionales();
                        while (vDataReaderResults.Read())
                        {
                            vConsecutivoContratoRegionales.IDConsecutivoContratoRegional = vDataReaderResults["IDConsecutivoContratoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDConsecutivoContratoRegional"].ToString()) : vConsecutivoContratoRegionales.IDConsecutivoContratoRegional;
                            vConsecutivoContratoRegionales.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vConsecutivoContratoRegionales.IdRegional;
                            vConsecutivoContratoRegionales.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vConsecutivoContratoRegionales.IdVigencia;
                            vConsecutivoContratoRegionales.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Consecutivo"].ToString()) : vConsecutivoContratoRegionales.Consecutivo;
                            vConsecutivoContratoRegionales.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsecutivoContratoRegionales.UsuarioCrea;
                            vConsecutivoContratoRegionales.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsecutivoContratoRegionales.FechaCrea;
                            vConsecutivoContratoRegionales.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsecutivoContratoRegionales.UsuarioModifica;
                            vConsecutivoContratoRegionales.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsecutivoContratoRegionales.FechaModifica;
                        }
                        return vConsecutivoContratoRegionales;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIdRegional"></param>
        /// <param name="pIdVigencia"></param>
        /// <param name="pConsecutivo"></param>
        public List<ConsecutivoContratoRegionales> ConsultarConsecutivoContratoRegionaless(int? pIdRegional, int? pIdVigencia, Decimal? pConsecutivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsecutivoContratoRegionaless_Consultar"))
                {
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pConsecutivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.Decimal, pConsecutivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConsecutivoContratoRegionales> vListaConsecutivoContratoRegionales = new List<ConsecutivoContratoRegionales>();
                        while (vDataReaderResults.Read())
                        {
                            ConsecutivoContratoRegionales vConsecutivoContratoRegionales = new ConsecutivoContratoRegionales();
                            vConsecutivoContratoRegionales.IDConsecutivoContratoRegional = vDataReaderResults["IDConsecutivoContratoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDConsecutivoContratoRegional"].ToString()) : vConsecutivoContratoRegionales.IDConsecutivoContratoRegional;
                            vConsecutivoContratoRegionales.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vConsecutivoContratoRegionales.IdRegional;
                            vConsecutivoContratoRegionales.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vConsecutivoContratoRegionales.IdVigencia;
                            vConsecutivoContratoRegionales.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vConsecutivoContratoRegionales.Regional;
                            vConsecutivoContratoRegionales.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vConsecutivoContratoRegionales.Vigencia;
                            vConsecutivoContratoRegionales.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Consecutivo"].ToString()) : vConsecutivoContratoRegionales.Consecutivo;
                            vConsecutivoContratoRegionales.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsecutivoContratoRegionales.UsuarioCrea;
                            vConsecutivoContratoRegionales.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsecutivoContratoRegionales.FechaCrea;
                            vConsecutivoContratoRegionales.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsecutivoContratoRegionales.UsuarioModifica;
                            vConsecutivoContratoRegionales.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsecutivoContratoRegionales.FechaModifica;
                            vListaConsecutivoContratoRegionales.Add(vConsecutivoContratoRegionales);
                        }
                        return vListaConsecutivoContratoRegionales;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public bool EsMayorConsecutivoContratoRegionales(Decimal pConsecutivo, int pIdVigencia, int pIdRegional)
        {
            try
            {
                
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsecutivoContratoRegionales_ConsultarEsMayor"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.Decimal, pConsecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                       
                        while (vDataReaderResults.Read())
                        {
                            return false;
                        }
                        return true;
                    }
                }
                return false;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public bool ExisteConsecutivoContratoRegionales(Decimal pConsecutivo, int pIdVigencia, int pIdRegional)
        {
            try
            {
                
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsecutivoContratoRegionales_ConsultarValidar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.Decimal, pConsecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                        return false;
                    }
                }
                return false;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

