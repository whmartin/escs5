﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;



namespace Icbf.Contrato.DataAccess
{
    public class EmpleadoDAL : GeneralDAL
    {
        public EmpleadoDAL()
        {
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Empleado
        /// </summary>
        /// <param name="pIdTipoIdentificacion">Cadena de texto con el identificador del tipo de identificación</param>
        /// <param name="pNumeroIdentificacion">Cadena de texto con el numero de identificación</param>
        /// <param name="pIdTipoVinculacionContractual">Cadena de texto con la descripcion del tipo de vinculación que es el mismo id</param>
        /// <param name="pIdRegional">Cadena de texto con el codigo de la regional</param>
        /// <param name="pPrimerNombre">Cadena de texto con el primer nombre del empleado</param>
        /// <param name="pSegundoNombre">Cadena de texto con el segundo nombre del empleado</param>
        /// <param name="pPrimerApellido">Cadena de texto con el primer apellido del empleado</param>
        /// <param name="pSegundoApellido">Cadena de texto con el segundo apellido del empleado</param>
        /// <returns>Lista con los empleados que coinciden con los filtros</returns>
        public List<Empleado> ConsultarEmpleados(String pIdTipoIdentificacion, String pNumeroIdentificacion, String pIdTipoVinculacionContractual, 
            String pIdRegional, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_EmpleadosICBFlupa_Consultar"))
                {
                    if (pIdTipoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.String, pIdTipoIdentificacion);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    if (pIdTipoVinculacionContractual != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoVinculacionContractual", DbType.String, pIdTipoVinculacionContractual);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, pIdRegional);
                    if (pPrimerNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pPrimerNombre);
                    if (pSegundoNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pSegundoNombre);
                    if (pPrimerApellido != null)
                        vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pPrimerApellido);
                    if (pSegundoApellido != null)
                        vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pSegundoApellido);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Empleado> vListaEmpleados = new List<Empleado>();
                        while (vDataReaderResults.Read())
                        {
                            Empleado vEmpleado = new Empleado();
                            vEmpleado.IdTipoIdentificacion = vDataReaderResults["IdTipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoIdentificacion"].ToString()) : vEmpleado.IdTipoIdentificacion;
                            vEmpleado.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vEmpleado.TipoIdentificacion;
                            vEmpleado.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vEmpleado.NumeroIdentificacion;
                            vEmpleado.IdTipoVinculacionContractual = vDataReaderResults["IdTipoVinculacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoVinculacionContractual"].ToString()) : vEmpleado.IdTipoVinculacionContractual;
                            vEmpleado.TipoVinculacionContractual = vDataReaderResults["TipoVinculacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacionContractual"].ToString()) : vEmpleado.TipoVinculacionContractual;
                            vEmpleado.NombreEmpleado = vDataReaderResults["NombreEmpleado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEmpleado"].ToString()) : vEmpleado.NombreEmpleado;
                            vEmpleado.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdRegional"].ToString()) : vEmpleado.IdRegional;
                            vEmpleado.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vEmpleado.Regional;
                            vEmpleado.IdDependencia = vDataReaderResults["IdDependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDependencia"].ToString()) : vEmpleado.IdDependencia;
                            vEmpleado.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vEmpleado.IdCargo;
                            vEmpleado.IdCargo = vDataReaderResults["IdCargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdCargo"].ToString()) : vEmpleado.Cargo;
                            vEmpleado.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vEmpleado.Cargo;
                            vEmpleado.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vEmpleado.PrimerNombre;
                            vEmpleado.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vEmpleado.SegundoNombre;
                            vEmpleado.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vEmpleado.PrimerApellido;
                            vEmpleado.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vEmpleado.SegundoApellido;
                            vEmpleado.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vEmpleado.Direccion;
                            vEmpleado.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vEmpleado.Telefono;
                            vListaEmpleados.Add(vEmpleado);
                        }
                        return vListaEmpleados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros de la tabla [KACTUS].[KPRODII].[dbo].[nm_tnomi]
        /// que contiene los tipos de vinculación contractual
        /// </summary>
        /// <param name="pIdTipoVinCont">Cadena de texto con el identificador numérico del tipo de vinculación</param>
        /// <param name="pTipoVincCont">Cadena de texto con la descripción del tipo de vinculación</param>
        /// <returns>Lista con los tipos de vinculación contractual encapsulados en entidades tipo empleado</returns>
        public List<Empleado> ConsultarTiposVinculacionContractual(String pIdTipoVinCont, String pTipoVincCont)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TiposVinculacionContractual_Consultar"))
                {
                    if (pIdTipoVinCont != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoVinCont", DbType.String, pIdTipoVinCont);
                    if (pTipoVincCont != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoVincCont", DbType.String, pTipoVincCont);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Empleado> vListaEmpleados = new List<Empleado>();
                        while (vDataReaderResults.Read())
                        {
                            Empleado vEmpleado = new Empleado();
                            vEmpleado.IdTipoVinculacionContractual = vDataReaderResults["IdTipoVinculacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoVinculacionContractual"].ToString()) : vEmpleado.IdTipoVinculacionContractual;
                            vEmpleado.TipoVinculacionContractual = vDataReaderResults["TipoVinculacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacionContractual"].ToString()) : vEmpleado.TipoVinculacionContractual;
                            vListaEmpleados.Add(vEmpleado);
                        }
                        return vListaEmpleados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Consulta la información del empleado en Kactus a partir de los filtros dados
        /// </summary>
        /// <param name="pNumeroIdentificacion">Cadena de texto con el numero de identificacion</param>
        /// <param name="pRegional">Cadena de texto con el identificador de la regional</param>
        /// <param name="pDependencia">Cadena de texto con el identificador de la dependencia</param>
        /// <param name="pCargo">Cadena de texto con el identificador del cargo</param>
        /// <returns>Entidad Empleado con la información obtenida de la base de datos</returns>
        public Empleado ConsultarEmpleado(String pNumeroIdentificacion, String pRegional, String pDependencia, String pCargo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_KACTUS_KPRODII_Da_Emple_Consultar_Empleado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, pRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependencia", DbType.String, pDependencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdCargo", DbType.String, pCargo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Empleado vEmpleado = new Empleado();
                        while (vDataReaderResults.Read())
                        {
                            vEmpleado.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vEmpleado.PrimerNombre;
                            vEmpleado.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vEmpleado.SegundoNombre;
                            vEmpleado.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vEmpleado.PrimerApellido;
                            vEmpleado.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vEmpleado.SegundoApellido;
                            vEmpleado.NombreEmpleado = vDataReaderResults["NombreEmpleado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEmpleado"].ToString()) : vEmpleado.NombreEmpleado;
                            vEmpleado.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vEmpleado.Regional;
                            vEmpleado.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vEmpleado.Dependencia;
                            vEmpleado.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vEmpleado.Cargo;
                            vEmpleado.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vEmpleado.TipoIdentificacion;
                            vEmpleado.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vEmpleado.NumeroIdentificacion;
                            vEmpleado.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdRegional"].ToString()) : vEmpleado.IdRegional;
                        }
                        return vEmpleado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta las dependencias del ICBF.
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        public Dictionary<string, string> ConsultarDependencias(string codigo, int cantidad)
        {
            Dictionary<string, string> result = new Dictionary<string,string>();

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Empleados_ConsultarDependencias"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@cantidad", DbType.Int32, cantidad);
                    vDataBase.AddInParameter(vDbCommand, "@codigo", DbType.String, codigo);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Empleado vEmpleado = new Empleado();
                        string key = string.Empty, value = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            key = vDataReaderResults["IdDependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDependencia"].ToString()) : key;
                            value = vDataReaderResults["NombreDependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDependencia"].ToString()) : value;
                            result.Add(key, value);
                        }

                        return result;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
    }
}

