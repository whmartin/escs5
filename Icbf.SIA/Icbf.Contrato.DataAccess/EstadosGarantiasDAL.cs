using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad EstadosGarantias
    /// </summary>
    public class EstadosGarantiasDAL : GeneralDAL
    {
        public EstadosGarantiasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int InsertarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_EstadosGarantias_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDEstadosGarantias", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoGarantia", DbType.String, pEstadosGarantias.CodigoEstadoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionEstadoGarantia", DbType.String, pEstadosGarantias.DescripcionEstadoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEstadosGarantias.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEstadosGarantias.IDEstadosGarantias = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDEstadosGarantias").ToString());
                    GenerarLogAuditoria(pEstadosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int ModificarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_EstadosGarantias_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDEstadosGarantias", DbType.Int32, pEstadosGarantias.IDEstadosGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoGarantia", DbType.String, pEstadosGarantias.CodigoEstadoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionEstadoGarantia", DbType.String, pEstadosGarantias.DescripcionEstadoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEstadosGarantias.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int EliminarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_EstadosGarantias_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDEstadosGarantias", DbType.Int32, pEstadosGarantias.IDEstadosGarantias);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGarantia"></param>
        /// <param name="estado"></param>
        public void EnviarAprobacionGarantia(int idGarantia, string estado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_EstadosGarantias_Aprobacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, idGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, estado);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pIDEstadosGarantias"></param>
        public EstadosGarantias ConsultarEstadosGarantias(int pIDEstadosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_EstadosGarantias_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDEstadosGarantias", DbType.Int32, pIDEstadosGarantias);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadosGarantias vEstadosGarantias = new EstadosGarantias();
                        while (vDataReaderResults.Read())
                        {
                            vEstadosGarantias.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vEstadosGarantias.IDEstadosGarantias;
                            vEstadosGarantias.CodigoEstadoGarantia = vDataReaderResults["CodigoEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoGarantia"].ToString()) : vEstadosGarantias.CodigoEstadoGarantia;
                            vEstadosGarantias.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstadoGarantia"].ToString()) : vEstadosGarantias.DescripcionEstadoGarantia;
                            vEstadosGarantias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadosGarantias.UsuarioCrea;
                            vEstadosGarantias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadosGarantias.FechaCrea;
                            vEstadosGarantias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadosGarantias.UsuarioModifica;
                            vEstadosGarantias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadosGarantias.FechaModifica;
                        }
                        return vEstadosGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pCodigoEstadoGarantia"></param>
        /// <param name="pDescripcionEstadoGarantia"></param>
        public List<EstadosGarantias> ConsultarEstadosGarantiass(String pCodigoEstadoGarantia, String pDescripcionEstadoGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_EstadosGarantiass_Consultar"))
                {
                    if(pCodigoEstadoGarantia != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoGarantia", DbType.String, pCodigoEstadoGarantia);
                    if(pDescripcionEstadoGarantia != null)
                         vDataBase.AddInParameter(vDbCommand, "@DescripcionEstadoGarantia", DbType.String, pDescripcionEstadoGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadosGarantias> vListaEstadosGarantias = new List<EstadosGarantias>();
                        while (vDataReaderResults.Read())
                        {
                                EstadosGarantias vEstadosGarantias = new EstadosGarantias();
                            vEstadosGarantias.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vEstadosGarantias.IDEstadosGarantias;
                            vEstadosGarantias.CodigoEstadoGarantia = vDataReaderResults["CodigoEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoGarantia"].ToString()) : vEstadosGarantias.CodigoEstadoGarantia;
                            vEstadosGarantias.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstadoGarantia"].ToString()) : vEstadosGarantias.DescripcionEstadoGarantia;
                            vEstadosGarantias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadosGarantias.UsuarioCrea;
                            vEstadosGarantias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadosGarantias.FechaCrea;
                            vEstadosGarantias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadosGarantias.UsuarioModifica;
                            vEstadosGarantias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadosGarantias.FechaModifica;
                                vListaEstadosGarantias.Add(vEstadosGarantias);
                        }
                        return vListaEstadosGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGarantia"></param>
        /// <returns></returns>
        public List<string> ValidarAprobacionGarantia(int idGarantia)
        {
             try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AprobacionGarantia_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, idGarantia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<string> vListaEstadosGarantias = new List<string>();
                        while (vDataReaderResults.Read())
                        {
                            string result = string.Empty;
                            result = vDataReaderResults["Mensaje"] != DBNull.Value ? vDataReaderResults["Mensaje"].ToString() : string.Empty;
                            vListaEstadosGarantias.Add(result);
                        }
                        return vListaEstadosGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
