using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad tipo obligaci�n
    /// </summary>
    public class TipoObligacionDAL : GeneralDAL
    {
        public TipoObligacionDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int InsertarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoObligacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoObligacion", DbType.String, pTipoObligacion.NombreTipoObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoObligacion.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoObligacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoObligacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoObligacion.IdTipoObligacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoObligacion").ToString());
                    GenerarLogAuditoria(pTipoObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627 || ex.Number == 2601)
                    throw new GenericException("Registro duplicado, verifique por favor");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int ModificarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoObligacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pTipoObligacion.IdTipoObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoObligacion", DbType.String, pTipoObligacion.NombreTipoObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoObligacion.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoObligacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoObligacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                //if (ex.Number == 2627 || ex.Number == 2601)
                //    throw new GenericException("El registro ya se encuentra creado");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int EliminarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoObligacion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pTipoObligacion.IdTipoObligacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad TipoObligacion
        /// </summary>
        /// <param name="pIdTipoObligacion"></param>
        /// <returns></returns>
        public TipoObligacion ConsultarTipoObligacion(int pIdTipoObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoObligacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pIdTipoObligacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoObligacion vTipoObligacion = new TipoObligacion();
                        while (vDataReaderResults.Read())
                        {
                            vTipoObligacion.IdTipoObligacion = vDataReaderResults["IdTipoObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObligacion"].ToString()) : vTipoObligacion.IdTipoObligacion;
                            vTipoObligacion.NombreTipoObligacion = vDataReaderResults["NombreTipoObligacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoObligacion"].ToString()) : vTipoObligacion.NombreTipoObligacion;
                            vTipoObligacion.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoObligacion.Descripcion;
                            vTipoObligacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoObligacion.Estado;
                            vTipoObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoObligacion.UsuarioCrea;
                            vTipoObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoObligacion.FechaCrea;
                            vTipoObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoObligacion.UsuarioModifica;
                            vTipoObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoObligacion.FechaModifica;
                        }
                        return vTipoObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoObligacion
        /// </summary>
        /// <param name="pNombreTipoObligacion"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoObligacion> ConsultarTipoObligacions(String pNombreTipoObligacion,String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoObligacions_Consultar"))
                {
                    if(pNombreTipoObligacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTipoObligacion", DbType.String, pNombreTipoObligacion);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoObligacion> vListaTipoObligacion = new List<TipoObligacion>();
                        while (vDataReaderResults.Read())
                        {
                                TipoObligacion vTipoObligacion = new TipoObligacion();
                            vTipoObligacion.IdTipoObligacion = vDataReaderResults["IdTipoObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObligacion"].ToString()) : vTipoObligacion.IdTipoObligacion;
                            vTipoObligacion.NombreTipoObligacion = vDataReaderResults["NombreTipoObligacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoObligacion"].ToString()) : vTipoObligacion.NombreTipoObligacion;
                            vTipoObligacion.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoObligacion.Descripcion;
                            vTipoObligacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoObligacion.Estado;
                            if (vTipoObligacion.Estado)
                            {
                                vTipoObligacion.EstadoString = "Activo";
                            } else
                            {
                                vTipoObligacion.EstadoString = "Inactivo";
                            }
                            vTipoObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoObligacion.UsuarioCrea;
                            vTipoObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoObligacion.FechaCrea;
                            vTipoObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoObligacion.UsuarioModifica;
                            vTipoObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoObligacion.FechaModifica;
                                vListaTipoObligacion.Add(vTipoObligacion);
                        }
                        return vListaTipoObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
