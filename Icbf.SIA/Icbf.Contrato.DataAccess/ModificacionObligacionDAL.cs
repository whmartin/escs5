using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class ModificacionObligacionDAL : GeneralDAL
    {
        public ModificacionObligacionDAL()
        {
        }
        public int InsertarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIAPrueba_CONTRATO_ModificacionObligacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdModObligacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pModificacionObligacion.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@ClausulaContrato", DbType.String, pModificacionObligacion.ClausulaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionModificacion", DbType.String, pModificacionObligacion.DescripcionModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pModificacionObligacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pModificacionObligacion.IdModObligacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdModObligacion").ToString());
                    GenerarLogAuditoria(pModificacionObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIAPrueba_CONTRATO_ModificacionObligacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModObligacion", DbType.Int32, pModificacionObligacion.IdModObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pModificacionObligacion.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@ClausulaContrato", DbType.String, pModificacionObligacion.ClausulaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionModificacion", DbType.String, pModificacionObligacion.DescripcionModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pModificacionObligacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModificacionObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIAPrueba_CONTRATO_ModificacionObligacion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModObligacion", DbType.Int32, pModificacionObligacion.IdModObligacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModificacionObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public ModificacionObligacion ConsultarModificacionObligacion(int pIdModObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIAPrueba_CONTRATO_ModificacionObligacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModObligacion", DbType.Int32, pIdModObligacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ModificacionObligacion vModificacionObligacion = new ModificacionObligacion();
                        while (vDataReaderResults.Read())
                        {
                            vModificacionObligacion.IdModObligacion = vDataReaderResults["IdModObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModObligacion"].ToString()) : vModificacionObligacion.IdModObligacion;
                            vModificacionObligacion.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vModificacionObligacion.IDDetalleConsModContractual;
                            vModificacionObligacion.ClausulaContrato = vDataReaderResults["ClausulaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClausulaContrato"].ToString()) : vModificacionObligacion.ClausulaContrato;
                            vModificacionObligacion.DescripcionModificacion = vDataReaderResults["DescripcionModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionModificacion"].ToString()) : vModificacionObligacion.DescripcionModificacion;
                            vModificacionObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModificacionObligacion.UsuarioCrea;
                            vModificacionObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModificacionObligacion.FechaCrea;
                            vModificacionObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModificacionObligacion.UsuarioModifica;
                            vModificacionObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModificacionObligacion.FechaModifica;
                        }
                        return vModificacionObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<ModificacionObligacion> ConsultarModificacionObligacionXIDDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIAPrueba_CONTRATO_ModificacionObligacion_Consultar_X_IDDetalleConsModContractual"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int64, pIDDetalleConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        
                        List<ModificacionObligacion> vListaModificacionObligacion = new List<ModificacionObligacion>();
                        while (vDataReaderResults.Read())
                        {
                            ModificacionObligacion vModificacionObligacion = new ModificacionObligacion();
                            vModificacionObligacion.IdModObligacion = vDataReaderResults["IdModObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModObligacion"].ToString()) : vModificacionObligacion.IdModObligacion;
                            vModificacionObligacion.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vModificacionObligacion.IDDetalleConsModContractual;
                            vModificacionObligacion.ClausulaContrato = vDataReaderResults["ClausulaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClausulaContrato"].ToString()) : vModificacionObligacion.ClausulaContrato;
                            vModificacionObligacion.DescripcionModificacion = vDataReaderResults["DescripcionModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionModificacion"].ToString()) : vModificacionObligacion.DescripcionModificacion;
                            vModificacionObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModificacionObligacion.UsuarioCrea;
                            vModificacionObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModificacionObligacion.FechaCrea;
                            vModificacionObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModificacionObligacion.UsuarioModifica;
                            vModificacionObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModificacionObligacion.FechaModifica;
                            vListaModificacionObligacion.Add(vModificacionObligacion);

                        }
                        return vListaModificacionObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<ModificacionObligacion> ConsultarModificacionObligacions()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIAPrueba_CONTRATO_ModificacionObligacions_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ModificacionObligacion> vListaModificacionObligacion = new List<ModificacionObligacion>();
                        while (vDataReaderResults.Read())
                        {
                                ModificacionObligacion vModificacionObligacion = new ModificacionObligacion();
                            vModificacionObligacion.IdModObligacion = vDataReaderResults["IdModObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModObligacion"].ToString()) : vModificacionObligacion.IdModObligacion;
                            vModificacionObligacion.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vModificacionObligacion.IDDetalleConsModContractual;
                            vModificacionObligacion.ClausulaContrato = vDataReaderResults["ClausulaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClausulaContrato"].ToString()) : vModificacionObligacion.ClausulaContrato;
                            vModificacionObligacion.DescripcionModificacion = vDataReaderResults["DescripcionModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionModificacion"].ToString()) : vModificacionObligacion.DescripcionModificacion;
                            vModificacionObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModificacionObligacion.UsuarioCrea;
                            vModificacionObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModificacionObligacion.FechaCrea;
                            vModificacionObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModificacionObligacion.UsuarioModifica;
                            vModificacionObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModificacionObligacion.FechaModifica;
                                vListaModificacionObligacion.Add(vModificacionObligacion);
                        }
                        return vListaModificacionObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
