using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class ConsModContractualDAL : GeneralDAL
    {
        public ConsModContractualDAL()
        {
        }
        public int InsertarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_CONTRATO_ConsModContractual_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDoc", DbType.String, pConsModContractual.NumeroDoc);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pConsModContractual.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, pConsModContractual.IDContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoSuscrito", DbType.String, pConsModContractual.ConsecutivoSuscrito);
                    vDataBase.AddInParameter(vDbCommand, "@Suscrito", DbType.Int32, pConsModContractual.Suscrito);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pConsModContractual.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pConsModContractual.IDCosModContractual = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDCosModContractual").ToString());
                    GenerarLogAuditoria(pConsModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_CONTRATO_ConsModContractual_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, pConsModContractual.IDCosModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDoc", DbType.String, pConsModContractual.NumeroDoc);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pConsModContractual.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, pConsModContractual.IDContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoSuscrito", DbType.String, pConsModContractual.ConsecutivoSuscrito);
                    vDataBase.AddInParameter(vDbCommand, "@Suscrito", DbType.Int32, pConsModContractual.Suscrito);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pConsModContractual.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pConsModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_CONTRATO_ConsModContractual_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, pConsModContractual.IDCosModContractual);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pConsModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ConsModContractual ConsultarConsModContractual(int pIDCosModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_CONTRATO_ConsModContractual_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, pIDCosModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ConsModContractual vConsModContractual = new ConsModContractual();
                        while (vDataReaderResults.Read())
                        {
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vConsModContractual.IDContrato;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.IdConsModContractualesEstado = vDataReaderResults["IdConsModContractualesEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractualesEstado"].ToString()) : vConsModContractual.IdConsModContractualesEstado;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vConsModContractual.FechaSolicitud;
                            vConsModContractual.IDTipoModificacionContractual = vDataReaderResults["IDTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoModificacionContractual"].ToString()) : vConsModContractual.IDTipoModificacionContractual;
                        }
                        return vConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public ConsModContractual ConsModContractualPorDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_CONTRATO_ConsModContractual_DetalleConsModContractual_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pIDDetalleConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ConsModContractual vConsModContractual = new ConsModContractual();
                        while (vDataReaderResults.Read())
                        {
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vConsModContractual.IDContrato;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vConsModContractual.FechaSolicitud;
                        }
                        return vConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ConsModContractual> ConsultarConsModContractuals(String pNumeroDoc, int? pIDContrato, String pConsecutivoSuscrito, int? pSuscrito, string pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsModContractuals_Consultar"))
                {
                    if(pNumeroDoc != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroDoc", DbType.String, pNumeroDoc);
                    if(pIDContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, pIDContrato);
                    if(pConsecutivoSuscrito != null)
                         vDataBase.AddInParameter(vDbCommand, "@ConsecutivoSuscrito", DbType.String, pConsecutivoSuscrito);
                    if(pSuscrito != null)
                         vDataBase.AddInParameter(vDbCommand, "@Suscrito", DbType.Int32, pSuscrito);
                    if (pUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, pUsuario);
                
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConsModContractual> vListaConsModContractual = new List<ConsModContractual>();
                        while (vDataReaderResults.Read())
                        {
                                ConsModContractual vConsModContractual = new ConsModContractual();
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vConsModContractual.IDContrato;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;

                            //vConsModContractual.RazonDevolucion = vDataReaderResults["RazonDevolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonDevolucion"].ToString()) : vConsModContractual.RazonDevolucion;
                            //vConsModContractual.FechaDevolucion = vDataReaderResults["FechaDevolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDevolucion"].ToString()) : vConsModContractual.FechaDevolucion;
                            vConsModContractual.IdConsModContractualesEstado = vDataReaderResults["IdConsModContractualesEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractualesEstado"].ToString()) : vConsModContractual.IdConsModContractualesEstado;
                            vConsModContractual.IDTipoModificacionContractual = vDataReaderResults["IDTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoModificacionContractual"].ToString()) : vConsModContractual.IDTipoModificacionContractual;
                            vConsModContractual.ConsModContractualesEstado_Descripcion = vDataReaderResults["ConsModContractualesEstado_Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsModContractualesEstado_Descripcion"].ToString()) : vConsModContractual.ConsModContractualesEstado_Descripcion;
                            vConsModContractual.TipoModificacionContractual_Codigo = vDataReaderResults["TipoModificacionContractual_Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacionContractual_Codigo"].ToString()) : vConsModContractual.TipoModificacionContractual_Codigo;
                            vConsModContractual.TipoModificacionContractual_Descripcion = vDataReaderResults["TipoModificacionContractual_Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacionContractual_Descripcion"].ToString()) : vConsModContractual.TipoModificacionContractual_Descripcion;
                            vConsModContractual.UsuarioAbogado = vDataReaderResults["UsuarioAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAbogado"].ToString()) : vConsModContractual.UsuarioAbogado;

                            vListaConsModContractual.Add(vConsModContractual);
                        }
                        return vListaConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitud(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ConsModContractual_ConsultarPorSolicitudes"))
                {
                    if (pnumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pnumeroContrato);
                    if (pvigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pvigenciaFiscal);
                    if (pregional != null)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int32, pregional);
                    if (pidCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pidCategoriaContrato);
                    if (pidTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pidTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractual> vConsModContractualist = new List<SolModContractual>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractual vConsModContractual = new SolModContractual();
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.Estado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vConsModContractual.Estado;
                            vConsModContractual.TipoModificacion = vDataReaderResults["DescripcionTipoMod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoMod"].ToString()) : vConsModContractual.TipoModificacion;
                            vConsModContractual.UsuarioAsignado = vDataReaderResults["UsuarioAsignado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAsignado"].ToString()) : vConsModContractual.UsuarioAsignado;

                            vConsModContractualist.Add(vConsModContractual);
                        }
                        return vConsModContractualist;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudAdiciones(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ConsModContractual_ConsultarPorSolicitudesAdiciones"))
                {
                    if (pnumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pnumeroContrato);
                    if (pvigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pvigenciaFiscal);
                    if (pregional != null)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int32, pregional);
                    if (pidCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pidCategoriaContrato);
                    if (pidTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pidTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractual> vConsModContractualist = new List<SolModContractual>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractual vConsModContractual = new SolModContractual();
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.Estado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vConsModContractual.Estado;
                            vConsModContractual.TipoModificacion = vDataReaderResults["DescripcionTipoMod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoMod"].ToString()) : vConsModContractual.TipoModificacion;
                            vConsModContractual.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vConsModContractual.IdAdicion;
                    
                            vConsModContractualist.Add(vConsModContractual);
                        }

                        return vConsModContractualist;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudCesion(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ConsModContractual_ConsultarPorSolicitudesCesion"))
                {
                    if (pnumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pnumeroContrato);
                    if (pvigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pvigenciaFiscal);
                    if (pregional != null)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int32, pregional);
                    if (pidCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pidCategoriaContrato);
                    if (pidTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pidTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractual> vConsModContractualist = new List<SolModContractual>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractual vConsModContractual = new SolModContractual();
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.Estado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vConsModContractual.Estado;
                            vConsModContractual.TipoModificacion = vDataReaderResults["DescripcionTipoMod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoMod"].ToString()) : vConsModContractual.TipoModificacion;
                            vConsModContractual.IdCesion = vDataReaderResults["IdCesion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCesion"].ToString()) : vConsModContractual.IdCesion;

                            vConsModContractualist.Add(vConsModContractual);
                        }

                        return vConsModContractualist;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudReparto(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ConsModContractual_ConsultarPorSolicitudesReparto"))
                {
                    if (pnumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pnumeroContrato);
                    if (pvigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pvigenciaFiscal);
                    if (pregional != null)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int32, pregional);
                    if (pidCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pidCategoriaContrato);
                    if (pidTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pidTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractual> vConsModContractualist = new List<SolModContractual>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractual vConsModContractual = new SolModContractual();
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.Estado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vConsModContractual.Estado;
                            vConsModContractual.TipoModificacion = vDataReaderResults["DescripcionTipoMod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoMod"].ToString()) : vConsModContractual.TipoModificacion;
                            vConsModContractual.UsuarioAsignado = vDataReaderResults["UsuarioAsignado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAsignado"].ToString()) : vConsModContractual.UsuarioAsignado;

                            vConsModContractualist.Add(vConsModContractual);
                        }
                        return vConsModContractualist;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualImpMultas(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ConsModContractual_ConsultarPorSolicitudesImpMultas"))
                {
                    if (pnumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pnumeroContrato);
                    if (pvigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pvigenciaFiscal);
                    if (pregional != null)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int32, pregional);
                    if (pidCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pidCategoriaContrato);
                    if (pidTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pidTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractual> vConsModContractualist = new List<SolModContractual>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractual vConsModContractual = new SolModContractual();
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.Estado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vConsModContractual.Estado;
                            vConsModContractual.TipoModificacion = vDataReaderResults["DescripcionTipoMod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoMod"].ToString()) : vConsModContractual.TipoModificacion;
                            vConsModContractual.UsuarioAsignado = vDataReaderResults["UsuarioAsignado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAsignado"].ToString()) : vConsModContractual.UsuarioAsignado;
                            vConsModContractual.IdProcesoImpMultas = vDataReaderResults["IdImposicionMulta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdImposicionMulta"].ToString()) : vConsModContractual.IdProcesoImpMultas;

                            
                            vConsModContractualist.Add(vConsModContractual);
                        }
                        return vConsModContractualist;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
