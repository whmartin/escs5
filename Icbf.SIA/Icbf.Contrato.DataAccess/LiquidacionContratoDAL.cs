﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class LiquidacionContratoDAL : GeneralDAL
    {
        public int InsertarLiquidacionContrato(LiquidacionContrato pLiquidacionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_LiquidacionContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdLiquidacionContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetConsModContractual", DbType.Int32, pLiquidacionContrato.IdDetConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRadicacion", DbType.DateTime, pLiquidacionContrato.FechaRaquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSupervisor", DbType.String, pLiquidacionContrato.NombreSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@CargoSupervisor", DbType.String, pLiquidacionContrato.CargoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@DependenciaSupervisor", DbType.String, pLiquidacionContrato.DependenciaSueprvisor);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pLiquidacionContrato.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@TipoLiquidacion", DbType.String, pLiquidacionContrato.TipodeLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorEjecutado", DbType.Decimal, pLiquidacionContrato.ValorEjecutado);
                    vDataBase.AddInParameter(vDbCommand, "@ValorLiberado", DbType.Decimal, pLiquidacionContrato.ValorLiberado);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionLiberacion", DbType.String, pLiquidacionContrato.JustificacionLiberacion);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pLiquidacionContrato.IdLiquidacionContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdLiquidacionContrato").ToString());
                    GenerarLogAuditoria(pLiquidacionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarLiquidacionContrato(LiquidacionContrato pLiquidacionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_LiquidacionContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdLiquidacionContrato", DbType.Int32, pLiquidacionContrato.IdLiquidacionContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRadicacion", DbType.DateTime, pLiquidacionContrato.FechaRaquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSupervisor", DbType.String, pLiquidacionContrato.NombreSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@CargoSupervisor", DbType.String, pLiquidacionContrato.CargoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@DependenciaSupervisor", DbType.String, pLiquidacionContrato.DependenciaSueprvisor);
                    vDataBase.AddInParameter(vDbCommand, "@TipoLiquidacion", DbType.String, pLiquidacionContrato.TipodeLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pLiquidacionContrato.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@ValorEjecutado", DbType.Decimal, pLiquidacionContrato.ValorEjecutado);
                    vDataBase.AddInParameter(vDbCommand, "@ValorLiberado", DbType.Decimal, pLiquidacionContrato.ValorLiberado);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionLiberacion", DbType.String, pLiquidacionContrato.JustificacionLiberacion);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pLiquidacionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionContrato(int IdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_LiquidacionContrato_ConsultarPorContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<LiquidacionContrato> vLista = new List<LiquidacionContrato>();
                        while (vDataReaderResults.Read())
                        {
                            LiquidacionContrato vLiquidacionContrato = new LiquidacionContrato();
                            vLiquidacionContrato.IdLiquidacionContrato = vDataReaderResults["IdLiquidacionContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLiquidacionContrato"].ToString()) : vLiquidacionContrato.IdLiquidacionContrato;
                            vLiquidacionContrato.FechaRaquidacion = vDataReaderResults["FechaRaquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRaquidacion"].ToString()) : vLiquidacionContrato.FechaRaquidacion;
                            vLiquidacionContrato.IdDetConsModContractual = vDataReaderResults["IdDetConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetConsModContractual"].ToString()) : vLiquidacionContrato.IdDetConsModContractual;
                            vLiquidacionContrato.NombreSupervisor = vDataReaderResults["NombreSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSupervisor"].ToString()) : vLiquidacionContrato.NombreSupervisor;
                            vLiquidacionContrato.DependenciaSueprvisor = vDataReaderResults["DependenciaSueprvisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSueprvisor"].ToString()) : vLiquidacionContrato.DependenciaSueprvisor;
                            vLiquidacionContrato.CargoSupervisor = vDataReaderResults["CargoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSupervisor"].ToString()) : vLiquidacionContrato.CargoSupervisor;
                            vLiquidacionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLiquidacionContrato.UsuarioCrea;
                            vLiquidacionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLiquidacionContrato.FechaCrea;
                            vLiquidacionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLiquidacionContrato.UsuarioModifica;
                            vLiquidacionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLiquidacionContrato.FechaModifica;
                            vLiquidacionContrato.JustificacionLiberacion = vDataReaderResults["JustificacionLiberacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionLiberacion"].ToString()) : vLiquidacionContrato.JustificacionLiberacion;
                            vLiquidacionContrato.ValorEjecutado = vDataReaderResults["ValorEjecutado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEjecutado"].ToString()) : vLiquidacionContrato.ValorEjecutado;
                            vLiquidacionContrato.ValorLiberado = vDataReaderResults["ValorLiberado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorLiberado"].ToString()) : vLiquidacionContrato.ValorLiberado;
                            vLista.Add(vLiquidacionContrato);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionContratoId(int idLiquidacionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_LiquidacionContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdLiquidacionContrato", DbType.Int32, idLiquidacionContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<LiquidacionContrato> vLista = new List<LiquidacionContrato>();
                        while (vDataReaderResults.Read())
                        {
                            LiquidacionContrato vLiquidacionContrato = new LiquidacionContrato();
                            vLiquidacionContrato.IdLiquidacionContrato = vDataReaderResults["IdLiquidacionContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLiquidacionContrato"].ToString()) : vLiquidacionContrato.IdLiquidacionContrato;
                            vLiquidacionContrato.FechaRaquidacion = vDataReaderResults["FechaRaquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRaquidacion"].ToString()) : vLiquidacionContrato.FechaRaquidacion;
                            vLiquidacionContrato.IdDetConsModContractual = vDataReaderResults["IdDetConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetConsModContractual"].ToString()) : vLiquidacionContrato.IdDetConsModContractual;
                            vLiquidacionContrato.NombreSupervisor = vDataReaderResults["NombreSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSupervisor"].ToString()) : vLiquidacionContrato.NombreSupervisor;
                            vLiquidacionContrato.DependenciaSueprvisor = vDataReaderResults["DependenciaSueprvisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSueprvisor"].ToString()) : vLiquidacionContrato.DependenciaSueprvisor;
                            vLiquidacionContrato.CargoSupervisor = vDataReaderResults["CargoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSupervisor"].ToString()) : vLiquidacionContrato.CargoSupervisor;
                            vLiquidacionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLiquidacionContrato.UsuarioCrea;
                            vLiquidacionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLiquidacionContrato.FechaCrea;
                            vLiquidacionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLiquidacionContrato.UsuarioModifica;
                            vLiquidacionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLiquidacionContrato.FechaModifica;
                            vLiquidacionContrato.TipodeLiquidacion = vDataReaderResults["TipoLiquidacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoLiquidacion"].ToString()) : vLiquidacionContrato.TipodeLiquidacion;
                            vLiquidacionContrato.JustificacionLiberacion = vDataReaderResults["JustificacionLiberacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionLiberacion"].ToString()) : vLiquidacionContrato.JustificacionLiberacion;
                            vLiquidacionContrato.ValorEjecutado = vDataReaderResults["ValorEjecutado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEjecutado"].ToString()) : vLiquidacionContrato.ValorEjecutado;
                            vLiquidacionContrato.ValorLiberado = vDataReaderResults["ValorLiberado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorLiberado"].ToString()) : vLiquidacionContrato.ValorLiberado;
                            vLista.Add(vLiquidacionContrato);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionContratoIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_LiquidacionContrato_ConsultarPorIdDetConsModContractual"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDetConsModContractual", DbType.Int32, IdDetConsModContractual);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<LiquidacionContrato> vLista = new List<LiquidacionContrato>();
                        while (vDataReaderResults.Read())
                        {
                            LiquidacionContrato vLiquidacionContrato = new LiquidacionContrato();
                            vLiquidacionContrato.IdLiquidacionContrato = vDataReaderResults["IdLiquidacionContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLiquidacionContrato"].ToString()) : vLiquidacionContrato.IdLiquidacionContrato;
                            vLiquidacionContrato.FechaRaquidacion = vDataReaderResults["FechaRaquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRaquidacion"].ToString()) : vLiquidacionContrato.FechaRaquidacion;
                            vLiquidacionContrato.IdDetConsModContractual = vDataReaderResults["IdDetConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetConsModContractual"].ToString()) : vLiquidacionContrato.IdDetConsModContractual;
                            vLiquidacionContrato.NombreSupervisor = vDataReaderResults["NombreSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSupervisor"].ToString()) : vLiquidacionContrato.NombreSupervisor;
                            vLiquidacionContrato.DependenciaSueprvisor = vDataReaderResults["DependenciaSueprvisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSueprvisor"].ToString()) : vLiquidacionContrato.DependenciaSueprvisor;
                            vLiquidacionContrato.CargoSupervisor = vDataReaderResults["CargoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSupervisor"].ToString()) : vLiquidacionContrato.CargoSupervisor;
                            vLiquidacionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLiquidacionContrato.UsuarioCrea;
                            vLiquidacionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLiquidacionContrato.FechaCrea;
                            vLiquidacionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLiquidacionContrato.UsuarioModifica;
                            vLiquidacionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLiquidacionContrato.FechaModifica;
                            vLiquidacionContrato.JustificacionLiberacion = vDataReaderResults["JustificacionLiberacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionLiberacion"].ToString()) : vLiquidacionContrato.JustificacionLiberacion;
                            vLiquidacionContrato.ValorEjecutado = vDataReaderResults["ValorEjecutado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEjecutado"].ToString()) : vLiquidacionContrato.ValorEjecutado;
                            vLiquidacionContrato.ValorLiberado = vDataReaderResults["ValorLiberado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorLiberado"].ToString()) : vLiquidacionContrato.ValorLiberado;
                            vLista.Add(vLiquidacionContrato);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
