using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad CertificacionesContratos
    /// </summary>
    public class CertificacionesContratosDAL : GeneralDAL
    {
        public CertificacionesContratosDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pCertificacionesContratos"></param>
        public int InsertarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CertificacionesContratos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDCertificacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, pCertificacionesContratos.IDContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCertificacion", DbType.DateTime, pCertificacionesContratos.FechaCertificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pCertificacionesContratos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCertificacionesContratos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCertificacionesContratos.IDCertificacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDCertificacion").ToString());
                    GenerarLogAuditoria(pCertificacionesContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pCertificacionesContratos"></param>
        public int ModificarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CertificacionesContratos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCertificacion", DbType.Int32, pCertificacionesContratos.IDCertificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, pCertificacionesContratos.IDContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCertificacion", DbType.DateTime, pCertificacionesContratos.FechaCertificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pCertificacionesContratos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCertificacionesContratos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCertificacionesContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AdicionarArchivoACertificacion(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CertificacionesContratos_AdicionarArchivo"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCertificacion", DbType.Int32, pCertificacionesContratos.IDCertificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDArchivo", DbType.Decimal, pCertificacionesContratos.IdArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCertificacionesContratos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCertificacionesContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pCertificacionesContratos"></param>
        public int EliminarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CertificacionesContratos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCertificacion", DbType.Int32, pCertificacionesContratos.IDCertificacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCertificacionesContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pIDCertificacion"></param>
        public CertificacionesContratos ConsultarCertificacionesContratos(int pIDCertificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CertificacionesContratos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDCertificacion", DbType.Int32, pIDCertificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CertificacionesContratos vCertificacionesContratos = new CertificacionesContratos();
                        while (vDataReaderResults.Read())
                        {
                            vCertificacionesContratos.IDCertificacion = vDataReaderResults["IDCertificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCertificacion"].ToString()) : vCertificacionesContratos.IDCertificacion;
                            vCertificacionesContratos.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vCertificacionesContratos.IDContrato;
                            vCertificacionesContratos.FechaCertificacion = vDataReaderResults["FechaCertificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacion"].ToString()) : vCertificacionesContratos.FechaCertificacion;
                            vCertificacionesContratos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCertificacionesContratos.Estado;
                            vCertificacionesContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCertificacionesContratos.UsuarioCrea;
                            vCertificacionesContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCertificacionesContratos.FechaCrea;
                            vCertificacionesContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCertificacionesContratos.UsuarioModifica;
                            vCertificacionesContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCertificacionesContratos.FechaModifica;
                        }
                        return vCertificacionesContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="vVigencia"></param>
        /// <param name="vIDRegional"></param>
        /// <param name="vNumeroContrato"></param>
        /// <param name="vFechaInicial"></param>
        /// <param name="vFechaFinal"></param>
        public List<CertificacionesContratos> ConsultarCertificacionesContratoss(int? vVigencia, int? vIDRegional
                    , string vNumeroContrato, DateTime? vFechaInicial, DateTime? vFechaFinal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CertificacionesContratoss_Consultar"))
                {
                    if (vVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, vVigencia);
                    if (vIDRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, vIDRegional);
                    if (vNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, vNumeroContrato);
                    if (vFechaInicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaInicial", DbType.DateTime, vFechaInicial);
                    if (vFechaFinal != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaFinal", DbType.DateTime, vFechaFinal);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CertificacionesContratos> vListaCertificacionesContratos = new List<CertificacionesContratos>();
                        while (vDataReaderResults.Read())
                        {
                            CertificacionesContratos vCertificacionesContratos = new CertificacionesContratos();

                            vCertificacionesContratos.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vCertificacionesContratos.IDContrato;
                            vCertificacionesContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vCertificacionesContratos.NumeroContrato;
                            vCertificacionesContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCertificacionesContratos.FechaCrea;
                            vCertificacionesContratos.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vCertificacionesContratos.ValorInicialContrato;
                            vCertificacionesContratos.DescEstado = vDataReaderResults["DescEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescEstado"].ToString()) : vCertificacionesContratos.DescEstado;
                            vCertificacionesContratos.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vCertificacionesContratos.FechaInicioEjecucion;
                            vCertificacionesContratos.DescRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vCertificacionesContratos.DescRegional;

                            vListaCertificacionesContratos.Add(vCertificacionesContratos);
                        }
                        return vListaCertificacionesContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="idContrato"></param>      
        public List<CertificacionesContratos> ConsultarCertificacionesPorContratos(int? idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CertificacionesPorIDContrato_Consultar"))
                {
                    if (idContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CertificacionesContratos> vListaCertificacionesContratos = new List<CertificacionesContratos>();
                        while (vDataReaderResults.Read())
                        {
                            CertificacionesContratos vCertificacionesContratos = new CertificacionesContratos();

                            //C.IDCertificacion, C.FechaCertificacion, C.UsuarioCrea, C.Estado
                            vCertificacionesContratos.IDCertificacion = vDataReaderResults["IDCertificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCertificacion"].ToString()) : vCertificacionesContratos.IDCertificacion;
                            vCertificacionesContratos.FechaCertificacion = vDataReaderResults["FechaCertificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacion"].ToString()) : vCertificacionesContratos.FechaCertificacion;
                            vCertificacionesContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCertificacionesContratos.UsuarioCrea;
                            vCertificacionesContratos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCertificacionesContratos.Estado;
                            vCertificacionesContratos.StrEstado = vDataReaderResults["StrEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["StrEstado"].ToString()) : vCertificacionesContratos.StrEstado;
                            vCertificacionesContratos.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vCertificacionesContratos.NombreArchivo;
                            vCertificacionesContratos.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vCertificacionesContratos.NombreArchivoOri;
                            
                            vListaCertificacionesContratos.Add(vCertificacionesContratos);
                        }
                        return vListaCertificacionesContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
