using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad AmparosGarantias
    /// </summary>
    public class AmparosGarantiasDAL : GeneralDAL
    {
        public AmparosGarantiasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int InsertarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AmparosGarantias_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDAmparosGarantias", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pAmparosGarantias.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoAmparo", DbType.Int32, pAmparosGarantias.IdTipoAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaDesde", DbType.DateTime, pAmparosGarantias.FechaVigenciaDesde);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaHasta", DbType.DateTime, pAmparosGarantias.FechaVigenciaHasta);
                    vDataBase.AddInParameter(vDbCommand, "@IDUnidadCalculo", DbType.Int32, pAmparosGarantias.IDUnidadCalculo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCalculoAsegurado", DbType.Decimal, pAmparosGarantias.ValorCalculoAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAsegurado", DbType.Decimal, pAmparosGarantias.ValorAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAmparosGarantias.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAmparosGarantias.IDAmparosGarantias = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDAmparosGarantias").ToString());
                    GenerarLogAuditoria(pAmparosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int ModificarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AmparosGarantias_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDAmparosGarantias", DbType.Int32, pAmparosGarantias.IDAmparosGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pAmparosGarantias.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoAmparo", DbType.Int32, pAmparosGarantias.IdTipoAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaDesde", DbType.DateTime, pAmparosGarantias.FechaVigenciaDesde);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaHasta", DbType.DateTime, pAmparosGarantias.FechaVigenciaHasta);
                    vDataBase.AddInParameter(vDbCommand, "@IDUnidadCalculo", DbType.Int32, pAmparosGarantias.IDUnidadCalculo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCalculoAsegurado", DbType.Decimal, pAmparosGarantias.ValorCalculoAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAsegurado", DbType.Decimal, pAmparosGarantias.ValorAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAmparosGarantias.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAmparosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int EliminarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AmparosGarantias_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDAmparosGarantias", DbType.Int32, pAmparosGarantias.IDAmparosGarantias);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAmparosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pIDAmparosGarantias"></param>
        public AmparosGarantias ConsultarAmparosGarantias(int pIDAmparosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AmparosGarantias_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDAmparosGarantias", DbType.Int32, pIDAmparosGarantias);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AmparosGarantias vAmparosGarantias = new AmparosGarantias();
                        while (vDataReaderResults.Read())
                        {
                            vAmparosGarantias.IDAmparosGarantias = vDataReaderResults["IDAmparosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDAmparosGarantias"].ToString()) : vAmparosGarantias.IDAmparosGarantias;
                            vAmparosGarantias.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vAmparosGarantias.IDGarantia;
                            vAmparosGarantias.IdTipoAmparo = vDataReaderResults["IdTipoAmparo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoAmparo"].ToString()) : vAmparosGarantias.IdTipoAmparo;
                            vAmparosGarantias.FechaVigenciaDesde = vDataReaderResults["FechaVigenciaDesde"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaDesde"].ToString()) : vAmparosGarantias.FechaVigenciaDesde;
                            vAmparosGarantias.FechaVigenciaHasta = vDataReaderResults["FechaVigenciaHasta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaHasta"].ToString()) : vAmparosGarantias.FechaVigenciaHasta;
                            vAmparosGarantias.IDUnidadCalculo = vDataReaderResults["IDUnidadCalculo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUnidadCalculo"].ToString()) : vAmparosGarantias.IDUnidadCalculo;
                            vAmparosGarantias.ValorCalculoAsegurado = vDataReaderResults["ValorCalculoAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCalculoAsegurado"].ToString()) : vAmparosGarantias.ValorCalculoAsegurado;
                            vAmparosGarantias.ValorAsegurado = vDataReaderResults["ValorAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAsegurado"].ToString()) : vAmparosGarantias.ValorAsegurado;
                            vAmparosGarantias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAmparosGarantias.UsuarioCrea;
                            vAmparosGarantias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAmparosGarantias.FechaCrea;
                            vAmparosGarantias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAmparosGarantias.UsuarioModifica;
                            vAmparosGarantias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAmparosGarantias.FechaModifica;
                        }
                        return vAmparosGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pFechaVigenciaDesde"></param>
        /// <param name="pFechaVigenciaHasta"></param>
        /// <param name="pValorCalculoAsegurado"></param>
        /// <param name="pValorAsegurado"></param>
        public List<AmparosGarantias> ConsultarAmparosGarantiass(DateTime? pFechaVigenciaDesde, DateTime? pFechaVigenciaHasta, Decimal? pValorCalculoAsegurado, Decimal? pValorAsegurado, int? pIDGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AmparosGarantiass_Consultar"))
                {
                    if(pFechaVigenciaDesde != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaDesde", DbType.DateTime, pFechaVigenciaDesde);
                    if(pFechaVigenciaHasta != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaHasta", DbType.DateTime, pFechaVigenciaHasta);
                    if(pValorCalculoAsegurado != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorCalculoAsegurado", DbType.Decimal, pValorCalculoAsegurado);
                    if(pValorAsegurado != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorAsegurado", DbType.Decimal, pValorAsegurado);
                    if (pIDGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pIDGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AmparosGarantias> vListaAmparosGarantias = new List<AmparosGarantias>();
                        while (vDataReaderResults.Read())
                        {
                                AmparosGarantias vAmparosGarantias = new AmparosGarantias();
                            vAmparosGarantias.IDAmparosGarantias = vDataReaderResults["IDAmparosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDAmparosGarantias"].ToString()) : vAmparosGarantias.IDAmparosGarantias;
                            vAmparosGarantias.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vAmparosGarantias.IDGarantia;
                            vAmparosGarantias.IdTipoAmparo = vDataReaderResults["IdTipoAmparo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoAmparo"].ToString()) : vAmparosGarantias.IdTipoAmparo;
                            vAmparosGarantias.NombreTipoAmparo = vDataReaderResults["NombreTipoAmparo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoAmparo"].ToString()) : vAmparosGarantias.NombreTipoAmparo;
                            vAmparosGarantias.FechaVigenciaDesde = vDataReaderResults["FechaVigenciaDesde"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaDesde"].ToString()) : vAmparosGarantias.FechaVigenciaDesde;
                            vAmparosGarantias.FechaVigenciaHasta = vDataReaderResults["FechaVigenciaHasta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaHasta"].ToString()) : vAmparosGarantias.FechaVigenciaHasta;
                            vAmparosGarantias.IDUnidadCalculo = vDataReaderResults["IDUnidadCalculo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUnidadCalculo"].ToString()) : vAmparosGarantias.IDUnidadCalculo;
                            vAmparosGarantias.NombreUnidadCalculo = vDataReaderResults["NombreUnidadCalculo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreUnidadCalculo"].ToString()) : vAmparosGarantias.NombreUnidadCalculo;
                            vAmparosGarantias.ValorCalculoAsegurado = vDataReaderResults["ValorCalculoAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCalculoAsegurado"].ToString()) : vAmparosGarantias.ValorCalculoAsegurado;
                            vAmparosGarantias.ValorAsegurado = vDataReaderResults["ValorAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAsegurado"].ToString()) : vAmparosGarantias.ValorAsegurado;
                            vAmparosGarantias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAmparosGarantias.UsuarioCrea;
                            vAmparosGarantias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAmparosGarantias.FechaCrea;
                            vAmparosGarantias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAmparosGarantias.UsuarioModifica;
                            vAmparosGarantias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAmparosGarantias.FechaModifica;
                            vAmparosGarantias.valorSIRECI = vDataReaderResults["valorSIRECI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["valorSIRECI"].ToString()) : vAmparosGarantias.valorSIRECI;
                               
                            vListaAmparosGarantias.Add(vAmparosGarantias);
                        }
                        return vListaAmparosGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para obtener los valores numero de convenio y valor inicial del contrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public Contrato.Entity.Contrato ConsultarValoresContrato(int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ValoresContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                        while (vDataReaderResults.Read())
                        {
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato; 
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            vContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContrato.UsuarioCrea;
                            vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContrato.FechaSuscripcion;
                            vContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalContrato"].ToString()) : vContrato.FechaFinalTerminacionContrato;
                            vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;

                        }
                        return vContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public AmparosGarantias ConsultarFechaVigenciaAmparoRelacionado(int? IdGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ConsultarFechaVigenciaAmparoRelacionado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdGarantia", DbType.Int32, IdGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AmparosGarantias vAmparosGarantias = new AmparosGarantias();
                        while (vDataReaderResults.Read())
                        {
                            vAmparosGarantias.FechaVigenciaDesde = vDataReaderResults["FechaVigenciaDesde"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaDesde"].ToString()) : vAmparosGarantias.FechaVigenciaDesde;
                            vAmparosGarantias.FechaVigenciaHasta = vDataReaderResults["FechaVigenciaHasta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaHasta"].ToString()) : vAmparosGarantias.FechaVigenciaHasta;
                        }
                        return vAmparosGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
