using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class ProrrogasDAL : GeneralDAL
    {
        public ProrrogasDAL()
        {
        }

        public int InsertarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Prorrogas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdProrroga", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pProrrogas.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pProrrogas.FechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@Dias", DbType.Int32, pProrrogas.Dias);
                    vDataBase.AddInParameter(vDbCommand, "@Meses", DbType.Int32, pProrrogas.Meses);
                    vDataBase.AddInParameter(vDbCommand, "@Años", DbType.Int32, pProrrogas.Años);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pProrrogas.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleConsModContractual", DbType.Int32, pProrrogas.IdDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pProrrogas.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProrrogas.IdProrroga = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProrroga").ToString());
                    GenerarLogAuditoria(pProrrogas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Prorrogas_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProrroga", DbType.Int32, pProrrogas.IdProrroga);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pProrrogas.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pProrrogas.FechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@Dias", DbType.Int32, pProrrogas.Dias);
                    vDataBase.AddInParameter(vDbCommand, "@Meses", DbType.Int32, pProrrogas.Meses);
                    vDataBase.AddInParameter(vDbCommand, "@Años", DbType.Int32, pProrrogas.Años);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pProrrogas.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProrrogas.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProrrogas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int EliminarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Prorrogas_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProrroga", DbType.Int32, pProrrogas.IdProrroga);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProrrogas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Prorrogas ConsultarProrrogas(int pIdProrroga)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Prorrogas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProrroga", DbType.Int32, pIdProrroga);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Prorrogas vProrrogas = new Prorrogas();
                        while (vDataReaderResults.Read())
                        {
                            vProrrogas.IdProrroga = vDataReaderResults["IdProrroga"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProrroga"].ToString()) : vProrrogas.IdProrroga;
                            vProrrogas.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vProrrogas.FechaInicio;
                            vProrrogas.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vProrrogas.FechaFin;
                            vProrrogas.Dias = vDataReaderResults["Dias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Dias"].ToString()) : vProrrogas.Dias;
                            vProrrogas.Meses = vDataReaderResults["Meses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Meses"].ToString()) : vProrrogas.Meses;
                            vProrrogas.Años = vDataReaderResults["Años"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Años"].ToString()) : vProrrogas.Años;
                            vProrrogas.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vProrrogas.Justificacion;
                            vProrrogas.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vProrrogas.IdDetalleConsModContractual;
                            vProrrogas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProrrogas.UsuarioCrea;
                            vProrrogas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProrrogas.FechaCrea;
                            vProrrogas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProrrogas.UsuarioModifica;
                            vProrrogas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProrrogas.FechaModifica;
                        }
                        return vProrrogas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Prorrogas> ConsultarProrrogass(DateTime? pFechaInicio, DateTime? pFechaFin, int? pDias, int? pMeses, int? pAños, String pJustificacion, int? pIdDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Prorrogass_Consultar"))
                {
                    if(pFechaInicio != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFechaInicio);
                    if(pFechaFin != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pFechaFin);
                    if(pDias != null)
                         vDataBase.AddInParameter(vDbCommand, "@Dias", DbType.Int32, pDias);
                    if(pMeses != null)
                         vDataBase.AddInParameter(vDbCommand, "@Meses", DbType.Int32, pMeses);
                    if(pAños != null)
                         vDataBase.AddInParameter(vDbCommand, "@Años", DbType.Int32, pAños);
                    if(pJustificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pJustificacion);
                    if(pIdDetalleConsModContractual != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdDetalleConsModContractual", DbType.Int32, pIdDetalleConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Prorrogas> vListaProrrogas = new List<Prorrogas>();
                        while (vDataReaderResults.Read())
                        {
                                Prorrogas vProrrogas = new Prorrogas();
                            vProrrogas.IdProrroga = vDataReaderResults["IdProrroga"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProrroga"].ToString()) : vProrrogas.IdProrroga;
                            vProrrogas.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vProrrogas.FechaInicio;
                            vProrrogas.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vProrrogas.FechaFin;
                            vProrrogas.Dias = vDataReaderResults["Dias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Dias"].ToString()) : vProrrogas.Dias;
                            vProrrogas.Meses = vDataReaderResults["Meses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Meses"].ToString()) : vProrrogas.Meses;
                            vProrrogas.Años = vDataReaderResults["Años"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Años"].ToString()) : vProrrogas.Años;
                            vProrrogas.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vProrrogas.Justificacion;
                            vProrrogas.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vProrrogas.IdDetalleConsModContractual;
                            vProrrogas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProrrogas.UsuarioCrea;
                            vProrrogas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProrrogas.FechaCrea;
                            vProrrogas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProrrogas.UsuarioModifica;
                            vProrrogas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProrrogas.FechaModifica;
                                vListaProrrogas.Add(vProrrogas);
                        }
                        return vListaProrrogas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Prorrogas> ConsultarProrrogasContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Prorrogas_ConsultarInformacionProrroga"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idContrato", DbType.Int32, idContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Prorrogas> vListaProrrogas = new List<Prorrogas>();
                        while (vDataReaderResults.Read())
                        {
                            Prorrogas vProrrogas = new Prorrogas();
                            vProrrogas.IdProrroga = vDataReaderResults["IdProrroga"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProrroga"].ToString()) : vProrrogas.IdProrroga;
                            vProrrogas.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vProrrogas.FechaInicio;
                            vProrrogas.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vProrrogas.FechaFin;
                            vProrrogas.Dias = vDataReaderResults["Dias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Dias"].ToString()) : vProrrogas.Dias;
                            vProrrogas.Meses = vDataReaderResults["Meses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Meses"].ToString()) : vProrrogas.Meses;
                            vProrrogas.Años = vDataReaderResults["Años"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Años"].ToString()) : vProrrogas.Años;
                            vProrrogas.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vProrrogas.Justificacion;
                            vProrrogas.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vProrrogas.IdDetalleConsModContractual;
                            vProrrogas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProrrogas.UsuarioCrea;
                            vProrrogas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProrrogas.FechaCrea;
                            vProrrogas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProrrogas.UsuarioModifica;
                            vProrrogas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProrrogas.FechaModifica;
                            vListaProrrogas.Add(vProrrogas);
                        }
                        return vListaProrrogas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<Prorrogas> ConsultarProrrogasAprobadasContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Prorrogas_ConsultarAprobadasContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idContrato", DbType.Int32, idContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Prorrogas> vListaProrrogas = new List<Prorrogas>();
                        while (vDataReaderResults.Read())
                        {
                            Prorrogas vProrrogas = new Prorrogas();
                            vProrrogas.IdProrroga = vDataReaderResults["IdProrroga"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProrroga"].ToString()) : vProrrogas.IdProrroga;
                            vProrrogas.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vProrrogas.FechaInicio;
                            vProrrogas.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vProrrogas.FechaFin;
                            vProrrogas.Dias = vDataReaderResults["Dias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Dias"].ToString()) : vProrrogas.Dias;
                            vProrrogas.Meses = vDataReaderResults["Meses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Meses"].ToString()) : vProrrogas.Meses;
                            vProrrogas.Años = vDataReaderResults["Años"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Años"].ToString()) : vProrrogas.Años;
                            vProrrogas.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vProrrogas.IdDetalleConsModContractual;
                            vProrrogas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProrrogas.UsuarioCrea;
                            vProrrogas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProrrogas.FechaCrea;
                            vProrrogas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProrrogas.UsuarioModifica;
                            vProrrogas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProrrogas.FechaModifica;
                            vListaProrrogas.Add(vProrrogas);
                        }
                        return vListaProrrogas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public Tiempo ConsultarProrrogasAprobadasContratoDetalle(int idContrato)
        {
            var result = new Tiempo();
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Prorrogas_ConsultarAporbadasDetalle"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand,  "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@totalDias", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@totalMeses", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@totalAnios", DbType.Int32, 18);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    if (!string.IsNullOrEmpty(vDataBase.GetParameterValue(vDbCommand, "@totalDias").ToString()))
                    result.Dias = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@totalDias").ToString());

                    if (!string.IsNullOrEmpty(vDataBase.GetParameterValue(vDbCommand, "@totalMeses").ToString()))
                    result.Meses = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@totalMeses").ToString());

                    if (!string.IsNullOrEmpty(vDataBase.GetParameterValue(vDbCommand, "@totalAnios").ToString()))
                    result.Anios = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@totalAnios").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }        
    }
}
