using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Gesti�n de Obligaciones
    /// </summary>
    public class GestionarObligacionDAL : GeneralDAL
    {
        public GestionarObligacionDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int InsertarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarObligacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdGestionObligacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionClausula", DbType.Int32, pGestionarObligacion.IdGestionClausula);
                    vDataBase.AddInParameter(vDbCommand, "@NombreObligacion", DbType.String, pGestionarObligacion.NombreObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pGestionarObligacion.IdTipoObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.String, pGestionarObligacion.Orden);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionObligacion", DbType.String, pGestionarObligacion.DescripcionObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pGestionarObligacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pGestionarObligacion.IdGestionObligacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdGestionObligacion").ToString());
                    GenerarLogAuditoria(pGestionarObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int ModificarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarObligacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionObligacion", DbType.Int32, pGestionarObligacion.IdGestionObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionClausula", DbType.Int32, pGestionarObligacion.IdGestionClausula);
                    vDataBase.AddInParameter(vDbCommand, "@NombreObligacion", DbType.String, pGestionarObligacion.NombreObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pGestionarObligacion.IdTipoObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.String, pGestionarObligacion.Orden);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionObligacion", DbType.String, pGestionarObligacion.DescripcionObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pGestionarObligacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGestionarObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int EliminarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarObligacion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionObligacion", DbType.Int32, pGestionarObligacion.IdGestionObligacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGestionarObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pIdGestionObligacion"></param>
        /// <returns></returns>
        public GestionarObligacion ConsultarGestionarObligacion(int pIdGestionObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarObligacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionObligacion", DbType.Int32, pIdGestionObligacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        GestionarObligacion vGestionarObligacion = new GestionarObligacion();
                        while (vDataReaderResults.Read())
                        {
                            vGestionarObligacion.IdGestionObligacion = vDataReaderResults["IdGestionObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionObligacion"].ToString()) : vGestionarObligacion.IdGestionObligacion;
                            vGestionarObligacion.IdGestionClausula = vDataReaderResults["IdGestionClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionClausula"].ToString()) : vGestionarObligacion.IdGestionClausula;
                            vGestionarObligacion.NombreObligacion = vDataReaderResults["NombreObligacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreObligacion"].ToString()) : vGestionarObligacion.NombreObligacion;
                            vGestionarObligacion.IdTipoObligacion = vDataReaderResults["IdTipoObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObligacion"].ToString()) : vGestionarObligacion.IdTipoObligacion;
                            vGestionarObligacion.Orden = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Orden"].ToString()) : vGestionarObligacion.Orden;
                            vGestionarObligacion.DescripcionObligacion = vDataReaderResults["DescripcionObligacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionObligacion"].ToString()) : vGestionarObligacion.DescripcionObligacion;
                            vGestionarObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGestionarObligacion.UsuarioCrea;
                            vGestionarObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGestionarObligacion.FechaCrea;
                            vGestionarObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGestionarObligacion.UsuarioModifica;
                            vGestionarObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGestionarObligacion.FechaModifica;
                        }
                        return vGestionarObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pIdGestionObligacion"></param>
        /// <param name="pIdGestionClausula"></param>
        /// <param name="pNombreObligacion"></param>
        /// <param name="pIdTipoObligacion"></param>
        /// <param name="pOrden"></param>
        /// <param name="pDescripcionObligacion"></param>
        /// <returns></returns>
        public List<GestionarObligacion> ConsultarGestionarObligacions(int? pIdGestionObligacion, int? pIdGestionClausula, String pNombreObligacion, int? pIdTipoObligacion, String pOrden, String pDescripcionObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarObligacions_Consultar"))
                {
                    if(pIdGestionObligacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdGestionObligacion", DbType.Int32, pIdGestionObligacion);
                    if(pIdGestionClausula != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdGestionClausula", DbType.Int32, pIdGestionClausula);
                    if(pNombreObligacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreObligacion", DbType.String, pNombreObligacion);
                    if(pIdTipoObligacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pIdTipoObligacion);
                    if(pOrden != null)
                         vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.String, pOrden);
                    if(pDescripcionObligacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@DescripcionObligacion", DbType.String, pDescripcionObligacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GestionarObligacion> vListaGestionarObligacion = new List<GestionarObligacion>();
                        while (vDataReaderResults.Read())
                        {
                                GestionarObligacion vGestionarObligacion = new GestionarObligacion();
                            vGestionarObligacion.IdGestionObligacion = vDataReaderResults["IdGestionObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionObligacion"].ToString()) : vGestionarObligacion.IdGestionObligacion;
                            vGestionarObligacion.IdGestionClausula = vDataReaderResults["IdGestionClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionClausula"].ToString()) : vGestionarObligacion.IdGestionClausula;
                            vGestionarObligacion.NombreObligacion = vDataReaderResults["NombreObligacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreObligacion"].ToString()) : vGestionarObligacion.NombreObligacion;
                            vGestionarObligacion.IdTipoObligacion = vDataReaderResults["IdTipoObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObligacion"].ToString()) : vGestionarObligacion.IdTipoObligacion;
                            vGestionarObligacion.Orden = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Orden"].ToString()) : vGestionarObligacion.Orden;
                            vGestionarObligacion.DescripcionObligacion = vDataReaderResults["DescripcionObligacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionObligacion"].ToString()) : vGestionarObligacion.DescripcionObligacion;
                            vGestionarObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGestionarObligacion.UsuarioCrea;
                            vGestionarObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGestionarObligacion.FechaCrea;
                            vGestionarObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGestionarObligacion.UsuarioModifica;
                            vGestionarObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGestionarObligacion.FechaModifica;
                                vListaGestionarObligacion.Add(vGestionarObligacion);
                        }
                        return vListaGestionarObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
