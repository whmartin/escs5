using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad unidad medida
    /// </summary>
    public class UnidadMedidaDAL : GeneralDAL
    {
        public UnidadMedidaDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int InsertarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_UnidadMedida_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdNumeroContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pUnidadMedida.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecuciónContrato", DbType.DateTime, pUnidadMedida.FechaInicioEjecuciónContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionInicialContrato", DbType.DateTime, pUnidadMedida.FechaTerminacionInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalTerminacionContrato", DbType.DateTime, pUnidadMedida.FechaFinalTerminacionContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUnidadMedida.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pUnidadMedida.IdNumeroContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdNumeroContrato").ToString());
                    GenerarLogAuditoria(pUnidadMedida, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int ModificarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_UnidadMedida_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroContrato", DbType.Int32, pUnidadMedida.IdNumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pUnidadMedida.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecuciónContrato", DbType.DateTime, pUnidadMedida.FechaInicioEjecuciónContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionInicialContrato", DbType.DateTime, pUnidadMedida.FechaTerminacionInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalTerminacionContrato", DbType.DateTime, pUnidadMedida.FechaFinalTerminacionContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUnidadMedida.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUnidadMedida, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int EliminarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_UnidadMedida_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroContrato", DbType.Int32, pUnidadMedida.IdNumeroContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUnidadMedida, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad UnidadMedida
        /// </summary>
        /// <param name="pIdNumeroContrato"></param>
        /// <returns></returns>
        public UnidadMedida ConsultarUnidadMedida(int pIdNumeroContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_UnidadMedida_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroContrato", DbType.Int32, pIdNumeroContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        UnidadMedida vUnidadMedida = new UnidadMedida();
                        while (vDataReaderResults.Read())
                        {
                            vUnidadMedida.IdNumeroContrato = vDataReaderResults["IdNumeroContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroContrato"].ToString()) : vUnidadMedida.IdNumeroContrato;
                            vUnidadMedida.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vUnidadMedida.NumeroContrato;
                            vUnidadMedida.FechaInicioEjecuciónContrato = vDataReaderResults["FechaInicioEjecuciónContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecuciónContrato"].ToString()) : vUnidadMedida.FechaInicioEjecuciónContrato;
                            vUnidadMedida.FechaTerminacionInicialContrato = vDataReaderResults["FechaTerminacionInicialContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionInicialContrato"].ToString()) : vUnidadMedida.FechaTerminacionInicialContrato;
                            vUnidadMedida.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vUnidadMedida.FechaFinalTerminacionContrato;
                            vUnidadMedida.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vUnidadMedida.UsuarioCrea;
                            vUnidadMedida.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vUnidadMedida.FechaCrea;
                            vUnidadMedida.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vUnidadMedida.UsuarioModifica;
                            vUnidadMedida.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vUnidadMedida.FechaModifica;
                        }
                        return vUnidadMedida;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad UnidadMedida
        /// </summary>
        /// <param name="pIdNumeroContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pFechaInicioEjecuciónContrato"></param>
        /// <param name="pFechaTerminacionInicialContrato"></param>
        /// <param name="pFechaFinalTerminacionContrato"></param>
        /// <returns></returns>
        public List<UnidadMedida> ConsultarUnidadMedidas(int? pIdNumeroContrato, String pNumeroContrato, DateTime? pFechaInicioEjecuciónContrato, DateTime? pFechaTerminacionInicialContrato, DateTime? pFechaFinalTerminacionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_UnidadMedidas_Consultar"))
                {
                    if(pIdNumeroContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdNumeroContrato", DbType.Int32, pIdNumeroContrato);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if(pFechaInicioEjecuciónContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecuciónContrato", DbType.DateTime, pFechaInicioEjecuciónContrato);
                    if(pFechaTerminacionInicialContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionInicialContrato", DbType.DateTime, pFechaTerminacionInicialContrato);
                    if(pFechaFinalTerminacionContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaFinalTerminacionContrato", DbType.DateTime, pFechaFinalTerminacionContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<UnidadMedida> vListaUnidadMedida = new List<UnidadMedida>();
                        while (vDataReaderResults.Read())
                        {
                                UnidadMedida vUnidadMedida = new UnidadMedida();
                            vUnidadMedida.IdNumeroContrato = vDataReaderResults["IdNumeroContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroContrato"].ToString()) : vUnidadMedida.IdNumeroContrato;
                            vUnidadMedida.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vUnidadMedida.NumeroContrato;
                            vUnidadMedida.FechaInicioEjecuciónContrato = vDataReaderResults["FechaInicioEjecuciónContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecuciónContrato"].ToString()) : vUnidadMedida.FechaInicioEjecuciónContrato;
                            vUnidadMedida.FechaTerminacionInicialContrato = vDataReaderResults["FechaTerminacionInicialContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionInicialContrato"].ToString()) : vUnidadMedida.FechaTerminacionInicialContrato;
                            vUnidadMedida.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vUnidadMedida.FechaFinalTerminacionContrato;
                            vUnidadMedida.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vUnidadMedida.UsuarioCrea;
                            vUnidadMedida.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vUnidadMedida.FechaCrea;
                            vUnidadMedida.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vUnidadMedida.UsuarioModifica;
                            vUnidadMedida.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vUnidadMedida.FechaModifica;
                                vListaUnidadMedida.Add(vUnidadMedida);
                        }
                        return vListaUnidadMedida;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
