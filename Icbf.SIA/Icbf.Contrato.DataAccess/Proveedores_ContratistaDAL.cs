﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Proveedores_Contratista
    /// </summary>
    public class Proveedores_ContratistaDAL : GeneralDAL
    {
        public Proveedores_ContratistaDAL()
        {
        }
        ///// <summary>
        ///// Método de inserción para la entidad Proveedores_Contratista
        ///// </summary>
        ///// <param name="pProveedores_Contratista"></param>
        //public int InsertarProveedores_Contratista(Proveedores_Contratista pProveedores_Contratista)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratista_Insertar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddOutParameter(vDbCommand, "@IdProveedoresContratista", DbType.Int32, 18);
        //            vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pProveedores_Contratista.IdEntidad);
        //            vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pProveedores_Contratista.IdTercero);
        //            vDataBase.AddInParameter(vDbCommand, "@NombreTipoPersona", DbType.String, pProveedores_Contratista.NombreTipoPersona);
        //            vDataBase.AddInParameter(vDbCommand, "@CodDocumento", DbType.String, pProveedores_Contratista.CodDocumento);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pProveedores_Contratista.NumeroIdentificacion);
        //            vDataBase.AddInParameter(vDbCommand, "@Razonsocial", DbType.String, pProveedores_Contratista.Razonsocial);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroidentificacionRepLegal", DbType.String, pProveedores_Contratista.NumeroidentificacionRepLegal);
        //            vDataBase.AddInParameter(vDbCommand, "@RazonsocialRepLegal", DbType.String, pProveedores_Contratista.RazonsocialRepLegal);
        //            vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pProveedores_Contratista.UsuarioCrea);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            pProveedores_Contratista.IdProveedoresContratista = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProveedoresContratista").ToString());
        //            GenerarLogAuditoria(pProveedores_Contratista, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de modificación para la entidad Proveedores_Contratista
        ///// </summary>
        ///// <param name="pProveedores_Contratista"></param>
        //public int ModificarProveedores_Contratista(Proveedores_Contratista pProveedores_Contratista)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratista_Modificar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddInParameter(vDbCommand, "@IdProveedoresContratista", DbType.Int32, pProveedores_Contratista.IdProveedoresContratista);
        //            vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pProveedores_Contratista.IdEntidad);
        //            vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pProveedores_Contratista.IdTercero);
        //            vDataBase.AddInParameter(vDbCommand, "@NombreTipoPersona", DbType.String, pProveedores_Contratista.NombreTipoPersona);
        //            vDataBase.AddInParameter(vDbCommand, "@CodDocumento", DbType.String, pProveedores_Contratista.CodDocumento);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pProveedores_Contratista.NumeroIdentificacion);
        //            vDataBase.AddInParameter(vDbCommand, "@Razonsocial", DbType.String, pProveedores_Contratista.Razonsocial);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroidentificacionRepLegal", DbType.String, pProveedores_Contratista.NumeroidentificacionRepLegal);
        //            vDataBase.AddInParameter(vDbCommand, "@RazonsocialRepLegal", DbType.String, pProveedores_Contratista.RazonsocialRepLegal);
        //            vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProveedores_Contratista.UsuarioModifica);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            GenerarLogAuditoria(pProveedores_Contratista, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de eliminación para la entidad Proveedores_Contratista
        ///// </summary>
        ///// <param name="pProveedores_Contratista"></param>
        //public int EliminarProveedores_Contratista(Proveedores_Contratista pProveedores_Contratista)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratista_Eliminar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddInParameter(vDbCommand, "@IdProveedoresContratista", DbType.Int32, pProveedores_Contratista.IdProveedoresContratista);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            GenerarLogAuditoria(pProveedores_Contratista, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}


        ///// <summary>
        ///// Método de consulta por id para la entidad Proveedores_Contratista
        ///// </summary>
        ///// <param name="pIdProveedoresContratista"></param>
        //public Proveedores_Contratista ConsultarProveedores_Contratista(int pIdProveedoresContratista)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratista_Consultar"))
        //        {
        //            vDataBase.AddInParameter(vDbCommand, "@IdProveedoresContratista", DbType.Int32, pIdProveedoresContratista);
        //            using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
        //            {
        //                Proveedores_Contratista vProveedores_Contratista = new Proveedores_Contratista();
        //                while (vDataReaderResults.Read())
        //                {
        //                    vProveedores_Contratista.IdProveedoresContratista = vDataReaderResults["IdProveedoresContratista"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratista"].ToString()) : vProveedores_Contratista.IdProveedoresContratista;
        //                    vProveedores_Contratista.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vProveedores_Contratista.IdEntidad;
        //                    vProveedores_Contratista.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProveedores_Contratista.IdTercero;
        //                    vProveedores_Contratista.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vProveedores_Contratista.NombreTipoPersona;
        //                    vProveedores_Contratista.CodDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vProveedores_Contratista.CodDocumento;
        //                    vProveedores_Contratista.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vProveedores_Contratista.NumeroIdentificacion;
        //                    vProveedores_Contratista.Razonsocial = vDataReaderResults["Razonsocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razonsocial"].ToString()) : vProveedores_Contratista.Razonsocial;
        //                    vProveedores_Contratista.NumeroidentificacionRepLegal = vDataReaderResults["NumeroidentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroidentificacionRepLegal"].ToString()) : vProveedores_Contratista.NumeroidentificacionRepLegal;
        //                    vProveedores_Contratista.RazonsocialRepLegal = vDataReaderResults["RazonsocialRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonsocialRepLegal"].ToString()) : vProveedores_Contratista.RazonsocialRepLegal;
        //                    vProveedores_Contratista.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProveedores_Contratista.UsuarioCrea;
        //                    vProveedores_Contratista.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProveedores_Contratista.FechaCrea;
        //                    vProveedores_Contratista.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProveedores_Contratista.UsuarioModifica;
        //                    vProveedores_Contratista.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProveedores_Contratista.FechaModifica;
        //                }
        //                return vProveedores_Contratista;
        //            }
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratista
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pIdTercero"></param>
        /// <param name="pNombreTipoPersona"></param>
        /// <param name="pCodDocumento"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pRazonsocial"></param>
        /// <param name="pNumeroidentificacionRepLegal"></param>
        /// <param name="pRazonsocialRepLegal"></param>
        public List<Proveedores_Contratista> ConsultarProveedores_Contratistas(int? pIdEntidad, int? pIdTercero, String pNombreTipoPersona, String pCodDocumento, String pNumeroIdentificacion, String pRazonsocial, String pNumeroidentificacionRepLegal, String pRazonsocialRepLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratistas_Consultar"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if (pIdTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    if (pNombreTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoPersona", DbType.String, pNombreTipoPersona);
                    if (pCodDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodDocumento", DbType.String, pCodDocumento);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    if (pRazonsocial != null)
                        vDataBase.AddInParameter(vDbCommand, "@Razonsocial", DbType.String, pRazonsocial);
                    if (pNumeroidentificacionRepLegal != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroidentificacionRepLegal", DbType.String, pNumeroidentificacionRepLegal);
                    if (pRazonsocialRepLegal != null)
                        vDataBase.AddInParameter(vDbCommand, "@RazonsocialRepLegal", DbType.String, pRazonsocialRepLegal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Proveedores_Contratista> vListaProveedores_Contratista = new List<Proveedores_Contratista>();
                        while (vDataReaderResults.Read())
                        {
                            Proveedores_Contratista vProveedores_Contratista = new Proveedores_Contratista();
                            //vProveedores_Contratista.IdProveedoresContratista = vDataReaderResults["IdProveedoresContratista"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratista"].ToString()) : vProveedores_Contratista.IdProveedoresContratista;
                            vProveedores_Contratista.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vProveedores_Contratista.IdEntidad;
                            vProveedores_Contratista.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProveedores_Contratista.IdTercero;
                            vProveedores_Contratista.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vProveedores_Contratista.NombreTipoPersona;
                            vProveedores_Contratista.CodDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vProveedores_Contratista.CodDocumento;
                            vProveedores_Contratista.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vProveedores_Contratista.NumeroIdentificacion;
                            vProveedores_Contratista.Razonsocial = vDataReaderResults["Razonsocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razonsocial"].ToString()) : vProveedores_Contratista.Razonsocial;
                            vProveedores_Contratista.NumeroidentificacionRepLegal = vDataReaderResults["NumeroidentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroidentificacionRepLegal"].ToString()) : vProveedores_Contratista.NumeroidentificacionRepLegal;
                            vProveedores_Contratista.RazonsocialRepLegal = vDataReaderResults["RazonsocialRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonsocialRepLegal"].ToString()) : vProveedores_Contratista.RazonsocialRepLegal;
                            //vProveedores_Contratista.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProveedores_Contratista.UsuarioCrea;
                            //vProveedores_Contratista.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProveedores_Contratista.FechaCrea;
                            //vProveedores_Contratista.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProveedores_Contratista.UsuarioModifica;
                            //vProveedores_Contratista.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProveedores_Contratista.FechaModifica;
                            vListaProveedores_Contratista.Add(vProveedores_Contratista);
                        }
                        return vListaProveedores_Contratista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
