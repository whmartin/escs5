using System;
using System.Collections.Generic;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad DirectorInterventoria
    /// </summary>
    public class DirectorInterventoriaDAL : GeneralDAL
    {
        public DirectorInterventoriaDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pDirectorInterventoria"></param>
        public int InsertarDirectorInterventoria(DirectorInterventoria pDirectorInterventoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DirectorInterventoria_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDDirectorInterventoria", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pDirectorInterventoria.IdTipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pDirectorInterventoria.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pDirectorInterventoria.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pDirectorInterventoria.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pDirectorInterventoria.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pDirectorInterventoria.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pDirectorInterventoria.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pDirectorInterventoria.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pDirectorInterventoria.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDirectorInterventoria.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDirectorInterventoria.IDDirectorInterventoria = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDDirectorInterventoria").ToString());
                    GenerarLogAuditoria(pDirectorInterventoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pDirectorInterventoria"></param>
        public int ModificarDirectorInterventoria(DirectorInterventoria pDirectorInterventoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DirectorInterventoria_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDDirectorInterventoria", DbType.Int32, pDirectorInterventoria.IDDirectorInterventoria);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.String, pDirectorInterventoria.IdTipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pDirectorInterventoria.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pDirectorInterventoria.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pDirectorInterventoria.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pDirectorInterventoria.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pDirectorInterventoria.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pDirectorInterventoria.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pDirectorInterventoria.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pDirectorInterventoria.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDirectorInterventoria.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDirectorInterventoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pDirectorInterventoria"></param>
        public int EliminarDirectorInterventoria(DirectorInterventoria pDirectorInterventoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DirectorInterventoria_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDDirectorInterventoria", DbType.Int32, pDirectorInterventoria.IDDirectorInterventoria);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDirectorInterventoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pIDDirectorInterventoria"></param>
        public DirectorInterventoria ConsultarDirectorInterventoria(int pIDDirectorInterventoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DirectorInterventoria_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDDirectorInterventoria", DbType.Int32, pIDDirectorInterventoria);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DirectorInterventoria vDirectorInterventoria = new DirectorInterventoria();
                        while (vDataReaderResults.Read())
                        {
                            vDirectorInterventoria.IDDirectorInterventoria = vDataReaderResults["IDDirectorInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDirectorInterventoria"].ToString()) : vDirectorInterventoria.IDDirectorInterventoria;
                            vDirectorInterventoria.IdTipoIdentificacion = vDataReaderResults["IdTipoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoIdentificacion"].ToString()) : vDirectorInterventoria.IdTipoIdentificacion;
                            vDirectorInterventoria.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vDirectorInterventoria.NumeroIdentificacion;
                            vDirectorInterventoria.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vDirectorInterventoria.PrimerNombre;
                            vDirectorInterventoria.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vDirectorInterventoria.SegundoNombre;
                            vDirectorInterventoria.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vDirectorInterventoria.PrimerApellido;
                            vDirectorInterventoria.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vDirectorInterventoria.SegundoApellido;
                            vDirectorInterventoria.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vDirectorInterventoria.Celular;
                            vDirectorInterventoria.Celular = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vDirectorInterventoria.Telefono;
                            vDirectorInterventoria.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vDirectorInterventoria.CorreoElectronico;
                            vDirectorInterventoria.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDirectorInterventoria.UsuarioCrea;
                            vDirectorInterventoria.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDirectorInterventoria.FechaCrea;
                            vDirectorInterventoria.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDirectorInterventoria.UsuarioModifica;
                            vDirectorInterventoria.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDirectorInterventoria.FechaModifica;
                        }
                        return vDirectorInterventoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pTipoIdentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pPrimerNombre"></param>
        /// <param name="pSegundoNombre"></param>
        /// <param name="pPrimerApellido"></param>
        /// <param name="pSegundoApellido"></param>
        /// <param name="pCelular"></param>
        /// <param name="pTelefono"></param>
        /// <param name="pCorreoElectronico"></param>
        public List<DirectorInterventoria> ConsultarDirectorInterventorias(int? pIdTipoIdentificacion, String pNumeroIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido, String pCelular, String pTelefono, String pCorreoElectronico)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DirectorInterventorias_Consultar"))
                {
                    if (pIdTipoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pIdTipoIdentificacion);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    if (pPrimerNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pPrimerNombre);
                    if (pSegundoNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pSegundoNombre);
                    if (pPrimerApellido != null)
                        vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pPrimerApellido);
                    if (pSegundoApellido != null)
                        vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pSegundoApellido);
                    if (pCelular != null)
                        vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pCelular);
                    if (pTelefono != null)
                        vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pTelefono);
                    if (pCorreoElectronico != null)
                        vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pCorreoElectronico);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DirectorInterventoria> vListaDirectorInterventoria = new List<DirectorInterventoria>();
                        while (vDataReaderResults.Read())
                        {
                            DirectorInterventoria vDirectorInterventoria = new DirectorInterventoria();
                            vDirectorInterventoria.IDDirectorInterventoria = vDataReaderResults["IDDirectorInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDirectorInterventoria"].ToString()) : vDirectorInterventoria.IDDirectorInterventoria;
                            vDirectorInterventoria.IdTipoIdentificacion = vDataReaderResults["IdTipoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoIdentificacion"].ToString()) : vDirectorInterventoria.IdTipoIdentificacion;
                            vDirectorInterventoria.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vDirectorInterventoria.NumeroIdentificacion;
                            vDirectorInterventoria.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vDirectorInterventoria.PrimerNombre;
                            vDirectorInterventoria.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vDirectorInterventoria.SegundoNombre;
                            vDirectorInterventoria.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vDirectorInterventoria.PrimerApellido;
                            vDirectorInterventoria.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vDirectorInterventoria.SegundoApellido;
                            vDirectorInterventoria.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vDirectorInterventoria.Celular;
                            vDirectorInterventoria.Celular = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vDirectorInterventoria.Telefono;
                            vDirectorInterventoria.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vDirectorInterventoria.CorreoElectronico;
                            vDirectorInterventoria.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDirectorInterventoria.UsuarioCrea;
                            vDirectorInterventoria.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDirectorInterventoria.FechaCrea;
                            vDirectorInterventoria.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDirectorInterventoria.UsuarioModifica;
                            vDirectorInterventoria.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDirectorInterventoria.FechaModifica;
                            vListaDirectorInterventoria.Add(vDirectorInterventoria);
                        }
                        return vListaDirectorInterventoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
