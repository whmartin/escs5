﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    public class SuscripcionContratoDAL : GeneralDAL
    {
        public SuscripcionContratoDAL()
        {
        }

        /// <summary>
        /// Método de inserción para la entidad ArchivoContrato
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int InsertarArchivoContrato(ArchivoContrato pArchivoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ArchivosContratos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdArchivoContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Decimal, pArchivoContrato.IdArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pArchivoContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pArchivoContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pArchivoContrato.IdArchivoContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdArchivoContrato").ToString());
                    GenerarLogAuditoria(pArchivoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivo sólo por Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdContrato">Valor entero Id Contrato</param>
        /// <returns>Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivo(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ArchivosContratos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivoContrato> vListaArchivo = new List<ArchivoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            ArchivoContrato vArchivo = new ArchivoContrato();
                            vArchivo.IdArchivoContrato = vDataReaderResults["IdArchivoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArchivoContrato"].ToString()) : vArchivo.IdArchivoContrato;
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vArchivo.IdUsuario;
                            vArchivo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vArchivo.IdFormatoArchivo;
                            vArchivo.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vArchivo.FechaRegistro;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.ServidorFTP = vDataReaderResults["ServidorFTP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ServidorFTP"].ToString()) : vArchivo.ServidorFTP;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.ResumenCarga = vDataReaderResults["ResumenCarga"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenCarga"].ToString()) : vArchivo.ResumenCarga;
                            vArchivo.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vArchivo.NombreArchivoOri;

                            vArchivo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivo.UsuarioCrea;
                            vArchivo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivo.FechaCrea;
                            vArchivo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivo.UsuarioModifica;
                            vArchivo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivo.FechaModifica;
                            //vArchivo.NombreEstructura = vDataReaderResults["NombreEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstructura"].ToString()) : vArchivo.NombreEstructura;
                            //vArchivo.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vArchivo.NombreArchivoOri;
                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Permite eliminar un documento Archivo Contrato
        ///     -Genera auditoria
        /// 26-Jun-2013
        /// </summary>
        /// <param name="pArchivoContrato">Instancia que contiene la información del documento  
        /// a eliminar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int EliminarArchivoContrato(ArchivoContrato pArchivoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ArchivoContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivoContrato", DbType.Int32, pArchivoContrato.IdArchivoContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pArchivoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="vFechaSuscripcion"></param>
        /// <param name="vIdEstadoContrato"></param>
        public int SuscribirContrato(int pIdContrato, string pUsuarioModifica, DateTime vFechaSuscripcion, int vIdEstadoContrato, 
                                     string consecutivoContrato, DateTime ? fechaFinalizacion, string vinculoSECOP)
        {
            try
            {
                Entity.Contrato vContrato = new Entity.Contrato();
                vContrato.IdContrato = pIdContrato;
                vContrato.UsuarioModifica = pUsuarioModifica;
                vContrato.FechaSuscripcion = vFechaSuscripcion;
                vContrato.IdEstadoContrato = vIdEstadoContrato;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Contrato_SuscribirContrato"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcionContrato", DbType.DateTime, vFechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, vIdEstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@consecutivoContrato", DbType.String, consecutivoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@vinculoSECOP", DbType.String, vinculoSECOP);
                    if(fechaFinalizacion.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@FechaFinalizacion", DbType.DateTime, fechaFinalizacion.Value);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(vContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para actualizar el numero de contrato de la entidad contrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="pNumeroContrato"></param>
        public int ActualizarNumeroContrato(int pIdContrato, string pUsuarioModifica, string pNumeroContrato, string pConsecutivoSuscrito)
        {
            try
            {
                Entity.Contrato vContrato = new Entity.Contrato();
                vContrato.IdContrato = pIdContrato;
                vContrato.UsuarioModifica = pUsuarioModifica;
                vContrato.NumeroContrato = pNumeroContrato;
                vContrato.ConsecutivoSuscrito = pConsecutivoSuscrito;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Actualizar_NumeroContrato"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoSuscrito", DbType.String, pConsecutivoSuscrito);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUsuarioModifica);
                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(vContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar Archivo sólo por Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdVigencia">Valor entero Id Contrato</param>
        /// <param name="pIdRegional">Valor entero Id Regional</param>
        /// <returns>Entity Archivo</returns>
        public List<ConsecutivoContratoRegionales> ConsultarConsecutivoContratoRegionales(int pIdVigencia, int pIdRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsecutivoContratoRegionalesVigArea_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConsecutivoContratoRegionales> vLConsecutivoContratoRegionales = new List<ConsecutivoContratoRegionales>();
                        while (vDataReaderResults.Read())
                        {
                            ConsecutivoContratoRegionales vConsecutivoContratoRegionales = new ConsecutivoContratoRegionales();
                            vConsecutivoContratoRegionales.IDConsecutivoContratoRegional = vDataReaderResults["IDConsecutivoContratoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDConsecutivoContratoRegional"].ToString()) : vConsecutivoContratoRegionales.IDConsecutivoContratoRegional;
                            vConsecutivoContratoRegionales.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vConsecutivoContratoRegionales.IdRegional;
                            vConsecutivoContratoRegionales.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vConsecutivoContratoRegionales.IdVigencia;
                            vConsecutivoContratoRegionales.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Consecutivo"].ToString()) : vConsecutivoContratoRegionales.Consecutivo;
                            vConsecutivoContratoRegionales.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsecutivoContratoRegionales.UsuarioCrea;
                            vConsecutivoContratoRegionales.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsecutivoContratoRegionales.FechaCrea;
                            vConsecutivoContratoRegionales.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsecutivoContratoRegionales.UsuarioModifica;
                            vConsecutivoContratoRegionales.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsecutivoContratoRegionales.FechaModifica;
                            vLConsecutivoContratoRegionales.Add(vConsecutivoContratoRegionales);
                        }
                        return vLConsecutivoContratoRegionales;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }




    }
}
