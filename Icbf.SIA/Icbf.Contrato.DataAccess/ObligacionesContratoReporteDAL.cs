using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad ObligacionesContratoReporte
    /// </summary>
    public class ObligacionesContratoReporteDAL : GeneralDAL
    {
        public ObligacionesContratoReporteDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int InsertarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ObligacionesContratoReporte_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDObligacionContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, pObligacionesContratoReporte.IDContrato);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionObligacion", DbType.String, pObligacionesContratoReporte.DescripcionObligacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IdObligacionSigepcyp", DbType.Int32, pObligacionesContratoReporte.IDObligacionSigepcyp);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pObligacionesContratoReporte.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pObligacionesContratoReporte.IDObligacionContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDObligacionContrato").ToString());
                    GenerarLogAuditoria(pObligacionesContratoReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int ModificarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ObligacionesContratoReporte_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDObligacionContrato", DbType.Int32, pObligacionesContratoReporte.IDObligacionContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, pObligacionesContratoReporte.IDContrato);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionObligacion", DbType.String, pObligacionesContratoReporte.DescripcionObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pObligacionesContratoReporte.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObligacionesContratoReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int EliminarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ObligacionesContratoReporte_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDObligacionContrato", DbType.Int32, pObligacionesContratoReporte.IDObligacionContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObligacionesContratoReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pIDObligacionContrato"></param>
        public ObligacionesContratoReporte ConsultarObligacionesContratoReporte(int pIDObligacionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ObligacionesContratoReporte_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDObligacionContrato", DbType.Int32, pIDObligacionContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ObligacionesContratoReporte vObligacionesContratoReporte = new ObligacionesContratoReporte();
                        while (vDataReaderResults.Read())
                        {
                            vObligacionesContratoReporte.IDObligacionContrato = vDataReaderResults["IDObligacionContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDObligacionContrato"].ToString()) : vObligacionesContratoReporte.IDObligacionContrato;
                            vObligacionesContratoReporte.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vObligacionesContratoReporte.IDContrato;
                            vObligacionesContratoReporte.DescripcionObligacion = vDataReaderResults["DescripcionObligacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionObligacion"].ToString()) : vObligacionesContratoReporte.DescripcionObligacion;
                            vObligacionesContratoReporte.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObligacionesContratoReporte.UsuarioCrea;
                            vObligacionesContratoReporte.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObligacionesContratoReporte.FechaCrea;
                            vObligacionesContratoReporte.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObligacionesContratoReporte.UsuarioModifica;
                            vObligacionesContratoReporte.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObligacionesContratoReporte.FechaModifica;
                        }
                        return vObligacionesContratoReporte;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pIDContrato"></param>
        /// <param name="pDescripcionObligacion"></param>
        public List<ObligacionesContratoReporte> ConsultarObligacionesContratoReportes(int? pIDContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ObligacionesContratoReportes_Consultar"))
                {
                    if(pIDContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, pIDContrato);
                  
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObligacionesContratoReporte> vListaObligacionesContratoReporte = new List<ObligacionesContratoReporte>();
                        while (vDataReaderResults.Read())
                        {
                                ObligacionesContratoReporte vObligacionesContratoReporte = new ObligacionesContratoReporte();
                            vObligacionesContratoReporte.IDObligacionContrato = vDataReaderResults["IDObligacionContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDObligacionContrato"].ToString()) : vObligacionesContratoReporte.IDObligacionContrato;
                            vObligacionesContratoReporte.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vObligacionesContratoReporte.IDContrato;
                            vObligacionesContratoReporte.DescripcionObligacion = vDataReaderResults["DescripcionObligacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionObligacion"].ToString()) : vObligacionesContratoReporte.DescripcionObligacion;
                            vObligacionesContratoReporte.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObligacionesContratoReporte.UsuarioCrea;
                            vObligacionesContratoReporte.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObligacionesContratoReporte.FechaCrea;
                            vObligacionesContratoReporte.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObligacionesContratoReporte.UsuarioModifica;
                            vObligacionesContratoReporte.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObligacionesContratoReporte.FechaModifica;
                                vListaObligacionesContratoReporte.Add(vObligacionesContratoReporte);
                        }
                        return vListaObligacionesContratoReporte;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObligacionesContratoReporte> ConsultarObligacionesSigepcyp(int NumCDP,int NumDocIdentificacion, int AnioVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ObligacionesContratoSigepcyp_Obtener"))
                {
                    
                        vDataBase.AddInParameter(vDbCommand, "@CDPresupuestal", DbType.Int32, NumCDP);                    
                        vDataBase.AddInParameter(vDbCommand, "@NumIdentificacion", DbType.String, NumDocIdentificacion);
                        vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.String, AnioVigencia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObligacionesContratoReporte> vListaObligacionesContratoReporte = new List<ObligacionesContratoReporte>();
                        while (vDataReaderResults.Read())
                        {
                            ObligacionesContratoReporte vObligacionesContratoReporte = new ObligacionesContratoReporte();
                            vObligacionesContratoReporte.DescripcionObligacion = vDataReaderResults["ObligacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObligacionContractual"].ToString()) : vObligacionesContratoReporte.DescripcionObligacion;
                            vObligacionesContratoReporte.IDObligacionSigepcyp = vDataReaderResults["IdObligacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObligacionContractual"].ToString()) : vObligacionesContratoReporte.IDObligacionSigepcyp;
                            vListaObligacionesContratoReporte.Add(vObligacionesContratoReporte);
                        }
                        return vListaObligacionesContratoReporte;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
