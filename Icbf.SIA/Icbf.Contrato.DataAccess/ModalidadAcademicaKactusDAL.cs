﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar 
    /// registros, entre otros para la entidad  Modalidad Academica Kactus
    /// </summary>
    public class ModalidadAcademicaKactusDAL : GeneralDAL
    {
        public ModalidadAcademicaKactusDAL()
        {
        }

        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo de consulta por filtros para la entidad ModalidadAcademicaKactus
        /// </summary>
        /// <param name="pNombre">Cadena de texto con el nombre</param>
        /// <param name="pId">Cadena de texto con el identificador</param>
        /// <param name="pEstado">Booleano con el estado</param>
        /// <returns>Lista con las modalidades que coinciden con los filtros dados</returns>
        public List<ModalidadAcademicaKactus> ConsultarModalidadesAcademicasKactus(String pNombre, String pId, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_KACTUS_KPRODII_Contrato_ModalidadesAcademicas_Consultar"))
                {
                    if (pNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pId != null)
                        vDataBase.AddInParameter(vDbCommand, "@Id", DbType.String, pId);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ModalidadAcademicaKactus> vListaModalidadAcademicaKactus = new List<ModalidadAcademicaKactus>();
                        while (vDataReaderResults.Read())
                        {
                            ModalidadAcademicaKactus vModalidadAcademicaKactus = new ModalidadAcademicaKactus();
                            vModalidadAcademicaKactus.Id = vDataReaderResults["Id"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Id"].ToString()) : vModalidadAcademicaKactus.Id;
                            vModalidadAcademicaKactus.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vModalidadAcademicaKactus.Codigo;
                            vModalidadAcademicaKactus.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vModalidadAcademicaKactus.Descripcion;
                            //vModalidadAcademicaKactus.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadSeleccion.UsuarioCrea;
                            //vModalidadAcademicaKactus.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadSeleccion.FechaCrea;
                            //vModalidadAcademicaKactus.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadSeleccion.UsuarioModifica;
                            //vModalidadAcademicaKactus.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadSeleccion.FechaModifica;
                            vListaModalidadAcademicaKactus.Add(vModalidadAcademicaKactus);
                        }
                        return vListaModalidadAcademicaKactus;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }

}
