using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class PreguntaDAL : GeneralDAL
    {
        public PreguntaDAL()
        {
        }
        public int InsertarPregunta(Pregunta pPregunta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Pregunta_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPregunta", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionPregunta", DbType.String, pPregunta.DescripcionPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pPregunta.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pPregunta.IdComponente);
                    vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pPregunta.IdSubComponente);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pPregunta.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereDocumento", DbType.Int32, pPregunta.RequiereDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pPregunta.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPregunta.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPregunta.IdPregunta = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPregunta").ToString());
                    GenerarLogAuditoria(pPregunta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarPregunta(Pregunta pPregunta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Pregunta_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pPregunta.IdPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionPregunta", DbType.String, pPregunta.DescripcionPregunta);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pPregunta.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pPregunta.IdComponente);
                    vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pPregunta.IdSubComponente);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pPregunta.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereDocumento", DbType.Int32, pPregunta.RequiereDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pPregunta.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPregunta.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPregunta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarPregunta(Pregunta pPregunta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Pregunta_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pPregunta.IdPregunta);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPregunta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Pregunta ConsultarPregunta(int pIdPregunta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Pregunta_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPregunta", DbType.Int32, pIdPregunta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Pregunta vPregunta = new Pregunta();
                        while (vDataReaderResults.Read())
                        {
                            vPregunta.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vPregunta.IdPregunta;
                            vPregunta.DescripcionPregunta = vDataReaderResults["DescripcionPregunta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionPregunta"].ToString()) : vPregunta.DescripcionPregunta;
                            vPregunta.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vPregunta.IdTipoContrato;
                            vPregunta.IdComponente = vDataReaderResults["IdComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComponente"].ToString()) : vPregunta.IdComponente;
                            vPregunta.IdSubComponente = vDataReaderResults["IdSubComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubComponente"].ToString()) : vPregunta.IdSubComponente;
                            vPregunta.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vPregunta.IdCategoriaContrato;
                            vPregunta.RequiereDocumento = vDataReaderResults["RequiereDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RequiereDocumento"].ToString()) : vPregunta.RequiereDocumento;
                            vPregunta.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vPregunta.Estado;
                            vPregunta.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPregunta.UsuarioCrea;
                            vPregunta.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPregunta.FechaCrea;
                            vPregunta.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPregunta.UsuarioModifica;
                            vPregunta.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPregunta.FechaModifica;
                        }
                        return vPregunta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Pregunta> ConsultarPreguntas(String pDescripcionPregunta, int? pIdTipoContrato, int? pIdComponente, int? pIdSubComponente, int? pIdCategoriaContrato, int? pRequiereDocumento, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Preguntas_Consultar"))
                {
                    if(pDescripcionPregunta != null)
                         vDataBase.AddInParameter(vDbCommand, "@DescripcionPregunta", DbType.String, pDescripcionPregunta);
                    if(pIdTipoContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pIdTipoContrato);
                    if(pIdComponente != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pIdComponente);
                    if(pIdSubComponente != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pIdSubComponente);
                    if(pIdCategoriaContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pIdCategoriaContrato);
                    if(pRequiereDocumento != null)
                         vDataBase.AddInParameter(vDbCommand, "@RequiereDocumento", DbType.Int32, pRequiereDocumento);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Pregunta> vListaPregunta = new List<Pregunta>();
                        while (vDataReaderResults.Read())
                        {
                                Pregunta vPregunta = new Pregunta();
                            vPregunta.IdPregunta = vDataReaderResults["IdPregunta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPregunta"].ToString()) : vPregunta.IdPregunta;
                            vPregunta.DescripcionPregunta = vDataReaderResults["DescripcionPregunta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionPregunta"].ToString()) : vPregunta.DescripcionPregunta;
                            vPregunta.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vPregunta.IdTipoContrato;
                            vPregunta.IdComponente = vDataReaderResults["IdComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComponente"].ToString()) : vPregunta.IdComponente;
                            vPregunta.IdSubComponente = vDataReaderResults["IdSubComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubComponente"].ToString()) : vPregunta.IdSubComponente;
                            vPregunta.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vPregunta.IdCategoriaContrato;
                            vPregunta.RequiereDocumento = vDataReaderResults["RequiereDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RequiereDocumento"].ToString()) : vPregunta.RequiereDocumento;
                            vPregunta.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vPregunta.Estado;
                            vPregunta.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPregunta.UsuarioCrea;
                            vPregunta.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPregunta.FechaCrea;
                            vPregunta.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPregunta.UsuarioModifica;
                            vPregunta.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPregunta.FechaModifica;
                                vListaPregunta.Add(vPregunta);
                        }
                        return vListaPregunta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
