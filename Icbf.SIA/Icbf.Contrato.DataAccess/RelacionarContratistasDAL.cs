using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Relación Contratistas
    /// </summary>
    public class RelacionarContratistasDAL : GeneralDAL
    {
        public RelacionarContratistasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int InsertarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarContratistas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContratistaContrato", DbType.Int32, pRelacionarContratistas.IdContratistaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pRelacionarContratistas.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.Int64, pRelacionarContratistas.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseEntidad", DbType.String, pRelacionarContratistas.ClaseEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeParticipacion", DbType.Int32, pRelacionarContratistas.PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoIntegrante", DbType.Boolean, pRelacionarContratistas.EstadoIntegrante);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionRepresentanteLegal", DbType.Int64, pRelacionarContratistas.NumeroIdentificacionRepresentanteLegal);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRelacionarContratistas.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRelacionarContratistas.IdContratistaContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContratistaContrato").ToString());
                    GenerarLogAuditoria(pRelacionarContratistas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (SqlException ex)
            {
                if (ex.Number == 50000)
                    throw new GenericException("El Porcentaje de Participación supera el 100%.");
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int ModificarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarContratistas_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratistaContrato", DbType.Int32, pRelacionarContratistas.IdContratistaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pRelacionarContratistas.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.Int64, pRelacionarContratistas.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseEntidad", DbType.String, pRelacionarContratistas.ClaseEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeParticipacion", DbType.Int32, pRelacionarContratistas.PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoIntegrante", DbType.Boolean, pRelacionarContratistas.EstadoIntegrante);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionRepresentanteLegal", DbType.Int64, pRelacionarContratistas.NumeroIdentificacionRepresentanteLegal);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRelacionarContratistas.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRelacionarContratistas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int EliminarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarContratistas_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratistaContrato", DbType.Int32, pRelacionarContratistas.IdContratistaContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRelacionarContratistas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pIdContratistaContrato"></param>
        /// <returns></returns>
        public RelacionarContratistas ConsultarRelacionarContratistas(int pIdContratistaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarContratistas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratistaContrato", DbType.Int32, pIdContratistaContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RelacionarContratistas vRelacionarContratistas = new RelacionarContratistas();
                        while (vDataReaderResults.Read())
                        {
                            vRelacionarContratistas.IdContratistaContrato = vDataReaderResults["IdContratistaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratistaContrato"].ToString()) : vRelacionarContratistas.IdContratistaContrato;
                            vRelacionarContratistas.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vRelacionarContratistas.IdContrato;
                            vRelacionarContratistas.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["NumeroIdentificacion"].ToString()) : vRelacionarContratistas.NumeroIdentificacion;
                            vRelacionarContratistas.ClaseEntidad = vDataReaderResults["ClaseEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntidad"].ToString()) : vRelacionarContratistas.ClaseEntidad;
                            vRelacionarContratistas.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vRelacionarContratistas.PorcentajeParticipacion;
                            vRelacionarContratistas.EstadoIntegrante = vDataReaderResults["EstadoIntegrante"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EstadoIntegrante"].ToString()) : vRelacionarContratistas.EstadoIntegrante;
                            vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal = vDataReaderResults["NumeroIdentificacionRepresentanteLegal"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["NumeroIdentificacionRepresentanteLegal"].ToString()) : vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal;
                            vRelacionarContratistas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRelacionarContratistas.UsuarioCrea;
                            vRelacionarContratistas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRelacionarContratistas.FechaCrea;
                            vRelacionarContratistas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRelacionarContratistas.UsuarioModifica;
                            vRelacionarContratistas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRelacionarContratistas.FechaModifica;
                        }
                        return vRelacionarContratistas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pIdContratistaContrato"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pClaseEntidad"></param>
        /// <param name="pPorcentajeParticipacion"></param>
        /// <param name="pEstadoIntegrante"></param>
        /// <param name="pNumeroIdentificacionRepresentanteLegal"></param>
        /// <returns></returns>
        public List<RelacionarContratistas> ConsultarRelacionarContratistass(int? pIdContratistaContrato, int? pIdContrato, long pNumeroIdentificacion, String pClaseEntidad, int? pPorcentajeParticipacion, Boolean pEstadoIntegrante, long pNumeroIdentificacionRepresentanteLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarContratistass_Consultar"))
                {
                    if(pIdContratistaContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdContratistaContrato", DbType.Int32, pIdContratistaContrato);
                    if(pIdContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pNumeroIdentificacion != 0)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.Int64, pNumeroIdentificacion);
                    if(pClaseEntidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@ClaseEntidad", DbType.String, pClaseEntidad);
                    if(pPorcentajeParticipacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@PorcentajeParticipacion", DbType.Int32, pPorcentajeParticipacion);
                    if (pEstadoIntegrante != null)
                        vDataBase.AddInParameter(vDbCommand, "@EstadoIntegrante", DbType.Boolean, pEstadoIntegrante);
                    if (pNumeroIdentificacionRepresentanteLegal != 0)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionRepresentanteLegal", DbType.Int64, pNumeroIdentificacionRepresentanteLegal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RelacionarContratistas> vListaRelacionarContratistas = new List<RelacionarContratistas>();
                        while (vDataReaderResults.Read())
                        {
                                RelacionarContratistas vRelacionarContratistas = new RelacionarContratistas();
                            vRelacionarContratistas.IdContratistaContrato = vDataReaderResults["IdContratistaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratistaContrato"].ToString()) : vRelacionarContratistas.IdContratistaContrato;
                            vRelacionarContratistas.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vRelacionarContratistas.IdContrato;
                            vRelacionarContratistas.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["NumeroIdentificacion"].ToString()) : vRelacionarContratistas.NumeroIdentificacion;
                            vRelacionarContratistas.ClaseEntidad = vDataReaderResults["ClaseEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntidad"].ToString()) : vRelacionarContratistas.ClaseEntidad;
                            vRelacionarContratistas.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vRelacionarContratistas.PorcentajeParticipacion;
                            vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal = vDataReaderResults["NumeroIdentificacionRepresentanteLegal"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["NumeroIdentificacionRepresentanteLegal"].ToString()) : vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal;
                            vRelacionarContratistas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRelacionarContratistas.UsuarioCrea;
                            vRelacionarContratistas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRelacionarContratistas.FechaCrea;
                            vRelacionarContratistas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRelacionarContratistas.UsuarioModifica;
                            vRelacionarContratistas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRelacionarContratistas.FechaModifica;
                            vRelacionarContratistas.EstadoIntegrante = Convert.ToBoolean(vDataReaderResults["EstadoIntegrante"].ToString());
                            
                            vListaRelacionarContratistas.Add(vRelacionarContratistas);
                        }
                        return vListaRelacionarContratistas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
