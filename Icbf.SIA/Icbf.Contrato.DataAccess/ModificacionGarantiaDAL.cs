using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class ModificacionGarantiaDAL : GeneralDAL
    {
        public ModificacionGarantiaDAL()
        {
        }
        public int InsertarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ModificacionGarantia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdModificacionGarantia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pModificacionGarantia.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.Int32, pModificacionGarantia.NumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pModificacionGarantia.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@TipoModificacion", DbType.String, pModificacionGarantia.TipoModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pModificacionGarantia.NumeroGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IdAmparo", DbType.Int32, pModificacionGarantia.IdAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@TipoAmparo", DbType.String, pModificacionGarantia.TipoAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaDesde", DbType.DateTime, pModificacionGarantia.FechaVigenciaDesde);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaHasta", DbType.DateTime, pModificacionGarantia.FechaVigenciaHasta);
                    vDataBase.AddInParameter(vDbCommand, "@CalculoValorAsegurado", DbType.Decimal, pModificacionGarantia.CalculoValorAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@TipoCalculo", DbType.String, pModificacionGarantia.TipoCalculo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pModificacionGarantia.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalReduccion", DbType.Decimal, pModificacionGarantia.ValorTotalReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAsegurado", DbType.Decimal, pModificacionGarantia.ValorAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesModificacion", DbType.String, pModificacionGarantia.ObservacionesModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pModificacionGarantia.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pModificacionGarantia.IdModificacionGarantia = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdModificacionGarantia").ToString());
                    GenerarLogAuditoria(pModificacionGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ModificacionGarantia_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModificacionGarantia", DbType.Int32, pModificacionGarantia.IdModificacionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pModificacionGarantia.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.Int32, pModificacionGarantia.NumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pModificacionGarantia.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@TipoModificacion", DbType.String, pModificacionGarantia.TipoModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pModificacionGarantia.NumeroGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IdAmparo", DbType.Int32, pModificacionGarantia.IdAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@TipoAmparo", DbType.String, pModificacionGarantia.TipoAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaDesde", DbType.DateTime, pModificacionGarantia.FechaVigenciaDesde);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaHasta", DbType.DateTime, pModificacionGarantia.FechaVigenciaHasta);
                    vDataBase.AddInParameter(vDbCommand, "@CalculoValorAsegurado", DbType.Decimal, pModificacionGarantia.CalculoValorAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@TipoCalculo", DbType.String, pModificacionGarantia.TipoCalculo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pModificacionGarantia.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalReduccion", DbType.Decimal, pModificacionGarantia.ValorTotalReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAsegurado", DbType.Decimal, pModificacionGarantia.ValorAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesModificacion", DbType.String, pModificacionGarantia.ObservacionesModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pModificacionGarantia.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModificacionGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ModificacionGarantia_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModificacionGarantia", DbType.Int32, pModificacionGarantia.IdModificacionGarantia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModificacionGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public ModificacionGarantia ConsultarModificacionGarantia(int pIdModificacionGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ModificacionGarantia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModificacionGarantia", DbType.Int32, pIdModificacionGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ModificacionGarantia vModificacionGarantia = new ModificacionGarantia();
                        while (vDataReaderResults.Read())
                        {
                            vModificacionGarantia.IdModificacionGarantia = vDataReaderResults["IdModificacionGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModificacionGarantia"].ToString()) : vModificacionGarantia.IdModificacionGarantia;
                            vModificacionGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vModificacionGarantia.IdContrato;
                            vModificacionGarantia.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroDocumento"].ToString()) : vModificacionGarantia.NumeroDocumento;
                            vModificacionGarantia.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vModificacionGarantia.FechaRegistro;
                            vModificacionGarantia.TipoModificacion = vDataReaderResults["TipoModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacion"].ToString()) : vModificacionGarantia.TipoModificacion;
                            vModificacionGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vModificacionGarantia.NumeroGarantia;
                            vModificacionGarantia.IdAmparo = vDataReaderResults["IdAmparo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAmparo"].ToString()) : vModificacionGarantia.IdAmparo;
                            vModificacionGarantia.TipoAmparo = vDataReaderResults["TipoAmparo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoAmparo"].ToString()) : vModificacionGarantia.TipoAmparo;
                            vModificacionGarantia.FechaVigenciaDesde = vDataReaderResults["FechaVigenciaDesde"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaDesde"].ToString()) : vModificacionGarantia.FechaVigenciaDesde;
                            vModificacionGarantia.FechaVigenciaHasta = vDataReaderResults["FechaVigenciaHasta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaHasta"].ToString()) : vModificacionGarantia.FechaVigenciaHasta;
                            vModificacionGarantia.CalculoValorAsegurado = vDataReaderResults["CalculoValorAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CalculoValorAsegurado"].ToString()) : vModificacionGarantia.CalculoValorAsegurado;
                            vModificacionGarantia.TipoCalculo = vDataReaderResults["TipoCalculo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCalculo"].ToString()) : vModificacionGarantia.TipoCalculo;
                            vModificacionGarantia.ValorAdicion = vDataReaderResults["ValorAdicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicion"].ToString()) : vModificacionGarantia.ValorAdicion;
                            vModificacionGarantia.ValorTotalReduccion = vDataReaderResults["ValorTotalReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalReduccion"].ToString()) : vModificacionGarantia.ValorTotalReduccion;
                            vModificacionGarantia.ValorAsegurado = vDataReaderResults["ValorAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAsegurado"].ToString()) : vModificacionGarantia.ValorAsegurado;
                            vModificacionGarantia.ObservacionesModificacion = vDataReaderResults["ObservacionesModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesModificacion"].ToString()) : vModificacionGarantia.ObservacionesModificacion;
                            vModificacionGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModificacionGarantia.UsuarioCrea;
                            vModificacionGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModificacionGarantia.FechaCrea;
                            vModificacionGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModificacionGarantia.UsuarioModifica;
                            vModificacionGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModificacionGarantia.FechaModifica;
                        }
                        return vModificacionGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModificacionGarantia> ConsultarModificacionGarantias(int? pIdContrato, int? pNumeroDocumento, DateTime? pFechaRegistro, String pTipoModificacion, String pNumeroGarantia, int? pIdAmparo, String pTipoAmparo, DateTime? pFechaVigenciaDesde, DateTime? pFechaVigenciaHasta, Decimal? pCalculoValorAsegurado, String pTipoCalculo, Decimal? pValorAdicion, Decimal? pValorTotalReduccion, Decimal? pValorAsegurado, String pObservacionesModificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ModificacionGarantias_Consultar"))
                {
                    if(pIdContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if(pNumeroDocumento != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.Int32, pNumeroDocumento);
                    if(pFechaRegistro != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pFechaRegistro);
                    if(pTipoModificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@TipoModificacion", DbType.String, pTipoModificacion);
                    if(pNumeroGarantia != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pNumeroGarantia);
                    if(pIdAmparo != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdAmparo", DbType.Int32, pIdAmparo);
                    if(pTipoAmparo != null)
                         vDataBase.AddInParameter(vDbCommand, "@TipoAmparo", DbType.String, pTipoAmparo);
                    if(pFechaVigenciaDesde != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaDesde", DbType.DateTime, pFechaVigenciaDesde);
                    if(pFechaVigenciaHasta != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaVigenciaHasta", DbType.DateTime, pFechaVigenciaHasta);
                    if(pCalculoValorAsegurado != null)
                        vDataBase.AddInParameter(vDbCommand, "@CalculoValorAsegurado", DbType.Decimal, pCalculoValorAsegurado);
                    if(pTipoCalculo != null)
                         vDataBase.AddInParameter(vDbCommand, "@TipoCalculo", DbType.String, pTipoCalculo);
                    if(pValorAdicion != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pValorAdicion);
                    if(pValorTotalReduccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalReduccion", DbType.Decimal, pValorTotalReduccion);
                    if(pValorAsegurado != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorAsegurado", DbType.Decimal, pValorAsegurado);
                    if(pObservacionesModificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@ObservacionesModificacion", DbType.String, pObservacionesModificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ModificacionGarantia> vListaModificacionGarantia = new List<ModificacionGarantia>();
                        while (vDataReaderResults.Read())
                        {
                                ModificacionGarantia vModificacionGarantia = new ModificacionGarantia();
                            vModificacionGarantia.IdModificacionGarantia = vDataReaderResults["IdModificacionGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModificacionGarantia"].ToString()) : vModificacionGarantia.IdModificacionGarantia;
                            vModificacionGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vModificacionGarantia.IdContrato;
                            vModificacionGarantia.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroDocumento"].ToString()) : vModificacionGarantia.NumeroDocumento;
                            vModificacionGarantia.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vModificacionGarantia.FechaRegistro;
                            vModificacionGarantia.TipoModificacion = vDataReaderResults["TipoModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacion"].ToString()) : vModificacionGarantia.TipoModificacion;
                            vModificacionGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vModificacionGarantia.NumeroGarantia;
                            vModificacionGarantia.IdAmparo = vDataReaderResults["IdAmparo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAmparo"].ToString()) : vModificacionGarantia.IdAmparo;
                            vModificacionGarantia.TipoAmparo = vDataReaderResults["TipoAmparo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoAmparo"].ToString()) : vModificacionGarantia.TipoAmparo;
                            vModificacionGarantia.FechaVigenciaDesde = vDataReaderResults["FechaVigenciaDesde"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaDesde"].ToString()) : vModificacionGarantia.FechaVigenciaDesde;
                            vModificacionGarantia.FechaVigenciaHasta = vDataReaderResults["FechaVigenciaHasta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigenciaHasta"].ToString()) : vModificacionGarantia.FechaVigenciaHasta;
                            vModificacionGarantia.CalculoValorAsegurado = vDataReaderResults["CalculoValorAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CalculoValorAsegurado"].ToString()) : vModificacionGarantia.CalculoValorAsegurado;
                            vModificacionGarantia.TipoCalculo = vDataReaderResults["TipoCalculo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCalculo"].ToString()) : vModificacionGarantia.TipoCalculo;
                            vModificacionGarantia.ValorAdicion = vDataReaderResults["ValorAdicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicion"].ToString()) : vModificacionGarantia.ValorAdicion;
                            vModificacionGarantia.ValorTotalReduccion = vDataReaderResults["ValorTotalReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalReduccion"].ToString()) : vModificacionGarantia.ValorTotalReduccion;
                            vModificacionGarantia.ValorAsegurado = vDataReaderResults["ValorAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAsegurado"].ToString()) : vModificacionGarantia.ValorAsegurado;
                            vModificacionGarantia.ObservacionesModificacion = vDataReaderResults["ObservacionesModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesModificacion"].ToString()) : vModificacionGarantia.ObservacionesModificacion;
                            vModificacionGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModificacionGarantia.UsuarioCrea;
                            vModificacionGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModificacionGarantia.FechaCrea;
                            vModificacionGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModificacionGarantia.UsuarioModifica;
                            vModificacionGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModificacionGarantia.FechaModifica;
                                vListaModificacionGarantia.Add(vModificacionGarantia);
                        }
                        return vListaModificacionGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
