﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad SupervisorInterventor
    /// </summary>
    public class SupervisorInterventorDAL : GeneralDAL
    {
        public SupervisorInterventorDAL()
        {
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterventor
        /// </summary>
        /// <param name="pIDTipoSupervisorInterventor"></param>
        
        public TipoSupervisorInterventor ConsultarSupervisorInterventorsID(int pIDTipoSupervisorInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoSupervisorInterventors_ConsultarID"))
                {
                   vDataBase.AddInParameter(vDbCommand, "@pIDTipoSupervisorInterventor", DbType.String, pIDTipoSupervisorInterventor);
                   
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoSupervisorInterventor vSupervisorInterventor = new TipoSupervisorInterventor();
                        while (vDataReaderResults.Read())
                        {
                        
                            vSupervisorInterventor.IDSupervisorInterventor = vDataReaderResults["IDTipoSupervisorInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSupervisorInterventor"].ToString()) : vSupervisorInterventor.IDSupervisorInterventor;
                            vSupervisorInterventor.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vSupervisorInterventor.Codigo;
                            vSupervisorInterventor.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vSupervisorInterventor.Descripcion;
                            vSupervisorInterventor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterventor.UsuarioCrea;
                            vSupervisorInterventor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterventor.FechaCrea;
                            vSupervisorInterventor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterventor.UsuarioModifica;
                            vSupervisorInterventor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterventor.FechaModifica;
                            
                        }
                        return vSupervisorInterventor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterventor
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pDescripcion"></param>
        public List<TipoSupervisorInterventor> ConsultarSupervisorInterventors(String pCodigo, String pDescripcion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoSupervisorInterventors_Consultar"))
                {
                    if (pCodigo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pCodigo);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoSupervisorInterventor> vListaSupervisorInterventor = new List<TipoSupervisorInterventor>();
                        while (vDataReaderResults.Read())
                        {
                            TipoSupervisorInterventor vSupervisorInterventor = new TipoSupervisorInterventor();
                            vSupervisorInterventor.IDSupervisorInterventor = vDataReaderResults["IDTipoSupervisorInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSupervisorInterventor"].ToString()) : vSupervisorInterventor.IDSupervisorInterventor;
                            vSupervisorInterventor.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vSupervisorInterventor.Codigo;
                            vSupervisorInterventor.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vSupervisorInterventor.Descripcion;
                            vSupervisorInterventor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterventor.UsuarioCrea;
                            vSupervisorInterventor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterventor.FechaCrea;
                            vSupervisorInterventor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterventor.UsuarioModifica;
                            vSupervisorInterventor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterventor.FechaModifica;
                            vListaSupervisorInterventor.Add(vSupervisorInterventor);
                        }
                        return vListaSupervisorInterventor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Interventor> ConsultarInterventors(int? pIdTipoPersona, int? pIdTipoIdentificacion, String pNumeroIdentificacion, String pNombreRazonSocial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Interventors_Consultar"))
                {
                    if (pIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIdTipoPersona);
                    if (pIdTipoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pIdTipoIdentificacion);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    if (pNombreRazonSocial != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreRazonSocial", DbType.String, pNombreRazonSocial);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Interventor> vListaInterventor = new List<Interventor>();
                        while (vDataReaderResults.Read())
                        {
                            Interventor vInterventor = new Interventor();
                            vInterventor.IdInterventor = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInterventor.IdInterventor;
                            vInterventor.TipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vInterventor.TipoPersona;
                            vInterventor.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vInterventor.TipoIdentificacion;
                            vInterventor.NumeroDocumento = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vInterventor.NumeroDocumento;
                            vInterventor.NombreRazonSocial = vDataReaderResults["NombreRazonsocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRazonsocial"].ToString()) : vInterventor.NombreRazonSocial;
                            vInterventor.PrimerNombre = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vInterventor.PrimerNombre;
                            vInterventor.SegundoNombre = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vInterventor.SegundoNombre;
                            vInterventor.PrimerApellido = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vInterventor.PrimerApellido;
                            vInterventor.SegundoApellido = vDataReaderResults["SEGUNDOAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDOAPELLIDO"].ToString()) : vInterventor.SegundoApellido;
                            vInterventor.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vInterventor.RazonSocial;
                            vInterventor.CorreoElectronico = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vInterventor.CorreoElectronico;
                            vInterventor.Direccion = vDataReaderResults["DireccionComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionComercial"].ToString()) : vInterventor.Direccion;
                            vInterventor.Telefono = vDataReaderResults["NumeroTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTelefono"].ToString()) : vInterventor.Telefono;
                            vListaInterventor.Add(vInterventor);
                        }
                        return vListaInterventor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Interventor ConsultarInterventor(int pIDSupervisorIntervContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Interventor_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pIDSupervisorIntervContrato);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        
                        while (vDataReaderResults.Read())
                        {
                            Interventor vInterventor = new Interventor();
                            vInterventor.IdInterventor = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInterventor.IdInterventor;
                            vInterventor.TipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vInterventor.TipoPersona;
                            vInterventor.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vInterventor.TipoIdentificacion;
                            vInterventor.NumeroDocumento = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vInterventor.NumeroDocumento;
                            vInterventor.NombreRazonSocial = vDataReaderResults["NombreRazonsocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRazonsocial"].ToString()) : vInterventor.NombreRazonSocial;
                            vInterventor.PrimerNombre = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vInterventor.PrimerNombre;
                            vInterventor.SegundoNombre = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vInterventor.SegundoNombre;
                            vInterventor.PrimerApellido = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vInterventor.PrimerApellido;
                            vInterventor.SegundoApellido = vDataReaderResults["SEGUNDOAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDOAPELLIDO"].ToString()) : vInterventor.SegundoApellido;
                            vInterventor.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vInterventor.RazonSocial;
                            vInterventor.CorreoElectronico = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vInterventor.CorreoElectronico;
                            vInterventor.Direccion = vDataReaderResults["DireccionComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionComercial"].ToString()) : vInterventor.Direccion;
                            vInterventor.Telefono = vDataReaderResults["NumeroTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTelefono"].ToString()) : vInterventor.Telefono;
                            return vInterventor;
                        }
                        return null;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Supervisor> ConsultarSupervisors(String pIdTipoidentificacion, String pNumeroIdentificacion, String pIdTipoVinculacion, String pIdRegional,
                                                    String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Supervisors_Consultar"))
                {
                    if (pIdTipoidentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoidentificacion", DbType.String, pIdTipoidentificacion);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    if (pIdTipoVinculacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoVinculacion", DbType.String, pIdTipoVinculacion);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, pIdRegional);
                    if (pPrimerNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pPrimerNombre);
                    if (pSegundoNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pSegundoNombre);
                    if (pPrimerApellido != null)
                        vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pPrimerApellido);
                    if (pSegundoApellido != null)
                        vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pSegundoApellido);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Supervisor> vListaSupervisor = new List<Supervisor>();
                        while (vDataReaderResults.Read())
                        {
                            Supervisor vSupervisor = new Supervisor();
                            vSupervisor.IdSupervisor = vDataReaderResults["IdEmpleado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleado"].ToString()) : vSupervisor.IdSupervisor;
                            vSupervisor.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisor.TipoIdentificacion;
                            vSupervisor.ID_Ident = vDataReaderResults["ID_Ident"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ID_Ident"].ToString()) : vSupervisor.ID_Ident;
                            vSupervisor.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vSupervisor.NumeroIdentificacion;
                            vSupervisor.TipoVinculacionContractual = vDataReaderResults["TipoVinculacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacion"].ToString()) : vSupervisor.TipoVinculacionContractual;
                            vSupervisor.Regional = vDataReaderResults["regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["regional"].ToString()) : vSupervisor.Regional;
                            vSupervisor.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisor.NombreCompleto;
                            vSupervisor.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vSupervisor.PrimerNombre;
                            vSupervisor.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vSupervisor.SegundoNombre;
                            vSupervisor.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vSupervisor.PrimerApellido;
                            vSupervisor.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vSupervisor.SegundoApellido;
                            vSupervisor.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vSupervisor.Dependencia;
                            vSupervisor.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vSupervisor.Cargo;
                            vSupervisor.Direccion = vDataReaderResults["direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["direccion"].ToString()) : vSupervisor.Direccion;
                            vSupervisor.Telefono = vDataReaderResults["telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["telefono"].ToString()) : vSupervisor.Telefono;
                            vSupervisor.CorreoElectronico = vDataReaderResults["CorreoE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoE"].ToString()) : vSupervisor.CorreoElectronico;
                            vListaSupervisor.Add(vSupervisor);
                        }
                        return vListaSupervisor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Supervisor ConsultarSupervisor(int pIDSupervisorIntervContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Supervisor_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.String, pIDSupervisorIntervContrato);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        
                        while (vDataReaderResults.Read())
                        {
                            Supervisor vSupervisor = new Supervisor();
                            vSupervisor.IdSupervisor = vDataReaderResults["IdEmpleado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleado"].ToString()) : vSupervisor.IdSupervisor;
                            vSupervisor.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisor.TipoIdentificacion;
                            vSupervisor.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vSupervisor.NumeroIdentificacion;
                            vSupervisor.TipoVinculacionContractual = vDataReaderResults["TipoVinculacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacion"].ToString()) : vSupervisor.TipoVinculacionContractual;
                            vSupervisor.Regional = vDataReaderResults["regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["regional"].ToString()) : vSupervisor.Regional;
                            vSupervisor.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisor.NombreCompleto;
                            vSupervisor.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vSupervisor.PrimerNombre;
                            vSupervisor.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vSupervisor.SegundoNombre;
                            vSupervisor.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vSupervisor.PrimerApellido;
                            vSupervisor.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vSupervisor.SegundoApellido;
                            vSupervisor.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vSupervisor.Dependencia;
                            vSupervisor.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vSupervisor.Cargo;
                            vSupervisor.Direccion = vDataReaderResults["direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["direccion"].ToString()) : vSupervisor.Direccion;
                            vSupervisor.Telefono = vDataReaderResults["telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["telefono"].ToString()) : vSupervisor.Telefono;
                            vSupervisor.CorreoElectronico = vDataReaderResults["CorreoE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoE"].ToString()) : vSupervisor.CorreoElectronico;
                            vSupervisor.NombreRol = vDataReaderResults["NombreRol"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRol"].ToString()) : vSupervisor.NombreRol;
                            return vSupervisor;
                        }
                        return null;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Supervisor ConsultarSupervisorTemporal(int pIDSupervisorIntervContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorTemporal_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.String, pIDSupervisorIntervContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        while (vDataReaderResults.Read())
                        {
                            Supervisor vSupervisor = new Supervisor();
                            vSupervisor.IdSupervisor = vDataReaderResults["IdEmpleado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleado"].ToString()) : vSupervisor.IdSupervisor;
                            vSupervisor.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisor.TipoIdentificacion;
                            vSupervisor.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vSupervisor.NumeroIdentificacion;
                            vSupervisor.TipoVinculacionContractual = vDataReaderResults["TipoVinculacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacion"].ToString()) : vSupervisor.TipoVinculacionContractual;
                            vSupervisor.Regional = vDataReaderResults["regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["regional"].ToString()) : vSupervisor.Regional;
                            vSupervisor.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisor.NombreCompleto;
                            vSupervisor.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vSupervisor.PrimerNombre;
                            vSupervisor.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vSupervisor.SegundoNombre;
                            vSupervisor.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vSupervisor.PrimerApellido;
                            vSupervisor.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vSupervisor.SegundoApellido;
                            vSupervisor.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vSupervisor.Dependencia;
                            vSupervisor.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vSupervisor.Cargo;
                            vSupervisor.Direccion = vDataReaderResults["direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["direccion"].ToString()) : vSupervisor.Direccion;
                            vSupervisor.Telefono = vDataReaderResults["telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["telefono"].ToString()) : vSupervisor.Telefono;
                            vSupervisor.CorreoElectronico = vDataReaderResults["CorreoE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoE"].ToString()) : vSupervisor.CorreoElectronico;
                            return vSupervisor;
                        }
                        return null;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarRolSuerpvisor(int idSupervisorInter, int idRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Supervisor_ActualizarRol"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSupervisorContrato", DbType.Int32, idSupervisorInter);
                    vDataBase.AddInParameter(vDbCommand, "@IdROL", DbType.Int32, idRol);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

