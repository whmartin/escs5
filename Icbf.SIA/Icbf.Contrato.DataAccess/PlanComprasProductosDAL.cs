using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad PlanComprasProductos
    /// </summary>
    public class PlanComprasProductosDAL : GeneralDAL
    {
        public PlanComprasProductosDAL()
        {
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// 

        public int InsertarProductoPlanComprasContrato(ProductoPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ProductoPlanCompraContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDProductoPlanCompraContrato", DbType.Int32, pPlanComprasProductos.IDProductoPlanCompraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDProducto", DbType.String, pPlanComprasProductos.IDProducto);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadCupos", DbType.Decimal, pPlanComprasProductos.CantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasProductos.IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorProducto", DbType.Decimal, pPlanComprasProductos.ValorProducto);
                    vDataBase.AddInParameter(vDbCommand, "@Tiempo", DbType.Decimal, pPlanComprasProductos.Tiempo);
                    vDataBase.AddInParameter(vDbCommand, "@UnidadTiempo", DbType.String, pPlanComprasProductos.UnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdUnidadTiempo", DbType.Int32, pPlanComprasProductos.IdUnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleObjeto", DbType.String, pPlanComprasProductos.IdDetalleObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@ReduccionCantidadCupos", DbType.Decimal, pPlanComprasProductos.ReduccionCantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pPlanComprasProductos.IdReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Decimal, pPlanComprasProductos.ValorReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IsReduccion", DbType.Boolean, pPlanComprasProductos.IsReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pPlanComprasProductos.IdAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pPlanComprasProductos.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IsAdicion", DbType.Boolean, pPlanComprasProductos.IsAdicion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasProductos, vDbCommand);
                    return vResultado;

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarProductoPlanComprasContrato(ProductoPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ProductoPlanCompraContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDProductoPlanCompraContrato", DbType.Int32, pPlanComprasProductos.IDProductoPlanCompraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDProducto", DbType.String, pPlanComprasProductos.IDProducto);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadCupos", DbType.Decimal, pPlanComprasProductos.CantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasProductos.IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorProducto", DbType.Decimal, pPlanComprasProductos.ValorProducto);
                    vDataBase.AddInParameter(vDbCommand, "@Tiempo", DbType.Decimal, pPlanComprasProductos.Tiempo);
                    vDataBase.AddInParameter(vDbCommand, "@UnidadTiempo", DbType.String, pPlanComprasProductos.UnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdUnidadTiempo", DbType.Int32, pPlanComprasProductos.IdUnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleObjeto", DbType.String, pPlanComprasProductos.IdDetalleObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@ReduccionCantidadCupos", DbType.Decimal, pPlanComprasProductos.ReduccionCantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pPlanComprasProductos.IdReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Decimal, pPlanComprasProductos.ValorReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IsReduccion", DbType.Boolean, pPlanComprasProductos.IsReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pPlanComprasProductos.IdAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pPlanComprasProductos.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IsAdicion", DbType.Boolean, pPlanComprasProductos.IsAdicion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasProductos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        

        public int InsertarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasProductos.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoProducto", DbType.String, pPlanComprasProductos.CodigoProducto);
                    vDataBase.AddInParameter(vDbCommand, "@NombreProducto", DbType.String, pPlanComprasProductos.NombreProducto);
                    vDataBase.AddInParameter(vDbCommand, "@TipoProducto", DbType.String, pPlanComprasProductos.TipoProducto);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadCupos", DbType.Decimal, pPlanComprasProductos.CantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@ValorUnitario", DbType.Decimal, pPlanComprasProductos.ValorUnitario);
                    vDataBase.AddInParameter(vDbCommand, "@Tiempo", DbType.Decimal, pPlanComprasProductos.Tiempo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotal", DbType.Decimal, pPlanComprasProductos.ValorTotal);
                    vDataBase.AddInParameter(vDbCommand, "@UnidadTiempo", DbType.String, pPlanComprasProductos.UnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@UnidadMedida", DbType.String, pPlanComprasProductos.UnidadMedida);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasProductos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int ModificarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_Modificar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDProductoPlanCompraContrato", DbType.Int32, pPlanComprasProductos.IdProductoPlanCompraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasProductos.IdPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoProducto", DbType.String, pPlanComprasProductos.CodigoProducto);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadCupos", DbType.Decimal, pPlanComprasProductos.CantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@ValorProducto", DbType.Decimal, pPlanComprasProductos.ValorUnitario);
                    vDataBase.AddInParameter(vDbCommand, "@Tiempo",DbType.Decimal, pPlanComprasProductos.Tiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdUnidadTiempo", DbType.Int32, Convert.ToInt32(pPlanComprasProductos.IdUnidadTiempo));
                    vDataBase.AddInParameter(vDbCommand, "@UnidadTiempo", DbType.String, pPlanComprasProductos.UnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlanComprasProductos.UsuarioModifica);
                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasProductos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int EliminarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_Eliminar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProductoPlanCompraContrato", DbType.Int32, pPlanComprasProductos.IdProductoPlanCompraContrato);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasProductos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubros(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_DetallePlanComprasProductosRubros_Insertar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasProductos.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pPlanComprasProductos.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pPlanComprasProductos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pPlanComprasProductos.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
   
                    var pDatosProductos = new SqlParameter("@ProductoPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosProductos
                    };
                    vDbCommand.Parameters.Add(pDatosProductos);

                    var pDatosRubros = new SqlParameter("@RubrosPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosRubros
                    };
                    vDbCommand.Parameters.Add(pDatosRubros);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubrosTotal(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_DetallePlanComprasProductosRubrosTotal_Insertar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasProductos.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pPlanComprasProductos.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pPlanComprasProductos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pPlanComprasProductos.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@valorTotal", DbType.Decimal, pPlanComprasProductos.ValorTotal);
                    vDataBase.AddInParameter(vDbCommand, "@esEdicion", DbType.Boolean, esEdicion);
                    vDataBase.AddInParameter(vDbCommand, "@idPlanCompras", DbType.Int32, pPlanComprasProductos.IdPlanDeComprasContratos);

                    var pDatosProductos = new SqlParameter("@ProductoPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosProductos
                    };
                    vDbCommand.Parameters.Add(pDatosProductos);

                    var pDatosRubros = new SqlParameter("@RubrosPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosRubros
                    };
                    vDbCommand.Parameters.Add(pDatosRubros);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <param name="esEdicion"></param>
        /// <returns></returns>
        public int InsertarDetallePlanComprasProductosRubrosConTotalPrecontractual(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_DetallePlanComprasProductosRubrosTotalPrecontractual_Insertar"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@IdPlanComprasContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasProductos.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pPlanComprasProductos.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pPlanComprasProductos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pPlanComprasProductos.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@valorTotal", DbType.Decimal, pPlanComprasProductos.ValorTotal);
                    vDataBase.AddInParameter(vDbCommand, "@esEdicion", DbType.Boolean, esEdicion);
                    vDataBase.AddInParameter(vDbCommand, "@idPlanCompras", DbType.Int32, pPlanComprasProductos.IdPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pPlanComprasProductos.Objeto);
                    vDataBase.AddInParameter(vDbCommand, "@Alcance", DbType.String, pPlanComprasProductos.Alcance);

                    var pDatosProductos = new SqlParameter("@ProductoPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosProductos
                    };
                    vDbCommand.Parameters.Add(pDatosProductos);

                    var pDatosRubros = new SqlParameter("@RubrosPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosRubros
                    };
                    vDbCommand.Parameters.Add(pDatosRubros);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPlanComprasProductos.IdPlanDeComprasContratos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPlanComprasContrato").ToString());

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubrosConTotalAdicion(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_DetallePlanComprasProductosRubrosTotalAdicion_Insertar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasProductos.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pPlanComprasProductos.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pPlanComprasProductos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pPlanComprasProductos.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@valorTotal", DbType.Decimal, pPlanComprasProductos.ValorTotal);
                    vDataBase.AddInParameter(vDbCommand, "@esEdicion", DbType.Boolean, esEdicion);
                    vDataBase.AddInParameter(vDbCommand, "@esAdicion", DbType.Boolean, pPlanComprasProductos.EsAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@idAdicion", DbType.Int32, pPlanComprasProductos.IdAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@idPlanCompras", DbType.Int32, pPlanComprasProductos.IdPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@idDetConsModContractual", DbType.Int32, pPlanComprasProductos.IdDetConsModContractual);


                    var pDatosProductos = new SqlParameter("@ProductoPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosProductos
                    };
                    vDbCommand.Parameters.Add(pDatosProductos);

                    var pDatosRubros = new SqlParameter("@RubrosPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosRubros
                    };
                    vDbCommand.Parameters.Add(pDatosRubros);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <param name="esEdicion"></param>
        /// <returns></returns>
        public int InsertarDetallePlanComprasProductosRubrosConTotalVigencias(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_DetallePlanComprasProductosRubrosTotalVigencia_Insertar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasProductos.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pPlanComprasProductos.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pPlanComprasProductos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pPlanComprasProductos.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@valorTotal", DbType.Decimal, pPlanComprasProductos.ValorTotal);
                    vDataBase.AddInParameter(vDbCommand, "@esEdicion", DbType.Boolean, esEdicion);
                    vDataBase.AddInParameter(vDbCommand, "@esVigenciaFutura", DbType.Boolean, pPlanComprasProductos.esVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@idVigenciaFutura", DbType.Int32, pPlanComprasProductos.IdVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@idPlanCompras", DbType.Int32, pPlanComprasProductos.IdPlanDeComprasContratos);
                   
                    var pDatosProductos = new SqlParameter("@ProductoPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosProductos
                    };
                    vDbCommand.Parameters.Add(pDatosProductos);

                    var pDatosRubros = new SqlParameter("@RubrosPlanCompraContratos", SqlDbType.Structured)
                    {
                        Value = pPlanComprasProductos.DtDatosRubros
                    };
                    vDbCommand.Parameters.Add(pDatosRubros);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPlanCompras"></param>
        /// <param name="valorPlanCompras"></param>
        /// <returns></returns>
        public int ActualizarPlanComprasTotal(int idPlanCompras, decimal valorPlanCompras)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasValorTotal_Actualizar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanCompras", DbType.Int32, idPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@valorTotal", DbType.Decimal, valorPlanCompras);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasProductos ConsultarPlanComprasProductos(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_Consultar"))
                {
                    if (pIdProductoPlanCompraContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProductoPlanCompraContrato", DbType.Int32, pIdProductoPlanCompraContrato);

                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vPlanComprasProductos = new PlanComprasProductos();
                        while (vDataReaderResults.Read())
                        {
                            vPlanComprasProductos.NumeroConsecutivoPlanCompras = vDataReaderResults["NumeroConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivoPlanCompras"].ToString()) : vPlanComprasProductos.NumeroConsecutivoPlanCompras;
                            vPlanComprasProductos.IdProductoPlanCompraContrato = vDataReaderResults["IDProductoPlanCompraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProductoPlanCompraContrato"].ToString()) : vPlanComprasProductos.IdProductoPlanCompraContrato;
                            vPlanComprasProductos.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vPlanComprasProductos.CodigoProducto;
                            vPlanComprasProductos.Tiempo = vDataReaderResults["Tiempo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Tiempo"].ToString()) : vPlanComprasProductos.Tiempo;
                            vPlanComprasProductos.ValorUnitario = vDataReaderResults["ValorProducto"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorProducto"].ToString()) : vPlanComprasProductos.ValorUnitario;
                            vPlanComprasProductos.CantidadCupos = vDataReaderResults["CantidadCupos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CantidadCupos"].ToString()) : vPlanComprasProductos.CantidadCupos;
                            vPlanComprasProductos.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasProductos.IdPlanDeComprasContratos;
                            vPlanComprasProductos.IsReduccion = vDataReaderResults["IsReduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["IsReduccion"].ToString()) : vPlanComprasProductos.IsReduccion;
                            vPlanComprasProductos.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vPlanComprasProductos.IdReduccion;
                            vPlanComprasProductos.ValorReduccion = vDataReaderResults["ValorReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorReduccion"].ToString()) : vPlanComprasProductos.ValorReduccion;
                            vPlanComprasProductos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasProductos.UsuarioCrea;
                            vPlanComprasProductos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasProductos.FechaCrea;
                            vPlanComprasProductos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasProductos.UsuarioModifica;
                            vPlanComprasProductos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasProductos.FechaModifica;
                        }
                        return vPlanComprasProductos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasProductos ConsultarPlanComprasProductosUltimaModificacion(int IDPlanDeComprasContratos, int IdProducto )
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@IdProducto", DbType.Int32, IdProducto);

                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vPlanComprasProductos = new PlanComprasProductos();
                        while (vDataReaderResults.Read())
                        {
                            vPlanComprasProductos.NumeroConsecutivoPlanCompras = vDataReaderResults["NumeroConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivoPlanCompras"].ToString()) : vPlanComprasProductos.NumeroConsecutivoPlanCompras;
                            vPlanComprasProductos.IdProductoPlanCompraContrato = vDataReaderResults["IDProductoPlanCompraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProductoPlanCompraContrato"].ToString()) : vPlanComprasProductos.IdProductoPlanCompraContrato;
                            vPlanComprasProductos.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vPlanComprasProductos.CodigoProducto;
                            vPlanComprasProductos.Tiempo = vDataReaderResults["Tiempo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Tiempo"].ToString()) : vPlanComprasProductos.Tiempo;
                            vPlanComprasProductos.CantidadCupos = vDataReaderResults["CantidadCupos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CantidadCupos"].ToString()) : vPlanComprasProductos.CantidadCupos;
                            vPlanComprasProductos.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasProductos.IdPlanDeComprasContratos;
                            vPlanComprasProductos.IsReduccion = vDataReaderResults["IsReduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["IsReduccion"].ToString()) : vPlanComprasProductos.IsReduccion;
                            vPlanComprasProductos.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vPlanComprasProductos.IdReduccion;
                            vPlanComprasProductos.ValorReduccion = vDataReaderResults["ValorReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorReduccion"].ToString()) : vPlanComprasProductos.ValorReduccion;
                            vPlanComprasProductos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasProductos.UsuarioCrea;
                            vPlanComprasProductos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasProductos.FechaCrea;
                            vPlanComprasProductos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasProductos.UsuarioModifica;
                            vPlanComprasProductos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasProductos.FechaModifica;
                        }
                        return vPlanComprasProductos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pIdPlanDeComprasContratos"></param>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        /// <param name="pIdProducto"></param>
        /// <param name="pCantidadCupos"></param>
        /// <param name="pValorUnitario"></param>
        /// <param name="pTiempo"></param>
        /// <param name="pValorTotal"></param>
        /// <param name="pUnidadTiempo"></param>
        /// <param name="pUnidadMedida"></param>
        public List<PlanComprasProductos> ConsultarPlanComprasProductoss(int? pNumeroConsecutivoPlanCompras,    
                                                                         String pIdPlanDeComprasContratos, 
                                                                         String pIdProductoPlanCompraContrato, 
                                                                         String pIdProducto, 
                                                                         Decimal? pCantidadCupos, 
                                                                         Decimal? pValorUnitario, 
                                                                         Decimal? pTiempo, 
                                                                         Decimal? pValorTotal, 
                                                                         String pUnidadTiempo, 
                                                                         String pUnidadMedida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductoss_Consultar"))
                {
                    if (pNumeroConsecutivoPlanCompras != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pNumeroConsecutivoPlanCompras);
                    if (pIdPlanDeComprasContratos != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pIdPlanDeComprasContratos);
                    if (pIdProductoPlanCompraContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDProductoPlanCompraContrato", DbType.Int32, pIdProductoPlanCompraContrato);
                    if (pIdProducto != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDProducto", DbType.String, pIdProducto);
                    if(pCantidadCupos != null)
                         vDataBase.AddInParameter(vDbCommand, "@CantidadCupos", DbType.Decimal, pCantidadCupos);

                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaPlanComprasProductos = new List<PlanComprasProductos>();
                        while (vDataReaderResults.Read())
                        {
                            var vPlanComprasProductos = new PlanComprasProductos();
                            vPlanComprasProductos.NumeroConsecutivoPlanCompras = vDataReaderResults["NumeroConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivoPlanCompras"].ToString()) : vPlanComprasProductos.NumeroConsecutivoPlanCompras;
                            vPlanComprasProductos.IdProductoPlanCompraContrato = vDataReaderResults["IDProductoPlanCompraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProductoPlanCompraContrato"].ToString()) : vPlanComprasProductos.IdProductoPlanCompraContrato;
                            vPlanComprasProductos.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vPlanComprasProductos.CodigoProducto;
                            vPlanComprasProductos.CantidadCupos = vDataReaderResults["CantidadCupos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CantidadCupos"].ToString()) : vPlanComprasProductos.CantidadCupos;
                            vPlanComprasProductos.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasProductos.IdPlanDeComprasContratos;
                            vPlanComprasProductos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasProductos.UsuarioCrea;
                            vPlanComprasProductos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasProductos.FechaCrea;
                            vPlanComprasProductos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasProductos.UsuarioModifica;
                            vPlanComprasProductos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasProductos.FechaModifica;
                            vPlanComprasProductos.ValorUnitario = vDataReaderResults["ValorProducto"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorProducto"].ToString()) : vPlanComprasProductos.ValorUnitario;
                            vPlanComprasProductos.Tiempo = vDataReaderResults["Tiempo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Tiempo"].ToString()) : vPlanComprasProductos.Tiempo;
                            vPlanComprasProductos.UnidadTiempo = vDataReaderResults["UnidadTiempo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UnidadTiempo"].ToString()) : vPlanComprasProductos.UnidadTiempo;
                            vPlanComprasProductos.IdUnidadTiempo = vDataReaderResults["IdUnidadTiempo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdUnidadTiempo"].ToString()) : vPlanComprasProductos.IdUnidadTiempo;
                            vPlanComprasProductos.IdDetalleObjeto = vDataReaderResults["IdDetalleObjeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDetalleObjeto"].ToString()) : vPlanComprasProductos.IdDetalleObjeto;
                            vPlanComprasProductos.EsAdicion = vDataReaderResults["EsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsAdicion"].ToString()) : vPlanComprasProductos.EsAdicion;
                            vPlanComprasProductos.IsReduccion = vDataReaderResults["IsReduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["IsReduccion"].ToString()) : vPlanComprasProductos.EsAdicion;
                            vPlanComprasProductos.ValorReduccion = vDataReaderResults["ValorReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorReduccion"].ToString()) : vPlanComprasProductos.ValorReduccion;
                            vPlanComprasProductos.ValorAdiccion = vDataReaderResults["ValorAdicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicion"].ToString()) : vPlanComprasProductos.ValorReduccion;
                            vPlanComprasProductos.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vPlanComprasProductos.IdAdicion;
                            
                            vListaPlanComprasProductos.Add(vPlanComprasProductos);
                        }
                        return vListaPlanComprasProductos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los productos asociados a un plan de compras para ser utilizado en la pagina de registro
        /// de contratos
        /// </summary>
        /// <param name="pIdPlanDeComprasContratos">Entero con el identificador del plan de compras</param>
        /// <returns>Listado de productos asociados al plan de compras obtenidos de la base de datos</returns>
        public List<PlanComprasProductos> ObtenerProductosPlanCompras(int pIdPlanDeComprasContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pIdPlanDeComprasContratos);
                    
                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaPlanComprasProductos = new List<PlanComprasProductos>();
                        while (vDataReaderResults.Read())
                        {
                            var vPlanComprasProductos = new PlanComprasProductos();
                            vPlanComprasProductos.IdProductoPlanCompraContrato = vDataReaderResults["IDProductoPlanCompraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProductoPlanCompraContrato"].ToString()) : vPlanComprasProductos.IdProductoPlanCompraContrato;
                            vPlanComprasProductos.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vPlanComprasProductos.CodigoProducto;
                            vPlanComprasProductos.CantidadCupos = vDataReaderResults["CantidadCupos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CantidadCupos"].ToString()) : vPlanComprasProductos.CantidadCupos;
                            vPlanComprasProductos.ValorUnitario = vDataReaderResults["ValorProducto"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorProducto"].ToString()) : vPlanComprasProductos.ValorUnitario;
                            vPlanComprasProductos.Tiempo = vDataReaderResults["Tiempo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Tiempo"].ToString()) : vPlanComprasProductos.Tiempo;
                            vPlanComprasProductos.UnidadTiempo = vDataReaderResults["UnidadTiempo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UnidadTiempo"].ToString()) : vPlanComprasProductos.UnidadTiempo;
                            vPlanComprasProductos.IdUnidadTiempo = vDataReaderResults["IdUnidadTiempo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdUnidadTiempo"].ToString()) : vPlanComprasProductos.IdUnidadTiempo;
                            vPlanComprasProductos.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasProductos.IdPlanDeComprasContratos;
                            vPlanComprasProductos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasProductos.UsuarioCrea;
                            vPlanComprasProductos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasProductos.FechaCrea;
                            vPlanComprasProductos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasProductos.UsuarioModifica;
                            vPlanComprasProductos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasProductos.FechaModifica;
                            vPlanComprasProductos.NumeroConsecutivoPlanCompras = vDataReaderResults["NumeroConsecutivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivo"].ToString()) : vPlanComprasProductos.NumeroConsecutivoPlanCompras;
                            vPlanComprasProductos.IdDetalleObjeto = vDataReaderResults["IdDetalleObjeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDetalleObjeto"].ToString()) : vPlanComprasProductos.IdDetalleObjeto;
                            vPlanComprasProductos.IsReduccion = vDataReaderResults["IsReduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["IsReduccion"].ToString()) : vPlanComprasProductos.IsReduccion;
                            vPlanComprasProductos.EsAdicion = vDataReaderResults["EsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsAdicion"].ToString()) : vPlanComprasProductos.EsAdicion;
                            vListaPlanComprasProductos.Add(vPlanComprasProductos);
                        }
                        return vListaPlanComprasProductos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdProductoPlanComprasContrato"></param>
        /// <returns></returns>
        public ValidacionProductoOriginal ValidarModifcacionProducto(int pIdProductoPlanComprasContrato)
        {
            ValidacionProductoOriginal result = new ValidacionProductoOriginal();
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_ValidarProductoOriginal"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand,  "@IDProductoPlanCompraContrato", DbType.Int32, pIdProductoPlanComprasContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@EsProductoOriginal", DbType.Boolean, 4);
                    vDataBase.AddOutParameter(vDbCommand, "@SufrioModificaciones", DbType.Boolean, 4);
                    vDataBase.AddOutParameter(vDbCommand, "@EsModificacionProceso", DbType.Boolean, 4);
                    vDataBase.AddOutParameter(vDbCommand, "@EsModificacionUltima", DbType.Boolean, 4);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    
                    string esProductoOriginal = vDataBase.GetParameterValue(vDbCommand, "@EsProductoOriginal").ToString();
                    if( ! string.IsNullOrEmpty(esProductoOriginal))
                    result.EsproductoOriginal = Convert.ToBoolean(esProductoOriginal);

                    string sufrioModificaciones = vDataBase.GetParameterValue(vDbCommand, "@SufrioModificaciones").ToString();
                    if (!string.IsNullOrEmpty(sufrioModificaciones))
                    result.SufrioModificaciones = Convert.ToBoolean(sufrioModificaciones);

                    string EsModificacionProceso = vDataBase.GetParameterValue(vDbCommand, "@EsModificacionProceso").ToString();
                    if (!string.IsNullOrEmpty(EsModificacionProceso))
                    result.EsModificacionProceso = Convert.ToBoolean(EsModificacionProceso);

                    string EsModificacionUltima = vDataBase.GetParameterValue(vDbCommand, "@EsModificacionUltima").ToString();
                    if (!string.IsNullOrEmpty(EsModificacionUltima))
                    result.EsModificacionUltima = Convert.ToBoolean(EsModificacionUltima);

                    return result;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }

            return result;
        }

        public ValidacionRubroOriginal ValidarModifcacionRubro(int pIdRubroPlanComprasContrato)
        {
            ValidacionRubroOriginal result = new ValidacionRubroOriginal();
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_RubroPlanCompras_ValidarProductoOriginal"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDRubroPlanCompraContrato", DbType.Int32, pIdRubroPlanComprasContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@EsProductoOriginal", DbType.Boolean, 4);
                    vDataBase.AddOutParameter(vDbCommand, "@SufrioModificaciones", DbType.Boolean, 4);
                    vDataBase.AddOutParameter(vDbCommand, "@EsModificacionProceso", DbType.Boolean, 4);
                    vDataBase.AddOutParameter(vDbCommand, "@EsModificacionUltima", DbType.Boolean, 4);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    string esProductoOriginal = vDataBase.GetParameterValue(vDbCommand, "@EsProductoOriginal").ToString();
                    if (!string.IsNullOrEmpty(esProductoOriginal))
                        result.EsproductoOriginal = Convert.ToBoolean(esProductoOriginal);

                    string sufrioModificaciones = vDataBase.GetParameterValue(vDbCommand, "@SufrioModificaciones").ToString();
                    if (!string.IsNullOrEmpty(sufrioModificaciones))
                        result.SufrioModificaciones = Convert.ToBoolean(sufrioModificaciones);

                    string EsModificacionProceso = vDataBase.GetParameterValue(vDbCommand, "@EsModificacionProceso").ToString();
                    if (!string.IsNullOrEmpty(EsModificacionProceso))
                        result.EsModificacionProceso = Convert.ToBoolean(EsModificacionProceso);

                    string EsModificacionUltima = vDataBase.GetParameterValue(vDbCommand, "@EsModificacionUltima").ToString();
                    if (!string.IsNullOrEmpty(EsModificacionUltima))
                        result.EsModificacionUltima = Convert.ToBoolean(EsModificacionUltima);

                    return result;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPlanComprasContrato"></param>
        /// <param name="codigoProducto"></param>
        /// <returns></returns>
        public PlanComprasProductos ConsultarPlanComprasProductoActual(int idPlanComprasContrato, string codigoProducto)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_ConsultarActual"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanComprasContrato", DbType.Int32, idPlanComprasContrato);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoProducto", DbType.String, codigoProducto);

                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vPlanComprasProductos = new PlanComprasProductos();
                        while (vDataReaderResults.Read())
                        {
                            vPlanComprasProductos.NumeroConsecutivoPlanCompras = vDataReaderResults["NumeroConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivoPlanCompras"].ToString()) : vPlanComprasProductos.NumeroConsecutivoPlanCompras;
                            vPlanComprasProductos.IdProductoPlanCompraContrato = vDataReaderResults["IDProductoPlanCompraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProductoPlanCompraContrato"].ToString()) : vPlanComprasProductos.IdProductoPlanCompraContrato;
                            vPlanComprasProductos.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vPlanComprasProductos.CodigoProducto;
                            vPlanComprasProductos.CantidadCupos = vDataReaderResults["CantidadCupos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CantidadCupos"].ToString()) : vPlanComprasProductos.CantidadCupos;
                            vPlanComprasProductos.Tiempo = vDataReaderResults["Tiempo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Tiempo"].ToString()) : vPlanComprasProductos.Tiempo;
                            vPlanComprasProductos.ValorUnitario = vDataReaderResults["ValorProducto"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorProducto"].ToString()) : vPlanComprasProductos.ValorUnitario;
                            vPlanComprasProductos.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasProductos.IdPlanDeComprasContratos;
                            vPlanComprasProductos.IsReduccion = vDataReaderResults["IsReduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["IsReduccion"].ToString()) : vPlanComprasProductos.IsReduccion;
                            vPlanComprasProductos.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vPlanComprasProductos.IdReduccion;
                            vPlanComprasProductos.ValorReduccion = vDataReaderResults["ValorReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorReduccion"].ToString()) : vPlanComprasProductos.ValorReduccion;
                            vPlanComprasProductos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasProductos.UsuarioCrea;
                            vPlanComprasProductos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasProductos.FechaCrea;
                            vPlanComprasProductos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasProductos.UsuarioModifica;
                            vPlanComprasProductos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasProductos.FechaModifica;
                        }
                        return vPlanComprasProductos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <returns></returns>
        public int InsertarProductoPlanComprasContratoAdiciones(ProductoPlanComprasContratos pPlanComprasProductos, int idAporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ProductoPlanCompraContratoAdiciones_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDProductoPlanCompraContrato", DbType.Int32, pPlanComprasProductos.IDProductoPlanCompraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDProducto", DbType.String, pPlanComprasProductos.IDProducto);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadCupos", DbType.Decimal, pPlanComprasProductos.CantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasProductos.IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorProducto", DbType.Decimal, pPlanComprasProductos.ValorProducto);
                    vDataBase.AddInParameter(vDbCommand, "@Tiempo", DbType.Decimal, pPlanComprasProductos.Tiempo);
                    vDataBase.AddInParameter(vDbCommand, "@UnidadTiempo", DbType.String, pPlanComprasProductos.UnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdUnidadTiempo", DbType.Int32, pPlanComprasProductos.IdUnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleObjeto", DbType.String, pPlanComprasProductos.IdDetalleObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@ReduccionCantidadCupos", DbType.Decimal, pPlanComprasProductos.ReduccionCantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pPlanComprasProductos.IdReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Decimal, pPlanComprasProductos.ValorReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IsReduccion", DbType.Boolean, pPlanComprasProductos.IsReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pPlanComprasProductos.IdAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pPlanComprasProductos.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IsAdicion", DbType.Boolean, pPlanComprasProductos.IsAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAporte", DbType.Int32, idAporte);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasProductos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <returns></returns>
        public int ModificarProductoPlanComprasContratoAdiciones(ProductoPlanComprasContratos pPlanComprasProductos, int idAporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ProductoPlanCompraContratoAdiciones_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDProductoPlanCompraContrato", DbType.Int32, pPlanComprasProductos.IDProductoPlanCompraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDProducto", DbType.String, pPlanComprasProductos.IDProducto);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadCupos", DbType.Decimal, pPlanComprasProductos.CantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasProductos.IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorProducto", DbType.Decimal, pPlanComprasProductos.ValorProducto);
                    vDataBase.AddInParameter(vDbCommand, "@Tiempo", DbType.Decimal, pPlanComprasProductos.Tiempo);
                    vDataBase.AddInParameter(vDbCommand, "@UnidadTiempo", DbType.String, pPlanComprasProductos.UnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdUnidadTiempo", DbType.Int32, pPlanComprasProductos.IdUnidadTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleObjeto", DbType.String, pPlanComprasProductos.IdDetalleObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@ReduccionCantidadCupos", DbType.Decimal, pPlanComprasProductos.ReduccionCantidadCupos);
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pPlanComprasProductos.IdReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Decimal, pPlanComprasProductos.ValorReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IsReduccion", DbType.Boolean, pPlanComprasProductos.IsReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pPlanComprasProductos.IdAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pPlanComprasProductos.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IsAdicion", DbType.Boolean, pPlanComprasProductos.IsAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAporte", DbType.Int32, idAporte);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasProductos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
