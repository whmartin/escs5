using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Tipo Contrato
    /// </summary>
    public class TipoContratoDAL : GeneralDAL
    {
        public TipoContratoDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int InsertarTipoContrato(TipoContrato pTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoContrato", DbType.String, pTipoContrato.NombreTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pTipoContrato.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ActaInicio", DbType.Boolean, pTipoContrato.ActaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@AporteCofinaciacion", DbType.Boolean, pTipoContrato.AporteCofinaciacion);
                    vDataBase.AddInParameter(vDbCommand, "@RecursoFinanciero", DbType.Boolean, pTipoContrato.RecursoFinanciero);
                    vDataBase.AddInParameter(vDbCommand, "@RegimenContrato", DbType.Int32, pTipoContrato.RegimenContrato);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionTipoContrato", DbType.String, pTipoContrato.DescripcionTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoContrato.IdTipoContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoContrato").ToString());
                    GenerarLogAuditoria(pTipoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int ModificarTipoContrato(TipoContrato pTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pTipoContrato.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoContrato", DbType.String, pTipoContrato.NombreTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pTipoContrato.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ActaInicio", DbType.Boolean, pTipoContrato.ActaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@AporteCofinaciacion", DbType.Boolean, pTipoContrato.AporteCofinaciacion);
                    vDataBase.AddInParameter(vDbCommand, "@RecursoFinanciero", DbType.Boolean, pTipoContrato.RecursoFinanciero);
                    vDataBase.AddInParameter(vDbCommand, "@RegimenContrato", DbType.Int32, pTipoContrato.RegimenContrato);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionTipoContrato", DbType.String, pTipoContrato.DescripcionTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int EliminarTipoContrato(TipoContrato pTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pTipoContrato.IdTipoContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad TipoContrato
        /// </summary>
        /// <param name="pIdTipoContrato"></param>
        /// <returns></returns>
        public TipoContrato ConsultarTipoContrato(int pIdTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pIdTipoContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoContrato vTipoContrato = new TipoContrato();
                        while (vDataReaderResults.Read())
                        {
                            vTipoContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vTipoContrato.IdTipoContrato;
                            vTipoContrato.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : vTipoContrato.NombreTipoContrato;
                            vTipoContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vTipoContrato.IdCategoriaContrato;
                            vTipoContrato.ActaInicio = vDataReaderResults["ActaInicio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ActaInicio"].ToString()) : vTipoContrato.ActaInicio;
                            vTipoContrato.AporteCofinaciacion = vDataReaderResults["AporteCofinaciacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteCofinaciacion"].ToString()) : vTipoContrato.AporteCofinaciacion;
                            vTipoContrato.RecursoFinanciero = vDataReaderResults["RecursoFinanciero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RecursoFinanciero"].ToString()) : vTipoContrato.RecursoFinanciero;
                            vTipoContrato.RegimenContrato = vDataReaderResults["RegimenContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegimenContrato"].ToString()) : vTipoContrato.RegimenContrato;
                            vTipoContrato.DescripcionTipoContrato = vDataReaderResults["DescripcionTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoContrato"].ToString()) : vTipoContrato.DescripcionTipoContrato;
                            vTipoContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoContrato.Estado;
                            vTipoContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoContrato.UsuarioCrea;
                            vTipoContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoContrato.FechaCrea;
                            vTipoContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoContrato.UsuarioModifica;
                            vTipoContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoContrato.FechaModifica;
                            vTipoContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"].ToString()) : vTipoContrato.CodigoTipoContrato; 
                        }
                        return vTipoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoContrato
        /// </summary>
        /// <param name="pNombreTipoContrato"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pEstado"></param>
        /// <param name="pActaInicio"></param>
        /// <param name="pAporteCofinanciacion"></param>
        /// <param name="pRecursosFinancieros"></param>
        /// <param name="pRegimenContrato"></param>
        /// <param name="pDescripcionTipoContrato"></param>
        /// <returns></returns>
        public List<TipoContrato> ConsultarTipoContratos(String pNombreTipoContrato, int? pIdCategoriaContrato, Boolean? pEstado, Boolean? pActaInicio, Boolean? pAporteCofinanciacion, Boolean? pRecursosFinancieros, int? pRegimenContrato, String pDescripcionTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoContratos_Consultar"))
                {
                    if (pNombreTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoContrato", DbType.String, pNombreTipoContrato);
                    if (pIdCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pIdCategoriaContrato);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (pActaInicio !=null)
                        vDataBase.AddInParameter(vDbCommand, "@ACTAINICIO", DbType.Boolean, pActaInicio);
                    if (pAporteCofinanciacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@APORTECOFINANCIACION", DbType.Boolean, pAporteCofinanciacion);
                    if (pRecursosFinancieros != null)
                        vDataBase.AddInParameter(vDbCommand, "@RECURSOFINANCIERO", DbType.Boolean, pRecursosFinancieros);
                    if (pRegimenContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@REGIMENCONTRATO", DbType.Int32, pRegimenContrato);
                    if (pDescripcionTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@DESCRIPCIONTIPOCONTRATO", DbType.String, pDescripcionTipoContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoContrato> vListaTipoContrato = new List<TipoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            TipoContrato vTipoContrato = new TipoContrato();
                            vTipoContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vTipoContrato.IdTipoContrato;
                            vTipoContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"].ToString()) : vTipoContrato.CodigoTipoContrato;
                            vTipoContrato.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : vTipoContrato.NombreTipoContrato;
                            vTipoContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vTipoContrato.IdCategoriaContrato;
                            vTipoContrato.ActaInicio = vDataReaderResults["ActaInicio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ActaInicio"].ToString()) : vTipoContrato.ActaInicio;
                            vTipoContrato.AporteCofinaciacion = vDataReaderResults["AporteCofinaciacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteCofinaciacion"].ToString()) : vTipoContrato.AporteCofinaciacion;
                            vTipoContrato.RecursoFinanciero = vDataReaderResults["RecursoFinanciero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RecursoFinanciero"].ToString()) : vTipoContrato.RecursoFinanciero;
                            vTipoContrato.RegimenContrato = vDataReaderResults["RegimenContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegimenContrato"].ToString()) : vTipoContrato.RegimenContrato;
                            vTipoContrato.DescripcionTipoContrato = vDataReaderResults["DescripcionTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoContrato"].ToString()) : vTipoContrato.DescripcionTipoContrato;
                            vTipoContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoContrato.Estado;
                            vTipoContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoContrato.UsuarioCrea;
                            vTipoContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoContrato.FechaCrea;
                            vTipoContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoContrato.UsuarioModifica;
                            vTipoContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoContrato.FechaModifica;
                            vListaTipoContrato.Add(vTipoContrato);
                        }
                        return vListaTipoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos) del tipo de contrato la 
        /// informaci�n del mismo
        /// </summary>
        /// <param name="pTipoContrato">Entidad con la informaci�n del filtro</param>
        /// <returns>Entidad con la informaci�n de tipo de contrato recuperado</returns>
        public TipoContrato IdentificadorCodigoTipoContrato(TipoContrato pTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoContrato_IdentificadorCodigo"))
                {
                    if (pTipoContrato.IdTipoContrato != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pTipoContrato.IdTipoContrato);
                    if (pTipoContrato.CodigoTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoTipoContrato", DbType.String, pTipoContrato.CodigoTipoContrato);
                    if (pTipoContrato.IdCategoriaContrato != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pTipoContrato.IdCategoriaContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        //TipoContrato vTipoContrato = new TipoContrato();
                        while (vDataReaderResults.Read())
                        {
                            pTipoContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : pTipoContrato.IdTipoContrato;
                            pTipoContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"].ToString()) : pTipoContrato.CodigoTipoContrato;
                            pTipoContrato.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : pTipoContrato.NombreTipoContrato;
                            pTipoContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : pTipoContrato.IdCategoriaContrato;
                            pTipoContrato.ActaInicio = vDataReaderResults["ActaInicio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ActaInicio"].ToString()) : pTipoContrato.ActaInicio;
                            pTipoContrato.AporteCofinaciacion = vDataReaderResults["AporteCofinaciacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteCofinaciacion"].ToString()) : pTipoContrato.AporteCofinaciacion;
                            pTipoContrato.RecursoFinanciero = vDataReaderResults["RecursoFinanciero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RecursoFinanciero"].ToString()) : pTipoContrato.RecursoFinanciero;
                            pTipoContrato.RegimenContrato = vDataReaderResults["RegimenContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegimenContrato"].ToString()) : pTipoContrato.RegimenContrato;
                            pTipoContrato.DescripcionTipoContrato = vDataReaderResults["DescripcionTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoContrato"].ToString()) : pTipoContrato.DescripcionTipoContrato;
                            pTipoContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : pTipoContrato.Estado;
                            pTipoContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : pTipoContrato.UsuarioCrea;
                            pTipoContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : pTipoContrato.FechaCrea;
                            pTipoContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : pTipoContrato.UsuarioModifica;
                            pTipoContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : pTipoContrato.FechaModifica;
                        }
                        return pTipoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
