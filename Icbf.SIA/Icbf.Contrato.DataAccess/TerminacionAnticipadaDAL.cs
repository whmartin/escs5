﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class TerminacionAnticipadaDAL : GeneralDAL
    {
        public int InsertarTerminacionAnticipada(TerminacionAnticipada pTerminacionAnticipada)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_TerminacionAnticipada_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTerminacionAnticipada", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pTerminacionAnticipada.IDDetalleConsModeContractual);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRadicacion", DbType.DateTime, pTerminacionAnticipada.FechaRadicacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRadicacion", DbType.String, pTerminacionAnticipada.NumeroRadicacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionAnticipada", DbType.DateTime, pTerminacionAnticipada.FechaTerminacionAnticipada);
                    vDataBase.AddInParameter(vDbCommand, "@Consideraciones", DbType.String, pTerminacionAnticipada.Consideraciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTerminacionAnticipada.UsuarioCrea);
                   
                    vDataBase.AddInParameter(vDbCommand, "@TipoTerminacion", DbType.String, pTerminacionAnticipada.TipoTerminacion);


                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTerminacionAnticipada.IdTerminacionAnticipada = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTerminacionAnticipada").ToString());
                    GenerarLogAuditoria(pTerminacionAnticipada, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTerminacionAnticipada(TerminacionAnticipada pTerminacionAnticipada)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_TerminacionAnticipada_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTerminacionAnticipada", DbType.Int32, pTerminacionAnticipada.IdTerminacionAnticipada);                    
                    vDataBase.AddInParameter(vDbCommand, "@FechaRadicacion", DbType.DateTime, pTerminacionAnticipada.FechaRadicacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRadicacion", DbType.String, pTerminacionAnticipada.NumeroRadicacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionAnticipada", DbType.DateTime, pTerminacionAnticipada.FechaTerminacionAnticipada);
                    vDataBase.AddInParameter(vDbCommand, "@Consideraciones", DbType.String, pTerminacionAnticipada.Consideraciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTerminacionAnticipada.UsuarioModifica);
                    
                    vDataBase.AddInParameter(vDbCommand, "@TipoTerminacion", DbType.String, pTerminacionAnticipada.TipoTerminacion);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTerminacionAnticipada, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaContrato(int IdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_TerminacionAnticipada_ConsultarPorContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TerminacionAnticipada> vLista = new List<TerminacionAnticipada>();
                        while (vDataReaderResults.Read())
                        {
                            TerminacionAnticipada vAdiciones = new TerminacionAnticipada();
                            vAdiciones.IdTerminacionAnticipada = vDataReaderResults["IdTerminacionAnticipada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTerminacionAnticipada"].ToString()) : vAdiciones.IdTerminacionAnticipada;
                            vAdiciones.FechaRadicacion = vDataReaderResults["FechaRadicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicacion"].ToString()) : vAdiciones.FechaRadicacion;
                            vAdiciones.NumeroRadicacion = vDataReaderResults["NumeroRadicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicacion"].ToString()) : vAdiciones.NumeroRadicacion;
                            vAdiciones.FechaTerminacionAnticipada = vDataReaderResults["FechaTerminacionAnticipada"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionAnticipada"].ToString()) : vAdiciones.FechaTerminacionAnticipada;
                            vAdiciones.Consideraciones = vDataReaderResults["Consideraciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consideraciones"].ToString()) : vAdiciones.Consideraciones;
                            vAdiciones.IDDetalleConsModeContractual = vDataReaderResults["IDDetalleConsModeContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModeContractual"].ToString()) : vAdiciones.IDDetalleConsModeContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;
                            vAdiciones.TipoTerminacion = vDataReaderResults["TipoTerminacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoTerminacion"].ToString()) : vAdiciones.TipoTerminacion;
                            vLista.Add(vAdiciones);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaId(int IdTerminacionAnticipada)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_TerminacionAnticipada_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTerminacionAnticipada", DbType.Int32, IdTerminacionAnticipada);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TerminacionAnticipada> vLista = new List<TerminacionAnticipada>();
                        while (vDataReaderResults.Read())
                        {
                            TerminacionAnticipada vAdiciones = new TerminacionAnticipada();
                            vAdiciones.IdTerminacionAnticipada = vDataReaderResults["IdTerminacionAnticipada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTerminacionAnticipada"].ToString()) : vAdiciones.IdTerminacionAnticipada;
                            vAdiciones.FechaRadicacion = vDataReaderResults["FechaRadicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicacion"].ToString()) : vAdiciones.FechaRadicacion;
                            vAdiciones.NumeroRadicacion = vDataReaderResults["NumeroRadicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicacion"].ToString()) : vAdiciones.NumeroRadicacion;
                            vAdiciones.FechaTerminacionAnticipada = vDataReaderResults["FechaTerminacionAnticipada"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionAnticipada"].ToString()) : vAdiciones.FechaTerminacionAnticipada;
                            vAdiciones.Consideraciones = vDataReaderResults["Consideraciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consideraciones"].ToString()) : vAdiciones.Consideraciones;
                            vAdiciones.IDDetalleConsModeContractual = vDataReaderResults["IDDetalleConsModeContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModeContractual"].ToString()) : vAdiciones.IDDetalleConsModeContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;
                            vAdiciones.TipoTerminacion = vDataReaderResults["TipoTerminacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoTerminacion"].ToString()) : vAdiciones.TipoTerminacion;
                            vLista.Add(vAdiciones);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_TerminacionAnticipada_ConsultarPorContratoIdDetConsModContractual"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDetConsModContractual", DbType.Int32, IdDetConsModContractual);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TerminacionAnticipada> vLista = new List<TerminacionAnticipada>();
                        while (vDataReaderResults.Read())
                        {
                            TerminacionAnticipada vAdiciones = new TerminacionAnticipada();
                            vAdiciones.IdTerminacionAnticipada = vDataReaderResults["IdTerminacionAnticipada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTerminacionAnticipada"].ToString()) : vAdiciones.IdTerminacionAnticipada;
                            vAdiciones.FechaRadicacion = vDataReaderResults["FechaRadicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicacion"].ToString()) : vAdiciones.FechaRadicacion;
                            vAdiciones.NumeroRadicacion = vDataReaderResults["NumeroRadicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicacion"].ToString()) : vAdiciones.NumeroRadicacion;
                            vAdiciones.FechaTerminacionAnticipada = vDataReaderResults["FechaTerminacionAnticipada"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionAnticipada"].ToString()) : vAdiciones.FechaTerminacionAnticipada;
                            vAdiciones.Consideraciones = vDataReaderResults["Consideraciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consideraciones"].ToString()) : vAdiciones.Consideraciones;
                            vAdiciones.IDDetalleConsModeContractual = vDataReaderResults["IDDetalleConsModeContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModeContractual"].ToString()) : vAdiciones.IDDetalleConsModeContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;
                            vAdiciones.TipoTerminacion = vDataReaderResults["TipoTerminacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoTerminacion"].ToString()) : vAdiciones.TipoTerminacion;
                            vLista.Add(vAdiciones);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
