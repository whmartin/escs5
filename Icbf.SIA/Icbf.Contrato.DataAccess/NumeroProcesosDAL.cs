using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad NumeroProcesos
    /// </summary>
    public class NumeroProcesosDAL : GeneralDAL
    {
        public NumeroProcesosDAL()
        {
        }
        /// <summary>
        /// Gonet
        /// Método de inserción para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int InsertarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_NumeroProcesos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdNumeroProceso", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.Int32, pNumeroProcesos.NumeroProceso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicacion", DbType.DateTime, pNumeroProcesos.FechaAdjudicacion);
                    vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pNumeroProcesos.Inactivo);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroProcesoGenerado", DbType.String, pNumeroProcesos.NumeroProcesoGenerado);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pNumeroProcesos.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pNumeroProcesos.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pNumeroProcesos.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pNumeroProcesos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pNumeroProcesos.IdNumeroProceso = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdNumeroProceso").ToString());
                    GenerarLogAuditoria(pNumeroProcesos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad NumeroProcesos
        /// </summary>
        /// <param name="pNumeroProcesos"></param>
        public int ModificarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_NumeroProcesos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroProceso", DbType.Int32, pNumeroProcesos.IdNumeroProceso);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.Int32, pNumeroProcesos.NumeroProceso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicacion", DbType.DateTime, pNumeroProcesos.FechaAdjudicacion);
                    vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pNumeroProcesos.Inactivo);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroProcesoGenerado", DbType.String, pNumeroProcesos.NumeroProcesoGenerado);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pNumeroProcesos.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pNumeroProcesos.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pNumeroProcesos.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pNumeroProcesos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNumeroProcesos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Método de modificación para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int EliminarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_NumeroProcesos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroProceso", DbType.Int32, pNumeroProcesos.IdNumeroProceso);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNumeroProcesos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Gonet
        /// Método de consulta por id para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pIdNumeroProceso">Valor entero con el Id del numero del proceso</param>
        public NumeroProcesos ConsultarNumeroProcesos(int pIdNumeroProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_NumeroProcesos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroProceso", DbType.Int32, pIdNumeroProceso);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        NumeroProcesos vNumeroProcesos = new NumeroProcesos();
                        while (vDataReaderResults.Read())
                        {
                            vNumeroProcesos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vNumeroProcesos.IdNumeroProceso;
                            vNumeroProcesos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroProceso"].ToString()) : vNumeroProcesos.NumeroProceso;
                            vNumeroProcesos.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vNumeroProcesos.Inactivo;
                            vNumeroProcesos.FechaAdjudicacion = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vNumeroProcesos.FechaAdjudicacion;
                            vNumeroProcesos.NumeroProcesoGenerado = vDataReaderResults["NumeroProcesoGenerado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProcesoGenerado"].ToString()) : vNumeroProcesos.NumeroProcesoGenerado;
                            vNumeroProcesos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vNumeroProcesos.IdModalidadSeleccion;
                            vNumeroProcesos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vNumeroProcesos.IdRegional;
                            vNumeroProcesos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vNumeroProcesos.IdVigencia;
                            vNumeroProcesos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNumeroProcesos.UsuarioCrea;
                            vNumeroProcesos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNumeroProcesos.FechaCrea;
                            vNumeroProcesos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNumeroProcesos.UsuarioModifica;
                            vNumeroProcesos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNumeroProcesos.FechaModifica;
                        }
                        return vNumeroProcesos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Método de consulta por filtros para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProceso">Valor entero con el número del proceso</param>
        /// <param name="pInactivo">Valor Booleano del estado del registro</param>
        /// <param name="pNumeroProcesoGenerado">Cadena de texto con el numero de proceso generado</param>
        /// <param name="pIdModalidadSeleccion">Valor entero con el Id de la modalidad de selección</param>
        /// <param name="pIdRegional">Valor entero con el Id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el Id de la vigencia</param>
        public List<NumeroProcesos> ConsultarNumeroProcesoss(int? pIdNumeroProceso,int? pNumeroProceso, Boolean? pInactivo, String pNumeroProcesoGenerado, int? pIdModalidadSeleccion, int? pIdRegional, int? pIdVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_NumeroProcesoss_Consultar"))
                {
                    if (pIdNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdNumeroProceso", DbType.Int32, pIdNumeroProceso);
                    if(pNumeroProceso != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.Int32, pNumeroProceso);
                    if(pInactivo != null)
                         vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pInactivo);
                    if(pNumeroProcesoGenerado != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroProcesoGenerado", DbType.String, pNumeroProcesoGenerado);
                    if(pIdModalidadSeleccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    if(pIdRegional != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if(pIdVigencia != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NumeroProcesos> vListaNumeroProcesos = new List<NumeroProcesos>();
                        while (vDataReaderResults.Read())
                        {
                                NumeroProcesos vNumeroProcesos = new NumeroProcesos();
                            vNumeroProcesos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vNumeroProcesos.IdNumeroProceso;
                            vNumeroProcesos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroProceso"].ToString()) : vNumeroProcesos.NumeroProceso;
                            vNumeroProcesos.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vNumeroProcesos.Inactivo;
                            vNumeroProcesos.NumeroProcesoGenerado = vDataReaderResults["NumeroProcesoGenerado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProcesoGenerado"].ToString()).Trim() : vNumeroProcesos.NumeroProcesoGenerado.Trim();
                            vNumeroProcesos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vNumeroProcesos.IdModalidadSeleccion;
                            vNumeroProcesos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vNumeroProcesos.IdRegional;
                            vNumeroProcesos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vNumeroProcesos.IdVigencia;
                            vNumeroProcesos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNumeroProcesos.UsuarioCrea;
                            vNumeroProcesos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNumeroProcesos.FechaCrea;
                            vNumeroProcesos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNumeroProcesos.UsuarioModifica;
                            vNumeroProcesos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNumeroProcesos.FechaModifica;
                            vNumeroProcesos.FechaAdjudicacion = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vNumeroProcesos.FechaAdjudicacion;  

                            vListaNumeroProcesos.Add(vNumeroProcesos);
                        }
                        return vListaNumeroProcesos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Método de consulta por id NumeroProcesos opr cada contrato
        /// Fecha: 21/05/2014
        /// </summary>
        /// <param name="pIdNumeroProceso">Valor entero con el Id del numero del proceso</param>
        public bool ConsultarNumeroProcesosPorContrato(int pIdNumeroProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_NumeroProcesosPorContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroProceso", DbType.Int32, pIdNumeroProceso);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        
                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                        return false;
                        
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
