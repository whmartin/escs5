using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de la capa de acceso que contiene los métodos para Insertar, Modificar, Eliminar y Consultar registros de los funcionarios del ICBF
    /// </summary>
    public class FuncionarioICBFDAL : GeneralDAL
    {
        public FuncionarioICBFDAL()
        {
        }
        /// <summary>
        /// Gonet
        /// Insertar funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioICBF">Instancia que contiene la información del funcionario del ICBF</param>
        /// <returns>Identificador de la base de datos del registro insertado</returns>
        public int InsertarFuncionarioICBF(FuncionarioICBF pFuncionarioICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioICBF_Insertar"))
                {
                    pFuncionarioICBF.IdDependencia = null;
                    pFuncionarioICBF.IdTipoVinculacon = null;
                    pFuncionarioICBF.IdTercero = null;

                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdFuncionario", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependencia", DbType.Int32, pFuncionarioICBF.IdDependencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pFuncionarioICBF.IdRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoVinculacon", DbType.Int32, pFuncionarioICBF.IdTipoVinculacon);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pFuncionarioICBF.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pFuncionarioICBF.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pFuncionarioICBF.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Resolucion", DbType.String, pFuncionarioICBF.Resolucion);
                    vDataBase.AddInParameter(vDbCommand, "@NoContarto", DbType.String, pFuncionarioICBF.NoContarto);
                    vDataBase.AddInParameter(vDbCommand, "@NoTelefonoCelular", DbType.String, pFuncionarioICBF.NoTelefonoCelular);
                    vDataBase.AddInParameter(vDbCommand, "@NoTelefonoFijo", DbType.String, pFuncionarioICBF.NoTelefonoFijo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pFuncionarioICBF.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFuncionarioICBF.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFuncionarioICBF.IdFuncionario = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdFuncionario").ToString());
                    GenerarLogAuditoria(pFuncionarioICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Modificar funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioICBF">Instancia que contiene la información del funcionario del ICBF</param>
        /// <returns>Identificador de la base de datos del registro modificado</returns>
        public int ModificarFuncionarioICBF(FuncionarioICBF pFuncionarioICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioICBF_Modificar"))
                {
                    pFuncionarioICBF.IdDependencia = null;
                    pFuncionarioICBF.IdTipoVinculacon = null;
                    pFuncionarioICBF.IdTercero = null;

                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFuncionario", DbType.Int32, pFuncionarioICBF.IdFuncionario);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependencia", DbType.Int32, pFuncionarioICBF.IdDependencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pFuncionarioICBF.IdRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoVinculacon", DbType.Int32, pFuncionarioICBF.IdTipoVinculacon);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pFuncionarioICBF.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pFuncionarioICBF.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pFuncionarioICBF.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Resolucion", DbType.String, pFuncionarioICBF.Resolucion);
                    vDataBase.AddInParameter(vDbCommand, "@NoContarto", DbType.String, pFuncionarioICBF.NoContarto);
                    vDataBase.AddInParameter(vDbCommand, "@NoTelefonoCelular", DbType.String, pFuncionarioICBF.NoTelefonoCelular);
                    vDataBase.AddInParameter(vDbCommand, "@NoTelefonoFijo", DbType.String, pFuncionarioICBF.NoTelefonoFijo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pFuncionarioICBF.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFuncionarioICBF.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFuncionarioICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioICBF">Instancia que contiene la información del funcionario del ICBF</param>
        /// <returns>Identificador de la base de datos del registro eliminado</returns>
        public int EliminarFuncionarioICBF(FuncionarioICBF pFuncionarioICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioICBF_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFuncionario", DbType.Int32, pFuncionarioICBF.IdFuncionario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFuncionarioICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar funcionario del ICBF por Id
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdFuncionario">Valor entero con el Id del funcionario</param>
        /// <returns>Instancia que contiene la información del funcionario del ICBF</returns>
        public FuncionarioICBF ConsultarFuncionarioICBF(int pIdFuncionario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioICBF_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFuncionario", DbType.Int32, pIdFuncionario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        FuncionarioICBF vFuncionarioICBF = new FuncionarioICBF();
                        while (vDataReaderResults.Read())
                        {
                            vFuncionarioICBF.IdFuncionario = vDataReaderResults["IdFuncionario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFuncionario"].ToString()) : vFuncionarioICBF.IdFuncionario;
                            vFuncionarioICBF.IdDependencia = vDataReaderResults["IdDependencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependencia"].ToString()) : vFuncionarioICBF.IdDependencia;
                            vFuncionarioICBF.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vFuncionarioICBF.IdRegionalPCI;
                            vFuncionarioICBF.NombreRegional = vDataReaderResults["DescripcionPCI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionPCI"].ToString()) : vFuncionarioICBF.NombreRegional;
                            vFuncionarioICBF.IdTipoVinculacon = vDataReaderResults["IdTipoVinculacon"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoVinculacon"].ToString()) : vFuncionarioICBF.IdTipoVinculacon;
                            vFuncionarioICBF.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vFuncionarioICBF.IdTercero;
                            vFuncionarioICBF.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vFuncionarioICBF.IdUsuario;
                            vFuncionarioICBF.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vFuncionarioICBF.NumeroIdentificacion;
                            vFuncionarioICBF.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vFuncionarioICBF.PrimerNombre;
                            vFuncionarioICBF.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vFuncionarioICBF.SegundoNombre;
                            vFuncionarioICBF.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vFuncionarioICBF.PrimerApellido;
                            vFuncionarioICBF.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vFuncionarioICBF.SegundoApellido;
                            vFuncionarioICBF.Email = vDataReaderResults["Email"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Email"].ToString()) : vFuncionarioICBF.Email;
                            vFuncionarioICBF.Resolucion = vDataReaderResults["Resolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Resolucion"].ToString()) : vFuncionarioICBF.Resolucion;
                            vFuncionarioICBF.NoContarto = vDataReaderResults["NoContarto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NoContarto"].ToString()) : vFuncionarioICBF.NoContarto;
                            vFuncionarioICBF.NoTelefonoCelular = vDataReaderResults["NoTelefonoCelular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NoTelefonoCelular"].ToString()) : vFuncionarioICBF.NoTelefonoCelular;
                            vFuncionarioICBF.NoTelefonoFijo = vDataReaderResults["NoTelefonoFijo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NoTelefonoFijo"].ToString()) : vFuncionarioICBF.NoTelefonoFijo;
                            vFuncionarioICBF.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFuncionarioICBF.Estado;
                            vFuncionarioICBF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFuncionarioICBF.UsuarioCrea;
                            vFuncionarioICBF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFuncionarioICBF.FechaCrea;
                            vFuncionarioICBF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFuncionarioICBF.UsuarioModifica;
                            vFuncionarioICBF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFuncionarioICBF.FechaModifica;
                        }
                        return vFuncionarioICBF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdRegionalPCI">Valor entero con el Id de la Regional , permite valores nulos</param>
        /// <param name="pIdUsuario">Valor entero con el Id del Usuario, permite valores nulos</param>
        /// <param name="pEstado">Valor booleano</param>
        /// <returns>Lista de la Instancia que contiene la información del funcionario del ICBF</returns>
        public List<FuncionarioICBF> ConsultarFuncionarioICBFs(int? pIdRegionalPCI, int? pIdUsuario, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioICBFs_Consultar"))
                {
                         
                    if(pIdRegionalPCI != null)////////////////
                         vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pIdRegionalPCI);
                    if (pIdUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                         
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FuncionarioICBF> vListaFuncionarioICBF = new List<FuncionarioICBF>();
                        while (vDataReaderResults.Read())
                        {
                                FuncionarioICBF vFuncionarioICBF = new FuncionarioICBF();
                            vFuncionarioICBF.IdFuncionario = vDataReaderResults["IdFuncionario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFuncionario"].ToString()) : vFuncionarioICBF.IdFuncionario;
                            vFuncionarioICBF.IdDependencia = vDataReaderResults["IdDependencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependencia"].ToString()) : vFuncionarioICBF.IdDependencia;
                            vFuncionarioICBF.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vFuncionarioICBF.IdRegionalPCI;
                            vFuncionarioICBF.NombreRegional = vDataReaderResults["DescripcionPCI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionPCI"].ToString()) : vFuncionarioICBF.NombreRegional;
                            vFuncionarioICBF.IdTipoVinculacon = vDataReaderResults["IdTipoVinculacon"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoVinculacon"].ToString()) : vFuncionarioICBF.IdTipoVinculacon;
                            vFuncionarioICBF.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vFuncionarioICBF.IdTercero;
                            vFuncionarioICBF.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vFuncionarioICBF.IdUsuario;
                            vFuncionarioICBF.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vFuncionarioICBF.NumeroIdentificacion;
                            vFuncionarioICBF.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vFuncionarioICBF.PrimerNombre;
                            vFuncionarioICBF.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vFuncionarioICBF.SegundoNombre;
                            vFuncionarioICBF.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vFuncionarioICBF.PrimerApellido;
                            vFuncionarioICBF.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vFuncionarioICBF.SegundoApellido;
                            vFuncionarioICBF.Resolucion = vDataReaderResults["Resolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Resolucion"].ToString()) : vFuncionarioICBF.Resolucion;
                            vFuncionarioICBF.NoContarto = vDataReaderResults["NoContarto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NoContarto"].ToString()) : vFuncionarioICBF.NoContarto;
                            vFuncionarioICBF.NoTelefonoCelular = vDataReaderResults["NoTelefonoCelular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NoTelefonoCelular"].ToString()) : vFuncionarioICBF.NoTelefonoCelular;
                            vFuncionarioICBF.NoTelefonoFijo = vDataReaderResults["NoTelefonoFijo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NoTelefonoFijo"].ToString()) : vFuncionarioICBF.NoTelefonoFijo;
                            vFuncionarioICBF.Email = vDataReaderResults["Email"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Email"].ToString()) : vFuncionarioICBF.Email;
                            vFuncionarioICBF.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFuncionarioICBF.Estado;
                            vFuncionarioICBF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFuncionarioICBF.UsuarioCrea;
                            vFuncionarioICBF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFuncionarioICBF.FechaCrea;
                            vFuncionarioICBF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFuncionarioICBF.UsuarioModifica;
                            vFuncionarioICBF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFuncionarioICBF.FechaModifica;
                                vListaFuncionarioICBF.Add(vFuncionarioICBF);
                        }
                        return vListaFuncionarioICBF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
