﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Common;
//using System.Linq;
//using System.Text;

//using Icbf.Contrato.Entity;
//using Icbf.Utilities.Exceptions;
//using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;




namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros 
    /// para la entidad TipoFormaPago
    /// </summary>
    public class TipoFormaPagoDAL : GeneralDAL
    {
        public TipoFormaPagoDAL()
        {
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Metodo que permite consultar la entidad TipoFormaPago filtrando los resultados por los filtros
        /// codigoTipoFormaPago, nombreTipoFormaPago, descripcion, estado
        /// </summary>
        /// <param name="pCodigoTipoFormaPago">Cadena de texto con el codigo del tipo forma pago</param>
        /// <param name="pNombreTipoFormaPago">Cadena de texto con el nombre del tipo forma pago</param>
        /// <param name="pDescripcion">Cadena de texto con la descripción del tipo forma pago</param>
        /// <param name="pEstado">Booleano con el estado del tipo forma de pago</param>
        /// <returns>Listado de entidades TipoFormaPago con la información encontrada en la base de datos</returns>
        public List<TipoFormaPago> ConsultarTipoMedioPagos(String pCodigoTipoFormaPago, String pNombreTipoFormaPago, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATOS_TipoFormaPago_Consultar"))
                {
                    if (pCodigoTipoFormaPago != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoTipoFormaPago", DbType.String, pCodigoTipoFormaPago);
                    if (pNombreTipoFormaPago != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoFormaPago", DbType.String, pNombreTipoFormaPago);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoFormaPago> vListavTipoFormaPago = new List<TipoFormaPago>();
                        while (vDataReaderResults.Read())
                        {
                            TipoFormaPago vTipoFormaPago = new TipoFormaPago();
                            vTipoFormaPago.IdTipoFormaPago = vDataReaderResults["IdTipoFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoFormaPago"].ToString()) : vTipoFormaPago.IdTipoFormaPago;
                            vTipoFormaPago.CodigoTipoFormaPago = vDataReaderResults["CodigoTipoFormaPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoFormaPago"].ToString()) : vTipoFormaPago.CodigoTipoFormaPago;
                            vTipoFormaPago.NombreTipoFormaPago = vDataReaderResults["NombreTipoFormaPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoFormaPago"].ToString()) : vTipoFormaPago.NombreTipoFormaPago;
                            vTipoFormaPago.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoFormaPago.Descripcion;
                            vTipoFormaPago.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoFormaPago.Estado;
                            vTipoFormaPago.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoFormaPago.UsuarioCrea;
                            vTipoFormaPago.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoFormaPago.FechaCrea;
                            vTipoFormaPago.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoFormaPago.UsuarioModifica;
                            vTipoFormaPago.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoFormaPago.FechaModifica;
                            vListavTipoFormaPago.Add(vTipoFormaPago);
                        }
                        return vListavTipoFormaPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int InsertarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoFormaPago_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoFormaPago", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoFormaPago", DbType.String, pTipoFormaPago.NombreTipoFormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoFormaPago.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoFormaPago.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoFormaPago.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoFormaPago.IdTipoFormaPago = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoFormaPago").ToString());
                    GenerarLogAuditoria(pTipoFormaPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int ModificarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoFormaPago_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoFormaPago", DbType.Int32, pTipoFormaPago.IdTipoFormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoFormaPago", DbType.String, pTipoFormaPago.NombreTipoFormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoFormaPago.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoFormaPago.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoFormaPago.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoFormaPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int EliminarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoFormaPago_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoFormaPago", DbType.Int32, pTipoFormaPago.IdTipoFormaPago);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoFormaPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pIdTipoFormaPago"></param>
        /// <returns></returns>
        public TipoFormaPago ConsultarTipoFormaPago(int pIdTipoFormaPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoFormaPago_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoFormaPago", DbType.Int32, pIdTipoFormaPago);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoFormaPago vTipoFormaPago = new TipoFormaPago();
                        while (vDataReaderResults.Read())
                        {
                            vTipoFormaPago.IdTipoFormaPago = vDataReaderResults["IdTipoFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoFormaPago"].ToString()) : vTipoFormaPago.IdTipoFormaPago;
                            vTipoFormaPago.NombreTipoFormaPago = vDataReaderResults["NombreTipoFormaPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoFormaPago"].ToString()) : vTipoFormaPago.NombreTipoFormaPago;
                            vTipoFormaPago.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoFormaPago.Descripcion;
                            vTipoFormaPago.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoFormaPago.Estado;
                            vTipoFormaPago.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoFormaPago.UsuarioCrea;
                            vTipoFormaPago.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoFormaPago.FechaCrea;
                            vTipoFormaPago.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoFormaPago.UsuarioModifica;
                            vTipoFormaPago.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoFormaPago.FechaModifica;
                        }
                        return vTipoFormaPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pNombreTipoFormaPago"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoFormaPago> ConsultarTipoFormaPagos(String pNombreTipoFormaPago, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoFormaPagos_Consultar"))
                {
                    if (pNombreTipoFormaPago != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoFormaPago", DbType.String, pNombreTipoFormaPago);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoFormaPago> vListaTipoFormaPago = new List<TipoFormaPago>();
                        while (vDataReaderResults.Read())
                        {
                            TipoFormaPago vTipoFormaPago = new TipoFormaPago();
                            vTipoFormaPago.IdTipoFormaPago = vDataReaderResults["IdTipoFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoFormaPago"].ToString()) : vTipoFormaPago.IdTipoFormaPago;
                            vTipoFormaPago.NombreTipoFormaPago = vDataReaderResults["NombreTipoFormaPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoFormaPago"].ToString()) : vTipoFormaPago.NombreTipoFormaPago;
                            vTipoFormaPago.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoFormaPago.Descripcion;
                            vTipoFormaPago.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoFormaPago.Estado;
                            if (vTipoFormaPago.Estado)
                            {
                                vTipoFormaPago.EstadoString = "Activo";
                            }
                            else
                            {
                                vTipoFormaPago.EstadoString = "Inactivo";
                            }
                            vTipoFormaPago.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoFormaPago.UsuarioCrea;
                            vTipoFormaPago.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoFormaPago.FechaCrea;
                            vTipoFormaPago.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoFormaPago.UsuarioModifica;
                            vTipoFormaPago.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoFormaPago.FechaModifica;
                            vListaTipoFormaPago.Add(vTipoFormaPago);
                        }
                        return vListaTipoFormaPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
