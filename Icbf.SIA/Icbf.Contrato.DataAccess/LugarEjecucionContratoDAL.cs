using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad LugarEjecucionContrato
    /// </summary>
    public class LugarEjecucionContratoDAL : GeneralDAL
    {
        public LugarEjecucionContratoDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int InsertarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugarEjecucionContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdLugarEjecucionContratos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pLugarEjecucionContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdsRegionales", DbType.String, pLugarEjecucionContrato.IdsRegionales);
                    vDataBase.AddInParameter(vDbCommand, "@NivelNacional", DbType.Boolean, pLugarEjecucionContrato.NivelNacional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pLugarEjecucionContrato.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@DatosAdicionales", DbType.String, pLugarEjecucionContrato.DatosAdicionales);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pLugarEjecucionContrato.IdLugarEjecucionContratos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdLugarEjecucionContratos").ToString());
                    GenerarLogAuditoria(pLugarEjecucionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int ModificarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugarEjecucionContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdLugarEjecucionContratos", DbType.Int32, pLugarEjecucionContrato.IdLugarEjecucionContratos);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pLugarEjecucionContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pLugarEjecucionContrato.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pLugarEjecucionContrato.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@NivelNacional", DbType.Boolean, pLugarEjecucionContrato.NivelNacional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pLugarEjecucionContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pLugarEjecucionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int EliminarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugarEjecucionContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdLugarEjecucionContratos", DbType.Int32, pLugarEjecucionContrato.IdLugarEjecucionContratos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pLugarEjecucionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// M�todo de consulta por id para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdLugarEjecucionContratos"></param>
        public LugarEjecucionContrato ConsultarLugarEjecucionContrato(int pIdLugarEjecucionContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugarEjecucionContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdLugarEjecucionContratos", DbType.Int32, pIdLugarEjecucionContratos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();
                        while (vDataReaderResults.Read())
                        {
                            vLugarEjecucionContrato.IdLugarEjecucionContratos = vDataReaderResults["IdLugarEjecucionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucionContratos"].ToString()) : vLugarEjecucionContrato.IdLugarEjecucionContratos;
                            vLugarEjecucionContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vLugarEjecucionContrato.IdContrato;
                            vLugarEjecucionContrato.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vLugarEjecucionContrato.IdDepartamento;
                            vLugarEjecucionContrato.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vLugarEjecucionContrato.IdRegional;
                            vLugarEjecucionContrato.NivelNacional = vDataReaderResults["NivelNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NivelNacional"].ToString()) : vLugarEjecucionContrato.NivelNacional;
                            vLugarEjecucionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucionContrato.UsuarioCrea;
                            vLugarEjecucionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucionContrato.FechaCrea;
                            vLugarEjecucionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucionContrato.UsuarioModifica;
                            vLugarEjecucionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucionContrato.FechaModifica;
                        }
                        return vLugarEjecucionContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdRegional"></param>
        /// <param name="pNivelNacional"></param>
        public List<LugarEjecucionContrato> ConsultarLugarEjecucionContratos(int? pIdContrato, int? pIdDepartamento, int? pIdRegional, Boolean? pNivelNacional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugarEjecucionContratos_Consultar"))
                {
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pIdDepartamento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pNivelNacional != null)
                        vDataBase.AddInParameter(vDbCommand, "@NivelNacional", DbType.Boolean, pNivelNacional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<LugarEjecucionContrato> vListaLugarEjecucionContrato = new List<LugarEjecucionContrato>();
                        while (vDataReaderResults.Read())
                        {
                            LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();
                            vLugarEjecucionContrato.IdLugarEjecucionContratos = vDataReaderResults["IdLugarEjecucionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucionContratos"].ToString()) : vLugarEjecucionContrato.IdLugarEjecucionContratos;
                            vLugarEjecucionContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vLugarEjecucionContrato.IdContrato;
                            vLugarEjecucionContrato.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vLugarEjecucionContrato.IdDepartamento;
                            vLugarEjecucionContrato.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vLugarEjecucionContrato.IdRegional;
                            vLugarEjecucionContrato.NivelNacional = vDataReaderResults["NivelNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NivelNacional"].ToString()) : vLugarEjecucionContrato.NivelNacional;
                            vLugarEjecucionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucionContrato.UsuarioCrea;
                            vLugarEjecucionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucionContrato.FechaCrea;
                            vLugarEjecucionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucionContrato.UsuarioModifica;
                            vLugarEjecucionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucionContrato.FechaModifica;
                            vListaLugarEjecucionContrato.Add(vLugarEjecucionContrato);
                        }
                        return vListaLugarEjecucionContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Obtiene los lugares de ejecuci�n asociados a un contrato
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado con los lugares obtenidos de la base de datos</returns>
        public List<LugarEjecucionContrato> ConsultarLugaresEjecucionContrato(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugaresEjecucionContrato_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@DatosAdicionales", DbType.String, 2000);
                    vDataBase.AddOutParameter(vDbCommand, "@EsNivelNacional", DbType.Boolean, 1);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string datosAdicionales = vDataBase.GetParameterValue(vDbCommand, "@DatosAdicionales") != null ? vDataBase.GetParameterValue(vDbCommand, "@DatosAdicionales").ToString() : string.Empty;

                        bool esNivelNacional = vDataBase.GetParameterValue(vDbCommand, "@EsNivelNacional") != null ? bool.Parse(vDataBase.GetParameterValue(vDbCommand, "@EsNivelNacional").ToString()) : false;

                        List<LugarEjecucionContrato> vListaLugarEjecucionContrato = new List<LugarEjecucionContrato>();
                        LugarEjecucionContrato vLugarEjecucionContrato;

                        if(esNivelNacional)
                        {
                            vLugarEjecucionContrato = new LugarEjecucionContrato();
                            vLugarEjecucionContrato.EsNivelNacional = esNivelNacional;
                            vListaLugarEjecucionContrato.Add(vLugarEjecucionContrato);
                        }
                        else
                        {
                            while(vDataReaderResults.Read())
                            {
                                vLugarEjecucionContrato = new LugarEjecucionContrato();
                                vLugarEjecucionContrato.IdLugarEjecucionContratos = vDataReaderResults["IdLugarEjecucionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucionContratos"].ToString()) : vLugarEjecucionContrato.IdLugarEjecucionContratos;
                                vLugarEjecucionContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vLugarEjecucionContrato.IdContrato;
                                vLugarEjecucionContrato.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vLugarEjecucionContrato.IdDepartamento;
                                vLugarEjecucionContrato.Departamento = vDataReaderResults["Departamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Departamento"].ToString()) : vLugarEjecucionContrato.Departamento;
                                vLugarEjecucionContrato.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vLugarEjecucionContrato.IdMunicipio;
                                vLugarEjecucionContrato.Municipio = vDataReaderResults["Municipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Municipio"].ToString()) : vLugarEjecucionContrato.Municipio;
                                vLugarEjecucionContrato.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vLugarEjecucionContrato.IdRegional;
                                vLugarEjecucionContrato.NivelNacional = vDataReaderResults["NivelNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NivelNacional"].ToString()) : vLugarEjecucionContrato.NivelNacional;
                                vLugarEjecucionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucionContrato.UsuarioCrea;
                                vLugarEjecucionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucionContrato.FechaCrea;
                                vLugarEjecucionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucionContrato.UsuarioModifica;
                                vLugarEjecucionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucionContrato.FechaModifica;
                                vLugarEjecucionContrato.Historico = vDataReaderResults["Historico"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Historico"].ToString()) : vLugarEjecucionContrato.Historico;
                                vLugarEjecucionContrato.CodigoMunicipio = vDataReaderResults["CodigoMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoMunicipio"].ToString()) : vLugarEjecucionContrato.CodigoMunicipio;
                                vLugarEjecucionContrato.CodigoDepartamento = vDataReaderResults["CodigoDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDepartamento"].ToString()) : vLugarEjecucionContrato.CodigoDepartamento;
                                vLugarEjecucionContrato.Historico = vDataReaderResults["Historico"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Historico"].ToString()) : vLugarEjecucionContrato.Historico;

                                vLugarEjecucionContrato.EsNivelNacional = false;
                                if(vListaLugarEjecucionContrato.Count == 0)
                                    vLugarEjecucionContrato.DatosAdicionales = datosAdicionales;

                                vListaLugarEjecucionContrato.Add(vLugarEjecucionContrato);
                            }
                        }

                        return vListaLugarEjecucionContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<LugarEjecucionContrato> ConsultarLugaresEjecucionContratoHistorico(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugaresEjecucionContratoHistorico_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                   

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                       

                        List<LugarEjecucionContrato> vListaLugarEjecucionContrato = new List<LugarEjecucionContrato>();
                        LugarEjecucionContrato vLugarEjecucionContrato;

                        
                            while (vDataReaderResults.Read())
                            {
                                vLugarEjecucionContrato = new LugarEjecucionContrato();
                                vLugarEjecucionContrato.IdLugarEjecucionContratos = vDataReaderResults["IdLugarEjecucionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucionContratos"].ToString()) : vLugarEjecucionContrato.IdLugarEjecucionContratos;
                                vLugarEjecucionContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vLugarEjecucionContrato.IdContrato;
                                vLugarEjecucionContrato.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vLugarEjecucionContrato.IdDepartamento;
                                vLugarEjecucionContrato.Departamento = vDataReaderResults["Departamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Departamento"].ToString()) : vLugarEjecucionContrato.Departamento;
                                vLugarEjecucionContrato.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vLugarEjecucionContrato.IdMunicipio;
                                vLugarEjecucionContrato.Municipio = vDataReaderResults["Municipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Municipio"].ToString()) : vLugarEjecucionContrato.Municipio;
                                vLugarEjecucionContrato.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vLugarEjecucionContrato.IdRegional;
                                vLugarEjecucionContrato.NivelNacional = vDataReaderResults["NivelNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NivelNacional"].ToString()) : vLugarEjecucionContrato.NivelNacional;
                                vLugarEjecucionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucionContrato.UsuarioCrea;
                                vLugarEjecucionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucionContrato.FechaCrea;
                                vLugarEjecucionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucionContrato.UsuarioModifica;
                                vLugarEjecucionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucionContrato.FechaModifica;
                                vLugarEjecucionContrato.Historico = vDataReaderResults["Historico"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Historico"].ToString()) : vLugarEjecucionContrato.Historico;
                                //vLugarEjecucionContrato.EsNivelNacional = false;                               

                                vListaLugarEjecucionContrato.Add(vLugarEjecucionContrato);
                            }
                        

                        return vListaLugarEjecucionContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
