using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Secuencia n�mero proceso
    /// </summary>
    public class SecuenciaNumeroProcesoDAL : GeneralDAL
    {
        public SecuenciaNumeroProcesoDAL()
        {
        }
        /// <summary>
        /// M�todo que Genera el Numero de Proceso
        /// </summary>
        /// <param name="pSecuenciaNumeroProceso"></param>
        /// <returns></returns>
        public string GenerarNumeroProceso(SecuenciaNumeroProceso pSecuenciaNumeroProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@NumeroProceso", DbType.String, 25);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.Int32, pSecuenciaNumeroProceso.CodigoRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pSecuenciaNumeroProceso.Sigla);
                    vDataBase.AddInParameter(vDbCommand, "@AnoVigencia", DbType.Int32, pSecuenciaNumeroProceso.AnoVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSecuenciaNumeroProceso.UsuarioCrea);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    pSecuenciaNumeroProceso.NumeroProceso = vDataBase.GetParameterValue(vDbCommand, "@NumeroProceso").ToString();
                    GenerarLogAuditoria(pSecuenciaNumeroProceso, vDbCommand);
                    return pSecuenciaNumeroProceso.NumeroProceso;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        
    }
}
