using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad UnidadCalculo
    /// </summary>
    public class UnidadCalculoDAL : GeneralDAL
    {
        public UnidadCalculoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int InsertarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_UnidadCalculo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDUnidadCalculo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodUnidadCalculo", DbType.String, pUnidadCalculo.CodUnidadCalculo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pUnidadCalculo.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pUnidadCalculo.Inactivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUnidadCalculo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pUnidadCalculo.IDUnidadCalculo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDUnidadCalculo").ToString());
                    GenerarLogAuditoria(pUnidadCalculo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int ModificarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_UnidadCalculo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDUnidadCalculo", DbType.Int32, pUnidadCalculo.IDUnidadCalculo);
                    vDataBase.AddInParameter(vDbCommand, "@CodUnidadCalculo", DbType.String, pUnidadCalculo.CodUnidadCalculo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pUnidadCalculo.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pUnidadCalculo.Inactivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUnidadCalculo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUnidadCalculo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int EliminarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_UnidadCalculo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDUnidadCalculo", DbType.Int32, pUnidadCalculo.IDUnidadCalculo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUnidadCalculo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pIDUnidadCalculo"></param>
        public UnidadCalculo ConsultarUnidadCalculo(int pIDUnidadCalculo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_UnidadCalculo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDUnidadCalculo", DbType.Int32, pIDUnidadCalculo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        UnidadCalculo vUnidadCalculo = new UnidadCalculo();
                        while (vDataReaderResults.Read())
                        {
                            vUnidadCalculo.IDUnidadCalculo = vDataReaderResults["IDUnidadCalculo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUnidadCalculo"].ToString()) : vUnidadCalculo.IDUnidadCalculo;
                            vUnidadCalculo.CodUnidadCalculo = vDataReaderResults["CodUnidadCalculo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodUnidadCalculo"].ToString()) : vUnidadCalculo.CodUnidadCalculo;
                            vUnidadCalculo.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vUnidadCalculo.Descripcion;
                            vUnidadCalculo.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vUnidadCalculo.Inactivo;
                            vUnidadCalculo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vUnidadCalculo.UsuarioCrea;
                            vUnidadCalculo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vUnidadCalculo.FechaCrea;
                            vUnidadCalculo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vUnidadCalculo.UsuarioModifica;
                            vUnidadCalculo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vUnidadCalculo.FechaModifica;
                        }
                        return vUnidadCalculo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pCodUnidadCalculo"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pInactivo"></param>
        public List<UnidadCalculo> ConsultarUnidadCalculos(String pCodUnidadCalculo, String pDescripcion, Boolean? pInactivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_UnidadCalculos_Consultar"))
                {
                    if(pCodUnidadCalculo != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodUnidadCalculo", DbType.String, pCodUnidadCalculo);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pInactivo != null)
                         vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pInactivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<UnidadCalculo> vListaUnidadCalculo = new List<UnidadCalculo>();
                        while (vDataReaderResults.Read())
                        {
                                UnidadCalculo vUnidadCalculo = new UnidadCalculo();
                            vUnidadCalculo.IDUnidadCalculo = vDataReaderResults["IDUnidadCalculo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUnidadCalculo"].ToString()) : vUnidadCalculo.IDUnidadCalculo;
                            vUnidadCalculo.CodUnidadCalculo = vDataReaderResults["CodUnidadCalculo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodUnidadCalculo"].ToString()) : vUnidadCalculo.CodUnidadCalculo;
                            vUnidadCalculo.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vUnidadCalculo.Descripcion;
                            vUnidadCalculo.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vUnidadCalculo.Inactivo;
                            vUnidadCalculo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vUnidadCalculo.UsuarioCrea;
                            vUnidadCalculo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vUnidadCalculo.FechaCrea;
                            vUnidadCalculo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vUnidadCalculo.UsuarioModifica;
                            vUnidadCalculo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vUnidadCalculo.FechaModifica;
                                vListaUnidadCalculo.Add(vUnidadCalculo);
                        }
                        return vListaUnidadCalculo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
