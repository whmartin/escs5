using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad ArchivosGarantias
    /// </summary>
    public class ArchivosGarantiasDAL : GeneralDAL
    {
        public ArchivosGarantiasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int InsertarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ArchivosGarantias_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDArchivosGarantias", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDArchivo", DbType.Int32, pArchivosGarantias.IDArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pArchivosGarantias.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pArchivosGarantias.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pArchivosGarantias.IDArchivosGarantias = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDArchivosGarantias").ToString());
                    GenerarLogAuditoria(pArchivosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int ModificarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ArchivosGarantias_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDArchivosGarantias", DbType.Int32, pArchivosGarantias.IDArchivosGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@IDArchivo", DbType.Int32, pArchivosGarantias.IDArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pArchivosGarantias.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pArchivosGarantias.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pArchivosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int EliminarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ArchivosGarantias_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDArchivosGarantias", DbType.Int32, pArchivosGarantias.IDArchivosGarantias);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pArchivosGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pIDArchivosGarantias"></param>
        public ArchivosGarantias ConsultarArchivosGarantias(int pIDArchivosGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ArchivosGarantias_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDArchivosGarantias", DbType.Int32, pIDArchivosGarantias);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ArchivosGarantias vArchivosGarantias = new ArchivosGarantias();
                        while (vDataReaderResults.Read())
                        {
                            vArchivosGarantias.IDArchivosGarantias = vDataReaderResults["IDArchivosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDArchivosGarantias"].ToString()) : vArchivosGarantias.IDArchivosGarantias;
                            vArchivosGarantias.IDArchivo = vDataReaderResults["IDArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDArchivo"].ToString()) : vArchivosGarantias.IDArchivo;
                            vArchivosGarantias.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vArchivosGarantias.IDGarantia;
                            vArchivosGarantias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivosGarantias.UsuarioCrea;
                            vArchivosGarantias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivosGarantias.FechaCrea;
                            vArchivosGarantias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivosGarantias.UsuarioModifica;
                            vArchivosGarantias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivosGarantias.FechaModifica;
                        }
                        return vArchivosGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pIDArchivo"></param>
        /// <param name="pIDGarantia"></param>
        public List<ArchivosGarantias> ConsultarArchivosGarantiass(int? pIDArchivo, int? pIDGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ArchivosGarantiass_Consultar"))
                {
                    if(pIDArchivo != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDArchivo", DbType.Int32, pIDArchivo);
                    if(pIDGarantia != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pIDGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivosGarantias> vListaArchivosGarantias = new List<ArchivosGarantias>();
                        while (vDataReaderResults.Read())
                        {
                                ArchivosGarantias vArchivosGarantias = new ArchivosGarantias();
                            vArchivosGarantias.IDArchivosGarantias = vDataReaderResults["IDArchivosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDArchivosGarantias"].ToString()) : vArchivosGarantias.IDArchivosGarantias;
                            vArchivosGarantias.IDArchivo = vDataReaderResults["IDArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDArchivo"].ToString()) : vArchivosGarantias.IDArchivo;
                            vArchivosGarantias.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vArchivosGarantias.NombreArchivoOri;
                            vArchivosGarantias.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivosGarantias.NombreArchivo;
                            vArchivosGarantias.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vArchivosGarantias.IDGarantia;
                            vArchivosGarantias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivosGarantias.UsuarioCrea;
                            vArchivosGarantias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivosGarantias.FechaCrea;
                            vArchivosGarantias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivosGarantias.UsuarioModifica;
                            vArchivosGarantias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivosGarantias.FechaModifica;
                                vListaArchivosGarantias.Add(vArchivosGarantias);
                        }
                        return vListaArchivosGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
