using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de la capa de acceso que contiene los métodos para Insertar, Modificar, Eliminar y Consultar registros de los cargos de los funcionarios del ICBF
    /// </summary>
    public class FuncionarioCargosICBFDAL : GeneralDAL
    {
        public FuncionarioCargosICBFDAL()
        {
        }
        /// <summary>
        /// Gonet
        /// Insertar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioCargosICBF">Instancia que contiene la información de los cargos funcionarios del ICBF</param>
        /// <returns>Identificador de la base de datos del registro insertado</returns>
        public int InsertarFuncionarioCargosICBF(FuncionarioCargosICBF pFuncionarioCargosICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioCargosICBF_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdFuncCargo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdCargo", DbType.Int32, pFuncionarioCargosICBF.IdCargo);
                    vDataBase.AddInParameter(vDbCommand, "@IdFuncionario", DbType.Int32, pFuncionarioCargosICBF.IdFuncionario);
                    vDataBase.AddInParameter(vDbCommand, "@ResolucionNombramiento", DbType.String, pFuncionarioCargosICBF.ResolucionNombramiento);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pFuncionarioCargosICBF.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFuncionarioCargosICBF.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFuncionarioCargosICBF.IdFuncCargo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdFuncCargo").ToString());
                    GenerarLogAuditoria(pFuncionarioCargosICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Modificar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioCargosICBF">Instancia que contiene la información de los cargos funcionarios del ICBF</param>
        /// <returns>Identificador de la base de datos del registro modificado</returns>
        public int ModificarFuncionarioCargosICBF(FuncionarioCargosICBF pFuncionarioCargosICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioCargosICBF_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFuncCargo", DbType.Int32, pFuncionarioCargosICBF.IdFuncCargo);
                    vDataBase.AddInParameter(vDbCommand, "@IdCargo", DbType.Int32, pFuncionarioCargosICBF.IdCargo);
                    vDataBase.AddInParameter(vDbCommand, "@IdFuncionario", DbType.Int32, pFuncionarioCargosICBF.IdFuncionario);
                    vDataBase.AddInParameter(vDbCommand, "@ResolucionNombramiento", DbType.String, pFuncionarioCargosICBF.ResolucionNombramiento);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pFuncionarioCargosICBF.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFuncionarioCargosICBF.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFuncionarioCargosICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioCargosICBF">Instancia que contiene la información de los cargos funcionarios del ICBF</param>
        /// <returns>Identificador de la base de datos del registro eliminado</returns>
        public int EliminarFuncionarioCargosICBF(FuncionarioCargosICBF pFuncionarioCargosICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioCargosICBF_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFuncCargo", DbType.Int32, pFuncionarioCargosICBF.IdFuncCargo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFuncionarioCargosICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar cargo funcionario del ICBF por Id
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdFuncCargo">Valor entero con el Id cargo del funcionario</param>
        /// <returns>Instancia que contiene la información de los cargos funcionarios del ICBF</returns>
        public FuncionarioCargosICBF ConsultarFuncionarioCargosICBF(int pIdFuncCargo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioCargosICBF_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFuncCargo", DbType.Int32, pIdFuncCargo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        FuncionarioCargosICBF vFuncionarioCargosICBF = new FuncionarioCargosICBF();
                        while (vDataReaderResults.Read())
                        {
                            vFuncionarioCargosICBF.IdFuncCargo = vDataReaderResults["IdFuncCargo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFuncCargo"].ToString()) : vFuncionarioCargosICBF.IdFuncCargo;
                            vFuncionarioCargosICBF.IdCargo = vDataReaderResults["IdCargo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargo"].ToString()) : vFuncionarioCargosICBF.IdCargo;
                            vFuncionarioCargosICBF.IdFuncionario = vDataReaderResults["IdFuncionario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFuncionario"].ToString()) : vFuncionarioCargosICBF.IdFuncionario;
                            vFuncionarioCargosICBF.ResolucionNombramiento = vDataReaderResults["ResolucionNombramiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResolucionNombramiento"].ToString()) : vFuncionarioCargosICBF.ResolucionNombramiento;
                            vFuncionarioCargosICBF.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFuncionarioCargosICBF.Estado;
                            vFuncionarioCargosICBF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFuncionarioCargosICBF.UsuarioCrea;
                            vFuncionarioCargosICBF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFuncionarioCargosICBF.FechaCrea;
                            vFuncionarioCargosICBF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFuncionarioCargosICBF.UsuarioModifica;
                            vFuncionarioCargosICBF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFuncionarioCargosICBF.FechaModifica;
                        }
                        return vFuncionarioCargosICBF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdCargo">Valor entero con el Id del cargo, permite valores nulos</param>
        /// <param name="pIdFuncionario">Valor entero con el Id del funcionario, permite valores nulos</param>
        /// <param name="pResolucionNombramiento">Cadena de texto con la resolución del nombramiento</param>
        /// <param name="pEstado">Cadena de texto con el estado</param>
        /// <returns>Lista con la Instancia que contiene la información los cargos funcionarios del ICBF</returns>
        public List<FuncionarioCargosICBF> ConsultarFuncionarioCargosICBFs(int? pIdCargo, int? pIdFuncionario, String pResolucionNombramiento, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioCargosICBFs_Consultar"))
                {
                    if (pIdCargo != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCargo", DbType.Int32, pIdCargo);
                    if (pIdFuncionario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdFuncionario", DbType.Int32, pIdFuncionario);
                    if (pResolucionNombramiento != null)
                        vDataBase.AddInParameter(vDbCommand, "@ResolucionNombramiento", DbType.String, pResolucionNombramiento);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FuncionarioCargosICBF> vListaFuncionarioCargosICBF = new List<FuncionarioCargosICBF>();
                        while (vDataReaderResults.Read())
                        {
                            FuncionarioCargosICBF vFuncionarioCargosICBF = new FuncionarioCargosICBF();
                            vFuncionarioCargosICBF.IdFuncCargo = vDataReaderResults["IdFuncCargo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFuncCargo"].ToString()) : vFuncionarioCargosICBF.IdFuncCargo;
                            vFuncionarioCargosICBF.IdCargo = vDataReaderResults["IdCargo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargo"].ToString()) : vFuncionarioCargosICBF.IdCargo;
                            vFuncionarioCargosICBF.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vFuncionarioCargosICBF.Nombre;
                            vFuncionarioCargosICBF.IdFuncionario = vDataReaderResults["IdFuncionario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFuncionario"].ToString()) : vFuncionarioCargosICBF.IdFuncionario;
                            vFuncionarioCargosICBF.NombreFuncionario = vDataReaderResults["NombreFuncionario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFuncionario"].ToString()) : vFuncionarioCargosICBF.NombreFuncionario;
                            vFuncionarioCargosICBF.ResolucionNombramiento = vDataReaderResults["ResolucionNombramiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResolucionNombramiento"].ToString()) : vFuncionarioCargosICBF.ResolucionNombramiento;
                            vFuncionarioCargosICBF.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFuncionarioCargosICBF.Estado;
                            vFuncionarioCargosICBF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFuncionarioCargosICBF.UsuarioCrea;
                            vFuncionarioCargosICBF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFuncionarioCargosICBF.FechaCrea;
                            vFuncionarioCargosICBF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFuncionarioCargosICBF.UsuarioModifica;
                            vFuncionarioCargosICBF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFuncionarioCargosICBF.FechaModifica;
                            vListaFuncionarioCargosICBF.Add(vFuncionarioCargosICBF);
                        }
                        return vListaFuncionarioCargosICBF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdCargo">Valor entero con el Id del cargo, permite valores nulos</param>
        /// <param name="pIdFuncionario">Valor entero con el Id del funcionario, permite valores nulos</param>
        /// <param name="pResolucionNombramiento">Cadena de texto con la resolución del nombramiento</param>
        /// <param name="pEstado">Cadena de texto con el estado</param>
        /// <returns>Lista con la Instancia que contiene la información los cargos funcionarios del ICBF</returns>
        public List<FuncionarioCargosICBF> ConsultarFuncionarioCargosICBFs(int? pIdCargo, int? pIdFuncionario, String pResolucionNombramiento, Boolean? pEstado, Boolean? pEstadoFunc)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_FuncionarioCargosICBFs_Consultar"))
                {
                    if(pIdCargo != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdCargo", DbType.Int32, pIdCargo);
                    if(pIdFuncionario != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdFuncionario", DbType.Int32, pIdFuncionario);
                    if(pResolucionNombramiento != null)
                         vDataBase.AddInParameter(vDbCommand, "@ResolucionNombramiento", DbType.String, pResolucionNombramiento);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (pEstadoFunc != null)
                        vDataBase.AddInParameter(vDbCommand, "@EstadoFun", DbType.Boolean, pEstadoFunc);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FuncionarioCargosICBF> vListaFuncionarioCargosICBF = new List<FuncionarioCargosICBF>();
                        while (vDataReaderResults.Read())
                        {
                                FuncionarioCargosICBF vFuncionarioCargosICBF = new FuncionarioCargosICBF();
                            vFuncionarioCargosICBF.IdFuncCargo = vDataReaderResults["IdFuncCargo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFuncCargo"].ToString()) : vFuncionarioCargosICBF.IdFuncCargo;
                            vFuncionarioCargosICBF.IdCargo = vDataReaderResults["IdCargo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargo"].ToString()) : vFuncionarioCargosICBF.IdCargo;
                            vFuncionarioCargosICBF.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vFuncionarioCargosICBF.Nombre;
                            vFuncionarioCargosICBF.IdFuncionario = vDataReaderResults["IdFuncionario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFuncionario"].ToString()) : vFuncionarioCargosICBF.IdFuncionario;
                            vFuncionarioCargosICBF.NombreFuncionario = vDataReaderResults["NombreFuncionario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFuncionario"].ToString()) : vFuncionarioCargosICBF.NombreFuncionario;
                            vFuncionarioCargosICBF.ResolucionNombramiento = vDataReaderResults["ResolucionNombramiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResolucionNombramiento"].ToString()) : vFuncionarioCargosICBF.ResolucionNombramiento;
                            vFuncionarioCargosICBF.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFuncionarioCargosICBF.Estado;
                            vFuncionarioCargosICBF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFuncionarioCargosICBF.UsuarioCrea;
                            vFuncionarioCargosICBF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFuncionarioCargosICBF.FechaCrea;
                            vFuncionarioCargosICBF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFuncionarioCargosICBF.UsuarioModifica;
                            vFuncionarioCargosICBF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFuncionarioCargosICBF.FechaModifica;
                                vListaFuncionarioCargosICBF.Add(vFuncionarioCargosICBF);
                        }
                        return vListaFuncionarioCargosICBF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
