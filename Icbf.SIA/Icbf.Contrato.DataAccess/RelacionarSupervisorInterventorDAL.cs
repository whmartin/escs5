using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad relaci�n de supervisores e interventores
    /// </summary>
    public class RelacionarSupervisorInterventorDAL : GeneralDAL
    {
        public RelacionarSupervisorInterventorDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int InsertarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDSupervisorInterv", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDContratoSupervisa", DbType.Int32, pRelacionarSupervisorInterventor.IDContratoSupervisa);
                    vDataBase.AddInParameter(vDbCommand, "@OrigenTipoSupervisor", DbType.Boolean, pRelacionarSupervisorInterventor.OrigenTipoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoSupvInterventor", DbType.Int32, pRelacionarSupervisorInterventor.IDTipoSupvInterventor);
                    vDataBase.AddInParameter(vDbCommand, "@IDTerceroExterno", DbType.Int32, pRelacionarSupervisorInterventor.IDTerceroExterno);
                    vDataBase.AddInParameter(vDbCommand, "@IDFuncionarioInterno", DbType.Int32, pRelacionarSupervisorInterventor.IDFuncionarioInterno);
                    vDataBase.AddInParameter(vDbCommand, "@IDContratoInterventoria", DbType.Int32, pRelacionarSupervisorInterventor.IDContratoInterventoria);
                    vDataBase.AddInParameter(vDbCommand, "@IDTerceroInterventoria", DbType.Int32, pRelacionarSupervisorInterventor.IDDirectorInterventoria);
                    vDataBase.AddInParameter(vDbCommand, "@TipoVinculacion", DbType.Boolean, pRelacionarSupervisorInterventor.TipoVinculacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicia", DbType.DateTime, pRelacionarSupervisorInterventor.FechaInicia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinaliza", DbType.DateTime, pRelacionarSupervisorInterventor.FechaFinaliza);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRelacionarSupervisorInterventor.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModificaInterventor", DbType.DateTime, pRelacionarSupervisorInterventor.FechaModificaInterventor);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRelacionarSupervisorInterventor.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRelacionarSupervisorInterventor.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRelacionarSupervisorInterventor.IDSupervisorInterv = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDSupervisorInterv").ToString());
                    GenerarLogAuditoria(pRelacionarSupervisorInterventor, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int ModificarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorInterv", DbType.Int32, pRelacionarSupervisorInterventor.IDSupervisorInterv);
                    vDataBase.AddInParameter(vDbCommand, "@IDContratoSupervisa", DbType.Int32, pRelacionarSupervisorInterventor.IDContratoSupervisa);
                    vDataBase.AddInParameter(vDbCommand, "@OrigenTipoSupervisor", DbType.Boolean, pRelacionarSupervisorInterventor.OrigenTipoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoSupvInterventor", DbType.Int32, pRelacionarSupervisorInterventor.IDTipoSupvInterventor);
                    vDataBase.AddInParameter(vDbCommand, "@IDTerceroExterno", DbType.Int32, pRelacionarSupervisorInterventor.IDTerceroExterno);
                    vDataBase.AddInParameter(vDbCommand, "@IDFuncionarioInterno", DbType.Int32, pRelacionarSupervisorInterventor.IDFuncionarioInterno);
                    vDataBase.AddInParameter(vDbCommand, "@IDContratoInterventoria", DbType.Int32, pRelacionarSupervisorInterventor.IDContratoInterventoria);
                    vDataBase.AddInParameter(vDbCommand, "@IDTerceroInterventoria", DbType.Int32, pRelacionarSupervisorInterventor.IDDirectorInterventoria);
                    vDataBase.AddInParameter(vDbCommand, "@TipoVinculacion", DbType.Boolean, pRelacionarSupervisorInterventor.TipoVinculacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicia", DbType.DateTime, pRelacionarSupervisorInterventor.FechaInicia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinaliza", DbType.DateTime, pRelacionarSupervisorInterventor.FechaFinaliza);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRelacionarSupervisorInterventor.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModificaInterventor", DbType.DateTime, pRelacionarSupervisorInterventor.FechaModificaInterventor);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRelacionarSupervisorInterventor.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRelacionarSupervisorInterventor, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int EliminarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorInterv", DbType.Int32, pRelacionarSupervisorInterventor.IDSupervisorInterv);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRelacionarSupervisorInterventor, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pIDSupervisorInterv"></param>
        /// <returns></returns>
        public RelacionarSupervisorInterventor ConsultarRelacionarSupervisorInterventor(int pIDSupervisorInterv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorInterv", DbType.Int32, pIDSupervisorInterv);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RelacionarSupervisorInterventor vRelacionarSupervisorInterventor = new RelacionarSupervisorInterventor();
                        while (vDataReaderResults.Read())
                        {
                            vRelacionarSupervisorInterventor.IDSupervisorInterv = vDataReaderResults["IDSupervisorInterv"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorInterv"].ToString()) : vRelacionarSupervisorInterventor.IDSupervisorInterv;
                            vRelacionarSupervisorInterventor.IDContratoSupervisa = vDataReaderResults["IDContratoSupervisa"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContratoSupervisa"].ToString()) : vRelacionarSupervisorInterventor.IDContratoSupervisa;
                            vRelacionarSupervisorInterventor.OrigenTipoSupervisor = vDataReaderResults["OrigenTipoSupervisor"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["OrigenTipoSupervisor"].ToString()) : vRelacionarSupervisorInterventor.OrigenTipoSupervisor;
                            vRelacionarSupervisorInterventor.IDTipoSupvInterventor = vDataReaderResults["IDTipoSupvInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSupvInterventor"].ToString()) : vRelacionarSupervisorInterventor.IDTipoSupvInterventor;
                            vRelacionarSupervisorInterventor.IDTerceroExterno = vDataReaderResults["IDTerceroExterno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTerceroExterno"].ToString()) : vRelacionarSupervisorInterventor.IDTerceroExterno;
                            vRelacionarSupervisorInterventor.IDFuncionarioInterno = vDataReaderResults["IDFuncionarioInterno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDFuncionarioInterno"].ToString()) : vRelacionarSupervisorInterventor.IDFuncionarioInterno;
                            vRelacionarSupervisorInterventor.IDContratoInterventoria = vDataReaderResults["IDContratoInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContratoInterventoria"].ToString()) : vRelacionarSupervisorInterventor.IDContratoInterventoria;
                            vRelacionarSupervisorInterventor.TipoVinculacion = vDataReaderResults["TipoVinculacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TipoVinculacion"].ToString()) : vRelacionarSupervisorInterventor.TipoVinculacion;
                            vRelacionarSupervisorInterventor.FechaInicia = vDataReaderResults["FechaInicia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicia"].ToString()) : vRelacionarSupervisorInterventor.FechaInicia;
                            vRelacionarSupervisorInterventor.FechaFinaliza = vDataReaderResults["FechaFinaliza"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinaliza"].ToString()) : vRelacionarSupervisorInterventor.FechaFinaliza;
                            vRelacionarSupervisorInterventor.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRelacionarSupervisorInterventor.Estado;
                            vRelacionarSupervisorInterventor.FechaModificaInterventor = vDataReaderResults["FechaModificaInterventor"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificaInterventor"].ToString()) : vRelacionarSupervisorInterventor.FechaModificaInterventor;
                            vRelacionarSupervisorInterventor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRelacionarSupervisorInterventor.FechaCrea;
                            vRelacionarSupervisorInterventor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? vDataReaderResults["UsuarioCrea"].ToString() : vRelacionarSupervisorInterventor.UsuarioCrea;
                            vRelacionarSupervisorInterventor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRelacionarSupervisorInterventor.FechaModifica;
                            vRelacionarSupervisorInterventor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? vDataReaderResults["UsuarioModifica"].ToString() : vRelacionarSupervisorInterventor.UsuarioModifica;
                            vRelacionarSupervisorInterventor.IDDirectorInterventoria = vDataReaderResults["IDTerceroInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTerceroInterventoria"].ToString()) : vRelacionarSupervisorInterventor.IDDirectorInterventoria;
                        }
                        return vRelacionarSupervisorInterventor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pOrigenTipoSupervisor"></param>
        /// <param name="pIDTipoSupvInterventor"></param>
        /// <param name="pIDTerceroExterno"></param>
        /// <param name="pTipoVinculacion"></param>
        /// <param name="pFechaInicia"></param>
        /// <param name="pFechaFinaliza"></param>
        /// <param name="pEstado"></param>
        /// <param name="pFechaModificaInterventor"></param>
        /// <returns></returns>
        public List<RelacionarSupervisorInterventor> ConsultarRelacionarSupervisorInterventors(bool pOrigenTipoSupervisor, int? pIDTipoSupvInterventor, int? pIDTerceroExterno,bool  pTipoVinculacion, DateTime? pFechaInicia, DateTime? pFechaFinaliza,bool  pEstado, DateTime? pFechaModificaInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar"))
                {
                    if(pOrigenTipoSupervisor != null)
                         vDataBase.AddInParameter(vDbCommand, "@OrigenTipoSupervisor", DbType.Boolean, pOrigenTipoSupervisor);
                    if(pIDTipoSupvInterventor != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDTipoSupvInterventor", DbType.Int32, pIDTipoSupvInterventor);
                    if(pIDTerceroExterno != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDTerceroExterno", DbType.Int32, pIDTerceroExterno);
                    if(pTipoVinculacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@TipoVinculacion", DbType.Boolean, pTipoVinculacion);
                    if(pFechaInicia != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaInicia", DbType.DateTime, pFechaInicia);
                    if(pFechaFinaliza != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaFinaliza", DbType.DateTime, pFechaFinaliza);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if(pFechaModificaInterventor != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaModificaInterventor", DbType.DateTime, pFechaModificaInterventor);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RelacionarSupervisorInterventor> vListaRelacionarSupervisorInterventor = new List<RelacionarSupervisorInterventor>();
                        while (vDataReaderResults.Read())
                        {
                                RelacionarSupervisorInterventor vRelacionarSupervisorInterventor = new RelacionarSupervisorInterventor();
                            vRelacionarSupervisorInterventor.IDSupervisorInterv = vDataReaderResults["IDSupervisorInterv"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorInterv"].ToString()) : vRelacionarSupervisorInterventor.IDSupervisorInterv;
                            vRelacionarSupervisorInterventor.IDContratoSupervisa = vDataReaderResults["IDContratoSupervisa"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContratoSupervisa"].ToString()) : vRelacionarSupervisorInterventor.IDContratoSupervisa;
                            vRelacionarSupervisorInterventor.OrigenTipoSupervisor = vDataReaderResults["OrigenTipoSupervisor"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["OrigenTipoSupervisor"].ToString()) : vRelacionarSupervisorInterventor.OrigenTipoSupervisor;
                            vRelacionarSupervisorInterventor.IDTipoSupvInterventor = vDataReaderResults["IDTipoSupvInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSupvInterventor"].ToString()) : vRelacionarSupervisorInterventor.IDTipoSupvInterventor;
                            vRelacionarSupervisorInterventor.IDTerceroExterno = vDataReaderResults["IDTerceroExterno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTerceroExterno"].ToString()) : vRelacionarSupervisorInterventor.IDTerceroExterno;
                            vRelacionarSupervisorInterventor.IDFuncionarioInterno = vDataReaderResults["IDFuncionarioInterno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDFuncionarioInterno"].ToString()) : vRelacionarSupervisorInterventor.IDFuncionarioInterno;
                            vRelacionarSupervisorInterventor.IDContratoInterventoria = vDataReaderResults["IDContratoInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContratoInterventoria"].ToString()) : vRelacionarSupervisorInterventor.IDContratoInterventoria;
                            vRelacionarSupervisorInterventor.TipoVinculacion = vDataReaderResults["TipoVinculacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TipoVinculacion"].ToString()) : vRelacionarSupervisorInterventor.TipoVinculacion;
                            vRelacionarSupervisorInterventor.FechaInicia = vDataReaderResults["FechaInicia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicia"].ToString()) : vRelacionarSupervisorInterventor.FechaInicia;
                            vRelacionarSupervisorInterventor.FechaFinaliza = vDataReaderResults["FechaFinaliza"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinaliza"].ToString()) : vRelacionarSupervisorInterventor.FechaFinaliza;
                            vRelacionarSupervisorInterventor.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRelacionarSupervisorInterventor.Estado;
                            vRelacionarSupervisorInterventor.FechaModificaInterventor = vDataReaderResults["FechaModificaInterventor"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificaInterventor"].ToString()) : vRelacionarSupervisorInterventor.FechaModificaInterventor;
                            vRelacionarSupervisorInterventor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRelacionarSupervisorInterventor.FechaCrea;
                            vRelacionarSupervisorInterventor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? vDataReaderResults["UsuarioCrea"].ToString() : vRelacionarSupervisorInterventor.UsuarioCrea;
                            vRelacionarSupervisorInterventor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRelacionarSupervisorInterventor.FechaModifica;
                            vRelacionarSupervisorInterventor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? vDataReaderResults["UsuarioModifica"].ToString() : vRelacionarSupervisorInterventor.UsuarioModifica;
                            vRelacionarSupervisorInterventor.IDDirectorInterventoria = vDataReaderResults["IDTerceroInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTerceroInterventoria"].ToString()) : vRelacionarSupervisorInterventor.IDDirectorInterventoria;
                            vListaRelacionarSupervisorInterventor.Add(vRelacionarSupervisorInterventor);
                        }
                        return vListaRelacionarSupervisorInterventor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
