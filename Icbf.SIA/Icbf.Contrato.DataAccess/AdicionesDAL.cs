using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class AdicionesDAL : GeneralDAL
    {
        public AdicionesDAL()
        {
        }
        public int InsertarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Adiciones_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAdicion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Int32, pAdiciones.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionAdicion", DbType.String, pAdiciones.JustificacionAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pAdiciones.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdicion", DbType.DateTime, pAdiciones.FechaAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pAdiciones.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAdiciones.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAdiciones.IdAdicion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAdicion").ToString());
                    GenerarLogAuditoria(pAdiciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Adiciones_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pAdiciones.IdAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Int32, pAdiciones.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionAdicion", DbType.String, pAdiciones.JustificacionAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pAdiciones.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdicion", DbType.DateTime, pAdiciones.FechaAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pAdiciones.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAdiciones.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdiciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        
        public int EliminarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Adiciones_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pAdiciones.IdAdicion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdiciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarAdiciones(int  IdPlanComprasContrato, string IdProducto, int idAdiccion, int idProductoPlanCompras, string tipoProducto, decimal ValorActualAdiccion )
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_ModificarValorProductoAdicion"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, IdPlanComprasContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdProducto", DbType.String, IdProducto);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, idAdiccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdProductoPlanCompras", DbType.Int32, idProductoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@TipoProducto", DbType.String, tipoProducto);
                    vDataBase.AddInParameter(vDbCommand, "@ValorActualAdicion", DbType.Decimal, ValorActualAdiccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
       
        public Adiciones ConsultarAdiciones(int pIdAdicion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Adiciones_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pIdAdicion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Adiciones vAdiciones = new Adiciones();
                        while (vDataReaderResults.Read())
                        {
                            vAdiciones.IdCDP = vDataReaderResults["IdCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCDP"].ToString()) : vAdiciones.IdCDP;
                            vAdiciones.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vAdiciones.IdAdicion;
                            vAdiciones.ValorAdicion = vDataReaderResults["ValorAdicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicion"].ToString()) : vAdiciones.ValorAdicion;
                            vAdiciones.JustificacionAdicion = vDataReaderResults["JustificacionAdicion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionAdicion"].ToString()) : vAdiciones.JustificacionAdicion;
                            vAdiciones.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vAdiciones.Estado;
                            vAdiciones.FechaAdicion = vDataReaderResults["FechaAdicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdicion"].ToString()) : vAdiciones.FechaAdicion;
                            vAdiciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vAdiciones.IDDetalleConsModContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;
                            vAdiciones.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vAdiciones.ValorCDP;
                            vAdiciones.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vAdiciones.ValorRP;
                        }
                        return vAdiciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Adiciones> ConsultarAdicioness(int? pValorAdicion, String pJustificacionAdicion, int? pEstado, DateTime? pFechaAdicion, int? pIDDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Adicioness_Consultar"))
                {
                    if(pValorAdicion != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Int32, pValorAdicion);
                    if(pJustificacionAdicion != null)
                         vDataBase.AddInParameter(vDbCommand, "@JustificacionAdicion", DbType.String, pJustificacionAdicion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    if(pFechaAdicion != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaAdicion", DbType.DateTime, pFechaAdicion);
                    if(pIDDetalleConsModContractual != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pIDDetalleConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Adiciones> vListaAdiciones = new List<Adiciones>();
                        while (vDataReaderResults.Read())
                        {
                                Adiciones vAdiciones = new Adiciones();
                            vAdiciones.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vAdiciones.IdAdicion;
                            vAdiciones.ValorAdicion = vDataReaderResults["ValorAdicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicion"].ToString()) : vAdiciones.ValorAdicion;
                            vAdiciones.JustificacionAdicion = vDataReaderResults["JustificacionAdicion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionAdicion"].ToString()) : vAdiciones.JustificacionAdicion;
                            vAdiciones.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vAdiciones.Estado;
                            vAdiciones.FechaAdicion = vDataReaderResults["FechaAdicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdicion"].ToString()) : vAdiciones.FechaAdicion;
                            vAdiciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vAdiciones.IDDetalleConsModContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;
                                vListaAdiciones.Add(vAdiciones);
                        }
                        return vListaAdiciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Adiciones> ConsultarAdicionesAprobadasContrato(int IdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Adiciones_ConsultarAprobadasContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Adiciones> vListaAdiciones = new List<Adiciones>();
                        while (vDataReaderResults.Read())
                        {
                            Adiciones vAdiciones = new Adiciones();
                            vAdiciones.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vAdiciones.IdAdicion;
                            vAdiciones.ValorAdicion = vDataReaderResults["ValorAdicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicion"].ToString()) : vAdiciones.ValorAdicion;
                            vAdiciones.JustificacionAdicion = vDataReaderResults["JustificacionAdicion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionAdicion"].ToString()) : vAdiciones.JustificacionAdicion;
                            vAdiciones.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vAdiciones.Estado;
                            vAdiciones.FechaAdicion = vDataReaderResults["FechaAdicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdicion"].ToString()) : vAdiciones.FechaAdicion;
                            vAdiciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vAdiciones.IDDetalleConsModContractual;
                            vAdiciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdiciones.UsuarioCrea;
                            vAdiciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdiciones.FechaCrea;
                            vAdiciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdiciones.UsuarioModifica;
                            vAdiciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdiciones.FechaModifica;
                            //vAdiciones.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vAdiciones.NumeroRP;
                            vAdiciones.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vAdiciones.ValorRP;
                            vAdiciones.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vAdiciones.ValorCDP;
                            vAdiciones.FechaSubscripcion = vDataReaderResults["FechaSubscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSubscripcion"].ToString()) : vAdiciones.FechaSubscripcion;
                            vListaAdiciones.Add(vAdiciones);
                        }
                        return vListaAdiciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorTotalAdicionado(int IdContrato)
        {
            decimal result = 0;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Adiciones_ConsultarValorTotalAprobadas"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@ValorAdicionado", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    var valor = vDataBase.GetParameterValue(vDbCommand, "@ValorAdicionado").ToString();
                    if (!string.IsNullOrEmpty(valor))
                        result = Convert.ToDecimal(valor);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public void ActualizarAdicionRP(int idAdicion, int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Adiciones_AprobarRP"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, idAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void ActualizarCesionRP(int idCesion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Cesiones_AprobarRP"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, idCesion);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
