﻿using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess
{
    public class ReportesDAL : GeneralDAL
    {
        private ContratosCDPDAL _contratosCDPDAL;
        private RPContratoDAL _RPDAL;
        private GarantiaDAL _GarantiaDAL;
        private AmparosGarantiasDAL _AmparosDAL;
        private ProrrogasDAL _ProrrogasDAL;
        private AdicionesDAL _AdicionesDAL;
        private Proveedores_ContratosDAL _IntegrantesUT;
        private AporteContratoDAL _AportesContratoDAL;
        private PlanComprasRubrosCDPDAL _RubrosDAL;
        private LugarEjecucionDAL _LugarEjecucionDAL;
        private ReduccionesDAL _ReduccionesDAL;
        private CesionesDAL _CesionesDAL;

        public ReportesDAL()
        {
            _contratosCDPDAL = new ContratosCDPDAL();
            _RPDAL = new RPContratoDAL();
            _GarantiaDAL = new GarantiaDAL();
            _ProrrogasDAL = new ProrrogasDAL();
            _AdicionesDAL = new AdicionesDAL();
            _IntegrantesUT = new Proveedores_ContratosDAL();
            _AmparosDAL = new AmparosGarantiasDAL();
            _AportesContratoDAL = new AporteContratoDAL();
            _RubrosDAL = new PlanComprasRubrosCDPDAL();
            _LugarEjecucionDAL = new LugarEjecucionDAL();
            _ReduccionesDAL = new ReduccionesDAL();
            _CesionesDAL = new CesionesDAL();
        }

        public List<ReporteFUC> ObtenerReporteFUC
            (
             DateTime fechaDesde,
             DateTime fechaHasta, 
             int vigencia, 
             string idRegional, 
             int? idModalidad, 
             int? idCategoria, 
             int? idTipoContrato, 
             int? idEstadoContrato, 
             int? idTipoPersona 
            )
        {

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_ReporteFUC"))
                {
                    vDbCommand.CommandTimeout = 3600;
                    vDataBase.AddInParameter(vDbCommand,     "@FechaRegistroSistemaDesde", DbType.DateTime, fechaDesde);
                    vDataBase.AddInParameter(vDbCommand,     "@FechaRegistroSistemaHasta", DbType.DateTime, fechaHasta);
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, vigencia);
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, idRegional);
                    if (idModalidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, idModalidad);
                    if (idCategoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoria", DbType.Int32, idCategoria);
                    if (idTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, idTipoContrato);
                    if (idEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, idEstadoContrato);
                    if (idTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, idTipoPersona);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.ReporteFUC> vListaContrato = new List<Contrato.Entity.ReporteFUC>();
                        while (vDataReaderResults.Read())
                        {
                            try
                            {


                            var idContratox = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idContrato"].ToString()) : -1;

                            if (!vListaContrato.Any(e1 => e1.idContrato == idContratox))
                            {
                                Contrato.Entity.ReporteFUC vContrato = new Contrato.Entity.ReporteFUC();
                                vContrato.idContrato = idContratox;
                                vContrato.VIgenciaInicial = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vContrato.VIgenciaInicial;
                                vContrato.Regional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContrato.Regional;
                                vContrato.CodigoRegional = vDataReaderResults["codigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["codigoRegional"].ToString()) : vContrato.CodigoRegional;
                                vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                                vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"].ToString()).ToShortDateString() : vContrato.FechaSuscripcion;
                                vContrato.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContrato.ModalidadSeleccion;
                                vContrato.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContrato.TipoContrato;
                                vContrato.CategoriaContato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContrato.CategoriaContato;
                                vContrato.CodigoTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vContrato.CodigoTipoPersona;
                                vContrato.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vContrato.CategoriaContato;
                                vContrato.NumeroIdentificacion = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NUMEROIDENTIFICACION"].ToString()) : vContrato.NumeroIdentificacion;
                                vContrato.DV = vDataReaderResults["digitoverificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["digitoverificacion"].ToString()) : vContrato.DV;
                                vContrato.NombreRazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vContrato.NombreRazonSocial;
                                vContrato.ProfesionContratista = vDataReaderResults["des_prof"] != DBNull.Value ? Convert.ToString(vDataReaderResults["des_prof"].ToString()) : vContrato.ProfesionContratista;
                                vContrato.GeneroContratista = vDataReaderResults["sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["sexo"].ToString()) : vContrato.GeneroContratista;
                                vContrato.DireccionContratista = vDataReaderResults["direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["direccion"].ToString()) : vContrato.DireccionContratista;
                                vContrato.TelefonoContratista = vDataReaderResults["telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["telefono"].ToString()) : vContrato.TelefonoContratista;
                                vContrato.LugarUbicacionContratista = vDataReaderResults["LugarUbicacionCotratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LugarUbicacionCotratista"].ToString()) : vContrato.LugarUbicacionContratista;
                                vContrato.TipoOrganizacion = vDataReaderResults["ClaseEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntidad"].ToString()) : vContrato.TipoOrganizacion;
                                vContrato.IdentificacionRepresentantelegal = vDataReaderResults["NumeroIdentificacionRL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionRL"].ToString()) : vContrato.IdentificacionRepresentantelegal;
                                vContrato.NombreRepresentantelegal = vDataReaderResults["NombresRL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombresRL"].ToString()) : vContrato.NombreRepresentantelegal;
                                vContrato.AfectacionRecurso = vDataReaderResults["manejarecurso"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["manejarecurso"].ToString()) ? "SI" : "NO" : vContrato.AfectacionRecurso;
                                vContrato.ObjetoContrato = vDataReaderResults["ObjetodelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetodelContrato"].ToString()) : vContrato.ObjetoContrato;
                                vContrato.FechaInicioContrato = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()).ToShortDateString() : vContrato.FechaInicioContrato;
                                vContrato.FechaTerminacionIncial = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"]).ToShortDateString() : vContrato.FechaTerminacionIncial;
                                vContrato.DiasIniciales = vDataReaderResults["DiasIniciales"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasIniciales"]) : vContrato.DiasIniciales;
                                vContrato.Anticipos = vDataReaderResults["Anticipo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Anticipo"]) ? "SI" : "NO" : vContrato.Anticipos;
                                vContrato.NumeroVigenciaFutura = vDataReaderResults["NumeroVigenciaFutura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroVigenciaFutura"].ToString()) : vContrato.NumeroVigenciaFutura;
                                vContrato.ValorVigenciaFutura = vDataReaderResults["ValorVigenciaFutura"] != DBNull.Value ? string.Format("{0:C3}",Convert.ToDecimal(vDataReaderResults["ValorVigenciaFutura"])) : vContrato.ValorVigenciaFutura;

                                vContrato.OrdenadorGastoDocumentoIdentificacion = vDataReaderResults["IdentificacionOrdenador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionOrdenador"]) : vContrato.OrdenadorGastoDocumentoIdentificacion;
                                vContrato.OrdenadorGastoNombreCompleto = vDataReaderResults["NombresOrdenador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombresOrdenador"]) : vContrato.OrdenadorGastoNombreCompleto;
                                vContrato.OrdenadorGastoArea = vDataReaderResults["DependenciaOrdenador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaOrdenador"]) : vContrato.OrdenadorGastoArea;
                                
                                vContrato.SupervisorNumeroDocumentoIdentificacion = vDataReaderResults["IdentificacionSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionSupervisor"]) : vContrato.SupervisorNumeroDocumentoIdentificacion;
                                vContrato.SupervisorNombreCompleto = vDataReaderResults["NombresSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombresSupervisor"]) : vContrato.SupervisorNombreCompleto;
                                vContrato.SupervisorArea = vDataReaderResults["DependenciaSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSupervisor"]) : vContrato.SupervisorArea;
                                vContrato.SupervisorRol = vDataReaderResults["RolSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RolSupervisor"]) : vContrato.SupervisorRol;


                                vContrato.AportesDinero = vDataReaderResults["AportesDineroICBF"] != DBNull.Value ?   Convert.ToDecimal(vDataReaderResults["AportesDineroICBF"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.AportesDinero;
                                vContrato.AportesEspecie = vDataReaderResults["AportesEspecieICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["AportesEspecieICBF"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.AportesEspecie;

                                decimal valorTotalAportes = (!string.IsNullOrEmpty(vContrato.AportesDinero)  ? Convert.ToDecimal(vDataReaderResults["AportesDineroICBF"]) : 0) +
                                                            (!string.IsNullOrEmpty(vContrato.AportesEspecie) ? Convert.ToDecimal(vDataReaderResults["AportesEspecieICBF"]): 0);

                                vContrato.ValorTotalICBF = valorTotalAportes.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));

                                decimal valorContratoInicial =  vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Decimal.Parse(vDataReaderResults["ValorInicialContrato"].ToString()) :0;

                                vContrato.ValorTotalContratoImcial = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? valorContratoInicial.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.ValorTotalContratoImcial;
                                vContrato.ValorTotalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"]).ToString("C3",System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.ValorTotalContrato;

                                vContrato.FechaFinalTerminacion = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()).ToShortDateString() : vContrato.FechaFinalTerminacion;

                                vContrato.DiasFinales = vDataReaderResults["DiasFinales"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasFinales"]) : vContrato.DiasFinales;
                                vContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"]) : vContrato.EstadoContrato;

                                vContrato.CodigoSECOP = vDataReaderResults["CodigoSECOP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSECOP"]) : vContrato.CodigoSECOP;
                                vContrato.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"]) : vContrato.NumeroProceso;
                                vContrato.FechaAdjudicacionProceso = vDataReaderResults["FechaAdjudicacionProceso"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacionProceso"]).ToShortDateString() : vContrato.FechaAdjudicacionProceso;
                                vContrato.PorcentajeAnticipo = vDataReaderResults["PorcentajeAnticipo"] != DBNull.Value ? Math.Round( Convert.ToDecimal(vDataReaderResults["PorcentajeAnticipo"]),2) + "%" : vContrato.PorcentajeAnticipo;
                                vContrato.NumeroPACCO = vDataReaderResults["PlanCompras"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PlanCompras"]) : vContrato.NumeroPACCO;
                                vContrato.Rubro = vDataReaderResults["Rubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Rubro"]) : vContrato.Rubro;
                                vContrato.UrlSECOP = vDataReaderResults["UrlSECOP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UrlSECOP"]) : vContrato.UrlSECOP;

                                var misCDPS = _contratosCDPDAL.ConsultarContratosCDP(vContrato.idContrato);

                                if (misCDPS.Count > 0)
                                {
                                    foreach (var item in misCDPS)
                                    {
                                        vContrato.NumeroCDP += item.NumeroCDP.ToString() + "\n\r";
                                        vContrato.FechaCDP += item.FechaCDP.HasValue ? item.FechaCDP.Value.ToShortDateString() + "\n\r" : "\n\r";
                                        vContrato.ValorCDP +=  item.ValorCDP.Value.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) + "\n\r";
                                    }
                                }

                                var misRPS = _RPDAL.ConsultarRPContratosAsociados(vContrato.idContrato, null);

                                if (misRPS.Count > 0)
                                {
                                    foreach (var item in misRPS)
                                    {
                                        vContrato.NumeroRP += item.NumeroRP.ToString() + "\n\r";
                                        vContrato.FechaRP  += item.FechaExpedicionRP.HasValue ? item.FechaExpedicionRP.Value.ToShortDateString() + "\n\r" : "\n\r";
                                        vContrato.ValorRP  += item.ValorRP.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) + "\n\r";
                                    }
                                }

                                var garantias = _GarantiaDAL.ConsultarInfoGarantias(vContrato.idContrato);

                                if (garantias.Count > 0)
                                {
                                    foreach (var item in garantias)
	                                {
                                        vContrato.FechaGarantia += item.FechaExpedicionGarantia.HasValue ? item.FechaExpedicionGarantia.Value.ToShortDateString() + "\n\r" : "\n\r";
                                        vContrato.TipoGarantia += item.NombreTipoGarantia + "\n\r";
                                        vContrato.EntidadAseguradora += item.EntidadAseguradora + "\n\r";
                                        var Porcentaje = valorContratoInicial > 0 ? Math.Round(decimal.Parse(item.ValorGarantia.Replace("$",string.Empty)) * 100 / valorContratoInicial,2):0;
                                        vContrato.Porcentaje += Porcentaje + " %" + "\n\r";
                                        vContrato.ValorAsegurado += decimal.Parse(item.ValorGarantia.Replace("$", string.Empty)).ToString("C3",System.Globalization.CultureInfo.GetCultureInfo("es-co")) + "\n\r";

                                        var amparos = _AmparosDAL.ConsultarAmparosGarantiass(null, null, null, null, item.IDGarantia);

                                        if (amparos.Count > 0)
                                        {
                                            foreach (var itemAmparo in amparos)
                                            {
                                                vContrato.Riesgo1 += itemAmparo.NombreTipoAmparo + "\n\r";
                                                vContrato.RiesgoValor1 += itemAmparo.ValorAsegurado.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) + "\n\r";
                                                var porcentaje = valorContratoInicial > 0 ? Math.Round(itemAmparo.ValorAsegurado * 100 / valorContratoInicial,2) : 0;
                                                vContrato.RiesgoPorcentaje += Porcentaje + " %" + "\n\r";
                                            }  
                                        }
	                                }
                                }

                                var cofinanciadores = _AportesContratoDAL.ObtenerCofinanciadores(vContrato.idContrato);

                                if (cofinanciadores.Count > 0)
	                            {
                                    int i = 0;
                                        decimal valorApoetesContratista = 0;

                                    foreach (var itemCofinanciador in cofinanciadores)
                                    {
                                        if (i == 0)
                                        {
                                            vContrato.Cofinanciador = itemCofinanciador.InformacionAportante;
                                            vContrato.CofinanciadorIdentificacion = itemCofinanciador.NumeroIdentificacionContratista;
                                            vContrato.CofinanciacionValor =
                                                 string.Format
                                                 (
                                                 "{0}/{1}",
                                                 itemCofinanciador.ValorAporte.ToString("C3",System.Globalization.CultureInfo.GetCultureInfo("es-co")),
                                                 itemCofinanciador.ValorAporteE.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"))
                                                 );
                                                
                                        }
                                        else  if (i == 1)
                                        {
                                            vContrato.Cofinanciador1 = itemCofinanciador.InformacionAportante;
                                            vContrato.CofinanciadorIdentificacion1 = itemCofinanciador.NumeroIdentificacionContratista;
                                            vContrato.CofinanciacionValor1 =
                                                 string.Format
                                                 (
                                                 "{0}/{1}",
                                                 itemCofinanciador.ValorAporte.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")),
                                                 itemCofinanciador.ValorAporteE.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"))
                                                 );
                                        }
                                        else if (i == 2)
                                        {
                                            vContrato.Cofinanciador2 = itemCofinanciador.InformacionAportante;
                                            vContrato.CofinanciadorIdentificacion2 = itemCofinanciador.NumeroIdentificacionContratista;
                                            vContrato.CofinanciacionValor2 =
                                                 string.Format
                                                 (
                                                 "{0}/{1}",
                                                 itemCofinanciador.ValorAporte.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")),
                                                 itemCofinanciador.ValorAporteE.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"))
                                                 );
                                        }

                                            valorApoetesContratista += itemCofinanciador.ValorAporte + itemCofinanciador.ValorAporteE;

                                            i++;
                                    }
                                        vContrato.ValorTotalCofinanciacion = string.Format(valorApoetesContratista.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")));
	                            }

                                var rubros = _RubrosDAL.ObtenerRubrosContrato(vContrato.idContrato);

                                if (rubros.Count > 0)
	                            {
                                    foreach (var itemrubro in rubros)
                                        vContrato.Rubro += itemrubro.CodigoRubro + "\n\r";
	                            }

                                var Lugares = _LugarEjecucionDAL.ConsultarLugarEjecucion(vContrato.idContrato);

                                if (Lugares.Count > 0)
                                {
                                    foreach (var itemlugar in Lugares)
                                        vContrato.LugarEjecucionContrato += itemlugar + "\n\r";
                                }
                                else
                                vContrato.LugarEjecucionContrato = "Sede Nacional";

                                var prorrogas = _ProrrogasDAL.ConsultarProrrogasAprobadasContrato(vContrato.idContrato);

                                if (prorrogas.Count > 0)
                                {
                                    int i = 1;
                                    foreach (var item in prorrogas)
                                    {
                                        if (i == 1)
                                        {
                                            vContrato.FechaProrroga = item.FechaInicioView;
                                            vContrato.PlazoProrroga = (item.FechaFin - item.FechaInicio).Days;
                                        }
                                        else if (i == 2)
                                        {
                                            vContrato.FechaProrroga1 = item.FechaInicioView;
                                            vContrato.PlazoProrroga1 = (item.FechaFin - item.FechaInicio).Days;
                                        }
                                        else if (i == 3)
                                        {
                                            vContrato.FechaProrroga2 = item.FechaInicioView;
                                            vContrato.PlazoProrroga2 = (item.FechaFin - item.FechaInicio).Days;
                                        }

                                        i++;
                                    }

                                        vContrato.TotalDiasProrroga = vContrato.PlazoProrroga +
                                        vContrato.PlazoProrroga1 +
                                        vContrato.PlazoProrroga2; 
                                }

                                var adicion = _AdicionesDAL.ConsultarAdicionesAprobadasContrato(vContrato.idContrato);

                                if (adicion.Count > 0)
                                {
                                    int i = 1;
                                    decimal valorTotalAdicion= 0;
                                    foreach (var item in adicion)
                                    {
                                        if (i == 1)
                                        {
                                                vContrato.FechaAdicion = item.FechaSubscripcionView;
                                            vContrato.NumeroRPAdicion = item.NumeroRP;
                                            vContrato.ValorAdicion = item.ValorAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                        }
                                        else if (i == 2)
                                        {
                                            vContrato.FechaAdicion1 = item.FechaSubscripcionView;
                                            vContrato.NumeroRPAdicion1 = item.NumeroRP;
                                            vContrato.ValorAdicion1 = item.ValorAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                        }
                                        else if (i == 3)
                                        {
                                            vContrato.FechaAdicion2 = item.FechaSubscripcionView;
                                            vContrato.NumeroRPAdicion2 = item.NumeroRP;
                                            vContrato.ValorAdicion2 = item.ValorAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                        }
                                        else if (i == 4)
                                        {
                                            vContrato.FechaAdicion3 = item.FechaSubscripcionView;
                                            vContrato.NumeroRPAdicion3 = item.NumeroRP;
                                            vContrato.ValorAdicion3 = item.ValorAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                        }

                                        valorTotalAdicion += item.ValorAdicion;
                                        i++;
                                    }

                                    vContrato.TotalAdicion = valorTotalAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                }

                                var Reduccion = _ReduccionesDAL.ConsultarReduccionesAprobadasContrato(vContrato.idContrato);

                                    if (Reduccion.Count > 0)
                                    {
                                        int i = 1;
                                        decimal valorTotalReduccion = 0;
                                        foreach (var item in Reduccion)
                                        {
                                            valorTotalReduccion += item.ValorReduccion;
                                            i++;
                                        }

                                        vContrato.ReduccionValor = valorTotalReduccion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                    }

                                    var cesiones = _CesionesDAL.ConsultarInfoCesionesSuscritasReporte(vContrato.idContrato);
                                    if (cesiones.Count > 0)
                                    {
                                        vContrato.TieneCesion = "SI";

                                        int indice = 1;

                                        foreach (var item in cesiones)
                                        {
                                            if (indice == 1)
                                            {
                                                vContrato.FechaSuscripcionCesion1 = item.FechaSuscripcion.ToShortDateString();
                                                vContrato.RPCesion1 = item.IdRP.ToString();
                                                vContrato.ContratistaCedente1 = item.RepresentanteLegal;
                                                vContrato.NumeroIdentificacion1 = item.NumeroIdentificacion;
                                            }
                                            else if (indice == 2)
                                            {
                                                vContrato.FechaSuscripcionCesion2 = item.FechaSuscripcion.ToShortDateString();
                                                vContrato.RPCesion2 = item.IdRP.ToString();
                                                vContrato.ContratistaCedente2 = item.RepresentanteLegal;
                                                vContrato.NumeroIdentificacion2 = item.NumeroIdentificacion;
                                            }
                                            else if (indice == 3)
                                            {
                                                vContrato.FechaSuscripcionCesion3 = item.FechaSuscripcion.ToShortDateString();
                                                vContrato.RPCesion3 = item.IdRP.ToString();
                                                vContrato.ContratistaCedente3 = item.RepresentanteLegal;
                                                vContrato.NumeroIdentificacion3 = item.NumeroIdentificacion;
                                            }

                                            indice++;
                                        }
                                    }
                                    else
                                    {
                                        vContrato.TieneCesion = "NO";
                                    }

                                    var codigoPersona = vDataReaderResults["CodigoTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoPersona"].ToString()) : string.Empty;
                           
                                if (codigoPersona != "002" && codigoPersona != "001" && ! string.IsNullOrEmpty(codigoPersona))
                                {
                                    var Integrantes = _IntegrantesUT.ObtenerProveedoresContrato(vContrato.idContrato, 1);

                                    if (Integrantes.Count > 0)
                                    {
                                        int i = 1;

                                        foreach (var item in Integrantes)
                                        {
                                            if (i == 1)
                                            {
                                               vContrato.IntegranteConsorcionUnionTemporalNombre = item.Proveedor;
                                               vContrato.IntegranteConsorcionUnionTemporalIdentificacion = item.NumeroIdentificacionIntegrante;
                                               vContrato.IntegranteConsorcionUnionTemporalPorcentaje = item.PorcentajeParticipacion;
                                            }
                                            if (i == 2)
                                            {
                                                vContrato.IntegranteConsorcionUnionTemporalNombre1 = item.Proveedor;
                                                vContrato.IntegranteConsorcionUnionTemporalIdentificacion1 = item.NumeroIdentificacionIntegrante;
                                                vContrato.IntegranteConsorcionUnionTemporalPorcentaje1 = item.PorcentajeParticipacion;
                                            }
                                            if (i == 3)
                                            {
                                                vContrato.IntegranteConsorcionUnionTemporalNombre2 = item.Proveedor;
                                                vContrato.IntegranteConsorcionUnionTemporalIdentificacion2 = item.NumeroIdentificacionIntegrante;
                                                vContrato.IntegranteConsorcionUnionTemporalPorcentaje2 = item.PorcentajeParticipacion;
                                            }
                                        }
                                    }

                                }

                                vListaContrato.Add(vContrato);


                        }

                            }
                            catch (Exception ex)
                            {
                                var a1 = vDataReaderResults["idContrato"].ToString(); // ex.Message;
                            }

                        }
                        return vListaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


        }


        public List<ReporteFUC> ObtenerReporteFUCHistorico
            (
             DateTime fechaDesde,
             DateTime fechaHasta,
             int vigencia,
             string idRegional,
             int? idModalidad,
             int? idCategoria,
             int? idTipoContrato,
             int? idEstadoContrato,
             int? idTipoPersona
            )
        {

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_ReporteFUC_Historico"))
                {
                    vDbCommand.CommandTimeout = 3600;
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaDesde", DbType.DateTime, fechaDesde);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaHasta", DbType.DateTime, fechaHasta);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, idRegional);
                    if (idModalidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, idModalidad);
                    if (idCategoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoria", DbType.Int32, idCategoria);
                    if (idTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, idTipoContrato);
                    if (idEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, idEstadoContrato);
                    if (idTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, idTipoPersona);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.ReporteFUC> vListaContrato = new List<Contrato.Entity.ReporteFUC>();
                        while (vDataReaderResults.Read())
                        {
                            var idContratox = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idContrato"].ToString()) : -1;

                            if (!vListaContrato.Any(e1 => e1.idContrato == idContratox))
                            {
                                Contrato.Entity.ReporteFUC vContrato = new Contrato.Entity.ReporteFUC();
                                vContrato.idContrato = idContratox;
                                vContrato.VIgenciaInicial = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vContrato.VIgenciaInicial;
                                vContrato.Regional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContrato.Regional;
                                vContrato.CodigoRegional = vDataReaderResults["codigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["codigoRegional"].ToString()) : vContrato.CodigoRegional;
                                vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                                vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"].ToString()).ToShortDateString() : vContrato.FechaSuscripcion;
                                vContrato.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContrato.ModalidadSeleccion;
                                vContrato.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContrato.TipoContrato;
                                vContrato.CategoriaContato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContrato.CategoriaContato;
                                vContrato.CodigoTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vContrato.CodigoTipoPersona;
                                vContrato.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vContrato.CategoriaContato;
                                vContrato.NumeroIdentificacion = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NUMEROIDENTIFICACION"].ToString()) : vContrato.NumeroIdentificacion;
                                vContrato.DV = vDataReaderResults["digitoverificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["digitoverificacion"].ToString()) : vContrato.DV;
                                vContrato.NombreRazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vContrato.NombreRazonSocial;
                                vContrato.ProfesionContratista = vDataReaderResults["des_prof"] != DBNull.Value ? Convert.ToString(vDataReaderResults["des_prof"].ToString()) : vContrato.ProfesionContratista;
                                vContrato.GeneroContratista = vDataReaderResults["sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["sexo"].ToString()) : vContrato.GeneroContratista;
                                vContrato.DireccionContratista = vDataReaderResults["direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["direccion"].ToString()) : vContrato.DireccionContratista;
                                vContrato.TelefonoContratista = vDataReaderResults["telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["telefono"].ToString()) : vContrato.TelefonoContratista;
                                vContrato.LugarUbicacionContratista = vDataReaderResults["LugarUbicacionCotratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LugarUbicacionCotratista"].ToString()) : vContrato.LugarUbicacionContratista;
                                vContrato.TipoOrganizacion = vDataReaderResults["ClaseEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntidad"].ToString()) : vContrato.TipoOrganizacion;
                                vContrato.IdentificacionRepresentantelegal = vDataReaderResults["NumeroIdentificacionRL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionRL"].ToString()) : vContrato.IdentificacionRepresentantelegal;
                                vContrato.NombreRepresentantelegal = vDataReaderResults["NombresRL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombresRL"].ToString()) : vContrato.NombreRepresentantelegal;
                                vContrato.AfectacionRecurso = vDataReaderResults["manejarecurso"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["manejarecurso"].ToString()) ? "SI" : "NO" : vContrato.AfectacionRecurso;
                                vContrato.ObjetoContrato = vDataReaderResults["ObjetodelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetodelContrato"].ToString()) : vContrato.ObjetoContrato;
                                vContrato.FechaInicioContrato = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()).ToShortDateString() : vContrato.FechaInicioContrato;
                                vContrato.FechaTerminacionIncial = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"]).ToShortDateString() : vContrato.FechaTerminacionIncial;
                                vContrato.DiasIniciales = vDataReaderResults["DiasIniciales"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasIniciales"]) : vContrato.DiasIniciales;
                                vContrato.Anticipos = vDataReaderResults["Anticipo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Anticipo"]) ? "SI" : "NO" : vContrato.Anticipos;
                                vContrato.NumeroVigenciaFutura = vDataReaderResults["NumeroVigenciaFutura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroVigenciaFutura"].ToString()) : vContrato.NumeroVigenciaFutura;
                                vContrato.ValorVigenciaFutura = vDataReaderResults["ValorVigenciaFutura"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorVigenciaFutura"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.ValorVigenciaFutura;

                                vContrato.OrdenadorGastoDocumentoIdentificacion = vDataReaderResults["IdentificacionOrdenador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionOrdenador"]) : vContrato.OrdenadorGastoDocumentoIdentificacion;
                                vContrato.OrdenadorGastoNombreCompleto = vDataReaderResults["NombresOrdenador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombresOrdenador"]) : vContrato.OrdenadorGastoNombreCompleto;
                                vContrato.OrdenadorGastoArea = vDataReaderResults["DependenciaOrdenador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaOrdenador"]) : vContrato.OrdenadorGastoArea;

                                vContrato.SupervisorNumeroDocumentoIdentificacion = vDataReaderResults["IdentificacionSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionSupervisor"]) : vContrato.SupervisorNumeroDocumentoIdentificacion;
                                vContrato.SupervisorNombreCompleto = vDataReaderResults["NombresSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombresSupervisor"]) : vContrato.SupervisorNombreCompleto;
                                vContrato.SupervisorArea = vDataReaderResults["DependenciaSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSupervisor"]) : vContrato.SupervisorArea;

                                vContrato.AportesDinero = vDataReaderResults["AportesDineroICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["AportesDineroICBF"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.AportesDinero;
                                vContrato.AportesEspecie = vDataReaderResults["AportesEspecieICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["AportesEspecieICBF"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.AportesEspecie;

                                decimal valorContratoInicial = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Decimal.Parse(vDataReaderResults["ValorInicialContrato"].ToString()) : 0;
                                vContrato.ValorTotalContratoImcial = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? valorContratoInicial.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.ValorTotalContratoImcial;
                                vContrato.ValorTotalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.ValorTotalContrato;

                                vContrato.FechaFinalTerminacion = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()).ToShortDateString() : vContrato.FechaFinalTerminacion;

                                vContrato.DiasFinales = vDataReaderResults["DiasFinales"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasFinales"]) : vContrato.DiasFinales;
                                vContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"]) : vContrato.EstadoContrato;

                                vContrato.FechaGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"]).ToShortDateString() : vContrato.FechaGarantia;
                                vContrato.TipoGarantia = vDataReaderResults["TipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoGarantia"]) : vContrato.TipoGarantia;
                                vContrato.EntidadAseguradora = vDataReaderResults["NombreEntidadAseguradora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadAseguradora"]) : vContrato.TipoGarantia;
                                vContrato.ValorAsegurado = vDataReaderResults["ValorTotalAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalAsegurado"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.ValorAsegurado;
                                vContrato.CodigoSECOP = vDataReaderResults["CodigoSECOP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSECOP"]) : vContrato.CodigoSECOP;

                                vContrato.Riesgo1 = vDataReaderResults["RiesgosAsegurados"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RiesgosAsegurados"]) : vContrato.Riesgo1;

                                vContrato.Cofinanciador = vDataReaderResults["NombreCofinanciador1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCofinanciador1"]) : vContrato.Cofinanciador;
                                vContrato.CofinanciadorIdentificacion = vDataReaderResults["IdentificacionCofinanciador1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionCofinanciador1"]) : vContrato.CofinanciadorIdentificacion;
                                vContrato.CofinanciacionValor = vDataReaderResults["ValorCofinanciacion1"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCofinanciacion1"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.CofinanciacionValor;

                                vContrato.Cofinanciador1 = vDataReaderResults["NombreCofinanciador2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCofinanciador2"]) : vContrato.Cofinanciador1;
                                vContrato.CofinanciadorIdentificacion1 = vDataReaderResults["IdentificacionCofinanciador2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionCofinanciador2"]) : vContrato.CofinanciadorIdentificacion1;
                                vContrato.CofinanciacionValor1 = vDataReaderResults["ValorCofinanciacion2"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCofinanciacion2"]).ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) : vContrato.CofinanciacionValor1;

                                var misCDPS = _contratosCDPDAL.ConsultarContratosCDP(vContrato.idContrato);

                                if (misCDPS.Count > 0)
                                {
                                    foreach (var item in misCDPS)
                                    {
                                        vContrato.NumeroCDP += item.NumeroCDP.ToString() + "\n\r";
                                        vContrato.FechaCDP += item.FechaCDP.HasValue ? item.FechaCDP.Value.ToShortDateString() + "\n\r" : "\n\r";
                                        vContrato.ValorCDP += item.ValorCDP.Value.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) + "\n\r";
                                    }
                                }

                                var misRPS = _RPDAL.ConsultarRPContratosAsociados(vContrato.idContrato, null);

                                if (misRPS.Count > 0)
                                {
                                    foreach (var item in misRPS)
                                    {
                                        vContrato.NumeroRP += item.IdRP.ToString() + "\n\r";
                                        vContrato.FechaRP += item.FechaRP.HasValue ? item.FechaRP.Value.ToShortDateString() + "\n\r" : "\n\r";
                                        vContrato.ValorRP += item.ValorRP.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) + "\n\r";
                                    }
                                }

                                var garantias = _GarantiaDAL.ConsultarInfoGarantias(vContrato.idContrato);

                                if (garantias.Count > 0)
                                {
                                    foreach (var item in garantias)
                                    {
                                        vContrato.FechaGarantia += item.FechaExpedicionGarantia.HasValue ? item.FechaExpedicionGarantia.Value.ToShortDateString() + "\n\r" : "\n\r";
                                        vContrato.TipoGarantia += item.NombreTipoGarantia + "\n\r";
                                        vContrato.EntidadAseguradora += item.EntidadAseguradora + "\n\r";
                                        var Porcentaje = valorContratoInicial > 0 ? decimal.Parse(item.ValorGarantia.Replace("$", string.Empty)) * 100 / valorContratoInicial : 0;
                                        vContrato.Porcentaje += Porcentaje + "\n\r";
                                        vContrato.ValorAsegurado += item.ValorGarantia.ToString(System.Globalization.CultureInfo.GetCultureInfo("es-co")) + "\n\r";

                                        var amparos = _AmparosDAL.ConsultarAmparosGarantiass(null, null, null, null, item.IDGarantia);

                                        if (amparos.Count > 0)
                                        {
                                            foreach (var itemAmparo in amparos)
                                            {
                                                vContrato.Riesgo1 += itemAmparo.NombreTipoAmparo + "\n\r";
                                                vContrato.RiesgoValor1 += itemAmparo.ValorAsegurado.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co")) + "\n\r";
                                                var porcentaje = valorContratoInicial > 0 ? itemAmparo.ValorAsegurado * 100 / valorContratoInicial : 0;
                                                vContrato.RiesgoPorcentaje += Porcentaje + "\n\r";
                                            }
                                        }
                                    }
                                }


                                var rubros = _RubrosDAL.ObtenerRubrosContrato(vContrato.idContrato);

                                if (rubros.Count > 0)
                                {
                                    foreach (var itemrubro in rubros)
                                        vContrato.Rubro += itemrubro.CodigoRubro + "\n\r";
                                }


                                var prorrogas = _ProrrogasDAL.ConsultarProrrogasAprobadasContrato(vContrato.idContrato);

                                if (prorrogas.Count > 0)
                                {
                                    int i = 1;
                                    foreach (var item in prorrogas)
                                    {
                                        if (i == 1)
                                        {
                                            vContrato.FechaProrroga = item.FechaInicioView;
                                            vContrato.PlazoProrroga = (item.FechaFin - item.FechaInicio).Days;
                                        }
                                        else if (i == 2)
                                        {
                                            vContrato.FechaProrroga1 = item.FechaInicioView;
                                            vContrato.PlazoProrroga1 = (item.FechaFin - item.FechaInicio).Days;
                                        }
                                        else if (i == 3)
                                        {
                                            vContrato.FechaProrroga2 = item.FechaInicioView;
                                            vContrato.PlazoProrroga2 = (item.FechaFin - item.FechaInicio).Days;
                                        }

                                        i++;
                                    }

                                    vContrato.TotalDiasProrroga = vContrato.PlazoProrroga +
                                    vContrato.PlazoProrroga1 +
                                    vContrato.PlazoProrroga2;
                                }

                                var adicion = _AdicionesDAL.ConsultarAdicionesAprobadasContrato(vContrato.idContrato);

                                if (adicion.Count > 0)
                                {
                                    int i = 1;
                                    decimal valorTotalAdicion = 0;
                                    foreach (var item in adicion)
                                    {
                                        if (i == 1)
                                        {
                                            vContrato.FechaAdicion = item.FechaAdicionView;
                                            vContrato.NumeroRPAdicion = item.NumeroRP;
                                            vContrato.ValorAdicion = item.ValorAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                        }
                                        else if (i == 2)
                                        {
                                            vContrato.FechaAdicion1 = item.FechaAdicionView;
                                            vContrato.NumeroRPAdicion1 = item.NumeroRP;
                                            vContrato.ValorAdicion1 = item.ValorAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                        }
                                        else if (i == 3)
                                        {
                                            vContrato.FechaAdicion2 = item.FechaAdicionView;
                                            vContrato.NumeroRPAdicion2 = item.NumeroRP;
                                            vContrato.ValorAdicion2 = item.ValorAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                        }
                                        else if (i == 4)
                                        {
                                            vContrato.FechaAdicion3 = item.FechaAdicionView;
                                            vContrato.NumeroRPAdicion3 = item.NumeroRP;
                                            vContrato.ValorAdicion3 = item.ValorAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                        }

                                        valorTotalAdicion += item.ValorAdicion;
                                        i++;
                                    }

                                    vContrato.TotalAdicion = valorTotalAdicion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                }

                                var Reduccion = _ReduccionesDAL.ConsultarReduccionesAprobadasContrato(vContrato.idContrato);

                                if (Reduccion.Count > 0)
                                {
                                    int i = 1;
                                    decimal valorTotalReduccion = 0;
                                    foreach (var item in Reduccion)
                                    {
                                        valorTotalReduccion += item.ValorReduccion;
                                        i++;
                                    }

                                    vContrato.ReduccionValor = valorTotalReduccion.ToString("C3", System.Globalization.CultureInfo.GetCultureInfo("es-co"));
                                }

                                var codigoPersona = vDataReaderResults["CodigoTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoPersona"].ToString()) : string.Empty;

                                if (codigoPersona != "002" && codigoPersona != "001" && !string.IsNullOrEmpty(codigoPersona))
                                {
                                    var Integrantes = _IntegrantesUT.ObtenerProveedoresContrato(vContrato.idContrato, 1);

                                    if (Integrantes.Count > 0)
                                    {
                                        int i = 1;

                                        foreach (var item in Integrantes)
                                        {
                                            if (i == 1)
                                            {
                                                vContrato.IntegranteConsorcionUnionTemporalNombre = item.Proveedor;
                                                vContrato.IntegranteConsorcionUnionTemporalIdentificacion = item.NumeroIdentificacionIntegrante;
                                                vContrato.IntegranteConsorcionUnionTemporalPorcentaje = item.PorcentajeParticipacion;
                                            }
                                            if (i == 2)
                                            {
                                                vContrato.IntegranteConsorcionUnionTemporalNombre1 = item.Proveedor;
                                                vContrato.IntegranteConsorcionUnionTemporalIdentificacion1 = item.NumeroIdentificacionIntegrante;
                                                vContrato.IntegranteConsorcionUnionTemporalPorcentaje1 = item.PorcentajeParticipacion;
                                            }
                                            if (i == 3)
                                            {
                                                vContrato.IntegranteConsorcionUnionTemporalNombre2 = item.Proveedor;
                                                vContrato.IntegranteConsorcionUnionTemporalIdentificacion2 = item.NumeroIdentificacionIntegrante;
                                                vContrato.IntegranteConsorcionUnionTemporalPorcentaje2 = item.PorcentajeParticipacion;
                                            }
                                        }
                                    }

                                }

                                vListaContrato.Add(vContrato);
                            }
                        }
                        return vListaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


        }



    }
}
