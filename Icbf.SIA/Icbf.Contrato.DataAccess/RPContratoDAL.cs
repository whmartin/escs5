using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad AsociarRPContrato
    /// </summary>
    public class RPContratoDAL : GeneralDAL
    {
        public RPContratoDAL()
        {
        }

        /// <summary>
        /// Gonet
        /// Método de inserción para la entidad RPContrato
        /// Fecha: 11/07/2014
        /// </summary>
        /// <param name="pRPContrato">instancia que contiene la información de RPContratos</param>
        public int InsertarRPContratos(RPContrato pRPContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RPContratos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRPContratos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pRPContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdRP", DbType.Int32, pRPContrato.IdRP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRPContrato.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRP", DbType.DateTime, pRPContrato.FechaRP);
                    if (pRPContrato.FechaFinalizacion.HasValue)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaFinalizacion", DbType.DateTime, pRPContrato.FechaFinalizacion.Value);
                    }
                    if (pRPContrato.EsVigenciaFutura)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@EsVigenciaFutura", DbType.Boolean, pRPContrato.EsVigenciaFutura);
                        vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFutura", DbType.Int32, pRPContrato.IdVigenciaFutura);
                        vDataBase.AddInParameter(vDbCommand, "@ValorRP", DbType.Decimal, pRPContrato.ValorRP);
                        vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.Int32,Convert.ToInt32(pRPContrato.NumeroRP));
                    }

                    if(pRPContrato.EsEnLinea)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.Int32, Convert.ToInt32(pRPContrato.NumeroRP));
                        vDataBase.AddInParameter(vDbCommand, "@ValorRP", DbType.Decimal, pRPContrato.ValorRP);
                        vDataBase.AddInParameter(vDbCommand, "@EsEnLinea", DbType.Boolean, pRPContrato.EsEnLinea);
                    }

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRPContrato.IdRPContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRPContratos").ToString());
                    GenerarLogAuditoria(pRPContrato, vDbCommand);
                    return vResultado;

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public List<RPContrato> ConsultarRPContratosAsociados(int? pIdContrato, int? pIdRP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RPContratoss_Consultar"))
                {
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pIdRP != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRP", DbType.Int32, pIdRP);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RPContrato> vListaAsociarRPContrato = new List<RPContrato>();
                        while (vDataReaderResults.Read())
                        {
                            RPContrato vAsociarRPContrato = new RPContrato();
                            vAsociarRPContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAsociarRPContrato.IdContrato;
                            vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                            vAsociarRPContrato.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vAsociarRPContrato.ValorRP;
                            vAsociarRPContrato.ValorInicialRP = vDataReaderResults["ValorInicialRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialRP"].ToString()) : vAsociarRPContrato.ValorInicialRP;
                            vAsociarRPContrato.ValorActualRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorActualRP;
                            vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                            vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                            vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                            vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                            vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                            vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                            vAsociarRPContrato.FechaExpedicionRP = vDataReaderResults["FechaExpedicionRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionRP"].ToString()) : vAsociarRPContrato.FechaExpedicionRP;
                            vAsociarRPContrato.NumeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : vAsociarRPContrato.NumeroRP;
                            vAsociarRPContrato.EsCesion = vDataReaderResults["Escesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Escesion"].ToString()) : vAsociarRPContrato.EsCesion;
                            vListaAsociarRPContrato.Add(vAsociarRPContrato);
                        }
                        return vListaAsociarRPContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<RPContrato> ConsultarRPContratosAsociados(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ppto_CompromisosPresupuestalesContratoActualizado_Consultar"))
                {
                   vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RPContrato> vListaAsociarRPContrato = new List<RPContrato>();
                        while (vDataReaderResults.Read())
                        {
                            RPContrato vAsociarRPContrato = new RPContrato();
                            vAsociarRPContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAsociarRPContrato.IdContrato;
                            vAsociarRPContrato.IdRPContrato = vDataReaderResults["IdRPContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRPContratos"].ToString()) : vAsociarRPContrato.IdRPContrato;
                            vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                            vAsociarRPContrato.ValorInicialRP = vDataReaderResults["ValorInicialRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialRP"].ToString()) : vAsociarRPContrato.ValorInicialRP;
                            vAsociarRPContrato.ValorActualRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorActualRP;
                            vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                            vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                            vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                            vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                            vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                            vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                            vAsociarRPContrato.NumeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : vAsociarRPContrato.NumeroRP;
                            vAsociarRPContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAsociarRPContrato.FechaRP;
                            vListaAsociarRPContrato.Add(vAsociarRPContrato);
                        }
                        return vListaAsociarRPContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pesVigenciaFutura"></param>
        /// <param name="pAnioVigencia"></param>
        /// <returns></returns>
        public List<RPContrato> ConsultarRPContratosAsociadosVigencias(int? pIdContrato, bool? pesVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RPContratosVigencias_Consultar"))
                {
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);                    
                    if (pesVigenciaFutura != null)
                        vDataBase.AddInParameter(vDbCommand, "@esVigenciaFutura", DbType.Boolean, pesVigenciaFutura);
                    if (pAnioVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.Int32, pAnioVigencia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RPContrato> vListaAsociarRPContrato = new List<RPContrato>();
                        while (vDataReaderResults.Read())
                        {
                            RPContrato vAsociarRPContrato = new RPContrato();
                            vAsociarRPContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAsociarRPContrato.IdContrato;
                            vAsociarRPContrato.IdRPContrato = vDataReaderResults["IdRPContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRPContratos"].ToString()) : vAsociarRPContrato.IdRPContrato;
                            vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                            vAsociarRPContrato.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vAsociarRPContrato.ValorRP;
                            vAsociarRPContrato.ValorInicialRP = vDataReaderResults["ValorInicialRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialRP"].ToString()) : vAsociarRPContrato.ValorInicialRP;
                            vAsociarRPContrato.ValorActualRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorActualRP;
                            vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                            vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                            vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                            vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                            vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                            vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                            vAsociarRPContrato.FechaExpedicionRP = vDataReaderResults["FechaExpedicionRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionRP"].ToString()) : vAsociarRPContrato.FechaExpedicionRP;
                            vAsociarRPContrato.NumeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : vAsociarRPContrato.NumeroRP;
                            vAsociarRPContrato.EsCesion = vDataReaderResults["Escesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Escesion"].ToString()) : vAsociarRPContrato.EsCesion;
                            vListaAsociarRPContrato.Add(vAsociarRPContrato);
                        }
                        return vListaAsociarRPContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public List<RPContrato> ConsultarRPContratosExiste(int pIdRP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RPContratos_Consultar_Existe"))
                {
                    if (pIdRP != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRP", DbType.Int32, pIdRP);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RPContrato> vListaAsociarRPContrato = new List<RPContrato>();
                        while (vDataReaderResults.Read())
                        {
                            RPContrato vAsociarRPContrato = new RPContrato();
                            vAsociarRPContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAsociarRPContrato.IdContrato;
                            vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                            vAsociarRPContrato.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vAsociarRPContrato.ValorRP;
                            vAsociarRPContrato.ValorInicialRP = vDataReaderResults["ValorInicialRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialRP"].ToString()) : vAsociarRPContrato.ValorInicialRP;
                            vAsociarRPContrato.ValorActualRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorActualRP;
                            vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                            vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                            vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                            vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                            vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                            vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                            vAsociarRPContrato.FechaExpedicionRP = vDataReaderResults["FechaExpedicionRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionRP"].ToString()) : vAsociarRPContrato.FechaExpedicionRP;
                            vAsociarRPContrato.NumeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : vAsociarRPContrato.NumeroRP;

                            vListaAsociarRPContrato.Add(vAsociarRPContrato);
                        }
                        return vListaAsociarRPContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public List<RPContrato> ConsultarRPContratosExiste(int pNumeroRP, int idRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RPContratos_Consultar_ExisteRegional"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.Int32, pNumeroRP);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, idRegional);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RPContrato> vListaAsociarRPContrato = new List<RPContrato>();
                        while(vDataReaderResults.Read())
                        {
                            RPContrato vAsociarRPContrato = new RPContrato();
                            vAsociarRPContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAsociarRPContrato.IdContrato;
                            vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                            vAsociarRPContrato.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vAsociarRPContrato.ValorRP;
                            vAsociarRPContrato.ValorInicialRP = vDataReaderResults["ValorInicialRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialRP"].ToString()) : vAsociarRPContrato.ValorInicialRP;
                            vAsociarRPContrato.ValorActualRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorActualRP;
                            vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                            vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                            vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                            vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                            vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                            vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                            vAsociarRPContrato.FechaExpedicionRP = vDataReaderResults["FechaExpedicionRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionRP"].ToString()) : vAsociarRPContrato.FechaExpedicionRP;
                            vAsociarRPContrato.NumeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : vAsociarRPContrato.NumeroRP;

                            vListaAsociarRPContrato.Add(vAsociarRPContrato);
                        }
                        return vListaAsociarRPContrato;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        /// <param name="pIdRegional">Valor entero con el id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el id de la vigencia</param>
        /// <param name="pValorTotalDesde">Valor decimal con el Valor Total Desde</param>
        /// <param name="pValorTotalHasta">Valor decimal con el Valor Total hasta</param>
        /// <param name="pFechaRpDesde">Valor Fecha Rp Desde</param>
        /// <param name="pFechaRpHasta">Valor Fecha Rp Hasta</param>
        public List<RPContrato> ConsultarRPContratoss(int? pIdRP, int? pIdRegional, int? pIdVigencia, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaRpDesde, DateTime? pFechaRpHasta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ppto_CompromisosPresupuestales_Consultar"))
                {
                    if (pIdRP != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRP", DbType.Int32, pIdRP);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pValorTotalDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalDesde", DbType.Decimal, pValorTotalDesde);
                    if (pValorTotalHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalHasta", DbType.Decimal, pValorTotalHasta);
                    if (pFechaRpDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRPDesde", DbType.DateTime, pFechaRpDesde);
                    if (pFechaRpHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRPHasta", DbType.DateTime, pFechaRpHasta);
                    

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RPContrato> vListaAsociarRPContrato = new List<RPContrato>();

                        string numeroRP;
                        int idVigencia, idRegional;

                        while (vDataReaderResults.Read())
                        {
                            numeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : string.Empty;
                            idVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : 0;
                            idRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : 0;

                            if (!vListaAsociarRPContrato.Any(e => e.NumeroRP == numeroRP && e.IdVigenciaFiscal == idVigencia && e.IdRegional == idRegional))
                            {
                                RPContrato vAsociarRPContrato = new RPContrato();
                                vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                                vAsociarRPContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAsociarRPContrato.FechaRP;
                                vAsociarRPContrato.ValorInicialRP = vDataReaderResults["ValorInicialRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialRP"].ToString()) : vAsociarRPContrato.ValorInicialRP;
                                vAsociarRPContrato.ValorActualRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorActualRP;
                                vAsociarRPContrato.ValorRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorRP;
                                vAsociarRPContrato.IdVigenciaFiscal = idVigencia;
                                vAsociarRPContrato.VigenciaFiscal = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vAsociarRPContrato.VigenciaFiscal;
                                vAsociarRPContrato.IdRegional = idRegional;
                                vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                                vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                                vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                                vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                                vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                                vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                                vAsociarRPContrato.NumeroRP = numeroRP;

                                vListaAsociarRPContrato.Add(vAsociarRPContrato);
                            }
                        }
                        return vListaAsociarRPContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        /// <param name="pIdRegional">Valor entero con el id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el id de la vigencia</param>
        /// <param name="pValorTotalDesde">Valor decimal con el Valor Total Desde</param>
        /// <param name="pValorTotalHasta">Valor decimal con el Valor Total hasta</param>
        /// <param name="pFechaRpDesde">Valor Fecha Rp Desde</param>
        /// <param name="pFechaRpHasta">Valor Fecha Rp Hasta</param>
        public List<RPContrato> ConsultarRPContratossActualizado(int? pIdRP, int? pIdRegional, int? pIdVigencia, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaRpDesde, DateTime? pFechaRpHasta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ppto_CompromisosPresupuestalesActualizado_Consultar"))
                {
                    if (pIdRP != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRP", DbType.Int32, pIdRP);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pValorTotalDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalDesde", DbType.Decimal, pValorTotalDesde);
                    if (pValorTotalHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalHasta", DbType.Decimal, pValorTotalHasta);
                    if (pFechaRpDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRPDesde", DbType.DateTime, pFechaRpDesde);
                    if (pFechaRpHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRPHasta", DbType.DateTime, pFechaRpHasta);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RPContrato> vListaAsociarRPContrato = new List<RPContrato>();
                        string numeroRP = string.Empty, codigoRegional = string.Empty;
                        int anioVigencia = 0;
                        while (vDataReaderResults.Read())
                        {
                            numeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : numeroRP;
                            codigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : codigoRegional;
                            anioVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : anioVigencia;

                            if (! vListaAsociarRPContrato.Any( e => e.NumeroRP == numeroRP && e.CodigoRegional == codigoRegional && e.VigenciaFiscal == anioVigencia))
                            {
                                RPContrato vAsociarRPContrato = new RPContrato();
                                vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                                vAsociarRPContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAsociarRPContrato.FechaRP;
                                vAsociarRPContrato.ValorInicialRP = vDataReaderResults["ValorInicialRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialRP"].ToString()) : vAsociarRPContrato.ValorInicialRP;
                                vAsociarRPContrato.ValorActualRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorActualRP;
                                vAsociarRPContrato.IdVigenciaFiscal = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vAsociarRPContrato.IdVigenciaFiscal;
                                vAsociarRPContrato.VigenciaFiscal = anioVigencia;
                                vAsociarRPContrato.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vAsociarRPContrato.IdRegional;
                                vAsociarRPContrato.CodigoRegional = codigoRegional;
                                vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                                vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                                vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                                vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                                vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                                vAsociarRPContrato.NumeroRP = numeroRP;

                                vListaAsociarRPContrato.Add(vAsociarRPContrato);
                            }
                        }
                        return vListaAsociarRPContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdRP"></param>
        /// <returns></returns>
        public RPContrato ConsultarRPPorId(int pIdRP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ppto_CompromisosPresupuestales_ConsultarPorId"))
                {
                    if (pIdRP != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRP", DbType.Int32, pIdRP);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string numeroRP;
                        int idVigencia, idRegional;

                        RPContrato vAsociarRPContrato = new RPContrato();

                        while (vDataReaderResults.Read())
                        {
                            numeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : string.Empty;
                            idVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : 0;
                            idRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : 0;
      
                            vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                            vAsociarRPContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAsociarRPContrato.FechaRP;
                            vAsociarRPContrato.ValorInicialRP = vDataReaderResults["ValorInicialRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialRP"].ToString()) : vAsociarRPContrato.ValorInicialRP;
                            vAsociarRPContrato.ValorActualRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorActualRP;
                            vAsociarRPContrato.ValorRP = vDataReaderResults["ValorActualRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualRP"].ToString()) : vAsociarRPContrato.ValorRP;
                            vAsociarRPContrato.IdVigenciaFiscal = idVigencia;
                            vAsociarRPContrato.VigenciaFiscal = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vAsociarRPContrato.VigenciaFiscal;
                            vAsociarRPContrato.IdRegional = idRegional;
                            vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                            vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                            vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                            vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                            vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                            vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                            vAsociarRPContrato.NumeroRP = numeroRP;
                          }

                        return vAsociarRPContrato;
                     }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de actualización de un contrato suscrito a contrato RP
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="vFechaSuscripcion"></param>
        /// <param name="vIdEstadoContrato"></param>
        public int ActualizarContratoRP(int pIdContrato, string pUsuarioModifica, int vIdEstadoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Contrato_ActualizarContratoRP"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, vIdEstadoContrato);
                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    //GenerarLogAuditoria(pIdContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAdicion"></param>
        /// <param name="usuario"></param>
        /// <param name="idRP"></param>
        /// <returns></returns>
        public int ActualizarAdicionRP(int idAdicion, string usuario, int idRP, decimal valorRP, int idRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Contrato_ActualizarAdicionRP"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, idAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vDataBase.AddInParameter(vDbCommand, "@idRP", DbType.Int32, idRP);
                    vDataBase.AddInParameter(vDbCommand, "@idRegional", DbType.Int32, idRegional);
                    vDataBase.AddInParameter(vDbCommand, "@valorRP", DbType.Decimal, valorRP);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAdicion"></param>
        /// <param name="usuario"></param>
        /// <param name="idRP"></param>
        /// <returns></returns>
        public int ActualizarCesionRP(int idCesion, string usuario, int idRP, decimal valorRP, int idRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Contrato_ActualizarCesionRP"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, idCesion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vDataBase.AddInParameter(vDbCommand, "@idRP", DbType.Int32, idRP);
                    vDataBase.AddInParameter(vDbCommand, "@idRegional", DbType.Int32, idRegional);
                    vDataBase.AddInParameter(vDbCommand, "@valorRP", DbType.Decimal, valorRP);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public RPContrato ConsultarRPContratosAsociados(int pIdContrato, int pIdAdicion, bool esAdicion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RPContratoss_ConsultarPorAdicion"))
                {
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                        vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pIdAdicion);
                        vDataBase.AddInParameter(vDbCommand, "@EsAdicion", DbType.Boolean, esAdicion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RPContrato vAsociarRPContrato = null;

                        if(vDataReaderResults.Read())
                        {
                            vAsociarRPContrato = new RPContrato();
                            vAsociarRPContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAsociarRPContrato.IdContrato;
                            vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                            vAsociarRPContrato.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vAsociarRPContrato.ValorRP;
                            vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                            vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                            vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                            vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                            vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                            vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                            vAsociarRPContrato.FechaExpedicionRP = vDataReaderResults["FechaExpedicionRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionRP"].ToString()) : vAsociarRPContrato.FechaExpedicionRP;
                            vAsociarRPContrato.NumeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : vAsociarRPContrato.NumeroRP;                            
                        }

                        return vAsociarRPContrato;                           
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarRPContratos(RPContrato pRPContratos, int pVigenciaFutura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RPContratos_Eliminar"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@IdRPContratos", DbType.Int32, pRPContratos.IdRPContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFutura", DbType.Int32, pVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRPContratos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRPContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
