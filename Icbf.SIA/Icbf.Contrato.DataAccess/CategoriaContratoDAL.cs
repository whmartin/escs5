using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad categor�a Contrato
    /// </summary>
    public class CategoriaContratoDAL : GeneralDAL
    {
        public CategoriaContratoDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int InsertarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_CategoriaContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreCategoriaContrato", DbType.String, pCategoriaContrato.NombreCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pCategoriaContrato.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pCategoriaContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCategoriaContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCategoriaContrato.IdCategoriaContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCategoriaContrato").ToString());
                    GenerarLogAuditoria(pCategoriaContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }

            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int ModificarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_CategoriaContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pCategoriaContrato.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NombreCategoriaContrato", DbType.String, pCategoriaContrato.NombreCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pCategoriaContrato.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pCategoriaContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCategoriaContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCategoriaContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int EliminarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_CategoriaContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pCategoriaContrato.IdCategoriaContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCategoriaContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pIdCategoriaContrato">Entero con el identificador de la categoria contrato</param>
        /// <returns>Entidad categoria contrato con la informaci�n obtenida de la base de datos</returns>
        public CategoriaContrato ConsultarCategoriaContrato(int pIdCategoriaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_CategoriaContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pIdCategoriaContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CategoriaContrato vCategoriaContrato = new CategoriaContrato();
                        while (vDataReaderResults.Read())
                        {
                            vCategoriaContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vCategoriaContrato.IdCategoriaContrato;
                            vCategoriaContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vCategoriaContrato.NombreCategoriaContrato;
                            vCategoriaContrato.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCategoriaContrato.Descripcion;
                            vCategoriaContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCategoriaContrato.Estado;
                            vCategoriaContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCategoriaContrato.UsuarioCrea;
                            vCategoriaContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCategoriaContrato.FechaCrea;
                            vCategoriaContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCategoriaContrato.UsuarioModifica;
                            vCategoriaContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCategoriaContrato.FechaModifica;
                        }
                        return vCategoriaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pNombreCategoriaContrato"></param>
        /// <param name="pDescripcionCategoriaContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<CategoriaContrato> ConsultarCategoriaContratos(String pNombreCategoriaContrato, String pDescripcionCategoriaContrato, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_CategoriaContratos_Consultar"))
                {
                    if(pNombreCategoriaContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreCategoriaContrato", DbType.String, pNombreCategoriaContrato);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (pDescripcionCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@DescripcionCategoriaContrato", DbType.String, pDescripcionCategoriaContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CategoriaContrato> vListaCategoriaContrato = new List<CategoriaContrato>();
                        while (vDataReaderResults.Read())
                        {
                                CategoriaContrato vCategoriaContrato = new CategoriaContrato();
                            vCategoriaContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vCategoriaContrato.IdCategoriaContrato;
                            vCategoriaContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vCategoriaContrato.NombreCategoriaContrato;
                            vCategoriaContrato.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCategoriaContrato.Descripcion;
                            vCategoriaContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCategoriaContrato.Estado;
                            if (vCategoriaContrato.Estado)
                            {
                                vCategoriaContrato.EstadoString="Activo";
                            } else
                            {
                                vCategoriaContrato.EstadoString = "Inactivo";
                            }
                            vCategoriaContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCategoriaContrato.UsuarioCrea;
                            vCategoriaContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCategoriaContrato.FechaCrea;
                            vCategoriaContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCategoriaContrato.UsuarioModifica;
                            vCategoriaContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCategoriaContrato.FechaModifica;
                                vListaCategoriaContrato.Add(vCategoriaContrato);
                        }
                        return vListaCategoriaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos), de la categoria contrato, la 
        /// informaci�n del mismo
        /// </summary>
        /// <param name="pCategoriaContrato">Entidad con la informaci�n del filtro</param>
        /// <returns>Entidad con la informaci�n de categoria recuperado</returns>
        public CategoriaContrato IdentificadorCodigoCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_CategoriaContrato_IdentificadorCodigo"))
                {
                    if (pCategoriaContrato.IdCategoriaContrato != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pCategoriaContrato.IdCategoriaContrato);
                    if (pCategoriaContrato.CodigoCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoCategoriaContrato", DbType.String, pCategoriaContrato.CodigoCategoriaContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CategoriaContrato vCategoriaContrato = new CategoriaContrato();
                        while (vDataReaderResults.Read())
                        {
                            vCategoriaContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vCategoriaContrato.IdCategoriaContrato;
                            vCategoriaContrato.CodigoCategoriaContrato = vDataReaderResults["CodigoCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCategoriaContrato"].ToString()) : vCategoriaContrato.CodigoCategoriaContrato;
                            vCategoriaContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vCategoriaContrato.NombreCategoriaContrato;
                            vCategoriaContrato.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCategoriaContrato.Descripcion;
                            vCategoriaContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCategoriaContrato.Estado;
                            vCategoriaContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCategoriaContrato.UsuarioCrea;
                            vCategoriaContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCategoriaContrato.FechaCrea;
                            vCategoriaContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCategoriaContrato.UsuarioModifica;
                            vCategoriaContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCategoriaContrato.FechaModifica;
                        }
                        return vCategoriaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
