﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;



namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene métodos para la entidad ContratoAsociado
    /// </summary>
    public class TipoContratoAsociadoDAL : GeneralDAL
    {
        public TipoContratoAsociadoDAL()
        { }

        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo para obtener la información de la entidad Contrato Asociado a partir del identificador de tabla
        /// o el codigo del contrato asociado
        /// </summary>
        /// <param name="pTipoContratoAsociado">Entidad con la información de identificador o código según la necesidad</param>
        /// <returns>Entidad con la información recuperada de la base de datos</returns>
        public TipoContratoAsociado IdentificadorCodigoTipoContratoAsociado(TipoContratoAsociado pTipoContratoAsociado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoContratoAsociado_IdentificadorCodigo"))
                {
                    if (pTipoContratoAsociado.IdContratoAsociado != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContratoAsociado", DbType.Int32, pTipoContratoAsociado.IdContratoAsociado);
                    if (pTipoContratoAsociado.CodContratoAsociado != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoTipoContratoAsociado", DbType.String, pTipoContratoAsociado.CodContratoAsociado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoContratoAsociado vContratoAsociado = new TipoContratoAsociado();
                        while (vDataReaderResults.Read())
                        {
                            vContratoAsociado.IdContratoAsociado = vDataReaderResults["IdContratoAsociado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoAsociado"].ToString()) : vContratoAsociado.IdContratoAsociado;
                            vContratoAsociado.CodContratoAsociado = vDataReaderResults["CodContratoAsociado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodContratoAsociado"].ToString()) : vContratoAsociado.CodContratoAsociado;
                            vContratoAsociado.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vContratoAsociado.Descripcion;
                            vContratoAsociado.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vContratoAsociado.Inactivo;
                            //vContratoAsociado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCategoriaContrato.UsuarioCrea;
                            //vContratoAsociado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCategoriaContrato.FechaCrea;
                            //vContratoAsociado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCategoriaContrato.UsuarioModifica;
                            //vContratoAsociado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCategoriaContrato.FechaModifica;
                        }
                        return vContratoAsociado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
