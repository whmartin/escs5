﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions; 

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Tabla paramétrica
    /// </summary>
    public class TablaParametricaDAL : GeneralDAL
    {
        public TablaParametricaDAL()
        { 
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TablaParametrica
        /// </summary>
        /// <param name="pCodigoTablaParametrica"></param>
        /// <param name="pNombreTablaParametrica"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TablaParametrica> ConsultarTablaParametricas(string pCodigoTablaParametrica, String pNombreTablaParametrica, bool? pEstado, bool pEsPrecontractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TablaParametricas_Consultar"))
                {
                    if (pCodigoTablaParametrica != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoTablaParametrica", DbType.String, pCodigoTablaParametrica);
                    if (pNombreTablaParametrica != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTablaParametrica", DbType.String, pNombreTablaParametrica);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                        vDataBase.AddInParameter(vDbCommand, "@EsPrecontractual", DbType.Boolean, pEsPrecontractual);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TablaParametrica> vListaGarantia = new List<TablaParametrica>();
                        while (vDataReaderResults.Read())
                        {
                            TablaParametrica vTablaParametrica = new TablaParametrica();
                            vTablaParametrica.IdTablaParametrica = vDataReaderResults["IdTablaParametrica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTablaParametrica"].ToString()) : vTablaParametrica.IdTablaParametrica;
                            vTablaParametrica.CodigoTablaParametrica = vDataReaderResults["CodigoTablaParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTablaParametrica"].ToString()) : vTablaParametrica.CodigoTablaParametrica;
                            vTablaParametrica.NombreTablaParametrica = vDataReaderResults["NombreTablaParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTablaParametrica"].ToString()) : vTablaParametrica.NombreTablaParametrica;
                            vTablaParametrica.Url = vDataReaderResults["Url"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Url"].ToString()) : vTablaParametrica.Url;
                            vTablaParametrica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTablaParametrica.Estado;
                            vTablaParametrica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTablaParametrica.UsuarioCrea;
                            vTablaParametrica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTablaParametrica.FechaCrea;
                            vTablaParametrica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTablaParametrica.UsuarioModifica;
                            vTablaParametrica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTablaParametrica.FechaModifica;
                            vListaGarantia.Add(vTablaParametrica);
                        }
                        return vListaGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
