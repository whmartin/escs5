﻿using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess
{
    public class IntegrationContratosDAL : GeneralDAL
    {
        private RPContratoDAL _RPDAL;
        private ContratosCDPDAL _contratosCDPDAL;
        private VigenciaFuturasDAL _VigenciasFuturasDAL;
        private PlanComprasProductosDAL _PlanComprasProductosDAL;
        private PlanComprasRubrosCDPDAL _PlanComprasRubrosDAL;
        private Proveedores_ContratosDAL _ProoveedorContratoDAL;
        private GarantiaDAL _GarantiasDAL;
        private SupervisorInterContratoDAL _SupervisorDAL;
        private LugarEjecucionContratoDAL _LugaresEjecuccionDAL;

        public IntegrationContratosDAL()
        {
            _RPDAL = new RPContratoDAL();
            _contratosCDPDAL = new ContratosCDPDAL();
            _VigenciasFuturasDAL = new VigenciaFuturasDAL();
            _PlanComprasProductosDAL = new PlanComprasProductosDAL();
            _PlanComprasRubrosDAL = new PlanComprasRubrosCDPDAL();
            _ProoveedorContratoDAL = new Proveedores_ContratosDAL();
            _GarantiasDAL = new GarantiaDAL();
            _SupervisorDAL = new SupervisorInterContratoDAL();
            _LugaresEjecuccionDAL = new LugarEjecucionContratoDAL();
        } 

        public InfoContratoDTO ObtenerInfoContratosPaccoPorId(int idPlanCompras)
        {
            InfoContratoDTO vInfoContrato = null;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Integration_ConsultarContratoPacco"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanCompras",DbType.Int32, idPlanCompras);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int idContrato = 0;

                        while(vDataReaderResults.Read())
                        {
                            vInfoContrato = new InfoContratoDTO();
                            idContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : 0;
                            vInfoContrato.IdContrato = idContrato;
                            vInfoContrato.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vInfoContrato.ConsecutivoPlanCompras;
                            vInfoContrato.ConsecutivoContrato = vDataReaderResults["ConsecutivoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoContrato"].ToString()) : vInfoContrato.ConsecutivoContrato;
                            vInfoContrato.VigenciaInicial = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vInfoContrato.VigenciaInicial;
                            vInfoContrato.VigenciaFinal = vDataReaderResults["VigenciaF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaF"].ToString()) : vInfoContrato.VigenciaFinal;
                            vInfoContrato.CodRegional = vDataReaderResults["CodRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegional"].ToString()) : vInfoContrato.CodRegional;
                            vInfoContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vInfoContrato.NombreRegional;
                            vInfoContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vInfoContrato.EstadoContrato;
                            vInfoContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vInfoContrato.ValorInicialContrato;
                            vInfoContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vInfoContrato.ValorFinalContrato;
                            vInfoContrato.CodigoModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidad"].ToString()) : vInfoContrato.CodigoModalidad;
                            vInfoContrato.NombreModalidad = vDataReaderResults["NombreModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModalidad"].ToString()) : vInfoContrato.NombreModalidad;
                            vInfoContrato.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vInfoContrato.NombreTipoContrato;
                            vInfoContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"].ToString()) : vInfoContrato.CodigoTipoContrato;
                            vInfoContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vInfoContrato.NombreCategoriaContrato;
                            vInfoContrato.CodigoCategoriaContrato = vDataReaderResults["CodigoCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCategoriaContrato"].ToString()) : vInfoContrato.CodigoCategoriaContrato;
                            vInfoContrato.FechaFinalizacionInicialContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vInfoContrato.FechaFinalizacionInicialContrato;
                            vInfoContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vInfoContrato.FechaFinalTerminacionContrato;
                            vInfoContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vInfoContrato.FechaInicioEjecucion;

                            var misCDPS = _contratosCDPDAL.ConsultarContratosCDP(idContrato);
                            if(misCDPS.Count > 0)
                            {
                                vInfoContrato.CDP = new List<InfoContratoCDPDTO>();
                                foreach(var item in misCDPS)
                                    if(!vInfoContrato.CDP.Any(e => e.CDP == item.NumeroCDP))
                                        vInfoContrato.CDP.Add(new InfoContratoCDPDTO() { CDP = item.NumeroCDP, Fecha = item.FechaCDP });
                            }


                            var misRPS = _RPDAL.ConsultarRPContratosAsociados(idContrato, null);
                            if(misRPS.Count > 0)
                            {
                                vInfoContrato.RP = new List<InfoContratoRPDTO>();
                                foreach(var item in misRPS)
                                    vInfoContrato.RP.Add(new InfoContratoRPDTO() { RP = item.NumeroRP, Fecha = item.FechaExpedicionRP });
                            }

                            var misVigencias = _VigenciasFuturasDAL.ConsultarVigenciaFuturass(null, null, idContrato, null);
                            if(misVigencias.Count > 0)
                            {
                                vInfoContrato.VigenciasFuturas = new List<InfoContratoVFDTO>();
                                foreach(var item in misVigencias)
                                    vInfoContrato.VigenciasFuturas.Add(new InfoContratoVFDTO() { Numero = item.NumeroRadicado, Vigencia = item.AnioVigencia, Valor = item.ValorVigenciaFutura });
                            }

                            var rubros = _PlanComprasRubrosDAL.ConsultarPlanComprasRubrosCDPs(vInfoContrato.ConsecutivoPlanCompras, null, null, null);
                            if(rubros.Count > 0)
                            {
                                vInfoContrato.RubrosPlanCompras = new List<InfoRubroPlanComprasDTO>();
                                foreach(var item in rubros)
                                    vInfoContrato.RubrosPlanCompras.Add(new InfoRubroPlanComprasDTO() { Rubro = item.CodigoRubro, Valor = item.ValorRubro.Value });
                            }

                            var productos = _PlanComprasProductosDAL.ConsultarPlanComprasProductoss(vInfoContrato.ConsecutivoPlanCompras, null, null, null, null, null, null, null, null, null);
                            if(productos.Count > 0)
                            {
                                vInfoContrato.ProductosPlanCompras = new List<InfoProductoPlanComprasDTO>();
                                foreach(var item in productos)
                                    vInfoContrato.ProductosPlanCompras.Add(new InfoProductoPlanComprasDTO() { Cantidad = item.CantidadCupos, IdProducto = item.CodigoProducto.ToString(), Tiempo = item.Tiempo, Valor = item.ValorUnitario });
                            }


                            var proveedores = _ProoveedorContratoDAL.ObtenerProveedoresContrato(idContrato, null);
                            if(proveedores.Count > 0)
                            {

                                vInfoContrato.Contratistas = new List<InfoProveedoresDTO>();
                                foreach(var item in proveedores)
                                    vInfoContrato.Contratistas.Add(new InfoProveedoresDTO() { NumeroDocumento = item.NumeroIdentificacion, Tercero = item.Proveedor, TipoDocumento = item.TipoIdentificacion, TipoPersona = item.TipoPersonaNombre });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return vInfoContrato;
        }

        public List<InfoContratoDTO> GetInfoContratoGeneralPorRubro(string codigoRegional, int vigencia, string codigoRubro, string consecutivo)
        {
            List<InfoContratoDTO> vInfoContratoList = new List<InfoContratoDTO>();

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Integration_ConsultarContratoGeneralPorRubro"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.String, codigoRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRubro", DbType.String, codigoRubro);

                    if (! string.IsNullOrEmpty(consecutivo))
                        vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.String, consecutivo);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int idContrato = 0;
                        InfoContratoDTO vInfoContrato;

                        while(vDataReaderResults.Read())
                        {
                            vInfoContrato = new InfoContratoDTO();
                            idContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : 0;
                            vInfoContrato.IdContrato = idContrato;
                            vInfoContrato.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vInfoContrato.ConsecutivoPlanCompras;
                            vInfoContrato.ConsecutivoContrato = vDataReaderResults["ConsecutivoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoContrato"].ToString()) : vInfoContrato.ConsecutivoContrato;
                            vInfoContrato.VigenciaInicial = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vInfoContrato.VigenciaInicial;
                            vInfoContrato.VigenciaFinal = vDataReaderResults["VigenciaF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaF"].ToString()) : vInfoContrato.VigenciaFinal;
                            vInfoContrato.CodRegional = vDataReaderResults["CodRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegional"].ToString()) : vInfoContrato.CodRegional;
                            vInfoContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vInfoContrato.NombreRegional;
                            vInfoContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vInfoContrato.EstadoContrato;
                            vInfoContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vInfoContrato.ValorInicialContrato;
                            vInfoContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vInfoContrato.ValorFinalContrato;
                            vInfoContrato.CodigoModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidad"].ToString()) : vInfoContrato.CodigoModalidad;
                            vInfoContrato.NombreModalidad = vDataReaderResults["NombreModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModalidad"].ToString()) : vInfoContrato.NombreModalidad;
                            vInfoContrato.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vInfoContrato.NombreTipoContrato;
                            vInfoContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"].ToString()) : vInfoContrato.CodigoTipoContrato;
                            vInfoContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vInfoContrato.NombreCategoriaContrato;
                            vInfoContrato.CodigoCategoriaContrato = vDataReaderResults["CodigoCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCategoriaContrato"].ToString()) : vInfoContrato.CodigoCategoriaContrato;
                            vInfoContrato.FechaFinalizacionInicialContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vInfoContrato.FechaFinalizacionInicialContrato;
                            vInfoContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vInfoContrato.FechaFinalTerminacionContrato;
                            vInfoContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vInfoContrato.FechaInicioEjecucion;

                            var misCDPS = _contratosCDPDAL.ConsultarContratosCDP(idContrato);
                            if(misCDPS.Count > 0)
                            {
                                vInfoContrato.CDP = new List<InfoContratoCDPDTO>();
                                foreach(var item in misCDPS)
                                    if(!vInfoContrato.CDP.Any(e => e.CDP == item.NumeroCDP))
                                        vInfoContrato.CDP.Add(new InfoContratoCDPDTO() { CDP = item.NumeroCDP, Fecha = item.FechaCDP });
                            }


                            var misRPS = _RPDAL.ConsultarRPContratosAsociados(idContrato, null);
                            if(misRPS.Count > 0)
                            {
                                vInfoContrato.RP = new List<InfoContratoRPDTO>();
                                foreach(var item in misRPS)
                                    vInfoContrato.RP.Add(new InfoContratoRPDTO() { RP = item.NumeroRP, Fecha = item.FechaExpedicionRP });
                            }

                            var misVigencias = _VigenciasFuturasDAL.ConsultarVigenciaFuturass(null, null, idContrato, null);
                            if(misVigencias.Count > 0)
                            {
                                vInfoContrato.VigenciasFuturas = new List<InfoContratoVFDTO>();
                                foreach(var item in misVigencias)
                                    vInfoContrato.VigenciasFuturas.Add(new InfoContratoVFDTO() { Numero = item.NumeroRadicado, Vigencia = item.AnioVigencia, Valor = item.ValorVigenciaFutura });
                            }

                            var rubros = _PlanComprasRubrosDAL.ConsultarPlanComprasRubrosCDPs(vInfoContrato.ConsecutivoPlanCompras, null, null, null);
                            if(rubros.Count > 0)
                            {
                                StringBuilder rubrosConcatenados = new StringBuilder();

                                vInfoContrato.RubrosPlanCompras = new List<InfoRubroPlanComprasDTO>();
                                foreach(var item in rubros)
                                {
                                    vInfoContrato.RubrosPlanCompras.Add(new InfoRubroPlanComprasDTO() { Rubro = item.CodigoRubro, Valor = item.ValorRubro.Value });

                                    rubrosConcatenados.Append(item.CodigoRubro + ",");
                                }

                                vInfoContrato.RubrosPlanComprasConcatenados = rubrosConcatenados.ToString().Substring(0, rubrosConcatenados.Length - 1);
                            }

                            var productos = _PlanComprasProductosDAL.ConsultarPlanComprasProductoss(vInfoContrato.ConsecutivoPlanCompras, null, null, null, null, null, null, null, null, null);
                            if(productos.Count > 0)
                            {
                                vInfoContrato.ProductosPlanCompras = new List<InfoProductoPlanComprasDTO>();
                                foreach(var item in productos)
                                    vInfoContrato.ProductosPlanCompras.Add(new InfoProductoPlanComprasDTO() { Cantidad = item.CantidadCupos, IdProducto = item.CodigoProducto.ToString(), Tiempo = item.Tiempo, Valor = item.ValorUnitario });
                            }


                            var proveedores = _ProoveedorContratoDAL.ObtenerProveedoresContrato(idContrato, null);
                            if(proveedores.Count > 0)
                            {

                                vInfoContrato.Contratistas = new List<InfoProveedoresDTO>();
                                foreach(var item in proveedores)
                                    vInfoContrato.Contratistas.Add
                                        (
                                         new InfoProveedoresDTO()
                                         { NumeroDocumento = item.NumeroIdentificacion,
                                           Tercero = item.Proveedor,
                                           TipoDocumento = item.TipoIdentificacion,
                                           TipoPersona = item.TipoPersonaNombre,
                                           NombresRLegal = item.RepresentanteLegal,
                                           NumeroRLegal = item.NumeroIDRepresentanteLegal,
                                           TipoDocumentoRLegal = item.TipoDocumentoRepresentanteLegal
                                         }
                                        );
                            }

                            vInfoContratoList.Add(vInfoContrato);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return vInfoContratoList;
        }

        public InfoContratoDTO ObtenerInfoContratosGeneral(string consecutivoContrato)
        {
            InfoContratoDTO vInfoContrato = new InfoContratoDTO();

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Integration_ConsultarContratoGeneral"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoContrato", DbType.String, consecutivoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {                        
                        int idContrato = 0;

                        while(vDataReaderResults.Read())
                        {
                            idContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : 0;
                            vInfoContrato.IdContrato = idContrato;
                            vInfoContrato.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vInfoContrato.ConsecutivoPlanCompras;
                            vInfoContrato.ConsecutivoContrato = vDataReaderResults["ConsecutivoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoContrato"].ToString()) : vInfoContrato.ConsecutivoContrato;
                            vInfoContrato.VigenciaInicial = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vInfoContrato.VigenciaInicial;
                            vInfoContrato.VigenciaFinal = vDataReaderResults["VigenciaF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaF"].ToString()) : vInfoContrato.VigenciaFinal;
                            vInfoContrato.CodRegional = vDataReaderResults["CodRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegional"].ToString()) : vInfoContrato.CodRegional;
                            vInfoContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vInfoContrato.NombreRegional;
                            vInfoContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vInfoContrato.EstadoContrato;
                            vInfoContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vInfoContrato.ValorInicialContrato;
                            vInfoContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vInfoContrato.ValorFinalContrato;
                            vInfoContrato.CodigoModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidad"].ToString()) : vInfoContrato.CodigoModalidad;
                            vInfoContrato.NombreModalidad = vDataReaderResults["NombreModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModalidad"].ToString()) : vInfoContrato.NombreModalidad;
                            vInfoContrato.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vInfoContrato.NombreTipoContrato;
                            vInfoContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"].ToString()) : vInfoContrato.CodigoTipoContrato;
                            vInfoContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vInfoContrato.NombreCategoriaContrato;
                            vInfoContrato.CodigoCategoriaContrato = vDataReaderResults["CodigoCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCategoriaContrato"].ToString()) : vInfoContrato.CodigoCategoriaContrato;
                            vInfoContrato.FechaFinalizacionInicialContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vInfoContrato.FechaFinalizacionInicialContrato;
                            vInfoContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vInfoContrato.FechaFinalTerminacionContrato;
                            vInfoContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vInfoContrato.FechaInicioEjecucion;
                            vInfoContrato.FechaSuscripcionContrato = vDataReaderResults["FechaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaContrato"].ToString()) : vInfoContrato.FechaSuscripcionContrato;
                        }
                            
                        var misCDPS = _contratosCDPDAL.ConsultarContratosCDP(idContrato);
                        if (misCDPS.Count > 0)
                        {
                            vInfoContrato.CDP = new List<InfoContratoCDPDTO>();
                            foreach (var item in misCDPS)
                                vInfoContrato.CDP.Add(new InfoContratoCDPDTO() { CDP = item.NumeroCDP, Fecha = item.FechaCDP });
                        }


                        var misRPS = _RPDAL.ConsultarRPContratosAsociados(idContrato, null);
                        if (misRPS.Count > 0)
                        {
                            vInfoContrato.RP = new List<InfoContratoRPDTO>();
                            foreach (var item in misRPS)
                                vInfoContrato.RP.Add(new InfoContratoRPDTO() { RP = item.NumeroRP, Fecha = item.FechaExpedicionRP });
                        }

                        var misVigencias = _VigenciasFuturasDAL.ConsultarVigenciaFuturass(null, null, idContrato, null);
                        if (misVigencias.Count > 0)
                        {
                            vInfoContrato.VigenciasFuturas = new List<InfoContratoVFDTO>();
                            foreach (var item in misVigencias)
                                vInfoContrato.VigenciasFuturas.Add(new InfoContratoVFDTO() { Numero = item.NumeroRadicado, Vigencia = item.AnioVigencia, Valor = item.ValorVigenciaFutura });
                        }

                        var rubros = _PlanComprasRubrosDAL.ConsultarPlanComprasRubrosCDPs(vInfoContrato.ConsecutivoPlanCompras, null, null, null);
                        if (rubros.Count > 0)
                        {
                            vInfoContrato.RubrosPlanCompras = new List<InfoRubroPlanComprasDTO>();
                            foreach (var item in rubros)
                                vInfoContrato.RubrosPlanCompras.Add(new InfoRubroPlanComprasDTO() { Rubro = item.CodigoRubro, Valor = item.ValorRubro.Value });
                        }

                        var productos = _PlanComprasProductosDAL.ConsultarPlanComprasProductoss(vInfoContrato.ConsecutivoPlanCompras, null, null, null,null,null,null,null,null,null);
                        if (productos.Count > 0)
                        {
                            vInfoContrato.ProductosPlanCompras = new List<InfoProductoPlanComprasDTO>();
                            foreach (var item in productos)
                                vInfoContrato.ProductosPlanCompras.Add(new InfoProductoPlanComprasDTO() { Cantidad = item.CantidadCupos, IdProducto = item.CodigoProducto.ToString(), Tiempo = item.Tiempo, Valor = item.ValorUnitario });
                        }


                        var proveedores = _ProoveedorContratoDAL.ObtenerProveedoresContrato(idContrato, null);
                        if(proveedores.Count > 0)
                        {
                            vInfoContrato.Contratistas = new List<InfoProveedoresDTO>();
                            foreach(var item in proveedores)
                                vInfoContrato.Contratistas.Add
                                    (
                                     new InfoProveedoresDTO()
                                     {
                                         NumeroDocumento = item.NumeroIdentificacion,
                                         Tercero = item.Proveedor,
                                         TipoDocumento = item.TipoIdentificacion,
                                         TipoPersona = item.TipoPersonaNombre,
                                         NombresRLegal = item.RepresentanteLegal,
                                         NumeroRLegal = item.NumeroIDRepresentanteLegal,
                                         TipoDocumentoRLegal = item.TipoDocumentoRepresentanteLegal
                                     }
                                    );
                        }

                        var supervisores = _SupervisorDAL.ObtenerSupervisoresInterventoresContrato(idContrato, null);
                        if(supervisores.Count > 0)
                        {
                            vInfoContrato.Supervisores = new List<InfoSupervisorDTO>();
                            foreach(var item in supervisores)
                            {
                                vInfoContrato.Supervisores.Add
                                    (
                                       new InfoSupervisorDTO()
                                       {
                                            Identificacion = item.Identificacion,
                                            Nombres = item.NombreCompletoSuperInterventor,
                                            TipoIdentificacion = item.TipoIdentificacion
                                       }
                                    );
                            }
                        }

                        var lugarEjeccucion = _LugaresEjecuccionDAL.ConsultarLugaresEjecucionContrato(idContrato);
                        if(lugarEjeccucion.Count > 0)
                        {
                            if(lugarEjeccucion.Count == 1 && lugarEjeccucion.First().EsNivelNacional)
                                vInfoContrato.EsLugarEjeccucionNacional = true;
                            else
                            {
                                vInfoContrato.LugarEjeccuion = new List<InfoLugarEjeccuionDTO>();
                                foreach(var item in lugarEjeccucion)
                                {
                                    vInfoContrato.LugarEjeccuion.Add
                                        (
                                            new InfoLugarEjeccuionDTO()
                                            {
                                                 CodigoDepartamento = item.CodigoDepartamento,
                                                 CodigoMunicipio = item.CodigoMunicipio,
                                                 Departamento = item.Departamento,
                                                 Municipio = item.Municipio
                                            }
                                        );
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return vInfoContrato;
        }

        public List<InfoContratoDTO> ObtenerInfoContratoGeneralPorPeriodo(string codigoRegional, DateTime fechaDesde, DateTime fechaHasta)
        {
            List<InfoContratoDTO> vInfoContratoList = new List<InfoContratoDTO>();

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Integration_ConsultarContratoGeneralPorPeriodo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.String, codigoRegional);
                    vDataBase.AddInParameter(vDbCommand, "@FechaDesde", DbType.DateTime, fechaDesde);
                    vDataBase.AddInParameter(vDbCommand, "@FechaHasta", DbType.DateTime, fechaHasta);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int idContrato = 0;
                        InfoContratoDTO vInfoContrato;

                        while (vDataReaderResults.Read())
                        {
                            vInfoContrato = new InfoContratoDTO();
                            idContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : 0;
                            vInfoContrato.IdContrato = idContrato;
                            vInfoContrato.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vInfoContrato.ConsecutivoPlanCompras;
                            vInfoContrato.ConsecutivoContrato = vDataReaderResults["ConsecutivoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoContrato"].ToString()) : vInfoContrato.ConsecutivoContrato;
                            vInfoContrato.VigenciaInicial = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vInfoContrato.VigenciaInicial;
                            vInfoContrato.VigenciaFinal = vDataReaderResults["VigenciaF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaF"].ToString()) : vInfoContrato.VigenciaFinal;
                            vInfoContrato.CodRegional = vDataReaderResults["CodRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegional"].ToString()) : vInfoContrato.CodRegional;
                            vInfoContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vInfoContrato.NombreRegional;
                            vInfoContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vInfoContrato.EstadoContrato;
                            vInfoContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vInfoContrato.ValorInicialContrato;
                            vInfoContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vInfoContrato.ValorFinalContrato;
                            vInfoContrato.CodigoModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidad"].ToString()) : vInfoContrato.CodigoModalidad;
                            vInfoContrato.NombreModalidad = vDataReaderResults["NombreModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModalidad"].ToString()) : vInfoContrato.NombreModalidad;
                            vInfoContrato.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vInfoContrato.NombreTipoContrato;
                            vInfoContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"].ToString()) : vInfoContrato.CodigoTipoContrato;
                            vInfoContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vInfoContrato.NombreCategoriaContrato;
                            vInfoContrato.CodigoCategoriaContrato = vDataReaderResults["CodigoCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCategoriaContrato"].ToString()) : vInfoContrato.CodigoCategoriaContrato;
                            vInfoContrato.FechaFinalizacionInicialContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vInfoContrato.FechaFinalizacionInicialContrato;
                            vInfoContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vInfoContrato.FechaFinalTerminacionContrato;
                            vInfoContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vInfoContrato.FechaInicioEjecucion;

                            var misCDPS = _contratosCDPDAL.ConsultarContratosCDP(idContrato);
                            if (misCDPS.Count > 0)
                            {
                                vInfoContrato.CDP = new List<InfoContratoCDPDTO>();
                                foreach (var item in misCDPS)
                                    vInfoContrato.CDP.Add(new InfoContratoCDPDTO() { CDP = item.NumeroCDP, Fecha = item.FechaCDP });
                            }


                            var misRPS = _RPDAL.ConsultarRPContratosAsociados(idContrato, null);
                            if (misRPS.Count > 0)
                            {
                                vInfoContrato.RP = new List<InfoContratoRPDTO>();
                                foreach (var item in misRPS)
                                    vInfoContrato.RP.Add(new InfoContratoRPDTO() { RP = item.NumeroRP, Fecha = item.FechaExpedicionRP });
                            }

                            var misVigencias = _VigenciasFuturasDAL.ConsultarVigenciaFuturass(null, null, idContrato, null);
                            if (misVigencias.Count > 0)
                            {
                                vInfoContrato.VigenciasFuturas = new List<InfoContratoVFDTO>();
                                foreach (var item in misVigencias)
                                    vInfoContrato.VigenciasFuturas.Add(new InfoContratoVFDTO() { Numero = item.NumeroRadicado, Vigencia = item.AnioVigencia, Valor = item.ValorVigenciaFutura });
                            }

                            var rubros = _PlanComprasRubrosDAL.ConsultarPlanComprasRubrosCDPs(vInfoContrato.ConsecutivoPlanCompras, null, null, null);
                            if (rubros.Count > 0)
                            {
                                vInfoContrato.RubrosPlanCompras = new List<InfoRubroPlanComprasDTO>();
                                foreach (var item in rubros)
                                    vInfoContrato.RubrosPlanCompras.Add(new InfoRubroPlanComprasDTO() { Rubro = item.CodigoRubro, Valor = item.ValorRubro.Value });
                            }

                            var productos = _PlanComprasProductosDAL.ConsultarPlanComprasProductoss(vInfoContrato.ConsecutivoPlanCompras, null, null, null, null, null, null, null, null, null);
                            if (productos.Count > 0)
                            {
                                vInfoContrato.ProductosPlanCompras = new List<InfoProductoPlanComprasDTO>();
                                foreach (var item in productos)
                                    vInfoContrato.ProductosPlanCompras.Add(new InfoProductoPlanComprasDTO() { Cantidad = item.CantidadCupos, IdProducto = item.CodigoProducto.ToString(), Tiempo = item.Tiempo, Valor = item.ValorUnitario });
                            }


                            var proveedores = _ProoveedorContratoDAL.ObtenerProveedoresContrato(idContrato, null);
                            if (proveedores.Count > 0)
                            {

                                vInfoContrato.Contratistas = new List<InfoProveedoresDTO>();
                                foreach (var item in proveedores)
                                    vInfoContrato.Contratistas.Add(new InfoProveedoresDTO() { NumeroDocumento = item.NumeroIdentificacion, Tercero = item.Proveedor, TipoDocumento = item.TipoIdentificacion, TipoPersona = item.TipoPersonaNombre });
                            }

                            vInfoContratoList.Add(vInfoContrato);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return vInfoContratoList;
        }
    }
}

