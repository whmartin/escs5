using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Modalidad Selecci�n
    /// </summary>
    public class ModalidadSeleccionDAL : GeneralDAL
    {
        public ModalidadSeleccionDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int InsertarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            int vResultado;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ModalidadSeleccion_Insertar"))
                {
                    
                    vDataBase.AddOutParameter(vDbCommand, "@IdModalidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pModalidadSeleccion.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pModalidadSeleccion.Sigla);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pModalidadSeleccion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pModalidadSeleccion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pModalidadSeleccion.IdModalidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdModalidad").ToString());
                    GenerarLogAuditoria(pModalidadSeleccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                //if (ex.Number == 2627)
                //    throw new GenericException("Registro duplicado, verifique por favor.");
                //throw;
                if (ex.Message.Contains("Registro duplicado"))
                {
                    vResultado = 4;
                    return vResultado;
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int ModificarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            int vResultado;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ModalidadSeleccion_Modificar"))
                {
                    
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pModalidadSeleccion.IdModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pModalidadSeleccion.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pModalidadSeleccion.Sigla);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pModalidadSeleccion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pModalidadSeleccion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModalidadSeleccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                //if (ex.Number == 2627)
                //    throw new GenericException("Registro duplicado, verifique por favor.");
                //throw;
                if (ex.Message.Contains("Registro duplicado"))
                {
                    vResultado = 4;
                    return vResultado;
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int EliminarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pModalidadSeleccion.IdModalidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModalidadSeleccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pIdModalidad">Entero con el identificador de la modalidad</param>
        /// <returns>Entidad modalidad selecci�n con la informaci�n obtenidad de la base de datos</returns>
        public ModalidadSeleccion ConsultarModalidadSeleccion(int pIdModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ModalidadSeleccion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pIdModalidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();
                        while (vDataReaderResults.Read())
                        {
                            vModalidadSeleccion.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vModalidadSeleccion.IdModalidad;
                            vModalidadSeleccion.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vModalidadSeleccion.Nombre;
                            vModalidadSeleccion.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vModalidadSeleccion.Sigla;
                            vModalidadSeleccion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModalidadSeleccion.Estado;
                            vModalidadSeleccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadSeleccion.UsuarioCrea;
                            vModalidadSeleccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadSeleccion.FechaCrea;
                            vModalidadSeleccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadSeleccion.UsuarioModifica;
                            vModalidadSeleccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadSeleccion.FechaModifica;
                        }
                        return vModalidadSeleccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pNombre">Cadena de texto con la modalidad de seleccion</param>
        /// <param name="pSigla">Cadena de texto con la sigla</param>
        /// <param name="pEstado">Booleano con el estado</param>
        /// <returns>Listado de modalidades de selecci�n que coinciden con los filtros</returns>
        public List<ModalidadSeleccion> ConsultarModalidadSeleccions(String pNombre, String pSigla, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ModalidadSeleccions_Consultar"))
                {
                    if (pNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pSigla != null)
                        vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pSigla);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ModalidadSeleccion> vListaModalidadSeleccion = new List<ModalidadSeleccion>();
                        while (vDataReaderResults.Read())
                        {
                            ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();
                            vModalidadSeleccion.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vModalidadSeleccion.IdModalidad;
                            vModalidadSeleccion.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vModalidadSeleccion.Nombre;
                            vModalidadSeleccion.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vModalidadSeleccion.Sigla;
                            vModalidadSeleccion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModalidadSeleccion.Estado;
                            if (vModalidadSeleccion.Estado)
                            {
                                vModalidadSeleccion.EstadoString="Activo";
                            } else
                            {
                                vModalidadSeleccion.EstadoString = "Inactivo";
                            }
                            vModalidadSeleccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadSeleccion.UsuarioCrea;
                            vModalidadSeleccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadSeleccion.FechaCrea;
                            vModalidadSeleccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadSeleccion.UsuarioModifica;
                            vModalidadSeleccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadSeleccion.FechaModifica;
                            vListaModalidadSeleccion.Add(vModalidadSeleccion);
                        }
                        return vListaModalidadSeleccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos), de la modalidad de seleccion, la 
        /// informaci�n del mismo
        /// </summary>
        /// <param name="pModalidadSeleccion">Entidad con la informaci�n del filtro</param>
        /// <returns>Entidad con la informaci�n de la modalidad recuperada</returns>
        public ModalidadSeleccion IdentificadorCodigoModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ModalidadSeleccion_IdentificadorCodigo"))
                {
                    if (pModalidadSeleccion.IdModalidad != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pModalidadSeleccion.IdModalidad);
                    if (pModalidadSeleccion.CodigoModalidad != null) 
                        vDataBase.AddInParameter(vDbCommand, "@CodigoModalidad", DbType.String, pModalidadSeleccion.CodigoModalidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();
                        while (vDataReaderResults.Read())
                        {
                            vModalidadSeleccion.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vModalidadSeleccion.IdModalidad;
                            vModalidadSeleccion.CodigoModalidad = vDataReaderResults["CodigoModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoModalidad"].ToString()) : vModalidadSeleccion.CodigoModalidad;
                            vModalidadSeleccion.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vModalidadSeleccion.Nombre;
                            vModalidadSeleccion.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vModalidadSeleccion.Sigla;
                            vModalidadSeleccion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModalidadSeleccion.Estado;
                            vModalidadSeleccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadSeleccion.UsuarioCrea;
                            vModalidadSeleccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadSeleccion.FechaCrea;
                            vModalidadSeleccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadSeleccion.UsuarioModifica;
                            vModalidadSeleccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadSeleccion.FechaModifica;
                        }
                        return vModalidadSeleccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModalidadSeleccion> ConsultarModalidadSeleccionTipoContrato(int pIdTipocontrato, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Sia_Contrato_ModalidadSeleccionTipoContato_Consultar"))
                {
                    
                       vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pIdTipocontrato);                   
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ModalidadSeleccion> vListaModalidadSeleccion = new List<ModalidadSeleccion>();
                        while (vDataReaderResults.Read())
                        {
                            ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();
                            vModalidadSeleccion.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vModalidadSeleccion.IdModalidad;
                            vModalidadSeleccion.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vModalidadSeleccion.Nombre;
                            vModalidadSeleccion.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vModalidadSeleccion.Sigla;
                            vModalidadSeleccion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModalidadSeleccion.Estado;
                            if (vModalidadSeleccion.Estado)
                            {
                                vModalidadSeleccion.EstadoString = "Activo";
                            }
                            else
                            {
                                vModalidadSeleccion.EstadoString = "Inactivo";
                            }
                            vModalidadSeleccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadSeleccion.UsuarioCrea;
                            vModalidadSeleccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadSeleccion.FechaCrea;
                            vModalidadSeleccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadSeleccion.UsuarioModifica;
                            vModalidadSeleccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadSeleccion.FechaModifica;
                            vListaModalidadSeleccion.Add(vModalidadSeleccion);
                        }
                        return vListaModalidadSeleccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
