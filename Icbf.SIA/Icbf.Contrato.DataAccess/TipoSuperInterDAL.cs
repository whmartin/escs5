﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad TipoSuperInter
    /// </summary>
    public class TipoSuperInterDAL : GeneralDAL
    {
        public TipoSuperInterDAL()
        {
        }
       

        /// <summary>
        /// Método de consulta por filtros para la entidad TipoSuperInter
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pDescripcion"></param>
        public List<TipoSuperInter> ConsultarTipoSuperInters(String pCodigo, String pDescripcion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoSuperInters_Consultar"))
                {
                    if (pCodigo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pCodigo);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoSuperInter> vListaTipoSuperInter = new List<TipoSuperInter>();
                        while (vDataReaderResults.Read())
                        {
                            TipoSuperInter vTipoSuperInter = new TipoSuperInter();
                            vTipoSuperInter.IDTipoSuperInter = vDataReaderResults["IDTipoSuperInter"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSuperInter"].ToString()) : vTipoSuperInter.IDTipoSuperInter;
                            vTipoSuperInter.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vTipoSuperInter.Codigo;
                            vTipoSuperInter.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoSuperInter.Descripcion;
                            vTipoSuperInter.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoSuperInter.Estado;
                            vTipoSuperInter.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoSuperInter.UsuarioCrea;
                            vTipoSuperInter.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoSuperInter.FechaCrea;
                            vTipoSuperInter.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoSuperInter.UsuarioModifica;
                            vTipoSuperInter.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoSuperInter.FechaModifica;
                            vListaTipoSuperInter.Add(vTipoSuperInter);
                        }
                        return vListaTipoSuperInter;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
