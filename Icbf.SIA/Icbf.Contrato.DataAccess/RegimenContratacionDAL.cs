using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    ///  Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad r�gimen Contrataci�n
    /// </summary>
    public class RegimenContratacionDAL : GeneralDAL
    {
        public RegimenContratacionDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int InsertarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RegimenContratacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRegimenContratacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRegimenContratacion", DbType.String, pRegimenContratacion.NombreRegimenContratacion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pRegimenContratacion.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRegimenContratacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRegimenContratacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRegimenContratacion.IdRegimenContratacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRegimenContratacion").ToString());
                    GenerarLogAuditoria(pRegimenContratacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int ModificarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RegimenContratacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRegimenContratacion", DbType.Int32, pRegimenContratacion.IdRegimenContratacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRegimenContratacion", DbType.String, pRegimenContratacion.NombreRegimenContratacion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pRegimenContratacion.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRegimenContratacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRegimenContratacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRegimenContratacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int EliminarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RegimenContratacion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRegimenContratacion", DbType.Int32, pRegimenContratacion.IdRegimenContratacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRegimenContratacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pIdRegimenContratacion"></param>
        /// <returns></returns>
        public RegimenContratacion ConsultarRegimenContratacion(int pIdRegimenContratacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RegimenContratacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegimenContratacion", DbType.Int32, pIdRegimenContratacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegimenContratacion vRegimenContratacion = new RegimenContratacion();
                        while (vDataReaderResults.Read())
                        {
                            vRegimenContratacion.IdRegimenContratacion = vDataReaderResults["IdRegimenContratacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegimenContratacion"].ToString()) : vRegimenContratacion.IdRegimenContratacion;
                            vRegimenContratacion.NombreRegimenContratacion = vDataReaderResults["NombreRegimenContratacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegimenContratacion"].ToString()) : vRegimenContratacion.NombreRegimenContratacion;
                            vRegimenContratacion.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vRegimenContratacion.Descripcion;
                            vRegimenContratacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRegimenContratacion.Estado;
                            vRegimenContratacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegimenContratacion.UsuarioCrea;
                            vRegimenContratacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegimenContratacion.FechaCrea;
                            vRegimenContratacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegimenContratacion.UsuarioModifica;
                            vRegimenContratacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegimenContratacion.FechaModifica;
                        }
                        return vRegimenContratacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pNombreRegimenContratacion"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<RegimenContratacion> ConsultarRegimenContratacions(String pNombreRegimenContratacion,String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RegimenContratacions_Consultar"))
                {
                    if(pNombreRegimenContratacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreRegimenContratacion", DbType.String, pNombreRegimenContratacion);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegimenContratacion> vListaRegimenContratacion = new List<RegimenContratacion>();
                        while (vDataReaderResults.Read())
                        {
                            RegimenContratacion vRegimenContratacion = new RegimenContratacion();
                            vRegimenContratacion.IdRegimenContratacion = vDataReaderResults["IdRegimenContratacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegimenContratacion"].ToString()) : vRegimenContratacion.IdRegimenContratacion;
                            vRegimenContratacion.NombreRegimenContratacion = vDataReaderResults["NombreRegimenContratacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegimenContratacion"].ToString()) : vRegimenContratacion.NombreRegimenContratacion;
                            vRegimenContratacion.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vRegimenContratacion.Descripcion;
                            vRegimenContratacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRegimenContratacion.Estado;
                            if (vRegimenContratacion.Estado)
                            {
                                vRegimenContratacion.EstadoString = "Activo";
                            } else
                            {
                                vRegimenContratacion.EstadoString = "Inactivo";
                            }
                            vRegimenContratacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegimenContratacion.UsuarioCrea;
                            vRegimenContratacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegimenContratacion.FechaCrea;
                            vRegimenContratacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegimenContratacion.UsuarioModifica;
                            vRegimenContratacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegimenContratacion.FechaModifica;
                            vListaRegimenContratacion.Add(vRegimenContratacion);
                        }
                        return vListaRegimenContratacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
