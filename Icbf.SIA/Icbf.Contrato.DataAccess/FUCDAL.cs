using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class FUCDAL : GeneralDAL
    {
        public FUCDAL()
        {
        }
        public int InsertarFUC(FUC pFUC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FUC_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@Id", DbType.Int64, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.String, pFUC.Regional);
                    vDataBase.AddInParameter(vDbCommand, "@CodRegional", DbType.String, pFUC.CodRegional);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.Int32, pFUC.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pFUC.FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Modalidad", DbType.String, pFUC.Modalidad);
                    vDataBase.AddInParameter(vDbCommand, "@ProcesoSeleccion", DbType.String, pFUC.ProcesoSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@CategoriaContrato", DbType.String, pFUC.CategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseContrato", DbType.String, pFUC.ClaseContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdAreaSolicita", DbType.Int32, pFUC.IdAreaSolicita);
                    vDataBase.AddInParameter(vDbCommand, "@Area", DbType.String, pFUC.Area);
                    vDataBase.AddInParameter(vDbCommand, "@IdNaturaleza", DbType.Int32, pFUC.IdNaturaleza);
                    vDataBase.AddInParameter(vDbCommand, "@Naturaleza", DbType.String, pFUC.Naturaleza);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaTipoIdentificacion", DbType.String, pFUC.ContratistaTipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaIdentificacion", DbType.String, pFUC.ContratistaIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaDigVerifi", DbType.Int32, pFUC.ContratistaDigVerifi);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaNombre", DbType.String, pFUC.ContratistaNombre);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaProfesion", DbType.String, pFUC.ContratistaProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaDireccion", DbType.String, pFUC.ContratistaDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaTelefono", DbType.String, pFUC.ContratistaTelefono);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaLugarUbicacion", DbType.String, pFUC.ContratistaLugarUbicacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoActividad", DbType.Int32, pFUC.IdTipoActividad);
                    vDataBase.AddInParameter(vDbCommand, "@TipoActividad", DbType.String, pFUC.TipoActividad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoOrganizacion", DbType.Int32, pFUC.IdTipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@TipoOrganizacion", DbType.String, pFUC.TipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaRepresentanteLegal", DbType.String, pFUC.ContratistaRepresentanteLegal);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaRepresentanteIdentificacion", DbType.String, pFUC.ContratistaRepresentanteIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp1TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp1Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp1DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp1PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp2TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp2Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp2DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp2PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp3TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp3Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp3DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp3PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp4TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp4Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp4DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp4PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp5TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp5Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp5DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp5PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@AfectacionRecurso", DbType.String, pFUC.AfectacionRecurso);
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoSecop", DbType.Int32, pFUC.IdCodigoSecop);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSecop", DbType.String, pFUC.CodigoSecop);
                    vDataBase.AddInParameter(vDbCommand, "@ObjectoContrato", DbType.String, pFUC.ObjectoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@LugarEjecucion", DbType.String, pFUC.LugarEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFUC.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacion", DbType.DateTime, pFUC.FechaTerminacion);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoInicial", DbType.String, pFUC.PlazoInicial);
                    vDataBase.AddInParameter(vDbCommand, "@CDPNumero", DbType.String, pFUC.CDPNumero);
                    vDataBase.AddInParameter(vDbCommand, "@CDPFecha", DbType.DateTime, pFUC.CDPFecha);
                    vDataBase.AddInParameter(vDbCommand, "@CDPValor", DbType.Decimal, pFUC.CDPValor);
                    vDataBase.AddInParameter(vDbCommand, "@RPNumero", DbType.String, pFUC.RPNumero);
                    vDataBase.AddInParameter(vDbCommand, "@RPFecha", DbType.DateTime, pFUC.RPFecha);
                    vDataBase.AddInParameter(vDbCommand, "@RPValor", DbType.Decimal, pFUC.RPValor);
                    vDataBase.AddInParameter(vDbCommand, "@RPRubro1", DbType.String, pFUC.RPRubro1);
                    vDataBase.AddInParameter(vDbCommand, "@RPRubro2", DbType.String, pFUC.RPRubro2);
                    vDataBase.AddInParameter(vDbCommand, "@RPRubro3", DbType.String, pFUC.RPRubro3);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInicialContrato", DbType.Decimal, pFUC.ValorInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FormaPago", DbType.Decimal, pFUC.FormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@Anticipo", DbType.Decimal, pFUC.Anticipo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAnticipado", DbType.Decimal, pFUC.ValorAnticipado);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1IdPersona", DbType.Int32, pFUC.Confinanciador1IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1TipoPersona", DbType.String, pFUC.Confinanciador1TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1TipoIdentificacion", DbType.String, pFUC.Confinanciador1TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1NumeroIdentificacion", DbType.String, pFUC.Confinanciador1NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1NombreRazonSocial", DbType.String, pFUC.Confinanciador1NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1Valor", DbType.Decimal, pFUC.Confinanciador1Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2IdPersona", DbType.Int32, pFUC.Confinanciador2IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2TipoPersona", DbType.String, pFUC.Confinanciador2TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2TipoIdentificacion", DbType.String, pFUC.Confinanciador2TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2NumeroIdentificacion", DbType.String, pFUC.Confinanciador2NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2NombreRazonSocial", DbType.String, pFUC.Confinanciador2NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2Valor", DbType.Decimal, pFUC.Confinanciador2Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3IdPersona", DbType.Int32, pFUC.Confinanciador3IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3TipoPersona", DbType.String, pFUC.Confinanciador3TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3TipoIdentificacion", DbType.String, pFUC.Confinanciador3TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3NumeroIdentificacion", DbType.String, pFUC.Confinanciador3NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3NombreRazonSocial", DbType.String, pFUC.Confinanciador3NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3Valor", DbType.Decimal, pFUC.Confinanciador3Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4IdPersona", DbType.Int32, pFUC.Confinanciador4IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4TipoPersona", DbType.String, pFUC.Confinanciador4TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4TipoIdentificacion", DbType.String, pFUC.Confinanciador4TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4NumeroIdentificacion", DbType.String, pFUC.Confinanciador4NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4NombreRazonSocial", DbType.String, pFUC.Confinanciador4NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4Valor", DbType.Decimal, pFUC.Confinanciador4Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5IdPersona", DbType.Int32, pFUC.Confinanciador5IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5TipoPersona", DbType.String, pFUC.Confinanciador5TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5TipoIdentificacion", DbType.String, pFUC.Confinanciador5TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5NumeroIdentificacion", DbType.String, pFUC.Confinanciador5NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5NombreRazonSocial", DbType.String, pFUC.Confinanciador5NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5Valor", DbType.Decimal, pFUC.Confinanciador5Valor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalCofinanciacion", DbType.Decimal, pFUC.ValorTotalCofinanciacion);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFuturaNumero", DbType.String, pFUC.VigenciaFuturaNumero);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFuturaValor", DbType.Decimal, pFUC.VigenciaFuturaValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalInicialContrato", DbType.Decimal, pFUC.ValorTotalInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInicialContratoIncluidaCofinanciacion", DbType.Decimal, pFUC.ValorInicialContratoIncluidaCofinanciacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAprobacionGarantias", DbType.DateTime, pFUC.FechaAprobacionGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@TipoGarantia", DbType.String, pFUC.TipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraNumeroIdentificacion", DbType.String, pFUC.EntidadAseguradoraNumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraGarantias", DbType.String, pFUC.EntidadAseguradoraGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraTipoGarantia", DbType.String, pFUC.EntidadAseguradoraTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRiesgos", DbType.Int32, pFUC.IdRiesgos);
                    vDataBase.AddInParameter(vDbCommand, "@GarantiasRiesgosAsegurados", DbType.String, pFUC.GarantiasRiesgosAsegurados);
                    vDataBase.AddInParameter(vDbCommand, "@GarantiasPorcentajeAsegurado", DbType.Decimal, pFUC.GarantiasPorcentajeAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@GarantiasValorTotalAsegurado", DbType.Decimal, pFUC.GarantiasValorTotalAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pFUC.IdArea);
                    vDataBase.AddInParameter(vDbCommand, "@Supervisor", DbType.String, pFUC.Supervisor);
                    vDataBase.AddInParameter(vDbCommand, "@SupervisorArea", DbType.String, pFUC.SupervisorArea);
                    vDataBase.AddInParameter(vDbCommand, "@SupervisorCargo", DbType.String, pFUC.SupervisorCargo);
                    vDataBase.AddInParameter(vDbCommand, "@SupervisorNombre", DbType.String, pFUC.SupervisorNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SupervisorIdentificacion", DbType.String, pFUC.SupervisorIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSupervisor", DbType.String, pFUC.IdTipoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@TipoSupervisor", DbType.String, pFUC.TipoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@IdentificacionGasto", DbType.String, pFUC.IdentificacionGasto);
                    vDataBase.AddInParameter(vDbCommand, "@NombreOrdenadorGasto", DbType.String, pFUC.NombreOrdenadorGasto);
                    vDataBase.AddInParameter(vDbCommand, "@IdOrdenadorGasto", DbType.Int32, pFUC.IdOrdenadorGasto);
                    vDataBase.AddInParameter(vDbCommand, "@CargoOrdenadorGasto", DbType.String, pFUC.CargoOrdenadorGasto);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga1Fecha", DbType.DateTime, pFUC.Prorroga1Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga1Plazo", DbType.String, pFUC.Prorroga1Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga2Fecha", DbType.DateTime, pFUC.Prorroga2Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga2Plazo", DbType.String, pFUC.Prorroga2Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga3Fecha", DbType.DateTime, pFUC.Prorroga3Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga3Plazo", DbType.String, pFUC.Prorroga3Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga4Fecha", DbType.DateTime, pFUC.Prorroga4Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga4Plazo", DbType.String, pFUC.Prorroga4Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga5Fecha", DbType.DateTime, pFUC.Prorroga5Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga5Plazo", DbType.String, pFUC.Prorroga5Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@ProrrogaTotalDias", DbType.Int32, pFUC.ProrrogaTotalDias);
                    vDataBase.AddInParameter(vDbCommand, "@DisminucionDias", DbType.Int32, pFUC.DisminucionDias);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionAnticipada", DbType.DateTime, pFUC.FechaTerminacionAnticipada);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoTotalContrato", DbType.Int32, pFUC.PlazoTotalContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionFinal", DbType.DateTime, pFUC.FechaTerminacionFinal);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion1FechaSuscripcion", DbType.DateTime, pFUC.Adicion1FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion1RP", DbType.String, pFUC.Adicion1RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion1Valor", DbType.Decimal, pFUC.Adicion1Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion2FechaSuscripcion", DbType.DateTime, pFUC.Adicion2FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion2RP", DbType.String, pFUC.Adicion2RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion2Valor", DbType.Decimal, pFUC.Adicion2Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion3FechaSuscripcion", DbType.DateTime, pFUC.Adicion3FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion3RP", DbType.String, pFUC.Adicion3RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion3Valor", DbType.Decimal, pFUC.Adicion3Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion4FechaSuscripcion", DbType.DateTime, pFUC.Adicion4FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion4RP", DbType.String, pFUC.Adicion4RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion4Valor", DbType.Decimal, pFUC.Adicion4Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion5FechaSuscripcion", DbType.DateTime, pFUC.Adicion5FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion5RP", DbType.String, pFUC.Adicion5RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion5Valor", DbType.Decimal, pFUC.Adicion5Valor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalAdicion", DbType.Decimal, pFUC.ValorTotalAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@DisminucionValor", DbType.Decimal, pFUC.DisminucionValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorFinalContrato", DbType.Decimal, pFUC.ValorFinalContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorFinalContratoCofinanciacion", DbType.Decimal, pFUC.ValorFinalContratoCofinanciacion);
                    vDataBase.AddInParameter(vDbCommand, "@DesembolsoEfectuado", DbType.Decimal, pFUC.DesembolsoEfectuado);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvanceFisico", DbType.String, pFUC.PorcentajeAvanceFisico);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvanceFisicoReal", DbType.String, pFUC.PorcentajeAvanceFisicoReal);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvancePresupuestado", DbType.String, pFUC.PorcentajeAvancePresupuestado);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvancePresupuestadoReal", DbType.String, pFUC.PorcentajeAvancePresupuestadoReal);
                    vDataBase.AddInParameter(vDbCommand, "@FechaLiquidacion", DbType.DateTime, pFUC.FechaLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoContrato", DbType.String, pFUC.EstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario1Identificacion", DbType.String, pFUC.Cesionario1Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario1TipoIdentificacion", DbType.String, pFUC.Cesionario1TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario1NombreRazonSocial", DbType.String, pFUC.Cesionario1NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario2Identificacion", DbType.String, pFUC.Cesionario2Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario2TipoIdentificacion", DbType.String, pFUC.Cesionario2TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario2NombreRazonSocial", DbType.String, pFUC.Cesionario2NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario3Identificacion", DbType.String, pFUC.Cesionario3Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario3TipoIdentificacion", DbType.String, pFUC.Cesionario3TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario3NombreRazonSocial", DbType.String, pFUC.Cesionario3NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario4Identificacion", DbType.String, pFUC.Cesionario4Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario4TipoIdentificacion", DbType.String, pFUC.Cesionario4TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario4NombreRazonSocial", DbType.String, pFUC.Cesionario4NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario5Identificacion", DbType.String, pFUC.Cesionario5Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario5TipoIdentificacion", DbType.String, pFUC.Cesionario5TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario5NombreRazonSocial", DbType.String, pFUC.Cesionario5NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioSuspencion", DbType.DateTime, pFUC.FechaInicioSuspencion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionSuspension", DbType.DateTime, pFUC.FechaTerminacionSuspension);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pFUC.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFUC.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFUC.Id = Convert.ToInt64(vDataBase.GetParameterValue(vDbCommand, "@Id").ToString());
                    GenerarLogAuditoria(pFUC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarFUC(FUC pFUC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FUC_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@Id", DbType.Int64, pFUC.Id);
                    vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.String, pFUC.Regional);
                    vDataBase.AddInParameter(vDbCommand, "@CodRegional", DbType.String, pFUC.CodRegional);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pFUC.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pFUC.FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Modalidad", DbType.String, pFUC.Modalidad);
                    vDataBase.AddInParameter(vDbCommand, "@ProcesoSeleccion", DbType.String, pFUC.ProcesoSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@CategoriaContrato", DbType.String, pFUC.CategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseContrato", DbType.String, pFUC.ClaseContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdAreaSolicita", DbType.String, pFUC.IdAreaSolicita);
                    vDataBase.AddInParameter(vDbCommand, "@Area", DbType.String, pFUC.Area);
                    vDataBase.AddInParameter(vDbCommand, "@IdNaturaleza", DbType.String, pFUC.IdNaturaleza);
                    vDataBase.AddInParameter(vDbCommand, "@Naturaleza", DbType.String, pFUC.Naturaleza);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaTipoIdentificacion", DbType.String, pFUC.ContratistaTipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaIdentificacion", DbType.String, pFUC.ContratistaIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaDigVerifi", DbType.String, pFUC.ContratistaDigVerifi);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaNombre", DbType.String, pFUC.ContratistaNombre);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaProfesion", DbType.String, pFUC.ContratistaProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaDireccion", DbType.String, pFUC.ContratistaDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaTelefono", DbType.String, pFUC.ContratistaTelefono);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaLugarUbicacion", DbType.String, pFUC.ContratistaLugarUbicacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoActividad", DbType.String, pFUC.IdTipoActividad);
                    vDataBase.AddInParameter(vDbCommand, "@TipoActividad", DbType.String, pFUC.TipoActividad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoOrganizacion", DbType.String, pFUC.IdTipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@TipoOrganizacion", DbType.String, pFUC.TipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaRepresentanteLegal", DbType.String, pFUC.ContratistaRepresentanteLegal);
                    vDataBase.AddInParameter(vDbCommand, "@ContratistaRepresentanteIdentificacion", DbType.String, pFUC.ContratistaRepresentanteIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp1TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp1Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1DigitoVerificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp1DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1PorcentajeParticipacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp1PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp2TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp2Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2DigitoVerificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp2DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2PorcentajeParticipacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp2PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp3TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp3Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3DigitoVerificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp3DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3PorcentajeParticipacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp3PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp4TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp4Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4DigitoVerificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp4DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4PorcentajeParticipacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp4PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp5TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp5Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5DigitoVerificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp5DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5PorcentajeParticipacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp5PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@AfectacionRecurso", DbType.String, pFUC.AfectacionRecurso);
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoSecop", DbType.String, pFUC.IdCodigoSecop);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSecop", DbType.String, pFUC.CodigoSecop);
                    vDataBase.AddInParameter(vDbCommand, "@ObjectoContrato", DbType.String, pFUC.ObjectoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@LugarEjecucion", DbType.String, pFUC.LugarEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFUC.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacion", DbType.DateTime, pFUC.FechaTerminacion);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoInicial", DbType.String, pFUC.PlazoInicial);
                    vDataBase.AddInParameter(vDbCommand, "@CDPNumero", DbType.String, pFUC.CDPNumero);
                    vDataBase.AddInParameter(vDbCommand, "@CDPFecha", DbType.DateTime, pFUC.CDPFecha);
                    vDataBase.AddInParameter(vDbCommand, "@CDPValor", DbType.String, pFUC.CDPValor);
                    vDataBase.AddInParameter(vDbCommand, "@RPNumero", DbType.String, pFUC.RPNumero);
                    vDataBase.AddInParameter(vDbCommand, "@RPFecha", DbType.DateTime, pFUC.RPFecha);
                    vDataBase.AddInParameter(vDbCommand, "@RPValor", DbType.String, pFUC.RPValor);
                    vDataBase.AddInParameter(vDbCommand, "@RPRubro1", DbType.String, pFUC.RPRubro1);
                    vDataBase.AddInParameter(vDbCommand, "@RPRubro2", DbType.String, pFUC.RPRubro2);
                    vDataBase.AddInParameter(vDbCommand, "@RPRubro3", DbType.String, pFUC.RPRubro3);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInicialContrato", DbType.String, pFUC.ValorInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FormaPago", DbType.String, pFUC.FormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@Anticipo", DbType.String, pFUC.Anticipo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAnticipado", DbType.String, pFUC.ValorAnticipado);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1IdPersona", DbType.String, pFUC.Confinanciador1IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1TipoPersona", DbType.String, pFUC.Confinanciador1TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1TipoIdentificacion", DbType.String, pFUC.Confinanciador1TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1NumeroIdentificacion", DbType.String, pFUC.Confinanciador1NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1NombreRazonSocial", DbType.String, pFUC.Confinanciador1NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador1Valor", DbType.String, pFUC.Confinanciador1Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2IdPersona", DbType.String, pFUC.Confinanciador2IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2TipoPersona", DbType.String, pFUC.Confinanciador2TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2TipoIdentificacion", DbType.String, pFUC.Confinanciador2TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2NumeroIdentificacion", DbType.String, pFUC.Confinanciador2NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2NombreRazonSocial", DbType.String, pFUC.Confinanciador2NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador2Valor", DbType.String, pFUC.Confinanciador2Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3IdPersona", DbType.String, pFUC.Confinanciador3IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3TipoPersona", DbType.String, pFUC.Confinanciador3TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3TipoIdentificacion", DbType.String, pFUC.Confinanciador3TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3NumeroIdentificacion", DbType.String, pFUC.Confinanciador3NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3NombreRazonSocial", DbType.String, pFUC.Confinanciador3NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador3Valor", DbType.String, pFUC.Confinanciador3Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4IdPersona", DbType.String, pFUC.Confinanciador4IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4TipoPersona", DbType.String, pFUC.Confinanciador4TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4TipoIdentificacion", DbType.String, pFUC.Confinanciador4TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4NumeroIdentificacion", DbType.String, pFUC.Confinanciador4NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4NombreRazonSocial", DbType.String, pFUC.Confinanciador4NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador4Valor", DbType.String, pFUC.Confinanciador4Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5IdPersona", DbType.String, pFUC.Confinanciador5IdPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5TipoPersona", DbType.String, pFUC.Confinanciador5TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5TipoIdentificacion", DbType.String, pFUC.Confinanciador5TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5NumeroIdentificacion", DbType.String, pFUC.Confinanciador5NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5NombreRazonSocial", DbType.String, pFUC.Confinanciador5NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Confinanciador5Valor", DbType.String, pFUC.Confinanciador5Valor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalCofinanciacion", DbType.String, pFUC.ValorTotalCofinanciacion);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFuturaNumero", DbType.String, pFUC.VigenciaFuturaNumero);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFuturaValor", DbType.String, pFUC.VigenciaFuturaValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalInicialContrato", DbType.String, pFUC.ValorTotalInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInicialContratoIncluidaCofinanciacion", DbType.String, pFUC.ValorInicialContratoIncluidaCofinanciacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAprobacionGarantias", DbType.DateTime, pFUC.FechaAprobacionGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@TipoGarantia", DbType.String, pFUC.TipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraNumeroIdentificacion", DbType.String, pFUC.EntidadAseguradoraNumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraGarantias", DbType.String, pFUC.EntidadAseguradoraGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraTipoGarantia", DbType.String, pFUC.EntidadAseguradoraTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRiesgos", DbType.String, pFUC.IdRiesgos);
                    vDataBase.AddInParameter(vDbCommand, "@GarantiasRiesgosAsegurados", DbType.String, pFUC.GarantiasRiesgosAsegurados);
                    vDataBase.AddInParameter(vDbCommand, "@GarantiasPorcentajeAsegurado", DbType.String, pFUC.GarantiasPorcentajeAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@GarantiasValorTotalAsegurado", DbType.String, pFUC.GarantiasValorTotalAsegurado);
                    vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.String, pFUC.IdArea);
                    vDataBase.AddInParameter(vDbCommand, "@Supervisor", DbType.String, pFUC.Supervisor);
                    vDataBase.AddInParameter(vDbCommand, "@SupervisorArea", DbType.String, pFUC.SupervisorArea);
                    vDataBase.AddInParameter(vDbCommand, "@SupervisorCargo", DbType.String, pFUC.SupervisorCargo);
                    vDataBase.AddInParameter(vDbCommand, "@SupervisorNombre", DbType.String, pFUC.SupervisorNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SupervisorIdentificacion", DbType.String, pFUC.SupervisorIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSupervisor", DbType.String, pFUC.IdTipoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@TipoSupervisor", DbType.String, pFUC.TipoSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@IdentificacionGasto", DbType.String, pFUC.IdentificacionGasto);
                    vDataBase.AddInParameter(vDbCommand, "@NombreOrdenadorGasto", DbType.String, pFUC.NombreOrdenadorGasto);
                    vDataBase.AddInParameter(vDbCommand, "@IdOrdenadorGasto", DbType.String, pFUC.IdOrdenadorGasto);
                    vDataBase.AddInParameter(vDbCommand, "@CargoOrdenadorGasto", DbType.String, pFUC.CargoOrdenadorGasto);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga1Fecha", DbType.DateTime, pFUC.Prorroga1Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga1Plazo", DbType.String, pFUC.Prorroga1Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga2Fecha", DbType.DateTime, pFUC.Prorroga2Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga2Plazo", DbType.String, pFUC.Prorroga2Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga3Fecha", DbType.DateTime, pFUC.Prorroga3Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga3Plazo", DbType.String, pFUC.Prorroga3Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga4Fecha", DbType.DateTime, pFUC.Prorroga4Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga4Plazo", DbType.String, pFUC.Prorroga4Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga5Fecha", DbType.DateTime, pFUC.Prorroga5Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Prorroga5Plazo", DbType.String, pFUC.Prorroga5Plazo);
                    vDataBase.AddInParameter(vDbCommand, "@ProrrogaTotalDias", DbType.String, pFUC.ProrrogaTotalDias);
                    vDataBase.AddInParameter(vDbCommand, "@DisminucionDias", DbType.String, pFUC.DisminucionDias);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionAnticipada", DbType.DateTime, pFUC.FechaTerminacionAnticipada);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoTotalContrato", DbType.String, pFUC.PlazoTotalContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionFinal", DbType.DateTime, pFUC.FechaTerminacionFinal);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion1FechaSuscripcion", DbType.DateTime, pFUC.Adicion1FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion1RP", DbType.String, pFUC.Adicion1RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion1Valor", DbType.String, pFUC.Adicion1Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion2FechaSuscripcion", DbType.DateTime, pFUC.Adicion2FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion2RP", DbType.String, pFUC.Adicion2RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion2Valor", DbType.String, pFUC.Adicion2Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion3FechaSuscripcion", DbType.DateTime, pFUC.Adicion3FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion3RP", DbType.String, pFUC.Adicion3RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion3Valor", DbType.String, pFUC.Adicion3Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion4FechaSuscripcion", DbType.DateTime, pFUC.Adicion4FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion4RP", DbType.String, pFUC.Adicion4RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion4Valor", DbType.String, pFUC.Adicion4Valor);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion5FechaSuscripcion", DbType.DateTime, pFUC.Adicion5FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion5RP", DbType.String, pFUC.Adicion5RP);
                    vDataBase.AddInParameter(vDbCommand, "@Adicion5Valor", DbType.String, pFUC.Adicion5Valor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalAdicion", DbType.String, pFUC.ValorTotalAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@DisminucionValor", DbType.String, pFUC.DisminucionValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorFinalContrato", DbType.String, pFUC.ValorFinalContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorFinalContratoCofinanciacion", DbType.String, pFUC.ValorFinalContratoCofinanciacion);
                    vDataBase.AddInParameter(vDbCommand, "@DesembolsoEfectuado", DbType.String, pFUC.DesembolsoEfectuado);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvanceFisico", DbType.String, pFUC.PorcentajeAvanceFisico);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvanceFisicoReal", DbType.String, pFUC.PorcentajeAvanceFisicoReal);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvancePresupuestado", DbType.String, pFUC.PorcentajeAvancePresupuestado);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvancePresupuestadoReal", DbType.String, pFUC.PorcentajeAvancePresupuestadoReal);
                    vDataBase.AddInParameter(vDbCommand, "@FechaLiquidacion", DbType.DateTime, pFUC.FechaLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoContrato", DbType.String, pFUC.EstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario1Identificacion", DbType.String, pFUC.Cesionario1Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario1TipoIdentificacion", DbType.String, pFUC.Cesionario1TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario1NombreRazonSocial", DbType.String, pFUC.Cesionario1NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario2Identificacion", DbType.String, pFUC.Cesionario2Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario2TipoIdentificacion", DbType.String, pFUC.Cesionario2TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario2NombreRazonSocial", DbType.String, pFUC.Cesionario2NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario3Identificacion", DbType.String, pFUC.Cesionario3Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario3TipoIdentificacion", DbType.String, pFUC.Cesionario3TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario3NombreRazonSocial", DbType.String, pFUC.Cesionario3NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario4Identificacion", DbType.String, pFUC.Cesionario4Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario4TipoIdentificacion", DbType.String, pFUC.Cesionario4TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario4NombreRazonSocial", DbType.String, pFUC.Cesionario4NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario5Identificacion", DbType.String, pFUC.Cesionario5Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario5TipoIdentificacion", DbType.String, pFUC.Cesionario5TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Cesionario5NombreRazonSocial", DbType.String, pFUC.Cesionario5NombreRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioSuspencion", DbType.DateTime, pFUC.FechaInicioSuspencion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionSuspension", DbType.DateTime, pFUC.FechaTerminacionSuspension);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pFUC.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFUC.UsuarioModifica);

                    //vDataBase.AddInParameter(vDbCommand, "@Id", DbType.Int64, pFUC.Id);
                    //vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.String, pFUC.Regional);
                    //vDataBase.AddInParameter(vDbCommand, "@CodRegional", DbType.String, pFUC.CodRegional);
                    //vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.Int32, pFUC.NumeroContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pFUC.FechaSuscripcion);
                    //vDataBase.AddInParameter(vDbCommand, "@Modalidad", DbType.String, pFUC.Modalidad);
                    //vDataBase.AddInParameter(vDbCommand, "@ProcesoSeleccion", DbType.String, pFUC.ProcesoSeleccion);
                    //vDataBase.AddInParameter(vDbCommand, "@CategoriaContrato", DbType.String, pFUC.CategoriaContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@ClaseContrato", DbType.String, pFUC.ClaseContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@IdAreaSolicita", DbType.Int32, pFUC.IdAreaSolicita);
                    //vDataBase.AddInParameter(vDbCommand, "@Area", DbType.String, pFUC.Area);
                    //vDataBase.AddInParameter(vDbCommand, "@IdNaturaleza", DbType.String, pFUC.IdNaturaleza);
                    //vDataBase.AddInParameter(vDbCommand, "@Naturaleza", DbType.String, pFUC.Naturaleza);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaTipoIdentificacion", DbType.String, pFUC.ContratistaTipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaIdentificacion", DbType.String, pFUC.ContratistaIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaDigVerifi", DbType.Int32, pFUC.ContratistaDigVerifi);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaNombre", DbType.String, pFUC.ContratistaNombre);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaProfesion", DbType.String, pFUC.ContratistaProfesion);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaDireccion", DbType.String, pFUC.ContratistaDireccion);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaTelefono", DbType.String, pFUC.ContratistaTelefono);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaLugarUbicacion", DbType.String, pFUC.ContratistaLugarUbicacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IdTipoActividad", DbType.Int32, pFUC.IdTipoActividad);
                    //vDataBase.AddInParameter(vDbCommand, "@TipoActividad", DbType.String, pFUC.TipoActividad);
                    //vDataBase.AddInParameter(vDbCommand, "@IdTipoOrganizacion", DbType.Int32, pFUC.IdTipoOrganizacion);
                    //vDataBase.AddInParameter(vDbCommand, "@TipoOrganizacion", DbType.String, pFUC.TipoOrganizacion);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaRepresentanteLegal", DbType.String, pFUC.ContratistaRepresentanteLegal);
                    //vDataBase.AddInParameter(vDbCommand, "@ContratistaRepresentanteIdentificacion", DbType.String, pFUC.ContratistaRepresentanteIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp1TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp1Nombre);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp1DigitoVerificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp1PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp1PorcentajeParticipacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp2TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp2Nombre);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp2DigitoVerificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp2PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp2PorcentajeParticipacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp3TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp3Nombre);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp3DigitoVerificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp3PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp3PorcentajeParticipacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp4TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp4Nombre);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp4DigitoVerificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp4PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp4PorcentajeParticipacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5TipoIdentificacion", DbType.String, pFUC.IntegrantesConsorcioUnionTemp5TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5Nombre", DbType.String, pFUC.IntegrantesConsorcioUnionTemp5Nombre);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5DigitoVerificacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp5DigitoVerificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IntegrantesConsorcioUnionTemp5PorcentajeParticipacion", DbType.Int32, pFUC.IntegrantesConsorcioUnionTemp5PorcentajeParticipacion);
                    //vDataBase.AddInParameter(vDbCommand, "@AfectacionRecurso", DbType.String, pFUC.AfectacionRecurso);
                    //vDataBase.AddInParameter(vDbCommand, "@IdCodigoSecop", DbType.Int32, pFUC.IdCodigoSecop);
                    //vDataBase.AddInParameter(vDbCommand, "@CodigoSecop", DbType.String, pFUC.CodigoSecop);
                    //vDataBase.AddInParameter(vDbCommand, "@ObjectoContrato", DbType.String, pFUC.ObjectoContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@LugarEjecucion", DbType.String, pFUC.LugarEjecucion);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFUC.FechaInicio);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaTerminacion", DbType.DateTime, pFUC.FechaTerminacion);
                    //vDataBase.AddInParameter(vDbCommand, "@PlazoInicial", DbType.String, pFUC.PlazoInicial);
                    //vDataBase.AddInParameter(vDbCommand, "@CDPNumero", DbType.String, pFUC.CDPNumero);
                    //vDataBase.AddInParameter(vDbCommand, "@CDPFecha", DbType.DateTime, pFUC.CDPFecha);
                    //vDataBase.AddInParameter(vDbCommand, "@CDPValor", DbType.Decimal, pFUC.CDPValor);
                    //vDataBase.AddInParameter(vDbCommand, "@RPNumero", DbType.String, pFUC.RPNumero);
                    //vDataBase.AddInParameter(vDbCommand, "@RPFecha", DbType.DateTime, pFUC.RPFecha);
                    //vDataBase.AddInParameter(vDbCommand, "@RPValor", DbType.Decimal, pFUC.RPValor);
                    //vDataBase.AddInParameter(vDbCommand, "@RPRubro1", DbType.String, pFUC.RPRubro1);
                    //vDataBase.AddInParameter(vDbCommand, "@RPRubro2", DbType.String, pFUC.RPRubro2);
                    //vDataBase.AddInParameter(vDbCommand, "@RPRubro3", DbType.String, pFUC.RPRubro3);
                    //vDataBase.AddInParameter(vDbCommand, "@ValorInicialContrato", DbType.Decimal, pFUC.ValorInicialContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@FormaPago", DbType.Decimal, pFUC.FormaPago);
                    //vDataBase.AddInParameter(vDbCommand, "@Anticipo", DbType.Decimal, pFUC.Anticipo);
                    //vDataBase.AddInParameter(vDbCommand, "@ValorAnticipado", DbType.Decimal, pFUC.ValorAnticipado);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador1IdPersona", DbType.Int32, pFUC.Confinanciador1IdPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador1TipoPersona", DbType.String, pFUC.Confinanciador1TipoPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador1TipoIdentificacion", DbType.String, pFUC.Confinanciador1TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador1NumeroIdentificacion", DbType.String, pFUC.Confinanciador1NumeroIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador1NombreRazonSocial", DbType.String, pFUC.Confinanciador1NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador1Valor", DbType.Decimal, pFUC.Confinanciador1Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador2IdPersona", DbType.Int32, pFUC.Confinanciador2IdPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador2TipoPersona", DbType.String, pFUC.Confinanciador2TipoPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador2TipoIdentificacion", DbType.String, pFUC.Confinanciador2TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador2NumeroIdentificacion", DbType.String, pFUC.Confinanciador2NumeroIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador2NombreRazonSocial", DbType.String, pFUC.Confinanciador2NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador2Valor", DbType.Decimal, pFUC.Confinanciador2Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador3IdPersona", DbType.Int32, pFUC.Confinanciador3IdPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador3TipoPersona", DbType.String, pFUC.Confinanciador3TipoPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador3TipoIdentificacion", DbType.String, pFUC.Confinanciador3TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador3NumeroIdentificacion", DbType.String, pFUC.Confinanciador3NumeroIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador3NombreRazonSocial", DbType.String, pFUC.Confinanciador3NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador3Valor", DbType.Decimal, pFUC.Confinanciador3Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador4IdPersona", DbType.Int32, pFUC.Confinanciador4IdPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador4TipoPersona", DbType.String, pFUC.Confinanciador4TipoPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador4TipoIdentificacion", DbType.String, pFUC.Confinanciador4TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador4NumeroIdentificacion", DbType.String, pFUC.Confinanciador4NumeroIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador4NombreRazonSocial", DbType.String, pFUC.Confinanciador4NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador4Valor", DbType.Decimal, pFUC.Confinanciador4Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador5IdPersona", DbType.Int32, pFUC.Confinanciador5IdPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador5TipoPersona", DbType.String, pFUC.Confinanciador5TipoPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador5TipoIdentificacion", DbType.String, pFUC.Confinanciador5TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador5NumeroIdentificacion", DbType.String, pFUC.Confinanciador5NumeroIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador5NombreRazonSocial", DbType.String, pFUC.Confinanciador5NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Confinanciador5Valor", DbType.Decimal, pFUC.Confinanciador5Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@ValorTotalCofinanciacion", DbType.Decimal, pFUC.ValorTotalCofinanciacion);
                    //vDataBase.AddInParameter(vDbCommand, "@VigenciaFuturaNumero", DbType.String, pFUC.VigenciaFuturaNumero);
                    //vDataBase.AddInParameter(vDbCommand, "@VigenciaFuturaValor", DbType.Decimal, pFUC.VigenciaFuturaValor);
                    //vDataBase.AddInParameter(vDbCommand, "@ValorTotalInicialContrato", DbType.Decimal, pFUC.ValorTotalInicialContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@ValorInicialContratoIncluidaCofinanciacion", DbType.Decimal, pFUC.ValorInicialContratoIncluidaCofinanciacion);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaAprobacionGarantias", DbType.DateTime, pFUC.FechaAprobacionGarantias);
                    //vDataBase.AddInParameter(vDbCommand, "@TipoGarantia", DbType.String, pFUC.TipoGarantia);
                    //vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraNumeroIdentificacion", DbType.String, pFUC.EntidadAseguradoraNumeroIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraGarantias", DbType.String, pFUC.EntidadAseguradoraGarantias);
                    //vDataBase.AddInParameter(vDbCommand, "@EntidadAseguradoraTipoGarantia", DbType.String, pFUC.EntidadAseguradoraTipoGarantia);
                    //vDataBase.AddInParameter(vDbCommand, "@IdRiesgos", DbType.Int32, pFUC.IdRiesgos);
                    //vDataBase.AddInParameter(vDbCommand, "@GarantiasRiesgosAsegurados", DbType.String, pFUC.GarantiasRiesgosAsegurados);
                    //vDataBase.AddInParameter(vDbCommand, "@GarantiasPorcentajeAsegurado", DbType.Decimal, pFUC.GarantiasPorcentajeAsegurado);
                    //vDataBase.AddInParameter(vDbCommand, "@GarantiasValorTotalAsegurado", DbType.Decimal, pFUC.GarantiasValorTotalAsegurado);
                    //vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pFUC.IdArea);
                    //vDataBase.AddInParameter(vDbCommand, "@Supervisor", DbType.String, pFUC.Supervisor);
                    //vDataBase.AddInParameter(vDbCommand, "@SupervisorArea", DbType.String, pFUC.SupervisorArea);
                    //vDataBase.AddInParameter(vDbCommand, "@SupervisorCargo", DbType.String, pFUC.SupervisorCargo);
                    //vDataBase.AddInParameter(vDbCommand, "@SupervisorNombre", DbType.String, pFUC.SupervisorNombre);
                    //vDataBase.AddInParameter(vDbCommand, "@SupervisorIdentificacion", DbType.String, pFUC.SupervisorIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@IdTipoSupervisor", DbType.String, pFUC.IdTipoSupervisor);
                    //vDataBase.AddInParameter(vDbCommand, "@TipoSupervisor", DbType.String, pFUC.TipoSupervisor);
                    //vDataBase.AddInParameter(vDbCommand, "@IdentificacionGasto", DbType.String, pFUC.IdentificacionGasto);
                    //vDataBase.AddInParameter(vDbCommand, "@NombreOrdenadorGasto", DbType.String, pFUC.NombreOrdenadorGasto);
                    //vDataBase.AddInParameter(vDbCommand, "@IdOrdenadorGasto", DbType.Int32, pFUC.IdOrdenadorGasto);
                    //vDataBase.AddInParameter(vDbCommand, "@CargoOrdenadorGasto", DbType.String, pFUC.CargoOrdenadorGasto);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga1Fecha", DbType.DateTime, pFUC.Prorroga1Fecha);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga1Plazo", DbType.String, pFUC.Prorroga1Plazo);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga2Fecha", DbType.DateTime, pFUC.Prorroga2Fecha);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga2Plazo", DbType.String, pFUC.Prorroga2Plazo);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga3Fecha", DbType.DateTime, pFUC.Prorroga3Fecha);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga3Plazo", DbType.String, pFUC.Prorroga3Plazo);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga4Fecha", DbType.DateTime, pFUC.Prorroga4Fecha);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga4Plazo", DbType.String, pFUC.Prorroga4Plazo);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga5Fecha", DbType.DateTime, pFUC.Prorroga5Fecha);
                    //vDataBase.AddInParameter(vDbCommand, "@Prorroga5Plazo", DbType.String, pFUC.Prorroga5Plazo);
                    //vDataBase.AddInParameter(vDbCommand, "@ProrrogaTotalDias", DbType.Int32, pFUC.ProrrogaTotalDias);
                    //vDataBase.AddInParameter(vDbCommand, "@DisminucionDias", DbType.Int32, pFUC.DisminucionDias);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionAnticipada", DbType.DateTime, pFUC.FechaTerminacionAnticipada);
                    //vDataBase.AddInParameter(vDbCommand, "@PlazoTotalContrato", DbType.Int32, pFUC.PlazoTotalContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionFinal", DbType.DateTime, pFUC.FechaTerminacionFinal);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion1FechaSuscripcion", DbType.DateTime, pFUC.Adicion1FechaSuscripcion);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion1RP", DbType.String, pFUC.Adicion1RP);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion1Valor", DbType.Decimal, pFUC.Adicion1Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion2FechaSuscripcion", DbType.DateTime, pFUC.Adicion2FechaSuscripcion);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion2RP", DbType.String, pFUC.Adicion2RP);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion2Valor", DbType.Decimal, pFUC.Adicion2Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion3FechaSuscripcion", DbType.DateTime, pFUC.Adicion3FechaSuscripcion);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion3RP", DbType.String, pFUC.Adicion3RP);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion3Valor", DbType.Decimal, pFUC.Adicion3Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion4FechaSuscripcion", DbType.DateTime, pFUC.Adicion4FechaSuscripcion);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion4RP", DbType.String, pFUC.Adicion4RP);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion4Valor", DbType.Decimal, pFUC.Adicion4Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion5FechaSuscripcion", DbType.DateTime, pFUC.Adicion5FechaSuscripcion);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion5RP", DbType.String, pFUC.Adicion5RP);
                    //vDataBase.AddInParameter(vDbCommand, "@Adicion5Valor", DbType.Decimal, pFUC.Adicion5Valor);
                    //vDataBase.AddInParameter(vDbCommand, "@ValorTotalAdicion", DbType.Decimal, pFUC.ValorTotalAdicion);
                    //vDataBase.AddInParameter(vDbCommand, "@DisminucionValor", DbType.Decimal, pFUC.DisminucionValor);
                    //vDataBase.AddInParameter(vDbCommand, "@ValorFinalContrato", DbType.Decimal, pFUC.ValorFinalContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@ValorFinalContratoCofinanciacion", DbType.Decimal, pFUC.ValorFinalContratoCofinanciacion);
                    //vDataBase.AddInParameter(vDbCommand, "@DesembolsoEfectuado", DbType.Decimal, pFUC.DesembolsoEfectuado);
                    //vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvanceFisico", DbType.String, pFUC.PorcentajeAvanceFisico);
                    //vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvanceFisicoReal", DbType.String, pFUC.PorcentajeAvanceFisicoReal);
                    //vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvancePresupuestado", DbType.String, pFUC.PorcentajeAvancePresupuestado);
                    //vDataBase.AddInParameter(vDbCommand, "@PorcentajeAvancePresupuestadoReal", DbType.String, pFUC.PorcentajeAvancePresupuestadoReal);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaLiquidacion", DbType.DateTime, pFUC.FechaLiquidacion);
                    //vDataBase.AddInParameter(vDbCommand, "@EstadoContrato", DbType.String, pFUC.EstadoContrato);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario1Identificacion", DbType.String, pFUC.Cesionario1Identificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario1TipoIdentificacion", DbType.String, pFUC.Cesionario1TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario1NombreRazonSocial", DbType.String, pFUC.Cesionario1NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario2Identificacion", DbType.String, pFUC.Cesionario2Identificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario2TipoIdentificacion", DbType.String, pFUC.Cesionario2TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario2NombreRazonSocial", DbType.String, pFUC.Cesionario2NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario3Identificacion", DbType.String, pFUC.Cesionario3Identificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario3TipoIdentificacion", DbType.String, pFUC.Cesionario3TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario3NombreRazonSocial", DbType.String, pFUC.Cesionario3NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario4Identificacion", DbType.String, pFUC.Cesionario4Identificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario4TipoIdentificacion", DbType.String, pFUC.Cesionario4TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario4NombreRazonSocial", DbType.String, pFUC.Cesionario4NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario5Identificacion", DbType.String, pFUC.Cesionario5Identificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario5TipoIdentificacion", DbType.String, pFUC.Cesionario5TipoIdentificacion);
                    //vDataBase.AddInParameter(vDbCommand, "@Cesionario5NombreRazonSocial", DbType.String, pFUC.Cesionario5NombreRazonSocial);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaInicioSuspencion", DbType.DateTime, pFUC.FechaInicioSuspencion);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaTerminacionSuspension", DbType.DateTime, pFUC.FechaTerminacionSuspension);
                    //vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pFUC.Observaciones);
                    //vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFUC.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFUC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarFUC(FUC pFUC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FUC_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@Id", DbType.Int64, pFUC.Id);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFUC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public FUC ConsultarFUC(Int64 pId)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FUC_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Id", DbType.Int64, pId);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        FUC vFUC = new FUC();
                        while (vDataReaderResults.Read())
                        {
                            vFUC.Id = vDataReaderResults["Id"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Id"].ToString()) : vFUC.Id;
                            vFUC.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vFUC.Regional;
                            vFUC.CodRegional = vDataReaderResults["CodRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegional"].ToString()) : vFUC.CodRegional;
                            vFUC.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vFUC.NumeroContrato;
                            vFUC.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaSuscripcion"].ToString()) : vFUC.FechaSuscripcion;
                            vFUC.Modalidad = vDataReaderResults["Modalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Modalidad"].ToString()) : vFUC.Modalidad;
                            vFUC.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidad"].ToString()) : vFUC.IdModalidad;
                            vFUC.ProcesoSeleccion = vDataReaderResults["ProcesoSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProcesoSeleccion"].ToString()) : vFUC.ProcesoSeleccion;
                            vFUC.CategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vFUC.CategoriaContrato;
                            vFUC.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdCategoriaContrato"].ToString()) : vFUC.IdCategoriaContrato;
                            vFUC.ClaseContrato = vDataReaderResults["ClaseContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseContrato"].ToString()) : vFUC.ClaseContrato;
                            vFUC.IdAreaSolicita = vDataReaderResults["IdAreaSolicita"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdAreaSolicita"].ToString()) : vFUC.IdAreaSolicita;
                            vFUC.Area = vDataReaderResults["Area"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Area"].ToString()) : vFUC.Area;
                            vFUC.IdNaturaleza = vDataReaderResults["IdNaturaleza"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdNaturaleza"].ToString()) : vFUC.IdNaturaleza;
                            vFUC.Naturaleza = vDataReaderResults["Naturaleza"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Naturaleza"].ToString()) : vFUC.Naturaleza;
                            vFUC.ContratistaTipoIdentificacion = vDataReaderResults["ContratistaTipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaTipoIdentificacion"].ToString()) : vFUC.ContratistaTipoIdentificacion;
                            vFUC.ContratistaIdentificacion = vDataReaderResults["ContratistaIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaIdentificacion"].ToString()) : vFUC.ContratistaIdentificacion;
                            vFUC.ContratistaDigVerifi = vDataReaderResults["ContratistaDigVerifi"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaDigVerifi"].ToString()) : vFUC.ContratistaDigVerifi;
                            vFUC.ContratistaNombre = vDataReaderResults["ContratistaNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaNombre"].ToString()) : vFUC.ContratistaNombre;
                            vFUC.ContratistaProfesion = vDataReaderResults["ContratistaProfesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaProfesion"].ToString()) : vFUC.ContratistaProfesion;
                            vFUC.ContratistaDireccion = vDataReaderResults["ContratistaDireccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaDireccion"].ToString()) : vFUC.ContratistaDireccion;
                            vFUC.ContratistaTelefono = vDataReaderResults["ContratistaTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaTelefono"].ToString()) : vFUC.ContratistaTelefono;
                            vFUC.ContratistaLugarUbicacion = vDataReaderResults["ContratistaLugarUbicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaLugarUbicacion"].ToString()) : vFUC.ContratistaLugarUbicacion;
                            vFUC.IdDepartamentoLugarUbicacion = vDataReaderResults["IdDepartamentoLugarUbicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDepartamentoLugarUbicacion"].ToString()) : vFUC.IdDepartamentoLugarUbicacion;
                            vFUC.IdContratistaLugarUbicacion = vDataReaderResults["IdContratistaLugarUbicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdContratistaLugarUbicacion"].ToString()) : vFUC.IdContratistaLugarUbicacion;
                            vFUC.IdTipoActividad = vDataReaderResults["IdTipoActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoActividad"].ToString()) : vFUC.IdTipoActividad;
                            vFUC.TipoActividad = vDataReaderResults["TipoActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoActividad"].ToString()) : vFUC.TipoActividad;
                            vFUC.IdTipoOrganizacion = vDataReaderResults["IdTipoOrganizacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoOrganizacion"].ToString()) : vFUC.IdTipoOrganizacion;
                            vFUC.TipoOrganizacion = vDataReaderResults["TipoOrganizacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoOrganizacion"].ToString()) : vFUC.TipoOrganizacion;
                            vFUC.ContratistaRepresentanteLegal = vDataReaderResults["ContratistaRepresentanteLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaRepresentanteLegal"].ToString()) : vFUC.ContratistaRepresentanteLegal;
                            vFUC.ContratistaRepresentanteIdentificacion = vDataReaderResults["ContratistaRepresentanteIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaRepresentanteIdentificacion"].ToString()) : vFUC.ContratistaRepresentanteIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp1TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp1TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp1TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp1TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp1Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp1Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp1Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp1Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp1DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp1DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp1DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp1DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp1PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp1PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp1PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp1PorcentajeParticipacion;
                            vFUC.IntegrantesConsorcioUnionTemp2TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp2TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp2TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp2TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp2Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp2Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp2Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp2Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp2DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp2DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp2DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp2DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp2PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp2PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp2PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp2PorcentajeParticipacion;
                            vFUC.IntegrantesConsorcioUnionTemp3TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp3TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp3TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp3TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp3Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp3Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp3Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp3Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp3DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp3DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp3DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp3DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp3PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp3PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp3PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp3PorcentajeParticipacion;
                            vFUC.IntegrantesConsorcioUnionTemp4TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp4TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp4TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp4TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp4Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp4Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp4Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp4Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp4DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp4DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp4DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp4DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp4PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp4PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp4PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp4PorcentajeParticipacion;
                            vFUC.IntegrantesConsorcioUnionTemp5TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp5TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp5TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp5TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp5Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp5Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp5Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp5Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp5DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp5DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp5DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp5DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp5PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp5PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp5PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp5PorcentajeParticipacion;
                            vFUC.AfectacionRecurso = vDataReaderResults["AfectacionRecurso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AfectacionRecurso"].ToString()) : vFUC.AfectacionRecurso;
                            vFUC.IdCodigoSecop = vDataReaderResults["IdCodigoSecop"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdCodigoSecop"].ToString()) : vFUC.IdCodigoSecop;
                            vFUC.CodigoSecop = vDataReaderResults["CodigoSecop"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSecop"].ToString()) : vFUC.CodigoSecop;
                            vFUC.ObjectoContrato = vDataReaderResults["ObjectoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjectoContrato"].ToString()) : vFUC.ObjectoContrato;
                            vFUC.LugarEjecucion = vDataReaderResults["LugarEjecucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LugarEjecucion"].ToString()) : vFUC.LugarEjecucion;
                            vFUC.idDptoLugarEjecucion = vDataReaderResults["idDptoLugarEjecucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["idDptoLugarEjecucion"].ToString()) : vFUC.idDptoLugarEjecucion;
                            vFUC.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaInicio"].ToString()) : vFUC.FechaInicio;
                            vFUC.FechaTerminacion = vDataReaderResults["FechaTerminacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaTerminacion"].ToString()) : vFUC.FechaTerminacion;
                            vFUC.PlazoInicial = vDataReaderResults["PlazoInicial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PlazoInicial"].ToString()) : vFUC.PlazoInicial;
                            vFUC.CDPNumero = vDataReaderResults["CDPNumero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CDPNumero"].ToString()) : vFUC.CDPNumero;
                            vFUC.CDPFecha = vDataReaderResults["CDPFecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CDPFecha"].ToString()) : vFUC.CDPFecha;
                            vFUC.CDPValor = vDataReaderResults["CDPValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CDPValor"].ToString()) : vFUC.CDPValor;
                            vFUC.RPNumero = vDataReaderResults["RPNumero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPNumero"].ToString()) : vFUC.RPNumero;
                            vFUC.RPFecha = vDataReaderResults["RPFecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPFecha"].ToString()) : vFUC.RPFecha;
                            vFUC.RPValor = vDataReaderResults["RPValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPValor"].ToString()) : vFUC.RPValor;
                            vFUC.RPRubro1 = vDataReaderResults["RPRubro1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPRubro1"].ToString()) : vFUC.RPRubro1;
                            vFUC.RPRubro2 = vDataReaderResults["RPRubro2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPRubro2"].ToString()) : vFUC.RPRubro2;
                            vFUC.RPRubro3 = vDataReaderResults["RPRubro3"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPRubro3"].ToString()) : vFUC.RPRubro3;
                            vFUC.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorInicialContrato"].ToString()) : vFUC.ValorInicialContrato;
                            vFUC.FormaPago = vDataReaderResults["FormaPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FormaPago"].ToString()) : vFUC.FormaPago;
                            vFUC.Anticipo = vDataReaderResults["Anticipo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Anticipo"].ToString()) : vFUC.Anticipo;
                            vFUC.ValorAnticipado = vDataReaderResults["ValorAnticipado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorAnticipado"].ToString()) : vFUC.ValorAnticipado;
                            vFUC.Confinanciador1IdPersona = vDataReaderResults["Confinanciador1IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1IdPersona"].ToString()) : vFUC.Confinanciador1IdPersona;
                            vFUC.Confinanciador1TipoPersona = vDataReaderResults["Confinanciador1TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1TipoPersona"].ToString()) : vFUC.Confinanciador1TipoPersona;
                            vFUC.Confinanciador1TipoIdentificacion = vDataReaderResults["Confinanciador1TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1TipoIdentificacion"].ToString()) : vFUC.Confinanciador1TipoIdentificacion;
                            vFUC.Confinanciador1NumeroIdentificacion = vDataReaderResults["Confinanciador1NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1NumeroIdentificacion"].ToString()) : vFUC.Confinanciador1NumeroIdentificacion;
                            vFUC.Confinanciador1NombreRazonSocial = vDataReaderResults["Confinanciador1NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1NombreRazonSocial"].ToString()) : vFUC.Confinanciador1NombreRazonSocial;
                            vFUC.Confinanciador1Valor = vDataReaderResults["Confinanciador1Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1Valor"].ToString()) : vFUC.Confinanciador1Valor;
                            vFUC.Confinanciador2IdPersona = vDataReaderResults["Confinanciador2IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2IdPersona"].ToString()) : vFUC.Confinanciador2IdPersona;
                            vFUC.Confinanciador2TipoPersona = vDataReaderResults["Confinanciador2TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2TipoPersona"].ToString()) : vFUC.Confinanciador2TipoPersona;
                            vFUC.Confinanciador2TipoIdentificacion = vDataReaderResults["Confinanciador2TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2TipoIdentificacion"].ToString()) : vFUC.Confinanciador2TipoIdentificacion;
                            vFUC.Confinanciador2NumeroIdentificacion = vDataReaderResults["Confinanciador2NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2NumeroIdentificacion"].ToString()) : vFUC.Confinanciador2NumeroIdentificacion;
                            vFUC.Confinanciador2NombreRazonSocial = vDataReaderResults["Confinanciador2NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2NombreRazonSocial"].ToString()) : vFUC.Confinanciador2NombreRazonSocial;
                            vFUC.Confinanciador2Valor = vDataReaderResults["Confinanciador2Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2Valor"].ToString()) : vFUC.Confinanciador2Valor;
                            vFUC.Confinanciador3IdPersona = vDataReaderResults["Confinanciador3IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3IdPersona"].ToString()) : vFUC.Confinanciador3IdPersona;
                            vFUC.Confinanciador3TipoPersona = vDataReaderResults["Confinanciador3TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3TipoPersona"].ToString()) : vFUC.Confinanciador3TipoPersona;
                            vFUC.Confinanciador3TipoIdentificacion = vDataReaderResults["Confinanciador3TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3TipoIdentificacion"].ToString()) : vFUC.Confinanciador3TipoIdentificacion;
                            vFUC.Confinanciador3NumeroIdentificacion = vDataReaderResults["Confinanciador3NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3NumeroIdentificacion"].ToString()) : vFUC.Confinanciador3NumeroIdentificacion;
                            vFUC.Confinanciador3NombreRazonSocial = vDataReaderResults["Confinanciador3NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3NombreRazonSocial"].ToString()) : vFUC.Confinanciador3NombreRazonSocial;
                            vFUC.Confinanciador3Valor = vDataReaderResults["Confinanciador3Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3Valor"].ToString()) : vFUC.Confinanciador3Valor;
                            vFUC.Confinanciador4IdPersona = vDataReaderResults["Confinanciador4IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4IdPersona"].ToString()) : vFUC.Confinanciador4IdPersona;
                            vFUC.Confinanciador4TipoPersona = vDataReaderResults["Confinanciador4TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4TipoPersona"].ToString()) : vFUC.Confinanciador4TipoPersona;
                            vFUC.Confinanciador4TipoIdentificacion = vDataReaderResults["Confinanciador4TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4TipoIdentificacion"].ToString()) : vFUC.Confinanciador4TipoIdentificacion;
                            vFUC.Confinanciador4NumeroIdentificacion = vDataReaderResults["Confinanciador4NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4NumeroIdentificacion"].ToString()) : vFUC.Confinanciador4NumeroIdentificacion;
                            vFUC.Confinanciador4NombreRazonSocial = vDataReaderResults["Confinanciador4NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4NombreRazonSocial"].ToString()) : vFUC.Confinanciador4NombreRazonSocial;
                            vFUC.Confinanciador4Valor = vDataReaderResults["Confinanciador4Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4Valor"].ToString()) : vFUC.Confinanciador4Valor;
                            vFUC.Confinanciador5IdPersona = vDataReaderResults["Confinanciador5IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5IdPersona"].ToString()) : vFUC.Confinanciador5IdPersona;
                            vFUC.Confinanciador5TipoPersona = vDataReaderResults["Confinanciador5TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5TipoPersona"].ToString()) : vFUC.Confinanciador5TipoPersona;
                            vFUC.Confinanciador5TipoIdentificacion = vDataReaderResults["Confinanciador5TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5TipoIdentificacion"].ToString()) : vFUC.Confinanciador5TipoIdentificacion;
                            vFUC.Confinanciador5NumeroIdentificacion = vDataReaderResults["Confinanciador5NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5NumeroIdentificacion"].ToString()) : vFUC.Confinanciador5NumeroIdentificacion;
                            vFUC.Confinanciador5NombreRazonSocial = vDataReaderResults["Confinanciador5NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5NombreRazonSocial"].ToString()) : vFUC.Confinanciador5NombreRazonSocial;
                            vFUC.Confinanciador5Valor = vDataReaderResults["Confinanciador5Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5Valor"].ToString()) : vFUC.Confinanciador5Valor;
                            vFUC.ValorTotalCofinanciacion = vDataReaderResults["ValorTotalCofinanciacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorTotalCofinanciacion"].ToString()) : vFUC.ValorTotalCofinanciacion;
                            vFUC.VigenciaFuturaNumero = vDataReaderResults["VigenciaFuturaNumero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaFuturaNumero"].ToString()) : vFUC.VigenciaFuturaNumero;
                            vFUC.VigenciaFuturaValor = vDataReaderResults["VigenciaFuturaValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaFuturaValor"].ToString()) : vFUC.VigenciaFuturaValor;
                            vFUC.ValorTotalInicialContrato = vDataReaderResults["ValorTotalInicialContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorTotalInicialContrato"].ToString()) : vFUC.ValorTotalInicialContrato;
                            vFUC.ValorInicialContratoIncluidaCofinanciacion = vDataReaderResults["ValorInicialContratoIncluidaCofinanciacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorInicialContratoIncluidaCofinanciacion"].ToString()) : vFUC.ValorInicialContratoIncluidaCofinanciacion;
                            vFUC.FechaAprobacionGarantias = vDataReaderResults["FechaAprobacionGarantias"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaAprobacionGarantias"].ToString()) : vFUC.FechaAprobacionGarantias;
                            vFUC.TipoGarantia = vDataReaderResults["TipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoGarantia"].ToString()) : vFUC.TipoGarantia;
                            vFUC.EntidadAseguradoraNumeroIdentificacion = vDataReaderResults["EntidadAseguradoraNumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadAseguradoraNumeroIdentificacion"].ToString()) : vFUC.EntidadAseguradoraNumeroIdentificacion;
                            vFUC.EntidadAseguradoraGarantias = vDataReaderResults["EntidadAseguradoraGarantias"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadAseguradoraGarantias"].ToString()) : vFUC.EntidadAseguradoraGarantias;
                            vFUC.EntidadAseguradoraTipoGarantia = vDataReaderResults["EntidadAseguradoraTipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadAseguradoraTipoGarantia"].ToString()) : vFUC.EntidadAseguradoraTipoGarantia;
                            vFUC.IdRiesgos = vDataReaderResults["IdRiesgos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdRiesgos"].ToString()) : vFUC.IdRiesgos;
                            vFUC.GarantiasRiesgosAsegurados = vDataReaderResults["GarantiasRiesgosAsegurados"] != DBNull.Value ? Convert.ToString(vDataReaderResults["GarantiasRiesgosAsegurados"].ToString()) : vFUC.GarantiasRiesgosAsegurados;
                            vFUC.GarantiasPorcentajeAsegurado = vDataReaderResults["GarantiasPorcentajeAsegurado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["GarantiasPorcentajeAsegurado"].ToString()) : vFUC.GarantiasPorcentajeAsegurado;
                            vFUC.GarantiasValorTotalAsegurado = vDataReaderResults["GarantiasValorTotalAsegurado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["GarantiasValorTotalAsegurado"].ToString()) : vFUC.GarantiasValorTotalAsegurado;
                            vFUC.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdArea"].ToString()) : vFUC.IdArea;
                            vFUC.Supervisor = vDataReaderResults["Supervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Supervisor"].ToString()) : vFUC.Supervisor;
                            vFUC.SupervisorArea = vDataReaderResults["SupervisorArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorArea"].ToString()) : vFUC.SupervisorArea;
                            vFUC.SupervisorCargo = vDataReaderResults["SupervisorCargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorCargo"].ToString()) : vFUC.SupervisorCargo;
                            vFUC.SupervisorNombre = vDataReaderResults["SupervisorNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorNombre"].ToString()) : vFUC.SupervisorNombre;
                            vFUC.SupervisorIdentificacion = vDataReaderResults["SupervisorIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorIdentificacion"].ToString()) : vFUC.SupervisorIdentificacion;
                            vFUC.IdTipoSupervisor = vDataReaderResults["IdTipoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoSupervisor"].ToString()) : vFUC.IdTipoSupervisor;
                            vFUC.TipoSupervisor = vDataReaderResults["TipoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSupervisor"].ToString()) : vFUC.TipoSupervisor;
                            vFUC.IdentificacionGasto = vDataReaderResults["IdentificacionGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionGasto"].ToString()) : vFUC.IdentificacionGasto;
                            vFUC.NombreOrdenadorGasto = vDataReaderResults["NombreOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreOrdenadorGasto"].ToString()) : vFUC.NombreOrdenadorGasto;
                            vFUC.IdOrdenadorGasto = vDataReaderResults["IdOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdOrdenadorGasto"].ToString()) : vFUC.IdOrdenadorGasto;
                            vFUC.CargoOrdenadorGasto = vDataReaderResults["CargoOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoOrdenadorGasto"].ToString()) : vFUC.CargoOrdenadorGasto;
                            vFUC.Prorroga1Fecha = vDataReaderResults["Prorroga1Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga1Fecha"].ToString()) : vFUC.Prorroga1Fecha;
                            vFUC.Prorroga1Plazo = vDataReaderResults["Prorroga1Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga1Plazo"].ToString()) : vFUC.Prorroga1Plazo;
                            vFUC.Prorroga2Fecha = vDataReaderResults["Prorroga2Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga2Fecha"].ToString()) : vFUC.Prorroga2Fecha;
                            vFUC.Prorroga2Plazo = vDataReaderResults["Prorroga2Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga2Plazo"].ToString()) : vFUC.Prorroga2Plazo;
                            vFUC.Prorroga3Fecha = vDataReaderResults["Prorroga3Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga3Fecha"].ToString()) : vFUC.Prorroga3Fecha;
                            vFUC.Prorroga3Plazo = vDataReaderResults["Prorroga3Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga3Plazo"].ToString()) : vFUC.Prorroga3Plazo;
                            vFUC.Prorroga4Fecha = vDataReaderResults["Prorroga4Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga4Fecha"].ToString()) : vFUC.Prorroga4Fecha;
                            vFUC.Prorroga4Plazo = vDataReaderResults["Prorroga4Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga4Plazo"].ToString()) : vFUC.Prorroga4Plazo;
                            vFUC.Prorroga5Fecha = vDataReaderResults["Prorroga5Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga5Fecha"].ToString()) : vFUC.Prorroga5Fecha;
                            vFUC.Prorroga5Plazo = vDataReaderResults["Prorroga5Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga5Plazo"].ToString()) : vFUC.Prorroga5Plazo;
                            vFUC.ProrrogaTotalDias = vDataReaderResults["ProrrogaTotalDias"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProrrogaTotalDias"].ToString()) : vFUC.ProrrogaTotalDias;
                            vFUC.DisminucionDias = vDataReaderResults["DisminucionDias"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DisminucionDias"].ToString()) : vFUC.DisminucionDias;
                            vFUC.FechaTerminacionAnticipada = vDataReaderResults["FechaTerminacionAnticipada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaTerminacionAnticipada"].ToString()) : vFUC.FechaTerminacionAnticipada;
                            vFUC.PlazoTotalContrato = vDataReaderResults["PlazoTotalContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PlazoTotalContrato"].ToString()) : vFUC.PlazoTotalContrato;
                            vFUC.FechaTerminacionFinal = vDataReaderResults["FechaTerminacionFinal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaTerminacionFinal"].ToString()) : vFUC.FechaTerminacionFinal;
                            vFUC.Adicion1FechaSuscripcion = vDataReaderResults["Adicion1FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion1FechaSuscripcion"].ToString()) : vFUC.Adicion1FechaSuscripcion;
                            vFUC.Adicion1RP = vDataReaderResults["Adicion1RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion1RP"].ToString()) : vFUC.Adicion1RP;
                            vFUC.Adicion1Valor = vDataReaderResults["Adicion1Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion1Valor"].ToString()) : vFUC.Adicion1Valor;
                            vFUC.Adicion2FechaSuscripcion = vDataReaderResults["Adicion2FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion2FechaSuscripcion"].ToString()) : vFUC.Adicion2FechaSuscripcion;
                            vFUC.Adicion2RP = vDataReaderResults["Adicion2RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion2RP"].ToString()) : vFUC.Adicion2RP;
                            vFUC.Adicion2Valor = vDataReaderResults["Adicion2Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion2Valor"].ToString()) : vFUC.Adicion2Valor;
                            vFUC.Adicion3FechaSuscripcion = vDataReaderResults["Adicion3FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion3FechaSuscripcion"].ToString()) : vFUC.Adicion3FechaSuscripcion;
                            vFUC.Adicion3RP = vDataReaderResults["Adicion3RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion3RP"].ToString()) : vFUC.Adicion3RP;
                            vFUC.Adicion3Valor = vDataReaderResults["Adicion3Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion3Valor"].ToString()) : vFUC.Adicion3Valor;
                            vFUC.Adicion4FechaSuscripcion = vDataReaderResults["Adicion4FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion4FechaSuscripcion"].ToString()) : vFUC.Adicion4FechaSuscripcion;
                            vFUC.Adicion4RP = vDataReaderResults["Adicion4RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion4RP"].ToString()) : vFUC.Adicion4RP;
                            vFUC.Adicion4Valor = vDataReaderResults["Adicion4Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion4Valor"].ToString()) : vFUC.Adicion4Valor;
                            vFUC.Adicion5FechaSuscripcion = vDataReaderResults["Adicion5FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion5FechaSuscripcion"].ToString()) : vFUC.Adicion5FechaSuscripcion;
                            vFUC.Adicion5RP = vDataReaderResults["Adicion5RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion5RP"].ToString()) : vFUC.Adicion5RP;
                            vFUC.Adicion5Valor = vDataReaderResults["Adicion5Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion5Valor"].ToString()) : vFUC.Adicion5Valor;
                            vFUC.ValorTotalAdicion = vDataReaderResults["ValorTotalAdicion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorTotalAdicion"].ToString()) : vFUC.ValorTotalAdicion;
                            vFUC.DisminucionValor = vDataReaderResults["DisminucionValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DisminucionValor"].ToString()) : vFUC.DisminucionValor;
                            vFUC.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorFinalContrato"].ToString()) : vFUC.ValorFinalContrato;
                            vFUC.ValorFinalContratoCofinanciacion = vDataReaderResults["ValorFinalContratoCofinanciacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorFinalContratoCofinanciacion"].ToString()) : vFUC.ValorFinalContratoCofinanciacion;
                            vFUC.DesembolsoEfectuado = vDataReaderResults["DesembolsoEfectuado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DesembolsoEfectuado"].ToString()) : vFUC.DesembolsoEfectuado;
                            vFUC.PorcentajeAvanceFisico = vDataReaderResults["PorcentajeAvanceFisico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeAvanceFisico"].ToString()) : vFUC.PorcentajeAvanceFisico;
                            vFUC.PorcentajeAvanceFisicoReal = vDataReaderResults["PorcentajeAvanceFisicoReal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeAvanceFisicoReal"].ToString()) : vFUC.PorcentajeAvanceFisicoReal;
                            vFUC.PorcentajeAvancePresupuestado = vDataReaderResults["PorcentajeAvancePresupuestado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeAvancePresupuestado"].ToString()) : vFUC.PorcentajeAvancePresupuestado;
                            vFUC.PorcentajeAvancePresupuestadoReal = vDataReaderResults["PorcentajeAvancePresupuestadoReal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeAvancePresupuestadoReal"].ToString()) : vFUC.PorcentajeAvancePresupuestadoReal;
                            vFUC.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaLiquidacion"].ToString()) : vFUC.FechaLiquidacion;
                            vFUC.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vFUC.EstadoContrato;
                            vFUC.Cesionario1Identificacion = vDataReaderResults["Cesionario1Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario1Identificacion"].ToString()) : vFUC.Cesionario1Identificacion;
                            vFUC.Cesionario1TipoIdentificacion = vDataReaderResults["Cesionario1TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario1TipoIdentificacion"].ToString()) : vFUC.Cesionario1TipoIdentificacion;
                            vFUC.Cesionario1NombreRazonSocial = vDataReaderResults["Cesionario1NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario1NombreRazonSocial"].ToString()) : vFUC.Cesionario1NombreRazonSocial;
                            vFUC.Cesionario2Identificacion = vDataReaderResults["Cesionario2Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario2Identificacion"].ToString()) : vFUC.Cesionario2Identificacion;
                            vFUC.Cesionario2TipoIdentificacion = vDataReaderResults["Cesionario2TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario2TipoIdentificacion"].ToString()) : vFUC.Cesionario2TipoIdentificacion;
                            vFUC.Cesionario2NombreRazonSocial = vDataReaderResults["Cesionario2NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario2NombreRazonSocial"].ToString()) : vFUC.Cesionario2NombreRazonSocial;
                            vFUC.Cesionario3Identificacion = vDataReaderResults["Cesionario3Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario3Identificacion"].ToString()) : vFUC.Cesionario3Identificacion;
                            vFUC.Cesionario3TipoIdentificacion = vDataReaderResults["Cesionario3TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario3TipoIdentificacion"].ToString()) : vFUC.Cesionario3TipoIdentificacion;
                            vFUC.Cesionario3NombreRazonSocial = vDataReaderResults["Cesionario3NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario3NombreRazonSocial"].ToString()) : vFUC.Cesionario3NombreRazonSocial;
                            vFUC.Cesionario4Identificacion = vDataReaderResults["Cesionario4Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario4Identificacion"].ToString()) : vFUC.Cesionario4Identificacion;
                            vFUC.Cesionario4TipoIdentificacion = vDataReaderResults["Cesionario4TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario4TipoIdentificacion"].ToString()) : vFUC.Cesionario4TipoIdentificacion;
                            vFUC.Cesionario4NombreRazonSocial = vDataReaderResults["Cesionario4NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario4NombreRazonSocial"].ToString()) : vFUC.Cesionario4NombreRazonSocial;
                            vFUC.Cesionario5Identificacion = vDataReaderResults["Cesionario5Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario5Identificacion"].ToString()) : vFUC.Cesionario5Identificacion;
                            vFUC.Cesionario5TipoIdentificacion = vDataReaderResults["Cesionario5TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario5TipoIdentificacion"].ToString()) : vFUC.Cesionario5TipoIdentificacion;
                            vFUC.Cesionario5NombreRazonSocial = vDataReaderResults["Cesionario5NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario5NombreRazonSocial"].ToString()) : vFUC.Cesionario5NombreRazonSocial;
                            vFUC.FechaInicioSuspencion = vDataReaderResults["FechaInicioSuspencion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaInicioSuspencion"].ToString()) : vFUC.FechaInicioSuspencion;
                            vFUC.FechaTerminacionSuspension = vDataReaderResults["FechaTerminacionSuspension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaTerminacionSuspension"].ToString()) : vFUC.FechaTerminacionSuspension;
                            vFUC.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vFUC.Observaciones;
                            vFUC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFUC.UsuarioCrea;
                            vFUC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaCrea"].ToString()) : vFUC.FechaCrea;
                            vFUC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFUC.UsuarioModifica;
                            vFUC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaModifica"].ToString()) : vFUC.FechaModifica;
                        }
                        return vFUC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<FUC> ConsultarFUCs(String pRegional, int? pNumeroContrato, DateTime? pFechaSuscripcion, int? pIdAreaSolicita, String pContratistaTipoIdentificacion, String pContratistaNombre, String pRPRubro1, String pSupervisor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FUCs_Consultar"))
                {
                    if(pRegional != null)
                         vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.String, pRegional);
                    if(pNumeroContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.Int32, pNumeroContrato);
                    if(pFechaSuscripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pFechaSuscripcion);
                    if(pIdAreaSolicita != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdAreaSolicita", DbType.Int32, pIdAreaSolicita);
                    if(pContratistaTipoIdentificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@ContratistaTipoIdentificacion", DbType.String, pContratistaTipoIdentificacion);
                    if(pContratistaNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@ContratistaNombre", DbType.String, pContratistaNombre);
                    if(pRPRubro1 != null)
                         vDataBase.AddInParameter(vDbCommand, "@RPRubro1", DbType.String, pRPRubro1);
                    if(pSupervisor != null)
                         vDataBase.AddInParameter(vDbCommand, "@Supervisor", DbType.String, pSupervisor);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FUC> vListaFUC = new List<FUC>();
                        while (vDataReaderResults.Read())
                        {
                                FUC vFUC = new FUC();
                            vFUC.Id = vDataReaderResults["Id"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Id"].ToString()) : vFUC.Id;
                            vFUC.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vFUC.Regional;
                            vFUC.CodRegional = vDataReaderResults["CodRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegional"].ToString()) : vFUC.CodRegional;
                            vFUC.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vFUC.NumeroContrato;
                            vFUC.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaSuscripcion"].ToString()) : vFUC.FechaSuscripcion;
                            vFUC.Modalidad = vDataReaderResults["Modalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Modalidad"].ToString()) : vFUC.Modalidad;
                            vFUC.ProcesoSeleccion = vDataReaderResults["ProcesoSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProcesoSeleccion"].ToString()) : vFUC.ProcesoSeleccion;
                            vFUC.CategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vFUC.CategoriaContrato;
                            vFUC.ClaseContrato = vDataReaderResults["ClaseContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseContrato"].ToString()) : vFUC.ClaseContrato;
                            vFUC.IdAreaSolicita = vDataReaderResults["IdAreaSolicita"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdAreaSolicita"].ToString()) : vFUC.IdAreaSolicita;
                            vFUC.Area = vDataReaderResults["Area"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Area"].ToString()) : vFUC.Area;
                            vFUC.IdNaturaleza = vDataReaderResults["IdNaturaleza"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdNaturaleza"].ToString()) : vFUC.IdNaturaleza;
                            vFUC.Naturaleza = vDataReaderResults["Naturaleza"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Naturaleza"].ToString()) : vFUC.Naturaleza;
                            vFUC.ContratistaTipoIdentificacion = vDataReaderResults["ContratistaTipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaTipoIdentificacion"].ToString()) : vFUC.ContratistaTipoIdentificacion;
                            vFUC.ContratistaIdentificacion = vDataReaderResults["ContratistaIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaIdentificacion"].ToString()) : vFUC.ContratistaIdentificacion;
                            vFUC.ContratistaDigVerifi = vDataReaderResults["ContratistaDigVerifi"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaDigVerifi"].ToString()) : vFUC.ContratistaDigVerifi;
                            vFUC.ContratistaNombre = vDataReaderResults["ContratistaNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaNombre"].ToString()) : vFUC.ContratistaNombre;
                            vFUC.ContratistaProfesion = vDataReaderResults["ContratistaProfesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaProfesion"].ToString()) : vFUC.ContratistaProfesion;
                            vFUC.ContratistaDireccion = vDataReaderResults["ContratistaDireccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaDireccion"].ToString()) : vFUC.ContratistaDireccion;
                            vFUC.ContratistaTelefono = vDataReaderResults["ContratistaTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaTelefono"].ToString()) : vFUC.ContratistaTelefono;
                            vFUC.ContratistaLugarUbicacion = vDataReaderResults["ContratistaLugarUbicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaLugarUbicacion"].ToString()) : vFUC.ContratistaLugarUbicacion;
                            vFUC.IdTipoActividad = vDataReaderResults["IdTipoActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoActividad"].ToString()) : vFUC.IdTipoActividad;
                            vFUC.TipoActividad = vDataReaderResults["TipoActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoActividad"].ToString()) : vFUC.TipoActividad;
                            vFUC.IdTipoOrganizacion = vDataReaderResults["IdTipoOrganizacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoOrganizacion"].ToString()) : vFUC.IdTipoOrganizacion;
                            vFUC.TipoOrganizacion = vDataReaderResults["TipoOrganizacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoOrganizacion"].ToString()) : vFUC.TipoOrganizacion;
                            vFUC.ContratistaRepresentanteLegal = vDataReaderResults["ContratistaRepresentanteLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaRepresentanteLegal"].ToString()) : vFUC.ContratistaRepresentanteLegal;
                            vFUC.ContratistaRepresentanteIdentificacion = vDataReaderResults["ContratistaRepresentanteIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContratistaRepresentanteIdentificacion"].ToString()) : vFUC.ContratistaRepresentanteIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp1TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp1TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp1TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp1TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp1Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp1Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp1Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp1Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp1DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp1DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp1DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp1DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp1PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp1PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp1PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp1PorcentajeParticipacion;
                            vFUC.IntegrantesConsorcioUnionTemp2TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp2TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp2TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp2TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp2Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp2Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp2Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp2Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp2DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp2DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp2DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp2DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp2PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp2PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp2PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp2PorcentajeParticipacion;
                            vFUC.IntegrantesConsorcioUnionTemp3TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp3TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp3TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp3TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp3Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp3Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp3Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp3Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp3DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp3DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp3DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp3DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp3PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp3PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp3PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp3PorcentajeParticipacion;
                            vFUC.IntegrantesConsorcioUnionTemp4TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp4TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp4TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp4TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp4Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp4Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp4Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp4Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp4DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp4DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp4DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp4DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp4PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp4PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp4PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp4PorcentajeParticipacion;
                            vFUC.IntegrantesConsorcioUnionTemp5TipoIdentificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp5TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp5TipoIdentificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp5TipoIdentificacion;
                            vFUC.IntegrantesConsorcioUnionTemp5Nombre = vDataReaderResults["IntegrantesConsorcioUnionTemp5Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp5Nombre"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp5Nombre;
                            vFUC.IntegrantesConsorcioUnionTemp5DigitoVerificacion = vDataReaderResults["IntegrantesConsorcioUnionTemp5DigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp5DigitoVerificacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp5DigitoVerificacion;
                            vFUC.IntegrantesConsorcioUnionTemp5PorcentajeParticipacion = vDataReaderResults["IntegrantesConsorcioUnionTemp5PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IntegrantesConsorcioUnionTemp5PorcentajeParticipacion"].ToString()) : vFUC.IntegrantesConsorcioUnionTemp5PorcentajeParticipacion;
                            vFUC.AfectacionRecurso = vDataReaderResults["AfectacionRecurso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AfectacionRecurso"].ToString()) : vFUC.AfectacionRecurso;
                            vFUC.IdCodigoSecop = vDataReaderResults["IdCodigoSecop"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdCodigoSecop"].ToString()) : vFUC.IdCodigoSecop;
                            vFUC.CodigoSecop = vDataReaderResults["CodigoSecop"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSecop"].ToString()) : vFUC.CodigoSecop;
                            vFUC.ObjectoContrato = vDataReaderResults["ObjectoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjectoContrato"].ToString()) : vFUC.ObjectoContrato;
                            vFUC.LugarEjecucion = vDataReaderResults["LugarEjecucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LugarEjecucion"].ToString()) : vFUC.LugarEjecucion;
                            vFUC.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaInicio"].ToString()) : vFUC.FechaInicio;
                            vFUC.FechaTerminacion = vDataReaderResults["FechaTerminacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaTerminacion"].ToString()) : vFUC.FechaTerminacion;
                            vFUC.PlazoInicial = vDataReaderResults["PlazoInicial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PlazoInicial"].ToString()) : vFUC.PlazoInicial;
                            vFUC.CDPNumero = vDataReaderResults["CDPNumero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CDPNumero"].ToString()) : vFUC.CDPNumero;
                            vFUC.CDPFecha = vDataReaderResults["CDPFecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CDPFecha"].ToString()) : vFUC.CDPFecha;
                            vFUC.CDPValor = vDataReaderResults["CDPValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CDPValor"].ToString()) : vFUC.CDPValor;
                            vFUC.RPNumero = vDataReaderResults["RPNumero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPNumero"].ToString()) : vFUC.RPNumero;
                            vFUC.RPFecha = vDataReaderResults["RPFecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPFecha"].ToString()) : vFUC.RPFecha;
                            vFUC.RPValor = vDataReaderResults["RPValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPValor"].ToString()) : vFUC.RPValor;
                            vFUC.RPRubro1 = vDataReaderResults["RPRubro1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPRubro1"].ToString()) : vFUC.RPRubro1;
                            vFUC.RPRubro2 = vDataReaderResults["RPRubro2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPRubro2"].ToString()) : vFUC.RPRubro2;
                            vFUC.RPRubro3 = vDataReaderResults["RPRubro3"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RPRubro3"].ToString()) : vFUC.RPRubro3;
                            vFUC.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorInicialContrato"].ToString()) : vFUC.ValorInicialContrato;
                            vFUC.FormaPago = vDataReaderResults["FormaPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FormaPago"].ToString()) : vFUC.FormaPago;
                            vFUC.Anticipo = vDataReaderResults["Anticipo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Anticipo"].ToString()) : vFUC.Anticipo;
                            vFUC.ValorAnticipado = vDataReaderResults["ValorAnticipado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorAnticipado"].ToString()) : vFUC.ValorAnticipado;
                            vFUC.Confinanciador1IdPersona = vDataReaderResults["Confinanciador1IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1IdPersona"].ToString()) : vFUC.Confinanciador1IdPersona;
                            vFUC.Confinanciador1TipoPersona = vDataReaderResults["Confinanciador1TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1TipoPersona"].ToString()) : vFUC.Confinanciador1TipoPersona;
                            vFUC.Confinanciador1TipoIdentificacion = vDataReaderResults["Confinanciador1TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1TipoIdentificacion"].ToString()) : vFUC.Confinanciador1TipoIdentificacion;
                            vFUC.Confinanciador1NumeroIdentificacion = vDataReaderResults["Confinanciador1NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1NumeroIdentificacion"].ToString()) : vFUC.Confinanciador1NumeroIdentificacion;
                            vFUC.Confinanciador1NombreRazonSocial = vDataReaderResults["Confinanciador1NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1NombreRazonSocial"].ToString()) : vFUC.Confinanciador1NombreRazonSocial;
                            vFUC.Confinanciador1Valor = vDataReaderResults["Confinanciador1Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador1Valor"].ToString()) : vFUC.Confinanciador1Valor;
                            vFUC.Confinanciador2IdPersona = vDataReaderResults["Confinanciador2IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2IdPersona"].ToString()) : vFUC.Confinanciador2IdPersona;
                            vFUC.Confinanciador2TipoPersona = vDataReaderResults["Confinanciador2TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2TipoPersona"].ToString()) : vFUC.Confinanciador2TipoPersona;
                            vFUC.Confinanciador2TipoIdentificacion = vDataReaderResults["Confinanciador2TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2TipoIdentificacion"].ToString()) : vFUC.Confinanciador2TipoIdentificacion;
                            vFUC.Confinanciador2NumeroIdentificacion = vDataReaderResults["Confinanciador2NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2NumeroIdentificacion"].ToString()) : vFUC.Confinanciador2NumeroIdentificacion;
                            vFUC.Confinanciador2NombreRazonSocial = vDataReaderResults["Confinanciador2NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2NombreRazonSocial"].ToString()) : vFUC.Confinanciador2NombreRazonSocial;
                            vFUC.Confinanciador2Valor = vDataReaderResults["Confinanciador2Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador2Valor"].ToString()) : vFUC.Confinanciador2Valor;
                            vFUC.Confinanciador3IdPersona = vDataReaderResults["Confinanciador3IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3IdPersona"].ToString()) : vFUC.Confinanciador3IdPersona;
                            vFUC.Confinanciador3TipoPersona = vDataReaderResults["Confinanciador3TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3TipoPersona"].ToString()) : vFUC.Confinanciador3TipoPersona;
                            vFUC.Confinanciador3TipoIdentificacion = vDataReaderResults["Confinanciador3TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3TipoIdentificacion"].ToString()) : vFUC.Confinanciador3TipoIdentificacion;
                            vFUC.Confinanciador3NumeroIdentificacion = vDataReaderResults["Confinanciador3NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3NumeroIdentificacion"].ToString()) : vFUC.Confinanciador3NumeroIdentificacion;
                            vFUC.Confinanciador3NombreRazonSocial = vDataReaderResults["Confinanciador3NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3NombreRazonSocial"].ToString()) : vFUC.Confinanciador3NombreRazonSocial;
                            vFUC.Confinanciador3Valor = vDataReaderResults["Confinanciador3Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador3Valor"].ToString()) : vFUC.Confinanciador3Valor;
                            vFUC.Confinanciador4IdPersona = vDataReaderResults["Confinanciador4IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4IdPersona"].ToString()) : vFUC.Confinanciador4IdPersona;
                            vFUC.Confinanciador4TipoPersona = vDataReaderResults["Confinanciador4TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4TipoPersona"].ToString()) : vFUC.Confinanciador4TipoPersona;
                            vFUC.Confinanciador4TipoIdentificacion = vDataReaderResults["Confinanciador4TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4TipoIdentificacion"].ToString()) : vFUC.Confinanciador4TipoIdentificacion;
                            vFUC.Confinanciador4NumeroIdentificacion = vDataReaderResults["Confinanciador4NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4NumeroIdentificacion"].ToString()) : vFUC.Confinanciador4NumeroIdentificacion;
                            vFUC.Confinanciador4NombreRazonSocial = vDataReaderResults["Confinanciador4NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4NombreRazonSocial"].ToString()) : vFUC.Confinanciador4NombreRazonSocial;
                            vFUC.Confinanciador4Valor = vDataReaderResults["Confinanciador4Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador4Valor"].ToString()) : vFUC.Confinanciador4Valor;
                            vFUC.Confinanciador5IdPersona = vDataReaderResults["Confinanciador5IdPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5IdPersona"].ToString()) : vFUC.Confinanciador5IdPersona;
                            vFUC.Confinanciador5TipoPersona = vDataReaderResults["Confinanciador5TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5TipoPersona"].ToString()) : vFUC.Confinanciador5TipoPersona;
                            vFUC.Confinanciador5TipoIdentificacion = vDataReaderResults["Confinanciador5TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5TipoIdentificacion"].ToString()) : vFUC.Confinanciador5TipoIdentificacion;
                            vFUC.Confinanciador5NumeroIdentificacion = vDataReaderResults["Confinanciador5NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5NumeroIdentificacion"].ToString()) : vFUC.Confinanciador5NumeroIdentificacion;
                            vFUC.Confinanciador5NombreRazonSocial = vDataReaderResults["Confinanciador5NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5NombreRazonSocial"].ToString()) : vFUC.Confinanciador5NombreRazonSocial;
                            vFUC.Confinanciador5Valor = vDataReaderResults["Confinanciador5Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Confinanciador5Valor"].ToString()) : vFUC.Confinanciador5Valor;
                            vFUC.ValorTotalCofinanciacion = vDataReaderResults["ValorTotalCofinanciacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorTotalCofinanciacion"].ToString()) : vFUC.ValorTotalCofinanciacion;
                            vFUC.VigenciaFuturaNumero = vDataReaderResults["VigenciaFuturaNumero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaFuturaNumero"].ToString()) : vFUC.VigenciaFuturaNumero;
                            vFUC.VigenciaFuturaValor = vDataReaderResults["VigenciaFuturaValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaFuturaValor"].ToString()) : vFUC.VigenciaFuturaValor;
                            vFUC.ValorTotalInicialContrato = vDataReaderResults["ValorTotalInicialContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorTotalInicialContrato"].ToString()) : vFUC.ValorTotalInicialContrato;
                            vFUC.ValorInicialContratoIncluidaCofinanciacion = vDataReaderResults["ValorInicialContratoIncluidaCofinanciacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorInicialContratoIncluidaCofinanciacion"].ToString()) : vFUC.ValorInicialContratoIncluidaCofinanciacion;
                            vFUC.FechaAprobacionGarantias = vDataReaderResults["FechaAprobacionGarantias"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaAprobacionGarantias"].ToString()) : vFUC.FechaAprobacionGarantias;
                            vFUC.TipoGarantia = vDataReaderResults["TipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoGarantia"].ToString()) : vFUC.TipoGarantia;
                            vFUC.EntidadAseguradoraNumeroIdentificacion = vDataReaderResults["EntidadAseguradoraNumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadAseguradoraNumeroIdentificacion"].ToString()) : vFUC.EntidadAseguradoraNumeroIdentificacion;
                            vFUC.EntidadAseguradoraGarantias = vDataReaderResults["EntidadAseguradoraGarantias"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadAseguradoraGarantias"].ToString()) : vFUC.EntidadAseguradoraGarantias;
                            vFUC.EntidadAseguradoraTipoGarantia = vDataReaderResults["EntidadAseguradoraTipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadAseguradoraTipoGarantia"].ToString()) : vFUC.EntidadAseguradoraTipoGarantia;
                            vFUC.IdRiesgos = vDataReaderResults["IdRiesgos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdRiesgos"].ToString()) : vFUC.IdRiesgos;
                            vFUC.GarantiasRiesgosAsegurados = vDataReaderResults["GarantiasRiesgosAsegurados"] != DBNull.Value ? Convert.ToString(vDataReaderResults["GarantiasRiesgosAsegurados"].ToString()) : vFUC.GarantiasRiesgosAsegurados;
                            vFUC.GarantiasPorcentajeAsegurado = vDataReaderResults["GarantiasPorcentajeAsegurado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["GarantiasPorcentajeAsegurado"].ToString()) : vFUC.GarantiasPorcentajeAsegurado;
                            vFUC.GarantiasValorTotalAsegurado = vDataReaderResults["GarantiasValorTotalAsegurado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["GarantiasValorTotalAsegurado"].ToString()) : vFUC.GarantiasValorTotalAsegurado;
                            vFUC.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdArea"].ToString()) : vFUC.IdArea;
                            vFUC.Supervisor = vDataReaderResults["Supervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Supervisor"].ToString()) : vFUC.Supervisor;
                            vFUC.SupervisorArea = vDataReaderResults["SupervisorArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorArea"].ToString()) : vFUC.SupervisorArea;
                            vFUC.SupervisorCargo = vDataReaderResults["SupervisorCargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorCargo"].ToString()) : vFUC.SupervisorCargo;
                            vFUC.SupervisorNombre = vDataReaderResults["SupervisorNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorNombre"].ToString()) : vFUC.SupervisorNombre;
                            vFUC.SupervisorIdentificacion = vDataReaderResults["SupervisorIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorIdentificacion"].ToString()) : vFUC.SupervisorIdentificacion;
                            vFUC.IdTipoSupervisor = vDataReaderResults["IdTipoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoSupervisor"].ToString()) : vFUC.IdTipoSupervisor;
                            vFUC.TipoSupervisor = vDataReaderResults["TipoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSupervisor"].ToString()) : vFUC.TipoSupervisor;
                            vFUC.IdentificacionGasto = vDataReaderResults["IdentificacionGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionGasto"].ToString()) : vFUC.IdentificacionGasto;
                            vFUC.NombreOrdenadorGasto = vDataReaderResults["NombreOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreOrdenadorGasto"].ToString()) : vFUC.NombreOrdenadorGasto;
                            vFUC.IdOrdenadorGasto = vDataReaderResults["IdOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdOrdenadorGasto"].ToString()) : vFUC.IdOrdenadorGasto;
                            vFUC.CargoOrdenadorGasto = vDataReaderResults["CargoOrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoOrdenadorGasto"].ToString()) : vFUC.CargoOrdenadorGasto;
                            vFUC.Prorroga1Fecha = vDataReaderResults["Prorroga1Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga1Fecha"].ToString()) : vFUC.Prorroga1Fecha;
                            vFUC.Prorroga1Plazo = vDataReaderResults["Prorroga1Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga1Plazo"].ToString()) : vFUC.Prorroga1Plazo;
                            vFUC.Prorroga2Fecha = vDataReaderResults["Prorroga2Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga2Fecha"].ToString()) : vFUC.Prorroga2Fecha;
                            vFUC.Prorroga2Plazo = vDataReaderResults["Prorroga2Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga2Plazo"].ToString()) : vFUC.Prorroga2Plazo;
                            vFUC.Prorroga3Fecha = vDataReaderResults["Prorroga3Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga3Fecha"].ToString()) : vFUC.Prorroga3Fecha;
                            vFUC.Prorroga3Plazo = vDataReaderResults["Prorroga3Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga3Plazo"].ToString()) : vFUC.Prorroga3Plazo;
                            vFUC.Prorroga4Fecha = vDataReaderResults["Prorroga4Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga4Fecha"].ToString()) : vFUC.Prorroga4Fecha;
                            vFUC.Prorroga4Plazo = vDataReaderResults["Prorroga4Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga4Plazo"].ToString()) : vFUC.Prorroga4Plazo;
                            vFUC.Prorroga5Fecha = vDataReaderResults["Prorroga5Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga5Fecha"].ToString()) : vFUC.Prorroga5Fecha;
                            vFUC.Prorroga5Plazo = vDataReaderResults["Prorroga5Plazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Prorroga5Plazo"].ToString()) : vFUC.Prorroga5Plazo;
                            vFUC.ProrrogaTotalDias = vDataReaderResults["ProrrogaTotalDias"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProrrogaTotalDias"].ToString()) : vFUC.ProrrogaTotalDias;
                            vFUC.DisminucionDias = vDataReaderResults["DisminucionDias"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DisminucionDias"].ToString()) : vFUC.DisminucionDias;
                            vFUC.FechaTerminacionAnticipada = vDataReaderResults["FechaTerminacionAnticipada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaTerminacionAnticipada"].ToString()) : vFUC.FechaTerminacionAnticipada;
                            vFUC.PlazoTotalContrato = vDataReaderResults["PlazoTotalContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PlazoTotalContrato"].ToString()) : vFUC.PlazoTotalContrato;
                            vFUC.FechaTerminacionFinal = vDataReaderResults["FechaTerminacionFinal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaTerminacionFinal"].ToString()) : vFUC.FechaTerminacionFinal;
                            vFUC.Adicion1FechaSuscripcion = vDataReaderResults["Adicion1FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion1FechaSuscripcion"].ToString()) : vFUC.Adicion1FechaSuscripcion;
                            vFUC.Adicion1RP = vDataReaderResults["Adicion1RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion1RP"].ToString()) : vFUC.Adicion1RP;
                            vFUC.Adicion1Valor = vDataReaderResults["Adicion1Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion1Valor"].ToString()) : vFUC.Adicion1Valor;
                            vFUC.Adicion2FechaSuscripcion = vDataReaderResults["Adicion2FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion2FechaSuscripcion"].ToString()) : vFUC.Adicion2FechaSuscripcion;
                            vFUC.Adicion2RP = vDataReaderResults["Adicion2RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion2RP"].ToString()) : vFUC.Adicion2RP;
                            vFUC.Adicion2Valor = vDataReaderResults["Adicion2Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion2Valor"].ToString()) : vFUC.Adicion2Valor;
                            vFUC.Adicion3FechaSuscripcion = vDataReaderResults["Adicion3FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion3FechaSuscripcion"].ToString()) : vFUC.Adicion3FechaSuscripcion;
                            vFUC.Adicion3RP = vDataReaderResults["Adicion3RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion3RP"].ToString()) : vFUC.Adicion3RP;
                            vFUC.Adicion3Valor = vDataReaderResults["Adicion3Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion3Valor"].ToString()) : vFUC.Adicion3Valor;
                            vFUC.Adicion4FechaSuscripcion = vDataReaderResults["Adicion4FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion4FechaSuscripcion"].ToString()) : vFUC.Adicion4FechaSuscripcion;
                            vFUC.Adicion4RP = vDataReaderResults["Adicion4RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion4RP"].ToString()) : vFUC.Adicion4RP;
                            vFUC.Adicion4Valor = vDataReaderResults["Adicion4Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion4Valor"].ToString()) : vFUC.Adicion4Valor;
                            vFUC.Adicion5FechaSuscripcion = vDataReaderResults["Adicion5FechaSuscripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion5FechaSuscripcion"].ToString()) : vFUC.Adicion5FechaSuscripcion;
                            vFUC.Adicion5RP = vDataReaderResults["Adicion5RP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion5RP"].ToString()) : vFUC.Adicion5RP;
                            vFUC.Adicion5Valor = vDataReaderResults["Adicion5Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Adicion5Valor"].ToString()) : vFUC.Adicion5Valor;
                            vFUC.ValorTotalAdicion = vDataReaderResults["ValorTotalAdicion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorTotalAdicion"].ToString()) : vFUC.ValorTotalAdicion;
                            vFUC.DisminucionValor = vDataReaderResults["DisminucionValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DisminucionValor"].ToString()) : vFUC.DisminucionValor;
                            vFUC.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorFinalContrato"].ToString()) : vFUC.ValorFinalContrato;
                            vFUC.ValorFinalContratoCofinanciacion = vDataReaderResults["ValorFinalContratoCofinanciacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorFinalContratoCofinanciacion"].ToString()) : vFUC.ValorFinalContratoCofinanciacion;
                            vFUC.DesembolsoEfectuado = vDataReaderResults["DesembolsoEfectuado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DesembolsoEfectuado"].ToString()) : vFUC.DesembolsoEfectuado;
                            vFUC.PorcentajeAvanceFisico = vDataReaderResults["PorcentajeAvanceFisico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeAvanceFisico"].ToString()) : vFUC.PorcentajeAvanceFisico;
                            vFUC.PorcentajeAvanceFisicoReal = vDataReaderResults["PorcentajeAvanceFisicoReal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeAvanceFisicoReal"].ToString()) : vFUC.PorcentajeAvanceFisicoReal;
                            vFUC.PorcentajeAvancePresupuestado = vDataReaderResults["PorcentajeAvancePresupuestado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeAvancePresupuestado"].ToString()) : vFUC.PorcentajeAvancePresupuestado;
                            vFUC.PorcentajeAvancePresupuestadoReal = vDataReaderResults["PorcentajeAvancePresupuestadoReal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeAvancePresupuestadoReal"].ToString()) : vFUC.PorcentajeAvancePresupuestadoReal;
                            vFUC.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaLiquidacion"].ToString()) : vFUC.FechaLiquidacion;
                            vFUC.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vFUC.EstadoContrato;
                            vFUC.Cesionario1Identificacion = vDataReaderResults["Cesionario1Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario1Identificacion"].ToString()) : vFUC.Cesionario1Identificacion;
                            vFUC.Cesionario1TipoIdentificacion = vDataReaderResults["Cesionario1TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario1TipoIdentificacion"].ToString()) : vFUC.Cesionario1TipoIdentificacion;
                            vFUC.Cesionario1NombreRazonSocial = vDataReaderResults["Cesionario1NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario1NombreRazonSocial"].ToString()) : vFUC.Cesionario1NombreRazonSocial;
                            vFUC.Cesionario2Identificacion = vDataReaderResults["Cesionario2Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario2Identificacion"].ToString()) : vFUC.Cesionario2Identificacion;
                            vFUC.Cesionario2TipoIdentificacion = vDataReaderResults["Cesionario2TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario2TipoIdentificacion"].ToString()) : vFUC.Cesionario2TipoIdentificacion;
                            vFUC.Cesionario2NombreRazonSocial = vDataReaderResults["Cesionario2NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario2NombreRazonSocial"].ToString()) : vFUC.Cesionario2NombreRazonSocial;
                            vFUC.Cesionario3Identificacion = vDataReaderResults["Cesionario3Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario3Identificacion"].ToString()) : vFUC.Cesionario3Identificacion;
                            vFUC.Cesionario3TipoIdentificacion = vDataReaderResults["Cesionario3TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario3TipoIdentificacion"].ToString()) : vFUC.Cesionario3TipoIdentificacion;
                            vFUC.Cesionario3NombreRazonSocial = vDataReaderResults["Cesionario3NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario3NombreRazonSocial"].ToString()) : vFUC.Cesionario3NombreRazonSocial;
                            vFUC.Cesionario4Identificacion = vDataReaderResults["Cesionario4Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario4Identificacion"].ToString()) : vFUC.Cesionario4Identificacion;
                            vFUC.Cesionario4TipoIdentificacion = vDataReaderResults["Cesionario4TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario4TipoIdentificacion"].ToString()) : vFUC.Cesionario4TipoIdentificacion;
                            vFUC.Cesionario4NombreRazonSocial = vDataReaderResults["Cesionario4NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario4NombreRazonSocial"].ToString()) : vFUC.Cesionario4NombreRazonSocial;
                            vFUC.Cesionario5Identificacion = vDataReaderResults["Cesionario5Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario5Identificacion"].ToString()) : vFUC.Cesionario5Identificacion;
                            vFUC.Cesionario5TipoIdentificacion = vDataReaderResults["Cesionario5TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario5TipoIdentificacion"].ToString()) : vFUC.Cesionario5TipoIdentificacion;
                            vFUC.Cesionario5NombreRazonSocial = vDataReaderResults["Cesionario5NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cesionario5NombreRazonSocial"].ToString()) : vFUC.Cesionario5NombreRazonSocial;
                            vFUC.FechaInicioSuspencion = vDataReaderResults["FechaInicioSuspencion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaInicioSuspencion"].ToString()) : vFUC.FechaInicioSuspencion;
                            vFUC.FechaTerminacionSuspension = vDataReaderResults["FechaTerminacionSuspension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaTerminacionSuspension"].ToString()) : vFUC.FechaTerminacionSuspension;
                            vFUC.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vFUC.Observaciones;
                            vFUC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFUC.UsuarioCrea;
                            vFUC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaCrea"].ToString()) : vFUC.FechaCrea;
                            vFUC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFUC.UsuarioModifica;
                            vFUC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaModifica"].ToString()) : vFUC.FechaModifica;
                                vListaFUC.Add(vFUC);
                        }
                        return vListaFUC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
