using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Contratos
    /// </summary>
    public class ContratoDAL : GeneralDAL
    {
        public ContratoDAL()
        {
        }
        
        /// <summary>
        /// M�todo de inserci�n para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int InsertarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pContrato.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pContrato.NumeroProceso);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pContrato.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicacion", DbType.DateTime, pContrato.FechaAdjudicacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pContrato.IdModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pContrato.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pContrato.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoModalidad", DbType.Int32, pContrato.IdCodigoModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadAcademica", DbType.Int32, pContrato.IdModalidadAcademica);
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoProfesion", DbType.Int32, pContrato.IdCodigoProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@IdNombreProfesion", DbType.Int32, pContrato.IdNombreProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalContrato", DbType.Int32, pContrato.IdRegionalContrato);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereActa", DbType.Boolean, pContrato.RequiereActa);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaAportes", DbType.Boolean, pContrato.ManejaAportes);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaRecursos", DbType.Boolean, pContrato.ManejaRecursos);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaVigenciasFuturas", DbType.Boolean, pContrato.ManejaVigenciasFuturas);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegimenContratacion", DbType.Int32, pContrato.IdRegimenContratacion);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.Int32, pContrato.CodigoRegional);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSolicitante", DbType.String, pContrato.NombreSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@DependenciaSolicitante", DbType.String, pContrato.DependenciaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@CargoSolicitante", DbType.String, pContrato.CargoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoContrato", DbType.String, pContrato.ObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@AlcanceObjetoContrato", DbType.String, pContrato.AlcanceObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInicialContrato", DbType.Decimal, pContrato.ValorInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalAdiciones", DbType.Decimal, pContrato.ValorTotalAdiciones);
                    vDataBase.AddInParameter(vDbCommand, "@ValorFinalContrato", DbType.Decimal, pContrato.ValorFinalContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAportesICBF", DbType.Decimal, pContrato.ValorAportesICBF);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAportesOperador", DbType.Decimal, pContrato.ValorAportesOperador);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalReduccion", DbType.Decimal, pContrato.ValorTotalReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionAdicionSuperior50porc", DbType.String, pContrato.JustificacionAdicionSuperior50porc);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pContrato.FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.DateTime, pContrato.FechaInicioEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalizacionInicial", DbType.DateTime, pContrato.FechaFinalizacionIniciaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoInicial", DbType.Int32, pContrato.PlazoInicial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicialTerminacion", DbType.DateTime, pContrato.FechaInicialTerminacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalTerminacion", DbType.DateTime, pContrato.FechaFinalTerminacionContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaProyectadaLiquidacion", DbType.DateTime, pContrato.FechaProyectadaLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAnulacion", DbType.DateTime, pContrato.FechaAnulacion);
                    vDataBase.AddInParameter(vDbCommand, "@Prorrogas", DbType.Int32, pContrato.Prorrogas);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoTotal", DbType.Int32, pContrato.PlazoTotal);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFirmaActaInicio", DbType.DateTime, pContrato.FechaFirmaActaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalInicial", DbType.Int32, pContrato.VigenciaFiscalInicial);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalFinal", DbType.Int32, pContrato.VigenciaFiscalFinal);
                    vDataBase.AddInParameter(vDbCommand, "@IdUnidadEjecucion", DbType.Int32, pContrato.IdUnidadEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdLugarEjecucion", DbType.Int32, pContrato.IdLugarEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@DatosAdicionales", DbType.String, pContrato.DatosAdicionales);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, pContrato.IdEstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoContratista", DbType.String, pContrato.IdTipoDocumentoContratista);
                    vDataBase.AddInParameter(vDbCommand, "@IdentificacionContratista", DbType.String, pContrato.IdentificacionContratista);
                    vDataBase.AddInParameter(vDbCommand, "@NombreContratista", DbType.String, pContrato.NombreContratista);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaPago", DbType.Int32, pContrato.IdFormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidad", DbType.Int32, pContrato.IdTipoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContrato.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseContrato", DbType.String, pContrato.ClaseContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.String, pContrato.Consecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@AfectaPlanCompras", DbType.Boolean, pContrato.AfectaPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitante", DbType.Int32, pContrato.IdSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdProducto", DbType.Int32, pContrato.IdProducto);
                    vDataBase.AddInParameter(vDbCommand, "@FechaLiquidacion", DbType.DateTime, pContrato.FechaLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumentoVigenciaFutura", DbType.Int32, pContrato.NumeroDocumentoVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereGarantia", DbType.Boolean, pContrato.RequiereGarantias); 
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pContrato.IdContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContrato").ToString());
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int ModificarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pContrato.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pContrato.NumeroProceso);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pContrato.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicacion", DbType.DateTime, pContrato.FechaAdjudicacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pContrato.IdModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pContrato.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pContrato.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoModalidad", DbType.Int32, pContrato.IdCodigoModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadAcademica", DbType.Int32, pContrato.IdModalidadAcademica);
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoProfesion", DbType.Int32, pContrato.IdCodigoProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@IdNombreProfesion", DbType.Int32, pContrato.IdNombreProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalContrato", DbType.Int32, pContrato.IdRegionalContrato);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereActa", DbType.Boolean, pContrato.RequiereActa);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaAportes", DbType.Boolean, pContrato.ManejaAportes);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaRecursos", DbType.Boolean, pContrato.ManejaRecursos);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaVigenciasFuturas", DbType.Boolean, pContrato.ManejaVigenciasFuturas);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegimenContratacion", DbType.Int32, pContrato.IdRegimenContratacion);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.Int32, pContrato.CodigoRegional);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSolicitante", DbType.String, pContrato.NombreSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@DependenciaSolicitante", DbType.String, pContrato.DependenciaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@CargoSolicitante", DbType.String, pContrato.CargoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoContrato", DbType.String, pContrato.ObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@AlcanceObjetoContrato", DbType.String, pContrato.AlcanceObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInicialContrato", DbType.Decimal, pContrato.ValorInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalAdiciones", DbType.Decimal, pContrato.ValorTotalAdiciones);
                    vDataBase.AddInParameter(vDbCommand, "@ValorFinalContrato", DbType.Decimal, pContrato.ValorFinalContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAportesICBF", DbType.Decimal, pContrato.ValorAportesICBF);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAportesOperador", DbType.Decimal, pContrato.ValorAportesOperador);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalReduccion", DbType.Decimal, pContrato.ValorTotalReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionAdicionSuperior50porc", DbType.String, pContrato.JustificacionAdicionSuperior50porc);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pContrato.FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.DateTime, pContrato.FechaInicioEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalizacionInicial", DbType.DateTime, pContrato.FechaFinalizacionIniciaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoInicial", DbType.Int32, pContrato.PlazoInicial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicialTerminacion", DbType.DateTime, pContrato.FechaInicialTerminacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalTerminacion", DbType.DateTime, pContrato.FechaFinalTerminacionContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaProyectadaLiquidacion", DbType.DateTime, pContrato.FechaProyectadaLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAnulacion", DbType.DateTime, pContrato.FechaAnulacion);
                    vDataBase.AddInParameter(vDbCommand, "@Prorrogas", DbType.Int32, pContrato.Prorrogas);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoTotal", DbType.Int32, pContrato.PlazoTotal);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFirmaActaInicio", DbType.DateTime, pContrato.FechaFirmaActaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalInicial", DbType.Int32, pContrato.VigenciaFiscalInicial);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalFinal", DbType.Int32, pContrato.VigenciaFiscalFinal);
                    vDataBase.AddInParameter(vDbCommand, "@IdUnidadEjecucion", DbType.Int32, pContrato.IdUnidadEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdLugarEjecucion", DbType.Int32, pContrato.IdLugarEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@DatosAdicionales", DbType.String, pContrato.DatosAdicionales);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, pContrato.IdEstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoContratista", DbType.String, pContrato.IdTipoDocumentoContratista);
                    vDataBase.AddInParameter(vDbCommand, "@IdentificacionContratista", DbType.String, pContrato.IdentificacionContratista);
                    vDataBase.AddInParameter(vDbCommand, "@NombreContratista", DbType.String, pContrato.NombreContratista);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaPago", DbType.Int32, pContrato.IdFormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidad", DbType.Int32, pContrato.IdTipoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pContrato.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseContrato", DbType.String, pContrato.ClaseContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.String, pContrato.Consecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@AfectaPlanCompras", DbType.Boolean, pContrato.AfectaPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitante", DbType.Int32, pContrato.IdSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdProducto", DbType.Int32, pContrato.IdProducto);
                    vDataBase.AddInParameter(vDbCommand, "@FechaLiquidacion", DbType.DateTime, pContrato.FechaLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumentoVigenciaFutura", DbType.Int32, pContrato.NumeroDocumentoVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereGarantia", DbType.Boolean, pContrato.RequiereGarantias); 
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="fechaAnulacion"></param>
        /// <returns></returns>
        public int AnularContrato(int idContrato, DateTime fechaAnulacion, string usuarioModifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Anular"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAnulacion", DbType.DateTime, fechaAnulacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, usuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de eliminaci�n para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int EliminarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato.IdContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad Contrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public Contrato.Entity.Contrato ConsultarContratoModificacion(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                        while (vDataReaderResults.Read())
                        {
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContrato.NombreRegional;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoDelContrato"].ToString()) : vContrato.ObjetoContrato;
                            vContrato.AlcanceObjetoContrato = vDataReaderResults["AlcanceObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AlcanceObjetoDelContrato"].ToString()) : vContrato.AlcanceObjetoContrato;
                            vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;
                            vContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vContrato.FechaFinalTerminacion;
                            vContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContrato.IdEstadoContrato;
                            vContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContrato.UsuarioCrea;
                            vContrato.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContrato.FechaFinalizacionIniciaContrato;
                            
                        }
                        return vContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad Contrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public Contrato.Entity.Contrato ConsultarContrato(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                        while (vDataReaderResults.Read())
                        {                                                        
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContrato.NombreRegional;
                            vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;
                            vContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vContrato.FechaFinalTerminacionContrato;
                            vContrato.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoDelContrato"].ToString()) : vContrato.ObjetoContrato;
                            vContrato.AlcanceObjetoContrato = vDataReaderResults["AlcanceObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AlcanceObjetoDelContrato"].ToString()) : vContrato.AlcanceObjetoContrato;
                            vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            vContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContrato.IdEstadoContrato;
                            vContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContrato.UsuarioCrea;
                            vContrato.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContrato.FechaFinalizacionIniciaContrato;
                            vContrato.VinculoSECOP = vDataReaderResults["VinculoSECOP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VinculoSECOP"].ToString()) : vContrato.VinculoSECOP;
                            vContrato.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : vContrato.NombreTipoContrato;
                            //vContrato.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vContrato.FechaRegistro;
                            //vContrato.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContrato.NumeroProceso;
                            //vContrato.FechaAdjudicacion = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vContrato.FechaAdjudicacion;
                            //vContrato.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vContrato.IdModalidad;
                            //vContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContrato.IdCategoriaContrato;
                            //vContrato.IdCodigoModalidad = vDataReaderResults["IdCodigoModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCodigoModalidad"].ToString()) : vContrato.IdCodigoModalidad;
                            //vContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContrato.IdTipoContrato;
                            //vContrato.RequiereActa = vDataReaderResults["RequiereActa"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereActa"].ToString()) : vContrato.RequiereActa;
                            //vContrato.ManejaAportes = vDataReaderResults["ManejaAportes"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaAportes"].ToString()) : vContrato.ManejaAportes;
                            vContrato.ManejaRecursos = vDataReaderResults["ManejaRecurso"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaRecurso"].ToString()) : vContrato.ManejaRecursos;
                            //vContrato.ManejaVigenciasFuturas = vDataReaderResults["ManejaVigenciasFuturas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaVigenciasFuturas"].ToString()) : vContrato.ManejaVigenciasFuturas;
                            //vContrato.IdRegimenContratacion = vDataReaderResults["IdRegimenContratacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegimenContratacion"].ToString()) : vContrato.IdRegimenContratacion;
                            //vContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoRegional"].ToString()) : vContrato.CodigoRegional;
                            //vContrato.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSolicitante"].ToString()) : vContrato.NombreSolicitante;
                            //vContrato.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vContrato.DependenciaSolicitante;
                            //vContrato.CargoSolicitante = vDataReaderResults["CargoSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSolicitante"].ToString()) : vContrato.CargoSolicitante;
                            //vContrato.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : vContrato.ObjetoContrato;
                            //vContrato.AlcanceObjetoContrato = vDataReaderResults["AlcanceObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AlcanceObjetoContrato"].ToString()) : vContrato.AlcanceObjetoContrato;
                            //vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            //vContrato.ValorTotalAdiciones = vDataReaderResults["ValorTotalAdiciones"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalAdiciones"].ToString()) : vContrato.ValorTotalAdiciones;
                            //vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            //vContrato.ValorAportesICBF = vDataReaderResults["ValorAportesICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesICBF"].ToString()) : vContrato.ValorAportesICBF;
                            //vContrato.ValorAportesOperador = vDataReaderResults["ValorAportesOperador"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesOperador"].ToString()) : vContrato.ValorAportesOperador;
                            //vContrato.ValorTotalReduccion = vDataReaderResults["ValorTotalReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalReduccion"].ToString()) : vContrato.ValorTotalReduccion;
                            //vContrato.JustificacionAdicionSuperior50porc = vDataReaderResults["JustificacionAdicionSuperior50porc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionAdicionSuperior50porc"].ToString()) : vContrato.JustificacionAdicionSuperior50porc;
                            vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"].ToString()) : vContrato.FechaSuscripcion;
                            //vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;
                            //vContrato.FechaFinalizacionInicial = vDataReaderResults["FechaFinalizacionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionInicial"].ToString()) : vContrato.FechaFinalizacionInicial;
                            //vContrato.PlazoInicial = vDataReaderResults["PlazoInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PlazoInicial"].ToString()) : vContrato.PlazoInicial;
                            //vContrato.FechaInicialTerminacion = vDataReaderResults["FechaInicialTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicialTerminacion"].ToString()) : vContrato.FechaInicialTerminacion;
                            //vContrato.FechaFinalTerminacion = vDataReaderResults["FechaFinalTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacion"].ToString()) : vContrato.FechaFinalTerminacion;
                            //vContrato.FechaProyectadaLiquidacion = vDataReaderResults["FechaProyectadaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaProyectadaLiquidacion"].ToString()) : vContrato.FechaProyectadaLiquidacion;
                            //vContrato.FechaAnulacion = vDataReaderResults["FechaAnulacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAnulacion"].ToString()) : vContrato.FechaAnulacion;
                            //vContrato.Prorrogas = vDataReaderResults["Prorrogas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Prorrogas"].ToString()) : vContrato.Prorrogas;
                            //vContrato.PlazoTotal = vDataReaderResults["PlazoTotal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PlazoTotal"].ToString()) : vContrato.PlazoTotal;
                            //vContrato.FechaFirmaActaInicio = vDataReaderResults["FechaFirmaActaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFirmaActaInicio"].ToString()) : vContrato.FechaFirmaActaInicio;
                            //vContrato.VigenciaFiscalInicial = vDataReaderResults["VigenciaFiscalInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalInicial"].ToString()) : vContrato.VigenciaFiscalInicial;
                            //vContrato.VigenciaFiscalFinal = vDataReaderResults["VigenciaFiscalFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalFinal"].ToString()) : vContrato.VigenciaFiscalFinal;
                            //vContrato.IdUnidadEjecucion = vDataReaderResults["IdUnidadEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUnidadEjecucion"].ToString()) : vContrato.IdUnidadEjecucion;
                            //vContrato.IdLugarEjecucion = vDataReaderResults["IdLugarEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucion"].ToString()) : vContrato.IdLugarEjecucion;
                            //vContrato.DatosAdicionales = vDataReaderResults["DatosAdicionales"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DatosAdicionales"].ToString()) : vContrato.DatosAdicionales;                            
                            //vContrato.IdTipoDocumentoContratista = vDataReaderResults["IdTipoDocumentoContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoDocumentoContratista"].ToString()) : vContrato.IdTipoDocumentoContratista;
                            //vContrato.IdentificacionContratista = vDataReaderResults["IdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionContratista"].ToString()) : vContrato.IdentificacionContratista;
                            //vContrato.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vContrato.NombreContratista;
                            //vContrato.IdFormaPago = vDataReaderResults["IdFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaPago"].ToString()) : vContrato.IdFormaPago;
                            //vContrato.IdTipoEntidad = vDataReaderResults["IdTipoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidad"].ToString()) : vContrato.IdTipoEntidad;                            
                            //vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            //vContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContrato.UsuarioModifica;
                            //vContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContrato.FechaModifica;
                            vContrato.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContrato.IdRegionalContrato;
                            //vContrato.ClaseContrato = vDataReaderResults["ClaseContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseContrato"].ToString()) : vContrato.ClaseContrato;
                            //vContrato.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consecutivo"].ToString()) : vContrato.Consecutivo;
                            //vContrato.AfectaPlanCompras = vDataReaderResults["AfectaPlanCompras"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AfectaPlanCompras"].ToString()) : vContrato.AfectaPlanCompras;
                            //vContrato.IdSolicitante = vDataReaderResults["IdSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitante"].ToString()) : vContrato.IdSolicitante;
                            //vContrato.IdProducto = vDataReaderResults["IdProducto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProducto"].ToString()) : vContrato.IdProducto;
                            //vContrato.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLiquidacion"].ToString()) : vContrato.FechaLiquidacion;
                            //vContrato.NumeroDocumentoVigenciaFutura = vDataReaderResults["NumeroDocumentoVigenciaFutura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroDocumentoVigenciaFutura"].ToString()) : vContrato.NumeroDocumentoVigenciaFutura;
                            //vContrato.RequiereGarantias = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"].ToString()) : vContrato.RequiereGarantias;                            
                        }
                        return vContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.Contrato ConsultarContratoAdiciones(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                        while (vDataReaderResults.Read())
                        {
                            vContrato.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContrato.IdRegionalContrato;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContrato.NombreRegional;
                            vContrato.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoDelContrato"].ToString()) : vContrato.ObjetoContrato;
                            vContrato.AlcanceObjetoContrato = vDataReaderResults["AlcanceObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AlcanceObjetoDelContrato"].ToString()) : vContrato.AlcanceObjetoContrato;
                            vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;
                            vContrato.FechaFinalTerminacion = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vContrato.FechaFinalTerminacion;
                            vContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContrato.IdEstadoContrato;
                            vContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContrato.UsuarioCrea;
                            vContrato.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContrato.FechaFinalizacionIniciaContrato;
                            vContrato.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContrato.IdVigenciaInicial;
                            vContrato.IdVigenciaFinal = vDataReaderResults["IdVigenciaFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaFinal"].ToString()) : vContrato.IdVigenciaFinal;
                        }
                        return vContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contrato
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratos(DateTime? pFechaRegistro, String pNumeroProceso, Decimal? pNumeroContrato, int? pIdModalidad, int? pIdCategoriaContrato, int? pIdTipoContrato, string pClaseContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_Consultar"))
                {
                    if (pFechaRegistro != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pFechaRegistro);
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.Decimal, pNumeroContrato);
                    if (pIdModalidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pIdModalidad);
                    if (pIdCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pIdCategoriaContrato);
                    if (pIdTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pIdTipoContrato);
                    if (pClaseContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@ClaseContrato", DbType.String, pClaseContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContrato = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vContrato.FechaRegistro;
                            vContrato.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContrato.NumeroProceso;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.FechaAdjudicacion = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vContrato.FechaAdjudicacion;
                            vContrato.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vContrato.IdModalidad;
                            vContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContrato.IdCategoriaContrato;
                            vContrato.IdCodigoModalidad = vDataReaderResults["IdCodigoModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCodigoModalidad"].ToString()) : vContrato.IdCodigoModalidad;
                            vContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContrato.IdTipoContrato;
                            vContrato.RequiereActa = vDataReaderResults["RequiereActa"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereActa"].ToString()) : vContrato.RequiereActa;
                            vContrato.ManejaAportes = vDataReaderResults["ManejaAportes"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaAportes"].ToString()) : vContrato.ManejaAportes;
                            vContrato.ManejaRecursos = vDataReaderResults["ManejaRecursos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaRecursos"].ToString()) : vContrato.ManejaRecursos;
                            vContrato.ManejaVigenciasFuturas = vDataReaderResults["ManejaVigenciasFuturas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaVigenciasFuturas"].ToString()) : vContrato.ManejaVigenciasFuturas;
                            vContrato.IdRegimenContratacion = vDataReaderResults["IdRegimenContratacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegimenContratacion"].ToString()) : vContrato.IdRegimenContratacion;
                            vContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoRegional"].ToString()) : vContrato.CodigoRegional;
                            vContrato.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSolicitante"].ToString()) : vContrato.NombreSolicitante;
                            vContrato.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vContrato.DependenciaSolicitante;
                            vContrato.CargoSolicitante = vDataReaderResults["CargoSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSolicitante"].ToString()) : vContrato.CargoSolicitante;
                            vContrato.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : vContrato.ObjetoContrato;
                            vContrato.AlcanceObjetoContrato = vDataReaderResults["AlcanceObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AlcanceObjetoContrato"].ToString()) : vContrato.AlcanceObjetoContrato;
                            vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            vContrato.ValorTotalAdiciones = vDataReaderResults["ValorTotalAdiciones"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalAdiciones"].ToString()) : vContrato.ValorTotalAdiciones;
                            vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            vContrato.ValorAportesICBF = vDataReaderResults["ValorAportesICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesICBF"].ToString()) : vContrato.ValorAportesICBF;
                            vContrato.ValorAportesOperador = vDataReaderResults["ValorAportesOperador"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesOperador"].ToString()) : vContrato.ValorAportesOperador;
                            vContrato.ValorTotalReduccion = vDataReaderResults["ValorTotalReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalReduccion"].ToString()) : vContrato.ValorTotalReduccion;
                            vContrato.JustificacionAdicionSuperior50porc = vDataReaderResults["JustificacionAdicionSuperior50porc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionAdicionSuperior50porc"].ToString()) : vContrato.JustificacionAdicionSuperior50porc;
                            //vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContrato.FechaSuscripcion;
                            vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;
                            //vContrato.FechaFinalizacionInicial = vDataReaderResults["FechaFinalizacionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionInicial"].ToString()) : vContrato.FechaFinalizacionInicial;
                            vContrato.PlazoInicial = vDataReaderResults["PlazoInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PlazoInicial"].ToString()) : vContrato.PlazoInicial;
                            vContrato.FechaInicialTerminacion = vDataReaderResults["FechaInicialTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicialTerminacion"].ToString()) : vContrato.FechaInicialTerminacion;
                            //vContrato.FechaFinalTerminacion = vDataReaderResults["FechaFinalTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacion"].ToString()) : vContrato.FechaFinalTerminacion;
                            vContrato.FechaProyectadaLiquidacion = vDataReaderResults["FechaProyectadaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaProyectadaLiquidacion"].ToString()) : vContrato.FechaProyectadaLiquidacion;
                            vContrato.FechaAnulacion = vDataReaderResults["FechaAnulacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAnulacion"].ToString()) : vContrato.FechaAnulacion;
                            vContrato.Prorrogas = vDataReaderResults["Prorrogas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Prorrogas"].ToString()) : vContrato.Prorrogas;
                            vContrato.PlazoTotal = vDataReaderResults["PlazoTotal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PlazoTotal"].ToString()) : vContrato.PlazoTotal;
                            vContrato.FechaFirmaActaInicio = vDataReaderResults["FechaFirmaActaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFirmaActaInicio"].ToString()) : vContrato.FechaFirmaActaInicio;
                            vContrato.VigenciaFiscalInicial = vDataReaderResults["VigenciaFiscalInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalInicial"].ToString()) : vContrato.VigenciaFiscalInicial;
                            vContrato.VigenciaFiscalFinal = vDataReaderResults["VigenciaFiscalFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalFinal"].ToString()) : vContrato.VigenciaFiscalFinal;
                            vContrato.IdUnidadEjecucion = vDataReaderResults["IdUnidadEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUnidadEjecucion"].ToString()) : vContrato.IdUnidadEjecucion;
                            vContrato.IdLugarEjecucion = vDataReaderResults["IdLugarEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucion"].ToString()) : vContrato.IdLugarEjecucion;
                            vContrato.DatosAdicionales = vDataReaderResults["DatosAdicionales"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DatosAdicionales"].ToString()) : vContrato.DatosAdicionales;
                            vContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContrato.IdEstadoContrato;
                            vContrato.IdTipoDocumentoContratista = vDataReaderResults["IdTipoDocumentoContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoDocumentoContratista"].ToString()) : vContrato.IdTipoDocumentoContratista;
                            vContrato.IdentificacionContratista = vDataReaderResults["IdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionContratista"].ToString()) : vContrato.IdentificacionContratista;
                            vContrato.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vContrato.NombreContratista;
                            vContrato.IdFormaPago = vDataReaderResults["IdFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaPago"].ToString()) : vContrato.IdFormaPago;
                            vContrato.IdTipoEntidad = vDataReaderResults["IdTipoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidad"].ToString()) : vContrato.IdTipoEntidad;
                            vContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContrato.UsuarioCrea;
                            vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            vContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContrato.UsuarioModifica;
                            vContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContrato.FechaModifica;
                            vContrato.ClaseContrato = vDataReaderResults["ClaseContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseContrato"].ToString()) : vContrato.ClaseContrato;
                            vContrato.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consecutivo"].ToString()) : vContrato.Consecutivo;
                            vContrato.AfectaPlanCompras = vDataReaderResults["AfectaPlanCompras"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AfectaPlanCompras"].ToString()) : vContrato.AfectaPlanCompras;
                            vContrato.IdSolicitante = vDataReaderResults["IdSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitante"].ToString()) : vContrato.IdSolicitante;
                            vContrato.IdProducto = vDataReaderResults["IdProducto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProducto"].ToString()) : vContrato.IdProducto;
                            vContrato.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLiquidacion"].ToString()) : vContrato.FechaLiquidacion;
                            vContrato.NumeroDocumentoVigenciaFutura = vDataReaderResults["NumeroDocumentoVigenciaFutura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroDocumentoVigenciaFutura"].ToString()) : vContrato.NumeroDocumentoVigenciaFutura;
                            vContrato.RequiereGarantias = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"].ToString()) : vContrato.RequiereGarantias;
                            vListaContrato.Add(vContrato);
                        }
                        return vListaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contratos
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pFechaRegistroSistemaHasta"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContrato"></param>
        /// <param name="pIdDependenciaSolicitante"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratoss(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, Boolean? pManejaRecurso, string pNombreContratistas, int? pIDTipoIdentificacion, int? pNumeroIdentificacion, string pUsuarioCreacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Contratoss_Consultar"))
                {
                    if (pFechaRegistroSistemaDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaDesde", DbType.DateTime, pFechaRegistroSistemaDesde);
                    if (pFechaRegistroSistemaHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaHasta", DbType.DateTime, pFechaRegistroSistemaHasta);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pVigenciaFiscalinicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                    if (pIDRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    if (pIDModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDModalidadSeleccion", DbType.Int32, pIDModalidadSeleccion);
                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);
                    if (pIDEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEstadoContraro", DbType.Int32, pIDEstadoContrato);
                    if (pIdDependenciaSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDependenciaSolicitante", DbType.Int32, pIdDependenciaSolicitante);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pNumeroIdentificacion);                    
                    if (pIDTipoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Tipoidentificacion", DbType.Int32, pIDTipoIdentificacion);
                    //if (pNumeroIdentificacion.HasValue)
                    //    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pNumeroIdentificacion.Value);
                    //if (pIDTipoIdentificacion.HasValue)
                    //    vDataBase.AddInParameter(vDbCommand, "@Tipoidentificacion", DbType.Int32, pIDTipoIdentificacion.Value);
                    if(pNombreContratistas !=null)
                        vDataBase.AddInParameter(vDbCommand, "@Proveedor", DbType.String, pNombreContratistas);
                    if (pManejaRecurso != null)
                        vDataBase.AddInParameter(vDbCommand, "@ManejaRecurso", DbType.Boolean, pManejaRecurso);
                    if(pUsuarioCreacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pUsuarioCreacion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContratos = new Contrato.Entity.Contrato();
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            vContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratos.NumeroContrato;
                            vContratos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContratos.IdNumeroProceso;
                            vContratos.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContratos.IdVigenciaInicial;
                            vContratos.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContratos.IdRegionalContrato;
                            vContratos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContratos.IdModalidadSeleccion;
                            vContratos.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContratos.IdCategoriaContrato;
                            vContratos.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContratos.IdTipoContrato;
                            vContratos.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContratos.IdEstadoContrato;
                            vContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratos.UsuarioCrea;
                            vContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratos.FechaCrea;
                            vContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratos.UsuarioModifica;
                            vContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratos.FechaModifica;
                            vContratos.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratos.NombreRegional;
                            vContratos.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContratos.NombreModalidadSeleccion;
                            vContratos.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContratos.NombreCategoriaContrato;
                            vContratos.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContratos.NombreTipoContrato;
                            vContratos.NombreEstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratos.NombreEstadoContrato;
                            vContratos.AcnoVigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vContratos.AcnoVigencia;
                            vContratos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;
                            vContratos.FechaAdjudicacionProceso = vDataReaderResults["FechaAdjudicacionDelProceso"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacionDelProceso"].ToString()) : vContratos.FechaAdjudicacionProceso;
                            vContratos.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContratos.ValorInicialContrato;
                            vContratos.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContratos.ValorFinalContrato;
                            vContratos.IdDependenciaEmpSol = vDataReaderResults["IdDependenciaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaSolicitante"].ToString()) : vContratos.IdDependenciaEmpSol;
                            vContratos.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vContratos.DependenciaSolicitante;
                            vContratos.NombreContratista = vDataReaderResults["Razonsocila"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razonsocila"].ToString()) : vContratos.NombreContratista;
                            vContratos.IdentificacionContratista = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NUMEROIDENTIFICACION"].ToString()) : vContratos.IdentificacionContratista;
                            

                            vListaContratos.Add(vContratos);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contratos
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pFechaRegistroSistemaHasta"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContrato"></param>
        /// <param name="pIdDependenciaSolicitante"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratosSimple(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, Boolean? pManejaRecurso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Contratoss_ConsultarSimple"))
                {
                    if (pFechaRegistroSistemaDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaDesde", DbType.DateTime, pFechaRegistroSistemaDesde);
                    if (pFechaRegistroSistemaHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaHasta", DbType.DateTime, pFechaRegistroSistemaHasta);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pVigenciaFiscalinicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                    if (pIDRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    if (pIDModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDModalidadSeleccion", DbType.Int32, pIDModalidadSeleccion);
                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);
                    if (pIDEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEstadoContraro", DbType.Int32, pIDEstadoContrato);
                    if (pIdDependenciaSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDependenciaSolicitante", DbType.Int32, pIdDependenciaSolicitante);
                    if (pManejaRecurso != null)
                        vDataBase.AddInParameter(vDbCommand, "@ManejaRecurso", DbType.Boolean, pManejaRecurso);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContratos = new Contrato.Entity.Contrato();
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            vContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratos.NumeroContrato;
                            vContratos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContratos.IdNumeroProceso;
                            vContratos.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContratos.IdVigenciaInicial;
                            vContratos.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContratos.IdRegionalContrato;
                            vContratos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContratos.IdModalidadSeleccion;
                            vContratos.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContratos.IdCategoriaContrato;
                            vContratos.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContratos.IdTipoContrato;
                            vContratos.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContratos.IdEstadoContrato;
                            vContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratos.UsuarioCrea;
                            vContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratos.FechaCrea;
                            vContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratos.UsuarioModifica;
                            vContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratos.FechaModifica;
                            vContratos.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratos.NombreRegional;
                            vContratos.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContratos.NombreModalidadSeleccion;
                            vContratos.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContratos.NombreCategoriaContrato;
                            vContratos.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContratos.NombreTipoContrato;
                            vContratos.NombreEstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratos.NombreEstadoContrato;
                            vContratos.AcnoVigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vContratos.AcnoVigencia;
                            vContratos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;
                            vContratos.FechaAdjudicacionProceso = vDataReaderResults["FechaAdjudicacionDelProceso"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacionDelProceso"].ToString()) : vContratos.FechaAdjudicacionProceso;
                            vContratos.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContratos.ValorInicialContrato;
                            vContratos.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContratos.ValorFinalContrato;
                            vContratos.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoDelContrato"].ToString()) : vContratos.ObjetoContrato;
                            vContratos.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratos.FechaInicioEjecucion;
                            vContratos.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vContratos.FechaFinalTerminacionContrato;
                            vListaContratos.Add(vContratos);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosHistorico(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, Boolean? pManejaRecurso, string pNombreContratistas, int? pIDTipoIdentificacion, int? pNumeroIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosHistorico_Consultar"))
                {
                    if (pFechaRegistroSistemaDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaDesde", DbType.DateTime, pFechaRegistroSistemaDesde);
                    if (pFechaRegistroSistemaHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaHasta", DbType.DateTime, pFechaRegistroSistemaHasta);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pVigenciaFiscalinicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                    if (pIDRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    if (pIDModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDModalidadSeleccion", DbType.Int32, pIDModalidadSeleccion);
                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);
                    if (pIDEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEstadoContraro", DbType.Int32, pIDEstadoContrato);
                    if (pIdDependenciaSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDependenciaSolicitante", DbType.Int32, pIdDependenciaSolicitante);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pNumeroIdentificacion);
                    if (pIDTipoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Tipoidentificacion", DbType.String, pIDTipoIdentificacion.ToString());
                    //if (pNumeroIdentificacion.HasValue)
                    //    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pNumeroIdentificacion.Value);
                    //if (pIDTipoIdentificacion.HasValue)
                    //    vDataBase.AddInParameter(vDbCommand, "@Tipoidentificacion", DbType.Int32, pIDTipoIdentificacion.Value);
                    if (pNombreContratistas != null)
                        vDataBase.AddInParameter(vDbCommand, "@Proveedor", DbType.String, pNombreContratistas);
                    if (pManejaRecurso != null)
                        vDataBase.AddInParameter(vDbCommand, "@ManejaRecurso", DbType.Boolean, pManejaRecurso);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContratos = new Contrato.Entity.Contrato();
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            vContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratos.NumeroContrato;
                            vContratos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContratos.IdNumeroProceso;
                            vContratos.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContratos.IdVigenciaInicial;
                            vContratos.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContratos.IdRegionalContrato;
                            vContratos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContratos.IdModalidadSeleccion;
                            vContratos.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContratos.IdCategoriaContrato;
                            vContratos.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContratos.IdTipoContrato;
                            vContratos.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContratos.IdEstadoContrato;
                            vContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratos.UsuarioCrea;
                            vContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratos.FechaCrea;
                            vContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratos.UsuarioModifica;
                            vContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratos.FechaModifica;
                            vContratos.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratos.NombreRegional;
                            vContratos.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContratos.NombreModalidadSeleccion;
                            vContratos.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContratos.NombreCategoriaContrato;
                            vContratos.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContratos.NombreTipoContrato;
                            vContratos.NombreEstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratos.NombreEstadoContrato;
                            vContratos.AcnoVigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vContratos.AcnoVigencia;
                            //vContratos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;
                            //vContratos.FechaAdjudicacionProceso = vDataReaderResults["FechaAdjudicacionDelProceso"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacionDelProceso"].ToString()) : vContratos.FechaAdjudicacionProceso;
                            //vContratos.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContratos.ValorInicialContrato;
                           // vContratos.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContratos.ValorFinalContrato;
                           // vContratos.IdDependenciaEmpSol = vDataReaderResults["IdDependenciaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaSolicitante"].ToString()) : vContratos.IdDependenciaEmpSol;
                            //vContratos.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vContratos.DependenciaSolicitante;
                            vContratos.NombreContratista = vDataReaderResults["NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRazonSocial"].ToString()) : vContratos.NombreContratista;
                            vContratos.IdentificacionContratista = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vContratos.IdentificacionContratista;


                            vListaContratos.Add(vContratos);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contratos
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pFechaRegistroSistemaHasta"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContraro"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratosRegistrados(DateTime? pFechaRegistroSistema, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContraro, string pUsuarioCrea, DateTime? pFechaInicioEjecucion, DateTime? pFechaFinalizaInicioContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosAnulados_Consultar"))
                {
                    if (pFechaRegistroSistema != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistema", DbType.DateTime, pFechaRegistroSistema);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pVigenciaFiscalinicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                    if (pIDRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    if (pIDModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDModalidadSeleccion", DbType.Int32, pIDModalidadSeleccion);
                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);
                    if (pIDEstadoContraro != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEstadoContraro", DbType.Int32, pIDEstadoContraro);
                    if (pUsuarioCrea != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUsuarioCrea);
                    if (pFechaInicioEjecucion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.DateTime, pFechaInicioEjecucion);
                    if (pFechaFinalizaInicioContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaFinalizaInicioContrato", DbType.DateTime, pFechaFinalizaInicioContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContratos = new Contrato.Entity.Contrato();
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            vContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratos.NumeroContrato;
                            vContratos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContratos.IdNumeroProceso;
                            vContratos.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContratos.IdVigenciaInicial;
                            vContratos.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContratos.IdRegionalContrato;
                            vContratos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContratos.IdModalidadSeleccion;
                            vContratos.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContratos.IdCategoriaContrato;
                            vContratos.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContratos.IdTipoContrato;
                            vContratos.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContratos.IdEstadoContrato;
                            vContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratos.UsuarioCrea;
                            vContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratos.FechaCrea;
                            vContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratos.UsuarioModifica;
                            vContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratos.FechaModifica;
                            vContratos.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratos.NombreRegional;
                            vContratos.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContratos.NombreModalidadSeleccion;
                            vContratos.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContratos.NombreCategoriaContrato;
                            vContratos.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContratos.NombreTipoContrato;
                            vContratos.NombreEstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratos.NombreEstadoContrato;
                            vContratos.AcnoVigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vContratos.AcnoVigencia;
                            vContratos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;
                            vContratos.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratos.FechaInicioEjecucion;
                            vContratos.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContratos.FechaFinalizacionIniciaContrato;
                            
                            //vContratos = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;

                            vListaContratos.Add(vContratos);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contratos
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pFechaRegistroSistemaHasta"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContraro"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratosAnulacion(DateTime? pFechaRegistroSistema, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContraro, string pUsuarioCrea, DateTime? pFechaInicioEjecucion, DateTime? pFechaFinalizaInicioContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosAnulados_Consultar"))
                {
                    if (pFechaRegistroSistema != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistema", DbType.DateTime, pFechaRegistroSistema);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pVigenciaFiscalinicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                    if (pIDRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    if (pIDModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDModalidadSeleccion", DbType.Int32, pIDModalidadSeleccion);
                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);
                    if (pIDEstadoContraro != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEstadoContraro", DbType.Int32, pIDEstadoContraro);
                    if (pUsuarioCrea != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUsuarioCrea);
                    if (pFechaInicioEjecucion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.DateTime, pFechaInicioEjecucion);
                    if (pFechaFinalizaInicioContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaFinalizaInicioContrato", DbType.DateTime, pFechaFinalizaInicioContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContratos = new Contrato.Entity.Contrato();
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            vContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratos.NumeroContrato;
                            vContratos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContratos.IdNumeroProceso;
                            vContratos.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContratos.IdVigenciaInicial;
                            vContratos.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContratos.IdRegionalContrato;
                            vContratos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContratos.IdModalidadSeleccion;
                            vContratos.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContratos.IdCategoriaContrato;
                            vContratos.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContratos.IdTipoContrato;
                            vContratos.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContratos.IdEstadoContrato;
                            vContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratos.UsuarioCrea;
                            vContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratos.FechaCrea;
                            vContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratos.UsuarioModifica;
                            vContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratos.FechaModifica;
                            vContratos.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratos.NombreRegional;
                            vContratos.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContratos.NombreModalidadSeleccion;
                            vContratos.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContratos.NombreCategoriaContrato;
                            vContratos.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContratos.NombreTipoContrato;
                            vContratos.NombreEstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratos.NombreEstadoContrato;
                            vContratos.AcnoVigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vContratos.AcnoVigencia;
                            vContratos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;
                            vContratos.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratos.FechaInicioEjecucion;
                            vContratos.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContratos.FechaFinalizacionIniciaContrato;

                            //vContratos = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;

                            vListaContratos.Add(vContratos);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
      
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contrato en una Ventana Emergente
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public DataTable ConsultarContratosLupa(DateTime? pFechaRegistro, String pNumeroProceso, string pNumeroContrato, int? pIdModalidad, int? pIdCategoriaContrato, int? pIdTipoContrato, string pClaseContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_Consultar_lupa"))
                {
                    if (pFechaRegistro != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pFechaRegistro);
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if (pIdModalidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pIdModalidad);
                    if (pIdCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pIdCategoriaContrato);
                    if (pIdTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pIdTipoContrato);
                    if (pClaseContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@ClaseContrato", DbType.String, pClaseContrato);
                    return vDataBase.ExecuteDataSet(vDbCommand).Tables[0];
                    
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contrato
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosLupa(int? pIdContrato, String pNumeroContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_Consultar_lupaPlanCompras"))
                {
                    
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContrato = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            ////vContrato.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vContrato.FechaRegistro;
                            //vContrato.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContrato.NumeroProceso;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            //vContrato.FechaAdjudicacion = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vContrato.FechaAdjudicacion;
                            //vContrato.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vContrato.IdModalidad;
                            //vContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContrato.IdCategoriaContrato;
                            //vContrato.IdCodigoModalidad = vDataReaderResults["IdCodigoModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCodigoModalidad"].ToString()) : vContrato.IdCodigoModalidad;
                            //vContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContrato.IdTipoContrato;
                            //vContrato.RequiereActa = vDataReaderResults["RequiereActa"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereActa"].ToString()) : vContrato.RequiereActa;
                            //vContrato.ManejaAportes = vDataReaderResults["ManejaAportes"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaAportes"].ToString()) : vContrato.ManejaAportes;
                            //vContrato.ManejaRecursos = vDataReaderResults["ManejaRecursos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaRecursos"].ToString()) : vContrato.ManejaRecursos;
                            //vContrato.ManejaVigenciasFuturas = vDataReaderResults["ManejaVigenciasFuturas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaVigenciasFuturas"].ToString()) : vContrato.ManejaVigenciasFuturas;
                            //vContrato.IdRegimenContratacion = vDataReaderResults["IdRegimenContratacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegimenContratacion"].ToString()) : vContrato.IdRegimenContratacion;
                            //vContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoRegional"].ToString()) : vContrato.CodigoRegional;
                            //vContrato.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSolicitante"].ToString()) : vContrato.NombreSolicitante;
                            //vContrato.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vContrato.DependenciaSolicitante;
                            //vContrato.CargoSolicitante = vDataReaderResults["CargoSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSolicitante"].ToString()) : vContrato.CargoSolicitante;
                            //vContrato.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : vContrato.ObjetoContrato;
                            //vContrato.AlcanceObjetoContrato = vDataReaderResults["AlcanceObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AlcanceObjetoContrato"].ToString()) : vContrato.AlcanceObjetoContrato;
                            //vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            //vContrato.ValorTotalAdiciones = vDataReaderResults["ValorTotalAdiciones"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalAdiciones"].ToString()) : vContrato.ValorTotalAdiciones;
                            //vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            //vContrato.ValorAportesICBF = vDataReaderResults["ValorAportesICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesICBF"].ToString()) : vContrato.ValorAportesICBF;
                            //vContrato.ValorAportesOperador = vDataReaderResults["ValorAportesOperador"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesOperador"].ToString()) : vContrato.ValorAportesOperador;
                            //vContrato.ValorTotalReduccion = vDataReaderResults["ValorTotalReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalReduccion"].ToString()) : vContrato.ValorTotalReduccion;
                            //vContrato.JustificacionAdicionSuperior50porc = vDataReaderResults["JustificacionAdicionSuperior50porc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionAdicionSuperior50porc"].ToString()) : vContrato.JustificacionAdicionSuperior50porc;
                            //vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContrato.FechaSuscripcion;
                            //vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;
                            //vContrato.FechaFinalizacionInicial = vDataReaderResults["FechaFinalizacionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionInicial"].ToString()) : vContrato.FechaFinalizacionInicial;
                            //vContrato.PlazoInicial = vDataReaderResults["PlazoInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PlazoInicial"].ToString()) : vContrato.PlazoInicial;
                            //vContrato.FechaInicialTerminacion = vDataReaderResults["FechaInicialTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicialTerminacion"].ToString()) : vContrato.FechaInicialTerminacion;
                            //vContrato.FechaFinalTerminacion = vDataReaderResults["FechaFinalTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacion"].ToString()) : vContrato.FechaFinalTerminacion;
                            //vContrato.FechaProyectadaLiquidacion = vDataReaderResults["FechaProyectadaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaProyectadaLiquidacion"].ToString()) : vContrato.FechaProyectadaLiquidacion;
                            //vContrato.FechaAnulacion = vDataReaderResults["FechaAnulacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAnulacion"].ToString()) : vContrato.FechaAnulacion;
                            //vContrato.Prorrogas = vDataReaderResults["Prorrogas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Prorrogas"].ToString()) : vContrato.Prorrogas;
                            //vContrato.PlazoTotal = vDataReaderResults["PlazoTotal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PlazoTotal"].ToString()) : vContrato.PlazoTotal;
                            //vContrato.FechaFirmaActaInicio = vDataReaderResults["FechaFirmaActaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFirmaActaInicio"].ToString()) : vContrato.FechaFirmaActaInicio;
                            //vContrato.VigenciaFiscalInicial = vDataReaderResults["VigenciaFiscalInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalInicial"].ToString()) : vContrato.VigenciaFiscalInicial;
                            //vContrato.VigenciaFiscalFinal = vDataReaderResults["VigenciaFiscalFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalFinal"].ToString()) : vContrato.VigenciaFiscalFinal;
                            //vContrato.IdUnidadEjecucion = vDataReaderResults["IdUnidadEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUnidadEjecucion"].ToString()) : vContrato.IdUnidadEjecucion;
                            //vContrato.IdLugarEjecucion = vDataReaderResults["IdLugarEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucion"].ToString()) : vContrato.IdLugarEjecucion;
                            //vContrato.DatosAdicionales = vDataReaderResults["DatosAdicionales"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DatosAdicionales"].ToString()) : vContrato.DatosAdicionales;
                            //vContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContrato.IdEstadoContrato;
                            //vContrato.IdTipoDocumentoContratista = vDataReaderResults["IdTipoDocumentoContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoDocumentoContratista"].ToString()) : vContrato.IdTipoDocumentoContratista;
                            //vContrato.IdentificacionContratista = vDataReaderResults["IdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionContratista"].ToString()) : vContrato.IdentificacionContratista;
                            //vContrato.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vContrato.NombreContratista;
                            //vContrato.IdFormaPago = vDataReaderResults["IdFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaPago"].ToString()) : vContrato.IdFormaPago;
                            //vContrato.IdTipoEntidad = vDataReaderResults["IdTipoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidad"].ToString()) : vContrato.IdTipoEntidad;
                            //vContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContrato.UsuarioCrea;
                            //vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            //vContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContrato.UsuarioModifica;
                            //vContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContrato.FechaModifica;
                            //vContrato.ClaseContrato = vDataReaderResults["ClaseContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseContrato"].ToString()) : vContrato.ClaseContrato;
                            //vContrato.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consecutivo"].ToString()) : vContrato.Consecutivo;
                            //vContrato.AfectaPlanCompras = vDataReaderResults["AfectaPlanCompras"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AfectaPlanCompras"].ToString()) : vContrato.AfectaPlanCompras;
                            //vContrato.IdSolicitante = vDataReaderResults["IdSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitante"].ToString()) : vContrato.IdSolicitante;
                            //vContrato.IdProducto = vDataReaderResults["IdProducto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProducto"].ToString()) : vContrato.IdProducto;
                            //vContrato.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLiquidacion"].ToString()) : vContrato.FechaLiquidacion;
                            //vContrato.NumeroDocumentoVigenciaFutura = vDataReaderResults["NumeroDocumentoVigenciaFutura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroDocumentoVigenciaFutura"].ToString()) : vContrato.NumeroDocumentoVigenciaFutura;
                            //vContrato.RequiereGarantias = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"].ToString()) : vContrato.RequiereGarantias;
                            vListaContrato.Add(vContrato);
                        }
                        return vListaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contrato
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosSuscritos(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, String pNumeroContratoConvenio, String pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_SuscritosUsuario"))
                {
                    if (pFechaRegistroDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroDesde", DbType.DateTime, pFechaRegistroDesde);
                    if (pFechaRegistroHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroHasta", DbType.DateTime, pFechaRegistroHasta);
                    if (pNumeroContratoConvenio != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContratoConvenio", DbType.String, pNumeroContratoConvenio);
                    if (pVigenciaFiscalInicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalInicial", DbType.String, pVigenciaFiscalInicial);
                    if (pRegionalContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegionalContrato", DbType.Int32, pRegionalContrato);
                    if (pCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CategoriaContrato", DbType.Int32, pCategoriaContrato);
                    if (pTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoContrato", DbType.Int32, pTipoContrato);
                    if (pUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContrato = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vContrato.AcnoVigencia;
                            vContrato.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContrato.NombreRegional;
                            vContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vContrato.NombreCategoriaContrato;
                            vContrato.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : vContrato.NombreTipoContrato;
                            vListaContrato.Add(vContrato);
                        }
                        return vListaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pFechaRegistroDesde"></param>
        /// <param name="pFechaRegistroHasta"></param>
        /// <param name="pNumeroContratoConvenio"></param>
        /// <param name="pVigenciaFiscalInicial"></param>
        /// <param name="pRegionalContrato"></param>
        /// <param name="pCategoriaContrato"></param>
        /// <param name="pTipoContrato"></param>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public List<Entity.Contrato> ConsultarContratosConRP(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, string pNumeroContratoConvenio, string pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_RPUsuario"))
                {
                    if (pFechaRegistroDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroDesde", DbType.DateTime, pFechaRegistroDesde);
                    if (pFechaRegistroHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroHasta", DbType.DateTime, pFechaRegistroHasta);
                    if (pNumeroContratoConvenio != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContratoConvenio", DbType.String, pNumeroContratoConvenio);
                    if (pVigenciaFiscalInicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalInicial", DbType.String, pVigenciaFiscalInicial);
                    if (pRegionalContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegionalContrato", DbType.Int32, pRegionalContrato);
                    if (pCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CategoriaContrato", DbType.Int32, pCategoriaContrato);
                    if (pTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoContrato", DbType.Int32, pTipoContrato);
                    if (pUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContrato = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vContrato.AcnoVigencia;
                            vContrato.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContrato.NombreRegional;
                            vContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vContrato.NombreCategoriaContrato;
                            vContrato.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : vContrato.NombreTipoContrato;
                            vContrato.RequiereGarantia = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"].ToString()) : vContrato.RequiereGarantia;
                            vListaContrato.Add(vContrato);
                        }
                        return vListaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contrato
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosAprobacionGarantia(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, String pNumeroContratoConvenio, String pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_ConAprobacionGarantia"))
                {
                    if (pFechaRegistroDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroDesde", DbType.DateTime, pFechaRegistroDesde);
                    if (pFechaRegistroHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroHasta", DbType.DateTime, pFechaRegistroHasta);
                    if (pNumeroContratoConvenio != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContratoConvenio", DbType.String, pNumeroContratoConvenio);
                    if (pVigenciaFiscalInicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalInicial", DbType.String, pVigenciaFiscalInicial);
                    if (pRegionalContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegionalContrato", DbType.Int32, pRegionalContrato);
                    if (pCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CategoriaContrato", DbType.Int32, pCategoriaContrato);
                    if (pTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoContrato", DbType.Int32, pTipoContrato);
                    if (pUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContrato = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vContrato.AcnoVigencia;
                            vContrato.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContrato.NombreRegional;
                            vContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vContrato.NombreCategoriaContrato;
                            vContrato.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : vContrato.NombreTipoContrato;
                            vListaContrato.Add(vContrato);
                        }
                        return vListaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad EstadoContrato
        /// </summary>
        /// <param name="pIdEstadoContrato">Valor entero con el id del Estado del contrato</param>
        /// <param name="pCodigoEstadoContrato">Cadena de texto con el c�digo del estado del contrato</param>
        /// <param name="pDescripcion">Cadena de texto con la descripcipon del estado del contrato</param>
        /// <param name="pInactivo">Valor Booleano con el estado del registro</param>
        /// <returns>Lista de la instancia EstadoContrato</returns>
        public List<EstadoContrato> ConsultarEstadoContrato(int? pIdEstadoContrato, string pCodigoEstadoContrato, string pDescripcion, bool pInactivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_EstadoContrato_Consultar"))
                {
                    if (pIdEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, pIdEstadoContrato);
                    if (pCodigoEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoContrato", DbType.String, pCodigoEstadoContrato);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pInactivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.String, pInactivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoContrato> vListaEstadoContrato = new List<EstadoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoContrato vEstadoContrato = new EstadoContrato();
                            vEstadoContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vEstadoContrato.IdEstadoContrato;
                            vEstadoContrato.CodEstado = vDataReaderResults["CodEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodEstado"].ToString()) : vEstadoContrato.CodEstado;
                            vEstadoContrato.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoContrato.Descripcion;
                            vEstadoContrato.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vEstadoContrato.Inactivo;
                            vEstadoContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoContrato.FechaCrea;
                            vEstadoContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoContrato.UsuarioCrea;
                            vEstadoContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoContrato.FechaModifica;
                            vEstadoContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoContrato.UsuarioModifica;
                            vListaEstadoContrato.Add(vEstadoContrato);
                        }
                        return vListaEstadoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region LupaEmpleado
        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Actualiza la informaci�n de solicitante de un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informaci�n del solicitante del contrato</param>
        /// <returns>Entero con el resultado de la operacion</returns>
        public int LupaEmpleadoInsertarSolicitanteContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_ModificarSolicitante"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalEmpSol", DbType.Int32, pContrato.IdRegionalEmpSol);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependenciaEmpSol", DbType.Int32, pContrato.IdDependenciaEmpSol);
                    vDataBase.AddInParameter(vDbCommand, "@IdCargoEmpSol", DbType.Int32, pContrato.IdCargoEmpSol);
                    vDataBase.AddInParameter(vDbCommand, "@IdEmpleadoSolicitante", DbType.Int32, pContrato.IdEmpleadoSolicitante);

                    vDataBase.AddInParameter(vDbCommand, "@RegionalEmpSol", DbType.String, pContrato.RegionalEmpleadoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@DependenciaEmpSol", DbType.String, pContrato.DependenciaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@CargoEmpSol", DbType.String, pContrato.CargoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@EmpleadoSolicitante", DbType.String, pContrato.NombreEmpleadoSolicitante);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Actualiza la informaci�n de solicitante de un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informaci�n del solicitante del contrato</param>
        /// <returns>Entero con el resultado de la operacion</returns>
        public int LupaEmpleadoInsertarSolicitanteContratoPrecontractual(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Precontractual_ModificarSolicitante"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalEmpSol", DbType.Int32, pContrato.IdRegionalEmpSol);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependenciaEmpSol", DbType.Int32, pContrato.IdDependenciaEmpSol);
                    vDataBase.AddInParameter(vDbCommand, "@IdCargoEmpSol", DbType.Int32, pContrato.IdCargoEmpSol);
                    vDataBase.AddInParameter(vDbCommand, "@IdEmpleadoSolicitante", DbType.Int32, pContrato.IdEmpleadoSolicitante);

                    vDataBase.AddInParameter(vDbCommand, "@RegionalEmpSol", DbType.String, pContrato.RegionalEmpleadoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@DependenciaEmpSol", DbType.String, pContrato.DependenciaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@CargoEmpSol", DbType.String, pContrato.CargoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@EmpleadoSolicitante", DbType.String, pContrato.NombreEmpleadoSolicitante);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Actualiza la informaci�n de ordenador de gasto de un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informaci�n del ordenador de gasto del contrato</param>
        /// <returns>Entero con el resultado de la operacion</returns>
        public int LupaEmpleadoInsertarOrdenadoGasto(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_ModificarOrdenadorGasto"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalEmpOrdG", DbType.Int32, pContrato.IdRegionalEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependenciaEmpOrdG", DbType.Int32, pContrato.IdDependenciaEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@IdCargoEmpOrdG", DbType.Int32, pContrato.IdCargoEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@IdEmpleadoOrdenadorGasto", DbType.Int32, pContrato.IdEmpleadoOrdenadorGasto);

                    vDataBase.AddInParameter(vDbCommand, "@RegionalEmpOrdG", DbType.String, pContrato.RegionalEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@DependenciaEmpOrdG", DbType.String, pContrato.DependenciaEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@CargoEmpOrdG", DbType.String, pContrato.CargoEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@EmpleadoOrdenadorGasto", DbType.String, pContrato.NombreEmpleadoOrdenadorGasto);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int LupaEmpleadoInsertarOrdenadoGastoPrecontractual(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Precontractual_ModificarOrdenadorGasto"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalEmpOrdG", DbType.Int32, pContrato.IdRegionalEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependenciaEmpOrdG", DbType.Int32, pContrato.IdDependenciaEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@IdCargoEmpOrdG", DbType.Int32, pContrato.IdCargoEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@IdEmpleadoOrdenadorGasto", DbType.Int32, pContrato.IdEmpleadoOrdenadorGasto);

                    vDataBase.AddInParameter(vDbCommand, "@RegionalEmpOrdG", DbType.String, pContrato.RegionalEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@DependenciaEmpOrdG", DbType.String, pContrato.DependenciaEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@CargoEmpOrdG", DbType.String, pContrato.CargoEmpOrdG);
                    vDataBase.AddInParameter(vDbCommand, "@EmpleadoOrdenadorGasto", DbType.String, pContrato.NombreEmpleadoOrdenadorGasto);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region MetodosPaginaRegistroContratos
        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Registra un contrato vacio sobre el que se va a trabajar, devolviendo su identificador para el funcionamiento
        /// de las lupas
        /// </summary>
        /// <param name="pContrato">Entidad con la informaci�n m�nima para la creaci�n del contrato, en ella se
        /// almacena el identificador del contrato creado</param>
        /// <returns>Entero con el resultado de la operaci�n</returns>
        public int ContratoRegistroInicial(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_RegistroInicial"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContrato", DbType.Int32, 18);
                    if(pContrato.IdEstadoContrato.HasValue)
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, pContrato.IdEstadoContrato.Value);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContrato.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalContrato", DbType.Int32, pContrato.IdRegionalContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pContrato.IdContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContrato").ToString());
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Realiza la actualizacion de la informaci�n contenida dentro de la entidad de entrada de tipo contrato
        /// </summary>
        /// <param name="pContrato">Entidad con la informaci�n a actualizar</param>
        /// <returns>Entero con el resultado de la operaci�n</returns>
        public int ContratoActualizacion(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Actualizacion"))
                {
                    int vResultado;

                    #region DatosGeneralesContrato
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, pContrato.IdEstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContratoAsociado", DbType.Int32, pContrato.IdContratoAsociado);
                    vDataBase.AddInParameter(vDbCommand, "@ConvenioMarco", DbType.Boolean, pContrato.ConvenioMarco);
                    vDataBase.AddInParameter(vDbCommand, "@ConvenioAdhesion", DbType.Boolean, pContrato.ConvenioAdhesion);
                    vDataBase.AddInParameter(vDbCommand, "@FK_IdContrato", DbType.Int32, pContrato.FK_IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, pContrato.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pContrato.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadAcademica", DbType.String, pContrato.IdModalidadAcademica);
                    vDataBase.AddInParameter(vDbCommand, "@IdProfesion", DbType.String, pContrato.IdProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pContrato.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroProceso", DbType.Int32, pContrato.IdNumeroProceso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicacionProceso", DbType.DateTime, pContrato.FechaAdjudicacionProceso);
                    vDataBase.AddInParameter(vDbCommand, "@ActaDeInicio", DbType.Boolean, pContrato.ActaDeInicio);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaAporte", DbType.Boolean, pContrato.ManejaAporte);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaRecurso", DbType.Boolean, pContrato.ManejaRecurso);
                    vDataBase.AddInParameter(vDbCommand, "@EsContSinValor", DbType.Boolean, pContrato.EsContSinValor);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegimenContratacion", DbType.Int32, pContrato.IdRegimenContratacion);
                    if(pContrato.FechaActaInicio.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@FechaActaInicio", DbType.DateTime, pContrato.FechaActaInicio.Value);

                    if (pContrato.IdRegionalContrato.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pContrato.IdRegionalContrato.Value);

                    #endregion

                    #region ConsecutivoPlanCompras
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoPlanCompras", DbType.Int32, pContrato.ConsecutivoPlanComprasAsociado);
                    #endregion

                    #region VigenciaValorContrato
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoDelContrato", DbType.String, pContrato.ObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@AlcanceObjetoDelContrato", DbType.String, pContrato.AlcanceObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInicialContrato", DbType.Decimal, pContrato.ValorInicialContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.DateTime, pContrato.FechaInicioEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@EsFechaFinalCalculada", DbType.Boolean, pContrato.EsFechaFinalCalculada);
                    vDataBase.AddInParameter(vDbCommand, "@DiasFechaFinalCalculada", DbType.Int16, pContrato.DiasFechaFinalCalculada);
                    vDataBase.AddInParameter(vDbCommand, "@MesesFechaFinalCalculada", DbType.Int16, pContrato.MesesFechaFinalCalculada);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalizacionIniciaContrato", DbType.DateTime, pContrato.FechaFinalizacionIniciaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalTerminacionContrato", DbType.DateTime, pContrato.FechaFinalTerminacionContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaVigenciaFuturas", DbType.Boolean, pContrato.ManejaVigenciaFuturas);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaInicial", DbType.Int32, pContrato.IdVigenciaInicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFinal", DbType.Int32, pContrato.IdVigenciaFinal);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaPago", DbType.Int32, pContrato.IdFormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereGarantia", DbType.Boolean, pContrato.RequiereGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@AportesEspecieICBF", DbType.Boolean, pContrato.AportesEspecie);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAnticipo", DbType.Int32, pContrato.ValorAnticipo);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeAnticipo", DbType.Int32, pContrato.PorcentajeAnticipo);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPago", DbType.Int32, pContrato.IdTipoPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoSECOP", DbType.Int32, pContrato.IdCodigoSECOP);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContratoMigrado", DbType.String, pContrato.NumeroContratoMigrado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pContrato.UsuarioModifica);
                    #endregion

                    #region LugarEjecucion
                    vDataBase.AddInParameter(vDbCommand, "@DatosAdicionaleslugarEjecucion", DbType.String, pContrato.DatosAdicionaleslugarEjecucion);
                    #endregion

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Obtiene el contrato asociado con el identificador dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato a obtener.</param>
        /// <returns>Entidad contrato con la informaci�n obtenida de la base de datos</returns>
        public Contrato.Entity.Contrato ContratoObtener(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contrato_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                        while (vDataReaderResults.Read())
                        {
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContrato.IdRegionalContrato;
                            vContrato.IdContratoAsociado = vDataReaderResults["IdContratoAsociado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoAsociado"].ToString()) : vContrato.IdContratoAsociado;
                            vContrato.ConvenioMarco = vDataReaderResults["ConvenioMarco"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvenioMarco"].ToString()) : vContrato.ConvenioMarco;
                            vContrato.ConvenioAdhesion = vDataReaderResults["ConvenioAdhesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvenioAdhesion"].ToString()) : vContrato.ConvenioAdhesion;
                            vContrato.FK_IdContrato = vDataReaderResults["FK_IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FK_IdContrato"].ToString()) : vContrato.FK_IdContrato;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContrato.IdEstadoContrato;
                            vContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContrato.IdCategoriaContrato;
                            vContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContrato.IdTipoContrato;
                            vContrato.IdModalidadAcademica = vDataReaderResults["IdModalidadAcademica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidadAcademica"].ToString()) : vContrato.IdModalidadAcademica;
                            vContrato.IdProfesion = vDataReaderResults["IdProfesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdProfesion"].ToString()) : vContrato.IdProfesion;
                            vContrato.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContrato.IdModalidadSeleccion;
                            vContrato.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContrato.IdNumeroProceso;
                            vContrato.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContrato.NumeroProceso;
                            vContrato.FechaAdjudicacionProceso = vDataReaderResults["FechaAdjudicacionDelProceso"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacionDelProceso"].ToString()) : vContrato.FechaAdjudicacionProceso;
                            vContrato.ActaDeInicio = vDataReaderResults["ActaDeInicio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ActaDeInicio"].ToString()) : vContrato.ActaDeInicio;
                            vContrato.ManejaAporte = vDataReaderResults["ManejaAporte"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaAporte"].ToString()) : vContrato.ManejaAporte;
                            vContrato.ManejaRecurso = vDataReaderResults["ManejaRecurso"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaRecurso"].ToString()) : vContrato.ManejaRecurso;
                            vContrato.ConsecutivoPlanComprasAsociado = vDataReaderResults["ConsecutivoPlanCompasAsociado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompasAsociado"].ToString()) : vContrato.ConsecutivoPlanComprasAsociado; 
                            vContrato.IdRegimenContratacion = vDataReaderResults["IdRegimenContratacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegimenContratacion"].ToString()) : vContrato.IdRegimenContratacion;
                            vContrato.IdEmpleadoSolicitante = vDataReaderResults["IdEmpleadoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleadoSolicitante"].ToString()) : vContrato.IdEmpleadoSolicitante;
                            vContrato.IdRegionalEmpSol = vDataReaderResults["IdRegionalSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalSolicitante"].ToString()) : vContrato.IdRegionalEmpSol;
                            vContrato.IdDependenciaEmpSol = vDataReaderResults["IdDependenciaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaSolicitante"].ToString()) : vContrato.IdDependenciaEmpSol;
                            vContrato.IdCargoEmpSol = vDataReaderResults["IdCargoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoSolicitante"].ToString()) : vContrato.IdCargoEmpSol;
                            vContrato.IdEmpleadoOrdenadorGasto = vDataReaderResults["IdEmpleadoOrdenadorGasto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleadoOrdenadorGasto"].ToString()) : vContrato.IdEmpleadoOrdenadorGasto;
                            vContrato.IdRegionalEmpOrdG = vDataReaderResults["IdRegionalOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalOrdenador"].ToString()) : vContrato.IdRegionalEmpOrdG;
                            vContrato.IdDependenciaEmpOrdG = vDataReaderResults["IdDependenciaOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaOrdenador"].ToString()) : vContrato.IdDependenciaEmpOrdG;
                            vContrato.IdCargoEmpOrdG = vDataReaderResults["IdCargoOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoOrdenador"].ToString()) : vContrato.IdCargoEmpOrdG;
                            vContrato.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoDelContrato"].ToString()) : vContrato.ObjetoContrato;
                            vContrato.AlcanceObjetoContrato = vDataReaderResults["AlcanceObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AlcanceObjetoDelContrato"].ToString()) : vContrato.AlcanceObjetoContrato;
                            vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;
                            vContrato.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContrato.FechaFinalizacionIniciaContrato;
                            vContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vContrato.FechaFinalTerminacionContrato;
                            vContrato.FechaSuscripcionContrato = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"].ToString()) : vContrato.FechaSuscripcionContrato; 
                            vContrato.ManejaVigenciaFuturas = vDataReaderResults["ManejaVigenciasFuturas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaVigenciasFuturas"].ToString()) : vContrato.ManejaVigenciaFuturas;
                            vContrato.IdVigenciaInicial= vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContrato.IdVigenciaInicial;
                            vContrato.IdVigenciaFinal= vDataReaderResults["IdVigenciaFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaFinal"].ToString()) : vContrato.IdVigenciaFinal;
                            vContrato.IdFormaPago= vDataReaderResults["IdFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaPago"].ToString()) : vContrato.IdFormaPago;
                            vContrato.DatosAdicionaleslugarEjecucion = vDataReaderResults["DatosAdicionaleslugarEjecucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DatosAdicionaleslugarEjecucion"].ToString()) : vContrato.DatosAdicionaleslugarEjecucion;
                            vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            vContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContrato.FechaModifica;
                            vContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContrato.UsuarioCrea;
                            vContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContrato.UsuarioModifica;

                            vContrato.FechaActaInicio = vDataReaderResults["FechaActaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaActaInicio"].ToString()) : vContrato.FechaActaInicio;
                            vContrato.RequiereGarantia = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"].ToString()) : vContrato.RequiereGarantia;
                            vContrato.AportesEspecie = vDataReaderResults["AportesEspecieICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportesEspecieICBF"].ToString()) : vContrato.AportesEspecie;
                            vContrato.ValorAnticipo = vDataReaderResults["ValorAnticipo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ValorAnticipo"].ToString()) : vContrato.ValorAnticipo;
                            vContrato.PorcentajeAnticipo = vDataReaderResults["PorcentajeAnticipo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorcentajeAnticipo"].ToString()) : vContrato.PorcentajeAnticipo;
                            vContrato.IdTipoPago = vDataReaderResults["IdTipoPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPago"].ToString()) : vContrato.IdTipoPago;
                            vContrato.NumeroContratoMigrado = vDataReaderResults["NumeroContratoMigrado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContratoMigrado"].ToString()) : vContrato.NumeroContratoMigrado;
                            vContrato.IdCodigoSECOP = vDataReaderResults["IdCodigoSECOP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCodigoSECOP"].ToString()) : vContrato.IdCodigoSECOP;

                            vContrato.NombreEstadoContrato = vDataReaderResults["NombreEstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoContrato"].ToString()) : vContrato.NombreEstadoContrato;
                            vContrato.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLiquidacion"].ToString()) : vContrato.FechaLiquidacion;
                            vContrato.ValorTotalCDP = vDataReaderResults["ValorTotalCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalCDP"].ToString()) : vContrato.ValorTotalCDP;
                            vContrato.ValorTotalAportesICBF = vDataReaderResults["ValorTotalAportesICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalAportesICBF"].ToString()) : vContrato.ValorTotalAportesICBF;
                            vContrato.ValorTotalAportesContratista = vDataReaderResults["ValorAportesContratista"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesContratista"].ToString()) : vContrato.ValorTotalAportesContratista;
                            vContrato.ValorTotalContratosAdheridos = vDataReaderResults["ValorTotalContratosAdheridos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalContratosAdheridos"].ToString()) : vContrato.ValorTotalContratosAdheridos;
                            vContrato.EsFechaFinalCalculada = vDataReaderResults["EsFechaFinalCalculada"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsFechaFinalCalculada"].ToString()) : vContrato.EsFechaFinalCalculada;
                            vContrato.DiasFechaFinalCalculada = vDataReaderResults["DiasFechaFinalCalculada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasFechaFinalCalculada"].ToString()) : vContrato.DiasFechaFinalCalculada;
                            vContrato.MesesFechaFinalCalculada = vDataReaderResults["MesesFechaFinalCalculada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["MesesFechaFinalCalculada"].ToString()) : vContrato.MesesFechaFinalCalculada;
                            vContrato.VinculoSECOP = vDataReaderResults["VinculoSECOP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VinculoSECOP"].ToString()) : vContrato.VinculoSECOP;
                            vContrato.ValorAportesInicialDineroICBF = vDataReaderResults["ValorInicialAportesDineroICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialAportesDineroICBF"].ToString()) : vContrato.ValorAportesInicialDineroICBF;
                            vContrato.ValorVigenciasFuturas = vDataReaderResults["ValorVigenciasFuturas"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorVigenciasFuturas"].ToString()) : vContrato.ValorVigenciasFuturas;

                        }
                        return vContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ActualizarValorInicialContrato(int vIdContrato, decimal vValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Calcular_ValorInicial"))
                {
                    int vResultado;
                    decimal vValorInicial;

                    vDataBase.AddOutParameter(vDbCommand, "@ValorInicial", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, vIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorProductos", DbType.Decimal, vValor);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vValorInicial = Convert.ToDecimal(vDataBase.GetParameterValue(vDbCommand, "@ValorInicial").ToString());
                    return vValorInicial;
                    
                }            
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorsinAportes(int vIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ConsultarValorContratoSinAportes"))
                {
                    int vResultado;
                    decimal vValorInicial;

                    vDataBase.AddOutParameter(vDbCommand, "@ValorInicial", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, vIdContrato);                   
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vValorInicial = Convert.ToDecimal(vDataBase.GetParameterValue(vDbCommand, "@ValorInicial").ToString());
                    return vValorInicial;

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Valida que el valor final del contrato sea mayor igual a la sumatoria de los valores finales
        /// de los contratos asociados a dicho contrato + el valor inical dado
        /// </summary>
        /// <param name="pIdContratoConvMarco">Entero con el identificador del contrato convenio marco</param>
        /// <param name="pValorInicialContConv">Decimal con el valor inicial del contrato a validar</param>
        /// <returns>Retorna false si el ValorFinalContratoConvMarco no supera la sumatoria de los contratos que contiene, 
        /// Retorna true si la validacion permite paso</returns>
        public bool ValidacionValorInicialSuperaValorFinal(int pIdContratoConvMarco, decimal pValorInicialContConv, int pIdContrato)
        {
            //return true;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVlrFinal"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoConvMarco", DbType.Int32, pIdContratoConvMarco);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInicialContConv", DbType.Decimal, pValorInicialContConv);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratosCDP vContratos = new ContratosCDP();
                        while (vDataReaderResults.Read())
                        {
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Valida si la regional del contrato convenio coincide con la regional de
        /// los registros de plan de compras
        /// </summary>
        /// <param name="pIdContratoConvMarco">Entero con el identificador del contrato</param>
        /// <param name="pCodigoRegionalContConv">Cadena de texto con el codigo de la regional</param>
        /// <returns>Retorna true si la validacion permite paso</returns>
        public bool ValidacionCoincideRegionalesContratoPlanComprasContrato(int pIdContratoConvMarco, string pCodigoRegionalContConv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Contrato_ValidacionRegionalesContratoPlanComp"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoConvMarco", DbType.Int32, pIdContratoConvMarco);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegionalContConv", DbType.String, pCodigoRegionalContConv);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratosCDP vContratos = new ContratosCDP();
                        while (vDataReaderResults.Read())
                        {
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /*
         * INTENTO POR SACAR VALIDACIONES DE PLANCOMPRAS VS CDP A SP, NO SE PUEDE REALIZAR YA QUE EL VALOR DEL RUBRO DEL PLAN DE COMPRAS SE OBTIENE
         * DE UN WEBSERVICE Y NO QUEDA ALMACENADO EN LOS REGISTROS DE LA BASE DE DATOS
        public bool ValidacionPlanComprasVsCDP(int pIdContrato, bool pValidaValor, bool pValidaRubro)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Contrato_Validaciones_PlanCompras_Vs_CDP"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValidaValor", DbType.String, pValidaValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValidaRubro", DbType.String, pValidaRubro);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratosCDP vContratos = new ContratosCDP();
                        while (vDataReaderResults.Read())
                        {
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        */
        
        #endregion

        #region ContratosAdheridos

        /// <summary>
        /// M�todo de consulta por id para la entidad Contrato de Contratos Relacionados Convenio Marco
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el IdContrato</param>
        /// <returns>Lista de la instancia Contrato</returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosRelacionadosConvMarco(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contratos_Relacionados_Conv_Marco_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            vListaContratos.Add(vContrato);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad Contrato de Contratos Adheridos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el IdContrato</param>
        /// <returns>Lista de la instancia Contrato</returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosAdheridos(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contratos_Adheridos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            vListaContratos.Add(vContrato);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pVigenciaFuturas"></param>
        /// <returns></returns>
        public bool ValidaValorInicialVsCDPplusVigenciasFuturas(int pIdContrato, VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Contrato_ValidacionVlrInicialVsCDPplusVigenciasFuturas"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorVigenciaFutura", DbType.Decimal, pVigenciaFuturas.ValorVigenciaFutura);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratosCDP vContratos = new ContratosCDP();
                        while (vDataReaderResults.Read())
                        {
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public bool ConsultarContratoRPGarantiasAprobadas(int pIdContrato)
        {
            bool isValid = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Contrato_ContratoConGarantiasAprobadasyRP"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddOutParameter(vDbCommand,"@EsValido", DbType.Boolean,10);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    isValid = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@EsValido").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return isValid;

        }

        public int ConsultarIdentificacionOrdenadorG(int vIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ConultarIndentificacionOrdenadorG"))
                {
                    int vResultado;
                    int NumeroIdentificacion;

                    vDataBase.AddOutParameter(vDbCommand, "@NumeroIdentificacion", DbType.Int32,18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, vIdContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    NumeroIdentificacion =Convert.ToInt32( vDataBase.GetParameterValue(vDbCommand, "@NumeroIdentificacion"));
                    return NumeroIdentificacion;

                }
              
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        public Icbf.Contrato.Entity.ContratistaMigrados ConsultarContratistaMigracion(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratistasMigrados_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Icbf.Contrato.Entity.ContratistaMigrados vContratistaMigrado = new Icbf.Contrato.Entity.ContratistaMigrados();                        
                        while (vDataReaderResults.Read())
                        {
                            vContratistaMigrado.IdContratosMigradosContratista = vDataReaderResults["IdContratosMigradosContratista"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosMigradosContratista"].ToString()) : vContratistaMigrado.IdContratosMigradosContratista;


                            vContratistaMigrado.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vContratistaMigrado.IdTipoPersona;
                            vContratistaMigrado.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vContratistaMigrado.TipoIdentificacion;
                            vContratistaMigrado.NombreRazonSocial = vDataReaderResults["NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRazonSocial"].ToString()) : vContratistaMigrado.NombreRazonSocial;
                            vContratistaMigrado.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vContratistaMigrado.NumeroIdentificacion;
                            vContratistaMigrado.Genero = vDataReaderResults["Genero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Genero"].ToString()) : vContratistaMigrado.Genero;
                            vContratistaMigrado.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vContratistaMigrado.Direccion;
                            vContratistaMigrado.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vContratistaMigrado.Telefono;
                            vContratistaMigrado.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vContratistaMigrado.CorreoElectronico;
                            vContratistaMigrado.Profesion = vDataReaderResults["Profesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Profesion"].ToString()) : vContratistaMigrado.Profesion;
                            vContratistaMigrado.Ubicacion = vDataReaderResults["Ubicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ubicacion"].ToString()) : vContratistaMigrado.Ubicacion;
                            vContratistaMigrado.Otro = vDataReaderResults["Otro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Otro"].ToString()) : vContratistaMigrado.Otro;
                            vContratistaMigrado.TipoIdentificacion = vDataReaderResults["TipoIdentificaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificaciones"].ToString()) : vContratistaMigrado.TipoIdentificacion;
                            vContratistaMigrado.TipoPersona = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vContratistaMigrado.TipoPersona;
                            vContratistaMigrado.NombreRepresentanteLegal = vDataReaderResults["NombreRepresentanteLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRepresentanteLegal"].ToString()) : vContratistaMigrado.NombreRepresentanteLegal;
                            vContratistaMigrado.IdentificacionRepresentanteLegal = vDataReaderResults["IdentificacionRepresentanteLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepresentanteLegal"].ToString()) : vContratistaMigrado.IdentificacionRepresentanteLegal;

                           
                        }
                        return vContratistaMigrado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataSet ConsultarContratosSuscritosObligacionesPresupuestales(string ids, string usuario, string rol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_Suscritos_GeneracionArchivoObligaciones"))
                {
                    if (! string.IsNullOrEmpty(ids))
                        vDataBase.AddInParameter(vDbCommand, "@IdsContratos", DbType.String, ids);

                    vDataBase.AddInParameter(vDbCommand, "@usuario", DbType.String, usuario);
                    vDataBase.AddInParameter(vDbCommand, "@rol", DbType.String, rol);

                    DataSet vResultado = vDataBase.ExecuteDataSet(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarContratosSuscritosObligacionesPresupuestales(string ids, string usuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_Suscritos_GeneracionLogCargueObligaciones"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdsContratos", DbType.String, ids);
                    vDataBase.AddInParameter(vDbCommand, "@usuario", DbType.String, usuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contratos
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pFechaRegistroSistemaHasta"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContraro"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratosGenerarObligaciones(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_Suscritos_GeneracionLogCargueObligaciones_Consultar"))
                {
                    vDbCommand.CommandTimeout = 3600;

                    if (pFechaSuscripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pFechaSuscripcion);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContratos = new Contrato.Entity.Contrato();
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            vContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratos.NumeroContrato;
                            vContratos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContratos.IdNumeroProceso;
                            vContratos.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContratos.IdVigenciaInicial;
                            vContratos.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContratos.IdRegionalContrato;
                            vContratos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContratos.IdModalidadSeleccion;
                            vContratos.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContratos.IdCategoriaContrato;
                            vContratos.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContratos.IdTipoContrato;
                            vContratos.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContratos.IdEstadoContrato;
                            vContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratos.UsuarioCrea;
                            vContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratos.FechaCrea;
                            vContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratos.UsuarioModifica;
                            vContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratos.FechaModifica;
                            vContratos.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratos.NombreRegional;
                            vContratos.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContratos.NombreModalidadSeleccion;
                            vContratos.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContratos.NombreCategoriaContrato;
                            vContratos.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContratos.NombreTipoContrato;
                            vContratos.NombreEstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratos.NombreEstadoContrato;
                            vContratos.AcnoVigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vContratos.AcnoVigencia;
                            vContratos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;
                            vContratos.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratos.FechaInicioEjecucion;
                            vContratos.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContratos.FechaFinalizacionIniciaContrato;
                            vContratos.FechaSuscripcionContrato = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContratos.FechaSuscripcionContrato;
                            vContratos.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vContratos.NombreContratista;
                            vContratos.NumeroDocumentoIdentificacion = vDataReaderResults["NumeroDocumentoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumentoIdentificacion"].ToString()) : vContratos.NumeroDocumentoIdentificacion;
                            vContratos.TipoDocumentoIdentificacion = vDataReaderResults["TipoDocumentoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoIdentificacion"].ToString()) : vContratos.TipoDocumentoIdentificacion;
                            vContratos.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoRegional"].ToString()) : vContratos.CodigoRegional;

                            vListaContratos.Add(vContratos);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosInformeObligaciones(DateTime? pFechaInicioDesde, DateTime? pFechaInicioHasta, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_InformeObligaciones_Consultar"))
                {
                    vDbCommand.CommandTimeout = 3600;

                    if (pFechaInicioDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaDesde", DbType.DateTime, pFechaInicioDesde);
                    if (pFechaInicioHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaHasta", DbType.DateTime, pFechaInicioHasta);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                    vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContratos = new Contrato.Entity.Contrato();
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            vContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratos.NumeroContrato;
                            vContratos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContratos.IdNumeroProceso;
                            vContratos.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContratos.IdVigenciaInicial;
                            vContratos.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContratos.IdRegionalContrato;
                            vContratos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContratos.IdModalidadSeleccion;
                            vContratos.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContratos.IdCategoriaContrato;
                            vContratos.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContratos.IdTipoContrato;
                            vContratos.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContratos.IdEstadoContrato;
                            vContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratos.UsuarioCrea;
                            vContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratos.FechaCrea;
                            vContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratos.UsuarioModifica;
                            vContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratos.FechaModifica;
                            vContratos.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratos.NombreRegional;
                            vContratos.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContratos.NombreModalidadSeleccion;
                            vContratos.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContratos.NombreCategoriaContrato;
                            vContratos.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContratos.NombreTipoContrato;
                            vContratos.NombreEstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratos.NombreEstadoContrato;
                            vContratos.AcnoVigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vContratos.AcnoVigencia;
                            vContratos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;
                            vContratos.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratos.FechaInicioEjecucion;
                            vContratos.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContratos.FechaFinalizacionIniciaContrato;
                            vContratos.FechaSuscripcionContrato = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContratos.FechaSuscripcionContrato;
                            vContratos.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vContratos.NombreContratista;
                            vContratos.NumeroDocumentoIdentificacion = vDataReaderResults["NumeroDocumentoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumentoIdentificacion"].ToString()) : vContratos.NumeroDocumentoIdentificacion;
                            vContratos.TipoDocumentoIdentificacion = vDataReaderResults["TipoDocumentoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoIdentificacion"].ToString()) : vContratos.TipoDocumentoIdentificacion;
                            vContratos.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoRegional"].ToString()) : vContratos.CodigoRegional;

                            vListaContratos.Add(vContratos);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Contrato.Entity.Contrato> ConsultarContratosGenerarAreas(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato, string idDependencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_Reportes_ConsultarContratosGenerarAreas"))
                {
                    vDbCommand.CommandTimeout = 3600;

                    if (pFechaSuscripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pFechaSuscripcion);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                    vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IDDependencia", DbType.String, idDependencia);

                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContratos = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContratos = new Contrato.Entity.Contrato();
                            vContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratos.IdContrato;
                            vContratos.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratos.NumeroContrato;
                            vContratos.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"].ToString()) : vContratos.IdNumeroProceso;
                            vContratos.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"].ToString()) : vContratos.IdVigenciaInicial;
                            vContratos.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContratos.IdRegionalContrato;
                            vContratos.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vContratos.IdModalidadSeleccion;
                            vContratos.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContratos.IdCategoriaContrato;
                            vContratos.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContratos.IdTipoContrato;
                            vContratos.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContratos.IdEstadoContrato;
                            vContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratos.UsuarioCrea;
                            vContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratos.FechaCrea;
                            vContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratos.UsuarioModifica;
                            vContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratos.FechaModifica;
                            vContratos.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratos.NombreRegional;
                            vContratos.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vContratos.NombreModalidadSeleccion;
                            vContratos.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vContratos.NombreCategoriaContrato;
                            vContratos.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vContratos.NombreTipoContrato;
                            vContratos.NombreEstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratos.NombreEstadoContrato;
                            vContratos.AcnoVigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vContratos.AcnoVigencia;
                            vContratos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContratos.NumeroProceso;
                            vContratos.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratos.FechaInicioEjecucion;
                            vContratos.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"].ToString()) : vContratos.FechaFinalizacionIniciaContrato;
                            vContratos.FechaSuscripcionContrato = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContratos.FechaSuscripcionContrato;
                            vContratos.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vContratos.NombreContratista;
                            vContratos.NumeroDocumentoIdentificacion = vDataReaderResults["NumeroDocumentoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumentoIdentificacion"].ToString()) : vContratos.NumeroDocumentoIdentificacion;
                            vContratos.TipoDocumentoIdentificacion = vDataReaderResults["TipoDocumentoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoIdentificacion"].ToString()) : vContratos.TipoDocumentoIdentificacion;

                            vListaContratos.Add(vContratos);
                        }
                        return vListaContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataSet ConsultarContratosGenerarAreasDataSet(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato, string idDependencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_Reportes_ConsultarContratosGenerarAreasDataSet"))
                {
                    vDbCommand.CommandTimeout = 3600;

                    if (pFechaSuscripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pFechaSuscripcion);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, pVigenciaFiscalinicial);
                    vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIDRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IDDependencia", DbType.String, idDependencia);

                    if (pIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, pIDCategoriaContrato);
                    if (pIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, pIDTipoContrato);

                    return vDataBase.ExecuteDataSet(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
