﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad PlanDeComprasContratos
    /// </summary>
    public class PlanDeComprasContratosDAL : GeneralDAL
    {
        public PlanDeComprasContratosDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int InsertarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeComprasContratos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pPlanDeComprasContratos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeCompras", DbType.Int32, pPlanDeComprasContratos.IDPlanDeCompras);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanDeComprasContratos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPlanDeComprasContratos.IDPlanDeComprasContratos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDPlanDeComprasContratos").ToString());
                    GenerarLogAuditoria(pPlanDeComprasContratos, vDbCommand);
                    return vResultado;

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int ModificarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeComprasContratos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanDeComprasContratos.IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pPlanDeComprasContratos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeCompras", DbType.Int32, pPlanDeComprasContratos.IDPlanDeCompras);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlanDeComprasContratos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanDeComprasContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int EliminarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeComprasContratos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanDeComprasContratos.IDPlanDeComprasContratos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanDeComprasContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pIDPlanDeComprasContratos"></param>
        public PlanDeComprasContratos ConsultarPlanDeComprasContratos(int pIDPlanDeComprasContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeComprasContratos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pIDPlanDeComprasContratos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        PlanDeComprasContratos vPlanDeComprasContratos = new PlanDeComprasContratos();
                        while (vDataReaderResults.Read())
                        {
                            vPlanDeComprasContratos.IDPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanDeComprasContratos.IDPlanDeComprasContratos;
                            vPlanDeComprasContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vPlanDeComprasContratos.IdContrato;
                            vPlanDeComprasContratos.IDPlanDeCompras = vDataReaderResults["IDPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeCompras"].ToString()) : vPlanDeComprasContratos.IDPlanDeCompras;
                            vPlanDeComprasContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanDeComprasContratos.UsuarioCrea;
                            vPlanDeComprasContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanDeComprasContratos.FechaCrea;
                            vPlanDeComprasContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanDeComprasContratos.UsuarioModifica;
                            vPlanDeComprasContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanDeComprasContratos.FechaModifica;
                        }
                        return vPlanDeComprasContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIDPlanDeCompras"></param>
        public List<PlanDeComprasContratos> ConsultarPlanDeComprasContratoss(int? pIdContrato, int? pIDPlanDeCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_PlanDeComprasContratoss_Consultar"))
                {
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pIDPlanDeCompras != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDPlanDeCompras", DbType.Int32, pIDPlanDeCompras);                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<PlanDeComprasContratos> vListaPlanDeComprasContratos = new List<PlanDeComprasContratos>();
                        while (vDataReaderResults.Read())
                        {
                            PlanDeComprasContratos vPlanDeComprasContratos = new PlanDeComprasContratos();
                            vPlanDeComprasContratos.IDPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanDeComprasContratos.IDPlanDeComprasContratos;
                            vPlanDeComprasContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vPlanDeComprasContratos.IdContrato;
                            vPlanDeComprasContratos.IDPlanDeCompras = vDataReaderResults["IDPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeCompras"].ToString()) : vPlanDeComprasContratos.IDPlanDeCompras;
                            vPlanDeComprasContratos.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vPlanDeComprasContratos.Vigencia;
                            vPlanDeComprasContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanDeComprasContratos.UsuarioCrea;
                            vPlanDeComprasContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanDeComprasContratos.FechaCrea;
                            vPlanDeComprasContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanDeComprasContratos.UsuarioModifica;
                            vPlanDeComprasContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanDeComprasContratos.FechaModifica;
                            vPlanDeComprasContratos.ValorTotal = vDataReaderResults["ValorTotal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotal"].ToString()) : vPlanDeComprasContratos.ValorTotal;
                            vPlanDeComprasContratos.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vPlanDeComprasContratos.IdAdicion;
                            vPlanDeComprasContratos.EsAdicion = vDataReaderResults["EsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsAdicion"].ToString()) : vPlanDeComprasContratos.EsAdicion;                          
                            vListaPlanDeComprasContratos.Add(vPlanDeComprasContratos);
                        }
                        return vListaPlanDeComprasContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlanDeComprasContratos> ConsultarPlanDeComprasContratosVigencias(int? pIdContrato, int? pIDPlanDeCompras,bool? pEsVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasContratosVigencias_Consultar"))
                {
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pIDPlanDeCompras != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDPlanDeCompras", DbType.Int32, pIDPlanDeCompras);
                    if (pEsVigenciaFutura != null)
                        vDataBase.AddInParameter(vDbCommand, "@esVigenciaFutura", DbType.Int32, pEsVigenciaFutura);
                    if (pAnioVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.Int32, pAnioVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<PlanDeComprasContratos> vListaPlanDeComprasContratos = new List<PlanDeComprasContratos>();
                        while (vDataReaderResults.Read())
                        {
                            
                            PlanDeComprasContratos vPlanDeComprasContratos = new PlanDeComprasContratos();
                            vPlanDeComprasContratos.IDPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanDeComprasContratos.IDPlanDeComprasContratos;                            
                            vPlanDeComprasContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vPlanDeComprasContratos.IdContrato;
                            vPlanDeComprasContratos.IDPlanDeCompras = vDataReaderResults["IDPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeCompras"].ToString()) : vPlanDeComprasContratos.IDPlanDeCompras;
                            vPlanDeComprasContratos.NumeroConsecutivoPlanCompras = vDataReaderResults["IDPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeCompras"].ToString()) : vPlanDeComprasContratos.NumeroConsecutivoPlanCompras;
                            vPlanDeComprasContratos.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vPlanDeComprasContratos.Vigencia;
                            vPlanDeComprasContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanDeComprasContratos.UsuarioCrea;
                            vPlanDeComprasContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanDeComprasContratos.FechaCrea;
                            vPlanDeComprasContratos.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vPlanDeComprasContratos.CodigoRegional;
                            vPlanDeComprasContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanDeComprasContratos.UsuarioModifica;
                            vPlanDeComprasContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanDeComprasContratos.FechaModifica;
                            vPlanDeComprasContratos.ValorTotal = vDataReaderResults["ValorTotal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotal"].ToString()) : vPlanDeComprasContratos.ValorTotal;
                            vPlanDeComprasContratos.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vPlanDeComprasContratos.IdAdicion;
                            vPlanDeComprasContratos.EsAdicion = vDataReaderResults["EsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsAdicion"].ToString()) : vPlanDeComprasContratos.EsAdicion;
                            vListaPlanDeComprasContratos.Add(vPlanDeComprasContratos);
                        }
                        return vListaPlanDeComprasContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool VerificarExistenciaModificacionProducto(int IDPlanDeComprasContratos, string IdProducto, bool esAdicion, int idModificacion)
        {
            bool isValid = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_VerificarExistenciaModificacionProducto"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@IdProducto", DbType.String, IdProducto);
                    vDataBase.AddInParameter(vDbCommand, "@esAdicion", DbType.Boolean, esAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@idModificacion", DbType.Int32, idModificacion);
                    vDataBase.AddOutParameter(vDbCommand,"@existe", DbType.Boolean, 5);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    isValid = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@existe").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return isValid;
        }
        

    }
}

