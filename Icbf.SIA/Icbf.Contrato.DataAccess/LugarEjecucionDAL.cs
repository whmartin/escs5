using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Lugar ejecuci�n
    /// </summary>
    public class LugarEjecucionDAL : GeneralDAL
    {
        public LugarEjecucionDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad LugarEjecucion
        /// </summary>
        /// <param name="pLugarEjecucion"></param>
        /// <returns></returns>
        public int InsertarLugarEjecucion(LugarEjecucion pLugarEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_LugarEjecucion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoLugarEjecucion", DbType.Int32, pLugarEjecucion.IdContratoLugarEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@IDDepartamento", DbType.String, pLugarEjecucion.IDDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IDMunicipio", DbType.String, pLugarEjecucion.IDMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pLugarEjecucion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pLugarEjecucion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad LugarEjecucion
        /// </summary>
        /// <param name="pLugarEjecucion"></param>
        /// <returns></returns>
        public int EliminarLugaresEjecucion(LugarEjecucion pLugarEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_LugarEjecucion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoLugarEjecucion", DbType.Int32, pLugarEjecucion.IdContratoLugarEjecucion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pLugarEjecucion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad LugarEjecucion
        /// </summary>
        /// <param name="IdContratoLugarEjecucion"></param>
        /// <returns></returns>
        public List<LugarEjecucion> ConsultarLugarEjecucions(int IdContratoLugarEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_LugarEjecucions_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoLugarEjecucion", DbType.Int32, IdContratoLugarEjecucion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<LugarEjecucion> vListaLugarEjecucion = new List<LugarEjecucion>();
                        while (vDataReaderResults.Read())
                        {
                            LugarEjecucion vLugarEjecucion = new LugarEjecucion();
                            vLugarEjecucion.IdContratoLugarEjecucion = vDataReaderResults["IdContratoLugarEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoLugarEjecucion"].ToString()) : vLugarEjecucion.IdContratoLugarEjecucion;
                            vLugarEjecucion.IDDepartamento = vDataReaderResults["IDDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDepartamento"].ToString()) : vLugarEjecucion.IDDepartamento;
                            vLugarEjecucion.IDMunicipio = vDataReaderResults["IDMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDMunicipio"].ToString()) : vLugarEjecucion.IDMunicipio;
                            vLugarEjecucion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucion.UsuarioCrea;
                            vLugarEjecucion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucion.FechaCrea;
                            vLugarEjecucion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucion.UsuarioModifica;
                            vLugarEjecucion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucion.FechaModifica;
                            vListaLugarEjecucion.Add(vLugarEjecucion);
                        }
                        return vListaLugarEjecucion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad LugarEjecucion
        /// </summary>
        /// <param name="IdContratoLugarEjecucion"></param>
        /// <returns></returns>
        public List<string> ConsultarLugarEjecucion(int IdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_LugarEjecuci�n_ConsultarNombres"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, @IdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<string> vListaLugarEjecucion = new List<string>();

                        while (vDataReaderResults.Read())
                        {
                            string nombre = vDataReaderResults["LugarUbicacion"] != DBNull.Value ? vDataReaderResults["LugarUbicacion"].ToString() : string.Empty;
                            vListaLugarEjecucion.Add(nombre);
                        }
                        return vListaLugarEjecucion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
