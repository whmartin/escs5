﻿using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess
{
    public class HistoricoSolicitudesEliminadasDAL : GeneralDAL
    {
        public HistoricoSolicitudesEliminadasDAL()
        {

        }

      

        public HistoricoSolicitudesEliminadas ConsultarSolicitudesEliminacion(int IDCosModContractual)
        {
            try
            {

                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarModificacionEliminacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDCosModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        HistoricoSolicitudesEliminadas vHistoricoSolicitudesHistorico = new HistoricoSolicitudesEliminadas();

                        while (vDataReaderResults.Read())
                        {

                            vHistoricoSolicitudesHistorico.IdConsModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vHistoricoSolicitudesHistorico.IdConsModContractual;
                            vHistoricoSolicitudesHistorico.IdConsModContractualesEstado = vDataReaderResults["IdConsModContractualesEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractualesEstado"].ToString()) : vHistoricoSolicitudesHistorico.IdConsModContractualesEstado;
                            vHistoricoSolicitudesHistorico.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vHistoricoSolicitudesHistorico.IdContrato;
                            vHistoricoSolicitudesHistorico.JustificacionEliminacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vHistoricoSolicitudesHistorico.JustificacionEliminacion;
                            vHistoricoSolicitudesHistorico.TipoModificacion = vDataReaderResults["TipoModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacion"].ToString()) : vHistoricoSolicitudesHistorico.TipoModificacion;
                            vHistoricoSolicitudesHistorico.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vHistoricoSolicitudesHistorico.NumeroContrato;
                            vHistoricoSolicitudesHistorico.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vHistoricoSolicitudesHistorico.NombreRegional;
                           
                        }

                        return vHistoricoSolicitudesHistorico;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }     


        public int InsertarSolModContractualHistorico(HistoricoSolicitudesEliminadas psModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_InsertarSolModContractualHistorico"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoSolicitudEliminada", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@TipoModificacion", DbType.String, psModContractual.TipoModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionEliminacion", DbType.String, psModContractual.JustificacionEliminacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, psModContractual.IdConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRegional", DbType.String, psModContractual.NombreRegional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, psModContractual.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, psModContractual.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsModContractualesEstado", DbType.Int32, psModContractual.IdConsModContractualesEstado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, psModContractual.NumeroContrato);



                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    psModContractual.IdHistoricoSolicitudEliminada = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoSolicitudEliminada").ToString());
                    GenerarLogAuditoria(psModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoSolicitudesEliminadas> ConsultarSolicitudesElimindasHistorico()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_HistoricoSolicitudesEliminadas_ConsultaSolicitudes"))
                {

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoSolicitudesEliminadas> vHistoricoSolicitudesHistoricoList = new List<HistoricoSolicitudesEliminadas>();

                        while (vDataReaderResults.Read())
                        {
                            HistoricoSolicitudesEliminadas vHistoricoSolicitudesHistorico = new HistoricoSolicitudesEliminadas();
                            vHistoricoSolicitudesHistorico.IdConsModContractual = vDataReaderResults["IdCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCosModContractual"].ToString()) : vHistoricoSolicitudesHistorico.IdConsModContractual;
                            vHistoricoSolicitudesHistorico.IdConsModContractualesEstado = vDataReaderResults["IdCosModContractualEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCosModContractualEstado"].ToString()) : vHistoricoSolicitudesHistorico.IdConsModContractualesEstado;
                            vHistoricoSolicitudesHistorico.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoSolicitudesHistorico.UsuarioCrea;
                            vHistoricoSolicitudesHistorico.FechaEliminacion = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoSolicitudesHistorico.FechaEliminacion;
                            vHistoricoSolicitudesHistorico.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vHistoricoSolicitudesHistorico.NombreRegional;
                            vHistoricoSolicitudesHistorico.JustificacionEliminacion = vDataReaderResults["JustificacionEliminacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionEliminacion"].ToString()) : vHistoricoSolicitudesHistorico.JustificacionEliminacion;
                            vHistoricoSolicitudesHistorico.TipoModificacion = vDataReaderResults["TipoModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacion"].ToString()) : vHistoricoSolicitudesHistorico.TipoModificacion;
                            vHistoricoSolicitudesHistorico.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vHistoricoSolicitudesHistorico.NumeroContrato;
                            vHistoricoSolicitudesHistorico.NumeroSolicitud = vDataReaderResults["IdCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCosModContractual"].ToString()) : vHistoricoSolicitudesHistorico.NumeroSolicitud;
                            vHistoricoSolicitudesHistorico.NombreDependenciaSolicitante = vDataReaderResults["NombreDependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDependenciaSolicitante"].ToString()) : vHistoricoSolicitudesHistorico.NombreDependenciaSolicitante;

                            vHistoricoSolicitudesHistoricoList.Add(vHistoricoSolicitudesHistorico);
                        }
                        return vHistoricoSolicitudesHistoricoList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
    }
}
