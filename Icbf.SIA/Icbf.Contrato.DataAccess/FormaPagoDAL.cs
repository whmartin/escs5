using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad FormaPago
    /// </summary>
    public class FormaPagoDAL : GeneralDAL
    {
        public FormaPagoDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la informaci�n de la entidad Forma de pago</param>
        public int InsertarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FormaPago_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdFormaPago", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pFormaPago.IdProveedores);
                    vDataBase.AddInParameter(vDbCommand, "@IdMedioPago", DbType.Int32, pFormaPago.IdMedioPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidadFinanciera", DbType.Int32, pFormaPago.IdEntidadFinanciera);
                    vDataBase.AddInParameter(vDbCommand, "@TipoCuentaBancaria", DbType.Int32, pFormaPago.TipoCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCuentaBancaria", DbType.String, pFormaPago.NumeroCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFormaPago.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFormaPago.IdFormaPago = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdFormaPago").ToString());
                    GenerarLogAuditoria(pFormaPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la informaci�n de la entidad Forma de pago</param>
        public int ModificarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FormaPago_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaPago", DbType.Int32, pFormaPago.IdFormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pFormaPago.IdProveedores);
                    vDataBase.AddInParameter(vDbCommand, "@IdMedioPago", DbType.Int32, pFormaPago.IdMedioPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidadFinanciera", DbType.Int32, pFormaPago.IdEntidadFinanciera);
                    vDataBase.AddInParameter(vDbCommand, "@TipoCuentaBancaria", DbType.Int32, pFormaPago.TipoCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCuentaBancaria", DbType.String, pFormaPago.NumeroCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFormaPago.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFormaPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la informaci�n de la entidad Forma de pago</param>
        public int EliminarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FormaPago_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaPago", DbType.Int32, pFormaPago.IdFormaPago);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFormaPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// M�todo de consulta por id para la entidad FormaPago
        /// </summary>
        /// <param name="pIdFormaPago">Valor entero con el Id de la forma de pago</param>
        public FormaPago ConsultarFormaPago(int pIdFormaPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FormaPago_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaPago", DbType.Int32, pIdFormaPago);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        FormaPago vFormaPago = new FormaPago();
                        while (vDataReaderResults.Read())
                        {
                            vFormaPago.IdFormaPago = vDataReaderResults["IdFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaPago"].ToString()) : vFormaPago.IdFormaPago;
                            vFormaPago.IdProveedores = vDataReaderResults["IdProveedores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedores"].ToString()) : vFormaPago.IdProveedores;
                            vFormaPago.IdMedioPago = vDataReaderResults["IdMedioPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMedioPago"].ToString()) : vFormaPago.IdMedioPago;
                            vFormaPago.IdEntidadFinanciera = vDataReaderResults["IdEntidadFinanciera"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidadFinanciera"].ToString()) : vFormaPago.IdEntidadFinanciera;
                            vFormaPago.TipoCuentaBancaria = vDataReaderResults["TipoCuentaBancaria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoCuentaBancaria"].ToString()) : vFormaPago.TipoCuentaBancaria;
                            vFormaPago.NumeroCuentaBancaria = vDataReaderResults["NumeroCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCuentaBancaria"].ToString()) : vFormaPago.NumeroCuentaBancaria;
                            vFormaPago.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFormaPago.UsuarioCrea;
                            vFormaPago.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFormaPago.FechaCrea;
                            vFormaPago.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFormaPago.UsuarioModifica;
                            vFormaPago.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFormaPago.FechaModifica;
                        }
                        return vFormaPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad FormaPago
        /// </summary>
        /// <param name="pIdMedioPago">Valor entero con el Id del medio de pago</param>
        /// <param name="pIdEntidadFinanciera">Valor entero con el id de entidad financiera</param>
        /// <param name="pTipoCuentaBancaria">Valor entero con el Id del tipo de cuenta bancaria</param>
        /// <param name="pNumeroCuentaBancaria">Cadena de texto con el n�mero de la cuenta bancaria</param>
        public List<FormaPago> ConsultarFormaPagos(int? pIdProveedores, int? pIdMedioPago, int? pIdEntidadFinanciera, int? pTipoCuentaBancaria, String pNumeroCuentaBancaria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FormaPagos_Consultar"))
                {
                    if (pIdProveedores != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pIdProveedores);
                    if (pIdMedioPago != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdMedioPago", DbType.Int32, pIdMedioPago);
                    if (pIdEntidadFinanciera != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidadFinanciera", DbType.Int32, pIdEntidadFinanciera);
                    if (pTipoCuentaBancaria != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoCuentaBancaria", DbType.Int32, pTipoCuentaBancaria);
                    if (pNumeroCuentaBancaria != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroCuentaBancaria", DbType.String, pNumeroCuentaBancaria);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FormaPago> vListaFormaPago = new List<FormaPago>();
                        while (vDataReaderResults.Read())
                        {
                            FormaPago vFormaPago = new FormaPago();
                            vFormaPago.IdFormaPago = vDataReaderResults["IdFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaPago"].ToString()) : vFormaPago.IdFormaPago;
                            vFormaPago.IdProveedores = vDataReaderResults["IdProveedores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedores"].ToString()) : vFormaPago.IdProveedores;
                            vFormaPago.IdMedioPago = vDataReaderResults["IdMedioPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMedioPago"].ToString()) : vFormaPago.IdMedioPago;
                            vFormaPago.IdEntidadFinanciera = vDataReaderResults["IdEntidadFinanciera"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidadFinanciera"].ToString()) : vFormaPago.IdEntidadFinanciera;
                            vFormaPago.TipoCuentaBancaria = vDataReaderResults["TipoCuentaBancaria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoCuentaBancaria"].ToString()) : vFormaPago.TipoCuentaBancaria;
                            vFormaPago.NumeroCuentaBancaria = vDataReaderResults["NumeroCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCuentaBancaria"].ToString()) : vFormaPago.NumeroCuentaBancaria;
                            vFormaPago.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFormaPago.UsuarioCrea;
                            vFormaPago.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFormaPago.FechaCrea;
                            vFormaPago.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFormaPago.UsuarioModifica;
                            vFormaPago.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFormaPago.FechaModifica;
                            vFormaPago.NombreEntidadFinanciera = vDataReaderResults["NombreEntFin"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntFin"].ToString()) : vFormaPago.NombreEntidadFinanciera;
                            vFormaPago.NombreTipoCuenta = vDataReaderResults["NombreTipoCta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoCta"].ToString()) : vFormaPago.NombreTipoCuenta;
                            vFormaPago.NombreTipoMedioPago = vDataReaderResults["DescTipoMedioPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescTipoMedioPago"].ToString()) : vFormaPago.NombreTipoMedioPago;
                            vListaFormaPago.Add(vFormaPago);
                        }
                        return vListaFormaPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Permite consultar el tipo de medio de pago a partir de los 
        /// filtros dados
        /// 7-Feb-2014
        /// </summary>
        /// <param name="pCodMedioPago">Cadena de texto con el codigo medio de pago</param>
        /// <param name="pDescTipoMedioPago">Cadena de texto con la descripcion del tipo medio de pago</param>
        /// <param name="pEstado">Booleano que indica el estado</param>
        /// <returns>Lista con las instancias que contiene los tipos de medio de pago, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<TipoMedioPago> ConsultarTipoMedioPagos(String pCodMedioPago, String pDescTipoMedioPago, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_NMF_Ppto_TipoMedioPagos_Consultar"))
                {
                    if (pCodMedioPago != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodMedioPago", DbType.String, pCodMedioPago);
                    if (pDescTipoMedioPago != null)
                        vDataBase.AddInParameter(vDbCommand, "@DescTipoMedioPago", DbType.String, pDescTipoMedioPago);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoMedioPago> vListaTipoMedioPago = new List<TipoMedioPago>();
                        while (vDataReaderResults.Read())
                        {
                            TipoMedioPago vTipoMedioPago = new TipoMedioPago();
                            vTipoMedioPago.IdTipoMedioPago = vDataReaderResults["IdTipoMedioPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoMedioPago"].ToString()) : vTipoMedioPago.IdTipoMedioPago;
                            vTipoMedioPago.CodMedioPago = vDataReaderResults["CodMedioPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodMedioPago"].ToString()) : vTipoMedioPago.CodMedioPago;
                            vTipoMedioPago.DescTipoMedioPago = vDataReaderResults["DescTipoMedioPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescTipoMedioPago"].ToString()) : vTipoMedioPago.DescTipoMedioPago;
                            vTipoMedioPago.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoMedioPago.Estado;
                            vTipoMedioPago.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoMedioPago.UsuarioCrea;
                            vTipoMedioPago.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoMedioPago.FechaCrea;
                            vTipoMedioPago.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoMedioPago.UsuarioModifica;
                            vTipoMedioPago.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoMedioPago.FechaModifica;
                            vListaTipoMedioPago.Add(vTipoMedioPago);
                        }
                        return vListaTipoMedioPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consulta los tipos de cuenta de entidad financiera segun los filtros dados
        /// 30-01-2014
        /// </summary>
        /// <param name="pCodTipoCta">Cadena de texto con el codigo del tipo de cuenta</param>
        /// <param name="pDescripcion">Cadena de texto con la descripcion del tipo de cuenta</param>
        /// <param name="pEstado">Booleano que indica el estado</param>
        /// <returns>Lista de instancias que contiene los tipo c�digo de retenci�n</returns>
        public List<TipoCuentaEntFin> ConsultarTipoCuentaEntFins(String pCodTipoCta, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_NMF_BaseSIF_TipoCuentaEntFins_Consultar"))
                {
                    if (pCodTipoCta != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodTipoCta", DbType.String, pCodTipoCta);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCuentaEntFin> vListaTipoCuentaEntFin = new List<TipoCuentaEntFin>();
                        while (vDataReaderResults.Read())
                        {
                            TipoCuentaEntFin vTipoCuentaEntFin = new TipoCuentaEntFin();
                            vTipoCuentaEntFin.IdTipoCta = vDataReaderResults["IdTipoCta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCta"].ToString()) : vTipoCuentaEntFin.IdTipoCta;
                            vTipoCuentaEntFin.CodTipoCta = vDataReaderResults["CodTipoCta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodTipoCta"].ToString()) : vTipoCuentaEntFin.CodTipoCta;
                            vTipoCuentaEntFin.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoCuentaEntFin.Descripcion;
                            vTipoCuentaEntFin.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoCuentaEntFin.Estado;
                            vTipoCuentaEntFin.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoCuentaEntFin.UsuarioCrea;
                            vTipoCuentaEntFin.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoCuentaEntFin.FechaCrea;
                            vTipoCuentaEntFin.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoCuentaEntFin.UsuarioModifica;
                            vTipoCuentaEntFin.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoCuentaEntFin.FechaModifica;
                            vListaTipoCuentaEntFin.Add(vTipoCuentaEntFin);
                        }
                        return vListaTipoCuentaEntFin;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar Entidad Financiera 
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pCodigoEntFin">Cadena de texto con el c�digo de la entidad financiera</param>
        /// <param name="pNombreEntFin">Cadena de texto con el nombre de la entidad financiera</param>
        /// <param name="pEstado">Valor Booleano con el estado de la entidad financiera</param>
        /// <param name="pEsPagoPorOrdenDePago">Valor Booleano con el estado pago por orden de pago</param>
        /// <param name="pEsPagoPorOrdenBancaria">Valor Booleano con el estado pago por orden bancaria</param>
        /// <returns>Lista con la Instancia que contiene la informaci�n de la entidad financiera</returns>
        public List<EntidadFinanciera> ConsultarEntidadFinancieras(String pCodigoEntFin, String pNombreEntFin, Boolean? pEstado, Boolean? pEsPagoPorOrdenDePago, Boolean? pEsPagoPorOrdenBancaria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_NMF_BaseSIF_EntidadFinancieras_Consultar"))
                {
                    if (pCodigoEntFin != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoEntFin", DbType.String, pCodigoEntFin);
                    if (pNombreEntFin != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreEntFin", DbType.String, pNombreEntFin);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);

                    if (pEsPagoPorOrdenBancaria != null)
                        vDataBase.AddInParameter(vDbCommand, "@EsPagoPorOrdenDePago", DbType.Boolean, pEsPagoPorOrdenBancaria);

                    if (pEsPagoPorOrdenDePago != null)
                        vDataBase.AddInParameter(vDbCommand, "@EsPagoPorOrdenBancaria", DbType.Boolean, pEsPagoPorOrdenDePago);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EntidadFinanciera> vListaEntidadFinanciera = new List<EntidadFinanciera>();
                        while (vDataReaderResults.Read())
                        {
                            EntidadFinanciera vEntidadFinanciera = new EntidadFinanciera();
                            vEntidadFinanciera.IdEntidadFinanciera = vDataReaderResults["IdEntidadFinanciera"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidadFinanciera"].ToString()) : vEntidadFinanciera.IdEntidadFinanciera;
                            vEntidadFinanciera.CodigoEntFin = vDataReaderResults["CodigoEntFin"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEntFin"].ToString()) : vEntidadFinanciera.CodigoEntFin;
                            vEntidadFinanciera.NombreEntFin = vDataReaderResults["NombreEntFin"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntFin"].ToString()) : vEntidadFinanciera.NombreEntFin;
                            vEntidadFinanciera.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEntidadFinanciera.Estado;
                            vEntidadFinanciera.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntidadFinanciera.UsuarioCrea;
                            vEntidadFinanciera.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadFinanciera.FechaCrea;
                            vEntidadFinanciera.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntidadFinanciera.UsuarioModifica;
                            vEntidadFinanciera.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntidadFinanciera.FechaModifica;
                            vEntidadFinanciera.EsPagoPorOrdenBancaria = vDataReaderResults["EsPagoPorOrdenBancaria"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPagoPorOrdenBancaria"].ToString()) : vEntidadFinanciera.EsPagoPorOrdenBancaria;
                            vEntidadFinanciera.EsPagoPorOrdenDePago = vDataReaderResults["EsPagoPorOrdenDePago"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPagoPorOrdenDePago"].ToString()) : vEntidadFinanciera.EsPagoPorOrdenDePago;
                            vListaEntidadFinanciera.Add(vEntidadFinanciera);
                        }
                        return vListaEntidadFinanciera;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Gonet
        /// M�todo de consulta por id para la Tercero
        /// 7-Feb-2014
        /// </summary>
        /// <param name="pIdTercero">Entero con el identificador del tercero</param>
        /// <returns>Instancia que contiene la informaci�n del tercero recuperado 
        /// de la base de datos</returns>
        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_NMF_Oferente_Tercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;

                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;

                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"].ToString()) : vTercero.DigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTercero.NombreTipoPersona;
                            vTercero.NombreListaTipoDocumento = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTercero.NombreListaTipoDocumento;

                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros si el usuario que cre� el registro sea el mismo que el que ingresa al sistema
        /// </summary>
        /// <param name="pIdMedioPago">Valor entero con el Id del medio de pago</param>
        /// <param name="pIdEntidadFinanciera">Valor entero con el id de entidad financiera</param>
        /// <param name="pTipoCuentaBancaria">Valor entero con el Id del tipo de cuenta bancaria</param>
        /// <param name="pNumeroCuentaBancaria">Cadena de texto con el n�mero de la cuenta bancaria</param>
        /// <param name="pUsuarioCrea">Cadena de texto con el nombre del usuario creaci�n</param>
        public bool ConsultarFormaPagosUsuarioCrea(int? pIdProveedores, int? pIdMedioPago, int? pIdEntidadFinanciera, int? pTipoCuentaBancaria, String pNumeroCuentaBancaria, string pUsuarioCrea)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_FormaPagosUsuarioCrea_Consultar"))
                {
                    if (pIdProveedores != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pIdProveedores);
                    if (pIdMedioPago != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdMedioPago", DbType.Int32, pIdMedioPago);
                    if (pIdEntidadFinanciera != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidadFinanciera", DbType.Int32, pIdEntidadFinanciera);
                    if (pTipoCuentaBancaria != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoCuentaBancaria", DbType.Int32, pTipoCuentaBancaria);
                    if (pNumeroCuentaBancaria != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroCuentaBancaria", DbType.String, pNumeroCuentaBancaria);
                    if (pUsuarioCrea != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUsuarioCrea);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        
                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros si el usuario que cre� el registro sea el mismo que el que ingresa al sistema
        /// </summary>
        /// <param name="pUsuarioCrea">Cadena de texto con el nombre del usuario creaci�n</param>
        public bool ConsultarContratoUsuarioCrea(string pUsuarioCrea, int pidContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratoUsuarioCrea_Consultar"))
                {
                    if (pUsuarioCrea != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUsuarioCrea);
                    if (pidContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pidContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de modificaci�n para la entidad FormaPago por contrato
        /// </summary>
        /// /// <param name="pIdContrato">Instancia con la informaci�n del id del contrato</param>
        /// <param name="pIdFormaPago">Instancia con la informaci�n del id de la forma de pago</param>
        public int ModificarFormaPagoContrato(int pIdProveedoresContratos, int pIdFormaPago, string pUsuarioModifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ContratoFormapago_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedoresContratos", DbType.Int32, pIdProveedoresContratos);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaPago", DbType.Int32, pIdFormaPago);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta un(a) Contratos por el id de la forma de pago retorna verdadero si la forma de pago esta asociada a un contrato
        /// </summary>
        /// <param name="pIdFormapago">Valor Entero con el Id de la forma de pago</param>
        /// /// <param name="pIdProveedoresContratos">Valor Entero con el Id ProveedoresContratos</param>
        public bool ConsultarFormaPagoContrato(int pIdFormapago, int pIdProveedoresContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Contratos_FormaPago_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFormapago", DbType.Int32, pIdFormapago);
                    vDataBase.AddInParameter(vDbCommand, "@IDProveedoresContratos", DbType.Int32, pIdProveedoresContratos);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
