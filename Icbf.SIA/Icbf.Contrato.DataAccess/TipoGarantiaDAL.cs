using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Tipo Garant�a
    /// </summary>
    public class TipoGarantiaDAL : GeneralDAL
    {
        public TipoGarantiaDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int InsertarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoGarantia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoGarantia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoGarantia", DbType.String, pTipoGarantia.NombreTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoGarantia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoGarantia.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoGarantia.IdTipoGarantia = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoGarantia").ToString());
                    GenerarLogAuditoria(pTipoGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int ModificarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoGarantia_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoGarantia", DbType.Int32, pTipoGarantia.IdTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoGarantia", DbType.String, pTipoGarantia.NombreTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoGarantia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoGarantia.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int EliminarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoGarantia_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoGarantia", DbType.Int32, pTipoGarantia.IdTipoGarantia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad TipoGarantia
        /// </summary>
        /// <param name="pIdTipoGarantia"></param>
        /// <returns></returns>
        public TipoGarantia ConsultarTipoGarantia(int pIdTipoGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoGarantia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoGarantia", DbType.Int32, pIdTipoGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoGarantia vTipoGarantia = new TipoGarantia();
                        while (vDataReaderResults.Read())
                        {
                            vTipoGarantia.IdTipoGarantia = vDataReaderResults["IdTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoGarantia"].ToString()) : vTipoGarantia.IdTipoGarantia;
                            vTipoGarantia.NombreTipoGarantia = vDataReaderResults["NombreTipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoGarantia"].ToString()) : vTipoGarantia.NombreTipoGarantia;
                            vTipoGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoGarantia.Estado;
                            vTipoGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoGarantia.UsuarioCrea;
                            vTipoGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoGarantia.FechaCrea;
                            vTipoGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoGarantia.UsuarioModifica;
                            vTipoGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoGarantia.FechaModifica;
                        }
                        return vTipoGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoGarantia
        /// </summary>
        /// <param name="pNombreTipoGarantia"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoGarantia> ConsultarTipoGarantias(String pNombreTipoGarantia, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoGarantias_Consultar"))
                {
                    if(pNombreTipoGarantia != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTipoGarantia", DbType.String, pNombreTipoGarantia);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoGarantia> vListaTipoGarantia = new List<TipoGarantia>();
                        while (vDataReaderResults.Read())
                        {
                            TipoGarantia vTipoGarantia = new TipoGarantia();
                            vTipoGarantia.IdTipoGarantia = vDataReaderResults["IdTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoGarantia"].ToString()) : vTipoGarantia.IdTipoGarantia;
                            vTipoGarantia.NombreTipoGarantia = vDataReaderResults["NombreTipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoGarantia"].ToString()) : vTipoGarantia.NombreTipoGarantia;
                            vTipoGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoGarantia.Estado;
                            if (vTipoGarantia.Estado)
                            {
                                vTipoGarantia.EstadoString = "Activo";
                            } else
                            {
                                vTipoGarantia.EstadoString = "Inactivo";
                            }
                            vTipoGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoGarantia.UsuarioCrea;
                            vTipoGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoGarantia.FechaCrea;
                            vTipoGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoGarantia.UsuarioModifica;
                            vTipoGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoGarantia.FechaModifica;
                            vListaTipoGarantia.Add(vTipoGarantia);
                        }
                        return vListaTipoGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
