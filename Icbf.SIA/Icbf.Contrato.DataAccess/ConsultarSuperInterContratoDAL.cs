﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad ConsultarSuperInterContrato
    /// </summary>
    public class ConsultarSuperInterContratoDAL : GeneralDAL
    {
        public ConsultarSuperInterContratoDAL()
        {
        }
        ///// <summary>
        ///// Método de inserción para la entidad ConsultarSuperInterContrato
        ///// </summary>
        ///// <param name="pConsultarSuperInterContrato"></param>
        //public int InsertarConsultarSuperInterContrato(ConsultarSuperInterContrato pConsultarSuperInterContrato)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsultarSuperInterContrato_Insertar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddOutParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, 18);
        //            vDataBase.AddInParameter(vDbCommand, "@IDSupervisorInterventor", DbType.Int32, pConsultarSuperInterContrato.IDSupervisorInterventor);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pConsultarSuperInterContrato.NumeroContrato);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pConsultarSuperInterContrato.NumeroIdentificacion);
        //            vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, pConsultarSuperInterContrato.TipoIdentificacion);
        //            vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pConsultarSuperInterContrato.RazonSocial);
        //            vDataBase.AddInParameter(vDbCommand, "@NombreSuperInterv", DbType.String, pConsultarSuperInterContrato.NombreSuperInterv);
        //            vDataBase.AddInParameter(vDbCommand, "@NumIdentifDirInterventoria", DbType.String, pConsultarSuperInterContrato.NumIdentifDirInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@NombreDirInterventoria", DbType.String, pConsultarSuperInterContrato.NombreDirInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@TipoContrato", DbType.String, pConsultarSuperInterContrato.TipoContrato);
        //            vDataBase.AddInParameter(vDbCommand, "@RegionalContrato", DbType.String, pConsultarSuperInterContrato.RegionalContrato);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pConsultarSuperInterContrato.FechaSuscripcion);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaTerminacion", DbType.DateTime, pConsultarSuperInterContrato.FechaTerminacion);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaInicioSuperInterv", DbType.DateTime, pConsultarSuperInterContrato.FechaInicioSuperInterv);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaFinalSuperInterv", DbType.DateTime, pConsultarSuperInterContrato.FechaFinalSuperInterv);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaModSuperInterv", DbType.DateTime, pConsultarSuperInterContrato.FechaModSuperInterv);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroContratoInterventoria", DbType.String, pConsultarSuperInterContrato.NumeroContratoInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@TelefonoInterventoria", DbType.String, pConsultarSuperInterContrato.TelefonoInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@TelCelularInterventoria", DbType.String, pConsultarSuperInterContrato.TelCelularInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pConsultarSuperInterContrato.UsuarioCrea);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            pConsultarSuperInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDSupervisorIntervContrato").ToString());
        //            GenerarLogAuditoria(pConsultarSuperInterContrato, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de modificación para la entidad ConsultarSuperInterContrato
        ///// </summary>
        ///// <param name="pConsultarSuperInterContrato"></param>
        //public int ModificarConsultarSuperInterContrato(ConsultarSuperInterContrato pConsultarSuperInterContrato)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsultarSuperInterContrato_Modificar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pConsultarSuperInterContrato.IDSupervisorIntervContrato);
        //            vDataBase.AddInParameter(vDbCommand, "@IDSupervisorInterventor", DbType.Int32, pConsultarSuperInterContrato.IDSupervisorInterventor);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pConsultarSuperInterContrato.NumeroContrato);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pConsultarSuperInterContrato.NumeroIdentificacion);
        //            vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, pConsultarSuperInterContrato.TipoIdentificacion);
        //            vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pConsultarSuperInterContrato.RazonSocial);
        //            vDataBase.AddInParameter(vDbCommand, "@NombreSuperInterv", DbType.String, pConsultarSuperInterContrato.NombreSuperInterv);
        //            vDataBase.AddInParameter(vDbCommand, "@NumIdentifDirInterventoria", DbType.String, pConsultarSuperInterContrato.NumIdentifDirInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@NombreDirInterventoria", DbType.String, pConsultarSuperInterContrato.NombreDirInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@TipoContrato", DbType.String, pConsultarSuperInterContrato.TipoContrato);
        //            vDataBase.AddInParameter(vDbCommand, "@RegionalContrato", DbType.String, pConsultarSuperInterContrato.RegionalContrato);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, pConsultarSuperInterContrato.FechaSuscripcion);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaTerminacion", DbType.DateTime, pConsultarSuperInterContrato.FechaTerminacion);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaInicioSuperInterv", DbType.DateTime, pConsultarSuperInterContrato.FechaInicioSuperInterv);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaFinalSuperInterv", DbType.DateTime, pConsultarSuperInterContrato.FechaFinalSuperInterv);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaModSuperInterv", DbType.DateTime, pConsultarSuperInterContrato.FechaModSuperInterv);
        //            vDataBase.AddInParameter(vDbCommand, "@NumeroContratoInterventoria", DbType.String, pConsultarSuperInterContrato.NumeroContratoInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@TelefonoInterventoria", DbType.String, pConsultarSuperInterContrato.TelefonoInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@TelCelularInterventoria", DbType.String, pConsultarSuperInterContrato.TelCelularInterventoria);
        //            vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pConsultarSuperInterContrato.UsuarioModifica);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            GenerarLogAuditoria(pConsultarSuperInterContrato, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de eliminación para la entidad ConsultarSuperInterContrato
        ///// </summary>
        ///// <param name="pConsultarSuperInterContrato"></param>
        //public int EliminarConsultarSuperInterContrato(ConsultarSuperInterContrato pConsultarSuperInterContrato)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsultarSuperInterContrato_Eliminar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pConsultarSuperInterContrato.IDSupervisorIntervContrato);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            GenerarLogAuditoria(pConsultarSuperInterContrato, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}


        /// <summary>
        /// Método de consulta por id para la entidad ConsultarSuperInterContrato
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        public ConsultarSuperInterContrato ConsultarConsultarSuperInterContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsultarSuperInterContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pIDSupervisorIntervContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ConsultarSuperInterContrato vConsultarSuperInterContrato = new ConsultarSuperInterContrato();
                        while (vDataReaderResults.Read())
                        {
                            vConsultarSuperInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vConsultarSuperInterContrato.IDSupervisorIntervContrato;

                            vConsultarSuperInterContrato.SupervisorInterventor = vDataReaderResults["SupervisorInterventor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorInterventor"].ToString()) : vConsultarSuperInterContrato.SupervisorInterventor;
                            vConsultarSuperInterContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsultarSuperInterContrato.NumeroContrato;
                            vConsultarSuperInterContrato.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vConsultarSuperInterContrato.TipoContrato;
                            vConsultarSuperInterContrato.RegionalContrato = vDataReaderResults["RegionalContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegionalContrato"].ToString()) : vConsultarSuperInterContrato.RegionalContrato;
                            vConsultarSuperInterContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"].ToString()) : vConsultarSuperInterContrato.FechaSuscripcion;
                            vConsultarSuperInterContrato.FechaTerminacion = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vConsultarSuperInterContrato.FechaTerminacion;
                            vConsultarSuperInterContrato.FechaInicioSuperInterv = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vConsultarSuperInterContrato.FechaInicioSuperInterv;
                            vConsultarSuperInterContrato.FechaFinalSuperInterv = vDataReaderResults["FechaFinalizacionSuperInterv"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionSuperInterv"].ToString()) : vConsultarSuperInterContrato.FechaFinalSuperInterv;
                            vConsultarSuperInterContrato.FechaModSuperInterv = vDataReaderResults["FechaModifSuperInter"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifSuperInter"].ToString()) : vConsultarSuperInterContrato.FechaModSuperInterv;
                            vConsultarSuperInterContrato.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vConsultarSuperInterContrato.NumeroIdentificacion;
                            vConsultarSuperInterContrato.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vConsultarSuperInterContrato.TipoIdentificacion;
                            vConsultarSuperInterContrato.NombreRazonSocialSuperInterv = vDataReaderResults["NombreRazonsocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRazonsocial"].ToString()) : vConsultarSuperInterContrato.NombreRazonSocialSuperInterv;
                            vConsultarSuperInterContrato.NumeroContratoInterventoria = vDataReaderResults["NumeroContratoInterventoria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContratoInterventoria"].ToString()) : vConsultarSuperInterContrato.NumeroContratoInterventoria;

                            vConsultarSuperInterContrato.CodTipoSuperInter = vDataReaderResults["CodigoSuperInter"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSuperInter"].ToString()) : vConsultarSuperInterContrato.CodTipoSuperInter;

                            if (vConsultarSuperInterContrato.CodTipoSuperInter.Equals("02"))
                            {
                                vConsultarSuperInterContrato.NumIdentifDirInterventoria = vDataReaderResults["NumeroIdentificacionDirector"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionDirector"].ToString()) : vConsultarSuperInterContrato.NumIdentifDirInterventoria;
                                vConsultarSuperInterContrato.NombreDirInterventoria = vDataReaderResults["NombreDirInterventoria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDirInterventoria"].ToString()) : vConsultarSuperInterContrato.NombreDirInterventoria;
                                vConsultarSuperInterContrato.TelefonoInterventoria = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vConsultarSuperInterContrato.TelefonoInterventoria;
                                vConsultarSuperInterContrato.TelCelularInterventoria = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vConsultarSuperInterContrato.TelCelularInterventoria;
                            }

                            vConsultarSuperInterContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vConsultarSuperInterContrato.EstadoContrato;
                            vConsultarSuperInterContrato.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoDelContrato"].ToString()) : vConsultarSuperInterContrato.ObjetoContrato;
                            vConsultarSuperInterContrato.Inactivo = vDataReaderResults["InactivoSuperInter"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["InactivoSuperInter"].ToString()) : vConsultarSuperInterContrato.Inactivo;

                            vConsultarSuperInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsultarSuperInterContrato.UsuarioCrea;
                            vConsultarSuperInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsultarSuperInterContrato.FechaCrea;
                            vConsultarSuperInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsultarSuperInterContrato.UsuarioModifica;
                            vConsultarSuperInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsultarSuperInterContrato.FechaModifica;
                        }
                        return vConsultarSuperInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ConsultarSuperInterContrato
        /// </summary>
        /// <param name="pIDSupervisorInterventor"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pTipoIdentificacion"></param>
        /// <param name="pRazonSocial"></param>
        /// <param name="pNombreSuperInterv"></param>
        /// <param name="pNumIdentifDirInterventoria"></param>
        /// <param name="pNombreDirInterventoria"></param>
        /// <param name="pTipoContrato"></param>
        /// <param name="pRegionalContrato"></param>
        /// <param name="pFechaSuscripcion"></param>
        /// <param name="pFechaTerminacion"></param>
        /// <param name="pFechaInicioSuperInterv"></param>
        /// <param name="pFechaFinalSuperInterv"></param>
        /// <param name="pFechaModSuperInterv"></param>
        /// <param name="pNumeroContratoInterventoria"></param>
        /// <param name="pTelefonoInterventoria"></param>
        /// <param name="pTelCelularInterventoria"></param>
        public List<ConsultarSuperInterContrato> ConsultarConsultarSuperInterContratos(String pCodSupervisorInterventor, String pNumeroContrato, String pNumeroIdentificacion, String pRazonSocial, String pNombreSuperInterv, String pNumIdentifDirInterventoria, String pNombreDirInterventoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ConsultarSuperInterContratos_Consultar"))
                {
                    if (pCodSupervisorInterventor != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodSupervisorInterventor", DbType.String, pCodSupervisorInterventor);
                    if (pNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);

                    if (pRazonSocial != null)
                        vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pRazonSocial);
                    if (pNombreSuperInterv != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreSuperInterv", DbType.String, pNombreSuperInterv);
                    if (pNumIdentifDirInterventoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumIdentifDirInterventoria", DbType.String, pNumIdentifDirInterventoria);
                    if (pNombreDirInterventoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreDirInterventoria", DbType.String, pNombreDirInterventoria);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConsultarSuperInterContrato> vListaConsultarSuperInterContrato = new List<ConsultarSuperInterContrato>();
                        try
                        {
                            while (vDataReaderResults.Read())
                            {
                                ConsultarSuperInterContrato vConsultarSuperInterContrato = new ConsultarSuperInterContrato();
                                vConsultarSuperInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vConsultarSuperInterContrato.IDSupervisorIntervContrato;

                                vConsultarSuperInterContrato.SupervisorInterventor = vDataReaderResults["SupervisorInterventor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SupervisorInterventor"].ToString()) : vConsultarSuperInterContrato.SupervisorInterventor;
                                vConsultarSuperInterContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsultarSuperInterContrato.NumeroContrato;
                                vConsultarSuperInterContrato.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vConsultarSuperInterContrato.TipoContrato;
                                vConsultarSuperInterContrato.RegionalContrato = vDataReaderResults["RegionalContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegionalContrato"].ToString()) : vConsultarSuperInterContrato.RegionalContrato;
                                vConsultarSuperInterContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"].ToString()) : vConsultarSuperInterContrato.FechaSuscripcion;
                                vConsultarSuperInterContrato.FechaTerminacion = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vConsultarSuperInterContrato.FechaTerminacion;
                                vConsultarSuperInterContrato.FechaInicioSuperInterv = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vConsultarSuperInterContrato.FechaInicioSuperInterv;
                                vConsultarSuperInterContrato.FechaFinalSuperInterv = vDataReaderResults["FechaFinalizacionSuperInterv"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionSuperInterv"].ToString()) : vConsultarSuperInterContrato.FechaFinalSuperInterv;
                                vConsultarSuperInterContrato.FechaModSuperInterv = vDataReaderResults["FechaModifSuperInter"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifSuperInter"].ToString()) : vConsultarSuperInterContrato.FechaModSuperInterv;
                                vConsultarSuperInterContrato.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vConsultarSuperInterContrato.NumeroIdentificacion;
                                vConsultarSuperInterContrato.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vConsultarSuperInterContrato.TipoIdentificacion;
                                vConsultarSuperInterContrato.NombreRazonSocialSuperInterv = vDataReaderResults["NombreRazonsocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRazonsocial"].ToString()) : vConsultarSuperInterContrato.NombreRazonSocialSuperInterv;
                                vConsultarSuperInterContrato.NumeroContratoInterventoria = vDataReaderResults["NumeroContratoInterventoria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContratoInterventoria"].ToString()) : vConsultarSuperInterContrato.NumeroContratoInterventoria;

                                vConsultarSuperInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsultarSuperInterContrato.UsuarioCrea;
                                vConsultarSuperInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsultarSuperInterContrato.FechaCrea;
                                vConsultarSuperInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsultarSuperInterContrato.UsuarioModifica;
                                vConsultarSuperInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsultarSuperInterContrato.FechaModifica;
                                vListaConsultarSuperInterContrato.Add(vConsultarSuperInterContrato);
                            }
                        }
                        catch (System.Data.SqlClient.SqlException e)
                        {
                            return vListaConsultarSuperInterContrato;
                        }
                        return vListaConsultarSuperInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

