using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Gest�n Cl�usulas
    /// </summary>
    public class GestionarClausulasContratoDAL : GeneralDAL
    {
        public GestionarClausulasContratoDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int InsertarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdGestionClausula", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.String, pGestionarClausulasContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NombreClausula", DbType.String, pGestionarClausulasContrato.NombreClausula);
                    vDataBase.AddInParameter(vDbCommand, "@TipoClausula", DbType.Int32, pGestionarClausulasContrato.TipoClausula);
                    vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.String, pGestionarClausulasContrato.Orden);
                    vDataBase.AddInParameter(vDbCommand, "@OrdenNumero", DbType.Int32, pGestionarClausulasContrato.OrdenNumero);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionClausula", DbType.String, pGestionarClausulasContrato.DescripcionClausula);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pGestionarClausulasContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pGestionarClausulasContrato.IdGestionClausula = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdGestionClausula").ToString());
                    GenerarLogAuditoria(pGestionarClausulasContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int ModificarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionClausula", DbType.Int32, pGestionarClausulasContrato.IdGestionClausula);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.String, pGestionarClausulasContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NombreClausula", DbType.String, pGestionarClausulasContrato.NombreClausula);
                    vDataBase.AddInParameter(vDbCommand, "@TipoClausula", DbType.Int32, pGestionarClausulasContrato.TipoClausula);
                    vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.String, pGestionarClausulasContrato.Orden);
                    vDataBase.AddInParameter(vDbCommand, "@OrdenNumero", DbType.Int32, pGestionarClausulasContrato.OrdenNumero);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionClausula", DbType.String, pGestionarClausulasContrato.DescripcionClausula);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pGestionarClausulasContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGestionarClausulasContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int EliminarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionClausula", DbType.Int32, pGestionarClausulasContrato.IdGestionClausula);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGestionarClausulasContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pIdGestionClausula"></param>
        /// <returns></returns>
        public GestionarClausulasContrato ConsultarGestionarClausulasContrato(int pIdGestionClausula)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionClausula", DbType.Int32, pIdGestionClausula);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        GestionarClausulasContrato vGestionarClausulasContrato = new GestionarClausulasContrato();
                        while (vDataReaderResults.Read())
                        {
                            vGestionarClausulasContrato.IdGestionClausula = vDataReaderResults["IdGestionClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionClausula"].ToString()) : vGestionarClausulasContrato.IdGestionClausula;
                            vGestionarClausulasContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdContrato"].ToString()) : vGestionarClausulasContrato.IdContrato;
                            vGestionarClausulasContrato.NombreClausula = vDataReaderResults["NombreClausula"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreClausula"].ToString()) : vGestionarClausulasContrato.NombreClausula;
                            vGestionarClausulasContrato.TipoClausula = vDataReaderResults["TipoClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoClausula"].ToString()) : vGestionarClausulasContrato.TipoClausula;
                            vGestionarClausulasContrato.Orden = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Orden"].ToString()) : vGestionarClausulasContrato.Orden;
                            vGestionarClausulasContrato.OrdenNumero = vDataReaderResults["OrdenNumero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["OrdenNumero"].ToString()) : vGestionarClausulasContrato.OrdenNumero;
                            vGestionarClausulasContrato.DescripcionClausula = vDataReaderResults["DescripcionClausula"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionClausula"].ToString()) : vGestionarClausulasContrato.DescripcionClausula;
                            vGestionarClausulasContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGestionarClausulasContrato.UsuarioCrea;
                            vGestionarClausulasContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGestionarClausulasContrato.FechaCrea;
                            vGestionarClausulasContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGestionarClausulasContrato.UsuarioModifica;
                            vGestionarClausulasContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGestionarClausulasContrato.FechaModifica;
                        }
                        return vGestionarClausulasContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pIdGestionClausula"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNombreClausula"></param>
        /// <param name="pTipoClausula"></param>
        /// <param name="pOrden"></param>
        /// <param name="pOrdenNumero"></param>
        /// <param name="pDescripcionClausula"></param>
        /// <returns></returns>
        public List<GestionarClausulasContrato> ConsultarGestionarClausulasContratos(int? pIdGestionClausula, String pIdContrato, String pNombreClausula, int? pTipoClausula, String pOrden, int? pOrdenNumero, String pDescripcionClausula)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_GestionarClausulasContratos_Consultar"))
                {
                    if(pIdGestionClausula != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdGestionClausula", DbType.Int32, pIdGestionClausula);
                    if(pIdContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.String, pIdContrato);
                    if(pNombreClausula != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreClausula", DbType.String, pNombreClausula);
                    if(pTipoClausula != null)
                         vDataBase.AddInParameter(vDbCommand, "@TipoClausula", DbType.Int32, pTipoClausula);
                    if(pOrden != null)
                         vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.String, pOrden);
                    if (pOrdenNumero != null)
                        vDataBase.AddInParameter(vDbCommand, "@OrdenNumero", DbType.Int32, pOrdenNumero);
                    if (pDescripcionClausula != null)
                         vDataBase.AddInParameter(vDbCommand, "@DescripcionClausula", DbType.String, pDescripcionClausula);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GestionarClausulasContrato> vListaGestionarClausulasContrato = new List<GestionarClausulasContrato>();
                        while (vDataReaderResults.Read())
                        {
                                GestionarClausulasContrato vGestionarClausulasContrato = new GestionarClausulasContrato();
                            vGestionarClausulasContrato.IdGestionClausula = vDataReaderResults["IdGestionClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionClausula"].ToString()) : vGestionarClausulasContrato.IdGestionClausula;
                            vGestionarClausulasContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdContrato"].ToString()) : vGestionarClausulasContrato.IdContrato;
                            vGestionarClausulasContrato.NombreClausula = vDataReaderResults["NombreClausula"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreClausula"].ToString()) : vGestionarClausulasContrato.NombreClausula;
                            vGestionarClausulasContrato.TipoClausula = vDataReaderResults["TipoClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoClausula"].ToString()) : vGestionarClausulasContrato.TipoClausula;
                            vGestionarClausulasContrato.Orden = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Orden"].ToString()) : vGestionarClausulasContrato.Orden;
                            vGestionarClausulasContrato.DescripcionClausula = vDataReaderResults["DescripcionClausula"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionClausula"].ToString()) : vGestionarClausulasContrato.DescripcionClausula;
                            vGestionarClausulasContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGestionarClausulasContrato.UsuarioCrea;
                            vGestionarClausulasContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGestionarClausulasContrato.FechaCrea;
                            vGestionarClausulasContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGestionarClausulasContrato.UsuarioModifica;
                            vGestionarClausulasContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGestionarClausulasContrato.FechaModifica;
                                vListaGestionarClausulasContrato.Add(vGestionarClausulasContrato);
                        }
                        return vListaGestionarClausulasContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
