﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;



namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener  
    /// registros, entre otros para la entidad  Profesion Kactus
    /// </summary>
    public class ProfesionKactusDAL : GeneralDAL
    {
        public ProfesionKactusDAL()
        { 
        
        }

        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo de consulta por filtros para la entidad ProfesionKactus
        /// </summary>
        /// <param name="pModalidadAcademica">Cadena de texto con la modalidad academica de la profesion</param>
        /// <param name="pDescripcion">Cadena de texto</param>
        /// <param name="pCodigo">Cadena de texto con el identificador</param>
        /// <param name="pEstado">Booleano con el estado</param>
        /// <returns>Lista con las profesiones que coinciden con los filtros dados</returns>
        public List<ProfesionKactus> ConsultarProfesionesKactus(String pModalidadAcademica, String pDescripcion, String pCodigo, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_KACTUS_KPRODII_Contrato_Profesiones_Consultar"))
                {
                    if (pModalidadAcademica != null)
                        vDataBase.AddInParameter(vDbCommand, "@ModalidadAcademica", DbType.String, pModalidadAcademica);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pCodigo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pCodigo);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProfesionKactus> vListaProfesionesKactus = new List<ProfesionKactus>();
                        while (vDataReaderResults.Read())
                        {
                            ProfesionKactus vModalidadAcademicaKactus = new ProfesionKactus();
                            //vModalidadAcademicaKactus.Id = vDataReaderResults["Id"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Id"].ToString()) : vModalidadAcademicaKactus.Id;
                            vModalidadAcademicaKactus.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vModalidadAcademicaKactus.Codigo;
                            vModalidadAcademicaKactus.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vModalidadAcademicaKactus.Descripcion;
                            //vModalidadAcademicaKactus.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadSeleccion.UsuarioCrea;
                            //vModalidadAcademicaKactus.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadSeleccion.FechaCrea;
                            //vModalidadAcademicaKactus.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadSeleccion.UsuarioModifica;
                            //vModalidadAcademicaKactus.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadSeleccion.FechaModifica;
                            vListaProfesionesKactus.Add(vModalidadAcademicaKactus);
                        }
                        return vListaProfesionesKactus;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }


}
