﻿using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess
{
    public class SolModContractualDAL : GeneralDAL
    {
        public SolModContractualDAL()
        {

        }

        public int InsertarSolModContractual(ConsModContractual psModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, psModContractual.IDContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, psModContractual.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDoc", DbType.String, psModContractual.NumeroDoc);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoSuscrito", DbType.String, psModContractual.ConsecutivoSuscrito);
                    vDataBase.AddInParameter(vDbCommand, "@Suscrito", DbType.Boolean, psModContractual.Suscrito);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, psModContractual.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, psModContractual.FechaSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoModificacionContractual", DbType.Int32, psModContractual.IDTipoModificacionContractual);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    psModContractual.IDCosModContractual = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDCosModContractual").ToString());
                    GenerarLogAuditoria(psModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarSolModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, pConsModContractual.IDCosModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDoc", DbType.String, pConsModContractual.NumeroDoc);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pConsModContractual.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoSuscrito", DbType.String, pConsModContractual.ConsecutivoSuscrito);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioMod", DbType.String, pConsModContractual.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Suscrito", DbType.Boolean, pConsModContractual.Suscrito);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pConsModContractual.FechaSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoModificacionContractual", DbType.String, pConsModContractual.IDTipoModificacionContractual);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pConsModContractual.IdConsModContractualesEstado == 1 ? true : false);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pConsModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractualContrato> ConsultarContratos(string numeroContrato,
                                                                  int? vigenciaFiscal,
                                                                  int? regional,
                                                                  int? idCategoriaContrato,
                                                                  int? idTipoContrato,
                                                                  string numeroIdentificacion,
                                                                  int? valorContratoDesde,
                                                                  int? valorContratoHasta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarContratos"))
                {
                    if (numeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, numeroContrato);
                    if (vigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, vigenciaFiscal);
                    if (regional != null)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int32, regional);
                    if (idCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, idCategoriaContrato);
                    if (idTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, idTipoContrato);
                    if (numeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, numeroIdentificacion);
                    if (valorContratoDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorContratoDesde", DbType.Int32, numeroIdentificacion);
                    if (valorContratoHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorContratoHasta", DbType.Int32, numeroIdentificacion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractualContrato> vListaConsModContractual = new List<SolModContractualContrato>();
                        while (vDataReaderResults.Read())
                        {
                            int idContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : -1;

                            if ( idContrato != -1 && !vListaConsModContractual.Any(e => e.IdContrato == idContrato))
                            {
                                SolModContractualContrato vConsModContractual = new SolModContractualContrato();
                                vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                                vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                                vConsModContractual.VigenciaFiscalInicial = vDataReaderResults["VigenciaFiscalInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalInicial"].ToString()) : vConsModContractual.VigenciaFiscalInicial;
                                vConsModContractual.VigenciaFiscalFinal = vDataReaderResults["VigenciaFiscalFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalFinal"].ToString()) : vConsModContractual.VigenciaFiscalFinal;
                                vConsModContractual.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vConsModContractual.NombreRegional;
                                vConsModContractual.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vConsModContractual.ModalidadSeleccion;
                                vConsModContractual.CategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vConsModContractual.CategoriaContrato;
                                vConsModContractual.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vConsModContractual.TipoContrato;
                                vConsModContractual.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vConsModContractual.NombreRegional;
                                vConsModContractual.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vConsModContractual.DependenciaSolicitante;
                                vConsModContractual.IdTipoDocumentoContratista = vDataReaderResults["IdTipoDocumentoContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoDocumentoContratista"].ToString()) : vConsModContractual.IdTipoDocumentoContratista;
                                vConsModContractual.IdentificacionContratista = vDataReaderResults["IdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionContratista"].ToString()) : vConsModContractual.IdentificacionContratista;
                                vConsModContractual.TipoPersona = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vConsModContractual.IdentificacionContratista;

                                if (vDataReaderResults["RazonSocial"] != DBNull.Value && !string.IsNullOrEmpty(vDataReaderResults["RazonSocial"].ToString()))
                                    vConsModContractual.NombreContratista = vDataReaderResults["RazonSocial"].ToString();
                                else
                                {
                                    vConsModContractual.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vConsModContractual.NombreContratista;
                                }

                                vConsModContractual.CodDocumentoContratista = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vConsModContractual.CodDocumentoContratista;
                                vListaConsModContractual.Add(vConsModContractual);
                            }
                        }
                        return vListaConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolModContractualContrato ConsultarContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SolModContractualContrato vConsModContractual = new SolModContractualContrato();

                        while (vDataReaderResults.Read())
                        {
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.VigenciaFiscalInicial = vDataReaderResults["VigenciaFiscalInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalInicial"].ToString()) : vConsModContractual.VigenciaFiscalInicial;
                            vConsModContractual.VigenciaFiscalFinal = vDataReaderResults["VigenciaFiscalFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalFinal"].ToString()) : vConsModContractual.VigenciaFiscalFinal;
                            vConsModContractual.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vConsModContractual.NombreRegional;
                            vConsModContractual.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vConsModContractual.DependenciaSolicitante;
                            vConsModContractual.IdTipoDocumentoContratista = vDataReaderResults["IdTipoDocumentoContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoDocumentoContratista"].ToString()) : vConsModContractual.IdTipoDocumentoContratista;
                            vConsModContractual.IdentificacionContratista = vDataReaderResults["IdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionContratista"].ToString()) : vConsModContractual.IdentificacionContratista;
                            vConsModContractual.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vConsModContractual.NombreContratista;
                            vConsModContractual.CategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vConsModContractual.CategoriaContrato;
                            vConsModContractual.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : vConsModContractual.NombreTipoContrato;

                        }
                        return vConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ConsModContractual ConsultarUltimaSolicitud(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarUltimaContratoSolicitudes"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ConsModContractual vConsModContractual = new ConsModContractual();

                        while (vDataReaderResults.Read())
                        {
                            vConsModContractual.IDContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IDContrato;
                            vConsModContractual.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vConsModContractual.FechaSolicitud;
                            vConsModContractual.ConsModContractualesEstado_Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vConsModContractual.ConsModContractualesEstado_Descripcion;                          
                        }
                        return vConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarSolicitudesPorContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarContratoSolicitudes"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractual> vConsModContractualist = new List<SolModContractual>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractual vConsModContractual = new SolModContractual();
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.Estado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vConsModContractual.Estado;
                            vConsModContractual.TipoModificacion = vDataReaderResults["DescripcionTipoMod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoMod"].ToString()) : vConsModContractual.TipoModificacion;
                            //vConsModContractual.FechaDevolucion = vDataReaderResults["FechaDevolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDevolucion"].ToString()) : vConsModContractual.FechaDevolucion;
                            //vConsModContractual.RazonDevolucion = vDataReaderResults["RazonDevolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonDevolucion"].ToString()) : vConsModContractual.RazonDevolucion;

                            vConsModContractualist.Add(vConsModContractual);
                        }
                        return vConsModContractualist;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<SolModContractual> ConsultarSolicitudesRechazadasPorContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarContratoSolicitudesRechazadas"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractual> vConsModContractualist = new List<SolModContractual>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractual vConsModContractual = new SolModContractual();
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.Estado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vConsModContractual.Estado;
                            vConsModContractual.TipoModificacion = vDataReaderResults["DescripcionTipoMod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoMod"].ToString()) : vConsModContractual.TipoModificacion;
                            //vConsModContractual.FechaDevolucion = vDataReaderResults["FechaDevolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDevolucion"].ToString()) : vConsModContractual.FechaDevolucion;
                            //vConsModContractual.RazonDevolucion = vDataReaderResults["RazonDevolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonDevolucion"].ToString()) : vConsModContractual.RazonDevolucion;

                            vConsModContractualist.Add(vConsModContractual);
                        }
                        return vConsModContractualist;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public SolModContractual ConsultarSolitud(int IDCosModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarSolicitud"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDCosModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SolModContractual vConsModContractual = new SolModContractual();

                        while (vDataReaderResults.Read())
                        {
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.TipoModificacion = vDataReaderResults["TipoModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacion"].ToString()) : vConsModContractual.TipoModificacion;
                            vConsModContractual.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vConsModContractual.Codigo;
                            vConsModContractual.FechaCrea = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.IDTipoModificacionContractual = vDataReaderResults["IDTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoModificacionContractual"].ToString()) : vConsModContractual.IDTipoModificacionContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vConsModContractual.FechaSolicitud;
                            //vConsModContractual.JustificacionRechazo = vDataReaderResults["JustificacionRechazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionRechazo"].ToString()) : vConsModContractual.JustificacionRechazo;
                            //vConsModContractual.JustificacionDevolucion = vDataReaderResults["JustificacionDevolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionDevolucion"].ToString()) : vConsModContractual.JustificacionDevolucion;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vConsModContractual.Estado;
                        }
                        return vConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<SolModContractualDetalle> ConsultarSolicitudesDetalle(int IDCosModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarSolicitudDetalle"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDCosModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractualDetalle> vConsModContractualDetalleList = new List<SolModContractualDetalle>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractualDetalle vConsModContractualDetalle = new SolModContractualDetalle();
                            vConsModContractualDetalle.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vConsModContractualDetalle.IDDetalleConsModContractual;
                            vConsModContractualDetalle.IdConsModContractual = vDataReaderResults["IdConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractual"].ToString()) : vConsModContractualDetalle.IdConsModContractual;
                            vConsModContractualDetalle.TipoModificacion = vDataReaderResults["TipoModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacion"].ToString()) : vConsModContractualDetalle.TipoModificacion;
                            vConsModContractualDetalle.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : vConsModContractualDetalle.Fecha;
                            vConsModContractualDetalleList.Add(vConsModContractualDetalle);

                        }
                        return vConsModContractualDetalleList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public bool ConsultarExisteSolicitudModificacion(int idContrato, int idTipoSolicitud)
        {
            bool existe = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarExisteSolicitudModificacion"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@idContrato", DbType.Int32, idContrato);
                    vDataBase.AddInParameter(vDbCommand, "@idTipoModificacion", DbType.Int32, idTipoSolicitud);
                    vDataBase.AddOutParameter(vDbCommand, "@existeSolicitud", DbType.Int32, 2);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    existe = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@existeSolicitud").ToString()) > 0 ? true : false;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }

        public bool ConsultarPuedeModificarTipoSolicitud(int idCosModContractual)
        {
            bool existe = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarPuedeModificarTipoSolicitud"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, idCosModContractual);
                    vDataBase.AddOutParameter(vDbCommand, "@PuedeModificar", DbType.Int32, 2);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    existe = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@PuedeModificar").ToString()) == 0 ? true : false;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }

        public bool ConsultarSiesContratoSV(int idContrato)
        {
            bool existe = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarSiesContratoSV"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, idContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@EsContratoSV", DbType.Int32, 2);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    existe = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@EsContratoSV").ToString()) == 0 ? false : true;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }

        public List<SolModContractualValidacion> ValidarCambiodeEstadoaEnviada(int IDConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ValidarCambiarEstadoEnviada"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractualValidacion> vConsModContractualDetalleList = new List<SolModContractualValidacion>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractualValidacion vConsModContractualDetalle = new SolModContractualValidacion();
                            vConsModContractualDetalle.Tipo = vDataReaderResults["modificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["modificacion"].ToString()) : vConsModContractualDetalle.Tipo;
                            vConsModContractualDetalle.Descripcion = vDataReaderResults["mensaje"] != DBNull.Value ? Convert.ToString(vDataReaderResults["mensaje"].ToString()) : vConsModContractualDetalle.Descripcion;
                            vConsModContractualDetalleList.Add(vConsModContractualDetalle);
                        }
                        return vConsModContractualDetalleList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoRegistroaEnviada(int IDConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_CambiarEstadoEnviada"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDConsModContractual);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificacionEstadoContractualContrato(int IDConsModContractual, int Estado, String JustificacionRechazo, String JustificacionDevolucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_CambiarEstado"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsModContractualesEstado", DbType.Int32, Estado);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionRechazo", DbType.String, JustificacionRechazo);
                    vDataBase.AddInParameter(vDbCommand, "@JustificacionDevolucion", DbType.String, JustificacionDevolucion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<string> PuedeCrearSolicitud(int idContrato, int idTipoModificacion, int idConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarPuedeCrearTipoSolicitud"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idContrato", DbType.Int32, idContrato);
                    vDataBase.AddInParameter(vDbCommand, "@idTipoModificacion", DbType.Int32, idTipoModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@idConsModContractual", DbType.Int32, idConsModContractual);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<string> vConsModContractualDetalleList = new List<string>();

                        while (vDataReaderResults.Read())
                        {
                            var mensaje = vDataReaderResults["mensaje"] != DBNull.Value ? Convert.ToString(vDataReaderResults["mensaje"].ToString()) : string.Empty;
                            vConsModContractualDetalleList.Add(mensaje);
                        }

                        return vConsModContractualDetalleList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificacionContractualContrato(int IDCosModContractual, string usuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Solicitudes_ModificacionContractual"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDCosModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@usuarioModifica", DbType.String, usuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificacionContractualContratoRechazoDevolucion(int IDCosModContractual, bool esRechazo, string usuario, string justificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Solicitudes_ModificacionContractualRechazoDevolucion"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDCosModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@usuario", DbType.String, usuario);
                    vDataBase.AddInParameter(vDbCommand, "@EsRechazo", DbType.Boolean, esRechazo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractualHistorico> ConsultarSolicitudesHistorico(int IDCosModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarSolicitudHistorico"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, IDCosModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolModContractualHistorico> vConsModContractualDetalleList = new List<SolModContractualHistorico>();

                        while (vDataReaderResults.Read())
                        {
                            SolModContractualHistorico vConsModContractualDetalle = new SolModContractualHistorico();
                            vConsModContractualDetalle.IdConsModContractual = vDataReaderResults["IdConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractual"].ToString()) : vConsModContractualDetalle.IdConsModContractual;
                            vConsModContractualDetalle.IdConsModContractualEstado = vDataReaderResults["IdConsModContractualEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractualEstado"].ToString()) : vConsModContractualDetalle.IdConsModContractualEstado;
                            vConsModContractualDetalle.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractualDetalle.UsuarioCrea;
                            vConsModContractualDetalle.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vConsModContractualDetalle.FechaCreacion;
                            vConsModContractualDetalle.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vConsModContractualDetalle.Estado;
                            vConsModContractualDetalle.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractualDetalle.Justificacion;

                            vConsModContractualDetalleList.Add(vConsModContractualDetalle);
                        }
                        return vConsModContractualDetalleList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public IDictionary<string, int> ConsultarConsolidadoModificaciones(int idContrato)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Modificaciones_ConsultarConsolidado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@cantidadAdiciones", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@cantidadReducciones", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@cantidadSuspensiones", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@cantidadProrrogas", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@cantidadcesiones", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@cantidadModificacionO", DbType.Int32, 18);

                    int vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    int cantidadAdiciones = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@cantidadAdiciones").ToString());
                    int cantidadReducciones = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@cantidadReducciones").ToString());
                    int cantidadSuspensiones = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@cantidadSuspensiones").ToString());
                    int cantidadProrrogas = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@cantidadProrrogas").ToString());
                    int cantidadCesiones = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@cantidadcesiones").ToString());
                    int cantidadModificacionesO = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@cantidadModificacionO").ToString());

                    result.Add("Adiciones", cantidadAdiciones);
                    result.Add("Reducciones", cantidadReducciones);
                    result.Add("Suspensiones", cantidadSuspensiones);
                    result.Add("Prorrogas", cantidadProrrogas);
                    result.Add("Cesiones", cantidadCesiones);
                    result.Add("Modificaciones", cantidadModificacionesO);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public List<HistoricoSolicitudesContrato> ConsultarModificacionesHistorico(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarHistoricoSolicitudesContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDContrato", DbType.Int32, idContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoSolicitudesContrato> vHistoricoSolicitudesContratoList = new List<HistoricoSolicitudesContrato>();

                        while (vDataReaderResults.Read())
                        {
                            HistoricoSolicitudesContrato vHistoricoSolicitudesContrato = new HistoricoSolicitudesContrato();
                            vHistoricoSolicitudesContrato.IdContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vHistoricoSolicitudesContrato.IdContrato;
                            vHistoricoSolicitudesContrato.IdCosModContractual = vDataReaderResults["IdCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCosModContractual"].ToString()) : vHistoricoSolicitudesContrato.IdCosModContractual;
                            vHistoricoSolicitudesContrato.NumeroModificacion = vDataReaderResults["NumeroModificacion"] != DBNull.Value 
                                    ? ! string.IsNullOrEmpty(vDataReaderResults["NumeroModificacion"].ToString()) ? Convert.ToInt32(vDataReaderResults["NumeroModificacion"].ToString()) :0 : vHistoricoSolicitudesContrato.NumeroModificacion;
                            
                            vHistoricoSolicitudesContrato.EstadoModificacion = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vHistoricoSolicitudesContrato.EstadoModificacion;
                            vHistoricoSolicitudesContrato.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vHistoricoSolicitudesContrato.FechaModificacion;
                            vHistoricoSolicitudesContrato.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vHistoricoSolicitudesContrato.FechaSolicitud;
                            vHistoricoSolicitudesContrato.FechaSuscripcion = vDataReaderResults["FechaSubscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSubscripcion"].ToString()) : vHistoricoSolicitudesContrato.FechaSuscripcion;
                            vHistoricoSolicitudesContrato.TipoModificacion = vDataReaderResults["DescripcionTipoMod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoMod"].ToString()) : vHistoricoSolicitudesContrato.TipoModificacion;

                            vHistoricoSolicitudesContratoList.Add(vHistoricoSolicitudesContrato);
                        }
                        return vHistoricoSolicitudesContratoList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int SubscribirModificacionContractualContrato(int idConsmodContractual, string usuario, DateTime fechaAprobacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Solicitudes_ModificacionContractualSubscribir"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, idConsmodContractual);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSubscripcion", DbType.DateTime, fechaAprobacion);
                    vDataBase.AddInParameter(vDbCommand, "@usuarioModifica", DbType.String, usuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolModContractual ObtenerModificacionSinModificacionGarantiaPorTipo(int idTipoModificacion, int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Solicitudes_ConsultarModificacionSinModificacionGarantiaPorTipo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, idTipoModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SolModContractual vConsModContractual = new SolModContractual();

                        while (vDataReaderResults.Read())
                        {
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.TipoModificacion = vDataReaderResults["TipoModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacion"].ToString()) : vConsModContractual.TipoModificacion;
                            vConsModContractual.FechaCrea = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.IDTipoModificacionContractual = vDataReaderResults["IDTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoModificacionContractual"].ToString()) : vConsModContractual.IDTipoModificacionContractual;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vConsModContractual.Estado;
                            vConsModContractual.FechaSuscripcion = vDataReaderResults["FechaSubscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSubscripcion"]) : vConsModContractual.FechaSuscripcion;
                        }
                        return vConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
