using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad PlanComprasRubrosCDP
    /// </summary>
    public class PlanComprasRubrosCDPDAL : GeneralDAL
    {
        public PlanComprasRubrosCDPDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int InsertarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasRubrosCDP_Insertar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasRubrosCDP.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@RecursoPresupuestal", DbType.String, pPlanComprasRubrosCDP.RecursoPresupuestal);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRubro", DbType.String, pPlanComprasRubrosCDP.CodigoRubro);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionRubro", DbType.String, pPlanComprasRubrosCDP.DescripcionRubro);
                    vDataBase.AddInParameter(vDbCommand, "@ValorRubro", DbType.Decimal, pPlanComprasRubrosCDP.ValorRubro);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasRubrosCDP.UsuarioCrea);
                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasRubrosCDP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int ModificarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasRubrosCDP_Modificar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDRubroPlanComprasContrato", DbType.Int32, pPlanComprasRubrosCDP.IdRubroPlanComprasContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasRubrosCDP.IdPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRubro", DbType.String, pPlanComprasRubrosCDP.CodigoRubro);
                    vDataBase.AddInParameter(vDbCommand, "@ValorRubro", DbType.Decimal, pPlanComprasRubrosCDP.ValorRubro);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlanComprasRubrosCDP.UsuarioModifica);
                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasRubrosCDP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int EliminarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasRubrosCDP_Eliminar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRubroPlanComprasContrato", DbType.Int32, pPlanComprasRubrosCDP.IdRubroPlanComprasContrato);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasRubrosCDP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasRubrosCDP ConsultarPlanComprasRubrosCDP(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasRubrosCDP_Consultar"))
                {
                    if (pIdProductoPlanCompraContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRubroPlanComprasContrato", DbType.Int32, pIdProductoPlanCompraContrato);

                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vPlanComprasRubrosCDP = new PlanComprasRubrosCDP();
                        while (vDataReaderResults.Read())
                        {
                            vPlanComprasRubrosCDP.NumeroConsecutivoPlanCompras = vDataReaderResults["NumeroConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivoPlanCompras"].ToString()) : vPlanComprasRubrosCDP.NumeroConsecutivoPlanCompras;
                            vPlanComprasRubrosCDP.IdRubroPlanComprasContrato = vDataReaderResults["IDRubroPlanComprasContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDRubroPlanComprasContrato"].ToString()) : vPlanComprasRubrosCDP.IdRubroPlanComprasContrato;
                            vPlanComprasRubrosCDP.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasRubrosCDP.IdPlanDeComprasContratos;
                            vPlanComprasRubrosCDP.ValorRubro = vDataReaderResults["ValorRubroPresupuestal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubroPresupuestal"].ToString()) : vPlanComprasRubrosCDP.ValorRubro;
                            vPlanComprasRubrosCDP.CodigoRubro = vDataReaderResults["IDRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IDRubro"].ToString()) : vPlanComprasRubrosCDP.CodigoRubro;
                            //vPlanComprasRubrosCDP.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vPlanComprasRubrosCDP.RecursoPresupuestal;
                            vPlanComprasRubrosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasRubrosCDP.UsuarioCrea;
                            vPlanComprasRubrosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasRubrosCDP.FechaCrea;
                            vPlanComprasRubrosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasRubrosCDP.UsuarioModifica;
                            vPlanComprasRubrosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasRubrosCDP.FechaModifica;
                        }
                        return vPlanComprasRubrosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pIdRubroPlanComprasContrato"></param>
        /// <param name="pRecursoPresupuestal"></param>
        /// <param name="pValorRubro"></param>
        public List<PlanComprasRubrosCDP> ConsultarPlanComprasRubrosCDPs(int? pNumeroConsecutivoPlanCompras, 
                                                                         String pIdRubroPlanComprasContrato,
                                                                         String pRecursoPresupuestal, 
                                                                         Decimal? pValorRubro)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasRubrosCDPs_Consultar"))
                {
                    if(pNumeroConsecutivoPlanCompras != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pNumeroConsecutivoPlanCompras);
                    if (pIdRubroPlanComprasContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRubroPlanComprasContrato", DbType.Int32, pIdRubroPlanComprasContrato);
                    if(pValorRubro != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorRubro", DbType.Decimal, pValorRubro);
                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaPlanComprasRubrosCDP = new List<PlanComprasRubrosCDP>();
                        while (vDataReaderResults.Read())
                        {
                            var vPlanComprasRubrosCDP = new PlanComprasRubrosCDP();
                            vPlanComprasRubrosCDP.NumeroConsecutivoPlanCompras = vDataReaderResults["NumeroConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivoPlanCompras"].ToString()) : vPlanComprasRubrosCDP.NumeroConsecutivoPlanCompras;
                            vPlanComprasRubrosCDP.IdRubroPlanComprasContrato = vDataReaderResults["IDRubroPlanComprasContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDRubroPlanComprasContrato"].ToString()) : vPlanComprasRubrosCDP.IdRubroPlanComprasContrato;
                            vPlanComprasRubrosCDP.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasRubrosCDP.IdPlanDeComprasContratos;
                            vPlanComprasRubrosCDP.ValorRubro = vDataReaderResults["ValorRubroPresupuestal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubroPresupuestal"].ToString()) : vPlanComprasRubrosCDP.ValorRubro;
                            vPlanComprasRubrosCDP.CodigoRubro = vDataReaderResults["IDRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IDRubro"].ToString()) : vPlanComprasRubrosCDP.CodigoRubro;
                            //vPlanComprasRubrosCDP.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vPlanComprasRubrosCDP.RecursoPresupuestal;
                            vPlanComprasRubrosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasRubrosCDP.UsuarioCrea;
                            vPlanComprasRubrosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasRubrosCDP.FechaCrea;
                            vPlanComprasRubrosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasRubrosCDP.UsuarioModifica;
                            vPlanComprasRubrosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasRubrosCDP.FechaModifica;
                            vPlanComprasRubrosCDP.IdPagosDetalle = vDataReaderResults["IdPagosDetalle"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdPagosDetalle"].ToString()) : vPlanComprasRubrosCDP.IdPagosDetalle;
                            vListaPlanComprasRubrosCDP.Add(vPlanComprasRubrosCDP);
                        }
                        return vListaPlanComprasRubrosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los rubros asociados a un plan de compras para ser utilizado en la pagina de registro
        /// de contratos
        /// </summary>
        /// <param name="pIdPlanDeComprasContratos">Entero con el identificador del plan de compras</param>
        /// <returns>Listado de rubros asociados al plan de compras obtenidos de la base de datos</returns>
        public List<PlanComprasRubrosCDP> ObtenerRubrosPlanCompras(int pIdPlanDeComprasContratos)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasRubros_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pIdPlanDeComprasContratos);
                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaPlanComprasRubrosCDP = new List<PlanComprasRubrosCDP>();
                        while (vDataReaderResults.Read())
                        {
                            var vPlanComprasRubrosCDP = new PlanComprasRubrosCDP();
                            vPlanComprasRubrosCDP.IdRubroPlanComprasContrato = vDataReaderResults["IDRubroPlanComprasContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDRubroPlanComprasContrato"].ToString()) : vPlanComprasRubrosCDP.IdRubroPlanComprasContrato;
                            vPlanComprasRubrosCDP.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasRubrosCDP.IdPlanDeComprasContratos;
                            vPlanComprasRubrosCDP.ValorRubro = vDataReaderResults["ValorRubroPresupuestal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubroPresupuestal"].ToString()) : vPlanComprasRubrosCDP.ValorRubro;
                            vPlanComprasRubrosCDP.CodigoRubro = vDataReaderResults["IDRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IDRubro"].ToString()) : vPlanComprasRubrosCDP.CodigoRubro;
                            vPlanComprasRubrosCDP.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vPlanComprasRubrosCDP.RecursoPresupuestal;
                            vPlanComprasRubrosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasRubrosCDP.UsuarioCrea;
                            vPlanComprasRubrosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasRubrosCDP.FechaCrea;
                            vPlanComprasRubrosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasRubrosCDP.UsuarioModifica;
                            vPlanComprasRubrosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasRubrosCDP.FechaModifica;
                            vPlanComprasRubrosCDP.NumeroConsecutivoPlanCompras = vDataReaderResults["NumeroConsecutivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecutivo"].ToString()) : vPlanComprasRubrosCDP.NumeroConsecutivoPlanCompras;
                            vPlanComprasRubrosCDP.IdPagosDetalle = vDataReaderResults["IdPagosDetalle"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdPagosDetalle"].ToString()) : vPlanComprasRubrosCDP.IdPagosDetalle;
                            vPlanComprasRubrosCDP.IsReduccion = vDataReaderResults["IsReduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["IsReduccion"].ToString()) : vPlanComprasRubrosCDP.IsReduccion;
                            vPlanComprasRubrosCDP.EsAdiccion = vDataReaderResults["IsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["IsAdicion"].ToString()) : vPlanComprasRubrosCDP.EsAdiccion;
                            
                            
                            vListaPlanComprasRubrosCDP.Add(vPlanComprasRubrosCDP);
                        }
                        return vListaPlanComprasRubrosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool VerificarExistenciaModificacionRubro(int IDPlanDeComprasContratos, string IdRubro, bool esAdicion, int idModificacion)
        {
            bool isValid = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasProductos_VerificarExistenciaModificacionRubro"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@IdRubro", DbType.String, IdRubro);
                    vDataBase.AddInParameter(vDbCommand, "@esAdicion", DbType.Boolean, esAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@idModificacion", DbType.Int32, idModificacion);
                    vDataBase.AddOutParameter(vDbCommand, "@existe", DbType.Boolean, 5);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    isValid = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@existe").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return isValid;
        }

        public int InsertarRubroPlanComprasContrato(RubroPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_RubroPlanComprasContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDRubroPlanComprasContrato", DbType.Int32, pPlanComprasProductos.IDRubroPlanComprasContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorRubroPresupuestal", DbType.Decimal, pPlanComprasProductos.ValorRubroPresupuestal);
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasProductos.IDPlanDeComprasContratos);
                    vDataBase.AddInParameter(vDbCommand, "@IDRubro", DbType.String, pPlanComprasProductos.IDRubro);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasProductos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdPagosDetalle", DbType.String, pPlanComprasProductos.IdPagosDetalle);
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pPlanComprasProductos.IdReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Decimal, pPlanComprasProductos.ValorReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IsReduccion", DbType.Boolean, pPlanComprasProductos.IsReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pPlanComprasProductos.IdAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pPlanComprasProductos.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IsAdicion", DbType.Boolean, pPlanComprasProductos.IsAdicion);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasProductos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarRubroPlanComprasContratoReduccionAdicion(RubroPlanComprasContratos pPlanComprasRubrosCDP)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_RubroPlanCompraContrato_Modificar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@ValorRubroPresupuestal", DbType.Int32, pPlanComprasRubrosCDP.ValorRubroPresupuestal);
                    vDataBase.AddInParameter(vDbCommand, "@IDRubroPlanComprasContrato", DbType.String, pPlanComprasRubrosCDP.IDRubroPlanComprasContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlanComprasRubrosCDP.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pPlanComprasRubrosCDP.IdReduccion);                    
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Decimal, pPlanComprasRubrosCDP.ValorReduccion);                    
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Decimal, pPlanComprasRubrosCDP.IdAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.String, pPlanComprasRubrosCDP.ValorAdicion);
                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasRubrosCDP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlanComprasRubrosCDP> ObtenerRubrosContrato(int IdContratos)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Rubros_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContratos);
                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaPlanComprasRubrosCDP = new List<PlanComprasRubrosCDP>();
                        while (vDataReaderResults.Read())
                        {
                            var vPlanComprasRubrosCDP = new PlanComprasRubrosCDP();
                            vPlanComprasRubrosCDP.ValorRubro = vDataReaderResults["ValorRubroPresupuestal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubroPresupuestal"].ToString()) : vPlanComprasRubrosCDP.ValorRubro;
                            vPlanComprasRubrosCDP.CodigoRubro = vDataReaderResults["IDRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IDRubro"].ToString()) : vPlanComprasRubrosCDP.CodigoRubro;
                    
                            vListaPlanComprasRubrosCDP.Add(vPlanComprasRubrosCDP);
                        }

                        return vListaPlanComprasRubrosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
