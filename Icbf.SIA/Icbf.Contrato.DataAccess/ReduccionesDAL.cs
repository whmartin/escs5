using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class ReduccionesDAL : GeneralDAL
    {
        public ReduccionesDAL()
        {
        }
        public int InsertarReducciones(Reducciones pReducciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Reducciones_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdReduccion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Int32, pReducciones.ValorReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pReducciones.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaReduccion", DbType.DateTime, pReducciones.FechaReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pReducciones.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pReducciones.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pReducciones.IdReduccion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdReduccion").ToString());
                    GenerarLogAuditoria(pReducciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarReducciones(Reducciones pReducciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Reducciones_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pReducciones.IdReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pReducciones.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Int32, pReducciones.ValorReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaReduccion", DbType.DateTime, pReducciones.FechaReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pReducciones.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pReducciones.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pReducciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarValorReducciones(Reducciones pReducciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Reducciones_ModificarValor"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pReducciones.IdReduccion);
                    //vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pReducciones.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Int32, pReducciones.ValorReduccion);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaReduccion", DbType.DateTime, pReducciones.FechaReduccion);
                    //vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pReducciones.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pReducciones.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@SumarValor", DbType.Int32, pReducciones.SumaValor);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pReducciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarReducciones(Reducciones pReducciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Reducciones_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pReducciones.IdReduccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pReducciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Reducciones ConsultarReduccion(int pIdReducciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Reduccion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, pIdReducciones);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Reducciones vReducciones = new Reducciones();
                        while (vDataReaderResults.Read())
                        {
                            vReducciones.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vReducciones.IdReduccion;
                            vReducciones.ValorReduccion = vDataReaderResults["ValorReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorReduccion"].ToString()) : vReducciones.ValorReduccion;
                            vReducciones.FechaReduccion = vDataReaderResults["FechaReduccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReduccion"].ToString()) : vReducciones.FechaReduccion;
                            vReducciones.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vReducciones.Justificacion;
                            vReducciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vReducciones.IDDetalleConsModContractual;
                            vReducciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReducciones.UsuarioCrea;
                            vReducciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReducciones.FechaCrea;
                            vReducciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReducciones.UsuarioModifica;
                            vReducciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReducciones.FechaModifica;
                        }
                        return vReducciones;
                    }
                }
            }


            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ReduccionContrato ConsultarContratoReduccion(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Reducciones_ConsultarContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ReduccionContrato vReduccionContrato = new ReduccionContrato();
                        while (vDataReaderResults.Read())
                        {
                            vReduccionContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vReduccionContrato.NumeroContrato;
                            vReduccionContrato.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vReduccionContrato.NombreRegional;
                            vReduccionContrato.FechaInicioContrato = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vReduccionContrato.FechaInicioContrato;
                            vReduccionContrato.FechaFinalizacionInicialContrato = vDataReaderResults["FechaInicialFinalizacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicialFinalizacion"].ToString()) : vReduccionContrato.FechaFinalizacionInicialContrato;
                            vReduccionContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacion"].ToString()) : vReduccionContrato.FechaFinalTerminacionContrato;
                            vReduccionContrato.ObjetoContrato = vDataReaderResults["Objeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Objeto"].ToString()) : vReduccionContrato.ObjetoContrato;
                            vReduccionContrato.AlcanceObjeto = vDataReaderResults["Alcance"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Alcance"].ToString()) : vReduccionContrato.AlcanceObjeto;
                            vReduccionContrato.ValorInicial = vDataReaderResults["ValorInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicial"].ToString()) : vReduccionContrato.ValorInicial;
                            vReduccionContrato.ValorFinal = vDataReaderResults["ValorFinal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinal"].ToString()) : vReduccionContrato.ValorFinal;
                            vReduccionContrato.PrimerNombreSupervisor = vDataReaderResults["NombreSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSupervisor"].ToString()) : vReduccionContrato.PrimerNombreSupervisor;
                            vReduccionContrato.SegundoNombreSupervisor = vDataReaderResults["SegNombreSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegNombreSupervisor"].ToString()) : vReduccionContrato.SegundoNombreSupervisor;
                            vReduccionContrato.PrimerApellidoSupervisor = vDataReaderResults["PriApellidoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PriApellidoSupervisor"].ToString()) : vReduccionContrato.PrimerApellidoSupervisor;
                            vReduccionContrato.SegundoApellidoSupervisor = vDataReaderResults["SegApellidoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegApellidoSupervisor"].ToString()) : vReduccionContrato.SegundoApellidoSupervisor;

                        }
                        return vReduccionContrato;
                    }
                }


            }


            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Reducciones> ConsultarReduccioness(int? pValorReduccion, DateTime? pFechaReduccion, int? pIDDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Reducciones_Consultar"))
                {
                    if(pValorReduccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorReduccion", DbType.Int32, pValorReduccion);
                    if(pFechaReduccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaReduccion", DbType.DateTime, pFechaReduccion);
                    if(pIDDetalleConsModContractual != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pIDDetalleConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Reducciones> vListaReducciones = new List<Reducciones>();
                        while (vDataReaderResults.Read())
                        {
                                Reducciones vReducciones = new Reducciones();
                            vReducciones.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vReducciones.IdReduccion;
                            vReducciones.ValorReduccion = vDataReaderResults["ValorReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorReduccion"].ToString()) : vReducciones.ValorReduccion;
                            vReducciones.FechaReduccion = vDataReaderResults["FechaReduccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReduccion"].ToString()) : vReducciones.FechaReduccion;
                            vReducciones.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vReducciones.Justificacion;
                            vReducciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vReducciones.IDDetalleConsModContractual;
                            vReducciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReducciones.UsuarioCrea;
                            vReducciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReducciones.FechaCrea;
                            vReducciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReducciones.UsuarioModifica;
                            vReducciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReducciones.FechaModifica;
                                vListaReducciones.Add(vReducciones);
                        }
                        return vListaReducciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Reducciones> ConsultarReduccionesAprobadasContrato(int IdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Reducciones_ConsultarAprobadasContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);
                   
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Reducciones> vListaReducciones = new List<Reducciones>();

                        while (vDataReaderResults.Read())
                        {
                            Reducciones vReducciones = new Reducciones();
                            vReducciones.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vReducciones.IdReduccion;
                            vReducciones.ValorReduccion = vDataReaderResults["ValorReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorReduccion"].ToString()) : vReducciones.ValorReduccion;
                            vReducciones.FechaReduccion = vDataReaderResults["FechaReduccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReduccion"].ToString()) : vReducciones.FechaReduccion;
                            vReducciones.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vReducciones.Justificacion;
                            vReducciones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vReducciones.IDDetalleConsModContractual;
                            vReducciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReducciones.UsuarioCrea;
                            vReducciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReducciones.FechaCrea;
                            vReducciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReducciones.UsuarioModifica;
                            vReducciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReducciones.FechaModifica;
                            vListaReducciones.Add(vReducciones);
                        }

                        return vListaReducciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorTotalReducido(int IdContrato)
        {
            decimal result = 0;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Reducciones_ConsultarValorTotalAprobadas"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@ValorReducido", DbType.Decimal, 30);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    var valor = vDataBase.GetParameterValue(vDbCommand, "@ValorReducido").ToString();
                    if (!string.IsNullOrEmpty(valor))
                        result = Convert.ToDecimal(valor);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

    }
}
