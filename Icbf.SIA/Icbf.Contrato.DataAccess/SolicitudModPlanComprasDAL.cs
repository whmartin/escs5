﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad SolicitudModPlanCompras
    /// </summary>
    public class SolicitudModPlanComprasDAL : GeneralDAL
    {
        public SolicitudModPlanComprasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int InsertarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SolicitudModPlanCompras_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSolicitudModPlanCompra", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pSolicitudModPlanCompras.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroSolicitud", DbType.Int32, pSolicitudModPlanCompras.NumeroSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoSolicitud", DbType.String, pSolicitudModPlanCompras.IdEstadoSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioSolicitud", DbType.Int32, pSolicitudModPlanCompras.IdUsuarioSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanDeCompras", DbType.Int32, pSolicitudModPlanCompras.IdPlanDeCompras);
                    //vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pSolicitudModPlanCompras.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSolicitudModPlanCompras.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSolicitudModPlanCompras.IdSolicitudModPlanCompra = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSolicitudModPlanCompra").ToString());
                    GenerarLogAuditoria(pSolicitudModPlanCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int ModificarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SolicitudModPlanCompras_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudModPlanCompra", DbType.Int32, pSolicitudModPlanCompras.IdSolicitudModPlanCompra);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, pSolicitudModPlanCompras.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroSolicitud", DbType.Int32, pSolicitudModPlanCompras.NumeroSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoSolicitud", DbType.String, pSolicitudModPlanCompras.IdEstadoSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioSolicitud", DbType.Int32, pSolicitudModPlanCompras.IdUsuarioSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanDeCompras", DbType.Int32, pSolicitudModPlanCompras.IdPlanDeCompras);
                    //vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pSolicitudModPlanCompras.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSolicitudModPlanCompras.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSolicitudModPlanCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int EliminarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SolicitudModPlanCompras_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudModPlanCompra", DbType.Int32, pSolicitudModPlanCompras.IdSolicitudModPlanCompra);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSolicitudModPlanCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pIdSolicitudModPlanCompra"></param>
        public SolicitudModPlanCompras ConsultarSolicitudModPlanCompras(int pIdSolicitudModPlanCompra)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SolicitudModPlanCompras_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSolicitudModPlanCompra", DbType.Int32, pIdSolicitudModPlanCompra);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SolicitudModPlanCompras vSolicitudModPlanCompras = new SolicitudModPlanCompras();
                        while (vDataReaderResults.Read())
                        {
                            vSolicitudModPlanCompras.IdSolicitudModPlanCompra = vDataReaderResults["IdSolicitudModPlanCompra"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudModPlanCompra"].ToString()) : vSolicitudModPlanCompras.IdSolicitudModPlanCompra;
                            vSolicitudModPlanCompras.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vSolicitudModPlanCompras.Justificacion;
                            vSolicitudModPlanCompras.NumeroSolicitud = vDataReaderResults["NumeroSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroSolicitud"].ToString()) : vSolicitudModPlanCompras.NumeroSolicitud;
                            vSolicitudModPlanCompras.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vSolicitudModPlanCompras.FechaSolicitud;
                            vSolicitudModPlanCompras.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSolicitudModPlanCompras.IdContrato;
                            vSolicitudModPlanCompras.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vSolicitudModPlanCompras.NumeroContrato;
                            vSolicitudModPlanCompras.NumeroConsecPlanDeCompras = vDataReaderResults["NumeroConsecPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecPlanDeCompras"].ToString()) : vSolicitudModPlanCompras.NumeroConsecPlanDeCompras;
                            vSolicitudModPlanCompras.UsuarioSolicitud = vDataReaderResults["UsuarioSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioSolicitud"].ToString()) : vSolicitudModPlanCompras.UsuarioSolicitud;
                            vSolicitudModPlanCompras.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vSolicitudModPlanCompras.Vigencia;
                            vSolicitudModPlanCompras.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoSolicitud"].ToString()) : vSolicitudModPlanCompras.EstadoSolicitud;

                            vSolicitudModPlanCompras.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vSolicitudModPlanCompras.IdEstadoSolicitud;
                            vSolicitudModPlanCompras.IdUsuarioSolicitud = vDataReaderResults["IdUsuarioSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuarioSolicitud"].ToString()) : vSolicitudModPlanCompras.IdUsuarioSolicitud;
                            vSolicitudModPlanCompras.IdPlanDeCompras = vDataReaderResults["IdPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanDeCompras"].ToString()) : vSolicitudModPlanCompras.IdPlanDeCompras;
                            // vSolicitudModPlanCompras.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vSolicitudModPlanCompras.IdVigencia;
                            vSolicitudModPlanCompras.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudModPlanCompras.UsuarioCrea;
                            vSolicitudModPlanCompras.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudModPlanCompras.FechaCrea;
                            vSolicitudModPlanCompras.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudModPlanCompras.UsuarioModifica;
                            vSolicitudModPlanCompras.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudModPlanCompras.FechaModifica;
                        }
                        return vSolicitudModPlanCompras;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pJustificacion"></param>
        /// <param name="pNumeroSolicitud"></param>
        /// <param name="pFechaSolicitud"></param>
        /// <param name="pIdEstadoSolicitud"></param>
        /// <param name="pIdUsuarioSolicitud"></param>
        /// <param name="pIdPlanDeCompras"></param>
        /// <param name="pIdVigencia"></param>
        public List<SolicitudModPlanCompras> ConsultarSolicitudModPlanComprass(int? pNumeroConsecutivoPlanCompras, int? pIdContrato, int? pNumeroSolicitud, string pVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SolicitudModPlanComprass_Consultar"))
                {
                    if (pNumeroConsecutivoPlanCompras != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pNumeroConsecutivoPlanCompras);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pNumeroSolicitud != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroSolicitud", DbType.Int32, pNumeroSolicitud);
                    if (pVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.String, pVigencia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SolicitudModPlanCompras> vListaSolicitudModPlanCompras = new List<SolicitudModPlanCompras>();
                        while (vDataReaderResults.Read())
                        {
                            SolicitudModPlanCompras vSolicitudModPlanCompras = new SolicitudModPlanCompras();
                            vSolicitudModPlanCompras.IdSolicitudModPlanCompra = vDataReaderResults["IdSolicitudModPlanCompra"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudModPlanCompra"].ToString()) : vSolicitudModPlanCompras.IdSolicitudModPlanCompra;
                            vSolicitudModPlanCompras.NumeroSolicitud = vDataReaderResults["NumeroSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroSolicitud"].ToString()) : vSolicitudModPlanCompras.NumeroSolicitud;
                            vSolicitudModPlanCompras.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vSolicitudModPlanCompras.FechaSolicitud;
                            vSolicitudModPlanCompras.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSolicitudModPlanCompras.IdContrato;
                            vSolicitudModPlanCompras.NumeroConsecPlanDeCompras = vDataReaderResults["NumeroConsecPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroConsecPlanDeCompras"].ToString()) : vSolicitudModPlanCompras.NumeroConsecPlanDeCompras;
                            vSolicitudModPlanCompras.UsuarioSolicitud = vDataReaderResults["UsuarioSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioSolicitud"].ToString()) : vSolicitudModPlanCompras.UsuarioSolicitud;
                            vSolicitudModPlanCompras.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vSolicitudModPlanCompras.Justificacion;
                            vSolicitudModPlanCompras.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vSolicitudModPlanCompras.Vigencia;
                            vSolicitudModPlanCompras.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoSolicitud"].ToString()) : vSolicitudModPlanCompras.EstadoSolicitud;

                            vSolicitudModPlanCompras.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vSolicitudModPlanCompras.IdEstadoSolicitud;
                            vSolicitudModPlanCompras.IdUsuarioSolicitud = vDataReaderResults["IdUsuarioSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuarioSolicitud"].ToString()) : vSolicitudModPlanCompras.IdUsuarioSolicitud;
                            vSolicitudModPlanCompras.IdPlanDeCompras = vDataReaderResults["IdPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanDeCompras"].ToString()) : vSolicitudModPlanCompras.IdPlanDeCompras;
                            vSolicitudModPlanCompras.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudModPlanCompras.UsuarioCrea;
                            vSolicitudModPlanCompras.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudModPlanCompras.FechaCrea;
                            vSolicitudModPlanCompras.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudModPlanCompras.UsuarioModifica;
                            vSolicitudModPlanCompras.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudModPlanCompras.FechaModifica;
                            vListaSolicitudModPlanCompras.Add(vSolicitudModPlanCompras);
                        }
                        return vListaSolicitudModPlanCompras;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ExisteEnviadoSolicitudModPlanCompras(string pCodEstado, int NumeroConsecutivoPlanCompras, string pVigenciapc)
        {
            try
            {
                bool vExiste = false;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SolicitudModPlanCompras_VerificarEnviado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, pCodEstado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecPlanCompras", DbType.Int32, NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.String, pVigenciapc);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
