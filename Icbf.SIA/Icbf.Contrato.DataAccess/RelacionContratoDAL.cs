﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Icbf.Utilities.Exceptions;
using System.Data;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Relación Contrato
    /// </summary>
    public class RelacionContratoDAL : GeneralDAL
    {
        public RelacionContratoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad RelacionContrato
        /// </summary>
        /// <param name="pRelacionContrato"></param>
        /// <returns></returns>
        public int InsertarRelacionContrato(RelacionContrato pRelacionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoMaestro", DbType.Int32, pRelacionContrato.IdContratoMaestro);
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoAsociado", DbType.Int32, pRelacionContrato.IdContratoAsociado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAsociacion", DbType.DateTime, pRelacionContrato.FechaAsociacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAbogadoResponsable", DbType.Int32, pRelacionContrato.IdPersonaAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRelacionContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRelacionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RelacionContrato
        /// </summary>
        /// <param name="pRelacionContrato"></param>
        /// <returns></returns>
        public int EliminarRelacionContrato(RelacionContrato pRelacionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoMaestro", DbType.Int32, pRelacionContrato.IdContratoMaestro);
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoAsociado", DbType.Int32, pRelacionContrato.IdContratoAsociado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRelacionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad RelacionContrato
        /// </summary>
        /// <param name="idContratoMaestro"></param>
        /// <returns></returns>
        public List<RelacionContrato> ConsultarRelacionContrato(int idContratoMaestro)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionContratos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoMaestro", DbType.Int32, idContratoMaestro);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RelacionContrato> vListaRelacionContrato = new List<RelacionContrato>();
                        while (vDataReaderResults.Read())
                        {
                            RelacionContrato vLugarEjecucion = new RelacionContrato();
                            vLugarEjecucion.IdContratoMaestro = vDataReaderResults["IdContratoMaestro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoMaestro"].ToString()) : vLugarEjecucion.IdContratoMaestro;
                            vLugarEjecucion.IdContratoAsociado = vDataReaderResults["IdContratoAsociado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoAsociado"].ToString()) : vLugarEjecucion.IdContratoAsociado;
                            vLugarEjecucion.IdPersonaAbogado = vDataReaderResults["IdAbogadoResponsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAbogadoResponsable"].ToString()) : vLugarEjecucion.IdPersonaAbogado;
                            vLugarEjecucion.FechaAsociacion = vDataReaderResults["FechaAsociacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsociacion"].ToString()) : vLugarEjecucion.FechaAsociacion;
                            vLugarEjecucion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucion.UsuarioCrea;
                            vLugarEjecucion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucion.FechaCrea;
                            vLugarEjecucion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucion.UsuarioModifica;
                            vLugarEjecucion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucion.FechaModifica;
                            vListaRelacionContrato.Add(vLugarEjecucion);
                        }
                        return vListaRelacionContrato;
                    }
                    
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por NumeroContrato para la entidad RelacionContrato
        /// </summary>
        /// <param name="pNumeroContrato"></param>
        /// <returns></returns>
        public List<RelacionContrato> ConsultarRelacionsContrato(string pNumeroContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_RelacionContratos_Consultars"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pNumeroContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RelacionContrato> vListaRelacionContrato = new List<RelacionContrato>();
                        while (vDataReaderResults.Read())
                        {
                            RelacionContrato vLugarEjecucion = new RelacionContrato();
                            vLugarEjecucion.IdContratoMaestro = vDataReaderResults["IdContratoMaestro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoMaestro"].ToString()) : vLugarEjecucion.IdContratoMaestro;
                            vLugarEjecucion.IdContratoAsociado = vDataReaderResults["IdContratoAsociado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoAsociado"].ToString()) : vLugarEjecucion.IdContratoAsociado;
                            vLugarEjecucion.IdPersonaAbogado = vDataReaderResults["IdAbogadoResponsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAbogadoResponsable"].ToString()) : vLugarEjecucion.IdPersonaAbogado;
                            vLugarEjecucion.FechaAsociacion = vDataReaderResults["FechaAsociacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsociacion"].ToString()) : vLugarEjecucion.FechaAsociacion;
                            vLugarEjecucion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucion.UsuarioCrea;
                            vLugarEjecucion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucion.FechaCrea;
                            vLugarEjecucion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucion.UsuarioModifica;
                            vLugarEjecucion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucion.FechaModifica;
                            vListaRelacionContrato.Add(vLugarEjecucion);
                        }
                        return vListaRelacionContrato;
                    }

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad RelacionContrato
        /// </summary>
        /// <param name="pIdContratoAsociado"></param>
        /// <returns></returns>
        public Contrato.Entity.Contrato ConsultarContratoMaestro(int pIdContratoAsociado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Consultar_ContratoMaestro"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdContratoAsociado", DbType.Int32, pIdContratoAsociado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                        while (vDataReaderResults.Read())
                        {
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vContrato.FechaRegistro;
                            vContrato.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vContrato.NumeroProceso;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.FechaAdjudicacion = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vContrato.FechaAdjudicacion;
                            vContrato.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vContrato.IdModalidad;
                            vContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vContrato.IdCategoriaContrato;
                            vContrato.IdCodigoModalidad = vDataReaderResults["IdCodigoModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCodigoModalidad"].ToString()) : vContrato.IdCodigoModalidad;
                            vContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vContrato.IdTipoContrato;
                            vContrato.RequiereActa = vDataReaderResults["RequiereActa"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereActa"].ToString()) : vContrato.RequiereActa;
                            vContrato.ManejaAportes = vDataReaderResults["ManejaAportes"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaAportes"].ToString()) : vContrato.ManejaAportes;
                            vContrato.ManejaRecursos = vDataReaderResults["ManejaRecursos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaRecursos"].ToString()) : vContrato.ManejaRecursos;
                            vContrato.ManejaVigenciasFuturas = vDataReaderResults["ManejaVigenciasFuturas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaVigenciasFuturas"].ToString()) : vContrato.ManejaVigenciasFuturas;
                            vContrato.IdRegimenContratacion = vDataReaderResults["IdRegimenContratacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegimenContratacion"].ToString()) : vContrato.IdRegimenContratacion;
                            vContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoRegional"].ToString()) : vContrato.CodigoRegional;
                            vContrato.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSolicitante"].ToString()) : vContrato.NombreSolicitante;
                            vContrato.DependenciaSolicitante = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vContrato.DependenciaSolicitante;
                            vContrato.CargoSolicitante = vDataReaderResults["CargoSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSolicitante"].ToString()) : vContrato.CargoSolicitante;
                            vContrato.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : vContrato.ObjetoContrato;
                            vContrato.AlcanceObjetoContrato = vDataReaderResults["AlcanceObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AlcanceObjetoContrato"].ToString()) : vContrato.AlcanceObjetoContrato;
                            vContrato.ValorInicialContrato = vDataReaderResults["ValorInicialContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialContrato"].ToString()) : vContrato.ValorInicialContrato;
                            vContrato.ValorTotalAdiciones = vDataReaderResults["ValorTotalAdiciones"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalAdiciones"].ToString()) : vContrato.ValorTotalAdiciones;
                            vContrato.ValorFinalContrato = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinalContrato;
                            vContrato.ValorAportesICBF = vDataReaderResults["ValorAportesICBF"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesICBF"].ToString()) : vContrato.ValorAportesICBF;
                            vContrato.ValorAportesOperador = vDataReaderResults["ValorAportesOperador"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAportesOperador"].ToString()) : vContrato.ValorAportesOperador;
                            vContrato.ValorTotalReduccion = vDataReaderResults["ValorTotalReduccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalReduccion"].ToString()) : vContrato.ValorTotalReduccion;
                            vContrato.JustificacionAdicionSuperior50porc = vDataReaderResults["JustificacionAdicionSuperior50porc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["JustificacionAdicionSuperior50porc"].ToString()) : vContrato.JustificacionAdicionSuperior50porc;
                            vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContrato.FechaSuscripcion;
                            vContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContrato.FechaInicioEjecucion;
                            vContrato.FechaFinalizacionIniciaContrato = vDataReaderResults["FechaFinalizacionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionInicial"].ToString()) : vContrato.FechaFinalizacionIniciaContrato;
                            vContrato.PlazoInicial = vDataReaderResults["PlazoInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PlazoInicial"].ToString()) : vContrato.PlazoInicial;
                            vContrato.FechaInicialTerminacion = vDataReaderResults["FechaInicialTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicialTerminacion"].ToString()) : vContrato.FechaInicialTerminacion;
                            vContrato.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacion"].ToString()) : vContrato.FechaFinalTerminacionContrato;
                            vContrato.FechaProyectadaLiquidacion = vDataReaderResults["FechaProyectadaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaProyectadaLiquidacion"].ToString()) : vContrato.FechaProyectadaLiquidacion;
                            vContrato.FechaAnulacion = vDataReaderResults["FechaAnulacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAnulacion"].ToString()) : vContrato.FechaAnulacion;
                            vContrato.Prorrogas = vDataReaderResults["Prorrogas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Prorrogas"].ToString()) : vContrato.Prorrogas;
                            vContrato.PlazoTotal = vDataReaderResults["PlazoTotal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PlazoTotal"].ToString()) : vContrato.PlazoTotal;
                            vContrato.FechaFirmaActaInicio = vDataReaderResults["FechaFirmaActaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFirmaActaInicio"].ToString()) : vContrato.FechaFirmaActaInicio;
                            vContrato.VigenciaFiscalInicial = vDataReaderResults["VigenciaFiscalInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalInicial"].ToString()) : vContrato.VigenciaFiscalInicial;
                            vContrato.VigenciaFiscalFinal = vDataReaderResults["VigenciaFiscalFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalFinal"].ToString()) : vContrato.VigenciaFiscalFinal;
                            vContrato.IdUnidadEjecucion = vDataReaderResults["IdUnidadEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUnidadEjecucion"].ToString()) : vContrato.IdUnidadEjecucion;
                            vContrato.IdLugarEjecucion = vDataReaderResults["IdLugarEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucion"].ToString()) : vContrato.IdLugarEjecucion;
                            vContrato.DatosAdicionales = vDataReaderResults["DatosAdicionales"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DatosAdicionales"].ToString()) : vContrato.DatosAdicionales;
                            vContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vContrato.IdEstadoContrato;
                            vContrato.IdTipoDocumentoContratista = vDataReaderResults["IdTipoDocumentoContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoDocumentoContratista"].ToString()) : vContrato.IdTipoDocumentoContratista;
                            vContrato.IdentificacionContratista = vDataReaderResults["IdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionContratista"].ToString()) : vContrato.IdentificacionContratista;
                            vContrato.NombreContratista = vDataReaderResults["NombreContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContratista"].ToString()) : vContrato.NombreContratista;
                            vContrato.IdFormaPago = vDataReaderResults["IdFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaPago"].ToString()) : vContrato.IdFormaPago;
                            vContrato.IdTipoEntidad = vDataReaderResults["IdTipoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidad"].ToString()) : vContrato.IdTipoEntidad;
                            vContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContrato.UsuarioCrea;
                            vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            vContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContrato.UsuarioModifica;
                            vContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContrato.FechaModifica;
                            vContrato.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContrato.IdRegionalContrato;
                            vContrato.ClaseContrato = vDataReaderResults["ClaseContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseContrato"].ToString()) : vContrato.ClaseContrato;
                            vContrato.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consecutivo"].ToString()) : vContrato.Consecutivo;
                            vContrato.AfectaPlanCompras = vDataReaderResults["AfectaPlanCompras"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AfectaPlanCompras"].ToString()) : vContrato.AfectaPlanCompras;
                            vContrato.IdSolicitante = vDataReaderResults["IdSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitante"].ToString()) : vContrato.IdSolicitante;
                            vContrato.IdProducto = vDataReaderResults["IdProducto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProducto"].ToString()) : vContrato.IdProducto;
                            vContrato.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLiquidacion"].ToString()) : vContrato.FechaLiquidacion;
                            vContrato.NumeroDocumentoVigenciaFutura = vDataReaderResults["NumeroDocumentoVigenciaFutura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroDocumentoVigenciaFutura"].ToString()) : vContrato.NumeroDocumentoVigenciaFutura;
                            vContrato.RequiereGarantias = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"].ToString()) : vContrato.RequiereGarantias;
                        }
                        return vContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
 
        }
    }
}
