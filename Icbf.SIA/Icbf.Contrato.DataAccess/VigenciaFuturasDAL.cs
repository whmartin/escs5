using System;
using System.Collections.Generic;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad VigenciaFuturas
    /// </summary>
    public class VigenciaFuturasDAL : GeneralDAL
    {
        public VigenciaFuturasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int InsertarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDVigenciaFuturas", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRadicado", DbType.String, pVigenciaFuturas.NumeroRadicado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicion", DbType.DateTime, pVigenciaFuturas.FechaExpedicion);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pVigenciaFuturas.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorVigenciaFutura", DbType.Decimal, pVigenciaFuturas.ValorVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.Int32, pVigenciaFuturas.AnioVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pVigenciaFuturas.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pVigenciaFuturas.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pVigenciaFuturas.IDVigenciaFuturas = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDVigenciaFuturas").ToString());
                    GenerarLogAuditoria(pVigenciaFuturas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarVigenciaFuturasModificacionContrato(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_InsertarModificacionContrato"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDVigenciaFuturas", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRadicado", DbType.String, pVigenciaFuturas.NumeroRadicado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicion", DbType.DateTime, pVigenciaFuturas.FechaExpedicion);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pVigenciaFuturas.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorVigenciaFutura", DbType.Decimal, pVigenciaFuturas.ValorVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.Int32, pVigenciaFuturas.AnioVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pVigenciaFuturas.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pVigenciaFuturas.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pVigenciaFuturas.IDVigenciaFuturas = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDVigenciaFuturas").ToString());
                    GenerarLogAuditoria(pVigenciaFuturas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int ModificarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDVigenciaFuturas", DbType.Int32, pVigenciaFuturas.IDVigenciaFuturas);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRadicado", DbType.String, pVigenciaFuturas.NumeroRadicado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicion", DbType.DateTime, pVigenciaFuturas.FechaExpedicion);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pVigenciaFuturas.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorVigenciaFutura", DbType.Decimal, pVigenciaFuturas.ValorVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pVigenciaFuturas.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pVigenciaFuturas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int EliminarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDVigenciaFuturas", DbType.Int32, pVigenciaFuturas.IDVigenciaFuturas);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pVigenciaFuturas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public int EliminarVigenciaFuturasContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_EliminarContrato"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public int EliminarVigenciaFuturasModificacionContratoTodas(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_EliminarModificacionContratoTodas"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public int EliminarVigenciaFuturasModificacionContrato(int @IDVigenciaFuturas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_EliminarModificacionContrato"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDVigenciaFuturas", DbType.Int32, @IDVigenciaFuturas);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pIDVigenciaFuturas"></param>
        public VigenciaFuturas ConsultarVigenciaFuturas(int pIDVigenciaFuturas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDVigenciaFuturas", DbType.Int32, pIDVigenciaFuturas);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        VigenciaFuturas vVigenciaFuturas = new VigenciaFuturas();
                        while (vDataReaderResults.Read())
                        {
                            vVigenciaFuturas.IDVigenciaFuturas = vDataReaderResults["IDVigenciaFuturas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDVigenciaFuturas"].ToString()) : vVigenciaFuturas.IDVigenciaFuturas;
                            vVigenciaFuturas.NumeroRadicado = vDataReaderResults["NumeroRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicado"].ToString()) : vVigenciaFuturas.NumeroRadicado;
                            vVigenciaFuturas.FechaExpedicion = vDataReaderResults["FechaExpedicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicion"].ToString()) : vVigenciaFuturas.FechaExpedicion;
                            vVigenciaFuturas.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vVigenciaFuturas.IdContrato;
                            vVigenciaFuturas.ValorVigenciaFutura = vDataReaderResults["ValorVigenciaFutura"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorVigenciaFutura"].ToString()) : vVigenciaFuturas.ValorVigenciaFutura;
                            vVigenciaFuturas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVigenciaFuturas.UsuarioCrea;
                            vVigenciaFuturas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVigenciaFuturas.FechaCrea;
                            vVigenciaFuturas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVigenciaFuturas.UsuarioModifica;
                            vVigenciaFuturas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVigenciaFuturas.FechaModifica;
                        }
                        return vVigenciaFuturas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pNumeroRadicado"></param>
        /// <param name="pFechaExpedicion"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pValorVigenciaFutura"></param>
        public List<VigenciaFuturas> ConsultarVigenciaFuturass(String pNumeroRadicado, DateTime? pFechaExpedicion, int? pIdContrato, Decimal? pValorVigenciaFutura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturass_Consultar"))
                {
                    if (pNumeroRadicado != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroRadicado", DbType.String, pNumeroRadicado);
                    if (pFechaExpedicion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaExpedicion", DbType.DateTime, pFechaExpedicion);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pValorVigenciaFutura != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorVigenciaFutura", DbType.Decimal, pValorVigenciaFutura);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<VigenciaFuturas> vListaVigenciaFuturas = new List<VigenciaFuturas>();
                        while (vDataReaderResults.Read())
                        {
                            VigenciaFuturas vVigenciaFuturas = new VigenciaFuturas();
                            vVigenciaFuturas.IDVigenciaFuturas = vDataReaderResults["IDVigenciaFuturas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDVigenciaFuturas"].ToString()) : vVigenciaFuturas.IDVigenciaFuturas;
                            vVigenciaFuturas.NumeroRadicado = vDataReaderResults["NumeroRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicado"].ToString()) : vVigenciaFuturas.NumeroRadicado;
                            vVigenciaFuturas.FechaExpedicion = vDataReaderResults["FechaExpedicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicion"].ToString()) : vVigenciaFuturas.FechaExpedicion;
                            vVigenciaFuturas.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vVigenciaFuturas.IdContrato;
                            vVigenciaFuturas.ValorVigenciaFutura = vDataReaderResults["ValorVigenciaFutura"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorVigenciaFutura"].ToString()) : vVigenciaFuturas.ValorVigenciaFutura;
                            vVigenciaFuturas.AnioVigencia = vDataReaderResults["AnioVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnioVigencia"].ToString()) : vVigenciaFuturas.AnioVigencia;
                            vVigenciaFuturas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vVigenciaFuturas.Estado;
                            vVigenciaFuturas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVigenciaFuturas.UsuarioCrea;
                            vVigenciaFuturas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVigenciaFuturas.FechaCrea;
                            vVigenciaFuturas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVigenciaFuturas.UsuarioModifica;
                            vVigenciaFuturas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVigenciaFuturas.FechaModifica;
                            vListaVigenciaFuturas.Add(vVigenciaFuturas);
                        }
                        return vListaVigenciaFuturas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public VigenciaFuturas ConsultarVigenciaFuturasporContrato(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturasporContrato_Consultar"))
                {                
                   vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);    
                                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        VigenciaFuturas vVigenciaFuturas = new VigenciaFuturas();
                        while (vDataReaderResults.Read())
                        {                            
                            vVigenciaFuturas.IDVigenciaFuturas = vDataReaderResults["IDVigenciaFuturas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDVigenciaFuturas"].ToString()) : vVigenciaFuturas.IDVigenciaFuturas;
                            vVigenciaFuturas.NumeroRadicado = vDataReaderResults["NumeroRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicado"].ToString()) : vVigenciaFuturas.NumeroRadicado;
                            vVigenciaFuturas.FechaExpedicion = vDataReaderResults["FechaExpedicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicion"].ToString()) : vVigenciaFuturas.FechaExpedicion;
                            vVigenciaFuturas.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vVigenciaFuturas.IdContrato;
                            vVigenciaFuturas.ValorVigenciaFutura = vDataReaderResults["ValorVigenciaFutura"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorVigenciaFutura"].ToString()) : vVigenciaFuturas.ValorVigenciaFutura;
                            vVigenciaFuturas.AnioVigencia = vDataReaderResults["AnioVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnioVigencia"].ToString()) : vVigenciaFuturas.AnioVigencia;
                            vVigenciaFuturas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vVigenciaFuturas.Estado;                         
                            
                        }
                        return vVigenciaFuturas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Entity.Contrato> ConsultarContratosVigenciasFuturas(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, string pNumeroContratoConvenio, string pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Consultar_Contratos_VigenciasFuturas"))
                {
                    if (pFechaRegistroDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroDesde", DbType.DateTime, pFechaRegistroDesde);
                    if (pFechaRegistroHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroHasta", DbType.DateTime, pFechaRegistroHasta);
                    if (pNumeroContratoConvenio != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContratoConvenio", DbType.String, pNumeroContratoConvenio);
                    if (pVigenciaFiscalInicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalInicial", DbType.String, pVigenciaFiscalInicial);
                    if (pRegionalContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegionalContrato", DbType.Int32, pRegionalContrato);
                    if (pCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CategoriaContrato", DbType.Int32, pCategoriaContrato);
                    if (pTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoContrato", DbType.Int32, pTipoContrato);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Contrato.Entity.Contrato> vListaContrato = new List<Contrato.Entity.Contrato>();
                        while (vDataReaderResults.Read())
                        {
                            Contrato.Entity.Contrato vContrato = new Contrato.Entity.Contrato();
                            
                            vContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContrato.FechaCrea;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContrato.NumeroContrato;
                            vContrato.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vContrato.AcnoVigencia;
                            vContrato.AcnoVigenciaFinal = vDataReaderResults["AcnoVigenciaFinal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigenciaFinal"].ToString()) : vContrato.AcnoVigenciaFinal;
                            vContrato.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContrato.NombreRegional;
                            vContrato.NombreCategoriaContrato = vDataReaderResults["NombreCategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCategoriaContrato"].ToString()) : vContrato.NombreCategoriaContrato;
                            vContrato.NombreTipoContrato = vDataReaderResults["NombreTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoContrato"].ToString()) : vContrato.NombreTipoContrato;
                            vContrato.RequiereGarantia = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"].ToString()) : vContrato.RequiereGarantia;
                            vContrato.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vContrato.IdRegionalContrato;
                            vContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContrato.EstadoContrato;
                            vListaContrato.Add(vContrato);
                        }
                        return vListaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public VigenciaFuturas ConsultarVigenciaFuturasContrato(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDVigenciaFuturas", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        VigenciaFuturas vVigenciaFuturas = new VigenciaFuturas();
                        while (vDataReaderResults.Read())
                        {
                            vVigenciaFuturas.IDVigenciaFuturas = vDataReaderResults["IDVigenciaFuturas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDVigenciaFuturas"].ToString()) : vVigenciaFuturas.IDVigenciaFuturas;
                            vVigenciaFuturas.NumeroRadicado = vDataReaderResults["NumeroRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRadicado"].ToString()) : vVigenciaFuturas.NumeroRadicado;
                            vVigenciaFuturas.FechaExpedicion = vDataReaderResults["FechaExpedicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicion"].ToString()) : vVigenciaFuturas.FechaExpedicion;
                            vVigenciaFuturas.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vVigenciaFuturas.IdContrato;
                            vVigenciaFuturas.ValorVigenciaFutura = vDataReaderResults["ValorVigenciaFutura"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorVigenciaFutura"].ToString()) : vVigenciaFuturas.ValorVigenciaFutura;
                            vVigenciaFuturas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVigenciaFuturas.UsuarioCrea;
                            vVigenciaFuturas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVigenciaFuturas.FechaCrea;
                            vVigenciaFuturas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVigenciaFuturas.UsuarioModifica;
                            vVigenciaFuturas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVigenciaFuturas.FechaModifica;
                        }
                        return vVigenciaFuturas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
