using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class SuspensionesDAL : GeneralDAL
    {
        public SuspensionesDAL()
        {
        }
        public int InsertarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Suspensiones_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSuspension", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pSuspensiones.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pSuspensiones.FechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pSuspensiones.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@ValorConmutado", DbType.Int32, pSuspensiones.ValorConmutado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSuspensiones.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSuspensiones.IdSuspension = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSuspension").ToString());
                    GenerarLogAuditoria(pSuspensiones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Suspensiones_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSuspension", DbType.Int32, pSuspensiones.IdSuspension);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pSuspensiones.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pSuspensiones.FechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pSuspensiones.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@ValorConmutado", DbType.Int32, pSuspensiones.ValorConmutado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSuspensiones.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Anios", DbType.Int32, pSuspensiones.Suspensionyear);
                    vDataBase.AddInParameter(vDbCommand, "@Mes", DbType.Int32, pSuspensiones.SuspensionMeses);
                    vDataBase.AddInParameter(vDbCommand, "@Dia", DbType.Int32, pSuspensiones.SuspensionDias);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSuspensiones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int AplicarSuspensiones(int pIdSuspension)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Suspensiones_Aplicar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSuspension", DbType.Int32, pIdSuspension);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Suspensiones_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSuspension", DbType.Int32, pSuspensiones.IdSuspension);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSuspensiones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public Suspensiones ConsultarSuspensiones(int pIdSuspension)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Suspensiones_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSuspension", DbType.Int32, pIdSuspension);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Suspensiones vSuspensiones = new Suspensiones();
                        while (vDataReaderResults.Read())
                        {
                            vSuspensiones.IdSuspension = vDataReaderResults["IdSuspension"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSuspension"].ToString()) : vSuspensiones.IdSuspension;
                            vSuspensiones.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSuspensiones.FechaInicio;
                            vSuspensiones.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vSuspensiones.FechaFin;
                            vSuspensiones.ReinicioContrato = vDataReaderResults["ReinicioContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["ReinicioContrato"].ToString()) : vSuspensiones.ReinicioContrato;
                            vSuspensiones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vSuspensiones.IDDetalleConsModContractual;
                            vSuspensiones.Suspensionyear = vDataReaderResults["Suspensionyear"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Suspensionyear"].ToString()) : vSuspensiones.Suspensionyear;
                            vSuspensiones.SuspensionMeses = vDataReaderResults["SuspensionMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["SuspensionMeses"].ToString()) : vSuspensiones.SuspensionMeses;
                            vSuspensiones.SuspensionDias = vDataReaderResults["SuspensionDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["SuspensionDias"].ToString()) : vSuspensiones.SuspensionDias;
                            vSuspensiones.ValorConmutado = vDataReaderResults["ValorConmutado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ValorConmutado"].ToString()) : vSuspensiones.ValorConmutado;
                            vSuspensiones.ValorSuspencion = vDataReaderResults["ValorSuspencion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorSuspencion"].ToString()) : vSuspensiones.ValorSuspencion;
                            vSuspensiones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSuspensiones.UsuarioCrea;
                            vSuspensiones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSuspensiones.FechaCrea;
                            vSuspensiones.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vSuspensiones.FechaSolicitud;
                            vSuspensiones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSuspensiones.UsuarioModifica;
                            vSuspensiones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSuspensiones.FechaModifica;
                        }
                        return vSuspensiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Suspensiones Suspensiones_DetalleConsModContractual_Consultar(int pIdDetalleConsModContractualn)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Suspensiones_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pIdDetalleConsModContractualn);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Suspensiones vSuspensiones = new Suspensiones();
                        while (vDataReaderResults.Read())
                        {
                            vSuspensiones.IdSuspension = vDataReaderResults["IdSuspension"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSuspension"].ToString()) : vSuspensiones.IdSuspension;
                            
                        }
                        return vSuspensiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Suspensiones ConsultarCalculosSuspensiones(int pIdContrato, DateTime pFechaInicio, DateTime pFechaFin, int pValorConmutado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_CONTRATO_Suspenciones_FechaSuspension_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pFechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@ValorConmutado", DbType.Int32, pValorConmutado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Suspensiones vSuspensiones = new Suspensiones();
                        while (vDataReaderResults.Read())
                        {
                            vSuspensiones.ValorSuspencion = vDataReaderResults["ValorSuspencion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorSuspencion"].ToString()) : vSuspensiones.ValorSuspencion;
                            vSuspensiones.ReinicioContrato = vDataReaderResults["ReinicioContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["ReinicioContrato"].ToString()) : vSuspensiones.ReinicioContrato;
                            vSuspensiones.Suspensionyear = vDataReaderResults["Suspensionyear"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Suspensionyear"].ToString()) : vSuspensiones.Suspensionyear;
                            vSuspensiones.SuspensionMeses = vDataReaderResults["SuspensionMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["SuspensionMeses"].ToString()) : vSuspensiones.SuspensionMeses;
                            vSuspensiones.SuspensionDias = vDataReaderResults["SuspensionDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["SuspensionDias"].ToString()) : vSuspensiones.SuspensionDias;
                        }
                        return vSuspensiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Suspensiones> ConsultarSuspensioness(DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Suspensioness_Consultar"))
                {
                    if(pFechaInicio != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFechaInicio);
                    if(pFechaFin != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pFechaFin);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Suspensiones> vListaSuspensiones = new List<Suspensiones>();
                        while (vDataReaderResults.Read())
                        {
                                Suspensiones vSuspensiones = new Suspensiones();
                                vSuspensiones.IdSuspension = vDataReaderResults["IdSuspension"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSuspension"].ToString()) : vSuspensiones.IdSuspension;
                                vSuspensiones.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSuspensiones.FechaInicio;
                                vSuspensiones.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vSuspensiones.FechaFin;
                                vSuspensiones.ReinicioContrato = vDataReaderResults["ReinicioContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["ReinicioContrato"].ToString()) : vSuspensiones.ReinicioContrato;
                                vSuspensiones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vSuspensiones.IDDetalleConsModContractual;
                                vSuspensiones.Suspensionyear = vDataReaderResults["Suspensionyear"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Suspensionyear"].ToString()) : vSuspensiones.Suspensionyear;
                                vSuspensiones.SuspensionMeses = vDataReaderResults["SuspensionMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["SuspensionMeses"].ToString()) : vSuspensiones.SuspensionMeses;
                                vSuspensiones.SuspensionDias = vDataReaderResults["SuspensionDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["SuspensionDias"].ToString()) : vSuspensiones.SuspensionDias;
                                vSuspensiones.ValorConmutado = vDataReaderResults["ValorConmutado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ValorConmutado"].ToString()) : vSuspensiones.ValorConmutado;
                                vSuspensiones.ValorSuspencion = vDataReaderResults["ValorSuspencion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorSuspencion"].ToString()) : vSuspensiones.ValorSuspencion;
                                vSuspensiones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSuspensiones.UsuarioCrea;
                                vSuspensiones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSuspensiones.FechaCrea;
                                vSuspensiones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSuspensiones.UsuarioModifica;
                                vSuspensiones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSuspensiones.FechaModifica;
                                vListaSuspensiones.Add(vSuspensiones);
                        }
                        return vListaSuspensiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Suspensiones ConsultarSuspensionesPorDetalle(int? pDetalleConModificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_CONTRATO_Suspenciones_Modificacion_Consultar"))
                {

                    if (pDetalleConModificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pDetalleConModificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Suspensiones vSuspensiones = new Suspensiones();
                        while (vDataReaderResults.Read())
                        {
                            vSuspensiones.IdSuspension = vDataReaderResults["IdSuspension"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSuspension"].ToString()) : vSuspensiones.IdSuspension;
                            vSuspensiones.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vSuspensiones.IDCosModContractual;
                            vSuspensiones.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vSuspensiones.IDContrato;
                            vSuspensiones.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSuspensiones.FechaInicio;
                            vSuspensiones.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vSuspensiones.FechaFin;
                            vSuspensiones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vSuspensiones.IDDetalleConsModContractual;
                            vSuspensiones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSuspensiones.UsuarioCrea;
                            vSuspensiones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSuspensiones.FechaCrea;
                            vSuspensiones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSuspensiones.UsuarioModifica;
                            vSuspensiones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSuspensiones.FechaModifica;
                            //vListaSuspensiones.Add(vSuspensiones);
                        }
                        return vSuspensiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Suspensiones> ConsultarSuspensionesPorDetalleModificacion(int? pDetalleConModificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_CONTRATO_Suspenciones_Modificacion_Consultar"))
                {

                    if (pDetalleConModificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pDetalleConModificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Suspensiones> vListaSuspensiones = new List<Suspensiones>();
                        while (vDataReaderResults.Read())
                        {
                            Suspensiones vSuspensiones = new Suspensiones();
                            vSuspensiones.IdSuspension = vDataReaderResults["IdSuspension"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSuspension"].ToString()) : vSuspensiones.IdSuspension;
                            vSuspensiones.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vSuspensiones.IDCosModContractual;
                            vSuspensiones.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vSuspensiones.IDContrato;
                            vSuspensiones.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSuspensiones.FechaInicio;
                            vSuspensiones.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vSuspensiones.FechaFin;
                            vSuspensiones.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vSuspensiones.IDDetalleConsModContractual;
                            vSuspensiones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSuspensiones.UsuarioCrea;
                            vSuspensiones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSuspensiones.FechaCrea;
                            vSuspensiones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSuspensiones.UsuarioModifica;
                            vSuspensiones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSuspensiones.FechaModifica;
                            vListaSuspensiones.Add(vSuspensiones);
                        }
                        return vListaSuspensiones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
