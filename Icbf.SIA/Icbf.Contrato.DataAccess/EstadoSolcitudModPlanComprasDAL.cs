﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad EstadoSolcitudModPlanCompras
    /// </summary>
    public class EstadoSolcitudModPlanComprasDAL : GeneralDAL
    {
        public EstadoSolcitudModPlanComprasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int InsertarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_EstadoSolcitudModPlanCompras_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEstadoSolicitud", DbType.String, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, pEstadoSolcitudModPlanCompras.CodEstado);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoSolcitudModPlanCompras.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pEstadoSolcitudModPlanCompras.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEstadoSolcitudModPlanCompras.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEstadoSolcitudModPlanCompras.IdEstadoSolicitud = Convert.ToString(vDataBase.GetParameterValue(vDbCommand, "@IdEstadoSolicitud").ToString());
                    GenerarLogAuditoria(pEstadoSolcitudModPlanCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int ModificarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_EstadoSolcitudModPlanCompras_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoSolicitud", DbType.String, pEstadoSolcitudModPlanCompras.IdEstadoSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, pEstadoSolcitudModPlanCompras.CodEstado);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoSolcitudModPlanCompras.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pEstadoSolcitudModPlanCompras.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEstadoSolcitudModPlanCompras.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoSolcitudModPlanCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int EliminarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_EstadoSolcitudModPlanCompras_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoSolicitud", DbType.String, pEstadoSolcitudModPlanCompras.IdEstadoSolicitud);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoSolcitudModPlanCompras, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pIdEstadoSolicitud"></param>
        public EstadoSolcitudModPlanCompras ConsultarEstadoSolcitudModPlanCompras(String pIdEstadoSolicitud)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_EstadoSolcitudModPlanCompras_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoSolicitud", DbType.String, pIdEstadoSolicitud);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoSolcitudModPlanCompras vEstadoSolcitudModPlanCompras = new EstadoSolcitudModPlanCompras();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoSolcitudModPlanCompras.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vEstadoSolcitudModPlanCompras.IdEstadoSolicitud;
                            vEstadoSolcitudModPlanCompras.CodEstado = vDataReaderResults["CodEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodEstado"].ToString()) : vEstadoSolcitudModPlanCompras.CodEstado;
                            vEstadoSolcitudModPlanCompras.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoSolcitudModPlanCompras.Descripcion;
                            vEstadoSolcitudModPlanCompras.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEstadoSolcitudModPlanCompras.Activo;
                            vEstadoSolcitudModPlanCompras.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoSolcitudModPlanCompras.UsuarioCrea;
                            vEstadoSolcitudModPlanCompras.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoSolcitudModPlanCompras.FechaCrea;
                            vEstadoSolcitudModPlanCompras.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoSolcitudModPlanCompras.UsuarioModifica;
                            vEstadoSolcitudModPlanCompras.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoSolcitudModPlanCompras.FechaModifica;
                        }
                        return vEstadoSolcitudModPlanCompras;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pCodEstado"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pActivo"></param>
        public List<EstadoSolcitudModPlanCompras> ConsultarEstadoSolcitudModPlanComprass(String pCodEstado, String pDescripcion, Boolean? pActivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_EstadoSolcitudModPlanComprass_Consultar"))
                {
                    if (pCodEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, pCodEstado);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pActivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pActivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoSolcitudModPlanCompras> vListaEstadoSolcitudModPlanCompras = new List<EstadoSolcitudModPlanCompras>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoSolcitudModPlanCompras vEstadoSolcitudModPlanCompras = new EstadoSolcitudModPlanCompras();
                            vEstadoSolcitudModPlanCompras.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vEstadoSolcitudModPlanCompras.IdEstadoSolicitud;
                            vEstadoSolcitudModPlanCompras.CodEstado = vDataReaderResults["CodEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodEstado"].ToString()) : vEstadoSolcitudModPlanCompras.CodEstado;
                            vEstadoSolcitudModPlanCompras.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoSolcitudModPlanCompras.Descripcion;
                            vEstadoSolcitudModPlanCompras.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEstadoSolcitudModPlanCompras.Activo;
                            vEstadoSolcitudModPlanCompras.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoSolcitudModPlanCompras.UsuarioCrea;
                            vEstadoSolcitudModPlanCompras.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoSolcitudModPlanCompras.FechaCrea;
                            vEstadoSolcitudModPlanCompras.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoSolcitudModPlanCompras.UsuarioModifica;
                            vEstadoSolcitudModPlanCompras.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoSolcitudModPlanCompras.FechaModifica;
                            vListaEstadoSolcitudModPlanCompras.Add(vEstadoSolcitudModPlanCompras);
                        }
                        return vListaEstadoSolcitudModPlanCompras;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
