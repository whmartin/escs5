using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Tipo Amparo
    /// </summary>
    public class TipoAmparoDAL : GeneralDAL
    {
        public TipoAmparoDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int InsertarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoAmparo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoAmparo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoAmparo", DbType.String, pTipoAmparo.NombreTipoAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoGarantia", DbType.Int32, pTipoAmparo.IdTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoAmparo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoAmparo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoAmparo.IdTipoAmparo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoAmparo").ToString());
                    GenerarLogAuditoria(pTipoAmparo, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int ModificarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoAmparo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoAmparo", DbType.Int32, pTipoAmparo.IdTipoAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoAmparo", DbType.String, pTipoAmparo.NombreTipoAmparo);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoGarantia", DbType.Int32, pTipoAmparo.IdTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoAmparo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoAmparo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoAmparo, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int EliminarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoAmparo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoAmparo", DbType.Int32, pTipoAmparo.IdTipoAmparo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoAmparo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoAmparo
        /// </summary>
        /// <param name="pIdTipoAmparo"></param>
        /// <returns></returns>        
        public TipoAmparo ConsultarTipoAmparo(int pIdTipoAmparo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoAmparo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoAmparo", DbType.Int32, pIdTipoAmparo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoAmparo vTipoAmparo = new TipoAmparo();
                        while (vDataReaderResults.Read())
                        {
                            vTipoAmparo.IdTipoAmparo = vDataReaderResults["IdTipoAmparo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoAmparo"].ToString()) : vTipoAmparo.IdTipoAmparo;
                            vTipoAmparo.NombreTipoAmparo = vDataReaderResults["NombreTipoAmparo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoAmparo"].ToString()) : vTipoAmparo.NombreTipoAmparo;
                            vTipoAmparo.IdTipoGarantia = vDataReaderResults["IdTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoGarantia"].ToString()) : vTipoAmparo.IdTipoGarantia;
                            vTipoAmparo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoAmparo.Estado;
                            vTipoAmparo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoAmparo.UsuarioCrea;
                            vTipoAmparo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoAmparo.FechaCrea;
                            vTipoAmparo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoAmparo.UsuarioModifica;
                            vTipoAmparo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoAmparo.FechaModifica;
                        }
                        return vTipoAmparo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoAmparo
        /// </summary>
        /// <param name="pNombreTipoAmparo"></param>
        /// <param name="pIdTipoGarantia"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoAmparo> ConsultarTipoAmparos(String pNombreTipoAmparo, int? pIdTipoGarantia, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoAmparos_Consultar"))
                {
                    if (pNombreTipoAmparo != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoAmparo", DbType.String, pNombreTipoAmparo);
                    if (pIdTipoGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoGarantia", DbType.Int32, pIdTipoGarantia);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoAmparo> vListaTipoAmparo = new List<TipoAmparo>();
                        while (vDataReaderResults.Read())
                        {
                            TipoAmparo vTipoAmparo = new TipoAmparo();
                            vTipoAmparo.IdTipoAmparo = vDataReaderResults["IdTipoAmparo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoAmparo"].ToString()) : vTipoAmparo.IdTipoAmparo;
                            vTipoAmparo.NombreTipoAmparo = vDataReaderResults["NombreTipoAmparo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoAmparo"].ToString()) : vTipoAmparo.NombreTipoAmparo;
                            vTipoAmparo.IdTipoGarantia = vDataReaderResults["IdTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoGarantia"].ToString()) : vTipoAmparo.IdTipoGarantia;
                            vTipoAmparo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoAmparo.Estado;
                            vTipoAmparo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoAmparo.UsuarioCrea;
                            vTipoAmparo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoAmparo.FechaCrea;
                            vTipoAmparo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoAmparo.UsuarioModifica;
                            vTipoAmparo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoAmparo.FechaModifica;
                            vListaTipoAmparo.Add(vTipoAmparo);
                        }
                        return vListaTipoAmparo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
