using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Informaci�n presupuestal RP
    /// </summary>
    public class InformacionPresupuestalRPDAL : GeneralDAL
    {
        public InformacionPresupuestalRPDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int InsertarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdInformacionPresupuestalRP", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudRP", DbType.DateTime, pInformacionPresupuestalRP.FechaSolicitudRP);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.String, pInformacionPresupuestalRP.NumeroRP);
                    vDataBase.AddInParameter(vDbCommand, "@ValorRP", DbType.Decimal, pInformacionPresupuestalRP.ValorRP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionRP", DbType.DateTime, pInformacionPresupuestalRP.FechaExpedicionRP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInformacionPresupuestalRP.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pInformacionPresupuestalRP.IdInformacionPresupuestalRP = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdInformacionPresupuestalRP").ToString());
                    GenerarLogAuditoria(pInformacionPresupuestalRP, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int ModificarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionPresupuestalRP", DbType.Int32, pInformacionPresupuestalRP.IdInformacionPresupuestalRP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudRP", DbType.DateTime, pInformacionPresupuestalRP.FechaSolicitudRP);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.String, pInformacionPresupuestalRP.NumeroRP);
                    vDataBase.AddInParameter(vDbCommand, "@ValorRP", DbType.Decimal, pInformacionPresupuestalRP.ValorRP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionRP", DbType.DateTime, pInformacionPresupuestalRP.FechaExpedicionRP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInformacionPresupuestalRP.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInformacionPresupuestalRP, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int EliminarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionPresupuestalRP", DbType.Int32, pInformacionPresupuestalRP.IdInformacionPresupuestalRP);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInformacionPresupuestalRP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pIdInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public InformacionPresupuestalRP ConsultarInformacionPresupuestalRP(int pIdInformacionPresupuestalRP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionPresupuestalRP", DbType.Int32, pIdInformacionPresupuestalRP);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        InformacionPresupuestalRP vInformacionPresupuestalRP = new InformacionPresupuestalRP();
                        while (vDataReaderResults.Read())
                        {
                            vInformacionPresupuestalRP.IdInformacionPresupuestalRP = vDataReaderResults["IdInformacionPresupuestalRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionPresupuestalRP"].ToString()) : vInformacionPresupuestalRP.IdInformacionPresupuestalRP;
                            vInformacionPresupuestalRP.FechaSolicitudRP = vDataReaderResults["FechaSolicitudRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudRP"].ToString()) : vInformacionPresupuestalRP.FechaSolicitudRP;
                            vInformacionPresupuestalRP.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vInformacionPresupuestalRP.NumeroRP;
                            vInformacionPresupuestalRP.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vInformacionPresupuestalRP.ValorRP;
                            vInformacionPresupuestalRP.FechaExpedicionRP = vDataReaderResults["FechaExpedicionRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionRP"].ToString()) : vInformacionPresupuestalRP.FechaExpedicionRP;
                            vInformacionPresupuestalRP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInformacionPresupuestalRP.UsuarioCrea;
                            vInformacionPresupuestalRP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInformacionPresupuestalRP.FechaCrea;
                            vInformacionPresupuestalRP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInformacionPresupuestalRP.UsuarioModifica;
                            vInformacionPresupuestalRP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInformacionPresupuestalRP.FechaModifica;
                        }
                        return vInformacionPresupuestalRP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pFechaSolicitudRP"></param>
        /// <param name="pNumeroRP"></param>
        /// <returns></returns>
        public List<InformacionPresupuestalRP> ConsultarInformacionPresupuestalRPs(DateTime? pFechaSolicitudRP, String pNumeroRP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar"))
                {
                    if(pFechaSolicitudRP != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudRP", DbType.DateTime, pFechaSolicitudRP);
                    if(pNumeroRP != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.String, pNumeroRP);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InformacionPresupuestalRP> vListaInformacionPresupuestalRP = new List<InformacionPresupuestalRP>();
                        while (vDataReaderResults.Read())
                        {
                                InformacionPresupuestalRP vInformacionPresupuestalRP = new InformacionPresupuestalRP();
                            vInformacionPresupuestalRP.IdInformacionPresupuestalRP = vDataReaderResults["IdInformacionPresupuestalRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionPresupuestalRP"].ToString()) : vInformacionPresupuestalRP.IdInformacionPresupuestalRP;
                            vInformacionPresupuestalRP.FechaSolicitudRP = vDataReaderResults["FechaSolicitudRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudRP"].ToString()) : vInformacionPresupuestalRP.FechaSolicitudRP;
                            vInformacionPresupuestalRP.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vInformacionPresupuestalRP.NumeroRP;
                            vInformacionPresupuestalRP.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vInformacionPresupuestalRP.ValorRP;
                            vInformacionPresupuestalRP.FechaExpedicionRP = vDataReaderResults["FechaExpedicionRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionRP"].ToString()) : vInformacionPresupuestalRP.FechaExpedicionRP;
                            vInformacionPresupuestalRP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInformacionPresupuestalRP.UsuarioCrea;
                            vInformacionPresupuestalRP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInformacionPresupuestalRP.FechaCrea;
                            vInformacionPresupuestalRP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInformacionPresupuestalRP.UsuarioModifica;
                            vInformacionPresupuestalRP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInformacionPresupuestalRP.FechaModifica;
                                vListaInformacionPresupuestalRP.Add(vInformacionPresupuestalRP);
                        }
                        return vListaInformacionPresupuestalRP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
