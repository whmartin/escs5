using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class SubComponenteDAL : GeneralDAL
    {
        public SubComponenteDAL()
        {
        }
        public int InsertarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SubComponente_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSubComponente", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSubComponente", DbType.String, pSubComponente.NombreSubComponente);
                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pSubComponente.IdComponente);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSubComponente.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSubComponente.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSubComponente.IdSubComponente = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSubComponente").ToString());
                    GenerarLogAuditoria(pSubComponente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SubComponente_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pSubComponente.IdSubComponente);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSubComponente", DbType.String, pSubComponente.NombreSubComponente);
                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pSubComponente.IdComponente);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSubComponente.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSubComponente.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSubComponente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SubComponente_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pSubComponente.IdSubComponente);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSubComponente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public SubComponente ConsultarSubComponente(int pIdSubComponente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SubComponente_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSubComponente", DbType.Int32, pIdSubComponente);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SubComponente vSubComponente = new SubComponente();
                        while (vDataReaderResults.Read())
                        {
                            vSubComponente.IdSubComponente = vDataReaderResults["IdSubComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubComponente"].ToString()) : vSubComponente.IdSubComponente;
                            vSubComponente.NombreSubComponente = vDataReaderResults["NombreSubComponente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSubComponente"].ToString()) : vSubComponente.NombreSubComponente;
                            vSubComponente.IdComponente = vDataReaderResults["IdComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComponente"].ToString()) : vSubComponente.IdComponente;
                            vSubComponente.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSubComponente.Estado;
                            vSubComponente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSubComponente.UsuarioCrea;
                            vSubComponente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSubComponente.FechaCrea;
                            vSubComponente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSubComponente.UsuarioModifica;
                            vSubComponente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSubComponente.FechaModifica;
                        }
                        return vSubComponente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SubComponente> ConsultarSubComponentes(String pNombreSubComponente, int? pIdComponente, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SubComponentes_Consultar"))
                {
                    if(pNombreSubComponente != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreSubComponente", DbType.String, pNombreSubComponente);
                    if(pIdComponente != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pIdComponente);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SubComponente> vListaSubComponente = new List<SubComponente>();
                        while (vDataReaderResults.Read())
                        {
                                SubComponente vSubComponente = new SubComponente();
                            vSubComponente.IdSubComponente = vDataReaderResults["IdSubComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubComponente"].ToString()) : vSubComponente.IdSubComponente;
                            vSubComponente.NombreSubComponente = vDataReaderResults["NombreSubComponente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSubComponente"].ToString()) : vSubComponente.NombreSubComponente;
                            vSubComponente.IdComponente = vDataReaderResults["IdComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComponente"].ToString()) : vSubComponente.IdComponente;
                            vSubComponente.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSubComponente.Estado;
                            vSubComponente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSubComponente.UsuarioCrea;
                            vSubComponente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSubComponente.FechaCrea;
                            vSubComponente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSubComponente.UsuarioModifica;
                            vSubComponente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSubComponente.FechaModifica;
                                vListaSubComponente.Add(vSubComponente);
                        }
                        return vListaSubComponente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
