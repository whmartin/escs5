using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad RegistroInformacionPresupuestal
    /// </summary>
    public class RegistroInformacionPresupuestalDAL : GeneralDAL
    {
        public RegistroInformacionPresupuestalDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pRegistroInformacionPresupuestal"></param>
        public int InsertarRegistroInformacionPresupuestal(RegistroInformacionPresupuestal pRegistroInformacionPresupuestal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RegistroInformacionPresupuestal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pRegistroInformacionPresupuestal.VigenciaFiscal);
                    vDataBase.AddInParameter(vDbCommand, "@RegionalICBF", DbType.Int32, pRegistroInformacionPresupuestal.RegionalICBF);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.Decimal, pRegistroInformacionPresupuestal.NumeroCDP);
                    vDataBase.AddInParameter(vDbCommand, "@Area", DbType.Int32, pRegistroInformacionPresupuestal.Area);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCDP", DbType.Decimal, pRegistroInformacionPresupuestal.ValorCDP);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalHasta", DbType.Decimal, pRegistroInformacionPresupuestal.ValorTotalHasta);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCDPDesde", DbType.DateTime, pRegistroInformacionPresupuestal.FechaCDP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRegistroInformacionPresupuestal.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRegistroInformacionPresupuestal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pRegistroInformacionPresupuestal"></param>
        public int ModificarRegistroInformacionPresupuestal(RegistroInformacionPresupuestal pRegistroInformacionPresupuestal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RegistroInformacionPresupuestal_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pRegistroInformacionPresupuestal.VigenciaFiscal);
                    vDataBase.AddInParameter(vDbCommand, "@RegionalICBF", DbType.Int32, pRegistroInformacionPresupuestal.RegionalICBF);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.Decimal, pRegistroInformacionPresupuestal.NumeroCDP);
                    vDataBase.AddInParameter(vDbCommand, "@Area", DbType.Int32, pRegistroInformacionPresupuestal.Area);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCDP", DbType.Decimal, pRegistroInformacionPresupuestal.ValorCDP);
                    vDataBase.AddInParameter(vDbCommand, "@ValorTotalHasta", DbType.Decimal, pRegistroInformacionPresupuestal.ValorTotalHasta);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCDP", DbType.DateTime, pRegistroInformacionPresupuestal.FechaCDP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRegistroInformacionPresupuestal.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRegistroInformacionPresupuestal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pRegistroInformacionPresupuestal"></param>
        public int EliminarRegistroInformacionPresupuestal(RegistroInformacionPresupuestal pRegistroInformacionPresupuestal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RegistroInformacionPresupuestal_Eliminar"))
                {
                    int vResultado;
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRegistroInformacionPresupuestal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name=""></param>
        public RegistroInformacionPresupuestal ConsultarRegistroInformacionPresupuestal()
        {
            try
            { 
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RegistroInformacionPresupuestal_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegistroInformacionPresupuestal vRegistroInformacionPresupuestal = new RegistroInformacionPresupuestal();
                        while (vDataReaderResults.Read())
                        {
                            vRegistroInformacionPresupuestal.VigenciaFiscal = vDataReaderResults["VigenciaFiscal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscal"].ToString()) : vRegistroInformacionPresupuestal.VigenciaFiscal;
                            vRegistroInformacionPresupuestal.RegionalICBF = vDataReaderResults["RegionalICBF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.RegionalICBF;
                            vRegistroInformacionPresupuestal.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vRegistroInformacionPresupuestal.NumeroCDP;
                            vRegistroInformacionPresupuestal.Area = vDataReaderResults["Area"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Area"].ToString()) : vRegistroInformacionPresupuestal.Area;
                            vRegistroInformacionPresupuestal.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vRegistroInformacionPresupuestal.ValorCDP;
                            vRegistroInformacionPresupuestal.ValorTotalHasta = vDataReaderResults["ValorTotalHasta"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalHasta"].ToString()) : vRegistroInformacionPresupuestal.ValorTotalHasta;
                            vRegistroInformacionPresupuestal.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vRegistroInformacionPresupuestal.FechaCDP;
                            vRegistroInformacionPresupuestal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroInformacionPresupuestal.UsuarioCrea;
                            vRegistroInformacionPresupuestal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroInformacionPresupuestal.FechaCrea;
                            vRegistroInformacionPresupuestal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroInformacionPresupuestal.UsuarioModifica;
                            vRegistroInformacionPresupuestal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroInformacionPresupuestal.FechaModifica;
                        }
                        return vRegistroInformacionPresupuestal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestals(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, Decimal? pValorTotalDesde, Decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Ppto_RegistroInformacionPresupuestal_Consultar"))
                {
                    vDbCommand.CommandTimeout = 1000;

                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    if (pNumeroCDP != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.String, pNumeroCDP);
                    if(pVigenciaFiscal != null)
                         vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pVigenciaFiscal);
                    if(pRegionalICBF != null)
                         vDataBase.AddInParameter(vDbCommand, "@RegionalICBF", DbType.Int32, pRegionalICBF);
                    if(pArea != null)
                         vDataBase.AddInParameter(vDbCommand, "@Area", DbType.Int32, pArea);
                    if(pValorTotalDesde != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorTotalDesde", DbType.Decimal, pValorTotalDesde);
                    if(pValorTotalHasta != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorTotalHasta", DbType.Decimal, pValorTotalHasta);
                    if(pFechaCDPDesde != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaCDPDesde", DbType.DateTime, Convert.ToDateTime(pFechaCDPDesde).ToShortDateString());

                    if(pFechaCDPHasta != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaCDPHasta", DbType.DateTime, Convert.ToDateTime(pFechaCDPHasta).ToShortDateString());

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroInformacionPresupuestal> vListaRegistroInformacionPresupuestal = new List<RegistroInformacionPresupuestal>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroInformacionPresupuestal vRegistroInformacionPresupuestal = new RegistroInformacionPresupuestal();
                            vRegistroInformacionPresupuestal.VigenciaFiscal = vDataReaderResults["VigenciaFiscal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscal"].ToString()) : vRegistroInformacionPresupuestal.VigenciaFiscal;
                            vRegistroInformacionPresupuestal.RegionalICBF = vDataReaderResults["RegionalICBF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.RegionalICBF;
                            vRegistroInformacionPresupuestal.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vRegistroInformacionPresupuestal.NumeroCDP;
                            vRegistroInformacionPresupuestal.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vRegistroInformacionPresupuestal.ValorCDP;
                            vRegistroInformacionPresupuestal.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vRegistroInformacionPresupuestal.FechaCDP;
                            vRegistroInformacionPresupuestal.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGastos"].ToString()) : vRegistroInformacionPresupuestal.DependenciaAfectacionGastos;
                            vRegistroInformacionPresupuestal.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vRegistroInformacionPresupuestal.TipoFuenteFinanciamiento;
                            vRegistroInformacionPresupuestal.PosicionCatalogoGastos = vDataReaderResults["PosicionCatalogoGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PosicionCatalogoGastos"].ToString()) : vRegistroInformacionPresupuestal.PosicionCatalogoGastos;
                            vRegistroInformacionPresupuestal.RubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vRegistroInformacionPresupuestal.RubroPresupuestal;
                            vRegistroInformacionPresupuestal.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vRegistroInformacionPresupuestal.RecursoPresupuestal;
                            vRegistroInformacionPresupuestal.TipoDocumentoSoporte = vDataReaderResults["TipoDocumentoSoporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoSoporte"].ToString()) : vRegistroInformacionPresupuestal.TipoDocumentoSoporte;
                            vRegistroInformacionPresupuestal.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vRegistroInformacionPresupuestal.TipoSituacionFondos;
                            vRegistroInformacionPresupuestal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroInformacionPresupuestal.UsuarioCrea;
                            vRegistroInformacionPresupuestal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroInformacionPresupuestal.FechaCrea;
                            vRegistroInformacionPresupuestal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroInformacionPresupuestal.UsuarioModifica;
                            vRegistroInformacionPresupuestal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroInformacionPresupuestal.FechaModifica;
                            vRegistroInformacionPresupuestal.NombreRegionalICBF = vDataReaderResults["NombreRegionalICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.NombreRegionalICBF;
                            vRegistroInformacionPresupuestal.IdEtlCDP = vDataReaderResults["IdEtlCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEtlCDP"].ToString()) : vRegistroInformacionPresupuestal.IdEtlCDP;
                            vListaRegistroInformacionPresupuestal.Add(vRegistroInformacionPresupuestal);
                        }
                        return vListaRegistroInformacionPresupuestal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (SqlException ex)
            {


                throw new GenericException(ex);
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        /// <param name="pNumeroRegistros"></param>
        /// <returns></returns>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalSimple(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Ppto_RegistroInformacionPresupuestal_ConsultarSimple"))
                {
                    vDbCommand.CommandTimeout = 1000;

                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    if (pNumeroCDP != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.String, pNumeroCDP);
                    if (pVigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pVigenciaFiscal);
                    if (pRegionalICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegionalICBF", DbType.Int32, pRegionalICBF);
                    if (pArea != null)
                        vDataBase.AddInParameter(vDbCommand, "@Area", DbType.Int32, pArea);
                    if (pValorTotalDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalDesde", DbType.Decimal, pValorTotalDesde);
                    if (pValorTotalHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalHasta", DbType.Decimal, pValorTotalHasta);
                    if (pFechaCDPDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaCDPDesde", DbType.DateTime, Convert.ToDateTime(pFechaCDPDesde).ToShortDateString());

                    if (pFechaCDPHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaCDPHasta", DbType.DateTime, Convert.ToDateTime(pFechaCDPHasta).ToShortDateString());

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroInformacionPresupuestal> vListaRegistroInformacionPresupuestal = new List<RegistroInformacionPresupuestal>();

                        int idVigencia, idregional;
                        string numeroCDP;

                        while (vDataReaderResults.Read())
                        {
                            RegistroInformacionPresupuestal vRegistroInformacionPresupuestal = new RegistroInformacionPresupuestal();
                            idVigencia = vDataReaderResults["VigenciaFiscal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscal"].ToString()) : vRegistroInformacionPresupuestal.VigenciaFiscal;
                            idregional = vDataReaderResults["RegionalICBF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.RegionalICBF;
                            numeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vRegistroInformacionPresupuestal.NumeroCDP;

                            if (! vListaRegistroInformacionPresupuestal.Any( e => e.VigenciaFiscal == idVigencia && e.RegionalICBF == idregional && e.NumeroCDP == numeroCDP ))
                            {
                                vRegistroInformacionPresupuestal.VigenciaFiscal = idVigencia;
                                vRegistroInformacionPresupuestal.RegionalICBF = idregional;
                                vRegistroInformacionPresupuestal.NumeroCDP = numeroCDP;
                                vRegistroInformacionPresupuestal.VigenciaFiscal = vDataReaderResults["VigenciaFiscal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscal"].ToString()) : vRegistroInformacionPresupuestal.VigenciaFiscal;
                                vRegistroInformacionPresupuestal.RegionalICBF = vDataReaderResults["RegionalICBF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.RegionalICBF;
                                vRegistroInformacionPresupuestal.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vRegistroInformacionPresupuestal.NumeroCDP;
                                vRegistroInformacionPresupuestal.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vRegistroInformacionPresupuestal.ValorCDP;
                                vRegistroInformacionPresupuestal.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vRegistroInformacionPresupuestal.FechaCDP;
                                vRegistroInformacionPresupuestal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroInformacionPresupuestal.UsuarioCrea;
                                vRegistroInformacionPresupuestal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroInformacionPresupuestal.FechaCrea;
                                vRegistroInformacionPresupuestal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroInformacionPresupuestal.UsuarioModifica;
                                vRegistroInformacionPresupuestal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroInformacionPresupuestal.FechaModifica;
                                vRegistroInformacionPresupuestal.NombreRegionalICBF = vDataReaderResults["NombreRegionalICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.NombreRegionalICBF;
                                vRegistroInformacionPresupuestal.IdEtlCDP = vDataReaderResults["IdEtlCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEtlCDP"].ToString()) : vRegistroInformacionPresupuestal.IdEtlCDP;
                                vListaRegistroInformacionPresupuestal.Add(vRegistroInformacionPresupuestal);
                            }
                        }
                        return vListaRegistroInformacionPresupuestal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (SqlException ex)
            {


                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        /// <param name="pNumeroRegistros"></param>
        /// <returns></returns>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalUnificado(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Ppto_RegistroInformacionPresupuestal_ConsultarUnificado"))
                {
                    vDbCommand.CommandTimeout = 1000;

                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    if (pNumeroCDP != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.String, pNumeroCDP);
                    if (pVigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pVigenciaFiscal);
                    if (pRegionalICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegionalICBF", DbType.Int32, pRegionalICBF);
                    if (pArea != null)
                        vDataBase.AddInParameter(vDbCommand, "@Area", DbType.Int32, pArea);
                    if (pValorTotalDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalDesde", DbType.Decimal, pValorTotalDesde);
                    if (pValorTotalHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorTotalHasta", DbType.Decimal, pValorTotalHasta);
                    if (pFechaCDPDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaCDPDesde", DbType.DateTime, Convert.ToDateTime(pFechaCDPDesde).ToShortDateString());

                    if (pFechaCDPHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaCDPHasta", DbType.DateTime, Convert.ToDateTime(pFechaCDPHasta).ToShortDateString());

                    List<RegistroInformacionPresupuestal> vListaRegistroInformacionPresupuestal = new List<RegistroInformacionPresupuestal>();

                    int idVigencia, idregional;
                    string numeroCDP;

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            RegistroInformacionPresupuestal vRegistroInformacionPresupuestal = new RegistroInformacionPresupuestal();
                            idVigencia = vDataReaderResults["VigenciaFiscal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscal"].ToString()) : vRegistroInformacionPresupuestal.VigenciaFiscal;
                            idregional = vDataReaderResults["RegionalICBF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.RegionalICBF;
                            numeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vRegistroInformacionPresupuestal.NumeroCDP;

                            if (!vListaRegistroInformacionPresupuestal.Any(e => e.VigenciaFiscal == idVigencia && e.RegionalICBF == idregional && e.NumeroCDP == numeroCDP))
                            {
                                vRegistroInformacionPresupuestal.VigenciaFiscal = idVigencia;
                                vRegistroInformacionPresupuestal.RegionalICBF = idregional;
                                vRegistroInformacionPresupuestal.NumeroCDP = numeroCDP;
                                vRegistroInformacionPresupuestal.VigenciaFiscal = vDataReaderResults["VigenciaFiscal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscal"].ToString()) : vRegistroInformacionPresupuestal.VigenciaFiscal;
                                vRegistroInformacionPresupuestal.RegionalICBF = vDataReaderResults["RegionalICBF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.RegionalICBF;
                                vRegistroInformacionPresupuestal.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vRegistroInformacionPresupuestal.NumeroCDP;
                                vRegistroInformacionPresupuestal.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vRegistroInformacionPresupuestal.ValorCDP;
                                vRegistroInformacionPresupuestal.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vRegistroInformacionPresupuestal.FechaCDP;
                                vRegistroInformacionPresupuestal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroInformacionPresupuestal.UsuarioCrea;
                                vRegistroInformacionPresupuestal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroInformacionPresupuestal.FechaCrea;
                                vRegistroInformacionPresupuestal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroInformacionPresupuestal.UsuarioModifica;
                                vRegistroInformacionPresupuestal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroInformacionPresupuestal.FechaModifica;
                                vRegistroInformacionPresupuestal.NombreRegionalICBF = vDataReaderResults["NombreRegionalICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegionalICBF"].ToString()) : vRegistroInformacionPresupuestal.NombreRegionalICBF;
                                vRegistroInformacionPresupuestal.IdEtlCDP = vDataReaderResults["IdEtlCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEtlCDP"].ToString()) : vRegistroInformacionPresupuestal.IdEtlCDP;
                                vListaRegistroInformacionPresupuestal.Add(vRegistroInformacionPresupuestal);
                            }
                        }

                        return vListaRegistroInformacionPresupuestal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (SqlException ex)
            {


                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <returns></returns>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalSimpleRubro(int pVigenciaFiscal, int pRegionalICBF, string pNumeroCDP)
        {
            Database vDataBase = ObtenerInstancia();
            using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Ppto_RegistroInformacionPresupuestal_ConsultarRubroSimple"))
            {
                vDbCommand.CommandTimeout = 1000;

                vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.String, pNumeroCDP);
                vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pVigenciaFiscal);
                vDataBase.AddInParameter(vDbCommand, "@RegionalICBF", DbType.Int32, pRegionalICBF);

                using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                {
                    List<RegistroInformacionPresupuestal> vListaRegistroInformacionPresupuestal = new List<RegistroInformacionPresupuestal>();

                    string rubroPresupuestal, recursoPresupuestal;
                    RegistroInformacionPresupuestal vRegistroInformacionPresupuestal = new RegistroInformacionPresupuestal();

                    while (vDataReaderResults.Read())
                    {
                        rubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vRegistroInformacionPresupuestal.RubroPresupuestal;
                        recursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vRegistroInformacionPresupuestal.RecursoPresupuestal;

                        if (! vListaRegistroInformacionPresupuestal.Any( e=> e.RecursoPresupuestal == recursoPresupuestal && e.RubroPresupuestal == rubroPresupuestal))
                        {
                            vRegistroInformacionPresupuestal = new RegistroInformacionPresupuestal();
                            vRegistroInformacionPresupuestal.IdEtlCDP = vDataReaderResults["IdEtlCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEtlCDP"].ToString()) : vRegistroInformacionPresupuestal.IdEtlCDP;
                            vRegistroInformacionPresupuestal.ValorCDP = vDataReaderResults["ValorRubro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubro"].ToString()) : vRegistroInformacionPresupuestal.ValorCDP;
                            vRegistroInformacionPresupuestal.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGastos"].ToString()) : vRegistroInformacionPresupuestal.DependenciaAfectacionGastos;
                            vRegistroInformacionPresupuestal.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vRegistroInformacionPresupuestal.TipoFuenteFinanciamiento;
                            vRegistroInformacionPresupuestal.PosicionCatalogoGastos = vDataReaderResults["PosicionCatalogoGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PosicionCatalogoGastos"].ToString()) : vRegistroInformacionPresupuestal.PosicionCatalogoGastos;
                            vRegistroInformacionPresupuestal.RubroPresupuestal = rubroPresupuestal;
                            vRegistroInformacionPresupuestal.RecursoPresupuestal = recursoPresupuestal;
                            vRegistroInformacionPresupuestal.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vRegistroInformacionPresupuestal.TipoSituacionFondos;
                            vRegistroInformacionPresupuestal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroInformacionPresupuestal.UsuarioCrea;
                            vRegistroInformacionPresupuestal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroInformacionPresupuestal.FechaCrea;
                            vRegistroInformacionPresupuestal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroInformacionPresupuestal.UsuarioModifica;
                            vRegistroInformacionPresupuestal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroInformacionPresupuestal.FechaModifica;
                            vRegistroInformacionPresupuestal.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vRegistroInformacionPresupuestal.CodigoRubro;
                            
                            vListaRegistroInformacionPresupuestal.Add(vRegistroInformacionPresupuestal);
                        }
                    }
                    return vListaRegistroInformacionPresupuestal;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <returns></returns>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalUnificadoRubro(int pVigenciaFiscal, int pRegionalICBF, string pNumeroCDP)
        {
            Database vDataBase = ObtenerInstancia();
            using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Ppto_RegistroInformacionPresupuestal_ConsultarRubroUnificado"))
            {
                vDbCommand.CommandTimeout = 1000;

                vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.String, pNumeroCDP);
                vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, pVigenciaFiscal);
                vDataBase.AddInParameter(vDbCommand, "@RegionalICBF", DbType.Int32, pRegionalICBF);

                using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                {
                    List<RegistroInformacionPresupuestal> vListaRegistroInformacionPresupuestal = new List<RegistroInformacionPresupuestal>();
                    while (vDataReaderResults.Read())
                    {
                        RegistroInformacionPresupuestal vRegistroInformacionPresupuestal = new RegistroInformacionPresupuestal();
                        vRegistroInformacionPresupuestal.IdEtlCDP = vDataReaderResults["IdEtlCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEtlCDP"].ToString()) : vRegistroInformacionPresupuestal.IdEtlCDP;
                        vRegistroInformacionPresupuestal.ValorCDP = vDataReaderResults["ValorRubro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubro"].ToString()) : vRegistroInformacionPresupuestal.ValorCDP;
                        vRegistroInformacionPresupuestal.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGastos"].ToString()) : vRegistroInformacionPresupuestal.DependenciaAfectacionGastos;
                        vRegistroInformacionPresupuestal.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vRegistroInformacionPresupuestal.TipoFuenteFinanciamiento;
                        vRegistroInformacionPresupuestal.PosicionCatalogoGastos = vDataReaderResults["PosicionCatalogoGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PosicionCatalogoGastos"].ToString()) : vRegistroInformacionPresupuestal.PosicionCatalogoGastos;
                        vRegistroInformacionPresupuestal.RubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vRegistroInformacionPresupuestal.RubroPresupuestal;
                        vRegistroInformacionPresupuestal.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vRegistroInformacionPresupuestal.RecursoPresupuestal;
                        vRegistroInformacionPresupuestal.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vRegistroInformacionPresupuestal.TipoSituacionFondos;
                        vRegistroInformacionPresupuestal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroInformacionPresupuestal.UsuarioCrea;
                        vRegistroInformacionPresupuestal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroInformacionPresupuestal.FechaCrea;
                        vRegistroInformacionPresupuestal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroInformacionPresupuestal.UsuarioModifica;
                        vRegistroInformacionPresupuestal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroInformacionPresupuestal.FechaModifica;
                        vListaRegistroInformacionPresupuestal.Add(vRegistroInformacionPresupuestal);
                    }
                    return vListaRegistroInformacionPresupuestal;
                }
            }
        }

        /// <summary>
        /// Gonet 
        /// Método de consulta por filtros de areas internas
        /// 22-05-2014
        /// </summary>
        /// <param name="pRegionalICBF">vlor entero con el codigo codigo de la regional</param>
        /// <returns>Lista con las instancias que contiene las areas, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<AreasInternas> ConsultarAreasInternas(int? pRegionalICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_RegInfoPresAreasInternas_Consultar"))
                {
                    if (pRegionalICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegionalICBF", DbType.Int32, pRegionalICBF);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AreasInternas> vListaAreasInternas = new List<AreasInternas>();
                        while (vDataReaderResults.Read())
                        {
                            AreasInternas vAreasInternas = new AreasInternas();
                            vAreasInternas.IdAreasInternas = vDataReaderResults["IdAreasInternas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAreasInternas"].ToString()) : vAreasInternas.IdAreasInternas;
                            vAreasInternas.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vAreasInternas.Codigo;
                            vAreasInternas.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vAreasInternas.Descripcion;
                            vAreasInternas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAreasInternas.Estado;
                            vAreasInternas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAreasInternas.UsuarioCrea;
                            vAreasInternas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAreasInternas.FechaCrea;
                            vAreasInternas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAreasInternas.UsuarioModifica;
                            vAreasInternas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAreasInternas.FechaModifica;
                            vAreasInternas.CodigoDescripcion = vDataReaderResults["CodigoDescripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDescripcion"].ToString()) : vAreasInternas.CodigoDescripcion;
                            vListaAreasInternas.Add(vAreasInternas);
                        }
                        return vListaAreasInternas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet 
        /// Método de consulta los contaros relacionados a un número de CDP
        /// 26-05-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdCDP">Valor entero con el id del CDP</param>
        /// <returns></returns>
        public bool ConsultarContratosCDP(int? pIdContrato, int? pIdCDP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_Consultar"))
                {
                    if (pIdCDP != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCDP", DbType.Int32, pIdCDP);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratosCDP vContratos = new ContratosCDP();
                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestals(int numeroCDP, int pIdEtlCDP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Ppto_RegistroInformacionPresupuestal_ConsultarExiste"))
                {
                    vDbCommand.CommandTimeout = 1000;

                    vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.Int32, numeroCDP);
                        
                    vDataBase.AddInParameter(vDbCommand, "@pIdEtlCDP", DbType.Int32, pIdEtlCDP);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroInformacionPresupuestal> vListaRegistroInformacionPresupuestal = new List<RegistroInformacionPresupuestal>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroInformacionPresupuestal vRegistroInformacionPresupuestal = new RegistroInformacionPresupuestal();
                            vRegistroInformacionPresupuestal.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vRegistroInformacionPresupuestal.NumeroCDP;
                            vRegistroInformacionPresupuestal.IdEtlCDP = vDataReaderResults["IdEtlCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEtlCDP"].ToString()) : vRegistroInformacionPresupuestal.IdEtlCDP;
                            vRegistroInformacionPresupuestal.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vRegistroInformacionPresupuestal.IdContrato;
                            vRegistroInformacionPresupuestal.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? vDataReaderResults["NumeroContrato"].ToString() : vRegistroInformacionPresupuestal.NumeroContrato;
                            
                            vListaRegistroInformacionPresupuestal.Add(vRegistroInformacionPresupuestal);
                        }
                        return vListaRegistroInformacionPresupuestal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (SqlException ex)
            {


                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroCDP"></param>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public int ConsultarExisteCPDAsociadoContrato(int numeroCDP, DateTime fecha, int idRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RegistroInformacionPresupuestal_ConsultarExisteCPDAsociadoContrato"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@numeroCDP", DbType.Int32, numeroCDP);
                    vDataBase.AddInParameter(vDbCommand, "@vigenciaCDP", DbType.Int32, fecha.Year);
                    vDataBase.AddInParameter(vDbCommand, "@RegionalCDP", DbType.Int32, idRegional);
                    vDataBase.AddOutParameter(vDbCommand, "@IdContrato", DbType.Int32, 18);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    int idContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContrato").ToString());
                    return idContrato;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
