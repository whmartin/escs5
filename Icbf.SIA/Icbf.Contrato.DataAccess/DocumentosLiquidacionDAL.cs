﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    public class DocumentosLiquidacionDAL : GeneralDAL
    {
        public DocumentosLiquidacionDAL()
        {
        }
        /// <summary>
        /// Gonet
        /// Método de inserción para la entidad NumeroProcesos
        /// Fecha: 11/05/2016
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int InsertarDocumentoLiquidacion(DocumentosLiquidacion pDocumentosLiquidacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DocumentosLiquidacionContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentoLiquidacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pDocumentosLiquidacion.NombreDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionDocumento", DbType.String, pDocumentosLiquidacion.DescripcionDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pDocumentosLiquidacion.Estado);                    
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosLiquidacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDocumentosLiquidacion.IdDocumentoLiquidacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentoLiquidacion").ToString());
                    GenerarLogAuditoria(pDocumentosLiquidacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad NumeroProcesos
        /// </summary>
        /// <param name="pNumeroProcesos"></param>
        public int ModificarDocumentoLiquidacion(DocumentosLiquidacion pDocumentosLiquidacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DocumentosLiquidacionContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoLiquidacion", DbType.Int32, pDocumentosLiquidacion.IdDocumentoLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pDocumentosLiquidacion.NombreDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionDocumento", DbType.String, pDocumentosLiquidacion.DescripcionDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pDocumentosLiquidacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentosLiquidacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocumentosLiquidacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Método de modificación para la entidad NumeroProcesos
        /// Fecha: 11/05/2016
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int EliminarDocumentoLiquidacion(DocumentosLiquidacion pDocumentosLiquidacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_DocumentoLiquidacionContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdLiquidacionContrato", DbType.Int32, pDocumentosLiquidacion.IdDocumentoLiquidacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocumentosLiquidacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DocumentosLiquidacion ConsultarDocumentoLiquidacion(int pIdDocumentoLiquidacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DocumentosLiquidacionContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "IdDocumentoLiquidacion", DbType.Int32, pIdDocumentoLiquidacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocumentosLiquidacion vDocumentoLiquidacion = new DocumentosLiquidacion();
                        while (vDataReaderResults.Read())
                        {
                            vDocumentoLiquidacion.IdDocumentoLiquidacion = vDataReaderResults["IdDocumentoLiquidacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoLiquidacion"].ToString()) : vDocumentoLiquidacion.IdDocumentoLiquidacion;
                            vDocumentoLiquidacion.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocumentoLiquidacion.NombreDocumento;
                            vDocumentoLiquidacion.DescripcionDocumento = vDataReaderResults["DescripcionDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDocumento"].ToString()) : vDocumentoLiquidacion.DescripcionDocumento;
                            vDocumentoLiquidacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vDocumentoLiquidacion.Estado;
                            vDocumentoLiquidacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentoLiquidacion.UsuarioCrea;
                            vDocumentoLiquidacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentoLiquidacion.FechaCrea;
                            vDocumentoLiquidacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentoLiquidacion.UsuarioModifica;
                            vDocumentoLiquidacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentoLiquidacion.FechaModifica;
                        }
                        return vDocumentoLiquidacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentosLiquidacion> ConsultarVariosDocumentosLiquidacion( Boolean? pEstado, String pNombreDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DocumentosLiquidacionContrato_ConsultarVarios"))
                {
                    
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (!string.IsNullOrEmpty( pNombreDocumento))
                        vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pNombreDocumento);
                   
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentosLiquidacion> vListaDocumentosLiquidacion = new List<DocumentosLiquidacion>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentosLiquidacion vDocumentoLiquidacion = new DocumentosLiquidacion();
                            vDocumentoLiquidacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vDocumentoLiquidacion.Estado;
                            vDocumentoLiquidacion.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocumentoLiquidacion.NombreDocumento;
                            vDocumentoLiquidacion.IdDocumentoLiquidacion = vDataReaderResults["IdDocumentoLiquidacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoLiquidacion"].ToString()) : vDocumentoLiquidacion.IdDocumentoLiquidacion;
                            vListaDocumentosLiquidacion.Add(vDocumentoLiquidacion);
                        }
                        return vListaDocumentosLiquidacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



    }
}
