using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Obligaci�n
    /// </summary>
    public class ObligacionDAL : GeneralDAL
    {
        public ObligacionDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int InsertarObligacion(Obligacion pObligacion)
        {
            int vResultado;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Obligacion_Insertar"))
                {
                    
                    vDataBase.AddOutParameter(vDbCommand, "@IdObligacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pObligacion.IdTipoObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pObligacion.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pObligacion.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pObligacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pObligacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pObligacion.IdObligacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdObligacion").ToString());
                    GenerarLogAuditoria(pObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                //if (ex.Number == 2627)
                //    throw new GenericException("Registro duplicado, verifique por favor.");
                //throw;
                if (ex.Message.Contains("Registro duplicado"))
                {
                    vResultado = 4;
                    return vResultado;
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int ModificarObligacion(Obligacion pObligacion)
        {
            int vResultado;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Obligacion_Modificar"))
                {
                    
                    vDataBase.AddInParameter(vDbCommand, "@IdObligacion", DbType.Int32, pObligacion.IdObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pObligacion.IdTipoObligacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pObligacion.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pObligacion.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pObligacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pObligacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                //if (ex.Number == 2627)
                //    throw new GenericException("Registro duplicado, verifique por favor.");
                //throw;
                if (ex.Message.Contains("Registro duplicado"))
                {
                    vResultado = 4;
                    return vResultado;
                }
                else
                {
                    throw new GenericException(ex);
                }

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int EliminarObligacion(Obligacion pObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Obligacion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObligacion", DbType.Int32, pObligacion.IdObligacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObligacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad Obligacion
        /// </summary>
        /// <param name="pIdObligacion"></param>
        /// <returns></returns>

        public Obligacion ConsultarObligacion(int pIdObligacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Obligacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdObligacion", DbType.Int32, pIdObligacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Obligacion vObligacion = new Obligacion();
                        while (vDataReaderResults.Read())
                        {
                            vObligacion.IdObligacion = vDataReaderResults["IdObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObligacion"].ToString()) : vObligacion.IdObligacion;
                            vObligacion.IdTipoObligacion = vDataReaderResults["IdTipoObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObligacion"].ToString()) : vObligacion.IdTipoObligacion;
                            vObligacion.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vObligacion.IdTipoContrato;
                            vObligacion.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vObligacion.Descripcion;
                            vObligacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vObligacion.Estado;
                            vObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObligacion.UsuarioCrea;
                            vObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObligacion.FechaCrea;
                            vObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObligacion.UsuarioModifica;
                            vObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObligacion.FechaModifica;
                        }
                        return vObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Obligacion
        /// </summary>
        /// <param name="pDescripcion"></param>
        /// <param name="pIdTipoObligacion"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<Obligacion> ConsultarObligacions(String pDescripcion,int? pIdTipoObligacion, int? pIdTipoContrato, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Obligacions_Consultar"))
                {
                    if (pIdTipoObligacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoObligacion", DbType.Int32, pIdTipoObligacion);
                    if (pIdTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pIdTipoContrato);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Obligacion> vListaObligacion = new List<Obligacion>();
                        while (vDataReaderResults.Read())
                        {
                            Obligacion vObligacion = new Obligacion();
                            vObligacion.IdObligacion = vDataReaderResults["IdObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObligacion"].ToString()) : vObligacion.IdObligacion;
                            vObligacion.IdTipoObligacion = vDataReaderResults["IdTipoObligacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObligacion"].ToString()) : vObligacion.IdTipoObligacion;
                            vObligacion.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vObligacion.IdTipoContrato;
                            vObligacion.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vObligacion.Descripcion;
                            vObligacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vObligacion.Estado;
                            vObligacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObligacion.UsuarioCrea;
                            vObligacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObligacion.FechaCrea;
                            vObligacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObligacion.UsuarioModifica;
                            vObligacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObligacion.FechaModifica;
                            vListaObligacion.Add(vObligacion);
                        }
                        return vListaObligacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
