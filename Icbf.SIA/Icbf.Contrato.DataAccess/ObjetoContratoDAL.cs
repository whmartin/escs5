using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Objeto Contrato
    /// </summary>
    public class ObjetoContratoDAL : GeneralDAL
    {
        public ObjetoContratoDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int InsertarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ObjetoContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdObjetoContratoContractual", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoContractual", DbType.String, pObjetoContrato.ObjetoContractual);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pObjetoContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pObjetoContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pObjetoContrato.IdObjetoContratoContractual = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdObjetoContratoContractual").ToString());
                    GenerarLogAuditoria(pObjetoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("El registro ya se encuentra creado");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int ModificarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ObjetoContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObjetoContratoContractual", DbType.Int32, pObjetoContrato.IdObjetoContratoContractual);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoContractual", DbType.String, pObjetoContrato.ObjetoContractual);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pObjetoContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pObjetoContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObjetoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("El registro ya se encuentra creado");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int EliminarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ObjetoContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObjetoContratoContractual", DbType.Int32, pObjetoContrato.IdObjetoContratoContractual);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObjetoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pIdObjetoContratoContractual"></param>
        /// <returns></returns>
        public ObjetoContrato ConsultarObjetoContrato(int pIdObjetoContratoContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ObjetoContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdObjetoContratoContractual", DbType.Int32, pIdObjetoContratoContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ObjetoContrato vObjetoContrato = new ObjetoContrato();
                        while (vDataReaderResults.Read())
                        {
                            vObjetoContrato.IdObjetoContratoContractual = vDataReaderResults["IdObjetoContratoContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObjetoContratoContractual"].ToString()) : vObjetoContrato.IdObjetoContratoContractual;
                            vObjetoContrato.ObjetoContractual = vDataReaderResults["ObjetoContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContractual"].ToString()) : vObjetoContrato.ObjetoContractual;
                            vObjetoContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vObjetoContrato.Estado;
                            vObjetoContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObjetoContrato.UsuarioCrea;
                            vObjetoContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObjetoContrato.FechaCrea;
                            vObjetoContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObjetoContrato.UsuarioModifica;
                            vObjetoContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObjetoContrato.FechaModifica;
                        }
                        return vObjetoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pIdObjetoContratoContractual"></param>
        /// <param name="pObjetoContractual"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<ObjetoContrato> ConsultarObjetoContratos(int? pIdObjetoContratoContractual, String pObjetoContractual, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ObjetoContratos_Consultar"))
                {
                    if(pIdObjetoContratoContractual != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdObjetoContratoContractual", DbType.Int32, pIdObjetoContratoContractual);
                    if(pObjetoContractual != null)
                         vDataBase.AddInParameter(vDbCommand, "@ObjetoContractual", DbType.String, pObjetoContractual);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObjetoContrato> vListaObjetoContrato = new List<ObjetoContrato>();
                        while (vDataReaderResults.Read())
                        {
                                ObjetoContrato vObjetoContrato = new ObjetoContrato();
                            vObjetoContrato.IdObjetoContratoContractual = vDataReaderResults["IdObjetoContratoContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObjetoContratoContractual"].ToString()) : vObjetoContrato.IdObjetoContratoContractual;
                            vObjetoContrato.ObjetoContractual = vDataReaderResults["ObjetoContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContractual"].ToString()) : vObjetoContrato.ObjetoContractual;
                            vObjetoContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vObjetoContrato.Estado;
                            if (vObjetoContrato.Estado)
                            {
                                vObjetoContrato.EstadoString = "Activo";
                            } else
                            {
                                vObjetoContrato.EstadoString = "Inactivo";
                            }
                            vObjetoContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObjetoContrato.UsuarioCrea;
                            vObjetoContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObjetoContrato.FechaCrea;
                            vObjetoContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObjetoContrato.UsuarioModifica;
                            vObjetoContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObjetoContrato.FechaModifica;
                                vListaObjetoContrato.Add(vObjetoContrato);
                        }
                        return vListaObjetoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
