using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad SucursalAseguradoraContrato
    /// </summary>
    public class SucursalAseguradoraContratoDAL : GeneralDAL
    {
        public SucursalAseguradoraContratoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int InsertarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_SucursalAseguradoraContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDSucursalAseguradoraContrato", DbType.Int32, 18);
                    if(pSucursalAseguradoraContrato.IDDepartamento != -1)
                        vDataBase.AddInParameter(vDbCommand, "@IDDepartamento", DbType.Int32, pSucursalAseguradoraContrato.IDDepartamento);
                    if (pSucursalAseguradoraContrato.IDMunicipio != -1)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IDMunicipio", DbType.Int32, pSucursalAseguradoraContrato.IDMunicipio);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSucursalAseguradoraContrato.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IDZona", DbType.Int32, pSucursalAseguradoraContrato.IDZona);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionNotificacion", DbType.String, pSucursalAseguradoraContrato.DireccionNotificacion);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pSucursalAseguradoraContrato.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pSucursalAseguradoraContrato.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pSucursalAseguradoraContrato.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pSucursalAseguradoraContrato.Extension);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pSucursalAseguradoraContrato.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pSucursalAseguradoraContrato.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSucursal", DbType.Int32, pSucursalAseguradoraContrato.CodigoSucursal);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSucursalAseguradoraContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSucursalAseguradoraContrato.IDSucursalAseguradoraContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDSucursalAseguradoraContrato").ToString());
                    GenerarLogAuditoria(pSucursalAseguradoraContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int ModificarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_SucursalAseguradoraContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDSucursalAseguradoraContrato", DbType.Int32, pSucursalAseguradoraContrato.IDSucursalAseguradoraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDDepartamento", DbType.Int32, pSucursalAseguradoraContrato.IDDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IDMunicipio", DbType.Int32, pSucursalAseguradoraContrato.IDMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSucursalAseguradoraContrato.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@IDZona", DbType.Int32, pSucursalAseguradoraContrato.IDZona);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionNotificacion", DbType.String, pSucursalAseguradoraContrato.DireccionNotificacion);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pSucursalAseguradoraContrato.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pSucursalAseguradoraContrato.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pSucursalAseguradoraContrato.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pSucursalAseguradoraContrato.Extension);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pSucursalAseguradoraContrato.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pSucursalAseguradoraContrato.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSucursal", DbType.Int32, pSucursalAseguradoraContrato.CodigoSucursal);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSucursalAseguradoraContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSucursalAseguradoraContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int EliminarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_SucursalAseguradoraContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDSucursalAseguradoraContrato", DbType.Int32, pSucursalAseguradoraContrato.IDSucursalAseguradoraContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSucursalAseguradoraContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pIDSucursalAseguradoraContrato"></param>
        public SucursalAseguradoraContrato ConsultarSucursalAseguradoraContrato(int pIDSucursalAseguradoraContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_SucursalAseguradoraContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDSucursalAseguradoraContrato", DbType.Int32, pIDSucursalAseguradoraContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SucursalAseguradoraContrato vSucursalAseguradoraContrato = new SucursalAseguradoraContrato();
                        while (vDataReaderResults.Read())
                        {
                            vSucursalAseguradoraContrato.IDSucursalAseguradoraContrato = vDataReaderResults["IDSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSucursalAseguradoraContrato"].ToString()) : vSucursalAseguradoraContrato.IDSucursalAseguradoraContrato;
                            vSucursalAseguradoraContrato.IDDepartamento = vDataReaderResults["IDDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDepartamento"].ToString()) : vSucursalAseguradoraContrato.IDDepartamento;
                            vSucursalAseguradoraContrato.IDMunicipio = vDataReaderResults["IDMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDMunicipio"].ToString()) : vSucursalAseguradoraContrato.IDMunicipio;
                            vSucursalAseguradoraContrato.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSucursalAseguradoraContrato.Nombre;
                            vSucursalAseguradoraContrato.IDZona = vDataReaderResults["IDZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDZona"].ToString()) : vSucursalAseguradoraContrato.IDZona;
                            vSucursalAseguradoraContrato.DireccionNotificacion = vDataReaderResults["DireccionNotificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionNotificacion"].ToString()) : vSucursalAseguradoraContrato.DireccionNotificacion;
                            vSucursalAseguradoraContrato.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vSucursalAseguradoraContrato.CorreoElectronico;
                            vSucursalAseguradoraContrato.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"].ToString()) : vSucursalAseguradoraContrato.Indicativo;
                            vSucursalAseguradoraContrato.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vSucursalAseguradoraContrato.Telefono;
                            vSucursalAseguradoraContrato.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vSucursalAseguradoraContrato.Extension;
                            vSucursalAseguradoraContrato.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vSucursalAseguradoraContrato.Celular;
                            vSucursalAseguradoraContrato.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vSucursalAseguradoraContrato.IdTercero;
                            vSucursalAseguradoraContrato.CodigoSucursal = vDataReaderResults["CodigoSucursal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoSucursal"].ToString()) : vSucursalAseguradoraContrato.CodigoSucursal;
                            vSucursalAseguradoraContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSucursalAseguradoraContrato.UsuarioCrea;
                            vSucursalAseguradoraContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSucursalAseguradoraContrato.FechaCrea;
                            vSucursalAseguradoraContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSucursalAseguradoraContrato.UsuarioModifica;
                            vSucursalAseguradoraContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSucursalAseguradoraContrato.FechaModifica;
                        }
                        return vSucursalAseguradoraContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pIDDepartamento"></param>
        /// <param name="pIDMunicipio"></param>
        /// <param name="pNombre"></param>
        /// <param name="pIDZona"></param>
        /// <param name="pDireccionNotificacion"></param>
        /// <param name="pCorreoElectronico"></param>
        /// <param name="pIndicativo"></param>
        /// <param name="pTelefono"></param>
        /// <param name="pExtension"></param>
        /// <param name="pCelular"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        /// <param name="pCodigoSucursal"></param>
        public List<SucursalAseguradoraContrato> ConsultarSucursalAseguradoraContratos(int? pIDDepartamento, int? pIDMunicipio, String pNombre, int? pIDZona, String pDireccionNotificacion, String pCorreoElectronico, String pIndicativo, String pTelefono, String pExtension, String pCelular, int? pIDEntidadProvOferente, int? pCodigoSucursal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_SucursalAseguradoraContratos_Consultar"))
                {
                    if(pIDDepartamento != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDDepartamento", DbType.Int32, pIDDepartamento);
                    if(pIDMunicipio != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDMunicipio", DbType.Int32, pIDMunicipio);
                    if(pNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if(pIDZona != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDZona", DbType.Int32, pIDZona);
                    if(pDireccionNotificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@DireccionNotificacion", DbType.String, pDireccionNotificacion);
                    if(pCorreoElectronico != null)
                         vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pCorreoElectronico);
                    if(pIndicativo != null)
                         vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pIndicativo);
                    if(pTelefono != null)
                         vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pTelefono);
                    if(pExtension != null)
                         vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pExtension);
                    if(pCelular != null)
                         vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pCelular);
                    if(pIDEntidadProvOferente != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDEntidadProvOferente", DbType.Int32, pIDEntidadProvOferente);
                    if(pCodigoSucursal != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoSucursal", DbType.Int32, pCodigoSucursal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SucursalAseguradoraContrato> vListaSucursalAseguradoraContrato = new List<SucursalAseguradoraContrato>();
                        while (vDataReaderResults.Read())
                        {
                                SucursalAseguradoraContrato vSucursalAseguradoraContrato = new SucursalAseguradoraContrato();
                            vSucursalAseguradoraContrato.IDSucursalAseguradoraContrato = vDataReaderResults["IDSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSucursalAseguradoraContrato"].ToString()) : vSucursalAseguradoraContrato.IDSucursalAseguradoraContrato;
                            vSucursalAseguradoraContrato.IDDepartamento = vDataReaderResults["IDDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDepartamento"].ToString()) : vSucursalAseguradoraContrato.IDDepartamento;
                            vSucursalAseguradoraContrato.IDMunicipio = vDataReaderResults["IDMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDMunicipio"].ToString()) : vSucursalAseguradoraContrato.IDMunicipio;
                            vSucursalAseguradoraContrato.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSucursalAseguradoraContrato.Nombre;
                            vSucursalAseguradoraContrato.IDZona = vDataReaderResults["IDZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDZona"].ToString()) : vSucursalAseguradoraContrato.IDZona;
                            vSucursalAseguradoraContrato.DireccionNotificacion = vDataReaderResults["DireccionNotificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionNotificacion"].ToString()) : vSucursalAseguradoraContrato.DireccionNotificacion;
                            vSucursalAseguradoraContrato.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vSucursalAseguradoraContrato.CorreoElectronico;
                            vSucursalAseguradoraContrato.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"].ToString()) : vSucursalAseguradoraContrato.Indicativo;
                            vSucursalAseguradoraContrato.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vSucursalAseguradoraContrato.Telefono;
                            vSucursalAseguradoraContrato.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vSucursalAseguradoraContrato.Extension;
                            vSucursalAseguradoraContrato.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vSucursalAseguradoraContrato.Celular;
                            vSucursalAseguradoraContrato.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vSucursalAseguradoraContrato.IdTercero;
                            vSucursalAseguradoraContrato.CodigoSucursal = vDataReaderResults["CodigoSucursal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoSucursal"].ToString()) : vSucursalAseguradoraContrato.CodigoSucursal;
                            vSucursalAseguradoraContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSucursalAseguradoraContrato.UsuarioCrea;
                            vSucursalAseguradoraContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSucursalAseguradoraContrato.FechaCrea;
                            vSucursalAseguradoraContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSucursalAseguradoraContrato.UsuarioModifica;
                            vSucursalAseguradoraContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSucursalAseguradoraContrato.FechaModifica;
                                vListaSucursalAseguradoraContrato.Add(vSucursalAseguradoraContrato);
                        }
                        return vListaSucursalAseguradoraContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
