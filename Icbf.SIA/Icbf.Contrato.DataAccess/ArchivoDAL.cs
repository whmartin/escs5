using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Data.SqlClient;


namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso para el registro de Archivos
    /// </summary>
    public class ArchivoDAL : GeneralDAL
    {
        public ArchivoDAL()
        {
        }
        /// <summary>
        /// Gonet
        /// Insertar Archivo nuevo
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>
        public int InsertarArchivo(ArchivoContrato pArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdArchivo", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pArchivo.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pArchivo.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pArchivo.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivoOri", DbType.String, pArchivo.NombreArchivoOri);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pArchivo.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@ServidorFTP", DbType.String, pArchivo.ServidorFTP);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pArchivo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@ResumenCarga", DbType.String, pArchivo.ResumenCarga);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pArchivo.UsuarioCrea);

                    if (!string.IsNullOrEmpty(pArchivo.NombreTabla))
                        vDataBase.AddInParameter(vDbCommand, "@NombreTabla", DbType.String, pArchivo.NombreTabla);
                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pArchivo.IdArchivo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdArchivo").ToString());
                    GenerarLogAuditoria(pArchivo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pArchivo"></param>
        /// <returns></returns>
        public int InsertarArchivoContrato(ArchivoContrato pArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_InsertarContrato"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdArchivo", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand,  "@IdUsuario", DbType.Int32, pArchivo.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand,  "@IdContrato", DbType.Int32, pArchivo.IdContrato);
                    vDataBase.AddInParameter(vDbCommand,  "@IdFormatoArchivo", DbType.Int32, pArchivo.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand,  "@FechaRegistro", DbType.DateTime, pArchivo.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand,  "@NombreArchivoOri", DbType.String, pArchivo.NombreArchivoOri);
                    vDataBase.AddInParameter(vDbCommand,  "@NombreArchivo", DbType.String, pArchivo.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand,  "@ServidorFTP", DbType.String, pArchivo.ServidorFTP);
                    vDataBase.AddInParameter(vDbCommand,  "@Estado", DbType.String, pArchivo.Estado);
                    vDataBase.AddInParameter(vDbCommand,  "@ResumenCarga", DbType.String, pArchivo.ResumenCarga);
                    vDataBase.AddInParameter(vDbCommand,  "@UsuarioCrea", DbType.String, pArchivo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pArchivo.IdArchivo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdArchivo").ToString());
                    GenerarLogAuditoria(pArchivo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pArchivo"></param>
        /// <returns></returns>
        public int InsertarArchivoAnexo(ArchivoContrato pArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_InsertarDocumentosAnexos"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdArchivo", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pArchivo.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pArchivo.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pArchivo.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivoOri", DbType.String, pArchivo.NombreArchivoOri);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pArchivo.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@TipoEstructura", DbType.String, pArchivo.TipoEstructura);

                    if (! string.IsNullOrEmpty(pArchivo.IdTabla))
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoEstructura", DbType.String, pArchivo.IdTabla);                    

                    vDataBase.AddInParameter(vDbCommand, "@ServidorFTP", DbType.String, pArchivo.ServidorFTP);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pArchivo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@ResumenCarga", DbType.String, pArchivo.ResumenCarga);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pArchivo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pArchivo.IdArchivo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdArchivo").ToString());
                    GenerarLogAuditoria(pArchivo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Insertar Archivo nuevo GCB
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>
        public int InsertarArchivoGCB(ArchivoContrato pArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdArchivo", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pArchivo.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pArchivo.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pArchivo.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pArchivo.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@ServidorFTP", DbType.String, pArchivo.ServidorFTP);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pArchivo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@ResumenCarga", DbType.String, pArchivo.ResumenCarga);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pArchivo.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivoOri", DbType.String, pArchivo.NombreArchivoOri);
                    vDataBase.AddInParameter(vDbCommand, "@idTabla", DbType.String, pArchivo.IdTabla);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTabla", DbType.String, pArchivo.NombreTabla);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pArchivo.IdArchivo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdArchivo").ToString());
                    GenerarLogAuditoria(pArchivo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Insertar Archivo nuevo retornando el Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Id Archivo</returns>
        public decimal InsertarArchivoReturID(ArchivoContrato pArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdArchivo", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pArchivo.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pArchivo.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pArchivo.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pArchivo.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@ServidorFTP", DbType.String, pArchivo.ServidorFTP);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pArchivo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@ResumenCarga", DbType.String, pArchivo.ResumenCarga);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pArchivo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pArchivo.IdArchivo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdArchivo").ToString());
                    GenerarLogAuditoria(pArchivo, vDbCommand);
                    return pArchivo.IdArchivo;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Modificar Archivo 
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>
        public int ModificarArchivo(ArchivoContrato pArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Decimal, pArchivo.IdArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pArchivo.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pArchivo.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pArchivo.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pArchivo.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@ServidorFTP", DbType.String, pArchivo.ServidorFTP);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pArchivo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@ResumenCarga", DbType.String, pArchivo.ResumenCarga);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pArchivo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pArchivo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar Archivo y Archivo Contrato Asociado
        /// Fecha: 23/11/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>
        public int EliminarDocumentoAnexo(decimal pIdArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_EliminarDocumentosAnexos"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Decimal, pIdArchivo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdArchivo"></param>
        /// <returns></returns>
        public int EliminarDocumentoAnexoContrato(decimal pIdArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_EliminarDocumentosAnexosContrato"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Decimal, pIdArchivo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar Archivo 
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>
        public int EliminarArchivo(ArchivoContrato pArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Decimal, pArchivo.IdArchivo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pArchivo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Tommy Puccini - Grupo Apoyo
        /// Consultar Archivo s�lo por IdContrato y el nombre del TipoEstructura
        /// Fecha: 23/11/2015
        /// </summary>
        /// <param name="pIdArchivo">Valor Decimal Id Archivo</param>
        /// <returns>Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivoTipoEstructurayContrato(int pIdcontrato, String pTipoEstructura, int idTipoEstructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_DocumentosPorTipoEstructurayContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdcontrato);
                    vDataBase.AddInParameter(vDbCommand, "@TipoEstructura", DbType.String, pTipoEstructura);

                    if (idTipoEstructura != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoEstructura", DbType.String, idTipoEstructura);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivoContrato> vListaArchivo = new List<ArchivoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            ArchivoContrato vArchivo = new ArchivoContrato();
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.ServidorFTP = vDataReaderResults["ServidorFTP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ServidorFTP"].ToString()) : vArchivo.ServidorFTP;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vArchivo.NombreArchivoOri;
                            vArchivo.NombreTabla = vDataReaderResults["NombreTabla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTabla"].ToString()) : vArchivo.NombreTabla;
                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivo s�lo por Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdArchivo">Valor Decimal Id Archivo</param>
        /// <returns>Entity Archivo</returns>
        public ArchivoContrato ConsultarArchivo(decimal pIdArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Decimal, pIdArchivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ArchivoContrato vArchivo = new ArchivoContrato();
                        while (vDataReaderResults.Read())
                        {
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vArchivo.IdUsuario;
                            vArchivo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vArchivo.IdFormatoArchivo;
                            vArchivo.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vArchivo.FechaRegistro;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.ServidorFTP = vDataReaderResults["ServidorFTP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ServidorFTP"].ToString()) : vArchivo.ServidorFTP;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.ResumenCarga = vDataReaderResults["ResumenCarga"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenCarga"].ToString()) : vArchivo.ResumenCarga;
                            vArchivo.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vArchivo.NombreArchivoOri;

                            vArchivo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivo.UsuarioCrea;
                            vArchivo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivo.FechaCrea;
                            vArchivo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivo.UsuarioModifica;
                            vArchivo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivo.FechaModifica;
                            //vArchivo.NombreEstructura = vDataReaderResults["NombreEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstructura"].ToString()) : vArchivo.NombreEstructura;
                            //vArchivo.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vArchivo.NombreArchivoOri;
                            
                        }
                        return vArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivo por Id tabla y por Nombre tabla
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="idTabla">Cadena String con el Id tabla</param>
        /// <param name="nombreTabla">Cadena String con el nombre de la tabla</param>
        /// <returns>Lista Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivoByNombreIDTabla(string idTabla, string nombreTabla)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_Consultar_NombreIDTabla"))
                {
                    if (!string.IsNullOrEmpty(idTabla))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idTabla", DbType.String, idTabla);
                    }

                    if (!string.IsNullOrEmpty(nombreTabla))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@NombreTabla", DbType.String, nombreTabla);
                    }
                    

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivoContrato> vListaArchivo = new List<ArchivoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            ArchivoContrato vArchivo = new ArchivoContrato();
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vArchivo.IdUsuario;
                            vArchivo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vArchivo.IdFormatoArchivo;
                            vArchivo.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vArchivo.FechaRegistro;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.ServidorFTP = vDataReaderResults["ServidorFTP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ServidorFTP"].ToString()) : vArchivo.ServidorFTP;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.ResumenCarga = vDataReaderResults["ResumenCarga"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenCarga"].ToString()) : vArchivo.ResumenCarga;

                            vArchivo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivo.UsuarioCrea;
                            vArchivo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivo.FechaCrea;
                            vArchivo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivo.UsuarioModifica;
                            vArchivo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivo.FechaModifica;
                            vArchivo.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vArchivo.NombreArchivoOri;

                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivos por Id usuario � por formato del archivo
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdUsuario">int con el Id usuario</param>
        /// <param name="pIdFormatoArchivo">int Id formato archivo</param>
        /// <returns>lista Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivos(int? pIdUsuario, int? pIdFormatoArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_sConsultar"))
                {
                    if(pIdUsuario != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);
                    if(pIdFormatoArchivo != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pIdFormatoArchivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivoContrato> vListaArchivo = new List<ArchivoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            ArchivoContrato vArchivo = new ArchivoContrato();
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vArchivo.IdUsuario;
                            vArchivo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vArchivo.IdFormatoArchivo;
                            vArchivo.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vArchivo.FechaRegistro;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.ServidorFTP = vDataReaderResults["ServidorFTP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ServidorFTP"].ToString()) : vArchivo.ServidorFTP;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.ResumenCarga = vDataReaderResults["ResumenCarga"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenCarga"].ToString()) : vArchivo.ResumenCarga;
                            
                            vArchivo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivo.UsuarioCrea;
                            vArchivo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivo.FechaCrea;
                            vArchivo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivo.UsuarioModifica;
                            vArchivo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivo.FechaModifica;

                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }       
        /// <summary>
        /// Gonet
        /// Consultar Archivos General
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>List Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivosList(ArchivoContrato pArchivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_ConsultarList"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidad", DbType.Int32, pArchivo.IdModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pArchivo.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pArchivo.Estado);
                    if (pArchivo.UsuarioCrea != string.Empty || pArchivo.UsuarioCrea == null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, pArchivo.UsuarioCrea);
                    }
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivoContrato> vListaArchivo = new List<ArchivoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            ArchivoContrato vArchivo = new ArchivoContrato();
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vArchivo.IdUsuario;
                            vArchivo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vArchivo.IdFormatoArchivo;
                            vArchivo.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vArchivo.FechaRegistro;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.NombreEstructura = vDataReaderResults["NombreEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstructura"].ToString()) : vArchivo.NombreEstructura;
                            vArchivo.ResumenCarga = vDataReaderResults["ResumenCarga"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenCarga"].ToString()) : vArchivo.ResumenCarga;

                            vArchivo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivo.UsuarioCrea;
                            vArchivo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivo.FechaCrea;
                            vArchivo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivo.UsuarioModifica;
                            vArchivo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivo.FechaModifica;

                            vArchivo.NombreEstructura = vDataReaderResults["NombreEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstructura"].ToString()) : vArchivo.NombreEstructura;
                            vArchivo.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vArchivo.NombreArchivoOri;
                            
      
                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivos listar GCB informe
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <param name="pIdInformeMensual">int Id Informe mensual</param>
        /// <returns>Lista Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivosListGCBInforme(ArchivoContrato pArchivo, int pIdInformeMensual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_ConsultarList_GCB_Informe"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pArchivo.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, pArchivo.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdInformeMensual", DbType.Int32, pIdInformeMensual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivoContrato> vListaArchivo = new List<ArchivoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            ArchivoContrato vArchivo = new ArchivoContrato();
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vArchivo.IdUsuario;
                            vArchivo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vArchivo.IdFormatoArchivo;
                            vArchivo.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vArchivo.FechaRegistro;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.NombreEstructura = vDataReaderResults["NombreEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstructura"].ToString()) : vArchivo.NombreEstructura;
                            vArchivo.ResumenCarga = vDataReaderResults["ResumenCarga"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenCarga"].ToString()) : vArchivo.ResumenCarga;

                            vArchivo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivo.UsuarioCrea;
                            vArchivo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivo.FechaCrea;
                            vArchivo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivo.UsuarioModifica;
                            vArchivo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivo.FechaModifica;

                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivos listar GCB
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <param name="pIdLlave">int Id llave</param>
        /// <param name="pSigla">Cadena string Sigla</param>
        /// <param name="pIdFormato">int Id formato</param>
        /// <returns>Lista Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivosListGCB(ArchivoContrato pArchivo, int pIdLlave, string pSigla, int pIdFormato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_ConsultarList_GCB"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pArchivo.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, pArchivo.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdLlave", DbType.Int32, pIdLlave);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pIdFormato);
                    vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pSigla);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivoContrato> vListaArchivo = new List<ArchivoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            ArchivoContrato vArchivo = new ArchivoContrato();
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vArchivo.IdUsuario;
                            vArchivo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vArchivo.IdFormatoArchivo;
                            vArchivo.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vArchivo.FechaRegistro;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.NombreEstructura = vDataReaderResults["NombreEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstructura"].ToString()) : vArchivo.NombreEstructura;
                            vArchivo.ResumenCarga = vDataReaderResults["ResumenCarga"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenCarga"].ToString()) : vArchivo.ResumenCarga;

                            vArchivo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivo.UsuarioCrea;
                            vArchivo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivo.FechaCrea;
                            vArchivo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivo.UsuarioModifica;
                            vArchivo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivo.FechaModifica;

                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public List<ArchivoContrato> ConsultarArchivosPorContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_Archivo_sConsultarContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ArchivoContrato> vListaArchivo = new List<ArchivoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            ArchivoContrato vArchivo = new ArchivoContrato();
                            vArchivo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vArchivo.IdArchivo;
                            vArchivo.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vArchivo.IdContrato;
                            vArchivo.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vArchivo.IdUsuario;
                            vArchivo.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vArchivo.Extension;
                            vArchivo.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vArchivo.FechaRegistro;
                            vArchivo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vArchivo.NombreArchivo;
                            vArchivo.ServidorFTP = vDataReaderResults["ServidorFTP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ServidorFTP"].ToString()) : vArchivo.ServidorFTP;
                            vArchivo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vArchivo.Estado;
                            vArchivo.ResumenCarga = vDataReaderResults["ResumenCarga"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenCarga"].ToString()) : vArchivo.ResumenCarga;

                            vArchivo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vArchivo.UsuarioCrea;
                            vArchivo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vArchivo.FechaCrea;
                            vArchivo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vArchivo.UsuarioModifica;
                            vArchivo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vArchivo.FechaModifica;

                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idRegional"></param>
        /// <returns></returns>
        public List<CargueContratoMasivo> ConsultarCargueContratosArchivos(int? idRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_Estructura_Archivo_ConsultarCargueArchivo"))
                {
                    if (idRegional.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, idRegional.Value);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CargueContratoMasivo> vListaArchivo = new List<CargueContratoMasivo>();
                        while (vDataReaderResults.Read())
                        {
                            CargueContratoMasivo vArchivo = new CargueContratoMasivo();
                            vArchivo.IdCargueContratoMasivo = vDataReaderResults["IdCargueContratoMasivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargueContratoMasivo"].ToString()) : vArchivo.IdCargueContratoMasivo;
                            vArchivo.TotalRegistros = vDataReaderResults["TotalRegistros"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalRegistros"].ToString()) : vArchivo.TotalRegistros;
                            vArchivo.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vArchivo.Descripcion;
                            vArchivo.Usuario = vDataReaderResults["Usuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Usuario"].ToString()) : vArchivo.Usuario;
                            vArchivo.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : vArchivo.Fecha;
                            vArchivo.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vArchivo.IdRegional;

                            vListaArchivo.Add(vArchivo);
                        }
                        return vListaArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public Dictionary<int, string> CargarRegistros(List<itemCargueArchivo> items, string usuario)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            try
            {
                Database vDataBase = ObtenerInstancia();

                foreach (var item in items)
                {
                    try
                    {
                        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_Estructura_Archivo_CargarItemArchivo"))
                        {
                            vDataBase.AddOutParameter(vDbCommand, "@outputResult", DbType.Int32, 18);
                            vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.String, item.CodigoRegional);
                            vDataBase.AddInParameter(vDbCommand, "@ConsecutivoContrato", DbType.String, item.ConsecutivoContrato);
                            vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, item.FechaSuscripcion);
                            vDataBase.AddInParameter(vDbCommand, "@CategoriaContrato", DbType.String, item.CategoriaContrato);
                            vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, item.ModalidadSeleccion);
                          
                            if (!string.IsNullOrEmpty(item.NumeroProceso))
                                vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, item.NumeroProceso);
                            if (item.FechaAdjudicacionProceso.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicacionProceso", DbType.DateTime, item.FechaAdjudicacionProceso);

                            vDataBase.AddInParameter(vDbCommand, "@TipoContrato", DbType.String, item.TipoContrato);
                            vDataBase.AddInParameter(vDbCommand, "@CedulaSolicitante", DbType.String, item.CedulaSolicitante);
                            vDataBase.AddInParameter(vDbCommand, "@AreaSolicitante", DbType.String, item.AreaSolicitante);
                            vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacionContratista", DbType.String, item.TipoIdentificacionContratista);
                            vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionContratista", DbType.String, item.NumeroIdentificacionContratista);
                            vDataBase.AddInParameter(vDbCommand, "@ModalidadAcademica", DbType.String, item.ModalidadAcademica);
                            vDataBase.AddInParameter(vDbCommand, "@Profesion", DbType.String, item.Profesion);
                            vDataBase.AddInParameter(vDbCommand, "@AfectacionRecurso", DbType.String, item.AfectacionRecurso);
                            vDataBase.AddInParameter(vDbCommand, "@CodigoSecop", DbType.String, item.CodigoSecop);
                            vDataBase.AddInParameter(vDbCommand, "@ObjetoContrato", DbType.String, item.ObjetoContrato);
                            vDataBase.AddInParameter(vDbCommand, "@LugarEjeccucion", DbType.String, item.LugarEjeccucion);
                            vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.DateTime, item.FechaInicioEjecucion);
                            vDataBase.AddInParameter(vDbCommand, "@FechaFinEjecuccion", DbType.DateTime, item.FechaFinEjecuccion);
                            vDataBase.AddInParameter(vDbCommand, "@ConsecutivoPlanCompras", DbType.Int32, item.ConsecutivoPlanCompras);
                            vDataBase.AddInParameter(vDbCommand, "@CDP", DbType.Int32, item.CDP);
                            vDataBase.AddInParameter(vDbCommand, "@RP", DbType.Int32, item.RP);
                            vDataBase.AddInParameter(vDbCommand, "@ValorIncialContrato", DbType.Decimal, item.ValorIncialContrato);

                            if (!string.IsNullOrEmpty(item.Anticipo))
                                vDataBase.AddInParameter(vDbCommand, "@Anticipo", DbType.String, item.Anticipo);
                            if (item.ValorAnticipo.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorAnticipo", DbType.Decimal, item.ValorAnticipo.Value);

                            if (!string.IsNullOrEmpty(item.cofinaciacionEntidad1))
                                vDataBase.AddInParameter(vDbCommand, "@cofinaciacionEntidad1", DbType.String, item.cofinaciacionEntidad1);
                            if (item.ValorCofinanciacion1.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorCofinanciacion1", DbType.Decimal, item.ValorCofinanciacion1.Value);

                            if (!string.IsNullOrEmpty(item.cofinaciacionEntidad2))
                                vDataBase.AddInParameter(vDbCommand, "@cofinaciacionEntidad2", DbType.String, item.cofinaciacionEntidad2);
                            if (item.ValorCofinanciacion2.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorCofinanciacion2", DbType.Decimal, item.ValorCofinanciacion2.Value);

                            if (!string.IsNullOrEmpty(item.VigenciaFutura))
                                vDataBase.AddInParameter(vDbCommand, "@VigenciaFutura", DbType.String, item.VigenciaFutura);
                            if (item.ValorVigenciaFutura.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorVigenciaFutura", DbType.Decimal, item.ValorVigenciaFutura.Value);


                            vDataBase.AddInParameter(vDbCommand, "@FechaGarantia", DbType.DateTime, item.FechaGarantia);
                            vDataBase.AddInParameter(vDbCommand, "@TipoGarantia", DbType.String, item.TipoGarantia);
                            vDataBase.AddInParameter(vDbCommand, "@Aseguradora", DbType.String, item.Aseguradora);
                            vDataBase.AddInParameter(vDbCommand, "@RiesgosGarantias", DbType.String, item.RiesgosGarantias);
                            vDataBase.AddInParameter(vDbCommand, "@ValorAseguradoGarantia", DbType.Decimal, item.ValorAseguradoGarantia);
                            vDataBase.AddInParameter(vDbCommand, "@PorcentajeGarantia", DbType.String, item.PorcentajeGarantia);

                            vDataBase.AddInParameter(vDbCommand, "@AreaSupervisor", DbType.String, item.AreaSupervisor);
                            vDataBase.AddInParameter(vDbCommand, "@CargoSupervisor", DbType.String, item.CargoSupervisor);
                            vDataBase.AddInParameter(vDbCommand, "@IdentificacionSupervisor", DbType.String, item.IdentificacionSupervisor);

                            vDataBase.AddInParameter(vDbCommand, "@CargoOrdenador", DbType.String, item.CargoOrdenador);
                            vDataBase.AddInParameter(vDbCommand, "@IdentificacionOrdenador", DbType.String, item.IdentificacionOrdenador);

                            if (item.FechaProrroga1.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaProrroga1", DbType.DateTime, item.FechaProrroga1.Value);
                            if (item.FechaProrroga2.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaProrroga2", DbType.DateTime, item.FechaProrroga2.Value);
                            if (item.FechaProrroga3.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaProrroga3", DbType.DateTime, item.FechaProrroga3.Value);

                            vDataBase.AddInParameter(vDbCommand, "@fechaTerminacionFinalas", DbType.DateTime, item.fechaTerminacionFinal);

                            if (item.RPAdicion1.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@RPAdicion1", DbType.Int32, item.RPAdicion1.Value);
                            if (item.FechaAdicion1.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaAdicion1", DbType.DateTime, item.FechaAdicion1.Value);
                            if (item.ValorAdicion1.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorAdicion1", DbType.Decimal, item.ValorAdicion1.Value);

                            if (item.RPAdicion2.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@RPAdicion2", DbType.Int32, item.RPAdicion2.Value);
                            if (item.FechaAdicion2.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaAdicion2", DbType.DateTime, item.FechaAdicion2.Value);
                            if (item.ValorAdicion2.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorAdicion2", DbType.Decimal, item.ValorAdicion2.Value);

                            if (item.RPAdicion3.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@RPAdicion2", DbType.Int32, item.RPAdicion3.Value);
                            if (item.FechaAdicion3.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaAdicion3", DbType.DateTime, item.FechaAdicion3.Value);
                            if (item.ValorAdicion3.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorAdicion3", DbType.Decimal, item.ValorAdicion3.Value);

                            if (item.RPAdicion4.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@RPAdicion2", DbType.Int32, item.RPAdicion4.Value);
                            if (item.FechaAdicion4.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaAdicion4", DbType.DateTime, item.FechaAdicion4.Value);
                            if (item.ValorAdicion4.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorAdicion4", DbType.Decimal, item.ValorAdicion4.Value);

                            if (item.ValorDisminucion.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@ValorDisminucion", DbType.Decimal, item.ValorDisminucion.Value);

                            vDataBase.AddInParameter(vDbCommand, "@ValorFinalContrato", DbType.Decimal, item.ValorFinalContrato);
                            vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                            vDataBase.AddInParameter(vDbCommand, "@TipoPago", DbType.String, item.TipoPago);


                            if (item.FechaLiquidacion.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@FechaLiquidacion", DbType.DateTime, item.FechaLiquidacion.Value);

                            vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, item.Estado);

                            if (item.DiasTerminacionAnticipada.HasValue)
                                vDataBase.AddInParameter(vDbCommand, "@DiasTerminacionAnticipada", DbType.Int32, item.DiasTerminacionAnticipada.Value);

                           
                            var itemPlanCompras = ObtenerPlanCompras(item.ConsecutivoPlanCompras, item.FechaInicioEjecucion.Year, item.CodigoRegional);

                            if (itemPlanCompras != null)
                            {
                                var pDatosProductos = new SqlParameter("@ProductoPlanCompraContratos", SqlDbType.Structured)
                                {
                                    Value = itemPlanCompras.Productos
                                };
                                vDbCommand.Parameters.Add(pDatosProductos);

                                var pDatosRubros = new SqlParameter("@RubrosPlanCompraContratos", SqlDbType.Structured)
                                {
                                    Value = itemPlanCompras.Rubros
                                };
                                vDbCommand.Parameters.Add(pDatosRubros);

                                vDataBase.AddInParameter(vDbCommand, "@valorTotalPlanCompras", DbType.Int32, itemPlanCompras.ValorTotal);

                                string key, value;

                                StringBuilder keyValue = new StringBuilder();

                                using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                                {
                                    while (vDataReaderResults.Read())
                                    {
                                        key = vDataReaderResults["llave"] != DBNull.Value ? Convert.ToString(vDataReaderResults["llave"].ToString()) : string.Empty;
                                        value = vDataReaderResults["Valor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Valor"].ToString()) : string.Empty;
                                        keyValue.AppendLine(key + "-" + value);
                                    }

                                    if (keyValue.Length > 0)
                                        result.Add(item.Fila, keyValue.ToString());     
                                }                       
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result.Add(item.Fila, ex.Message);
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public PlanComprasMigracion ObtenerPlanCompras(int consecutivoPlancompras, int vigencia, string codigoRegional)
        {
            PlanComprasMigracion result = null;

            try
            {
                if (codigoRegional == "5" || codigoRegional == "8")
                    codigoRegional = "0" + codigoRegional;

                ServicioPACCO.WSContratosPACCOSoapClient client = new ServicioPACCO.WSContratosPACCOSoapClient();

                codigoRegional = codigoRegional + "00";

                var items = client.GetListaConsecutivosEncabezadosPACCO
                            (vigencia,
                             codigoRegional,
                             0,
                             0,
                             consecutivoPlancompras,
                             null,
                             0,
                             0,
                             null);

                if (items != null && items.Count() > 0)
                {
                    ServicioPACCO.GetObjetosContractualesServicio_Result itemPlanCompras = items[0];
                    result = new PlanComprasMigracion();
                    result.Consecutivo = int.Parse(itemPlanCompras.consecutivo.ToString());
                    result.ValorTotal = itemPlanCompras.valor_contrato.HasValue ? itemPlanCompras.valor_contrato.Value : 0;
                    result.Vigencia = itemPlanCompras.anop_vigencia;

                    var vDtProductos = new DataTable();
                    vDtProductos.Columns.Add(new DataColumn("CodigoProducto", Type.GetType("System.Decimal")));
                    vDtProductos.Columns.Add(new DataColumn("CantidadCupos", Type.GetType("System.Decimal")));
                    vDtProductos.Columns.Add(new DataColumn("IdDetalleObjeto", Type.GetType("System.String")));
                    vDtProductos.Columns.Add(new DataColumn("Tiempo", Type.GetType("System.Decimal")));
                    vDtProductos.Columns.Add(new DataColumn("ValorUnitario", Type.GetType("System.Decimal")));
                    vDtProductos.Columns.Add(new DataColumn("UnidadMedida", Type.GetType("System.String")));
                    vDtProductos.Columns.Add(new DataColumn("UnidadTiempo", Type.GetType("System.String")));
                    vDtProductos.Columns.Add(new DataColumn("DescripcionProducto", Type.GetType("System.String")));

                    var misItemsProductos = client.GetListaDetalleProductoPACCO(vigencia, consecutivoPlancompras);

                    foreach (var itemProducto in misItemsProductos)
                    {
                        var filaDatatable = vDtProductos.NewRow();
                        filaDatatable[0] =  itemProducto.codigo_producto;
                        filaDatatable[1] =  itemProducto.cantidad;
                        filaDatatable[2] =  itemProducto.iddetalleobjetocontractual;
                        filaDatatable[3] =  itemProducto.tiempo;
                        filaDatatable[4] =  itemProducto.valor_unitario;
                        filaDatatable[5] =  itemProducto.unidad_medida;
                        filaDatatable[6] = itemProducto.tipotiempo;
                        filaDatatable[7] = itemProducto.nombre_producto;
                        vDtProductos.Rows.Add(filaDatatable);
                    }

                    result.Productos = vDtProductos;

                    var vDtRubros = new DataTable();
                    vDtRubros.Columns.Add(new DataColumn("CodigoRubro", Type.GetType("System.String")));
                    vDtRubros.Columns.Add(new DataColumn("ValorRubroPresupuestal", Type.GetType("System.Decimal")));
                    vDtRubros.Columns.Add(new DataColumn("IdPagosDetalle", Type.GetType("System.String")));

                    var misRubros = client.GetListaDetalleRubroPACCO(vigencia, consecutivoPlancompras);

                    foreach (var itemRubro in misRubros)
                    {
                        var filaDatatable = vDtRubros.NewRow();
                        filaDatatable[0] = itemRubro.codigo_rubro;
                        filaDatatable[1] = itemRubro.total_rubro;
                        filaDatatable[2] = itemRubro.idpagosdetalle.ToString();
                        vDtRubros.Rows.Add(filaDatatable);
                    }

                    result.Rubros = vDtRubros;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;

        }
    }
}
