﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Proveedores_Contratos
    /// </summary>
    public class Proveedores_ContratosDAL : GeneralDAL
    {
        public Proveedores_ContratosDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int InsertarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdProveedoresContratos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pProveedores_Contratos.IdProveedores);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pProveedores_Contratos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pProveedores_Contratos.UsuarioCrea);
                    
                    vDataBase.AddInParameter(vDbCommand, "@EsCesion", DbType.Boolean, pProveedores_Contratos.EsCesion);
                    vDataBase.AddInParameter(vDbCommand, "@IdCesion", DbType.Int32, pProveedores_Contratos.idCesion);
                    
                    if (pProveedores_Contratos.idProveedorPadre.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdProveedorPadre", DbType.Int32, pProveedores_Contratos.idProveedorPadre.Value);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProveedores_Contratos.IdProveedoresContratos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProveedoresContratos").ToString());
                    GenerarLogAuditoria(pProveedores_Contratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int ModificarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedoresContratos", DbType.Int32, pProveedores_Contratos.IdProveedoresContratos);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pProveedores_Contratos.IdProveedores);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pProveedores_Contratos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProveedores_Contratos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProveedores_Contratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int EliminarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedoresContratos", DbType.Int32, pProveedores_Contratos.IdProveedoresContratos);
                    if (pProveedores_Contratos.IdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pProveedores_Contratos.IdContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProveedores_Contratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedoresContratos"></param>
        public Proveedores_Contratos ConsultarProveedores_Contratos(int pIdProveedoresContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedoresContratos", DbType.Int32, pIdProveedoresContratos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
                        while (vDataReaderResults.Read())
                        {
                            vProveedores_Contratos.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vProveedores_Contratos.IdProveedoresContratos;
                            vProveedores_Contratos.IdProveedores = vDataReaderResults["IdProveedores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedores"].ToString()) : vProveedores_Contratos.IdProveedores;
                            vProveedores_Contratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vProveedores_Contratos.IdContrato;
                            vProveedores_Contratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProveedores_Contratos.UsuarioCrea;
                            vProveedores_Contratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProveedores_Contratos.FechaCrea;
                            vProveedores_Contratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProveedores_Contratos.UsuarioModifica;
                            vProveedores_Contratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProveedores_Contratos.FechaModifica;
                        }
                        return vProveedores_Contratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdContrato"></param>
        public List<Proveedores_Contratos> ConsultarProveedores_Contratoss(int? pIdProveedores, int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratoss_Consultar"))
                {
                    if (pIdProveedores != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pIdProveedores);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Proveedores_Contratos> vListaProveedores_Contratos = new List<Proveedores_Contratos>();
                        while (vDataReaderResults.Read())
                        {
                            Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
                            vProveedores_Contratos.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vProveedores_Contratos.IdProveedoresContratos;
                            vProveedores_Contratos.IdProveedores = vDataReaderResults["IdProveedores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedores"].ToString()) : vProveedores_Contratos.IdProveedores;
                            vProveedores_Contratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vProveedores_Contratos.IdContrato;
                            vProveedores_Contratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProveedores_Contratos.UsuarioCrea;
                            vProveedores_Contratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProveedores_Contratos.FechaCrea;
                            vProveedores_Contratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProveedores_Contratos.UsuarioModifica;
                            vProveedores_Contratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProveedores_Contratos.FechaModifica;
                            vListaProveedores_Contratos.Add(vProveedores_Contratos);
                        }
                        return vListaProveedores_Contratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de un contrato dentro de un proveedor
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public KeyValuePair<int,string> ExisteProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        { 
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratos_VerificaContratoProv"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pProveedores_Contratos.IdProveedores);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pProveedores_Contratos.IdContrato);

                    int key;
                    string valor;

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            key = vDataReaderResults["id"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["id"].ToString()) : 0;
                            valor = vDataReaderResults["mensaje"] != DBNull.Value ? Convert.ToString(vDataReaderResults["mensaje"].ToString()) : string.Empty;
                            return new KeyValuePair<int, string>(key, valor);
                        }
                    }
                    return new KeyValuePair<int, string>(0, string.Empty);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Proveedores Oferente</param>
        /// <param name="pTipoidentificacion">Tipo de identificaciòn en una entidad Proveedores Oferente</param>
        /// <param name="pIdentificacion">Identificaciòn en una entidad Proveedores Oferente</param>
        /// <param name="pProveedor">Proveedor en una entidad Proveedores Oferente</param>
        /// <param name="pEstado">Estado en una entidad Proveedores Oferente</param>
        /// <param name="pUsuarioCrea">Usuario que creaciòn para una entidad Proveedores Oferente</param>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<Proveedores_Contratos> ConsultarContratistasContratos(int? pcontrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ListaProveedores_Contratos_Consultar"))
                {

                    if (pcontrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pcontrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Proveedores_Contratos> vListaProveedores_Contratos = new List<Proveedores_Contratos>();
                        while (vDataReaderResults.Read())
                        {
                            Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();

                            vProveedores_Contratos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProveedores_Contratos.IdTercero;
                            vProveedores_Contratos.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vProveedores_Contratos.IdEntidad;
                            vProveedores_Contratos.Proveedor = vDataReaderResults["Razonsocila"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razonsocila"].ToString()) : vProveedores_Contratos.ObserValidador;
                            vProveedores_Contratos.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vProveedores_Contratos.ObserValidador;
                            vProveedores_Contratos.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vProveedores_Contratos.ObserValidador;
                            vProveedores_Contratos.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vProveedores_Contratos.IdTercero;
                            vProveedores_Contratos.TipoPersonaNombre = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vProveedores_Contratos.ObserValidador;
                            vListaProveedores_Contratos.Add(vProveedores_Contratos);
                        }
                        return vListaProveedores_Contratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la informacion de contratistas relacionados con un contrato
        /// </summary>
        /// <param name="pContrato">Entero con el identificador del contrato</param>
        /// <param name="pIntegrantesUnionTemporal">Booleano que indica si se obtiene la información de contratistas o la de los integrantes 
        /// del consorcio o union temporal asociados al contrato</param>
        /// <returns>Listado de contratistas obtenidos de la base de datos</returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContrato(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato);
                    if (pIntegrantesUnionTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IntegrantesUnionTemporal", DbType.Int32, pIntegrantesUnionTemporal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Proveedores_Contratos> vListaProveedores_Contratos = new List<Proveedores_Contratos>();
                        while (vDataReaderResults.Read())
                        {
                            Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
                            vProveedores_Contratos.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vProveedores_Contratos.IdProveedoresContratos;
                            vProveedores_Contratos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProveedores_Contratos.IdTercero;
                            vProveedores_Contratos.TipoPersonaNombre = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vProveedores_Contratos.TipoPersonaNombre;
                            vProveedores_Contratos.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vProveedores_Contratos.TipoIdentificacion;
                            vProveedores_Contratos.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vProveedores_Contratos.NumeroIdentificacion;
                            vProveedores_Contratos.Proveedor = vDataReaderResults["InformacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionContratista"].ToString()) : vProveedores_Contratos.Proveedor;
                            vProveedores_Contratos.TipoDocumentoRepresentanteLegal = vDataReaderResults["TipoDocumentoRepresentante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoRepresentante"].ToString()) : vProveedores_Contratos.TipoDocumentoRepresentanteLegal;
                            vProveedores_Contratos.NumeroIDRepresentanteLegal = vDataReaderResults["IdentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepLegal"].ToString()) : vProveedores_Contratos.NumeroIDRepresentanteLegal;
                            vProveedores_Contratos.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vProveedores_Contratos.RepresentanteLegal;
                            vProveedores_Contratos.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vProveedores_Contratos.PorcentajeParticipacion;
                            vProveedores_Contratos.NumeroIdentificacionIntegrante = vDataReaderResults["NumeroIdentificacionIntegrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionIntegrante"].ToString()) : vProveedores_Contratos.NumeroIdentificacionIntegrante;
                            vProveedores_Contratos.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vProveedores_Contratos.IdEntidad;
                            vProveedores_Contratos.EsCesion = vDataReaderResults["EsCesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsCesion"].ToString()) : vProveedores_Contratos.EsCesion;
                            vListaProveedores_Contratos.Add(vProveedores_Contratos);
                        }
                        return vListaProveedores_Contratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene
        /// </summary>
        /// <param name="idContratoMigrado"></param>
        /// <returns></returns>
        public List<IntegrantesUTConsorcio> ObtenerIntegrantesUTConsorcioHistorico(int idContratoMigrado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ProveedoresContratos_IntegrantesUTConsorcioHistorico_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoMigrado", DbType.Int32, idContratoMigrado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<IntegrantesUTConsorcio> vListaProveedores_Contratos = new List<IntegrantesUTConsorcio>();
                        while (vDataReaderResults.Read())
                        {
                            IntegrantesUTConsorcio vProveedores_Contratos = new IntegrantesUTConsorcio();
                            vProveedores_Contratos.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vProveedores_Contratos.IdTipoPersona;
                            vProveedores_Contratos.TipoIdentificacion = vDataReaderResults["Tipoidentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tipoidentificacion"].ToString()) : vProveedores_Contratos.TipoIdentificacion;
                            vProveedores_Contratos.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vProveedores_Contratos.NumeroIdentificacion;
                            vProveedores_Contratos.NombreRazonSocial = vDataReaderResults["NombreRazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRazonSocial"].ToString()) : vProveedores_Contratos.NombreRazonSocial;
                            vProveedores_Contratos.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vProveedores_Contratos.Direccion;
                            vProveedores_Contratos.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vProveedores_Contratos.Telefono;
                            vProveedores_Contratos.Ubicacion = vDataReaderResults["Ubicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ubicacion"].ToString()) : vProveedores_Contratos.Ubicacion;
                            vProveedores_Contratos.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vProveedores_Contratos.CorreoElectronico;
                            vProveedores_Contratos.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vProveedores_Contratos.PorcentajeParticipacion;
                            vProveedores_Contratos.NumeroIdentificacionRepresentanteLegal = vDataReaderResults["NumeroIdentificacionRepresentanteLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionRepresentanteLegal"].ToString()) : vProveedores_Contratos.NumeroIdentificacionRepresentanteLegal;
                            vProveedores_Contratos.NombreRepresentanteLegal = vDataReaderResults["NombreRepresentanteLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRepresentanteLegal"].ToString()) : vProveedores_Contratos.NombreRepresentanteLegal;
                            vListaProveedores_Contratos.Add(vProveedores_Contratos);
                        }
                        return vListaProveedores_Contratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContrato"></param>
        /// <param name="pIntegrantesUnionTemporal"></param>
        /// <returns></returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContratoHistorico(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener_Historico"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato);
                    if (pIntegrantesUnionTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IntegrantesUnionTemporal", DbType.Int32, pIntegrantesUnionTemporal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Proveedores_Contratos> vListaProveedores_Contratos = new List<Proveedores_Contratos>();
                        while (vDataReaderResults.Read())
                        {
                            Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
                            vProveedores_Contratos.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vProveedores_Contratos.IdProveedoresContratos;
                            vProveedores_Contratos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProveedores_Contratos.IdTercero;
                            vProveedores_Contratos.TipoPersonaNombre = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vProveedores_Contratos.TipoPersonaNombre;
                            vProveedores_Contratos.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vProveedores_Contratos.TipoIdentificacion;
                            vProveedores_Contratos.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vProveedores_Contratos.NumeroIdentificacion;
                            vProveedores_Contratos.Proveedor = vDataReaderResults["InformacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionContratista"].ToString()) : vProveedores_Contratos.Proveedor;
                            vProveedores_Contratos.TipoDocumentoRepresentanteLegal = vDataReaderResults["TipoDocumentoRepresentante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoRepresentante"].ToString()) : vProveedores_Contratos.TipoDocumentoRepresentanteLegal;
                            vProveedores_Contratos.NumeroIDRepresentanteLegal = vDataReaderResults["IdentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepLegal"].ToString()) : vProveedores_Contratos.NumeroIDRepresentanteLegal;
                            vProveedores_Contratos.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vProveedores_Contratos.RepresentanteLegal;
                            vProveedores_Contratos.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vProveedores_Contratos.PorcentajeParticipacion;
                            vProveedores_Contratos.NumeroIdentificacionIntegrante = vDataReaderResults["NumeroIdentificacionIntegrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionIntegrante"].ToString()) : vProveedores_Contratos.NumeroIdentificacionIntegrante;
                            vProveedores_Contratos.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vProveedores_Contratos.IdEntidad;
                            vProveedores_Contratos.EsCesion = vDataReaderResults["EsCesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsCesion"].ToString()) : vProveedores_Contratos.EsCesion;
                            vListaProveedores_Contratos.Add(vProveedores_Contratos);
                        }
                        return vListaProveedores_Contratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContrato"></param>
        /// <param name="pIntegrantesUnionTemporal"></param>
        /// <returns></returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContratoActuales(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ProveedoresContratos_Contrato_Obtener_Actuales"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato);
                    if (pIntegrantesUnionTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IntegrantesUnionTemporal", DbType.Int32, pIntegrantesUnionTemporal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Proveedores_Contratos> vListaProveedores_Contratos = new List<Proveedores_Contratos>();
                        while (vDataReaderResults.Read())
                        {
                            Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
                            vProveedores_Contratos.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vProveedores_Contratos.IdProveedoresContratos;
                            vProveedores_Contratos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProveedores_Contratos.IdTercero;
                            vProveedores_Contratos.TipoPersonaNombre = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vProveedores_Contratos.TipoPersonaNombre;
                            vProveedores_Contratos.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vProveedores_Contratos.TipoIdentificacion;
                            vProveedores_Contratos.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vProveedores_Contratos.NumeroIdentificacion;
                            vProveedores_Contratos.Proveedor = vDataReaderResults["InformacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionContratista"].ToString()) : vProveedores_Contratos.Proveedor;
                            vProveedores_Contratos.TipoDocumentoRepresentanteLegal = vDataReaderResults["TipoDocumentoRepresentante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoRepresentante"].ToString()) : vProveedores_Contratos.TipoDocumentoRepresentanteLegal;
                            vProveedores_Contratos.NumeroIDRepresentanteLegal = vDataReaderResults["IdentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepLegal"].ToString()) : vProveedores_Contratos.NumeroIDRepresentanteLegal;
                            vProveedores_Contratos.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vProveedores_Contratos.RepresentanteLegal;
                            vProveedores_Contratos.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vProveedores_Contratos.PorcentajeParticipacion;
                            vProveedores_Contratos.NumeroIdentificacionIntegrante = vDataReaderResults["NumeroIdentificacionIntegrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionIntegrante"].ToString()) : vProveedores_Contratos.NumeroIdentificacionIntegrante;
                            vProveedores_Contratos.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vProveedores_Contratos.IdEntidad;
                            vProveedores_Contratos.EsCesion = vDataReaderResults["EsCesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsCesion"].ToString()) : vProveedores_Contratos.EsCesion;
                            vListaProveedores_Contratos.Add(vProveedores_Contratos);
                        }
                        return vListaProveedores_Contratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Proveedores_Contratos> ObtenerProveedoresContratoMigrados(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ProveedoresContratosMigrados_Contrato_Obtener_Actuales"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContrato);
                    if (pIntegrantesUnionTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IntegrantesUnionTemporal", DbType.Int32, pIntegrantesUnionTemporal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Proveedores_Contratos> vListaProveedores_Contratos = new List<Proveedores_Contratos>();
                        while (vDataReaderResults.Read())
                        {
                            Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
                            //vProveedores_Contratos.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vProveedores_Contratos.IdProveedoresContratos;
                            //vProveedores_Contratos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProveedores_Contratos.IdTercero;
                            vProveedores_Contratos.TipoPersonaNombre = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vProveedores_Contratos.TipoPersonaNombre;
                            vProveedores_Contratos.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vProveedores_Contratos.TipoIdentificacion;
                            vProveedores_Contratos.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vProveedores_Contratos.NumeroIdentificacion;
                            vProveedores_Contratos.Proveedor = vDataReaderResults["InformacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionContratista"].ToString()) : vProveedores_Contratos.Proveedor;
                            //vProveedores_Contratos.TipoDocumentoRepresentanteLegal = vDataReaderResults["TipoDocumentoRepresentante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoRepresentante"].ToString()) : vProveedores_Contratos.TipoDocumentoRepresentanteLegal;
                            vProveedores_Contratos.NumeroIDRepresentanteLegal = vDataReaderResults["IdentificacionRepLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdentificacionRepLegal"].ToString()) : vProveedores_Contratos.NumeroIDRepresentanteLegal;
                            vProveedores_Contratos.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vProveedores_Contratos.RepresentanteLegal;
                            vProveedores_Contratos.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vProveedores_Contratos.PorcentajeParticipacion;
                            //vProveedores_Contratos.NumeroIdentificacionIntegrante = vDataReaderResults["NumeroIdentificacionIntegrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionIntegrante"].ToString()) : vProveedores_Contratos.NumeroIdentificacionIntegrante;
                            vProveedores_Contratos.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vProveedores_Contratos.IdEntidad;
                            //vProveedores_Contratos.EsCesion = vDataReaderResults["EsCesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsCesion"].ToString()) : vProveedores_Contratos.EsCesion;
                            vListaProveedores_Contratos.Add(vProveedores_Contratos);
                        }
                        return vListaProveedores_Contratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
