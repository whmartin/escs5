﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    public class CodigosSECOPDAL : GeneralDAL
    {
        public CodigosSECOPDAL()
        {
        }
        /// <summary>
        /// 
        /// Método de inserción para la entidad CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int InsertarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CodigoSECOP_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCodigoSECOP", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSECOP", DbType.String, cCodigoSECOP.CodigoSECOP);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, cCodigoSECOP.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, cCodigoSECOP.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, cCodigoSECOP.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    cCodigoSECOP.IdCodigoSECOP = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCodigoSECOP").ToString());
                    GenerarLogAuditoria(cCodigoSECOP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pNumeroProcesos"></param>
        public int ModificarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CodigoSECOP_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoSECOP", DbType.Int32, cCodigoSECOP.IdCodigoSECOP);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSECOP", DbType.String, cCodigoSECOP.CodigoSECOP);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, cCodigoSECOP.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, cCodigoSECOP.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, cCodigoSECOP.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(cCodigoSECOP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// Método de modificación para la entidad CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int EliminarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_CodigoSECOP_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoSECOP", DbType.Int32, cCodigoSECOP.IdCodigoSECOP);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(cCodigoSECOP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CodigosSECOP ConsultarCodigoSECOP(int cIdCodigoSECOP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CodigoSECOP_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "IdCodigoSECOP", DbType.Int32, cIdCodigoSECOP);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CodigosSECOP vCodigoSECOP = new CodigosSECOP();
                        while (vDataReaderResults.Read())
                        {
                            vCodigoSECOP.IdCodigoSECOP = vDataReaderResults["IdCodigoSECOP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCodigoSECOP"].ToString()) : vCodigoSECOP.IdCodigoSECOP;
                            vCodigoSECOP.CodigoSECOP = vDataReaderResults["CodigoSECOP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSECOP"].ToString()) : vCodigoSECOP.CodigoSECOP;
                            vCodigoSECOP.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCodigoSECOP.Descripcion;
                            vCodigoSECOP.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCodigoSECOP.Estado;
                            vCodigoSECOP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCodigoSECOP.UsuarioCrea;
                            vCodigoSECOP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCodigoSECOP.FechaCrea;
                            vCodigoSECOP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCodigoSECOP.UsuarioModifica;
                            vCodigoSECOP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCodigoSECOP.FechaModifica;
                        }
                        return vCodigoSECOP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CodigosSECOP> ConsultarVariosCodigoSECOP(Boolean? cEstado, String cCodigoSECOP, int? cantidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CodigoSECOP_ConsultarVarios"))
                {

                    if (cEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, cEstado);
                    if (!string.IsNullOrEmpty(cCodigoSECOP))
                        vDataBase.AddInParameter(vDbCommand, "@CodigoSECOP", DbType.String, cCodigoSECOP);
                    if ( cantidad.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@Cantidad", DbType.Int32, cantidad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CodigosSECOP> vListaCodigosSECOP = new List<CodigosSECOP>();
                        while (vDataReaderResults.Read())
                        {
                            CodigosSECOP vCodigoSECOP = new CodigosSECOP();
                            vCodigoSECOP.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCodigoSECOP.Estado;
                            vCodigoSECOP.CodigoSECOP = vDataReaderResults["CodigoSECOP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSECOP"].ToString()) : vCodigoSECOP.CodigoSECOP;
                            vCodigoSECOP.IdCodigoSECOP = vDataReaderResults["IdCodigoSECOP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCodigoSECOP"].ToString()) : vCodigoSECOP.IdCodigoSECOP;
                            vListaCodigosSECOP.Add(vCodigoSECOP);
                        }
                        return vListaCodigosSECOP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        /// 
        public Dictionary<int, string> ObtenerCodigosSECOPContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CodigoSECOP_ConsultarContrato"))
                {
                   vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.String, idContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Dictionary<int, string> vListaCodigosSECOP = new Dictionary<int, string>();
                        int key;
                        string value;
                        while (vDataReaderResults.Read())
                        {
                            value = vDataReaderResults["CodigoSECOP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSECOP"].ToString()) : string.Empty;
                            key   = vDataReaderResults["IdCodigoSECOP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCodigoSECOP"].ToString()) : 0;
                            vListaCodigosSECOP.Add(key,value);
                        }

                        return vListaCodigosSECOP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="idCodigoSecop"></param>
        /// <returns></returns>
        public int AsociarCodigoSECOPContrato(int idContrato, int idCodigoSecop)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CodigoSECOP_AsociarCodigoContrato"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoSECOP", DbType.Int32, idCodigoSecop);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="idCodigoSecop"></param>
        /// <returns></returns>
        public int EliminarCodigoSECOPContrato(int idContrato, int idCodigoSecop)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_CodigoSECOP_EliminarContrato"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCodigoSECOP", DbType.Int32, idCodigoSecop);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="vinculoSECOP"></param>
        /// <returns></returns>
        public int ActualizarVinculoSECOP(int idContrato, string vinculoSECOP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VinculoSECOP_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddInParameter(vDbCommand, "@VinculoSECOP", DbType.String, vinculoSECOP);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
