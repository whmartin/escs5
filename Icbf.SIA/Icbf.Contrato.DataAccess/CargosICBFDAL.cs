using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    ///  Clase de la capa de acceso que contiene los métodos para Insertar, Modificar, Eliminar y Consultar registros de la entidad  CargosICBFDAL
    /// </summary>
    public class CargosICBFDAL : GeneralDAL
    {
        public CargosICBFDAL()
        {
        }
        /// <summary>
        /// Gonet
        /// Insertar Cargos ICBF 
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pCargosICBF">Instancia que contiene la información del Cargo ICBF</param>
        /// <returns>Identificador de la base de datos del registro insertado</returns>
        public int InsertarCargosICBF(CargosICBF pCargosICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_CargosICBF_Consultar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCargo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodCargo", DbType.String, pCargosICBF.CodCargo);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pCargosICBF.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pCargosICBF.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCargosICBF.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCargosICBF.IdCargo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCargo").ToString());
                    GenerarLogAuditoria(pCargosICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Modificar Cargos ICBF 
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pCargosICBF">Instancia que contiene la información del Cargo ICBF</param>
        /// <returns>Identificador de la base de datos del registro Modificado</returns>
        public int ModificarCargosICBF(CargosICBF pCargosICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_CargosICBF_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCargo", DbType.Int32, pCargosICBF.IdCargo);
                    vDataBase.AddInParameter(vDbCommand, "@CodCargo", DbType.String, pCargosICBF.CodCargo);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pCargosICBF.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pCargosICBF.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCargosICBF.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCargosICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar Cargos ICBF 
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pCargosICBF">Instancia que contiene la información del Cargo ICBF</param>
        /// <returns>Identificador de la base de datos del registro Eliminado</returns>
        public int EliminarCargosICBF(CargosICBF pCargosICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_CargosICBF_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCargo", DbType.Int32, pCargosICBF.IdCargo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCargosICBF, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar Cargos ICBF por Id del cargo
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pIdCargo">Valor entero con el Id del cargo</param>
        /// <returns>Instancia que contiene la información del archivo recuperada de la base de datos con el cargo ICBF</returns>
        public CargosICBF ConsultarCargosICBF(int pIdCargo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_CargosICBF_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCargo", DbType.Int32, pIdCargo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CargosICBF vCargosICBF = new CargosICBF();
                        while (vDataReaderResults.Read())
                        {
                            vCargosICBF.IdCargo = vDataReaderResults["IdCargo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargo"].ToString()) : vCargosICBF.IdCargo;
                            vCargosICBF.CodCargo = vDataReaderResults["CodCargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodCargo"].ToString()) : vCargosICBF.CodCargo;
                            vCargosICBF.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vCargosICBF.Nombre;
                            vCargosICBF.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCargosICBF.Estado;
                            vCargosICBF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCargosICBF.UsuarioCrea;
                            vCargosICBF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCargosICBF.FechaCrea;
                            vCargosICBF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCargosICBF.UsuarioModifica;
                            vCargosICBF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCargosICBF.FechaModifica;
                        }
                        return vCargosICBF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Cargos ICBF
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pCodCargo">Cadena de texto con el código del cargo</param>
        /// <param name="pNombre">Cadena de texto con el Nombre del cargo</param>
        /// <param name="pEstado">Booleano con el estado del cargo</param>
        /// <returns>Lista de la Instancia que contiene la información del archivo recuperada de la base de datos con los cargos ICBF</returns>
        public List<CargosICBF> ConsultarCargosICBFs(String pCodCargo, String pNombre, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_CargosICBFs_Consultar"))
                {
                    if(pCodCargo != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodCargo", DbType.String, pCodCargo);
                    if(pNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CargosICBF> vListaCargosICBF = new List<CargosICBF>();
                        while (vDataReaderResults.Read())
                        {
                                CargosICBF vCargosICBF = new CargosICBF();
                            vCargosICBF.IdCargo = vDataReaderResults["IdCargo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargo"].ToString()) : vCargosICBF.IdCargo;
                            vCargosICBF.CodCargo = vDataReaderResults["CodCargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodCargo"].ToString()) : vCargosICBF.CodCargo;
                            vCargosICBF.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vCargosICBF.Nombre;
                            vCargosICBF.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCargosICBF.Estado;
                            vCargosICBF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCargosICBF.UsuarioCrea;
                            vCargosICBF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCargosICBF.FechaCrea;
                            vCargosICBF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCargosICBF.UsuarioModifica;
                            vCargosICBF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCargosICBF.FechaModifica;
                                vListaCargosICBF.Add(vCargosICBF);
                        }
                        return vListaCargosICBF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
