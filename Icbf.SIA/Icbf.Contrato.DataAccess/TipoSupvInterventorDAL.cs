using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Tipo supervisor interventor
    /// </summary>
    public class TipoSupvInterventorDAL : GeneralDAL
    {
        public TipoSupvInterventorDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int InsertarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoSupvInterventor_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoSupvInterventor", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTipoSupvInterventor.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoSupvInterventor.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoSupvInterventor.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoSupvInterventor.IdTipoSupvInterventor = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoSupvInterventor").ToString());
                    GenerarLogAuditoria(pTipoSupvInterventor, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int ModificarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoSupvInterventor_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSupvInterventor", DbType.Int32, pTipoSupvInterventor.IdTipoSupvInterventor);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTipoSupvInterventor.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoSupvInterventor.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoSupvInterventor.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoSupvInterventor, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int EliminarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSupvInterventor", DbType.Int32, pTipoSupvInterventor.IdTipoSupvInterventor);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoSupvInterventor, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pIdTipoSupvInterventor"></param>
        /// <returns></returns>
        public TipoSupvInterventor ConsultarTipoSupvInterventor(int pIdTipoSupvInterventor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoSupvInterventor_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSupvInterventor", DbType.Int32, pIdTipoSupvInterventor);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoSupvInterventor vTipoSupvInterventor = new TipoSupvInterventor();
                        while (vDataReaderResults.Read())
                        {
                            vTipoSupvInterventor.IdTipoSupvInterventor = vDataReaderResults["IdTipoSupvInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSupvInterventor"].ToString()) : vTipoSupvInterventor.IdTipoSupvInterventor;
                            vTipoSupvInterventor.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTipoSupvInterventor.Nombre;
                            vTipoSupvInterventor.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoSupvInterventor.Estado;
                            vTipoSupvInterventor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoSupvInterventor.UsuarioCrea;
                            vTipoSupvInterventor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoSupvInterventor.FechaCrea;
                            vTipoSupvInterventor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoSupvInterventor.UsuarioModifica;
                            vTipoSupvInterventor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoSupvInterventor.FechaModifica;
                        }
                        return vTipoSupvInterventor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pNombre"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoSupvInterventor> ConsultarTipoSupvInterventors(String pNombre, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_TipoSupvInterventors_Consultar"))
                {
                    if(pNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoSupvInterventor> vListaTipoSupvInterventor = new List<TipoSupvInterventor>();
                        while (vDataReaderResults.Read())
                        {
                                TipoSupvInterventor vTipoSupvInterventor = new TipoSupvInterventor();
                            vTipoSupvInterventor.IdTipoSupvInterventor = vDataReaderResults["IdTipoSupvInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSupvInterventor"].ToString()) : vTipoSupvInterventor.IdTipoSupvInterventor;
                            vTipoSupvInterventor.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTipoSupvInterventor.Nombre;
                            vTipoSupvInterventor.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoSupvInterventor.Estado;
                            if (vTipoSupvInterventor.Estado)
                            {
                                vTipoSupvInterventor.EstadoString = "Activo";
                            } else
                            {
                                vTipoSupvInterventor.EstadoString = "Inactivo";
                            }
                            vTipoSupvInterventor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoSupvInterventor.UsuarioCrea;
                            vTipoSupvInterventor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoSupvInterventor.FechaCrea;
                            vTipoSupvInterventor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoSupvInterventor.UsuarioModifica;
                            vTipoSupvInterventor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoSupvInterventor.FechaModifica;
                                vListaTipoSupvInterventor.Add(vTipoSupvInterventor);
                        }
                        return vListaTipoSupvInterventor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
