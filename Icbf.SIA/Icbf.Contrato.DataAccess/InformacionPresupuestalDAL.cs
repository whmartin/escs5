using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Informaci�n Presupuestal
    /// </summary>
    public class InformacionPresupuestalDAL : GeneralDAL
    {
        public InformacionPresupuestalDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int InsertarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdInformacionPresupuestal", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.String, pInformacionPresupuestal.NumeroCDP);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCDP", DbType.Decimal, pInformacionPresupuestal.ValorCDP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionCDP", DbType.DateTime, pInformacionPresupuestal.FechaExpedicionCDP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInformacionPresupuestal.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pInformacionPresupuestal.IdInformacionPresupuestal = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdInformacionPresupuestal").ToString());
                    GenerarLogAuditoria(pInformacionPresupuestal, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int ModificarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestal_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionPresupuestal", DbType.Int32, pInformacionPresupuestal.IdInformacionPresupuestal);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.String, pInformacionPresupuestal.NumeroCDP);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCDP", DbType.Decimal, pInformacionPresupuestal.ValorCDP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionCDP", DbType.DateTime, pInformacionPresupuestal.FechaExpedicionCDP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInformacionPresupuestal.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInformacionPresupuestal, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int EliminarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionPresupuestal", DbType.Int32, pInformacionPresupuestal.IdInformacionPresupuestal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInformacionPresupuestal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pIdInformacionPresupuestal"></param>
        /// <returns></returns>
        public InformacionPresupuestal ConsultarInformacionPresupuestal(int pIdInformacionPresupuestal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestal_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionPresupuestal", DbType.Int32, pIdInformacionPresupuestal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        InformacionPresupuestal vInformacionPresupuestal = new InformacionPresupuestal();
                        while (vDataReaderResults.Read())
                        {
                            vInformacionPresupuestal.IdInformacionPresupuestal = vDataReaderResults["IdInformacionPresupuestal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionPresupuestal"].ToString()) : vInformacionPresupuestal.IdInformacionPresupuestal;
                            vInformacionPresupuestal.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vInformacionPresupuestal.NumeroCDP;
                            vInformacionPresupuestal.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vInformacionPresupuestal.ValorCDP;
                            vInformacionPresupuestal.FechaExpedicionCDP = vDataReaderResults["FechaExpedicionCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionCDP"].ToString()) : vInformacionPresupuestal.FechaExpedicionCDP;
                            vInformacionPresupuestal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInformacionPresupuestal.UsuarioCrea;
                            vInformacionPresupuestal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInformacionPresupuestal.FechaCrea;
                            vInformacionPresupuestal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInformacionPresupuestal.UsuarioModifica;
                            vInformacionPresupuestal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInformacionPresupuestal.FechaModifica;
                        }
                        return vInformacionPresupuestal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pFechaExpedicionCDP"></param>
        /// <returns></returns>
        public List<InformacionPresupuestal> ConsultarInformacionPresupuestals(String pNumeroCDP, DateTime? pFechaExpedicionCDP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_InformacionPresupuestals_Consultar"))
                {
                    if(pNumeroCDP != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.String, pNumeroCDP);
                    if(pFechaExpedicionCDP != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionCDP", DbType.DateTime, pFechaExpedicionCDP);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InformacionPresupuestal> vListaInformacionPresupuestal = new List<InformacionPresupuestal>();
                        while (vDataReaderResults.Read())
                        {
                                InformacionPresupuestal vInformacionPresupuestal = new InformacionPresupuestal();
                            vInformacionPresupuestal.IdInformacionPresupuestal = vDataReaderResults["IdInformacionPresupuestal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionPresupuestal"].ToString()) : vInformacionPresupuestal.IdInformacionPresupuestal;
                            vInformacionPresupuestal.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vInformacionPresupuestal.NumeroCDP;
                            vInformacionPresupuestal.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vInformacionPresupuestal.ValorCDP;
                            vInformacionPresupuestal.FechaExpedicionCDP = vDataReaderResults["FechaExpedicionCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionCDP"].ToString()) : vInformacionPresupuestal.FechaExpedicionCDP;
                            vInformacionPresupuestal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInformacionPresupuestal.UsuarioCrea;
                            vInformacionPresupuestal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInformacionPresupuestal.FechaCrea;
                            vInformacionPresupuestal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInformacionPresupuestal.UsuarioModifica;
                            vInformacionPresupuestal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInformacionPresupuestal.FechaModifica;
                                vListaInformacionPresupuestal.Add(vInformacionPresupuestal);
                        }
                        return vListaInformacionPresupuestal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
