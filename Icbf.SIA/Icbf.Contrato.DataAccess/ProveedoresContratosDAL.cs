﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Proveedores_Contratos
    /// </summary>
    public class ProveedoresContratosDAL : GeneralDAL
    {
        public ProveedoresContratosDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedoresContratos"></param>
        public int InsertarProveedoresContratos(ProveedoresContratos pProveedoresContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratos_FormaPago_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdProveedoresContratos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pProveedoresContratos.IdProveedores);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pProveedoresContratos.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pProveedoresContratos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaPago", DbType.Int32, pProveedoresContratos.IdFormaPago);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProveedoresContratos.IdProveedoresContratos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProveedoresContratos").ToString());
                    GenerarLogAuditoria(pProveedoresContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdContrato"></param>
        public List<ProveedoresContratos> ConsultarProveedoresContratoss(int? pIdProveedores, int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Proveedores_Contratoss_Consultar"))
                {
                    if (pIdProveedores != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pIdProveedores);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProveedoresContratos> vListaProveedoresContratos = new List<ProveedoresContratos>();
                        while (vDataReaderResults.Read())
                        {
                            ProveedoresContratos vProveedoresContratos = new ProveedoresContratos();
                            vProveedoresContratos.IdProveedoresContratos = vDataReaderResults["IdProveedoresContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedoresContratos"].ToString()) : vProveedoresContratos.IdProveedoresContratos;
                            vProveedoresContratos.IdProveedores = vDataReaderResults["IdProveedores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedores"].ToString()) : vProveedoresContratos.IdProveedores;
                            vProveedoresContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vProveedoresContratos.IdContrato;
                            vProveedoresContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProveedoresContratos.UsuarioCrea;
                            vProveedoresContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProveedoresContratos.FechaCrea;
                            vProveedoresContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProveedoresContratos.UsuarioModifica;
                            vProveedoresContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProveedoresContratos.FechaModifica;
                            vProveedoresContratos.IdFormaPago = vDataReaderResults["IdFormaPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaPago"].ToString()) : vProveedoresContratos.IdFormaPago;
                            vListaProveedoresContratos.Add(vProveedoresContratos);
                        }
                        return vListaProveedoresContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos por forma de pago y id proveedor
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdFormapago"></param>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public Boolean ConsultarProveedoresContratosFormaPago(int? pIdProveedores, int? pIdFormapago, int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ProveedoresContratos_formaPago_Consultar"))
                {
                    if (pIdProveedores != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProveedores", DbType.Int32, pIdProveedores);
                    if (pIdFormapago != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdFormapago", DbType.Int32, pIdFormapago);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProveedoresContratos> vListaProveedoresContratos = new List<ProveedoresContratos>();
                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
    }
}
