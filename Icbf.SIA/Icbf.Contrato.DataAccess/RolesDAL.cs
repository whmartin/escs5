﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    public class RolesDAL : GeneralDAL
    {
        public RolesDAL()
        {
        }
        /// <summary>
        /// 
        /// Método de inserción para la entidad Roles
        /// Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los Roles</param>
        public int InsertarRoles(RolesContrato pROL)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Roles_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRol", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRol", DbType.String, pROL.NombreRol);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pROL.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pROL.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pROL.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pROL.IdRol = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRol").ToString());
                    GenerarLogAuditoria(pROL, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Roles
        /// Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los Roles</param>
        public int ModificarRoles(RolesContrato pRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Roles_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRol", DbType.Int32, pRol.IdRol);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRol", DbType.String, pRol.NombreRol);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pRol.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRol.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRol.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRol, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// Método de eliminar registros para la entidad Roles
        /// Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los Roles</param>
        public int EliminarRoles(RolesContrato pRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_Roles_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRol", DbType.Int32, pRol.IdRol);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRol, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RolesContrato ConsultarRoles(int pIdRoles)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Roles_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "IdRol", DbType.Int32, pIdRoles);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RolesContrato vRol = new RolesContrato();
                        while (vDataReaderResults.Read())
                        {
                            vRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vRol.IdRol;
                            vRol.NombreRol = vDataReaderResults["NombreRol"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRol"].ToString()) : vRol.NombreRol;
                            vRol.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vRol.Descripcion;
                            vRol.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRol.Estado;
                            vRol.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRol.UsuarioCrea;
                            vRol.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRol.FechaCrea;
                            vRol.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRol.UsuarioModifica;
                            vRol.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRol.FechaModifica;
                        }
                        return vRol;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RolesContrato> ConsultarVariosRoles(Boolean? pEstado, String pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Roles_ConsultarVarios"))
                {

                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (!string.IsNullOrEmpty(pNombreRol))
                        vDataBase.AddInParameter(vDbCommand, "@NombreRol", DbType.String, pNombreRol);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RolesContrato> vListaRoles = new List<RolesContrato>();
                        while (vDataReaderResults.Read())
                        {
                            RolesContrato vRol = new RolesContrato();
                            vRol.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRol.Estado;
                            vRol.NombreRol = vDataReaderResults["NombreRol"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRol"].ToString()) : vRol.NombreRol;
                            vRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vRol.IdRol;
                            vListaRoles.Add(vRol);
                        }
                        return vListaRoles;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
