﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    ///  Clase para la realización de modificaciones al contrato.
    /// </summary>
    public partial class ContratoModificacionDAL : GeneralDAL
    {
        public ContratoModificacionDAL()
        { 
        
        }

        public List<Entity.Contrato> ConsultarContratosModificacion(DateTime? vFechaRegistroSistemaDesde, DateTime? vFechaRegistroSistemaHasta, int? vIdContrato, int? vVigenciaFiscalinicial, int? vIDRegional, int? vIDModalidadSeleccion, int? vIDCategoriaContrato, int? vIDTipoContrato, int? vIDEstadoContrato, string vNumeroContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_Contratos_ModificacionContrato_Consultar"))
                {
                    if (vFechaRegistroSistemaDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaDesde", DbType.DateTime, vFechaRegistroSistemaDesde.Value);
                    if (vFechaRegistroSistemaHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRegistroSistemaHasta", DbType.Int32, vFechaRegistroSistemaHasta.Value);
                    if (vIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, vIdContrato);
                    if (vVigenciaFiscalinicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscalinicial", DbType.Int32, vVigenciaFiscalinicial.Value);
                    if (vIDRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, vIDRegional.Value);
                    if (vIDModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDModalidadSeleccion", DbType.Int32, vIDModalidadSeleccion.Value);
                    if (vIDCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDCategoriaContrato", DbType.Int32, vIDCategoriaContrato.Value);
                    if (vIDTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoContrato", DbType.Int32, vIDTipoContrato.Value);
                    if (vIDEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEstadoContrato", DbType.Int32, vIDEstadoContrato.Value);
                    if (vNumeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, vNumeroContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Entity.Contrato> vListaSolicitudContratos  = new List<Entity.Contrato>();

                        while (vDataReaderResults.Read())
                        {
                            Entity.Contrato vSolicitudContrato = new Entity.Contrato();
                            vSolicitudContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSolicitudContrato.IdContrato;
                            vSolicitudContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vSolicitudContrato.IdEstadoContrato;
                            vSolicitudContrato.NombreEstadoContrato = vDataReaderResults["NombreEstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoContrato"].ToString()) : vSolicitudContrato.NombreEstadoContrato;
                            vSolicitudContrato.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vSolicitudContrato.IdRegionalContrato;
                            vSolicitudContrato.NombreRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vSolicitudContrato.NombreRegional;
                            vSolicitudContrato.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vSolicitudContrato.IdModalidadSeleccion;
                            vSolicitudContrato.NombreModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"]) : vSolicitudContrato.NombreModalidadSeleccion;
                            vSolicitudContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudContrato.UsuarioCrea;
                            vSolicitudContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudContrato.FechaCrea;
                            vSolicitudContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudContrato.UsuarioModifica;
                            vSolicitudContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudContrato.FechaModifica;
                            vSolicitudContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vSolicitudContrato.IdCategoriaContrato;
                            vSolicitudContrato.NombreCategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vSolicitudContrato.NombreCategoriaContrato;
                            vSolicitudContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vSolicitudContrato.IdTipoContrato;
                            vSolicitudContrato.NombreTipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"]) : vSolicitudContrato.NombreTipoContrato;
                            vSolicitudContrato.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consecutivo"]) : vSolicitudContrato.Consecutivo;
                            vSolicitudContrato.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vSolicitudContrato.FechaInicioEjecucion;

                            vListaSolicitudContratos.Add(vSolicitudContrato);
                        }

                        return vListaSolicitudContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ContratoModificacionDetalle ConsultarPorId(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_Contratos_ModificacionContrato_ConsultarModificacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratoModificacionDetalle vSolicitudContrato = null;

                        while (vDataReaderResults.Read())
                        {
                            vSolicitudContrato = new ContratoModificacionDetalle();
                            vSolicitudContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSolicitudContrato.IdContrato;
                            vSolicitudContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vSolicitudContrato.NumeroContrato;
                            vSolicitudContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vSolicitudContrato.IdEstadoContrato;
                            vSolicitudContrato.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vSolicitudContrato.EstadoContrato;
                            vSolicitudContrato.IdRegional = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vSolicitudContrato.IdRegional;
                            vSolicitudContrato.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vSolicitudContrato.Regional;
                            vSolicitudContrato.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vSolicitudContrato.IdModalidadSeleccion;
                            vSolicitudContrato.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"]) : vSolicitudContrato.ModalidadSeleccion;
                            vSolicitudContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSolicitudContrato.UsuarioCrea;
                            vSolicitudContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSolicitudContrato.FechaCrea;
                            vSolicitudContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSolicitudContrato.UsuarioModifica;
                            vSolicitudContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSolicitudContrato.FechaModifica;
                            vSolicitudContrato.IdCategoriaContrato = vDataReaderResults["IdCategoriaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaContrato"].ToString()) : vSolicitudContrato.IdCategoriaContrato;
                            vSolicitudContrato.CategoriaContrato = vDataReaderResults["CategoriaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaContrato"].ToString()) : vSolicitudContrato.CategoriaContrato;
                            vSolicitudContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vSolicitudContrato.IdTipoContrato;
                            vSolicitudContrato.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"]) : vSolicitudContrato.TipoContrato;
                            vSolicitudContrato.CodigoTipoContrato = vDataReaderResults["CodigoTipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoContrato"]) : vSolicitudContrato.CodigoTipoContrato;

                            vSolicitudContrato.IdModalidadAcademica = vDataReaderResults["IdModalidadAcademica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidadAcademica"]) : vSolicitudContrato.IdModalidadAcademica;
                            vSolicitudContrato.IdProfesion = vDataReaderResults["IdProfesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdProfesion"]) : vSolicitudContrato.IdProfesion;
                            vSolicitudContrato.RequiereActaInicio = vDataReaderResults["ActaDeInicio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ActaDeInicio"]) : vSolicitudContrato.RequiereActaInicio;
                            vSolicitudContrato.ManejaCofinanciacion = vDataReaderResults["ManejaAporte"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaAporte"]) : vSolicitudContrato.ManejaCofinanciacion;
                            vSolicitudContrato.ManejaRecursosICBF = vDataReaderResults["ManejaRecurso"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaRecurso"]) : vSolicitudContrato.ManejaRecursosICBF;
                            vSolicitudContrato.RequiereGarantia = vDataReaderResults["RequiereGarantia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereGarantia"]) : vSolicitudContrato.RequiereGarantia;
                            vSolicitudContrato.ManejaRecursosEspecieICBF = vDataReaderResults["AportesEspecieICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportesEspecieICBF"]) : vSolicitudContrato.ManejaRecursosEspecieICBF;
                            vSolicitudContrato.ManejaVigenciasFuturas = vDataReaderResults["ManejaVigenciasFuturas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManejaVigenciasFuturas"]) : vSolicitudContrato.ManejaVigenciasFuturas;
                            vSolicitudContrato.EsContratoConvenioAdhesion = vDataReaderResults["ConvenioAdhesion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvenioAdhesion"]) : vSolicitudContrato.EsContratoConvenioAdhesion;
                            vSolicitudContrato.EsContratoMarco = vDataReaderResults["ConvenioMarco"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvenioMarco"]) : vSolicitudContrato.EsContratoMarco;

                            vSolicitudContrato.IdentificacionSolicitante = vDataReaderResults["IdEmpleadoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleadoSolicitante"]) : vSolicitudContrato.IdentificacionSolicitante;
                            vSolicitudContrato.IdRegionalSolicitante = vDataReaderResults["IdRegionalSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalSolicitante"]) : vSolicitudContrato.IdRegionalSolicitante;
                            vSolicitudContrato.IdCargoSolicitante = vDataReaderResults["IdCargoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoSolicitante"]) : vSolicitudContrato.IdCargoSolicitante;
                            vSolicitudContrato.IdDependenciaSolicitante = vDataReaderResults["IdDependenciaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaSolicitante"]) : vSolicitudContrato.IdDependenciaSolicitante;
            
                            vSolicitudContrato.IdentificacionOrdenadorGasto = vDataReaderResults["IdEmpleadoOrdenadorGasto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleadoOrdenadorGasto"]) : vSolicitudContrato.IdentificacionOrdenadorGasto;
                            vSolicitudContrato.IdRegionalOrdenadorGasto = vDataReaderResults["IdRegionalOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalOrdenador"]) : vSolicitudContrato.IdRegionalOrdenadorGasto;
                            vSolicitudContrato.IdCargoOrdenadorGasto = vDataReaderResults["IdCargoOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoOrdenador"]) : vSolicitudContrato.IdCargoOrdenadorGasto;
                            vSolicitudContrato.IdDependenciaOrdenadorGasto = vDataReaderResults["IdDependenciaOrdenador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaOrdenador"]) : vSolicitudContrato.IdDependenciaOrdenadorGasto;
            
                            vSolicitudContrato.IdVigenciaInicial = vDataReaderResults["IdVigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaInicial"]) : vSolicitudContrato.IdVigenciaInicial;
                            vSolicitudContrato.FechaInicioContrato = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"]) : vSolicitudContrato.FechaInicioContrato;
                            vSolicitudContrato.IdVigenciaFinal = vDataReaderResults["IdVigenciaFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigenciaFinal"]) : vSolicitudContrato.IdVigenciaFinal;
                            vSolicitudContrato.FechaFinalizaContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"]) : vSolicitudContrato.FechaFinalizaContrato;
                            vSolicitudContrato.FechaFinalizaContratoInicial = vDataReaderResults["FechaFinalizacionIniciaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionIniciaContrato"]) : vSolicitudContrato.FechaFinalizaContratoInicial;
                            vSolicitudContrato.FechaSuscripcionContrato = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"]) : vSolicitudContrato.FechaSuscripcionContrato;

                            vSolicitudContrato.ValorContrato = vDataReaderResults["ValorContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorContrato"]) : vSolicitudContrato.ValorContrato;
                            vSolicitudContrato.ValorContratoFinal = vDataReaderResults["ValorContratoFinal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorContratoFinal"]) : vSolicitudContrato.ValorContratoFinal;
                            vSolicitudContrato.ValorAporteDinero = vDataReaderResults["ValorAporteDinero"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporteDinero"]) : vSolicitudContrato.ValorAporteDinero;
                            
                            vSolicitudContrato.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Objeto"]) : vSolicitudContrato.Objeto;
                            vSolicitudContrato.Alcance = vDataReaderResults["Alcance"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Alcance"]) : vSolicitudContrato.Alcance;
                            vSolicitudContrato.IdPlanCompras = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"]) : vSolicitudContrato.IdPlanCompras;
                            vSolicitudContrato.EsFechaFinalCalculada = vDataReaderResults["EsFechaFinalCalculada"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsFechaFinalCalculada"]) : vSolicitudContrato.EsFechaFinalCalculada;

                            vSolicitudContrato.IdRegimenContratacion = vDataReaderResults["IdRegimenContratacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegimenContratacion"]) : vSolicitudContrato.IdRegimenContratacion;
                            vSolicitudContrato.CodigoEstadoContrato = vDataReaderResults["CodigoEstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoContrato"]) : vSolicitudContrato.CodigoEstadoContrato;

                            vSolicitudContrato.VigenciaFinal = vDataReaderResults["VigenciaFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFinal"]) : vSolicitudContrato.VigenciaFinal;
                            vSolicitudContrato.VigenciaInicial = vDataReaderResults["VigenciaInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaInicial"]) : vSolicitudContrato.VigenciaInicial;

                            vSolicitudContrato.FechaProceso = vDataReaderResults["FechaProceso"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaProceso"]) : vSolicitudContrato.FechaProceso;
                            vSolicitudContrato.IdNumeroProceso = vDataReaderResults["IdNumeroProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroProceso"]) : vSolicitudContrato.IdNumeroProceso;
                            vSolicitudContrato.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"]) : vSolicitudContrato.NumeroProceso;

                            vSolicitudContrato.TieneAdiciones = vDataReaderResults["TieneAdiciones"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TieneAdiciones"]) : vSolicitudContrato.TieneAdiciones;
                            vSolicitudContrato.TieneCesiones = vDataReaderResults["TieneCesiones"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TieneCesiones"]) : vSolicitudContrato.TieneCesiones;
                            vSolicitudContrato.TieneProrrogas = vDataReaderResults["TieneProrrogas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TieneProrrogas"]) : vSolicitudContrato.TieneProrrogas;
                            vSolicitudContrato.TieneReducciones = vDataReaderResults["TieneReducciones"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TieneReducciones"]) : vSolicitudContrato.TieneReducciones;
                            vSolicitudContrato.TieneCambioSupervisor = vDataReaderResults["TieneCambiosSupervisor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TieneCambiosSupervisor"]) : vSolicitudContrato.TieneCambioSupervisor;

                            vSolicitudContrato.FechaActaInicio     = vDataReaderResults["FechaActaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaActaInicio"]) : vSolicitudContrato.FechaActaInicio;


                        }

                        return vSolicitudContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
 
        public int Modificar(ContratoModificacionEditar item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_Contratos_ModificacionContrato_Modificar"))
                {
                    int vResultado;

                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, item.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, item.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, item.IdCategoriaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, item.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, item.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoContrato", DbType.String, item.CodigoEstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporteDineroICBF", DbType.Decimal, item.ValorAporteDinero);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSuscripcion", DbType.DateTime, item.FechaSuscripcion);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereGarantia", DbType.Boolean, item.RequiereGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@RequiereActaInicio", DbType.Boolean, item.RequiereActaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@EsFechaCalculada", DbType.Boolean, item.EsFechaCalculada);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.Date, item.FechaInicioEjecuccion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalEjecucion", DbType.Date, item.FechaFinalEjecuccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaInicial", DbType.Int32, item.IdVigenciaInicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFinal", DbType.Int32, item.IdVigenciaFinal);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaVigenciasFuturas", DbType.Boolean, item.ManejaVigenciasFuturas);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaCofinanciacion", DbType.Boolean, item.ManejaCofinanciacion);
                    vDataBase.AddInParameter(vDbCommand, "@ManejaRecursosEspecieICBF", DbType.Boolean, item.ManejaRecursosEspecieICBF);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegimenContratacion", DbType.Int32, item.IdRegimenContratacion);
                    if(item.DiasCalculados.HasValue)
                    vDataBase.AddInParameter(vDbCommand, "@DiasCalculados", DbType.Int32, item.DiasCalculados);                    
                    if(item.MesesCalculados.HasValue)
                    vDataBase.AddInParameter(vDbCommand, "@MesesCalculados", DbType.Int32, item.MesesCalculados);

                    if(item.FechaActaInicio.HasValue)                                                                                               
                        vDataBase.AddInParameter(vDbCommand, "@FechaActaInicio", DbType.DateTime, item.FechaActaInicio);
                                                                                                                      
                    if (!string.IsNullOrEmpty(item.IdModalidadAcademica))
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadAcademica", DbType.String, item.IdModalidadAcademica);

                    if (!string.IsNullOrEmpty(item.IdProfesion))
                        vDataBase.AddInParameter(vDbCommand, "@IdProfesion", DbType.String, item.IdProfesion);

                    if (item.FechaProceso.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@FechaProceso", DbType.DateTime, item.FechaProceso.Value);

                    if (item.IdNumeroProceso.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, item.IdNumeroProceso.Value);                    

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
