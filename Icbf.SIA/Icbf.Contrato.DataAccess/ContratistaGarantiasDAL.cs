using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad ContratistaGarantias
    /// </summary>
    public class ContratistaGarantiasDAL : GeneralDAL
    {
        public ContratistaGarantiasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int InsertarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ContratistaGarantias_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDContratista_Garantias", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pContratistaGarantias.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IDEntidadProvOferente", DbType.Int32, pContratistaGarantias.IDEntidadProvOferente);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContratistaGarantias.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pContratistaGarantias.IDContratista_Garantias = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDContratista_Garantias").ToString());
                    if (pContratistaGarantias.IDContratista_Garantias > 0) // EC-20140911 Se modifico procedimiento para que cuando exista  el contratista traiga el id y si lo trae actue como si lo hubiera creado satisfactoriamente este metodo ya que las validaciones previas dejan pasar a este punto y si existe el contratista se totea
                    {
                        vResultado = 1;
                    }
                    GenerarLogAuditoria(pContratistaGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int ModificarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ContratistaGarantias_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDContratista_Garantias", DbType.Int32, pContratistaGarantias.IDContratista_Garantias);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pContratistaGarantias.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IDEntidadProvOferente", DbType.Int32, pContratistaGarantias.IDEntidadProvOferente);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pContratistaGarantias.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContratistaGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int EliminarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ContratistaGarantias_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDContratista_Garantias", DbType.Int32, pContratistaGarantias.IDContratista_Garantias);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContratistaGarantias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int EliminarContratistaGarantias(int idGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ContratistaGarantias_EliminarPorGarantia"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGarantia", DbType.Int32, idGarantia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pIDContratista_Garantias"></param>
        public ContratistaGarantias ConsultarContratistaGarantias(int pIDContratista_Garantias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ContratistaGarantias_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDContratista_Garantias", DbType.Int32, pIDContratista_Garantias);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratistaGarantias vContratistaGarantias = new ContratistaGarantias();
                        while (vDataReaderResults.Read())
                        {
                            vContratistaGarantias.IDContratista_Garantias = vDataReaderResults["IDContratista_Garantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContratista_Garantias"].ToString()) : vContratistaGarantias.IDContratista_Garantias;
                            vContratistaGarantias.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vContratistaGarantias.IDGarantia;
                            vContratistaGarantias.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vContratistaGarantias.IDEntidadProvOferente;
                            vContratistaGarantias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratistaGarantias.UsuarioCrea;
                            vContratistaGarantias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratistaGarantias.FechaCrea;
                            vContratistaGarantias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratistaGarantias.UsuarioModifica;
                            vContratistaGarantias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratistaGarantias.FechaModifica;
                        }
                        return vContratistaGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pIDGarantia"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        public List<ContratistaGarantias> ConsultarContratistaGarantiass(int? pIDGarantia, int? pIDEntidadProvOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ContratistaGarantiass_Consultar"))
                {
                    if(pIDGarantia != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pIDGarantia);
                    if(pIDEntidadProvOferente != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDEntidadProvOferente", DbType.Int32, pIDEntidadProvOferente);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratistaGarantias> vListaContratistaGarantias = new List<ContratistaGarantias>();
                        while (vDataReaderResults.Read())
                        {
                                ContratistaGarantias vContratistaGarantias = new ContratistaGarantias();
                            vContratistaGarantias.IDContratista_Garantias = vDataReaderResults["IDContratista_Garantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContratista_Garantias"].ToString()) : vContratistaGarantias.IDContratista_Garantias;
                            vContratistaGarantias.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vContratistaGarantias.IDGarantia;
                            vContratistaGarantias.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vContratistaGarantias.IDEntidadProvOferente;

                            vContratistaGarantias.TipoPersonaNombre = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vContratistaGarantias.TipoPersonaNombre;
                            vContratistaGarantias.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vContratistaGarantias.TipoIdentificacion;
                            vContratistaGarantias.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vContratistaGarantias.NumeroIdentificacion;
                            vContratistaGarantias.Proveedor = vDataReaderResults["Razonsocila"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razonsocila"].ToString()) : vContratistaGarantias.Proveedor;
                            
                            
                            vContratistaGarantias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratistaGarantias.UsuarioCrea;
                            vContratistaGarantias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratistaGarantias.FechaCrea;
                            vContratistaGarantias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratistaGarantias.UsuarioModifica;
                            vContratistaGarantias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratistaGarantias.FechaModifica;
                                vListaContratistaGarantias.Add(vContratistaGarantias);
                        }
                        return vListaContratistaGarantias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
