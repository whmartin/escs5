﻿using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess
{
    public class ReporteContraloriaDAL : GeneralDAL
    {
        public ReporteContraloriaDAL()
        {

        }

        public List<ReporteContraloria> ObtenerInformacionBasica(DateTime fechaInicio, DateTime fechaFinal, string idRegional, int periodo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_ReporteContraloriaBasica"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@FechaDesde", DbType.DateTime, fechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaHasta", DbType.DateTime, fechaFinal);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, idRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Periodo", DbType.Int32, periodo);

                    List<ReporteContraloria> vListaContrato = new List<ReporteContraloria>();

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            var idContratox = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idContrato"].ToString()) : -1;

                            if (!vListaContrato.Any(e1 => e1.IdContrato == idContratox))
                            {
                                ReporteContraloria vContrato = new ReporteContraloria();
                                vContrato.IdContrato = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idContrato"].ToString()) : vContrato.IdContrato;
                                vContrato.CantidadVeces = vDataReaderResults["CantidadVeces"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadVeces"].ToString()) : vContrato.CantidadVeces;
                                vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? vDataReaderResults["NumeroContrato"].ToString() : vContrato.NumeroContrato;
                                vContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContrato.NombreRegional;
                                vContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vContrato.CodigoRegional;
                                vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"]) : DateTime.MinValue; ;
                                vContrato.FechaInicioContrato = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"]) : DateTime.MinValue; ;
                                vContrato.FechaFinalizacion = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"]) : DateTime.MinValue; ;
                                vContrato.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLiquidacion"]) : DateTime.MinValue;
                                vContrato.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? vDataReaderResults["ModalidadSeleccion"].ToString() : vContrato.ModalidadSeleccion;
                                vContrato.ClaseContratoOriginal = vDataReaderResults["ClaseContratoOriginal"] != DBNull.Value ? vDataReaderResults["ClaseContratoOriginal"].ToString() : vContrato.ClaseContratoOriginal;
                                vContrato.ClaseContrato = vDataReaderResults["ClaseContrato"] != DBNull.Value ? vDataReaderResults["ClaseContrato"].ToString() : vContrato.ClaseContrato;
                                vContrato.CodigoSecop = vDataReaderResults["CodigoSECOP"] != DBNull.Value ? vDataReaderResults["CodigoSECOP"].ToString() : vContrato.CodigoSecop;
                                vContrato.DiasTotales = vDataReaderResults["DiasTotales"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasTotales"].ToString()) : vContrato.DiasTotales;
                                vContrato.DiasProrrogados = vDataReaderResults["DiasProrrogados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasProrrogados"].ToString()) : vContrato.DiasProrrogados;
                                vContrato.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vContrato.ValorAdicionado;
                                vContrato.ValorInicial = vDataReaderResults["ValorInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicial"].ToString()) : vContrato.ValorInicial;
                                vContrato.ValorInicialDinero = vDataReaderResults["ValorInicialDinero"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialDinero"].ToString()) : vContrato.ValorInicialDinero;
                                vContrato.EsContrato = vDataReaderResults["EsContrato"] != DBNull.Value ? vDataReaderResults["EsContrato"].ToString() : vContrato.EsContrato;
                                vContrato.Naturaleza = vDataReaderResults["Naturaleza"] != DBNull.Value ? vDataReaderResults["Naturaleza"].ToString() : vContrato.Naturaleza;
                                vContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? vDataReaderResults["TipoIdentificacion"].ToString() : vContrato.TipoIdentificacion;
                                vContrato.NoIdentificacionContratista = vDataReaderResults["NoIdentificacionContratista"] != DBNull.Value ? vDataReaderResults["NoIdentificacionContratista"].ToString() : vContrato.NoIdentificacionContratista;
                                vContrato.NombresRazonSocialContratista = vDataReaderResults["NombresRazonSocialContratista"] != DBNull.Value ? vDataReaderResults["NombresRazonSocialContratista"].ToString() : vContrato.NombresRazonSocialContratista;
                                vContrato.FormaPago = vDataReaderResults["FormaPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FormaPago"]) : vContrato.FormaPago;
                                vContrato.PorcentajeAnticipo = vDataReaderResults["porcentajeanticipo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["porcentajeanticipo"]) : vContrato.PorcentajeAnticipo;
                                vContrato.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? vDataReaderResults["ObjetoDelContrato"].ToString() : vContrato.ObjetoContrato;
                                vContrato.IdentificacionInterventor = vDataReaderResults["IdentificacionInterventor"] != DBNull.Value ? vDataReaderResults["IdentificacionInterventor"].ToString() : vContrato.IdentificacionInterventor;
                                vContrato.NombreInterventor = vDataReaderResults["NombreInterventor"] != DBNull.Value ? vDataReaderResults["NombreInterventor"].ToString() : vContrato.NombreInterventor;
                                vContrato.TipoIdentificacionInterventor = vDataReaderResults["TipoIdentificacionInterventor"] != DBNull.Value ? vDataReaderResults["TipoIdentificacionInterventor"].ToString() : vContrato.TipoIdentificacionInterventor;
                                vContrato.TipoIdentificacionSupervisor = vDataReaderResults["TipoIdentificacionSupervisor"] != DBNull.Value ? vDataReaderResults["TipoIdentificacionSupervisor"].ToString() : vContrato.TipoIdentificacionSupervisor;
                                vContrato.IdentificacionSupervisor = vDataReaderResults["IdentificacionSupervisor"] != DBNull.Value ? vDataReaderResults["IdentificacionSupervisor"].ToString() : vContrato.IdentificacionSupervisor;
                                vContrato.NombresSupervisor = vDataReaderResults["NombresSupervisor"] != DBNull.Value ? vDataReaderResults["NombresSupervisor"].ToString() : vContrato.NombresSupervisor;
                                vContrato.FechaTerminacionAnticipada = vDataReaderResults["FechaTerminacionAnticipada"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionAnticipada"].ToString()) : vContrato.FechaTerminacionAnticipada;
                                vContrato.ValorFinal = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinal;
                                vContrato.ValorProgramado = vDataReaderResults["ValorProgramado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorProgramado"].ToString()) : vContrato.ValorProgramado;
                                vListaContrato.Add(vContrato);
                            }
                        }
                    }

                    return vListaContrato;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteContraloria> ObtenerInformacionConvenios(DateTime fechaInicio, DateTime fechaFinal, string idRegional, int periodo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_ReporteContraloriaConvenios"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@FechaDesde", DbType.DateTime, fechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaHasta", DbType.DateTime, fechaFinal);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, idRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Periodo", DbType.Int32, periodo);

                    List<ReporteContraloria> vListaContrato = new List<ReporteContraloria>();

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            var idContratox = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idContrato"].ToString()) : -1;

                            if (!vListaContrato.Any(e1 => e1.IdContrato == idContratox))
                            {
                                ReporteContraloria vContrato = new ReporteContraloria();
                                vContrato.IdContrato = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idContrato"].ToString()) : vContrato.IdContrato;
                                vContrato.CantidadVeces = vDataReaderResults["CantidadVeces"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadVeces"].ToString()) : vContrato.CantidadVeces;
                                vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? vDataReaderResults["NumeroContrato"].ToString() : vContrato.NumeroContrato;
                                vContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContrato.NombreRegional;
                                vContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vContrato.CodigoRegional;
                                vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"]) : DateTime.MinValue; ;
                                vContrato.FechaInicioContrato = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"]) : DateTime.MinValue; ;
                                vContrato.FechaFinalizacion = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"]) : DateTime.MinValue; ;
                                vContrato.FechaLiquidacion = vDataReaderResults["FechaLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLiquidacion"]) : DateTime.MinValue;
                                vContrato.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? vDataReaderResults["ModalidadSeleccion"].ToString() : vContrato.ModalidadSeleccion;
                                vContrato.ClaseContratoOriginal = vDataReaderResults["ClaseContratoOriginal"] != DBNull.Value ? vDataReaderResults["ClaseContratoOriginal"].ToString() : vContrato.ClaseContratoOriginal;
                                vContrato.ClaseContrato = vDataReaderResults["ClaseContrato"] != DBNull.Value ? vDataReaderResults["ClaseContrato"].ToString() : vContrato.ClaseContrato;
                                vContrato.CodigoSecop = vDataReaderResults["CodigoSECOP"] != DBNull.Value ? vDataReaderResults["CodigoSECOP"].ToString() : vContrato.CodigoSecop;
                                vContrato.DiasTotales = vDataReaderResults["DiasTotales"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasTotales"].ToString()) : vContrato.DiasTotales;
                                vContrato.DiasProrrogados = vDataReaderResults["DiasProrrogados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasProrrogados"].ToString()) : vContrato.DiasProrrogados;
                                vContrato.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vContrato.ValorAdicionado;
                                vContrato.ValorInicial = vDataReaderResults["ValorInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicial"].ToString()) : vContrato.ValorInicial;
                                vContrato.ValorInicialDinero = vDataReaderResults["ValorInicialDinero"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialDinero"].ToString()) : vContrato.ValorInicialDinero;
                                vContrato.EsContrato = vDataReaderResults["EsContrato"] != DBNull.Value ? vDataReaderResults["EsContrato"].ToString() : vContrato.EsContrato;
                                vContrato.Naturaleza = vDataReaderResults["Naturaleza"] != DBNull.Value ? vDataReaderResults["Naturaleza"].ToString() : vContrato.Naturaleza;
                                vContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? vDataReaderResults["TipoIdentificacion"].ToString() : vContrato.TipoIdentificacion;
                                vContrato.NoIdentificacionContratista = vDataReaderResults["NoIdentificacionContratista"] != DBNull.Value ? vDataReaderResults["NoIdentificacionContratista"].ToString() : vContrato.NoIdentificacionContratista;
                                vContrato.NombresRazonSocialContratista = vDataReaderResults["NombresRazonSocialContratista"] != DBNull.Value ? vDataReaderResults["NombresRazonSocialContratista"].ToString() : vContrato.NombresRazonSocialContratista;
                                vContrato.FormaPago = vDataReaderResults["FormaPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FormaPago"]) : vContrato.FormaPago;
                                vContrato.PorcentajeAnticipo = vDataReaderResults["porcentajeanticipo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["porcentajeanticipo"]) : vContrato.PorcentajeAnticipo;
                                vContrato.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? vDataReaderResults["ObjetoDelContrato"].ToString() : vContrato.ObjetoContrato;
                                vContrato.IdentificacionInterventor = vDataReaderResults["IdentificacionInterventor"] != DBNull.Value ? vDataReaderResults["IdentificacionInterventor"].ToString() : vContrato.IdentificacionInterventor;
                                vContrato.NombreInterventor = vDataReaderResults["NombreInterventor"] != DBNull.Value ? vDataReaderResults["NombreInterventor"].ToString() : vContrato.NombreInterventor;
                                vContrato.TipoIdentificacionInterventor = vDataReaderResults["TipoIdentificacionInterventor"] != DBNull.Value ? vDataReaderResults["TipoIdentificacionInterventor"].ToString() : vContrato.TipoIdentificacionInterventor;
                                vContrato.TipoIdentificacionSupervisor = vDataReaderResults["TipoIdentificacionSupervisor"] != DBNull.Value ? vDataReaderResults["TipoIdentificacionSupervisor"].ToString() : vContrato.TipoIdentificacionSupervisor;
                                vContrato.IdentificacionSupervisor = vDataReaderResults["IdentificacionSupervisor"] != DBNull.Value ? vDataReaderResults["IdentificacionSupervisor"].ToString() : vContrato.IdentificacionSupervisor;
                                vContrato.NombresSupervisor = vDataReaderResults["NombresSupervisor"] != DBNull.Value ? vDataReaderResults["NombresSupervisor"].ToString() : vContrato.NombresSupervisor;
                                vContrato.FechaTerminacionAnticipada = vDataReaderResults["FechaTerminacionAnticipada"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionAnticipada"].ToString()) : vContrato.FechaTerminacionAnticipada;
                                vContrato.ValorFinal = vDataReaderResults["ValorFinalContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorFinalContrato"].ToString()) : vContrato.ValorFinal;
                                vContrato.ValorProgramado = vDataReaderResults["ValorProgramado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorProgramado"].ToString()) : vContrato.ValorProgramado;
                                vListaContrato.Add(vContrato);
                            }
                        }
                    }

                    return vListaContrato;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteContraloriaUnionesConsorcios> ObtenerInformacionConsorciosUnionesTemporales(DateTime fechaInicio, DateTime fechaFinal, string idRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_ReporteContraloriaConsorciosUnionesTemporales"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@FechaDesde", DbType.DateTime, fechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaHasta", DbType.DateTime, fechaFinal);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.String, idRegional);

                    List<ReporteContraloriaUnionesConsorcios> vListaContrato = new List<ReporteContraloriaUnionesConsorcios>();

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            var idContratox = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idContrato"].ToString()) : -1;

                            ReporteContraloriaUnionesConsorcios vContrato = new ReporteContraloriaUnionesConsorcios();
                            vContrato.IdContrato = vDataReaderResults["idContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idContrato"].ToString()) : vContrato.IdContrato;
                            vContrato.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? vDataReaderResults["NumeroContrato"].ToString() : vContrato.NumeroContrato;
                            vContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContrato.NombreRegional;
                            vContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vContrato.CodigoRegional;
                            vContrato.FechaSuscripcion = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"]) : DateTime.MinValue; ;

                            vContrato.CodigoNaturaleza = vDataReaderResults["CodigoTipoPersona"] != DBNull.Value ? vDataReaderResults["CodigoTipoPersona"].ToString() : vContrato.CodigoNaturaleza;
                            vContrato.Naturaleza = vDataReaderResults["Naturaleza"] != DBNull.Value ? vDataReaderResults["Naturaleza"].ToString() : vContrato.Naturaleza;
                            vContrato.NoIdentificacionContratista = vDataReaderResults["NoIdentificacionContratista"] != DBNull.Value ? vDataReaderResults["NoIdentificacionContratista"].ToString() : vContrato.NoIdentificacionContratista;
                            vContrato.NombresRazonSocialContratista = vDataReaderResults["NombresRazonSocialContratista"] != DBNull.Value ? vDataReaderResults["NombresRazonSocialContratista"].ToString() : vContrato.NombresRazonSocialContratista;

                            vContrato.NaturalezaIntegrante = vDataReaderResults["NaturalezaIntegrante"] != DBNull.Value ? vDataReaderResults["NaturalezaIntegrante"].ToString() : vContrato.NaturalezaIntegrante;
                            vContrato.TipoIdentificacionIntegrante = vDataReaderResults["TiposDocumentoIntegrante"] != DBNull.Value ? vDataReaderResults["TiposDocumentoIntegrante"].ToString() : vContrato.TipoIdentificacionIntegrante;
                            vContrato.CodigoIdentificacionIntegrante = vDataReaderResults["CodigoDocumentoIntegrante"] != DBNull.Value ? vDataReaderResults["CodigoDocumentoIntegrante"].ToString() : vContrato.CodigoIdentificacionIntegrante;
                            vContrato.NoIdentificacionIntegrante = vDataReaderResults["NoIdentificacionContratistaIntegrante"] != DBNull.Value ? vDataReaderResults["NoIdentificacionContratistaIntegrante"].ToString() : vContrato.NoIdentificacionIntegrante;
                            vContrato.NombresRazonSocialIntegrante = vDataReaderResults["NombresRazonSocialContratistaIntegrantes"] != DBNull.Value ? vDataReaderResults["NombresRazonSocialContratistaIntegrantes"].ToString() : vContrato.NombresRazonSocialIntegrante;
                                
                            vListaContrato.Add(vContrato);
                        }
                    }

                    return vListaContrato;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ObtenerCantidadVecesReporta(ReporteContraloria itemFila)
        {
            return string.Empty;
        }

        public bool EsRUTPersonaNatural(int idContrato)
        {
            bool result = false;

            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_ReporteContraloriaVeficiacionEsRut"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand,  "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@Resultado", DbType.Boolean, 1);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    result = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@Resultado").ToString());
                    return result;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ExistenProrrogasAdiciones(int idContrato)
        {
            string  result = string.Empty;

            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_Contratos_ReporteContraloriaVeficiacionAdicionesyProrrogas"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@Resultado", DbType.String, 128);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    result = Convert.ToString(vDataBase.GetParameterValue(vDbCommand, "@Resultado").ToString());
                    return result;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
