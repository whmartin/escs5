using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Garantia
    /// </summary>
    public class GarantiaDAL : GeneralDAL
    {
        public GarantiaDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int InsertarGarantia(Garantia pGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDGarantia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoGarantia", DbType.Int32, pGarantia.IDTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pGarantia.NumeroGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAprobacionGarantia", DbType.DateTime, null);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCertificacionGarantia", DbType.DateTime, null);
                    vDataBase.AddInParameter(vDbCommand, "@FechaDevolucion", DbType.DateTime, null);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoDevolucion", DbType.String, null);
                    vDataBase.AddInParameter(vDbCommand, "@IDUsuarioAprobacion", DbType.Int32, null);
                    vDataBase.AddInParameter(vDbCommand, "@IDUsuarioDevolucion", DbType.Int32, null);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pGarantia.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@BeneficiarioICBF", DbType.Boolean, pGarantia.BeneficiarioICBF);
                    vDataBase.AddInParameter(vDbCommand, "@BeneficiarioOTROS", DbType.Boolean, pGarantia.BeneficiarioOTROS);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionBeneficiarios", DbType.String, pGarantia.DescripcionBeneficiarios);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioGarantia", DbType.DateTime, pGarantia.FechaInicioGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionGarantia", DbType.DateTime, pGarantia.FechaExpedicionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVencimientoInicialGarantia", DbType.DateTime, pGarantia.FechaVencimientoInicialGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVencimientoFinalGarantia", DbType.DateTime, pGarantia.FechaVencimientoFinalGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaReciboGarantia", DbType.DateTime, pGarantia.FechaReciboGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@ValorGarantia", DbType.String, pGarantia.ValorGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Anexos", DbType.Boolean, pGarantia.Anexos);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesAnexos", DbType.String, pGarantia.ObservacionesAnexos);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pGarantia.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pGarantia.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pGarantia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@IDSucursalAseguradoraContrato", DbType.Int32, pGarantia.IDSucursalAseguradoraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDEstadosGarantias", DbType.Int32, pGarantia.IDEstadosGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@EsGarantiaModificacion", DbType.Int32, pGarantia.EsGarantiaModificada);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, pGarantia.IdTipoModificacion);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pGarantia.IDGarantia = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDGarantia").ToString());
                    GenerarLogAuditoria(pGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int ModificarGarantia(Garantia pGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pGarantia.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoGarantia", DbType.Int32, pGarantia.IDTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pGarantia.NumeroGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAprobacionGarantia", DbType.DateTime, pGarantia.FechaAprobacionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCertificacionGarantia", DbType.DateTime, pGarantia.FechaCertificacionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaDevolucion", DbType.DateTime, pGarantia.FechaDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoDevolucion", DbType.String, pGarantia.MotivoDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@IDUsuarioAprobacion", DbType.Int32, pGarantia.IDUsuarioAprobacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDUsuarioDevolucion", DbType.Int32, pGarantia.IDUsuarioDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pGarantia.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@BeneficiarioICBF", DbType.Boolean, pGarantia.BeneficiarioICBF);
                    vDataBase.AddInParameter(vDbCommand, "@BeneficiarioOTROS", DbType.Boolean, pGarantia.BeneficiarioOTROS);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionBeneficiarios", DbType.String, pGarantia.DescripcionBeneficiarios);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioGarantia", DbType.DateTime, pGarantia.FechaInicioGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionGarantia", DbType.DateTime, pGarantia.FechaExpedicionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVencimientoInicialGarantia", DbType.DateTime, pGarantia.FechaVencimientoInicialGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVencimientoFinalGarantia", DbType.DateTime, pGarantia.FechaVencimientoFinalGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaReciboGarantia", DbType.DateTime, pGarantia.FechaReciboGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@ValorGarantia", DbType.String, pGarantia.ValorGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Anexos", DbType.Boolean, pGarantia.Anexos);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesAnexos", DbType.String, pGarantia.ObservacionesAnexos);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pGarantia.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pGarantia.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pGarantia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@IDSucursalAseguradoraContrato", DbType.Int32, pGarantia.IDSucursalAseguradoraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDEstadosGarantias", DbType.Int32, pGarantia.IDEstadosGarantias);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroModificacion", DbType.String, pGarantia.NumeroModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionModificacion", DbType.String, pGarantia.DescripcionModificacionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModificacion", DbType.DateTime, pGarantia.FechaModificacionGarantia);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int EliminarGarantia(Garantia pGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pGarantia.IDGarantia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarGarantia(Garantia pGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand,"@IdGarantia", DbType.Int32, pGarantia.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAprobacionGarantia", DbType.DateTime, pGarantia.FechaAprobacionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pGarantia.NumeroGarantia);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pGarantia.IDGarantia = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDGarantia").ToString());
                    GenerarLogAuditoria(pGarantia, vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Garantia
        /// </summary>
        /// <param name="pIDGarantia"></param>
        public Garantia ConsultarGarantia(int pIDGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pIDGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Garantia vGarantia = new Garantia();
                        while (vDataReaderResults.Read())
                        {
                            vGarantia.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vGarantia.IDGarantia;
                            vGarantia.IDTipoGarantia = vDataReaderResults["IDTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoGarantia"].ToString()) : vGarantia.IDTipoGarantia;
                            vGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vGarantia.NumeroGarantia;
                            vGarantia.FechaAprobacionGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"].ToString()) : vGarantia.FechaAprobacionGarantia;
                            vGarantia.FechaCertificacionGarantia = vDataReaderResults["FechaCertificacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacionGarantia"].ToString()) : vGarantia.FechaCertificacionGarantia;
                            vGarantia.FechaDevolucion = vDataReaderResults["FechaDevolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDevolucion"].ToString()) : vGarantia.FechaDevolucion;
                            vGarantia.MotivoDevolucion = vDataReaderResults["MotivoDevolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoDevolucion"].ToString()) : vGarantia.MotivoDevolucion;
                            vGarantia.IDUsuarioAprobacion = vDataReaderResults["IDUsuarioAprobacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUsuarioAprobacion"].ToString()) : vGarantia.IDUsuarioAprobacion;
                            vGarantia.IDUsuarioDevolucion = vDataReaderResults["IDUsuarioDevolucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUsuarioDevolucion"].ToString()) : vGarantia.IDUsuarioDevolucion;
                            vGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vGarantia.IdContrato;
                            vGarantia.BeneficiarioICBF = vDataReaderResults["BeneficiarioICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["BeneficiarioICBF"].ToString()) : vGarantia.BeneficiarioICBF;
                            vGarantia.BeneficiarioOTROS = vDataReaderResults["BeneficiarioOTROS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["BeneficiarioOTROS"].ToString()) : vGarantia.BeneficiarioOTROS;
                            vGarantia.DescripcionBeneficiarios = vDataReaderResults["DescripcionBeneficiarios"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBeneficiarios"].ToString()) : vGarantia.DescripcionBeneficiarios;
                            vGarantia.FechaInicioGarantia = vDataReaderResults["FechaInicioGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioGarantia"].ToString()) : vGarantia.FechaInicioGarantia;
                            vGarantia.FechaExpedicionGarantia = vDataReaderResults["FechaExpedicionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionGarantia"].ToString()) : vGarantia.FechaExpedicionGarantia;
                            vGarantia.FechaVencimientoInicialGarantia = vDataReaderResults["FechaVencimientoInicialGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoInicialGarantia"].ToString()) : vGarantia.FechaVencimientoInicialGarantia;
                            vGarantia.FechaVencimientoFinalGarantia = vDataReaderResults["FechaVencimientoFinalGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoFinalGarantia"].ToString()) : vGarantia.FechaVencimientoFinalGarantia;
                            vGarantia.FechaReciboGarantia = vDataReaderResults["FechaReciboGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReciboGarantia"].ToString()) : vGarantia.FechaReciboGarantia;
                            vGarantia.ValorGarantia = vDataReaderResults["ValorGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorGarantia"].ToString()) : vGarantia.ValorGarantia;
                            vGarantia.Anexos = vDataReaderResults["Anexos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Anexos"].ToString()) : vGarantia.Anexos;
                            vGarantia.ObservacionesAnexos = vDataReaderResults["ObservacionesAnexos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesAnexos"].ToString()) : vGarantia.ObservacionesAnexos;
                            vGarantia.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vGarantia.IdTercero;
                            vGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGarantia.UsuarioCrea;
                            vGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGarantia.FechaCrea;
                            vGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGarantia.UsuarioModifica;
                            vGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGarantia.FechaModifica;
                            vGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vGarantia.Estado;
                            vGarantia.IDSucursalAseguradoraContrato = vDataReaderResults["IDSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSucursalAseguradoraContrato"].ToString()) : vGarantia.IDSucursalAseguradoraContrato;
                            vGarantia.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vGarantia.IDEstadosGarantias;
                            vGarantia.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstadoGarantia"].ToString()) : vGarantia.DescripcionEstadoGarantia;
                            vGarantia.CodigoDescripcionEstadoGarantia = vDataReaderResults["CodDescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDescripcionEstadoGarantia"].ToString()) : vGarantia.CodigoDescripcionEstadoGarantia;
                            vGarantia.Zona = vDataReaderResults["Zona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Zona"].ToString()) : vGarantia.Zona;
                            vGarantia.NumeroModificacion = vDataReaderResults["NumeroModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroModificacion"]) : vGarantia.NumeroModificacion;
                            vGarantia.DescripcionModificacionGarantia = vDataReaderResults["DescripcionModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionModificacion"].ToString()) : vGarantia.DescripcionModificacionGarantia;
                            vGarantia.FechaModificacionGarantia = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vGarantia.FechaModificacionGarantia;
                            vGarantia.CodigoEstadoGarantia = vDataReaderResults["CodigoEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoGarantia"].ToString()) : vGarantia.CodigoEstadoGarantia;
                            vGarantia.IdTipoModificacion = vDataReaderResults["IdTipoModificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModificacion"].ToString()) : vGarantia.IdTipoModificacion;
                        }
                        return vGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad Garantia
        /// </summary>
        /// <param name="pIDGarantia"></param>
        public Garantia ConsultarMaximaGarantia(int pIDGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pIDGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Garantia vGarantia = null;
                        while (vDataReaderResults.Read())
                        {
                            vGarantia = new Garantia();
                            vGarantia.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vGarantia.IDGarantia;
                            vGarantia.IDTipoGarantia = vDataReaderResults["IDTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoGarantia"].ToString()) : vGarantia.IDTipoGarantia;
                            vGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vGarantia.NumeroGarantia;
                            vGarantia.FechaAprobacionGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"].ToString()) : vGarantia.FechaAprobacionGarantia;
                            vGarantia.FechaCertificacionGarantia = vDataReaderResults["FechaCertificacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacionGarantia"].ToString()) : vGarantia.FechaCertificacionGarantia;
                            vGarantia.FechaDevolucion = vDataReaderResults["FechaDevolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDevolucion"].ToString()) : vGarantia.FechaDevolucion;
                            vGarantia.MotivoDevolucion = vDataReaderResults["MotivoDevolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoDevolucion"].ToString()) : vGarantia.MotivoDevolucion;
                            vGarantia.IDUsuarioAprobacion = vDataReaderResults["IDUsuarioAprobacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUsuarioAprobacion"].ToString()) : vGarantia.IDUsuarioAprobacion;
                            vGarantia.IDUsuarioDevolucion = vDataReaderResults["IDUsuarioDevolucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUsuarioDevolucion"].ToString()) : vGarantia.IDUsuarioDevolucion;
                            vGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vGarantia.IdContrato;
                            vGarantia.BeneficiarioICBF = vDataReaderResults["BeneficiarioICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["BeneficiarioICBF"].ToString()) : vGarantia.BeneficiarioICBF;
                            vGarantia.BeneficiarioOTROS = vDataReaderResults["BeneficiarioOTROS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["BeneficiarioOTROS"].ToString()) : vGarantia.BeneficiarioOTROS;
                            vGarantia.DescripcionBeneficiarios = vDataReaderResults["DescripcionBeneficiarios"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBeneficiarios"].ToString()) : vGarantia.DescripcionBeneficiarios;
                            vGarantia.FechaInicioGarantia = vDataReaderResults["FechaInicioGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioGarantia"].ToString()) : vGarantia.FechaInicioGarantia;
                            vGarantia.FechaExpedicionGarantia = vDataReaderResults["FechaExpedicionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionGarantia"].ToString()) : vGarantia.FechaExpedicionGarantia;
                            vGarantia.FechaVencimientoInicialGarantia = vDataReaderResults["FechaVencimientoInicialGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoInicialGarantia"].ToString()) : vGarantia.FechaVencimientoInicialGarantia;
                            vGarantia.FechaVencimientoFinalGarantia = vDataReaderResults["FechaVencimientoFinalGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoFinalGarantia"].ToString()) : vGarantia.FechaVencimientoFinalGarantia;
                            vGarantia.FechaReciboGarantia = vDataReaderResults["FechaReciboGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReciboGarantia"].ToString()) : vGarantia.FechaReciboGarantia;
                            vGarantia.ValorGarantia = vDataReaderResults["ValorGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorGarantia"].ToString()) : vGarantia.ValorGarantia;
                            vGarantia.Anexos = vDataReaderResults["Anexos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Anexos"].ToString()) : vGarantia.Anexos;
                            vGarantia.ObservacionesAnexos = vDataReaderResults["ObservacionesAnexos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesAnexos"].ToString()) : vGarantia.ObservacionesAnexos;
                            vGarantia.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vGarantia.IdTercero;
                            vGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGarantia.UsuarioCrea;
                            vGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGarantia.FechaCrea;
                            vGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGarantia.UsuarioModifica;
                            vGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGarantia.FechaModifica;
                            vGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vGarantia.Estado;
                            vGarantia.IDSucursalAseguradoraContrato = vDataReaderResults["IDSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSucursalAseguradoraContrato"].ToString()) : vGarantia.IDSucursalAseguradoraContrato;
                            vGarantia.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vGarantia.IDEstadosGarantias;
                            vGarantia.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstadoGarantia"].ToString()) : vGarantia.DescripcionEstadoGarantia;
                            vGarantia.CodigoDescripcionEstadoGarantia = vDataReaderResults["CodDescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDescripcionEstadoGarantia"].ToString()) : vGarantia.CodigoDescripcionEstadoGarantia;
                            vGarantia.Zona = vDataReaderResults["Zona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Zona"].ToString()) : vGarantia.Zona;
                            vGarantia.NumeroModificacion = vDataReaderResults["NumeroModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroModificacion"]) : vGarantia.NumeroModificacion;
                            vGarantia.DescripcionModificacionGarantia = vDataReaderResults["DescripcionModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionModificacion"].ToString()) : vGarantia.DescripcionModificacionGarantia;
                            vGarantia.FechaModificacionGarantia = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vGarantia.FechaModificacionGarantia;
                            vGarantia.CodigoEstadoGarantia = vDataReaderResults["CodigoEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoGarantia"].ToString()) : vGarantia.CodigoEstadoGarantia;
                            vGarantia.IdTipoModificacion = vDataReaderResults["IdTipoModificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModificacion"].ToString()) : vGarantia.IdTipoModificacion;
                        }
                        return vGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad Garantia
        /// </summary>
        /// <param name="pIDTipoGarantia"></param>
        /// <param name="pNumeroGarantia"></param>
        /// <param name="pAprobada"></param>
        /// <param name="pDevuelta"></param>
        /// <param name="pFechaAprobacionGarantia"></param>
        /// <param name="pFechaCertificacionGarantia"></param>
        /// <param name="pFechaDevolucion"></param>
        /// <param name="pMotivoDevolucion"></param>
        /// <param name="pIDUsuarioAprobacion"></param>
        /// <param name="pIDUsuarioDevolucion"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pBeneficiarioICBF"></param>
        /// /// <param name="pBeneficiarioOTROS"></param>
        /// <param name="pDescripcionBeneficiarios"></param>
        /// <param name="pFechaInicioGarantia"></param>
        /// <param name="pFechaExpedicionGarantia"></param>
        /// <param name="pFechaVencimientoInicialGarantia"></param>
        /// <param name="pFechaVencimientoFinalGarantia"></param>
        /// <param name="pFechaReciboGarantia"></param>
        /// <param name="pValorGarantia"></param>
        /// <param name="pAnexos"></param>
        /// <param name="pObservacionesAnexos"></param>
        /// <param name="pEntidadProvOferenteAseguradora"></param>
        /// <param name="pEstado"></param>
        /// <param name="pIDSucursalAseguradoraContrato"></param>
        /// <param name="pIDEstadosGarantias"></param>
        public List<Garantia> ConsultarGarantias(int? pIDTipoGarantia, String pNumeroGarantia, DateTime? pFechaAprobacionGarantia, DateTime? pFechaCertificacionGarantia, DateTime? pFechaDevolucion,
            String pMotivoDevolucion, int? pIDUsuarioAprobacion, int? pIDUsuarioDevolucion, int? pIdContrato, Boolean? pBeneficiarioICBF, Boolean? pBeneficiarioOTROS, String pDescripcionBeneficiarios, DateTime? pFechaInicioGarantia,
            DateTime? pFechaExpedicionGarantia, DateTime? pFechaVencimientoInicialGarantia, DateTime? pFechaVencimientoFinalGarantia, DateTime? pFechaReciboGarantia, String pValorGarantia,
            Boolean? pAnexos, String pObservacionesAnexos, int? pEntidadProvOferenteAseguradora, Boolean? pEstado, int? pIDSucursalAseguradoraContrato, int? pIDEstadosGarantias, String pCodEstadoContrato, String pCodEstadoGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantias_Consultar"))
                {
                    if (pIDTipoGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoGarantia", DbType.Int32, pIDTipoGarantia);
                    if (pNumeroGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pNumeroGarantia);
                    if (pFechaAprobacionGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaAprobacionGarantia", DbType.DateTime, pFechaAprobacionGarantia);
                    if (pFechaCertificacionGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaCertificacionGarantia", DbType.DateTime, pFechaCertificacionGarantia);
                    if (pFechaDevolucion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaDevolucion", DbType.DateTime, pFechaDevolucion);
                    if (pMotivoDevolucion != null)
                        vDataBase.AddInParameter(vDbCommand, "@MotivoDevolucion", DbType.String, pMotivoDevolucion);
                    if (pIDUsuarioAprobacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDUsuarioAprobacion", DbType.Int32, pIDUsuarioAprobacion);
                    if (pIDUsuarioDevolucion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDUsuarioDevolucion", DbType.Int32, pIDUsuarioDevolucion);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pBeneficiarioICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@BeneficiarioICBF", DbType.Boolean, pBeneficiarioICBF);
                    if (pBeneficiarioOTROS != null)
                        vDataBase.AddInParameter(vDbCommand, "@BeneficiarioOTROS", DbType.Boolean, pBeneficiarioOTROS);
                    if (pDescripcionBeneficiarios != null)
                        vDataBase.AddInParameter(vDbCommand, "@DescripcionBeneficiarios", DbType.String, pDescripcionBeneficiarios);
                    if (pFechaInicioGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaInicioGarantia", DbType.DateTime, pFechaInicioGarantia);
                    if (pFechaExpedicionGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionGarantia", DbType.DateTime, pFechaExpedicionGarantia);
                    if (pFechaVencimientoInicialGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaVencimientoInicialGarantia", DbType.DateTime, pFechaVencimientoInicialGarantia);
                    if (pFechaVencimientoFinalGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaVencimientoFinalGarantia", DbType.DateTime, pFechaVencimientoFinalGarantia);
                    if (pFechaReciboGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaReciboGarantia", DbType.DateTime, pFechaReciboGarantia);
                    if (pValorGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorGarantia", DbType.String, pValorGarantia);
                    if (pAnexos != null)
                        vDataBase.AddInParameter(vDbCommand, "@Anexos", DbType.Boolean, pAnexos);
                    if (pObservacionesAnexos != null)
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesAnexos", DbType.String, pObservacionesAnexos);
                    if (pEntidadProvOferenteAseguradora != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pEntidadProvOferenteAseguradora);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (pIDSucursalAseguradoraContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDSucursalAseguradoraContrato", DbType.Int32, pIDSucursalAseguradoraContrato);
                    if (pIDEstadosGarantias != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEstadosGarantias", DbType.Int32, pIDEstadosGarantias);
                    if (pCodEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstadoContrato", DbType.String, pCodEstadoContrato);
                    if (pCodEstadoGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoGarantia", DbType.String, pCodEstadoGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Garantia> vListaGarantia = new List<Garantia>();
                        while (vDataReaderResults.Read())
                        {
                            Garantia vGarantia = new Garantia();
                            vGarantia.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vGarantia.IDGarantia;
                            vGarantia.IDTipoGarantia = vDataReaderResults["IDTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoGarantia"].ToString()) : vGarantia.IDTipoGarantia;
                            vGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vGarantia.NumeroGarantia;
                            vGarantia.FechaAprobacionGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"].ToString()) : vGarantia.FechaAprobacionGarantia;
                            vGarantia.FechaCertificacionGarantia = vDataReaderResults["FechaCertificacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacionGarantia"].ToString()) : vGarantia.FechaCertificacionGarantia;
                            vGarantia.FechaDevolucion = vDataReaderResults["FechaDevolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDevolucion"].ToString()) : vGarantia.FechaDevolucion;
                            vGarantia.MotivoDevolucion = vDataReaderResults["MotivoDevolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoDevolucion"].ToString()) : vGarantia.MotivoDevolucion;
                            vGarantia.IDUsuarioAprobacion = vDataReaderResults["IDUsuarioAprobacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUsuarioAprobacion"].ToString()) : vGarantia.IDUsuarioAprobacion;
                            vGarantia.IDUsuarioDevolucion = vDataReaderResults["IDUsuarioDevolucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUsuarioDevolucion"].ToString()) : vGarantia.IDUsuarioDevolucion;
                            vGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vGarantia.IdContrato;
                            vGarantia.BeneficiarioICBF = vDataReaderResults["BeneficiarioICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["BeneficiarioICBF"].ToString()) : vGarantia.BeneficiarioICBF;
                            vGarantia.BeneficiarioOTROS = vDataReaderResults["BeneficiarioOTROS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["BeneficiarioOTROS"].ToString()) : vGarantia.BeneficiarioOTROS;
                            vGarantia.DescripcionBeneficiarios = vDataReaderResults["DescripcionBeneficiarios"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBeneficiarios"].ToString()) : vGarantia.DescripcionBeneficiarios;
                            vGarantia.FechaInicioGarantia = vDataReaderResults["FechaInicioGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioGarantia"].ToString()) : vGarantia.FechaInicioGarantia;
                            vGarantia.FechaExpedicionGarantia = vDataReaderResults["FechaExpedicionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionGarantia"].ToString()) : vGarantia.FechaExpedicionGarantia;
                            vGarantia.FechaVencimientoInicialGarantia = vDataReaderResults["FechaVencimientoInicialGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoInicialGarantia"].ToString()) : vGarantia.FechaVencimientoInicialGarantia;
                            vGarantia.FechaVencimientoFinalGarantia = vDataReaderResults["FechaVencimientoFinalGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoFinalGarantia"].ToString()) : vGarantia.FechaVencimientoFinalGarantia;
                            vGarantia.FechaReciboGarantia = vDataReaderResults["FechaReciboGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReciboGarantia"].ToString()) : vGarantia.FechaReciboGarantia;
                            vGarantia.ValorGarantia = vDataReaderResults["ValorGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorGarantia"].ToString()) : vGarantia.ValorGarantia;
                            if (vGarantia.ValorGarantia.Contains("$") || vGarantia.ValorGarantia.Contains("."))
                                vGarantia.ValorGarantia = vGarantia.ValorGarantia.Replace("$", "").Replace(".", "");
                            vGarantia.Anexos = vDataReaderResults["Anexos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Anexos"].ToString()) : vGarantia.Anexos;
                            vGarantia.ObservacionesAnexos = vDataReaderResults["ObservacionesAnexos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesAnexos"].ToString()) : vGarantia.ObservacionesAnexos;
                            vGarantia.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vGarantia.IdTercero;
                            vGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGarantia.UsuarioCrea;
                            vGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGarantia.FechaCrea;
                            vGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGarantia.UsuarioModifica;
                            vGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGarantia.FechaModifica;
                            vGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vGarantia.Estado;
                            vGarantia.IDSucursalAseguradoraContrato = vDataReaderResults["IDSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSucursalAseguradoraContrato"].ToString()) : vGarantia.IDSucursalAseguradoraContrato;
                            vGarantia.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vGarantia.IDEstadosGarantias;
                            vGarantia.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstadoGarantia"].ToString()) : vGarantia.DescripcionEstadoGarantia;
                            vGarantia.CodigoDescripcionEstadoGarantia = vDataReaderResults["CodDescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDescripcionEstadoGarantia"].ToString()) : vGarantia.CodigoDescripcionEstadoGarantia;
                            vGarantia.CodigoEstadoGarantia = vDataReaderResults["CodigoEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoGarantia"].ToString()) : vGarantia.CodigoEstadoGarantia;
                            vGarantia.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vGarantia.NumeroContrato;
                            vGarantia.DescEstadoContrato = vDataReaderResults["DescEstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescEstadoContrato"].ToString()) : vGarantia.DescEstadoContrato;
                            vGarantia.FechaSuscripcionContrato = vDataReaderResults["FechaSuscripcionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcionContrato"].ToString()) : vGarantia.FechaSuscripcionContrato;
                            vGarantia.NombreSupervisor = vDataReaderResults["NombreSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSupervisor"].ToString()) : vGarantia.NombreSupervisor;
                            vGarantia.NombreTipoGarantia = vDataReaderResults["NombreTipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoGarantia"].ToString()) : vGarantia.NombreTipoGarantia;
                            vListaGarantia.Add(vGarantia);
                        }
                        return vListaGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de un numero de garantia existente
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public bool ExisteNumeroGarantia(String pNumeroGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Garantia_ExisteNumeroGarantia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pNumeroGarantia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int existeGarantiaContrato(String pNumeroGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int idcontrato = 0;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Garantia_ExisteNumeroGarantiaContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pNumeroGarantia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            idcontrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : idcontrato;
                        }
                   }
                    return idcontrato;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de un garantia
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public bool ExisteGarantia(String pNumGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Garantia_ExisteGarantia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumGarantia", DbType.String, pNumGarantia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por idcontrato para la entidad Garantia
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el Id del contrato</param>
        public List<Garantia> ConsultarInfoGarantias(int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_InfoGarantia_Consultar"))
                {

                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Garantia> vListaGarantia = new List<Garantia>();
                        while (vDataReaderResults.Read())
                        {
                            Garantia vGarantia = new Garantia();
                            vGarantia.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vGarantia.IDGarantia;
                            vGarantia.IDTipoGarantia = vDataReaderResults["IDTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoGarantia"].ToString()) : vGarantia.IDTipoGarantia;
                            vGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vGarantia.NumeroGarantia;
                            vGarantia.FechaExpedicionGarantia = vDataReaderResults["FechaExpedicionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionGarantia"].ToString()) : vGarantia.FechaExpedicionGarantia;
                            vGarantia.FechaAprobacionGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"].ToString()) : vGarantia.FechaAprobacionGarantia;
                            vGarantia.FechaCertificacionGarantia = vDataReaderResults["FechaCertificacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacionGarantia"].ToString()) : vGarantia.FechaCertificacionGarantia;
                            vGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vGarantia.IdContrato;
                            vGarantia.FechaInicioGarantia = vDataReaderResults["FechaInicioGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioGarantia"].ToString()) : vGarantia.FechaInicioGarantia;
                            vGarantia.ValorGarantia = vDataReaderResults["ValorGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorGarantia"].ToString()) : vGarantia.ValorGarantia;
                            vGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGarantia.UsuarioCrea;
                            vGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGarantia.FechaCrea;
                            vGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGarantia.UsuarioModifica;
                            vGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGarantia.FechaModifica;
                            vGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vGarantia.Estado;
                            vGarantia.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vGarantia.IDEstadosGarantias;
                            vGarantia.NombreTipoGarantia = vDataReaderResults["NombreTipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoGarantia"].ToString()) : vGarantia.NombreTipoGarantia;
                            vGarantia.CodigoEstadoGarantia = vDataReaderResults["CodigoEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoGarantia"].ToString()) : vGarantia.CodigoEstadoGarantia;
                            vGarantia.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstadoGarantia"].ToString()) : vGarantia.DescripcionEstadoGarantia;
                            vGarantia.IDSucursalAseguradoraContrato = vDataReaderResults["IdSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSucursalAseguradoraContrato"].ToString()) : vGarantia.IDSucursalAseguradoraContrato;
                            vGarantia.EsGarantiaModificada = vDataReaderResults["EsGarantiaModificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsGarantiaModificacion"].ToString()) : vGarantia.EsGarantiaModificada;
                            vGarantia.FechaVencimientoFinalGarantia = vDataReaderResults["FechaVencimientoFinalGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoFinalGarantia"].ToString()) : vGarantia.FechaVencimientoFinalGarantia;
                            vGarantia.FechaVencimientoInicialGarantia = vDataReaderResults["FechaVencimientoInicialGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoInicialGarantia"].ToString()) : vGarantia.FechaVencimientoInicialGarantia;
                            vGarantia.FechaVencimientoInicialGarantia = vDataReaderResults["FechaVencimientoInicialGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoInicialGarantia"].ToString()) : vGarantia.FechaVencimientoInicialGarantia;
                            vGarantia.EntidadAseguradora = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vGarantia.EntidadAseguradora;
                            vGarantia.ValorSIRECI = vDataReaderResults["ValorSIRECI"] != DBNull.Value ? vDataReaderResults["ValorSIRECI"].ToString() : vGarantia.ValorSIRECI;

                            vListaGarantia.Add(vGarantia);
                        }
                        return vListaGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoMigrado> ConsultarInfoGarantiasMigracion(int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_InfoGarantiaMigracion_Consultar"))
                {

                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratoMigrado> vListaGarantia = new List<ContratoMigrado>();
                        while (vDataReaderResults.Read())
                        {
                            ContratoMigrado vGarantia = new ContratoMigrado();
                            vGarantia.IdContratoEnContratos = vDataReaderResults["IdContratoEnContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoEnContratos"].ToString()) : vGarantia.IdContratoEnContratos;
                            vGarantia.TipoGarantia = vDataReaderResults["TipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoGarantia"].ToString()) : vGarantia.TipoGarantia;
                            vGarantia.FechaAprobacionGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"].ToString()) : vGarantia.FechaAprobacionGarantia;
                            vGarantia.ValorTotalAsegurado = vDataReaderResults["ValorTotalAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotalAsegurado"].ToString()) : vGarantia.ValorTotalAsegurado;
                            vGarantia.PorcentajeAsegurado = vDataReaderResults["PorcentajeAsegurado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeAsegurado"].ToString()) : vGarantia.PorcentajeAsegurado;
                            vGarantia.NombreEntidadAseguradora = vDataReaderResults["NombreEntidadAseguradora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadAseguradora"].ToString()) : vGarantia.NombreEntidadAseguradora;
                            vGarantia.NitEntidadAseguradora = vDataReaderResults["NitEntidadAseguradora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NitEntidadAseguradora"].ToString()) : vGarantia.NitEntidadAseguradora;

                            vListaGarantia.Add(vGarantia);
                        }
                        return vListaGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGarantia"></param>
        /// <param name="aprobada"></param>
        /// <returns></returns>
        public int CambiarEstadoGarantia(int idGarantia, bool aprobada, bool esregistro, DateTime Fecha, string observaciones, DateTime? fechaFinalizacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_CambiarEstado"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, idGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Aprobada", DbType.Boolean, aprobada);
                    vDataBase.AddInParameter(vDbCommand, "@EsRegistro", DbType.Boolean, esregistro);
                    vDataBase.AddInParameter(vDbCommand, "@Fecha", DbType.DateTime, Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, observaciones);
                    if (fechaFinalizacion.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@FechaFinalizacion", DbType.DateTime, fechaFinalizacion.Value);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="idTipoModificacionContractual"></param>
        /// <returns></returns>
        public ConsModContractual ConsultarModificacionContractualGarantia(int idContrato, int idGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_CONTRATO_GarantiaModificacion_ConsultarModificacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdGarantia", DbType.Int32, idGarantia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ConsModContractual vConsModContractual = new ConsModContractual();
                        while (vDataReaderResults.Read())
                        {
                            vConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vConsModContractual.IDCosModContractual;
                            vConsModContractual.NumeroDoc = vDataReaderResults["NumeroDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDoc"].ToString()) : vConsModContractual.NumeroDoc;
                            vConsModContractual.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vConsModContractual.Justificacion;
                            vConsModContractual.IDContrato = vDataReaderResults["IDContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDContrato"].ToString()) : vConsModContractual.IDContrato;
                            vConsModContractual.ConsecutivoSuscrito = vDataReaderResults["ConsecutivoSuscrito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoSuscrito"].ToString()) : vConsModContractual.ConsecutivoSuscrito;
                            vConsModContractual.IdConsModContractualesEstado = vDataReaderResults["IdConsModContractualesEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsModContractualesEstado"].ToString()) : vConsModContractual.IdConsModContractualesEstado;
                            vConsModContractual.Suscrito = vDataReaderResults["Suscrito"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Suscrito"].ToString()) : vConsModContractual.Suscrito;
                            vConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConsModContractual.UsuarioCrea;
                            vConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConsModContractual.FechaCrea;
                            vConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConsModContractual.UsuarioModifica;
                            vConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConsModContractual.FechaModifica;
                            vConsModContractual.IDTipoModificacionContractual = vDataReaderResults["IDTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoModificacionContractual"].ToString()) : vConsModContractual.IDTipoModificacionContractual;
                            vConsModContractual.FechaSuscripcion = vDataReaderResults["FechaSubscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSubscripcion"].ToString()) : vConsModContractual.FechaSuscripcion;
                        }
                        return vConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
