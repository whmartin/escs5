using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class DetalleConsModContractualDAL : GeneralDAL
    {
        public DetalleConsModContractualDAL()
        {
        }
        public int InsertarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_DetalleConsModContractual_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoModificacionContractual", DbType.Int32, pDetalleConsModContractual.IDTipoModificacionContractual);
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, pDetalleConsModContractual.IDCosModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDetalleConsModContractual.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDetalleConsModContractual.IDDetalleConsModContractual = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDDetalleConsModContractual").ToString());
                    GenerarLogAuditoria(pDetalleConsModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_DetalleConsModContractual_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pDetalleConsModContractual.IDDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoModificacionContractual", DbType.Int32, pDetalleConsModContractual.IDTipoModificacionContractual);
                    vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, pDetalleConsModContractual.IDCosModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDetalleConsModContractual.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDetalleConsModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_DetalleConsModContractual_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pDetalleConsModContractual.IDDetalleConsModContractual);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDetalleConsModContractual, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public DetalleConsModContractual ConsultarDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_DetalleConsModContractual_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDDetalleConsModContractual", DbType.Int32, pIDDetalleConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DetalleConsModContractual vDetalleConsModContractual = new DetalleConsModContractual();
                        while (vDataReaderResults.Read())
                        {
                            vDetalleConsModContractual.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vDetalleConsModContractual.IDDetalleConsModContractual;
                            vDetalleConsModContractual.IDTipoModificacionContractual = vDataReaderResults["IDTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoModificacionContractual"].ToString()) : vDetalleConsModContractual.IDTipoModificacionContractual;
                            vDetalleConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vDetalleConsModContractual.IDCosModContractual;
                            vDetalleConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDetalleConsModContractual.UsuarioCrea;
                            vDetalleConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDetalleConsModContractual.FechaCrea;
                            vDetalleConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDetalleConsModContractual.UsuarioModifica;
                            vDetalleConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDetalleConsModContractual.FechaModifica;
                        }
                        return vDetalleConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DetalleConsModContractual> ConsultarDetalleConsModContractuals(int? pIDTipoModificacionContractual, int? pIDCosModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_DetalleConsModContractuals_Consultar"))
                {
                    if(pIDTipoModificacionContractual != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDTipoModificacionContractual", DbType.Int32, pIDTipoModificacionContractual);
                    if(pIDCosModContractual != null)
                         vDataBase.AddInParameter(vDbCommand, "@IDCosModContractual", DbType.Int32, pIDCosModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DetalleConsModContractual> vListaDetalleConsModContractual = new List<DetalleConsModContractual>();
                        while (vDataReaderResults.Read())
                        {
                                DetalleConsModContractual vDetalleConsModContractual = new DetalleConsModContractual();
                            vDetalleConsModContractual.IDDetalleConsModContractual = vDataReaderResults["IDDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDetalleConsModContractual"].ToString()) : vDetalleConsModContractual.IDDetalleConsModContractual;
                            vDetalleConsModContractual.IDTipoModificacionContractual = vDataReaderResults["IDTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoModificacionContractual"].ToString()) : vDetalleConsModContractual.IDTipoModificacionContractual;
                            vDetalleConsModContractual.IDCosModContractual = vDataReaderResults["IDCosModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCosModContractual"].ToString()) : vDetalleConsModContractual.IDCosModContractual;
                            vDetalleConsModContractual.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDetalleConsModContractual.UsuarioCrea;
                            vDetalleConsModContractual.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDetalleConsModContractual.FechaCrea;
                            vDetalleConsModContractual.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDetalleConsModContractual.UsuarioModifica;
                            vDetalleConsModContractual.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDetalleConsModContractual.FechaModifica;
                                vListaDetalleConsModContractual.Add(vDetalleConsModContractual);
                        }
                        return vListaDetalleConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
