﻿using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.DataAccess
{
    public class GarantiaModificacionDAL : GeneralDAL
    {
        public GarantiaModificacionDAL() { }

        public List<GarantiaModificacionContrato> ConsultarContratos(string numeroContrato,
                                                          int? vigenciaFiscal,
                                                          int? regional,
                                                          int? idCategoriaContrato,
                                                          int? idTipoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantias_ConsultarContratosModificacionGarantia"))
                {
                    if (numeroContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, numeroContrato);
                    if (vigenciaFiscal != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaFiscal", DbType.Int32, vigenciaFiscal);
                    if (regional != null)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int32, regional);
                    if (idCategoriaContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaContrato", DbType.Int32, idCategoriaContrato);
                    if (idTipoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, idTipoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GarantiaModificacionContrato> vListaConsModContractual = new List<GarantiaModificacionContrato>();

                        while (vDataReaderResults.Read())
                        {
                            GarantiaModificacionContrato vConsModContractual = new GarantiaModificacionContrato();
                            vConsModContractual.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vConsModContractual.IdContrato;
                            vConsModContractual.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vConsModContractual.NumeroContrato;
                            vConsModContractual.VigenciaFiscalInicial = vDataReaderResults["VigenciaFiscalInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalInicial"].ToString()) : vConsModContractual.VigenciaFiscalInicial;
                            vConsModContractual.VigenciaFiscalFinal = vDataReaderResults["VigenciaFiscalFinal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VigenciaFiscalFinal"].ToString()) : vConsModContractual.VigenciaFiscalFinal;
                            vConsModContractual.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vConsModContractual.NombreRegional;
                            vListaConsModContractual.Add(vConsModContractual);
                        }
                        return vListaConsModContractual;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public  void GenerarModificacionGarantia(int idGarantia, string usuariocreacion, DateTime fechaAprobacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_Modificacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuariocreacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, idGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAprobacion", DbType.DateTime, fechaAprobacion);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool PuedeCrearGarantiasNuevasModificacion(int idTipoModificacion)
        {
            bool result = false;

            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_PuedeCrearNuevaGarantia"))
                {
                    vDataBase.AddInParameter(vDbCommand,  "@IdTipoModificacion", DbType.Int32, idTipoModificacion);
                    vDataBase.AddOutParameter(vDbCommand, "@EsValido", DbType.Boolean, 18);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    result =  Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@EsValido").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public  bool EsGarantiaEditada(int idTipoModificacion, int idContrato)
        {
            bool result = false;

            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_Garantia_EsGarantiaEditada"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, idTipoModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vDataBase.AddOutParameter(vDbCommand, "@EsValido", DbType.Boolean, 18);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    result = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@EsValido").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public  List<TipoModificacion> ObtenerTiposModificacionModGarantia(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoModificaciones_ConsultarModificanGarantiaPorContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoModificacion> vListaTipoModificacionBasica = new List<TipoModificacion>();
                        while (vDataReaderResults.Read())
                        {
                            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
                            vTipoModificacionBasica.IdTipoModificacionBasica = vDataReaderResults["IdTipoModificacionContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModificacionContractual"].ToString()) : vTipoModificacionBasica.IdTipoModificacionBasica;
                            vTipoModificacionBasica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoModificacionBasica.Descripcion;
                            vTipoModificacionBasica.RequiereModificacion = vDataReaderResults["RequiereModificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereModificacion"].ToString()) : vTipoModificacionBasica.RequiereModificacion;
                            vTipoModificacionBasica.EsPorDefecto = vDataReaderResults["EsPorDefecto"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsPorDefecto"].ToString()) : vTipoModificacionBasica.EsPorDefecto;
                            vTipoModificacionBasica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoModificacionBasica.Estado;
                            vTipoModificacionBasica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoModificacionBasica.UsuarioCrea;
                            vTipoModificacionBasica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoModificacionBasica.FechaCrea;
                            vTipoModificacionBasica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoModificacionBasica.UsuarioModifica;
                            vTipoModificacionBasica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoModificacionBasica.FechaModifica;
                            vListaTipoModificacionBasica.Add(vTipoModificacionBasica);
                        }
                        return vListaTipoModificacionBasica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Garantias Historicos

        public List<GarantiaHistorico> ConsultarInfoGarantiasHistorico(int? pIdGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_InfoGarantiaHistorico_Consultar"))
                {

                    if (pIdGarantia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdGarantia", DbType.Int32, pIdGarantia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GarantiaHistorico> vListaGarantia = new List<GarantiaHistorico>();
                        while (vDataReaderResults.Read())
                        {
                            GarantiaHistorico vGarantia = new GarantiaHistorico();
                            vGarantia.IDGarantiaHistorico = vDataReaderResults["IDGarantiaHistorico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantiaHistorico"].ToString()) : vGarantia.IDGarantiaHistorico;
                            vGarantia.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vGarantia.IDGarantia;
                            vGarantia.IDTipoGarantia = vDataReaderResults["IDTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoGarantia"].ToString()) : vGarantia.IDTipoGarantia;
                            vGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vGarantia.NumeroGarantia;
                            vGarantia.FechaAprobacionGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"].ToString()) : vGarantia.FechaAprobacionGarantia;
                            vGarantia.FechaCertificacionGarantia = vDataReaderResults["FechaCertificacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacionGarantia"].ToString()) : vGarantia.FechaCertificacionGarantia;
                            vGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vGarantia.IdContrato;
                            vGarantia.FechaInicioGarantia = vDataReaderResults["FechaInicioGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioGarantia"].ToString()) : vGarantia.FechaInicioGarantia;
                            vGarantia.ValorGarantia = vDataReaderResults["ValorGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorGarantia"].ToString()) : vGarantia.ValorGarantia;
                            vGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGarantia.UsuarioCrea;
                            vGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGarantia.FechaCrea;
                            vGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGarantia.UsuarioModifica;
                            vGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGarantia.FechaModifica;
                            vGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vGarantia.Estado;
                            vGarantia.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vGarantia.IDEstadosGarantias;
                            vGarantia.NombreTipoGarantia = vDataReaderResults["NombreTipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoGarantia"].ToString()) : vGarantia.NombreTipoGarantia;
                            vGarantia.CodigoEstadoGarantia = vDataReaderResults["CodigoEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoGarantia"].ToString()) : vGarantia.CodigoEstadoGarantia;
                            vGarantia.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoGarantia"].ToString()) : vGarantia.DescripcionEstadoGarantia;
                            vGarantia.IDSucursalAseguradoraContrato = vDataReaderResults["IdSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSucursalAseguradoraContrato"].ToString()) : vGarantia.IDSucursalAseguradoraContrato;
                            vGarantia.FechaModificacionContractual = vDataReaderResults["FechaModificacionContractual"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacionContractual"].ToString()) : vGarantia.FechaModificacionContractual;
                            vGarantia.TipoModificacionContractual = vDataReaderResults["TipoModificacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacionContractual"].ToString()) : vGarantia.TipoModificacionContractual;
                            
                            vListaGarantia.Add(vGarantia);
                        }
                        return vListaGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<GarantiaHistorico> ConsultarInfoGarantiasHistoricoPorContrato(int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_InfoGarantiaHistorico_ConsultarPorContrato"))
                {

                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GarantiaHistorico> vListaGarantia = new List<GarantiaHistorico>();
                        while (vDataReaderResults.Read())
                        {
                            GarantiaHistorico vGarantia = new GarantiaHistorico();
                            vGarantia.IDGarantiaHistorico = vDataReaderResults["IDGarantiaHistorico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantiaHistorico"].ToString()) : vGarantia.IDGarantiaHistorico;
                            vGarantia.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vGarantia.IDGarantia;
                            vGarantia.IDTipoGarantia = vDataReaderResults["IDTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoGarantia"].ToString()) : vGarantia.IDTipoGarantia;
                            vGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vGarantia.NumeroGarantia;
                            vGarantia.FechaAprobacionGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"].ToString()) : vGarantia.FechaAprobacionGarantia;
                            vGarantia.FechaCertificacionGarantia = vDataReaderResults["FechaCertificacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacionGarantia"].ToString()) : vGarantia.FechaCertificacionGarantia;
                            vGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vGarantia.IdContrato;
                            vGarantia.FechaInicioGarantia = vDataReaderResults["FechaInicioGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioGarantia"].ToString()) : vGarantia.FechaInicioGarantia;
                            vGarantia.ValorGarantia = vDataReaderResults["ValorGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorGarantia"].ToString()) : vGarantia.ValorGarantia;
                            vGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGarantia.UsuarioCrea;
                            vGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGarantia.FechaCrea;
                            vGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGarantia.UsuarioModifica;
                            vGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGarantia.FechaModifica;
                            vGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vGarantia.Estado;
                            vGarantia.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vGarantia.IDEstadosGarantias;
                            vGarantia.NombreTipoGarantia = vDataReaderResults["NombreTipoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoGarantia"].ToString()) : vGarantia.NombreTipoGarantia;
                            vGarantia.CodigoEstadoGarantia = vDataReaderResults["CodigoEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoGarantia"].ToString()) : vGarantia.CodigoEstadoGarantia;
                            vGarantia.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoGarantia"].ToString()) : vGarantia.DescripcionEstadoGarantia;
                            vGarantia.IDSucursalAseguradoraContrato = vDataReaderResults["IdSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSucursalAseguradoraContrato"].ToString()) : vGarantia.IDSucursalAseguradoraContrato;
                            vGarantia.FechaModificacionContractual = vDataReaderResults["FechaModificacionContractual"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacionContractual"].ToString()) : vGarantia.FechaModificacionContractual;
                            vGarantia.TipoModificacionContractual = vDataReaderResults["TipoModificacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModificacionContractual"].ToString()) : vGarantia.TipoModificacionContractual;

                            vListaGarantia.Add(vGarantia);
                        }
                        return vListaGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion 

        #region Garantias Temporales

        public GarantiaHistorico ConsultarGarantiaTemporal(int pIDGarantiaTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_GarantiaTemporal_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantiaTemporal", DbType.Int32, pIDGarantiaTemporal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        GarantiaHistorico vGarantia = null;

                        while (vDataReaderResults.Read())
                        {
                            vGarantia = new GarantiaHistorico();
                            vGarantia.IDGarantiaHistorico = vDataReaderResults["IDGarantiaTemp"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantiaTemp"].ToString()) : vGarantia.IDGarantiaHistorico;
                            vGarantia.IDGarantia = vDataReaderResults["IDGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDGarantia"].ToString()) : vGarantia.IDGarantia;
                            vGarantia.IDTipoGarantia = vDataReaderResults["IDTipoGarantia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoGarantia"].ToString()) : vGarantia.IDTipoGarantia;
                            vGarantia.NumeroGarantia = vDataReaderResults["NumeroGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroGarantia"].ToString()) : vGarantia.NumeroGarantia;
                            vGarantia.FechaAprobacionGarantia = vDataReaderResults["FechaAprobacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacionGarantia"].ToString()) : vGarantia.FechaAprobacionGarantia;
                            vGarantia.FechaCertificacionGarantia = vDataReaderResults["FechaCertificacionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCertificacionGarantia"].ToString()) : vGarantia.FechaCertificacionGarantia;
                            vGarantia.FechaDevolucion = vDataReaderResults["FechaDevolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDevolucion"].ToString()) : vGarantia.FechaDevolucion;
                            vGarantia.MotivoDevolucion = vDataReaderResults["MotivoDevolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoDevolucion"].ToString()) : vGarantia.MotivoDevolucion;
                            vGarantia.IDUsuarioAprobacion = vDataReaderResults["IDUsuarioAprobacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUsuarioAprobacion"].ToString()) : vGarantia.IDUsuarioAprobacion;
                            vGarantia.IDUsuarioDevolucion = vDataReaderResults["IDUsuarioDevolucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDUsuarioDevolucion"].ToString()) : vGarantia.IDUsuarioDevolucion;
                            vGarantia.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vGarantia.IdContrato;
                            vGarantia.BeneficiarioICBF = vDataReaderResults["BeneficiarioICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["BeneficiarioICBF"].ToString()) : vGarantia.BeneficiarioICBF;
                            vGarantia.BeneficiarioOTROS = vDataReaderResults["BeneficiarioOTROS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["BeneficiarioOTROS"].ToString()) : vGarantia.BeneficiarioOTROS;
                            vGarantia.DescripcionBeneficiarios = vDataReaderResults["DescripcionBeneficiarios"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBeneficiarios"].ToString()) : vGarantia.DescripcionBeneficiarios;
                            vGarantia.FechaInicioGarantia = vDataReaderResults["FechaInicioGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioGarantia"].ToString()) : vGarantia.FechaInicioGarantia;
                            vGarantia.FechaExpedicionGarantia = vDataReaderResults["FechaExpedicionGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionGarantia"].ToString()) : vGarantia.FechaExpedicionGarantia;
                            vGarantia.FechaVencimientoInicialGarantia = vDataReaderResults["FechaVencimientoInicialGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoInicialGarantia"].ToString()) : vGarantia.FechaVencimientoInicialGarantia;
                            vGarantia.FechaVencimientoFinalGarantia = vDataReaderResults["FechaVencimientoFinalGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimientoFinalGarantia"].ToString()) : vGarantia.FechaVencimientoFinalGarantia;
                            vGarantia.FechaReciboGarantia = vDataReaderResults["FechaReciboGarantia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReciboGarantia"].ToString()) : vGarantia.FechaReciboGarantia;
                            vGarantia.ValorGarantia = vDataReaderResults["ValorGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorGarantia"].ToString()) : vGarantia.ValorGarantia;
                            vGarantia.Anexos = vDataReaderResults["Anexos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Anexos"].ToString()) : vGarantia.Anexos;
                            vGarantia.ObservacionesAnexos = vDataReaderResults["ObservacionesAnexos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesAnexos"].ToString()) : vGarantia.ObservacionesAnexos;
                            vGarantia.EntidadProvOferenteAseguradora = vDataReaderResults["EntidadProvOferenteAseguradora"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EntidadProvOferenteAseguradora"].ToString()) : vGarantia.EntidadProvOferenteAseguradora;
                            vGarantia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGarantia.UsuarioCrea;
                            vGarantia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGarantia.FechaCrea;
                            vGarantia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGarantia.UsuarioModifica;
                            vGarantia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGarantia.FechaModifica;
                            vGarantia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vGarantia.Estado;
                            vGarantia.IDSucursalAseguradoraContrato = vDataReaderResults["IDSucursalAseguradoraContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSucursalAseguradoraContrato"].ToString()) : vGarantia.IDSucursalAseguradoraContrato;
                            vGarantia.IDEstadosGarantias = vDataReaderResults["IDEstadosGarantias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadosGarantias"].ToString()) : vGarantia.IDEstadosGarantias;
                            vGarantia.DescripcionEstadoGarantia = vDataReaderResults["DescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstadoGarantia"].ToString()) : vGarantia.DescripcionEstadoGarantia;
                            vGarantia.CodigoDescripcionEstadoGarantia = vDataReaderResults["CodDescripcionEstadoGarantia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDescripcionEstadoGarantia"].ToString()) : vGarantia.CodigoDescripcionEstadoGarantia;
                            vGarantia.Zona = vDataReaderResults["Zona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Zona"].ToString()) : vGarantia.Zona;

                        }
                        return vGarantia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int GenerarGarantiaTemporal(int idGarantia, int idTipoModificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_GarantiaTemporal_Generar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDGarantiaTemp", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, idGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModificacion", DbType.Int32, idTipoModificacion);


                    vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDGarantiaTemp").ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarGarantiaTemporal(GarantiaHistorico pGarantiaTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_GarantiaTemporal_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantiaTemporal", DbType.Int32, pGarantiaTemporal.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, pGarantiaTemporal.IDGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoGarantia", DbType.Int32, pGarantiaTemporal.IDTipoGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroGarantia", DbType.String, pGarantiaTemporal.NumeroGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAprobacionGarantia", DbType.DateTime, pGarantiaTemporal.FechaAprobacionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCertificacionGarantia", DbType.DateTime, pGarantiaTemporal.FechaCertificacionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaDevolucion", DbType.DateTime, pGarantiaTemporal.FechaDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoDevolucion", DbType.String, pGarantiaTemporal.MotivoDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@IDUsuarioAprobacion", DbType.Int32, pGarantiaTemporal.IDUsuarioAprobacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDUsuarioDevolucion", DbType.Int32, pGarantiaTemporal.IDUsuarioDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pGarantiaTemporal.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@BeneficiarioICBF", DbType.Boolean, pGarantiaTemporal.BeneficiarioICBF);
                    vDataBase.AddInParameter(vDbCommand, "@BeneficiarioOTROS", DbType.Boolean, pGarantiaTemporal.BeneficiarioOTROS);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionBeneficiarios", DbType.String, pGarantiaTemporal.DescripcionBeneficiarios);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioGarantia", DbType.DateTime, pGarantiaTemporal.FechaInicioGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionGarantia", DbType.DateTime, pGarantiaTemporal.FechaExpedicionGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVencimientoInicialGarantia", DbType.DateTime, pGarantiaTemporal.FechaVencimientoInicialGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVencimientoFinalGarantia", DbType.DateTime, pGarantiaTemporal.FechaVencimientoFinalGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaReciboGarantia", DbType.DateTime, pGarantiaTemporal.FechaReciboGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@ValorGarantia", DbType.String, pGarantiaTemporal.ValorGarantia);
                    vDataBase.AddInParameter(vDbCommand, "@Anexos", DbType.Boolean, pGarantiaTemporal.Anexos);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesAnexos", DbType.String, pGarantiaTemporal.ObservacionesAnexos);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadProvOferenteAseguradora", DbType.Int32, pGarantiaTemporal.EntidadProvOferenteAseguradora);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pGarantiaTemporal.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pGarantiaTemporal.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@IDSucursalAseguradoraContrato", DbType.Int32, pGarantiaTemporal.IDSucursalAseguradoraContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDEstadosGarantias", DbType.Int32, pGarantiaTemporal.IDEstadosGarantias);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGarantiaTemporal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string TipoModificacionGarantiaTemporal(int idGarantia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_GarantiaTemporal_ObtenerTipoModificacion"))
                {
                    string vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@TipoModificacion", DbType.String, 100);
                    vDataBase.AddInParameter(vDbCommand, "@IDGarantia", DbType.Int32, idGarantia);

                    vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToString(vDataBase.GetParameterValue(vDbCommand, "@TipoModificacion"));
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Amparos Garantias Temporales

        public List<AmparosGarantias> ObtenerAmparosGarantiaTemporal(int pidGarantiaTemporal)
        {
            List<AmparosGarantias> resultList = null;


            return resultList;
        }
        
        #endregion

    }
}
