﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad LugarEjecucionContrato
    /// </summary>
    public class CambioLugarEjecucionDAL : GeneralDAL
    {
        public CambioLugarEjecucionDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int InsertarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CambioLugarEjecucion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCambioLugarEjecucion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleConsModContractual", DbType.Int32, pLugarEjecucionContrato.IdDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@IdsRegionales", DbType.String, pLugarEjecucionContrato.IdsRegionales);
                    vDataBase.AddInParameter(vDbCommand, "@NivelNacional", DbType.Boolean, pLugarEjecucionContrato.NivelNacional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pLugarEjecucionContrato.UsuarioCrea);
                    //vDataBase.AddInParameter(vDbCommand, "@DatosAdicionales", DbType.String, pLugarEjecucionContrato.DatosAdicionales);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pLugarEjecucionContrato.IdCambioLugarEjecucion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCambioLugarEjecucion").ToString());
                    GenerarLogAuditoria(pLugarEjecucionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int ModificarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CambioLugarEjecucion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCambioLugarEjecucion", DbType.Int32, pLugarEjecucionContrato.IdCambioLugarEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleConsModContractual", DbType.Int32, pLugarEjecucionContrato.IdDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pLugarEjecucionContrato.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pLugarEjecucionContrato.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@NivelNacional", DbType.Boolean, pLugarEjecucionContrato.NivelNacional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pLugarEjecucionContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pLugarEjecucionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int EliminarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CambioLugarEjecucion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCambioLugarEjecucion", DbType.Int32, pLugarEjecucionContrato.IdCambioLugarEjecucion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pLugarEjecucionContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdLugarEjecucionContratos"></param>
        //public LugarEjecucionContrato ConsultarLugarEjecucionContrato(int pIdLugarEjecucionContratos)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugarEjecucionContrato_Consultar"))
        //        {
        //            vDataBase.AddInParameter(vDbCommand, "@IdLugarEjecucionContratos", DbType.Int32, pIdLugarEjecucionContratos);
        //            using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
        //            {
        //                LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();
        //                while (vDataReaderResults.Read())
        //                {
        //                    vLugarEjecucionContrato.IdLugarEjecucionContratos = vDataReaderResults["IdLugarEjecucionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucionContratos"].ToString()) : vLugarEjecucionContrato.IdLugarEjecucionContratos;
        //                    vLugarEjecucionContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vLugarEjecucionContrato.IdContrato;
        //                    vLugarEjecucionContrato.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vLugarEjecucionContrato.IdDepartamento;
        //                    vLugarEjecucionContrato.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vLugarEjecucionContrato.IdRegional;
        //                    vLugarEjecucionContrato.NivelNacional = vDataReaderResults["NivelNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NivelNacional"].ToString()) : vLugarEjecucionContrato.NivelNacional;
        //                    vLugarEjecucionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucionContrato.UsuarioCrea;
        //                    vLugarEjecucionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucionContrato.FechaCrea;
        //                    vLugarEjecucionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucionContrato.UsuarioModifica;
        //                    vLugarEjecucionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucionContrato.FechaModifica;
        //                }
        //                return vLugarEjecucionContrato;
        //            }
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Método de consulta por filtros para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdRegional"></param>
        /// <param name="pNivelNacional"></param>
        //public List<LugarEjecucionContrato> ConsultarLugarEjecucionContratos(int? pIdContrato, int? pIdDepartamento, int? pIdRegional, Boolean? pNivelNacional)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_LugarEjecucionContratos_Consultar"))
        //        {
        //            if (pIdContrato != null)
        //                vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
        //            if (pIdDepartamento != null)
        //                vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
        //            if (pIdRegional != null)
        //                vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
        //            if (pNivelNacional != null)
        //                vDataBase.AddInParameter(vDbCommand, "@NivelNacional", DbType.Boolean, pNivelNacional);
        //            using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
        //            {
        //                List<LugarEjecucionContrato> vListaLugarEjecucionContrato = new List<LugarEjecucionContrato>();
        //                while (vDataReaderResults.Read())
        //                {
        //                    LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();
        //                    vLugarEjecucionContrato.IdLugarEjecucionContratos = vDataReaderResults["IdLugarEjecucionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdLugarEjecucionContratos"].ToString()) : vLugarEjecucionContrato.IdLugarEjecucionContratos;
        //                    vLugarEjecucionContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vLugarEjecucionContrato.IdContrato;
        //                    vLugarEjecucionContrato.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vLugarEjecucionContrato.IdDepartamento;
        //                    vLugarEjecucionContrato.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vLugarEjecucionContrato.IdRegional;
        //                    vLugarEjecucionContrato.NivelNacional = vDataReaderResults["NivelNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NivelNacional"].ToString()) : vLugarEjecucionContrato.NivelNacional;
        //                    vLugarEjecucionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucionContrato.UsuarioCrea;
        //                    vLugarEjecucionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucionContrato.FechaCrea;
        //                    vLugarEjecucionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucionContrato.UsuarioModifica;
        //                    vLugarEjecucionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucionContrato.FechaModifica;
        //                    vListaLugarEjecucionContrato.Add(vLugarEjecucionContrato);
        //                }
        //                return vListaLugarEjecucionContrato;
        //            }
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>

        /// Obtiene los lugares de ejecución asociados a un contrato
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado con los lugares obtenidos de la base de datos</returns>
        public List<CambioLugarEjecucion> ConsultarCambiosdeLugaresEjecucion(int pIdDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CambioLugarEjecucion_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleConsModContractual", DbType.Int32, pIdDetalleConsModContractual);
                    //vDataBase.AddOutParameter(vDbCommand, "@DatosAdicionales", DbType.String, 2000);
                    vDataBase.AddOutParameter(vDbCommand, "@EsNivelNacional", DbType.Boolean, 1);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                       // string datosAdicionales = vDataBase.GetParameterValue(vDbCommand, "@DatosAdicionales") != null ? vDataBase.GetParameterValue(vDbCommand, "@DatosAdicionales").ToString() : string.Empty;

                        bool esNivelNacional = vDataBase.GetParameterValue(vDbCommand, "@EsNivelNacional") != null ? Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@EsNivelNacional").ToString()) : false;

                        List<CambioLugarEjecucion> vListaLugarEjecucionContrato = new List<CambioLugarEjecucion>();
                        CambioLugarEjecucion vLugarEjecucionContrato;

                        if (esNivelNacional)
                        {
                            vLugarEjecucionContrato = new CambioLugarEjecucion();
                            vLugarEjecucionContrato.EsNivelNacional = esNivelNacional;
                            vListaLugarEjecucionContrato.Add(vLugarEjecucionContrato);
                        }
                        else
                        {
                            while (vDataReaderResults.Read())
                            {
                                vLugarEjecucionContrato = new CambioLugarEjecucion();
                                vLugarEjecucionContrato.IdCambioLugarEjecucion = vDataReaderResults["IdCambioLugarEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCambioLugarEjecucion"].ToString()) : vLugarEjecucionContrato.IdCambioLugarEjecucion;
                                vLugarEjecucionContrato.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vLugarEjecucionContrato.IdDetalleConsModContractual;
                                vLugarEjecucionContrato.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vLugarEjecucionContrato.IdDepartamento;
                                vLugarEjecucionContrato.Departamento = vDataReaderResults["Departamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Departamento"].ToString()) : vLugarEjecucionContrato.Departamento;
                                vLugarEjecucionContrato.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vLugarEjecucionContrato.IdMunicipio;
                                vLugarEjecucionContrato.Municipio = vDataReaderResults["Municipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Municipio"].ToString()) : vLugarEjecucionContrato.Municipio;
                                vLugarEjecucionContrato.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vLugarEjecucionContrato.IdRegional;
                                vLugarEjecucionContrato.NivelNacional = vDataReaderResults["NivelNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NivelNacional"].ToString()) : vLugarEjecucionContrato.NivelNacional;
                                vLugarEjecucionContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vLugarEjecucionContrato.UsuarioCrea;
                                vLugarEjecucionContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vLugarEjecucionContrato.FechaCrea;
                                vLugarEjecucionContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vLugarEjecucionContrato.UsuarioModifica;
                                vLugarEjecucionContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vLugarEjecucionContrato.FechaModifica;
                                vLugarEjecucionContrato.EsNivelNacional = false;
                                //if (vListaLugarEjecucionContrato.Count == 0)
                                //    vLugarEjecucionContrato.DatosAdicionales = datosAdicionales;

                                vListaLugarEjecucionContrato.Add(vLugarEjecucionContrato);
                            }
                        }

                        return vListaLugarEjecucionContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarLugaresEjecucionActuales(int pIdContrato,int pIdDetalleConsModContractual, string pUsuarioCrea)
        {
            try
            {
                

                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_CambioLugarEjecucion_InsertarLugaresEjecucionActuales"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@IdCambioLugarEjecucion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleConsModContractual", DbType.Int32, pIdDetalleConsModContractual);                   
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUsuarioCrea);

                    int vResultado;
                   
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);                    

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }


    }
}

