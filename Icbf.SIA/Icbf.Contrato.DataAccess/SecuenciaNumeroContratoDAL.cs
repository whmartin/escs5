using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Secuencia N�mero Contrato
    /// </summary>
    public class SecuenciaNumeroContratoDAL : GeneralDAL
    {
        public SecuenciaNumeroContratoDAL()
        {
        }
        /// <summary>
        /// M�todo que Genera el Numero de Contrato
        /// </summary>
        /// <param name="pSecuenciaNumeroContrato"></param>
        /// <returns></returns>
        public string GenerarNumeroContrato(SecuenciaNumeroContrato pSecuenciaNumeroContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar"))
                {   
                    vDataBase.AddOutParameter(vDbCommand, "@NumeroContrato", DbType.String, 25);                    
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.Int32, pSecuenciaNumeroContrato.CodigoRegional);
                    vDataBase.AddInParameter(vDbCommand, "@AnoVigencia", DbType.Int32, pSecuenciaNumeroContrato.AnoVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSecuenciaNumeroContrato.UsuarioCrea);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    pSecuenciaNumeroContrato.NumeroContrato = vDataBase.GetParameterValue(vDbCommand, "@NumeroContrato").ToString();
                    GenerarLogAuditoria(pSecuenciaNumeroContrato, vDbCommand);
                    return pSecuenciaNumeroContrato.NumeroContrato;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        
    }
}
