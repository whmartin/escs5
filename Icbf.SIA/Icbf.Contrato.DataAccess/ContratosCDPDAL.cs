﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre 
    /// otros para la entidad ContratosCDP
    /// </summary>
    public class ContratosCDPDAL:GeneralDAL
    {
        /// <summary>
        /// Método de inserción para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP">Instancoa con el registro de contratosCDP</param>
        public int InsertarContratosCDP(ContratosCDP pContratosCDP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContratosCDP", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdCDP", DbType.Int32, pContratosCDP.IdCDP);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContratosCDP.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContratosCDP.UsuarioCrea);

                    if (pContratosCDP.EsAdicion)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pContratosCDP.IdAdicion);
                        vDataBase.AddInParameter(vDbCommand, "@EsAdicion", DbType.Boolean, pContratosCDP.EsAdicion);
                    }

                    if (pContratosCDP.EsVigenciaFutura)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@EsVigenciaFutura", DbType.Boolean, pContratosCDP.EsVigenciaFutura);
                        vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFutura", DbType.Int32, pContratosCDP.IdVigenciaFutura);
                        vDataBase.AddInParameter(vDbCommand, "@ValorCDP", DbType.Decimal, pContratosCDP.ValorCDP);
                        vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.Int32, Convert.ToInt32(pContratosCDP.NumeroCDP));
                    }

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pContratosCDP.IdContratosCDP = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContratosCDP").ToString());
                    GenerarLogAuditoria(pContratosCDP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP">Instancoa con el registro de contratosCDP</param>
        public int InsertarContratosCDP(ContratosCDP pContratosCDP, DataTable vDTRubros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_InsertarConRubros"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContratosCDP", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdCDP", DbType.Int32, pContratosCDP.IdCDP);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContratosCDP.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContratosCDP.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCDP", DbType.Decimal, pContratosCDP.ValorCDP);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCDP", DbType.Int32, Convert.ToInt32(pContratosCDP.NumeroCDP));
                    vDataBase.AddInParameter(vDbCommand, "FechaCDP", DbType.DateTime, pContratosCDP.FechaCDP);

                    if (pContratosCDP.EsAdicion)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, pContratosCDP.IdAdicion);
                        vDataBase.AddInParameter(vDbCommand, "@EsAdicion", DbType.Boolean, pContratosCDP.EsAdicion);
                    }

                    if (pContratosCDP.EsVigenciaFutura)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@EsVigenciaFutura", DbType.Boolean, pContratosCDP.EsVigenciaFutura);
                        vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFutura", DbType.Int32, pContratosCDP.IdVigenciaFutura);
                    }

                    var pDatosProductos = new SqlParameter("@RubrosCDP", SqlDbType.Structured)
                    {
                        Value = vDTRubros
                    };

                    vDbCommand.Parameters.Add(pDatosProductos);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pContratosCDP.IdContratosCDP = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContratosCDP").ToString());
                    GenerarLogAuditoria(pContratosCDP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminar para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP"></param>
        /// <returns></returns>
        public int EliminarContratosCDP(ContratosCDP pContratosCDP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_Eliminar"))
                {

                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratosCDP", DbType.Int32, pContratosCDP.IdCDP);

                    if(pContratosCDP.IdContrato != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContratosCDP.IdContrato);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContratosCDP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarContratosCDPVigencias(ContratosCDP pContratosCDP, int pVigenciaFutura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDPVigencias_Eliminar"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratosCDP", DbType.Int32, pContratosCDP.IdContratosCDP);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFutura", DbType.Int32, pVigenciaFutura);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pContratosCDP.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContratosCDP, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de consulta CDPs asociados a un contrato
        /// 01-07-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <returns>Lista de entidades ContratosCDP con la información obtenida de la base de datos</returns>
        public List<ContratosCDP> ConsultarContratosCDP(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        while (vDataReaderResults.Read())
                        {

                            ContratosCDP vContratosCDP = new ContratosCDP();
                            vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                            vContratosCDP.IdCDP = vDataReaderResults["IdCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCDP"].ToString()) : vContratosCDP.IdCDP;
                            vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                            vContratosCDP.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratosCDP.Regional;
                            vContratosCDP.Area = vDataReaderResults["Area"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Area"].ToString()) : vContratosCDP.Area;
                            vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                            vContratosCDP.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vContratosCDP.FechaCDP;
                            vContratosCDP.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vContratosCDP.ValorCDP;
                            vContratosCDP.ValorInicialCDP = vDataReaderResults["ValorInicialCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialCDP"].ToString()) : vContratosCDP.ValorInicialCDP;
                            vContratosCDP.ValorActualCDP = vDataReaderResults["ValorActualCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualCDP"].ToString()) : vContratosCDP.ValorActualCDP;                            
                            vContratosCDP.RubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vContratosCDP.RubroPresupuestal;
                            vContratosCDP.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vContratosCDP.TipoFuenteFinanciamiento;
                            vContratosCDP.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vContratosCDP.RecursoPresupuestal;
                            vContratosCDP.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGastos"].ToString()) : vContratosCDP.DependenciaAfectacionGastos;
                            vContratosCDP.TipoDocumentoSoporte = vDataReaderResults["TipoDocumentoSoporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoSoporte"].ToString()) : vContratosCDP.TipoDocumentoSoporte;
                            vContratosCDP.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vContratosCDP.TipoSituacionFondos;
                            vContratosCDP.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vContratosCDP.ConsecutivoPlanCompras;
                            vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                            vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                            vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                            vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                            vContratosCDP.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vContratosCDP.CodigoRubro;
                            vContratosCDP.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vContratosCDP.DescripcionRubro;
                            vContratosCDP.ValorRubro = vDataReaderResults["ValorRubro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubro"].ToString()) : vContratosCDP.ValorRubro;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<ContratosCDP> ConsultarContratosCDPUnificado(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_ObtenerUnificado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        while (vDataReaderResults.Read())
                        {

                            ContratosCDP vContratosCDP = new ContratosCDP();
                            vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                            vContratosCDP.IdCDP = vDataReaderResults["IdCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCDP"].ToString()) : vContratosCDP.IdCDP;
                            vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                            vContratosCDP.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratosCDP.Regional;
                            vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                            vContratosCDP.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vContratosCDP.FechaCDP;
                            vContratosCDP.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vContratosCDP.ValorCDP;
                            vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                            vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                            vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                            vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<ContratosCDP> ConsultarContratosCDPRubroUnificado(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_ObtenerRubroUnificado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        string rubroPresupuestal, recursoPresupuestal;
                        ContratosCDP vContratosCDP = new ContratosCDP();

                        while (vDataReaderResults.Read())
                        {
                            rubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vContratosCDP.RubroPresupuestal;
                            recursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vContratosCDP.RecursoPresupuestal;

                            if (! vListaContratosCDP.Any(e => e.RubroPresupuestal == rubroPresupuestal && e.RecursoPresupuestal == recursoPresupuestal))
                            {
                                vContratosCDP = new ContratosCDP();
                                vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                                vContratosCDP.IdCDP = vDataReaderResults["IdCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCDP"].ToString()) : vContratosCDP.IdCDP;
                                vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                                vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                                vContratosCDP.ValorCDP = vDataReaderResults["ValorRubro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubro"].ToString()) : vContratosCDP.ValorCDP;
                                vContratosCDP.ValorInicialCDP = vDataReaderResults["ValorInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicial"].ToString()) : vContratosCDP.ValorInicialCDP;
                                vContratosCDP.ValorActualCDP = vDataReaderResults["ValorActual"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActual"].ToString()) : vContratosCDP.ValorActualCDP;
                                vContratosCDP.RubroPresupuestal = rubroPresupuestal;
                                vContratosCDP.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vContratosCDP.TipoFuenteFinanciamiento;
                                vContratosCDP.RecursoPresupuestal = recursoPresupuestal;
                                vContratosCDP.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGastos"].ToString()) : vContratosCDP.DependenciaAfectacionGastos;
                                vContratosCDP.TipoDocumentoSoporte = vDataReaderResults["TipoDocumentoSoporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoSoporte"].ToString()) : vContratosCDP.TipoDocumentoSoporte;
                                vContratosCDP.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vContratosCDP.TipoSituacionFondos;
                                vContratosCDP.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vContratosCDP.ConsecutivoPlanCompras;
                                vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                                vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                                vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                                vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                                vContratosCDP.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vContratosCDP.CodigoRubro;
                                vContratosCDP.ValorRubro = vDataReaderResults["ValorRubro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubro"].ToString()) : vContratosCDP.ValorRubro;
                                vListaContratosCDP.Add(vContratosCDP);
                            }
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPSimple(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_ObtenerSimple"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        while (vDataReaderResults.Read())
                        {
                            ContratosCDP vContratosCDP = new ContratosCDP();
                            vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                            vContratosCDP.IdCDP = vDataReaderResults["IdCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCDP"].ToString()) : vContratosCDP.IdCDP;
                            vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                            vContratosCDP.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratosCDP.Regional;
                            vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                            vContratosCDP.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vContratosCDP.FechaCDP;
                            vContratosCDP.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vContratosCDP.ValorCDP;
                            vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                            vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                            vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                            vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPRubroSimple(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_ObtenerRubroSimple"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        string rubroPresupuestal, recursoPresupuestal;
                        ContratosCDP vContratosCDP= new ContratosCDP();

                        while (vDataReaderResults.Read())
                        {
                            rubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vContratosCDP.RubroPresupuestal;
                            recursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vContratosCDP.RecursoPresupuestal;

                            if (!vListaContratosCDP.Any(e => e.RubroPresupuestal == rubroPresupuestal && e.RecursoPresupuestal == recursoPresupuestal))
                            {
                                vContratosCDP = new ContratosCDP();
                                vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                                vContratosCDP.IdCDP = vDataReaderResults["IdCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCDP"].ToString()) : vContratosCDP.IdCDP;
                                vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                                vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                                vContratosCDP.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vContratosCDP.FechaCDP;
                                vContratosCDP.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vContratosCDP.ValorCDP;
                                vContratosCDP.ValorInicialCDP = vDataReaderResults["ValorInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicial"].ToString()) : vContratosCDP.ValorInicialCDP;
                                vContratosCDP.ValorActualCDP = vDataReaderResults["ValorActual"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActual"].ToString()) : vContratosCDP.ValorActualCDP;
                                vContratosCDP.RubroPresupuestal = rubroPresupuestal;
                                vContratosCDP.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vContratosCDP.TipoFuenteFinanciamiento;
                                vContratosCDP.RecursoPresupuestal = recursoPresupuestal;
                                vContratosCDP.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGastos"].ToString()) : vContratosCDP.DependenciaAfectacionGastos;
                                vContratosCDP.TipoDocumentoSoporte = vDataReaderResults["TipoDocumentoSoporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoSoporte"].ToString()) : vContratosCDP.TipoDocumentoSoporte;
                                vContratosCDP.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vContratosCDP.TipoSituacionFondos;
                                vContratosCDP.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vContratosCDP.ConsecutivoPlanCompras;
                                vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                                vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                                vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                                vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                                vContratosCDP.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vContratosCDP.CodigoRubro;
                                vContratosCDP.ValorRubro = vDataReaderResults["ValorRubro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubro"].ToString()) : vContratosCDP.ValorRubro;
                                vListaContratosCDP.Add(vContratosCDP);
                            }
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPVigencias(int pIdContrato,bool pEsVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDPVigencias_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@esVigenciaFutura", DbType.Int32, pEsVigenciaFutura);
                    if (pAnioVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.Int32, pAnioVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        while (vDataReaderResults.Read())
                        {
                            ContratosCDP vContratosCDP = new ContratosCDP();
                            vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                            vContratosCDP.IdCDP = vDataReaderResults["IdCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCDP"].ToString()) : vContratosCDP.IdCDP;
                            vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                            vContratosCDP.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratosCDP.Regional;
                            vContratosCDP.Area = vDataReaderResults["Area"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Area"].ToString()) : vContratosCDP.Area;
                            vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                            vContratosCDP.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vContratosCDP.FechaCDP;
                            vContratosCDP.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vContratosCDP.ValorCDP;
                            vContratosCDP.ValorInicialCDP = vDataReaderResults["ValorInicialCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicialCDP"].ToString()) : vContratosCDP.ValorInicialCDP;
                            vContratosCDP.ValorActualCDP = vDataReaderResults["ValorActualCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualCDP"].ToString()) : vContratosCDP.ValorActualCDP;
                            vContratosCDP.RubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vContratosCDP.RubroPresupuestal;
                            vContratosCDP.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vContratosCDP.TipoFuenteFinanciamiento;
                            vContratosCDP.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vContratosCDP.RecursoPresupuestal;
                            vContratosCDP.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGastos"].ToString()) : vContratosCDP.DependenciaAfectacionGastos;
                            vContratosCDP.TipoDocumentoSoporte = vDataReaderResults["TipoDocumentoSoporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoSoporte"].ToString()) : vContratosCDP.TipoDocumentoSoporte;
                            vContratosCDP.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vContratosCDP.TipoSituacionFondos;
                            vContratosCDP.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vContratosCDP.ConsecutivoPlanCompras;
                            vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                            vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                            vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                            vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                            vContratosCDP.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vContratosCDP.CodigoRubro;
                            vContratosCDP.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vContratosCDP.DescripcionRubro;
                            vContratosCDP.ValorRubro = vDataReaderResults["ValorRubro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubro"].ToString()) : vContratosCDP.ValorRubro;
                            vContratosCDP.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vContratosCDP.Estado;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.ObligacionesCDP ConsultarDatosContratoSigepcyp(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Consultar_DatosContratoSigepcyp"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Contrato.Entity.ObligacionesCDP vObligacionesCDP = new Contrato.Entity.ObligacionesCDP();
                        while (vDataReaderResults.Read())
                        {
                            vObligacionesCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroCDP"].ToString()) : vObligacionesCDP.NumeroCDP;
                            vObligacionesCDP.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroIdentificacion"].ToString()) : vObligacionesCDP.NumeroIdentificacion;
                            vObligacionesCDP.FechaSolicitud = vDataReaderResults["FechaSolicitudCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudCDP"].ToString()) : vObligacionesCDP.FechaSolicitud;
                            vObligacionesCDP.ValorActualCDP = vDataReaderResults["ValorActualCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorActualCDP"].ToString()) : vObligacionesCDP.ValorActualCDP;
                            vObligacionesCDP.AnioVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vObligacionesCDP.AnioVigencia;
                            vObligacionesCDP.IdRegionalContrato = vDataReaderResults["IdRegionalContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalContrato"].ToString()) : vObligacionesCDP.IdRegionalContrato;


                        }
                        return vObligacionesCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPRubroLinea(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_ObtenerRubroLinea"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        ContratosCDP vContratosCDP = new ContratosCDP();

                        while (vDataReaderResults.Read())
                        {
                                vContratosCDP = new ContratosCDP();
                                vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                                vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                                vContratosCDP.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vContratosCDP.TipoFuenteFinanciamiento;
                                vContratosCDP.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vContratosCDP.RecursoPresupuestal;     
                                vContratosCDP.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGasto"].ToString()) : vContratosCDP.DependenciaAfectacionGastos;
                                vContratosCDP.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vContratosCDP.TipoSituacionFondos;
                                vContratosCDP.RubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vContratosCDP.RubroPresupuestal;
                                vContratosCDP.ValorRubro = vDataReaderResults["valor"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["valor"].ToString()) : vContratosCDP.ValorRubro;
                                vContratosCDP.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vContratosCDP.CodigoRubro;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPLinea(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_ObtenerLinea"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        while (vDataReaderResults.Read())
                        {
                            ContratosCDP vContratosCDP = new ContratosCDP();
                            vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                            vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                            vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                            vContratosCDP.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vContratosCDP.FechaCDP;
                            vContratosCDP.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vContratosCDP.ValorCDP;
                            vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                            vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                            vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                            vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPRubroLineaVF(int pIdContrato,bool pEsVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_ObtenerRubroLineaVF"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    vDataBase.AddInParameter(vDbCommand, "@esVigenciaFutura", DbType.Int32, pEsVigenciaFutura);
                    if (pAnioVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.Int32, pAnioVigencia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        ContratosCDP vContratosCDP = new ContratosCDP();

                        while (vDataReaderResults.Read())
                        {
                            vContratosCDP = new ContratosCDP();
                            vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                            vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                            vContratosCDP.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vContratosCDP.TipoFuenteFinanciamiento;
                            vContratosCDP.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vContratosCDP.RecursoPresupuestal;
                            vContratosCDP.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGasto"].ToString()) : vContratosCDP.DependenciaAfectacionGastos;
                            vContratosCDP.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vContratosCDP.TipoSituacionFondos;
                            vContratosCDP.RubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vContratosCDP.RubroPresupuestal;
                            vContratosCDP.ValorRubro = vDataReaderResults["valor"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["valor"].ToString()) : vContratosCDP.ValorRubro;
                            vContratosCDP.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vContratosCDP.CodigoRubro;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPLineaVF(int pIdContrato,bool pEsVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_ObtenerLineaVF"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    vDataBase.AddInParameter(vDbCommand, "@esVigenciaFutura", DbType.Int32, pEsVigenciaFutura);
                    
                    if (pAnioVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.Int32, pAnioVigencia);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDP> vListaContratosCDP = new List<ContratosCDP>();
                        while (vDataReaderResults.Read())
                        {
                            ContratosCDP vContratosCDP = new ContratosCDP();
                            vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                            vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                            vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                            vContratosCDP.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vContratosCDP.FechaCDP;
                            vContratosCDP.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vContratosCDP.ValorCDP;
                            vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                            vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                            vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                            vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
