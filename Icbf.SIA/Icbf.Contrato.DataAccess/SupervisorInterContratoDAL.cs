﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad SupervisorInterContrato
    /// </summary>
    public class SupervisorInterContratoDAL : GeneralDAL
    {
        public SupervisorInterContratoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int InsertarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pSupervisorInterContrato.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pSupervisorInterContrato.Inactivo);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pSupervisorInterContrato.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, pSupervisorInterContrato.TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoSuperInter", DbType.Int32, pSupervisorInterContrato.IDTipoSuperInter);
                    if (pSupervisorInterContrato.IdNumeroContratoInterventoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdNumeroContratoInterventoria", DbType.Int32, pSupervisorInterContrato.IdNumeroContratoInterventoria);
                    if (pSupervisorInterContrato.IDProveedoresInterventor != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDProveedoresInterventor", DbType.Int32, pSupervisorInterContrato.IDProveedoresInterventor);
                    if (pSupervisorInterContrato.IDEmpleadosSupervisor != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEmpleadosSupervisor", DbType.Int32, pSupervisorInterContrato.IDEmpleadosSupervisor);
                    if (pSupervisorInterContrato.IdRol != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRol", DbType.Int32, pSupervisorInterContrato.IdRol);
                    // if (pSupervisorInterContrato.IdContrato != null)
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pSupervisorInterContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisorInterContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisorInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDSupervisorIntervContrato").ToString());
                    GenerarLogAuditoria(pSupervisorInterContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int InsertarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato, DirectorInterventoria pDirectorInterventoria)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_DirectorInterventoria_Insertar"))
                {
                    int vResultado = 0;

                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;

                    vDataBase.AddOutParameter(vDbCommand, "@IDDirectorInterventoria", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pDirectorInterventoria.IdTipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pDirectorInterventoria.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pDirectorInterventoria.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pDirectorInterventoria.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pDirectorInterventoria.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pDirectorInterventoria.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pDirectorInterventoria.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pDirectorInterventoria.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pDirectorInterventoria.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDirectorInterventoria.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    if (vResultado != 1)
                    {
                        throw new Exception("InsertarSupervisorInterContrato_DirectorInterventoria");
                    }
                    pDirectorInterventoria.IDDirectorInterventoria = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDDirectorInterventoria").ToString());
                    pSupervisorInterContrato.IDDirectorInterventoria = pDirectorInterventoria.IDDirectorInterventoria;
                    GenerarLogAuditoria(pDirectorInterventoria, vDbCommand);
                    vDbCommand.Parameters.Clear();

                    using (DbCommand vDbCommandSupervisorInterContrato = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContrato_Insertar"))
                    {   
                        vDbCommandSupervisorInterContrato.Transaction = vDbTransaction;

                        vDataBase.AddOutParameter(vDbCommandSupervisorInterContrato, "@IDSupervisorIntervContrato", DbType.Int32, 18);
                        vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@FechaInicio", DbType.DateTime, pSupervisorInterContrato.FechaInicio);
                        vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@Inactivo", DbType.Boolean, pSupervisorInterContrato.Inactivo);
                        vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@Identificacion", DbType.String, pSupervisorInterContrato.Identificacion);
                        vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@TipoIdentificacion", DbType.String, pSupervisorInterContrato.TipoIdentificacion);
                        vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@IDTipoSuperInter", DbType.Int32, pSupervisorInterContrato.IDTipoSuperInter);
                        if (pSupervisorInterContrato.IdNumeroContratoInterventoria != null)
                            vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@IdNumeroContratoInterventoria", DbType.Int32, pSupervisorInterContrato.IdNumeroContratoInterventoria);
                        if (pSupervisorInterContrato.IDProveedoresInterventor != null)
                            vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@IDProveedoresInterventor", DbType.Int32, pSupervisorInterContrato.IDProveedoresInterventor);
                        if (pSupervisorInterContrato.IDEmpleadosSupervisor != null)
                            vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@IDEmpleadosSupervisor", DbType.Int32, pSupervisorInterContrato.IDEmpleadosSupervisor);
                        if (pSupervisorInterContrato.IdContrato != null)
                            vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@IdContrato", DbType.Int32, pSupervisorInterContrato.IdContrato);
                        if (pSupervisorInterContrato.IDDirectorInterventoria != null)
                            vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@IDDirectorInterventoria", DbType.Int32, pSupervisorInterContrato.IDDirectorInterventoria);
                        vDataBase.AddInParameter(vDbCommandSupervisorInterContrato, "@UsuarioCrea", DbType.String, pSupervisorInterContrato.UsuarioCrea);
                        vResultado = vDataBase.ExecuteNonQuery(vDbCommandSupervisorInterContrato, vDbTransaction);
                        if (vResultado != 1)
                        {
                            throw new Exception("InsertarSupervisorInterContrato_SupervisorInterContrato");
                        }
                        pSupervisorInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommandSupervisorInterContrato, "@IDSupervisorIntervContrato").ToString());
                        GenerarLogAuditoria(pSupervisorInterContrato, vDbCommand);
                        vDbCommandSupervisorInterContrato.Parameters.Clear();
                    }

                    vDbTransaction.Commit();
                    vDBConnection.Close();
                    return vResultado;
                }               
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int ModificarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pSupervisorInterContrato.IDSupervisorIntervContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pSupervisorInterContrato.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pSupervisorInterContrato.Inactivo);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pSupervisorInterContrato.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, pSupervisorInterContrato.TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoSuperInter", DbType.Int32, pSupervisorInterContrato.IDTipoSuperInter);
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroContratoInterventoria", DbType.Int32, pSupervisorInterContrato.IdNumeroContratoInterventoria);
                    vDataBase.AddInParameter(vDbCommand, "@IDProveedoresInterventor", DbType.Int32, pSupervisorInterContrato.IDProveedoresInterventor);
                    vDataBase.AddInParameter(vDbCommand, "@IDEmpleadosSupervisor", DbType.Int32, pSupervisorInterContrato.IDEmpleadosSupervisor);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pSupervisorInterContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisorInterContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisorInterContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int EliminarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pSupervisorInterContrato.IDSupervisorIntervContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisorInterContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        public SupervisorInterContrato ConsultarSupervisorInterContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pIDSupervisorIntervContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisorInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vSupervisorInterContrato.IDSupervisorIntervContrato;
                            vSupervisorInterContrato.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSupervisorInterContrato.FechaInicio;
                            vSupervisorInterContrato.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vSupervisorInterContrato.Inactivo;
                            vSupervisorInterContrato.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : vSupervisorInterContrato.Identificacion;
                            vSupervisorInterContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisorInterContrato.TipoIdentificacion;
                            vSupervisorInterContrato.IDTipoSuperInter = vDataReaderResults["IDTipoSuperInter"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSuperInter"].ToString()) : vSupervisorInterContrato.IDTipoSuperInter;
                            vSupervisorInterContrato.IdNumeroContratoInterventoria = vDataReaderResults["IdNumeroContratoInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroContratoInterventoria"].ToString()) : vSupervisorInterContrato.IdNumeroContratoInterventoria;
                            vSupervisorInterContrato.IDProveedoresInterventor = vDataReaderResults["IDProveedoresInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProveedoresInterventor"].ToString()) : vSupervisorInterContrato.IDProveedoresInterventor;
                            vSupervisorInterContrato.IDEmpleadosSupervisor = vDataReaderResults["IDEmpleadosSupervisor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEmpleadosSupervisor"].ToString()) : vSupervisorInterContrato.IDEmpleadosSupervisor;
                            vSupervisorInterContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSupervisorInterContrato.IdContrato;
                            vSupervisorInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterContrato.UsuarioCrea;
                            vSupervisorInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterContrato.FechaCrea;
                            vSupervisorInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterContrato.UsuarioModifica;
                            vSupervisorInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterContrato.FechaModifica;
                        }
                        return vSupervisorInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pFechaInicio"></param>
        /// <param name="pInactivo"></param>
        /// <param name="pIdentificacion"></param>
        /// <param name="pTipoIdentificacion"></param>
        /// <param name="pIDTipoSuperInter"></param>
        /// <param name="pIdNumeroContratoInterventoria"></param>
        /// <param name="pIDProveedoresInterventor"></param>
        /// <param name="pIDEmpleadosSupervisor"></param>
        /// <param name="pIdContrato"></param>
        public List<SupervisorInterContrato> ConsultarSupervisorInterContratos(DateTime? pFechaInicio, Boolean? pInactivo, String pIdentificacion, String pTipoIdentificacion, int? pIDTipoSuperInter, int? pIdNumeroContratoInterventoria, int? pIDProveedoresInterventor, int? pIDEmpleadosSupervisor, int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContratos_Consultar"))
                {
                    if (pFechaInicio != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFechaInicio);
                    if (pInactivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pInactivo);
                    if (pIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pIdentificacion);
                    if (pTipoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, pTipoIdentificacion);
                    if (pIDTipoSuperInter != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTipoSuperInter", DbType.Int32, pIDTipoSuperInter);
                    if (pIdNumeroContratoInterventoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdNumeroContratoInterventoria", DbType.Int32, pIdNumeroContratoInterventoria);
                    if (pIDProveedoresInterventor != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDProveedoresInterventor", DbType.Int32, pIDProveedoresInterventor);
                    if (pIDEmpleadosSupervisor != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEmpleadosSupervisor", DbType.Int32, pIDEmpleadosSupervisor);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisorInterContrato> vListaSupervisorInterContrato = new List<SupervisorInterContrato>();
                        while (vDataReaderResults.Read())
                        {
                            SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                            vSupervisorInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vSupervisorInterContrato.IDSupervisorIntervContrato;
                            vSupervisorInterContrato.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSupervisorInterContrato.FechaInicio;
                            vSupervisorInterContrato.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vSupervisorInterContrato.Inactivo;
                            vSupervisorInterContrato.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : vSupervisorInterContrato.Identificacion;
                            vSupervisorInterContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisorInterContrato.TipoIdentificacion;
                            vSupervisorInterContrato.IDTipoSuperInter = vDataReaderResults["IDTipoSuperInter"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSuperInter"].ToString()) : vSupervisorInterContrato.IDTipoSuperInter;
                            vSupervisorInterContrato.IdNumeroContratoInterventoria = vDataReaderResults["IdNumeroContratoInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroContratoInterventoria"].ToString()) : vSupervisorInterContrato.IdNumeroContratoInterventoria;
                            vSupervisorInterContrato.IDProveedoresInterventor = vDataReaderResults["IDProveedoresInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProveedoresInterventor"].ToString()) : vSupervisorInterContrato.IDProveedoresInterventor;
                            vSupervisorInterContrato.IDEmpleadosSupervisor = vDataReaderResults["IDEmpleadosSupervisor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEmpleadosSupervisor"].ToString()) : vSupervisorInterContrato.IDEmpleadosSupervisor;
                            vSupervisorInterContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSupervisorInterContrato.IdContrato;
                            vSupervisorInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterContrato.UsuarioCrea;
                            vSupervisorInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterContrato.FechaCrea;
                            vSupervisorInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterContrato.UsuarioModifica;
                            vSupervisorInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterContrato.FechaModifica;
                            vListaSupervisorInterContrato.Add(vSupervisorInterContrato);
                        }
                        return vListaSupervisorInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información relacionada con los supervisores/interventores asociados a un contrato
        /// dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public List<SupervisorInterContrato> ObtenerSupervisoresInterventoresContrato(int pIdContrato, bool? pDirectoresInterventoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContrato_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pDirectoresInterventoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@DirectoresInterventoria", DbType.Boolean, pDirectoresInterventoria);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisorInterContrato> vListaSupervisorInterContrato = new List<SupervisorInterContrato>();
                        while (vDataReaderResults.Read())
                        {
                            var Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : string.Empty;

                            if (! vListaSupervisorInterContrato.Any(e => e.Identificacion == Identificacion))
                            {
                                SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                                vSupervisorInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vSupervisorInterContrato.IDSupervisorIntervContrato;
                                vSupervisorInterContrato.EtQSupervisorInterventor = vDataReaderResults["EtQSupervisorInterventor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQSupervisorInterventor"].ToString()) : vSupervisorInterContrato.EtQSupervisorInterventor;
                                vSupervisorInterContrato.EtQInternoExterno = vDataReaderResults["EtQInternoExterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQInternoExterno"].ToString()) : vSupervisorInterContrato.EtQInternoExterno;
                                vSupervisorInterContrato.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSupervisorInterContrato.FechaInicio;
                                vSupervisorInterContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisorInterContrato.TipoIdentificacion;
                                vSupervisorInterContrato.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : vSupervisorInterContrato.Identificacion;
                                vSupervisorInterContrato.NombreCompletoSuperInterventor = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.NombreCompleto;
                                vSupervisorInterContrato.SupervisorInterventor.TipoVinculacionContractual = vDataReaderResults["TipoVinculacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacionContractual"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.TipoVinculacionContractual;
                                vSupervisorInterContrato.SupervisorInterventor.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.NombreCompleto;
                                vSupervisorInterContrato.SupervisorInterventor.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Regional;
                                vSupervisorInterContrato.SupervisorInterventor.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Dependencia;
                                vSupervisorInterContrato.SupervisorInterventor.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Cargo;
                                vSupervisorInterContrato.SupervisorInterventor.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Direccion;
                                vSupervisorInterContrato.SupervisorInterventor.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Telefono;
                                vSupervisorInterContrato.SupervisorInterventor.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Celular;
                                vSupervisorInterContrato.SupervisorInterventor.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.CorreoElectronico;
                                vSupervisorInterContrato.NumeroContratoInterventoria = vDataReaderResults["IdNumeroContratoInterventoria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdNumeroContratoInterventoria"].ToString()) : vSupervisorInterContrato.NumeroContratoInterventoria;
                                vSupervisorInterContrato.IDTipoSuperInter = vDataReaderResults["IDTipoSuperInter"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSuperInter"].ToString()) : vSupervisorInterContrato.IDTipoSuperInter;
                                vSupervisorInterContrato.IDProveedoresInterventor = vDataReaderResults["IDProveedoresInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProveedoresInterventor"].ToString()) : vSupervisorInterContrato.IDProveedoresInterventor;
                                vSupervisorInterContrato.IDEmpleadosSupervisor = vDataReaderResults["IDEmpleadosSupervisor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEmpleadosSupervisor"].ToString()) : vSupervisorInterContrato.IDEmpleadosSupervisor;
                                vSupervisorInterContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSupervisorInterContrato.IdContrato;
                                vSupervisorInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterContrato.UsuarioCrea;
                                vSupervisorInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterContrato.FechaCrea;
                                vSupervisorInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterContrato.UsuarioModifica;
                                vSupervisorInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterContrato.FechaModifica;
                                vSupervisorInterContrato.NombreRol = vDataReaderResults["NombreRol"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRol"].ToString()) : vSupervisorInterContrato.NombreRol;
                                vListaSupervisorInterContrato.Add(vSupervisorInterContrato);
                            }
                        }
                        return vListaSupervisorInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisorInterContrato> ObtenerSupervisoresInterventoresContratoMigrado(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisoresMigracion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisorInterContrato> vListaSupervisorInterContrato = new List<SupervisorInterContrato>();
                        while (vDataReaderResults.Read())
                        {
                            var Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : string.Empty;

                            if (!vListaSupervisorInterContrato.Any(e => e.Identificacion == Identificacion))
                            {
                                SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                                vSupervisorInterContrato.EtQSupervisorInterventor = vDataReaderResults["EtQSupervisorInterventor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQSupervisorInterventor"].ToString()) : vSupervisorInterContrato.EtQSupervisorInterventor;
                                vSupervisorInterContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisorInterContrato.TipoIdentificacion;
                                vSupervisorInterContrato.NombreCompletoSuperInterventor = vDataReaderResults["NombreSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSupervisor"].ToString()) : vSupervisorInterContrato.NombreCompletoSuperInterventor;
                                vSupervisorInterContrato.SupervisorInterventor.Cargo = vDataReaderResults["CargoSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSupervisor"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Cargo;
                                vSupervisorInterContrato.SupervisorInterventor.Dependencia = vDataReaderResults["AreaSupervisor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AreaSupervisor"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Dependencia;
                                vSupervisorInterContrato.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : vSupervisorInterContrato.Identificacion;
                                
                                vListaSupervisorInterContrato.Add(vSupervisorInterContrato);
                            }
                        }
                        return vListaSupervisorInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de modificación del campo FechaInicio para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato">Entidad con la información a actualizar</param>
        /// <returns>Resultado de la operación</returns>
        public int ActualizarFechaInicioSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContrato_Actualiza_FechaInicio"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pSupervisorInterContrato.IDSupervisorIntervContrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pSupervisorInterContrato.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSupervisorInterContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Efrain Diaz
        /// Obtiene la información relacionada con los supervisores/interventores 
        /// dado
        /// </summary>
        /// <param name="pNumeroIdentidicacion">Entero con el identificador Supervisor/Inerventor</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public SupervisorInterContrato ObtenerSupervisorInterventoresContratoDetalle(int pNumeroIdentidicacion, bool? pDirectoresInterventoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterventorContrato_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentidicacion", DbType.Int32, pNumeroIdentidicacion);
                    if (pDirectoresInterventoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@DirectoresInterventoria", DbType.Boolean, pDirectoresInterventoria);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                        while (vDataReaderResults.Read())
                        {
                            
                            vSupervisorInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vSupervisorInterContrato.IDSupervisorIntervContrato;
                            vSupervisorInterContrato.EtQSupervisorInterventor = vDataReaderResults["EtQSupervisorInterventor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQSupervisorInterventor"].ToString()) : vSupervisorInterContrato.EtQSupervisorInterventor;
                            vSupervisorInterContrato.EtQInternoExterno = vDataReaderResults["EtQInternoExterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQInternoExterno"].ToString()) : vSupervisorInterContrato.EtQInternoExterno;
                            vSupervisorInterContrato.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSupervisorInterContrato.FechaInicio;
                            vSupervisorInterContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisorInterContrato.TipoIdentificacion;
                            vSupervisorInterContrato.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : vSupervisorInterContrato.Identificacion;
                            vSupervisorInterContrato.SupervisorInterventor.TipoVinculacionContractual = vDataReaderResults["TipoVinculacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacionContractual"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.TipoVinculacionContractual;
                            vSupervisorInterContrato.SupervisorInterventor.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.NombreCompleto;
                            vSupervisorInterContrato.SupervisorInterventor.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Regional;
                            vSupervisorInterContrato.SupervisorInterventor.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Dependencia;
                            vSupervisorInterContrato.SupervisorInterventor.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Cargo;
                            vSupervisorInterContrato.SupervisorInterventor.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Direccion;
                            vSupervisorInterContrato.SupervisorInterventor.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Telefono;
                            vSupervisorInterContrato.SupervisorInterventor.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Celular;
                            vSupervisorInterContrato.SupervisorInterventor.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.CorreoElectronico;
                            vSupervisorInterContrato.NumeroContratoInterventoria = vDataReaderResults["IdNumeroContratoInterventoria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdNumeroContratoInterventoria"].ToString()) : vSupervisorInterContrato.NumeroContratoInterventoria;
                            vSupervisorInterContrato.IDTipoSuperInter = vDataReaderResults["IDTipoSuperInter"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSuperInter"].ToString()) : vSupervisorInterContrato.IDTipoSuperInter;
                            vSupervisorInterContrato.IDProveedoresInterventor = vDataReaderResults["IDProveedoresInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProveedoresInterventor"].ToString()) : vSupervisorInterContrato.IDProveedoresInterventor;
                            vSupervisorInterContrato.IDEmpleadosSupervisor = vDataReaderResults["IDEmpleadosSupervisor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEmpleadosSupervisor"].ToString()) : vSupervisorInterContrato.IDEmpleadosSupervisor;
                            //vSupervisorInterContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSupervisorInterContrato.IdContrato;
                            vSupervisorInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterContrato.UsuarioCrea;
                            vSupervisorInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterContrato.FechaCrea;
                            vSupervisorInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterContrato.UsuarioModifica;
                            vSupervisorInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterContrato.FechaModifica;


                            vSupervisorInterContrato.SupervisorInterventor.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.PrimerNombre;
                            vSupervisorInterContrato.SupervisorInterventor.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.SegundoNombre;
                            vSupervisorInterContrato.SupervisorInterventor.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.PrimerApellido;
                            vSupervisorInterContrato.SupervisorInterventor.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.SegundoApellido;
                            vSupervisorInterContrato.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vSupervisorInterContrato.IdTipoPersona;
                            vSupervisorInterContrato.TipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vSupervisorInterContrato.TipoPersona;

                        }
                        return vSupervisorInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Supervisor Modificación

        /// <summary>
        /// Método de inserción para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int InsertarSupervisorTemporal(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorContratoTemporal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorAnterior", DbType.Int32, pSupervisorInterContrato.IDSupervisorAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pSupervisorInterContrato.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@Inactivo", DbType.Boolean, pSupervisorInterContrato.Inactivo);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pSupervisorInterContrato.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, pSupervisorInterContrato.TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDTipoSuperInter", DbType.Int32, pSupervisorInterContrato.IDTipoSuperInter);
                    if (pSupervisorInterContrato.IdNumeroContratoInterventoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdNumeroContratoInterventoria", DbType.Int32, pSupervisorInterContrato.IdNumeroContratoInterventoria);
                    if (pSupervisorInterContrato.IDProveedoresInterventor != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDProveedoresInterventor", DbType.Int32, pSupervisorInterContrato.IDProveedoresInterventor);
                    if (pSupervisorInterContrato.IDEmpleadosSupervisor != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEmpleadosSupervisor", DbType.Int32, pSupervisorInterContrato.IDEmpleadosSupervisor);
                    if (pSupervisorInterContrato.IdRol != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRol", DbType.Int32, pSupervisorInterContrato.IdRol);
                    // if (pSupervisorInterContrato.IdContrato != null)
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pSupervisorInterContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSupervisorInterContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSupervisorInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDSupervisorIntervContrato").ToString());
                    GenerarLogAuditoria(pSupervisorInterContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int EliminarSupervisorTemporal(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContratoTemporal_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pSupervisorInterContrato.IDSupervisorIntervContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSupervisorInterContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void GenerarSupervisoresTemporales(int idInterventor)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        /// <returns></returns>
        public SupervisorInterContrato ConsultarSupervisorTemporalContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContratoTemporal_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDSupervisorIntervContrato", DbType.Int32, pIDSupervisorIntervContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                        while (vDataReaderResults.Read())
                        {
                            vSupervisorInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vSupervisorInterContrato.IDSupervisorIntervContrato;
                            vSupervisorInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorAnterior"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorAnterior"].ToString()) : vSupervisorInterContrato.IDSupervisorAnterior;
                            vSupervisorInterContrato.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSupervisorInterContrato.FechaInicio;
                            vSupervisorInterContrato.Inactivo = vDataReaderResults["Inactivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Inactivo"].ToString()) : vSupervisorInterContrato.Inactivo;
                            vSupervisorInterContrato.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : vSupervisorInterContrato.Identificacion;
                            vSupervisorInterContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisorInterContrato.TipoIdentificacion;
                            vSupervisorInterContrato.IDTipoSuperInter = vDataReaderResults["IDTipoSuperInter"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSuperInter"].ToString()) : vSupervisorInterContrato.IDTipoSuperInter;
                            vSupervisorInterContrato.IdNumeroContratoInterventoria = vDataReaderResults["IdNumeroContratoInterventoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroContratoInterventoria"].ToString()) : vSupervisorInterContrato.IdNumeroContratoInterventoria;
                            vSupervisorInterContrato.IDProveedoresInterventor = vDataReaderResults["IDProveedoresInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProveedoresInterventor"].ToString()) : vSupervisorInterContrato.IDProveedoresInterventor;
                            vSupervisorInterContrato.IDEmpleadosSupervisor = vDataReaderResults["IDEmpleadosSupervisor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEmpleadosSupervisor"].ToString()) : vSupervisorInterContrato.IDEmpleadosSupervisor;
                            vSupervisorInterContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSupervisorInterContrato.IdContrato;
                            vSupervisorInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterContrato.UsuarioCrea;
                            vSupervisorInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterContrato.FechaCrea;
                            vSupervisorInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterContrato.UsuarioModifica;
                            vSupervisorInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterContrato.FechaModifica;
                        }
                        return vSupervisorInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información relacionada con los supervisores/interventores asociados a un contrato
        /// dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public List<SupervisorInterContrato> ObtenerSupervisoresTemporalesContrato(int pIdContrato, int ? idSupervisorAnterior)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContratoTemporal_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (idSupervisorAnterior.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IDSupervisorAnterior", DbType.Int32, idSupervisorAnterior.Value);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisorInterContrato> vListaSupervisorInterContrato = new List<SupervisorInterContrato>();
                        while (vDataReaderResults.Read())
                        {
                            string identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : string.Empty;

                            if (!vListaSupervisorInterContrato.Any(e => e.Identificacion == identificacion))
                            {
                                SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                                vSupervisorInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vSupervisorInterContrato.IDSupervisorIntervContrato;
                                vSupervisorInterContrato.EtQSupervisorInterventor = vDataReaderResults["EtQSupervisorInterventor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQSupervisorInterventor"].ToString()) : vSupervisorInterContrato.EtQSupervisorInterventor;
                                vSupervisorInterContrato.EtQInternoExterno = vDataReaderResults["EtQInternoExterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQInternoExterno"].ToString()) : vSupervisorInterContrato.EtQInternoExterno;
                                vSupervisorInterContrato.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSupervisorInterContrato.FechaInicio;
                                vSupervisorInterContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisorInterContrato.TipoIdentificacion;
                                vSupervisorInterContrato.Identificacion = identificacion;
                                vSupervisorInterContrato.NombreCompletoSuperInterventor = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.NombreCompleto;
                                vSupervisorInterContrato.SupervisorInterventor.TipoVinculacionContractual = vDataReaderResults["TipoVinculacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacionContractual"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.TipoVinculacionContractual;
                                vSupervisorInterContrato.SupervisorInterventor.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.NombreCompleto;
                                vSupervisorInterContrato.SupervisorInterventor.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Regional;
                                vSupervisorInterContrato.SupervisorInterventor.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Dependencia;
                                vSupervisorInterContrato.SupervisorInterventor.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Cargo;
                                vSupervisorInterContrato.SupervisorInterventor.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Direccion;
                                vSupervisorInterContrato.SupervisorInterventor.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Telefono;
                                vSupervisorInterContrato.SupervisorInterventor.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Celular;
                                vSupervisorInterContrato.SupervisorInterventor.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.CorreoElectronico;
                                vSupervisorInterContrato.NumeroContratoInterventoria = vDataReaderResults["IdNumeroContratoInterventoria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdNumeroContratoInterventoria"].ToString()) : vSupervisorInterContrato.NumeroContratoInterventoria;
                                vSupervisorInterContrato.IDTipoSuperInter = vDataReaderResults["IDTipoSuperInter"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSuperInter"].ToString()) : vSupervisorInterContrato.IDTipoSuperInter;
                                vSupervisorInterContrato.IDProveedoresInterventor = vDataReaderResults["IDProveedoresInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProveedoresInterventor"].ToString()) : vSupervisorInterContrato.IDProveedoresInterventor;
                                vSupervisorInterContrato.IDEmpleadosSupervisor = vDataReaderResults["IDEmpleadosSupervisor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEmpleadosSupervisor"].ToString()) : vSupervisorInterContrato.IDEmpleadosSupervisor;
                                vSupervisorInterContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSupervisorInterContrato.IdContrato;
                                vSupervisorInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterContrato.UsuarioCrea;
                                vSupervisorInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterContrato.FechaCrea;
                                vSupervisorInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterContrato.UsuarioModifica;
                                vSupervisorInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterContrato.FechaModifica;
                                vSupervisorInterContrato.NombreRol = vDataReaderResults["NombreRol"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRol"].ToString()) : vSupervisorInterContrato.NombreRol;
                                vListaSupervisorInterContrato.Add(vSupervisorInterContrato);
                            }
                        }
                        return vListaSupervisorInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información relacionada con los supervisores/interventores asociados a un contrato
        /// dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public List<SupervisorInterContrato> ObtenerSupervisoresHistoricoContrato(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_SupervisorInterContratoHistorico_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SupervisorInterContrato> vListaSupervisorInterContrato = new List<SupervisorInterContrato>();
                        while (vDataReaderResults.Read())
                        {
                            string identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : string.Empty;

                            if (!vListaSupervisorInterContrato.Any(e => e.Identificacion == identificacion))
                            {
                                SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                                vSupervisorInterContrato.IDSupervisorIntervContrato = vDataReaderResults["IDSupervisorIntervContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDSupervisorIntervContrato"].ToString()) : vSupervisorInterContrato.IDSupervisorIntervContrato;
                                vSupervisorInterContrato.EtQSupervisorInterventor = vDataReaderResults["EtQSupervisorInterventor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQSupervisorInterventor"].ToString()) : vSupervisorInterContrato.EtQSupervisorInterventor;
                                vSupervisorInterContrato.EtQInternoExterno = vDataReaderResults["EtQInternoExterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EtQInternoExterno"].ToString()) : vSupervisorInterContrato.EtQInternoExterno;
                                vSupervisorInterContrato.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vSupervisorInterContrato.FechaInicio;
                                vSupervisorInterContrato.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vSupervisorInterContrato.FechaFin;
                                vSupervisorInterContrato.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vSupervisorInterContrato.TipoIdentificacion;
                                vSupervisorInterContrato.Identificacion = identificacion;
                                vSupervisorInterContrato.NombreCompletoSuperInterventor = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.NombreCompleto;
                                vSupervisorInterContrato.SupervisorInterventor.TipoVinculacionContractual = vDataReaderResults["TipoVinculacionContractual"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoVinculacionContractual"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.TipoVinculacionContractual;
                                vSupervisorInterContrato.SupervisorInterventor.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.NombreCompleto;
                                vSupervisorInterContrato.SupervisorInterventor.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Regional;
                                vSupervisorInterContrato.SupervisorInterventor.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Dependencia;
                                vSupervisorInterContrato.SupervisorInterventor.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Cargo;
                                vSupervisorInterContrato.SupervisorInterventor.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Direccion;
                                vSupervisorInterContrato.SupervisorInterventor.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Telefono;
                                vSupervisorInterContrato.SupervisorInterventor.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.Celular;
                                vSupervisorInterContrato.SupervisorInterventor.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vSupervisorInterContrato.SupervisorInterventor.CorreoElectronico;
                                vSupervisorInterContrato.NumeroContratoInterventoria = vDataReaderResults["IdNumeroContratoInterventoria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdNumeroContratoInterventoria"].ToString()) : vSupervisorInterContrato.NumeroContratoInterventoria;
                                vSupervisorInterContrato.IDTipoSuperInter = vDataReaderResults["IDTipoSuperInter"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTipoSuperInter"].ToString()) : vSupervisorInterContrato.IDTipoSuperInter;
                                vSupervisorInterContrato.IDProveedoresInterventor = vDataReaderResults["IDProveedoresInterventor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProveedoresInterventor"].ToString()) : vSupervisorInterContrato.IDProveedoresInterventor;
                                vSupervisorInterContrato.IDEmpleadosSupervisor = vDataReaderResults["IDEmpleadosSupervisor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEmpleadosSupervisor"].ToString()) : vSupervisorInterContrato.IDEmpleadosSupervisor;
                                vSupervisorInterContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vSupervisorInterContrato.IdContrato;
                                vSupervisorInterContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSupervisorInterContrato.UsuarioCrea;
                                vSupervisorInterContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSupervisorInterContrato.FechaCrea;
                                vSupervisorInterContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSupervisorInterContrato.UsuarioModifica;
                                vSupervisorInterContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSupervisorInterContrato.FechaModifica;
                                vListaSupervisorInterContrato.Add(vSupervisorInterContrato);
                            }
                        }
                        return vListaSupervisorInterContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}

