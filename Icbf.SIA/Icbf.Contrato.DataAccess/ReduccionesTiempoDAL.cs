﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    public class ReduccionesTiempoDAL : GeneralDAL
    {
        public ReduccionesTiempoDAL()
        {
        }

        public int InsertarReduccionesTiempo(ReduccionesTiempo vReduccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ReduccionesTiempo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdReduccionTiempo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@FechaReduccion", DbType.DateTime, vReduccion.FechaReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@Dias", DbType.Int32, vReduccion.Dias);
                    vDataBase.AddInParameter(vDbCommand, "@Meses", DbType.Int32, vReduccion.Meses);
                    vDataBase.AddInParameter(vDbCommand, "@Años", DbType.Int32, vReduccion.Anios);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, vReduccion.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdDetalleConsModContractual", DbType.Int32, vReduccion.IdDetalleConsModContractual);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vReduccion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vReduccion.IdReduccionTiempo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdReduccionTiempo").ToString());
                    GenerarLogAuditoria(vReduccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarReduccionesTiempo(ReduccionesTiempo vReduccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ReduccionesTiempo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccionTiempo", DbType.Int32, vReduccion.IdReduccionTiempo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaReduccion", DbType.DateTime, vReduccion.FechaReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@Dias", DbType.Int32, vReduccion.Dias);
                    vDataBase.AddInParameter(vDbCommand, "@Meses", DbType.Int32, vReduccion.Meses);
                    vDataBase.AddInParameter(vDbCommand, "@Años", DbType.Int32, vReduccion.Anios);
                    vDataBase.AddInParameter(vDbCommand, "@Justificacion", DbType.String, vReduccion.Justificacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, vReduccion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(vReduccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarReduccionesTiempo(ReduccionesTiempo vReduccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ReduccionesTiempo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccionTiempo", DbType.Int32, vReduccion.IdReduccionTiempo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(vReduccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ReduccionesTiempo ConsultarReduccionTiempo(int vIdReduccionTiempo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ReduccionTiempo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccionTiempo", DbType.Int32, vIdReduccionTiempo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ReduccionesTiempo vReduccion = new ReduccionesTiempo();
                        while (vDataReaderResults.Read())
                        {
                            vReduccion.IdReduccionTiempo = vDataReaderResults["IdReduccionTiempo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccionTiempo"].ToString()) : vReduccion.IdReduccionTiempo;
                            vReduccion.FechaReduccion = vDataReaderResults["FechaReduccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReduccion"].ToString()) : vReduccion.FechaReduccion;
                            vReduccion.Dias = vDataReaderResults["Dias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Dias"].ToString()) : vReduccion.Dias;
                            vReduccion.Meses = vDataReaderResults["Meses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Meses"].ToString()) : vReduccion.Meses;
                            vReduccion.Anios = vDataReaderResults["Años"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Años"].ToString()) : vReduccion.Anios;
                            vReduccion.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vReduccion.Justificacion;
                            vReduccion.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vReduccion.IdDetalleConsModContractual;
                            vReduccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReduccion.UsuarioCrea;
                            vReduccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReduccion.FechaCrea;
                            vReduccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReduccion.UsuarioModifica;
                            vReduccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReduccion.FechaModifica;
                        }
                        return vReduccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempo(DateTime? pFechaReduccion,int? pIdDetalleConsModContractual)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ReduccionesTiempo_Consultar"))
                {
                    if (pFechaReduccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFechaReduccion);              
                    if (pIdDetalleConsModContractual != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDetalleConsModContractual", DbType.Int32, pIdDetalleConsModContractual);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ReduccionesTiempo> vListaReduccionesTiempo = new List<ReduccionesTiempo>();
                        while (vDataReaderResults.Read())
                        {
                            ReduccionesTiempo vReduccion = new ReduccionesTiempo();
                            vReduccion.IdReduccionTiempo = vDataReaderResults["IdReduccionTiempo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccionTiempo"].ToString()) : vReduccion.IdReduccionTiempo;
                            vReduccion.FechaReduccion = vDataReaderResults["FechaReduccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReduccion"].ToString()) : vReduccion.FechaReduccion;
                            vReduccion.Dias = vDataReaderResults["Dias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Dias"].ToString()) : vReduccion.Dias;
                            vReduccion.Meses = vDataReaderResults["Meses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Meses"].ToString()) : vReduccion.Meses;
                            vReduccion.Anios = vDataReaderResults["Años"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Años"].ToString()) : vReduccion.Anios;
                            vReduccion.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vReduccion.Justificacion;
                            vReduccion.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vReduccion.IdDetalleConsModContractual;
                            vReduccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReduccion.UsuarioCrea;
                            vReduccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReduccion.FechaCrea;
                            vReduccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReduccion.UsuarioModifica;
                            vReduccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReduccion.FechaModifica;
                            vListaReduccionesTiempo.Add(vReduccion);
                        }
                        return vListaReduccionesTiempo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempoContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ReduccionesTiempo_ConsultarInformacionReduccion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idContrato", DbType.Int32, idContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ReduccionesTiempo> vListaReducciones = new List<ReduccionesTiempo>();
                        while (vDataReaderResults.Read())
                        {
                            ReduccionesTiempo vReduccion = new ReduccionesTiempo();
                            vReduccion.IdReduccionTiempo = vDataReaderResults["IdReduccionTiempo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccionTiempo"].ToString()) : vReduccion.IdReduccionTiempo;
                            vReduccion.FechaReduccion = vDataReaderResults["FechaReduccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReduccion"].ToString()) : vReduccion.FechaReduccion;
                            vReduccion.Dias = vDataReaderResults["Dias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Dias"].ToString()) : vReduccion.Dias;
                            vReduccion.Meses = vDataReaderResults["Meses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Meses"].ToString()) : vReduccion.Meses;
                            vReduccion.Anios = vDataReaderResults["Años"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Años"].ToString()) : vReduccion.Anios;
                            vReduccion.Justificacion = vDataReaderResults["Justificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Justificacion"].ToString()) : vReduccion.Justificacion;
                            vReduccion.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vReduccion.IdDetalleConsModContractual;
                            vReduccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReduccion.UsuarioCrea;
                            vReduccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReduccion.FechaCrea;
                            vReduccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReduccion.UsuarioModifica;
                            vReduccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReduccion.FechaModifica;
                            vListaReducciones.Add(vReduccion);
                        }
                        return vListaReducciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempoAprobadasContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_ReduccionesTiempo_ConsultarAprobadasContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idContrato", DbType.Int32, idContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ReduccionesTiempo> vListaReducciones = new List<ReduccionesTiempo>();
                        while (vDataReaderResults.Read())
                        {
                            ReduccionesTiempo vReduccion = new ReduccionesTiempo();
                            vReduccion.IdReduccionTiempo = vDataReaderResults["IdReduccionTiempo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccionTiempo"].ToString()) : vReduccion.IdReduccionTiempo;
                            vReduccion.FechaReduccion = vDataReaderResults["FechaReduccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaReduccion"].ToString()) : vReduccion.FechaReduccion;                            
                            vReduccion.IdDetalleConsModContractual = vDataReaderResults["IdDetalleConsModContractual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDetalleConsModContractual"].ToString()) : vReduccion.IdDetalleConsModContractual;
                            vReduccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReduccion.UsuarioCrea;
                            vReduccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReduccion.FechaCrea;
                            vReduccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReduccion.UsuarioModifica;
                            vReduccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReduccion.FechaModifica;
                            vListaReducciones.Add(vReduccion);
                        }
                        return vListaReducciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        
    }
}
