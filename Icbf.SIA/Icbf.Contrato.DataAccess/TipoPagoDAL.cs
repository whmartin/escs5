﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    public class TipoPagoDAL : GeneralDAL
    {
        public TipoPagoDAL()
        {
        }
        /// <summary>
        /// 
        /// Método de inserción para la entidad TipoPago
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago</param>
        public int InsertarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoPago_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoPago", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoPago", DbType.String, pTipoPago.NombreTipoPago);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoPago.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoPago.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoPago.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoPago.IdTipoPago = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoPago").ToString());
                    GenerarLogAuditoria(pTipoPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoPago
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pTipoPago"></param>
        public int ModificarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoPago_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPago", DbType.Int32, pTipoPago.IdTipoPago);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoPago", DbType.String, pTipoPago.NombreTipoPago);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoPago.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoPago.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoPago.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// Método de modificación para la entidad TipoPago
        /// Fecha: 24/08/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago</param>
        public int EliminarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contrato_CONTRATO_TipoPago_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPago", DbType.Int32, pTipoPago.IdTipoPago);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoPago, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TipoPago ConsultarTipoPago(int cIdTipoPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoPago_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "IdTipoPago", DbType.Int32, cIdTipoPago);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoPago vTipoPago = new TipoPago();
                        while (vDataReaderResults.Read())
                        {
                            vTipoPago.IdTipoPago = vDataReaderResults["IdTipoPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPago"].ToString()) : vTipoPago.IdTipoPago;
                            vTipoPago.NombreTipoPago = vDataReaderResults["NombreTipoPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPago"].ToString()) : vTipoPago.NombreTipoPago;
                            vTipoPago.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoPago.Descripcion;
                            vTipoPago.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoPago.Estado;
                            vTipoPago.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoPago.UsuarioCrea;
                            vTipoPago.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoPago.FechaCrea;
                            vTipoPago.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoPago.UsuarioModifica;
                            vTipoPago.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoPago.FechaModifica;
                        }
                        return vTipoPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoPago> ConsultarVariosTipoPago(Boolean? cEstado, String cNombreTipoPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_TipoPago_ConsultarVarios"))
                {

                    if (cEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, cEstado);
                    if (!string.IsNullOrEmpty(cNombreTipoPago))
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoPago", DbType.String, cNombreTipoPago);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoPago> vListaTipoPago = new List<TipoPago>();
                        while (vDataReaderResults.Read())
                        {
                            TipoPago vTipoPago = new TipoPago();
                            vTipoPago.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoPago.Estado;
                            vTipoPago.NombreTipoPago = vDataReaderResults["NombreTipoPago"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPago"].ToString()) : vTipoPago.NombreTipoPago;
                            vTipoPago.IdTipoPago = vDataReaderResults["IdTipoPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPago"].ToString()) : vTipoPago.IdTipoPago;
                            vListaTipoPago.Add(vTipoPago);
                        }
                        return vListaTipoPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
