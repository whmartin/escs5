using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad AporteContrato
    /// </summary>
    public class AporteContratoDAL : GeneralDAL
    {
        public AporteContratoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int InsertarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAporteContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@AportanteICBF", DbType.Boolean, pAporteContrato.AportanteICBF);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionICBF", DbType.String, pAporteContrato.NumeroIdentificacionICBF);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Double, pAporteContrato.ValorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionAporte", DbType.String, pAporteContrato.DescripcionAporte);
                    vDataBase.AddInParameter(vDbCommand, "@AporteEnDinero", DbType.Boolean, pAporteContrato.AporteEnDinero);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pAporteContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDEntidadProvOferente", DbType.Int32, pAporteContrato.IDEntidadProvOferente);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRP", DbType.DateTime, pAporteContrato.FechaRP);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.String, pAporteContrato.NumeroRP);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pAporteContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAporteContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAporteContrato.IdAporteContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAporteContrato").ToString());
                    GenerarLogAuditoria(pAporteContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int ModificarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAporteContrato", DbType.Int32, pAporteContrato.IdAporteContrato);
                    vDataBase.AddInParameter(vDbCommand, "@AportanteICBF", DbType.Boolean, pAporteContrato.AportanteICBF);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionICBF", DbType.String, pAporteContrato.NumeroIdentificacionICBF);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, pAporteContrato.ValorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionAporte", DbType.String, pAporteContrato.DescripcionAporte);
                    vDataBase.AddInParameter(vDbCommand, "@AporteEnDinero", DbType.Boolean, pAporteContrato.AporteEnDinero);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pAporteContrato.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IDEntidadProvOferente", DbType.Int32, pAporteContrato.IDEntidadProvOferente);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRP", DbType.DateTime, pAporteContrato.FechaRP);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.String, pAporteContrato.NumeroRP);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pAporteContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAporteContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAporteContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int EliminarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAporteContrato", DbType.Int32, pAporteContrato.IdAporteContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAporteContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// /
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public int EliminarAporteContratoValor(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ValorAporteContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public int EliminarAportesCofinanciacionContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContrato_EliminarCofinanciacion"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public int EliminarAportesEspecieICBFContrato(int idContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContrato_EliminarEspecieICBF"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, idContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pValorAporte"></param>
        /// <param name="usuarioModifica"></param>
        /// <returns></returns>
        public int ModificarAporteContratoValor(int pIdContrato, decimal pValorAporte, string usuarioModifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ValorAporteContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, pValorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@usuarioModifica", DbType.String, usuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AporteContrato
        /// </summary>
        /// <param name="pIdAporteContrato"></param>
        public AporteContrato ConsultarAporteContrato(int pIdAporteContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAporteContrato", DbType.Int32, pIdAporteContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AporteContrato vAporteContrato = new AporteContrato();
                        while (vDataReaderResults.Read())
                        {
                            vAporteContrato.IdAporteContrato = vDataReaderResults["IdAporteContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAporteContrato"].ToString()) : vAporteContrato.IdAporteContrato;
                            vAporteContrato.AportanteICBF = vDataReaderResults["AportanteICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportanteICBF"].ToString()) : vAporteContrato.AportanteICBF;
                            vAporteContrato.NumeroIdentificacionICBF = vDataReaderResults["NumeroIdentificacionICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionICBF"].ToString()) : vAporteContrato.NumeroIdentificacionICBF;
                            vAporteContrato.ValorAporte = vDataReaderResults["ValorAporte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporte"].ToString()) : vAporteContrato.ValorAporte;
                            vAporteContrato.DescripcionAporte = vDataReaderResults["DescripcionAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionAporte"].ToString()) : vAporteContrato.DescripcionAporte;
                            vAporteContrato.AporteEnDinero = vDataReaderResults["AporteEnDinero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteEnDinero"].ToString()) : vAporteContrato.AporteEnDinero;
                            vAporteContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAporteContrato.IdContrato;
                            vAporteContrato.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vAporteContrato.IDEntidadProvOferente;
                            vAporteContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAporteContrato.FechaRP;
                            vAporteContrato.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vAporteContrato.NumeroRP;
                            vAporteContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAporteContrato.Estado;
                            vAporteContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAporteContrato.UsuarioCrea;
                            vAporteContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAporteContrato.FechaCrea;
                            vAporteContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAporteContrato.UsuarioModifica;
                            vAporteContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAporteContrato.FechaModifica;
                        }
                        return vAporteContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AporteContrato
        /// </summary>
        /// <param name="pAportanteICBF"></param>
        /// <param name="pNumeroIdentificacionICBF"></param>
        /// <param name="pValorAporte"></param>
        /// <param name="pDescripcionAporte"></param>
        /// <param name="pAporteEnDinero"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        /// <param name="pFechaRP"></param>
        /// <param name="pNumeroRP"></param>
        /// <param name="pEstado"></param>
        public List<AporteContrato> ConsultarAporteContratos(Boolean? pAportanteICBF, String pNumeroIdentificacionICBF, Decimal? pValorAporte, String pDescripcionAporte, Boolean? pAporteEnDinero, int? pIdContrato, int? pIDEntidadProvOferente, DateTime? pFechaRP, String pNumeroRP, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContratos_Consultar"))
                {
                    if (pAportanteICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@AportanteICBF", DbType.Boolean, pAportanteICBF);
                    if (pNumeroIdentificacionICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionICBF", DbType.String, pNumeroIdentificacionICBF);
                    if (pValorAporte != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, pValorAporte);
                    if (pDescripcionAporte != null)
                        vDataBase.AddInParameter(vDbCommand, "@DescripcionAporte", DbType.String, pDescripcionAporte);
                    if (pAporteEnDinero != null)
                        vDataBase.AddInParameter(vDbCommand, "@AporteEnDinero", DbType.Boolean, pAporteEnDinero);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pIDEntidadProvOferente != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEntidadProvOferente", DbType.Int32, pIDEntidadProvOferente);
                    if (pFechaRP != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaRP", DbType.DateTime, pFechaRP);
                    if (pNumeroRP != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroRP", DbType.String, pNumeroRP);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AporteContrato> vListaAporteContrato = new List<AporteContrato>();
                        while (vDataReaderResults.Read())
                        {
                            AporteContrato vAporteContrato = new AporteContrato();
                            vAporteContrato.IdAporteContrato = vDataReaderResults["IdAporteContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAporteContrato"].ToString()) : vAporteContrato.IdAporteContrato;
                            vAporteContrato.AportanteICBF = vDataReaderResults["AportanteICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportanteICBF"].ToString()) : vAporteContrato.AportanteICBF;
                            vAporteContrato.NumeroIdentificacionICBF = vDataReaderResults["NumeroIdentificacionICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionICBF"].ToString()) : vAporteContrato.NumeroIdentificacionICBF;
                            vAporteContrato.ValorAporte = vDataReaderResults["ValorAporte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporte"].ToString()) : vAporteContrato.ValorAporte;
                            vAporteContrato.DescripcionAporte = vDataReaderResults["DescripcionAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionAporte"].ToString()) : vAporteContrato.DescripcionAporte;
                            vAporteContrato.AporteEnDinero = vDataReaderResults["AporteEnDinero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteEnDinero"].ToString()) : vAporteContrato.AporteEnDinero;
                            vAporteContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAporteContrato.IdContrato;
                            vAporteContrato.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vAporteContrato.IDEntidadProvOferente;
                            vAporteContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAporteContrato.FechaRP;
                            vAporteContrato.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vAporteContrato.NumeroRP;
                            vAporteContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAporteContrato.Estado;
                            vAporteContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAporteContrato.UsuarioCrea;
                            vAporteContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAporteContrato.FechaCrea;
                            vAporteContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAporteContrato.UsuarioModifica;
                            vAporteContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAporteContrato.FechaModifica;
                            vAporteContrato.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vAporteContrato.IdAdicion;
                            vListaAporteContrato.Add(vAporteContrato);
                        }
                        return vListaAporteContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de un contratista asociado a un tipo y valor de aporte
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public bool ExisteContratoAporte(int pIdContrato, Boolean pAporteEnDinero, Decimal pValorAporte, bool pAportanteICBF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_AporteContrato_ExisteContratoAporte"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@AporteEnDinero", DbType.Boolean, pAporteEnDinero);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, pValorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@AportanteICBF", DbType.Boolean, pAportanteICBF);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información de los aportes asociados a un contrato seleccionados a partir de los filtros de entrada
        /// (Página Registro Contratos)
        /// </summary>
        /// <param name="pAportanteICBF">Booleano que indica si el aporte es hecho por el ICBF o no</param>
        /// <param name="pIdContrato">Entero con el identificador del contrato al que se asocia el aporte</param>
        /// <param name="pIDEntidadProvOferente">Entero con el identificador del contratista que realiza el aporte</param>
        /// <param name="pEstado">Booleano que indica el estado del aporte</param>
        /// <returns>Listado de aportes obtenidos de la base de datos que coinciden con los filtros</returns>
        public List<AporteContrato> ObtenerAportesContrato(Boolean? pAportanteICBF, int? pIdContrato, int? pIDEntidadProvOferente, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AportesContrato_Obtener"))
                {
                    if (pAportanteICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@AportanteICBF", DbType.Boolean, pAportanteICBF);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pIDEntidadProvOferente != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDEntidadProvOferente", DbType.Int32, pIDEntidadProvOferente);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AporteContrato> vListaAporteContrato = new List<AporteContrato>();
                        while (vDataReaderResults.Read())
                        {
                            AporteContrato vAporteContrato = new AporteContrato();
                            vAporteContrato.IdAporteContrato = vDataReaderResults["IdAporteContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAporteContrato"].ToString()) : vAporteContrato.IdAporteContrato;
                            vAporteContrato.AportanteICBF = vDataReaderResults["AportanteICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportanteICBF"].ToString()) : vAporteContrato.AportanteICBF;
                            vAporteContrato.NumeroIdentificacionICBF = vDataReaderResults["NumeroIdentificacionICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionICBF"].ToString()) : vAporteContrato.NumeroIdentificacionICBF;
                            vAporteContrato.ValorAporte = vDataReaderResults["ValorAporte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporte"].ToString()) : vAporteContrato.ValorAporte;
                            vAporteContrato.DescripcionAporte = vDataReaderResults["DescripcionAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionAporte"].ToString()) : vAporteContrato.DescripcionAporte;
                            vAporteContrato.AporteEnDinero = vDataReaderResults["AporteEnDinero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteEnDinero"].ToString()) : vAporteContrato.AporteEnDinero;
                            vAporteContrato.TipoAporte = vDataReaderResults["TipoAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoAporte"].ToString()) : vAporteContrato.TipoAporte;
                            vAporteContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAporteContrato.IdContrato;
                            vAporteContrato.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vAporteContrato.IDEntidadProvOferente;
                            vAporteContrato.NumeroIdentificacionContratista = vDataReaderResults["NumeroIdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionContratista"].ToString()) : vAporteContrato.NumeroIdentificacionContratista;
                            vAporteContrato.InformacionAportante = vDataReaderResults["InformacionAportante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionAportante"].ToString()) : vAporteContrato.InformacionAportante;
                            vAporteContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAporteContrato.FechaRP;
                            vAporteContrato.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vAporteContrato.NumeroRP;
                            vAporteContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAporteContrato.Estado;
                            vAporteContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAporteContrato.UsuarioCrea;
                            vAporteContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAporteContrato.FechaCrea;
                            vAporteContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAporteContrato.UsuarioModifica;
                            vAporteContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAporteContrato.FechaModifica;
                            vAporteContrato.NombreTipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vAporteContrato.NombreTipoDocumento;
                            vAporteContrato.CodigoTipoDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vAporteContrato.CodigoTipoDocumento;
                            vAporteContrato.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vAporteContrato.IdTipoDocumento;
                            vAporteContrato.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vAporteContrato.IdAdicion;
                            vAporteContrato.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vAporteContrato.IdReduccion;
                            vAporteContrato.Esreduccion = vDataReaderResults["Esreduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Esreduccion"].ToString()) : vAporteContrato.Esreduccion;
                            vAporteContrato.EsAdicion = vDataReaderResults["Esadicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Esadicion"].ToString()) : vAporteContrato.EsAdicion;

                            vListaAporteContrato.Add(vAporteContrato);
                        }
                        return vListaAporteContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<AporteContrato> ObtenerAportesContrato(int pIdContrato, bool adiciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AportesContrato_ObtenerTodo"))
                {
                   vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                   vDataBase.AddInParameter(vDbCommand, "@Esadicion", DbType.Boolean, adiciones);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AporteContrato> vListaAporteContrato = new List<AporteContrato>();
                        while (vDataReaderResults.Read())
                        {
                            AporteContrato vAporteContrato = new AporteContrato();
                            vAporteContrato.IdAporteContrato = vDataReaderResults["IdAporteContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAporteContrato"].ToString()) : vAporteContrato.IdAporteContrato;
                            vAporteContrato.AportanteICBF = vDataReaderResults["AportanteICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportanteICBF"].ToString()) : vAporteContrato.AportanteICBF;
                            vAporteContrato.NumeroIdentificacionICBF = vDataReaderResults["NumeroIdentificacionICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionICBF"].ToString()) : vAporteContrato.NumeroIdentificacionICBF;
                            vAporteContrato.ValorAporte = vDataReaderResults["ValorAporte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporte"].ToString()) : vAporteContrato.ValorAporte;
                            vAporteContrato.DescripcionAporte = vDataReaderResults["DescripcionAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionAporte"].ToString()) : vAporteContrato.DescripcionAporte;
                            vAporteContrato.AporteEnDinero = vDataReaderResults["AporteEnDinero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteEnDinero"].ToString()) : vAporteContrato.AporteEnDinero;
                            vAporteContrato.TipoAporte = vDataReaderResults["TipoAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoAporte"].ToString()) : vAporteContrato.TipoAporte;
                            vAporteContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAporteContrato.IdContrato;
                            vAporteContrato.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vAporteContrato.IDEntidadProvOferente;
                            vAporteContrato.NumeroIdentificacionContratista = vDataReaderResults["NumeroIdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionContratista"].ToString()) : vAporteContrato.NumeroIdentificacionContratista;
                            vAporteContrato.InformacionAportante = vDataReaderResults["InformacionAportante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionAportante"].ToString()) : vAporteContrato.InformacionAportante;
                            vAporteContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAporteContrato.FechaRP;
                            vAporteContrato.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vAporteContrato.NumeroRP;
                            vAporteContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAporteContrato.Estado;
                            vAporteContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAporteContrato.UsuarioCrea;
                            vAporteContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAporteContrato.FechaCrea;
                            vAporteContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAporteContrato.UsuarioModifica;
                            vAporteContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAporteContrato.FechaModifica;
                            vAporteContrato.NombreTipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vAporteContrato.NombreTipoDocumento;
                            vAporteContrato.CodigoTipoDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vAporteContrato.CodigoTipoDocumento;
                            vAporteContrato.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vAporteContrato.IdTipoDocumento;
                            vAporteContrato.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vAporteContrato.IdAdicion;
                            vAporteContrato.EsAdicion = vDataReaderResults["EsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsAdicion"].ToString()) : vAporteContrato.EsAdicion;

                            vListaAporteContrato.Add(vAporteContrato);
                        }
                        return vListaAporteContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<AporteContrato> ObtenerAportesContratoActuales(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AportesContrato_ObtenerActuales"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AporteContrato> vListaAporteContrato = new List<AporteContrato>();
                        while (vDataReaderResults.Read())
                        {
                            AporteContrato vAporteContrato = new AporteContrato();
                            vAporteContrato.IdAporteContrato = vDataReaderResults["IdAporteContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAporteContrato"].ToString()) : vAporteContrato.IdAporteContrato;
                            vAporteContrato.AportanteICBF = vDataReaderResults["AportanteICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportanteICBF"].ToString()) : vAporteContrato.AportanteICBF;
                            vAporteContrato.NumeroIdentificacionICBF = vDataReaderResults["NumeroIdentificacionICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionICBF"].ToString()) : vAporteContrato.NumeroIdentificacionICBF;
                            vAporteContrato.ValorAporte = vDataReaderResults["ValorAporte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporte"].ToString()) : vAporteContrato.ValorAporte;
                            vAporteContrato.DescripcionAporte = vDataReaderResults["DescripcionAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionAporte"].ToString()) : vAporteContrato.DescripcionAporte;
                            vAporteContrato.AporteEnDinero = vDataReaderResults["AporteEnDinero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteEnDinero"].ToString()) : vAporteContrato.AporteEnDinero;
                            vAporteContrato.TipoAporte = vDataReaderResults["TipoAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoAporte"].ToString()) : vAporteContrato.TipoAporte;
                            vAporteContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAporteContrato.IdContrato;
                            vAporteContrato.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vAporteContrato.IDEntidadProvOferente;
                            vAporteContrato.NumeroIdentificacionContratista = vDataReaderResults["NumeroIdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionContratista"].ToString()) : vAporteContrato.NumeroIdentificacionContratista;
                            vAporteContrato.InformacionAportante = vDataReaderResults["InformacionAportante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionAportante"].ToString()) : vAporteContrato.InformacionAportante;
                            vAporteContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAporteContrato.FechaRP;
                            vAporteContrato.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vAporteContrato.NumeroRP;
                            vAporteContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAporteContrato.Estado;
                            vAporteContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAporteContrato.UsuarioCrea;
                            vAporteContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAporteContrato.FechaCrea;
                            vAporteContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAporteContrato.UsuarioModifica;
                            vAporteContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAporteContrato.FechaModifica;
                            vAporteContrato.NombreTipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vAporteContrato.NombreTipoDocumento;
                            vAporteContrato.CodigoTipoDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vAporteContrato.CodigoTipoDocumento;
                            vAporteContrato.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vAporteContrato.IdTipoDocumento;
                            vAporteContrato.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vAporteContrato.IdAdicion;
                            vAporteContrato.EsAdicion = vDataReaderResults["EsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsAdicion"].ToString()) : vAporteContrato.EsAdicion;
                            vAporteContrato.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vAporteContrato.IdReduccion;
                            vAporteContrato.Esreduccion = vDataReaderResults["Esreduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Esreduccion"].ToString()) : vAporteContrato.Esreduccion;

                            vListaAporteContrato.Add(vAporteContrato);
                        }
                        return vListaAporteContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<AporteContrato> ObtenerCofinanciadores(int pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contratos_Confinanciadores_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AporteContrato> vListaAporteContrato = new List<AporteContrato>();
                        while (vDataReaderResults.Read())
                        {
                            AporteContrato vAporteContrato = new AporteContrato();
                            vAporteContrato.ValorAporte = vDataReaderResults["ValorAporteDinero"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporteDinero"].ToString()) : vAporteContrato.ValorAporte;
                            vAporteContrato.ValorAporteE = vDataReaderResults["ValorAporteEspecie"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporteEspecie"].ToString()) : vAporteContrato.ValorAporteE;
                            vAporteContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAporteContrato.IdContrato;
                            vAporteContrato.NumeroIdentificacionContratista = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vAporteContrato.NumeroIdentificacionContratista;
                            vAporteContrato.InformacionAportante = vDataReaderResults["razonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["razonSocial"].ToString()) : vAporteContrato.InformacionAportante;
                            
                            vListaAporteContrato.Add(vAporteContrato);
                        }
                        return vListaAporteContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pidAdicion"></param>
        /// <returns></returns>
        public List<AporteContrato> ObtenerAportesContratoAdicionReduccion(int pidModificacion, bool esAdicion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AportesContrato_ObtenerAdicionesReduccion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModificacion", DbType.Int32, pidModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@EsAdicion", DbType.Boolean, esAdicion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AporteContrato> vListaAporteContrato = new List<AporteContrato>();
                        while (vDataReaderResults.Read())
                        {
                            AporteContrato vAporteContrato = new AporteContrato();
                            vAporteContrato.IdAporteContrato = vDataReaderResults["IdAporteContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAporteContrato"].ToString()) : vAporteContrato.IdAporteContrato;
                            vAporteContrato.AportanteICBF = vDataReaderResults["AportanteICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportanteICBF"].ToString()) : vAporteContrato.AportanteICBF;
                            vAporteContrato.NumeroIdentificacionICBF = vDataReaderResults["NumeroIdentificacionICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionICBF"].ToString()) : vAporteContrato.NumeroIdentificacionICBF;
                            vAporteContrato.ValorAporte = vDataReaderResults["ValorAporte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporte"].ToString()) : vAporteContrato.ValorAporte;
                            vAporteContrato.DescripcionAporte = vDataReaderResults["DescripcionAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionAporte"].ToString()) : vAporteContrato.DescripcionAporte;
                            vAporteContrato.AporteEnDinero = vDataReaderResults["AporteEnDinero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteEnDinero"].ToString()) : vAporteContrato.AporteEnDinero;
                            vAporteContrato.TipoAporte = vDataReaderResults["TipoAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoAporte"].ToString()) : vAporteContrato.TipoAporte;
                            vAporteContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAporteContrato.IdContrato;
                            vAporteContrato.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vAporteContrato.IDEntidadProvOferente;
                            vAporteContrato.NumeroIdentificacionContratista = vDataReaderResults["NumeroIdentificacionContratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionContratista"].ToString()) : vAporteContrato.NumeroIdentificacionContratista;
                            vAporteContrato.InformacionAportante = vDataReaderResults["InformacionAportante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["InformacionAportante"].ToString()) : vAporteContrato.InformacionAportante;
                            vAporteContrato.FechaRP = vDataReaderResults["FechaRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRP"].ToString()) : vAporteContrato.FechaRP;
                            vAporteContrato.NumeroRP = vDataReaderResults["NumeroRP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRP"].ToString()) : vAporteContrato.NumeroRP;
                            vAporteContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAporteContrato.Estado;
                            vAporteContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAporteContrato.UsuarioCrea;
                            vAporteContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAporteContrato.FechaCrea;
                            vAporteContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAporteContrato.UsuarioModifica;
                            vAporteContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAporteContrato.FechaModifica;
                            vAporteContrato.NombreTipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vAporteContrato.NombreTipoDocumento;
                            vAporteContrato.CodigoTipoDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vAporteContrato.CodigoTipoDocumento;
                            vAporteContrato.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vAporteContrato.IdTipoDocumento;
                            vAporteContrato.IdAdicion = vDataReaderResults["IdAdicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicion"].ToString()) : vAporteContrato.IdAdicion;
                            vAporteContrato.EsAdicion = vDataReaderResults["EsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsAdicion"].ToString()) : vAporteContrato.EsAdicion;
                            vAporteContrato.IdReduccion = vDataReaderResults["IdReduccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReduccion"].ToString()) : vAporteContrato.IdReduccion;
                            vAporteContrato.Esreduccion = vDataReaderResults["EsReduccion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsReduccion"].ToString()) : vAporteContrato.Esreduccion;

                            vListaAporteContrato.Add(vAporteContrato);
                        }
                        return vListaAporteContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CrearAportesAdicion(int iddetCons, int idAdicion, int idAportePadre, decimal valorAporte, string usuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteAdicion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDetConsModContractual", DbType.Int32, iddetCons);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, idAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, valorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@idAportePadre", DbType.Int32, idAportePadre);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int actualizarAportesAdicion(int idAdicion, decimal valorAporte, int idAportePadre, string usuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteAdicion_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicion", DbType.Int32, idAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, valorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@idAportePadre", DbType.Int32, idAportePadre);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public KeyValuePair<int,int> CrearAporteDineroAdicion(int idDetCons, decimal valorAporte, string usuario, bool esAdicion, int idAporte, int idPlanCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteDineroModificacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDetConsModContractual", DbType.Int32, idDetCons);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, valorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vDataBase.AddInParameter(vDbCommand, "@Esadicion", DbType.Boolean, esAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAporte", DbType.Int32, idAporte);
                    vDataBase.AddOutParameter(vDbCommand,"@IdModificacion",DbType.Int32,12);
                    vDataBase.AddInParameter(vDbCommand, "@idPlanCompras", DbType.Int32, idPlanCompras);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    int idModificacion = int.Parse(vDataBase.GetParameterValue(vDbCommand,"@IdModificacion").ToString());
                    return new KeyValuePair<int, int>(vResultado, idModificacion); ;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarAporteDineroAdicion(int idModificacion, decimal valorAporte, string usuario, bool esAdicion, int idPlanCompras, int idAporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteDineroModificacion_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModificacion", DbType.Int32, idModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, valorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vDataBase.AddInParameter(vDbCommand, "@Esadicion", DbType.Boolean, esAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@idPlanCompras", DbType.Int32, idPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@IdAporte", DbType.Int32, idAporte);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public AporteContrato ObtenerAporteModificacion(bool esAdicion, int idModificacion, int idPlanCompras)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContrato_ConsultarModificacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@esAdicion", DbType.Boolean, esAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@idModificacion", DbType.Int32, idModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@idPlanCompras", DbType.Int32, idPlanCompras);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AporteContrato vAporteContrato = new AporteContrato();
                        while (vDataReaderResults.Read())
                        {
                            vAporteContrato.IdAporteContrato = vDataReaderResults["IdAporteContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAporteContrato"].ToString()) : vAporteContrato.IdAporteContrato;
                            vAporteContrato.AportanteICBF = vDataReaderResults["AportanteICBF"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AportanteICBF"].ToString()) : vAporteContrato.AportanteICBF;
                            vAporteContrato.NumeroIdentificacionICBF = vDataReaderResults["NumeroIdentificacionICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionICBF"].ToString()) : vAporteContrato.NumeroIdentificacionICBF;
                            vAporteContrato.ValorAporte = vDataReaderResults["ValorAporte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAporte"].ToString()) : vAporteContrato.ValorAporte;
                            vAporteContrato.DescripcionAporte = vDataReaderResults["DescripcionAporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionAporte"].ToString()) : vAporteContrato.DescripcionAporte;
                            vAporteContrato.AporteEnDinero = vDataReaderResults["AporteEnDinero"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AporteEnDinero"].ToString()) : vAporteContrato.AporteEnDinero;
                            vAporteContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAporteContrato.IdContrato;
                            vAporteContrato.IDEntidadProvOferente = vDataReaderResults["IDEntidadProvOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEntidadProvOferente"].ToString()) : vAporteContrato.IDEntidadProvOferente;
                            vAporteContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAporteContrato.Estado;
                            vAporteContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAporteContrato.UsuarioCrea;
                            vAporteContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAporteContrato.FechaCrea;
                            vAporteContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAporteContrato.UsuarioModifica;
                            vAporteContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAporteContrato.FechaModifica;
                        }
                        return vAporteContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ObtenerValorTotalPlanComprasContrato(int idPlanComprasContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteContrato_ObtenerValorPlanComprasContrato"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@ValorPlanCompras", DbType.Decimal, 32);
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanCompras", DbType.Int32, idPlanComprasContrato);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    return Convert.ToDecimal(vDataBase.GetParameterValue(vDbCommand, "@ValorPlanCompras").ToString());
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarAportesReduccion(int idReduccion, decimal valorAporte, int idAportePadre, string usuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteReduccion_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, idReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, valorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@idAportePadre", DbType.Int32, idAportePadre);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CrearAportesReduccion(int iddetCons, int idReduccion, int idAportePadre, decimal valorAporte, string usuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_AporteReduccion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDetConsModContractual", DbType.Int32, iddetCons);
                    vDataBase.AddInParameter(vDbCommand, "@IdReduccion", DbType.Int32, idReduccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAporte", DbType.Decimal, valorAporte);
                    vDataBase.AddInParameter(vDbCommand, "@idAportePadre", DbType.Int32, idAportePadre);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vDataBase.AddOutParameter(vDbCommand,"@IdCreado", DbType.Int16, 12);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return  Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCreado").ToString()); ;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
