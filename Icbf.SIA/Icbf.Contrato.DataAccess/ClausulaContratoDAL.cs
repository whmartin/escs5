using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Cl�usula Contrato
    /// </summary>
    public class ClausulaContratoDAL : GeneralDAL
    {
        public ClausulaContratoDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int InsertarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ClausulaContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdClausulaContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreClausulaContrato", DbType.String, pClausulaContrato.NombreClausulaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoClausula", DbType.Int32, pClausulaContrato.IdTipoClausula);
                    vDataBase.AddInParameter(vDbCommand, "@Contenido", DbType.String, pClausulaContrato.Contenido);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pClausulaContrato.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pClausulaContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pClausulaContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pClausulaContrato.IdClausulaContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdClausulaContrato").ToString());
                    GenerarLogAuditoria(pClausulaContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int ModificarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ClausulaContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdClausulaContrato", DbType.Int32, pClausulaContrato.IdClausulaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NombreClausulaContrato", DbType.String, pClausulaContrato.NombreClausulaContrato);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoClausula", DbType.Int32, pClausulaContrato.IdTipoClausula);
                    vDataBase.AddInParameter(vDbCommand, "@Contenido", DbType.String, pClausulaContrato.Contenido);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pClausulaContrato.IdTipoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pClausulaContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pClausulaContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pClausulaContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    throw new GenericException("Registro duplicado, verifique por favor.");
                throw;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int EliminarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ClausulaContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdClausulaContrato", DbType.Int32, pClausulaContrato.IdClausulaContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pClausulaContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pIdClausulaContrato"></param>
        /// <returns></returns>
        public ClausulaContrato ConsultarClausulaContrato(int pIdClausulaContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ClausulaContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdClausulaContrato", DbType.Int32, pIdClausulaContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ClausulaContrato vClausulaContrato = new ClausulaContrato();
                        while (vDataReaderResults.Read())
                        {
                            vClausulaContrato.IdClausulaContrato = vDataReaderResults["IdClausulaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClausulaContrato"].ToString()) : vClausulaContrato.IdClausulaContrato;
                            vClausulaContrato.NombreClausulaContrato = vDataReaderResults["NombreClausulaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreClausulaContrato"].ToString()) : vClausulaContrato.NombreClausulaContrato;
                            vClausulaContrato.IdTipoClausula = vDataReaderResults["IdTipoClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoClausula"].ToString()) : vClausulaContrato.IdTipoClausula;
                            vClausulaContrato.Contenido = vDataReaderResults["Contenido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Contenido"].ToString()) : vClausulaContrato.Contenido;
                            vClausulaContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vClausulaContrato.IdTipoContrato;
                            vClausulaContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vClausulaContrato.Estado;
                            vClausulaContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vClausulaContrato.UsuarioCrea;
                            vClausulaContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vClausulaContrato.FechaCrea;
                            vClausulaContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vClausulaContrato.UsuarioModifica;
                            vClausulaContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vClausulaContrato.FechaModifica;
                        }
                        return vClausulaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pNombreClausulaContrato"></param>
        /// <param name="pContenido"></param>
        /// <param name="pIdTipoClausula"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<ClausulaContrato> ConsultarClausulaContratos(String pNombreClausulaContrato, String pContenido, int? pIdTipoClausula, int? pIdTipoContrato, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Contrato_ClausulaContratos_Consultar"))
                {
                    if(pNombreClausulaContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreClausulaContrato", DbType.String, pNombreClausulaContrato);
                    if(pIdTipoClausula != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoClausula", DbType.Int32, pIdTipoClausula);
                    if(pIdTipoContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoContrato", DbType.Int32, pIdTipoContrato);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (pContenido != null)
                        vDataBase.AddInParameter(vDbCommand, "@Contenido", DbType.String, pContenido);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ClausulaContrato> vListaClausulaContrato = new List<ClausulaContrato>();
                        while (vDataReaderResults.Read())
                        {
                                ClausulaContrato vClausulaContrato = new ClausulaContrato();
                            vClausulaContrato.IdClausulaContrato = vDataReaderResults["IdClausulaContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClausulaContrato"].ToString()) : vClausulaContrato.IdClausulaContrato;
                            vClausulaContrato.NombreClausulaContrato = vDataReaderResults["NombreClausulaContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreClausulaContrato"].ToString()) : vClausulaContrato.NombreClausulaContrato;
                            vClausulaContrato.IdTipoClausula = vDataReaderResults["IdTipoClausula"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoClausula"].ToString()) : vClausulaContrato.IdTipoClausula;
                            vClausulaContrato.Contenido = vDataReaderResults["Contenido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Contenido"].ToString()) : vClausulaContrato.Contenido;
                            vClausulaContrato.IdTipoContrato = vDataReaderResults["IdTipoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoContrato"].ToString()) : vClausulaContrato.IdTipoContrato;
                            vClausulaContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vClausulaContrato.Estado;
                            vClausulaContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vClausulaContrato.UsuarioCrea;
                            vClausulaContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vClausulaContrato.FechaCrea;
                            vClausulaContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vClausulaContrato.UsuarioModifica;
                            vClausulaContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vClausulaContrato.FechaModifica;
                                vListaClausulaContrato.Add(vClausulaContrato);
                        }
                        return vListaClausulaContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
