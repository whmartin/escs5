using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Contrato.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad PlanComprasContratos
    /// </summary>
    public class PlanComprasContratosDAL : GeneralDAL
    {
        public PlanComprasContratosDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int InsertarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasContratos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasContratos.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pPlanComprasContratos.Vigencia);
                    //vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pPlanComprasContratos.IdRegional);
                    //vDataBase.AddInParameter(vDbCommand, "@IdDependencia", DbType.Int32, pPlanComprasContratos.IdDependencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioDuenoPlanCompras", DbType.String, pPlanComprasContratos.UsuarioDuenoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoPlanCompras", DbType.Int32, pPlanComprasContratos.IdEstadoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@IdAlcance", DbType.String, pPlanComprasContratos.IdAlcance);
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.Decimal, pPlanComprasContratos.Valor);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanComprasContratos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int ModificarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasContratos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@NumeroConsecutivoPlanCompras", DbType.Int32, pPlanComprasContratos.NumeroConsecutivoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pPlanComprasContratos.Vigencia);
                    //vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pPlanComprasContratos.IdRegional);
                    //vDataBase.AddInParameter(vDbCommand, "@IdDependencia", DbType.Int32, pPlanComprasContratos.IdDependencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioDuenoPlanCompras", DbType.String, pPlanComprasContratos.UsuarioDuenoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoPlanCompras", DbType.Int32, pPlanComprasContratos.IdEstadoPlanCompras);
                    vDataBase.AddInParameter(vDbCommand, "@IdAlcance", DbType.String, pPlanComprasContratos.IdAlcance);
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.Decimal, pPlanComprasContratos.Valor);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlanComprasContratos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int EliminarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                var vDataBase = ObtenerInstancia();

                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasContratos_Eliminar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasContratos.IdPlanDeComprasContratos);
                    if (pPlanComprasContratos.EsVigenciaFutura.HasValue)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@esVigenciaFutura", DbType.Boolean, pPlanComprasContratos.EsVigenciaFutura.Value);
                        vDataBase.AddInParameter(vDbCommand, "@IdVigenciaFutura", DbType.Int32, pPlanComprasContratos.IdVigenciaFutura);
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlanComprasContratos.UsuarioModifica);  
                                              
                    }

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int EliminarPlanComprasPreContractual(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                var vDataBase = ObtenerInstancia();

                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasPreContractual_Eliminar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDPlanDeComprasContratos", DbType.Int32, pPlanComprasContratos.IdPlanDeComprasContratos);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanComprasContratos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasContratos ConsultarPlanComprasContratos(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasContratos_Consultar"))
                {
                    if (pIdProductoPlanCompraContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProductoPlanCompraContrato", DbType.Int32, pIdProductoPlanCompraContrato);

                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vPlanComprasContratos = new PlanComprasContratos();
                        while (vDataReaderResults.Read())
                        {
                            vPlanComprasContratos.NumeroConsecutivoPlanCompras = vDataReaderResults["IDPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeCompras"].ToString()) : vPlanComprasContratos.NumeroConsecutivoPlanCompras;
                            vPlanComprasContratos.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vPlanComprasContratos.Vigencia;
                            vPlanComprasContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vPlanComprasContratos.IdContrato;
                            vPlanComprasContratos.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasContratos.IdPlanDeComprasContratos;
                            vPlanComprasContratos.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vPlanComprasContratos.CodigoRegional;
                            vPlanComprasContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasContratos.UsuarioCrea;
                            vPlanComprasContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasContratos.FechaCrea;
                            vPlanComprasContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasContratos.UsuarioModifica;
                            vPlanComprasContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasContratos.FechaModifica;
                        }
                        return vPlanComprasContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pVigencia"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdPlanDeCompras"></param>
        /// <param name="pCodigoRegional"></param>
        public List<PlanComprasContratos> ConsultarPlanComprasContratoss(int? pVigencia,
                                                                         int? pIdContrato,
                                                                         int? pIdPlanDeCompras,
                                                                         int? pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_PlanComprasContratoss_Consultar"))
                {
                    if(pVigencia != null)
                         vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pVigencia);
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pIdPlanDeCompras != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDPlanDeCompras", DbType.Int32, pIdPlanDeCompras);
                    if (pIdUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.Int32, pIdUsuario);                   

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaPlanComprasContratos = new List<PlanComprasContratos>();
                        while (vDataReaderResults.Read())
                        {
                                var vPlanComprasContratos = new PlanComprasContratos();
                                vPlanComprasContratos.NumeroConsecutivoPlanCompras = vDataReaderResults["IDPlanDeCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeCompras"].ToString()) : vPlanComprasContratos.NumeroConsecutivoPlanCompras;
                                vPlanComprasContratos.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vPlanComprasContratos.Vigencia;
                                vPlanComprasContratos.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vPlanComprasContratos.IdContrato;
                                vPlanComprasContratos.IdPlanDeComprasContratos = vDataReaderResults["IDPlanDeComprasContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDPlanDeComprasContratos"].ToString()) : vPlanComprasContratos.IdPlanDeComprasContratos;
                                vPlanComprasContratos.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vPlanComprasContratos.CodigoRegional;
                                vPlanComprasContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasContratos.UsuarioCrea;
                                vPlanComprasContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasContratos.FechaCrea;
                                vPlanComprasContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasContratos.UsuarioModifica;
                                vPlanComprasContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasContratos.FechaModifica;
                                vPlanComprasContratos.EsAdicion = vDataReaderResults["EsAdicion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsAdicion"].ToString()) : vPlanComprasContratos.EsAdicion;
                                vPlanComprasContratos.Valor = vDataReaderResults["ValorTotal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorTotal"].ToString()) : vPlanComprasContratos.Valor;
                            vPlanComprasContratos.IdSolicitudContrato = vDataReaderResults["IdSolicitudContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudContrato"].ToString()) : vPlanComprasContratos.IdSolicitudContrato;

                            vListaPlanComprasContratos.Add(vPlanComprasContratos);
                        }
                        return vListaPlanComprasContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pIdValoresContrato"></param>
        public List<PlanComprasContratos> ConsultarValoresPlanComprasContratos(int? pIdValoresContrato)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_ValoresPlanComprasContratos_Consultar"))
                {

                    if (pIdValoresContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdValoresContrato", DbType.Int32, pIdValoresContrato);

                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaPlanComprasContratos = new List<PlanComprasContratos>();
                        while (vDataReaderResults.Read())
                        {
                            var vPlanComprasContratos = new PlanComprasContratos();
                            vPlanComprasContratos.IdValoresContrato = vDataReaderResults["IdValoresContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValoresContrato"].ToString()) : vPlanComprasContratos.IdValoresContrato;
                            vPlanComprasContratos.Desde = vDataReaderResults["Desde"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Desde"].ToString()) : vPlanComprasContratos.Desde;
                            vPlanComprasContratos.Hasta = vDataReaderResults["Hasta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Hasta"].ToString()) : vPlanComprasContratos.Hasta;
                            vPlanComprasContratos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanComprasContratos.UsuarioCrea;
                            vPlanComprasContratos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanComprasContratos.FechaCrea;
                            vPlanComprasContratos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanComprasContratos.UsuarioModifica;
                            vPlanComprasContratos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanComprasContratos.FechaModifica;
                            vListaPlanComprasContratos.Add(vPlanComprasContratos);
                        }
                        return vListaPlanComprasContratos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vigencia"></param>
        /// <param name="codigoRegional"></param>
        /// <param name="trimestre"></param>
        public int ActualizarInformacionEjecucionContrato(int vigencia, string codigoRegional, ReporteContraloraTrimestre trimestre, string usuario)
        {
            try
            {
                    int mesInicial, mesFinal;
                    mesInicial = mesFinal = 0;

                    switch (trimestre)
                    {
                        case ReporteContraloraTrimestre.Primero:
                        mesInicial = 1;
                        mesFinal = 3;
                        break;
                        case ReporteContraloraTrimestre.Segundo:
                        mesInicial = 4;
                        mesFinal = 6;
                        break;
                        case ReporteContraloraTrimestre.Tercero:
                        mesInicial = 7;
                        mesFinal = 9;
                        break;
                        case ReporteContraloraTrimestre.Cuarto:
                        mesInicial = 10;
                        mesFinal = 12;
                        break;
                        default:
                        break;
                    }

                    var vDTValorsProgramados = new DataTable();
                    vDTValorsProgramados.Columns.Add(new DataColumn("ConsecutivoPlanCompras", Type.GetType("System.Int32")));
                    vDTValorsProgramados.Columns.Add(new DataColumn("ValorProgramado", Type.GetType("System.Decimal")));

                    using (var client = new ServicioPACCO.WSContratosPACCOSoapClient())
                    {
                        var itemsPagos = client.DetalleEjecucionRubrosXAcumuladoXcontrato(vigencia, codigoRegional, mesInicial, mesFinal);

                        foreach (var itemP in itemsPagos)
                         {
                            var filaDatatable = vDTValorsProgramados.NewRow();
                            filaDatatable[0] = itemP.consecutivo;
                            filaDatatable[1] = itemP.ejec_totalRubro;
                            vDTValorsProgramados.Rows.Add(filaDatatable);
                         }
                    }

                    var vDataBase = ObtenerInstancia();

                    int trimestreInt = (int)trimestre;

                    using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Contrato_InformacionEjecuccionContrato_Actualizar"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32,vigencia);
                        vDataBase.AddInParameter(vDbCommand, "@Trimestre", DbType.Int32, trimestreInt);
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);

                        var pDatosRubros = new SqlParameter("@ValoresProgramados", SqlDbType.Structured)
                        {
                           Value = vDTValorsProgramados
                        };

                        vDbCommand.Parameters.Add(pDatosRubros);

                        var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                        return vResultado;
                    }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
