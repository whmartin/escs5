using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class ProcesosJuridicosDAL : GeneralDAL
    {
        public ProcesosJuridicosDAL()
        {
        }
        public int InsertarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_ProcesosJuridicos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pProcesosJuridicos.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pProcesosJuridicos.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, pProcesosJuridicos.IdProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pProcesosJuridicos.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanContable", DbType.Int32, pProcesosJuridicos.IdPlanContable);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pProcesosJuridicos.NumeroProceso);
                    vDataBase.AddInParameter(vDbCommand, "@ValorProceso", DbType.String, pProcesosJuridicos.ValorProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pProcesosJuridicos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pProcesosJuridicos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ListTerceros", DbType.String, pProcesosJuridicos.ListTerceros);

                    vDataBase.AddInParameter(vDbCommand, "@IdNaturalezaProceso", DbType.Int32, pProcesosJuridicos.IdNaturalezaProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdFaseProceso", DbType.Int32, pProcesosJuridicos.IdFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdSubFaseProceso", DbType.Int32, pProcesosJuridicos.IdSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Juzgado", DbType.String, pProcesosJuridicos.Juzgado);
                    vDataBase.AddInParameter(vDbCommand, "@Apoderado", DbType.String, pProcesosJuridicos.Apoderado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdmisionJuzgado", DbType.DateTime, pProcesosJuridicos.FechaAdmisionJuzgado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdmisionICBF", DbType.DateTime, pProcesosJuridicos.FechaAdmisionICBF);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProcesosJuridicos.IdProcesosJuridicos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProcesosJuridicos").ToString());
                    vResultado = pProcesosJuridicos.IdProcesosJuridicos;
                    GenerarLogAuditoria(pProcesosJuridicos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_ProcesosJuridicos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, pProcesosJuridicos.IdProcesosJuridicos);
                    //vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pProcesosJuridicos.IdRegional);
                    //vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pProcesosJuridicos.IdVigencia);
                    //vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, pProcesosJuridicos.IdProceso);
                    //vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pProcesosJuridicos.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanContable", DbType.Int32, pProcesosJuridicos.IdPlanContable);
                    //vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pProcesosJuridicos.NumeroProceso);
                    vDataBase.AddInParameter(vDbCommand, "@ValorProceso", DbType.String, pProcesosJuridicos.ValorProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pProcesosJuridicos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProcesosJuridicos.UsuarioModifica);

                    vDataBase.AddInParameter(vDbCommand, "@IdNaturalezaProceso", DbType.Int32, pProcesosJuridicos.IdNaturalezaProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdFaseProceso", DbType.Int32, pProcesosJuridicos.IdFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdSubFaseProceso", DbType.Int32, pProcesosJuridicos.IdSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Juzgado", DbType.String, pProcesosJuridicos.Juzgado);
                    vDataBase.AddInParameter(vDbCommand, "@Apoderado", DbType.String, pProcesosJuridicos.Apoderado);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaAdmisionJuzgado", DbType.DateTime, pProcesosJuridicos.FechaAdmisionJuzgado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdmisionICBF", DbType.DateTime, pProcesosJuridicos.FechaAdmisionICBF);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pProcesosJuridicos.Observaciones);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProcesosJuridicos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_ProcesosJuridicos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, pProcesosJuridicos.IdProcesosJuridicos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProcesosJuridicos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ProcesosJuridicos ConsultarProcesosJuridicos(int pIdProcesosJuridicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_ProcesosJuridicos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, pIdProcesosJuridicos);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ProcesosJuridicos vProcesosJuridicos = new ProcesosJuridicos();
                        while (vDataReaderResults.Read())
                        {
                            vProcesosJuridicos.IdProcesosJuridicos = vDataReaderResults["IdProcesosJuridicos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProcesosJuridicos"].ToString()) : vProcesosJuridicos.IdProcesosJuridicos;
                            vProcesosJuridicos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProcesosJuridicos.IdRegional;
                            vProcesosJuridicos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProcesosJuridicos.IdVigencia;
                            vProcesosJuridicos.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vProcesosJuridicos.IdProceso;
                            vProcesosJuridicos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProcesosJuridicos.IdTercero;
                            vProcesosJuridicos.IdPlanContable = vDataReaderResults["IdPlanContable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanContable"].ToString()) : vProcesosJuridicos.IdPlanContable;
                            vProcesosJuridicos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vProcesosJuridicos.NumeroProceso;
                            vProcesosJuridicos.ValorProceso = vDataReaderResults["ValorProceso"] != DBNull.Value ? vDataReaderResults["ValorProceso"].ToString() : vProcesosJuridicos.ValorProceso;
                            vProcesosJuridicos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vProcesosJuridicos.Estado;
                            vProcesosJuridicos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProcesosJuridicos.UsuarioCrea;
                            vProcesosJuridicos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProcesosJuridicos.FechaCrea;
                            vProcesosJuridicos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProcesosJuridicos.UsuarioModifica;
                            vProcesosJuridicos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProcesosJuridicos.FechaModifica;
                            vProcesosJuridicos.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vProcesosJuridicos.Regional;
                            vProcesosJuridicos.VigenciaConcatenada = vDataReaderResults["VigenciaConcatenada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaConcatenada"].ToString()) : vProcesosJuridicos.VigenciaConcatenada;
                            vProcesosJuridicos.NombreTipoProceso = vDataReaderResults["NombreTipoProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoProceso"].ToString()) : vProcesosJuridicos.NombreTipoProceso;
                            vProcesosJuridicos.TerceroConcatenado = vDataReaderResults["TerceroConcatenado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TerceroConcatenado"].ToString()) : vProcesosJuridicos.TerceroConcatenado;
                            vProcesosJuridicos.ConcatenadoPlanContable = vDataReaderResults["ConcatenadoPlanContable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConcatenadoPlanContable"].ToString()) : vProcesosJuridicos.ConcatenadoPlanContable;
                            vProcesosJuridicos.Dado_de_Baja = vDataReaderResults["Dado_de_Baja"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dado_de_Baja"].ToString()) : vProcesosJuridicos.Dado_de_Baja;

                            vProcesosJuridicos.DescripcionNaturalezaProceso = vDataReaderResults["DescripcionNaturalezaProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionNaturalezaProceso"].ToString()) : vProcesosJuridicos.DescripcionNaturalezaProceso;
                            vProcesosJuridicos.DescripcionFaseProceso = vDataReaderResults["DescripcionFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionFaseProceso"].ToString()) : vProcesosJuridicos.DescripcionFaseProceso;
                            vProcesosJuridicos.DescripcionSubFaseProceso = vDataReaderResults["DescripcionSubFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionSubFaseProceso"].ToString()) : vProcesosJuridicos.DescripcionSubFaseProceso;
                            vProcesosJuridicos.Juzgado = vDataReaderResults["Juzgado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Juzgado"].ToString()) : vProcesosJuridicos.Juzgado;
                            vProcesosJuridicos.Apoderado = vDataReaderResults["Apoderado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Apoderado"].ToString()) : vProcesosJuridicos.Apoderado;
                            vProcesosJuridicos.FechaAdmisionJuzgado = vDataReaderResults["FechaAdmisionJuzgado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdmisionJuzgado"].ToString()) : vProcesosJuridicos.FechaAdmisionJuzgado;
                            vProcesosJuridicos.FechaAdmisionICBF = vDataReaderResults["FechaAdmisionICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdmisionICBF"].ToString()) : vProcesosJuridicos.FechaAdmisionICBF;
                            vProcesosJuridicos.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vProcesosJuridicos.Observaciones;
                            vProcesosJuridicos.IdNaturalezaProceso = vDataReaderResults["IdNaturalezaProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNaturalezaProceso"].ToString()) : vProcesosJuridicos.IdNaturalezaProceso;
                            vProcesosJuridicos.IdFaseProceso = vDataReaderResults["IdFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFaseProceso"].ToString()) : vProcesosJuridicos.IdFaseProceso;
                            vProcesosJuridicos.IdSubFaseProceso = vDataReaderResults["IdSubFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubFaseProceso"].ToString()) : vProcesosJuridicos.IdSubFaseProceso;
                            vProcesosJuridicos.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vProcesosJuridicos.Descripcion;
                        }

                        return vProcesosJuridicos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesosJuridicos> ConsultarProcesosJuridicoss(int? pIdRegional, int? pIdVigencia, int? pIdProceso,
            int? pIdTercero, string pIdPlanContable, string pNumeroProceso, string pValorProceso, int? pEstado,
            int? pIdNaturaleza, int? pIdFase, int? pIdSubFase)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_ProcesosJuridicoss_Consultar"))
                {
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pIdProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, pIdProceso);
                    if (pIdTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    if (!string.IsNullOrWhiteSpace(pIdPlanContable))
                        vDataBase.AddInParameter(vDbCommand, "@IdPlanContable", DbType.String, pIdPlanContable);
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pValorProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorProceso", DbType.String, pValorProceso);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);

                    if (pIdNaturaleza != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdNaturalezaProceso", DbType.Int32, pIdNaturaleza);
                    }

                    if (pIdFase != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdFaseProceso", DbType.Int32, pIdFase);
                    }

                    if (pIdSubFase != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdSubFaseProceso", DbType.Int32, pIdSubFase);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesosJuridicos> vListaProcesosJuridicos = new List<ProcesosJuridicos>();
                        while (vDataReaderResults.Read())
                        {
                            ProcesosJuridicos vProcesosJuridicos = new ProcesosJuridicos();
                            vProcesosJuridicos.IdProcesosJuridicos = vDataReaderResults["IdProcesosJuridicos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProcesosJuridicos"].ToString()) : vProcesosJuridicos.IdProcesosJuridicos;
                            vProcesosJuridicos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProcesosJuridicos.IdRegional;
                            vProcesosJuridicos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProcesosJuridicos.IdVigencia;
                            vProcesosJuridicos.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vProcesosJuridicos.IdProceso;
                            vProcesosJuridicos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProcesosJuridicos.IdTercero;
                            vProcesosJuridicos.IdPlanContable = vDataReaderResults["IdPlanContable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanContable"].ToString()) : vProcesosJuridicos.IdPlanContable;
                            vProcesosJuridicos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vProcesosJuridicos.NumeroProceso;
                            vProcesosJuridicos.ValorProceso = vDataReaderResults["ValorProceso"] != DBNull.Value ? vDataReaderResults["ValorProceso"].ToString() : vProcesosJuridicos.ValorProceso;
                            vProcesosJuridicos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vProcesosJuridicos.Estado;
                            vProcesosJuridicos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProcesosJuridicos.UsuarioCrea;
                            vProcesosJuridicos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProcesosJuridicos.FechaCrea;
                            vProcesosJuridicos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProcesosJuridicos.UsuarioModifica;
                            vProcesosJuridicos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProcesosJuridicos.FechaModifica;

                            vProcesosJuridicos.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vProcesosJuridicos.Regional;
                            vProcesosJuridicos.VigenciaConcatenada = vDataReaderResults["VigenciaConcatenada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaConcatenada"].ToString()) : vProcesosJuridicos.VigenciaConcatenada;
                            vProcesosJuridicos.NombreTipoProceso = vDataReaderResults["NombreTipoProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoProceso"].ToString()) : vProcesosJuridicos.NombreTipoProceso;
                            vProcesosJuridicos.TerceroConcatenado = vDataReaderResults["TerceroConcatenado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TerceroConcatenado"].ToString()) : vProcesosJuridicos.TerceroConcatenado;
                            vProcesosJuridicos.ConcatenadoPlanContable = vDataReaderResults["ConcatenadoPlanContable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConcatenadoPlanContable"].ToString()) : vProcesosJuridicos.ConcatenadoPlanContable;
                            vProcesosJuridicos.Dado_de_Baja = vDataReaderResults["Dado_de_Baja"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dado_de_Baja"].ToString()) : vProcesosJuridicos.Dado_de_Baja;
                            vProcesosJuridicos.DescripcionNaturalezaProceso = vDataReaderResults["DescripcionNaturalezaProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionNaturalezaProceso"].ToString()) : vProcesosJuridicos.DescripcionNaturalezaProceso;
                            vProcesosJuridicos.DescripcionFaseProceso = vDataReaderResults["DescripcionFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionFaseProceso"].ToString()) : vProcesosJuridicos.DescripcionFaseProceso;
                            vProcesosJuridicos.DescripcionSubFaseProceso = vDataReaderResults["DescripcionSubFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionSubFaseProceso"].ToString()) : vProcesosJuridicos.DescripcionSubFaseProceso;
                            vListaProcesosJuridicos.Add(vProcesosJuridicos);
                        }

                        return vListaProcesosJuridicos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar el correo del contador.
        /// </summary>
        /// <param name="pIdRegional">Parametro de consulta por id de regional</param>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoContador(int? pIdRegional, string pProviderKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_ProcesosJuridicoss_ConsultarCorreosContador"))
                {
                    if (pIdRegional != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    }

                    if (pProviderKey != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pProviderKey);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string vCorreoContador = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            vCorreoContador = string.Concat(vCorreoContador, (vDataReaderResults["CorreoElectronico"] != DBNull.Value ? vDataReaderResults["CorreoElectronico"] : vCorreoContador), "; ");
                        }

                        vCorreoContador = (!string.IsNullOrEmpty(vCorreoContador)) ? vCorreoContador.Remove(vCorreoContador.Length - 2) : vCorreoContador;

                        return vCorreoContador;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar el correo del administrador.
        /// </summary>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoAdministrador(string pProviderKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_ProcesosJuridicoss_ConsultarCorreosAdministrador"))
                {

                    if (pProviderKey != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pProviderKey);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string vCorreoAdministrador = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            vCorreoAdministrador = string.Concat(vCorreoAdministrador, (vDataReaderResults["CorreoElectronico"] != DBNull.Value ? vDataReaderResults["CorreoElectronico"] : vCorreoAdministrador), "; ");
                        }

                        vCorreoAdministrador = (!string.IsNullOrEmpty(vCorreoAdministrador)) ? vCorreoAdministrador.Remove(vCorreoAdministrador.Length - 2) : vCorreoAdministrador;

                        return vCorreoAdministrador;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta el CodigoCuenta y Descripcion del Plan Contable
        /// </summary>
        /// <param name="pIdNaturalezaProceso"></param>
        /// <param name="pIdFaseProceso"></param>
        /// <param name="pIdSubFaseProceso"></param>
        /// <param name="pIdProceso"></param>
        /// <returns></returns>
        public ProcesosJuridicos ConsultarProcesosJuridicosPlanContable(int pIdNaturalezaProceso,
            int pIdFaseProceso, int pIdSubFaseProceso, int pIdProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("CJR.usp_INGENIAN_CJR_ProcesosJuridicos_ConsultarPlanContable"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdNaturalezaProceso", DbType.Int32, pIdNaturalezaProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdFaseProceso", DbType.Int32, pIdFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdSubFaseProceso", DbType.Int32, pIdSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, pIdProceso);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ProcesosJuridicos vProcesosJuridicos = new ProcesosJuridicos();
                        while (vDataReaderResults.Read())
                        {
                            vProcesosJuridicos.IdPlanContable = vDataReaderResults["IdPlanContable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanContable"].ToString()) : vProcesosJuridicos.IdPlanContable;
                            vProcesosJuridicos.CodigoCuenta = vDataReaderResults["CodigoCuenta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCuenta"].ToString()) : vProcesosJuridicos.CodigoCuenta;
                            vProcesosJuridicos.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vProcesosJuridicos.Descripcion;
                        }

                        return vProcesosJuridicos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta si existe registros con la misma informaci�n
        /// </summary>
        /// <param name="pIdProceso">Id del Proceso</param>
        /// <param name="pIdNaturalezaProceso">Descripci�n de la Naturaleza del proceso</param>
        /// <param name="pIdFaseProceso">Descripci�n de la Fase del proceso</param>
        /// <param name="pIdSubFaseProceso">Descripci�n de la SubFase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarCombinacion(int pIdProceso, int? pIdNaturalezaProceso, int? pIdFaseProceso, int? pIdSubFaseProceso, int? pIdProcesosJuridicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_ProcesosJuridicos_ConsultarCombinacion]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.String, pIdProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdNaturalezaProceso", DbType.Int32, pIdNaturalezaProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdFaseProceso", DbType.String, pIdFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdSubFaseProceso", DbType.Int32, pIdSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, pIdProcesosJuridicos);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
