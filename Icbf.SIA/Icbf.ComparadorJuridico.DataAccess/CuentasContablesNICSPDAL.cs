﻿//-----------------------------------------------------------------------
// <copyright file="CuentasContablesNICSPDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase CuentasContablesNICSPDAL.</summary>
// <author>Ingenian Software</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo CuentasContablesNICSPDAL
    /// </summary>
    public class CuentasContablesNICSPDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor clase
        /// </summary>
        public CuentasContablesNICSPDAL()
        {

        }

        /// <summary>
        /// Método que retorna una lista de cuentas contables NICSP
        /// </summary>
        /// <param name="pIdCuentasContablesNICSP">Id cuenta contable NISP</param>
        /// <param name="pIdTipoProceso">Id tipo proceso</param>
        /// <param name="pIdFaseProceso">Id fase del proceso</param>
        /// <param name="pIdSubFaseProceso">Id sub fase del proceso</param>
        /// <param name="pIdNaturalezaProceso">Id naturaleza del proceso</param>
        /// <param name="pIdCodCtaConSiifConIntArea">Id cuentacontable inter area</param>
        /// <param name="pIdCodCtaConSiifDebito">id cuenta contable debito</param>
        /// <param name="pIdCodCtaConSiifCredito">Id cuenta contable credito</param>
        /// <param name="pEstado">Estado</param>
        /// <returns>Una lista de las cuentas contables NICSP</returns>
        public IEnumerable<CuentasContablesNICSP> ConsultarCuentasContablesNICSP(
           int? pIdCuentasContablesNICSP,
           int? pIdTipoProceso,
           int? pIdFaseProceso,
           int? pIdSubFaseProceso,
           int? pIdNaturalezaProceso,
           int? pIdCodCtaConSiifConIntArea,
           int? pIdCodCtaConSiifDebito,
           int? pIdCodCtaConSiifCredito,
           bool? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_ICBF_CJR_CuentasContablesNICSP_Consultar]"))
                {
                    if (pIdCuentasContablesNICSP != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdCuentasContablesNICSP", DbType.Int32, pIdCuentasContablesNICSP);
                    }
                    if (pIdTipoProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idTipoProceso", DbType.Int32, pIdTipoProceso);
                    }
                    if (pIdFaseProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idFaseProceso", DbType.Int32, pIdFaseProceso);
                    }
                    if (pIdSubFaseProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idSubFaseProceso", DbType.Int32, pIdSubFaseProceso);
                    }
                    if (pIdNaturalezaProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idNaturalezaProceso", DbType.Int32, pIdNaturalezaProceso);
                    }
                    if (pIdCodCtaConSiifConIntArea != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifConIntArea", DbType.Int32, pIdCodCtaConSiifConIntArea);
                    }
                    if (pIdCodCtaConSiifDebito != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifDebito", DbType.Int32, pIdCodCtaConSiifDebito);
                    }
                    if (pIdCodCtaConSiifCredito != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifCredito", DbType.Int32, pIdCodCtaConSiifCredito);
                    }
                    if (pEstado != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@estado", DbType.Boolean, pEstado);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CuentasContablesNICSP> vCuentasContablesNICSPs = new List<CuentasContablesNICSP>();
                        while (vDataReaderResults.Read())
                        {
                            CuentasContablesNICSP vCuentasContablesNICSP = new CuentasContablesNICSP();

                            vCuentasContablesNICSP.IdCuentasContablesNICSP = vDataReaderResults["idCuentasContablesNICSP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idCuentasContablesNICSP"].ToString()) : vCuentasContablesNICSP.IdCuentasContablesNICSP;
                            vCuentasContablesNICSP.IdTipoProceso = vDataReaderResults["idTipoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idTipoProceso"].ToString()) : vCuentasContablesNICSP.IdTipoProceso;
                            vCuentasContablesNICSP.IdFaseProceso = vDataReaderResults["idFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idFaseProceso"].ToString()) : vCuentasContablesNICSP.IdFaseProceso;
                            vCuentasContablesNICSP.IdSubFaseProceso = vDataReaderResults["idSubFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idSubFaseProceso"].ToString()) : vCuentasContablesNICSP.IdSubFaseProceso;
                            vCuentasContablesNICSP.IdNaturalezaProceso = vDataReaderResults["idNaturalezaProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idNaturalezaProceso"].ToString()) : vCuentasContablesNICSP.IdNaturalezaProceso;
                            vCuentasContablesNICSP.IdCodCtaConSiifConIntArea = vDataReaderResults["idCodCtaConSiifConIntArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idCodCtaConSiifConIntArea"].ToString()) : vCuentasContablesNICSP.IdCodCtaConSiifConIntArea;
                            vCuentasContablesNICSP.CodCtaContSiifIntArea = vDataReaderResults["codCtaContSiifIntArea"] != DBNull.Value ? vDataReaderResults["codCtaContSiifIntArea"].ToString() : vCuentasContablesNICSP.CodCtaContSiifIntArea;
                            vCuentasContablesNICSP.DescCtaContSiifIntArea = vDataReaderResults["descCtaContSiifIntArea"] != DBNull.Value ? vDataReaderResults["descCtaContSiifIntArea"].ToString() : vCuentasContablesNICSP.DescCtaContSiifIntArea;
                            vCuentasContablesNICSP.IdCodCtaConSiifDebito = vDataReaderResults["idCodCtaConSiifDebito"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idCodCtaConSiifDebito"].ToString()) : vCuentasContablesNICSP.IdCodCtaConSiifDebito;
                            vCuentasContablesNICSP.CodCtaContSiifDebito = vDataReaderResults["codCtaContSiifDebito"] != DBNull.Value ? vDataReaderResults["codCtaContSiifDebito"].ToString() : vCuentasContablesNICSP.CodCtaContSiifDebito;
                            vCuentasContablesNICSP.DescCtaContSiifDebito = vDataReaderResults["descCtaContSiifDebito"] != DBNull.Value ? vDataReaderResults["descCtaContSiifDebito"].ToString() : vCuentasContablesNICSP.DescCtaContSiifDebito;
                            vCuentasContablesNICSP.IdCodCtaConSiifCredito = vDataReaderResults["idCodCtaConSiifCredito"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idCodCtaConSiifCredito"].ToString()) : vCuentasContablesNICSP.IdCodCtaConSiifCredito;
                            vCuentasContablesNICSP.CodCtaContSiifCredito = vDataReaderResults["codCtaContSiifCredito"] != DBNull.Value ? vDataReaderResults["codCtaContSiifCredito"].ToString() : vCuentasContablesNICSP.CodCtaContSiifCredito;
                            vCuentasContablesNICSP.DescCtaContSiifCredito = vDataReaderResults["descCtaContSiifCredito"] != DBNull.Value ? vDataReaderResults["descCtaContSiifCredito"].ToString() : vCuentasContablesNICSP.DescCtaContSiifCredito;
                            vCuentasContablesNICSP.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCuentasContablesNICSP.Estado;
                            vCuentasContablesNICSP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCuentasContablesNICSP.UsuarioCrea;
                            vCuentasContablesNICSP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCuentasContablesNICSP.FechaCrea;
                            vCuentasContablesNICSP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCuentasContablesNICSP.UsuarioModifica;
                            vCuentasContablesNICSP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCuentasContablesNICSP.FechaModifica;

                            vCuentasContablesNICSPs.Add(vCuentasContablesNICSP);
                        }

                        return vCuentasContablesNICSPs;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que retorna una lista de cuentas contables NICSP
        /// </summary>
        /// <param name="pIdCuentasContablesNICSP">Id cuenta contable NISP</param>
        /// <param name="pIdTipoProceso">Id tipo proceso</param>
        /// <param name="pIdFaseProceso">Id fase del proceso</param>
        /// <param name="pIdSubFaseProceso">Id sub fase del proceso</param>
        /// <param name="pIdNaturalezaProceso">Id naturaleza del proceso</param>
        /// <param name="pIdCodCtaConSiifConIntArea">Id cuentacontable inter area</param>
        /// <param name="pIdCodCtaConSiifDebito">id cuenta contable debito</param>
        /// <param name="pIdCodCtaConSiifCredito">Id cuenta contable credito</param>
        /// <param name="pEstado">Estado</param>
        /// <returns>Una lista de las cuentas contables NICSP</returns>
        public IEnumerable<CuentasContablesNICSPResumen> ConsultarCuentasContablesNICSPResumen(
           int? pIdCuentasContablesNICSP,
           int? pIdTipoProceso,
           int? pIdFaseProceso,
           int? pIdSubFaseProceso,
           int? pIdNaturalezaProceso,
           int? pIdCodCtaConSiifConIntArea,
           int? pIdCodCtaConSiifDebito,
           int? pIdCodCtaConSiifCredito,
           bool? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_ICBF_CJR_CuentasContablesNICSP_ConsultarResumido]"))
                {
                    if (pIdCuentasContablesNICSP != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdCuentasContablesNICSP", DbType.Int32, pIdCuentasContablesNICSP);
                    }
                    if (pIdTipoProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idTipoProceso", DbType.Int32, pIdTipoProceso);
                    }
                    if (pIdFaseProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idFaseProceso", DbType.Int32, pIdFaseProceso);
                    }
                    if (pIdSubFaseProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idSubFaseProceso", DbType.Int32, pIdSubFaseProceso);
                    }
                    if (pIdNaturalezaProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idNaturalezaProceso", DbType.Int32, pIdNaturalezaProceso);
                    }
                    if (pIdCodCtaConSiifConIntArea != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifConIntArea", DbType.Int32, pIdCodCtaConSiifConIntArea);
                    }
                    if (pIdCodCtaConSiifDebito != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifDebito", DbType.Int32, pIdCodCtaConSiifDebito);
                    }
                    if (pIdCodCtaConSiifCredito != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifCredito", DbType.Int32, pIdCodCtaConSiifCredito);
                    }
                    if (pEstado != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@estado", DbType.Boolean, pEstado);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CuentasContablesNICSPResumen> vCuentasContablesNICSPs = new List<CuentasContablesNICSPResumen>();
                        while (vDataReaderResults.Read())
                        {
                            CuentasContablesNICSPResumen vCuentasContablesNICSP = new CuentasContablesNICSPResumen();

                            vCuentasContablesNICSP.IdCuentasContablesNICSP = vDataReaderResults["idCuentasContablesNICSP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idCuentasContablesNICSP"].ToString()) : vCuentasContablesNICSP.IdCuentasContablesNICSP;
                            vCuentasContablesNICSP.TipoProceso = vDataReaderResults["TipoProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoProceso"]) : vCuentasContablesNICSP.TipoProceso;
                            vCuentasContablesNICSP.Fase = vDataReaderResults["Fase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Fase"]) : vCuentasContablesNICSP.Fase;
                            vCuentasContablesNICSP.SubFase = vDataReaderResults["SubFase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SubFase"]) : vCuentasContablesNICSP.SubFase;
                            vCuentasContablesNICSP.Naturaleza = vDataReaderResults["Naturaleza"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Naturaleza"]) : vCuentasContablesNICSP.Naturaleza;
                            vCuentasContablesNICSP.CuentaInterAreas = vDataReaderResults["CuentaInterAreas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CuentaInterAreas"]) : vCuentasContablesNICSP.CuentaInterAreas;
                            vCuentasContablesNICSP.CuentaDebito = vDataReaderResults["CuentaDebito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CuentaDebito"]) : vCuentasContablesNICSP.CuentaDebito;
                            vCuentasContablesNICSP.CuentaCredito = vDataReaderResults["CuentaCredito"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CuentaCredito"]) : vCuentasContablesNICSP.CuentaCredito;

                            vCuentasContablesNICSP.IdFaseProceso = vDataReaderResults["IdFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFaseProceso"].ToString()) : vCuentasContablesNICSP.IdFaseProceso;
                            vCuentasContablesNICSP.IdSubFaseProceso = vDataReaderResults["IdSubFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubFaseProceso"].ToString()) : vCuentasContablesNICSP.IdSubFaseProceso;
                            vCuentasContablesNICSP.IdNaturalezaProceso = vDataReaderResults["IdNaturalezaProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNaturalezaProceso"].ToString()) : vCuentasContablesNICSP.IdNaturalezaProceso;

                            vCuentasContablesNICSPs.Add(vCuentasContablesNICSP);
                        }

                        return vCuentasContablesNICSPs;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para guardar la cuenta contable NICSP
        /// </summary>
        /// <param name="pCuentasContablesNICSP">Objeto que contiene la información a guardar</param>
        /// <returns>Resultado de la operación</returns>
        public int GuardarCuentasContablesNICSP(CuentasContablesNICSP pCuentasContablesNICSP)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_ICBF_CJR_CuentasContablesNICSP_Guardar]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@idCuentasContablesNICSP", DbType.Int32, pCuentasContablesNICSP.IdCuentasContablesNICSP);
                    vDataBase.AddInParameter(vDbCommand, "@idTipoProceso", DbType.Int32, pCuentasContablesNICSP.IdTipoProceso);
                    vDataBase.AddInParameter(vDbCommand, "@idFaseProceso", DbType.Int32, pCuentasContablesNICSP.IdFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@idSubFaseProceso", DbType.Int32, pCuentasContablesNICSP.IdSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@idNaturalezaProceso", DbType.Int32, pCuentasContablesNICSP.IdNaturalezaProceso);
                    vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifConIntArea", DbType.Int32, pCuentasContablesNICSP.IdCodCtaConSiifConIntArea);
                    vDataBase.AddInParameter(vDbCommand, "@codCtaContSiifIntArea", DbType.String, pCuentasContablesNICSP.CodCtaContSiifIntArea);
                    vDataBase.AddInParameter(vDbCommand, "@descCtaContSiifIntArea", DbType.String, pCuentasContablesNICSP.DescCtaContSiifIntArea);
                    vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifDebito", DbType.Int32, pCuentasContablesNICSP.IdCodCtaConSiifDebito);
                    vDataBase.AddInParameter(vDbCommand, "@codCtaContSiifDebito", DbType.String, pCuentasContablesNICSP.CodCtaContSiifDebito);
                    vDataBase.AddInParameter(vDbCommand, "@descCtaContSiifDebito", DbType.String, pCuentasContablesNICSP.DescCtaContSiifDebito);
                    vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifCredito", DbType.Int32, pCuentasContablesNICSP.IdCodCtaConSiifCredito);
                    vDataBase.AddInParameter(vDbCommand, "@codCtaContSiifCredito", DbType.String, pCuentasContablesNICSP.CodCtaContSiifCredito);
                    vDataBase.AddInParameter(vDbCommand, "@descCtaContSiifCredito", DbType.String, pCuentasContablesNICSP.DescCtaContSiifCredito);
                    vDataBase.AddInParameter(vDbCommand, "@estado", DbType.Boolean, pCuentasContablesNICSP.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@usuario", DbType.String, pCuentasContablesNICSP.Usuario);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);

                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pCuentasContablesNICSP;
                        vAuditorio.ProgramaGeneraLog = true;

                        if (pCuentasContablesNICSP.IdCuentasContablesNICSP == 0)
                        {
                            pCuentasContablesNICSP.IdCuentasContablesNICSP = vResultado;
                            pCuentasContablesNICSP.FechaCrea = DateTime.Now;
                            vAuditorio.Operacion = "INSERT";
                        }
                        else
                        {
                            pCuentasContablesNICSP.FechaModifica = DateTime.Now;
                            vAuditorio.Operacion = "UPDATE";
                        }

                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para validar si existe una cuenta parametrizada
        /// </summary>
        /// <param name="pIdCuentasContablesNICSP">Identificador de cuentas contables NICSP</param>
        /// <param name="pIdTipoProceso">Id tipo de proceso</param>
        /// <param name="pIdCodCtaConSiif">Id cuenta contable debito/credito</param>
        /// <returns>True si existe ó False si no existe</returns>
        public bool ValidaContablesNICSP(
           int pIdCuentasContablesNICSP,
           int pIdTipoProceso,
           int pIdCodCtaConSiifDebito,
           int pIdCodCtaConSiifCredito)
        {
            try
            {
                bool existe = false;
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_ICBF_CJR_CuentasContablesNICSP_Validar]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idCuentasContablesNICSP", DbType.Int32, pIdCuentasContablesNICSP);
                    vDataBase.AddInParameter(vDbCommand, "@idTipoProceso", DbType.Int32, pIdTipoProceso);
                    vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifDebito", DbType.Int32, pIdCodCtaConSiifDebito);
                    vDataBase.AddInParameter(vDbCommand, "@idCodCtaConSiifCredito", DbType.Int32, pIdCodCtaConSiifCredito);

                    existe = Convert.ToBoolean(vDataBase.ExecuteScalar(vDbCommand));
                }

                return existe;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
