﻿//-----------------------------------------------------------------------
// <copyright file="SubFaseProcesoDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase SubFaseProcesoDAL.</summary>
// <author>Ingenian Software</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo SubFaseProcesoDAL
    /// </summary>
    public class SubFaseProcesoDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor clase
        /// </summary>
        public SubFaseProcesoDAL()
        {

        }

        /// <summary>
        /// Consulta los datos de la SubFase del Proceso
        /// </summary>
        /// <param name="pIdSubFaseProceso">Id de la SubFase</param>
        /// <param name="pCodigoSubFaseProceso">Código de la SubFase</param>
        /// <param name="pDescripcionSubFaseProceso">Descripción de la SubFase</param>
        /// <param name="pEstado">Estado de la SubFase</param>
        /// <returns>Lista de Registros de SubFase del Proceso</returns>
        public List<SubFaseProceso> ConsultarSubFaseProceso(int? pIdSubFaseProceso, string pCodigoSubFaseProceso, string pDescripcionSubFaseProceso, int pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_SubFaseProceso_Consultar]"))
                {
                    if (pIdSubFaseProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdSubFaseProceso", DbType.Int32, pIdSubFaseProceso);
                    }

                    if (!string.IsNullOrEmpty(pCodigoSubFaseProceso))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@CodigoSubFaseProceso", DbType.String, pCodigoSubFaseProceso);
                    }

                    if (!string.IsNullOrEmpty(pDescripcionSubFaseProceso))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@DescripcionSubFaseProceso", DbType.String, pDescripcionSubFaseProceso);
                    }

                    if (pEstado != -1)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SubFaseProceso> vSubFaseProcesos = new List<SubFaseProceso>();
                        while (vDataReaderResults.Read())
                        {
                            SubFaseProceso vSubFaseProceso = new SubFaseProceso();
                            vSubFaseProceso.IdSubFaseProceso = vDataReaderResults["IdSubFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubFaseProceso"].ToString()) : vSubFaseProceso.IdSubFaseProceso;
                            vSubFaseProceso.CodigoSubFaseProceso = vDataReaderResults["CodigoSubFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSubFaseProceso"].ToString()) : vSubFaseProceso.CodigoSubFaseProceso;
                            vSubFaseProceso.DescripcionSubFaseProceso = vDataReaderResults["DescripcionSubFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionSubFaseProceso"].ToString()) : vSubFaseProceso.DescripcionSubFaseProceso;
                            vSubFaseProceso.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSubFaseProceso.Estado;
                            vSubFaseProceso.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSubFaseProceso.UsuarioCrea;
                            vSubFaseProceso.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSubFaseProceso.FechaCrea;
                            vSubFaseProceso.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSubFaseProceso.UsuarioModifica;
                            vSubFaseProceso.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSubFaseProceso.FechaModifica;
                            vSubFaseProcesos.Add(vSubFaseProceso);
                        }

                        return vSubFaseProcesos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de SubFase Proceso
        /// </summary>
        /// <param name="pIdSubFaseProceso">Id de la SubFase del proceso</param>
        /// <param name="pDescripcionSubFaseProceso">Nombre de la SubFase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarSubFaseProcesoNombre(int? pIdSubFaseProceso, string pDescripcionSubFaseProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_SubFaseProceso_ConsultarDescripcion]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    if (pIdSubFaseProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdSubFaseProceso", DbType.Int32, pIdSubFaseProceso);
                    }

                    vDataBase.AddInParameter(vDbCommand, "@DescripcionSubFaseProceso", DbType.String, pDescripcionSubFaseProceso);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de SubFase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarSubFaseproceso(SubFaseProceso pSubFaseProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_SubFaseProceso_Insertar]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@CodigoSubFaseProceso", DbType.String, pSubFaseProceso.CodigoSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionSubFaseProceso", DbType.String, pSubFaseProceso.DescripcionSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSubFaseProceso.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSubFaseProceso.UsuarioCrea);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);

                    if (vResultado > 0)
                    {
                        pSubFaseProceso.IdSubFaseProceso = vResultado;
                        pSubFaseProceso.FechaCrea = DateTime.Now;
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pSubFaseProceso;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala modificación de SubFase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarSubFaseproceso(SubFaseProceso pSubFaseProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_SubFaseProceso_Modificar]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@IdSubFaseProceso", DbType.Int32, pSubFaseProceso.IdSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionSubFaseProceso", DbType.String, pSubFaseProceso.DescripcionSubFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSubFaseProceso.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSubFaseProceso.UsuarioModifica);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);

                    if (vResultado > 0)
                    {
                        pSubFaseProceso.FechaModifica = DateTime.Now;
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pSubFaseProceso;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la SubFase
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoSubFase()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                string vResultado = string.Empty;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_SubFaseProceso_ObtenerCodigoSubFase]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        while (vDataReaderResults.Read())
                        {
                            SubFaseProceso vSubFaseProceso = new SubFaseProceso();
                            vSubFaseProceso.CodigoSubFaseProceso = vDataReaderResults["CodigoSubFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSubFaseProceso"].ToString()) : vSubFaseProceso.CodigoSubFaseProceso;
                            vResultado = vSubFaseProceso.CodigoSubFaseProceso;
                        }

                        return vResultado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la SubFase del Proceso Contactenado Código-Descripción
        /// </summary>
        /// <returns>Lista de Registros de SubFase del Proceso</returns>
        public List<SubFaseProceso> ConsultarSubFaseProcesoConcatenado()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_SubFaseProceso_ConsultarConcatenado]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SubFaseProceso> vSubFaseProcesos = new List<SubFaseProceso>();
                        while (vDataReaderResults.Read())
                        {
                            SubFaseProceso vSubFaseProceso = new SubFaseProceso();
                            vSubFaseProceso.IdSubFaseProceso = vDataReaderResults["IdSubFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSubFaseProceso"].ToString()) : vSubFaseProceso.IdSubFaseProceso;
                            vSubFaseProceso.DescripcionSubFaseProceso = vDataReaderResults["DescripcionSubFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionSubFaseProceso"].ToString()) : vSubFaseProceso.DescripcionSubFaseProceso;
                            vSubFaseProcesos.Add(vSubFaseProceso);
                        }

                        return vSubFaseProcesos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
