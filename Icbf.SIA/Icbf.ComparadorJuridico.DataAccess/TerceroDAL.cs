using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class TerceroDAL : GeneralDAL
    {
        public TerceroDAL()
        {
        }
        public int InsertarTercero(Tercero pTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_Tercero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTercero", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTercero.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pTercero.NumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombreTercero", DbType.String, pTercero.PrimerNombreTercero);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombreTercero", DbType.String, pTercero.SegundoNombreTercero);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellidoTercero", DbType.String, pTercero.PrimerApellidoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellidoTercero", DbType.String, pTercero.SegundoApellidoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pTercero.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTercero.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTercero.IdTercero = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTercero").ToString());
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTercero(Tercero pTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_Tercero_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTercero.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pTercero.NumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombreTercero", DbType.String, pTercero.PrimerNombreTercero);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombreTercero", DbType.String, pTercero.SegundoNombreTercero);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellidoTercero", DbType.String, pTercero.PrimerApellidoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellidoTercero", DbType.String, pTercero.SegundoApellidoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pTercero.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTercero.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTercero(Tercero pTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_Tercero_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pTercero.IdTercero);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_Tercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {
                            vTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vTercero.IdTercero;
                            vTercero.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTercero.IdTipoDocumento;
                            vTercero.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vTercero.NumeroDocumento;
                            vTercero.PrimerNombreTercero = vDataReaderResults["PrimerNombreTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombreTercero"].ToString()) : vTercero.PrimerNombreTercero;
                            vTercero.SegundoNombreTercero = vDataReaderResults["SegundoNombreTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombreTercero"].ToString()) : vTercero.SegundoNombreTercero;
                            vTercero.PrimerApellidoTercero = vDataReaderResults["PrimerApellidoTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellidoTercero"].ToString()) : vTercero.PrimerApellidoTercero;
                            vTercero.SegundoApellidoTercero = vDataReaderResults["SegundoApellidoTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellidoTercero"].ToString()) : vTercero.SegundoApellidoTercero;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;

                            vTercero.TipoDocumento = vDataReaderResults["TipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumento"].ToString()) : vTercero.TipoDocumento;
                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Tercero> ConsultarTerceros(int? pIdTipoDocumento, String pNumeroDocumento, String pPrimerNombreTercero, String pSegundoNombreTercero, String pPrimerApellidoTercero, String pSegundoApellidoTercero, String pRazonSocial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_Terceros_Consultar"))
                {
                    if(pIdTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pIdTipoDocumento <= 0 ? null : pIdTipoDocumento);
                    if(pNumeroDocumento != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pNumeroDocumento);
                    if(pPrimerNombreTercero != null)
                         vDataBase.AddInParameter(vDbCommand, "@PrimerNombreTercero", DbType.String, pPrimerNombreTercero);
                    if(pSegundoNombreTercero != null)
                         vDataBase.AddInParameter(vDbCommand, "@SegundoNombreTercero", DbType.String, pSegundoNombreTercero);
                    if(pPrimerApellidoTercero != null)
                         vDataBase.AddInParameter(vDbCommand, "@PrimerApellidoTercero", DbType.String, pPrimerApellidoTercero);
                    if(pSegundoApellidoTercero != null)
                         vDataBase.AddInParameter(vDbCommand, "@SegundoApellidoTercero", DbType.String, pSegundoApellidoTercero);
                    if(pRazonSocial != null)
                         vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pRazonSocial);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Tercero> vListaTercero = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                                Tercero vTercero = new Tercero();
                            vTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vTercero.IdTercero;
                            vTercero.TerceroConcatenado = vDataReaderResults["TerceroConcatenado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TerceroConcatenado"].ToString()) : vTercero.TerceroConcatenado;
                            vTercero.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTercero.IdTipoDocumento;
                            vTercero.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vTercero.NumeroDocumento;
                            vTercero.PrimerNombreTercero = vDataReaderResults["PrimerNombreTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombreTercero"].ToString()) : vTercero.PrimerNombreTercero;
                            vTercero.SegundoNombreTercero = vDataReaderResults["SegundoNombreTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombreTercero"].ToString()) : vTercero.SegundoNombreTercero;
                            vTercero.PrimerApellidoTercero = vDataReaderResults["PrimerApellidoTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellidoTercero"].ToString()) : vTercero.PrimerApellidoTercero;
                            vTercero.SegundoApellidoTercero = vDataReaderResults["SegundoApellidoTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellidoTercero"].ToString()) : vTercero.SegundoApellidoTercero;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;

                            vTercero.TipoDocumento = vDataReaderResults["TipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumento"].ToString()) : vTercero.TipoDocumento;
                                vListaTercero.Add(vTercero);
                        }
                        return vListaTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
