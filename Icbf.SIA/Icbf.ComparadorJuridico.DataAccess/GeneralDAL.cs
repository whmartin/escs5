﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Reflection;
using Icbf.Seguridad.Entity;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class GeneralDAL
    {
        public GeneralDAL()
        {

        }
        public Database ObtenerInstancia()
        {
            //return DatabaseFactory.CreateDatabase("DataBaseConnectionString");
            return Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
        }
        protected void GenerarLogAuditoria(Object pObjeto, DbCommand pDbCommand)
        {
            try
            {
                Icbf.Seguridad.Entity.EntityAuditoria pDatosAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pObjeto;
                if (pDatosAuditoria.ProgramaGeneraLog)
                {
                    pDatosAuditoria.Tabla = pDbCommand.CommandText;
                    pDatosAuditoria.ParametrosOperacion = "";
                    Type myType = pObjeto.GetType();
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    foreach (PropertyInfo prop in props)
                    {
                        pDatosAuditoria.ParametrosOperacion += String.Format("[{0} - {1}] ", prop.Name, prop.GetValue(pObjeto, null));
                        if (prop.Name == "Id" + myType.Name)
                            pDatosAuditoria.IdRegistro = (int)prop.GetValue(pObjeto, null);
                    }

                    Database vDataBase = DatabaseFactory.CreateDatabase("AuditaConnectionString");
                    using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Aud_InsertarLogAuditoria"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@pUsuario", DbType.String, pDatosAuditoria.Usuario);
                        vDataBase.AddInParameter(vDbCommand, "@pPrograma", DbType.String, pDatosAuditoria.Programa);
                        vDataBase.AddInParameter(vDbCommand, "@pOperacion", DbType.String, pDatosAuditoria.Operacion);
                        vDataBase.AddInParameter(vDbCommand, "@pParametrosOperacion", DbType.String, pDatosAuditoria.ParametrosOperacion);
                        vDataBase.AddInParameter(vDbCommand, "@pTabla", DbType.String, pDatosAuditoria.Tabla);
                        vDataBase.AddInParameter(vDbCommand, "@pIdRegistro", DbType.Int64, pDatosAuditoria.IdRegistro);
                        vDataBase.AddInParameter(vDbCommand, "@pDireccionIp", DbType.String, pDatosAuditoria.DireccionIP);
                        vDataBase.AddInParameter(vDbCommand, "@pNavegador", DbType.String, pDatosAuditoria.Navegador);
                        vDataBase.ExecuteNonQuery(vDbCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                //Generar log en visor de eventos del sistema
            }
        }
        public List<Auditoria> ConsultarAuditoria(String pNombrePrograma, Double pIdRegistro)
        {
            try
            {
                Database vDataBase = DatabaseFactory.CreateDatabase("AuditaConnectionString");
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarAuditoria"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pNombrePrograma", DbType.String, pNombrePrograma);
                    vDataBase.AddInParameter(vDbCommand, "@pIdRegistro", DbType.Double, pIdRegistro);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Auditoria> vListaAuditoria = new List<Auditoria>();
                        while (vDataReaderResults.Read())
                        {
                            Auditoria vAuditoria = new Auditoria();
                            vAuditoria.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : vAuditoria.Fecha;
                            vAuditoria.NombreUsuario = vDataReaderResults["NombreUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreUsuario"].ToString()) : vAuditoria.NombreUsuario;
                            vAuditoria.Operacion = vDataReaderResults["Operacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Operacion"].ToString()) : vAuditoria.Operacion;
                            vAuditoria.ParametrosOperacion = vDataReaderResults["ParametrosOperacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ParametrosOperacion"].ToString()) : vAuditoria.ParametrosOperacion;
                            vAuditoria.Tabla = vDataReaderResults["Tabla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tabla"].ToString()) : vAuditoria.Tabla;
                            vAuditoria.DireccionIp = vDataReaderResults["DireccionIp"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionIp"].ToString()) : vAuditoria.DireccionIp;
                            vAuditoria.Navegador = vDataReaderResults["Navegador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Navegador"]) : vAuditoria.Navegador;
                            vListaAuditoria.Add(vAuditoria);
                        }
                        return vListaAuditoria;
                    }
                }
            }
            catch (Exception ex)
            {
                //Generar log en visor de eventos del sistema
                return null;
            }
        }
    }
}
