﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess.cjr
{
    public class CabInfProcesosDAL : GeneralDAL
    {

        public int Guarda(Icbf.ComparadorJuridico.Entity.cjr.CabInfProcesos  pCabinfProcesos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spCabInfProcesosGuarda]"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@parIdCabInfProcesos", DbType.Int32, pCabinfProcesos.cipIdCabInfProcesos);
                    vDbCommand.Parameters["@parIdCabInfProcesos"].Direction = ParameterDirection.InputOutput;
                    vDataBase.AddInParameter(vDbCommand, "@parNomArch", DbType.String,pCabinfProcesos.cipNomArch );
                    vDataBase.AddInParameter(vDbCommand, "@parIdAno", DbType.Int32, pCabinfProcesos.cipIdAno );
                    vDataBase.AddInParameter(vDbCommand, "@parIdMes", DbType.Int32, pCabinfProcesos.cipIdMes );
                    vDataBase.AddInParameter(vDbCommand, "@parIdEstado", DbType.Int32, pCabinfProcesos.cipIdEstado );
                    vDataBase.AddInParameter(vDbCommand, "@parAudIdUsuario", DbType.Int32, pCabinfProcesos.cipAudIdUsuario);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    pCabinfProcesos.cipIdCabInfProcesos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@parIdCabInfProcesos").ToString());
                    //GenerarLogAuditoria(pTopesOperativos, vDbCommand);
                    return pCabinfProcesos.cipIdCabInfProcesos;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int GuardaDet(dtsInfProcesos.dtInfProcesosDataTable pDtTable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                System.Data.SqlClient.SqlBulkCopy oBk = new System.Data.SqlClient.SqlBulkCopy(vDataBase.ConnectionString );
                oBk.DestinationTableName = "CJR.DetTmpInfProcesos";
                oBk.WriteToServer(pDtTable);
                oBk.Close();

                return pDtTable.Rows.Count;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void ActualizaCarga(int pIdCabInfProcesos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spCabInfProcesosActCarga]"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@parIdCabInfProcesos", DbType.Int32, pIdCabInfProcesos);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataTable ConsInfProcesoXAnoMes(int parAno, int parMes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spCabInfProcConsulta]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@parIdAno", DbType.Int32, parAno);
                    vDataBase.AddInParameter(vDbCommand, "@parIdMes", DbType.Int32, parMes);
                    return vDataBase.ExecuteDataSet(vDbCommand).Tables[0];
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataTable DetTmpInfProcesosConsulta(int parIdCabInfProcesos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spDetTmpInfProcesosConsulta]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@parIdCabInfProcesos", DbType.Int32, parIdCabInfProcesos);
                    return vDataBase.ExecuteDataSet(vDbCommand).Tables[0];
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
          