﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess.cjr
{
    public class CabSIIFSaldosDAL : GeneralDAL
    {

        public int Guarda(Icbf.ComparadorJuridico.Entity.cjr.CabSIIFSaldos pCabSaldosSIIF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spCabSaldosSIIFGuarda]"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@parIdCabSaldosSIIF", DbType.Int32, pCabSaldosSIIF.csfIdCabSaldosSIIF);
                    vDbCommand.Parameters["@parIdCabSaldosSIIF"].Direction = ParameterDirection.InputOutput;
                    vDataBase.AddInParameter(vDbCommand, "@parNomArh", DbType.String, pCabSaldosSIIF.csfNomArh);
                    vDataBase.AddInParameter(vDbCommand, "@parIdAno", DbType.Int32, pCabSaldosSIIF.csfIdAno );
                    vDataBase.AddInParameter(vDbCommand, "@parIdMes", DbType.Int32, pCabSaldosSIIF.csfIdMes );
                    vDataBase.AddInParameter(vDbCommand, "@parIdEstado", DbType.Int32, pCabSaldosSIIF.csfIdEstado);
                    vDataBase.AddInParameter(vDbCommand, "@parAudIdUsuario", DbType.Int32, pCabSaldosSIIF.csfAudIdUsuario);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    pCabSaldosSIIF.csfIdCabSaldosSIIF = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@parIdCabSaldosSIIF").ToString());
                    //GenerarLogAuditoria(pTopesOperativos, vDbCommand);
                    return pCabSaldosSIIF.csfIdCabSaldosSIIF;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int GuardaDet(dtsSIIFSaldos.dtSIIFSaldosDataTable  pDtTable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                System.Data.SqlClient.SqlBulkCopy oBk = new System.Data.SqlClient.SqlBulkCopy(vDataBase.ConnectionString);
                oBk.DestinationTableName = "CJR.DetTmpSIIF";
                oBk.WriteToServer(pDtTable);
                oBk.Close();

                return pDtTable.Rows.Count;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void ActualizaCarga(int pIdCabInfProcesos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spCabSaldosSIIFActCarga]"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@parIdCabSaldosSIIF", DbType.Int32, pIdCabInfProcesos);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataTable ConsSIIFSaldos(int parAno, int parMes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spCabSaldosSIIFConsulta]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@parIdAno", DbType.Int32, parAno);
                    vDataBase.AddInParameter(vDbCommand, "@parIdMes", DbType.Int32, parMes);
                    return vDataBase.ExecuteDataSet(vDbCommand).Tables[0];
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataTable ConsDetSIIFSaldos(int parIdCabSaldoSIIF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spDetSaldosSIIFConsulta]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@parIdCabSaldoSIIF", DbType.Int32, parIdCabSaldoSIIF);
                    return vDataBase.ExecuteDataSet(vDbCommand).Tables[0];
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
