﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Icbf.ComparadorJuridico.DataAccess.cjr
{
    public class ComparaSaldosJurDAL : GeneralDAL
    {

        public DataTable GetTblComparaSaldosJur(int parIdAno, int parIdMes, int parRegional, string parCuenta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[spComparaSaldosJuridico]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@parIdAno", DbType.Int32, parIdAno);
                    vDataBase.AddInParameter(vDbCommand, "@parIdMes", DbType.Int32, parIdMes);
                    vDataBase.AddInParameter(vDbCommand, "@parIdRegional", DbType.Int32, parRegional);
                    vDataBase.AddInParameter(vDbCommand, "@parStrCuenta", DbType.String, parCuenta);


                    return vDataBase.ExecuteDataSet (vDbCommand).Tables[0];
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
