using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class IcbRegionalDAL : GeneralDAL
    {
        public IcbRegionalDAL()
        {
        }
        public int InsertarIcbRegional(IcbRegional pIcbRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_IcbRegional_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@RegIdRegional", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@RegNombre", DbType.String, pIcbRegional.RegNombre);
                    vDataBase.AddInParameter(vDbCommand, "@RegCodReg", DbType.String, pIcbRegional.RegCodReg);
                    vDataBase.AddInParameter(vDbCommand, "@RegCnnDb", DbType.String, pIcbRegional.RegCnnDb);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pIcbRegional.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pIcbRegional.RegIdRegional = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@RegIdRegional").ToString());
                    GenerarLogAuditoria(pIcbRegional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarIcbRegional(IcbRegional pIcbRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_IcbRegional_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@RegIdRegional", DbType.Int32, pIcbRegional.RegIdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@RegNombre", DbType.String, pIcbRegional.RegNombre);
                    vDataBase.AddInParameter(vDbCommand, "@RegCodReg", DbType.String, pIcbRegional.RegCodReg);
                    vDataBase.AddInParameter(vDbCommand, "@RegCnnDb", DbType.String, pIcbRegional.RegCnnDb);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pIcbRegional.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pIcbRegional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarIcbRegional(IcbRegional pIcbRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_IcbRegional_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@RegIdRegional", DbType.Int32, pIcbRegional.RegIdRegional);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pIcbRegional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public IcbRegional ConsultarIcbRegional(int pRegIdRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_IcbRegional_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@RegIdRegional", DbType.Int32, pRegIdRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        IcbRegional vIcbRegional = new IcbRegional();
                        while (vDataReaderResults.Read())
                        {
                            vIcbRegional.RegIdRegional = vDataReaderResults["RegIdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegIdRegional"].ToString()) : vIcbRegional.RegIdRegional;
                            vIcbRegional.RegNombre = vDataReaderResults["RegNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegNombre"].ToString()) : vIcbRegional.RegNombre;
                            vIcbRegional.RegCodReg = vDataReaderResults["RegCodReg"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegCodReg"].ToString()) : vIcbRegional.RegCodReg;
                            vIcbRegional.RegCnnDb = vDataReaderResults["RegCnnDb"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegCnnDb"].ToString()) : vIcbRegional.RegCnnDb;
                            vIcbRegional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vIcbRegional.UsuarioCrea;
                            vIcbRegional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vIcbRegional.FechaCrea;
                            vIcbRegional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vIcbRegional.UsuarioModifica;
                            vIcbRegional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vIcbRegional.FechaModifica;
                        }
                        return vIcbRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<IcbRegional> ConsultarIcbRegionals(String pRegNombre, String pRegCodReg, String pRegCnnDb)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_IcbRegionals_Consultar"))
                {
                    if(pRegNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@RegNombre", DbType.String, pRegNombre);
                    if(pRegCodReg != null)
                         vDataBase.AddInParameter(vDbCommand, "@RegCodReg", DbType.String, pRegCodReg);
                    if(pRegCnnDb != null)
                         vDataBase.AddInParameter(vDbCommand, "@RegCnnDb", DbType.String, pRegCnnDb);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<IcbRegional> vListaIcbRegional = new List<IcbRegional>();
                        while (vDataReaderResults.Read())
                        {
                                IcbRegional vIcbRegional = new IcbRegional();
                            vIcbRegional.RegIdRegional = vDataReaderResults["RegIdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["RegIdRegional"].ToString()) : vIcbRegional.RegIdRegional;
                            vIcbRegional.RegNombre = vDataReaderResults["RegNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegNombre"].ToString().ToUpper()) : vIcbRegional.RegNombre;
                            vIcbRegional.RegCodReg = vDataReaderResults["RegCodReg"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegCodReg"].ToString()) : vIcbRegional.RegCodReg;
                            vIcbRegional.RegCnnDb = vDataReaderResults["RegCnnDb"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegCnnDb"].ToString()) : vIcbRegional.RegCnnDb;

                                vListaIcbRegional.Add(vIcbRegional);
                        }
                        return vListaIcbRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
