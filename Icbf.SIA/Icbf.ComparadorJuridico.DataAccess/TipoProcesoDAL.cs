namespace Icbf.ComparadorJuridico.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Entity;
    using Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    public class TipoProcesoDAL : GeneralDAL
    {
        public TipoProcesoDAL()
        {
        }
        public List<TipoProceso> ConsultarTodosTipoProceso()
        {
            try
            {
                List<TipoProceso> oList = new List<TipoProceso>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("CJR.usp_ICBF_CJR_TipoProceso_ConsultarTodos"))
                {
                    IDataReader oIdr = vDataBase.ExecuteReader(vDbCommand);
                    if (oIdr != null)
                    {
                        while (oIdr.Read())
                            oList.Add(new TipoProceso() { IdTipoProceso = oIdr.GetInt32(0), NombreTipoProcesol = oIdr.GetString(1) });
                        return oList;
                    }
                    else
                        return null;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
