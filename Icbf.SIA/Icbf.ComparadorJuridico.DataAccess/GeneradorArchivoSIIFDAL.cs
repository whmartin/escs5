using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad GeneradorArchivoSIIF
    /// </summary>
    public class GeneradorArchivoSIIFDAL : GeneralDAL
    {
        public GeneradorArchivoSIIFDAL()
        {
        }
        ///// <summary>
        ///// Método de inserción para la entidad GeneradorArchivoSIIF
        ///// </summary>
        ///// <param name="pGeneradorArchivoSIIF"></param>
        //public int InsertarGeneradorArchivoSIIF(GeneradorArchivoSIIF pGeneradorArchivoSIIF)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_GeneradorArchivoSIIF_Insertar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddOutParameter(vDbCommand, "@IdGeneradorArchivoSIIF", DbType.Int32, 18);
        //            vDataBase.AddInParameter(vDbCommand, "@TipoArchivo", DbType.String, pGeneradorArchivoSIIF.TipoArchivo);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaGeneracion", DbType.DateTime, pGeneradorArchivoSIIF.FechaGeneracion);
        //            vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pGeneradorArchivoSIIF.NombreArchivo);
        //            vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pGeneradorArchivoSIIF.IdRegional);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pGeneradorArchivoSIIF.FechaInicio);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pGeneradorArchivoSIIF.FechaFin);
        //            vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pGeneradorArchivoSIIF.UsuarioCrea);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            pGeneradorArchivoSIIF.IdGeneradorArchivoSIIF = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdGeneradorArchivoSIIF").ToString());
        //            GenerarLogAuditoria(pGeneradorArchivoSIIF, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //         throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de modificación para la entidad GeneradorArchivoSIIF
        ///// </summary>
        ///// <param name="pGeneradorArchivoSIIF"></param>
        //public int ModificarGeneradorArchivoSIIF(GeneradorArchivoSIIF pGeneradorArchivoSIIF)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_GeneradorArchivoSIIF_Modificar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddInParameter(vDbCommand, "@IdGeneradorArchivoSIIF", DbType.Int32, pGeneradorArchivoSIIF.IdGeneradorArchivoSIIF);
        //            vDataBase.AddInParameter(vDbCommand, "@TipoArchivo", DbType.String, pGeneradorArchivoSIIF.TipoArchivo);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaGeneracion", DbType.DateTime, pGeneradorArchivoSIIF.FechaGeneracion);
        //            vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pGeneradorArchivoSIIF.NombreArchivo);
        //            vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pGeneradorArchivoSIIF.IdRegional);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pGeneradorArchivoSIIF.FechaInicio);
        //            vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pGeneradorArchivoSIIF.FechaFin);
        //            vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pGeneradorArchivoSIIF.UsuarioModifica);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            GenerarLogAuditoria(pGeneradorArchivoSIIF, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //         throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de eliminación para la entidad GeneradorArchivoSIIF
        ///// </summary>
        ///// <param name="pGeneradorArchivoSIIF"></param>
        //public int EliminarGeneradorArchivoSIIF(GeneradorArchivoSIIF pGeneradorArchivoSIIF)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_GeneradorArchivoSIIF_Eliminar"))
        //        {
        //            int vResultado;
        //            vDataBase.AddInParameter(vDbCommand, "@IdGeneradorArchivoSIIF", DbType.Int32, pGeneradorArchivoSIIF.IdGeneradorArchivoSIIF);
        //            vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
        //            GenerarLogAuditoria(pGeneradorArchivoSIIF, vDbCommand);
        //            return vResultado;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //         throw new GenericException(ex);
        //    }
        //}


        ///// <summary>
        ///// Método de consulta por id para la entidad GeneradorArchivoSIIF
        ///// </summary>
        ///// <param name="pIdGeneradorArchivoSIIF"></param>
        //public GeneradorArchivoSIIF ConsultarGeneradorArchivoSIIF(int pIdGeneradorArchivoSIIF)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_GeneradorArchivoSIIF_Consultar"))
        //        {
        //            vDataBase.AddInParameter(vDbCommand, "@IdGeneradorArchivoSIIF", DbType.Int32, pIdGeneradorArchivoSIIF);
        //            using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
        //            {
        //                GeneradorArchivoSIIF vGeneradorArchivoSIIF = new GeneradorArchivoSIIF();
        //                while (vDataReaderResults.Read())
        //                {
        //                    vGeneradorArchivoSIIF.IdGeneradorArchivoSIIF = vDataReaderResults["IdGeneradorArchivoSIIF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGeneradorArchivoSIIF"].ToString()) : vGeneradorArchivoSIIF.IdGeneradorArchivoSIIF;
        //                    vGeneradorArchivoSIIF.TipoArchivo = vDataReaderResults["TipoArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoArchivo"].ToString()) : vGeneradorArchivoSIIF.TipoArchivo;
        //                    vGeneradorArchivoSIIF.FechaGeneracion = vDataReaderResults["FechaGeneracion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaGeneracion"].ToString()) : vGeneradorArchivoSIIF.FechaGeneracion;
        //                    vGeneradorArchivoSIIF.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vGeneradorArchivoSIIF.NombreArchivo;
        //                    vGeneradorArchivoSIIF.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vGeneradorArchivoSIIF.IdRegional;
        //                    vGeneradorArchivoSIIF.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vGeneradorArchivoSIIF.FechaInicio;
        //                    vGeneradorArchivoSIIF.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vGeneradorArchivoSIIF.FechaFin;
        //                    vGeneradorArchivoSIIF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGeneradorArchivoSIIF.UsuarioCrea;
        //                    vGeneradorArchivoSIIF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGeneradorArchivoSIIF.FechaCrea;
        //                    vGeneradorArchivoSIIF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGeneradorArchivoSIIF.UsuarioModifica;
        //                    vGeneradorArchivoSIIF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGeneradorArchivoSIIF.FechaModifica;
        //                }
        //                return vGeneradorArchivoSIIF;
        //            }
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Método de consulta por filtros para la entidad GeneradorArchivoSIIF
        /// </summary>
        /// <param name="pTipoArchivo"></param>
        /// <param name="pFechaGeneracion"></param>
        /// <param name="pNombreArchivo"></param>
        /// <param name="pIdRegional"></param>
        /// <param name="pFechaInicio"></param>
        /// <param name="pFechaFin"></param>
        public List<GeneradorArchivoSIIF> ConsultarGeneradorArchivoSIIFs(String pTipoArchivo, DateTime? pFechaGeneracion, String pNombreArchivo, int? pIdRegional, DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_GeneradorArchivoSIIFs_Consultar"))
                {
                    if (pTipoArchivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoArchivo", DbType.String, pTipoArchivo);
                    if (pFechaGeneracion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaGeneracion", DbType.DateTime, pFechaGeneracion);
                    if (pNombreArchivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pNombreArchivo);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pFechaInicio != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFechaInicio);
                    if (pFechaFin != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pFechaFin);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GeneradorArchivoSIIF> vListaGeneradorArchivoSIIF = new List<GeneradorArchivoSIIF>();
                        while (vDataReaderResults.Read())
                        {
                            GeneradorArchivoSIIF vGeneradorArchivoSIIF = new GeneradorArchivoSIIF();
                            vGeneradorArchivoSIIF.IdGeneradorArchivoSIIF = vDataReaderResults["IdGeneradorArchivoSIIF"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGeneradorArchivoSIIF"].ToString()) : vGeneradorArchivoSIIF.IdGeneradorArchivoSIIF;
                            vGeneradorArchivoSIIF.TipoArchivo = vDataReaderResults["TipoArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoArchivo"].ToString()) : vGeneradorArchivoSIIF.TipoArchivo;
                            vGeneradorArchivoSIIF.FechaGeneracion = vDataReaderResults["FechaGeneracion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaGeneracion"].ToString()) : vGeneradorArchivoSIIF.FechaGeneracion;
                            vGeneradorArchivoSIIF.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vGeneradorArchivoSIIF.NombreArchivo;
                            vGeneradorArchivoSIIF.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vGeneradorArchivoSIIF.IdRegional;
                            vGeneradorArchivoSIIF.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vGeneradorArchivoSIIF.FechaInicio;
                            vGeneradorArchivoSIIF.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vGeneradorArchivoSIIF.FechaFin;
                            vGeneradorArchivoSIIF.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vGeneradorArchivoSIIF.UsuarioCrea;
                            vGeneradorArchivoSIIF.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGeneradorArchivoSIIF.FechaCrea;
                            vGeneradorArchivoSIIF.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vGeneradorArchivoSIIF.UsuarioModifica;
                            vGeneradorArchivoSIIF.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGeneradorArchivoSIIF.FechaModifica;
                            vListaGeneradorArchivoSIIF.Add(vGeneradorArchivoSIIF);
                        }
                        return vListaGeneradorArchivoSIIF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad GeneradorArchivoSIIF
        /// </summary>
        public DataSet ConsultarRegistrosAGenerarSiiF(DateTime pFechaInicialMov, DateTime pFechaFinMov, int pIdRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CJR_ConsultaRegistroGenerar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@FechaMovimientoIni", DbType.DateTime, pFechaInicialMov);
                    vDataBase.AddInParameter(vDbCommand, "@FechaMovimientoFin", DbType.DateTime, pFechaFinMov);
                    vDataBase.AddInParameter(vDbCommand, "@parIdRegional", DbType.Int32, pIdRegional);
                    

                    //vDbCommand.CommandTimeout = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["CommandTimeout"]);
                    DataSet vResultado = vDataBase.ExecuteDataSet(vDbCommand);
                    return vResultado;

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataSet GeneracionArchivoJuridica(DateTime pFechaInicialMov, DateTime pFechaFinMov,int pIdRegional, string usuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("spGeneraPlanoParametrizador"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@FechaMovimientoIni", DbType.DateTime, pFechaInicialMov);
                    vDataBase.AddInParameter(vDbCommand, "@FechaMovimientoFin", DbType.DateTime, pFechaFinMov);
                    vDataBase.AddInParameter(vDbCommand, "@parIdRegional", DbType.Int32, pIdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);

                    //vDbCommand.CommandTimeout = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["CommandTimeout"]);
                    DataSet vResultado = vDataBase.ExecuteDataSet(vDbCommand);
                    return vResultado;

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
