using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class VigenciaDAL : GeneralDAL
    {
        public VigenciaDAL()
        {
        }
        
        public Vigencia ConsultarVigencia(int pIdVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_Global_Vigencia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Vigencia vVigencia = new Vigencia();
                        while (vDataReaderResults.Read())
                        {                            
                            vVigencia.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vVigencia.IdVigencia;
                            vVigencia.Ano = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vVigencia.Ano;
                            vVigencia.IdMes = vDataReaderResults["IdMes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMes"].ToString()) : vVigencia.IdMes;
                            vVigencia.Mes = vDataReaderResults["Mes"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Mes"].ToString()) : vVigencia.Mes;
                            vVigencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVigencia.UsuarioCrea;
                            vVigencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVigencia.FechaCrea;
                            vVigencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVigencia.UsuarioModifica;
                            vVigencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVigencia.FechaModifica;
                            vVigencia.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaInicio"].ToString()) : vVigencia.FechaInicio;
                            vVigencia.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaFin"].ToString()) : vVigencia.FechaFin;
                        }
                        return vVigencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Vigencia> ConsultarVigencias(int? pVigencia, int? pIdMes, String pMes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_Global_Vigencias_Consultar"))
                {
                    if(pVigencia != null)
                         vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pVigencia);
                    if(pIdMes != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdMes", DbType.Int32, pIdMes);
                    if(pMes != null)
                         vDataBase.AddInParameter(vDbCommand, "@Mes", DbType.String, pMes);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Vigencia> vListaVigencia = new List<Vigencia>();
                        while (vDataReaderResults.Read())
                        {
                                Vigencia vVigencia = new Vigencia();
                            vVigencia.VigenciaConcatenada = vDataReaderResults["VigenciaConcatenada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaConcatenada"].ToString().ToUpper()) : vVigencia.VigenciaConcatenada;
                            vVigencia.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vVigencia.IdVigencia;
                            vVigencia.Ano = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vVigencia.Ano;
                            vVigencia.IdMes = vDataReaderResults["IdMes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMes"].ToString()) : vVigencia.IdMes;
                            vVigencia.Mes = vDataReaderResults["Mes"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Mes"].ToString()) : vVigencia.Mes;
                            vVigencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVigencia.UsuarioCrea;
                            vVigencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVigencia.FechaCrea;
                            vVigencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVigencia.UsuarioModifica;
                            vVigencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVigencia.FechaModifica;
                            vVigencia.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaInicio"].ToString()) : vVigencia.FechaInicio;
                            vVigencia.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaFin"].ToString()) : vVigencia.FechaFin;
                                vListaVigencia.Add(vVigencia);
                        }
                        return vListaVigencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta todas las vigencias
        /// </summary>
        /// <returns>Lista de Vigencias</returns>
        public List<Vigencia> ConsultarVigenciass()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_Global_Vigencias_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Vigencia> vListaVigencia = new List<Vigencia>();
                        while (vDataReaderResults.Read())
                        {
                            Vigencia vVigencia = new Vigencia();
                            vVigencia.VigenciaConcatenada = vDataReaderResults["VigenciaConcatenada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaConcatenada"].ToString().ToUpper()) : vVigencia.VigenciaConcatenada;
                            vVigencia.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vVigencia.IdVigencia;
                            vVigencia.Ano = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vVigencia.Ano;
                            vVigencia.IdMes = vDataReaderResults["IdMes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMes"].ToString()) : vVigencia.IdMes;
                            vVigencia.Mes = vDataReaderResults["Mes"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Mes"].ToString()) : vVigencia.Mes;
                            vVigencia.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaInicio"].ToString()) : vVigencia.FechaInicio;
                            vVigencia.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaFin"].ToString()) : vVigencia.FechaFin;
                            vVigencia.VigenciaActiva = vDataReaderResults["VigenciaActiva"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VigenciaActiva"].ToString()) : vVigencia.VigenciaActiva;
                            vListaVigencia.Add(vVigencia);
                        }

                        return vListaVigencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
