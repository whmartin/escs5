﻿//-----------------------------------------------------------------------
// <copyright file="ProcesosJuridicosDocumentosDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProcesosJuridicosDocumentosDAL.</summary>
// <author>Ingenian Software</author>
// <date>06/03/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo ProcesosJuridicosDocumentosDAL
    /// </summary>
    public class ProcesosJuridicosDocumentosDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor clase
        /// </summary>
        public ProcesosJuridicosDocumentosDAL()
        {

        }

        /// <summary>
        /// Elimina los documentos Asociados al Proceso Juridico
        /// </summary>
        /// <param name="pProcesosJuridicos">Parametro que envía el IdProcesosJuridicos</param>
        /// <returns>Resultado de la Operación</returns>
        public int EliminarProcesosJuridicosDocumentos(ProcesosJuridicosDocumentos pProcesosJuridicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_ProcesosJuridicosDocumentos_Eliminar]"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, pProcesosJuridicos.IdProcesosJuridicos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    this.GenerarLogAuditoria(pProcesosJuridicos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta los documentos Asociados al Proceso Juridico
        /// </summary>
        /// <param name="pProcesosJuridicos">Parametro de tipo Entidad ProcesosJuridicosDocumentos</param>
        /// <returns>Resultado de la Operación</returns>
        public int InsertarProcesosJuridicosDocumentos(ProcesosJuridicosDocumentos pProcesosJuridicos)
        {
            int vResultadoArchivo = 0;
            try
            {
                Database vDataBase = ObtenerInstancia();

                List<ProcesosJuridicosDocumentos> vListaOtrosDocumentos = pProcesosJuridicos.Documentos;
                foreach (ProcesosJuridicosDocumentos vOtrosDocumentos in vListaOtrosDocumentos)
                {
                    using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_ProcesosJuridicosDocumentos_Insertar]"))
                    {
                        vDataBase.AddOutParameter(vDbCommand, "@IdProcesosJuridicosDocumentos", DbType.Int32, 18);
                        vDataBase.AddInParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, pProcesosJuridicos.IdProcesosJuridicos);
                        vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, vOtrosDocumentos.NombreArchivo);
                        vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pProcesosJuridicos.IdProcesosJuridicos + @"\" + vOtrosDocumentos.NombreArchivo);
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pProcesosJuridicos.UsuarioCrea);

                        vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                        GenerarLogAuditoria(pProcesosJuridicos, vDbCommand);

                        //Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pProcesosJuridicos;
                        //vAuditorio.ProgramaGeneraLog = true;
                        //vAuditorio.Operacion = "INSERT";
                        //GenerarLogAuditoria(pProcesosJuridicos, vDbCommand);
                    }
                }

                return vResultadoArchivo;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta los Documentos x IdProcesoJuridico
        /// </summary>
        /// <param name="pIdProcesoJuridico"></param>
        /// <returns>Lista de Documentos x Proceso Juridico</returns>
        public List<ProcesosJuridicosDocumentos> ConsultaDocumentosProcesosJuridicos(int pIdProcesoJuridico)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_ProcesosJuridicosDocumentos_Consultar]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, pIdProcesoJuridico);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesosJuridicosDocumentos> vListaDocsProcesoJuridico = new List<ProcesosJuridicosDocumentos>();
                        while (vDataReaderResults.Read())
                        {
                            ProcesosJuridicosDocumentos vDocsProcesoJuridico = new ProcesosJuridicosDocumentos();
                            vDocsProcesoJuridico.IdProcesosJuridicosDocumentos = vDataReaderResults["IdProcesosJuridicosDocumentos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProcesosJuridicosDocumentos"].ToString()) : vDocsProcesoJuridico.IdProcesosJuridicosDocumentos;
                            vDocsProcesoJuridico.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocsProcesoJuridico.FechaCrea;
                            vDocsProcesoJuridico.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vDocsProcesoJuridico.RutaArchivo;
                            vDocsProcesoJuridico.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vDocsProcesoJuridico.NombreArchivo;
                            vDocsProcesoJuridico.ExisteArchivo = true;
                            vListaDocsProcesoJuridico.Add(vDocsProcesoJuridico);
                        }

                        return vListaDocsProcesoJuridico;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
