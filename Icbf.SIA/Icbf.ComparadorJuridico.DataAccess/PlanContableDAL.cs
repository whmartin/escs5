using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class PlanContableDAL : GeneralDAL
    {
        public PlanContableDAL()
        {
        }
        public int InsertarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_PlanContable_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPlanContable", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCuenta", DbType.Int32, pPlanContable.CodigoCuenta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreCuenta", DbType.String, pPlanContable.NombreCuenta);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSubcuenta", DbType.Int32, pPlanContable.CodigoSubcuenta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSubcuenta", DbType.String, pPlanContable.NombreSubcuenta);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTercerNivelCuenta", DbType.Int32, pPlanContable.CodigoTercerNivelCuenta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTercerNivelCuenta", DbType.String, pPlanContable.NombreTercerNivelCuenta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlanContable.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPlanContable.IdPlanContable = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPlanContable").ToString());
                    GenerarLogAuditoria(pPlanContable, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_PlanContable_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanContable", DbType.Int32, pPlanContable.IdPlanContable);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCuenta", DbType.Int32, pPlanContable.CodigoCuenta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreCuenta", DbType.String, pPlanContable.NombreCuenta);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSubcuenta", DbType.Int32, pPlanContable.CodigoSubcuenta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSubcuenta", DbType.String, pPlanContable.NombreSubcuenta);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTercerNivelCuenta", DbType.Int32, pPlanContable.CodigoTercerNivelCuenta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTercerNivelCuenta", DbType.String, pPlanContable.NombreTercerNivelCuenta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlanContable.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanContable, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_PlanContable_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanContable", DbType.Int32, pPlanContable.IdPlanContable);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlanContable, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public PlanContable ConsultarPlanContable(int pIdPlanContable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_PlanContable_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanContable", DbType.Int32, pIdPlanContable);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        PlanContable vPlanContable = new PlanContable();
                        while (vDataReaderResults.Read())
                        {
                            vPlanContable.IdPlanContable = vDataReaderResults["IdPlanContable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanContable"].ToString()) : vPlanContable.IdPlanContable;
                            vPlanContable.CodigoCuenta = vDataReaderResults["CodigoCuenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoCuenta"].ToString()) : vPlanContable.CodigoCuenta;
                            vPlanContable.NombreCuenta = vDataReaderResults["NombreCuenta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCuenta"].ToString()) : vPlanContable.NombreCuenta;
                            vPlanContable.CodigoSubcuenta = vDataReaderResults["CodigoSubcuenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoSubcuenta"].ToString()) : vPlanContable.CodigoSubcuenta;
                            vPlanContable.NombreSubcuenta = vDataReaderResults["NombreSubcuenta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSubcuenta"].ToString()) : vPlanContable.NombreSubcuenta;
                            vPlanContable.CodigoTercerNivelCuenta = vDataReaderResults["CodigoTercerNivelCuenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoTercerNivelCuenta"].ToString()) : vPlanContable.CodigoTercerNivelCuenta;
                            vPlanContable.NombreTercerNivelCuenta = vDataReaderResults["NombreTercerNivelCuenta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTercerNivelCuenta"].ToString()) : vPlanContable.NombreTercerNivelCuenta;
                            vPlanContable.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanContable.UsuarioCrea;
                            vPlanContable.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanContable.FechaCrea;
                            vPlanContable.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanContable.UsuarioModifica;
                            vPlanContable.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanContable.FechaModifica;
                        }
                        return vPlanContable;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlanContable> ConsultarPlanContables(int? pCodigoCuenta, String pNombreCuenta, int? pCodigoSubcuenta, String pNombreSubcuenta, int? pCodigoTercerNivelCuenta, String pNombreTercerNivelCuenta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_MAE_PlanContables_Consultar"))
                {
                    if(pCodigoCuenta != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoCuenta", DbType.Int32, pCodigoCuenta);
                    if(pNombreCuenta != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreCuenta", DbType.String, pNombreCuenta);
                    if(pCodigoSubcuenta != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoSubcuenta", DbType.Int32, pCodigoSubcuenta);
                    if(pNombreSubcuenta != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreSubcuenta", DbType.String, pNombreSubcuenta);
                    if(pCodigoTercerNivelCuenta != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTercerNivelCuenta", DbType.Int32, pCodigoTercerNivelCuenta);
                    if(pNombreTercerNivelCuenta != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTercerNivelCuenta", DbType.String, pNombreTercerNivelCuenta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<PlanContable> vListaPlanContable = new List<PlanContable>();
                        while (vDataReaderResults.Read())
                        {
                                PlanContable vPlanContable = new PlanContable();
                            vPlanContable.IdPlanContable = vDataReaderResults["IdPlanContable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanContable"].ToString()) : vPlanContable.IdPlanContable;
                            vPlanContable.ConcatenadoPlanContable = vDataReaderResults["ConcatenadoPlanContable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConcatenadoPlanContable"].ToString()) : vPlanContable.ConcatenadoPlanContable;
                            vPlanContable.CodigoCuenta = vDataReaderResults["CodigoCuenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoCuenta"].ToString()) : vPlanContable.CodigoCuenta;
                            vPlanContable.NombreCuenta = vDataReaderResults["NombreCuenta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCuenta"].ToString()) : vPlanContable.NombreCuenta;
                            vPlanContable.CodigoSubcuenta = vDataReaderResults["CodigoSubcuenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoSubcuenta"].ToString()) : vPlanContable.CodigoSubcuenta;
                            vPlanContable.NombreSubcuenta = vDataReaderResults["NombreSubcuenta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSubcuenta"].ToString()) : vPlanContable.NombreSubcuenta;
                            vPlanContable.CodigoTercerNivelCuenta = vDataReaderResults["CodigoTercerNivelCuenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoTercerNivelCuenta"].ToString()) : vPlanContable.CodigoTercerNivelCuenta;
                            vPlanContable.NombreTercerNivelCuenta = vDataReaderResults["NombreTercerNivelCuenta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTercerNivelCuenta"].ToString()) : vPlanContable.NombreTercerNivelCuenta;
                            vPlanContable.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlanContable.UsuarioCrea;
                            vPlanContable.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlanContable.FechaCrea;
                            vPlanContable.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlanContable.UsuarioModifica;
                            vPlanContable.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlanContable.FechaModifica;
                                vListaPlanContable.Add(vPlanContable);
                        }
                        return vListaPlanContable;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
