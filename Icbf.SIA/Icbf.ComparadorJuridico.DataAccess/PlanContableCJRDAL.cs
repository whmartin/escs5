﻿//-----------------------------------------------------------------------
// <copyright file="PlanContableCJRDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase PlanContableCJRDAL.</summary>
// <author>Ingenian Software</author>
// <date>10/04/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo PlanContableCJRDAL
    /// </summary>
    public class PlanContableCJRDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor clase
        /// </summary>
        public PlanContableCJRDAL()
        {

        }

        /// <summary>
        /// Consulta los datos del Plan Contable Concatenados
        /// </summary>
        /// <returns>Lista de Registros de Plan Contable Concatenados</returns>
        public List<PlanContableCJR> ConsultarPlanContableConcatenado()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_PlanContable_ConsultarConcatenado]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<PlanContableCJR> vPlanContablesCJR = new List<PlanContableCJR>();
                        while (vDataReaderResults.Read())
                        {
                            PlanContableCJR vPlanContableCJR = new PlanContableCJR();
                            vPlanContableCJR.IdPlanContable = vDataReaderResults["IdPlanContable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanContable"].ToString()) : vPlanContableCJR.IdPlanContable;
                            vPlanContableCJR.PlanContable = vDataReaderResults["PlanContable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PlanContable"].ToString()) : vPlanContableCJR.PlanContable;
                            vPlanContablesCJR.Add(vPlanContableCJR);
                        }

                        return vPlanContablesCJR;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
