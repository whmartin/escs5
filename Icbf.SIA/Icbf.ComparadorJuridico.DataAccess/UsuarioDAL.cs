﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class UsuarioDAL : GeneralDAL
    {
        public UsuarioDAL()
        {
        }
        public int InsertarUsuario(Usuario pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_InsertarUsuario"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdUsuario", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pUsuario.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pUsuario.NumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pUsuario.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pUsuario.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pUsuario.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pUsuario.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@TelefonoContacto", DbType.String, pUsuario.TelefonoContacto == null ? string.Empty : pUsuario.TelefonoContacto);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pUsuario.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pUsuario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@Providerkey", DbType.String, pUsuario.Providerkey);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pUsuario.UsuarioCreacion);

                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pUsuario.IdTipoUsuario == 0 ? 1 : pUsuario.IdTipoUsuario);

                    //AGV
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pUsuario.IdRegional);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pUsuario.IdUsuario = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdUsuario").ToString());
                    GenerarLogAuditoria(pUsuario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarUsuario(Usuario pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ModificarUsuario"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pUsuario.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pUsuario.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pUsuario.NumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pUsuario.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pUsuario.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pUsuario.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pUsuario.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pUsuario.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pUsuario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pUsuario.IdTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModificacion", DbType.String, pUsuario.UsuarioModificacion);

                    //AGV
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pUsuario.IdRegional);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUsuario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarUsuario(Usuario pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("spEliminarUsario"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pUsuario.IdUsuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUsuario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuario(string pProviderKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pProviderKey);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Usuario vUsuario = new Usuario();
                        while (vDataReaderResults.Read())
                        {
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vUsuario.IdTipoDocumento;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vUsuario.NumeroDocumento;
                            vUsuario.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vUsuario.PrimerNombre;
                            vUsuario.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vUsuario.SegundoNombre;
                            vUsuario.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vUsuario.PrimerApellido;
                            vUsuario.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vUsuario.SegundoApellido;
                            vUsuario.TelefonoContacto = vDataReaderResults["TelefonoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TelefonoContacto"].ToString()) : vUsuario.TelefonoContacto;
                            vUsuario.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vUsuario.CorreoElectronico;
                            vUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vUsuario.Estado;

                            vUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vUsuario.IdTipoUsuario;

                            vUsuario.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vUsuario.Providerkey;
                            vUsuario.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vUsuario.UsuarioCreacion;
                            vUsuario.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vUsuario.FechaCreacion;
                            vUsuario.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vUsuario.UsuarioModificacion;
                            vUsuario.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vUsuario.FechaModificacion;

                            //AGV
                            vUsuario.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vUsuario.NombreRegional;
                            vUsuario.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vUsuario.IdRegional;

                        }
                        return vUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuario(int pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ConsultarUsuario"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Usuario vUsuario = new Usuario();
                        while (vDataReaderResults.Read())
                        {
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vUsuario.IdTipoDocumento;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vUsuario.NumeroDocumento;
                            vUsuario.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vUsuario.PrimerNombre;
                            vUsuario.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vUsuario.SegundoNombre;
                            vUsuario.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vUsuario.PrimerApellido;
                            vUsuario.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vUsuario.SegundoApellido;
                            vUsuario.TelefonoContacto = vDataReaderResults["TelefonoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TelefonoContacto"].ToString()) : vUsuario.TelefonoContacto;
                            vUsuario.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vUsuario.CorreoElectronico;
                            vUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vUsuario.Estado;

                            vUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vUsuario.IdTipoUsuario;
                            
                            vUsuario.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vUsuario.Providerkey;
                            vUsuario.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vUsuario.UsuarioCreacion;
                            vUsuario.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vUsuario.FechaCreacion;
                            vUsuario.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vUsuario.UsuarioModificacion;
                            vUsuario.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vUsuario.FechaModificacion;

                            //AGV
                            vUsuario.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vUsuario.NombreRegional;
                            vUsuario.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vUsuario.IdRegional;
                        }
                        return vUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Usuario> ConsultarUsuarios(String pNumeroDocumento, String pPrimerNombre, String pPrimerApellido, int pPerfil, Boolean? pEstado, Int32? pTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ConsultarUsuarios"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPerfil", DbType.Int32, pPerfil);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pNumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pPrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pPrimerApellido);

                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);

                    if (pTipoUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pTipoUsuario);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Usuario> vListaUsuarios = new List<Usuario>();

                        while (vDataReaderResults.Read())
                        {
                            Usuario vUsuario = new Usuario();
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vUsuario.IdTipoDocumento;
                            vUsuario.TipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vUsuario.TipoDocumento;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vUsuario.NumeroDocumento;
                            vUsuario.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vUsuario.PrimerNombre;
                            vUsuario.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vUsuario.SegundoNombre;
                            vUsuario.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vUsuario.PrimerApellido;
                            vUsuario.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vUsuario.SegundoApellido;
                            vUsuario.TelefonoContacto = vDataReaderResults["TelefonoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TelefonoContacto"].ToString()) : vUsuario.TelefonoContacto;
                            vUsuario.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vUsuario.CorreoElectronico;
                            vUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vUsuario.Estado;

                            vUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vUsuario.IdTipoUsuario;

                            vUsuario.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vUsuario.Providerkey;
                            vUsuario.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vUsuario.UsuarioCreacion;
                            vUsuario.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vUsuario.FechaCreacion;
                            vUsuario.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vUsuario.UsuarioModificacion;
                            vUsuario.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vUsuario.FechaModificacion;

                            //AGV
                            vUsuario.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vUsuario.NombreRegional;
                            vUsuario.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vUsuario.IdRegional;

                            vListaUsuarios.Add(vUsuario);
                        }
                        return vListaUsuarios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuario(int pTipoDocumento, string pNumeroDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ConsultarUsuarioPorDocumento"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdTipoDocumento", DbType.Int32, pTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@pNumeroDocumento", DbType.String, pNumeroDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Usuario vUsuario = new Usuario();
                        while (vDataReaderResults.Read())
                        {
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vUsuario.IdTipoDocumento;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vUsuario.NumeroDocumento;
                            vUsuario.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vUsuario.PrimerNombre;
                            vUsuario.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vUsuario.SegundoNombre;
                            vUsuario.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vUsuario.PrimerApellido;
                            vUsuario.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vUsuario.SegundoApellido;
                            vUsuario.TelefonoContacto = vDataReaderResults["TelefonoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TelefonoContacto"].ToString()) : vUsuario.TelefonoContacto;
                            vUsuario.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vUsuario.CorreoElectronico;
                            vUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vUsuario.Estado;

                            vUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vUsuario.IdTipoUsuario;
                            
                            vUsuario.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vUsuario.Providerkey;
                            vUsuario.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vUsuario.UsuarioCreacion;
                            vUsuario.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vUsuario.FechaCreacion;
                            vUsuario.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vUsuario.UsuarioModificacion;
                            vUsuario.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vUsuario.FechaModificacion;

                            //AGV
                            vUsuario.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vUsuario.NombreRegional;
                            vUsuario.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vUsuario.IdRegional;

                        }
                        return vUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
