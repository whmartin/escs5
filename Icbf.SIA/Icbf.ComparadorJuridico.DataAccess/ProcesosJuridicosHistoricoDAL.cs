﻿//-----------------------------------------------------------------------
// <copyright file="ProcesosJuridicosHistoricoDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProcesosJuridicosHistoricoDAL.</summary>
// <author>Ingenian Software</author>
// <date>21/03/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo ProcesosJuridicosHistoricoDAL
    /// </summary>
    public class ProcesosJuridicosHistoricoDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor clase
        /// </summary>
        public ProcesosJuridicosHistoricoDAL()
        {

        }

        /// <summary>
        /// Método que consulta el historico de un Proceso Juridico
        /// </summary>
        /// <param name="pIdProcesosJuridicos">Id del dProceso Juridico</param>
        /// <returns>Lista de Registros Historicos del Proceso Jurídico</returns>
        public List<ProcesosJuridicosHistorico> ConsultarProcesosJuridicosDetalle(int pIdProcesosJuridicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_ProcesosJuridicosHistorico_Consultar]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProcesosJuridicos", DbType.Int32, pIdProcesosJuridicos);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesosJuridicosHistorico> vListaProcesosJuridicos = new List<ProcesosJuridicosHistorico>();
                        while (vDataReaderResults.Read())
                        {
                            ProcesosJuridicosHistorico vProcesosJuridicos = new ProcesosJuridicosHistorico();
                            vProcesosJuridicos.IdHistorico = vDataReaderResults["IdHistorico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistorico"].ToString()) : vProcesosJuridicos.IdHistorico;
                            vProcesosJuridicos.IdProcesosJuridicos = vDataReaderResults["IdProcesosJuridicos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProcesosJuridicos"].ToString()) : vProcesosJuridicos.IdProcesosJuridicos;
                            vProcesosJuridicos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProcesosJuridicos.IdRegional;
                            vProcesosJuridicos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProcesosJuridicos.IdVigencia;
                            vProcesosJuridicos.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vProcesosJuridicos.IdProceso;
                            vProcesosJuridicos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProcesosJuridicos.IdTercero;
                            vProcesosJuridicos.IdPlanContable = vDataReaderResults["IdPlanContable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanContable"].ToString()) : vProcesosJuridicos.IdPlanContable;
                            vProcesosJuridicos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vProcesosJuridicos.NumeroProceso;
                            vProcesosJuridicos.ValorProceso = vDataReaderResults["ValorProceso"] != DBNull.Value ? vDataReaderResults["ValorProceso"].ToString() : vProcesosJuridicos.ValorProceso;
                            vProcesosJuridicos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vProcesosJuridicos.Estado;
                            vProcesosJuridicos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProcesosJuridicos.UsuarioCrea;
                            vProcesosJuridicos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProcesosJuridicos.FechaCrea;
                            vProcesosJuridicos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProcesosJuridicos.UsuarioModifica;
                            vProcesosJuridicos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProcesosJuridicos.FechaModifica;

                            vProcesosJuridicos.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vProcesosJuridicos.Regional;
                            vProcesosJuridicos.VigenciaConcatenada = vDataReaderResults["VigenciaConcatenada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaConcatenada"].ToString()) : vProcesosJuridicos.VigenciaConcatenada;
                            vProcesosJuridicos.NombreTipoProceso = vDataReaderResults["NombreTipoProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoProceso"].ToString()) : vProcesosJuridicos.NombreTipoProceso;
                            vProcesosJuridicos.TerceroConcatenado = vDataReaderResults["TerceroConcatenado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TerceroConcatenado"].ToString()) : vProcesosJuridicos.TerceroConcatenado;
                            vProcesosJuridicos.ConcatenadoPlanContable = vDataReaderResults["ConcatenadoPlanContable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConcatenadoPlanContable"].ToString()) : vProcesosJuridicos.ConcatenadoPlanContable;
                            vProcesosJuridicos.Dado_de_Baja = vDataReaderResults["Dado_de_Baja"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dado_de_Baja"].ToString()) : vProcesosJuridicos.Dado_de_Baja;
                            vProcesosJuridicos.DescripcionNaturalezaProceso = vDataReaderResults["DescripcionNaturalezaProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionNaturalezaProceso"].ToString()) : vProcesosJuridicos.DescripcionNaturalezaProceso;
                            vProcesosJuridicos.DescripcionFaseProceso = vDataReaderResults["DescripcionFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionFaseProceso"].ToString()) : vProcesosJuridicos.DescripcionFaseProceso;
                            vProcesosJuridicos.DescripcionSubFaseProceso = vDataReaderResults["DescripcionSubFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionSubFaseProceso"].ToString()) : vProcesosJuridicos.DescripcionSubFaseProceso;
                            vProcesosJuridicos.FechaAdmisionICBF = vDataReaderResults["FechaAdmisionICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdmisionICBF"].ToString()) : vProcesosJuridicos.FechaAdmisionICBF;
                            vProcesosJuridicos.FechaAdmisionJuzgado = vDataReaderResults["FechaAdmisionJuzgado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdmisionJuzgado"].ToString()) : vProcesosJuridicos.FechaAdmisionJuzgado;
                            vProcesosJuridicos.FechaLog = vDataReaderResults["FechaLog"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLog"].ToString()) : vProcesosJuridicos.FechaLog;
                            vProcesosJuridicos.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vProcesosJuridicos.Observaciones;
                            vProcesosJuridicos.Juzgado = vDataReaderResults["Juzgado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Juzgado"].ToString()) : vProcesosJuridicos.Juzgado;
                            vProcesosJuridicos.Apoderado = vDataReaderResults["Apoderado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Apoderado"].ToString()) : vProcesosJuridicos.Apoderado;
                            vProcesosJuridicos.DescripcionPlanContable = vDataReaderResults["DescripcionPlanContable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionPlanContable"].ToString()) : vProcesosJuridicos.DescripcionPlanContable;
                            vListaProcesosJuridicos.Add(vProcesosJuridicos);
                        }

                        return vListaProcesosJuridicos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta el historico de un Proceso Juridico
        /// </summary>
        /// <param name="pIdProcesosJuridicos">Id del Historico</param>
        /// <returns>Lista de Registros Historicos</returns>
        public ProcesosJuridicosHistorico ConsultarProcesosJuridicosHistorico(int pIdHistorico)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_ProcesosJuridicosHistorico_ConsultarId]"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdHistorico", DbType.Int32, pIdHistorico);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ProcesosJuridicosHistorico vProcesosJuridicos = new ProcesosJuridicosHistorico();
                        while (vDataReaderResults.Read())
                        {
                            vProcesosJuridicos.IdProcesosJuridicos = vDataReaderResults["IdProcesosJuridicos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProcesosJuridicos"].ToString()) : vProcesosJuridicos.IdProcesosJuridicos;
                            vProcesosJuridicos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProcesosJuridicos.IdRegional;
                            vProcesosJuridicos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProcesosJuridicos.IdVigencia;
                            vProcesosJuridicos.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vProcesosJuridicos.IdProceso;
                            vProcesosJuridicos.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vProcesosJuridicos.IdTercero;
                            vProcesosJuridicos.IdPlanContable = vDataReaderResults["IdPlanContable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlanContable"].ToString()) : vProcesosJuridicos.IdPlanContable;
                            vProcesosJuridicos.NumeroProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroProceso"].ToString()) : vProcesosJuridicos.NumeroProceso;
                            vProcesosJuridicos.ValorProceso = vDataReaderResults["ValorProceso"] != DBNull.Value ? vDataReaderResults["ValorProceso"].ToString() : vProcesosJuridicos.ValorProceso;
                            vProcesosJuridicos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vProcesosJuridicos.Estado;
                            vProcesosJuridicos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProcesosJuridicos.UsuarioCrea;
                            vProcesosJuridicos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProcesosJuridicos.FechaCrea;
                            vProcesosJuridicos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProcesosJuridicos.UsuarioModifica;
                            vProcesosJuridicos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProcesosJuridicos.FechaModifica;

                            vProcesosJuridicos.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vProcesosJuridicos.Regional;
                            vProcesosJuridicos.VigenciaConcatenada = vDataReaderResults["VigenciaConcatenada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaConcatenada"].ToString()) : vProcesosJuridicos.VigenciaConcatenada;
                            vProcesosJuridicos.NombreTipoProceso = vDataReaderResults["NombreTipoProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoProceso"].ToString()) : vProcesosJuridicos.NombreTipoProceso;
                            vProcesosJuridicos.TerceroConcatenado = vDataReaderResults["TerceroConcatenado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TerceroConcatenado"].ToString()) : vProcesosJuridicos.TerceroConcatenado;
                            vProcesosJuridicos.ConcatenadoPlanContable = vDataReaderResults["ConcatenadoPlanContable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConcatenadoPlanContable"].ToString()) : vProcesosJuridicos.ConcatenadoPlanContable;
                            vProcesosJuridicos.Dado_de_Baja = vDataReaderResults["Dado_de_Baja"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dado_de_Baja"].ToString()) : vProcesosJuridicos.Dado_de_Baja;
                            vProcesosJuridicos.DescripcionNaturalezaProceso = vDataReaderResults["DescripcionNaturalezaProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionNaturalezaProceso"].ToString()) : vProcesosJuridicos.DescripcionNaturalezaProceso;
                            vProcesosJuridicos.Apoderado = vDataReaderResults["Apoderado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Apoderado"].ToString()) : vProcesosJuridicos.Apoderado;
                            vProcesosJuridicos.DescripcionFaseProceso = vDataReaderResults["DescripcionFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionFaseProceso"].ToString()) : vProcesosJuridicos.DescripcionFaseProceso;
                            vProcesosJuridicos.DescripcionSubFaseProceso = vDataReaderResults["DescripcionSubFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionSubFaseProceso"].ToString()) : vProcesosJuridicos.DescripcionSubFaseProceso;
                            vProcesosJuridicos.FechaAdmisionICBF = vDataReaderResults["FechaAdmisionICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdmisionICBF"].ToString()) : vProcesosJuridicos.FechaAdmisionICBF;
                            vProcesosJuridicos.FechaAdmisionJuzgado = vDataReaderResults["FechaAdmisionJuzgado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdmisionJuzgado"].ToString()) : vProcesosJuridicos.FechaAdmisionJuzgado;
                            vProcesosJuridicos.FechaLog = vDataReaderResults["FechaLog"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaLog"].ToString()) : vProcesosJuridicos.FechaLog;
                            vProcesosJuridicos.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vProcesosJuridicos.Observaciones;
                            vProcesosJuridicos.Juzgado = vDataReaderResults["Juzgado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Juzgado"].ToString()) : vProcesosJuridicos.Juzgado;
                            vProcesosJuridicos.DescripcionPlanContable = vDataReaderResults["DescripcionPlanContable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionPlanContable"].ToString()) : vProcesosJuridicos.DescripcionPlanContable;
                        }

                        return vProcesosJuridicos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
