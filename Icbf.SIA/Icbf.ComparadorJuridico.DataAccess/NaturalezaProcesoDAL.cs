﻿//-----------------------------------------------------------------------
// <copyright file="NaturalezaProcesoDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase NaturalezaProcesoDAL.</summary>
// <author>Ingenian Software</author>
// <date>18/01/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo NaturalezaProcesoDAL
    /// </summary>
    public class NaturalezaProcesoDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor clase
        /// </summary>
        public NaturalezaProcesoDAL()
        {

        }

        /// <summary>
        /// Consulta los datos de la Naturaleza del Proceso
        /// </summary>
        /// <param name="pCodigoNaturaleza">Código de la Naturaleza</param>
        /// <param name="pDescripcionNaturaleza">Descripción de la Naturaleza</param>        
        /// <param name="pEstado">Estado de la Naturaleza</param>
        /// <returns>Lista de Registros de Naturaleza del Proceso</returns>
        public List<NaturalezaProceso> ConsultarNaturalezaProceso(int? pIdNaturalezaProceso, string pCodigoNaturaleza, string pDescripcionNaturaleza, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_NaturalezaProceso_Consultar]"))
                {
                    if (pIdNaturalezaProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdNaturalezaProceso", DbType.Int32, pIdNaturalezaProceso);
                    }

                    if (!string.IsNullOrEmpty(pCodigoNaturaleza))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@CodigoNaturaleza", DbType.String, pCodigoNaturaleza);
                    }

                    if (!string.IsNullOrEmpty(pDescripcionNaturaleza))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@DescripcionNaturaleza", DbType.String, pDescripcionNaturaleza);
                    }

                    if (pEstado != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    }

                    

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NaturalezaProceso> vNaturalezaProcesos = new List<NaturalezaProceso>();
                        while (vDataReaderResults.Read())
                        {
                            NaturalezaProceso vNaturalezaProceso = new NaturalezaProceso();
                            vNaturalezaProceso.IdNaturalezaProceso = vDataReaderResults["IdNaturalezaProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNaturalezaProceso"].ToString()) : vNaturalezaProceso.IdNaturalezaProceso;
                            vNaturalezaProceso.CodigoNaturalezaProceso = vDataReaderResults["CodigoNaturalezaProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNaturalezaProceso"].ToString()) : vNaturalezaProceso.CodigoNaturalezaProceso;
                            vNaturalezaProceso.DescripcionNaturalezaProceso = vDataReaderResults["DescripcionNaturalezaProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionNaturalezaProceso"].ToString()) : vNaturalezaProceso.DescripcionNaturalezaProceso;                            
                            vNaturalezaProceso.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vNaturalezaProceso.Estado;
                            vNaturalezaProceso.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNaturalezaProceso.UsuarioCrea;
                            vNaturalezaProceso.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNaturalezaProceso.FechaCrea;
                            vNaturalezaProceso.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNaturalezaProceso.UsuarioModifica;
                            vNaturalezaProceso.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNaturalezaProceso.FechaModifica;
                            vNaturalezaProcesos.Add(vNaturalezaProceso);
                        }

                        return vNaturalezaProcesos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de Naturaleza Proceso
        /// </summary>
        /// <param name="pIdNaturaleza">Id de la Naturaleza del proceso</param>
        /// <param name="pDescripcionNaturalezaProceso">Nombre de la naturaleza del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarNaturalezaProcesoNombre(int? pIdNaturaleza, string pDescripcionNaturalezaProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_NaturalezaProceso_ConsultarDescripcion]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    if (pIdNaturaleza != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdNaturalezaProceso", DbType.Int32, pIdNaturaleza);
                    }

                    vDataBase.AddInParameter(vDbCommand, "@DescripcionNaturalezaProceso", DbType.String, pDescripcionNaturalezaProceso);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de Naturaleza del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarNaturalezaproceso(NaturalezaProceso pNaturalezaProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_NaturalezaProceso_Insertar]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;
                    vDataBase.AddInParameter(vDbCommand, "@CodigoNaturalezaProceso", DbType.String, pNaturalezaProceso.CodigoNaturalezaProceso);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionNaturalezaProceso", DbType.String, pNaturalezaProceso.DescripcionNaturalezaProceso);                    
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pNaturalezaProceso.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pNaturalezaProceso.UsuarioCrea);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria) pNaturalezaProceso;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }

                }
                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala modificación de Naturaleza del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarNaturalezaproceso(NaturalezaProceso pNaturalezaProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_NaturalezaProceso_Modificar]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@IdNaturalezaProceso", DbType.Int32, pNaturalezaProceso.IdNaturalezaProceso);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionNaturalezaProceso", DbType.String, pNaturalezaProceso.DescripcionNaturalezaProceso);                    
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pNaturalezaProceso.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pNaturalezaProceso.UsuarioModifica);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria) pNaturalezaProceso;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }

                }
                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la Naturaleza
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoNaturaleza()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                string vResultado = string.Empty;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_NaturalezaProceso_ObtenerCodigoNaturaleza]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        while (vDataReaderResults.Read())
                        {
                            NaturalezaProceso vNaturalezaProceso = new NaturalezaProceso();
                            vNaturalezaProceso.CodigoNaturalezaProceso = vDataReaderResults["CodigoNaturalezaProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNaturalezaProceso"].ToString()) : vNaturalezaProceso.CodigoNaturalezaProceso;
                            vResultado = vNaturalezaProceso.CodigoNaturalezaProceso;
                        }

                        return vResultado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la Naturaleza del Proceso Activos y concatenados por codigo y descripción
        /// </summary>
        /// <returns>Lista de Registros de Naturaleza del Proceso</returns>
        public List<NaturalezaProceso> ConsultarNaturalezaProcesoConcatenado()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_NaturalezaProceso_ConsultarConcatenado]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NaturalezaProceso> vNaturalezaProcesos = new List<NaturalezaProceso>();
                        while (vDataReaderResults.Read())
                        {
                            NaturalezaProceso vNaturalezaProceso = new NaturalezaProceso();
                            vNaturalezaProceso.IdNaturalezaProceso = vDataReaderResults["IdNaturalezaProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNaturalezaProceso"].ToString()) : vNaturalezaProceso.IdNaturalezaProceso;
                            vNaturalezaProceso.DescripcionNaturalezaProceso = vDataReaderResults["DescripcionNaturalezaProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionNaturalezaProceso"].ToString()) : vNaturalezaProceso.DescripcionNaturalezaProceso;
                            vNaturalezaProcesos.Add(vNaturalezaProceso);
                        }

                        return vNaturalezaProcesos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
