using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class TipoDocumentoDAL : GeneralDAL
    {
        public TipoDocumentoDAL()
        {
        }
        public int InsertarTipoDocumento(TipoDocumento pTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("spInsertarTipoDocumento"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pTipoDocumento.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTipoDocumento.Nombre);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoDocumento.IdTipoDocumento = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoDocumento").ToString());
                    GenerarLogAuditoria(pTipoDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTipoDocumento(TipoDocumento pTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("spModificarTipoDocumento"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTipoDocumento.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pTipoDocumento.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTipoDocumento.Nombre);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTipoDocumento(TipoDocumento pTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("spEliminarTipoDocumento"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTipoDocumento.IdTipoDocumento);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public TipoDocumento ConsultarTipoDocumento(int pIdTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_TipoDocumento_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Double, pIdTipoDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoDocumento vTipoDocumento = new TipoDocumento();
                        while (vDataReaderResults.Read())
                        {
                            vTipoDocumento.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumento.IdTipoDocumento;
                            vTipoDocumento.Codigo = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vTipoDocumento.Codigo;
                            vTipoDocumento.Nombre = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vTipoDocumento.Nombre;
                        }
                        return vTipoDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TipoDocumento> ConsultarTodosTipoDocumento()
        {
            try
            {
                List<TipoDocumento> oList = new List<TipoDocumento>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Global_Seg_ConsultarTodosTipoDocumento"))
                {
                    IDataReader oIdr = vDataBase.ExecuteReader(vDbCommand);
                    if (oIdr != null)
                    {
                        while (oIdr.Read())
                            oList.Add(new TipoDocumento() { IdTipoDocumento = oIdr.GetInt32(0), Codigo = oIdr.GetString(1), Nombre = oIdr.GetString(2) });
                        return oList;
                    }
                    else
                        return null;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
