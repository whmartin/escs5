﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class CierreVigenciaDAL : GeneralDAL
    {
        public CierreVigenciaDAL()
        {
        }
        public string CerrarVigencia(string NombreUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ComparadorJuridico_CerrarVigencia"))
                {
                    string vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@Resultado", DbType.String,500);
                    vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String,NombreUsuario);
                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand).ToString();
                    vResultado = vDataBase.GetParameterValue(vDbCommand, "@Resultado").ToString();
                    GenerarLogAuditoria(vResultado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

    }
}
