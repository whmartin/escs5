﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.ComparadorJuridico.DataAccess
{
    public class TipoUsuarioDAL : GeneralDAL
    {
        public TipoUsuarioDAL()
        {
        }
        public int InsertarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoUsuario", DbType.String, pTipoUsuario.CodigoTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoUsuario", DbType.String, pTipoUsuario.NombreTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pTipoUsuario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoUsuario.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoUsuario.IdTipoUsuario = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoUsuario").ToString());

                    GenerarLogAuditoria(pTipoUsuario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_Modificar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pTipoUsuario.IdTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoUsuario", DbType.String, pTipoUsuario.CodigoTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoUsuario", DbType.String, pTipoUsuario.NombreTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pTipoUsuario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoUsuario.UsuarioModifica);
                    GenerarLogAuditoria(pTipoUsuario, vDbCommand);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_Eliminar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pIdTipoUsuario);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public TipoUsuario ConsultarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pIdTipoUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoUsuario vTipoUsuario = new TipoUsuario();
                        while (vDataReaderResults.Read())
                        {
                            vTipoUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vTipoUsuario.IdTipoUsuario;
                            vTipoUsuario.CodigoTipoUsuario = vDataReaderResults["CodigoTipoUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoUsuario"].ToString()) : vTipoUsuario.CodigoTipoUsuario;
                            vTipoUsuario.NombreTipoUsuario = vDataReaderResults["NombreTipoUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoUsuario"].ToString()) : vTipoUsuario.NombreTipoUsuario;
                            vTipoUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vTipoUsuario.Estado;
                        }
                        return vTipoUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TipoUsuario> ConsultarTipoUsuarios(String pCodigoTipoUsuario, String pNombreTipoUsuario, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_sConsultar"))
                {
                    if (pCodigoTipoUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoTipoUsuario", DbType.String, pCodigoTipoUsuario);
                    if (pNombreTipoUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoUsuario", DbType.String, pNombreTipoUsuario);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoUsuario> vListaTipoUsuario = new List<TipoUsuario>();
                        while (vDataReaderResults.Read())
                        {
                            TipoUsuario vTipoUsuario = new TipoUsuario();
                            vTipoUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vTipoUsuario.IdTipoUsuario;
                            vTipoUsuario.CodigoTipoUsuario = vDataReaderResults["CodigoTipoUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoUsuario"].ToString()) : vTipoUsuario.CodigoTipoUsuario;
                            vTipoUsuario.NombreTipoUsuario = vDataReaderResults["NombreTipoUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoUsuario"].ToString()) : vTipoUsuario.NombreTipoUsuario;
                            vTipoUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vTipoUsuario.Estado;
                            vListaTipoUsuario.Add(vTipoUsuario);
                        }
                        return vListaTipoUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
