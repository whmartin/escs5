﻿//-----------------------------------------------------------------------
// <copyright file="FaseProcesoDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase FaseProcesoDAL.</summary>
// <author>Ingenian Software</author>
// <date>13/02/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Icbf.ComparadorJuridico.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.ComparadorJuridico.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo FaseProcesoDAL
    /// </summary>
    public class FaseProcesoDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor clase
        /// </summary>
        public FaseProcesoDAL()
        {

        }

        /// <summary>
        /// Consulta los datos de la Fase del Proceso
        /// </summary>
        /// <param name="pIdFaseProceso">Id de la Fase</param>
        /// <param name="pCodigoFaseProceso">Código de la Fase</param>
        /// <param name="pDescripcionFaseProceso">Descripción de la Fase</param>
        /// <param name="pEstado">Estado de la Fase</param>
        /// <returns>Lista de Registros de Fase del Proceso</returns>
        public List<FaseProceso> ConsultarFaseProceso(int? pIdFaseProceso, string pCodigoFaseProceso, string pDescripcionFaseProceso, int pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_FaseProceso_Consultar]"))
                {
                    if (pIdFaseProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdFaseProceso", DbType.Int32, pIdFaseProceso);
                    }

                    if (!string.IsNullOrEmpty(pCodigoFaseProceso))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@CodigoFaseProceso", DbType.String, pCodigoFaseProceso);
                    }

                    if (!string.IsNullOrEmpty(pDescripcionFaseProceso))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@DescripcionFaseProceso", DbType.String, pDescripcionFaseProceso);
                    }

                    if (pEstado != -1)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FaseProceso> vFaseProcesos = new List<FaseProceso>();
                        while (vDataReaderResults.Read())
                        {
                            FaseProceso vFaseProceso = new FaseProceso();
                            vFaseProceso.IdFaseProceso = vDataReaderResults["IdFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFaseProceso"].ToString()) : vFaseProceso.IdFaseProceso;
                            vFaseProceso.CodigoFaseProceso = vDataReaderResults["CodigoFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoFaseProceso"].ToString()) :  vFaseProceso.CodigoFaseProceso;
                            vFaseProceso.DescripcionFaseProceso = vDataReaderResults["DescripcionFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionFaseProceso"].ToString()) : vFaseProceso.DescripcionFaseProceso;
                            vFaseProceso.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vFaseProceso.Estado;
                            vFaseProceso.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFaseProceso.UsuarioCrea;
                            vFaseProceso.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFaseProceso.FechaCrea;
                            vFaseProceso.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFaseProceso.UsuarioModifica;
                            vFaseProceso.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFaseProceso.FechaModifica;
                            vFaseProcesos.Add(vFaseProceso);
                        }

                        return vFaseProcesos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de Fase Proceso
        /// </summary>
        /// <param name="pIdFaseProceso">Id de la Fase del proceso</param>
        /// <param name="pDescripcionFaseProceso">Nombre de la Fase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarFaseProcesoNombre(int? pIdFaseProceso, string pDescripcionFaseProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_FaseProceso_ConsultarDescripcion]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    if (pIdFaseProceso != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdFaseProceso", DbType.Int32, pIdFaseProceso);
                    }

                    vDataBase.AddInParameter(vDbCommand, "@DescripcionFaseProceso", DbType.String, pDescripcionFaseProceso);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de Fase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarFaseproceso(FaseProceso pFaseProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_FaseProceso_Insertar]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@CodigoFaseProceso", DbType.String, pFaseProceso.CodigoFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionFaseProceso", DbType.String, pFaseProceso.DescripcionFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pFaseProceso.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFaseProceso.UsuarioCrea);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);

                    if (vResultado > 0)
                    {
                        pFaseProceso.IdFaseProceso = vResultado;
                        pFaseProceso.FechaCrea = DateTime.Now;
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pFaseProceso;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala modificación de Fase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarFaseproceso(FaseProceso pFaseProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                int vResultado = 0;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_FaseProceso_Modificar]"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@IdFaseProceso", DbType.Int32, pFaseProceso.IdFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionFaseProceso", DbType.String, pFaseProceso.DescripcionFaseProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pFaseProceso.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFaseProceso.UsuarioModifica);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);

                    if (vResultado > 0)
                    {
                        pFaseProceso.FechaModifica = DateTime.Now;
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pFaseProceso;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }
                }

                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la Fase
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoFase()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                string vResultado = string.Empty;

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_FaseProceso_ObtenerCodigoFase]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        
                        while (vDataReaderResults.Read())
                        {
                            FaseProceso vFaseProceso = new FaseProceso();
                            vFaseProceso.CodigoFaseProceso = vDataReaderResults["CodigoFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoFaseProceso"].ToString()) : vFaseProceso.CodigoFaseProceso;
                            vResultado = vFaseProceso.CodigoFaseProceso;
                        }

                        return vResultado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la Fase del Proceso Contatenado Código-Descripción
        /// </summary>
        /// <returns>Lista de Registros de Fase del Proceso</returns>
        public List<FaseProceso> ConsultarFaseProcesoConcatenado()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CJR].[usp_INGENIAN_CJR_FaseProceso_ConsultarConcatenado]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FaseProceso> vFaseProcesos = new List<FaseProceso>();
                        while (vDataReaderResults.Read())
                        {
                            FaseProceso vFaseProceso = new FaseProceso();
                            vFaseProceso.IdFaseProceso = vDataReaderResults["IdFaseProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFaseProceso"].ToString()) : vFaseProceso.IdFaseProceso;
                            vFaseProceso.DescripcionFaseProceso = vDataReaderResults["DescripcionFaseProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionFaseProceso"].ToString()) : vFaseProceso.DescripcionFaseProceso;
                            vFaseProcesos.Add(vFaseProceso);
                        }

                        return vFaseProcesos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
