using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.DataAccess
{
    public class ContratoSiguvVehiculosDAL : GeneralDAL
    {
        public ContratoSiguvVehiculosDAL()
        {
        }
        public int InsertarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguvVehiculos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContratoSiguvVehiculos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pContratoSiguvVehiculos.IdContratoSiguv);
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculo", DbType.Int32, pContratoSiguvVehiculos.IdVehiculo);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pContratoSiguvVehiculos.IdRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@IdCentroZonal", DbType.Int32, pContratoSiguvVehiculos.IdCentroZonal);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContratoSiguvVehiculos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pContratoSiguvVehiculos.IdContratoSiguvVehiculos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContratoSiguvVehiculos").ToString());
                    GenerarLogAuditoria(pContratoSiguvVehiculos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguvVehiculos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguvVehiculos", DbType.Int32, pContratoSiguvVehiculos.IdContratoSiguvVehiculos);
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pContratoSiguvVehiculos.IdContratoSiguv);
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculo", DbType.Int32, pContratoSiguvVehiculos.IdVehiculo);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pContratoSiguvVehiculos.IdRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@IdCentroZonal", DbType.Int32, pContratoSiguvVehiculos.IdCentroZonal);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pContratoSiguvVehiculos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContratoSiguvVehiculos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguvVehiculos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguvVehiculos", DbType.Int32, pContratoSiguvVehiculos.IdContratoSiguvVehiculos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContratoSiguvVehiculos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public ContratoSiguvVehiculos ConsultarContratoSiguvVehiculos(int pIdContratoSiguvVehiculos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguvVehiculos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguvVehiculos", DbType.Int32, pIdContratoSiguvVehiculos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratoSiguvVehiculos vContratoSiguvVehiculos = new ContratoSiguvVehiculos();
                        while (vDataReaderResults.Read())
                        {
                            vContratoSiguvVehiculos.IdContratoSiguvVehiculos = vDataReaderResults["IdContratoSiguvVehiculos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoSiguvVehiculos"].ToString()) : vContratoSiguvVehiculos.IdContratoSiguvVehiculos;
                            vContratoSiguvVehiculos.IdContratoSiguv = vDataReaderResults["IdContratoSiguv"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoSiguv"].ToString()) : vContratoSiguvVehiculos.IdContratoSiguv;
                            vContratoSiguvVehiculos.IdVehiculo = vDataReaderResults["IdVehiculo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVehiculo"].ToString()) : vContratoSiguvVehiculos.IdVehiculo;
                            vContratoSiguvVehiculos.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vContratoSiguvVehiculos.IdRegionalPCI;
                            vContratoSiguvVehiculos.IdCentroZonal = vDataReaderResults["IdCentroZonal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCentroZonal"].ToString()) : vContratoSiguvVehiculos.IdCentroZonal;
                            vContratoSiguvVehiculos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratoSiguvVehiculos.UsuarioCrea;
                            vContratoSiguvVehiculos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratoSiguvVehiculos.FechaCrea;
                            vContratoSiguvVehiculos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratoSiguvVehiculos.UsuarioModifica;
                            vContratoSiguvVehiculos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratoSiguvVehiculos.FechaModifica;
                        }
                        return vContratoSiguvVehiculos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoSiguvVehiculos> ConsultarContratoSiguvVehiculoss(int? pIdContratoSiguv, int? pIdVehiculo, int? pIdRegionalPCI, int? pIdCentroZonal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguvVehiculoss_Consultar"))
                {
                    if(pIdContratoSiguv != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pIdContratoSiguv);
                    if(pIdVehiculo != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdVehiculo", DbType.Int32, pIdVehiculo);
                    if(pIdRegionalPCI != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pIdRegionalPCI);
                    if(pIdCentroZonal != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdCentroZonal", DbType.Int32, pIdCentroZonal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratoSiguvVehiculos> vListaContratoSiguvVehiculos = new List<ContratoSiguvVehiculos>();
                        while (vDataReaderResults.Read())
                        {
                                ContratoSiguvVehiculos vContratoSiguvVehiculos = new ContratoSiguvVehiculos();
                            vContratoSiguvVehiculos.IdContratoSiguvVehiculos = vDataReaderResults["IdContratoSiguvVehiculos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoSiguvVehiculos"].ToString()) : vContratoSiguvVehiculos.IdContratoSiguvVehiculos;
                            vContratoSiguvVehiculos.IdContratoSiguv = vDataReaderResults["IdContratoSiguv"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoSiguv"].ToString()) : vContratoSiguvVehiculos.IdContratoSiguv;
                            vContratoSiguvVehiculos.IdVehiculo = vDataReaderResults["IdVehiculo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVehiculo"].ToString()) : vContratoSiguvVehiculos.IdVehiculo;
                            vContratoSiguvVehiculos.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vContratoSiguvVehiculos.IdRegionalPCI;
                            vContratoSiguvVehiculos.IdCentroZonal = vDataReaderResults["IdCentroZonal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCentroZonal"].ToString()) : vContratoSiguvVehiculos.IdCentroZonal;
                            vContratoSiguvVehiculos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratoSiguvVehiculos.UsuarioCrea;
                            vContratoSiguvVehiculos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratoSiguvVehiculos.FechaCrea;
                            vContratoSiguvVehiculos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratoSiguvVehiculos.UsuarioModifica;
                            vContratoSiguvVehiculos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratoSiguvVehiculos.FechaModifica;
                                vListaContratoSiguvVehiculos.Add(vContratoSiguvVehiculos);
                        }
                        return vListaContratoSiguvVehiculos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
