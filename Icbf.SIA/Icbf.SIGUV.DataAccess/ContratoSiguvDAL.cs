using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.DataAccess
{
    public class ContratoSiguvDAL : GeneralDAL
    {
        public ContratoSiguvDAL()
        {
        }
        public int InsertarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguv_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pContratoSiguv.IdRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pContratoSiguv.IdMacroRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContratoSiguv.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContratoSiguv.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pContratoSiguv.IdContratoSiguv = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContratoSiguv").ToString());
                    GenerarLogAuditoria(pContratoSiguv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguv_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pContratoSiguv.IdContratoSiguv);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pContratoSiguv.IdRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pContratoSiguv.IdMacroRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContratoSiguv.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pContratoSiguv.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContratoSiguv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguv_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pContratoSiguv.IdContratoSiguv);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContratoSiguv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public ContratoSiguv ConsultarContratoSiguv(int pIdContratoSiguv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguv_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pIdContratoSiguv);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContratoSiguv vContratoSiguv = new ContratoSiguv();
                        while (vDataReaderResults.Read())
                        {
                            vContratoSiguv.IdContratoSiguv = vDataReaderResults["IdContratoSiguv"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoSiguv"].ToString()) : vContratoSiguv.IdContratoSiguv;
                            vContratoSiguv.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vContratoSiguv.IdRegionalPCI;
                            vContratoSiguv.IdMacroRegional = vDataReaderResults["IdMacroRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegional"].ToString()) : vContratoSiguv.IdMacroRegional;
                            vContratoSiguv.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratoSiguv.IdContrato;
                            vContratoSiguv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratoSiguv.UsuarioCrea;
                            vContratoSiguv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratoSiguv.FechaCrea;
                            vContratoSiguv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratoSiguv.UsuarioModifica;
                            vContratoSiguv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratoSiguv.FechaModifica;
                        }
                        return vContratoSiguv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoSiguv> ConsultarContratoSiguvs(int? pIdRegionalPCI, int? pIdMacroRegional, int? pIdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_ContratoSiguvs_Consultar"))
                {
                    if(pIdRegionalPCI != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pIdRegionalPCI);
                    if(pIdMacroRegional != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pIdMacroRegional);
                    if(pIdContrato != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratoSiguv> vListaContratoSiguv = new List<ContratoSiguv>();
                        while (vDataReaderResults.Read())
                        {
                                ContratoSiguv vContratoSiguv = new ContratoSiguv();
                            vContratoSiguv.IdContratoSiguv = vDataReaderResults["IdContratoSiguv"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoSiguv"].ToString()) : vContratoSiguv.IdContratoSiguv;
                            vContratoSiguv.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vContratoSiguv.IdRegionalPCI;
                            vContratoSiguv.IdMacroRegional = vDataReaderResults["IdMacroRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegional"].ToString()) : vContratoSiguv.IdMacroRegional;
                            vContratoSiguv.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratoSiguv.IdContrato;
                            vContratoSiguv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratoSiguv.UsuarioCrea;
                            vContratoSiguv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratoSiguv.FechaCrea;
                            vContratoSiguv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratoSiguv.UsuarioModifica;
                            vContratoSiguv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratoSiguv.FechaModifica;
                                vListaContratoSiguv.Add(vContratoSiguv);
                        }
                        return vListaContratoSiguv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
