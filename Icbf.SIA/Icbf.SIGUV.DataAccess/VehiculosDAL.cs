using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.DataAccess
{
    public class VehiculosDAL : GeneralDAL
    {
        public VehiculosDAL()
        {
        }
        public int InsertarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Vehiculos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdVehiculo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Propio", DbType.Int32, pVehiculos.Propio);
                    vDataBase.AddInParameter(vDbCommand, "@Relevo", DbType.Int32, pVehiculos.Relevo);
                    vDataBase.AddInParameter(vDbCommand, "@IdClase", DbType.Int32, pVehiculos.IdClase);
                    vDataBase.AddInParameter(vDbCommand, "@Placa", DbType.String, pVehiculos.Placa);
                    vDataBase.AddInParameter(vDbCommand, "@Cilindraje", DbType.Int32, pVehiculos.Cilindraje);
                    vDataBase.AddInParameter(vDbCommand, "@Modelo", DbType.Int32, pVehiculos.Modelo);
                    vDataBase.AddInParameter(vDbCommand, "@Capacidad", DbType.Int32, pVehiculos.Capacidad);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pVehiculos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pVehiculos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pVehiculos.IdVehiculo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdVehiculo").ToString());
                    GenerarLogAuditoria(pVehiculos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Vehiculos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculo", DbType.Int32, pVehiculos.IdVehiculo);
                    vDataBase.AddInParameter(vDbCommand, "@Propio", DbType.Int32, pVehiculos.Propio);
                    vDataBase.AddInParameter(vDbCommand, "@Relevo", DbType.Int32, pVehiculos.Relevo);
                    vDataBase.AddInParameter(vDbCommand, "@IdClase", DbType.Int32, pVehiculos.IdClase);
                    vDataBase.AddInParameter(vDbCommand, "@Placa", DbType.String, pVehiculos.Placa);
                    vDataBase.AddInParameter(vDbCommand, "@Cilindraje", DbType.Int32, pVehiculos.Cilindraje);
                    vDataBase.AddInParameter(vDbCommand, "@Modelo", DbType.Int32, pVehiculos.Modelo);
                    vDataBase.AddInParameter(vDbCommand, "@Capacidad", DbType.Int32, pVehiculos.Capacidad);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pVehiculos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pVehiculos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pVehiculos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Vehiculos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculo", DbType.Int32, pVehiculos.IdVehiculo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pVehiculos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Vehiculos ConsultarVehiculos(int pIdVehiculo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Vehiculos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculo", DbType.Int32, pIdVehiculo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Vehiculos vVehiculos = new Vehiculos();
                        while (vDataReaderResults.Read())
                        {
                            vVehiculos.IdVehiculo = vDataReaderResults["IdVehiculo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVehiculo"].ToString()) : vVehiculos.IdVehiculo;
                            vVehiculos.Propio = vDataReaderResults["Propio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Propio"].ToString()) : vVehiculos.Propio;
                            vVehiculos.Relevo = vDataReaderResults["Relevo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Relevo"].ToString()) : vVehiculos.Relevo;
                            vVehiculos.IdClase = vDataReaderResults["IdClase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClase"].ToString()) : vVehiculos.IdClase;
                            vVehiculos.Placa = vDataReaderResults["Placa"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Placa"].ToString()) : vVehiculos.Placa;
                            vVehiculos.Cilindraje = vDataReaderResults["Cilindraje"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cilindraje"].ToString()) : vVehiculos.Cilindraje;
                            vVehiculos.Modelo = vDataReaderResults["Modelo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Modelo"].ToString()) : vVehiculos.Modelo;
                            vVehiculos.Capacidad = vDataReaderResults["Capacidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Capacidad"].ToString()) : vVehiculos.Capacidad;
                            vVehiculos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vVehiculos.Estado;
                            vVehiculos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVehiculos.UsuarioCrea;
                            vVehiculos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVehiculos.FechaCrea;
                            vVehiculos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVehiculos.UsuarioModifica;
                            vVehiculos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVehiculos.FechaModifica;
                        }
                        return vVehiculos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Vehiculos> ConsultarVehiculoss(int? pPropio, int? pRelevo, int? pIdClase, String pPlaca, int? pCilindraje, int? pModelo, int? pCapacidad, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Vehiculoss_Consultar"))
                {
                    if(pPropio != null)
                         vDataBase.AddInParameter(vDbCommand, "@Propio", DbType.Int32, pPropio);
                    if(pRelevo != null)
                         vDataBase.AddInParameter(vDbCommand, "@Relevo", DbType.Int32, pRelevo);
                    if(pIdClase != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdClase", DbType.Int32, pIdClase);
                    if(pPlaca != null)
                         vDataBase.AddInParameter(vDbCommand, "@Placa", DbType.String, pPlaca);
                    if(pCilindraje != null)
                         vDataBase.AddInParameter(vDbCommand, "@Cilindraje", DbType.Int32, pCilindraje);
                    if(pModelo != null)
                         vDataBase.AddInParameter(vDbCommand, "@Modelo", DbType.Int32, pModelo);
                    if(pCapacidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@Capacidad", DbType.Int32, pCapacidad);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Vehiculos> vListaVehiculos = new List<Vehiculos>();
                        while (vDataReaderResults.Read())
                        {
                                Vehiculos vVehiculos = new Vehiculos();
                            vVehiculos.IdVehiculo = vDataReaderResults["IdVehiculo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVehiculo"].ToString()) : vVehiculos.IdVehiculo;
                            vVehiculos.Propio = vDataReaderResults["Propio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Propio"].ToString()) : vVehiculos.Propio;
                            vVehiculos.Relevo = vDataReaderResults["Relevo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Relevo"].ToString()) : vVehiculos.Relevo;
                            vVehiculos.IdClase = vDataReaderResults["IdClase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClase"].ToString()) : vVehiculos.IdClase;
                            vVehiculos.Placa = vDataReaderResults["Placa"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Placa"].ToString()) : vVehiculos.Placa;
                            vVehiculos.Cilindraje = vDataReaderResults["Cilindraje"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cilindraje"].ToString()) : vVehiculos.Cilindraje;
                            vVehiculos.Modelo = vDataReaderResults["Modelo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Modelo"].ToString()) : vVehiculos.Modelo;
                            vVehiculos.Capacidad = vDataReaderResults["Capacidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Capacidad"].ToString()) : vVehiculos.Capacidad;
                            vVehiculos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vVehiculos.Estado;
                            vVehiculos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVehiculos.UsuarioCrea;
                            vVehiculos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVehiculos.FechaCrea;
                            vVehiculos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVehiculos.UsuarioModifica;
                            vVehiculos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVehiculos.FechaModifica;
                                vListaVehiculos.Add(vVehiculos);
                        }
                        return vListaVehiculos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
