using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.DataAccess
{
    public class MacroRegionalRegionalPCIDAL : GeneralDAL
    {
        public MacroRegionalRegionalPCIDAL()
        {
        }
        public int InsertarMacroRegionalRegionalPCI(MacroRegionalRegionalPCI pMacroRegionalRegionalPCI)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegionalRegionalPCI_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdMacroRegionalRegionalPCI", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pMacroRegionalRegionalPCI.IdMacroRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pMacroRegionalRegionalPCI.IdRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pMacroRegionalRegionalPCI.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pMacroRegionalRegionalPCI.IdMacroRegionalRegionalPCI = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdMacroRegionalRegionalPCI").ToString());
                    GenerarLogAuditoria(pMacroRegionalRegionalPCI, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarMacroRegionalRegionalPCI(MacroRegionalRegionalPCI pMacroRegionalRegionalPCI)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegionalRegionalPCI_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegionalRegionalPCI", DbType.Int32, pMacroRegionalRegionalPCI.IdMacroRegionalRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pMacroRegionalRegionalPCI.IdMacroRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pMacroRegionalRegionalPCI.IdRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pMacroRegionalRegionalPCI.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pMacroRegionalRegionalPCI, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarMacroRegionalRegionalPCI(MacroRegionalRegionalPCI pMacroRegionalRegionalPCI)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegionalRegionalPCI_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegionalRegionalPCI", DbType.Int32, pMacroRegionalRegionalPCI.IdMacroRegionalRegionalPCI);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pMacroRegionalRegionalPCI, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public MacroRegionalRegionalPCI ConsultarMacroRegionalRegionalPCI(int pIdMacroRegionalRegionalPCI)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegionalRegionalPCI_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegionalRegionalPCI", DbType.Int32, pIdMacroRegionalRegionalPCI);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        MacroRegionalRegionalPCI vMacroRegionalRegionalPCI = new MacroRegionalRegionalPCI();
                        while (vDataReaderResults.Read())
                        {
                            vMacroRegionalRegionalPCI.IdMacroRegionalRegionalPCI = vDataReaderResults["IdMacroRegionalRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegionalRegionalPCI"].ToString()) : vMacroRegionalRegionalPCI.IdMacroRegionalRegionalPCI;
                            vMacroRegionalRegionalPCI.IdMacroRegional = vDataReaderResults["IdMacroRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegional"].ToString()) : vMacroRegionalRegionalPCI.IdMacroRegional;
                            vMacroRegionalRegionalPCI.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vMacroRegionalRegionalPCI.IdRegionalPCI;
                            vMacroRegionalRegionalPCI.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMacroRegionalRegionalPCI.UsuarioCrea;
                            vMacroRegionalRegionalPCI.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMacroRegionalRegionalPCI.FechaCrea;
                            vMacroRegionalRegionalPCI.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMacroRegionalRegionalPCI.UsuarioModifica;
                            vMacroRegionalRegionalPCI.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMacroRegionalRegionalPCI.FechaModifica;
                        }
                        return vMacroRegionalRegionalPCI;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<MacroRegionalRegionalPCI> ConsultarMacroRegionalRegionalPCIs(int? pIdMacroRegional, int? pIdRegionalPCI)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegionalRegionalPCIs_Consultar"))
                {
                    if(pIdMacroRegional != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pIdMacroRegional);
                    if(pIdRegionalPCI != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdRegionalPCI", DbType.Int32, pIdRegionalPCI);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<MacroRegionalRegionalPCI> vListaMacroRegionalRegionalPCI = new List<MacroRegionalRegionalPCI>();
                        while (vDataReaderResults.Read())
                        {
                                MacroRegionalRegionalPCI vMacroRegionalRegionalPCI = new MacroRegionalRegionalPCI();
                            vMacroRegionalRegionalPCI.IdMacroRegionalRegionalPCI = vDataReaderResults["IdMacroRegionalRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegionalRegionalPCI"].ToString()) : vMacroRegionalRegionalPCI.IdMacroRegionalRegionalPCI;
                            vMacroRegionalRegionalPCI.IdMacroRegional = vDataReaderResults["IdMacroRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegional"].ToString()) : vMacroRegionalRegionalPCI.IdMacroRegional;
                            vMacroRegionalRegionalPCI.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vMacroRegionalRegionalPCI.IdRegionalPCI;
                            vMacroRegionalRegionalPCI.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMacroRegionalRegionalPCI.UsuarioCrea;
                            vMacroRegionalRegionalPCI.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMacroRegionalRegionalPCI.FechaCrea;
                            vMacroRegionalRegionalPCI.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMacroRegionalRegionalPCI.UsuarioModifica;
                            vMacroRegionalRegionalPCI.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMacroRegionalRegionalPCI.FechaModifica;
                                vListaMacroRegionalRegionalPCI.Add(vMacroRegionalRegionalPCI);
                        }
                        return vListaMacroRegionalRegionalPCI;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
