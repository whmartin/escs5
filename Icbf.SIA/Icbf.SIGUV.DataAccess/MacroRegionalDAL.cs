using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.DataAccess
{
    public class MacroRegionalDAL : GeneralDAL
    {
        public MacroRegionalDAL()
        {
        }
        public int InsertarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegional_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pMacroRegional.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pMacroRegional.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pMacroRegional.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pMacroRegional.IdMacroRegional = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdMacroRegional").ToString());
                    GenerarLogAuditoria(pMacroRegional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegional_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pMacroRegional.IdMacroRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pMacroRegional.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pMacroRegional.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pMacroRegional.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pMacroRegional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegional_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pMacroRegional.IdMacroRegional);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pMacroRegional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public MacroRegional ConsultarMacroRegional(int pIdMacroRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegional_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pIdMacroRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        MacroRegional vMacroRegional = new MacroRegional();
                        while (vDataReaderResults.Read())
                        {
                            vMacroRegional.IdMacroRegional = vDataReaderResults["IdMacroRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegional"].ToString()) : vMacroRegional.IdMacroRegional;
                            vMacroRegional.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vMacroRegional.Nombre;
                            vMacroRegional.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vMacroRegional.Estado;
                            vMacroRegional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMacroRegional.UsuarioCrea;
                            vMacroRegional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMacroRegional.FechaCrea;
                            vMacroRegional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMacroRegional.UsuarioModifica;
                            vMacroRegional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMacroRegional.FechaModifica;
                        }
                        return vMacroRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<MacroRegional> ConsultarMacroRegionals(String pNombre, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegionals_Consultar"))
                {
                    if(pNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<MacroRegional> vListaMacroRegional = new List<MacroRegional>();
                        while (vDataReaderResults.Read())
                        {
                                MacroRegional vMacroRegional = new MacroRegional();
                            vMacroRegional.IdMacroRegional = vDataReaderResults["IdMacroRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegional"].ToString()) : vMacroRegional.IdMacroRegional;
                            vMacroRegional.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vMacroRegional.Nombre;
                            vMacroRegional.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vMacroRegional.Estado;
                            vMacroRegional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMacroRegional.UsuarioCrea;
                            vMacroRegional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMacroRegional.FechaCrea;
                            vMacroRegional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMacroRegional.UsuarioModifica;
                            vMacroRegional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMacroRegional.FechaModifica;
                                vListaMacroRegional.Add(vMacroRegional);
                        }
                        return vListaMacroRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<MacroRegional> ConsultarMacroRegionalMacroRegionalPCI(int pIdMacroRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_MacroRegionalMacroRegionalPCI_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdMacroRegional", DbType.Int32, pIdMacroRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<MacroRegional> vListaMacroRegional = new List<MacroRegional>();
                        while (vDataReaderResults.Read())
                        {
                            MacroRegional vMacroRegional = new MacroRegional();
                            vMacroRegional.IdMacroRegional = vDataReaderResults["IdMacroRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegional"].ToString()) : vMacroRegional.IdMacroRegional;
                            vMacroRegional.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vMacroRegional.Nombre;
                            vMacroRegional.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vMacroRegional.Estado;
                            vMacroRegional.IdMacroRegionalRegionalPCI = vDataReaderResults["IdMacroRegionalRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMacroRegionalRegionalPCI"].ToString()) : vMacroRegional.IdMacroRegionalRegionalPCI;
                            vMacroRegional.IdRegionalPCI = vDataReaderResults["IdRegionalPCI"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalPCI"].ToString()) : vMacroRegional.IdRegionalPCI;
                            vListaMacroRegional.Add(vMacroRegional);
                        }
                        return vListaMacroRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
