using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIGUV.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIGUV.DataAccess
{
    public class RelevoDAL : GeneralDAL
    {
        public RelevoDAL()
        {
        }
        public int InsertarRelevo(Relevo pRelevo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Relevo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRelevo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pRelevo.IdContratoSiguv);
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculoPrincipal", DbType.Int32, pRelevo.IdVehiculoPrincipal);
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculoReemplazo", DbType.Int32, pRelevo.IdVehiculoReemplazo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pRelevo.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pRelevo.FechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRelevo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRelevo.IdRelevo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRelevo").ToString());
                    GenerarLogAuditoria(pRelevo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarRelevo(Relevo pRelevo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Relevo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRelevo", DbType.Int32, pRelevo.IdRelevo);
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pRelevo.IdContratoSiguv);
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculoPrincipal", DbType.Int32, pRelevo.IdVehiculoPrincipal);
                    vDataBase.AddInParameter(vDbCommand, "@IdVehiculoReemplazo", DbType.Int32, pRelevo.IdVehiculoReemplazo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pRelevo.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pRelevo.FechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRelevo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRelevo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarRelevo(Relevo pRelevo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Relevo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRelevo", DbType.Int32, pRelevo.IdRelevo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRelevo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Relevo ConsultarRelevo(int pIdRelevo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Relevo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRelevo", DbType.Int32, pIdRelevo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Relevo vRelevo = new Relevo();
                        while (vDataReaderResults.Read())
                        {
                            vRelevo.IdRelevo = vDataReaderResults["IdRelevo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRelevo"].ToString()) : vRelevo.IdRelevo;
                            vRelevo.IdContratoSiguv = vDataReaderResults["IdContratoSiguv"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoSiguv"].ToString()) : vRelevo.IdContratoSiguv;
                            vRelevo.IdVehiculoPrincipal = vDataReaderResults["IdVehiculoPrincipal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVehiculoPrincipal"].ToString()) : vRelevo.IdVehiculoPrincipal;
                            vRelevo.IdVehiculoReemplazo = vDataReaderResults["IdVehiculoReemplazo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVehiculoReemplazo"].ToString()) : vRelevo.IdVehiculoReemplazo;
                            vRelevo.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vRelevo.FechaInicio;
                            vRelevo.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vRelevo.FechaFin;
                            vRelevo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRelevo.UsuarioCrea;
                            vRelevo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRelevo.FechaCrea;
                            vRelevo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRelevo.UsuarioModifica;
                            vRelevo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRelevo.FechaModifica;
                        }
                        return vRelevo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Relevo> ConsultarRelevos(int? pIdContratoSiguv, int? pIdVehiculoPrincipal, int? pIdVehiculoReemplazo, DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIGUV_SIGUV_Relevos_Consultar"))
                {
                    if(pIdContratoSiguv != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdContratoSiguv", DbType.Int32, pIdContratoSiguv);
                    if(pIdVehiculoPrincipal != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdVehiculoPrincipal", DbType.Int32, pIdVehiculoPrincipal);
                    if(pIdVehiculoReemplazo != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdVehiculoReemplazo", DbType.Int32, pIdVehiculoReemplazo);
                    if(pFechaInicio != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pFechaInicio);
                    if(pFechaFin != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pFechaFin);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Relevo> vListaRelevo = new List<Relevo>();
                        while (vDataReaderResults.Read())
                        {
                                Relevo vRelevo = new Relevo();
                            vRelevo.IdRelevo = vDataReaderResults["IdRelevo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRelevo"].ToString()) : vRelevo.IdRelevo;
                            vRelevo.IdContratoSiguv = vDataReaderResults["IdContratoSiguv"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoSiguv"].ToString()) : vRelevo.IdContratoSiguv;
                            vRelevo.IdVehiculoPrincipal = vDataReaderResults["IdVehiculoPrincipal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVehiculoPrincipal"].ToString()) : vRelevo.IdVehiculoPrincipal;
                            vRelevo.IdVehiculoReemplazo = vDataReaderResults["IdVehiculoReemplazo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVehiculoReemplazo"].ToString()) : vRelevo.IdVehiculoReemplazo;
                            vRelevo.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vRelevo.FechaInicio;
                            vRelevo.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vRelevo.FechaFin;
                            vRelevo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRelevo.UsuarioCrea;
                            vRelevo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRelevo.FechaCrea;
                            vRelevo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRelevo.UsuarioModifica;
                            vRelevo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRelevo.FechaModifica;
                                vListaRelevo.Add(vRelevo);
                        }
                        return vListaRelevo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
