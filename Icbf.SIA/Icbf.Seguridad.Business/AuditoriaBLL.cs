﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.DataAccess;
using Icbf.Seguridad.Entity;

namespace Icbf.Seguridad.Business
{
    public class AuditoriaBLL
    {
        private PermisoDAL vPermisoDAL;

        public AuditoriaBLL()
        {
            vPermisoDAL = new PermisoDAL();
        }
        public List<Auditoria> ConsultarAuditoria(String pNombrePrograma, Double pIdRegistro)
        {
            try
            {
                return vPermisoDAL.ConsultarAuditoria(pNombrePrograma, pIdRegistro);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
