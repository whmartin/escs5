using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.DataAccess;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.Business
{
    public class ModuloBLL
    {
        private ModuloDAL vModuloDAL;
        public ModuloBLL()
        {
            vModuloDAL = new ModuloDAL();
        }
        public int InsertarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloDAL.InsertarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloDAL.ModificarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModuloPorNombre(string pNombreModulo)
        {
            try
            {
                return vModuloDAL.ConsultarModuloPorNombre(pNombreModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModulosRol(string pNombreRol)
        {
            try
            {
                return vModuloDAL.ConsultarModulosRol(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarPermisosRol(string pNombreRol)
        {
            try
            {
                return vModuloDAL.ConsultarPermisosRol(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarPermisosRol(string[] pNombreRol)
        {
            try
            {
                return vModuloDAL.ConsultarPermisosRol(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModulos(string pNombreModulo, Boolean? pEstado)
        {
            try
            {
                return vModuloDAL.ConsultarModulos(pNombreModulo, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Modulo ConsultarModulo(int pIdModulo)
        {
            try
            {
                return vModuloDAL.ConsultarModulo(pIdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Modulo ConsultarTodosModulos()
        {
            try
            {
                return vModuloDAL.ConsultarTodosModulos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloDAL.EliminarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
