using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.DataAccess;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.Business
{
    public class ParametroBLL
    {
        private ParametroDAL vParametroDAL;
        public ParametroBLL()
        {
            vParametroDAL = new ParametroDAL();
        }
        public int InsertarParametro(Parametro pParametro)
        {
            try
            {
                return vParametroDAL.InsertarParametro(pParametro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarParametro(Parametro pParametro)
        {
            try
            {
                return vParametroDAL.ModificarParametro(pParametro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarParametro(Parametro pParametro)
        {
            try
            {
                return vParametroDAL.EliminarParametro(pParametro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Parametro ConsultarParametro(int pIdParametro)
        {
            try
            {
                return vParametroDAL.ConsultarParametro(pIdParametro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Parametro> ConsultarParametros(String pNombreParametro, String pValorParametro, Boolean? pEstado, string pFuncionalidad)
        {
            try
            {
                return vParametroDAL.ConsultarParametros(pNombreParametro, pValorParametro, pEstado, pFuncionalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
