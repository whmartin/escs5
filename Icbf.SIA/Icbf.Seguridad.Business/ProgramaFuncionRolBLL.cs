﻿//-----------------------------------------------------------------------
// <copyright file="ProgramaFuncionRolBLL.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncionRolBLL_Add.</summary>
// <author>Alvaro Mauricio Guerrero</author>
// <date>22/02/2017 0355 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Icbf.Seguridad.DataAccess;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.Business
{
    /// <summary>
    /// Clase parcial para gestionar la lógica de ProgramaFuncionRolBLL
    /// </summary>
    public class ProgramaFuncionRolBLL
    {
        /// <summary>
        /// Inicialización objeto tipo DAL
        /// </summary>
        private ProgramaFuncionRolDAL vProgramaFuncionRolDAL;

        /// <summary>
        /// Inicializa una nueva instancia de ProgramaFuncionRolDAL
        /// </summary>
        public ProgramaFuncionRolBLL()
        {
            vProgramaFuncionRolDAL = new ProgramaFuncionRolDAL();
        }

        /// <summary>
        /// Método para Insertar Programa Funcion Rol
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Id del Programa Funcion Rol insertado</returns>public int
        public int InsertarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                if (vProgramaFuncionRolDAL.ConsultarProgramaFuncionRols(pProgramaFuncionRol.IdPrograma, pProgramaFuncionRol.IdProgramaFuncion, pProgramaFuncionRol.IdRol, -1).Any())
                {
                    return -1;
                }

                return vProgramaFuncionRolDAL.InsertarProgramaFuncionRol(pProgramaFuncionRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar Programa Funcion Rol
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int
        public int ModificarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                int? vEstado = pProgramaFuncionRol.Estado ? 1 : 0;

                if (vProgramaFuncionRolDAL.ConsultarProgramaFuncionRols(pProgramaFuncionRol.IdPrograma, pProgramaFuncionRol.IdProgramaFuncion, pProgramaFuncionRol.IdRol, vEstado).Any())
                {
                    return -1;
                }

                return vProgramaFuncionRolDAL.ModificarProgramaFuncionRol(pProgramaFuncionRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Eliminar Programa Funcion Rol por Id
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int
        public int EliminarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                return vProgramaFuncionRolDAL.EliminarProgramaFuncionRol(pProgramaFuncionRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programa Funcion Rol
        /// </summary>
        /// <param name="pIdProgramaFuncionRol">Id ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public ProgramaFuncionRol
        public ProgramaFuncionRol ConsultarProgramaFuncionRol(int pIdProgramaFuncionRol)
        {
            try
            {
                return vProgramaFuncionRolDAL.ConsultarProgramaFuncionRol(pIdProgramaFuncionRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas Funcion Roles
        /// </summary>
        /// <param name="pIdPrograma">Id Programa a filtrar</param>
        /// <param name="pIdProgramaFuncion">Id Programa Funcion a filtrar</param>
        /// <param name="pIdRol">Id Rol a filtrar</param>
        /// <param name="pEstado">Estado a filtrar</param>
        /// <returns>Lista de Programas Funcion Roles consultado</returns>public List
        public List<ProgramaFuncionRol> ConsultarProgramaFuncionRols(int? pIdPrograma, int? pIdProgramaFuncion, int? pIdRol, int? pEstado)
        {
            try
            {
                return vProgramaFuncionRolDAL.ConsultarProgramaFuncionRols(pIdPrograma, pIdProgramaFuncion, pIdRol, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Roles
        /// </summary>
        /// <returns>Lista Rol consultada</returns>public List
        public List<Rol> ConsultarRoles()
        {
            try
            {
                return vProgramaFuncionRolDAL.ConsultarRoles();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas Funcion por Programa
        /// </summary>
        /// <param name="pIdPrograma">Id del Programa a filtrar</param>
        /// <returns>Lista de ProgramaFuncion consultada</returns>public List
        public List<ProgramaFuncion> ConsultarProgramasFuncionPorPrograma(int pIdPrograma)
        {
            try
            {
                return vProgramaFuncionRolDAL.ConsultarProgramasFuncionPorPrograma(pIdPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
