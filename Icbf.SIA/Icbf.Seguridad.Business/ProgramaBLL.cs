using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.DataAccess;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.Business
{
    public class ProgramaBLL
    {
        private ProgramaDAL vProgramaDAL;
        public ProgramaBLL()
        {
            vProgramaDAL = new ProgramaDAL();
        }
        public int InsertarPrograma(Programa pPrograma)
        {
            try
            {
                return vProgramaDAL.InsertarPrograma(pPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPrograma(Programa pPrograma)
        {
            try
            {
                return vProgramaDAL.ModificarPrograma(pPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPrograma(Programa pPrograma)
        {
            try
            {
                return vProgramaDAL.EliminarPrograma(pPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Programa ConsultarPrograma(int pIdPrograma)
        {
            try
            {
                return vProgramaDAL.ConsultarPrograma(pIdPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramas(int pIdModulo, String pNombreModulo)
        {
            try
            {
                return vProgramaDAL.ConsultarProgramas(pIdModulo, pNombreModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasPermitidos(int pIdModulo, String pNombreRol)
        {
            try
            {
                return vProgramaDAL.ConsultarProgramasPermitidos(pIdModulo, pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasHijosPermitidosPermiso(int pIdModulo, String pNombreRol, int? idPadre)
        {
            try
            {
                return vProgramaDAL.ConsultarProgramasHijoPermitidosPermiso(pIdModulo, pNombreRol, idPadre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Programa> ConsultarProgramasHijosPermitidosPermiso(int pIdModulo, String[] pNombreRol, int? idPadre)
        {
            try
            {
                return vProgramaDAL.ConsultarProgramasHijoPermitidosPermiso(pIdModulo, pNombreRol, idPadre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasPermitidosPermiso(int pIdModulo, String pNombreRol)
        {
            try
            {
                return vProgramaDAL.ConsultarProgramasPermitidosPermiso(pIdModulo, pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasConPermiso(String pNombreRol)
        {
            try
            {
                return vProgramaDAL.ConsultarProgramasConPermiso(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasConPermiso(String[] pNombreRol)
        {
            try
            {
                return vProgramaDAL.ConsultarProgramasConPermiso(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
