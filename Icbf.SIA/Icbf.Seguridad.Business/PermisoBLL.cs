using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.DataAccess;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.Business
{
    public class PermisoBLL
    {
        private PermisoDAL vPermisoDAL;
        public PermisoBLL()
        {
            vPermisoDAL = new PermisoDAL();
        }
        public int InsertarPermiso(Permiso pPermiso)
        {
            try
            {
                return vPermisoDAL.InsertarPermiso(pPermiso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPermiso(Permiso pPermiso)
        {
            try
            {
                return vPermisoDAL.ModificarPermiso(pPermiso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPermiso(int pIdPermiso)
        {
            try
            {
                return vPermisoDAL.EliminarPermiso(pIdPermiso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPermisosRol(int pIdRol)
        {
            try
            {
                return vPermisoDAL.EliminarPermisosRol(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Permiso> ConsultarPermisosRol(int pIdRol)
        {
            try
            {
                return vPermisoDAL.ConsultarPermisosRol(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
