﻿//-----------------------------------------------------------------------
// <copyright file="ProgramaFuncionBLL.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncion_Add.</summary>
// <author>Alvaro Mauricio Guerrero</author>
// <date>22/02/2017 0355 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Icbf.Seguridad.DataAccess;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.Business
{
    /// <summary>
    /// Clase parcial para gestionar la lógica de ProgramaFuncionBLL
    /// </summary>
    public class ProgramaFuncionBLL
    {
        /// <summary>
        /// Inicialización objeto tipo DAL
        /// </summary>
        private ProgramaFuncionDAL vProgramaFuncionDAL;

        /// <summary>
        /// Inicializa una nueva instancia de ProgramaFuncionDAL
        /// </summary>
        public ProgramaFuncionBLL()
        {
            vProgramaFuncionDAL = new ProgramaFuncionDAL();
        }

        /// <summary>
        /// Método para Insertar un ProgramaFuncion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Id del ProgramaFuncion insertado</returns>public int 
        public int InsertarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                if (vProgramaFuncionDAL.ConsultarProgramaFuncions(pProgramaFuncion.IdPrograma, pProgramaFuncion.NombreFuncion, -1).Any())
                {
                    return -1;
                }

                return vProgramaFuncionDAL.InsertarProgramaFuncion(pProgramaFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar un Programa Funcion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int 
        public int ModificarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                int? vEstado = pProgramaFuncion.Estado ? 1 : 0;

                if (vProgramaFuncionDAL.ConsultarProgramaFuncionExiste(pProgramaFuncion.IdPrograma, pProgramaFuncion.NombreFuncion, vEstado).Any(reg => reg.IdProgramaFuncion != pProgramaFuncion.IdProgramaFuncion))
                {
                    return -1;
                }

                return vProgramaFuncionDAL.ModificarProgramaFuncion(pProgramaFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Eliminar Programa Funcion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int 
        public int EliminarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                return vProgramaFuncionDAL.EliminarProgramaFuncion(pProgramaFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programa Funcion por Id
        /// </summary>
        /// <param name="pIdProgramaFuncion">Id del Programa Funcion a filtrar</param>
        /// <returns>Programa Funcion consultado</returns>public ProgramaFuncion
        public ProgramaFuncion ConsultarProgramaFuncion(int pIdProgramaFuncion)
        {
            try
            {
                return vProgramaFuncionDAL.ConsultarProgramaFuncion(pIdProgramaFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas de Funcion
        /// </summary>
        /// <param name="pIdPrograma">Id Programa a filtrar</param>
        /// <param name="pNombreFuncion">Nombre Funcion a filtrar</param>
        /// <param name="pEstado">Estado a filtrar</param>
        /// <returns>Lista de Programa Funcion</returns>public List
        public List<ProgramaFuncion> ConsultarProgramaFuncions(int? pIdPrograma, string pNombreFuncion, int? pEstado)
        {
            try
            {
                return vProgramaFuncionDAL.ConsultarProgramaFuncions(pIdPrograma, pNombreFuncion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas
        /// </summary>
        /// <returns>Lista de Programas consultados</returns>public List
        public List<Programa> ConsultarProgramas()
        {
            try
            {
                return vProgramaFuncionDAL.ConsultarProgramas();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        /// <summary>
        /// Método para Consultar Programas del Modulo Denuncias
        /// </summary>
        /// <returns>Lista de Programas consultados</returns>public List
        public List<Programa> ConsultarProgramasDenuncias()
        {
            try
            {
                return vProgramaFuncionDAL.ConsultarProgramasDenuncias();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Consulta de las funciones de una página a las que tiene acceso un rol
        /// </summary>
        /// <param name="pRolNombre">Nombre del Rol</param>
        /// <param name="pCodigoPrograma">Nombre en el code behind de la página</param>
        /// <returns>Lista de ProgramaFuncion</returns>public List
        public List<ProgramaFuncion> ConsultarUsuarioProgramaFuncionRol(string pRolNombre, string pCodigoPrograma)
        {
            try
            {
                try
                {
                    return new ProgramaFuncionDAL().ConsultarUsuarioProgramaFuncionRol(pRolNombre, pCodigoPrograma);
                }
                catch (UserInterfaceException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new GenericException(ex);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Consulta de las funciones de una página a las que tiene acceso un rol
        /// </summary>
        /// <param name="pRolNombre">Nombre del Rol</param>
        /// <param name="pCodigoPrograma">Nombre en el code behind de la página</param>
        /// <returns>Lista de ProgramaFuncion</returns>public List
        public List<ProgramaFuncion> ConsultarUsuarioProgramaFuncionRolComisiones(string pRolNombre, string pCodigoPrograma)
        {
            try
            {
                try
                {
                    return new ProgramaFuncionDAL().ConsultarUsuarioProgramaFuncionRolComisiones(pRolNombre, pCodigoPrograma);
                }
                catch (UserInterfaceException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new GenericException(ex);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

