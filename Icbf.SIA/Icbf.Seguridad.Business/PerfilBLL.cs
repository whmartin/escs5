using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.DataAccess;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.Business
{
    public class PerfilBLL
    {
        private PerfilDAL vPerfilDAL;
        public PerfilBLL()
        {
            vPerfilDAL = new PerfilDAL();
        }
        public int InsertarPerfil(Perfil pPerfil)
        {
            try
            {
                return vPerfilDAL.InsertarPerfil(pPerfil);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPerfil(Perfil pPerfil)
        {
            try
            {
                return vPerfilDAL.ModificarPerfil(pPerfil);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPerfil(int pIdPerfil)
        {
            try
            {
                return vPerfilDAL.EliminarPerfil(pIdPerfil);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Perfil> ConsultarTodosPerfil()
        {
            try
            {
                return vPerfilDAL.ConsultarTodosPerfil();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
