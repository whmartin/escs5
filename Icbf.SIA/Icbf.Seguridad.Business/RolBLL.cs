using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.DataAccess;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;
using System.Web.Security;
using System.Web.UI;

namespace Icbf.Seguridad.Business
{
    public class RolBLL
    {
        private RolDAL vRolDAL;
        private PermisoBLL vPermisoBLL;

        public RolBLL()
        {
            vRolDAL = new RolDAL();
            vPermisoBLL = new PermisoBLL();
        }
        public int InsertarRol(Rol pRol)
        {
            try
            {
                string[] sRolesMembership = Roles.GetAllRoles();
                if (sRolesMembership.Contains(pRol.NombreRol))
                {
                    return -1;
                }
                else
                {
                    Roles.CreateRole(pRol.NombreRol);
                    pRol.Providerkey = Guid.NewGuid().ToString();
                    int returnValue = vRolDAL.InsertarRol(pRol);
                    foreach (Permiso vPermiso in pRol.Permisos)
                    {
                        vPermiso.UsuarioCreacion = pRol.UsuarioCreacion;
                        vPermiso.IdRol = pRol.IdRol;
                        vPermisoBLL.InsertarPermiso(vPermiso);
                    }
                    
                    return returnValue;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Roles.DeleteRole(pRol.NombreRol);
                if (pRol.IdRol != 0)
                {
                    foreach (Permiso vPermiso in vPermisoBLL.ConsultarPermisosRol(pRol.IdRol))
                    {
                        vPermisoBLL.EliminarPermiso(vPermiso.IdPermiso);
                    }
                    vRolDAL.EliminarRol(pRol.IdRol);
                }
                throw new GenericException(ex);
            }
        }
        public int ModificarRol(Rol pRol)
        {
            try
            {
                string[] sRolesMembership = Roles.GetAllRoles();
                Rol vRol = ConsultarRol(pRol.IdRol);
                if (vRol.NombreRol.ToUpper() != pRol.NombreRol.ToUpper())
                {
                    if (sRolesMembership.Contains(pRol.NombreRol.ToUpper()))
                    {
                        return -1;
                    }
                    else
                    {
                        Roles.CreateRole(pRol.NombreRol);
                    }
                }
                int returnValue = vRolDAL.ModificarRol(pRol);
                vRolDAL.EliminarProgramaRol(pRol.IdRol);
                vPermisoBLL.EliminarPermisosRol(pRol.IdRol);
                foreach (Permiso vPermiso in pRol.Permisos)
                {
                    vPermiso.UsuarioCreacion = pRol.UsuarioModificacion;
                    vPermiso.IdRol = pRol.IdRol;
                    vPermisoBLL.InsertarPermiso(vPermiso);
                }
                
                return returnValue;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRol(int pIdRol)
        {
            try
            {
                return vRolDAL.EliminarRol(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Rol ConsultarRol(string pProviderKey)
        {
            try
            {
                return vRolDAL.ConsultarRol(pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Rol ConsultarRol(int pIdRol)
        {
            try
            {
                return vRolDAL.ConsultarRol(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Rol> ConsultarRoles(String pNombreRol, Boolean? pEstadoRol)
        {
            try
            {
                return vRolDAL.ConsultarRoles(pNombreRol, pEstadoRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ConsultarRolesNombreEsAdmin(String pNombreRol, Boolean? pEstadoRol)
        {
            try
            {
                return vRolDAL.ConsultarRolesNombreEsAdmin(pNombreRol, pEstadoRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasAsignados(int pIdRol)
        {
            try
            {
                return vRolDAL.ConsultarProgramasAsignados(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasSinAsignar(int pIdRol)
        {
            try
            {
                return vRolDAL.ConsultarProgramasExcluidos(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Permiso> ConsultarPermisosRol(int pIdRol)
        {
            try
            {
                return vPermisoBLL.ConsultarPermisosRol(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ConsultarUsuarioPorRol(string pNombreRol)
        {
            try
            {
                return vRolDAL.ConsultarUsuarioPorRol(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
