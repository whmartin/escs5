﻿using Icbf.ActualizacionAutomatica.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.ActualizacionAutomatica.Service
{
    public class ActualizacionAutomaticaService
    {
        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos()
        {
            try
            {
                return new Icbf.ActualizacionAutomatica.Business.ActualizacionAutomaticaBLL().ConsultarRegistroSolicitudEstudioSectoryCasos();
            }
            catch (Exception e)
            {
                throw;
            }
           
        }
        public int ActualizarRegistroSolicitudEstudioSectoryCasos(int vigenciapacco, decimal consecutivo, string area, string codigoarea, string estado, string FechaInicioProceso, string Id_modalidad, decimal? Id_tipo_contrato, string modalidad, string objeto_contractual, string tipo_contrato, string FechaInicioEjecucion, decimal valor_contrato)
        {
            try
            {
                return new Icbf.ActualizacionAutomatica.Business.ActualizacionAutomaticaBLL().ActualizarRegistroSolicitudEstudioSectoryCasos(vigenciapacco, consecutivo, area, codigoarea, estado, FechaInicioProceso, Id_modalidad, Id_tipo_contrato, modalidad, objeto_contractual, tipo_contrato, FechaInicioEjecucion, valor_contrato);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    
}
