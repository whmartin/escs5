using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    public class ReporteDAL : GeneralDAL
    {
        public ReporteDAL()
        {
        }
        public int InsertarReporte(Reporte pReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_Reporte_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdReporte", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreReporte", DbType.String, pReporte.NombreReporte);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pReporte.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Servidor", DbType.String, pReporte.Servidor);
                    vDataBase.AddInParameter(vDbCommand, "@Carpeta", DbType.String, pReporte.Carpeta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pReporte.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pReporte.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pReporte.IdReporte = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdReporte").ToString());
                    GenerarLogAuditoria(pReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarReporte(Reporte pReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_Reporte_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReporte", DbType.Int32, pReporte.IdReporte);
                    vDataBase.AddInParameter(vDbCommand, "@NombreReporte", DbType.String, pReporte.NombreReporte);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pReporte.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Servidor", DbType.String, pReporte.Servidor);
                    vDataBase.AddInParameter(vDbCommand, "@Carpeta", DbType.String, pReporte.Carpeta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pReporte.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pReporte.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarReporte(Reporte pReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_Reporte_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReporte", DbType.Int32, pReporte.IdReporte);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Reporte ConsultarReporte(int pIdReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_Reporte_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdReporte", DbType.Int32, pIdReporte);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Reporte vReporte = new Reporte();
                        while (vDataReaderResults.Read())
                        {
                            vReporte.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vReporte.IdReporte;
                            vReporte.NombreReporte = vDataReaderResults["NombreReporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreReporte"].ToString()) : vReporte.NombreReporte;
                            vReporte.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vReporte.Descripcion;
                            vReporte.Servidor = vDataReaderResults["Servidor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Servidor"].ToString()) : vReporte.Servidor;
                            vReporte.Carpeta = vDataReaderResults["Carpeta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Carpeta"].ToString()) : vReporte.Carpeta;
                            vReporte.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vReporte.NombreArchivo;
                            vReporte.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReporte.UsuarioCrea;
                            vReporte.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReporte.FechaCrea;
                            vReporte.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReporte.UsuarioModifica;
                            vReporte.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReporte.FechaModifica;
                        }
                        return vReporte;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Reporte> ConsultarReportes(String pNombreReporte, String pServidor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_Reportes_Consultar"))
                {
                    if(pNombreReporte != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreReporte", DbType.String, pNombreReporte);
                    if(pServidor != null)
                         vDataBase.AddInParameter(vDbCommand, "@Servidor", DbType.String, pServidor);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Reporte> vListaReporte = new List<Reporte>();
                        while (vDataReaderResults.Read())
                        {
                                Reporte vReporte = new Reporte();
                            vReporte.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vReporte.IdReporte;
                            vReporte.NombreReporte = vDataReaderResults["NombreReporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreReporte"].ToString()) : vReporte.NombreReporte;
                            vReporte.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vReporte.Descripcion;
                            vReporte.Servidor = vDataReaderResults["Servidor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Servidor"].ToString()) : vReporte.Servidor;
                            vReporte.Carpeta = vDataReaderResults["Carpeta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Carpeta"].ToString()) : vReporte.Carpeta;
                            vReporte.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vReporte.NombreArchivo;
                            vReporte.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReporte.UsuarioCrea;
                            vReporte.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReporte.FechaCrea;
                            vReporte.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReporte.UsuarioModifica;
                            vReporte.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReporte.FechaModifica;
                                vListaReporte.Add(vReporte);
                        }
                        return vListaReporte;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
