﻿using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Icbf.SIA.DataAccess
{
    public class GlobalAreasDAL : GeneralDAL
    {
        public List<GlobalAreas> ConsultarGlobalArea(int? pIdAreasInt, int? pIdRegionalInt)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_GLOBAL_ConsultarArea_Consultar"))
                {
                    if (pIdAreasInt != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdAreasInt", DbType.Int32, pIdAreasInt);
                    if (pIdRegionalInt != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDRegional", DbType.Int32, pIdRegionalInt);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GlobalAreas> vListaGlobalAreas = new List<GlobalAreas>();
                        while (vDataReaderResults.Read())
                        {
                            GlobalAreas vGlobalAreas = new GlobalAreas();
                            vGlobalAreas.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vGlobalAreas.IdArea;
                            vGlobalAreas.CodArea = vDataReaderResults["CodArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodArea"].ToString()) : vGlobalAreas.CodArea;
                            vGlobalAreas.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vGlobalAreas.NombreArea;
                            vGlobalAreas.EstadoArea = vDataReaderResults["EstadoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoArea"].ToString()) : vGlobalAreas.EstadoArea;

                            vListaGlobalAreas.Add(vGlobalAreas);
                        }
                        return vListaGlobalAreas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<GlobalAreas> ConsultarAreasRegionales(int? pIdAreasInt, String pIdRegionalesStr)
        {

            {
                try
                {
                    Database vDataBase = ObtenerInstancia();
                    using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_GLOBAL_ConsultarAreasRegional"))
                    {
                        if (pIdAreasInt != null)
                            vDataBase.AddInParameter(vDbCommand, "@IdAreaInt", DbType.Int32, pIdAreasInt);
                        if (pIdRegionalesStr != null)
                            vDataBase.AddInParameter(vDbCommand, "@IdRegionales", DbType.String, pIdRegionalesStr);

                        using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                        {
                            List<GlobalAreas> vListaAreasRegionales = new List<GlobalAreas>();
                            while (vDataReaderResults.Read())
                            {
                                GlobalAreas vGlobalAreas = new GlobalAreas();
                                vGlobalAreas.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vGlobalAreas.IdArea;
                                vGlobalAreas.CodArea = vDataReaderResults["CodArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodArea"].ToString()) : vGlobalAreas.CodArea;
                                vGlobalAreas.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vGlobalAreas.NombreArea;
                                vGlobalAreas.EstadoArea = vDataReaderResults["EstadoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoArea"].ToString()) : vGlobalAreas.EstadoArea;

                                vListaAreasRegionales.Add(vGlobalAreas);
                            }
                            return vListaAreasRegionales;
                        }
                    }
                }
                catch (UserInterfaceException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new GenericException(ex);
                }
            }
        }
    }
}
