using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad AdicionProyeccion
    /// </summary>
    public class AdicionProyeccionDAL : GeneralDAL
    {
        public AdicionProyeccionDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pAdicionProyeccion"></param>
        public int InsertarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProyeccion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAdicionProyeccion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pAdicionProyeccion.IdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicionado", DbType.Decimal, pAdicionProyeccion.ValorAdicionado);
                    vDataBase.AddInParameter(vDbCommand, "@Aprobado", DbType.Boolean, pAdicionProyeccion.Aprobado);
                    vDataBase.AddInParameter(vDbCommand, "@Rechazado", DbType.Boolean, pAdicionProyeccion.Rechazado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAdicionProyeccion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAdicionProyeccion.IdAdicionProyeccion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAdicionProyeccion").ToString());
                    GenerarLogAuditoria(pAdicionProyeccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pAdicionProyeccion"></param>
        public int ModificarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProyeccion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProyeccion", DbType.Int32, pAdicionProyeccion.IdAdicionProyeccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pAdicionProyeccion.IdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicionado", DbType.Decimal, pAdicionProyeccion.ValorAdicionado);
                    vDataBase.AddInParameter(vDbCommand, "@Aprobado", DbType.Boolean, pAdicionProyeccion.Aprobado);
                    vDataBase.AddInParameter(vDbCommand, "@Rechazado", DbType.Boolean, pAdicionProyeccion.Rechazado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAdicionProyeccion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdicionProyeccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pAdicionProyeccion"></param>
        public int EliminarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProyeccion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProyeccion", DbType.Int32, pAdicionProyeccion.IdAdicionProyeccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdicionProyeccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pIdAdicionProyeccion"></param>
        public AdicionProyeccion ConsultarAdicionProyeccion(int pIdAdicionProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProyeccion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProyeccion", DbType.Int32, pIdAdicionProyeccion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AdicionProyeccion vAdicionProyeccion = new AdicionProyeccion();
                        while (vDataReaderResults.Read())
                        {
                            vAdicionProyeccion.IdAdicionProyeccion = vDataReaderResults["IdAdicionProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProyeccion"].ToString()) : vAdicionProyeccion.IdAdicionProyeccion;
                            vAdicionProyeccion.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vAdicionProyeccion.IdProyeccionPresupuestos;
                            vAdicionProyeccion.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vAdicionProyeccion.ValorAdicionado;
                            vAdicionProyeccion.Aprobado = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Aprobado"].ToString()) : vAdicionProyeccion.Aprobado;
                            vAdicionProyeccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdicionProyeccion.UsuarioCrea;
                            vAdicionProyeccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdicionProyeccion.FechaCrea;
                            vAdicionProyeccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdicionProyeccion.UsuarioModifica;
                            vAdicionProyeccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdicionProyeccion.FechaModifica;
                        }
                        return vAdicionProyeccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pIdProyeccionPresupuestos"></param>
        /// <param name="pValorAdicionado"></param>
        /// <param name="pAprobado"></param>
        public AdicionProyeccion ConsultarAdicionProyeccions(int? pIdProyeccionPresupuestos, Decimal? pValorAdicionado, Boolean? pAprobado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProyeccions_Consultar"))
                {
                    if(pIdProyeccionPresupuestos != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    if(pValorAdicionado != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorAdicionado", DbType.Decimal, pValorAdicionado);
                    if(pAprobado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Aprobado", DbType.Boolean, pAprobado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AdicionProyeccion vListaAdicionProyeccion = new AdicionProyeccion();
                        while (vDataReaderResults.Read())
                        {
                                AdicionProyeccion vAdicionProyeccion = new AdicionProyeccion();
                            vAdicionProyeccion.IdAdicionProyeccion = vDataReaderResults["IdAdicionProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProyeccion"].ToString()) : vAdicionProyeccion.IdAdicionProyeccion;
                            vAdicionProyeccion.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vAdicionProyeccion.IdProyeccionPresupuestos;
                            vAdicionProyeccion.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vAdicionProyeccion.ValorAdicionado;
                            vAdicionProyeccion.Aprobado = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Aprobado"].ToString()) : vAdicionProyeccion.Aprobado;
                            vAdicionProyeccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdicionProyeccion.UsuarioCrea;
                            vAdicionProyeccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdicionProyeccion.FechaCrea;
                            vAdicionProyeccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdicionProyeccion.UsuarioModifica;
                            vAdicionProyeccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdicionProyeccion.FechaModifica;
                                vListaAdicionProyeccion=(vAdicionProyeccion);
                        }
                        return vListaAdicionProyeccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdProyeccionPresupuestos"></param>
        public List<ProyeccionAdiciones> ConsultarProyeccionAdiciones(int? pIdProyeccionPresupuestos, int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProyeccion_ConsultarProyeccion"))
                {
                    if (pIdProyeccionPresupuestos != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pIdArea != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pIdArea);
                    if (pUsuarioAprobo != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAprobo", DbType.String, pUsuarioAprobo);
                    if (pFechaAprobacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaAprobacion", DbType.DateTime, pFechaAprobacion);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdRubro != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRubro", DbType.Int32, pIdRubro);
                    if (CodEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, CodEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProyeccionAdiciones> vListaProyeccionPresupuestos = new List<ProyeccionAdiciones>();
                        
                        while (vDataReaderResults.Read())
                        {
                            ProyeccionAdiciones vProyeccionPresupuestos = new ProyeccionAdiciones();
                            vProyeccionPresupuestos.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vProyeccionPresupuestos.IdProyeccionPresupuestos;
                           // vProyeccionPresupuestos.IdAdicionProyeccion = vDataReaderResults["IdAdicionProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProyeccion"].ToString()) : vProyeccionPresupuestos.IdAdicionProyeccion;
                            vProyeccionPresupuestos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProyeccionPresupuestos.IdRegional;
                            vProyeccionPresupuestos.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vProyeccionPresupuestos.NombreRegional;
                            vProyeccionPresupuestos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProyeccionPresupuestos.IdVigencia;
                            vProyeccionPresupuestos.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vProyeccionPresupuestos.AcnoVigencia;
                            vProyeccionPresupuestos.IdRecurso = vDataReaderResults["IdRecurso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRecurso"].ToString()) : vProyeccionPresupuestos.IdRecurso;
                            vProyeccionPresupuestos.IdRubro = vDataReaderResults["IdRubro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRubro"].ToString()) : vProyeccionPresupuestos.IdRubro;
                            vProyeccionPresupuestos.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vProyeccionPresupuestos.DescripcionRubro;
                            vProyeccionPresupuestos.IdEstadoProceso = vDataReaderResults["IdEstadoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProceso"].ToString()) : vProyeccionPresupuestos.IdEstadoProceso;
                            vProyeccionPresupuestos.CodEstado = vDataReaderResults["CodEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodEstado"].ToString()) : vProyeccionPresupuestos.CodEstado;
                            vProyeccionPresupuestos.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vProyeccionPresupuestos.IdArea;
                            vProyeccionPresupuestos.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vProyeccionPresupuestos.NombreArea;
                            vProyeccionPresupuestos.ValorCupo = vDataReaderResults["ValorCupo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCupo"].ToString()) : vProyeccionPresupuestos.ValorCupo;
                            vProyeccionPresupuestos.ValorInicial = vDataReaderResults["ValorInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicial"].ToString()) : vProyeccionPresupuestos.ValorInicial;
                            vProyeccionPresupuestos.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vProyeccionPresupuestos.ValorAdicionado;
                            //vProyeccionPresupuestos.Aprobacion = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Aprobado"].ToString()) : vProyeccionPresupuestos.Aprobacion;
                            vProyeccionPresupuestos.TotalCupos = vDataReaderResults["TotalCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalCupos"].ToString()) : vProyeccionPresupuestos.TotalCupos;
                            vProyeccionPresupuestos.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vProyeccionPresupuestos.UsuarioAprobo;
                            vProyeccionPresupuestos.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vProyeccionPresupuestos.FechaAprobacion;
                            vProyeccionPresupuestos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProyeccionPresupuestos.UsuarioCrea;
                            vProyeccionPresupuestos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProyeccionPresupuestos.FechaCrea;
                            vProyeccionPresupuestos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProyeccionPresupuestos.UsuarioModifica;
                            vProyeccionPresupuestos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProyeccionPresupuestos.FechaModifica;
                            vListaProyeccionPresupuestos.Add(vProyeccionPresupuestos);
                        }

                        return vListaProyeccionPresupuestos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProyeccionAdiciones> ConsultarProyeccionAdicionesIndividual(int? pIdProyeccionPresupuestos, int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
          , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProyeccion_ConsultarProyeccionIndividual"))
                {
                    if (pIdProyeccionPresupuestos != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pIdArea != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pIdArea);
                    if (pUsuarioAprobo != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAprobo", DbType.String, pUsuarioAprobo);
                    if (pFechaAprobacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaAprobacion", DbType.DateTime, pFechaAprobacion);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdRubro != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRubro", DbType.Int32, pIdRubro);
                    if (CodEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, CodEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProyeccionAdiciones> vListaProyeccionPresupuestos = new List<ProyeccionAdiciones>();

                        while (vDataReaderResults.Read())
                        {
                            ProyeccionAdiciones vProyeccionPresupuestos = new ProyeccionAdiciones();
                            vProyeccionPresupuestos.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vProyeccionPresupuestos.IdProyeccionPresupuestos;
                            vProyeccionPresupuestos.IdAdicionProyeccion = vDataReaderResults["IdAdicionProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProyeccion"].ToString()) : vProyeccionPresupuestos.IdAdicionProyeccion;
                            vProyeccionPresupuestos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProyeccionPresupuestos.IdRegional;
                            vProyeccionPresupuestos.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vProyeccionPresupuestos.NombreRegional;
                            vProyeccionPresupuestos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProyeccionPresupuestos.IdVigencia;
                            vProyeccionPresupuestos.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vProyeccionPresupuestos.AcnoVigencia;
                            vProyeccionPresupuestos.IdRecurso = vDataReaderResults["IdRecurso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRecurso"].ToString()) : vProyeccionPresupuestos.IdRecurso;
                            vProyeccionPresupuestos.IdRubro = vDataReaderResults["IdRubro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRubro"].ToString()) : vProyeccionPresupuestos.IdRubro;
                            vProyeccionPresupuestos.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vProyeccionPresupuestos.DescripcionRubro;
                            vProyeccionPresupuestos.IdEstadoProceso = vDataReaderResults["IdEstadoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProceso"].ToString()) : vProyeccionPresupuestos.IdEstadoProceso;
                            vProyeccionPresupuestos.CodEstado = vDataReaderResults["CodEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodEstado"].ToString()) : vProyeccionPresupuestos.CodEstado;
                            vProyeccionPresupuestos.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vProyeccionPresupuestos.IdArea;
                            vProyeccionPresupuestos.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vProyeccionPresupuestos.NombreArea;
                            vProyeccionPresupuestos.ValorCupo = vDataReaderResults["ValorCupo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCupo"].ToString()) : vProyeccionPresupuestos.ValorCupo;
                            vProyeccionPresupuestos.ValorInicial = vDataReaderResults["ValorInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicial"].ToString()) : vProyeccionPresupuestos.ValorInicial;
                            vProyeccionPresupuestos.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vProyeccionPresupuestos.ValorAdicionado;
                            vProyeccionPresupuestos.Aprobacion = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Aprobado"].ToString()) : vProyeccionPresupuestos.Aprobacion;
                            vProyeccionPresupuestos.Rechazado = vDataReaderResults["Rechazado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Rechazado"].ToString()) : vProyeccionPresupuestos.Rechazado;
                            vProyeccionPresupuestos.TotalCupos = vDataReaderResults["TotalCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalCupos"].ToString()) : vProyeccionPresupuestos.TotalCupos;
                            vProyeccionPresupuestos.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vProyeccionPresupuestos.UsuarioAprobo;
                            vProyeccionPresupuestos.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vProyeccionPresupuestos.FechaAprobacion;
                            vProyeccionPresupuestos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProyeccionPresupuestos.UsuarioCrea;
                            vProyeccionPresupuestos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProyeccionPresupuestos.FechaCrea;
                            vProyeccionPresupuestos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProyeccionPresupuestos.UsuarioModifica;
                            vProyeccionPresupuestos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProyeccionPresupuestos.FechaModifica;
                            vProyeccionPresupuestos.RazonRechazo = vDataReaderResults["RazonRechazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonRechazo"].ToString()) : vProyeccionPresupuestos.RazonRechazo;
                            vListaProyeccionPresupuestos.Add(vProyeccionPresupuestos);
                        }

                        return vListaProyeccionPresupuestos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }





    }
}
