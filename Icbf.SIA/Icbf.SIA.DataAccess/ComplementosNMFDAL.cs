﻿using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.SIA.DataAccess
{
    public class ComplementosNMFDAL : GeneralDAL
    {

        public List<TipoRecursoFinPptal> ConsultarTipoRecursoFinPptal()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_NMF_ConsultarTipoRecursoPptal_Consultar"))
                {

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoRecursoFinPptal> vListaTipoRecursoFinPptal = new List<TipoRecursoFinPptal>();
                        while (vDataReaderResults.Read())
                        {
                            TipoRecursoFinPptal vTipoRecursoFinPptal = new TipoRecursoFinPptal();
                            vTipoRecursoFinPptal.IdTipoRecursoFinPptal = vDataReaderResults["IdTipoRecursoFinPptal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRecursoFinPptal"].ToString()) : vTipoRecursoFinPptal.IdTipoRecursoFinPptal;
                            vTipoRecursoFinPptal.CodTipoRecursoFinPptal = vDataReaderResults["CodTipoRecursoFinPptal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodTipoRecursoFinPptal"].ToString()) : vTipoRecursoFinPptal.CodTipoRecursoFinPptal;
                            vTipoRecursoFinPptal.DescTipoRecurso = vDataReaderResults["DescTipoRecurso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescTipoRecurso"].ToString()) : vTipoRecursoFinPptal.DescTipoRecurso;
                            vTipoRecursoFinPptal.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoRecursoFinPptal.Estado;

                            vListaTipoRecursoFinPptal.Add(vTipoRecursoFinPptal);
                        }
                        return vListaTipoRecursoFinPptal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TipoRecursoFinPptal ConsultarRecurso(int? pIdRecurso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_NMF_TipoRecursoPptal_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdTipoRecursoFinPptal", DbType.Int32, pIdRecurso);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoRecursoFinPptal vTipoRecursoFinPptal = new TipoRecursoFinPptal();
                        while (vDataReaderResults.Read())
                        {
                            vTipoRecursoFinPptal.IdTipoRecursoFinPptal = vDataReaderResults["IdTipoRecursoFinPptal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRecursoFinPptal"].ToString()) : vTipoRecursoFinPptal.IdTipoRecursoFinPptal;
                            vTipoRecursoFinPptal.CodTipoRecursoFinPptal = vDataReaderResults["CodTipoRecursoFinPptal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodTipoRecursoFinPptal"].ToString()) : vTipoRecursoFinPptal.CodTipoRecursoFinPptal;
                            vTipoRecursoFinPptal.DescTipoRecurso = vDataReaderResults["DescTipoRecurso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescTipoRecurso"].ToString()) : vTipoRecursoFinPptal.DescTipoRecurso;
                            vTipoRecursoFinPptal.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoRecursoFinPptal.Estado;
                        }
                        return vTipoRecursoFinPptal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CatGralRubrosPptalGasto> ConsultarCatGralRubrosPptalGasto(int? pIdAreasInt)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_NMF_ConsultarRubro_Consultar"))
                {
                    if (pIdAreasInt != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdAreasInt", DbType.Int32, pIdAreasInt);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CatGralRubrosPptalGasto> vListaCatGralRubrosPptalGasto = new List<CatGralRubrosPptalGasto>();
                        while (vDataReaderResults.Read())
                        {
                            CatGralRubrosPptalGasto vCatGralRubrosPptalGasto = new CatGralRubrosPptalGasto();
                            vCatGralRubrosPptalGasto.IdCatalogo = vDataReaderResults["IdCatalogo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCatalogo"].ToString()) : vCatGralRubrosPptalGasto.IdCatalogo;
                            vCatGralRubrosPptalGasto.RubroSIIF = vDataReaderResults["RubroSIIF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroSIIF"].ToString()) : vCatGralRubrosPptalGasto.RubroSIIF;
                            vCatGralRubrosPptalGasto.TipoRubro = vDataReaderResults["TipoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoRubro"].ToString()) : vCatGralRubrosPptalGasto.TipoRubro;
                            vCatGralRubrosPptalGasto.CtaPrograma = vDataReaderResults["CtaPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CtaPrograma"].ToString()) : vCatGralRubrosPptalGasto.CtaPrograma;
                            vCatGralRubrosPptalGasto.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vCatGralRubrosPptalGasto.DescripcionRubro;
                            vCatGralRubrosPptalGasto.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vCatGralRubrosPptalGasto.Estado;
                            vCatGralRubrosPptalGasto.DescripcionRubroCompleto = vCatGralRubrosPptalGasto.RubroSIIF + " - " + vCatGralRubrosPptalGasto.DescripcionRubro;

                            vListaCatGralRubrosPptalGasto.Add(vCatGralRubrosPptalGasto);
                        }
                        return vListaCatGralRubrosPptalGasto;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<AreasInternasNMF> ConsultarAreas(int? pIdAreasInt)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_NMF_ConsultarArea_Consultar"))
                {
                    if (pIdAreasInt != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdAreasInt", DbType.Int32, pIdAreasInt);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AreasInternasNMF> vListaAreasInternasNMF = new List<AreasInternasNMF>();
                        while (vDataReaderResults.Read())
                        {
                            AreasInternasNMF vAreasInternasNMF = new AreasInternasNMF();
                            vAreasInternasNMF.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vAreasInternasNMF.IdArea;
                            vAreasInternasNMF.CodArea = vDataReaderResults["CodArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodArea"].ToString()) : vAreasInternasNMF.CodArea;
                            vAreasInternasNMF.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vAreasInternasNMF.NombreArea;
                            vAreasInternasNMF.EstadoArea = vDataReaderResults["EstadoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoArea"].ToString()) : vAreasInternasNMF.EstadoArea;

                            vListaAreasInternasNMF.Add(vAreasInternasNMF);
                        }
                        return vListaAreasInternasNMF;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
