using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad HistoricoProrrogaCandidato
    /// </summary>
    public class HistoricoProrrogaCandidatoDAL : GeneralDAL
    {
        public HistoricoProrrogaCandidatoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pHistoricoProrrogaCandidato"></param>
        public int InsertarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoProrrogaCandidato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoProrrogaCandidato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pHistoricoProrrogaCandidato.IdCupoArea);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pHistoricoProrrogaCandidato.IdProyeccionPresuspuesto);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIncioAnterior", DbType.DateTime, pHistoricoProrrogaCandidato.FechaIncioAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIncioNueva", DbType.DateTime, pHistoricoProrrogaCandidato.FechaIncioNueva);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinAnterior", DbType.DateTime, pHistoricoProrrogaCandidato.FechaFinAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinNueva", DbType.DateTime, pHistoricoProrrogaCandidato.FechaFinNueva);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistoricoProrrogaCandidato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pHistoricoProrrogaCandidato.IdHistoricoProrrogaCandidato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoProrrogaCandidato").ToString());
                    GenerarLogAuditoria(pHistoricoProrrogaCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pHistoricoProrrogaCandidato"></param>
        public int ModificarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoProrrogaCandidato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoProrrogaCandidato", DbType.Int32, pHistoricoProrrogaCandidato.IdHistoricoProrrogaCandidato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pHistoricoProrrogaCandidato.IdCupoArea);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pHistoricoProrrogaCandidato.IdProyeccionPresuspuesto);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIncioAnterior", DbType.DateTime, pHistoricoProrrogaCandidato.FechaIncioAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIncioNueva", DbType.DateTime, pHistoricoProrrogaCandidato.FechaIncioNueva);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinAnterior", DbType.DateTime, pHistoricoProrrogaCandidato.FechaFinAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinNueva", DbType.DateTime, pHistoricoProrrogaCandidato.FechaFinNueva);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pHistoricoProrrogaCandidato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pHistoricoProrrogaCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pHistoricoProrrogaCandidato"></param>
        public int EliminarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoProrrogaCandidato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoProrrogaCandidato", DbType.Int32, pHistoricoProrrogaCandidato.IdHistoricoProrrogaCandidato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pHistoricoProrrogaCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pIdHistoricoProrrogaCandidato"></param>
        public HistoricoProrrogaCandidato ConsultarHistoricoProrrogaCandidato(int pIdHistoricoProrrogaCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoProrrogaCandidato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoProrrogaCandidato", DbType.Int32, pIdHistoricoProrrogaCandidato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        HistoricoProrrogaCandidato vHistoricoProrrogaCandidato = new HistoricoProrrogaCandidato();
                        while (vDataReaderResults.Read())
                        {
                            vHistoricoProrrogaCandidato.IdHistoricoProrrogaCandidato = vDataReaderResults["IdHistoricoProrrogaCandidato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoProrrogaCandidato"].ToString()) : vHistoricoProrrogaCandidato.IdHistoricoProrrogaCandidato;
                            vHistoricoProrrogaCandidato.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vHistoricoProrrogaCandidato.IdCupoArea;
                            vHistoricoProrrogaCandidato.IdProyeccionPresuspuesto = vDataReaderResults["IdProyeccionPresuspuesto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresuspuesto"].ToString()) : vHistoricoProrrogaCandidato.IdProyeccionPresuspuesto;
                            vHistoricoProrrogaCandidato.FechaIncioAnterior = vDataReaderResults["FechaIncioAnterior"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncioAnterior"].ToString()) : vHistoricoProrrogaCandidato.FechaIncioAnterior;
                            vHistoricoProrrogaCandidato.FechaIncioNueva = vDataReaderResults["FechaIncioNueva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncioNueva"].ToString()) : vHistoricoProrrogaCandidato.FechaIncioNueva;
                            vHistoricoProrrogaCandidato.FechaFinAnterior = vDataReaderResults["FechaFinAnterior"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinAnterior"].ToString()) : vHistoricoProrrogaCandidato.FechaFinAnterior;
                            vHistoricoProrrogaCandidato.FechaFinNueva = vDataReaderResults["FechaFinNueva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinNueva"].ToString()) : vHistoricoProrrogaCandidato.FechaFinNueva;
                            vHistoricoProrrogaCandidato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoProrrogaCandidato.UsuarioCrea;
                            vHistoricoProrrogaCandidato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoProrrogaCandidato.FechaCrea;
                            vHistoricoProrrogaCandidato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoProrrogaCandidato.UsuarioModifica;
                            vHistoricoProrrogaCandidato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoProrrogaCandidato.FechaModifica;
                        }
                        return vHistoricoProrrogaCandidato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pFechaIncioAnterior"></param>
        /// <param name="pFechaIncioNueva"></param>
        /// <param name="pFechaFinAnterior"></param>
        /// <param name="pFechaFinNueva"></param>
        public List<HistoricoProrrogaCandidato> ConsultarHistoricoProrrogaCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, DateTime? pFechaIncioAnterior, DateTime? pFechaIncioNueva, DateTime? pFechaFinAnterior, DateTime? pFechaFinNueva)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoProrrogaCandidatos_Consultar"))
                {
                    if(pIdCupoArea != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pIdCupoArea);
                    if(pIdProyeccionPresuspuesto != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pIdProyeccionPresuspuesto);
                    if(pFechaIncioAnterior != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaIncioAnterior", DbType.DateTime, pFechaIncioAnterior);
                    if(pFechaIncioNueva != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaIncioNueva", DbType.DateTime, pFechaIncioNueva);
                    if(pFechaFinAnterior != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaFinAnterior", DbType.DateTime, pFechaFinAnterior);
                    if(pFechaFinNueva != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaFinNueva", DbType.DateTime, pFechaFinNueva);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoProrrogaCandidato> vListaHistoricoProrrogaCandidato = new List<HistoricoProrrogaCandidato>();
                        while (vDataReaderResults.Read())
                        {
                                HistoricoProrrogaCandidato vHistoricoProrrogaCandidato = new HistoricoProrrogaCandidato();
                            vHistoricoProrrogaCandidato.IdHistoricoProrrogaCandidato = vDataReaderResults["IdHistoricoProrrogaCandidato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoProrrogaCandidato"].ToString()) : vHistoricoProrrogaCandidato.IdHistoricoProrrogaCandidato;
                            vHistoricoProrrogaCandidato.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vHistoricoProrrogaCandidato.IdCupoArea;
                            vHistoricoProrrogaCandidato.IdProyeccionPresuspuesto = vDataReaderResults["IdProyeccionPresuspuesto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresuspuesto"].ToString()) : vHistoricoProrrogaCandidato.IdProyeccionPresuspuesto;
                            vHistoricoProrrogaCandidato.FechaIncioAnterior = vDataReaderResults["FechaIncioAnterior"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncioAnterior"].ToString()) : vHistoricoProrrogaCandidato.FechaIncioAnterior;
                            vHistoricoProrrogaCandidato.FechaIncioNueva = vDataReaderResults["FechaIncioNueva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncioNueva"].ToString()) : vHistoricoProrrogaCandidato.FechaIncioNueva;
                            vHistoricoProrrogaCandidato.FechaFinAnterior = vDataReaderResults["FechaFinAnterior"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinAnterior"].ToString()) : vHistoricoProrrogaCandidato.FechaFinAnterior;
                            vHistoricoProrrogaCandidato.FechaFinNueva = vDataReaderResults["FechaFinNueva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinNueva"].ToString()) : vHistoricoProrrogaCandidato.FechaFinNueva;
                            vHistoricoProrrogaCandidato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoProrrogaCandidato.UsuarioCrea;
                            vHistoricoProrrogaCandidato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoProrrogaCandidato.FechaCrea;
                            vHistoricoProrrogaCandidato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoProrrogaCandidato.UsuarioModifica;
                            vHistoricoProrrogaCandidato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoProrrogaCandidato.FechaModifica;
                                vListaHistoricoProrrogaCandidato.Add(vHistoricoProrrogaCandidato);
                        }
                        return vListaHistoricoProrrogaCandidato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
