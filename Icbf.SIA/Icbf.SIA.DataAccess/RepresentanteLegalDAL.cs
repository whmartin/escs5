using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    public class RepresentanteLegalDAL : GeneralDAL
    {
        public RepresentanteLegalDAL()
        {
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int InsertarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_ECO_RepresentanteLegal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRepresentanteLegal", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pRepresentanteLegal.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pRepresentanteLegal.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pRepresentanteLegal.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdCentroPoblado", DbType.Int32, pRepresentanteLegal.IdCentroPoblado);
                    vDataBase.AddInParameter(vDbCommand, "@IdComuna", DbType.Int32, pRepresentanteLegal.IdComuna);
                    vDataBase.AddInParameter(vDbCommand, "@IdBarrio", DbType.Int32, pRepresentanteLegal.IdBarrio);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pRepresentanteLegal.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pRepresentanteLegal.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pRepresentanteLegal.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pRepresentanteLegal.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pRepresentanteLegal.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pRepresentanteLegal.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pRepresentanteLegal.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@ZonaUbicacion", DbType.String, pRepresentanteLegal.ZonaUbicacion);
                    //vDataBase.AddInParameter(vDbCommand, "@TipoCabecera", DbType.String, pRepresentanteLegal.TipoCabecera);
                    vDataBase.AddInParameter(vDbCommand, "@NombreZonaResto", DbType.String, pRepresentanteLegal.NombreZonaResto);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pRepresentanteLegal.Direccion);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pRepresentanteLegal.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRepresentanteLegal.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRepresentanteLegal.IdRepresentanteLegal = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRepresentanteLegal").ToString());
                    GenerarLogAuditoria(pRepresentanteLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int ModificarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_ECO_RepresentanteLegal_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRepresentanteLegal", DbType.Int32, pRepresentanteLegal.IdRepresentanteLegal);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pRepresentanteLegal.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pRepresentanteLegal.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pRepresentanteLegal.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdCentroPoblado", DbType.Int32, pRepresentanteLegal.IdCentroPoblado);
                    vDataBase.AddInParameter(vDbCommand, "@IdComuna", DbType.Int32, pRepresentanteLegal.IdComuna);
                    vDataBase.AddInParameter(vDbCommand, "@IdBarrio", DbType.Int32, pRepresentanteLegal.IdBarrio);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pRepresentanteLegal.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pRepresentanteLegal.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pRepresentanteLegal.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pRepresentanteLegal.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pRepresentanteLegal.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pRepresentanteLegal.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pRepresentanteLegal.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@ZonaUbicacion", DbType.String, pRepresentanteLegal.ZonaUbicacion);
                    //vDataBase.AddInParameter(vDbCommand, "@TipoCabecera", DbType.String, pRepresentanteLegal.TipoCabecera);
                    vDataBase.AddInParameter(vDbCommand, "@NombreZonaResto", DbType.String, pRepresentanteLegal.NombreZonaResto);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pRepresentanteLegal.Direccion);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pRepresentanteLegal.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRepresentanteLegal.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRepresentanteLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int EliminarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_ECO_RepresentanteLegal_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRepresentanteLegal", DbType.Int32, pRepresentanteLegal.IdRepresentanteLegal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRepresentanteLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pIdRepresentanteLegal"></param>
        /// <returns></returns>
        public RepresentanteLegal ConsultarRepresentanteLegal(int pIdRepresentanteLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_ECO_RepresentanteLegal_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRepresentanteLegal", DbType.Int32, pIdRepresentanteLegal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RepresentanteLegal vRepresentanteLegal = new RepresentanteLegal();
                        while (vDataReaderResults.Read())
                        {
                            vRepresentanteLegal.IdRepresentanteLegal = vDataReaderResults["IdRepresentanteLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRepresentanteLegal"].ToString()) : vRepresentanteLegal.IdRepresentanteLegal;
                            vRepresentanteLegal.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vRepresentanteLegal.IdTipoDocumento;
                            vRepresentanteLegal.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vRepresentanteLegal.IdDepartamento;
                            vRepresentanteLegal.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vRepresentanteLegal.IdMunicipio;
                            vRepresentanteLegal.IdCentroPoblado = vDataReaderResults["IdCentroPoblado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCentroPoblado"].ToString()) : vRepresentanteLegal.IdCentroPoblado;
                            vRepresentanteLegal.IdComuna = vDataReaderResults["IdComuna"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComuna"].ToString()) : vRepresentanteLegal.IdComuna;
                            vRepresentanteLegal.IdBarrio = vDataReaderResults["IdBarrio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdBarrio"].ToString()) : vRepresentanteLegal.IdBarrio;
                            vRepresentanteLegal.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : vRepresentanteLegal.Identificacion;
                            vRepresentanteLegal.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vRepresentanteLegal.PrimerNombre;
                            vRepresentanteLegal.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vRepresentanteLegal.SegundoNombre;
                            vRepresentanteLegal.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vRepresentanteLegal.PrimerApellido;
                            vRepresentanteLegal.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vRepresentanteLegal.SegundoApellido;
                            vRepresentanteLegal.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vRepresentanteLegal.Telefono;
                            vRepresentanteLegal.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vRepresentanteLegal.Celular;
                            vRepresentanteLegal.ZonaUbicacion = vDataReaderResults["ZonaUbicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ZonaUbicacion"].ToString()) : vRepresentanteLegal.ZonaUbicacion;
                            //vRepresentanteLegal.TipoCabecera = vDataReaderResults["TipoCabecera"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCabecera"].ToString()) : vRepresentanteLegal.TipoCabecera;
                            vRepresentanteLegal.NombreZonaResto = vDataReaderResults["NombreZonaResto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreZonaResto"].ToString()) : vRepresentanteLegal.NombreZonaResto;
                            vRepresentanteLegal.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vRepresentanteLegal.Direccion;
                            vRepresentanteLegal.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vRepresentanteLegal.CorreoElectronico;

                            vRepresentanteLegal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRepresentanteLegal.UsuarioCrea;
                            vRepresentanteLegal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRepresentanteLegal.FechaCrea;
                            vRepresentanteLegal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRepresentanteLegal.UsuarioModifica;
                            vRepresentanteLegal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRepresentanteLegal.FechaModifica;
                        }
                        return vRepresentanteLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pIdTipoDocumento"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdMunicipio"></param>
        /// <param name="pIdentificacion"></param>
        /// <param name="pPrimerNombre"></param>
        /// <param name="pSegundoNombre"></param>
        /// <param name="pPrimerApellido"></param>
        /// <param name="pSegundoApellido"></param>
        /// <returns></returns>
        public List<RepresentanteLegal> ConsultarRepresentanteLegals(int? pIdTipoDocumento, int? pIdDepartamento, int? pIdMunicipio, String pIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_ECO_RepresentanteLegal_sConsultar"))
                {
                    if(pIdTipoDocumento != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pIdTipoDocumento);
                    if(pIdDepartamento != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    if(pIdMunicipio != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pIdMunicipio);
                    if(pIdentificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pIdentificacion);
                    if(pPrimerNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pPrimerNombre);
                    if(pSegundoNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pSegundoNombre);
                    if(pPrimerApellido != null)
                         vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pPrimerApellido);
                    if(pSegundoApellido != null)
                         vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pSegundoApellido);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RepresentanteLegal> vListaRepresentanteLegal = new List<RepresentanteLegal>();
                        while (vDataReaderResults.Read())
                        {
                                RepresentanteLegal vRepresentanteLegal = new RepresentanteLegal();
                            vRepresentanteLegal.IdRepresentanteLegal = vDataReaderResults["IdRepresentanteLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRepresentanteLegal"].ToString()) : vRepresentanteLegal.IdRepresentanteLegal;
                            vRepresentanteLegal.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vRepresentanteLegal.IdTipoDocumento;
                            vRepresentanteLegal.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vRepresentanteLegal.IdDepartamento;
                            vRepresentanteLegal.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vRepresentanteLegal.NombreDepartamento;
                            vRepresentanteLegal.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vRepresentanteLegal.IdMunicipio;
                            vRepresentanteLegal.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vRepresentanteLegal.NombreMunicipio;
                            vRepresentanteLegal.IdCentroPoblado = vDataReaderResults["IdCentroPoblado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCentroPoblado"].ToString()) : vRepresentanteLegal.IdCentroPoblado;
                            vRepresentanteLegal.IdComuna = vDataReaderResults["IdComuna"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComuna"].ToString()) : vRepresentanteLegal.IdComuna;
                            vRepresentanteLegal.IdBarrio = vDataReaderResults["IdBarrio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdBarrio"].ToString()) : vRepresentanteLegal.IdBarrio;
                            vRepresentanteLegal.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Identificacion"].ToString()) : vRepresentanteLegal.Identificacion;
                            vRepresentanteLegal.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vRepresentanteLegal.PrimerNombre;
                            vRepresentanteLegal.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vRepresentanteLegal.SegundoNombre;
                            vRepresentanteLegal.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vRepresentanteLegal.PrimerApellido;
                            vRepresentanteLegal.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vRepresentanteLegal.SegundoApellido;
                            vRepresentanteLegal.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vRepresentanteLegal.Telefono;
                            vRepresentanteLegal.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vRepresentanteLegal.Celular;
                            vRepresentanteLegal.ZonaUbicacion = vDataReaderResults["ZonaUbicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ZonaUbicacion"].ToString()) : vRepresentanteLegal.ZonaUbicacion;
                            //vRepresentanteLegal.TipoCabecera = vDataReaderResults["TipoCabecera"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCabecera"].ToString()) : vRepresentanteLegal.TipoCabecera;
                            vRepresentanteLegal.NombreZonaResto = vDataReaderResults["NombreZonaResto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreZonaResto"].ToString()) : vRepresentanteLegal.NombreZonaResto;
                            vRepresentanteLegal.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vRepresentanteLegal.Direccion;
                            vRepresentanteLegal.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vRepresentanteLegal.CorreoElectronico;
                            vRepresentanteLegal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRepresentanteLegal.UsuarioCrea;
                            vRepresentanteLegal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRepresentanteLegal.FechaCrea;
                            vRepresentanteLegal.CodDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vRepresentanteLegal.CodDocumento;

                            vRepresentanteLegal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRepresentanteLegal.UsuarioCrea;
                            vRepresentanteLegal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRepresentanteLegal.FechaCrea;
                            vRepresentanteLegal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRepresentanteLegal.UsuarioModifica;
                            vRepresentanteLegal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRepresentanteLegal.FechaModifica;

                            vListaRepresentanteLegal.Add(vRepresentanteLegal);
                        }
                        return vListaRepresentanteLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
