using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad CategoriaEmpleado
    /// </summary>
    public class CategoriaEmpleadoDAL : GeneralDAL
    {
        public CategoriaEmpleadoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int InsertarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaEmpleado_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCategoria", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pCategoriaEmpleado.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pCategoriaEmpleado.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pCategoriaEmpleado.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCategoriaEmpleado.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoProducto", DbType.String, pCategoriaEmpleado.CodigoProducto);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pCategoriaEmpleado.Vigencia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCategoriaEmpleado.IdCategoria = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCategoria").ToString());
                    GenerarLogAuditoria(pCategoriaEmpleado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int ModificarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaEmpleado_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoria", DbType.Int32, pCategoriaEmpleado.IdCategoria);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pCategoriaEmpleado.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pCategoriaEmpleado.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pCategoriaEmpleado.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoProducto", DbType.String, pCategoriaEmpleado.CodigoProducto);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pCategoriaEmpleado.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCategoriaEmpleado.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCategoriaEmpleado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int EliminarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaEmpleado_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoria", DbType.Int32, pCategoriaEmpleado.IdCategoria);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCategoriaEmpleado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pIdCategoria"></param>
        public CategoriaEmpleado ConsultarCategoriaEmpleado(int pIdCategoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaEmpleado_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoria", DbType.Int32, pIdCategoria);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CategoriaEmpleado vCategoriaEmpleado = new CategoriaEmpleado();
                        while (vDataReaderResults.Read())
                        {
                            vCategoriaEmpleado.IdCategoria = vDataReaderResults["IdCategoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoria"].ToString()) : vCategoriaEmpleado.IdCategoria;
                            vCategoriaEmpleado.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCategoriaEmpleado.Descripcion;
                            vCategoriaEmpleado.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vCategoriaEmpleado.Codigo;
                            vCategoriaEmpleado.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vCategoriaEmpleado.Activo;
                            vCategoriaEmpleado.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vCategoriaEmpleado.CodigoProducto;
                            vCategoriaEmpleado.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vCategoriaEmpleado.Vigencia;
                            vCategoriaEmpleado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCategoriaEmpleado.UsuarioCrea;
                            vCategoriaEmpleado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCategoriaEmpleado.FechaCrea;
                            vCategoriaEmpleado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCategoriaEmpleado.UsuarioModifica;
                            vCategoriaEmpleado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCategoriaEmpleado.FechaModifica;
                        }
                        return vCategoriaEmpleado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pActivo"></param>
        public List<CategoriaEmpleado> ConsultarCategoriaEmpleados(String pCodigo, Boolean? pActivo, String pDescripcion, String pCodigoProducto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaEmpleados_Consultar"))
                {
                    if (pCodigo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pCodigo);
                    if (pActivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pActivo);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pCodigoProducto != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoProducto", DbType.String, pCodigoProducto);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CategoriaEmpleado> vListaCategoriaEmpleado = new List<CategoriaEmpleado>();
                        while (vDataReaderResults.Read())
                        {
                            CategoriaEmpleado vCategoriaEmpleado = new CategoriaEmpleado();
                            vCategoriaEmpleado.IdCategoria = vDataReaderResults["IdCategoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoria"].ToString()) : vCategoriaEmpleado.IdCategoria;
                            vCategoriaEmpleado.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCategoriaEmpleado.Descripcion;
                            vCategoriaEmpleado.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vCategoriaEmpleado.Codigo;
                            vCategoriaEmpleado.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vCategoriaEmpleado.Activo;
                            vCategoriaEmpleado.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vCategoriaEmpleado.CodigoProducto;
                            vCategoriaEmpleado.Vigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vCategoriaEmpleado.Vigencia;
                            vCategoriaEmpleado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCategoriaEmpleado.UsuarioCrea;
                            vCategoriaEmpleado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCategoriaEmpleado.FechaCrea;
                            vCategoriaEmpleado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCategoriaEmpleado.UsuarioModifica;
                            vCategoriaEmpleado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCategoriaEmpleado.FechaModifica;
                            vListaCategoriaEmpleado.Add(vCategoriaEmpleado);
                        }
                        return vListaCategoriaEmpleado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CategoriaEmpleado> ConsultarCategoriaEmpleadoAsignada(int? pIdProyeccionPresupuestos, String pCodEstadoProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaEmpleadoAsignada_Consultar"))
                {
                    if (pIdProyeccionPresupuestos != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);

                    if (pCodEstadoProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstadoProceso", DbType.String, pCodEstadoProceso);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CategoriaEmpleado> vListaCategoriaEmpleadoAsig = new List<CategoriaEmpleado>();
                        while (vDataReaderResults.Read())
                        {
                            CategoriaEmpleado vCategoriaEmpleadoAsig = new CategoriaEmpleado();
                            vCategoriaEmpleadoAsig.IdCategoria = vDataReaderResults["IDCategoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCategoria"].ToString()) : vCategoriaEmpleadoAsig.IdCategoria;
                            vCategoriaEmpleadoAsig.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCategoriaEmpleadoAsig.Descripcion;

                            vListaCategoriaEmpleadoAsig.Add(vCategoriaEmpleadoAsig);
                        }
                        return vListaCategoriaEmpleadoAsig;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
