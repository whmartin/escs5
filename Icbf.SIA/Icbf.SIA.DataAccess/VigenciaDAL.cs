using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    public class VigenciaDAL : GeneralDAL
    {
        public VigenciaDAL()
        {
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad Vigencia
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <returns></returns>
        public Vigencia ConsultarVigencia(int pIdVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_Vigencia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Vigencia vVigencia = new Vigencia();
                        while (vDataReaderResults.Read())
                        {
                            vVigencia.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vVigencia.IdVigencia;
                            vVigencia.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vVigencia.AcnoVigencia;
                            vVigencia.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Activo"].ToString()) : vVigencia.Activo;

                            vVigencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVigencia.UsuarioCrea;
                            vVigencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVigencia.FechaCrea;
                            vVigencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVigencia.UsuarioModifica;
                            vVigencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVigencia.FechaModifica;

                        }
                        return vVigencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Vigencia
        /// </summary>
        /// <param name="pActivo"></param>
        /// <returns></returns>
        public List<Vigencia> ConsultarVigenciasSinAnnoActual(String pActivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_Vigencias_SinAnnoActual"))
                {
                    if (pActivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.String, pActivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Vigencia> vListaVigencia = new List<Vigencia>();
                        while (vDataReaderResults.Read())
                        {
                            Vigencia vVigencia = new Vigencia();
                            vVigencia.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vVigencia.IdVigencia;
                            vVigencia.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vVigencia.AcnoVigencia;
                            vVigencia.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Activo"].ToString()) : vVigencia.Activo;

                            vVigencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVigencia.UsuarioCrea;
                            vVigencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVigencia.FechaCrea;
                            vVigencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVigencia.UsuarioModifica;
                            vVigencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVigencia.FechaModifica;

                            vListaVigencia.Add(vVigencia);
                        }
                        return vListaVigencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Vigencia
        /// </summary>
        /// <param name="pActivo">Valor booleano que representa el estado del registro activo o inactivo</param>
        /// <returns>Lista de la instancia Vigencia</returns>
        public List<Vigencia> ConsultarVigencias(bool? pActivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_Vigencia_sConsultar"))
                {
                    if (pActivo != null)
                    {
                        string pActivoParam = "0";
                        if (pActivo == true)
                            pActivoParam = "1";
                        vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.String, pActivoParam);
                    }
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Vigencia> vListaVigencia = new List<Vigencia>();
                        while (vDataReaderResults.Read())
                        {
                            Vigencia vVigencia = new Vigencia();
                            vVigencia.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vVigencia.IdVigencia;
                            vVigencia.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vVigencia.AcnoVigencia;
                            vVigencia.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Activo"].ToString()) : vVigencia.Activo;

                            vVigencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vVigencia.UsuarioCrea;
                            vVigencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vVigencia.FechaCrea;
                            vVigencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vVigencia.UsuarioModifica;
                            vVigencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vVigencia.FechaModifica;

                            vListaVigencia.Add(vVigencia);
                        }
                        return vListaVigencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ConsultarVigenciasExistenteAnio(int pIdContrato, int pAnio)
        {
            
                try
                {
                    Database vDataBase = ObtenerInstancia();
                    using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_VigenciaFuturas_Existe"))
                    {
                        int vResultado;
                        bool vResult;

                        vDataBase.AddOutParameter(vDbCommand, "@Result", DbType.Boolean, 18);
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                        vDataBase.AddInParameter(vDbCommand, "@AnioVigencia", DbType.Int32, pAnio);
                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                        vResult = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@Result").ToString());
                        return vResult;

                    }
                }
                catch (UserInterfaceException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new GenericException(ex);
                }
           
        }
    }
}
