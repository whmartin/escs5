﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.SIA.DataAccess
{
    public class UsuarioDAL : GeneralDAL
    {

        public UsuarioDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad Usuario
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int InsertarUsuario(Usuario pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_InsertarUsuario"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdUsuario", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pUsuario.IdTipoPersona == null ? 0 : pUsuario.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pUsuario.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pUsuario.NumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@DV", DbType.String, pUsuario.DV == null ? string.Empty : pUsuario.DV);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pUsuario.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pUsuario.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pUsuario.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pUsuario.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pUsuario.RazonSocial == null ? string.Empty : pUsuario.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@TelefonoContacto", DbType.String, pUsuario.TelefonoContacto == null ? string.Empty : pUsuario.TelefonoContacto);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pUsuario.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pUsuario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@Providerkey", DbType.String, pUsuario.Providerkey);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pUsuario.UsuarioCreacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pUsuario.IdTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pUsuario.IdRegional);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pUsuario.IdUsuario = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdUsuario").ToString());
                    GenerarLogAuditoria(pUsuario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Usuario
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int ModificarUsuario(Usuario pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ModificarUsuario"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pUsuario.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pUsuario.IdTipoPersona == null ? 0 : pUsuario.IdTipoPersona);
                    //vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pUsuario.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pUsuario.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pUsuario.NumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@DV", DbType.String, pUsuario.DV == null ? string.Empty : pUsuario.DV);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pUsuario.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pUsuario.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pUsuario.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pUsuario.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pUsuario.RazonSocial == null ? string.Empty : pUsuario.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@TelefonoContacto", DbType.String, pUsuario.TelefonoContacto == null ? string.Empty : pUsuario.TelefonoContacto);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pUsuario.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pUsuario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModificacion", DbType.String, pUsuario.UsuarioModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pUsuario.IdTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pUsuario.IdRegional);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUsuario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para cambio de correo de un usuario Proveedor, correo como usarName
        /// </summary>
        /// <param name="pcorreoErrado"></param>
        /// <param name="pcorreoNuevo"></param>
        /// <param name="pnumeroDocumento"></param>
        /// <returns></returns>
        public int modificarCorreoUsuario(Usuario pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ModificarCorreoUsuario"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@correoErrado", DbType.String, pUsuario.NombreUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@correoNuevo", DbType.String, pUsuario.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@identificacion", DbType.String, pUsuario.NumeroDocumento);                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUsuario, vDbCommand);
                    return vResultado;
                }

            }
            catch (UserInterfaceException)
            {
                
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int modificarCorreoUsuarioQoS(Usuario pUsuario, string appName)
        {
            try 
	        {
                Database vDataBase = ObtenerInstanciaQoS();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ModificarNombreUsuarioQos"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@correoErrado", DbType.String, pUsuario.NombreUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@correoNuevo", DbType.String, pUsuario.CorreoElectronico);
                    vDataBase.AddInParameter(vDbCommand, "@appName", DbType.String, appName);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUsuario, vDbCommand);
                    return vResultado;
                }
		
	        }
            catch (UserInterfaceException)
            {

                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

       

  




        /// <summary>
        /// Método de eliminación para la entidad Usuario
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int EliminarUsuario(Usuario pUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("spEliminarUsario"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pUsuario.IdUsuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pUsuario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por ProviderKey para la entidad Usuario
        /// </summary>
        /// <param name="pProviderKey"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(string pProviderKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ConsultarUsuarioPorProviderKey"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pProviderKey);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Usuario vUsuario = new Usuario();
                        while (vDataReaderResults.Read())
                        {
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vUsuario.IdTipoDocumento;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vUsuario.NumeroDocumento;
                            vUsuario.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vUsuario.PrimerNombre;
                            vUsuario.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vUsuario.SegundoNombre;
                            vUsuario.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vUsuario.PrimerApellido;
                            vUsuario.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vUsuario.SegundoApellido;
                            vUsuario.TelefonoContacto = vDataReaderResults["TelefonoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TelefonoContacto"].ToString()) : vUsuario.TelefonoContacto;
                            vUsuario.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vUsuario.CorreoElectronico;
                            vUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vUsuario.Estado;
                            vUsuario.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vUsuario.Providerkey;
                            vUsuario.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vUsuario.UsuarioCreacion;
                            vUsuario.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vUsuario.FechaCreacion;
                            vUsuario.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vUsuario.UsuarioModificacion;
                            vUsuario.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vUsuario.FechaModificacion;
                            vUsuario.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vUsuario.IdTipoPersona;
                            vUsuario.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vUsuario.RazonSocial;
                            vUsuario.DV = vDataReaderResults["DV"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DV"].ToString()) : vUsuario.DV;
                            vUsuario.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vUsuario.IdRegional;
                            vUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vUsuario.IdTipoUsuario;
                        }
                        return vUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Método de consulta por id para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(int pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ConsultarUsuario"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Usuario vUsuario = new Usuario();
                        while (vDataReaderResults.Read())
                        {
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.TipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vUsuario.TipoUsuario;
                            vUsuario.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vUsuario.IdTipoDocumento;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vUsuario.NumeroDocumento;
                            vUsuario.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vUsuario.PrimerNombre;
                            vUsuario.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vUsuario.SegundoNombre;
                            vUsuario.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vUsuario.PrimerApellido;
                            vUsuario.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vUsuario.SegundoApellido;
                            vUsuario.TelefonoContacto = vDataReaderResults["TelefonoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TelefonoContacto"].ToString()) : vUsuario.TelefonoContacto;
                            vUsuario.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vUsuario.CorreoElectronico;
                            vUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vUsuario.Estado;
                            vUsuario.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vUsuario.Providerkey;
                            vUsuario.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vUsuario.UsuarioCreacion;
                            vUsuario.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vUsuario.FechaCreacion;
                            vUsuario.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vUsuario.UsuarioModificacion;
                            vUsuario.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vUsuario.FechaModificacion;
                            vUsuario.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vUsuario.IdTipoPersona;
                            vUsuario.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vUsuario.RazonSocial;
                            vUsuario.DV = vDataReaderResults["DV"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DV"].ToString()) : vUsuario.DV;
                            vUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vUsuario.IdTipoUsuario;
                            vUsuario.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vUsuario.IdRegional;
                        }
                        return vUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Usuario
        /// </summary>
        /// <param name="pNumeroDocumento"></param>
        /// <param name="pPrimerNombre"></param>
        /// <param name="pPrimerApellido"></param>
        /// <param name="pPerfil"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<Usuario> ConsultarUsuarios(String pNumeroDocumento, String pPrimerNombre, String pPrimerApellido, int? pPerfil, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ConsultarUsuarios"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPerfil", DbType.Int32, pPerfil);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pNumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pPrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pPrimerApellido);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Usuario> vListaUsuarios = new List<Usuario>();

                        while (vDataReaderResults.Read())
                        {
                            Usuario vUsuario = new Usuario();
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vUsuario.IdTipoDocumento;
                            vUsuario.TipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vUsuario.TipoDocumento;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vUsuario.NumeroDocumento;
                            vUsuario.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vUsuario.PrimerNombre;
                            vUsuario.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vUsuario.SegundoNombre;
                            vUsuario.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vUsuario.PrimerApellido;
                            vUsuario.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vUsuario.SegundoApellido;
                            vUsuario.TelefonoContacto = vDataReaderResults["TelefonoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TelefonoContacto"].ToString()) : vUsuario.TelefonoContacto;
                            vUsuario.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vUsuario.CorreoElectronico;
                            vUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vUsuario.Estado;
                            vUsuario.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vUsuario.Providerkey;
                            vUsuario.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vUsuario.UsuarioCreacion;
                            vUsuario.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vUsuario.FechaCreacion;
                            vUsuario.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vUsuario.UsuarioModificacion;
                            vUsuario.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vUsuario.FechaModificacion;
                            vUsuario.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vUsuario.IdTipoPersona;
                            vUsuario.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vUsuario.RazonSocial;
                            vUsuario.DV = vDataReaderResults["DV"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DV"].ToString()) : vUsuario.DV;
                            vUsuario.IdRegional = vDataReaderResults["idRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idRegional"].ToString()) : vUsuario.IdRegional;
                            vListaUsuarios.Add(vUsuario);
                        }
                        return vListaUsuarios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id y NumeroDocumento para la entidad Usuario
        /// </summary>
        /// <param name="pTipoDocumento"></param>
        /// <param name="pNumeroDocumento"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(int pTipoDocumento, string pNumeroDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Seg_ConsultarUsuarioPorDocumento"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdTipoDocumento", DbType.Int32, pTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@pNumeroDocumento", DbType.String, pNumeroDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Usuario vUsuario = new Usuario();
                        while (vDataReaderResults.Read())
                        {
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vUsuario.IdTipoDocumento;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vUsuario.NumeroDocumento;
                            vUsuario.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vUsuario.PrimerNombre;
                            vUsuario.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vUsuario.SegundoNombre;
                            vUsuario.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vUsuario.PrimerApellido;
                            vUsuario.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vUsuario.SegundoApellido;
                            vUsuario.TelefonoContacto = vDataReaderResults["TelefonoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TelefonoContacto"].ToString()) : vUsuario.TelefonoContacto;
                            vUsuario.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vUsuario.CorreoElectronico;
                            vUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vUsuario.Estado;
                            vUsuario.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vUsuario.Providerkey;
                            vUsuario.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vUsuario.UsuarioCreacion;
                            vUsuario.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vUsuario.FechaCreacion;
                            vUsuario.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vUsuario.UsuarioModificacion;
                            vUsuario.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vUsuario.FechaModificacion;
                            vUsuario.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vUsuario.IdTipoPersona;
                            vUsuario.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vUsuario.RazonSocial;
                            vUsuario.DV = vDataReaderResults["DV"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DV"].ToString()) : vUsuario.DV;

                        }
                        return vUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Creado Por Juan Carlos Valverde Sámano
        /// Método de modificación para la Razon Social de un Usuario
        /// </summary>
        /// <param name="pRazonSocial"></param>
        /// <param name="pproviderKey"></param>
        /// <returns></returns>
        public bool ModificarRazonSocialDeUsuario(string pRazonSocial, string pproviderKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_Usuario_ModificarRazonSocial"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pRazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@providerLey", DbType.String, pproviderKey);
                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        /// <summary>
        /// Método de consulta por NumeroIdentificacion para la entidad Usuario
        /// </summary>
        /// <param name="pNumeroDocumento"></param>
        /// /// <param name="pNombre"></param>
        public List<Usuario> ConsultarUsuarioss(string pNumeroDocumento, string pNombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Usuarioss_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pNumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Usuario> vListaUsuarios = new List<Usuario>();

                        while (vDataReaderResults.Read())
                        {
                            Usuario vUsuario = new Usuario();
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vUsuario.NumeroDocumento;
                            vUsuario.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vUsuario.NombreCompleto;
                            vUsuario.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vUsuario.NombreArea;

                            vListaUsuarios.Add(vUsuario);
                        }
                        return vListaUsuarios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por Id Usuario para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        public Usuario ConsultarUsuarioPorId(int pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_UsuarioPorId_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Usuario vUsuario = new Usuario();

                        while (vDataReaderResults.Read())
                        {
                            vUsuario.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vUsuario.IdUsuario;
                            vUsuario.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vUsuario.NumeroDocumento;
                            vUsuario.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreCompleto"].ToString()) : vUsuario.NombreCompleto;
                            vUsuario.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vUsuario.NombreArea;
                        }
                        return vUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
