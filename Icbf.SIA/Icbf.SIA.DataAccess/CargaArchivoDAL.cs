﻿using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.SIA.Entity;
using System.Linq;
using System.Text;

namespace Icbf.SIA.DataAccess
{
    public class CargaArchivoDAL : GeneralDAL
    {
        public CargaArchivoDAL()
        {
        }

        /// <summary>
        /// Método de para la carga de datos desde excel a xml a BD, retornar errores en la carga
        /// </summary>

        public List<DescripcionErrorCarga> CargarDatosArchivo(String xml)
        {
            try
            {
                if (xml.Contains("<Table />"))
                    xml = xml.Replace("<Table />", "");

                if (xml.Contains(">1<"))
                    xml = xml.Replace(">1<", ">01<");
                if (xml.Contains(">2<"))
                    xml = xml.Replace(">2<", ">02<");
                if (xml.Contains(">3<"))
                    xml = xml.Replace(">3<", ">03<");
                if (xml.Contains(">4<"))
                    xml = xml.Replace(">4<", ">04<");
                if (xml.Contains(">5<"))
                    xml = xml.Replace(">5<", ">05<");
                if (xml.Contains(">6<"))
                    xml = xml.Replace(">6<", ">06<");
                if (xml.Contains(">7<"))
                    xml = xml.Replace(">7<", ">07<");
                if (xml.Contains(">8<"))
                    xml = xml.Replace(">8<", ">08<");
                if (xml.Contains(">9<"))
                    xml = xml.Replace(">9<", ">09<");

                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_insert_xml_ProyeccionNecesidades"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@param", DbType.String, xml);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DescripcionErrorCarga> vListaDescripcionError = new List<DescripcionErrorCarga>();
                        while (vDataReaderResults.Read())
                        {
                            DescripcionErrorCarga vDescripcionError = new DescripcionErrorCarga();
                            vDescripcionError.DescripcionError = vDataReaderResults["DescripcionError"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionError"].ToString()) : vDescripcionError.DescripcionError;
                            vListaDescripcionError.Add(vDescripcionError);
                        }
                        return vListaDescripcionError;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
