using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    public class RegionalDAL : GeneralDAL
    {
        public RegionalDAL()
        {
        }
       
        public List<Regional> ConsultarRegionals(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Regional_sConsultar"))
                {
                    if (pCodigoRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.String, pCodigoRegional);
                    if (pNombreRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreRegional", DbType.String, pNombreRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Regional> vListaRegional = new List<Regional>();
                        while (vDataReaderResults.Read())
                        {
                            Regional vRegional = new Regional();
                            vRegional.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vRegional.IdRegional;
                            vRegional.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vRegional.CodigoRegional;
                            vRegional.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegional.NombreRegional;
                            vRegional.CodigoNombreRegional = vDataReaderResults["CodigoNombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNombreRegional"].ToString()) : vRegional.CodigoNombreRegional;
                            vRegional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegional.UsuarioCrea;
                            vRegional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegional.FechaCrea;
                            vRegional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegional.UsuarioModifica;
                            vRegional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegional.FechaModifica;

                            vListaRegional.Add(vRegional);
                        }
                        return vListaRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Regional> ConsultarRegionalsNMF(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_NMF_PPTO_Regional_sConsultar"))
                {
                    if(pCodigoRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.String, pCodigoRegional);
                    if(pNombreRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreRegional", DbType.String, pNombreRegional);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Regional> vListaRegional = new List<Regional>();
                        while(vDataReaderResults.Read())
                        {
                            Regional vRegional = new Regional();
                            vRegional.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vRegional.IdRegional;
                            vRegional.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vRegional.CodigoRegional;
                            vRegional.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegional.NombreRegional;
                            vRegional.CodigoNombreRegional = vDataReaderResults["CodigoNombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNombreRegional"].ToString()) : vRegional.CodigoNombreRegional;
                            vRegional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegional.UsuarioCrea;
                            vRegional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegional.FechaCrea;
                            vRegional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegional.UsuarioModifica;
                            vRegional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegional.FechaModifica;
                            vRegional.CodigoRegionalPCI = vDataReaderResults["CodigoRegionalPCI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegionalPCI"].ToString()) : vRegional.CodigoRegionalPCI;
                            
                            vListaRegional.Add(vRegional);
                        }
                        return vListaRegional;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Gonet\Efrain Diaz mejia
        /// </summary>
        /// <param name="pCodigoRegional">Cadena de texto con el c�digo de la regional</param>
        /// <param name="pNombreRegional">Cadena de texto con el nombre de la regional</param>
        /// <returns></returns>
        public List<Regional> ConsultarRegionalPCIs(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_RegionalPCI_sConsultar"))
                {
                    if (pCodigoRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoRegional", DbType.String, pCodigoRegional);
                    if (pNombreRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreRegional", DbType.String, pNombreRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Regional> vListaRegional = new List<Regional>();
                        while (vDataReaderResults.Read())
                        {
                            Regional vRegional = new Regional();
                            vRegional.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vRegional.IdRegional;
                            vRegional.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vRegional.CodigoRegional;
                            vRegional.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegional.NombreRegional;

                            vRegional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegional.UsuarioCrea;
                            vRegional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegional.FechaCrea;
                            vRegional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegional.UsuarioModifica;
                            vRegional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegional.FechaModifica;

                            vListaRegional.Add(vRegional);
                        }
                        return vListaRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Gonet
        /// Emilio Calapi�a
        /// Obtiene la informaci�n de la regional cuyo identificador �nico de tabla coincida
        /// con la entrada dada
        /// </summary>
        /// <param name="pIdRegional">Entero con el identificador de la regional</param>
        /// <returns>Entidad con la informaci�n de la regional</returns>
        public Regional ConsultarRegional(int? pIdRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Regional_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Regional vRegional = new Regional();
                        while (vDataReaderResults.Read())
                        {
                            vRegional.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vRegional.IdRegional;
                            vRegional.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vRegional.CodigoRegional;
                            vRegional.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegional.NombreRegional;

                            vRegional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegional.UsuarioCrea;
                            vRegional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegional.FechaCrea;
                            vRegional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegional.UsuarioModifica;
                            vRegional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegional.FechaModifica;

                        }
                        return vRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet\Carlos Cardenas
        /// </summary>
        /// <param name="pIdUsuario">Cadena de texto con el id del usuario asociado a la regional</param>
        /// <returns></returns>
        public List<Regional> ConsultarRegionalsUsuario(int pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Contratos_Regional_Usuario"))
                {
                    if (pIdUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Regional> vListaRegional = new List<Regional>();
                        while (vDataReaderResults.Read())
                        {
                            Regional vRegional = new Regional();
                            vRegional.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vRegional.IdRegional;
                            vRegional.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegional.NombreRegional;
                            vListaRegional.Add(vRegional);
                        }
                        return vListaRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
    }
}
