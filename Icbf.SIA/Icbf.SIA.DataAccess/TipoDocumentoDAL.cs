using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    public class TipoDocumentoDAL : GeneralDAL
    {
        public TipoDocumentoDAL()
        {
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoDocumento
        /// </summary>
        /// <param name="pIdTipoDocumento"></param>
        /// <returns></returns>
        public TipoDocumento ConsultarTipoDocumento(int pIdTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_TipoDocumento_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Double, pIdTipoDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoDocumento vTipoDocumento = new TipoDocumento();
                        while (vDataReaderResults.Read())
                        {
                            vTipoDocumento.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumento.IdTipoDocumento;
                            vTipoDocumento.Codigo = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vTipoDocumento.Codigo;
                            vTipoDocumento.Nombre = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vTipoDocumento.Nombre;
                        }
                        return vTipoDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id o nombre para la entidad TipoDocumento
        /// </summary>
        /// <param name="pIdTipoDocumento"></param>
        /// <returns></returns>
        public List<TipoDocumento> ConsultarTiposDocumento(String pCodigoDocumento, String pNombreDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Global_TipoDocumento_sConsultar"))
                {
                    if (pCodigoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoDocumento", DbType.String, pCodigoDocumento);
                    if (pNombreDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pNombreDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocumento> oList = new List<TipoDocumento>();
                        while (vDataReaderResults.Read())
                        {
                            TipoDocumento vTipoDocumento = new TipoDocumento();
                            vTipoDocumento.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumento.IdTipoDocumento;
                            vTipoDocumento.Codigo = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vTipoDocumento.Codigo;
                            vTipoDocumento.Nombre = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vTipoDocumento.Nombre;
                            oList.Add(vTipoDocumento);
                        }
                        return oList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TipoDocumento> ConsultarTiposDocumentoNMF(String pCodigoDocumento, String pNombreDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_NMF_Global_TipoDocumento_sConsultar"))
                {
                    if(pCodigoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoDocumento", DbType.String, pCodigoDocumento);
                    if(pNombreDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pNombreDocumento);
                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocumento> oList = new List<TipoDocumento>();
                        while(vDataReaderResults.Read())
                        {
                            TipoDocumento vTipoDocumento = new TipoDocumento();
                            vTipoDocumento.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumento.IdTipoDocumento;
                            vTipoDocumento.Codigo = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vTipoDocumento.Codigo;
                            vTipoDocumento.Nombre = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vTipoDocumento.Nombre;
                            oList.Add(vTipoDocumento);
                        }
                        return oList;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoDocumento
        /// </summary>
        /// <returns></returns>
        public List<TipoDocumento> ConsultarTodosTipoDocumento()
        {
            try
            {
                List<TipoDocumento> oList = new List<TipoDocumento>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Global_Seg_ConsultarTodosTipoDocumento"))
                {
                    IDataReader oIdr = vDataBase.ExecuteReader(vDbCommand);
                    if (oIdr != null)
                    {
                        while (oIdr.Read())
                            oList.Add(new TipoDocumento() { IdTipoDocumento = oIdr.GetInt32(0), Codigo = oIdr.GetString(1), Nombre = oIdr.GetString(2) });
                        return oList;
                    }
                    else
                        return null;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
