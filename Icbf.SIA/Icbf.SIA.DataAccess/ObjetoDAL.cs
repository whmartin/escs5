using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Objeto
    /// </summary>
    public class ObjetoDAL : GeneralDAL
    {
        public ObjetoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int InsertarObjeto(Objeto pObjeto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_Objeto_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdObjetoCupos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Titulo", DbType.String, pObjeto.Titulo);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionObjeto", DbType.String, pObjeto.DescripcionObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pObjeto.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pObjeto.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pObjeto.IdObjetoCupos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdObjetoCupos").ToString());
                    GenerarLogAuditoria(pObjeto, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int ModificarObjeto(Objeto pObjeto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_Objeto_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObjetoCupos", DbType.Int32, pObjeto.IdObjetoCupos);
                    vDataBase.AddInParameter(vDbCommand, "@Titulo", DbType.String, pObjeto.Titulo);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionObjeto", DbType.String, pObjeto.DescripcionObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pObjeto.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pObjeto.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObjeto, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int EliminarObjeto(Objeto pObjeto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_Objeto_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObjetoCupos", DbType.Int32, pObjeto.IdObjetoCupos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObjeto, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad Objeto
        /// </summary>
        /// <param name="pIdObjetoCupos"></param>
        public Objeto ConsultarObjeto(int pIdObjetoCupos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_Objeto_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdObjetoCupos", DbType.Int32, pIdObjetoCupos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Objeto vObjeto = new Objeto();
                        while (vDataReaderResults.Read())
                        {
                            vObjeto.IdObjetoCupos = vDataReaderResults["IdObjetoCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObjetoCupos"].ToString()) : vObjeto.IdObjetoCupos;
                            vObjeto.Titulo = vDataReaderResults["Titulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Titulo"].ToString()) : vObjeto.Titulo;
                            vObjeto.DescripcionObjeto = vDataReaderResults["DescripcionObjeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionObjeto"].ToString()) : vObjeto.DescripcionObjeto;
                            vObjeto.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vObjeto.Estado;
                            vObjeto.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObjeto.UsuarioCrea;
                            vObjeto.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObjeto.FechaCrea;
                            vObjeto.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObjeto.UsuarioModifica;
                            vObjeto.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObjeto.FechaModifica;
                        }
                        return vObjeto;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Objeto
        /// </summary>
        /// <param name="pTitulo"></param>
        /// <param name="pDescripcionObjeto"></param>
        /// <param name="pEstado"></param>
        public List<Objeto> ConsultarObjetos(String pTitulo, String pDescripcionObjeto, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_Objetos_Consultar"))
                {
                    if(pTitulo != null)
                         vDataBase.AddInParameter(vDbCommand, "@Titulo", DbType.String, pTitulo);
                    if(pDescripcionObjeto != null)
                         vDataBase.AddInParameter(vDbCommand, "@DescripcionObjeto", DbType.String, pDescripcionObjeto);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Objeto> vListaObjeto = new List<Objeto>();
                        while (vDataReaderResults.Read())
                        {
                                Objeto vObjeto = new Objeto();
                            vObjeto.IdObjetoCupos = vDataReaderResults["IdObjetoCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObjetoCupos"].ToString()) : vObjeto.IdObjetoCupos;
                            vObjeto.Titulo = vDataReaderResults["Titulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Titulo"].ToString()) : vObjeto.Titulo;
                            vObjeto.DescripcionObjeto = vDataReaderResults["DescripcionObjeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionObjeto"].ToString()) : vObjeto.DescripcionObjeto;
                            vObjeto.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vObjeto.Estado;
                            vObjeto.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObjeto.UsuarioCrea;
                            vObjeto.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObjeto.FechaCrea;
                            vObjeto.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObjeto.UsuarioModifica;
                            vObjeto.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObjeto.FechaModifica;
                                vListaObjeto.Add(vObjeto);
                        }
                        return vListaObjeto;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
