using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad HistoricoAdicionProyeccion
    /// </summary>
    public class HistoricoAdicionProyeccionDAL : GeneralDAL
    {
        public HistoricoAdicionProyeccionDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pHistoricoAdicionProyeccion"></param>
        public int InsertarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionProyeccion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoAdicionProyeccion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProyeccion", DbType.Int32, pHistoricoAdicionProyeccion.IdAdicionProyeccion);
                    vDataBase.AddInParameter(vDbCommand, "@RazonRechazo", DbType.String, pHistoricoAdicionProyeccion.RazonRechazo);
                    vDataBase.AddInParameter(vDbCommand, "@Rechazado", DbType.Boolean, pHistoricoAdicionProyeccion.Rechazado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistoricoAdicionProyeccion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pHistoricoAdicionProyeccion.IdHistoricoAdicionProyeccion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoAdicionProyeccion").ToString());
                    GenerarLogAuditoria(pHistoricoAdicionProyeccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pHistoricoAdicionProyeccion"></param>
        public int ModificarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionProyeccion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoAdicionProyeccion", DbType.Int32, pHistoricoAdicionProyeccion.IdHistoricoAdicionProyeccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProyeccion", DbType.Int32, pHistoricoAdicionProyeccion.IdAdicionProyeccion);
                    vDataBase.AddInParameter(vDbCommand, "@RazonRechazo", DbType.String, pHistoricoAdicionProyeccion.RazonRechazo);
                    vDataBase.AddInParameter(vDbCommand, "@Rechazado", DbType.Boolean, pHistoricoAdicionProyeccion.Rechazado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pHistoricoAdicionProyeccion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pHistoricoAdicionProyeccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pHistoricoAdicionProyeccion"></param>
        public int EliminarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionProyeccion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoAdicionProyeccion", DbType.Int32, pHistoricoAdicionProyeccion.IdHistoricoAdicionProyeccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pHistoricoAdicionProyeccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pIdHistoricoAdicionProyeccion"></param>
        public HistoricoAdicionProyeccion ConsultarHistoricoAdicionProyeccion(int pIdHistoricoAdicionProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionProyeccion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoAdicionProyeccion", DbType.Int32, pIdHistoricoAdicionProyeccion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        HistoricoAdicionProyeccion vHistoricoAdicionProyeccion = new HistoricoAdicionProyeccion();
                        while (vDataReaderResults.Read())
                        {
                            vHistoricoAdicionProyeccion.IdHistoricoAdicionProyeccion = vDataReaderResults["IdHistoricoAdicionProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoAdicionProyeccion"].ToString()) : vHistoricoAdicionProyeccion.IdHistoricoAdicionProyeccion;
                            vHistoricoAdicionProyeccion.IdAdicionProyeccion = vDataReaderResults["IdAdicionProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProyeccion"].ToString()) : vHistoricoAdicionProyeccion.IdAdicionProyeccion;
                            vHistoricoAdicionProyeccion.RazonRechazo = vDataReaderResults["RazonRechazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonRechazo"].ToString()) : vHistoricoAdicionProyeccion.RazonRechazo;
                            vHistoricoAdicionProyeccion.Rechazado = vDataReaderResults["Rechazado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Rechazado"].ToString()) : vHistoricoAdicionProyeccion.Rechazado;
                            vHistoricoAdicionProyeccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoAdicionProyeccion.UsuarioCrea;
                            vHistoricoAdicionProyeccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoAdicionProyeccion.FechaCrea;
                            vHistoricoAdicionProyeccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoAdicionProyeccion.UsuarioModifica;
                            vHistoricoAdicionProyeccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoAdicionProyeccion.FechaModifica;
                        }
                        return vHistoricoAdicionProyeccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pIdAdicionProyeccion"></param>
        /// <param name="pRazonRechazo"></param>
        /// <param name="pRechazado"></param>
        public List<HistoricoAdicionProyeccion> ConsultarHistoricoAdicionProyeccions(int? pIdAdicionProyeccion, String pRazonRechazo, Boolean? pRechazado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionProyeccions_Consultar"))
                {
                    if(pIdAdicionProyeccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdAdicionProyeccion", DbType.Int32, pIdAdicionProyeccion);
                    if(pRazonRechazo != null)
                         vDataBase.AddInParameter(vDbCommand, "@RazonRechazo", DbType.String, pRazonRechazo);
                    if(pRechazado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Rechazado", DbType.Boolean, pRechazado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoAdicionProyeccion> vListaHistoricoAdicionProyeccion = new List<HistoricoAdicionProyeccion>();
                        while (vDataReaderResults.Read())
                        {
                                HistoricoAdicionProyeccion vHistoricoAdicionProyeccion = new HistoricoAdicionProyeccion();
                            vHistoricoAdicionProyeccion.IdHistoricoAdicionProyeccion = vDataReaderResults["IdHistoricoAdicionProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoAdicionProyeccion"].ToString()) : vHistoricoAdicionProyeccion.IdHistoricoAdicionProyeccion;
                            vHistoricoAdicionProyeccion.IdAdicionProyeccion = vDataReaderResults["IdAdicionProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProyeccion"].ToString()) : vHistoricoAdicionProyeccion.IdAdicionProyeccion;
                            vHistoricoAdicionProyeccion.RazonRechazo = vDataReaderResults["RazonRechazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonRechazo"].ToString()) : vHistoricoAdicionProyeccion.RazonRechazo;
                            vHistoricoAdicionProyeccion.Rechazado = vDataReaderResults["Rechazado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Rechazado"].ToString()) : vHistoricoAdicionProyeccion.Rechazado;
                            vHistoricoAdicionProyeccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoAdicionProyeccion.UsuarioCrea;
                            vHistoricoAdicionProyeccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoAdicionProyeccion.FechaCrea;
                            vHistoricoAdicionProyeccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoAdicionProyeccion.UsuarioModifica;
                            vHistoricoAdicionProyeccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoAdicionProyeccion.FechaModifica;
                                vListaHistoricoAdicionProyeccion.Add(vHistoricoAdicionProyeccion);
                        }
                        return vListaHistoricoAdicionProyeccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
