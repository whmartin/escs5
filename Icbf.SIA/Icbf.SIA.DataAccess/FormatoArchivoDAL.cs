using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    public class FormatoArchivoDAL : GeneralDAL
    {
        public FormatoArchivoDAL()
        {
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad FormatoArchivo
        /// </summary>
        /// <param name="pTablaTemporal"></param>
        /// <param name="pExt"></param>
        /// <returns></returns>
        public List<FormatoArchivo> ConsultarFormatoArchivosTemporalExt(string pTablaTemporal, string pExt)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Estructura_FormatoArchivo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@TablaTemporar", DbType.String, pTablaTemporal);
                    vDataBase.AddInParameter(vDbCommand, "@Ext", DbType.String, pExt);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FormatoArchivo> vListaFormatoArchivo = new List<FormatoArchivo>();
                        while (vDataReaderResults.Read())
                        {
                            FormatoArchivo vFArchivo = new FormatoArchivo();

                            vFArchivo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vFArchivo.IdFormatoArchivo;
                            vFArchivo.IdTipoEstructura = vDataReaderResults["IdTipoEstructura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstructura"].ToString()) : vFArchivo.IdTipoEstructura;
                            vFArchivo.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidad"].ToString()) : vFArchivo.IdModalidad;
                            vFArchivo.TablaTemporal = vDataReaderResults["TablaTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TablaTemporal"].ToString()) : vFArchivo.TablaTemporal;
                            vFArchivo.Separador = vDataReaderResults["Separador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Separador"].ToString()) : vFArchivo.Separador;
                            vFArchivo.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vFArchivo.Extension;


                            vListaFormatoArchivo.Add(vFArchivo);
                        }
                        return vListaFormatoArchivo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
