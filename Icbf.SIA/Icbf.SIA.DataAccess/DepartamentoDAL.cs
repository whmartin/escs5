using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    public class DepartamentoDAL : GeneralDAL
    {
        public DepartamentoDAL()
        {
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad Departamento
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public Departamento ConsultarDepartamento(int pIdDepartamento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Departamento_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Departamento vDepartamento = new Departamento();
                        while (vDataReaderResults.Read())
                        {
                            vDepartamento.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vDepartamento.IdDepartamento;
                            vDepartamento.IdPais = vDataReaderResults["IdPais"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPais"].ToString()) : vDepartamento.IdPais;
                            vDepartamento.CodigoDepartamento = vDataReaderResults["CodigoDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDepartamento"].ToString()) : vDepartamento.CodigoDepartamento;
                            vDepartamento.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDepartamento.NombreDepartamento;

                            vDepartamento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDepartamento.UsuarioCrea;
                            vDepartamento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDepartamento.FechaCrea;
                            vDepartamento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDepartamento.UsuarioModifica;
                            vDepartamento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDepartamento.FechaModifica;
                        }
                        return vDepartamento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Departamento
        /// </summary>
        /// <param name="pIdPais"></param>
        /// <param name="pCodigoDepartamento"></param>
        /// <param name="pNombreDepartamento"></param>
        /// <returns></returns>
        public List<Departamento> ConsultarDepartamentos(int? pIdPais, String pCodigoDepartamento, String pNombreDepartamento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Departamento_sConsultar"))
                {
                    if(pIdPais != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdPais", DbType.Int32, pIdPais);
                    if(pCodigoDepartamento != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoDepartamento", DbType.String, pCodigoDepartamento);
                    if(pNombreDepartamento != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreDepartamento", DbType.String, pNombreDepartamento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Departamento> vListaDepartamento = new List<Departamento>();
                        while (vDataReaderResults.Read())
                        {
                            Departamento vDepartamento = new Departamento();
                            vDepartamento.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vDepartamento.IdDepartamento;
                            vDepartamento.IdPais = vDataReaderResults["IdPais"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPais"].ToString()) : vDepartamento.IdPais;
                            vDepartamento.CodigoDepartamento = vDataReaderResults["CodigoDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDepartamento"].ToString()) : vDepartamento.CodigoDepartamento;
                            vDepartamento.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDepartamento.NombreDepartamento;

                            vDepartamento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDepartamento.UsuarioCrea;
                            vDepartamento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDepartamento.FechaCrea;
                            vDepartamento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDepartamento.UsuarioModifica;
                            vDepartamento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDepartamento.FechaModifica;

                            vListaDepartamento.Add(vDepartamento);
                        }
                        return vListaDepartamento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
