using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Xml.Linq;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad CupoAreas
    /// </summary>
    public class CupoAreasDAL : GeneralDAL
    {
        public CupoAreasDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int InsertarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CupoAreas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCupoArea", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pCupoAreas.IdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoInterno", DbType.String, pCupoAreas.ConsecutivoInterno);
                    vDataBase.AddInParameter(vDbCommand, "@IdObjeto", DbType.String, pCupoAreas.IdObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependenciaKactus", DbType.String, pCupoAreas.IdDependenciaKactus);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedor", DbType.Int32, pCupoAreas.IdProveedor);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicialProyectado", DbType.DateTime, pCupoAreas.FechaInicialProyectado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalProyectado", DbType.DateTime, pCupoAreas.FechaFinalProyectado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIngresoICBF", DbType.DateTime, pCupoAreas.FechaIngresoICBF);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValores", DbType.Int32, pCupoAreas.IdCategoriaValores);
                    vDataBase.AddInParameter(vDbCommand, "@HonorarioBase", DbType.Decimal, pCupoAreas.HonorarioBase);
                    vDataBase.AddInParameter(vDbCommand, "@IvaHonorario", DbType.Decimal, pCupoAreas.IvaHonorario);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeIVA", DbType.Decimal, pCupoAreas.PorcentajeIVA);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoMeses", DbType.Int32, pCupoAreas.TiempoProyectadoMeses);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoDias", DbType.Int32, pCupoAreas.TiempoProyectadoDias);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaEmpleado", DbType.Int32, pCupoAreas.IdCategoriaEmpleado);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoAnios", DbType.Int32, pCupoAreas.TiempoProyectadoAnios);
                    vDataBase.AddInParameter(vDbCommand, "@TotalHonorarioTiempoProyectado", DbType.Decimal, pCupoAreas.TotalHonorarioTiempoProyectado);

                    //vDataBase.AddInParameter(vDbCommand, "@Aprobado", DbType.Boolean, pCupoAreas.Aprobado);
                    //vDataBase.AddInParameter(vDbCommand, "@UsuarioAprobo", DbType.String, pCupoAreas.UsuarioAprobo);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaAprobacion", DbType.DateTime, pCupoAreas.FechaAprobacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCupoAreas.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCupoAreas.IdCupoArea = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCupoArea").ToString());
                    GenerarLogAuditoria(pCupoAreas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoCupoAreas(List<CupoAreas> pCupoAreasEstado, string pUsuarioModifica, int pIdProyeccionPresupuestos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CupoAreass_Aprobar"))
                {
                    int vResultado;

                    var xml = new XElement("DataCuposAreas",
                                                pCupoAreasEstado.Select(i => new XElement("CuposAreas",
                                                new XAttribute("IdCuposAreas", i.IdCupoArea),
                                                 new XAttribute("CodEstado", i.CodEstadoProceso))));

                    vDataBase.AddInParameter(vDbCommand, "@IdCupoAreaXml", DbType.String, xml.ToString());
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);


                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);


                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int ModificarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CupoAreas_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pCupoAreas.IdCupoArea);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pCupoAreas.IdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoInterno", DbType.String, pCupoAreas.ConsecutivoInterno);
                    vDataBase.AddInParameter(vDbCommand, "@IdObjeto", DbType.String, pCupoAreas.IdObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependenciaKactus", DbType.String, pCupoAreas.IdDependenciaKactus);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedor", DbType.Int32, pCupoAreas.IdProveedor);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicialProyectado", DbType.DateTime, pCupoAreas.FechaInicialProyectado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalProyectado", DbType.DateTime, pCupoAreas.FechaFinalProyectado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIngresoICBF", DbType.DateTime, pCupoAreas.FechaIngresoICBF);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValores", DbType.Int32, pCupoAreas.IdCategoriaValores);
                    vDataBase.AddInParameter(vDbCommand, "@HonorarioBase", DbType.Decimal, pCupoAreas.HonorarioBase);
                    vDataBase.AddInParameter(vDbCommand, "@IvaHonorario", DbType.Decimal, pCupoAreas.IvaHonorario);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeIVA", DbType.Decimal, pCupoAreas.PorcentajeIVA);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaEmpleado", DbType.Int32, pCupoAreas.IdCategoriaEmpleado);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoMeses", DbType.Int32, pCupoAreas.TiempoProyectadoMeses);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoDias", DbType.Int32, pCupoAreas.TiempoProyectadoDias);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoAnios", DbType.Int32, pCupoAreas.TiempoProyectadoAnios);
                    vDataBase.AddInParameter(vDbCommand, "@TotalHonorarioTiempoProyectado", DbType.Decimal, pCupoAreas.TotalHonorarioTiempoProyectado);
                    //vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pCupoAreas.IdProyeccionPresupuestos);
                    //vDataBase.AddInParameter(vDbCommand, "@ConsecutivoInterno", DbType.String, pCupoAreas.ConsecutivoInterno);
                    //vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pCupoAreas.IdObjeto);
                    //vDataBase.AddInParameter(vDbCommand, "@IdDependenciaKactus", DbType.String, pCupoAreas.IdDependenciaKactus);
                    //vDataBase.AddInParameter(vDbCommand, "@IdProveedor", DbType.Int32, pCupoAreas.IdProveedor);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaIngresoICBF", DbType.DateTime, pCupoAreas.FechaIngresoICBF);
                    //vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValores", DbType.Int32, pCupoAreas.IdCategoriaValores);
                    //vDataBase.AddInParameter(vDbCommand, "@HonorarioBase", DbType.Decimal, pCupoAreas.HonorarioBase);
                    //vDataBase.AddInParameter(vDbCommand, "@IvaHonorario", DbType.Decimal, pCupoAreas.IvaHonorario);
                    //vDataBase.AddInParameter(vDbCommand, "@PorcentajeIVA", DbType.Decimal, pCupoAreas.PorcentajeIVA);
                    //vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoMeses", DbType.Int32, pCupoAreas.TiempoProyectadoMeses);
                    //vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoDias", DbType.Int32, pCupoAreas.TiempoProyectadoDias);
                    ///
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCupoAreas.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCupoAreas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarAdicionYProrrogas(CupoAreas pCupoAreas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionYProrrogas_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pCupoAreas.IdCupoArea);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProrroga ", DbType.Int32, pCupoAreas.IdAdicionProrroga);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pCupoAreas.IdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoInterno", DbType.String, pCupoAreas.ConsecutivoInterno);
                    vDataBase.AddInParameter(vDbCommand, "@IdObjeto", DbType.String, pCupoAreas.IdObjeto);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependenciaKactus", DbType.String, pCupoAreas.IdDependenciaKactus);
                    vDataBase.AddInParameter(vDbCommand, "@IdProveedor", DbType.Int32, pCupoAreas.IdProveedor);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicialProyectado", DbType.DateTime, pCupoAreas.FechaInicialProyectado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinalProyectado", DbType.DateTime, pCupoAreas.FechaFinalProyectado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIngresoICBF", DbType.DateTime, pCupoAreas.FechaIngresoICBF);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValores", DbType.Int32, pCupoAreas.IdCategoriaValores);
                    vDataBase.AddInParameter(vDbCommand, "@HonorarioBase", DbType.Decimal, pCupoAreas.HonorarioBase);
                    vDataBase.AddInParameter(vDbCommand, "@IvaHonorario", DbType.Decimal, pCupoAreas.IvaHonorario);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeIVA", DbType.Decimal, pCupoAreas.PorcentajeIVA);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaEmpleado", DbType.Int32, pCupoAreas.IdCategoriaEmpleado);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoMeses", DbType.Int32, pCupoAreas.TiempoProyectadoMeses);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoDias", DbType.Int32, pCupoAreas.TiempoProyectadoDias);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoAnios", DbType.Int32, pCupoAreas.TiempoProyectadoAnios);
                    vDataBase.AddInParameter(vDbCommand, "@TotalHonorarioTiempoProyectado", DbType.Decimal, pCupoAreas.TotalHonorarioTiempoProyectado);
                    //vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pCupoAreas.IdProyeccionPresupuestos);
                    //vDataBase.AddInParameter(vDbCommand, "@ConsecutivoInterno", DbType.String, pCupoAreas.ConsecutivoInterno);
                    //vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pCupoAreas.IdObjeto);
                    //vDataBase.AddInParameter(vDbCommand, "@IdDependenciaKactus", DbType.String, pCupoAreas.IdDependenciaKactus);
                    //vDataBase.AddInParameter(vDbCommand, "@IdProveedor", DbType.Int32, pCupoAreas.IdProveedor);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaIngresoICBF", DbType.DateTime, pCupoAreas.FechaIngresoICBF);
                    //vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValores", DbType.Int32, pCupoAreas.IdCategoriaValores);
                    //vDataBase.AddInParameter(vDbCommand, "@HonorarioBase", DbType.Decimal, pCupoAreas.HonorarioBase);
                    //vDataBase.AddInParameter(vDbCommand, "@IvaHonorario", DbType.Decimal, pCupoAreas.IvaHonorario);
                    //vDataBase.AddInParameter(vDbCommand, "@PorcentajeIVA", DbType.Decimal, pCupoAreas.PorcentajeIVA);
                    //vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoMeses", DbType.Int32, pCupoAreas.TiempoProyectadoMeses);
                    //vDataBase.AddInParameter(vDbCommand, "@TiempoProyectadoDias", DbType.Int32, pCupoAreas.TiempoProyectadoDias);
                    ///
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCupoAreas.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCupoAreas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int EliminarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CupoAreas_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pCupoAreas.IdCupoArea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCupoAreas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad CupoAreas
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        public CupoAreas ConsultarCupoAreas(int pIdCupoArea)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CupoAreas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pIdCupoArea);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CupoAreas vCupoAreas = new CupoAreas();
                        while (vDataReaderResults.Read())
                        {
                            vCupoAreas.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vCupoAreas.IdCupoArea;
                            vCupoAreas.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vCupoAreas.IdProyeccionPresupuestos;
                            vCupoAreas.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vCupoAreas.ConsecutivoInterno;
                            vCupoAreas.IdObjeto = vDataReaderResults["IdObjeto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObjeto"].ToString()) : vCupoAreas.IdObjeto;
                            vCupoAreas.IdDependenciaKactus = vDataReaderResults["IdDependenciaKactus"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDependenciaKactus"].ToString()) : vCupoAreas.IdDependenciaKactus;
                            vCupoAreas.IdProveedor = vDataReaderResults["IdProveedor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedor"].ToString()) : vCupoAreas.IdProveedor;

                            vCupoAreas.FechaInicialProyectado = vDataReaderResults["FechaInicialProyectado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicialProyectado"].ToString()) : vCupoAreas.FechaInicialProyectado;
                            vCupoAreas.FechaFinalProyectado = vDataReaderResults["FechaFinalProyectado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalProyectado"].ToString()) : vCupoAreas.FechaFinalProyectado;

                            vCupoAreas.FechaIngresoICBF = vDataReaderResults["FechaIngresoICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIngresoICBF"].ToString()) : vCupoAreas.FechaIngresoICBF;
                            vCupoAreas.IdCategoriaValores = vDataReaderResults["IdCategoriaValores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValores"].ToString()) : vCupoAreas.IdCategoriaValores;
                            vCupoAreas.HonorarioBase = vDataReaderResults["HonorarioBase"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["HonorarioBase"].ToString()) : vCupoAreas.HonorarioBase;
                            vCupoAreas.IvaHonorario = vDataReaderResults["IvaHonorario"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IvaHonorario"].ToString()) : vCupoAreas.IvaHonorario;
                            vCupoAreas.PorcentajeIVA = vDataReaderResults["PorcentajeIVA"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeIVA"].ToString()) : vCupoAreas.PorcentajeIVA;
                            vCupoAreas.TiempoProyectadoMeses = vDataReaderResults["TiempoProyectadoMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoMeses"].ToString()) : vCupoAreas.TiempoProyectadoMeses;
                            vCupoAreas.TiempoProyectadoDias = vDataReaderResults["TiempoProyectadoDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoDias"].ToString()) : vCupoAreas.TiempoProyectadoDias;
                            vCupoAreas.IdEstadoProceso = vDataReaderResults["IdEstadoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProceso"].ToString()) : vCupoAreas.IdEstadoProceso;
                            vCupoAreas.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vCupoAreas.UsuarioAprobo;
                            vCupoAreas.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vCupoAreas.FechaAprobacion;
                            vCupoAreas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCupoAreas.UsuarioCrea;
                            vCupoAreas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCupoAreas.FechaCrea;
                            vCupoAreas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCupoAreas.UsuarioModifica;
                            vCupoAreas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCupoAreas.FechaModifica;

                            vCupoAreas.TotalHonorarioTiempoProyectado = vDataReaderResults["TotalHonorarioTiempoProyectado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalHonorarioTiempoProyectado"].ToString()) : vCupoAreas.TotalHonorarioTiempoProyectado;
                            vCupoAreas.TiempoProyectadoAnios = vDataReaderResults["TiempoProyectadoAnios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoAnios"].ToString()) : vCupoAreas.TiempoProyectadoAnios;
                        }
                        return vCupoAreas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CupoAreas
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        public List<AdicionesYProrrogas> ConsultarAdicionYProrrogas(CupoAreas pIdCupoArea)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionesYProrrogas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pIdCupoArea.IdCupoArea);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AdicionesYProrrogas> vAdiciones = new List<AdicionesYProrrogas>();
                        while (vDataReaderResults.Read())
                        {
                            AdicionesYProrrogas vCupoAreas = new AdicionesYProrrogas();
                            vCupoAreas.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vCupoAreas.IdCupoArea;
                            vCupoAreas.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vCupoAreas.IdProyeccionPresupuestos;
                            vCupoAreas.IdAdicionProrrogaCandidato = vDataReaderResults["IdAdicionProrrogaCandidato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProrrogaCandidato"].ToString()) : vCupoAreas.IdAdicionProrrogaCandidato;

                            vCupoAreas.Aprobado = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Aprobado"].ToString()) : vCupoAreas.Aprobado;
                            vCupoAreas.Rechazado = vDataReaderResults["Rechazado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Rechazado"].ToString()) : vCupoAreas.Rechazado;
                            vCupoAreas.RazonRechazo = vDataReaderResults["RazonRechazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonRechazo"].ToString()) : vCupoAreas.RazonRechazo;

                            vCupoAreas.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vCupoAreas.ConsecutivoInterno;
                            vCupoAreas.IdObjeto = vDataReaderResults["IdObjeto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObjeto"].ToString()) : vCupoAreas.IdObjeto;
                            vCupoAreas.IdDependenciaKactus = vDataReaderResults["IdDependenciaKactus"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDependenciaKactus"].ToString()) : vCupoAreas.IdDependenciaKactus;
                            vCupoAreas.IdProveedor = vDataReaderResults["IdProveedor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedor"].ToString()) : vCupoAreas.IdProveedor;
                            vCupoAreas.FechaInicialProyectado = vDataReaderResults["FechaInicialProyectado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicialProyectado"].ToString()) : vCupoAreas.FechaInicialProyectado;
                            vCupoAreas.FechaFinalProyectado = vDataReaderResults["FechaFinalProyectado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalProyectado"].ToString()) : vCupoAreas.FechaFinalProyectado;
                            vCupoAreas.FechaIngresoICBF = vDataReaderResults["FechaIngresoICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIngresoICBF"].ToString()) : vCupoAreas.FechaIngresoICBF;
                            vCupoAreas.IdCategoriaValores = vDataReaderResults["IdCategoriaValores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValores"].ToString()) : vCupoAreas.IdCategoriaValores;
                            vCupoAreas.HonorarioBase = vDataReaderResults["HonorarioBase"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["HonorarioBase"].ToString()) : vCupoAreas.HonorarioBase;
                            vCupoAreas.IvaHonorario = vDataReaderResults["IvaHonorario"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IvaHonorario"].ToString()) : vCupoAreas.IvaHonorario;
                            vCupoAreas.PorcentajeIVA = vDataReaderResults["PorcentajeIVA"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeIVA"].ToString()) : vCupoAreas.PorcentajeIVA;
                            vCupoAreas.TiempoProyectadoMeses = vDataReaderResults["TiempoProyectadoMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoMeses"].ToString()) : vCupoAreas.TiempoProyectadoMeses;
                            vCupoAreas.TiempoProyectadoDias = vDataReaderResults["TiempoProyectadoDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoDias"].ToString()) : vCupoAreas.TiempoProyectadoDias;
                            vCupoAreas.IdEstadoProceso = vDataReaderResults["IdEstadoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProceso"].ToString()) : vCupoAreas.IdEstadoProceso;
                            vCupoAreas.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vCupoAreas.UsuarioAprobo;
                            vCupoAreas.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vCupoAreas.FechaAprobacion;
                            vCupoAreas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCupoAreas.UsuarioCrea;
                            vCupoAreas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCupoAreas.FechaCrea;
                            vCupoAreas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCupoAreas.UsuarioModifica;
                            vCupoAreas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCupoAreas.FechaModifica;
                            vCupoAreas.TotalHonorarioTiempoProyectado = vDataReaderResults["TotalHonorarioTiempoProyectado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalHonorarioTiempoProyectado"].ToString()) : vCupoAreas.TotalHonorarioTiempoProyectado;
                            vCupoAreas.TiempoProyectadoAnios = vDataReaderResults["TiempoProyectadoAnios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoAnios"].ToString()) : vCupoAreas.TiempoProyectadoAnios;
                            vCupoAreas.FechaIncialProyectadaInicial = vDataReaderResults["FechaIncialProyectadaInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncialProyectadaInicial"].ToString()) : vCupoAreas.FechaIncialProyectadaInicial;
                            vCupoAreas.FechaFinalProyectadaInicial = vDataReaderResults["FechaFinalProyectadaInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalProyectadaInicial"].ToString()) : vCupoAreas.FechaFinalProyectadaInicial;
                            vCupoAreas.HonorariosInciales = vDataReaderResults["HonorariosInciales"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["HonorariosInciales"].ToString()) : vCupoAreas.HonorariosInciales;
                            vCupoAreas.Prorroga = vDataReaderResults["Prorroga"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Prorroga"].ToString()) : vCupoAreas.Prorroga;
                            vCupoAreas.Adicion = vDataReaderResults["Adicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Adicion"].ToString()) : vCupoAreas.Adicion;
                            vCupoAreas.ProrrogaIndividual = vDataReaderResults["ProrrogaIndividual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ProrrogaIndividual"].ToString()) : vCupoAreas.ProrrogaIndividual;
                            vCupoAreas.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vCupoAreas.ValorAdicionado;
                            vAdiciones.Add(vCupoAreas);
                        }
                        return vAdiciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CupoAreas
        /// </summary>
        /// <param name="pConsecutivoInterno"></param>
        /// <param name="pIdProveedor"></param>
        /// <param name="pFechaIngresoICBF"></param>
        /// <param name="pIdCategoriaValores"></param>
        /// <param name="pHonorarioBase"></param>
        /// <param name="pIvaHonorario"></param>
        /// <param name="pPorcentajeIVA"></param>
        /// <param name="pTiempoProyectadoMeses"></param>
        /// <param name="pTiempoProyectadoDias"></param>
        /// <param name="pIdEstadoProceso"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        public List<CupoAreas> ConsultarCupoAreass(String pConsecutivoInterno, int? pIdProveedor, DateTime? pFechaIngresoICBF
            , int? pIdCategoriaValores, int? pIdProyeccionPresupuestos, String pIdEstadoProceso, string pNombreProveedor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CupoAreass_Consultar"))
                {
                    if (pConsecutivoInterno != null)
                        vDataBase.AddInParameter(vDbCommand, "@ConsecutivoInterno", DbType.String, pConsecutivoInterno);
                    if (pIdProveedor != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProveedor", DbType.Int32, pIdProveedor);

                    if (pNombreProveedor != null)
                        vDataBase.AddInParameter(vDbCommand, "@pNombreProveedor", DbType.String, pNombreProveedor);

                    if (pFechaIngresoICBF != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaIngresoICBF", DbType.DateTime, pFechaIngresoICBF);
                    if (pIdCategoriaValores != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValores", DbType.Int32, pIdCategoriaValores);
                    if (pIdProyeccionPresupuestos != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    if (pIdEstadoProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstadoProceso", DbType.String, pIdEstadoProceso);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CupoAreas> vListaCupoAreas = new List<CupoAreas>();
                        while (vDataReaderResults.Read())
                        {
                            CupoAreas vCupoAreas = new CupoAreas();
                            vCupoAreas.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vCupoAreas.IdCupoArea;
                            vCupoAreas.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vCupoAreas.IdProyeccionPresupuestos;
                            vCupoAreas.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vCupoAreas.ConsecutivoInterno;
                            vCupoAreas.IdObjeto = vDataReaderResults["IdObjeto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObjeto"].ToString()) : vCupoAreas.IdObjeto;
                            vCupoAreas.IdDependenciaKactus = vDataReaderResults["IdDependenciaKactus"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDependenciaKactus"].ToString()) : vCupoAreas.IdDependenciaKactus;
                            vCupoAreas.IdProveedor = vDataReaderResults["IdProveedor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedor"].ToString()) : vCupoAreas.IdProveedor;
                            vCupoAreas.FechaIngresoICBF = vDataReaderResults["FechaIngresoICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIngresoICBF"].ToString()) : vCupoAreas.FechaIngresoICBF;
                            vCupoAreas.IdCategoriaValores = vDataReaderResults["IdCategoriaValores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValores"].ToString()) : vCupoAreas.IdCategoriaValores;
                            vCupoAreas.HonorarioBase = vDataReaderResults["HonorarioBase"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["HonorarioBase"].ToString()) : vCupoAreas.HonorarioBase;
                            vCupoAreas.IvaHonorario = vDataReaderResults["IvaHonorario"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IvaHonorario"].ToString()) : vCupoAreas.IvaHonorario;
                            vCupoAreas.PorcentajeIVA = vDataReaderResults["PorcentajeIVA"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeIVA"].ToString()) : vCupoAreas.PorcentajeIVA;
                            vCupoAreas.TiempoProyectadoMeses = vDataReaderResults["TiempoProyectadoMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoMeses"].ToString()) : vCupoAreas.TiempoProyectadoMeses;
                            vCupoAreas.TiempoProyectadoDias = vDataReaderResults["TiempoProyectadoDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoDias"].ToString()) : vCupoAreas.TiempoProyectadoDias;
                            vCupoAreas.IdEstadoProceso = vDataReaderResults["IdEstadoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProceso"].ToString()) : vCupoAreas.IdEstadoProceso;
                            vCupoAreas.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vCupoAreas.UsuarioAprobo;
                            vCupoAreas.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vCupoAreas.FechaAprobacion;
                            vCupoAreas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCupoAreas.UsuarioCrea;
                            vCupoAreas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCupoAreas.FechaCrea;
                            vCupoAreas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCupoAreas.UsuarioModifica;
                            vCupoAreas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCupoAreas.FechaModifica;
                            vCupoAreas.TotalHonorarioTiempoProyectado = vDataReaderResults["TotalHonorarioTiempoProyectado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalHonorarioTiempoProyectado"].ToString()) : vCupoAreas.TotalHonorarioTiempoProyectado;
                            vCupoAreas.TiempoProyectadoAnios = vDataReaderResults["TiempoProyectadoAnios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoAnios"].ToString()) : vCupoAreas.TiempoProyectadoAnios;
                            vCupoAreas.NombreDependenciaSolicitante = vDataReaderResults["NombreDependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDependenciaSolicitante"].ToString()) : vCupoAreas.NombreDependenciaSolicitante;
                            vCupoAreas.CategoriaEmpleados = vDataReaderResults["CategoriaEmpleados"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaEmpleados"].ToString()) : vCupoAreas.CategoriaEmpleados;
                            vCupoAreas.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vCupoAreas.NombreArea;
                            vCupoAreas.NombreTercero = vDataReaderResults["NombreTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTercero"].ToString()) : vCupoAreas.NombreTercero;
                            vCupoAreas.Nivel = vDataReaderResults["Nivel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Nivel"].ToString()) : vCupoAreas.Nivel;

                            vListaCupoAreas.Add(vCupoAreas);
                        }
                        return vListaCupoAreas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CupoAreas ConsultarCupoAreasCategoria(int pIdProyeccionPresupuestos, int pIdCategoriaValores)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CupoAreasCategoria_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@pIdCategoriaValores", DbType.Int32, pIdCategoriaValores);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CupoAreas vCupoAreas = new CupoAreas();
                        while (vDataReaderResults.Read())
                        {
                            vCupoAreas.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vCupoAreas.IdCupoArea;
                            vCupoAreas.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vCupoAreas.IdProyeccionPresupuestos;
                            vCupoAreas.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vCupoAreas.ConsecutivoInterno;
                            vCupoAreas.IdObjeto = vDataReaderResults["IdObjeto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObjeto"].ToString()) : vCupoAreas.IdObjeto;
                            vCupoAreas.IdDependenciaKactus = vDataReaderResults["IdDependenciaKactus"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDependenciaKactus"].ToString()) : vCupoAreas.IdDependenciaKactus;
                            vCupoAreas.IdProveedor = vDataReaderResults["IdProveedor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProveedor"].ToString()) : vCupoAreas.IdProveedor;
                            vCupoAreas.FechaIngresoICBF = vDataReaderResults["FechaIngresoICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIngresoICBF"].ToString()) : vCupoAreas.FechaIngresoICBF;
                            vCupoAreas.IdCategoriaValores = vDataReaderResults["IdCategoriaValores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValores"].ToString()) : vCupoAreas.IdCategoriaValores;
                            vCupoAreas.HonorarioBase = vDataReaderResults["HonorarioBase"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["HonorarioBase"].ToString()) : vCupoAreas.HonorarioBase;
                            vCupoAreas.IvaHonorario = vDataReaderResults["IvaHonorario"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IvaHonorario"].ToString()) : vCupoAreas.IvaHonorario;
                            vCupoAreas.PorcentajeIVA = vDataReaderResults["PorcentajeIVA"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeIVA"].ToString()) : vCupoAreas.PorcentajeIVA;
                            vCupoAreas.TiempoProyectadoMeses = vDataReaderResults["TiempoProyectadoMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoMeses"].ToString()) : vCupoAreas.TiempoProyectadoMeses;
                            vCupoAreas.TiempoProyectadoDias = vDataReaderResults["TiempoProyectadoDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoDias"].ToString()) : vCupoAreas.TiempoProyectadoDias;
                            vCupoAreas.IdEstadoProceso = vDataReaderResults["IdEstadoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProceso"].ToString()) : vCupoAreas.IdEstadoProceso;
                            vCupoAreas.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vCupoAreas.UsuarioAprobo;
                            vCupoAreas.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vCupoAreas.FechaAprobacion;
                            vCupoAreas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCupoAreas.UsuarioCrea;
                            vCupoAreas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCupoAreas.FechaCrea;
                            vCupoAreas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCupoAreas.UsuarioModifica;
                            vCupoAreas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCupoAreas.FechaModifica;
                            vCupoAreas.TotalHonorarioTiempoProyectado = vDataReaderResults["TotalHonorarioTiempoProyectado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalHonorarioTiempoProyectado"].ToString()) : vCupoAreas.TotalHonorarioTiempoProyectado;
                            vCupoAreas.TiempoProyectadoAnios = vDataReaderResults["TiempoProyectadoAnios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoProyectadoAnios"].ToString()) : vCupoAreas.TiempoProyectadoAnios;
                            vCupoAreas.NombreDependenciaSolicitante = vDataReaderResults["NombreDependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDependenciaSolicitante"].ToString()) : vCupoAreas.NombreDependenciaSolicitante;
                            vCupoAreas.CategoriaEmpleados = vDataReaderResults["CategoriaEmpleados"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaEmpleados"].ToString()) : vCupoAreas.CategoriaEmpleados;
                            vCupoAreas.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vCupoAreas.NombreArea;
                            vCupoAreas.NombreTercero = vDataReaderResults["NombreTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTercero"].ToString()) : vCupoAreas.NombreTercero;
                            vCupoAreas.Nivel = vDataReaderResults["Nivel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Nivel"].ToString()) : vCupoAreas.Nivel;
                        }
                        return vCupoAreas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
