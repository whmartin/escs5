
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    public class MunicipioDAL : GeneralDAL
    {
        public MunicipioDAL()
        {
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad Municipio
        /// </summary>
        /// <param name="pIdMunicipio"></param>
        /// <returns></returns>
        public Municipio ConsultarMunicipio(int pIdMunicipio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Municipio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pIdMunicipio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Municipio vMunicipio = new Municipio();
                        while (vDataReaderResults.Read())
                        {
                            vMunicipio.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vMunicipio.IdMunicipio;
                            vMunicipio.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vMunicipio.IdDepartamento;
                            vMunicipio.CodigoMunicipio = vDataReaderResults["CodigoMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoMunicipio"].ToString()) : vMunicipio.CodigoMunicipio;
                            vMunicipio.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vMunicipio.NombreMunicipio;
                            vMunicipio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMunicipio.UsuarioCrea;
                            vMunicipio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMunicipio.FechaCrea;
                            vMunicipio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMunicipio.UsuarioModifica;
                            vMunicipio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMunicipio.FechaModifica;

                        }
                        return vMunicipio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Municipio
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pCodigoMunicipio"></param>
        /// <param name="pNombreMunicipio"></param>
        /// <returns></returns>
        public List<Municipio> ConsultarMunicipios(int? pIdDepartamento, String pCodigoMunicipio, String pNombreMunicipio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Municipio_sConsultar"))
                {
                    if (pIdDepartamento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    if (pCodigoMunicipio != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoMunicipio", DbType.String, pCodigoMunicipio);
                    if (pNombreMunicipio != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreMunicipio", DbType.String, pNombreMunicipio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Municipio> vListaMunicipio = new List<Municipio>();
                        while (vDataReaderResults.Read())
                        {
                            Municipio vMunicipio = new Municipio();
                            vMunicipio.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vMunicipio.IdMunicipio;
                            vMunicipio.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vMunicipio.IdDepartamento;
                            vMunicipio.CodigoMunicipio = vDataReaderResults["CodigoMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoMunicipio"].ToString()) : vMunicipio.CodigoMunicipio;
                            vMunicipio.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString().ToUpper()) : vMunicipio.NombreMunicipio;
                            vMunicipio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMunicipio.UsuarioCrea;
                            vMunicipio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMunicipio.FechaCrea;
                            vMunicipio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMunicipio.UsuarioModifica;
                            vMunicipio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMunicipio.FechaModifica;

                            vListaMunicipio.Add(vMunicipio);
                        }
                        return vListaMunicipio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Municipio
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pCodigoMunicipio"></param>
        /// <param name="pNombreMunicipio"></param>
        /// <returns></returns>
        public List<Municipio> ConsultarMunicipiosPorIdsDep(string pIdsDepartamento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Municipio_sConsultarPorIdsDep"))
                {
                    if (pIdsDepartamento != string.Empty)
                        vDataBase.AddInParameter(vDbCommand, "@IdsDepartamento", DbType.String, pIdsDepartamento);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Municipio> vListaMunicipio = new List<Municipio>();
                        while (vDataReaderResults.Read())
                        {
                            Municipio vMunicipio = new Municipio();
                            vMunicipio.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vMunicipio.IdMunicipio;
                            vMunicipio.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vMunicipio.IdDepartamento;
                            vMunicipio.CodigoMunicipio = vDataReaderResults["CodigoMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoMunicipio"].ToString()) : vMunicipio.CodigoMunicipio;
                            vMunicipio.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vMunicipio.NombreMunicipio;
                            vMunicipio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMunicipio.UsuarioCrea;
                            vMunicipio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMunicipio.FechaCrea;
                            vMunicipio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMunicipio.UsuarioModifica;
                            vMunicipio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMunicipio.FechaModifica;

                            vListaMunicipio.Add(vMunicipio);
                        }
                        return vListaMunicipio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
