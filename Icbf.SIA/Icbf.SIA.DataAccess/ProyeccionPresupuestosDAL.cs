using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Xml.Linq;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad ProyeccionPresupuestos
    /// </summary>
    public class ProyeccionPresupuestosDAL : GeneralDAL
    {
        public ProyeccionPresupuestosDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pProyeccionPresupuestos"></param>
        public int InsertarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionPresupuestos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pProyeccionPresupuestos.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pProyeccionPresupuestos.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRecurso", DbType.Int32, pProyeccionPresupuestos.IdRecurso);
                    vDataBase.AddInParameter(vDbCommand, "@IdRubro", DbType.Int32, pProyeccionPresupuestos.IdRubro);
                    vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pProyeccionPresupuestos.IdArea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCupo", DbType.Decimal, pProyeccionPresupuestos.ValorCupo);
                    vDataBase.AddInParameter(vDbCommand, "@TotalCupos", DbType.Int32, pProyeccionPresupuestos.TotalCupos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pProyeccionPresupuestos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProyeccionPresupuestos.IdProyeccionPresupuestos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProyeccionPresupuestos").ToString());
                    GenerarLogAuditoria(pProyeccionPresupuestos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pProyeccionPresupuestos"></param>
        public int ModificarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionPresupuestos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pProyeccionPresupuestos.IdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pProyeccionPresupuestos.IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pProyeccionPresupuestos.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRecurso", DbType.Int32, pProyeccionPresupuestos.IdRecurso);
                    vDataBase.AddInParameter(vDbCommand, "@IdRubro", DbType.Int32, pProyeccionPresupuestos.IdRubro);
                    vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pProyeccionPresupuestos.IdArea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorCupo", DbType.Decimal, pProyeccionPresupuestos.ValorCupo);
                    vDataBase.AddInParameter(vDbCommand, "@TotalCupos", DbType.Int32, pProyeccionPresupuestos.TotalCupos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProyeccionPresupuestos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProyeccionPresupuestos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pProyeccionPresupuestos"></param>
        public int EliminarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionPresupuestos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pProyeccionPresupuestos.IdProyeccionPresupuestos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProyeccionPresupuestos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdProyeccionPresupuestos"></param>
        public ProyeccionPresupuestos ConsultarProyeccionPresupuestos(int pIdProyeccionPresupuestos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionPresupuestos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
                        while (vDataReaderResults.Read())
                        {
                            vProyeccionPresupuestos.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vProyeccionPresupuestos.IdProyeccionPresupuestos;
                            vProyeccionPresupuestos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProyeccionPresupuestos.IdRegional;
                            vProyeccionPresupuestos.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vProyeccionPresupuestos.NombreRegional;
                            vProyeccionPresupuestos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProyeccionPresupuestos.IdVigencia;
                            vProyeccionPresupuestos.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vProyeccionPresupuestos.AcnoVigencia;
                            vProyeccionPresupuestos.IdRecurso = vDataReaderResults["IdRecurso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRecurso"].ToString()) : vProyeccionPresupuestos.IdRecurso;
                            vProyeccionPresupuestos.IdRubro = vDataReaderResults["IdRubro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRubro"].ToString()) : vProyeccionPresupuestos.IdRubro;
                            vProyeccionPresupuestos.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vProyeccionPresupuestos.DescripcionRubro;
                            vProyeccionPresupuestos.IdEstadoProceso = vDataReaderResults["IdEstadoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProceso"].ToString()) : vProyeccionPresupuestos.IdEstadoProceso;
                            vProyeccionPresupuestos.CodEstado = vDataReaderResults["CodEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodEstado"].ToString()) : vProyeccionPresupuestos.CodEstado;
                            vProyeccionPresupuestos.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vProyeccionPresupuestos.IdArea;
                            vProyeccionPresupuestos.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vProyeccionPresupuestos.NombreArea;
                            vProyeccionPresupuestos.ValorCupo = vDataReaderResults["ValorCupo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCupo"].ToString()) : vProyeccionPresupuestos.ValorCupo;
                            vProyeccionPresupuestos.TotalCupos = vDataReaderResults["TotalCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalCupos"].ToString()) : vProyeccionPresupuestos.TotalCupos;
                            vProyeccionPresupuestos.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vProyeccionPresupuestos.UsuarioAprobo;
                            vProyeccionPresupuestos.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vProyeccionPresupuestos.FechaAprobacion;
                            vProyeccionPresupuestos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProyeccionPresupuestos.UsuarioCrea;
                            vProyeccionPresupuestos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProyeccionPresupuestos.FechaCrea;
                            vProyeccionPresupuestos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProyeccionPresupuestos.UsuarioModifica;
                            vProyeccionPresupuestos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProyeccionPresupuestos.FechaModifica;
                            vProyeccionPresupuestos.ValorInicial = vDataReaderResults["ValorInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorInicial"].ToString()) : vProyeccionPresupuestos.ValorInicial;
                        }

                        return vProyeccionPresupuestos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<HistoricoAproPresupuestos> ConsultarHistoricoProyeccionRechazados(int pIdProyeccionPresupuestos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoProyeccionPresupuestos_ConsultarRechazados"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoAproPresupuestos> ListHistoricoAproPresupuestos = new List<HistoricoAproPresupuestos>();

                        while (vDataReaderResults.Read())
                        {
                            HistoricoAproPresupuestos vHistoricoAproPresupuestos = new HistoricoAproPresupuestos();

                            vHistoricoAproPresupuestos.MotivoDevolucion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vHistoricoAproPresupuestos.MotivoDevolucion;
                            vHistoricoAproPresupuestos.UsuarioRechazo = vDataReaderResults["UsuarioRechazo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioRechazo"].ToString()) : vHistoricoAproPresupuestos.UsuarioRechazo;
                            vHistoricoAproPresupuestos.FechaRechazo = vDataReaderResults["FechaRechazo"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRechazo"].ToString()) : vHistoricoAproPresupuestos.FechaRechazo;
                            ListHistoricoAproPresupuestos.Add(vHistoricoAproPresupuestos);
                        }
                        return ListHistoricoAproPresupuestos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <param name="pIdArea"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        /// <param name="pIdRubro"></param>
        /// <param name="codEstado"></param>
        /// <param name="pIdRegional"></param>
        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestoCuposAre(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string codEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionPresupuestoCuposArea_Consultar"))
                {
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pIdArea != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pIdArea);
                    if (pUsuarioAprobo != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAprobo", DbType.String, pUsuarioAprobo);
                    if (pFechaAprobacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaAprobacion", DbType.DateTime, pFechaAprobacion);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdRubro != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRubro", DbType.Int32, pIdRubro);
                    if (codEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, codEstado);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProyeccionPresupuestos> vListaProyeccionPresupuestos = new List<ProyeccionPresupuestos>();
                        while (vDataReaderResults.Read())
                        {
                            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
                            vProyeccionPresupuestos.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vProyeccionPresupuestos.IdProyeccionPresupuestos;
                            vProyeccionPresupuestos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProyeccionPresupuestos.IdRegional;
                            vProyeccionPresupuestos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProyeccionPresupuestos.IdVigencia;
                            vProyeccionPresupuestos.IdRecurso = vDataReaderResults["IdRecurso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRecurso"].ToString()) : vProyeccionPresupuestos.IdRecurso;
                            vProyeccionPresupuestos.IdRubro = vDataReaderResults["IdRubro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRubro"].ToString()) : vProyeccionPresupuestos.IdRubro;
                            vProyeccionPresupuestos.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vProyeccionPresupuestos.IdArea;

                            vProyeccionPresupuestos.DescrArea = vDataReaderResults["DescrArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescrArea"].ToString()) : vProyeccionPresupuestos.DescrArea;

                            vProyeccionPresupuestos.ValorCupo = vDataReaderResults["ValorCupo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCupo"].ToString()) : vProyeccionPresupuestos.ValorCupo;
                            vProyeccionPresupuestos.TotalCupos = vDataReaderResults["TotalCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalCupos"].ToString()) : vProyeccionPresupuestos.TotalCupos;
                            vProyeccionPresupuestos.NecesidadesAsignadas = vDataReaderResults["NecesidadesAsignadas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NecesidadesAsignadas"].ToString()) : vProyeccionPresupuestos.NecesidadesAsignadas;
                            vProyeccionPresupuestos.NecesidadesDisponibles = vDataReaderResults["NecesidadesDisponibles"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NecesidadesDisponibles"].ToString()) : vProyeccionPresupuestos.NecesidadesDisponibles;
                            vProyeccionPresupuestos.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vProyeccionPresupuestos.UsuarioAprobo;
                            vProyeccionPresupuestos.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vProyeccionPresupuestos.FechaAprobacion;
                            vProyeccionPresupuestos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProyeccionPresupuestos.UsuarioCrea;
                            vProyeccionPresupuestos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProyeccionPresupuestos.FechaCrea;
                            vProyeccionPresupuestos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProyeccionPresupuestos.UsuarioModifica;
                            vProyeccionPresupuestos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProyeccionPresupuestos.FechaModifica;

                            vProyeccionPresupuestos.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vProyeccionPresupuestos.AcnoVigencia;
                            vProyeccionPresupuestos.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vProyeccionPresupuestos.NombreRegional;
                            vProyeccionPresupuestos.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vProyeccionPresupuestos.CodigoRubro;
                            vProyeccionPresupuestos.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vProyeccionPresupuestos.DescripcionEstado;

                            vProyeccionPresupuestos.DESCTIPORECURSO = vDataReaderResults["DESCTIPORECURSO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DESCTIPORECURSO"].ToString()) : vProyeccionPresupuestos.DESCTIPORECURSO;


                            vListaProyeccionPresupuestos.Add(vProyeccionPresupuestos);
                        }
                        return vListaProyeccionPresupuestos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <param name="pIdArea"></param>
        /// <param name="pValorCupo"></param>
        /// <param name="pTotalCupos"></param>      
        /// <param name="pActivo"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestoss(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionPresupuestoss_Consultar"))
                {
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pIdArea != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pIdArea);
                    if (pUsuarioAprobo != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAprobo", DbType.String, pUsuarioAprobo);
                    if (pFechaAprobacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaAprobacion", DbType.DateTime, pFechaAprobacion);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdRubro != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRubro", DbType.Int32, pIdRubro);
                    if (CodEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, CodEstado);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProyeccionPresupuestos> vListaProyeccionPresupuestos = new List<ProyeccionPresupuestos>();
                        while (vDataReaderResults.Read())
                        {
                            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
                            vProyeccionPresupuestos.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vProyeccionPresupuestos.IdProyeccionPresupuestos;
                            vProyeccionPresupuestos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProyeccionPresupuestos.IdRegional;
                            vProyeccionPresupuestos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProyeccionPresupuestos.IdVigencia;
                            vProyeccionPresupuestos.IdRecurso = vDataReaderResults["IdRecurso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRecurso"].ToString()) : vProyeccionPresupuestos.IdRecurso;
                            vProyeccionPresupuestos.IdRubro = vDataReaderResults["IdRubro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRubro"].ToString()) : vProyeccionPresupuestos.IdRubro;
                            vProyeccionPresupuestos.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vProyeccionPresupuestos.IdArea;

                            vProyeccionPresupuestos.DescrArea = vDataReaderResults["DescrArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescrArea"].ToString()) : vProyeccionPresupuestos.DescrArea;

                            vProyeccionPresupuestos.ValorCupo = vDataReaderResults["ValorCupo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCupo"].ToString()) : vProyeccionPresupuestos.ValorCupo;
                            vProyeccionPresupuestos.TotalCupos = vDataReaderResults["TotalCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalCupos"].ToString()) : vProyeccionPresupuestos.TotalCupos;
                            vProyeccionPresupuestos.NecesidadesAsignadas = vDataReaderResults["NecesidadesAsignadas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NecesidadesAsignadas"].ToString()) : vProyeccionPresupuestos.NecesidadesAsignadas;
                            vProyeccionPresupuestos.NecesidadesDisponibles = vDataReaderResults["NecesidadesDisponibles"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NecesidadesDisponibles"].ToString()) : vProyeccionPresupuestos.NecesidadesDisponibles;
                            vProyeccionPresupuestos.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vProyeccionPresupuestos.UsuarioAprobo;
                            vProyeccionPresupuestos.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vProyeccionPresupuestos.FechaAprobacion;
                            vProyeccionPresupuestos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProyeccionPresupuestos.UsuarioCrea;
                            vProyeccionPresupuestos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProyeccionPresupuestos.FechaCrea;
                            vProyeccionPresupuestos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProyeccionPresupuestos.UsuarioModifica;
                            vProyeccionPresupuestos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProyeccionPresupuestos.FechaModifica;

                            vProyeccionPresupuestos.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vProyeccionPresupuestos.AcnoVigencia;
                            vProyeccionPresupuestos.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vProyeccionPresupuestos.NombreRegional;
                            vProyeccionPresupuestos.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vProyeccionPresupuestos.CodigoRubro;
                            vProyeccionPresupuestos.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vProyeccionPresupuestos.DescripcionEstado;

                            vProyeccionPresupuestos.DESCTIPORECURSO = vDataReaderResults["DESCTIPORECURSO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DESCTIPORECURSO"].ToString()) : vProyeccionPresupuestos.DESCTIPORECURSO;


                            vListaProyeccionPresupuestos.Add(vProyeccionPresupuestos);
                        }
                        return vListaProyeccionPresupuestos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestosAdiciones(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
          , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionPresupuestosAdicion_Consultar"))
                {
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pIdArea != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdArea", DbType.Int32, pIdArea);
                    if (pUsuarioAprobo != null)
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAprobo", DbType.String, pUsuarioAprobo);
                    if (pFechaAprobacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaAprobacion", DbType.DateTime, pFechaAprobacion);
                    if (pIdRegional != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    if (pIdRubro != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRubro", DbType.Int32, pIdRubro);
                    if (CodEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodEstado", DbType.String, CodEstado);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProyeccionPresupuestos> vListaProyeccionPresupuestos = new List<ProyeccionPresupuestos>();
                        while (vDataReaderResults.Read())
                        {
                            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
                            vProyeccionPresupuestos.IdProyeccionPresupuestos = vDataReaderResults["IdProyeccionPresupuestos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresupuestos"].ToString()) : vProyeccionPresupuestos.IdProyeccionPresupuestos;
                            vProyeccionPresupuestos.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vProyeccionPresupuestos.IdRegional;
                            vProyeccionPresupuestos.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vProyeccionPresupuestos.IdVigencia;
                            vProyeccionPresupuestos.IdRecurso = vDataReaderResults["IdRecurso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRecurso"].ToString()) : vProyeccionPresupuestos.IdRecurso;
                            vProyeccionPresupuestos.IdRubro = vDataReaderResults["IdRubro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRubro"].ToString()) : vProyeccionPresupuestos.IdRubro;
                            vProyeccionPresupuestos.IdArea = vDataReaderResults["IdArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArea"].ToString()) : vProyeccionPresupuestos.IdArea;

                            vProyeccionPresupuestos.DescrArea = vDataReaderResults["DescrArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescrArea"].ToString()) : vProyeccionPresupuestos.DescrArea;

                            vProyeccionPresupuestos.ValorCupo = vDataReaderResults["ValorCupo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCupo"].ToString()) : vProyeccionPresupuestos.ValorCupo;
                            vProyeccionPresupuestos.TotalCupos = vDataReaderResults["TotalCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalCupos"].ToString()) : vProyeccionPresupuestos.TotalCupos;
                            vProyeccionPresupuestos.NecesidadesAsignadas = vDataReaderResults["NecesidadesAsignadas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NecesidadesAsignadas"].ToString()) : vProyeccionPresupuestos.NecesidadesAsignadas;
                            vProyeccionPresupuestos.NecesidadesDisponibles = vDataReaderResults["NecesidadesDisponibles"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NecesidadesDisponibles"].ToString()) : vProyeccionPresupuestos.NecesidadesDisponibles;
                            vProyeccionPresupuestos.UsuarioAprobo = vDataReaderResults["UsuarioAprobo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAprobo"].ToString()) : vProyeccionPresupuestos.UsuarioAprobo;
                            vProyeccionPresupuestos.FechaAprobacion = vDataReaderResults["FechaAprobacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAprobacion"].ToString()) : vProyeccionPresupuestos.FechaAprobacion;
                            vProyeccionPresupuestos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProyeccionPresupuestos.UsuarioCrea;
                            vProyeccionPresupuestos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProyeccionPresupuestos.FechaCrea;
                            vProyeccionPresupuestos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProyeccionPresupuestos.UsuarioModifica;
                            vProyeccionPresupuestos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProyeccionPresupuestos.FechaModifica;

                            vProyeccionPresupuestos.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vProyeccionPresupuestos.AcnoVigencia;
                            vProyeccionPresupuestos.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vProyeccionPresupuestos.NombreRegional;
                            vProyeccionPresupuestos.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vProyeccionPresupuestos.CodigoRubro;
                            vProyeccionPresupuestos.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vProyeccionPresupuestos.DescripcionEstado;

                            vProyeccionPresupuestos.DESCTIPORECURSO = vDataReaderResults["DESCTIPORECURSO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DESCTIPORECURSO"].ToString()) : vProyeccionPresupuestos.DESCTIPORECURSO;


                            vListaProyeccionPresupuestos.Add(vProyeccionPresupuestos);
                        }
                        return vListaProyeccionPresupuestos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int AprobarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionAprobacionPresupuestos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pProyeccionPresupuestos.IdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@Aprobado", DbType.Boolean, pProyeccionPresupuestos.Aprobado);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pProyeccionPresupuestos.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorSolicitado", DbType.Decimal, pProyeccionPresupuestos.ValorCupo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAprobado", DbType.Decimal, pProyeccionPresupuestos.ValorCupoAprobado);
                    vDataBase.AddInParameter(vDbCommand, "@CuposSolicitado", DbType.Int32, pProyeccionPresupuestos.TotalCupos);
                    vDataBase.AddInParameter(vDbCommand, "@CuposAprobado", DbType.Int32, pProyeccionPresupuestos.TotalCuposAprobado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModificar", DbType.String, pProyeccionPresupuestos.UsuarioModifica);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProyeccionPresupuestos.IdProyeccionPresupuestos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProyeccionPresupuestos").ToString());
                    GenerarLogAuditoria(pProyeccionPresupuestos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionPresupuestos(List<ProyeccionPresupuestos> pProyeccionPresupuestos, string pUsuarioModifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionAprobacionPresupuestos_XML"))
                {
                    int vResultado;

                    var xml = new XElement("DatosProyeccionPresupuesto",
                                              pProyeccionPresupuestos.Select(i => new XElement("ProyeccionPresupuesto",
                                              new XAttribute("IdProyeccionPresupuestos", i.IdProyeccionPresupuestos),
                                               new XAttribute("CodEstado", i.CodEstadoProceso),
                                               new XAttribute("ValorCupoSolicitado", i.ValorCupo),
                                               new XAttribute("ValorCupoAprobado", i.ValorCupoAprobado),
                                               new XAttribute("CuposSolicitado", i.TotalCupos),
                                               new XAttribute("CuposAprobados", i.TotalCuposAprobado),
                                               new XAttribute("Aprobado", i.Aprobado),
                                               new XAttribute("Descripcion", i.Descripcion)
                                               )));

                    vDataBase.AddInParameter(vDbCommand, "@ProyeccionPresupuestoXml", DbType.String, xml.ToString());
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoAproPresupuestos> ConsultarHistoricoProyeccionPresupuestos(int pIdProyeccionPresupuestos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoProyeccionPresupuestos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoAproPresupuestos> ListHistoricoAproPresupuestos = new List<HistoricoAproPresupuestos>();

                        while (vDataReaderResults.Read())
                        {
                            HistoricoAproPresupuestos vHistoricoAproPresupuestos = new HistoricoAproPresupuestos();

                            vHistoricoAproPresupuestos.MotivoDevolucion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vHistoricoAproPresupuestos.MotivoDevolucion;
                            vHistoricoAproPresupuestos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoAproPresupuestos.FechaCrea;
                            vHistoricoAproPresupuestos.FechaRechazo = vDataReaderResults["FechaRechazo"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRechazo"].ToString()) : vHistoricoAproPresupuestos.FechaRechazo;
                            ListHistoricoAproPresupuestos.Add(vHistoricoAproPresupuestos);
                        }
                        return ListHistoricoAproPresupuestos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
