using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad HistoricoAdicionCandidato
    /// </summary>
    public class HistoricoAdicionCandidatoDAL : GeneralDAL
    {
        public HistoricoAdicionCandidatoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pHistoricoAdicionCandidato"></param>
        public int InsertarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionCandidato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoAdicionCandidato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pHistoricoAdicionCandidato.IdCupoArea);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pHistoricoAdicionCandidato.IdProyeccionPresuspuesto);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pHistoricoAdicionCandidato.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicionAnterior", DbType.Decimal, pHistoricoAdicionCandidato.ValorAdicionAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistoricoAdicionCandidato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pHistoricoAdicionCandidato.IdHistoricoAdicionCandidato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoAdicionCandidato").ToString());
                    GenerarLogAuditoria(pHistoricoAdicionCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pHistoricoAdicionCandidato"></param>
        public int ModificarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionCandidato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoAdicionCandidato", DbType.Int32, pHistoricoAdicionCandidato.IdHistoricoAdicionCandidato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pHistoricoAdicionCandidato.IdCupoArea);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pHistoricoAdicionCandidato.IdProyeccionPresuspuesto);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pHistoricoAdicionCandidato.ValorAdicion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicionAnterior", DbType.Decimal, pHistoricoAdicionCandidato.ValorAdicionAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pHistoricoAdicionCandidato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pHistoricoAdicionCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pHistoricoAdicionCandidato"></param>
        public int EliminarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionCandidato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoAdicionCandidato", DbType.Int32, pHistoricoAdicionCandidato.IdHistoricoAdicionCandidato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pHistoricoAdicionCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pIdHistoricoAdicionCandidato"></param>
        public HistoricoAdicionCandidato ConsultarHistoricoAdicionCandidato(int pIdHistoricoAdicionCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionCandidato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoAdicionCandidato", DbType.Int32, pIdHistoricoAdicionCandidato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        HistoricoAdicionCandidato vHistoricoAdicionCandidato = new HistoricoAdicionCandidato();
                        while (vDataReaderResults.Read())
                        {
                            vHistoricoAdicionCandidato.IdHistoricoAdicionCandidato = vDataReaderResults["IdHistoricoAdicionCandidato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoAdicionCandidato"].ToString()) : vHistoricoAdicionCandidato.IdHistoricoAdicionCandidato;
                            vHistoricoAdicionCandidato.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vHistoricoAdicionCandidato.IdCupoArea;
                            vHistoricoAdicionCandidato.IdProyeccionPresuspuesto = vDataReaderResults["IdProyeccionPresuspuesto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresuspuesto"].ToString()) : vHistoricoAdicionCandidato.IdProyeccionPresuspuesto;
                            vHistoricoAdicionCandidato.ValorAdicion = vDataReaderResults["ValorAdicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicion"].ToString()) : vHistoricoAdicionCandidato.ValorAdicion;
                            vHistoricoAdicionCandidato.ValorAdicionAnterior = vDataReaderResults["ValorAdicionAnterior"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionAnterior"].ToString()) : vHistoricoAdicionCandidato.ValorAdicionAnterior;
                            vHistoricoAdicionCandidato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoAdicionCandidato.UsuarioCrea;
                            vHistoricoAdicionCandidato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoAdicionCandidato.FechaCrea;
                            vHistoricoAdicionCandidato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoAdicionCandidato.UsuarioModifica;
                            vHistoricoAdicionCandidato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoAdicionCandidato.FechaModifica;
                        }
                        return vHistoricoAdicionCandidato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pValorAdicion"></param>
        /// <param name="pValorAdicionAnterior"></param>
        public List<HistoricoAdicionCandidato> ConsultarHistoricoAdicionCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, Decimal? pValorAdicion, Decimal? pValorAdicionAnterior)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_HistoricoAdicionCandidatos_Consultar"))
                {
                    if(pIdCupoArea != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pIdCupoArea);
                    if(pIdProyeccionPresuspuesto != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pIdProyeccionPresuspuesto);
                    if(pValorAdicion != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorAdicion", DbType.Decimal, pValorAdicion);
                    if(pValorAdicionAnterior != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorAdicionAnterior", DbType.Decimal, pValorAdicionAnterior);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoAdicionCandidato> vListaHistoricoAdicionCandidato = new List<HistoricoAdicionCandidato>();
                        while (vDataReaderResults.Read())
                        {
                                HistoricoAdicionCandidato vHistoricoAdicionCandidato = new HistoricoAdicionCandidato();
                            vHistoricoAdicionCandidato.IdHistoricoAdicionCandidato = vDataReaderResults["IdHistoricoAdicionCandidato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoAdicionCandidato"].ToString()) : vHistoricoAdicionCandidato.IdHistoricoAdicionCandidato;
                            vHistoricoAdicionCandidato.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vHistoricoAdicionCandidato.IdCupoArea;
                            vHistoricoAdicionCandidato.IdProyeccionPresuspuesto = vDataReaderResults["IdProyeccionPresuspuesto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresuspuesto"].ToString()) : vHistoricoAdicionCandidato.IdProyeccionPresuspuesto;
                            vHistoricoAdicionCandidato.ValorAdicion = vDataReaderResults["ValorAdicion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicion"].ToString()) : vHistoricoAdicionCandidato.ValorAdicion;
                            vHistoricoAdicionCandidato.ValorAdicionAnterior = vDataReaderResults["ValorAdicionAnterior"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionAnterior"].ToString()) : vHistoricoAdicionCandidato.ValorAdicionAnterior;
                            vHistoricoAdicionCandidato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoAdicionCandidato.UsuarioCrea;
                            vHistoricoAdicionCandidato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoAdicionCandidato.FechaCrea;
                            vHistoricoAdicionCandidato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoAdicionCandidato.UsuarioModifica;
                            vHistoricoAdicionCandidato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoAdicionCandidato.FechaModifica;
                                vListaHistoricoAdicionCandidato.Add(vHistoricoAdicionCandidato);
                        }
                        return vListaHistoricoAdicionCandidato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
