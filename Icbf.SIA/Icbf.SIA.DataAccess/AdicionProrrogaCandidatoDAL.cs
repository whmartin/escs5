using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad AdicionProrrogaCandidato
    /// </summary>
    public class AdicionProrrogaCandidatoDAL : GeneralDAL
    {
        public AdicionProrrogaCandidatoDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pAdicionProrrogaCandidato"></param>
        public int InsertarAdicionProrrogaCandidato(AdicionProrrogaCandidato pAdicionProrrogaCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProrrogaCandidato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAdicionProrrogaCandidato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pAdicionProrrogaCandidato.IdCupoArea);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pAdicionProrrogaCandidato.IdProyeccionPresuspuesto);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIncioProyeccion", DbType.DateTime, pAdicionProrrogaCandidato.FechaIncioProyeccion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinProyeccion", DbType.DateTime, pAdicionProrrogaCandidato.FechaFinProyeccion);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdicionado", DbType.Decimal, pAdicionProrrogaCandidato.ValorAdicionado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAdicionProrrogaCandidato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAdicionProrrogaCandidato.IdAdicionProrrogaCandidato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAdicionProrrogaCandidato").ToString());
                    GenerarLogAuditoria(pAdicionProrrogaCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pAdicionProrrogaCandidato"></param>
        public int ModificarAdicionProrrogaCandidato(AdicionProrrogaCandidato pAdicionProrrogaCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProrrogaCandidato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProrrogaCandidato", DbType.Int32, pAdicionProrrogaCandidato.IdAdicionProrrogaCandidato);
                    vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pAdicionProrrogaCandidato.IdCupoArea);
                    vDataBase.AddInParameter(vDbCommand, "@Aprobado", DbType.Boolean, pAdicionProrrogaCandidato.Aprobado);
                    vDataBase.AddInParameter(vDbCommand, "@Rechazado", DbType.Boolean, pAdicionProrrogaCandidato.Rechazado);
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pAdicionProrrogaCandidato.IdProyeccionPresuspuesto);;
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAdicionProrrogaCandidato.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@RazonRechazo", DbType.String, pAdicionProrrogaCandidato.RazonRechazo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdicionProrrogaCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pAdicionProrrogaCandidato"></param>
        public int EliminarAdicionProrrogaCandidato(int pAdicionProrrogaCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProrrogaCandidato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProrrogaCandidato", DbType.Int32, pAdicionProrrogaCandidato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    // GenerarLogAuditoria(pAdicionProrrogaCandidato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pIdAdicionProrrogaCandidato"></param>
        public AdicionProrrogaCandidato ConsultarAdicionProrrogaCandidato(int pIdAdicionProrrogaCandidato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProrrogaCandidato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAdicionProrrogaCandidato", DbType.Int32, pIdAdicionProrrogaCandidato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AdicionProrrogaCandidato vAdicionProrrogaCandidato = new AdicionProrrogaCandidato();
                        while (vDataReaderResults.Read())
                        {
                            vAdicionProrrogaCandidato.IdAdicionProrrogaCandidato = vDataReaderResults["IdAdicionProrrogaCandidato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProrrogaCandidato"].ToString()) : vAdicionProrrogaCandidato.IdAdicionProrrogaCandidato;
                            vAdicionProrrogaCandidato.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vAdicionProrrogaCandidato.IdCupoArea;
                            vAdicionProrrogaCandidato.Aprobado = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Aprobado"].ToString()) : vAdicionProrrogaCandidato.Aprobado;
                            vAdicionProrrogaCandidato.Rechazado = vDataReaderResults["Rechazado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Rechazado"].ToString()) : vAdicionProrrogaCandidato.Rechazado;
                            vAdicionProrrogaCandidato.IdProyeccionPresuspuesto = vDataReaderResults["IdProyeccionPresuspuesto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresuspuesto"].ToString()) : vAdicionProrrogaCandidato.IdProyeccionPresuspuesto;
                            vAdicionProrrogaCandidato.FechaIncioProyeccion = vDataReaderResults["FechaIncioProyeccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncioProyeccion"].ToString()) : vAdicionProrrogaCandidato.FechaIncioProyeccion;
                            vAdicionProrrogaCandidato.FechaFinProyeccion = vDataReaderResults["FechaFinProyeccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinProyeccion"].ToString()) : vAdicionProrrogaCandidato.FechaFinProyeccion;
                            vAdicionProrrogaCandidato.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vAdicionProrrogaCandidato.ValorAdicionado;
                            vAdicionProrrogaCandidato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdicionProrrogaCandidato.UsuarioCrea;
                            vAdicionProrrogaCandidato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdicionProrrogaCandidato.FechaCrea;
                            vAdicionProrrogaCandidato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdicionProrrogaCandidato.UsuarioModifica;
                            vAdicionProrrogaCandidato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdicionProrrogaCandidato.FechaModifica;
                        }
                        return vAdicionProrrogaCandidato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pFechaIncioProyeccion"></param>
        /// <param name="pFechaFinProyeccion"></param>
        /// <param name="pValorAdicionado"></param>
        public List<AdicionProrrogaCandidato> ConsultarAdicionProrrogaCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, DateTime? pFechaIncioProyeccion, DateTime? pFechaFinProyeccion, Decimal? pValorAdicionado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_AdicionProrrogaCandidatos_Consultar"))
                {
                    if(pIdCupoArea != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdCupoArea", DbType.Int32, pIdCupoArea);
                    if(pIdProyeccionPresuspuesto != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresuspuesto", DbType.Int32, pIdProyeccionPresuspuesto);
                    if(pFechaIncioProyeccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaIncioProyeccion", DbType.DateTime, pFechaIncioProyeccion);
                    if(pFechaFinProyeccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaFinProyeccion", DbType.DateTime, pFechaFinProyeccion);
                    if(pValorAdicionado != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorAdicionado", DbType.Decimal, pValorAdicionado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AdicionProrrogaCandidato> vListaAdicionProrrogaCandidato = new List<AdicionProrrogaCandidato>();
                        while (vDataReaderResults.Read())
                        {
                                AdicionProrrogaCandidato vAdicionProrrogaCandidato = new AdicionProrrogaCandidato();
                            vAdicionProrrogaCandidato.IdAdicionProrrogaCandidato = vDataReaderResults["IdAdicionProrrogaCandidato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdicionProrrogaCandidato"].ToString()) : vAdicionProrrogaCandidato.IdAdicionProrrogaCandidato;
                            vAdicionProrrogaCandidato.IdCupoArea = vDataReaderResults["IdCupoArea"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCupoArea"].ToString()) : vAdicionProrrogaCandidato.IdCupoArea;
                            vAdicionProrrogaCandidato.IdProyeccionPresuspuesto = vDataReaderResults["IdProyeccionPresuspuesto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProyeccionPresuspuesto"].ToString()) : vAdicionProrrogaCandidato.IdProyeccionPresuspuesto;
                            vAdicionProrrogaCandidato.FechaIncioProyeccion = vDataReaderResults["FechaIncioProyeccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncioProyeccion"].ToString()) : vAdicionProrrogaCandidato.FechaIncioProyeccion;
                            vAdicionProrrogaCandidato.FechaFinProyeccion = vDataReaderResults["FechaFinProyeccion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinProyeccion"].ToString()) : vAdicionProrrogaCandidato.FechaFinProyeccion;
                            vAdicionProrrogaCandidato.ValorAdicionado = vDataReaderResults["ValorAdicionado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdicionado"].ToString()) : vAdicionProrrogaCandidato.ValorAdicionado;
                            vAdicionProrrogaCandidato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdicionProrrogaCandidato.UsuarioCrea;
                            vAdicionProrrogaCandidato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdicionProrrogaCandidato.FechaCrea;
                            vAdicionProrrogaCandidato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdicionProrrogaCandidato.UsuarioModifica;
                            vAdicionProrrogaCandidato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdicionProrrogaCandidato.FechaModifica;
                                vListaAdicionProrrogaCandidato.Add(vAdicionProrrogaCandidato);
                        }
                        return vListaAdicionProrrogaCandidato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
