using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad CategoriaValores
    /// </summary>
    public class CategoriaValoresDAL : GeneralDAL
    {
        public CategoriaValoresDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad CategoriaValores
        /// </summary>
        /// <param name="pCategoriaValores"></param>
        public int InsertarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaValores_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCategoriaValor", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaEmpleados", DbType.Int32, pCategoriaValores.IdCategoriaEmpleados);
                    vDataBase.AddInParameter(vDbCommand, "@Nivel", DbType.Int32, pCategoriaValores.Nivel);
                    vDataBase.AddInParameter(vDbCommand, "@ValorMinimo", DbType.Decimal, pCategoriaValores.ValorMinimo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorMaximo", DbType.Decimal, pCategoriaValores.ValorMaximo);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pCategoriaValores.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pCategoriaValores.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCategoriaValores.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCategoriaValores.IdCategoriaValor = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCategoriaValor").ToString());
                    GenerarLogAuditoria(pCategoriaValores, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CategoriaValores
        /// </summary>
        /// <param name="pCategoriaValores"></param>
        public int ModificarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaValores_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValor", DbType.Int32, pCategoriaValores.IdCategoriaValor);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaEmpleados", DbType.Int32, pCategoriaValores.IdCategoriaEmpleados);
                    vDataBase.AddInParameter(vDbCommand, "@Nivel", DbType.Int32, pCategoriaValores.Nivel);
                    vDataBase.AddInParameter(vDbCommand, "@ValorMinimo", DbType.Decimal, pCategoriaValores.ValorMinimo);
                    vDataBase.AddInParameter(vDbCommand, "@ValorMaximo", DbType.Decimal, pCategoriaValores.ValorMaximo);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pCategoriaValores.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pCategoriaValores.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCategoriaValores.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCategoriaValores, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CategoriaValores
        /// </summary>
        /// <param name="pCategoriaValores"></param>
        public int EliminarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaValores_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValor", DbType.Int32, pCategoriaValores.IdCategoriaValor);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCategoriaValores, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad CategoriaValores
        /// </summary>
        /// <param name="pIdCategoriaValor"></param>
        public CategoriaValores ConsultarCategoriaValores(int pIdCategoriaValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaValores_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValor", DbType.Int32, pIdCategoriaValor);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CategoriaValores vCategoriaValores = new CategoriaValores();
                        while (vDataReaderResults.Read())
                        {
                            vCategoriaValores.IdCategoriaValor = vDataReaderResults["IdCategoriaValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValor"].ToString()) : vCategoriaValores.IdCategoriaValor;
                            vCategoriaValores.IdCategoriaEmpleados = vDataReaderResults["IdCategoriaEmpleados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaEmpleados"].ToString()) : vCategoriaValores.IdCategoriaEmpleados;
                            vCategoriaValores.Nivel = vDataReaderResults["Nivel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Nivel"].ToString()) : vCategoriaValores.Nivel;
                            vCategoriaValores.ValorMinimo = vDataReaderResults["ValorMinimo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorMinimo"].ToString()) : vCategoriaValores.ValorMinimo;
                            vCategoriaValores.ValorMaximo = vDataReaderResults["ValorMaximo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorMaximo"].ToString()) : vCategoriaValores.ValorMaximo;
                            vCategoriaValores.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vCategoriaValores.IdVigencia;
                            vCategoriaValores.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vCategoriaValores.Activo;
                            vCategoriaValores.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCategoriaValores.UsuarioCrea;
                            vCategoriaValores.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCategoriaValores.FechaCrea;
                            vCategoriaValores.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCategoriaValores.UsuarioModifica;
                            vCategoriaValores.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCategoriaValores.FechaModifica;
                        }
                        return vCategoriaValores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CategoriaValores
        /// </summary>
        /// <param name="pNivel"></param>
        /// <param name="pIdVigencia"></param>
        /// <param name="pActivo"></param>
        public List<CategoriaValores> ConsultarCategoriaValoress(int? pNivel, int? pIdVigencia, Boolean? pActivo, int? IdCategoriaEmpleados)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaValoress_Consultar"))
                {
                    if (pNivel != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nivel", DbType.Int32, pNivel);
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pActivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pActivo);

                    if (IdCategoriaEmpleados != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaEmpleados", DbType.Int32, IdCategoriaEmpleados);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CategoriaValores> vListaCategoriaValores = new List<CategoriaValores>();
                        while (vDataReaderResults.Read())
                        {
                            CategoriaValores vCategoriaValores = new CategoriaValores();
                            vCategoriaValores.IdCategoriaValor = vDataReaderResults["IdCategoriaValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValor"].ToString()) : vCategoriaValores.IdCategoriaValor;
                            vCategoriaValores.IdCategoriaEmpleados = vDataReaderResults["IdCategoriaEmpleados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaEmpleados"].ToString()) : vCategoriaValores.IdCategoriaEmpleados;
                            vCategoriaValores.Nivel = vDataReaderResults["Nivel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Nivel"].ToString()) : vCategoriaValores.Nivel;
                            vCategoriaValores.ValorMinimo = vDataReaderResults["ValorMinimo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorMinimo"].ToString()) : vCategoriaValores.ValorMinimo;
                            vCategoriaValores.ValorMaximo = vDataReaderResults["ValorMaximo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorMaximo"].ToString()) : vCategoriaValores.ValorMaximo;
                            vCategoriaValores.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vCategoriaValores.IdVigencia;
                            vCategoriaValores.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vCategoriaValores.Activo;
                            vCategoriaValores.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCategoriaValores.UsuarioCrea;
                            vCategoriaValores.CategoriaEmpleado = vDataReaderResults["CategoriaEmpleado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CategoriaEmpleado"].ToString()) : vCategoriaValores.CategoriaEmpleado;
                            vCategoriaValores.AcnioVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vCategoriaValores.AcnioVigencia;

                            vCategoriaValores.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCategoriaValores.FechaCrea;
                            vCategoriaValores.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCategoriaValores.UsuarioModifica;
                            vCategoriaValores.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCategoriaValores.FechaModifica;
                            vListaCategoriaValores.Add(vCategoriaValores);
                        }
                        return vListaCategoriaValores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CategoriaValores> ConsultarCategoriaValoresAsignada(int? pIdProyeccionPresupuestos, int? pIdCategoriaEmpleado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_CategoriaValoresAsignada_Consultar"))
                {
                    if (pIdProyeccionPresupuestos != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProyeccionPresupuestos", DbType.Int32, pIdProyeccionPresupuestos);
                    if (pIdCategoriaEmpleado != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdCategoriaEmpleado", DbType.Int32, pIdCategoriaEmpleado);



                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CategoriaValores> vListaCategoriaValoresAsig = new List<CategoriaValores>();
                        while (vDataReaderResults.Read())
                        {
                            CategoriaValores vCategoriaValoresAsig = new CategoriaValores();
                            vCategoriaValoresAsig.IdCategoriaValor = vDataReaderResults["IdCategoriaValores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValores"].ToString()) : vCategoriaValoresAsig.IdCategoriaValor;
                            vCategoriaValoresAsig.Nivel = vDataReaderResults["Nivel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Nivel"].ToString()) : vCategoriaValoresAsig.Nivel;
                            vListaCategoriaValoresAsig.Add(vCategoriaValoresAsig);
                        }
                        return vListaCategoriaValoresAsig;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
