﻿using System;
using System.Collections.Generic;
using Icbf.SIA.Entity.Concursales;
using Icbf.Utilities.Exceptions;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace Icbf.SIA.DataAccess.Concursales
{
    public class SolicitudesDAL : GeneralDAL
    {
        private string vURL = System.Configuration.ConfigurationManager.AppSettings["URLWebApiNMF"];

        private const string CarteraActiva = "ConsultarCarteraActiva";
        private const string CarteraActivaAgrupada = "ConsultarCarteraActivaAgrupada";
        private const string CarteraActivaPorId = "ConsultarCarteraActivaPorId";

        private ParametricasDAL _ParametricasDAL;
        public SolicitudesDAL()
        {
            _ParametricasDAL = new ParametricasDAL();
        }

        #region Solicitud Proceso Concursal

        public List<CarteraActiva> ConsultarCarteraActiva(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            var consumeServicio = _ParametricasDAL.ConsultarParametricaPorCodigo("SPARAM", "ConsumeServicioWEB-NMF");

            if(consumeServicio.Auxiliar1 == "1")
                return ConsultarCarteraActivaServicio(vIdentificacion, vIdTipoDocumento, vIDRegional);
            else
                return ConsultarCarteraActivaLServer(vIdentificacion, vIdTipoDocumento, vIDRegional);
        }

        private List<CarteraActiva> ConsultarCarteraActivaServicio(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {

            List<CarteraActiva> result = new List<CarteraActiva>();
            try
            {
                string queryStrings = string.Empty;

                if(!string.IsNullOrEmpty(vIdentificacion))
                    queryStrings = "identificacion=" + vIdentificacion;

                if(vIdTipoDocumento.HasValue)
                {
                    if(queryStrings == string.Empty)
                        queryStrings = "idTipoIdentificacion=" + vIdTipoDocumento.Value;
                    else
                        queryStrings = "&idTipoIdentificacion=" + vIdTipoDocumento.Value;
                }

                if(!string.IsNullOrEmpty(vIDRegional))
                {
                    if(queryStrings == string.Empty)
                        queryStrings = "codigoRegionalPCI=" + vIDRegional;
                    else
                        queryStrings = "&codigoRegionalPCI=" + vIDRegional;
                }

                if(queryStrings != string.Empty)
                    queryStrings = "?" + queryStrings;

                string uri = vURL + CarteraActiva + queryStrings;

                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(uri);

                HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                using(StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    string responseJson = sr.ReadToEnd();
                    var serializer = new JavaScriptSerializer();
                    result = serializer.Deserialize<List<CarteraActiva>>(responseJson);
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        private List<CarteraActiva> ConsultarCarteraActivaLServer(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_NMF_CarteraActivaRazonSocial_Identificacion_Consulta"))
                {

                    if(!string.IsNullOrEmpty(vIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@identificacion", DbType.String, vIdentificacion);
                    if(vIdTipoDocumento.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@idTipoDocumento", DbType.Int32, vIdTipoDocumento);
                    if(!string.IsNullOrEmpty(vIDRegional))
                        vDataBase.AddInParameter(vDbCommand, "@codRegionalPCI", DbType.String, vIDRegional);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CarteraActiva> vListaValores = new List<CarteraActiva>();
                        while(vDataReaderResults.Read())
                        {
                            CarteraActiva vParametrica = new CarteraActiva();
                            vParametrica.identificacion = vDataReaderResults["identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["identificacion"].ToString()) : vParametrica.identificacion;
                            vParametrica.razonSocial = vDataReaderResults["razonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["razonSocial"].ToString()) : vParametrica.razonSocial;
                            vParametrica.capitalInicial = vDataReaderResults["capitalInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["capitalInicial"].ToString()) : vParametrica.capitalInicial;
                            vParametrica.interesesInicial = vDataReaderResults["interesesInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["interesesInicial"].ToString()) : vParametrica.interesesInicial;
                            vParametrica.saldoCapital = vDataReaderResults["saldoCapital"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["saldoCapital"].ToString()) : vParametrica.capitalInicial;
                            vParametrica.saldoIntereses = vDataReaderResults["saldoIntereses"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["saldoIntereses"].ToString()) : vParametrica.saldoIntereses;
                            vParametrica.liquidacion = vDataReaderResults["liquidacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["liquidacion"].ToString()) : vParametrica.liquidacion;
                            vParametrica.resolucion = vDataReaderResults["resolucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["resolucion"].ToString()) : vParametrica.resolucion;
                            vParametrica.idCarteraActiva = vDataReaderResults["idCarteraActiva"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idCarteraActiva"].ToString()) : vParametrica.idCarteraActiva;
                            vParametrica.DescripcionPCI = vDataReaderResults["DescripcionPCI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionPCI"].ToString()) : vParametrica.DescripcionPCI;
                            vParametrica.CodRegionalPCI = vDataReaderResults["CodRegionalPCI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegionalPCI"].ToString()) : vParametrica.CodRegionalPCI;
                            vParametrica.CodRegionalICBF = vDataReaderResults["CodRegionalICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegionalICBF"].ToString()) : vParametrica.CodRegionalICBF;
                            vListaValores.Add(vParametrica);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CarteraAgrupada> ConsultarCarteraActivaAgrupada(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            var consumeServicio = _ParametricasDAL.ConsultarParametricaPorCodigo("SPARAM", "ConsumeServicioWEB-NMF");

            if(consumeServicio.Auxiliar1 == "1")
                return ConsultarCarteraActivaAgrupadaServicio(vIdentificacion, vIdTipoDocumento, vIDRegional);
            else
                return ConsultarCarteraActivaAgrupadaLServer(vIdentificacion, vIdTipoDocumento, vIDRegional);
        }

        private List<CarteraAgrupada> ConsultarCarteraActivaAgrupadaServicio(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            List<CarteraAgrupada> result = new List<CarteraAgrupada>();
            try
            {
                string queryStrings = string.Empty;

                if(!string.IsNullOrEmpty(vIdentificacion))
                    queryStrings = "identificacion=" + vIdentificacion;

                if(vIdTipoDocumento.HasValue)
                {
                    if(queryStrings == string.Empty)
                        queryStrings = "idTipoIdentificacion=" + vIdTipoDocumento.Value;
                    else
                        queryStrings += "&idTipoIdentificacion=" + vIdTipoDocumento.Value;
                }

                if(!string.IsNullOrEmpty(vIDRegional))
                {
                    if(queryStrings == string.Empty)
                        queryStrings = "codigoRegionalPCI=" + vIDRegional;
                    else
                        queryStrings += "&codigoRegionalPCI=" + vIDRegional;
                }

                if(queryStrings != string.Empty)
                    queryStrings = "?" + queryStrings;

                string uri = vURL + CarteraActivaAgrupada + queryStrings;

                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(uri);

                HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                using(StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    string responseJson = sr.ReadToEnd();
                    var serializer = new JavaScriptSerializer();
                    result = serializer.Deserialize<List<CarteraAgrupada>>(responseJson);
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        private List<CarteraAgrupada> ConsultarCarteraActivaAgrupadaLServer(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_NMF_CarteraActivaRazonSocial_Identificacion_ConsultaAgrupada"))
                {

                    if(! string.IsNullOrEmpty(vIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@identificacion", DbType.String, vIdentificacion);
                    if(vIdTipoDocumento.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@idTipoDocumento", DbType.Int32, vIdTipoDocumento);
                    if(!string.IsNullOrEmpty(vIDRegional))
                        vDataBase.AddInParameter(vDbCommand, "@codRegionalPCI", DbType.String, vIDRegional);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CarteraAgrupada> vListaValores = new List<CarteraAgrupada>();
                        while(vDataReaderResults.Read())
                        {
                            CarteraAgrupada vParametrica = new CarteraAgrupada();
                            vParametrica.identificacion = vDataReaderResults["identificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["identificacion"].ToString()) : vParametrica.identificacion;
                            vParametrica.razonSocial = vDataReaderResults["razonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["razonSocial"].ToString()) : vParametrica.razonSocial;
                            vParametrica.capitalInicial = vDataReaderResults["capitalInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["capitalInicial"].ToString()) : vParametrica.capitalInicial;
                            vParametrica.interesesInicial = vDataReaderResults["interesesInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["interesesInicial"].ToString()) : vParametrica.interesesInicial;
                            vParametrica.saldoCapital = vDataReaderResults["saldoCapital"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["saldoCapital"].ToString()) : vParametrica.capitalInicial;
                            vParametrica.saldoIntereses = vDataReaderResults["saldoIntereses"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["saldoIntereses"].ToString()) : vParametrica.saldoIntereses;

                            vListaValores.Add(vParametrica);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public KeyValuePair<int, bool> ValidarCarteraTercero(string identificacion)
        {
            bool existe = false;
            int idTercero = 0;

            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Concursales_Proceso_ValidarCarteraTercero"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@Existe", DbType.Boolean, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IdTercero", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, identificacion);
                    var result = vDataBase.ExecuteNonQuery(vDbCommand);
                    existe = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@Existe").ToString());
                    idTercero = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTercero").ToString());
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }

            return new KeyValuePair<int, bool>(idTercero, existe);
        }

        public CarteraActiva ConsultarCarteraActivaPorId(int vId)
        {

            CarteraActiva result = null;
            try
            {
                string queryStrings = string.Empty;

                queryStrings = "?Id=" + vId;

                string uri = vURL + CarteraActivaPorId + queryStrings;

                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(uri);

                HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                using(StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    string responseJson = sr.ReadToEnd();
                    var serializer = new JavaScriptSerializer();
                    result = serializer.Deserialize<CarteraActiva>(responseJson);
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public bool ExisteCarteraActivaAsociada(int vId, string resolucion)
        {
            bool vResultado = false;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Concursales_Proceso_ExisteProceso"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@Existe", DbType.Boolean, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdCarteraActiva", DbType.Int32, vId);
                    vDataBase.AddInParameter(vDbCommand, "@resolucion", DbType.String, resolucion);
                    var result = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@Existe").ToString());
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string IniciarProcesoConcursal(CarteraActiva item)
        {
            string vConsecutivo = string.Empty;

            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Concursales_Proceso_IniciarProceso"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@Consecutivo", DbType.String, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CapitalInicial", DbType.Decimal, item.capitalInicial);
                    vDataBase.AddInParameter(vDbCommand, "@SaldoCapital", DbType.Decimal, item.saldoCapital);
                    vDataBase.AddInParameter(vDbCommand, "@SaldoIntereses", DbType.Decimal, item.saldoIntereses);
                    vDataBase.AddInParameter(vDbCommand, "@InteresesInicial", DbType.Decimal, item.interesesInicial);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegionalPCI", DbType.String, item.CodRegionalPCI);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegionalICBF", DbType.String, item.CodRegionalICBF);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, item.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, item.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdsDocumnetos", DbType.String, item.IdsDocumentos);

                    var result = vDataBase.ExecuteNonQuery(vDbCommand);
                    vConsecutivo = Convert.ToString(vDataBase.GetParameterValue(vDbCommand, "@Consecutivo").ToString());
                    return vConsecutivo;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Solicitud Proceso Concursal Documento

        public int EliminarDocumentoProcesoConcursal(int idDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Documentos_EliminarDocumento"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, idDocumento);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosProcesoConcursal(ProcesoConcursalDocumento item)
        {
            int idDocumento = 0;

            try
            {
                Database vDataBase = ObtenerInstancia();

                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Documentos_CrearDocumentoTemporal"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumento", DbType.String, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, item.NombreDocumento );
                    vDataBase.AddInParameter(vDbCommand, "@NombreOriginal", DbType.String, item.NombreOriginal);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, item.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMedioPublicacion", DbType.Int32, item.IdMedioPublicacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPublicacion", DbType.Int32, item.IdTipoPublicacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacion", DbType.DateTime, item.FechaPublicacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, item.UsuarioCrea);

                    var result = vDataBase.ExecuteNonQuery(vDbCommand);
                    idDocumento = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumento").ToString());
                    return idDocumento;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

    }
}
