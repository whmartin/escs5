﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity.Concursales;
using System.Data.SqlClient;

namespace Icbf.SIA.DataAccess.Concursales
{
    public class TipoProcesoNaturalezaDAL : GeneralDAL
    {
        public TipoProcesoNaturalezaDAL()
        { }

        public List<TipoProcesoNaturaleza> ConsultarTiposdeProcesosporNaturaleza(string vTipoProceso, string vNaturaleza, Boolean? vEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_TipoProcesoNaturaleza_ConsultarTiposdeProcesosporNaturaleza"))
                {

                    if (vEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, vEstado);
                    if (!string.IsNullOrEmpty(vTipoProceso))
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, vTipoProceso);
                    if (!string.IsNullOrEmpty(vNaturaleza))
                        vDataBase.AddInParameter(vDbCommand, "@Nombre2", DbType.String, vNaturaleza);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoProcesoNaturaleza> vListaValores = new List<TipoProcesoNaturaleza>();
                        while (vDataReaderResults.Read())
                        {
                            TipoProcesoNaturaleza vParametrica = new TipoProcesoNaturaleza();
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdTipoProcesoNaturaleza = vDataReaderResults["IdTipoProcesoNaturaleza"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoProcesoNaturaleza"].ToString()) : vParametrica.IdTipoProcesoNaturaleza;
                            vParametrica.IdTipoProceso = vDataReaderResults["IdTipoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoProceso"].ToString()) : vParametrica.IdTipoProceso;
                            vParametrica.IdNaturaleza = vDataReaderResults["IdNaturaleza"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNaturaleza"].ToString()) : vParametrica.IdNaturaleza;
                            vParametrica.Nombre2 = vDataReaderResults["Nombre2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre2"].ToString()) : vParametrica.Nombre2;
                            vListaValores.Add(vParametrica);
                        }
                        return vListaValores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        public TipoProcesoNaturaleza ConsultarTipoProcesoNaturaleza(int vIdTipoProcesoNaturaleza)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_TipoProcesoNaturaleza_ConsultarTipoProcesoNaturaleza"))
                {


                    vDataBase.AddInParameter(vDbCommand, "@IdTipoProcesoNaturaleza", DbType.String, vIdTipoProcesoNaturaleza);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        TipoProcesoNaturaleza vParametrica = new TipoProcesoNaturaleza();
                        while (vDataReaderResults.Read())
                        {
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdTipoProcesoNaturaleza = vDataReaderResults["IdTipoProcesoNaturaleza"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoProcesoNaturaleza"].ToString()) : vParametrica.IdTipoProcesoNaturaleza;
                            vParametrica.IdTipoProceso = vDataReaderResults["IdTipoProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoProceso"].ToString()) : vParametrica.IdTipoProceso;
                            vParametrica.IdNaturaleza = vDataReaderResults["IdNaturaleza"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNaturaleza"].ToString()) : vParametrica.IdNaturaleza;
                            vParametrica.Nombre2 = vDataReaderResults["Nombre2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre2"].ToString()) : vParametrica.Nombre2;

                        }
                        return vParametrica;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTipoProcesoNaturaleza(TipoProcesoNaturaleza vIdTipoProcesoNaturaleza)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_TipoProcesoNaturaleza_ModificarTipoProcesoNaturaleza"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoProcesoNaturaleza", DbType.Int32, vIdTipoProcesoNaturaleza.IdTipoProcesoNaturaleza);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoProceso", DbType.Int32, vIdTipoProcesoNaturaleza.IdTipoProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdNaturaleza", DbType.Int32, vIdTipoProcesoNaturaleza.IdNaturaleza);                                        
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, vIdTipoProcesoNaturaleza.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, vIdTipoProcesoNaturaleza.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(vIdTipoProcesoNaturaleza, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarTipoProcesoNaturaleza(TipoProcesoNaturaleza vIdTipoProcesoNaturaleza)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_TipoProcesoNaturaleza_InsertarTipoProcesoNaturaleza"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoProcesoNaturaleza", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoProceso", DbType.Int32, vIdTipoProcesoNaturaleza.IdTipoProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdNaturaleza", DbType.Int32, vIdTipoProcesoNaturaleza.IdNaturaleza);                                       
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, vIdTipoProcesoNaturaleza.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vIdTipoProcesoNaturaleza.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vIdTipoProcesoNaturaleza.IdTipoProcesoNaturaleza = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoProcesoNaturaleza").ToString());
                    GenerarLogAuditoria(vIdTipoProcesoNaturaleza, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarTipoProcesoNaturaleza(TipoProcesoNaturaleza vIdTipoProcesoNaturaleza)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_TipoProcesoNaturaleza_EliminarTipoProcesoNaturaleza"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoProcesoNaturaleza", DbType.Int32, vIdTipoProcesoNaturaleza.IdTipoProcesoNaturaleza);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(vIdTipoProcesoNaturaleza, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
