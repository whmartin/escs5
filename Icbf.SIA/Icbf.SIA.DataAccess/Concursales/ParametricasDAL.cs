﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity.Concursales;
using System.Data.SqlClient;

namespace Icbf.SIA.DataAccess.Concursales
{
    public class ParametricasDAL : GeneralDAL
    {
        public ParametricasDAL()
        { }

        public List<Clasificador> ConsultarParameticasConcursales(Boolean? vEstado, String vNombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Clasificador_ConsultarParametricas"))
                {

                    if (vEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, vEstado);
                    if (!string.IsNullOrEmpty(vNombre))
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, vNombre);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Clasificador> vListaParametricas = new List<Clasificador>();
                        while (vDataReaderResults.Read())
                        {
                            Clasificador vParametrica = new Clasificador();
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdClasificador = vDataReaderResults["IdClasificador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasificador"].ToString()) : vParametrica.IdClasificador;
                            vParametrica.RequiereObservaciones = vDataReaderResults["RequiereObservaciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereObservaciones"].ToString()) : vParametrica.RequiereObservaciones;
                            vParametrica.RequiereAuxiliar1 = vDataReaderResults["RequiereAuxiliar1"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereAuxiliar1"].ToString()) : vParametrica.RequiereAuxiliar1;
                            vParametrica.RequiereAuxiliar2 = vDataReaderResults["RequiereAuxiliar2"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereAuxiliar2"].ToString()) : vParametrica.RequiereAuxiliar2;
                            vParametrica.ParametricaCustom = vDataReaderResults["ParametricaCustom"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ParametricaCustom"].ToString()) : vParametrica.ParametricaCustom;
                            vParametrica.EsCustom = vDataReaderResults["EsCustom"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsCustom"].ToString()) : vParametrica.EsCustom;
                            vListaParametricas.Add(vParametrica);
                        }
                        return vListaParametricas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParametricaPorCodigo(string vCodigo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Valor_ConsultarParameticasPorCodigo"))
                {
                   vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, vCodigo);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Valor> vListaValores = new List<Valor>();
                        while(vDataReaderResults.Read())
                        {
                            Valor vParametrica = new Valor();
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdClasificador = vDataReaderResults["IdClasificador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasificador"].ToString()) : vParametrica.IdClasificador;
                            vParametrica.IdValor = vDataReaderResults["IdValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValor"].ToString()) : vParametrica.IdValor;
                            vParametrica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametrica.Descripcion;
                            vListaValores.Add(vParametrica);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Valor ConsultarParametricaPorCodigo(string vCodigoClasificador, string vNombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Valor_ConsultarParameticasPorCodigoClasificadorNombre"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, vCodigoClasificador);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, vNombre);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Valor vListaValor = null;

                        while(vDataReaderResults.Read())
                        {
                            vListaValor = new Valor();
                            vListaValor.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vListaValor.Nombre;
                            vListaValor.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vListaValor.Estado;
                            vListaValor.IdClasificador = vDataReaderResults["IdClasificador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasificador"].ToString()) : vListaValor.IdClasificador;
                            vListaValor.IdValor = vDataReaderResults["IdValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValor"].ToString()) : vListaValor.IdValor;
                            vListaValor.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vListaValor.Descripcion;
                            vListaValor.Auxiliar1 = vDataReaderResults["Auxiliar1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Auxiliar1"].ToString()) : vListaValor.Auxiliar1;
                        }

                        return vListaValor;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParametricaPorPadre(int vIdPadre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Valor_ConsultarParameticasPorPadre"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPadre", DbType.String, vIdPadre);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Valor> vListaValores = new List<Valor>();
                        while(vDataReaderResults.Read())
                        {
                            Valor vParametrica = new Valor();
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdClasificador = vDataReaderResults["IdClasificador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasificador"].ToString()) : vParametrica.IdClasificador;
                            vParametrica.IdValor = vDataReaderResults["IdValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValor"].ToString()) : vParametrica.IdValor;
                            vParametrica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametrica.Descripcion;
                            vListaValores.Add(vParametrica);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParameticasConcursalesDetalle(Boolean? vEstado, String vNombre, int vIdClasificador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Clasificador_ConsultarParametricasDetalle"))
                {

                    if (vEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, vEstado);
                    if (!string.IsNullOrEmpty(vNombre))
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, vNombre);

                    vDataBase.AddInParameter(vDbCommand, "@IdClasificador", DbType.Int32, vIdClasificador);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Valor> vListaValores = new List<Valor>();
                        while (vDataReaderResults.Read())
                        {
                            Valor vParametrica = new Valor();
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdClasificador = vDataReaderResults["IdClasificador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasificador"].ToString()) : vParametrica.IdClasificador;
                            vParametrica.IdValor = vDataReaderResults["IdValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValor"].ToString()) : vParametrica.IdValor;
                            vParametrica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametrica.Descripcion;
                            vListaValores.Add(vParametrica);
                        }
                        return vListaValores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParameticasNaturalezaTipoProceso(Boolean? vEstado, String vNombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Valor_ConsultarParameticasNaturalezaTipoProceso"))
                {

                    if (vEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, vEstado);
                    if (!string.IsNullOrEmpty(vNombre))
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, vNombre);
                    

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Valor> vListaValores = new List<Valor>();
                        while (vDataReaderResults.Read())
                        {
                            Valor vParametrica = new Valor();
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdClasificador = vDataReaderResults["IdClasificador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasificador"].ToString()) : vParametrica.IdClasificador;
                            vParametrica.IdValor = vDataReaderResults["IdValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValor"].ToString()) : vParametrica.IdValor;
                            vParametrica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametrica.Descripcion;
                            vListaValores.Add(vParametrica);
                        }
                        return vListaValores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Clasificador ConsultarClasificador(int vIdClasificador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Clasificador_ConsultarClasificador"))
                {

                    
                        vDataBase.AddInParameter(vDbCommand, "@IdClasificador", DbType.String, vIdClasificador);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {                        
                        
                            Clasificador vParametrica = new Clasificador();
                        while (vDataReaderResults.Read())
                        {
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdClasificador = vDataReaderResults["IdClasificador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasificador"].ToString()) : vParametrica.IdClasificador;
                            vParametrica.RequiereObservaciones = vDataReaderResults["RequiereObservaciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereObservaciones"].ToString()) : vParametrica.RequiereObservaciones;
                            vParametrica.RequiereAuxiliar1 = vDataReaderResults["RequiereAuxiliar1"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereAuxiliar1"].ToString()) : vParametrica.RequiereAuxiliar1;
                            vParametrica.RequiereAuxiliar2 = vDataReaderResults["RequiereAuxiliar2"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RequiereAuxiliar2"].ToString()) : vParametrica.RequiereAuxiliar2;
                            vParametrica.LabelAuxiliar1 = vDataReaderResults["LabelAuxiliar1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LabelAuxiliar1"].ToString()) : vParametrica.LabelAuxiliar1;
                            vParametrica.LabelAuxiliar2 = vDataReaderResults["LabelAuxiliar2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LabelAuxiliar2"].ToString()) : vParametrica.LabelAuxiliar2;

                        }
                        return vParametrica; 
                        
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Valor ConsultarValorParametrica(int vIdValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Clasificador_ConsultarValor"))
                {


                    vDataBase.AddInParameter(vDbCommand, "@IdValor", DbType.String, vIdValor);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        Valor vParametrica = new Valor();
                        while (vDataReaderResults.Read())
                        {
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Estado;
                            vParametrica.IdClasificador = vDataReaderResults["IdClasificador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasificador"].ToString()) : vParametrica.IdClasificador;
                            vParametrica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametrica.Descripcion;
                            vParametrica.IdValor = vDataReaderResults["IdValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValor"].ToString()) : vParametrica.IdValor;
                            vParametrica.Auxiliar1 = vDataReaderResults["Auxiliar1"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Auxiliar1"].ToString()) : vParametrica.Auxiliar1;
                            vParametrica.Auxiliar2 = vDataReaderResults["Auxiliar2"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Auxiliar2"].ToString()) : vParametrica.Auxiliar2;

                        }
                        return vParametrica;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarValorParametrica(Valor vValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Valor_ModificarValorParametrica"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValor", DbType.Int32, vValor.IdValor);
                    vDataBase.AddInParameter(vDbCommand, "@IdClasificador", DbType.Int32, vValor.IdClasificador);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, vValor.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, vValor.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Auxiliar1", DbType.String, vValor.Auxiliar1);
                    vDataBase.AddInParameter(vDbCommand, "@Auxiliar2", DbType.String, vValor.Auxiliar2);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, vValor.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, vValor.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(vValor, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarValorParametrica(Valor vValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Valor_InsertarValorParametrica"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdValor", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdClasificador", DbType.Int32, vValor.IdClasificador);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, vValor.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, vValor.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Auxiliar1", DbType.String, vValor.Auxiliar1);
                    vDataBase.AddInParameter(vDbCommand, "@Auxiliar2", DbType.String, vValor.Auxiliar2);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, vValor.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vValor.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vValor.IdValor = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdValor").ToString());
                    GenerarLogAuditoria(vValor, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarValorParametrica(Valor vValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Valor_EliminarValorParametrica"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValor", DbType.Int32, vValor.IdValor);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(vValor, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
