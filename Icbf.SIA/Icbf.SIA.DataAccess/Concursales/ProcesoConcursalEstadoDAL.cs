﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity.Concursales;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess.Concursales
{
    public class ProcesoConcursalEstadoDAL : GeneralDAL
    {
        public ProcesoConcursalEstado ConsultarParametricaPorCodigo(string codigo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Estados_ConsultarEstadoPorCodigo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, codigo);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ProcesoConcursalEstado vParametrica = null;

                        while(vDataReaderResults.Read())
                        {
                             vParametrica = new ProcesoConcursalEstado();
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Activo;
                            vParametrica.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vParametrica.IdEstado;
                            vParametrica.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vParametrica.Codigo;
                        }

                        return vParametrica;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursalEstado> ConsultarEstadosOmitir(string vCodigos)
        {
            List<ProcesoConcursalEstado> resultEstados = null;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Estados_ConsultarEstadosOmitir"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Codigos", DbType.String, vCodigos);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        resultEstados = new List<ProcesoConcursalEstado>();
                        ProcesoConcursalEstado vParametrica = null;

                        while(vDataReaderResults.Read())
                        {
                            vParametrica = new ProcesoConcursalEstado();
                            vParametrica.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vParametrica.Nombre;
                            vParametrica.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametrica.Activo;
                            vParametrica.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vParametrica.IdEstado;
                            vParametrica.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vParametrica.Codigo;
                            resultEstados.Add(vParametrica);
                        }

                        return resultEstados;
                    }
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
