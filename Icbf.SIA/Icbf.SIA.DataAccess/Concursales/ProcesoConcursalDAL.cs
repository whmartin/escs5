﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity.Concursales;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess.Concursales
{
    public class ProcesoConcursalDAL : GeneralDAL
    {
        public ProcesoConcursal ObtenerProcesosConcursalPorId(int idProceso)
        {
            ProcesoConcursal vReulst = null;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Proceso_ConsultarProcesoPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, idProceso);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while(vDataReaderResults.Read())
                        {
                             vReulst = new ProcesoConcursal();
                            vReulst.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vReulst.IdProceso;
                            vReulst.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vReulst.IdEstado;
                            vReulst.CapitalInicial = vDataReaderResults["CapitalInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CapitalInicial"].ToString()) : vReulst.CapitalInicial;
                            vReulst.InteresesInicial = vDataReaderResults["InteresesInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["InteresesInicial"].ToString()) : vReulst.InteresesInicial;
                            vReulst.SaldoCapital = vDataReaderResults["SaldoCapital"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoCapital"].ToString()) : vReulst.SaldoCapital;
                            vReulst.SaldoInteres = vDataReaderResults["SaldoInteres"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoInteres"].ToString()) : vReulst.SaldoInteres;
                            vReulst.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vReulst.NombreEstado;
                            vReulst.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vReulst.CodigoEstado;
                            vReulst.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vReulst.RazonSocial;
                            vReulst.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vReulst.NumeroIdentificacion;
                            vReulst.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vReulst.NumeroIdentificacion;
                            vReulst.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vReulst.Regional;
                            vReulst.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Consecutivo"].ToString()) : vReulst.Consecutivo;
                            vReulst.UsuarioAsignado = vDataReaderResults["UsuarioAsignado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioAsignado"].ToString()) : vReulst.UsuarioAsignado;
                            vReulst.RegionalTraslado = vDataReaderResults["RegionalTraslado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegionalTraslado"].ToString()) : vReulst.RegionalTraslado;
                            vReulst.IdRegionalTraslado = vDataReaderResults["IdRegionalTraslado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalTraslado"].ToString()) : vReulst.IdRegionalTraslado;
                            vReulst.IdEstadoOriginal = vDataReaderResults["IdEstadoOriginal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoOriginal"].ToString()) : vReulst.IdEstadoOriginal;
                        }

                        return vReulst;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AsignarUsuarioProceso(int id, string usuario, string usuarioModifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Proceso_AsociarUsuario"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, id);
                    vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, usuario);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String,usuarioModifica );
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursalDocumento> ObtenerDocumentosPorIdProceso(int idProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Proceso_ConsultarDocumentos"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, idProceso);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesoConcursalDocumento> vListaValores = new List<ProcesoConcursalDocumento>();
                        while(vDataReaderResults.Read())
                        {
                            ProcesoConcursalDocumento vReulst = new ProcesoConcursalDocumento();
                            vReulst.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vReulst.IdProceso;
                            vReulst.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vReulst.IdDocumento;
                            vReulst.FechaPublicacion = vDataReaderResults["FechaPublicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacion"].ToString()) : vReulst.FechaPublicacion;
                            vReulst.IdMedioPublicacion = vDataReaderResults["IdMedioPublicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMedioPublicacion"].ToString()) : vReulst.IdMedioPublicacion;
                            vReulst.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vReulst.IdTipoDocumento;
                            vReulst.IdTipoPublicacion = vDataReaderResults["IdTipoPublicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPublicacion"].ToString()) : vReulst.IdTipoPublicacion;
                            vReulst.MedioPublicacion = vDataReaderResults["MedioPublicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MedioPublicacion"].ToString()) : vReulst.MedioPublicacion;
                            vReulst.TipoDocumento = vDataReaderResults["TipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumento"].ToString()) : vReulst.TipoDocumento;
                            vReulst.TipoPublicacion = vDataReaderResults["TipoPublicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPublicacion"].ToString()) : vReulst.TipoPublicacion;
                            vReulst.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReulst.UsuarioCrea;
                            vReulst.FechaCreacion = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReulst.FechaCreacion;
                            vReulst.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vReulst.NombreDocumento;
                            vReulst.NombreOriginal = vDataReaderResults["NombreOriginal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreOriginal"].ToString()) : vReulst.NombreOriginal;

                            vListaValores.Add(vReulst);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursales(int idRegional, string numeroIdentificacion, int? idEstado, string usuarioAsignado, string consecutivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Proceso_ConsultarProcesos"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, idRegional);

                    if(! string.IsNullOrEmpty(numeroIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, numeroIdentificacion);

                    if(idEstado.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, idEstado);

                    if(!string.IsNullOrEmpty(usuarioAsignado))
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAsignado", DbType.String, usuarioAsignado);

                    if(!string.IsNullOrEmpty(consecutivo))
                        vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.String, consecutivo);


                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesoConcursal> vListaValores = new List<ProcesoConcursal>();
                        while(vDataReaderResults.Read())
                        {
                            ProcesoConcursal vReulst = new ProcesoConcursal();
                            vReulst.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vReulst.IdProceso;
                            vReulst.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vReulst.IdEstado;
                            vReulst.CapitalInicial = vDataReaderResults["CapitalInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CapitalInicial"].ToString()) : vReulst.CapitalInicial;
                            vReulst.InteresesInicial = vDataReaderResults["InteresesInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["InteresesInicial"].ToString()) : vReulst.InteresesInicial;
                            vReulst.SaldoCapital = vDataReaderResults["SaldoCapital"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoCapital"].ToString()) : vReulst.SaldoCapital;
                            vReulst.SaldoInteres = vDataReaderResults["SaldoInteres"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoInteres"].ToString()) : vReulst.SaldoInteres;
                            vReulst.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vReulst.NombreEstado;
                            vReulst.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vReulst.CodigoEstado;
                            vReulst.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vReulst.RazonSocial;
                            vReulst.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vReulst.NumeroIdentificacion;
                            vListaValores.Add(vReulst);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesOmitirEstados(int idRegional, string numeroIdentificacion, string usuarioAsignado, string estadosOmitir, string consecutivo, int? vIdEstado = null)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Proceso_ConsultarProcesos_OmitirEstados"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, idRegional);

                    if(!string.IsNullOrEmpty(numeroIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, numeroIdentificacion);

                    if(! string.IsNullOrEmpty(estadosOmitir))
                        vDataBase.AddInParameter(vDbCommand, "@EstadosOmitir", DbType.String, estadosOmitir);

                    if(!string.IsNullOrEmpty(usuarioAsignado))
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAsignado", DbType.String, usuarioAsignado);

                    if(!string.IsNullOrEmpty(consecutivo))
                        vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.String, consecutivo);

                    if(vIdEstado.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, vIdEstado);


                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesoConcursal> vListaValores = new List<ProcesoConcursal>();
                        while(vDataReaderResults.Read())
                        {
                            ProcesoConcursal vReulst = new ProcesoConcursal();
                            vReulst.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vReulst.IdProceso;
                            vReulst.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vReulst.IdEstado;
                            vReulst.CapitalInicial = vDataReaderResults["CapitalInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CapitalInicial"].ToString()) : vReulst.CapitalInicial;
                            vReulst.InteresesInicial = vDataReaderResults["InteresesInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["InteresesInicial"].ToString()) : vReulst.InteresesInicial;
                            vReulst.SaldoCapital = vDataReaderResults["SaldoCapital"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoCapital"].ToString()) : vReulst.SaldoCapital;
                            vReulst.SaldoInteres = vDataReaderResults["SaldoInteres"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoInteres"].ToString()) : vReulst.SaldoInteres;
                            vReulst.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vReulst.NombreEstado;
                            vReulst.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vReulst.CodigoEstado;
                            vReulst.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vReulst.RazonSocial;
                            vReulst.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vReulst.NumeroIdentificacion;
                            vListaValores.Add(vReulst);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesMultipleEstado(int idRegional, string numeroIdentificacion, string usuarioAsignado, string estados, string consecutivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Proceso_ConsultarProcesos_MultiplesEstados"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, idRegional);

                    if(!string.IsNullOrEmpty(numeroIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, numeroIdentificacion);

                    if(!string.IsNullOrEmpty(estados))
                        vDataBase.AddInParameter(vDbCommand, "@Estados", DbType.String, estados);

                    if(!string.IsNullOrEmpty(usuarioAsignado))
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAsignado", DbType.String, usuarioAsignado);

                    if(!string.IsNullOrEmpty(consecutivo))
                        vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.String, consecutivo);
            
                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesoConcursal> vListaValores = new List<ProcesoConcursal>();
                        while(vDataReaderResults.Read())
                        {
                            ProcesoConcursal vReulst = new ProcesoConcursal();
                            vReulst.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vReulst.IdProceso;
                            vReulst.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vReulst.IdEstado;
                            vReulst.CapitalInicial = vDataReaderResults["CapitalInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CapitalInicial"].ToString()) : vReulst.CapitalInicial;
                            vReulst.InteresesInicial = vDataReaderResults["InteresesInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["InteresesInicial"].ToString()) : vReulst.InteresesInicial;
                            vReulst.SaldoCapital = vDataReaderResults["SaldoCapital"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoCapital"].ToString()) : vReulst.SaldoCapital;
                            vReulst.SaldoInteres = vDataReaderResults["SaldoInteres"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoInteres"].ToString()) : vReulst.SaldoInteres;
                            vReulst.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vReulst.NombreEstado;
                            vReulst.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vReulst.CodigoEstado;
                            vReulst.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vReulst.RazonSocial;
                            vReulst.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vReulst.NumeroIdentificacion;
                            vListaValores.Add(vReulst);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesGestionNovedades(int idRegional, string numeroIdentificacion, int? idEstado, string usuarioAsignado, string consecutivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Proceso_ConsultarProcesos_GestionNovedades"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, idRegional);

                    if(!string.IsNullOrEmpty(numeroIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, numeroIdentificacion);

                    if(idEstado.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, idEstado);

                    if(!string.IsNullOrEmpty(usuarioAsignado))
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAsignado", DbType.String, usuarioAsignado);

                    if(!string.IsNullOrEmpty(consecutivo))
                        vDataBase.AddInParameter(vDbCommand, "@Consecutivo", DbType.String, consecutivo);


                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesoConcursal> vListaValores = new List<ProcesoConcursal>();
                        while(vDataReaderResults.Read())
                        {
                            ProcesoConcursal vReulst = new ProcesoConcursal();
                            vReulst.IdProceso = vDataReaderResults["IdProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProceso"].ToString()) : vReulst.IdProceso;
                            vReulst.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vReulst.IdEstado;
                            vReulst.CapitalInicial = vDataReaderResults["CapitalInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CapitalInicial"].ToString()) : vReulst.CapitalInicial;
                            vReulst.InteresesInicial = vDataReaderResults["InteresesInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["InteresesInicial"].ToString()) : vReulst.InteresesInicial;
                            vReulst.SaldoCapital = vDataReaderResults["SaldoCapital"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoCapital"].ToString()) : vReulst.SaldoCapital;
                            vReulst.SaldoInteres = vDataReaderResults["SaldoInteres"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["SaldoInteres"].ToString()) : vReulst.SaldoInteres;
                            vReulst.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vReulst.NombreEstado;
                            vReulst.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vReulst.CodigoEstado;
                            vReulst.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vReulst.RazonSocial;
                            vReulst.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vReulst.NumeroIdentificacion;
                            vListaValores.Add(vReulst);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int IngresarNovedad(ProcesoConcursalNovedad item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_Novedades_Ingresar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, item.IdProceso);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioNovedad", DbType.String, item.UsuarioNovedad);
                    vDataBase.AddInParameter(vDbCommand, "@Accion", DbType.String, item.Accion);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, item.Observaciones);

                    if(item.IdRegionalTraslado.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, item.IdRegionalTraslado.Value);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int IngresarGestionNovedad(ProcesoConcursalGestionNovedad item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_NovedadesGestion_Ingresar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, item.IdProceso);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioNovedad", DbType.String, item.UsuarioNovedad);
                    vDataBase.AddInParameter(vDbCommand, "@Accion", DbType.String, item.Accion);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, item.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, item.Estado);

                    if(! string.IsNullOrEmpty(item.UsuarioAsignar))
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioAsignar", DbType.String, item.UsuarioAsignar);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursalGestion> ObtenerGestionProcesoPorIdProceso(int idProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using(DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Concursales_ProcesoGestion_ConsultarPorIdProceso"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProceso", DbType.Int32, idProceso);

                    using(IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProcesoConcursalGestion> vListaValores = new List<ProcesoConcursalGestion>();
                        while(vDataReaderResults.Read())
                        {
                            ProcesoConcursalGestion vReulst = new ProcesoConcursalGestion();
                            vReulst.IdProcesoConcursal = vDataReaderResults["IdProcesoConcursal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProcesoConcursal"].ToString()) : vReulst.IdProcesoConcursal;
                            vReulst.IdProcesoConcursalGestion = vDataReaderResults["IdProcesoConcursalGestion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProcesoConcursalGestion"].ToString()) : vReulst.IdProcesoConcursalGestion;
                            vReulst.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : vReulst.Fecha;
                            vReulst.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vReulst.Estado;
                            vReulst.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vReulst.IdEstado;
                            vReulst.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vReulst.Observaciones;
                            vReulst.Usuario = vDataReaderResults["Usuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Usuario"].ToString()) : vReulst.Usuario;

                            vListaValores.Add(vReulst);
                        }
                        return vListaValores;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
