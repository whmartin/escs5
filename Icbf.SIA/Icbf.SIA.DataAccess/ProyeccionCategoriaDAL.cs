using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Xml.Linq;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad ProyeccionCategoria
    /// </summary>
    public class ProyeccionCategoriaDAL : GeneralDAL
    {
        public ProyeccionCategoriaDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pProyeccionCategoria"></param>
        public int InsertarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionCategoria_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDProyeccionCategoria", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDProyeccion", DbType.Int32, pProyeccionCategoria.IdProyeccionPresupuestos);
                    vDataBase.AddInParameter(vDbCommand, "@IDCategoria", DbType.Int32, pProyeccionCategoria.IDCategoria);
                    vDataBase.AddInParameter(vDbCommand, "@Cantidad", DbType.Int32, pProyeccionCategoria.Cantidad);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pProyeccionCategoria.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValor", DbType.Int32, pProyeccionCategoria.IdCategoriaValor);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProyeccionCategoria.IDProyeccionCategoria = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDProyeccionCategoria").ToString());
                    GenerarLogAuditoria(pProyeccionCategoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pProyeccionCategoria"></param>
        public int ModificarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionCategoria_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDProyeccionCategoria", DbType.Int32, pProyeccionCategoria.IDProyeccionCategoria);
                    vDataBase.AddInParameter(vDbCommand, "@IDProyeccion", DbType.Int32, pProyeccionCategoria.IDProyeccion);
                    vDataBase.AddInParameter(vDbCommand, "@IDCategoria", DbType.Int32, pProyeccionCategoria.IDCategoria);
                    vDataBase.AddInParameter(vDbCommand, "@IdCategoriaValor", DbType.Int32, pProyeccionCategoria.IdCategoriaValor);
                    vDataBase.AddInParameter(vDbCommand, "@Cantidad", DbType.Int32, pProyeccionCategoria.Cantidad);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProyeccionCategoria.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProyeccionCategoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pProyeccionCategoria"></param>
        public int EliminarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionCategoria_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDProyeccionCategoria", DbType.Int32, pProyeccionCategoria.IDProyeccionCategoria);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProyeccionCategoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pIDProyeccionCategoria"></param>
        public ProyeccionCategoria ConsultarProyeccionCategoria(int? pIDProyeccionCategoria, int? pIdProyeccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionCategoria_Consultar"))
                {
                    if (pIDProyeccionCategoria != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDProyeccionCategoria", DbType.Int32, pIDProyeccionCategoria);

                    if (pIdProyeccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProyeccion", DbType.Int32, pIdProyeccion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ProyeccionCategoria vProyeccionCategoria = new ProyeccionCategoria();
                        while (vDataReaderResults.Read())
                        {
                            vProyeccionCategoria.IDProyeccionCategoria = vDataReaderResults["IDProyeccionCategoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProyeccionCategoria"].ToString()) : vProyeccionCategoria.IDProyeccionCategoria;
                            vProyeccionCategoria.IDProyeccion = vDataReaderResults["IDProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProyeccion"].ToString()) : vProyeccionCategoria.IDProyeccion;
                            vProyeccionCategoria.IDCategoria = vDataReaderResults["IDCategoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCategoria"].ToString()) : vProyeccionCategoria.IDCategoria;
                            vProyeccionCategoria.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vProyeccionCategoria.Descripcion;
                            vProyeccionCategoria.Cantidad = vDataReaderResults["Cantidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cantidad"].ToString()) : vProyeccionCategoria.Cantidad;
                            vProyeccionCategoria.IdCategoriaValor = vDataReaderResults["IdCategoriaValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValor"].ToString()) : vProyeccionCategoria.IdCategoriaValor;
                            vProyeccionCategoria.Nivel = vDataReaderResults["Nivel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Nivel"].ToString()) : vProyeccionCategoria.Nivel;
                            vProyeccionCategoria.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProyeccionCategoria.UsuarioCrea;
                            vProyeccionCategoria.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProyeccionCategoria.FechaCrea;
                            vProyeccionCategoria.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProyeccionCategoria.UsuarioModifica;
                            vProyeccionCategoria.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProyeccionCategoria.FechaModifica;
                        }
                        return vProyeccionCategoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pCantidad"></param>
        public List<ProyeccionCategoria> ConsultarProyeccionCategorias(int? pIdProyeccion, String pCodEstadoProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionCategorias_Consultar"))
                {
                    if (pIdProyeccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@pIdProyeccion", DbType.Int32, pIdProyeccion);
                    if (pCodEstadoProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@pCodEstadoProceso", DbType.String, pCodEstadoProceso);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProyeccionCategoria> vListaProyeccionCategoria = new List<ProyeccionCategoria>();
                        while (vDataReaderResults.Read())
                        {
                            ProyeccionCategoria vProyeccionCategoria = new ProyeccionCategoria();
                            vProyeccionCategoria.IDProyeccionCategoria = vDataReaderResults["IDProyeccionCategoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProyeccionCategoria"].ToString()) : vProyeccionCategoria.IDProyeccionCategoria;
                            vProyeccionCategoria.IDProyeccion = vDataReaderResults["IDProyeccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDProyeccion"].ToString()) : vProyeccionCategoria.IDProyeccion;
                            vProyeccionCategoria.IDCategoria = vDataReaderResults["IDCategoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDCategoria"].ToString()) : vProyeccionCategoria.IDCategoria;
                            vProyeccionCategoria.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vProyeccionCategoria.Descripcion;
                            vProyeccionCategoria.Cantidad = vDataReaderResults["Cantidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cantidad"].ToString()) : vProyeccionCategoria.Cantidad;
                            vProyeccionCategoria.CantidadAsignada = vDataReaderResults["CantidadAsignada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadAsignada"].ToString()) : vProyeccionCategoria.CantidadAsignada;
                            vProyeccionCategoria.CantidadDisponible = vDataReaderResults["CantidadDisponible"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadDisponible"].ToString()) : vProyeccionCategoria.CantidadDisponible;
                            vProyeccionCategoria.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vProyeccionCategoria.NombreRegional;
                            vProyeccionCategoria.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArea"].ToString()) : vProyeccionCategoria.NombreArea;
                            vProyeccionCategoria.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vProyeccionCategoria.UsuarioCrea;
                            vProyeccionCategoria.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vProyeccionCategoria.FechaCrea;
                            vProyeccionCategoria.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vProyeccionCategoria.UsuarioModifica;
                            vProyeccionCategoria.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vProyeccionCategoria.FechaModifica;
                            vProyeccionCategoria.IdCategoriaValor = vDataReaderResults["IdCategoriaValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCategoriaValor"].ToString()) : vProyeccionCategoria.IdCategoriaValor;
                            vProyeccionCategoria.Nivel = vDataReaderResults["Nivel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Nivel"].ToString()) : vProyeccionCategoria.Nivel;
                            vListaProyeccionCategoria.Add(vProyeccionCategoria);
                        }
                        return vListaProyeccionCategoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionAprobacionCategoria_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProyeccionCategoria", DbType.Int32, pProyeccionCategoria.IDProyeccionCategoria);
                    vDataBase.AddInParameter(vDbCommand, "@Aprobado", DbType.Boolean, pProyeccionCategoria.Aprobado);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionRechazo", DbType.String, pProyeccionCategoria.DescripcionRechazo);
                    vDataBase.AddInParameter(vDbCommand, "@Cantidad", DbType.Int32, pProyeccionCategoria.Cantidad);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadAprobada", DbType.Int32, pProyeccionCategoria.CantidadAprobada);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProyeccionCategoria.UsuarioModifica);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProyeccionCategoria.IdProyeccionPresupuestos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProyeccionCategoria").ToString());
                    GenerarLogAuditoria(pProyeccionCategoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionCategorias(List<ProyeccionCategoria> pProyeccionCategoria, string pUsuarioModifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_ProyeccionAprobacionCategorias_XML"))
                {
                    int vResultado;

                    var xml = new XElement("DatosProyeccionCategoria",
                                              pProyeccionCategoria.Select(i => new XElement("ProyeccionCategoria",
                                              new XAttribute("IDProyeccionCategoria", i.IDProyeccionCategoria),
                                               new XAttribute("CodEstado", i.CodEstadoProceso),
                                               new XAttribute("Cantidad", i.Cantidad),
                                               new XAttribute("CantidadAprobada", i.CantidadAprobada),
                                               new XAttribute("Aprobado", i.Aprobado),
                                               new XAttribute("DescripcionRechazo", i.DescripcionRechazo)
                                               )));

                    vDataBase.AddInParameter(vDbCommand, "@ProyeccionCategoriaXml", DbType.String, xml.ToString());
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pUsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}