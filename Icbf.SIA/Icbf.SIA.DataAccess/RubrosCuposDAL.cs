using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad RubrosCupos
    /// </summary>
    public class RubrosCuposDAL : GeneralDAL
    {
        public RubrosCuposDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad RubrosCupos
        /// </summary>
        /// <param name="pRubrosCupos"></param>
        public int InsertarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_RubrosCupos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRubroCupos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRubro", DbType.String, pRubrosCupos.CodigoRubro);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionRubro", DbType.String, pRubrosCupos.DescripcionRubro);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRubrosCupos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRubrosCupos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRubrosCupos.IdRubroCupos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRubroCupos").ToString());
                    GenerarLogAuditoria(pRubrosCupos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RubrosCupos
        /// </summary>
        /// <param name="pRubrosCupos"></param>
        public int ModificarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_RubrosCupos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRubroCupos", DbType.Int32, pRubrosCupos.IdRubroCupos);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRubro", DbType.String, pRubrosCupos.CodigoRubro);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionRubro", DbType.String, pRubrosCupos.DescripcionRubro);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRubrosCupos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRubrosCupos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRubrosCupos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RubrosCupos
        /// </summary>
        /// <param name="pRubrosCupos"></param>
        public int EliminarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_RubrosCupos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRubroCupos", DbType.Int32, pRubrosCupos.IdRubroCupos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRubrosCupos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad RubrosCupos
        /// </summary>
        /// <param name="pIdRubroCupos"></param>
        public RubrosCupos ConsultarRubrosCupos(int pIdRubroCupos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_RubrosCupos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRubroCupos", DbType.Int32, pIdRubroCupos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RubrosCupos vRubrosCupos = new RubrosCupos();
                        while (vDataReaderResults.Read())
                        {
                            vRubrosCupos.IdRubroCupos = vDataReaderResults["IdRubroCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRubroCupos"].ToString()) : vRubrosCupos.IdRubroCupos;
                            vRubrosCupos.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vRubrosCupos.CodigoRubro;
                            vRubrosCupos.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vRubrosCupos.DescripcionRubro;
                            vRubrosCupos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRubrosCupos.Estado;
                            vRubrosCupos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRubrosCupos.UsuarioCrea;
                            vRubrosCupos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRubrosCupos.FechaCrea;
                            vRubrosCupos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRubrosCupos.UsuarioModifica;
                            vRubrosCupos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRubrosCupos.FechaModifica;
                        }
                        return vRubrosCupos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RubrosCupos
        /// </summary>
        /// <param name="pCodigoRubro"></param>
        /// <param name="pDescripcionRubro"></param>
        /// <param name="pEstado"></param>
        public List<RubrosCupos> ConsultarRubrosCuposs(String pCodigoRubro, String pDescripcionRubro, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CUP_RubrosCuposs_Consultar"))
                {
                    if(pCodigoRubro != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoRubro", DbType.String, pCodigoRubro);
                    if(pDescripcionRubro != null)
                         vDataBase.AddInParameter(vDbCommand, "@DescripcionRubro", DbType.String, pDescripcionRubro);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RubrosCupos> vListaRubrosCupos = new List<RubrosCupos>();
                        while (vDataReaderResults.Read())
                        {
                                RubrosCupos vRubrosCupos = new RubrosCupos();
                            vRubrosCupos.IdRubroCupos = vDataReaderResults["IdRubroCupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRubroCupos"].ToString()) : vRubrosCupos.IdRubroCupos;
                            vRubrosCupos.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vRubrosCupos.CodigoRubro;
                            vRubrosCupos.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vRubrosCupos.DescripcionRubro;
                            vRubrosCupos.DescripcionRubroCompleto = vDataReaderResults["DescripcionRubroCompleto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubroCompleto"].ToString()) : vRubrosCupos.DescripcionRubroCompleto;
                            vRubrosCupos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRubrosCupos.Estado;
                            vRubrosCupos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRubrosCupos.UsuarioCrea;
                            vRubrosCupos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRubrosCupos.FechaCrea;
                            vRubrosCupos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRubrosCupos.UsuarioModifica;
                            vRubrosCupos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRubrosCupos.FechaModifica;
                                vListaRubrosCupos.Add(vRubrosCupos);
                        }
                        return vListaRubrosCupos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
