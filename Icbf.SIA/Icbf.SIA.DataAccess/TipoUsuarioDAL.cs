﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.SIA.DataAccess
{

    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, 
    /// entre otros para TipoUsuario
    /// </summary>
    public class TipoUsuarioDAL : GeneralDAL
    {
        public TipoUsuarioDAL()
        {
        }
        /// <summary>
        /// Gonet 
        /// Permite insertar un tipo de usuario
        ///     -Genera auditoria
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pTipoUsuario">Instancia con la información del tipo a insertar</param>
        /// <returns>Identificador de base de datos de la información insertada</returns>
        public int InsertarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoUsuario", DbType.String, pTipoUsuario.CodigoTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoUsuario", DbType.String, pTipoUsuario.NombreTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pTipoUsuario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoUsuario.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoUsuario.IdTipoUsuario = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoUsuario").ToString());

                    GenerarLogAuditoria(pTipoUsuario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Permite modificar un tipo de usuario
        ///     -Genera auditoria
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pTipoUsuario">Instancia con la información a modificar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int ModificarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_Modificar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pTipoUsuario.IdTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoUsuario", DbType.String, pTipoUsuario.CodigoTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoUsuario", DbType.String, pTipoUsuario.NombreTipoUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pTipoUsuario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoUsuario.UsuarioModifica);
                    GenerarLogAuditoria(pTipoUsuario, vDbCommand);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Permite eliminar un tipo de usuario
        ///     -Genera auditoria
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pIdTipoUsuario">Instancia con la información a eliminar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int EliminarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_Eliminar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pIdTipoUsuario);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Consulta y recupera la información de tipo de usuario a partir del identificador de base de datos
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pIdTipoUsuario">Entero con el identificador del tipo</param>
        /// <returns>Instancia que contiene la información recuperada de la base de datos</returns>
        public TipoUsuario ConsultarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoUsuario", DbType.Int32, pIdTipoUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoUsuario vTipoUsuario = new TipoUsuario();
                        while (vDataReaderResults.Read())
                        {
                            vTipoUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vTipoUsuario.IdTipoUsuario;
                            vTipoUsuario.CodigoTipoUsuario = vDataReaderResults["CodigoTipoUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoUsuario"].ToString()) : vTipoUsuario.CodigoTipoUsuario;
                            vTipoUsuario.NombreTipoUsuario = vDataReaderResults["NombreTipoUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoUsuario"].ToString()) : vTipoUsuario.NombreTipoUsuario;
                            vTipoUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vTipoUsuario.Estado;
                        }
                        return vTipoUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consulta y recupera la información de los tipos de usuario a partir de los filtros dados
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pCodigoTipoUsuario">Cadena de texto con el codigo de tipo de usuario</param>
        /// <param name="pNombreTipoUsuario">Cadena de texto con el nombre de tipo de usuario</param>
        /// <param name="pEstado">Cadena de texto con el estado</param>
        /// <returns>Lista con las instancias que contiene la información, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<TipoUsuario> ConsultarTipoUsuarios(String pCodigoTipoUsuario, String pNombreTipoUsuario, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_TipoUsuario_sConsultar"))
                {
                    if (pCodigoTipoUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoTipoUsuario", DbType.String, pCodigoTipoUsuario);
                    if (pNombreTipoUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreTipoUsuario", DbType.String, pNombreTipoUsuario);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoUsuario> vListaTipoUsuario = new List<TipoUsuario>();
                        while (vDataReaderResults.Read())
                        {
                            TipoUsuario vTipoUsuario = new TipoUsuario();
                            vTipoUsuario.IdTipoUsuario = vDataReaderResults["IdTipoUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoUsuario"].ToString()) : vTipoUsuario.IdTipoUsuario;
                            vTipoUsuario.CodigoTipoUsuario = vDataReaderResults["CodigoTipoUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoUsuario"].ToString()) : vTipoUsuario.CodigoTipoUsuario;
                            vTipoUsuario.NombreTipoUsuario = vDataReaderResults["NombreTipoUsuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoUsuario"].ToString()) : vTipoUsuario.NombreTipoUsuario;
                            vTipoUsuario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vTipoUsuario.Estado;
                            vListaTipoUsuario.Add(vTipoUsuario);
                        }
                        return vListaTipoUsuario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
