using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIGUV.Entity
{
    public class ContratoSiguvVehiculos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdContratoSiguvVehiculos
        {
            get;
            set;
        }
        public int IdContratoSiguv
        {
            get;
            set;
        }
        public int IdVehiculo
        {
            get;
            set;
        }
        public int IdRegionalPCI
        {
            get;
            set;
        }
        public int IdCentroZonal
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public ContratoSiguvVehiculos()
        {
        }
    }
}
