using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIGUV.Entity
{
    public class Relevo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRelevo
        {
            get;
            set;
        }
        public int IdContratoSiguv
        {
            get;
            set;
        }
        public int IdVehiculoPrincipal
        {
            get;
            set;
        }
        public int IdVehiculoReemplazo
        {
            get;
            set;
        }
        public DateTime FechaInicio
        {
            get;
            set;
        }
        public DateTime FechaFin
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Relevo()
        {
        }
    }
}
