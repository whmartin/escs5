using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIGUV.Entity
{
    public class Vehiculos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdVehiculo
        {
            get;
            set;
        }
        public int Propio
        {
            get;
            set;
        }
        public int Relevo
        {
            get;
            set;
        }
        public int IdClase
        {
            get;
            set;
        }
        public String Placa
        {
            get;
            set;
        }
        public int Cilindraje
        {
            get;
            set;
        }
        public int Modelo
        {
            get;
            set;
        }
        public int Capacidad
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Vehiculos()
        {
        }
    }
}
