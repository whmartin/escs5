using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIGUV.Entity
{
    public class MacroRegionalRegionalPCI : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdMacroRegionalRegionalPCI
        {
            get;
            set;
        }
        public int IdMacroRegional
        {
            get;
            set;
        }
        public int IdRegionalPCI
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public MacroRegionalRegionalPCI()
        {
        }
    }
}
