﻿using Icbf.Contrato.Business.PreContractual;
using Icbf.Contrato.Bussines.PreContractual;
using Icbf.Contrato.DataAccess.PreContractual;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Service
{
    public class PreContractualService
    {
        public PreContractualService(){ }

        #region Solicitudes de Contrato

        public int InsertarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                return  new SolicitudContratoBLL().InsertarSolicitudContrato(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                return new SolicitudContratoBLL().ModificarSolicitudContrato(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                return new SolicitudContratoBLL().EliminarSolicitudContrato(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolicitudContrato ConsultarSolicitudPorId(int idSolicitudContrato)
        {
            try
            {
                return new SolicitudContratoBLL().ConsultarPorId(idSolicitudContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContrato> ConsultarSolicitudContrato(DateTime? vFechaRegistroSistemaDesde, DateTime? vFechaRegistroSistemaHasta, int? vIdSolicitudContrato, int? vVigenciaFiscalinicial, int? vIDRegional, int? vIDModalidadSeleccion, int? vIDCategoriaContrato, int? vIDTipoContrato, int? vIDEstadoSolicitud, string usuarioArea, string usuarioRevision)
        {
            try
            {
                return new SolicitudContratoBLL().ConsultarSolicitudContrato(vFechaRegistroSistemaDesde,vFechaRegistroSistemaHasta,vIdSolicitudContrato,vVigenciaFiscalinicial,vIDRegional,vIDModalidadSeleccion,vIDCategoriaContrato,vIDTipoContrato,vIDEstadoSolicitud,usuarioArea, usuarioRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoEstado> ConsultarEstados()
        {
            try
            {
                return new SolicitudContratoBLL().ConsultarEstados();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoGestion> ConsultarGestionSolicitud(int idSolicitudContrato)
        {
            try
            {
                return new SolicitudContratoBLL().ConsultarGestionSolicitud(idSolicitudContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public Dictionary<string,string> ValidarSolicitudContrato(int idSolicitud)
        {
            try
            {
                return new SolicitudContratoBLL().ValidarSolicitudContrato(idSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoSolicitud(SolicitudContratoCambioEstado solCambioEstado)
        {
            try
            {
                return new SolicitudContratoBLL().CambiarEstadoSolicitud(solCambioEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoDocumentos> ConsultarDocumentosPorIdSolicitud(int idSolicitud)
        {
            try
            {
                return new SolicitudContratoBLL().ConsultarDocumentosPorIdSolicitud(idSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSolicitudContratoDocumentos(int idSolicitudDocumento)
        {
            try
            {
                return new SolicitudContratoBLL().EliminarSolicitudContratoDocumentos(idSolicitudDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarSolicitudContratoDocumentos(SolicitudContratoDocumentos item)
        {
            try
            {
                return new SolicitudContratoBLL().InsertarSolicitudContratoDocumentos(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarDocumentosFaltantesPorCrear(int idSolicitud, int idModalidadSeleccion)
        {
            try
            {
                return new SolicitudContratoBLL().ConsultarDocumentosFaltantesPorCrear(idSolicitud, idModalidadSeleccion); 
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion 

        #region Actividades 

        public int InsertarActividad(SolicitudContratoActividad pActividad)
        {
            try
            {
                return new ActividadesBLL().InsertarActividad(pActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividad(SolicitudContratoActividad pActividad)
        {
            try
            {
                return new ActividadesBLL().ModificarActividad(pActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividadDocumentos(int idActividad, string TiposDocumentos)
        {
            try
            {
                return new ActividadesBLL().ModificarActividadDocumentos(idActividad,TiposDocumentos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividadSubActividadDocumentos(int idActividad, string TiposDocumentos, string subActividades)
        {
            try
            {
                return new ActividadesBLL().ModificarActividadSubActividadDocumentos(idActividad, TiposDocumentos, subActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarActividad(SolicitudContratoActividad pActividad)
        {
            try
            {
                return new ActividadesBLL().EliminarActividad(pActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolicitudContratoActividad ConsultarActividadPorId(int pIdSolicitudContratoActividades)
        {
            try
            {
                return new ActividadesBLL().ConsultarActividadPorId(pIdSolicitudContratoActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarActividades(int? pIdModalidadSeleccion, String pNombre, int ? pTipoDocumento ,bool ? pEstado, bool pEsPadre)
        {
            try
            {
                return new ActividadesBLL().ConsultarActividades(pIdModalidadSeleccion, pNombre, pTipoDocumento, pEstado, pEsPadre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarSubActividadesPorActividad(int pIdActividad)
        {
            try
            {
                return new ActividadesBLL().ConsultarSubActividadesPorActividad(pIdActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarSubActividadesPosiblesPorActividad(int pIdActividad)
        {
            try
            {
                return new ActividadesBLL().ConsultarSubActividadesPosiblesPorActividad(pIdActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarModalidadSeleccionPorIdActividad(int pIdActividad)
        {
            try
            {
                return new ActividadesBLL().ConsultarModalidadSeleccionPorIdActividad(pIdActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Tipos de Documentos

        public int InsertarTipoDocumento(TiposDocumento pTipoDocumento)
        {
            try
            {
                return new TiposDocumentosBLL().InsertarTipoDocumento(pTipoDocumento);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTipoDocumento(TiposDocumento pTipoDocumento)
        {
            try
            {
                return new TiposDocumentosBLL().ModificarTipoDocumento(pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TiposDocumento ConsultarTipoDocumentoPorId(int pIdTipoDocumento)
        {
            try
            {
                return new TiposDocumentosBLL().ConsultarTipoDocumentoPorId(pIdTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTiposDocumentos(int? pIdModalidadSeleccion, string pNombre, bool ? estado)
        {
            try
            {
                return new TiposDocumentosBLL().ConsultarTiposDocumentos(pIdModalidadSeleccion, pNombre, estado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTipoDocumentoPorActividad(int idActividad)
        {
            try
            {
                return new TiposDocumentosBLL().ConsultarTipoDocumentoPorActividad(idActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTipoDocumentoPosiblesPorActividad(int idActividad)
        {
            try
            {
                return new TiposDocumentosBLL().ConsultarTipoDocumentoPosiblesPorActividad(idActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarModalidadSeleccionPorIdTipoDocumento(int pIdTipoDocumento)
        {
            try
            {
                return new TiposDocumentosBLL().ConsultarModalidadSeleccionPorIdTipoDocumento(pIdTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Desarrollo Actividades

        public List<SolicitudContratoActividadesDesarrollo> ConsultarDesarrolloActividadPorId(int idSolicitud)
        {
            try
            {
                return new DesarrolloActividadBLL().ConsultarDesarrolloActividadPorId(idSolicitud);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividadesDesarrollo> ConsultarDesarrolloActividadPorIdActividad(int idSolicitud, int idActividad)
        {
            try
            {
                return new DesarrolloActividadBLL().ConsultarDesarrolloActividadPorIdActividad(idSolicitud,idActividad);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolicitudContratoActividadesDesarrolloDetalle ConsultarDesarrolloActividadPorId(int idSolicitudContrato, int idActividad, int? idActividadPadre)
        {
            try
            {
                return new DesarrolloActividadBLL().ConsultarDesarrolloActividadPorId(idSolicitudContrato, idActividad, idActividadPadre);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDesarrolloActividad(SolicitudContratoActividadesDesarrolloDetalle item)
        {
            try
            {
                return new DesarrolloActividadBLL().InsertarDesarrolloActividad(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDesarrolloActividad(SolicitudContratoActividadesDesarrolloDetalle item)
        {
            try
            {
                return new DesarrolloActividadBLL().ModificarDesarrolloActividad(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int FinalizarGestionActividad(int idDesarrolloActividad)
        {
            try
            {
                return new DesarrolloActividadBLL().FinalizarGestionActividad(idDesarrolloActividad);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDesarrolloActividadDocumentos(SolicitudContratoActividadesDesarrolloDocumentos item)
        {
            try
            {
                return new DesarrolloActividadBLL().InsertarDesarrolloActividadDocumentos(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSDesarrolloActividadDocumentos(int idDesarrolloActividadDocumento)
        {
            try
            {
                return new DesarrolloActividadBLL().EliminarSDesarrolloActividadDocumentos(idDesarrolloActividadDocumento);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividadesDesarrolloDocumentos> ConsultarDocumentosPorIdDesarrolloActividad(int idDesarrolloActividad)
        {
            try
            {
                return new DesarrolloActividadBLL().ConsultarDocumentosPorIdDesarrolloActividad(idDesarrolloActividad);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Parametros Precontractual


        public ParametrosModuloPrecontractual ConsultarParametro(string Nombre)
        {
            try
            {
                return new ParametrosModuloPrecontractualBLL().ConsultarParametro(Nombre);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}
