﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Business;
using Icbf.Contrato.Entity;
using Icbf.Oferente.Entity;
using Icbf.SIA.Business;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;
using TablaParametrica = Icbf.Contrato.Entity.TablaParametrica;
using System.IO;
using ICBF.MasterData.Entity;
using Icbf.SIA.Entity.Concursales;

namespace Icbf.Contrato.Service
{
    /// <summary> 
    /// Clase que contiene todos los métodos de acceso a las entidades de contratos para uso desde página web o web service
    /// </summary>
    public class ContratoService
    {
        private CategoriaContratoBLL vCategoriaContratoBLL;
        private ClausulaContratoBLL vClausulaContratoBLL;
        private Icbf.Contrato.Business.TipoContratoBLL vTipoContratoBLL;
        private TipoClausulaBLL vTipoClausulaBLL;
        private ObligacionBLL vObligacionBLL;
        private TipoAmparoBLL vTipoAmparoBLL;
        private TipoObligacionBLL vTipoObligacionBLL;
        private TipoGarantiaBLL vTipoGarantiaBLL;
        private FormaPagoBLL vFormaPagoBLL;
        private RegimenContratacionBLL vRegimenContratacionBLL;
        private ModalidadAcademicaKactusBLL vModalidadAcademicaKactusBLL;
        private ModalidadSeleccionBLL vModalidadSeleccionBLL;
        private TipoSupvInterventorBLL vTipoSupvInterventorBLL;
        private SecuenciaNumeroContratoBLL vSecuenciaNumeroContratoBLL;
        private SecuenciaNumeroProcesoBLL vSecuenciaNumeroProcesoBLL;
        private NumeroProcesosBLL vNumeroProcesosBLL;
        // private DocumentosLiquidacionBLL vDocumentoLiquidacion;
        private ObjetoContratoBLL vObjetoContratoBLL;

        private GestionarClausulasContratoBLL vGestionarClausulasContratoBLL;
        private CesionesBLL vCesionesBLL;
        private ConsultarSupervisorInterventorBLL vConsultarSupervisorInterventorBLL;
        private RelacionarSupervisorInterventorBLL vRelacionarSupervisorInterventorBLL;
        private Icbf.Contrato.Business.ContratoBLL vContratoBLL;
        private GestionarObligacionBLL vGestionarObligacionBLL;
        private TablaParametricaBLL vTablaParametricaBLL;
        private LugarEjecucionContratoBLL vLugarEjecucionContratoBLL;
        private LugarEjecucionBLL vLugarEjecucionBLL;
        private UnidadMedidaBLL vUnidadMedidaBLL;
        private RegionalBLL vRegionalTipoContrato;
        private InformacionPresupuestalBLL vInformacionPresupuestalBLL;
        private InformacionPresupuestalRPBLL vInformacionPresupuestalRP;
        private RelacionarContratistasBLL vRelacionarContratistas;
        private RelacionContratoBLL vRelacionContratoBLL;
        private PreguntaBLL vPreguntaBLL;
        private Proveedores_ContratistaBLL vProveedores_ContratistaBLL;
        private Proveedores_ContratosBLL vProveedores_ContratosBLL;
        private ProveedoresContratosBLL vProveedoresContratosBLL;
        private RegistroInformacionPresupuestalBLL vRegistroInformacionPresupuestalBLL;
        private ContratosCDPBLL vContratosCDPBLL;
        private EstadoSolcitudModPlanComprasBLL vEstadoSolcitudModPlanComprasBLL;
        private PlanDeComprasBLL vPlanDeComprasBLL;
        private SolicitudModPlanComprasBLL vSolicitudModPlanComprasBLL;
        private PlanDeComprasContratosBLL vPlanDeComprasContratosBLL;
        private AporteContratoBLL vAporteContratoBLL;
        private EmpleadoBLL vEmpleadoBLL;
        private ConsecutivoContratoRegionalesBLL vConsecutivoContratoRegionalesBLL;
        private TipoSuperInterBLL vTipoSuperInterBLL;
        private SupervisorInterContratoBLL vSupervisorInterContratoBLL;
        private SubComponenteBLL vSubComponenteBLL;
        private AmparosGarantiasBLL vAmparosGarantiasBLL;
        private SupervisorInterventorBLL vSupervisorInterventorBLL;
        private SupervisorDatosBLL vSupervisorDatosBLL;



        private UnidadCalculoBLL vUnidadCalculoBLL;
        private GarantiaBLL vGarantiaBLL;
        private ConsultarSuperInterContratoBLL vConsultarSuperInterContratoBLL;
        private ProfesionKactusBLL vProfesionKactusBLL;
        private ArchivosGarantiasBLL vArchivosGarantiasBLL;
        private SucursalAseguradoraContratoBLL vSucursalAseguradoraContratoBLL;
        private ContratistaGarantiasBLL vContratistaGarantiasBLL;
        private TipoContratoAsociadoBLL vTipoContratoAsociadoBLL;
        private SuscripcionContratoBLL vSuscripcionContratoBLL;
        private SuspensionesBLL vSuspensionesBLL;

        private PlanComprasContratosBll vPlanComprasContratosBll;
        private EstadosGarantiasBLL vEstadosGarantiasBLL;
        private PlanComprasProductosBLL vPlanComprasProductosBll;
        private PlanComprasRubrosCDPBLL vPlanComprasRubrosCDPBll;
        private TipoFormaPagoBLL vTipoFormaPagoBLL;
        private VigenciaFuturasBLL vVigenciaFuturasBLL;
        private RPContratoBLL vRPContratoBLL;
        private ReduccionesBLL vReduccionesBLL;
        private DependenciaSolicitanteBLL vDependenciaSolicitanteBLL;
        private FUCBLL vFUCBLL;

        private ConsModContractualBLL vConsModContractualBLL;
        private TipoModificacionBLL vTipoModificacion;

        private SolModContractualBLL vSolModContractualBLL;
        private ModificacionGarantiaBLL vModificacionGarantiaBLL;
        private ProrrogasBLL vProrrogasBLL;
        
        private GarantiaModificacionBLL vGarantiaModificacionBLL;
        private CertificacionesContratosBLL _vCertificacionesContratosBLL;
        private ObligacionesContratoReporteBLL _vObligacionesContratoReporte;
        private HistoricoSolicitudesEliminadasBLL _vHistoricoSolicitudesEliminadasBLL;


        public ContratoService()
        {
            vCategoriaContratoBLL = new CategoriaContratoBLL();
            vClausulaContratoBLL = new ClausulaContratoBLL();
            vTipoContratoBLL = new Icbf.Contrato.Business.TipoContratoBLL();
            vTipoClausulaBLL = new TipoClausulaBLL();
            vObligacionBLL = new ObligacionBLL();
            vTipoAmparoBLL = new TipoAmparoBLL();
            vTipoObligacionBLL = new TipoObligacionBLL();
            vTipoGarantiaBLL = new TipoGarantiaBLL();
            vFormaPagoBLL = new FormaPagoBLL();
            vRegimenContratacionBLL = new RegimenContratacionBLL();
            vModalidadAcademicaKactusBLL = new ModalidadAcademicaKactusBLL();
            vModalidadSeleccionBLL = new ModalidadSeleccionBLL();
            vTipoSupvInterventorBLL = new TipoSupvInterventorBLL();
            vSecuenciaNumeroContratoBLL = new SecuenciaNumeroContratoBLL();
            vSecuenciaNumeroProcesoBLL = new SecuenciaNumeroProcesoBLL();
            vNumeroProcesosBLL = new NumeroProcesosBLL();
            vObjetoContratoBLL = new ObjetoContratoBLL();
            vGestionarClausulasContratoBLL = new GestionarClausulasContratoBLL();
            vConsultarSupervisorInterventorBLL = new ConsultarSupervisorInterventorBLL();
            vCesionesBLL = new CesionesBLL();
            vRelacionarSupervisorInterventorBLL = new RelacionarSupervisorInterventorBLL();
            vContratoBLL = new Icbf.Contrato.Business.ContratoBLL();
            vGestionarObligacionBLL = new GestionarObligacionBLL();
            vTablaParametricaBLL = new TablaParametricaBLL();
            vUnidadMedidaBLL = new UnidadMedidaBLL();
            vRegionalTipoContrato = new RegionalBLL();
            vPreguntaBLL = new PreguntaBLL();
            vProveedores_ContratosBLL = new Proveedores_ContratosBLL();
            vInformacionPresupuestalBLL = new InformacionPresupuestalBLL();
            vLugarEjecucionBLL = new LugarEjecucionBLL();
            vLugarEjecucionContratoBLL = new LugarEjecucionContratoBLL();
            vInformacionPresupuestalRP = new InformacionPresupuestalRPBLL();
            vRelacionarContratistas = new RelacionarContratistasBLL();
            vRelacionContratoBLL = new RelacionContratoBLL();
            vProveedores_ContratistaBLL = new Proveedores_ContratistaBLL();
            vRegistroInformacionPresupuestalBLL = new RegistroInformacionPresupuestalBLL();
            vContratosCDPBLL = new ContratosCDPBLL();
            vEstadoSolcitudModPlanComprasBLL = new EstadoSolcitudModPlanComprasBLL();
            vPlanDeComprasBLL = new PlanDeComprasBLL();
            vSolicitudModPlanComprasBLL = new SolicitudModPlanComprasBLL();
            vPlanDeComprasContratosBLL = new PlanDeComprasContratosBLL();
            vAporteContratoBLL = new AporteContratoBLL();
            vEmpleadoBLL = new EmpleadoBLL();
            vConsecutivoContratoRegionalesBLL = new ConsecutivoContratoRegionalesBLL();
            vTipoSuperInterBLL = new TipoSuperInterBLL();
            vSupervisorInterContratoBLL = new SupervisorInterContratoBLL();
            vAmparosGarantiasBLL = new AmparosGarantiasBLL();
            vSupervisorInterventorBLL = new SupervisorInterventorBLL();
            vSupervisorDatosBLL = new SupervisorDatosBLL();
            vSuspensionesBLL = new SuspensionesBLL();
            vUnidadCalculoBLL = new UnidadCalculoBLL();
            vGarantiaBLL = new GarantiaBLL();
            vConsultarSuperInterContratoBLL = new ConsultarSuperInterContratoBLL();
            vProfesionKactusBLL = new ProfesionKactusBLL();
            vArchivosGarantiasBLL = new ArchivosGarantiasBLL();
            vSucursalAseguradoraContratoBLL = new SucursalAseguradoraContratoBLL();
            vContratistaGarantiasBLL = new ContratistaGarantiasBLL();
            vTipoContratoAsociadoBLL = new TipoContratoAsociadoBLL();
            vPlanComprasContratosBll = new PlanComprasContratosBll();
            vSuscripcionContratoBLL = new SuscripcionContratoBLL();
            vSubComponenteBLL = new SubComponenteBLL();
            vEstadosGarantiasBLL = new EstadosGarantiasBLL();
            vPlanComprasProductosBll = new PlanComprasProductosBLL();
            vPlanComprasRubrosCDPBll = new PlanComprasRubrosCDPBLL();
            vTipoFormaPagoBLL = new TipoFormaPagoBLL();
            vProveedoresContratosBLL = new ProveedoresContratosBLL();
            vVigenciaFuturasBLL = new VigenciaFuturasBLL();
            vRPContratoBLL = new RPContratoBLL();
            vReduccionesBLL = new ReduccionesBLL();
            vDependenciaSolicitanteBLL = new DependenciaSolicitanteBLL();
            vFUCBLL = new FUCBLL();
            vConsModContractualBLL = new ConsModContractualBLL();
            vTipoModificacion = new TipoModificacionBLL();
            vSolModContractualBLL = new SolModContractualBLL();
            vModificacionGarantiaBLL = new ModificacionGarantiaBLL();
            vProrrogasBLL = new ProrrogasBLL();
            vGarantiaModificacionBLL = new GarantiaModificacionBLL();
            _vCertificacionesContratosBLL = new CertificacionesContratosBLL();
            _vObligacionesContratoReporte = new ObligacionesContratoReporteBLL();
            _vHistoricoSolicitudesEliminadasBLL = new HistoricoSolicitudesEliminadasBLL();
        }




        #region Categoria Contratos
        /// <summary>
        /// Método de inserción para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int InsertarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoBLL.InsertarCategoriaContrato(pCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int ModificarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoBLL.ModificarCategoriaContrato(pCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int EliminarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoBLL.EliminarCategoriaContrato(pCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pIdCategoriaContrato"></param>
        /// <returns></returns>
        public CategoriaContrato ConsultarCategoriaContrato(int pIdCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoBLL.ConsultarCategoriaContrato(pIdCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pNombreCategoriaContrato"></param>
        /// <param name="pDescripcionCategoriaContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<CategoriaContrato> ConsultarCategoriaContratos(String pNombreCategoriaContrato, String pDescripcionCategoriaContrato, Boolean? pEstado)
        {
            try
            {
                return vCategoriaContratoBLL.ConsultarCategoriaContratos(pNombreCategoriaContrato, pDescripcionCategoriaContrato, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Permite obtener a partir del identificador de tabla o el código(Uno de los dos) de la categoria de contrato la 
        /// información del mismo
        /// </summary>
        /// <param name="pTipoContrato">Entidad con la información del filtro</param>
        /// <returns>Entidad con la información de tipo de contrato recuperado</returns>
        public CategoriaContrato IdentificadorCodigoCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoBLL.IdentificadorCodigoCategoriaContrato(pCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Clausula Contratos
        /// <summary>
        /// Método de inserción para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int InsertarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                return vClausulaContratoBLL.InsertarClausulaContrato(pClausulaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int ModificarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                return vClausulaContratoBLL.ModificarClausulaContrato(pClausulaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int EliminarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                return vClausulaContratoBLL.EliminarClausulaContrato(pClausulaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pIdClausulaContrato"></param>
        /// <returns></returns>
        public ClausulaContrato ConsultarClausulaContrato(int pIdClausulaContrato)
        {
            try
            {
                return vClausulaContratoBLL.ConsultarClausulaContrato(pIdClausulaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pNombreClausulaContrato"></param>
        /// <param name="pContenido"></param>
        /// <param name="pIdTipoClausula"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<ClausulaContrato> ConsultarClausulaContratos(String pNombreClausulaContrato, String pContenido, int? pIdTipoClausula, int? pIdTipoContrato, Boolean? pEstado)
        {
            try
            {
                return vClausulaContratoBLL.ConsultarClausulaContratos(pNombreClausulaContrato, pContenido, pIdTipoClausula, pIdTipoContrato, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Cesiones

        public int InsertarCesiones(Cesiones pCesiones)
        {
            try
            {
                return vCesionesBLL.InsertarCesiones(pCesiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarCesiones(Cesiones pCesiones)
        {
            try
            {
                return vCesionesBLL.ModificarCesiones(pCesiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarCesiones(Cesiones pCesiones)
        {
            try
            {
                return vCesionesBLL.EliminarCesiones(pCesiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Cesiones ConsultarCesiones(int pIdCesion)
        {
            try
            {
                return vCesionesBLL.ConsultarCesiones(pIdCesion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Cesiones ConsultarCesionesContrato(int pIdContrato)
        {
            try
            {
                return vCesionesBLL.ConsultarCesionesContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<Cesiones> ConsultarInfoCesionesSuscritas(int pIdContrato)
        {
            try
            {
                return vCesionesBLL.ConsultarInfoCesionesSuscritas(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarInfoCesiones(int pIdContrato)
        {
            try
            {
                return vCesionesBLL.ConsultarInfoCesiones(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarCesioness(String pFechaCesion, String pJustificacion, int? pEstado, int? pIDDetalleConsModContractual)
        {
            try
            {
                return vCesionesBLL.ConsultarCesioness(pFechaCesion, pJustificacion, pEstado, pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarCesionessContrato(int pContrato, int? pIntegrantesUnionTemporal, int? pIdCesion)
        {
            try
            {
                return vCesionesBLL.ConsultarCesionessContrato(pContrato, pIntegrantesUnionTemporal, pIdCesion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarContratisasActualesCesiones(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vCesionesBLL.ConsultarContratisasActuales(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarContratisasCesion(int pContrato, int idcesion, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vCesionesBLL.ConsultarContratisasCesion(pContrato, idcesion, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarExisteCesionario(int pContrato, int idcesion, int idProveedorePadre)
        {
            try
            {
                return vCesionesBLL.ConsultarExisteCesionario(pContrato, idcesion, idProveedorePadre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Tipo Contrato
        /// <summary>
        /// Método de inserción para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int InsertarTipoContrato(Icbf.Contrato.Entity.TipoContrato pTipoContrato)
        {
            try
            {
                return vTipoContratoBLL.InsertarTipoContrato(pTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int ModificarTipoContrato(Icbf.Contrato.Entity.TipoContrato pTipoContrato)
        {
            try
            {
                return vTipoContratoBLL.ModificarTipoContrato(pTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int EliminarTipoContrato(Icbf.Contrato.Entity.TipoContrato pTipoContrato)
        {
            try
            {
                return vTipoContratoBLL.EliminarTipoContrato(pTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad TipoContrato
        /// </summary>
        /// <param name="pIdTipoContrato"></param>
        /// <returns></returns>
        public Icbf.Contrato.Entity.TipoContrato ConsultarTipoContrato(int pIdTipoContrato)
        {
            try
            {
                return vTipoContratoBLL.ConsultarTipoContrato(pIdTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoContrato
        /// </summary>
        /// <param name="pNombreTipoContrato"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pEstado"></param>
        /// <param name="pActaInicio"></param>
        /// <param name="pAporteCofinanciacion"></param>
        /// <param name="pRecursosFinancieros"></param>
        /// <param name="pRegimenContrato"></param>
        /// <param name="pDescripcionTipoContrato"></param>
        /// <returns></returns>
        public List<Icbf.Contrato.Entity.TipoContrato> ConsultarTipoContratos(String pNombreTipoContrato, int? pIdCategoriaContrato, Boolean? pEstado, Boolean? pActaInicio, Boolean? pAporteCofinanciacion, Boolean? pRecursosFinancieros, int? pRegimenContrato, String pDescripcionTipoContrato)
        {
            try
            {
                return vTipoContratoBLL.ConsultarTipoContratos(pNombreTipoContrato, pIdCategoriaContrato, pEstado, pActaInicio, pAporteCofinanciacion, pRecursosFinancieros, pRegimenContrato, pDescripcionTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Permite obtener a partir del identificador de tabla o el código(Uno de los dos) del tipo de contrato la 
        /// información del mismo
        /// </summary>
        /// <param name="pTipoContrato">Entidad con la información del filtro</param>
        /// <returns>Entidad con la información de tipo de contrato recuperado</returns>
        public Icbf.Contrato.Entity.TipoContrato IdentificadorCodigoTipoContrato(Icbf.Contrato.Entity.TipoContrato pTipoContrato)
        {
            try
            {
                return vTipoContratoBLL.IdentificadorCodigoTipoContrato(pTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Tipo Clausula
        /// <summary>
        /// Método de inserción para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int InsertarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                return vTipoClausulaBLL.InsertarTipoClausula(pTipoClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int ModificarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                return vTipoClausulaBLL.ModificarTipoClausula(pTipoClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int EliminarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                return vTipoClausulaBLL.EliminarTipoClausula(pTipoClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad TipoClausula
        /// </summary>
        /// <param name="pIdTipoClausula"></param>
        /// <returns></returns>
        public TipoClausula ConsultarTipoClausula(int pIdTipoClausula)
        {
            try
            {
                return vTipoClausulaBLL.ConsultarTipoClausula(pIdTipoClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoClausula
        /// </summary>
        /// <param name="pNombreTipoClausula"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoClausula> ConsultarTipoClausulas(String pNombreTipoClausula, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoClausulaBLL.ConsultarTipoClausulas(pNombreTipoClausula, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Obligacion
        /// <summary>
        /// Método de inserción para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int InsertarObligacion(Obligacion pObligacion)
        {
            try
            {
                return vObligacionBLL.InsertarObligacion(pObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int ModificarObligacion(Obligacion pObligacion)
        {
            try
            {
                return vObligacionBLL.ModificarObligacion(pObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int EliminarObligacion(Obligacion pObligacion)
        {
            try
            {
                return vObligacionBLL.EliminarObligacion(pObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad Obligacion
        /// </summary>
        /// <param name="pIdObligacion"></param>
        /// <returns></returns>
        public Obligacion ConsultarObligacion(int pIdObligacion)
        {
            try
            {
                return vObligacionBLL.ConsultarObligacion(pIdObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Obligacion
        /// </summary>
        /// <param name="pDescripcion"></param>
        /// <param name="pIdTipoObligacion"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<Obligacion> ConsultarObligacions(String pDescripcion, int? pIdTipoObligacion, int? pIdTipoContrato, Boolean? pEstado)
        {
            try
            {
                return vObligacionBLL.ConsultarObligacions(pDescripcion, pIdTipoObligacion, pIdTipoContrato, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Tipo Garantia
        /// <summary>
        /// Método de inserción para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int InsertarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                return vTipoGarantiaBLL.InsertarTipoGarantia(pTipoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int ModificarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                return vTipoGarantiaBLL.ModificarTipoGarantia(pTipoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int EliminarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                return vTipoGarantiaBLL.EliminarTipoGarantia(pTipoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad TipoGarantia
        /// </summary>
        /// <param name="pIdTipoGarantia"></param>
        /// <returns></returns>
        public TipoGarantia ConsultarTipoGarantia(int pIdTipoGarantia)
        {
            try
            {
                return vTipoGarantiaBLL.ConsultarTipoGarantia(pIdTipoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoGarantia
        /// </summary>
        /// <param name="pNombreTipoGarantia"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoGarantia> ConsultarTipoGarantias(String pNombreTipoGarantia, Boolean? pEstado)
        {
            try
            {
                return vTipoGarantiaBLL.ConsultarTipoGarantias(pNombreTipoGarantia, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Tipo Amparo
        /// <summary>
        /// Método de inserción para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int InsertarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                return vTipoAmparoBLL.InsertarTipoAmparo(pTipoAmparo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int ModificarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                return vTipoAmparoBLL.ModificarTipoAmparo(pTipoAmparo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int EliminarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                return vTipoAmparoBLL.EliminarTipoAmparo(pTipoAmparo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad TipoAmparo
        /// </summary>
        /// <param name="pIdTipoAmparo"></param>
        /// <returns></returns>
        public TipoAmparo ConsultarTipoAmparo(int pIdTipoAmparo)
        {
            try
            {
                return vTipoAmparoBLL.ConsultarTipoAmparo(pIdTipoAmparo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoAmparo
        /// </summary>
        /// <param name="pNombreTipoAmparo"></param>
        /// <param name="pIdTipoGarantia"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoAmparo> ConsultarTipoAmparos(String pNombreTipoAmparo, int? pIdTipoGarantia, Boolean? pEstado)
        {
            try
            {
                return vTipoAmparoBLL.ConsultarTipoAmparos(pNombreTipoAmparo, pIdTipoGarantia, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Tipo Obligacion
        /// <summary>
        /// Método de inserción para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int InsertarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                return vTipoObligacionBLL.InsertarTipoObligacion(pTipoObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int ModificarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                return vTipoObligacionBLL.ModificarTipoObligacion(pTipoObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int EliminarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                return vTipoObligacionBLL.EliminarTipoObligacion(pTipoObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad TipoObligacion
        /// </summary>
        /// <param name="pIdTipoObligacion"></param>
        /// <returns></returns>
        public TipoObligacion ConsultarTipoObligacion(int pIdTipoObligacion)
        {
            try
            {
                return vTipoObligacionBLL.ConsultarTipoObligacion(pIdTipoObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoObligacion
        /// </summary>
        /// <param name="pNombreTipoObligacion"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoObligacion> ConsultarTipoObligacions(String pNombreTipoObligacion, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoObligacionBLL.ConsultarTipoObligacions(pNombreTipoObligacion, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Forma Pago

        /// <summary>
        /// Método de inserción para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la información de la entidad Forma de pago</param>
        public int InsertarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                return vFormaPagoBLL.InsertarFormaPago(pFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la información de la entidad Forma de pago</param>
        public int ModificarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                return vFormaPagoBLL.ModificarFormaPago(pFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la información de la entidad Forma de pago</param>
        public int EliminarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                return vFormaPagoBLL.EliminarFormaPago(pFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad FormaPago
        /// </summary>
        /// <param name="pIdFormaPago">Valor entero con el Id de la forma de pago</param>
        public FormaPago ConsultarFormaPago(int pIdFormaPago)
        {
            try
            {
                return vFormaPagoBLL.ConsultarFormaPago(pIdFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad FormaPago
        /// </summary>
        /// <param name="pIdMedioPago">Valor entero con el Id del medio de pago</param>
        /// <param name="pIdEntidadFinanciera">Valor entero con el id de entidad financiera</param>
        /// <param name="pTipoCuentaBancaria">Valor entero con el Id del tipo de cuenta bancaria</param>
        /// <param name="pNumeroCuentaBancaria">Cadena de texto con el número de la cuenta bancaria</param>
        public List<FormaPago> ConsultarFormaPagos(int? pIdProveedores, int? pIdMedioPago, int? pIdEntidadFinanciera, int? pTipoCuentaBancaria, String pNumeroCuentaBancaria)
        {
            try
            {
                return vFormaPagoBLL.ConsultarFormaPagos(pIdProveedores, pIdMedioPago, pIdEntidadFinanciera, pTipoCuentaBancaria, pNumeroCuentaBancaria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar Entidad Financiera 
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pCodigoEntFin">Cadena de texto con el código de la entidad financiera</param>
        /// <param name="pNombreEntFin">Cadena de texto con el nombre de la entidad financiera</param>
        /// <param name="pEstado">Valor Booleano con el estado de la entidad financiera</param>
        /// <param name="pEsPagoPorOrdenDePago">Valor Booleano con el estado pago por orden de pago</param>
        /// <param name="pEsPagoPorOrdenBancaria">Valor Booleano con el estado pago por orden bancaria</param>
        /// <returns>Lista con la Instancia que contiene la información de la entidad financiera</returns>
        public List<EntidadFinanciera> ConsultarEntidadFinancieras(String pCodigoEntFin, String pNombreEntFin, Boolean? pEstado, Boolean? pEsPagoPorOrdenDePago, Boolean? pEsPagoPorOrdenBancaria)
        {
            try
            {
                return vFormaPagoBLL.ConsultarEntidadFinancieras(pCodigoEntFin, pNombreEntFin, pEstado, pEsPagoPorOrdenDePago, pEsPagoPorOrdenBancaria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consulta los tipos de cuenta de entidad financiera segun los filtros dados
        /// 30-01-2014
        /// </summary>
        /// <param name="pCodTipoCta">Cadena de texto con el codigo del tipo de cuenta</param>
        /// <param name="pDescripcion">Cadena de texto con la descripcion del tipo de cuenta</param>
        /// <param name="pEstado">Booleano que indica el estado</param>
        /// <returns>Lista de instancias que contiene los tipo código de retención</returns>
        public List<TipoCuentaEntFin> ConsultarTipoCuentaEntFins(String pCodTipoCta, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vFormaPagoBLL.ConsultarTipoCuentaEntFins(pCodTipoCta, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet 
        /// Permite consultar el tipo de medio de pago a partir de los 
        /// filtros dados
        /// 7-Feb-2014
        /// </summary>
        /// <param name="pCodMedioPago">Cadena de texto con el codigo medio de pago</param>
        /// <param name="pDescTipoMedioPago">Cadena de texto con la descripcion del tipo medio de pago</param>
        /// <param name="pEstado">Booleano que indica el estado</param>
        /// <returns>Lista con las instancias que contiene los tipos de medio de pago, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<TipoMedioPago> ConsultarTipoMedioPagos(String pCodMedioPago, String pDescTipoMedioPago, Boolean? pEstado)
        {
            try
            {
                return vFormaPagoBLL.ConsultarTipoMedioPagos(pCodMedioPago, pDescTipoMedioPago, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros si el usuario que creó el registro sea el mismo que el que ingresa al sistema
        /// </summary>
        /// <param name="pIdMedioPago">Valor entero con el Id del medio de pago</param>
        /// <param name="pIdEntidadFinanciera">Valor entero con el id de entidad financiera</param>
        /// <param name="pTipoCuentaBancaria">Valor entero con el Id del tipo de cuenta bancaria</param>
        /// <param name="pNumeroCuentaBancaria">Cadena de texto con el número de la cuenta bancaria</param>
        /// <param name="pUsuarioCrea">Cadena de texto con el nombre del usuario creación</param>
        public bool ConsultarFormaPagosUsuarioCrea(int? pIdProveedores, int? pIdMedioPago, int? pIdEntidadFinanciera,
                                                   int? pTipoCuentaBancaria, String pNumeroCuentaBancaria,
                                                   string pUsuarioCrea)
        {
            try
            {
                return vFormaPagoBLL.ConsultarFormaPagosUsuarioCrea(pIdProveedores, pIdMedioPago, pIdEntidadFinanciera, pTipoCuentaBancaria, pNumeroCuentaBancaria, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros si el usuario que creó el registro sea el mismo que el que ingresa al sistema
        /// </summary>
        /// <param name="pUsuarioCrea">Cadena de texto con el nombre del usuario creación</param>
        public bool ConsultarContratoUsuarioCrea(string pUsuarioCrea, int pidContrato)
        {
            try
            {
                return vFormaPagoBLL.ConsultarContratoUsuarioCrea(pUsuarioCrea, pidContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Método de consulta por id para la Tercero
        /// 7-Feb-2014
        /// </summary>
        /// <param name="pIdTercero">Entero con el identificador del tercero</param>
        /// <returns>Instancia que contiene la información del tercero recuperado 
        /// de la base de datos</returns>
        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                return vFormaPagoBLL.ConsultarTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad FormaPago por contrato
        /// </summary>
        /// /// <param name="pIdContrato">Instancia con la información del id del contrato</param>
        /// <param name="pIdFormaPago">Instancia con la información del id de la forma de pago</param>
        public int ModificarFormaPagoContrato(int pIdProveedoresContratos, int pIdFormaPago, string pUsuarioModifica)
        {
            try
            {
                return vFormaPagoBLL.ModificarFormaPagoContrato(pIdProveedoresContratos, pIdFormaPago, pUsuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta un(a) Contratos por el id de la forma de pago retorna verdadero si la forma de pago esta asociada a un contrato
        /// </summary>
        /// <param name="pIdFormapago">Valor Entero con el Id de la forma de pago</param>
        /// /// <param name="pIdProveedoresContratos">Valor Entero con el Id ProveedoresContratos</param>
        public bool ConsultarFormaPagoContrato(int pIdFormapago, int pIdProveedoresContratos)
        {
            try
            {
                return vFormaPagoBLL.ConsultarFormaPagoContrato(pIdFormapago, pIdProveedoresContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Regimen Contratacion
        /// <summary>
        /// Método de inserción para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int InsertarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                return vRegimenContratacionBLL.InsertarRegimenContratacion(pRegimenContratacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int ModificarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                return vRegimenContratacionBLL.ModificarRegimenContratacion(pRegimenContratacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int EliminarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                return vRegimenContratacionBLL.EliminarRegimenContratacion(pRegimenContratacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pIdRegimenContratacion"></param>
        /// <returns></returns>
        public RegimenContratacion ConsultarRegimenContratacion(int pIdRegimenContratacion)
        {
            try
            {
                return vRegimenContratacionBLL.ConsultarRegimenContratacion(pIdRegimenContratacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pNombreRegimenContratacion"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<RegimenContratacion> ConsultarRegimenContratacions(String pNombreRegimenContratacion, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vRegimenContratacionBLL.ConsultarRegimenContratacions(pNombreRegimenContratacion, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region ModalidadAcademica
        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo de consulta por filtros para la entidad ModalidadAcademicaKactus
        /// </summary>
        /// <param name="pNombre">Cadena de texto con el nombre</param>
        /// <param name="pId">Cadena de texto con el identificador</param>
        /// <param name="pEstado">Booleano con el estado</param>
        /// <returns>Lista con las modalidades que coinciden con los filtros dados</returns>
        public List<ModalidadAcademicaKactus> ConsultarModalidadesAcademicasKactus(String pNombre, String pId, Boolean? pEstado)
        {
            try
            {
                return vModalidadAcademicaKactusBLL.ConsultarModalidadesAcademicasKactus(pNombre, pId, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Modalidad Seleccion
        /// <summary>
        /// Método de inserción para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int InsertarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                return vModalidadSeleccionBLL.InsertarModalidadSeleccion(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int ModificarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                return vModalidadSeleccionBLL.ModificarModalidadSeleccion(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int EliminarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                return vModalidadSeleccionBLL.EliminarModalidadSeleccion(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pIdModalidad"></param>
        /// <returns></returns>
        public ModalidadSeleccion ConsultarModalidadSeleccion(int pIdModalidad)
        {
            try
            {
                return vModalidadSeleccionBLL.ConsultarModalidadSeleccion(pIdModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pNombre"></param>
        /// <param name="pSigla"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<ModalidadSeleccion> ConsultarModalidadSeleccions(String pNombre, String pSigla, Boolean? pEstado)
        {
            try
            {
                return vModalidadSeleccionBLL.ConsultarModalidadSeleccions(pNombre, pSigla, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModalidadSeleccion> ConsultarModalidadSeleccionTipoContrato(int pIdTipoContrato, Boolean? pEstado)
        {
            try
            {
                return vModalidadSeleccionBLL.ConsultarModalidadSeleccionTipoContrato(pIdTipoContrato,pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Permite obtener a partir del identificador de tabla o el código(Uno de los dos), de la modalidad de seleccion, la 
        /// información del mismo
        /// </summary>
        /// <param name="pModalidadSeleccion">Entidad con la información del filtro</param>
        /// <returns>Entidad con la información de la modalidad de seleccion recuperada</returns>
        public ModalidadSeleccion IdentificadorCodigoModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                return vModalidadSeleccionBLL.IdentificadorCodigoModalidadSeleccion(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Tipo Supervisor Interventor
        /// <summary>
        /// Método de inserción para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int InsertarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                return vTipoSupvInterventorBLL.InsertarTipoSupvInterventor(pTipoSupvInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int ModificarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                return vTipoSupvInterventorBLL.ModificarTipoSupvInterventor(pTipoSupvInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int EliminarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                return vTipoSupvInterventorBLL.EliminarTipoSupvInterventor(pTipoSupvInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pIdTipoSupvInterventor"></param>
        /// <returns></returns>
        public TipoSupvInterventor ConsultarTipoSupvInterventor(int pIdTipoSupvInterventor)
        {
            try
            {
                return vTipoSupvInterventorBLL.ConsultarTipoSupvInterventor(pIdTipoSupvInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pNombre"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoSupvInterventor> ConsultarTipoSupvInterventors(String pNombre, Boolean? pEstado)
        {
            try
            {
                return vTipoSupvInterventorBLL.ConsultarTipoSupvInterventors(pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Secuencia Numero Contrato
        /// <summary>
        /// Método que Genera Numero de Contrato
        /// </summary>
        /// <param name="pSecuenciaNumeroContrato"></param>
        /// <returns></returns>
        public string GenerarNumeroContrato(SecuenciaNumeroContrato pSecuenciaNumeroContrato)
        {
            try
            {
                return vSecuenciaNumeroContratoBLL.GenerarNumeroContrato(pSecuenciaNumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Secuencia Numero proceso
        /// <summary>
        /// Método que Genera el Numero de Proceso
        /// </summary>
        /// <param name="pSecuenciaNumeroProceso"></param>
        /// <returns></returns>
        public string GenerarNumeroProceso(SecuenciaNumeroProceso pSecuenciaNumeroProceso)
        {
            try
            {
                return vSecuenciaNumeroProcesoBLL.GenerarNumeroProceso(pSecuenciaNumeroProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Numero proceso
        /// <summary>
        /// Gonet
        /// Método de inserción para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int InsertarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                return vNumeroProcesosBLL.InsertarNumeroProcesos(pNumeroProcesos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Método de modificación para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int ModificarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                return vNumeroProcesosBLL.ModificarNumeroProcesos(pNumeroProcesos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Método de eliminación para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int EliminarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                return vNumeroProcesosBLL.EliminarNumeroProcesos(pNumeroProcesos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Método de consulta por id para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pIdNumeroProceso">Valor entero con el Id del numero del proceso</param>
        public NumeroProcesos ConsultarNumeroProcesos(int pIdNumeroProceso)
        {
            try
            {
                return vNumeroProcesosBLL.ConsultarNumeroProcesos(pIdNumeroProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Método de consulta por filtros para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProceso">Valor entero con el número del proceso</param>
        /// <param name="pInactivo">Valor Booleano del estado del registro</param>
        /// <param name="pNumeroProcesoGenerado">Cadena de texto con el numero de proceso generado</param>
        /// <param name="pIdModalidadSeleccion">Valor entero con el Id de la modalidad de selección</param>
        /// <param name="pIdRegional">Valor entero con el Id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el Id de la vigencia</param>
        public List<NumeroProcesos> ConsultarNumeroProcesoss(int? pIdNumeroProceso, int? pNumeroProceso, Boolean? pInactivo, String pNumeroProcesoGenerado, int? pIdModalidadSeleccion, int? pIdRegional, int? pIdVigencia)
        {
            try
            {

                return vNumeroProcesosBLL.ConsultarNumeroProcesoss(pIdNumeroProceso, pNumeroProceso, pInactivo, pNumeroProcesoGenerado, pIdModalidadSeleccion, pIdRegional, pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Método de consulta por id NumeroProcesos si el mumero de proceso esta asignado a un contrato registrado
        /// Fecha: 21/05/2014
        /// </summary>
        /// <param name="pIdNumeroProceso">Valor entero con el Id del numero del proceso</param>
        public bool ConsultarNumeroProcesosPorContrato(int pIdNumeroProceso)
        {
            try
            {
                return vNumeroProcesosBLL.ConsultarNumeroProcesosPorContrato(pIdNumeroProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Objeto Contrato
        /// <summary>
        /// Método de inserción para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int InsertarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                return vObjetoContratoBLL.InsertarObjetoContrato(pObjetoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int ModificarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                return vObjetoContratoBLL.ModificarObjetoContrato(pObjetoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int EliminarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                return vObjetoContratoBLL.EliminarObjetoContrato(pObjetoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pIdObjetoContratoContractual"></param>
        /// <returns></returns>
        public ObjetoContrato ConsultarObjetoContrato(int pIdObjetoContratoContractual)
        {
            try
            {
                return vObjetoContratoBLL.ConsultarObjetoContrato(pIdObjetoContratoContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pIdObjetoContratoContractual"></param>
        /// <param name="pObjetoContractual"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<ObjetoContrato> ConsultarObjetoContratos(int? pIdObjetoContratoContractual, String pObjetoContractual, Boolean? pEstado)
        {
            try
            {
                return vObjetoContratoBLL.ConsultarObjetoContratos(pIdObjetoContratoContractual, pObjetoContractual, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region GestionarClausulasContrato
        /// <summary>
        /// Método de inserción para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int InsertarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                return vGestionarClausulasContratoBLL.InsertarGestionarClausulasContrato(pGestionarClausulasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int ModificarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                return vGestionarClausulasContratoBLL.ModificarGestionarClausulasContrato(pGestionarClausulasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int EliminarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                return vGestionarClausulasContratoBLL.EliminarGestionarClausulasContrato(pGestionarClausulasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pIdGestionClausula"></param>
        /// <returns></returns>
        public GestionarClausulasContrato ConsultarGestionarClausulasContrato(int pIdGestionClausula)
        {
            try
            {
                return vGestionarClausulasContratoBLL.ConsultarGestionarClausulasContrato(pIdGestionClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pIdGestionClausula"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNombreClausula"></param>
        /// <param name="pTipoClausula"></param>
        /// <param name="pOrden"></param>
        /// <param name="pOrdenNumero"></param>
        /// <param name="pDescripcionClausula"></param>
        /// <returns></returns>
        public List<GestionarClausulasContrato> ConsultarGestionarClausulasContratos(int? pIdGestionClausula, String pIdContrato, String pNombreClausula, int? pTipoClausula, String pOrden, int? pOrdenNumero, String pDescripcionClausula)
        {
            try
            {
                return vGestionarClausulasContratoBLL.ConsultarGestionarClausulasContratos(pIdGestionClausula, pIdContrato, pNombreClausula, pTipoClausula, pOrden, pOrdenNumero, pDescripcionClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Consultar Supervisor/interventor
        /// <summary>
        /// Método de consulta por filtros para la entidad ConsultarSupervisorInterventor
        /// </summary>
        /// <param name="pTipoSupervisorInterventor"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pNombreRazonSocialSupervisorInterventor"></param>
        /// <param name="pNumeroIdentificacionDirectorInterventoria"></param>
        /// <param name="pNombreRazonSocialDirectorInterventoria"></param>
        /// <returns></returns>
        public DataTable ConsultarConsultarSupervisorInterventors(int? pTipoSupervisorInterventor, String pNumeroContrato, String pNumeroIdentificacion, String pNombreRazonSocialSupervisorInterventor, String pNumeroIdentificacionDirectorInterventoria, String pNombreRazonSocialDirectorInterventoria)
        {
            try
            {
                return vConsultarSupervisorInterventorBLL.ConsultarConsultarSupervisorInterventors(pTipoSupervisorInterventor, pNumeroContrato, pNumeroIdentificacion, pNombreRazonSocialSupervisorInterventor, pNumeroIdentificacionDirectorInterventoria, pNombreRazonSocialDirectorInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region RelacionarSupervisor/Interventor
        /// <summary>
        /// Método de inserción para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int InsertarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                return vRelacionarSupervisorInterventorBLL.InsertarRelacionarSupervisorInterventor(pRelacionarSupervisorInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int ModificarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                return vRelacionarSupervisorInterventorBLL.ModificarRelacionarSupervisorInterventor(pRelacionarSupervisorInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pIDSupervisorInterv"></param>
        /// <returns></returns>
        public RelacionarSupervisorInterventor ConsultarRelacionarSupervisorInterventor(int pIDSupervisorInterv)
        {
            try
            {
                return vRelacionarSupervisorInterventorBLL.ConsultarRelacionarSupervisorInterventor(pIDSupervisorInterv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pOrigenTipoSupervisor"></param>
        /// <param name="pIDTipoSupvInterventor"></param>
        /// <param name="pIDTerceroExterno"></param>
        /// <param name="pTipoVinculacion"></param>
        /// <param name="pFechaInicia"></param>
        /// <param name="pFechaFinaliza"></param>
        /// <param name="pEstado"></param>
        /// <param name="pFechaModificaInterventor"></param>
        /// <returns></returns>
        public List<RelacionarSupervisorInterventor> ConsultarRelacionarSupervisorInterventors(bool pOrigenTipoSupervisor, int? pIDTipoSupvInterventor, int? pIDTerceroExterno, bool pTipoVinculacion, DateTime? pFechaInicia, DateTime? pFechaFinaliza, bool pEstado, DateTime? pFechaModificaInterventor)
        {
            try
            {
                return vRelacionarSupervisorInterventorBLL.ConsultarRelacionarSupervisorInterventors(pOrigenTipoSupervisor, pIDTipoSupvInterventor, pIDTerceroExterno, pTipoVinculacion, pFechaInicia, pFechaFinaliza, pEstado, pFechaModificaInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int EliminarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                return vRelacionarSupervisorInterventorBLL.EliminarRelacionarSupervisorInterventor(pRelacionarSupervisorInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Contrato
        /// <summary>
        /// Método de inserción para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int InsertarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.InsertarContrato(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int ModificarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.ModificarContrato(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int EliminarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.EliminarContrato(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="fechaAnulacion"></param>
        /// <param name="usuarioModifica"></param>
        /// <returns></returns>
        public int AnularContrato(int idContrato, DateTime fechaAnulacion, string usuarioModifica)
        {
            try
            {
                return vContratoBLL.AnularContrato(idContrato, fechaAnulacion, usuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad Contrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public Contrato.Entity.Contrato ConsultarContrato(int pIdContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.Contrato ConsultarContratoModificacion(int pIdContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratoModificacion(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.Contrato ConsultarContratoAdiciones(int pIdContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratoAdiciones(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Contrato
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratos(DateTime? pFechaRegistro, String pNumeroProceso, Decimal? pNumeroContrato, int? pIdModalidad, int? pIdCategoriaContrato, int? pIdTipoContrato, string pClaseContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratos(pFechaRegistro, pNumeroProceso, pNumeroContrato, pIdModalidad, pIdCategoriaContrato, pIdTipoContrato, pClaseContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Contratos
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pFechaRegistroSistemaHasta"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContrato"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratoss(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, Boolean? pManejaRecurso, string pNombreContratistas, int? pIDTipoIdentificacion, int? pNumeroIdentificacion, string pUsuarioCreacion)
        {
            try
            {
                return vContratoBLL.ConsultarContratoss(pFechaRegistroSistemaDesde, pFechaRegistroSistemaHasta, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContrato, pIdDependenciaSolicitante, pManejaRecurso, pNombreContratistas, pIDTipoIdentificacion, pNumeroIdentificacion, pUsuarioCreacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<Contrato.Entity.Contrato> ConsultarContratosSimple(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, Boolean? pManejaRecurso)
        {
            try
            {
                return vContratoBLL.ConsultarContratosSimple(pFechaRegistroSistemaDesde, pFechaRegistroSistemaHasta, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContrato, pIdDependenciaSolicitante, pManejaRecurso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        public List<Contrato.Entity.Contrato> ConsultarContratosHistorico(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, Boolean? pManejaRecurso, string pNombreContratistas, int? pIDTipoIdentificacion, int? pNumeroIdentificacion)
        {
            try
            {
                return vContratoBLL.ConsultarContratosHistorico(pFechaRegistroSistemaDesde, pFechaRegistroSistemaHasta, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContrato, pIdDependenciaSolicitante, pManejaRecurso, pNombreContratistas, pIDTipoIdentificacion, pNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Contratos Registrados
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContraro"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratosRegistrados(DateTime? pFechaRegistroSistemaDesde, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContraro, string UsuarioCrea, DateTime? pFechaInicioEjecucion, DateTime? pFechaFinalizaInicioContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratosRegistrados(pFechaRegistroSistemaDesde, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContraro, UsuarioCrea, pFechaInicioEjecucion, pFechaFinalizaInicioContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Contratos Registrados
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContraro"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratosAnulados(DateTime? pFechaRegistroSistemaDesde, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContraro, string UsuarioCrea, DateTime? pFechaInicioEjecucion, DateTime? pFechaFinalizaInicioContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratosAnulados(pFechaRegistroSistemaDesde, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContraro, UsuarioCrea, pFechaInicioEjecucion, pFechaFinalizaInicioContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para los contratos suscritos
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosSuscritos(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, String pNumeroContratoConvenio, String pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                return vContratoBLL.ConsultarContratosSuscritos(pFechaRegistroDesde, pFechaRegistroHasta,
                    pNumeroContratoConvenio, pVigenciaFiscalInicial, pRegionalContrato, pCategoriaContrato,
                    pTipoContrato, pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta para los contratos suscritos
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosConRP(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, String pNumeroContratoConvenio, String pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                return vContratoBLL.ConsultarContratosConRP(pFechaRegistroDesde, pFechaRegistroHasta,
                    pNumeroContratoConvenio, pVigenciaFiscalInicial, pRegionalContrato, pCategoriaContrato,
                    pTipoContrato, pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta para los contratos suscritos
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosAprobacionGarantia(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, String pNumeroContratoConvenio, String pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                return vContratoBLL.ConsultarContratosAprobacionGarantia(pFechaRegistroDesde, pFechaRegistroHasta,
                    pNumeroContratoConvenio, pVigenciaFiscalInicial, pRegionalContrato, pCategoriaContrato,
                    pTipoContrato, pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }




        /// <summary>
        /// Método de consulta por filtros para la entidad Contrato Para una ventana Emergente
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public DataTable ConsultarContratosLupa(DateTime? pFechaRegistro, String pNumeroProceso, string pNumeroContrato, int? pIdModalidad, int? pIdCategoriaContrato, int? pIdTipoContrato, string pClaseContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratosLupa(pFechaRegistro, pNumeroProceso, pNumeroContrato, pIdModalidad, pIdCategoriaContrato, pIdTipoContrato, pClaseContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta contratos por el filtro
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosLupa(int? pIdContrato, String pNumeroContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratosLupa(pIdContrato, pNumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad EstadoContrato
        /// </summary>
        /// <param name="pIdEstadoContrato">Valor entero con el id del Estado del contrato</param>
        /// <param name="pCodigoEstadoContrato">Cadena de texto con el código del estado del contrato</param>
        /// <param name="pDescripcion">Cadena de texto con la descripcipon del estado del contrato</param>
        /// <param name="pInactivo">Valor Booleano con el estado del registro</param>
        /// <returns>Lista de la instancia EstadoContrato</returns>
        public List<Contrato.Entity.EstadoContrato> ConsultarEstadoContrato(int? pIdEstadoContrato, string pCodigoEstadoContrato,
                                                            string pDescripcion, bool pInactivo)
        {
            try
            {
                return vContratoBLL.ConsultarEstadoContrato(pIdEstadoContrato, pCodigoEstadoContrato,
                                                             pDescripcion, pInactivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #region LupaEmpleado
        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Actualiza la informacion de solicitante a un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informacion del solicitante y contrato a asociar</param>
        /// <returns>Entero con el resultado de la operación</returns>
        public int LupaEmpleadoInsertarSolicitanteContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.LupaEmpleadoInsertarSolicitanteContrato(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Actualiza la informacion de solicitante a un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informacion del solicitante y contrato a asociar</param>
        /// <returns>Entero con el resultado de la operación</returns>
        public int LupaEmpleadoInsertarSolicitanteContratoPrecontractual(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.LupaEmpleadoInsertarSolicitanteContratoPrecontractual(pContrato);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Actualiza la informacion de ordenador gasto a un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informacion del solicitante y contrato a asociar</param>
        /// <returns>Entero con el resultado de la operación</returns>
        public int LupaEmpleadoInsertarOrdenadoGasto(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.LupaEmpleadoInsertarOrdenadoGasto(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Actualiza la informacion de ordenador gasto a un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informacion del solicitante y contrato a asociar</param>
        /// <returns>Entero con el resultado de la operación</returns>
        public int LupaEmpleadoInsertarOrdenadoGastoPreContractual(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.LupaEmpleadoInsertarOrdenadoGastoPreContractual(pContrato);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region MetodosPaginaRegistroContratos
        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Registra un contrato vacio sobre el que se va a trabajar, devolviendo su identificador para el funcionamiento
        /// de las lupas
        /// </summary>
        /// <param name="pContrato">Entidad con la información mínima para la creación del contrato, en ella se
        /// almacena el identificador del contrato creado</param>
        /// <returns>Entero con el resultado de la operación</returns>
        public int ContratoRegistroInicial(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.ContratoRegistroInicial(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Realiza la actualizacion de la información contenida dentro de la entidad de entrada de tipo contrato
        /// </summary>
        /// <param name="pContrato">Entidad con la información a actualizar</param>
        /// <returns>Entero con el resultado de la operación</returns>
        public int ContratoActualizacion(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoBLL.ContratoActualizacion(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene el contrato asociado con el identificador dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato a obtener.</param>
        /// <returns>Entidad contrato con la información obtenida de la base de datos</returns>
        public Contrato.Entity.Contrato ContratoObtener(int pIdContrato)
        {
            try
            {
                return vContratoBLL.ContratoObtener(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapina
        /// Valida que el valor final del contrato sea mayor igual a la sumatoria de los valores finales
        /// de los contratos asociados a dicho contrato + el valor inical dado
        /// </summary>
        /// <param name="idContratoConvMarco"></param>
        /// <param name="valorInicialContConv"></param>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public bool ValidacionValorInicialSuperaValorFinal(int pIdContratoConvMarco, decimal pValorInicialContConv, int pIdContrato)
        {
            try
            {
                return vContratoBLL.ValidacionValorInicialSuperaValorFinal(pIdContratoConvMarco, pValorInicialContConv, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Valida si la regional del contrato convenio coincide con la regional de
        /// los registros de plan de compras
        /// </summary>
        /// <param name="pIdContratoConvMarco">Entero con el identificador del contrato</param>
        /// <param name="pCodigoRegionalContConv">Cadena de texto con el codigo de la regional</param>
        /// <returns>Retorna true si la validacion permite paso</returns>
        public bool ValidacionCoincideRegionalesContratoPlanComprasContrato(int pIdContratoConvMarco, string pCodigoRegionalContConv)
        {
            try
            {
                return vContratoBLL.ValidacionCoincideRegionalesContratoPlanComprasContrato(pIdContratoConvMarco, pCodigoRegionalContConv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pVigenciaFuturas"></param>
        /// <returns></returns>
        public bool ValidaValorInicialVsCDPplusVigenciasFuturas(int pIdContrato, VigenciaFuturas pVigenciaFuturas)//, decimal? pValorTotalCDP)
        {
            try
            {
                return vContratoBLL.ValidaValorInicialVsCDPplusVigenciasFuturas(pIdContrato, pVigenciaFuturas);//, pValorTotalCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public bool ConsultarContratoRPGarantiasAprobadas(int pIdContrato)
        {
            bool isValid = false;

            try
            {
                isValid = new ContratoBLL().ConsultarContratoRPGarantiasAprobadas(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return isValid;

        }

        public int ConsultarIdentificacionOrdenadorG(int pIdContrato)
        {
            try
            {
                return vContratoBLL.ConsultarIdentificacionOrdenadorG(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region RelacionContrato
        /// <summary>
        /// Método de inserción para la entidad RelacionContrato
        /// </summary>
        /// <param name="pRelacionContrato"></param>
        /// <returns></returns>
        public int InsertarRelacionContrato(RelacionContrato pRelacionContrato)
        {
            try
            {
                return vRelacionContratoBLL.InsertarRelacionContrato(pRelacionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RelacionContrato
        /// </summary>
        /// <param name="pRelacionContrato"></param>
        /// <returns></returns>
        public int EliminarRelacionContrato(RelacionContrato pRelacionContrato)
        {
            try
            {
                return vRelacionContratoBLL.EliminarRelacionContrato(pRelacionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad RelacionContrato
        /// </summary>
        /// <param name="idContratoMaestro"></param>
        /// <returns></returns>
        public List<RelacionContrato> ConsultarRelacionContrato(int idContratoMaestro)
        {
            try
            {
                return vRelacionContratoBLL.ConsultarRelacionContrato(idContratoMaestro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por NumeroContrato para la entidad RelacionContrato
        /// </summary>
        /// <param name="pNumeroContrato"></param>
        /// <returns></returns>
        public List<RelacionContrato> ConsultarRelacionContratos(string pNumeroContrato)
        {
            try
            {
                return vRelacionContratoBLL.ConsultarRelacionsContrato(pNumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad RelacionContrato
        /// </summary>
        /// <param name="idContratoAsociado"></param>
        /// <returns></returns>
        public Contrato.Entity.Contrato ConsultarContratoMaestro(int idContratoAsociado)
        {
            try
            {
                return vRelacionContratoBLL.ConsultarContratoMaestro(idContratoAsociado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Garantia
        /// <summary>
        /// Método de inserción para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int InsertarGarantia(Garantia pGarantia)
        {
            try
            {
                return vGarantiaBLL.InsertarGarantia(pGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int ModificarGarantia(Garantia pGarantia)
        {
            try
            {
                return vGarantiaBLL.ModificarGarantia(pGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarGarantia(Garantia item)
        {
            try
            {
                return vGarantiaBLL.ActualizarGarantia(item);
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int EliminarGarantia(Garantia pGarantia)
        {
            try
            {
                return vGarantiaBLL.EliminarGarantia(pGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Garantia
        /// </summary>
        /// <param name="pIDGarantia"></param>
        public Garantia ConsultarGarantia(int pIDGarantia)
        {
            try
            {
                return vGarantiaBLL.ConsultarGarantia(pIDGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Garantia
        /// </summary>
        /// <param name="pIDGarantia"></param>
        public Garantia ConsultarMaximaGarantia(int pIDGarantia)
        {
            try
            {
                return vGarantiaBLL.ConsultarMaximaGarantia(pIDGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="idTipoModificacionContractual"></param>
        /// <returns></returns>
        public ConsModContractual ConsultarModificacionContractualGarantia(int idContrato, int idGarantia)
        {
            try
            {
                return vGarantiaBLL.ConsultarModificacionContractualGarantia(idContrato, idGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Garantia
        /// </summary>
        /// <param name="pIDTipoGarantia"></param>
        /// <param name="pNumeroGarantia"></param>
        /// <param name="pAprobada"></param>
        /// <param name="pDevuelta"></param>
        /// <param name="pFechaAprobacionGarantia"></param>
        /// <param name="pFechaCertificacionGarantia"></param>
        /// <param name="pFechaDevolucion"></param>
        /// <param name="pMotivoDevolucion"></param>
        /// <param name="pIDUsuarioAprobacion"></param>
        /// <param name="pIDUsuarioDevolucion"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pBeneficiarioICBF"></param>
        /// <param name="pDescripcionBeneficiarios"></param>
        /// <param name="pFechaInicioGarantia"></param>
        /// <param name="pFechaExpedicionGarantia"></param>
        /// <param name="pFechaVencimientoInicialGarantia"></param>
        /// <param name="pFechaVencimientoFinalGarantia"></param>
        /// <param name="pFechaReciboGarantia"></param>
        /// <param name="pValorGarantia"></param>
        /// <param name="pAnexos"></param>
        /// <param name="pObservacionesAnexos"></param>
        /// <param name="pEstado"></param>
        /// <param name="pEntidadProvOferenteAseguradora"></param>
        /// <param name="pIDSucursalAseguradoraContrato"></param>
        /// <param name="pIDEstadosGarantias"></param>
        public List<Garantia> ConsultarGarantias(int? pIDTipoGarantia, String pNumeroGarantia, DateTime? pFechaAprobacionGarantia, DateTime? pFechaCertificacionGarantia, DateTime? pFechaDevolucion, String pMotivoDevolucion, int? pIDUsuarioAprobacion, int? pIDUsuarioDevolucion, int? pIdContrato, Boolean? pBeneficiarioICBF, Boolean? pBeneficiarioOTROS, String pDescripcionBeneficiarios, DateTime? pFechaInicioGarantia, DateTime? pFechaExpedicionGarantia,
            DateTime? pFechaVencimientoInicialGarantia, DateTime? pFechaVencimientoFinalGarantia, DateTime? pFechaReciboGarantia, String pValorGarantia, Boolean? pAnexos,
            String pObservacionesAnexos, int? pEntidadProvOferenteAseguradora, Boolean? pEstado, int? pIDSucursalAseguradoraContrato, int? pIDEstadosGarantias, String pCodEstadoContrato, String pCodEstadoGarantia)
        {
            try
            {
                return vGarantiaBLL.ConsultarGarantias(pIDTipoGarantia, pNumeroGarantia, pFechaAprobacionGarantia, pFechaCertificacionGarantia, pFechaDevolucion, pMotivoDevolucion, pIDUsuarioAprobacion, pIDUsuarioDevolucion, pIdContrato, pBeneficiarioICBF, pBeneficiarioOTROS, pDescripcionBeneficiarios, pFechaInicioGarantia, pFechaExpedicionGarantia, pFechaVencimientoInicialGarantia,
                    pFechaVencimientoFinalGarantia, pFechaReciboGarantia, pValorGarantia, pAnexos, pObservacionesAnexos, pEntidadProvOferenteAseguradora, pEstado, pIDSucursalAseguradoraContrato, pIDEstadosGarantias, pCodEstadoContrato, pCodEstadoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de un numero de garantia existente
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public bool ExisteNumeroGarantia(String pNumerogarantia)
        {
            try
            {
                return vGarantiaBLL.ExisteNumeroGarantia(pNumerogarantia)
                ;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int existeGarantiaContrato(String pNumerogarantia)
        {
            try
            {
                return vGarantiaBLL.existeGarantiaContrato(pNumerogarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por idcontrato para la entidad Garantia
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el Id del contrato</param>
        public List<Garantia> ConsultarInfoGarantias(int? pIdContrato)
        {
            try
            {
                return vGarantiaBLL.ConsultarInfoGarantias(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoMigrado> ConsultarInfoGarantiasMigracion(int? pIdContrato)
        {
            try
            {
                return vGarantiaBLL.ConsultarInfoGarantiasMigracion(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGarantia"></param>
        /// <param name="aprobada"></param>
        /// <returns></returns>
        public int CambiarEstadoGarantia(int idGarantia, bool aprobada, bool esRegistro, DateTime fecha, string observaciones, DateTime? fechaFinalizacion)
        {
            try
            {
                return vGarantiaBLL.CambiarEstadoGarantia(idGarantia, aprobada, esRegistro, fecha, observaciones, fechaFinalizacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGarantia"></param>
        /// <returns></returns>
        public List<string> ValidarAprobacionGarantia(int idGarantia)
        {
            try
            {
                return vEstadosGarantiasBLL.ValidarAprobacionGarantia(idGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<GarantiaHistorico> ConsultarInfoGarantiasHistoricoPorContrato(int vIdContrato)
        {
            try
            {
                return vGarantiaModificacionBLL.ConsultarInfoGarantiasHistoricoPorContrato(vIdContrato);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region GestionarObligacion
        /// <summary>
        /// Método de inserción para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int InsertarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                return vGestionarObligacionBLL.InsertarGestionarObligacion(pGestionarObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int ModificarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                return vGestionarObligacionBLL.ModificarGestionarObligacion(pGestionarObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int EliminarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                return vGestionarObligacionBLL.EliminarGestionarObligacion(pGestionarObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pIdGestionObligacion"></param>
        /// <returns></returns>
        public GestionarObligacion ConsultarGestionarObligacion(int pIdGestionObligacion)
        {
            try
            {
                return vGestionarObligacionBLL.ConsultarGestionarObligacion(pIdGestionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pIdGestionObligacion"></param>
        /// <param name="pIdGestionClausula"></param>
        /// <param name="pNombreObligacion"></param>
        /// <param name="pIdTipoObligacion"></param>
        /// <param name="pOrden"></param>
        /// <param name="pDescripcionObligacion"></param>
        /// <returns></returns>
        public List<GestionarObligacion> ConsultarGestionarObligacions(int? pIdGestionObligacion, int? pIdGestionClausula, String pNombreObligacion, int? pIdTipoObligacion, String pOrden, String pDescripcionObligacion)
        {
            try
            {
                return vGestionarObligacionBLL.ConsultarGestionarObligacions(pIdGestionObligacion, pIdGestionClausula, pNombreObligacion, pIdTipoObligacion, pOrden, pDescripcionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region TablaParametrica
        /// <summary>
        /// Método de consulta por filtros para la entidad TablaParametrica
        /// </summary>
        /// <param name="pCodigoTablaParametrica"></param>
        /// <param name="pNombreTablaParametrica"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TablaParametrica> ConsultarTablaParametricas(String pCodigoTablaParametrica, String pNombreTablaParametrica, Boolean? pEstado, bool pEsPrecontractual)
        {
            try
            {
                return vTablaParametricaBLL.ConsultarTablaParametricas(pCodigoTablaParametrica, pNombreTablaParametrica, pEstado, pEsPrecontractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region LugarEjecucionContrato
        /// <summary>
        /// Método de inserción para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int InsertarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoBLL.InsertarLugarEjecucionContrato(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int ModificarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoBLL.ModificarLugarEjecucionContrato(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int EliminarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoBLL.EliminarLugarEjecucionContrato(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdLugarEjecucionContratos"></param>
        public LugarEjecucionContrato ConsultarLugarEjecucionContrato(int pIdLugarEjecucionContratos)
        {
            try
            {
                return vLugarEjecucionContratoBLL.ConsultarLugarEjecucionContrato(pIdLugarEjecucionContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdRegional"></param>
        /// <param name="pNivelNacional"></param>
        public List<LugarEjecucionContrato> ConsultarLugarEjecucionContratos(int? pIdContrato, int? pIdDepartamento, int? pIdRegional, Boolean? pNivelNacional)
        {
            try
            {
                return vLugarEjecucionContratoBLL.ConsultarLugarEjecucionContratos(pIdContrato, pIdDepartamento, pIdRegional, pNivelNacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los lugares de ejecución asociados a un contrato
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado con los lugares obtenidos de la base de datos</returns>
        public List<LugarEjecucionContrato> ConsultarLugaresEjecucionContrato(int pIdContrato)
        {
            try
            {
                return vLugarEjecucionContratoBLL.ConsultarLugaresEjecucionContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LugarEjecucionContrato> ConsultarLugaresEjecucionContratoHistorico(int pIdContrato)
        {
            try
            {
                return vLugarEjecucionContratoBLL.ConsultarLugaresEjecucionContratoHistorico(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region LugarEjecucion
        /// <summary>
        /// Método de inserción para la entidad LugarEjecucion
        /// </summary>
        /// <param name="pLugarEjecucion"></param>
        /// <returns></returns>
        public int InsertarLugarEjecucion(LugarEjecucion pLugarEjecucion)
        {
            try
            {
                return vLugarEjecucionBLL.InsertarLugarEjecucion(pLugarEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad LugarEjecucion
        /// </summary>
        /// <param name="IdContratoLugarEjecucion"></param>
        /// <returns></returns>
        public List<LugarEjecucion> ConsultarLugarEjecucions(int IdContratoLugarEjecucion)
        {
            try
            {
                return vLugarEjecucionBLL.ConsultarLugarEjecucions(IdContratoLugarEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad LugarEjecucion
        /// </summary>
        /// <param name="pLugarEjecucion"></param>
        /// <returns></returns>
        public int EliminarLugaresEjecucion(LugarEjecucion pLugarEjecucion)
        {
            try
            {
                return vLugarEjecucionBLL.EliminarLugarEjecucion(pLugarEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region UnidadContrato
        /// <summary>
        /// Método de inserción para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int InsertarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                return vUnidadMedidaBLL.InsertarUnidadMedida(pUnidadMedida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int ModificarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                return vUnidadMedidaBLL.ModificarUnidadMedida(pUnidadMedida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int EliminarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                return vUnidadMedidaBLL.EliminarUnidadMedida(pUnidadMedida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad UnidadMedida
        /// </summary>
        /// <param name="pIdNumeroContrato"></param>
        /// <returns></returns>
        public UnidadMedida ConsultarUnidadMedida(int pIdNumeroContrato)
        {
            try
            {
                return vUnidadMedidaBLL.ConsultarUnidadMedida(pIdNumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad UnidadMedida
        /// </summary>
        /// <param name="pIdNumeroContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pFechaInicioEjecuciónContrato"></param>
        /// <param name="pFechaTerminacionInicialContrato"></param>
        /// <param name="pFechaFinalTerminacionContrato"></param>
        /// <returns></returns>
        public List<UnidadMedida> ConsultarUnidadMedidas(int? pIdNumeroContrato, String pNumeroContrato, DateTime? pFechaInicioEjecuciónContrato, DateTime? pFechaTerminacionInicialContrato, DateTime? pFechaFinalTerminacionContrato)
        {
            try
            {
                return vUnidadMedidaBLL.ConsultarUnidadMedidas(pIdNumeroContrato, pNumeroContrato, pFechaInicioEjecuciónContrato, pFechaTerminacionInicialContrato, pFechaFinalTerminacionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Consultar Regional
        public List<Regional> ConsultarRegionals(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                return vRegionalTipoContrato.ConsultarRegionals(pCodigoRegional, pNombreRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region AporteContrato
        /// <summary>
        /// Método de inserción para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int InsertarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                return vAporteContratoBLL.InsertarAporteContrato(pAporteContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarAporteContratoValor(int pIdContrato)
        {
            try
            {
                return vAporteContratoBLL.EliminarAporteContratoValor(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarAporteContratoValor(int pIdContrato, decimal pValorAporte, string usuarioModifica)
        {
            try
            {
                return vAporteContratoBLL.ModificarAporteContratoValor(pIdContrato, pValorAporte, usuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int ModificarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                return vAporteContratoBLL.ModificarAporteContrato(pAporteContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int EliminarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                return vAporteContratoBLL.EliminarAporteContrato(pAporteContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int EliminarAportesCofinanciacionContrato(int idContrato)
        {
            try
            {
                return vAporteContratoBLL.EliminarAportesCofinanciacionContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int EliminarAportesEspecieICBFContrato(int idContrato)
        {
            try
            {
                return vAporteContratoBLL.EliminarAportesEspecieICBFContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AporteContrato
        /// </summary>
        /// <param name="pIdAporteContrato"></param>
        public AporteContrato ConsultarAporteContrato(int pIdAporteContrato)
        {
            try
            {
                return vAporteContratoBLL.ConsultarAporteContrato(pIdAporteContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AporteContrato
        /// </summary>
        /// <param name="pAportanteICBF"></param>
        /// <param name="pNumeroIdentificacionICBF"></param>
        /// <param name="pValorAporte"></param>
        /// <param name="pDescripcionAporte"></param>
        /// <param name="pAporteEnDinero"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        /// <param name="pFechaRP"></param>
        /// <param name="pNumeroRP"></param>
        /// <param name="pEstado"></param>
        public List<AporteContrato> ConsultarAporteContratos(Boolean? pAportanteICBF, String pNumeroIdentificacionICBF, Decimal? pValorAporte, String pDescripcionAporte, Boolean? pAporteEnDinero, int? pIdContrato, int? pIDEntidadProvOferente, DateTime? pFechaRP, String pNumeroRP, Boolean? pEstado)
        {
            try
            {
                return vAporteContratoBLL.ConsultarAporteContratos(pAportanteICBF, pNumeroIdentificacionICBF, pValorAporte, pDescripcionAporte, pAporteEnDinero, pIdContrato, pIDEntidadProvOferente, pFechaRP, pNumeroRP, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de un contratista asociado a un tipo y valor de aporte
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public bool ExisteContratoAporte(int pIdContrato, Boolean pAporteEnDinero, Decimal pValorAporte, bool pAportanteICBF)
        {
            try
            {
                return vAporteContratoBLL.ExisteContratoAporte(pIdContrato, pAporteEnDinero, pValorAporte, pAportanteICBF)
                ;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información de los aportes asociados a un contrato seleccionados a partir de los filtros de entrada
        /// (Página Registro Contratos)
        /// </summary>
        /// <param name="pAportanteICBF">Booleano que indica si el aporte es hecho por el ICBF o no</param>
        /// <param name="pIdContrato">Entero con el identificador del contrato al que se asocia el aporte</param>
        /// <param name="pIDEntidadProvOferente">Entero con el identificador del contratista que realiza el aporte</param>
        /// <param name="pEstado">Booleano que indica el estado del aporte</param>
        /// <returns>Listado de aportes obtenidos de la base de datos que coinciden con los filtros</returns>
        public List<AporteContrato> ObtenerAportesContrato(Boolean? pAportanteICBF, int? pIdContrato, int? pIDEntidadProvOferente, Boolean? pEstado)
        {
            try
            {
                return vAporteContratoBLL.ObtenerAportesContrato(pAportanteICBF, pIdContrato, pIDEntidadProvOferente, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="esAdicion"></param>
        /// <param name="idModificacion"></param>
        /// <param name="idPlanCompras"></param>
        /// <returns></returns>
        public AporteContrato ObtenerAporteModificacion(bool esAdicion, int idModificacion, int idPlanCompras)
        {
            try
            {
                return vAporteContratoBLL.ObtenerAporteModificacion(esAdicion, idModificacion, idPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public decimal ObtenerValorTotalPlanComprasContrato(int idPlanComprasContrato)
        {
            try
            {
                return vAporteContratoBLL.ObtenerValorTotalPlanComprasContrato(idPlanComprasContrato);        
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        /// <summary>
        /// ICBF/SSII
        /// Obtiene los aportes que se le realizan a un contrato.
        /// </summary>
        /// <param name="pIdContrato">
        ///  Id del contrato aosciado
        /// </param>
        /// <returns>
        /// Listado de aportes asociados al contrato.
        /// </returns>
        public List<AporteContrato> ObtenerAportesContrato(int pIdContrato, bool adiciones)
        {
            try
            {
                return vAporteContratoBLL.ObtenerAportesContrato(pIdContrato, adiciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// ICBF/SSII
        /// Obtiene los aportes que se le realizan a un contrato.
        /// </summary>
        /// <param name="pIdContrato">
        ///  Id del contrato aosciado
        /// </param>
        /// <returns>
        /// Listado de aportes asociados al contrato.
        /// </returns>
        public List<AporteContrato> ObtenerAportesContratoAdicionReduccion(int pidModificacion, bool esAdicion)
        {
            try
            {
                return vAporteContratoBLL.ObtenerAportesContratoAdicionReduccion(pidModificacion, esAdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// ICBF/SSII
        /// Obtiene los aportes que se le realizan a un contrato.
        /// </summary>
        /// <param name="pIdContrato">
        ///  Id del contrato aosciado
        /// </param>
        /// <returns>
        /// Listado de aportes asociados al contrato.
        /// </returns>
        public List<AporteContrato> ObtenerAportesContratoActuales(int pIdContrato)
        {
            try
            {
                return vAporteContratoBLL.ObtenerAportesContratoActuales(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Aporte Adicion

        public int CrearAportesAdicion(int iddetCons, int idAdicion, int idAportePadre, decimal valorAporte, string usuario)
        {
            try
            {
                return vAporteContratoBLL.CrearAportesAdicion(iddetCons, idAdicion, idAportePadre, valorAporte, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int actualizarAportesAdicion(int idAdicion, decimal valorAporte, int idAportePadre, string usuario)
        {
            try
            {
                return vAporteContratoBLL.actualizarAportesAdicion(idAdicion, valorAporte, idAportePadre, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public KeyValuePair<int, int> CrearAporteDineroAdicion(int idDetCons, decimal valorAporte, string usuario, bool esAdicion, int idAporte, int idplanCompras)
        {
            try
            {
                return vAporteContratoBLL.CrearAporteDineroAdicion(idDetCons, valorAporte, usuario, esAdicion, idAporte, idplanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarAporteDineroAdicion(int idModificacion, decimal valorAporte, string usuario, bool esAdicion, int idPlanCompras, int idAporte)
        {
            try
            {
                return vAporteContratoBLL.ActualizarAporteDineroAdicion(idModificacion, valorAporte, usuario, esAdicion, idPlanCompras, idAporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion 

        #region Aportes Reduccion

        public int CrearAportesReduccion(int iddetCons, int idReduccion, int idAportePadre, decimal valorAporte, string usuario)
        {
            try
            {
                return vAporteContratoBLL.CrearAportesReduccion(iddetCons, idReduccion, idAportePadre, valorAporte, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarAportesReduccion(int idReduccion, decimal valorAporte, int idAportePadre, string usuario)
        {
            try
            {
                return vAporteContratoBLL.ActualizarAportesReduccion(idReduccion, valorAporte, idAportePadre, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ProveedoresContratos

        /// <summary>
        /// Método de inserción para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedoresContratos"></param>
        public int InsertarProveedoresContratos(ProveedoresContratos pProveedoresContratos)
        {
            try
            {
                return vProveedoresContratosBLL.InsertarProveedoresContratos(pProveedoresContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdContrato"></param>
        public List<ProveedoresContratos> ConsultarProveedoresContratoss(int? pIdProveedores, int? pIdContrato)
        {
            try
            {
                return vProveedoresContratosBLL.ConsultarProveedoresContratoss(pIdProveedores, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos por forma de pago y id proveedor
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdFormapago"></param>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public Boolean ConsultarProveedoresContratosFormaPago(int? pIdProveedores, int? pIdFormapago, int? pIdContrato)
        {
            try
            {
                return vProveedoresContratosBLL.ConsultarProveedoresContratosFormaPago(pIdProveedores, pIdFormapago, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Proveedores_Contratos
        /// <summary>
        /// Método de inserción para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int InsertarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                return vProveedores_ContratosBLL.InsertarProveedores_Contratos(pProveedores_Contratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int ModificarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                return vProveedores_ContratosBLL.ModificarProveedores_Contratos(pProveedores_Contratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int EliminarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                return vProveedores_ContratosBLL.EliminarProveedores_Contratos(pProveedores_Contratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedoresContratos"></param>
        public Proveedores_Contratos ConsultarProveedores_Contratos(int pIdProveedoresContratos)
        {
            try
            {
                return vProveedores_ContratosBLL.ConsultarProveedores_Contratos(pIdProveedoresContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdContrato"></param>
        public List<Proveedores_Contratos> ConsultarProveedores_Contratoss(int? pIdProveedores, int? pIdContrato)
        {
            try
            {
                return vProveedores_ContratosBLL.ConsultarProveedores_Contratoss(pIdProveedores, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta de la lista de proveedores asociados a un contrato
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdContrato"></param>
        public List<Proveedores_Contratos> ConsultarContratistasContratos(int? pcontrato)
        {
            try
            {
                return vProveedores_ContratosBLL.ConsultarContratistasContratos(pcontrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica existencia de contrato ya asociado a un proveedor
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public KeyValuePair<int,string> ExisteProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                return vProveedores_ContratosBLL.ExisteProveedores_Contratos(pProveedores_Contratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la informacion de contratistas relacionados con un contrato
        /// </summary>
        /// <param name="pContrato">Entero con el identificador del contrato</param>
        /// <param name="pIntegrantesUnionTemporal">Booleano que indica si se obtiene la información de contratistas o la de los integrantes 
        /// del consorcio o union temporal asociados al contrato</param>
        /// <returns>Listado de contratistas obtenidos de la base de datos</returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContrato(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vProveedores_ContratosBLL.ObtenerProveedoresContrato(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContrato"></param>
        /// <param name="pIntegrantesUnionTemporal"></param>
        /// <returns></returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContratoActuales(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vProveedores_ContratosBLL.ObtenerProveedoresContratoActuales(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Proveedores_Contratos> ObtenerProveedoresContratoMigrados(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vProveedores_ContratosBLL.ObtenerProveedoresContratoMigrados(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContrato"></param>
        /// <param name="pIntegrantesUnionTemporal"></param>
        /// <returns></returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContratoHistorico(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vProveedores_ContratosBLL.ObtenerProveedoresContratoHistorico(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region InformacionPresupuestal
        /// <summary>
        /// Método de inserción para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int InsertarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                return vInformacionPresupuestalBLL.InsertarInformacionPresupuestal(pInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int ModificarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                return vInformacionPresupuestalBLL.ModificarInformacionPresupuestal(pInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int EliminarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                return vInformacionPresupuestalBLL.EliminarInformacionPresupuestal(pInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pIdInformacionPresupuestal"></param>
        /// <returns></returns>
        public InformacionPresupuestal ConsultarInformacionPresupuestal(int pIdInformacionPresupuestal)
        {
            try
            {
                return vInformacionPresupuestalBLL.ConsultarInformacionPresupuestal(pIdInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pFechaExpedicionCDP"></param>
        /// <returns></returns>
        public List<InformacionPresupuestal> ConsultarInformacionPresupuestals(String pNumeroCDP, DateTime? pFechaExpedicionCDP)
        {
            try
            {
                return vInformacionPresupuestalBLL.ConsultarInformacionPresupuestals(pNumeroCDP, pFechaExpedicionCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region InformacionPresupuestalRP
        /// <summary>
        /// Método de inserción para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int InsertarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                return vInformacionPresupuestalRP.InsertarInformacionPresupuestalRP(pInformacionPresupuestalRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int ModificarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                return vInformacionPresupuestalRP.ModificarInformacionPresupuestalRP(pInformacionPresupuestalRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int EliminarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                return vInformacionPresupuestalRP.EliminarInformacionPresupuestalRP(pInformacionPresupuestalRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pIdInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public InformacionPresupuestalRP ConsultarInformacionPresupuestalRP(int pIdInformacionPresupuestalRP)
        {
            try
            {
                return vInformacionPresupuestalRP.ConsultarInformacionPresupuestalRP(pIdInformacionPresupuestalRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pFechaSolicitudRP"></param>
        /// <param name="pNumeroRP"></param>
        /// <returns></returns>
        public List<InformacionPresupuestalRP> ConsultarInformacionPresupuestalRPs(DateTime? pFechaSolicitudRP, String pNumeroRP)
        {
            try
            {
                return vInformacionPresupuestalRP.ConsultarInformacionPresupuestalRPs(pFechaSolicitudRP, pNumeroRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region RelacionarContratistas
        /// <summary>
        /// Método de inserción para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int InsertarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                return vRelacionarContratistas.InsertarRelacionarContratistas(pRelacionarContratistas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int ModificarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                return vRelacionarContratistas.ModificarRelacionarContratistas(pRelacionarContratistas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int EliminarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                return vRelacionarContratistas.EliminarRelacionarContratistas(pRelacionarContratistas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pIdContratistaContrato"></param>
        /// <returns></returns>
        public RelacionarContratistas ConsultarRelacionarContratistas(int pIdContratistaContrato)
        {
            try
            {
                return vRelacionarContratistas.ConsultarRelacionarContratistas(pIdContratistaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pIdContratistaContrato"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pClaseEntidad"></param>
        /// <param name="pPorcentajeParticipacion"></param>
        /// <param name="pEstadoIntegrante"></param>
        /// <param name="pNumeroIdentificacionRepresentanteLegal"></param>
        /// <returns></returns>
        public List<RelacionarContratistas> ConsultarRelacionarContratistass(int? pIdContratistaContrato, int? pIdContrato, long pNumeroIdentificacion, String pClaseEntidad, int? pPorcentajeParticipacion, Boolean pEstadoIntegrante, long pNumeroIdentificacionRepresentanteLegal)
        {
            try
            {
                return vRelacionarContratistas.ConsultarRelacionarContratistass(pIdContratistaContrato, pIdContrato, pNumeroIdentificacion, pClaseEntidad, pPorcentajeParticipacion, pEstadoIntegrante, pNumeroIdentificacionRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region EstadoSolicitudCompras

        /// <summary>
        /// Método de inserción para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int InsertarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasBLL.InsertarEstadoSolcitudModPlanCompras(pEstadoSolcitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int ModificarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasBLL.ModificarEstadoSolcitudModPlanCompras(pEstadoSolcitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int EliminarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasBLL.EliminarEstadoSolcitudModPlanCompras(pEstadoSolcitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pIdEstadoSolicitud"></param>
        public EstadoSolcitudModPlanCompras ConsultarEstadoSolcitudModPlanCompras(String pIdEstadoSolicitud)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasBLL.ConsultarEstadoSolcitudModPlanCompras(pIdEstadoSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pCodEstado"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pActivo"></param>
        public List<EstadoSolcitudModPlanCompras> ConsultarEstadoSolcitudModPlanComprass(String pCodEstado, String pDescripcion, Boolean? pActivo)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasBLL.ConsultarEstadoSolcitudModPlanComprass(pCodEstado, pDescripcion, pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Proveedores_Contratistas

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratista
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pIdTercero"></param>
        /// <param name="pNombreTipoPersona"></param>
        /// <param name="pCodDocumento"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pRazonsocial"></param>
        /// <param name="pNumeroidentificacionRepLegal"></param>
        /// <param name="pRazonsocialRepLegal"></param>
        public List<Proveedores_Contratista> ConsultarProveedores_Contratistas(int? pIdEntidad, int? pIdTercero, String pNombreTipoPersona, String pCodDocumento, String pNumeroIdentificacion, String pRazonsocial, String pNumeroidentificacionRepLegal, String pRazonsocialRepLegal)
        {
            try
            {
                return vProveedores_ContratistaBLL.ConsultarProveedores_Contratistas(pIdEntidad, pIdTercero, pNombreTipoPersona, pCodDocumento, pNumeroIdentificacion, pRazonsocial, pNumeroidentificacionRepLegal, pRazonsocialRepLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ContratosCDP
        /// <summary>
        /// Método de inserción para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP">Instancoa con el registro de contratosCDP</param>
        public int InsertarContratosCDP(ContratosCDP pContratosCDP)
        {
            try
            {
                return vContratosCDPBLL.InsertarContratosCDP(pContratosCDP);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP">Instancoa con el registro de contratosCDP</param>
        public int InsertarContratosCDP(ContratosCDP pContratosCDP, DataTable vDTRubros)
        {
            try
            {
                return vContratosCDPBLL.InsertarContratosCDP(pContratosCDP, vDTRubros);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de eliminar para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP"></param>
        /// <returns></returns>
        public int EliminarContratosCDP(ContratosCDP pContratosCDP)
        {
            try
            {
                return vContratosCDPBLL.EliminarContratosCDP(pContratosCDP);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarContratosCDPVigencias(ContratosCDP pContratosCDP, int pIdVigencia)
        {
            try
            {
                return vContratosCDPBLL.EliminarContratosCDPVigencias(pContratosCDP, pIdVigencia);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de consulta CDPs asociados a un contrato
        /// 01-07-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <returns>Lista de entidades ContratosCDP con la información obtenida de la base de datos</returns>
        public List<ContratosCDP> ConsultarContratosCDP(int pIdContrato)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDP(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de consulta CDPs asociados a un contrato
        /// 01-07-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <returns>Lista de entidades ContratosCDP con la información obtenida de la base de datos</returns>
        public List<ContratosCDP> ConsultarContratosCDPUnificado(int pIdContrato)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPUnificado(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<ContratosCDP> ConsultarContratosCDPRubroUnificado(int pIdContrato)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPRubroUnificado(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de consulta CDPs asociados a un contrato
        /// 01-07-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <returns>Lista de entidades ContratosCDP con la información obtenida de la base de datos</returns>
        public List<ContratosCDP> ConsultarContratosCDPLinea(int pIdContrato)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPLinea(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato">
        /// 
        /// </param>
        /// <returns></returns>
        public List<ContratosCDP> ConsultarContratosCDPRubroLinea(int pIdContrato)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPRubroLinea(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de consulta CDPs asociados a un contrato
        /// 01-07-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <returns>Lista de entidades ContratosCDP con la información obtenida de la base de datos</returns>
        public List<ContratosCDP> ConsultarContratosCDPLineaVF(int pIdContrato, bool EsVigenciaFutura, int? aniovigencia)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPLineaVF(pIdContrato,EsVigenciaFutura,aniovigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato">
        /// 
        /// </param>
        /// <returns></returns>
        public List<ContratosCDP> ConsultarContratosCDPRubroLineaVF(int pIdContrato, bool EsVigenciaFutura, int? aniovigencia)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPRubroLineaVF(pIdContrato,EsVigenciaFutura,aniovigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<ContratosCDP> ConsultarContratosCDPSimple(int pIdContrato)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPSimple(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<ContratosCDP> ConsultarContratosCDPRubroSimple(int pIdContrato)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPRubroSimple(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<ContratosCDP> ConsultarContratosCDPVigencias(int pIdContrato, bool pEsVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                return vContratosCDPBLL.ConsultarContratosCDPVigencias(pIdContrato, pEsVigenciaFutura, pAnioVigencia);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion 

        #region LupaRegInfoPresupuestal

        /// <summary>
        /// Método de consulta por filtros para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestals(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, Decimal? pValorTotalDesde, Decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarRegistroInformacionPresupuestals(pVigenciaFiscal, pRegionalICBF, pNumeroCDP, pArea, pValorTotalDesde, pValorTotalHasta, pFechaCDPDesde, pFechaCDPHasta, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalUnificado(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, Decimal? pValorTotalDesde, Decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarRegistroInformacionPresupuestalUnificado(pVigenciaFiscal, pRegionalICBF, pNumeroCDP, pArea, pValorTotalDesde, pValorTotalHasta, pFechaCDPDesde, pFechaCDPHasta, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalSimple(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, Decimal? pValorTotalDesde, Decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarRegistroInformacionPresupuestalSimple(pVigenciaFiscal, pRegionalICBF, pNumeroCDP, pArea, pValorTotalDesde, pValorTotalHasta, pFechaCDPDesde, pFechaCDPHasta, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalSimpleRubro(int pVigenciaFiscal, int pRegionalICBF, string pNumeroCDP)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarRegistroInformacionPresupuestalSimpleRubro(pVigenciaFiscal, pRegionalICBF, pNumeroCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalUnificadoRubro(int pVigenciaFiscal, int pRegionalICBF, string pNumeroCDP)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarRegistroInformacionPresupuestalUnificadoRubro(pVigenciaFiscal, pRegionalICBF, pNumeroCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public KeyValuePair<int,ConsultaCDPMaestro> ConsultarCDPConRubros(int pVigenciaFiscal, int pRegionalICBF, string pNumeroCDP, int? pArea, Decimal? pValorTotalDesde, Decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarCDPConRubros(pVigenciaFiscal, pRegionalICBF, pNumeroCDP, pArea, pValorTotalDesde, pValorTotalHasta, pFechaCDPDesde, pFechaCDPHasta, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet 
        /// Método de consulta por filtros de areas internas
        /// 4-Feb-2014
        /// </summary>
        /// <param name="pRegionalICBF">vlor entero con el codigo codigo de la regional</param>
        /// <returns>Lista con las instancias que contiene las areas, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<AreasInternas> ConsultarAreasInternas(int? pRegionalICBF)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarAreasInternas(pRegionalICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet 
        /// Método de consulta los contaros relacionados a un número de CDP
        /// 26-05-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdCDP">Valor entero con el id del CDP</param>
        /// <returns></returns>
        public bool ConsultarContratosCDP(int pIdContrato, int pIdCDP)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarContratosCDP(pIdContrato, pIdCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalExiste(int numeroCDP, int pIdEtlCdp)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarRegistroInformacionPresupuestals(numeroCDP, pIdEtlCdp);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroCDP"></param>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public int ConsultarExisteCPDAsociadoContrato(int numeroCDP, DateTime fecha, int idRegional)
        {
            try
            {
                return vRegistroInformacionPresupuestalBLL.ConsultarExisteCPDAsociadoContrato(numeroCDP, fecha,idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region PlanDeCompras

        /// <summary>
        /// Método de inserción para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int InsertarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                return vPlanDeComprasBLL.InsertarPlanDeCompras(pPlanDeCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int ModificarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                return vPlanDeComprasBLL.ModificarPlanDeCompras(pPlanDeCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int EliminarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                return vPlanDeComprasBLL.EliminarPlanDeCompras(pPlanDeCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pIdPlanDeCompras"></param>
        public PlanDeCompras ConsultarPlanDeCompras(int pIdPlanDeCompras, string pVigencia)
        {
            try
            {
                return vPlanDeComprasBLL.ConsultarPlanDeCompras(pIdPlanDeCompras, pVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pNumeroConsecutivo"></param>
        public List<PlanDeCompras> ConsultarPlanDeComprass(int? pNumeroConsecutivo)
        {
            try
            {
                return vPlanDeComprasBLL.ConsultarPlanDeComprass(pNumeroConsecutivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region SolicitudModPlanCompras

        /// <summary>
        /// Método de inserción para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int InsertarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                return vSolicitudModPlanComprasBLL.InsertarSolicitudModPlanCompras(pSolicitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int ModificarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                return vSolicitudModPlanComprasBLL.ModificarSolicitudModPlanCompras(pSolicitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int EliminarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                return vSolicitudModPlanComprasBLL.EliminarSolicitudModPlanCompras(pSolicitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pIdSolicitudModPlanCompra"></param>
        public SolicitudModPlanCompras ConsultarSolicitudModPlanCompras(int pIdSolicitudModPlanCompra)
        {
            try
            {
                return vSolicitudModPlanComprasBLL.ConsultarSolicitudModPlanCompras(pIdSolicitudModPlanCompra);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroSolicitud"></param>
        /// <param name="pVigencia">Año de vigencia</param>
        /// <returns></returns>
        public List<SolicitudModPlanCompras> ConsultarSolicitudModPlanComprass(int? pNumeroConsecutivoPlanCompras, int? pIdContrato, int? pNumeroSolicitud, string pVigencia)
        {
            try
            {
                return vSolicitudModPlanComprasBLL.ConsultarSolicitudModPlanComprass(pNumeroConsecutivoPlanCompras, pIdContrato, pNumeroSolicitud, pVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de una exolicitud en estado
        /// </summary>
        /// <param name="pIdEstado"></param>
        /// <param name="NumeroConsecutivoPlanCompras"></param>
        /// <returns></returns>
        public bool ExisteEnviadoSolicitudModPlanCompras(string pCodEstado, int NumeroConsecutivoPlanCompras, string pVigenciapc)
        {
            try
            {
                return vSolicitudModPlanComprasBLL.ExisteEnviadoSolicitudModPlanCompras(pCodEstado, NumeroConsecutivoPlanCompras, pVigenciapc);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region PlanDeComprasContratos
        /// <summary>
        /// Método de inserción para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int InsertarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                return vPlanDeComprasContratosBLL.InsertarPlanDeComprasContratos(pPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int ModificarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                return vPlanDeComprasContratosBLL.ModificarPlanDeComprasContratos(pPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int EliminarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                return vPlanDeComprasContratosBLL.EliminarPlanDeComprasContratos(pPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pIDPlanDeComprasContratos"></param>
        public PlanDeComprasContratos ConsultarPlanDeComprasContratos(int pIDPlanDeComprasContratos)
        {
            try
            {
                return vPlanDeComprasContratosBLL.ConsultarPlanDeComprasContratos(pIDPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIDPlanDeCompras"></param>
        public List<PlanDeComprasContratos> ConsultarPlanDeComprasContratoss(int? pIdContrato, int? pIDPlanDeCompras)
        {
            try
            {
                return vPlanDeComprasContratosBLL.ConsultarPlanDeComprasContratoss(pIdContrato, pIDPlanDeCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlanDeComprasContratos> ConsultarPlanDeComprasContratosVigencias(int? pIdContrato, int? pIDPlanDeCompras, bool? pesVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                return vPlanDeComprasContratosBLL.ConsultarPlanDeComprasContratosVigencias(pIdContrato, pIDPlanDeCompras, pesVigenciaFutura, pAnioVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool VerificarExistenciaModificacionProducto(int IDPlanDeComprasContratos, string IdProducto, bool esAdicion, int idModificacion)
        {
            bool isValid = false;

            try
            {
                isValid = vPlanDeComprasContratosBLL.VerificarExistenciaModificacionProducto(IDPlanDeComprasContratos, IdProducto, esAdicion, idModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return isValid;
        }

        public bool VerificarExistenciaModificacionRubro(int IDPlanDeComprasContratos, string IdRubro, bool esAdicion, int idModificacion)
        {
            bool isValid = false;

            try
            {
                isValid = vPlanComprasRubrosCDPBll.VerificarExistenciaModificacionRubro(IDPlanDeComprasContratos, IdRubro, esAdicion, idModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return isValid;
        }



        #endregion

        #region Empleado
        /// <summary>
        /// Método de consulta por filtros para la entidad Empleado
        /// </summary>
        /// <param name="pIdTipoIdentificacion">Cadena de texto con el identificador del tipo de identificación</param>
        /// <param name="pNumeroIdentificacion">Cadena de texto con el numero de identificación</param>
        /// <param name="pIdTipoVinculacionContractual">Cadena de texto con la descripcion del tipo de vinculación que es el mismo id</param>
        /// <param name="pIdRegional">Cadena de texto con el codigo de la regional</param>
        /// <param name="pPrimerNombre">Cadena de texto con el primer nombre del empleado</param>
        /// <param name="pSegundoNombre">Cadena de texto con el segundo nombre del empleado</param>
        /// <param name="pPrimerApellido">Cadena de texto con el primer apellido del empleado</param>
        /// <param name="pSegundoApellido">Cadena de texto con el segundo apellido del empleado</param>
        /// <returns>Lista con los empleados que coinciden con los filtros</returns>
        public List<Empleado> ConsultarEmpleados(String pIdTipoIdentificacion, String pNumeroIdentificacion, String pIdTipoVinculacionContractual,
            String pIdRegional, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                return vEmpleadoBLL.ConsultarEmpleados(pIdTipoIdentificacion, pNumeroIdentificacion, pIdTipoVinculacionContractual,
                    pIdRegional, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros de la tabla [KACTUS].[KPRODII].[dbo].[nm_tnomi]
        /// que contiene los tipos de vinculación contractual
        /// </summary>
        /// <param name="pIdTipoVinCont">Cadena de texto con el identificador numérico del tipo de vinculación</param>
        /// <param name="pTipoVincCont">Cadena de texto con la descripción del tipo de vinculación</param>
        /// <returns>Lista con los tipos de vinculación contractual encapsulados en entidades tipo empleado</returns>
        public List<Empleado> ConsultarTiposVinculacionContractual(String pIdTipoVinCont, String pTipoVincCont)
        {
            try
            {
                return vEmpleadoBLL.ConsultarTiposVinculacionContractual(pIdTipoVinCont, pTipoVincCont);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Consulta la información del empleado en Kactus a partir de los filtros dados
        /// </summary>
        /// <param name="pNumeroIdentificacion">Cadena de texto con el numero de identificacion</param>
        /// <param name="pRegional">Cadena de texto con el identificador de la regional</param>
        /// <param name="pDependencia">Cadena de texto con el identificador de la dependencia</param>
        /// <param name="pCargo">Cadena de texto con el identificador del cargo</param>
        /// <returns>Entidad Empleado con la información obtenida de la base de datos</returns>
        public Empleado ConsultarEmpleado(String pNumeroIdentificacion, String pRegional, String pDependencia, String pCargo)
        {
            try
            {
                return vEmpleadoBLL.ConsultarEmpleado(pNumeroIdentificacion, pRegional, pDependencia, pCargo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public Dictionary<string,string> ConsultarDependencias(string codigo, int cantidad)
        {
            try
            {
                return vEmpleadoBLL.ConsultarDependencias(codigo, cantidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ConsecutivoContratoRegionales


        /// <summary>
        /// Método de inserción para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int InsertarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                return vConsecutivoContratoRegionalesBLL.InsertarConsecutivoContratoRegionales(pConsecutivoContratoRegionales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int ModificarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                return vConsecutivoContratoRegionalesBLL.ModificarConsecutivoContratoRegionales(pConsecutivoContratoRegionales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int EliminarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                return vConsecutivoContratoRegionalesBLL.EliminarConsecutivoContratoRegionales(pConsecutivoContratoRegionales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public ConsecutivoContratoRegionales ConsultarConsecutivoContratoRegionales(int pIDConsecutivoContratoRegional)
        {
            try
            {
                return vConsecutivoContratoRegionalesBLL.ConsultarConsecutivoContratoRegionales(pIDConsecutivoContratoRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIdRegional"></param>
        /// <param name="pIdVigencia"></param>
        /// <param name="pConsecutivo"></param>
        public List<ConsecutivoContratoRegionales> ConsultarConsecutivoContratoRegionaless(int? pIdRegional, int? pIdVigencia, Decimal? pConsecutivo)
        {
            try
            {
                return vConsecutivoContratoRegionalesBLL.ConsultarConsecutivoContratoRegionaless(pIdRegional, pIdVigencia, pConsecutivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public bool EsMayorConsecutivoContratoRegionales(Decimal pConsecutivo, int pIdVigencia, int pIdRegional)
        {
            try
            {
                return vConsecutivoContratoRegionalesBLL.EsMayorConsecutivoContratoRegionales(pConsecutivo, pIdVigencia, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public bool ExisteConsecutivoContratoRegionales(Decimal pConsecutivo, int pIdVigencia, int pIdRegional)
        {
            try
            {
                return vConsecutivoContratoRegionalesBLL.ExisteConsecutivoContratoRegionales(pConsecutivo, pIdVigencia, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TipoSuperInter

        /// <summary>
        /// Método de consulta por filtros para la entidad TipoSuperInter
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pDescripcion"></param>
        public List<TipoSuperInter> ConsultarTipoSuperInters(String pCodigo, String pDescripcion)
        {
            try
            {
                return vTipoSuperInterBLL.ConsultarTipoSuperInters(pCodigo, pDescripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region SupervisorInterContratos

        /// <summary>
        /// Método de inserción para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int InsertarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoBLL.InsertarSupervisorInterContrato(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de inserción para la entidad SupervisorInterContrato - DirectorInterventoria
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        /// <returns></returns>
        public int InsertarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato, DirectorInterventoria pDirectorInterventoria)
        {
            try
            {
                return vSupervisorInterContratoBLL.InsertarSupervisorInterContrato(pSupervisorInterContrato, pDirectorInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int ModificarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoBLL.ModificarSupervisorInterContrato(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int EliminarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoBLL.EliminarSupervisorInterContrato(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        public SupervisorInterContrato ConsultarSupervisorInterContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterContratoBLL.ConsultarSupervisorInterContrato(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pFechaInicio"></param>
        /// <param name="pInactivo"></param>
        /// <param name="pIdentificacion"></param>
        /// <param name="pTipoIdentificacion"></param>
        /// <param name="pIDTipoSuperInter"></param>
        /// <param name="pIdNumeroContratoInterventoria"></param>
        /// <param name="pIDProveedoresInterventor"></param>
        /// <param name="pIDEmpleadosSupervisor"></param>
        /// <param name="pIdContrato"></param>
        public List<SupervisorInterContrato> ConsultarSupervisorInterContratos(DateTime? pFechaInicio, Boolean? pInactivo, String pIdentificacion, String pTipoIdentificacion, int? pIDTipoSuperInter, int? pIdNumeroContratoInterventoria, int? pIDProveedoresInterventor, int? pIDEmpleadosSupervisor, int? pIdContrato)
        {
            try
            {
                return vSupervisorInterContratoBLL.ConsultarSupervisorInterContratos(pFechaInicio, pInactivo, pIdentificacion, pTipoIdentificacion, pIDTipoSuperInter, pIdNumeroContratoInterventoria, pIDProveedoresInterventor, pIDEmpleadosSupervisor, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información relacionada con los supervisores/interventores asociados a un contrato
        /// dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public List<SupervisorInterContrato> ObtenerSupervisoresInterventoresContrato(int pIdContrato, bool? pDirectoresInterventoria)
        {
            try
            {
                return vSupervisorInterContratoBLL.ObtenerSupervisoresInterventoresContrato(pIdContrato, pDirectoresInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisorInterContrato> ObtenerSupervisoresInterventoresContratoMigrado(int pIdContrato)
        {
            try
            {
                return vSupervisorInterContratoBLL.ObtenerSupervisoresInterventoresContratoMigrado(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Efrain Diaz
        /// Obtiene la información relacionada con los supervisores/interventores 
        /// dado
        /// </summary>
        /// <param name="pNumeroIdentidicacion">Entero con el identificador Supervisor/Inerventor</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public SupervisorInterContrato ObtenerSupervisorInterventoresContratoDetalle(int pNumeroIdentidicacion,
                                                                                     bool? pDirectoresInterventoria)
        {
            try
            {
                return vSupervisorInterContratoBLL.ObtenerSupervisorInterventoresContratoDetalle(pNumeroIdentidicacion,
                                                                                     pDirectoresInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de inserción para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int InsertarSupervisorTemporal(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return new SupervisorInterContratoBLL().InsertarSupervisorTemporal(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int EliminarSupervisorTemporal(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return new SupervisorInterContratoBLL().EliminarSupervisorTemporal(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        /// <returns></returns>
        public SupervisorInterContrato ConsultarSupervisorTemporalContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                return new SupervisorInterContratoBLL().ConsultarSupervisorTemporalContrato(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void GenerarSupervisoresTemporales(int idContrato)
        {
            try
            {
                new SupervisorInterContratoBLL().GenerarSupervisoresTemporales(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información relacionada con los supervisores/interventores asociados a un contrato
        /// dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public List<SupervisorInterContrato> ObtenerSupervisoresTemporalesContrato(int pIdContrato, int? IdSupervisorAnterior)
        {
            try
            {
                return new SupervisorInterContratoBLL().ObtenerSupervisoresTemporalesContrato(pIdContrato, IdSupervisorAnterior);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información relacionada con los supervisores/interventores asociados a un contrato
        /// dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public List<SupervisorInterContrato> ObtenerSupervisoresHistoricoContrato(int pIdContrato)
        {
            try
            {
                return new SupervisorInterContratoBLL().ObtenerSupervisoresHistoricoContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Amparos Garantias
        /// <summary>
        /// Método de inserción para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int InsertarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                return vAmparosGarantiasBLL.InsertarAmparosGarantias(pAmparosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int ModificarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                return vAmparosGarantiasBLL.ModificarAmparosGarantias(pAmparosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int EliminarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                return vAmparosGarantiasBLL.EliminarAmparosGarantias(pAmparosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pIDAmparosGarantias"></param>
        public AmparosGarantias ConsultarAmparosGarantias(int pIDAmparosGarantias)
        {
            try
            {
                return vAmparosGarantiasBLL.ConsultarAmparosGarantias(pIDAmparosGarantias);
            }

            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pFechaVigenciaDesde"></param>
        /// <param name="pFechaVigenciaHasta"></param>
        /// <param name="pValorCalculoAsegurado"></param>
        /// <param name="pValorAsegurado"></param>
        public List<AmparosGarantias> ConsultarAmparosGarantiass(DateTime? pFechaVigenciaDesde, DateTime? pFechaVigenciaHasta, Decimal? pValorCalculoAsegurado, Decimal? pValorAsegurado, int? pIDGarantia)
        {
            try
            {
                return vAmparosGarantiasBLL.ConsultarAmparosGarantiass(pFechaVigenciaDesde, pFechaVigenciaHasta, pValorCalculoAsegurado, pValorAsegurado, pIDGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.Contrato ConsultarValoresContrato(int? pIdContrato)
        {
            try
            {
                return vAmparosGarantiasBLL.ConsultarValoresContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public AmparosGarantias ConsultarFechaVigenciaAmparoRelacionado(int? IdGarantia)
        {
            try
            {
                return vAmparosGarantiasBLL.ConsultarFechaVigenciaAmparoRelacionado(IdGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region SupervisorInterventor

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterventor
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pDescripcion"></param>
        public TipoSupervisorInterventor ConsultarSupervisorInterventorsID(int IDSuperinter)
        {
            try
            {
                return vSupervisorInterventorBLL.ConsultarSupervisorInterventorsID(IDSuperinter);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterventor
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pDescripcion"></param>
        public List<TipoSupervisorInterventor> ConsultarSupervisorInterventors(String pCodigo, String pDescripcion)
        {
            try
            {
                return vSupervisorInterventorBLL.ConsultarSupervisorInterventors(pCodigo, pDescripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Interventor> ConsultarInterventors(int? pIdTipoPersona, int? pIdTipoIdentificacion, String pNumeroIdentificacion, String pNombreRazonSocial)
        {
            try
            {
                return vSupervisorInterventorBLL.ConsultarInterventors(pIdTipoPersona, pIdTipoIdentificacion, pNumeroIdentificacion, pNombreRazonSocial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Supervisor> ConsultarSupervisors(String pIdTipoidentificacion, String pNumeroIdentificacion, String pIdTipoVinculacion, String pIdRegional,
                                                    String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                return vSupervisorInterventorBLL.ConsultarSupervisors(pIdTipoidentificacion, pNumeroIdentificacion, pIdTipoVinculacion, pIdRegional, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Interventor ConsultarInterventor(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterventorBLL.ConsultarInterventor(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Supervisor ConsultarSupervisor(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterventorBLL.ConsultarSupervisor(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Supervisor ConsultarSupervisorTemporal(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterventorBLL.ConsultarSupervisorTemporal(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarRolSuerpvisor(int idSupervisorInter, int idRol)
        {
            try
            {
                return vSupervisorInterventorBLL.ActualizarRolSuerpvisor(idSupervisorInter,idRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de modificación del campo FechaInicio para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato">Entidad con la información a actualizar</param>
        /// <returns>Resultado de la operación</returns>
        public int ActualizarFechaInicioSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoBLL.ActualizarFechaInicioSupervisorInterContrato(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region SupervisorDatos

        public List<SupervisorTipoIdentificacion> ConsultarSupervisorTipoIdentificacions()
        {
            try
            {
                return vSupervisorDatosBLL.ConsultarSupervisorTipoIdentificacions();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorTipoVinculacion
        /// </summary>
        public List<SupervisorTipoVinculacion> ConsultarSupervisorTipoVinculacions()
        {
            try
            {
                return vSupervisorDatosBLL.ConsultarSupervisorTipoVinculacions();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorRegional
        /// </summary>
        public List<SupervisorRegional> ConsultarSupervisorRegionals()
        {
            try
            {
                return vSupervisorDatosBLL.ConsultarSupervisorRegionals();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region SubComponentes

        public int InsertarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                return vSubComponenteBLL.InsertarSubComponente(pSubComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                return vSubComponenteBLL.ModificarSubComponente(pSubComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                return vSubComponenteBLL.EliminarSubComponente(pSubComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SubComponente ConsultarSubComponente(int pIdSubComponente)
        {
            try
            {
                return vSubComponenteBLL.ConsultarSubComponente(pIdSubComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SubComponente> ConsultarSubComponentes(String pNombreSubComponente, int? pIdComponente, int? pEstado)
        {
            try
            {
                return vSubComponenteBLL.ConsultarSubComponentes(pNombreSubComponente, pIdComponente, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Unidad Calculo
        /// <summary>
        /// Método de inserción para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int InsertarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                return vUnidadCalculoBLL.InsertarUnidadCalculo(pUnidadCalculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int ModificarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                return vUnidadCalculoBLL.ModificarUnidadCalculo(pUnidadCalculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int EliminarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                return vUnidadCalculoBLL.EliminarUnidadCalculo(pUnidadCalculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pIDUnidadCalculo"></param>
        public UnidadCalculo ConsultarUnidadCalculo(int pIDUnidadCalculo)
        {
            try
            {
                return vUnidadCalculoBLL.ConsultarUnidadCalculo(pIDUnidadCalculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pCodUnidadCalculo"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pInactivo"></param>
        public List<UnidadCalculo> ConsultarUnidadCalculos(String pCodUnidadCalculo, String pDescripcion, Boolean? pInactivo)
        {
            try
            {
                return vUnidadCalculoBLL.ConsultarUnidadCalculos(pCodUnidadCalculo, pDescripcion, pInactivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region ConsultarSuperInterContrato

        /// <summary>
        /// Método de consulta por id para la entidad ConsultarSuperInterContrato
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        public ConsultarSuperInterContrato ConsultarConsultarSuperInterContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vConsultarSuperInterContratoBLL.ConsultarConsultarSuperInterContrato(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ConsultarSuperInterContrato
        /// </summary>
        /// <param name="pIDSupervisorInterventor"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pTipoIdentificacion"></param>
        /// <param name="pRazonSocial"></param>
        /// <param name="pNombreSuperInterv"></param>
        /// <param name="pNumIdentifDirInterventoria"></param>
        /// <param name="pNombreDirInterventoria"></param>
        /// <param name="pTipoContrato"></param>
        /// <param name="pRegionalContrato"></param>
        /// <param name="pFechaSuscripcion"></param>
        /// <param name="pFechaTerminacion"></param>
        /// <param name="pFechaInicioSuperInterv"></param>
        /// <param name="pFechaFinalSuperInterv"></param>
        /// <param name="pFechaModSuperInterv"></param>
        /// <param name="pNumeroContratoInterventoria"></param>
        /// <param name="pTelefonoInterventoria"></param>
        /// <param name="pTelCelularInterventoria"></param>
        public List<ConsultarSuperInterContrato> ConsultarConsultarSuperInterContratos(String pCodSupervisorInterventor, String pNumeroContrato, String pNumeroIdentificacion, String pRazonSocial, String pNombreSuperInterv, String pNumIdentifDirInterventoria, String pNombreDirInterventoria)
        {
            try
            {
                return vConsultarSuperInterContratoBLL.ConsultarConsultarSuperInterContratos(pCodSupervisorInterventor, pNumeroContrato, pNumeroIdentificacion, pRazonSocial, pNombreSuperInterv, pNumIdentifDirInterventoria, pNombreDirInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ProfesionKactus
        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo de consulta por filtros para la entidad ProfesionKactus
        /// </summary>
        /// <param name="pModalidadAcademica">Cadena de texto con la modalidad academica asociada a la profesión</param>
        /// <param name="pDescripcion">Cadena de texto con la descripcion de la profesión</param>
        /// <param name="pCodigo">Cadena de texto con el codigo de la profesión</param>
        /// <param name="pEstado">Booleano con el estado de la profesión</param>
        /// <returns>Lista con las profesiones que coinciden con los filtros dados</returns>
        public List<ProfesionKactus> ConsultarProfesionesKactus(String pModalidadAcademica, String pDescripcion, String pCodigo, Boolean? pEstado)
        {
            try
            {
                return vProfesionKactusBLL.ConsultarProfesionesKactus(pModalidadAcademica, pDescripcion, pCodigo, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Archivos Garantías
        /// <summary>
        /// Método de inserción para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int InsertarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                return vArchivosGarantiasBLL.InsertarArchivosGarantias(pArchivosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int ModificarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                return vArchivosGarantiasBLL.ModificarArchivosGarantias(pArchivosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int EliminarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                return vArchivosGarantiasBLL.EliminarArchivosGarantias(pArchivosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pIDArchivosGarantias"></param>
        public ArchivosGarantias ConsultarArchivosGarantias(int pIDArchivosGarantias)
        {
            try
            {
                return vArchivosGarantiasBLL.ConsultarArchivosGarantias(pIDArchivosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pIDArchivo"></param>
        /// <param name="pIDGarantia"></param>
        public List<ArchivosGarantias> ConsultarArchivosGarantiass(int? pIDArchivo, int? pIDGarantia)
        {
            try
            {
                return vArchivosGarantiasBLL.ConsultarArchivosGarantiass(pIDArchivo, pIDGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Quita los acentos de una cadena de texto eliminando también espacios
        /// en blanco existentes en cualquier ubicación dentro de la cadena de
        /// texto dada
        /// 26-Dic-2013
        /// </summary>
        /// <param name="Texto">Cadena de texto a la que se van a quitar los acentos</param>
        /// <returns>Cadena texto sin acentos</returns>
        public string QuitarAcentos(string Texto)
        {
            return vArchivosGarantiasBLL.QuitarAcentos(Texto);
        }
        #endregion

        #region  Sucursal Aseguradora Contrato
        /// <summary>
        /// Método de inserción para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int InsertarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                return vSucursalAseguradoraContratoBLL.InsertarSucursalAseguradoraContrato(pSucursalAseguradoraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int ModificarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                return vSucursalAseguradoraContratoBLL.ModificarSucursalAseguradoraContrato(pSucursalAseguradoraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int EliminarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                return vSucursalAseguradoraContratoBLL.EliminarSucursalAseguradoraContrato(pSucursalAseguradoraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pIDSucursalAseguradoraContrato"></param>
        public SucursalAseguradoraContrato ConsultarSucursalAseguradoraContrato(int pIDSucursalAseguradoraContrato)
        {
            try
            {
                return vSucursalAseguradoraContratoBLL.ConsultarSucursalAseguradoraContrato(pIDSucursalAseguradoraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pIDDepartamento"></param>
        /// <param name="pIDMunicipio"></param>
        /// <param name="pNombre"></param>
        /// <param name="pIDZona"></param>
        /// <param name="pDireccionNotificacion"></param>
        /// <param name="pCorreoElectronico"></param>
        /// <param name="pIndicativo"></param>
        /// <param name="pTelefono"></param>
        /// <param name="pExtension"></param>
        /// <param name="pCelular"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        /// <param name="pCodigoSucursal"></param>
        public List<SucursalAseguradoraContrato> ConsultarSucursalAseguradoraContratos(int? pIDDepartamento, int? pIDMunicipio, String pNombre, int? pIDZona, String pDireccionNotificacion, String pCorreoElectronico, String pIndicativo, String pTelefono, String pExtension, String pCelular, int? pIDEntidadProvOferente, int? pCodigoSucursal)
        {
            try
            {
                return vSucursalAseguradoraContratoBLL.ConsultarSucursalAseguradoraContratos(pIDDepartamento, pIDMunicipio, pNombre, pIDZona, pDireccionNotificacion, pCorreoElectronico, pIndicativo, pTelefono, pExtension, pCelular, pIDEntidadProvOferente, pCodigoSucursal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Contratista Garantias
        /// <summary>
        /// Método de inserción para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int InsertarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                return vContratistaGarantiasBLL.InsertarContratistaGarantias(pContratistaGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int ModificarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                return vContratistaGarantiasBLL.ModificarContratistaGarantias(pContratistaGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int EliminarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                return vContratistaGarantiasBLL.EliminarContratistaGarantias(pContratistaGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarContratistaGarantias(int idGarantia)
        {
            try
            {
                return vContratistaGarantiasBLL.EliminarContratistaGarantias(idGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pIDContratista_Garantias"></param>
        public ContratistaGarantias ConsultarContratistaGarantias(int pIDContratista_Garantias)
        {
            try
            {
                return vContratistaGarantiasBLL.ConsultarContratistaGarantias(pIDContratista_Garantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pIDGarantia"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        public List<ContratistaGarantias> ConsultarContratistaGarantiass(int? pIDGarantia, int? pIDEntidadProvOferente)
        {
            try
            {
                return vContratistaGarantiasBLL.ConsultarContratistaGarantiass(pIDGarantia, pIDEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TipoContratoAsociado

        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo para obtener la información de la entidad Contrato Asociado a partir del identificador de tabla
        /// o el codigo del contrato asociado
        /// </summary>
        /// <param name="pContratoAsociado">Entidad con la información de identificador o código según la necesidad</param>
        /// <returns>Entidad con la información recuperada de la base de datos</returns>
        public TipoContratoAsociado IdentificadorCodigoTipoContratoAsociado(TipoContratoAsociado pTipoContratoAsociado)
        {
            try
            {
                return vTipoContratoAsociadoBLL.IdentificadorCodigoTipoContratoAsociado(pTipoContratoAsociado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Plan De Compras Contratos

        public int InsertarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                return vPlanComprasContratosBll.InsertarPlanComprasContratos(pPlanComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int ModificarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                return vPlanComprasContratosBll.ModificarPlanComprasContratos(pPlanComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int EliminarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                return vPlanComprasContratosBll.EliminarPlanComprasContratos(pPlanComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int EliminarPlanComprasPreContractual(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                return vPlanComprasContratosBll.EliminarPlanComprasPrecontractual(pPlanComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasContratos ConsultarPlanComprasContratos(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                return vPlanComprasContratosBll.ConsultarPlanComprasContratos(pIdProductoPlanCompraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pVigencia"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdPlanDeComprasContratos"></param>
        /// <param name="pCodigoRegional"></param>
        public List<PlanComprasContratos> ConsultarPlanComprasContratoss(int? pVigencia,
                                                                         int? pIdContrato,
                                                                         int? pIdPlanDeComprasContratos,
                                                                         int? pIdUsuario)
        {
            try
            {
                return vPlanComprasContratosBll.ConsultarPlanComprasContratoss(pVigencia,
                                                                                pIdContrato,
                                                                                pIdPlanDeComprasContratos,
                                                                                pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pIdValoresContrato"></param>
        public List<PlanComprasContratos> ConsultarValoresPlanComprasContratos(int? pIdValoresContrato)
        {
            try
            {
                return vPlanComprasContratosBll.ConsultarValoresPlanComprasContratos(pIdValoresContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vigencia"></param>
        /// <param name="codigoRegional"></param>
        /// <param name="trimestre"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int ActualizarInformacionEjecucionContrato(int vigencia, string codigoRegional, ReporteContraloraTrimestre trimestre, string usuario)
        {
            try
            {
                return vPlanComprasContratosBll.ActualizarInformacionEjecucionContrato(vigencia, codigoRegional, trimestre, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }


        #endregion

        #region Archivo

        /// <summary>
        /// Gonet
        /// Insertar Archivo nuevo
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>

        public int EliminarDocumentoAnexo(decimal pIdArchivo)
        {
            try
            {
                ArchivoBLL vArchivoBLL = new ArchivoBLL();
                return vArchivoBLL.EliminarDocumentoAnexo(pIdArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarDocumentoAnexoContrato(decimal pIdArchivo)
        {
            try
            {
                ArchivoBLL vArchivoBLL = new ArchivoBLL();
                return vArchivoBLL.EliminarDocumentoAnexoContrato(pIdArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int InsertarArchivo(ArchivoContrato pArchivo)
        {
            try
            {
                ArchivoBLL vArchivoBLL = new ArchivoBLL();
                return vArchivoBLL.InsertarArchivo(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarArchivoAnexo(ArchivoContrato pArchivo)
        {
            try
            {
                ArchivoBLL vArchivoBLL = new ArchivoBLL();
                return vArchivoBLL.InsertarArchivoAnexo(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public ArchivoContrato ConsultarArchivo(decimal pIdArchivo)
        {
            try
            {
                ArchivoBLL vArchivoBLL = new ArchivoBLL();
                return vArchivoBLL.ConsultarArchivo(pIdArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarArchivoContratoGeneral(ArchivoContrato pArchivo)
        {
            try
            {
                ArchivoBLL vArchivoBLL = new ArchivoBLL();
                return vArchivoBLL.InsertarArchivoContrato(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<ArchivoContrato> ConsultarArchivoTipoEstructurayContrato(int pIdcontrato, String pTipoEstructura, int idTipoEstructura = 0)
        {
            try
            {
                ArchivoBLL vArchivoBLL = new ArchivoBLL();
                return vArchivoBLL.ConsultarArchivoTipoEstructurayContrato(pIdcontrato, pTipoEstructura,idTipoEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ArchivoContrato> ConsultarArchivosPorContrato(int idContrato)
        {
            try
            {
                ArchivoBLL vArchivoBLL = new ArchivoBLL();
                return vArchivoBLL.ConsultarArchivosPorContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region SuscripcionContrato

        /// <summary>
        /// Método de inserción para la entidad ArchivoContrato
        /// </summary>
        /// <param name="pArchivoContrato"></param>
        /// <returns></returns>
        public int InsertarArchivoContrato(ArchivoContrato pArchivoContrato)
        {
            try
            {
                return vSuscripcionContratoBLL.InsertarArchivoContrato(pArchivoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivo sólo por Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdContrato">Valor entero Id Contrato</param>
        /// <returns>Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivo(int pIdContrato)
        {
            try
            {

                return vSuscripcionContratoBLL.ConsultarArchivo(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Permite eliminar un documento Archivo Contrato
        ///     -Genera auditoria
        /// 26-Jun-2013
        /// </summary>
        /// <param name="pArchivoContrato">Instancia que contiene la información del documento  
        /// a eliminar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int EliminarArchivoContrato(ArchivoContrato pArchivoContrato)
        {
            try
            {
                return vSuscripcionContratoBLL.EliminarArchivoContrato(pArchivoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="vFechaSuscripcion"></param>
        /// <param name="vIdEstadoContrato"></param>
        public int SuscribirContrato(int pIdContrato, string pUsuarioModifica, DateTime vFechaSuscripcion,
                                     int vIdEstadoContrato, string consecutivoContrato, DateTime? fechaFinalizacion, string vinculoSECOP)
        {
            try
            {
                return vSuscripcionContratoBLL.SuscribirContrato(pIdContrato, pUsuarioModifica, vFechaSuscripcion, vIdEstadoContrato,
                                                                 consecutivoContrato, fechaFinalizacion, vinculoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para actualizar el numero de contrato de la entidad contrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="pNumerContrato"></param>
        public int ActualizarNumeroContrato(int pIdContrato, string pUsuarioModifica, string pNumerContrato, string pConsecutivoSuscrito)
        {
            try
            {
                return vSuscripcionContratoBLL.ActualizarNumeroContrato(pIdContrato, pUsuarioModifica, pNumerContrato, pConsecutivoSuscrito);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar Archivo sólo por Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdVigencia">Valor entero Id Contrato</param>
        /// <param name="pIdRegional">Valor entero Id Regional</param>
        /// <returns>Entity Archivo</returns>
        public List<ConsecutivoContratoRegionales> ConsultarConsecutivoContratoRegionales(int pIdVigencia,
                                                                                          int pIdRegional)
        {
            try
            {
                return vSuscripcionContratoBLL.ConsultarConsecutivoContratoRegionales(pIdVigencia, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Suspensiones
        public int InsertarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                return vSuspensionesBLL.InsertarSuspensiones(pSuspensiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int AplicarSuspensiones(int pIdSuspension)
        {
            try
            {
                return vSuspensionesBLL.AplicarSuspensiones(pIdSuspension);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Suspensiones Suspensiones_DetalleConsModContractual_Consultar(int pIdSuspension)
        {
            try
            {
                return vSuspensionesBLL.Suspensiones_DetalleConsModContractual_Consultar(pIdSuspension);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                return vSuspensionesBLL.ModificarSuspensiones(pSuspensiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                return vSuspensionesBLL.EliminarSuspensiones(pSuspensiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Suspensiones ConsultarSuspensiones(int pIdSuspension)
        {
            try
            {
                return vSuspensionesBLL.ConsultarSuspensiones(pIdSuspension);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Suspensiones ConsultarCalculosSuspensiones(int pIdContrato, DateTime pFechaInicio, DateTime pFechaFin, int pValorConmutado)
        {
            try
            {
                return vSuspensionesBLL.ConsultarCalculosSuspensiones(pIdContrato, pFechaInicio, pFechaFin, pValorConmutado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Suspensiones> ConsultarSuspensioness(DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                return vSuspensionesBLL.ConsultarSuspensioness(pFechaInicio, pFechaFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Suspensiones> ConsultarSuspensionesPorDetalleModificacion(int? pDetalleConModificacion)
        {
            try
            {
                return vSuspensionesBLL.ConsultarSuspensionesPorDetalleModificacion(pDetalleConModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Suspensiones ConsultarSuspensionesPorDetalle(int? pDetalleConModificacion)
        {
            try
            {
                return vSuspensionesBLL.ConsultarSuspensionesPorDetalle(pDetalleConModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Estados Garantias
        /// <summary>
        /// Método de inserción para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int InsertarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                return vEstadosGarantiasBLL.InsertarEstadosGarantias(pEstadosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int ModificarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                return vEstadosGarantiasBLL.ModificarEstadosGarantias(pEstadosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int EliminarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                return vEstadosGarantiasBLL.EliminarEstadosGarantias(pEstadosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pIDEstadosGarantias"></param>
        public EstadosGarantias ConsultarEstadosGarantias(int pIDEstadosGarantias)
        {
            try
            {
                return vEstadosGarantiasBLL.ConsultarEstadosGarantias(pIDEstadosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pCodigoEstadoGarantia"></param>
        /// <param name="pDescripcionEstadoGarantia"></param>
        public List<EstadosGarantias> ConsultarEstadosGarantiass(String pCodigoEstadoGarantia, String pDescripcionEstadoGarantia)
        {
            try
            {
                return vEstadosGarantiasBLL.ConsultarEstadosGarantiass(pCodigoEstadoGarantia, pDescripcionEstadoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void EnviarAprobacionGarantia(int idGarantia, string estado)
        {
            try
            {
                vEstadosGarantiasBLL.EnviarAprobacionGarantia(idGarantia, estado);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region Plan Compras Productos

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosBll.InsertarPlanComprasProductos(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarProductoPlanComprasContrato(ProductoPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosBll.InsertarProductoPlanComprasContrato(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <returns></returns>
        public int ModificarProductoPlanComprasContrato(ProductoPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosBll.ModificarProductoPlanComprasContrato(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <returns></returns>
        public int InsertarProductoPlanComprasContratoAdiciones(ProductoPlanComprasContratos pPlanComprasProductos, int idAporte)
        {
            try
            {
                return vPlanComprasProductosBll.InsertarProductoPlanComprasContratoAdiciones(pPlanComprasProductos, idAporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <returns></returns>
        public int ModificarProductoPlanComprasContratoAdiciones(ProductoPlanComprasContratos pPlanComprasProductos, int idAporte)
        {
            try
            {
                return vPlanComprasProductosBll.ModificarProductoPlanComprasContratoAdiciones(pPlanComprasProductos, idAporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <returns></returns>
        public int InsertarRubroPlanComprasContrato(RubroPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasRubrosCDPBll.InsertarRubroPlanComprasContrato(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubros(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosBll.InsertarDetallePlanComprasProductosRubros(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubrosConTotal(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                return vPlanComprasProductosBll.InsertarDetallePlanComprasProductosRubrosTotal(pPlanComprasProductos, esEdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vPlanComprasProductos"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public int InsertarDetallePlanComprasProductosRubrosConTotalPrecontractual(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                return vPlanComprasProductosBll.InsertarDetallePlanComprasProductosRubrosConTotalPrecontractual(pPlanComprasProductos, esEdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubrosConTotalAdicion(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                return vPlanComprasProductosBll.InsertarDetallePlanComprasProductosRubrosConTotalAdicion(pPlanComprasProductos, esEdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDetallePlanComprasProductosRubrosConTotalVigencias(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                return vPlanComprasProductosBll.InsertarDetallePlanComprasProductosRubrosTotalVigencias(pPlanComprasProductos, esEdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPlanCompras"></param>
        /// <param name="valorPlanCompras"></param>
        /// <returns></returns>
        public int ActualizarPlanComprasTotal(int idPlanCompras, decimal valorPlanCompras)
        {
            try
            {
                return vPlanComprasProductosBll.ActualizarPlanComprasTotal(idPlanCompras, valorPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int ModificarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosBll.ModificarPlanComprasProductos(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int EliminarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosBll.EliminarPlanComprasProductos(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasProductos ConsultarPlanComprasProductos(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                return vPlanComprasProductosBll.ConsultarPlanComprasProductos(pIdProductoPlanCompraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PlanComprasProductos ConsultarPlanComprasProductoActual(int idPlanComprasContrato, string codigoProducto)
        {
            try
            {
                return vPlanComprasProductosBll.ConsultarPlanComprasProductoActual(idPlanComprasContrato, codigoProducto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pCodigoProducto"></param>
        /// <param name="pNombreProducto"></param>
        /// <param name="pTipoProducto"></param>
        /// <param name="pCantidadCupos"></param>
        /// <param name="pValorUnitario"></param>
        /// <param name="pTiempo"></param>
        /// <param name="pValorTotal"></param>
        /// <param name="pUnidadTiempo"></param>
        /// <param name="pUnidadMedida"></param>
        public List<PlanComprasProductos> ConsultarPlanComprasProductoss(int? pNumeroConsecutivoPlanCompras, String pCodigoProducto, String pNombreProducto, String pTipoProducto, Decimal? pCantidadCupos, Decimal? pValorUnitario, Decimal? pTiempo, Decimal? pValorTotal, String pUnidadTiempo, String pUnidadMedida)
        {
            try
            {
                return vPlanComprasProductosBll.ConsultarPlanComprasProductoss(pNumeroConsecutivoPlanCompras, pCodigoProducto, pNombreProducto, pTipoProducto, pCantidadCupos, pValorUnitario, pTiempo, pValorTotal, pUnidadTiempo, pUnidadMedida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los productos asociados a un plan de compras para ser utilizado en la pagina de registro
        /// de contratos
        /// </summary>
        /// <param name="pIdPlanDeComprasContratos">Entero con el identificador del plan de compras</param>
        /// <returns>Listado de productos asociados al plan de compras obtenidos de la base de datos</returns>
        public List<PlanComprasProductos> ObtenerProductosPlanCompras(int pIdPlanDeComprasContratos)
        {
            try
            {
                return vPlanComprasProductosBll.ObtenerProductosPlanCompras(pIdPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdProductoPlanComprasContrato"></param>
        /// <returns></returns>
        public ValidacionProductoOriginal ValidarModifcacionProducto(int pIdProductoPlanComprasContrato)
        {
            try
            {
                return vPlanComprasProductosBll.ValidarModifcacionProducto(pIdProductoPlanComprasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdRubroPlanComprasContrato"></param>
        /// <returns></returns>
        public ValidacionRubroOriginal ValidarModifcacionRubro(int pIdRubroPlanComprasContrato)
        {
            try
            {
                return vPlanComprasProductosBll.ValidarModifcacionRubro(pIdRubroPlanComprasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Plan Compras Rubros CDP

        /// <summary>
        /// Método de inserción para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int InsertarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                return vPlanComprasRubrosCDPBll.InsertarPlanComprasRubrosCDP(pPlanComprasRubrosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int ModificarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                return vPlanComprasRubrosCDPBll.ModificarPlanComprasRubrosCDP(pPlanComprasRubrosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarRubroPlanComprasContratoReduccionAdicion(RubroPlanComprasContratos pPlanComprasRubrosCDP)
        {
            try
            {
                return vPlanComprasRubrosCDPBll.ModificarRubroPlanComprasContratoReduccionAdicion(pPlanComprasRubrosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int EliminarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                return vPlanComprasRubrosCDPBll.EliminarPlanComprasRubrosCDP(pPlanComprasRubrosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasRubrosCDP ConsultarPlanComprasRubrosCDP(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                return vPlanComprasRubrosCDPBll.ConsultarPlanComprasRubrosCDP(pIdProductoPlanCompraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pCodigoRubro"></param>
        /// <param name="pDescripcionRubro"></param>
        /// <param name="pValorRubro"></param>
        public List<PlanComprasRubrosCDP> ConsultarPlanComprasRubrosCDPs(int? pNumeroConsecutivoPlanCompras, String pCodigoRubro, String pDescripcionRubro, Decimal? pValorRubro)
        {
            try
            {
                return vPlanComprasRubrosCDPBll.ConsultarPlanComprasRubrosCDPs(pNumeroConsecutivoPlanCompras, pCodigoRubro, pDescripcionRubro, pValorRubro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los rubros asociados a un plan de compras para ser utilizado en la pagina de registro
        /// de contratos
        /// </summary>
        /// <param name="pIdPlanDeComprasContratos">Entero con el identificador del plan de compras</param>
        /// <returns>Listado de rubros asociados al plan de compras obtenidos de la base de datos</returns>
        public List<PlanComprasRubrosCDP> ObtenerRubrosPlanCompras(int pIdPlanDeComprasContratos)
        {
            try
            {
                return vPlanComprasRubrosCDPBll.ObtenerRubrosPlanCompras(pIdPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Pregunta

        /// <summary>
        /// Grupo de apoyo / TOMMY DAVID PUCCINI
        /// 
        public int InsertarPregunta(Pregunta pPregunta)
        {
            try
            {
                return vPreguntaBLL.InsertarPregunta(pPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPregunta(Pregunta pPregunta)
        {
            try
            {
                return vPreguntaBLL.ModificarPregunta(pPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPregunta(Pregunta pPregunta)
        {
            try
            {
                return vPreguntaBLL.EliminarPregunta(pPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Pregunta ConsultarPregunta(int pIdPregunta)
        {
            try
            {
                return vPreguntaBLL.ConsultarPregunta(pIdPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Pregunta> ConsultarPreguntas(String pDescripcionPregunta, int? pIdTipoContrato, int? pIdComponente, int? pIdSubComponente, int? pIdCategoriaContrato, int? pRequiereDocumento, int? pEstado)
        {
            try
            {
                return vPreguntaBLL.ConsultarPreguntas(pDescripcionPregunta, pIdTipoContrato, pIdComponente, pIdSubComponente, pIdCategoriaContrato, pRequiereDocumento, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TipoFormaPago


        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Metodo que permite consultar la entidad TipoFormaPago filtrando los resultados por los filtros
        /// codigoTipoFormaPago, nombreTipoFormaPago, descripcion, estado
        /// </summary>
        /// <param name="pCodigoTipoFormaPago">Cadena de texto con el codigo del tipo forma pago</param>
        /// <param name="pNombreTipoFormaPago">Cadena de texto con el nombre del tipo forma pago</param>
        /// <param name="pDescripcion">Cadena de texto con la descripción del tipo forma pago</param>
        /// <param name="pEstado">Booleano con el estado del tipo forma de pago</param>
        /// <returns>Listado de entidades TipoFormaPago con la información encontrada en la base de datos</returns>
        public List<TipoFormaPago> ConsultarTipoMedioPagos(String pCodigoTipoFormaPago, String pNombreTipoFormaPago, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoFormaPagoBLL.ConsultarTipoMedioPagos(pCodigoTipoFormaPago, pNombreTipoFormaPago, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int InsertarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                return vTipoFormaPagoBLL.InsertarTipoFormaPago(pTipoFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int ModificarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                return vTipoFormaPagoBLL.ModificarTipoFormaPago(pTipoFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int EliminarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                return vTipoFormaPagoBLL.EliminarTipoFormaPago(pTipoFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pIdTipoFormaPago"></param>
        /// <returns></returns>
        public TipoFormaPago ConsultarTipoFormaPago(int pIdTipoFormaPago)
        {
            try
            {
                return vTipoFormaPagoBLL.ConsultarTipoFormaPago(pIdTipoFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pNombreTipoFormaPago"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoFormaPago> ConsultarTipoFormaPagos(String pNombreTipoFormaPago, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoFormaPagoBLL.ConsultarTipoFormaPagos(pNombreTipoFormaPago, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region VigenciaFuturas
        /// <summary>
        /// Método de inserción para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int InsertarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasBLL.InsertarVigenciaFuturas(pVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int InsertarVigenciaFuturasModificacionContrato(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasBLL.InsertarVigenciaFuturasModificacionContrato(pVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int ModificarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasBLL.ModificarVigenciaFuturas(pVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int EliminarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasBLL.EliminarVigenciaFuturas(pVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int EliminarVigenciaFuturasContrato(int idContrato)
        {
            try
            {
                return vVigenciaFuturasBLL.EliminarVigenciaFuturasContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarVigenciaFuturasModificacionContrato(int IDVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasBLL.EliminarVigenciaFuturasModificacionContrato(IDVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarVigenciaFuturasModificacionContratoTodas(int idContrato)
        {
            try
            {
                return vVigenciaFuturasBLL.EliminarVigenciaFuturasModificacionContratoTodas(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pNumeroRadicado"></param>
        /// <param name="pFechaExpedicion"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pValorVigenciaFutura"></param>
        public List<VigenciaFuturas> ConsultarVigenciaFuturass(String pNumeroRadicado, DateTime? pFechaExpedicion, int? pIdContrato, Decimal? pValorVigenciaFutura)
        {
            try
            {
                return vVigenciaFuturasBLL.ConsultarVigenciaFuturass(pNumeroRadicado, pFechaExpedicion, pIdContrato, pValorVigenciaFutura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public VigenciaFuturas ConsultarVigenciaFuturasporContrato(int pIdContrato)
        {
            try
            {
                return vVigenciaFuturasBLL.ConsultarVigenciaFuturasporContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosVigenciasFuturas(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, String pNumeroContratoConvenio, String pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                return vVigenciaFuturasBLL.ConsultarContratosVigenciasFuturas(pFechaRegistroDesde, pFechaRegistroHasta,
                    pNumeroContratoConvenio, pVigenciaFiscalInicial, pRegionalContrato, pCategoriaContrato,
                    pTipoContrato, pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region RPContrato
        /// <summary>
        /// Gonet
        /// Método de inserción para la entidad RPContrato
        /// Fecha: 11/07/2014
        /// </summary>
        /// <param name="pRPContrato">instancia que contiene la información de RPContratos</param>
        public int InsertarRPContratos(RPContrato pRPContrato)
        {
            try
            {
                return vRPContratoBLL.InsertarRPContratos(pRPContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public List<RPContrato> ConsultarRPContratosAsociados(int? pIdContrato, int? pIdRP)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPContratosAsociados(pIdContrato, pIdRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public List<RPContrato> ConsultarRPContratosAsociados(int pIdContrato)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPContratosAsociados(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<RPContrato> ConsultarRPContratosAsociadosVigencias(int? pIdContrato, bool? pesVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPContratosAsociadosVigencias(pIdContrato, pesVigenciaFutura, pAnioVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public List<RPContrato> ConsultarRPContratosExiste(int pIdRP)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPContratosExiste(pIdRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RPContrato> ConsultarRPContratosExiste(int numeroRP, int idRegional)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPContratosExiste(numeroRP, idRegional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        /// <param name="pIdRegional">Valor entero con el id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el id de la vigencia</param>
        /// <param name="pValorTotalDesde">Valor decimal con el Valor Total Desde</param>
        /// <param name="pValorTotalHasta">Valor decimal con el Valor Total hasta</param>
        /// <param name="pFechaRpDesde">Valor Fecha Rp Desde</param>
        /// <param name="pFechaRpHasta">Valor Fecha Rp Hasta</param>
        public List<RPContrato> ConsultarRPContratoss(int? pIdRP, int? pIdRegional, int? pIdVigencia, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaRpDesde, DateTime? pFechaRpHasta)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPContratoss(pIdRP, pIdRegional, pIdVigencia, pValorTotalDesde,
                                                            pValorTotalHasta, pFechaRpDesde, pFechaRpHasta);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public RPContrato ConsultarRPPorId(int pIdRP)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPPorId(pIdRP);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        /// <param name="pIdRegional">Valor entero con el id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el id de la vigencia</param>
        /// <param name="pValorTotalDesde">Valor decimal con el Valor Total Desde</param>
        /// <param name="pValorTotalHasta">Valor decimal con el Valor Total hasta</param>
        /// <param name="pFechaRpDesde">Valor Fecha Rp Desde</param>
        /// <param name="pFechaRpHasta">Valor Fecha Rp Hasta</param>
        public List<RPContrato> ConsultarRPContratossActualizado(int? pIdRP, int? pIdRegional, int? pIdVigencia, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaRpDesde, DateTime? pFechaRpHasta)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPContratossActualizado(pIdRP, pIdRegional, pIdVigencia, pValorTotalDesde,
                                                            pValorTotalHasta, pFechaRpDesde, pFechaRpHasta);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de actualización de un contrato suscrito a contrato RP
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="vFechaSuscripcion"></param>
        /// <param name="vIdEstadoContrato"></param>
        public int ActualizarContratoRP(int pIdContrato, string pUsuarioModifica, int vIdEstadoContrato)
        {
            try
            {
                return vRPContratoBLL.ActualizarContratoRP(pIdContrato, pUsuarioModifica, vIdEstadoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAdicion"></param>
        /// <param name="usuario"></param>
        /// <param name="p"></param>
        public int ActualizarAdicionRP(int idAdicion, string usuario, int idRP, decimal valorRP, int idRegional)
        {
            try
            {
                return vRPContratoBLL.ActualizarAdicionRP(idAdicion, usuario, idRP, valorRP, idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAdicion"></param>
        /// <param name="usuario"></param>
        /// <param name="p"></param>
        public int ActualizarCesionRP(int idCesion, string usuario, int idRP, decimal valorRP, int idRegional)
        {
            try
            {
                return vRPContratoBLL.ActualizarCesionRP(idCesion, usuario, idRP, valorRP, idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdAdicion"></param>
        /// <returns></returns>
        public RPContrato ConsultarRPContratosAsociados(int pIdContrato, int pIdAdicion, bool esAdcion = true)
        {
            try
            {
                return vRPContratoBLL.ConsultarRPAdicionAsociados(pIdContrato, pIdAdicion, esAdcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarRPContratos(RPContrato pRPContrato, int pIdVigenciaFutura)
        {
            try
            {
                return vRPContratoBLL.EliminarRPContratos(pRPContrato, pIdVigenciaFutura);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Reducciones
        public int InsertarReducciones(Reducciones pReducciones)
        {
            try
            {
                return vReduccionesBLL.InsertarReducciones(pReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarReducciones(Reducciones pReducciones)
        {
            try
            {
                return vReduccionesBLL.ModificarReducciones(pReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarValorReducciones(Reducciones pReducciones)
        {
            try
            {
                return vReduccionesBLL.ModificarValorReducciones(pReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int EliminarReducciones(Reducciones pReducciones)
        {
            try
            {
                return vReduccionesBLL.EliminarReducciones(pReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Reducciones ConsultarReducciones(int pIdReducciones)
        {
            try
            {
                return vReduccionesBLL.ConsultarReducciones(pIdReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Reducciones> ConsultarReduccioness(int? pValorReduccion, DateTime? pFechaReduccion, int? pIDDetalleConsModContractual)
        {
            try
            {
                return vReduccionesBLL.ConsultarReduccioness(pValorReduccion, pFechaReduccion, pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ReduccionContrato ConsultarContratoReduccion(int pIdContrato)
        {

            try
            {
                return vReduccionesBLL.ConsultarContratoReduccion(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Reducciones> ConsultarReduccionesAprobadasContrato(int IdContrato)
        {
            try
            {
                return vReduccionesBLL.ConsultarReduccionesAprobadasContrato(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorTotalReducido(int IdContrato)
        {
            decimal result = 0;

            try
            {
                return vReduccionesBLL.ConsultarValorTotalReducido(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        #endregion

        #region DependenciaSolicitante

        /// <summary>
        /// Método de consulta por filtros para la entidad DependenciaSolicitante
        /// </summary>
        /// <param name="pIdDependenciaSolicitante"></param>
        /// <param name="pNombreDependenciaSolicitante"></param>
        /// <returns></returns>
        public List<DependenciaSolicitante> ConsultarDependenciaSolicitante(int? pIdDependenciaSolicitante
            , String pNombreDependenciaSolicitante, string pCodigoRegional)
        {
            try
            {
                return vDependenciaSolicitanteBLL.ConsultarDependenciaSolicitante(pIdDependenciaSolicitante, pNombreDependenciaSolicitante, pCodigoRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region ContratosAdheridos

        /// <summary>
        /// Método de consulta por id para la entidad Contrato de Contratos Relacionados Convenio Marco
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el IdContrato</param>
        /// <returns>Lista de la instancia Contrato</returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosRelacionadosConvMarco(int pIdContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratosRelacionadosConvMarco(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Contrato de Contratos Adheridos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el IdContrato</param>
        /// <returns>Lista de la instancia Contrato</returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosAdheridos(int pIdContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratosAdheridos(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region FUC
        public int InsertarFUC(FUC pFUC)
        {
            try
            {
                return vFUCBLL.InsertarFUC(pFUC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarFUC(FUC pFUC)
        {
            try
            {
                return vFUCBLL.ModificarFUC(pFUC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarFUC(FUC pFUC)
        {
            try
            {
                return vFUCBLL.EliminarFUC(pFUC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public FUC ConsultarFUC(Int64 pId)
        {
            try
            {
                return vFUCBLL.ConsultarFUC(pId);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<FUC> ConsultarFUCs(String pRegional, int? pNumeroContrato, DateTime? pFechaSuscripcion, int? pIdAreaSolicita, String pContratistaTipoIdentificacion, String pContratistaNombre, String pRPRubro1, String pSupervisor)
        {
            try
            {
                return vFUCBLL.ConsultarFUCs(pRegional, pNumeroContrato, pFechaSuscripcion, pIdAreaSolicita, pContratistaTipoIdentificacion, pContratistaNombre, pRPRubro1, pSupervisor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region ConsModContractual
        public int InsertarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                return vConsModContractualBLL.InsertarConsModContractual(pConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                return vConsModContractualBLL.ModificarConsModContractual(pConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                return vConsModContractualBLL.EliminarConsModContractual(pConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ConsModContractual ConsultarConsModContractual(int pIDCosModContractual)
        {
            try
            {
                return vConsModContractualBLL.ConsultarConsModContractual(pIDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public ConsModContractual ConsModContractualPorDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                return vConsModContractualBLL.ConsModContractualPorDetalleConsModContractual(pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ConsModContractual> ConsultarConsModContractuals(String pNumeroDoc, int? pIDContrato, String pConsecutivoSuscrito, int? pSuscrito, string pUsuario)
        {
            try
            {
                return vConsModContractualBLL.ConsultarConsModContractuals(pNumeroDoc, pIDContrato, pConsecutivoSuscrito, pSuscrito, pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitud(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualBLL.ConsultarConsModContractualPorSolicitud(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualImpMultas(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualBLL.ConsultarConsModContractualImpMultas(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudReparto(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualBLL.ConsultarConsModContractualPorSolicitudReparto(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudAdiciones(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualBLL.ConsultarConsModContractualPorSolicitudAdiciones(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudCesion(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualBLL.ConsultarConsModContractualPorSolicitudCesion(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public IDictionary<string, int> ConsultarConsolidadoModificaciones(int idContrato)
        {
            try
            {
                return new SolModContractualBLL().ConsultarConsolidadoModificaciones(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoSolicitudesContrato> ConsultarModificacionesHistorico(int idContrato)
        {
            try
            {
                return new SolModContractualBLL().ConsultarModificacionesHistorico(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region  tipo de modificacion

        /// <summary>
        /// método de inserción para la entidad tipoformapago
        /// </summary>
        /// <param name="ptipoformapago"></param>
        /// <returns></returns>
        public int InsertarTipoModificacion(TipoModificacion ptipo, string idsAsociados)
        {
            try
            {
                return vTipoModificacion.InsertarTipoModificacion(ptipo, idsAsociados);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// método de modificación para la entidad tipoformapago
        /// </summary>
        /// <param name="ptipoformapago"></param>
        /// <returns></returns>
        public int ModificarTipoModificacion(TipoModificacion ptipo)
        {
            try
            {
                return vTipoModificacion.ModificarTipoModificacion(ptipo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// método de consulta por id para la entidad tipoformapago
        /// </summary>
        /// <param name="pidtipoformapago"></param>
        /// <returns></returns>
        public TipoModificacion consultartipomodificacion(int pidtipo)
        {
            try
            {
                return vTipoModificacion.ConsultarTipoModificacion(pidtipo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// método de consulta por filtros para la entidad tipoformapago
        /// </summary>
        /// <param name="pnombretipoformapago"></param>
        /// <param name="pdescripcion"></param>
        /// <param name="pestado"></param>
        /// <returns></returns>
        public List<TipoModificacion> ConsultarTipoModificaciones(int? pcodigo, string pDescripcion, Boolean? prequiereModificacion, Boolean? pestado, string estado)
        {
            try
            {
                return vTipoModificacion.ConsultarTipoModificaciones(pcodigo, pDescripcion, prequiereModificacion, pestado, estado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Metodo que consulta la existencia de una combinación de tipos de modificación.
        /// </summary>
        /// <param name="idsAsociados"></param>
        /// <returns></returns>
        public bool ConsultarExistenciaCombinacion(string idsAsociados, int idTipoModificacion)
        {
            try
            {
                return vTipoModificacion.ConsultarExistenciaCombinacion(idsAsociados, idTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TipoModificacion> ConsultarTipoModificacionPorDefecto(bool todos)
        {
            try
            {
                return vTipoModificacion.ConsultarTipoModificacionPorDefecto(todos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdTipoModificacionBasica"></param>
        /// <returns></returns>
        public List<TipoModificacion> ConsultarTipoModificacionHijos(int pIdTipoModificacionBasica)
        {
            try
            {
                return vTipoModificacion.ConsultarTipoModificacionHijos(pIdTipoModificacionBasica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdTipoModificacion"></param>
        /// <returns></returns>
        public bool ConsultarPuedeEliminarTipodeModificacion(int pIdTipoModificacion)
        {
            bool result = false;

            try
            {
                result = vTipoModificacion.ConsultarPuedeEliminar(pIdTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdTipoModificacion"></param>
        /// <returns></returns>
        public int EliminarTipoModificacionContractual(int pIdTipoModificacion)
        {
            int result = 0;

            try
            {
                result = vTipoModificacion.EliminarModificacion(pIdTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        #endregion 

        #region Adiciones
        public int InsertarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                return new Icbf.Contrato.Business.AdicionesBLL().InsertarAdiciones(pAdiciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                return new Icbf.Contrato.Business.AdicionesBLL().ModificarAdiciones(pAdiciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                return new Icbf.Contrato.Business.AdicionesBLL().EliminarAdiciones(pAdiciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarAdiciones(int IdPlanComprasContrato, string IdProducto, int idAdiccion, int idProductoPlanCompras, string tipoProducto, decimal ValorActualAdiccion)
        {
            try
            {
                return new Icbf.Contrato.Business.AdicionesBLL().ModificarAdiciones(IdPlanComprasContrato, IdProducto, idAdiccion, idProductoPlanCompras, tipoProducto, ValorActualAdiccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Adiciones ConsultarAdiciones(int pIdAdicion)
        {
            try
            {
                return new Icbf.Contrato.Business.AdicionesBLL().ConsultarAdiciones(pIdAdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Adiciones> ConsultarAdicioness(int? pValorAdicion, String pJustificacionAdicion, int? pEstado, DateTime? pFechaAdicion, int? pIDDetalleConsModContractual)
        {
            try
            {
                var adiciones = new Icbf.Contrato.Business.AdicionesBLL().ConsultarAdicioness(pValorAdicion, pJustificacionAdicion, pEstado, pFechaAdicion, pIDDetalleConsModContractual);

                if (adiciones != null)
                {
                    foreach (var item in adiciones)
                    {
                        var aportes = ObtenerAportesContratoAdicionReduccion(item.IdAdicion, true);

                        if (aportes != null && aportes.Count > 0)
                            item.ValorAdicion = aportes.Sum(a => a.ValorAporte);
                    }

                    //adiciones.ForEach(e => e.ValorAdicion = ObtenerAportesContratoAdicion(e.IdAdicion).Sum(a => a.ValorAporte));
                }

                return adiciones;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Adiciones> ConsultarAdicionesAprobadasContrato(int IdContrato)
        {
            try
            {
                return new Icbf.Contrato.Business.AdicionesBLL().ConsultarAdicionesAprobadasContrato(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorTotalAdicionado(int IdContrato)
        {
            decimal result = 0;

            try
            {
                result = new Icbf.Contrato.Business.AdicionesBLL().ConsultarValorTotalAdicionado(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public void ActualizarAdicionRP(int idAdicion, int idContrato)
        {
            try
            {
                new Icbf.Contrato.Business.AdicionesBLL().ActualizarAdicionRP(idAdicion, idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void ActualizarCesionRP(int idAdicion)
        {
            try
            {
                new Icbf.Contrato.Business.AdicionesBLL().ActualizarCesionRP(idAdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region DetalleConsModContractual
        public int InsertarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                return new Icbf.Contrato.Business.DetalleConsModContractualBLL().InsertarDetalleConsModContractual(pDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                return new Icbf.Contrato.Business.DetalleConsModContractualBLL().ModificarDetalleConsModContractual(pDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                return new Icbf.Contrato.Business.DetalleConsModContractualBLL().EliminarDetalleConsModContractual(pDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DetalleConsModContractual ConsultarDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                return new Icbf.Contrato.Business.DetalleConsModContractualBLL().ConsultarDetalleConsModContractual(pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DetalleConsModContractual> ConsultarDetalleConsModContractuals(int? pIDTipoModificacionContractual, int? pIDCosModContractual)
        {
            try
            {
                return new Icbf.Contrato.Business.DetalleConsModContractualBLL().ConsultarDetalleConsModContractuals(pIDTipoModificacionContractual, pIDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Solicitudes de Modificaciones contractuales

        public int InsertarSolModContractual(ConsModContractual psModContractual)
        {
            try
            {
                return vSolModContractualBLL.InsertarSolModContractual(psModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }       

        public int ModificarSolModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                return vSolModContractualBLL.ModificarSolModContractual(pConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractualContrato> ConsultarContratosSolicitud(string numeroContrato,
                                                                  int? vigenciaFiscal,
                                                                  int? regional,
                                                                  int? idCategoriaContrato,
                                                                  int? idTipoContrato,
                                                                  string numeroIdentificacion,
                                                                  int? valorContratoDesde,
                                                                  int? valorContratoHasta)
        {
            try
            {
                return vSolModContractualBLL.ConsultarContratos(numeroContrato, vigenciaFiscal, regional, idCategoriaContrato, idTipoContrato, numeroIdentificacion, valorContratoDesde, valorContratoHasta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolModContractualContrato ConsultarContratoSolicitud(int idContrato)
        {
            try
            {
                return vSolModContractualBLL.ConsultarContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolModContractual ObtenerModificacionSinModificacionGarantiaPorTipo(int idTipoModificacion, int idContrato)
        {
            try
            {
                return vSolModContractualBLL.ObtenerModificacionSinModificacionGarantiaPorTipo(idTipoModificacion, idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarSolicitudesPorContrato(int idContrato)
        {
            try
            {
                return vSolModContractualBLL.ConsultarSolicitudesPorContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarSolicitudesRechazadasPorContrato(int idContrato)
        {
            try
            {
                return vSolModContractualBLL.ConsultarSolicitudesRechazadasPorContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolModContractual ConsultarSolitud(int IDCosModContractual)
        {
            try
            {
                return vSolModContractualBLL.ConsultarSolitud(IDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ConsModContractual ConsultarUltimaSolitud(int idContrato)
        {
            try
            {
                return vSolModContractualBLL.ConsultarUltimaSolitud(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractualDetalle> ConsultarSolicitudesDetalle(int IDCosModContractual)
        {
            try
            {
                return vSolModContractualBLL.ConsultarSolicitudesDetalle(IDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }        

        public bool ConsultarExisteSolicitudModificacion(int idContrato, int idTipoSolicitud)
        {
            bool existe = false;

            try
            {
                return vSolModContractualBLL.ConsultarExisteSolicitudModificacion(idContrato, idTipoSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }

        public bool ConsultarPuedeModificarTipoSolicitud(int idCosModContractual)
        {
            bool existe = false;

            try
            {
                return vSolModContractualBLL.ConsultarPuedeModificarTipoSolicitud(idCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }


        public bool ConsultarSiesContratoSV(int idContrato)
        {
            bool existe = false;

            try
            {
                return vSolModContractualBLL.ConsultarSiesContratoSV(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }

        public List<SolModContractualValidacion> ValidarCambiodeEstadoaEnviada(int IDConsModContractual)
        {
            try
            {
                return vSolModContractualBLL.ValidarCambiodeEstadoaEnviada(IDConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoRegistroaEnviada(int IDConsModContractual)
        {
            try
            {
                return vSolModContractualBLL.CambiarEstadoRegistroaEnviada(IDConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificacionEstadoContractualContrato(int IDConsModContractual, int Estado, String JustificacionRechazo, String JustificacionDevolucion)
        {
            try
            {
                return vSolModContractualBLL.ModificacionEstadoContractualContrato(IDConsModContractual, Estado, JustificacionRechazo, JustificacionDevolucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificacionContractualContrato(int IDCosModContractual, string usuario)
        {
            try
            {
                return vSolModContractualBLL.ModificacionContractualContrato(IDCosModContractual, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificacionContractualContratoRechazoDevolucion(int IDCosModContractual, bool esRechazo, string usuario, string justificacion)
        {
            try
            {
                return vSolModContractualBLL.ModificacionContractualContratoRechazoDevolucion(IDCosModContractual, esRechazo, usuario, justificacion);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<string> PuedeCrearSolicitud(int idContrato, int idTipoModificacion, int idConsModcontractual)
        {
            try
            {
                return vSolModContractualBLL.PuedeCrearSolicitud(idContrato, idTipoModificacion, idConsModcontractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractualHistorico> ConsultarSolicitudesHistorico(int IDCosModContractual)
        {
            try
            {
                return vSolModContractualBLL.ConsultarSolicitudesHistorico(IDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int SubscribirModificacionContractualContrato(int idConsmodContractual, string usuario, DateTime fechaAprobacion)
        {
            try
            {
                return vSolModContractualBLL.SubscribirModificacionContractualContrato(idConsmodContractual, usuario, fechaAprobacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        #endregion 

        #region Modificacion de Garantias

        public int InsertarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                return vModificacionGarantiaBLL.InsertarModificacionGarantia(pModificacionGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                return vModificacionGarantiaBLL.ModificarModificacionGarantia(pModificacionGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                return vModificacionGarantiaBLL.EliminarModificacionGarantia(pModificacionGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ModificacionGarantia ConsultarModificacionGarantia(int pIdModificacionGarantia)
        {
            try
            {
                return vModificacionGarantiaBLL.ConsultarModificacionGarantia(pIdModificacionGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModificacionGarantia> ConsultarModificacionGarantias(int? pIdContrato, int? pNumeroDocumento, DateTime? pFechaRegistro, String pTipoModificacion, String pNumeroGarantia, int? pIdAmparo, String pTipoAmparo, DateTime? pFechaVigenciaDesde, DateTime? pFechaVigenciaHasta, Decimal? pCalculoValorAsegurado, String pTipoCalculo, Decimal? pValorAdicion, Decimal? pValorTotalReduccion, Decimal? pValorAsegurado, String pObservacionesModificacion)
        {
            try
            {
                return vModificacionGarantiaBLL.ConsultarModificacionGarantias(pIdContrato, pNumeroDocumento, pFechaRegistro, pTipoModificacion, pNumeroGarantia, pIdAmparo, pTipoAmparo, pFechaVigenciaDesde, pFechaVigenciaHasta, pCalculoValorAsegurado, pTipoCalculo, pValorAdicion, pValorTotalReduccion, pValorAsegurado, pObservacionesModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Prorrogas

        public int InsertarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                return vProrrogasBLL.InsertarProrrogas(pProrrogas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                return vProrrogasBLL.ModificarProrrogas(pProrrogas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                return vProrrogasBLL.EliminarProrrogas(pProrrogas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Prorrogas ConsultarProrrogas(int pIdProrroga)
        {
            try
            {
                return vProrrogasBLL.ConsultarProrrogas(pIdProrroga);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Prorrogas> ConsultarProrrogass(DateTime? pFechaInicio, DateTime? pFechaFin, int? pDias, int? pMeses, int? pAños, String pJustificacion, int? pIdDetalleConsModContractual)
        {
            try
            {
                return vProrrogasBLL.ConsultarProrrogass(pFechaInicio, pFechaFin, pDias, pMeses, pAños, pJustificacion, pIdDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Prorrogas> ConsultarProrrogasContrato(int idContrato)
        {
            try
            {
                return vProrrogasBLL.ConsultarProrrogasContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<Prorrogas> ConsultarProrrogasAprobadasContrato(int idContrato)
        {
            try
            {
                return vProrrogasBLL.ConsultarProrrogasAprobadasContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public Tiempo ConsultarProrrogasAprobadasContratoDetalle(int idContrato)
        {
            try
            {
                return vProrrogasBLL.ConsultarProrrogasAprobadasContratoDetalle(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Reducciones Tiempo
        public int InsertarReduccionesTiempo(ReduccionesTiempo vReduccion)
        {
            try
            {
                return new ReduccionesTiempoBLL().InsertarReduccionesTiempo(vReduccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarReduccionesTiempo(ReduccionesTiempo vReduccion)
        {
            try
            {
                return new ReduccionesTiempoBLL().ModificarReduccionesTiempo(vReduccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarReduccionesTiempo(ReduccionesTiempo vReduccion)
        {
            try
            {
                return new ReduccionesTiempoBLL().EliminarReduccionesTiempo(vReduccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ReduccionesTiempo ConsultarReduccionTiempo(int pIdReduccion)
        {
            try
            {
                return new ReduccionesTiempoBLL().ConsultarReduccionTiempo(pIdReduccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempo(DateTime? pFechaReduccion, int? pIdDetalleConsModContractual)
        {
            try
            {
                return new ReduccionesTiempoBLL().ConsultarReduccionesTiempo(pFechaReduccion, pIdDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempoContrato(int idContrato)
        {
            try
            {
                return new ReduccionesTiempoBLL().ConsultarReduccionesTiempoContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempoAprobadasContrato(int idContrato)
        {
            try
            {
                return new ReduccionesTiempoBLL().ConsultarReduccionesTiempoAprobadasContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }


        #endregion

        #region Cambio de Lugar de Ejecución

       
        /// <summary>
        /// Método de inserción para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int InsertarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                return new CambioLugarEjecucionBLL().InsertarCambioLugarEjecucion(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int ModificarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                return new CambioLugarEjecucionBLL().ModificarCambioLugarEjecucion(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int EliminarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                return new CambioLugarEjecucionBLL().EliminarCambioLugarEjecucion(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdLugarEjecucionContratos"></param>
        public int InsertarLugaresEjecucionActuales(int pIdContrato, int pIdDetalleConsModContractual, string pUsuarioCrea)
        {
            try
            {
                return new CambioLugarEjecucionBLL().InsertarLugaresEjecucionActuales(pIdContrato, pIdDetalleConsModContractual, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdRegional"></param>
        /// <param name="pNivelNacional"></param>
        //public List<LugarEjecucionContrato> ConsultarLugarEjecucionContratos(int? pIdContrato, int? pIdDepartamento, int? pIdRegional, Boolean? pNivelNacional)
        //{
        //    try
        //    {
        //        return vLugarEjecucionContratoBLL.ConsultarLugarEjecucionContratos(pIdContrato, pIdDepartamento, pIdRegional, pNivelNacional);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los lugares de ejecución asociados a un contrato
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado con los lugares obtenidos de la base de datos</returns>
        public List<CambioLugarEjecucion> ConsultarCambiosLugaresEjecucion(int pIdDetalleConsModContractual)
        {
            try
            {
                return new CambioLugarEjecucionBLL().ConsultarCambiosLugaresEjecucion(pIdDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        

        #endregion

        #region Modificacion de Obligacion

        public int InsertarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                return new Icbf.Contrato.Business.ModificacionObligacionBLL().InsertarModificacionObligacion(pModificacionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                return new Icbf.Contrato.Business.ModificacionObligacionBLL().ModificarModificacionObligacion(pModificacionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                return new Icbf.Contrato.Business.ModificacionObligacionBLL().EliminarModificacionObligacion(pModificacionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ModificacionObligacion ConsultarModificacionObligacion(int pIdModObligacion)
        {
            try
            {
                return new Icbf.Contrato.Business.ModificacionObligacionBLL().ConsultarModificacionObligacion(pIdModObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModificacionObligacion> ConsultarModificacionObligacions()
        {
            try
            {
                return new Icbf.Contrato.Business.ModificacionObligacionBLL().ConsultarModificacionObligacions();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModificacionObligacion> ConsultarModificacionObligacionXIDDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                return new Icbf.Contrato.Business.ModificacionObligacionBLL().ConsultarModificacionObligacionXIDDetalleConsModContractual(pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        public List<Componentes> ConsultarComponentes(string Nombre, int Estado)
        {
            try
            {
                return new Icbf.Contrato.Business.ComponentesBLL().ConsultarComponentess(Nombre, Estado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Componentes ConsultarComponentes(int IdComponente)
        {
            try
            {
                return new Icbf.Contrato.Business.ComponentesBLL().ConsultarComponentess(IdComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarComponente(Componentes _Entidad)
        {
            try
            {
                return new Icbf.Contrato.Business.ComponentesBLL().InsertarComponentes(_Entidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarComponente(Componentes _Entidad)
        {
            try
            {
                return new Icbf.Contrato.Business.ComponentesBLL().EliminarComponentes(_Entidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ActualizarValorInicialContrato(int vIdContraro, decimal vValor)
        {
            try
            {
                return new Icbf.Contrato.Business.ContratoBLL().ActualizarValorInicialContrato(vIdContraro, vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorsinAportes(int vIdContraro)
        {
            try
            {
                return new Icbf.Contrato.Business.ContratoBLL().ConsultarValorsinAportes(vIdContraro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ContratistaMigrados ConsultarContratistaMigracion(int pIdEntidad)
        {
            try
            {
                return new Icbf.Contrato.Business.ContratoBLL().ConsultarContratistaMigracion(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Utilitarios

        public static DateTime CalcularFechaFinalContrato(DateTime fechaInicio, int dias, int meses)
        {
            DateTime result = new DateTime();
            result = fechaInicio;
            result = result.AddMonths(meses);
            result = result.AddDays(dias);
            result = result.AddDays(-1);
            return result;
        }

        private static int ObtenerdiasEntreFechas360(DateTime fechaInicial, DateTime fechafinal)
        {
            fechafinal = fechafinal.AddDays(1);

            int result = 0;
            int ai, mi, di;
            int af, mf, df;

            ai = fechaInicial.Year;
            mi = fechaInicial.Month;
            di = fechaInicial.Day;

            af = fechafinal.Year;
            mf = fechafinal.Month;
            df = fechafinal.Day;


            if (di == 31 || (mi == 2 && di > 27))
                di = 30;
            if (df > 27 && mf == 2)
                df = 30;
            if (df == 31 && di < 30)
            {
                mf++;
                df = 1;
            }
            else if (df == 31)
                df = 30;
            else
            {
                if (di == 31 || (mi == 2 && di > 27))
                    di = 30;
                if (df == 31 || (mf == 2 && df > 27))
                    df = 30;
            }

            if (Math.Abs(af - ai) == 0)
                result = (mf - mi) * 30 + df - di;
            else
                result = Math.Abs(af - ai - 1) * 360 + 360 - mi * 30 + 30 - di + 30 * (mf - 1) + df;


            return result;
        }

        public static bool ObtenerDiferenciaFechas(DateTime fechaInicio, DateTime fechaFin, out string result)
        {
            bool isValid = true;
            result = string.Empty;
            try
            {
                int dias360 = ObtenerdiasEntreFechas360(fechaInicio, fechaFin);

                decimal dias360toMonth = dias360 / 30;

                int meses = (int)(Math.Round(dias360toMonth, 0, MidpointRounding.ToEven));

                //int anio = meses / 12;

                //meses = meses % 12;

                int dias = dias360 - (meses * 30);

                result = "0|" + meses + "|" + dias;
            }
            catch (Exception ex)
            {
                isValid = false;
            }

            return isValid;
        }

        #endregion  

        #region Modificaciones a las garantias

        public List<GarantiaModificacionContrato> ConsultarContratosModificacionGarantia(string numeroContrato,
                                  int? vigenciaFiscal,
                                  int? regional,
                                  int? idCategoriaContrato,
                                  int? idTipoContrato)
        {
            try
            {
                return vGarantiaModificacionBLL.ConsultarContratos(numeroContrato, vigenciaFiscal, regional, idCategoriaContrato, idTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void GenerarModificacionGarantia(int idGarantia, string usuariocreacion,DateTime fechaAprobacion)
        {
            try
            {
                vGarantiaModificacionBLL.GenerarModificacionGarantia(idGarantia, usuariocreacion, fechaAprobacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool EsGarantiaEditada(int idTipoModificacion1, int idContrato)
        {
            try
            {
                return vGarantiaModificacionBLL.EsGarantiaEditada(idTipoModificacion1, idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool PuedeCrearGarantiasNuevasModificacion(int idTipoModificacion)
        {
            try
            {
                return vGarantiaModificacionBLL.PuedeCrearGarantiasNuevasModificacion(idTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoModificacion> ObtenerTiposModificacionModGarantia(int idContrato)
        {
            try
            {
                return vGarantiaModificacionBLL.ObtenerTiposModificacionModGarantia(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Garantias Historicos

        public List<GarantiaHistorico> ConsultarInfoGarantiasHistorico(int? pIdGarantia)
        {
            try
            {
                return vGarantiaModificacionBLL.ConsultarInfoGarantiasHistorico(pIdGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Garantias Temporales

        public GarantiaHistorico ConsultarGarantiaTemporal(int pIDGarantiaTemporal)
        {
            try
            {
                return vGarantiaModificacionBLL.ConsultarGarantiaTemporal(pIDGarantiaTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string TipoModificacionGarantiaTemporal(int idGarantia)
        {
            try
            {
                return vGarantiaModificacionBLL.TipoModificacionGarantiaTemporal(idGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int GenerarGarantiaTemporal(int idGarantia, int idTipoModificacion)
        {
            try
            {
                return vGarantiaModificacionBLL.GenerarGarantiaTemporal(idGarantia, idTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificarGarantiaTemporal(GarantiaHistorico pGarantiaTemporal)
        {
            try
            {
                return vGarantiaModificacionBLL.ModificarGarantiaTemporal(pGarantiaTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Amparos Garantias Temporales

        public List<AmparosGarantias> ObtenerAmparosGarantiaTemporal(int pidGarantiaTemporal)
        {
            try
            {
                return vGarantiaModificacionBLL.ObtenerAmparosGarantiaTemporal(pidGarantiaTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #endregion

        #region Terminación Anticipada

        public int InsertarTerminacionAnticipada(TerminacionAnticipada pTerminacionAnticipada)
        {
            try
            {
                return new TerminacionAnticipadaBLL().InsertarTerminacionAnticipada(pTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTerminacionAnticipada(TerminacionAnticipada pTerminacionAnticipada)
        {
            try
            {
                return new TerminacionAnticipadaBLL().ModificarTerminacionAnticipada(pTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaContrato(int IdContrato)
        {
            try
            {
                return new TerminacionAnticipadaBLL().ConsultarTerminacionAnticipadaContrato(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaId(int IdTerminacionAnticipada)
        {
            try
            {
                return new TerminacionAnticipadaBLL().ConsultarTerminacionAnticipadaId(IdTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                return new TerminacionAnticipadaBLL().ConsultarTerminacionAnticipadaIdDetalle(IdDetConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Imposición de multas


        public int InsertarImposicionMulta(ImposicionMulta pImposicion)
        {
            try
            {
                return new ImposicionMultasBLL().InsertarImposicionMulta(pImposicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarImposicionMulta(ImposicionMulta pImposicion)
        {
            try
            {
                return new ImposicionMultasBLL().ModificarImposicionMulta(pImposicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMulta(int IdContrato)
        {
            try
            {
                return new ImposicionMultasBLL().ConsultarImposicionMulta(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMultaId(int IdImposicion)
        {
            try
            {
                return new ImposicionMultasBLL().ConsultarImposicionMultaId(IdImposicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMultaIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                return new ImposicionMultasBLL().ConsultarImposicionMultaIdDetalle(IdDetConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMultaHistorico> ConsultarHistoricoProcesoImpMultas(int IdImposicion)
        {
            try
            {
                return new ImposicionMultasBLL().ConsultarHistoricoProcesoImpMultas(IdImposicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion 

        #region Liquidación Contrato

        public int InsertarLiquidacionContrato(LiquidacionContrato pTerminacionAnticipada)
        {
            try
            {
                return new LiquidacionContratoBLL().InsertarLiquidacionContrato(pTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarLiquidacionContrato(LiquidacionContrato pTerminacionAnticipada)
        {
            try
            {
                return new LiquidacionContratoBLL().ModificarLiquidacionContrato(pTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionContrato(int IdContrato)
        {
            try
            {
                return new LiquidacionContratoBLL().ConsultarLiquidacionContrato(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionId(int IdTerminacionAnticipada)
        {
            try
            {
                return new LiquidacionContratoBLL().ConsultarLiquidacionId(IdTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                return new LiquidacionContratoBLL().ConsultarLiquidacionIdDetalle(IdDetConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region CertificacionesContrato
        public int InsertarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                return _vCertificacionesContratosBLL.InsertarCertificacionesContratos(pCertificacionesContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pCertificacionesContratos"></param>
        public int ModificarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                return _vCertificacionesContratosBLL.ModificarCertificacionesContratos(pCertificacionesContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AdicionarArchivoACertificacion(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                return _vCertificacionesContratosBLL.AdicionarArchivoACertificacion(pCertificacionesContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pCertificacionesContratos"></param>
        public int EliminarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                return _vCertificacionesContratosBLL.EliminarCertificacionesContratos(pCertificacionesContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pIDCertificacion"></param>
        public CertificacionesContratos ConsultarCertificacionesContratos(int pIDCertificacion)
        {
            try
            {
                return _vCertificacionesContratosBLL.ConsultarCertificacionesContratos(pIDCertificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pIDContrato"></param>
        /// <param name="pFechaCertificacion"></param>
        /// <param name="pEstado"></param>
        public List<CertificacionesContratos> ConsultarCertificacionesContratoss(int? vVigencia, int? vIDRegional
                    , string vNumeroContrato, DateTime? vFechaInicial, DateTime? vFechaFinal)
        {
            try
            {
                return _vCertificacionesContratosBLL.ConsultarCertificacionesContratoss(vVigencia, vIDRegional, vNumeroContrato, vFechaInicial, vFechaFinal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CertificacionesContratos> ConsultarCertificacionesPorContratos(int IDContrato)
        {
            try
            {
                return _vCertificacionesContratosBLL.ConsultarCertificacionesPorContratos(IDContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Documentos Liquidacion

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de insertar en la tabla Documento Liquidacion
        /// Fecha: 12/05/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int InsertarDocumentosLiquidacion(DocumentosLiquidacion pDocumentoLiquidacion)
        {
            try
            {
                return new DocumentosLiquidacionBLL().InsertarDocumentosLiquidacion(pDocumentoLiquidacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Modificar en la tabla Documento Liquidacion
        /// Fecha: 12/05/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int ModificarDocumentosLiquidacion(DocumentosLiquidacion pDocumentoLiquidacion)
        {
            try
            {
                return new DocumentosLiquidacionBLL().ModificarDocumentosLiquidacion(pDocumentoLiquidacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Eliminar en la tabla Documento Liquidacion
        /// Fecha: 12/05/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int EliminarDocumentosLiquidacion(DocumentosLiquidacion pDocumentoLiquidacion)
        {
            try
            {
                return new DocumentosLiquidacionBLL().EliminarDocumentosLiquidacion(pDocumentoLiquidacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Consultar en la tabla Documento Liquidacion
        /// Fecha: 12/05/2016
        /// </summary>
        /// <param name="pIdDocumentoLiquidacion">Valor entero con el Id del numero del proceso</param>
        public DocumentosLiquidacion ConsultarDocumentosLiquidacion(int pIdDocumentoLiquidacion)
        {
            try
            {
                return new DocumentosLiquidacionBLL().ConsultarDocumentosLiquidacion(pIdDocumentoLiquidacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentosLiquidacion> ConsultarVariosDocumentosLiquidacion(Boolean? dEstado, string dNombre)
        {
            try
            {
                return new DocumentosLiquidacionBLL().ConsultarVariosDocumentosLiquidacion(dEstado, dNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Codigo SECOP

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de insertar en la CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int InsertarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                return new CodigosSECOPBLL().InsertarCodigoSECOP(cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Modificar en la tabla CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int ModificarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                return new CodigosSECOPBLL().ModificarCodigoSECOP(cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Eliminar en la tabla CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int EliminarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                return new CodigosSECOPBLL().EliminarCodigoSECOP(cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Consultar en la CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pIdDocumentoLiquidacion">Valor entero con el Id del numero del proceso</param>
        public CodigosSECOP ConsultarCodigoSECOP(int cCodigoSECOP)
        {
            try
            {
                return new CodigosSECOPBLL().ConsultarCodigoSECOP(cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CodigosSECOP> ConsultarVariosCodigosSECOP(Boolean? cEstado, string cCodigo, int ? cantidad = null)
        {
            try
            {
                return new CodigosSECOPBLL().ConsultarVariosCodigosSECOP(cEstado, cCodigo, cantidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarVinculoSECOP(int idContrato, string cCodigoSECOP)
        {
            try
            {
                return new CodigosSECOPBLL().ActualizarVinculoSECOP(idContrato, cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        #endregion 

        #region Tipo de Pago

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de insertar Tipos de Pago
        /// Fecha: 24/08/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago</param>
        public int InsertarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                return new TipoPagoBLL().InsertarTipoPago(pTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Modificar en la tabla CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago</param>
        public int ModificarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                return new TipoPagoBLL().ModificarTipoPago(pTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Eliminar en la tabla CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago</param>
        public int EliminarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                return new TipoPagoBLL().EliminarTipoPago(pTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Consultar en la CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago</param>
        public TipoPago ConsultarTipoPago(int cIdTipoPago)
        {
            try
            {
                return new TipoPagoBLL().ConsultarTipoPago(cIdTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoPago> ConsultarVariosTipoPago(Boolean? cEstado, string cNombre)
        {
            try
            {
                return new TipoPagoBLL().ConsultarVariosTipoPago(cEstado, cNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Roles

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de insertar Roles
        /// Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información del Rol</param>
        public int InsertarRoles(RolesContrato pRol)
        {
            try
            {
                return new RolesBLL().InsertarRoles(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Modificar en la tabla Roles
        /// Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los Roles</param>
        public int ModificarRoles(RolesContrato pRol)
        {
            try
            {
                return new RolesBLL().ModificarRoles(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Eliminar en la tabla Roles
        /// Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los roles</param>
        public int EliminarRoles(RolesContrato pRol)
        {
            try
            {
                return new RolesBLL().EliminarRoles(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Consultar en la Roles
        /// Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los Roles</param>
        public RolesContrato ConsultarRoles(int pIdRol)
        {
            try
            {
                return new RolesBLL().ConsultarRoles(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RolesContrato> ConsultarVariosRoles(Boolean? pEstado, string pNombre)
        {
            try
            {
                return new RolesBLL().ConsultarVariosRoles(pEstado, pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion 

        #region ObligacionesContratoReporte
        /// <summary>
        /// Método de inserción para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int InsertarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                return _vObligacionesContratoReporte.InsertarObligacionesContratoReporte(pObligacionesContratoReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int ModificarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                return _vObligacionesContratoReporte.ModificarObligacionesContratoReporte(pObligacionesContratoReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int EliminarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                return _vObligacionesContratoReporte.EliminarObligacionesContratoReporte(pObligacionesContratoReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pIDObligacionContrato"></param>
        public ObligacionesContratoReporte ConsultarObligacionesContratoReporte(int pIDObligacionContrato)
        {
            try
            {
                return _vObligacionesContratoReporte.ConsultarObligacionesContratoReporte(pIDObligacionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pIDContrato"></param>
        /// <param name="pDescripcionObligacion"></param>
        public List<ObligacionesContratoReporte> ConsultarObligacionesContratoReportes(int? pIDContrato)
        {
            try
            {
                return _vObligacionesContratoReporte.ConsultarObligacionesContratoReportes(pIDContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObligacionesContratoReporte> ConsultarObligacionesSigepcyp(int NumCDP, int NumDocIdentificacion, int AnioVigencia)
        {
            try
            {
                return _vObligacionesContratoReporte.ConsultarObligacionesSigepcyp(NumCDP, NumDocIdentificacion, AnioVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Reportes FUC


        public List<ReporteFUC> ObtenerReporteFUC
        (
        DateTime fechaDesde,
        DateTime fechaHasta,
        int vigencia,
        string idRegional,
        int? idModalidad,
        int? idCategoria,
        int? idTipoContrato,
        int? idEstadoContrato,
        int? idTipoPersona
        )
        {
            try
            {
                return new ReportesBLL().ObtenerReporteFUC(fechaDesde, fechaHasta, vigencia, idRegional, idModalidad, idCategoria, idTipoContrato, idEstadoContrato, idTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


        }


        public List<ReporteFUC> ObtenerReporteFUCHistorico
        (
        DateTime fechaDesde,
        DateTime fechaHasta,
        int vigencia,
        string idRegional,
        int? idModalidad,
        int? idCategoria,
        int? idTipoContrato,
        int? idEstadoContrato,
        int? idTipoPersona
        )
        {
            try
            {
                return new ReportesBLL().ObtenerReporteFUCHistorico(fechaDesde, fechaHasta, vigencia, idRegional, idModalidad, idCategoria, idTipoContrato, idEstadoContrato, idTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


        }


        #endregion

        #region Reporte de la Contraloría

        public string GenerarReporteContraloria(int vigencia, int periodo, string idRegional, string pathPlantilla)
        {
            try
            {
                return new ReporteContraloriaBLL().GenerarReporteContraloria(vigencia, periodo, idRegional, pathPlantilla);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Solicitudes CDP

        public int InsertarLoteSolicitudesCDP(SolicitudCDP item, string NombreArchivo)
        {
            try
            {
                return new SolicitudCDPBLL().InsertarLoteSolicitudesCDP(item, NombreArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TrazabilidadSolicitudCDP> ObtenerTrazabilidad()
        {
            try
            {
                return new SolicitudCDPBLL().ObtenerTrazabilidad();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.ObligacionesCDP ConsultarDatosContratoSigepcyp(int pIdContrato)
        {
            try
            {
                return vContratosCDPBLL.ConsultarDatosContratoSigepcyp(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Cargue Contratos Masivos

        public List<CargueContratoMasivo> ConsultarCargueContratosArchivos(int? idRegional)
        {
            try
            {
                return new ArchivoBLL().ConsultarCargueContratosArchivos(idRegional);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CargueContratoMasivo ValidarArchivo(Stream archivo, string usuario)
        {
            try
            {
                return new CargueArchivosBLL().ValidarArchivo(archivo, usuario);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string GenerarCargueArchivoValidaciones(List<itemCargueError> RegistrosErradosList, string pathPlantilla)
        {
            try
            {
                return new CargueArchivosBLL().GenerarArchivoValidaciones(RegistrosErradosList, pathPlantilla);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Codigo SECOP

        public int AsociarCodigoSECOPContrato(int idContrato, int idCodigoSecop)
        {
            try
            {
                return new CodigosSECOPBLL().AsociarCodigoSECOPContrato(idContrato, idCodigoSecop);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ObtenerCodigosSECOPContrato(int idContrato)
        {
            try
            {
                return new CodigosSECOPBLL().ObtenerCodigosSECOPContrato(idContrato);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarCodigoSECOPContrato(int idContrato, int idCodigoSecop)
        {
            try
            {
                return new CodigosSECOPBLL().EliminarCodigoSECOPContrato(idContrato, idCodigoSecop);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Modificación Contrato

        public List<Entity.Contrato> ConsultarContratosModificacion
            (
             DateTime? vFechaRegistroSistemaDesde,
             DateTime? vFechaRegistroSistemaHasta,
             int? vIdContrato,
             int? vVigenciaFiscalinicial,
             int? vIDRegional,
             int? vIDModalidadSeleccion,
             int? vIDCategoriaContrato,
             int? vIDTipoContrato,
             int? vIDEstadoContrato,
             string vNumeroContrato
            )
        {
            try
            {
                return new ContratoModificacionBLL().ConsultarContratosModificacion(vFechaRegistroSistemaDesde, vFechaRegistroSistemaHasta, vIdContrato, vVigenciaFiscalinicial, vIDRegional, vIDModalidadSeleccion, vIDCategoriaContrato, vIDTipoContrato, vIDEstadoContrato, vNumeroContrato);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ContratoModificacionDetalle ConsultarContratosModificacionPorId(int idContrato)
        {
            try
            {
                return new ContratoModificacionBLL().ConsultarPorId(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarContratosModificacion(ContratoModificacionEditar item)
        {
            try
            {
                return new ContratoModificacionBLL().Modificar(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Generación de Obligaciones Presupuestales

        public DataSet ConsultarContratosSuscritosObligacionesPresupuestales(string ids, string usuario, string rol)
        {
            try
            {
                return vContratoBLL.ConsultarContratosSuscritosObligacionesPresupuestales(ids, usuario, rol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarContratosSuscritosObligacionesPresupuestales(string ids, string usuario)
        {
            try
            {
                return vContratoBLL.ActualizarContratosSuscritosObligacionesPresupuestales(ids,usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosGenerarObligaciones(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratosGenerarObligaciones(pFechaSuscripcion, pNumeroContrato, pVigenciaFiscalinicial, pIDRegional, pIDCategoriaContrato, pIDTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosInformeObligaciones(DateTime? pFechaInicioDesde, DateTime? pFechaInicioHasta, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato)
        {
            try
            {
                return vContratoBLL.ConsultarContratosInformeObligaciones(pFechaInicioDesde, pFechaInicioHasta, pNumeroContrato, pVigenciaFiscalinicial, pIDRegional, pIDCategoriaContrato, pIDTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosGenerarAreas(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato, string idDependencia)
        {
            try
            {
                return vContratoBLL.ConsultarContratosGenerarAreas(pFechaSuscripcion, pNumeroContrato, pVigenciaFiscalinicial, pIDRegional, pIDCategoriaContrato, pIDTipoContrato,idDependencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataSet ConsultarContratosGenerarAreasDataSet(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato, string idDependencia)
        {
            try
            {
                return vContratoBLL.ConsultarContratosGenerarAreasDataSet(pFechaSuscripcion, pNumeroContrato, pVigenciaFiscalinicial, pIDRegional, pIDCategoriaContrato, pIDTipoContrato, idDependencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Historico Solicitudes Eliminadas

        public HistoricoSolicitudesEliminadas ConsultarSolicitudesEliminacion(int IDCosModContractual)
        {
            try
            {
                return _vHistoricoSolicitudesEliminadasBLL.ConsultarSolicitudesEliminacion(IDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarSolModContractualHistorico(HistoricoSolicitudesEliminadas psModContractual)
        {
            try
            {
                return _vHistoricoSolicitudesEliminadasBLL.InsertarSolModContractualHistorico(psModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoSolicitudesEliminadas> ConsultarSolicitudesElimindasHistorico()
        {
            try
            {
                return _vHistoricoSolicitudesEliminadasBLL.ConsultarSolicitudesElimindasHistorico();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        #endregion

    }
}
