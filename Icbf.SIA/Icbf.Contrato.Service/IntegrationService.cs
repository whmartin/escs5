﻿using Icbf.Contrato.Business;
using Icbf.Contrato.Entity;
using ICBF.MasterData.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Service
{
    public class IntegrationService
    {
        public InfoContratoDTO ObtenerInfoContratosPaccoPorId(int idPlanCompras)
        {
             try
             {
                 return new IntegrationContratosBLL().ObtenerInfoContratosPaccoPorId(idPlanCompras);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
        }

        public InfoContratoDTO ObtenerInfoContratosGeneral(int anio, int numero, string codigoRegional)
        {
            InfoContratoDTO result = null;
            try
            {
                result = new IntegrationContratosBLL().ObtenerInfoContratosGeneral(anio,numero,codigoRegional);

                if(result != null)
                {
                    string plazo = string.Empty;
                    bool convirtio = ContratoService.ObtenerDiferenciaFechas(result.FechaInicioEjecucion, result.FechaFinalTerminacionContrato, out plazo);
                    if(convirtio)
                        result.PlazoEjecuccion = plazo;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<InfoContratoDTO> ObtenerInfoContratoGeneralPorPeriodo(string codigoRegional, DateTime fechaDesde, DateTime fechaHasta)
        {
            try
            {
                return new IntegrationContratosBLL().ObtenerInfoContratoGeneralPorPeriodo(codigoRegional, fechaDesde, fechaHasta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ConsultaCDPMaestro ObtenerCDP(string codigoCDP, int regional)
        {
            ConsultaCDPMaestro result = new ConsultaCDPMaestro();

            try
            {
                result = new MasterDataBLL().ObtenerCDP(codigoCDP, regional);
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public List<InfoContratoDTO> GetInfoContratoGeneralPorRubro(string codigoRegional, int vigencia, string codigoRubro, int numeroContrato)
        {
            try
            {
                return new IntegrationContratosBLL().GetInfoContratoGeneralPorRubro(codigoRegional, vigencia, codigoRubro, numeroContrato);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RPContrato> ObtenerRP(int vIdRP, int vRegionalICBF, int tipoCompromiso, int tipoVigencia)
        {
            List<RPContrato> result = new List<RPContrato>();

            try
            {
                result = new MasterDataBLL().ObtenerRP(vIdRP, vRegionalICBF, tipoCompromiso, tipoVigencia);
            }
            catch(Exception ex)
            {
            }

            return result;
        }
    }
}
