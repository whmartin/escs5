﻿
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class HistoricoDocumentosSolicitadosDenunciaBienDAL : GeneralDAL
    {
        /// <summary>
        /// consulta la información de los históricos de documentos solicitados filtrados por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>lista de tipo  HistóricoDocumentosSolicitadosDenunciaBien con la información resultante de la consulta</returns>
        public List<HistoricoDocumentosSolicitadosDenunciaBien> ConsultarHistoricoDocumentosDenunciaPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoDocumentosSolicitadosDenunciaBien_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoDocumentosSolicitadosDenunciaBien> vListaHistorico = new List<HistoricoDocumentosSolicitadosDenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            HistoricoDocumentosSolicitadosDenunciaBien vHistorico = new HistoricoDocumentosSolicitadosDenunciaBien();
                            vHistorico.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vHistorico.FechaRecibido;
                            vHistorico.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"]) : vHistorico.FechaSolicitud;
                            vHistorico.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vHistorico.IdDocumentosSolicitadosDenunciaBien;
                            vHistorico.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vHistorico.IdEstadoDocumento;
                            vHistorico.IdHistoricoDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdHistoricoDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoDocumentosSolicitadosDenunciaBien"].ToString()) : vHistorico.IdHistoricoDocumentosSolicitadosDenunciaBien;
                            vHistorico.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"]) : vHistorico.IdTipoDocumentoBienDenunciado;
                            vHistorico.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vHistorico.NombreArchivo;
                            vHistorico.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistorico.UsuarioCrea;
                            vHistorico.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistorico.FechaCrea;
                            vHistorico.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistorico.UsuarioModifica;
                            vHistorico.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistorico.FechaModifica;
                        }

                        return vListaHistorico;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo registro en la tabla HistóricoDocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pHistorico"> variable de tipo HistóricoDocumentosSolicitadosDenunciaBien con la información a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int InsertarHistoricoDocumentosSolicitadosDenunciaBien(HistoricoDocumentosSolicitadosDenunciaBien pHistorico)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoDocumentosSolicitadosDenunciaBien_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoDocumentosSolicitadosDenunciaBien", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int64, pHistorico.IdDocumentosSolicitadosDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int64, pHistorico.IdTipoDocumentoBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pHistorico.FechaSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pHistorico.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pHistorico.FechaRecibido);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pHistorico.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistorico.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoDocumentosSolicitadosDenunciaBien"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pHistorico;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pHistorico, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
