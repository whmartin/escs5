﻿//-----------------------------------------------------------------------
// <copyright file="EstadoTerceroDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase EstadoTerceroDAL.</summary>
// <author>INDESAP[Jesus Eduardo Cortes]</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;    

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class EstadoTerceroDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta el estado de un tercero por el id estado
        /// </summary>
        /// <param name="pIdEstadoTercero">Id del estado del tercero</param>
        /// <returns>Variable de tipo Estado tercero con la información encontrada</returns>
        public EstadoTercero ConsultarEstadoTerceroXId(int pIdEstadoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EstadoTercero_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoTercero", DbType.Int32, pIdEstadoTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoTercero vEstadoTercero = new EstadoTercero();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoTercero.CodigoEstadotercero = vDataReaderResults["CodigoEstadotercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadotercero"].ToString()) : vEstadoTercero.CodigoEstadotercero;
                            vEstadoTercero.CodPCI = vDataReaderResults["CodPCI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodPCI"]) : vEstadoTercero.CodPCI;
                            vEstadoTercero.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vEstadoTercero.DescripcionEstado;
                            vEstadoTercero.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vEstadoTercero.Estado;
                            vEstadoTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoTercero.UsuarioCrea;
                            vEstadoTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoTercero.FechaCrea;
                            vEstadoTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoTercero.UsuarioModifica;
                            vEstadoTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoTercero.FechaModifica;
                        }

                        vEstadoTercero.IdEstadoTercero = pIdEstadoTercero;
                        return vEstadoTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}