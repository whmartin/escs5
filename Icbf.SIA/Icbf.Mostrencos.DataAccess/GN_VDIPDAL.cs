﻿//-----------------------------------------------------------------------
// <copyright file="GN_VDIPDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase GN_VDIPDAL.</summary>
// <author>INGENIAN</author>
// <date>16/01/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class GN_VDIPDAL : GeneralDAL
    {
        private Database vDataBaseSeven;

        public GN_VDIPDAL()
        {
            this.vDataBaseSeven = DatabaseFactory.CreateDatabase("DataBaseSEVEN");
        }

        /// <summary>
        /// Consulta los departamentos de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        public List<GN_VDIP> ConsultarDepartamentos()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Departamento_sConsultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdPais", DbType.Int32, 41);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GN_VDIP> vListGN_VDIP = new List<GN_VDIP>();
                        while (vDataReaderResults.Read())
                        {
                            GN_VDIP vGN_VDIP = new GN_VDIP();
                            vGN_VDIP.CODIGO_DEPARTAMENTO = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDepartamento"].ToString()) : vGN_VDIP.CODIGO_DEPARTAMENTO;
                            vGN_VDIP.NOMBRE_DEPARTAMENTO = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString().ToUpper()) : vGN_VDIP.NOMBRE_DEPARTAMENTO;
                            vListGN_VDIP.Add(vGN_VDIP);
                            vGN_VDIP = null;
                        }
                        return vListGN_VDIP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los municipios de un departamento de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        /// <param name="pCodMunicipio">id del departamento</param>
        public List<GN_VDIP> ConsultarMunicipios(string pCodDepto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_DIV_Municipio_sConsultarPorIdsDep"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdsDepartamento", DbType.String, pCodDepto);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GN_VDIP> vListGN_VDIP = new List<GN_VDIP>();
                        while (vDataReaderResults.Read())
                        {
                            GN_VDIP vGN_VDIP = new GN_VDIP();
                            vGN_VDIP.CODIGO_MUNICIPIO = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"].ToString()) : vGN_VDIP.CODIGO_MUNICIPIO;
                            vGN_VDIP.NOMBRE_MUNICIPIO = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString().ToUpper()) : vGN_VDIP.NOMBRE_MUNICIPIO;
                            vListGN_VDIP.Add(vGN_VDIP);
                            vGN_VDIP = null;


                        }
                        return vListGN_VDIP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }       

        /// <summary>
        /// Consulta los departamentos de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        public List<GN_VDIP> ConsultarDepartamentosSeven()
        {
            try
            {
                using (DbConnection vDbConnection = vDataBaseSeven.CreateConnection())
                {
                    vDbConnection.Open();
                    DbCommand vDbCommand = vDbConnection.CreateCommand();
                    vDbCommand.CommandText = "SELECT DISTINCT([CODIGO_DEPARTAMENTO]) ,[NOMBRE_DEPARTAMENTO] FROM [produccion].[dbo].[GN_VDIP] WHERE [CODIGP_PAIS] = 169";
                    using (IDataReader vDataReaderResults = vDataBaseSeven.ExecuteReader(vDbCommand))
                    {
                        List<GN_VDIP> vListGN_VDIP = new List<GN_VDIP>();
                        while (vDataReaderResults.Read())
                        {
                            GN_VDIP vGN_VDIP = new GN_VDIP();
                            vGN_VDIP.CODIGO_DEPARTAMENTO = vDataReaderResults["CODIGO_DEPARTAMENTO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CODIGO_DEPARTAMENTO"].ToString()) : vGN_VDIP.CODIGO_DEPARTAMENTO;
                            vGN_VDIP.NOMBRE_DEPARTAMENTO = vDataReaderResults["NOMBRE_DEPARTAMENTO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBRE_DEPARTAMENTO"].ToString().ToUpper()) : vGN_VDIP.NOMBRE_DEPARTAMENTO;
                            vListGN_VDIP.Add(vGN_VDIP);
                            vGN_VDIP = null;
                        }

                        vDbConnection.Close();
                        return vListGN_VDIP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los municipios de un departamento de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        /// <param name="pCodMunicipio">id del departamento</param>
        public List<GN_VDIP> ConsultarMunicipiosSeven(string pCodDepto)
        {
            try
            {
                using (DbConnection vDbConnection = vDataBaseSeven.CreateConnection())
                {
                    vDbConnection.Open();
                    DbCommand vDbCommand = vDbConnection.CreateCommand();
                    vDbCommand.CommandText = "SELECT DISTINCT([CODIGO_MUNICIPIO]) ,[NOMBRE_MUNICIPIO] FROM [produccion].[dbo].[GN_VDIP] WHERE [CODIGO_DEPARTAMENTO] =" + pCodDepto;
                    using (IDataReader vDataReaderResults = vDataBaseSeven.ExecuteReader(vDbCommand))
                    {
                        List<GN_VDIP> vListGN_VDIP = new List<GN_VDIP>();
                        while (vDataReaderResults.Read())
                        {
                            GN_VDIP vGN_VDIP = new GN_VDIP();
                            vGN_VDIP.CODIGO_MUNICIPIO = vDataReaderResults["CODIGO_MUNICIPIO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CODIGO_MUNICIPIO"].ToString()) : vGN_VDIP.CODIGO_MUNICIPIO;
                            vGN_VDIP.NOMBRE_MUNICIPIO = vDataReaderResults["NOMBRE_MUNICIPIO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBRE_MUNICIPIO"].ToString().ToUpper()) : vGN_VDIP.NOMBRE_MUNICIPIO;
                            vListGN_VDIP.Add(vGN_VDIP);
                            vGN_VDIP = null;
                        }

                        vDbConnection.Close();
                        return vListGN_VDIP;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }       
    }
}
