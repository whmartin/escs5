
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using Icbf.Seguridad.Entity;
    using System.Data.SqlClient;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class HistoricoEstadosDenunciaBienDAL : GeneralDAL
    {
        /// <summary>
        /// inserta un nuevo Histórico Estados DenunciaBien
        /// </summary>
        /// <param name="pHistoricoEstadosDenunciaBien">Variable de tipo Histórico Estados DenunciaBien que contiene la información del histórico</param>
        /// <returns>id del histórico insertado</returns>
        public int InsertarHistoricoEstadosDenunciaBien(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoEstadosDenunciaBien_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoEstadoDenunciaBien", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pHistoricoEstadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDenuncia", DbType.Int32, pHistoricoEstadosDenunciaBien.IdEstadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pHistoricoEstadosDenunciaBien.IdUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdFase", DbType.Int32, pHistoricoEstadosDenunciaBien.IdFase);
                    vDataBase.AddInParameter(vDbCommand, "@IdAccion", DbType.Int32, pHistoricoEstadosDenunciaBien.IdAccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdActuacion", DbType.Int32, pHistoricoEstadosDenunciaBien.IdActuacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pHistoricoEstadosDenunciaBien;
                    vAuditorio.ProgramaGeneraLog = true;
                    vAuditorio.Operacion = "INSERT";
                    pHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoEstadoDenunciaBien").ToString());
                    this.GenerarLogAuditoria(pHistoricoEstadosDenunciaBien, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el histórico de estados de la denuncia por el id del histórico
        /// </summary>
        /// <param name="pIdHistoricoEstadoDenunciaBien">id del histórico a consultar</param>
        /// <returns>variable de tipo Histórico Estados DenunciaBien con la información consultada</returns>
        public HistoricoEstadosDenunciaBien ConsultarHistoricoEstadosDenunciaBien(int pIdHistoricoEstadoDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoEstadosDenunciaBien_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdHistoricoEstadoDenunciaBien", DbType.Int32, pIdHistoricoEstadoDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        HistoricoEstadosDenunciaBien vHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
                        while (vDataReaderResults.Read())
                        {
                            vHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien = vDataReaderResults["IdHistoricoEstadoDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoEstadoDenunciaBien"].ToString()) : vHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien;
                            vHistoricoEstadosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vHistoricoEstadosDenunciaBien.IdDenunciaBien;
                            vHistoricoEstadosDenunciaBien.IdEstadoDenuncia = vDataReaderResults["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDenuncia"].ToString()) : vHistoricoEstadosDenunciaBien.IdEstadoDenuncia;
                            vHistoricoEstadosDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoEstadosDenunciaBien.UsuarioCrea;
                            vHistoricoEstadosDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoEstadosDenunciaBien.FechaCrea;
                            vHistoricoEstadosDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoEstadosDenunciaBien.UsuarioModifica;
                            vHistoricoEstadosDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoEstadosDenunciaBien.FechaModifica;
                        }

                        return vHistoricoEstadosDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta todos los históricos de los estados 
        /// </summary>
        /// <returns>lista de tipo Histórico Estados DenunciaBien con la información consultada</returns>
        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBien()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoEstadosDenunciaBiens_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoEstadosDenunciaBien> vListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();

                        while (vDataReaderResults.Read())
                        {
                            vListaHistoricoEstadosDenunciaBien.Add(new HistoricoEstadosDenunciaBien()
                            {
                                IdHistoricoEstadoDenunciaBien = vDataReaderResults["IdHistoricoEstadoDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoEstadoDenunciaBien"].ToString()) : 0,
                                IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : 0,
                                NombreEstadoDenuncia = vDataReaderResults["EstadoDenuncia"].ToString(),
                                UsuarioCrea = vDataReaderResults["UsuarioCrea"].ToString(),
                                FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : DateTime.Today,
                                UsuarioModifica = vDataReaderResults["UsuarioModifica"].ToString(),
                                FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : DateTime.Today,
                                NombreEstado = vDataReaderResults["NombreEstado"].ToString()
                                //Responsable = vDataReaderResults["Responsable"].ToString()
                            });
                        }

                        return vListaHistoricoEstadosDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta todos los históricos de los estados 
        /// </summary>
        /// <returns>lista de tipo Histórico Estados DenunciaBien con la información consultada</returns>
        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBienByID(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoEstadosDenunciaBien_ConsultarByIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoEstadosDenunciaBien> vListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();

                        while (vDataReaderResults.Read())
                        {
                            vListaHistoricoEstadosDenunciaBien.Add(new HistoricoEstadosDenunciaBien()
                            {
                                IdHistoricoEstadoDenunciaBien = vDataReaderResults["IdHistoricoEstadoDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoEstadoDenunciaBien"].ToString()) : 0,
                                IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : 0,
                                NombreEstadoDenuncia = vDataReaderResults["EstadoDenuncia"].ToString(),
                                UsuarioCrea = vDataReaderResults["UsuarioCrea"].ToString(),
                                FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : DateTime.Today,
                                UsuarioModifica = vDataReaderResults["UsuarioModifica"].ToString(),
                                FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : DateTime.Today,
                                NombreEstado = vDataReaderResults["NombreEstado"].ToString(),
                                Fase = vDataReaderResults["Fase"].ToString(),
                                Actuacion = vDataReaderResults["Actuacion"].ToString(),
                                Accion = vDataReaderResults["Accion"].ToString()
                            });
                        }

                        return vListaHistoricoEstadosDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los históricos de los estados de una denuncia por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de la denuncia consultada </returns>
        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoEstadosDenunciaBien_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoEstadosDenunciaBien> vListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            HistoricoEstadosDenunciaBien vHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
                            vHistoricoEstadosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vHistoricoEstadosDenunciaBien.IdDenunciaBien;
                            vHistoricoEstadosDenunciaBien.IdEstadoDenuncia = vDataReaderResults["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDenuncia"].ToString()) : vHistoricoEstadosDenunciaBien.IdEstadoDenuncia;
                            vHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien = vDataReaderResults["IdHistoricoEstadoDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoEstadoDenunciaBien"]) : vHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien;
                            vHistoricoEstadosDenunciaBien.IdUsuarioCrea = vDataReaderResults["IdUsuarioCreacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuarioCreacion"]) : vHistoricoEstadosDenunciaBien.IdUsuarioCrea;
                            vHistoricoEstadosDenunciaBien.Fase = vDataReaderResults["Fase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Fase"].ToString()) : vHistoricoEstadosDenunciaBien.Fase;
                            vHistoricoEstadosDenunciaBien.NombreEstadoDenuncia = vDataReaderResults["EstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoDenuncia"].ToString()) : vHistoricoEstadosDenunciaBien.Accion;
                            vHistoricoEstadosDenunciaBien.Accion = vDataReaderResults["Accion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Accion"].ToString()) : vHistoricoEstadosDenunciaBien.Accion;
                            vHistoricoEstadosDenunciaBien.Actuacion = vDataReaderResults["Actuacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Actuacion"].ToString()) : vHistoricoEstadosDenunciaBien.Actuacion;
                            vHistoricoEstadosDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoEstadosDenunciaBien.UsuarioCrea;
                            vHistoricoEstadosDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoEstadosDenunciaBien.UsuarioCrea;
                            vHistoricoEstadosDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoEstadosDenunciaBien.FechaCrea;
                            vHistoricoEstadosDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoEstadosDenunciaBien.UsuarioModifica;
                            vHistoricoEstadosDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoEstadosDenunciaBien.FechaModifica;
                            vListaHistoricoEstadosDenunciaBien.Add(vHistoricoEstadosDenunciaBien);
                            vHistoricoEstadosDenunciaBien = null;
                        }

                        return vListaHistoricoEstadosDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoEstadosDenunciaBien> HistoricoEstadosDenunciaBienConsultar(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_HistoricoEstadosDenunciaBien_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoEstadosDenunciaBien> vListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            HistoricoEstadosDenunciaBien vHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
                            vHistoricoEstadosDenunciaBien.IdEstadoDenuncia = vDataReaderResults["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDenuncia"].ToString()) : vHistoricoEstadosDenunciaBien.IdEstadoDenuncia;
                            vHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien = vDataReaderResults["IdHistoricoEstadoDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoEstadoDenunciaBien"]) : vHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien;
                            vHistoricoEstadosDenunciaBien.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vHistoricoEstadosDenunciaBien.NombreEstado;
                            vHistoricoEstadosDenunciaBien.Responsable = vDataReaderResults["Responsable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Responsable"].ToString()) : vHistoricoEstadosDenunciaBien.Responsable;
                            vHistoricoEstadosDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoEstadosDenunciaBien.FechaCrea;
                            vHistoricoEstadosDenunciaBien.Fase = vDataReaderResults["Fase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Fase"].ToString()) : vHistoricoEstadosDenunciaBien.Fase;
                            vHistoricoEstadosDenunciaBien.Accion = vDataReaderResults["Accion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Accion"].ToString()) : vHistoricoEstadosDenunciaBien.Accion;
                            vHistoricoEstadosDenunciaBien.Actuacion = vDataReaderResults["Actuacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Actuacion"].ToString()) : vHistoricoEstadosDenunciaBien.Actuacion;
                            vListaHistoricoEstadosDenunciaBien.Add(vHistoricoEstadosDenunciaBien);
                            vHistoricoEstadosDenunciaBien = null;
                        }

                        return vListaHistoricoEstadosDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// inserta un nuevo Histórico Estados DenunciaBien
        /// </summary>
        /// <param name="pHistoricoEstadosDenunciaBien">Variable de tipo Histórico Estados DenunciaBien que contiene la información del histórico</param>
        /// <returns>id del histórico insertado</returns>
        public int InsertarHistoricoEstadosDenunciaBienGeneral(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_HistoricoEstadosDenunciaBien_InsertarGeneral"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoEstadoDenunciaBien", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pHistoricoEstadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pHistoricoEstadosDenunciaBien.IdUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistoricoEstadosDenunciaBien.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdFase", DbType.Int32, pHistoricoEstadosDenunciaBien.IdFase);
                    vDataBase.AddInParameter(vDbCommand, "@IdAccion", DbType.Int32, pHistoricoEstadosDenunciaBien.IdAccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdActuacion", DbType.Int32, pHistoricoEstadosDenunciaBien.IdActuacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDenuncia", DbType.Int32, pHistoricoEstadosDenunciaBien.IdEstadoDenuncia);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pHistoricoEstadosDenunciaBien;
                    vAuditorio.ProgramaGeneraLog = true;
                    vAuditorio.Operacion = "INSERT";
                    pHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoEstadoDenunciaBien").ToString());
                    this.GenerarLogAuditoria(pHistoricoEstadosDenunciaBien, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarHistoricoEstadosDenunciaXContratoParticipacion(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoEstadosDenunciaBien_InsertarContratoParticipacion"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoEstadoDenunciaBien", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pHistoricoEstadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.String, pHistoricoEstadosDenunciaBien.IdUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistoricoEstadosDenunciaBien.UsuarioCrea);

                    //if (pHistoricoEstadosDenunciaBien.UsuarioModifica != null)
                    //{
                    //    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pHistoricoEstadosDenunciaBien.UsuarioModifica);
                    //}

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        EntityAuditoria vAuditorio = (EntityAuditoria)pHistoricoEstadosDenunciaBien;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        pHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoEstadoDenunciaBien").ToString());
                        this.GenerarLogAuditoria(pHistoricoEstadosDenunciaBien, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarHistoricoEstadosDenunciaXContratoParticipacionDocumentacionRecibida(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoEstadosDenunciaBien_InsertarDocumentacionRecibida"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoEstadoDenunciaBien", DbType.Int32, 18);

                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pHistoricoEstadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDenuncia", DbType.Int32, pHistoricoEstadosDenunciaBien.IdEstadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdActuacion", DbType.String, pHistoricoEstadosDenunciaBien.IdActuacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdFase", DbType.String, pHistoricoEstadosDenunciaBien.IdFase);
                    vDataBase.AddInParameter(vDbCommand, "@IdAccion", DbType.String, pHistoricoEstadosDenunciaBien.IdAccion);

                    //vDataBase.AddInParameter(vDbCommand, "@NuevaFase", DbType.String, pHistoricoEstadosDenunciaBien.Fase);
                    //vDataBase.AddInParameter(vDbCommand, "@NuevaAccion", DbType.String, pHistoricoEstadosDenunciaBien.Accion);
                    //vDataBase.AddInParameter(vDbCommand, "@EstadoProceso", DbType.String, 2);

                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.String, pHistoricoEstadosDenunciaBien.IdUsuarioCrea);
                    //vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pHistoricoEstadosDenunciaBien.FechaCrea);
                    //if (pHistoricoEstadosDenunciaBien.UsuarioModifica != null)
                    //{
                    //    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pHistoricoEstadosDenunciaBien.UsuarioModifica);
                    //}

                    //if (pHistoricoEstadosDenunciaBien.FechaModifica != new DateTime())
                    //{
                    //    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pHistoricoEstadosDenunciaBien.FechaModifica);
                    //}

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        EntityAuditoria vAuditorio = (EntityAuditoria)pHistoricoEstadosDenunciaBien;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        pHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoEstadoDenunciaBien").ToString());
                        this.GenerarLogAuditoria(pHistoricoEstadosDenunciaBien, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta todas las fases
        /// </summary>
        /// <returns></returns>
        public List<FasesDenuncia> FasesConsultarTodos()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_FasesDenuncia_Consultar_Lista"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FasesDenuncia> vListaFases = new List<FasesDenuncia>();

                        while (vDataReaderResults.Read())
                        {
                            FasesDenuncia vFase = new FasesDenuncia();
                            vFase.IdFase = vDataReaderResults["IdFase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFase"].ToString()) : vFase.IdFase;
                            vFase.NombreFase = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vFase.NombreFase;
                            //vFase.UsuarioCrea = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vFase.UsuarioCrea;
                            //vFase.FechaCrea = vDataReaderResults["Responsable"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Responsable"].ToString()) : vFase.FechaCrea;
                            //vFase.UsuarioModifica = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaCrea"].ToString()) : vFase.UsuarioModifica;
                            //vFase.FechaModifica = vDataReaderResults["Fase"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fase"].ToString()) : vFase.FechaModifica;
                            vListaFases.Add(vFase);
                            vFase = null;
                        }
                        return vListaFases;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// inserta un nuevo Histórico Estados DenunciaBien
        /// </summary>
        /// <param name="pHistoricoEstadosDenunciaBien">Variable de tipo Histórico Estados DenunciaBien que contiene la información del histórico</param>
        /// <returns>id del histórico insertado</returns>
        public int InsertarHistoricoEstadosDenuncia(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            int vResultado;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoEstadosDenuncia_Insertar"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pHistoricoEstadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDenuncia", DbType.Int32, pHistoricoEstadosDenunciaBien.IdEstadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdFase", DbType.Int32, pHistoricoEstadosDenunciaBien.IdFase);
                    vDataBase.AddInParameter(vDbCommand, "@IdActuacion", DbType.Int32, pHistoricoEstadosDenunciaBien.IdActuacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAccion", DbType.Int32, pHistoricoEstadosDenunciaBien.IdAccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pHistoricoEstadosDenunciaBien.IdUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistoricoEstadosDenunciaBien.UsuarioCrea);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);

                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pHistoricoEstadosDenunciaBien;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pHistoricoEstadosDenunciaBien, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que valida si la denuncia ya ejecuto la Fse de "Protocolización" y la acción Comunicación Administrativa y Financiera
        /// </summary>
        /// <param name="pIdDenunciaBien">Id de la Denuncia</param>
        /// <returns>Resultadod e la operación (Bool)</returns>
        public List<HistoricoEstadosDenunciaBien> ValidaFaseAccion(int pIdDenunciaBien, int pFase, int? pAccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_HistoricoEstadosDenunciaBien_ConsultarFaseAccion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@Fase", DbType.Int32, pFase);

                    if (pAccion != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Accion", DbType.Int32, pAccion);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoEstadosDenunciaBien> vListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            HistoricoEstadosDenunciaBien vHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();

                            vHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien = vDataReaderResults["IdHistoricoEstadoDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoEstadoDenunciaBien"]) : vHistoricoEstadosDenunciaBien.IdHistoricoEstadoDenunciaBien;
                            vHistoricoEstadosDenunciaBien.IdFase = vDataReaderResults["IdFase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFase"].ToString()) : vHistoricoEstadosDenunciaBien.IdFase;
                            vHistoricoEstadosDenunciaBien.IdAccion = vDataReaderResults["IdAccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAccion"].ToString()) : vHistoricoEstadosDenunciaBien.IdAccion;
                            vListaHistoricoEstadosDenunciaBien.Add(vHistoricoEstadosDenunciaBien);
                            vHistoricoEstadosDenunciaBien = null;
                        }

                        return vListaHistoricoEstadosDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
