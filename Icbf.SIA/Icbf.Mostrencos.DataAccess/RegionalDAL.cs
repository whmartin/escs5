﻿
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class RegionalDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta la regional por el id regional
        /// </summary>
        /// <param name="pIdRegional">id de la regional que se va a consultar</param>
        /// <returns>variable de tipo Regional con la información encontrada</returns>
        public Regional ConsultarRegionalXId(int pIdRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Regional_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Regional vRegional = new Regional();
                        while (vDataReaderResults.Read())
                        {
                            vRegional.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vRegional.CodigoRegional;
                            vRegional.CodPCI = vDataReaderResults["CodPCI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodPCI"]) : vRegional.CodPCI;
                            vRegional.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vRegional.Estado;
                            vRegional.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegional.NombreRegional;
                            vRegional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegional.UsuarioCrea;
                            vRegional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegional.FechaCrea;
                            vRegional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegional.UsuarioModifica;
                            vRegional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegional.FechaModifica;
                        }

                        vRegional.IdRegional = pIdRegional;
                        return vRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

       
    }
}
