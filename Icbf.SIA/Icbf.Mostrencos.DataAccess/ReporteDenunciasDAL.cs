using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.DataAccess
{
    public class ReporteDenunciasDAL : GeneralDAL
    {
        public ReporteDenunciasDAL()
        {
        }
        public int InsertarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_ReporteDenuncias_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdReporteDenuncias", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersonaAsociacion", DbType.Int32, pReporteDenuncias.IdTipoPersonaAsociacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pReporteDenuncias.IdTipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pReporteDenuncias.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@DepartamentoUbicacion", DbType.String, pReporteDenuncias.DepartamentoUbicacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pReporteDenuncias.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicial", DbType.String, pReporteDenuncias.RegistradoDesde);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinal", DbType.String, pReporteDenuncias.RegistradoHasta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pReporteDenuncias.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pReporteDenuncias.IdReporteDenuncias = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdReporteDenuncias").ToString());
                    GenerarLogAuditoria(pReporteDenuncias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_ReporteDenuncias_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReporteDenuncias", DbType.Int32, pReporteDenuncias.IdReporteDenuncias);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersonaAsociacion", DbType.Int32, pReporteDenuncias.IdTipoPersonaAsociacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pReporteDenuncias.IdTipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pReporteDenuncias.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@DepartamentoUbicacion", DbType.String, pReporteDenuncias.DepartamentoUbicacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pReporteDenuncias.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicial", DbType.String, pReporteDenuncias.RegistradoDesde);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinal", DbType.String, pReporteDenuncias.RegistradoHasta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pReporteDenuncias.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pReporteDenuncias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_ReporteDenuncias_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReporteDenuncias", DbType.Int32, pReporteDenuncias.IdReporteDenuncias);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pReporteDenuncias, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public ReporteDenuncias ConsultarReporteDenuncias(int pIdReporteDenuncias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_ReporteDenuncias_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdReporteDenuncias", DbType.Int32, pIdReporteDenuncias);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ReporteDenuncias vReporteDenuncias = new ReporteDenuncias();
                        while (vDataReaderResults.Read())
                        {
                            vReporteDenuncias.IdReporteDenuncias = vDataReaderResults["IdReporteDenuncias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporteDenuncias"].ToString()) : vReporteDenuncias.IdReporteDenuncias;
                            vReporteDenuncias.IdTipoPersonaAsociacion = vDataReaderResults["IdTipoPersonaAsociacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersonaAsociacion"].ToString()) : vReporteDenuncias.IdTipoPersonaAsociacion;
                            vReporteDenuncias.IdTipoIdentificacion = vDataReaderResults["IdTipoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacion"].ToString()) : vReporteDenuncias.IdTipoIdentificacion;
                            vReporteDenuncias.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vReporteDenuncias.NumeroIdentificacion;
                            vReporteDenuncias.DepartamentoUbicacion = vDataReaderResults["DepartamentoUbicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DepartamentoUbicacion"].ToString()) : vReporteDenuncias.DepartamentoUbicacion;
                            vReporteDenuncias.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vReporteDenuncias.Estado;
                            vReporteDenuncias.RegistradoDesde = vDataReaderResults["FechaInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicial"].ToString()) : vReporteDenuncias.RegistradoDesde;
                            vReporteDenuncias.RegistradoHasta = vDataReaderResults["FechaFinal"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinal"].ToString()) : vReporteDenuncias.RegistradoHasta;
                            vReporteDenuncias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReporteDenuncias.UsuarioCrea;
                            vReporteDenuncias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReporteDenuncias.FechaCrea;
                            vReporteDenuncias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReporteDenuncias.UsuarioModifica;
                            vReporteDenuncias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReporteDenuncias.FechaModifica;
                        }
                        return vReporteDenuncias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarReporteDenunciass(ReporteDenunciaFiltros pFiltros)
        {
            try
            {
                int vCantidadRegistros = 0;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_ReporteDenuncias_Consultar"))
                {
                    if (!string.IsNullOrEmpty(pFiltros.TipoPersonaAsociacion))
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersonaAsociacion", DbType.AnsiString, pFiltros.TipoPersonaAsociacion);

                    if (pFiltros.TipoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.Int32, pFiltros.TipoIdentificacion);

                    if (pFiltros.NumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pFiltros.NumeroIdentificacion);

                    if (!string.IsNullOrEmpty(pFiltros.DepartamentoUbicacion))
                        vDataBase.AddInParameter(vDbCommand, "@DepartamentoUbicacion", DbType.AnsiString, pFiltros.DepartamentoUbicacion);

                    if (!string.IsNullOrEmpty(pFiltros.Estado))
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.AnsiString, pFiltros.Estado);

                    if (pFiltros.RegistradoDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegistradoDesde", DbType.DateTime, pFiltros.RegistradoDesde);

                    if (pFiltros.RegistradoHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@RegistradoHasta", DbType.DateTime, pFiltros.RegistradoHasta);

                    if (!string.IsNullOrEmpty(pFiltros.ClaseDenuncia))
                        vDataBase.AddInParameter(vDbCommand, "@ClaseDenuncia", DbType.AnsiString, pFiltros.ClaseDenuncia);

                    if (pFiltros.Fase != null)
                        vDataBase.AddInParameter(vDbCommand, "@Fase", DbType.Int32, pFiltros.Fase);

                    if (!string.IsNullOrEmpty(pFiltros.DecisionReconociemiento))
                        vDataBase.AddInParameter(vDbCommand, "@DecisionReconociemiento", DbType.AnsiString, pFiltros.DecisionReconociemiento);

                    if (pFiltros.TipoTramite != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoTramite", DbType.Int32, pFiltros.TipoTramite);

                    if (pFiltros.DecisionTramiteNot != null)
                        vDataBase.AddInParameter(vDbCommand, "@DecisionTramiteNot", DbType.Int32, pFiltros.DecisionTramiteNot);

                    if (pFiltros.ResultadoDesicionTramiteNot != null)
                        vDataBase.AddInParameter(vDbCommand, "@ResultadoDesicionTramiteNot", DbType.Int32, pFiltros.ResultadoDesicionTramiteNot);

                    if (pFiltros.TerminacionAnormalTramiteNot != null)
                        vDataBase.AddInParameter(vDbCommand, "@TerminacionAnormalTramiteNot", DbType.Int32, pFiltros.TerminacionAnormalTramiteNot);

                    if (pFiltros.DecisionAuto != null)
                        vDataBase.AddInParameter(vDbCommand, "@DecisionAuto", DbType.AnsiString, pFiltros.DecisionAuto);

                    if (pFiltros.ResultadoDecisionAuto != null)
                        vDataBase.AddInParameter(vDbCommand, "@ResultadoDecisionAuto", DbType.Int32, pFiltros.ResultadoDecisionAuto);

                    if (!string.IsNullOrEmpty(pFiltros.SentidoSentencia))
                        vDataBase.AddInParameter(vDbCommand, "@SentidoSentencia", DbType.AnsiString, pFiltros.SentidoSentencia);

                    if (pFiltros.ModalidadPago != null)
                        vDataBase.AddInParameter(vDbCommand, "@ModalidadPago", DbType.Int32, pFiltros.ModalidadPago);

                    IDataReader vIdr = vDataBase.ExecuteReader(vDbCommand);
                    if (vIdr != null)
                    {
                        while (vIdr.Read())
                        {
                            vCantidadRegistros += 1;
                        }

                        return vCantidadRegistros;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarTipoPersonaAsociacion()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoPersonaAsociacionReporteDenuncias_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ReporteDenuncias> vListaTipoPersonaAsociacion = new List<ReporteDenuncias>();
                        while (vDataReaderResults.Read())
                        {
                            ReporteDenuncias vTipoPersonaAsociacion = new ReporteDenuncias();

                            vTipoPersonaAsociacion.IdTipoPersonaAsociacion = vDataReaderResults["IdTipoPersonaAsociacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersonaAsociacion"].ToString()) : vTipoPersonaAsociacion.IdTipoPersonaAsociacion;
                            vTipoPersonaAsociacion.NombreTipoPersonaAsociacion = vDataReaderResults["NombreTipoPersonaAsociacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersonaAsociacion"].ToString()) : vTipoPersonaAsociacion.NombreTipoPersonaAsociacion;
                            vListaTipoPersonaAsociacion.Add(vTipoPersonaAsociacion);
                        }
                        return vListaTipoPersonaAsociacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarTipoDeIdentificacion()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoIdentificacion_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ReporteDenuncias> vListaTipoIdentificacion = new List<ReporteDenuncias>();
                        while (vDataReaderResults.Read())
                        {
                            ReporteDenuncias vTipoIdentificacion = new ReporteDenuncias();

                            vTipoIdentificacion.IdTipoIdentificacion = vDataReaderResults["IdTipoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacion"].ToString()) : vTipoIdentificacion.IdTipoIdentificacion;
                            vTipoIdentificacion.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vTipoIdentificacion.TipoIdentificacion;
                            vListaTipoIdentificacion.Add(vTipoIdentificacion);
                        }
                        return vListaTipoIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        ///// <summary>
        ///// Consultar Tipo Identificacion.
        ///// </summary>
        ///// <returns>Lista resultado de la operación</returns>
        //public List<TiposDocumentosGlobal> ConsultarTiposIdentificacionGlobal(string pTiposDocumentos)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_Global_TiposDocumentos_Consultar"))
        //        {
        //            vDataBase.AddInParameter(vDbCommand, "@ids", DbType.String, pTiposDocumentos);

        //            using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
        //            {
        //                List<TiposDocumentosGlobal> vListaTiposDocumentosGlobal = new List<TiposDocumentosGlobal>();
        //                while (vDataReaderResults.Read())
        //                {
        //                    TiposDocumentosGlobal vTiposDocumentosGlobal = new TiposDocumentosGlobal();
        //                    vTiposDocumentosGlobal.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTiposDocumentosGlobal.IdTipoDocumento;
        //                    vTiposDocumentosGlobal.CodDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vTiposDocumentosGlobal.CodDocumento;
        //                    vTiposDocumentosGlobal.NomTipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vTiposDocumentosGlobal.NomTipoDocumento;
        //                    vTiposDocumentosGlobal.codSiif = vDataReaderResults["codSiif"] != DBNull.Value ? Convert.ToString(vDataReaderResults["codSiif"].ToString()) : vTiposDocumentosGlobal.codSiif;
        //                    vListaTiposDocumentosGlobal.Add(vTiposDocumentosGlobal);
        //                }

        //                return vListaTiposDocumentosGlobal;
        //            }
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        public List<ReporteDenuncias> ConsultarEstadoDenuncia()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EstadoDenuncia_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ReporteDenuncias> vListaEstado = new List<ReporteDenuncias>();
                        while (vDataReaderResults.Read())
                        {
                            ReporteDenuncias vEstado = new ReporteDenuncias();

                            vEstado.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vEstado.Estado;
                            vEstado.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vEstado.NombreEstado;
                            vListaEstado.Add(vEstado);
                        }

                        return vListaEstado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<ReporteDenuncias> ConsultarDepartamentoUbicacion()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DepartamentoUbicacion_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ReporteDenuncias> vListaDpto = new List<ReporteDenuncias>();
                        while (vDataReaderResults.Read())
                        {
                            ReporteDenuncias vDpto = new ReporteDenuncias();

                            vDpto.IdDepartamentoUbicacion = vDataReaderResults["IdDepartamentoUbicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamentoUbicacion"].ToString()) : vDpto.IdDepartamentoUbicacion;
                            vDpto.DepartamentoUbicacion = vDataReaderResults["DepartamentoUbicacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DepartamentoUbicacion"].ToString()) : vDpto.DepartamentoUbicacion;
                            vListaDpto.Add(vDpto);
                        }
                        return vListaDpto;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
