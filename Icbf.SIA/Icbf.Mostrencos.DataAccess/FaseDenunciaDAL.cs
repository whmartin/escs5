﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    public class FaseDenunciaDAL : GeneralDAL
    {
        public FaseDenunciaDAL()
        {
        }

        public List<FasesDenuncia> ConsultarFasesDenuncia()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Consultar_FasesDenuncia"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FasesDenuncia> vListaFasesDenuncia = new List<FasesDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            FasesDenuncia vFasesDenuncia = new FasesDenuncia();
                            vFasesDenuncia.IdFase = vDataReaderResults["IdFase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFase"].ToString()) : vFasesDenuncia.IdFase;
                            vFasesDenuncia.NombreFase = vDataReaderResults["NombreFase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFase"].ToString()) : vFasesDenuncia.NombreFase;
                            vListaFasesDenuncia.Add(vFasesDenuncia);
                        }

                        return vListaFasesDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta las fases en estado Activo
        /// </summary>
        /// <returns>Lista de Fases</returns>
        public List<FasesDenuncia> ConsultarFasesActivas()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ConsultarFasesActivas"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FasesDenuncia> vListaFasesDenuncia = new List<FasesDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            FasesDenuncia vFasesDenuncia = new FasesDenuncia();
                            vFasesDenuncia.IdFase = vDataReaderResults["IdFase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFase"].ToString()) : vFasesDenuncia.IdFase;
                            vFasesDenuncia.NombreFase = vDataReaderResults["NombreFase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFase"].ToString()) : vFasesDenuncia.NombreFase;
                            vListaFasesDenuncia.Add(vFasesDenuncia);
                        }

                        return vListaFasesDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
