﻿

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class ClaseBienDAL : GeneralDAL
    {

        private Database vDataBase;

        public ClaseBienDAL()
        {
            this.vDataBase = DatabaseFactory.CreateDatabase("DataBaseSEVEN");
        }

        /// <summary>
        /// consulta los ClaseBien
        /// </summary>
        /// <returns>LISTA DE ClaseBien</returns>
        public List<ClaseBien> ConsultarClaseBien(string pIdSubTipoBien)
        {
            try
            {
                using (DbConnection vDbConnection = vDataBase.CreateConnection())
                {
                    vDbConnection.Open();
                    DbCommand vDbCommand = vDbConnection.CreateCommand();
                    string vIdSubTipoBien = pIdSubTipoBien.Substring(0, 3);
                    if (vIdSubTipoBien.Equals("308"))
                    {
                        vDbCommand.CommandText = "SELECT [ITE_CODI] ,[ITE_NOMB] FROM [produccion].[dbo].[GN_VITEM] WHERE [ITE_ACTI] = N's' and [TIT_CONT] = 151";
                    }
                    else
                    {
                        vDbCommand.CommandText = "SELECT [pro_codi] AS ITE_CODI, [pro_nomb] AS ITE_NOMB FROM [produccion].[dbo].[in_vprod] WHERE [pro_co02] = " + vIdSubTipoBien;
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ClaseBien> vListClaseBien = new List<ClaseBien>();
                        while (vDataReaderResults.Read())
                        {
                            ClaseBien vClaseBien = new ClaseBien();
                            vClaseBien.IdClaseBien = vDataReaderResults["ITE_CODI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_CODI"].ToString()) : vClaseBien.IdClaseBien;
                            vClaseBien.NombreClaseBien = vDataReaderResults["ITE_NOMB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_NOMB"].ToString().ToUpper()) : vClaseBien.NombreClaseBien;
                            vClaseBien.IdSubTipoBien = pIdSubTipoBien;
                            vListClaseBien.Add(vClaseBien);
                            vClaseBien = null;
                        }

                        return vListClaseBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
