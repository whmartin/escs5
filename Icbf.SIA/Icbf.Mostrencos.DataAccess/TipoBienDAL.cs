﻿//-----------------------------------------------------------------------
// <copyright file="TipoBienDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoBienDAL.</summary>
// <author>INGENIAN</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System.Collections.Generic;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class TipoBienDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta todos los TipoBien de la DB SEVEN
        /// </summary>
        /// <returns>List tipo TipoBien con la información consultada</returns>
        public List<TipoBien> ConsultarSeven()
        {
            try
            {
                Database vDataBase = ObtenerInstanciaSeven();
                using (DbCommand vDbCommand = vDataBase.GetSqlStringCommand("SELECT DISTINCT [pro_NO02], tipo_bien_ FROM [produccion].[dbo].[in_vprod]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoBien> vTipoBienList = new List<TipoBien>();
                        while (vDataReaderResults.Read())
                        {
                            TipoBien vTipoBien = new TipoBien();

                            vTipoBien.idTipoBien = vDataReaderResults["tipo_bien_"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["tipo_bien_"].ToString()) : vTipoBien.idTipoBien;
                            vTipoBien.tipoProducto = vDataReaderResults["pro_NO02"] != DBNull.Value ? Convert.ToString(vDataReaderResults["pro_NO02"].ToString()) : vTipoBien.tipoProducto;
                            vTipoBienList.Add(vTipoBien);
                        }

                        return vTipoBienList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta todos los SubTipoBien de la DB SEVEN
        /// </summary>
        /// <returns>List tipo String con la información consultada</returns>
        public List<string> ConsultarSubTipoBien(int tipoBien)
        {
            try
            {
                Database vDataBase = ObtenerInstanciaSeven();
                using (DbCommand vDbCommand = vDataBase.GetSqlStringCommand(string.Concat("select distinct  d.pro_NO02 from [dbo].[in_vprod] d where d.tipo_bien_ =", tipoBien)))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<string> vSubTipoBienList = new List<string>();
                        while (vDataReaderResults.Read())
                        {
                            vSubTipoBienList.Add(vDataReaderResults["pro_NO02"] != DBNull.Value ? Convert.ToString(vDataReaderResults["pro_NO02"].ToString()) : string.Empty);
                        }

                        return vSubTipoBienList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta todos los Marca Bien de la DB SEVEN
        /// </summary>
        /// <returns>List tipo String con la información consultada</returns>
        public List<String> ConsultarMarcaBien()
        {
            try
            {
                Database vDataBase = ObtenerInstanciaSeven();
                using (DbCommand vDbCommand = vDataBase.GetSqlStringCommand("SELECT [ITE_NOMB] FROM [produccion].[dbo].[GN_VITEM] where TIT_CONT=177"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<string> vMarcaBienList = new List<string>();
                        while (vDataReaderResults.Read())
                        {
                            vMarcaBienList.Add(vDataReaderResults["ITE_NOMB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_NOMB"].ToString()):string.Empty);
                        }

                        return vMarcaBienList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta todos las Clase Bien de la DB SEVEN
        /// </summary>
        /// <returns>List tipo String con la información consultada</returns>
        public List<String> ConsultarClaseBien()
        {
            try
            {
                Database vDataBase = ObtenerInstanciaSeven();
                using (DbCommand vDbCommand = vDataBase.GetSqlStringCommand("SELECT pro_nomb FROM [produccion].[dbo].[in_vprod] where pro_nomb != 'Terreno'"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<string> vclaseBienList = new List<string>();
                        while (vDataReaderResults.Read())
                        {
                            vclaseBienList.Add(vDataReaderResults["pro_nomb"] != DBNull.Value ? Convert.ToString(vDataReaderResults["pro_nomb"].ToString()) : string.Empty);
                        }

                        return vclaseBienList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

