﻿using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Icbf.Mostrencos.DataAccess
{
    public class TipoDocumentoSoporteDenunciaDAL : GeneralDAL
    {
        public List<TipoDocumentoSoporteDenuncia> ListarTipoDocumentoSporteDenuncia()
        {
            try
            {
                List<TipoDocumentoSoporteDenuncia> vlstTipoDocumentoSoporteDenuncia = new List<TipoDocumentoSoporteDenuncia>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_TipoDocumentoSoporteDenuncia_Lista"))
                {
                    //vDataBase.AddInParameter(vDbCommand, "@IdInformeDenunciante", DbType.Int32, IdInformeDenunciante);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            TipoDocumentoSoporteDenuncia vTipoDocumentoSoporteDenuncia = new TipoDocumentoSoporteDenuncia();
                            vTipoDocumentoSoporteDenuncia.IdTipoDocumentoSoporteDenuncia = vDataReaderResults["IdTipoDocumentoSoporteDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoSoporteDenuncia"].ToString()) : vTipoDocumentoSoporteDenuncia.IdTipoDocumentoSoporteDenuncia;
                            vTipoDocumentoSoporteDenuncia.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? vDataReaderResults["NombreDocumento"].ToString() : vTipoDocumentoSoporteDenuncia.NombreDocumento;

                            vlstTipoDocumentoSoporteDenuncia.Add(vTipoDocumentoSoporteDenuncia);
                        }
                        return vlstTipoDocumentoSoporteDenuncia;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TipoDocumentoSoporteDenuncia ConsultarTipoDocumentoSoporteDenunciadoPorId(int pIdTipoDocumentoSoporteDenunciado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoDocumentoSoporteDenuncia_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoSoporteDenuncia", DbType.Int32, pIdTipoDocumentoSoporteDenunciado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoDocumentoSoporteDenuncia vTipoDocumentoSoporteDenunciado = new TipoDocumentoSoporteDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vTipoDocumentoSoporteDenunciado.Estado = vDataReaderResults["Estado"] != DBNull.Value ? vDataReaderResults["Estado"].ToString() : vTipoDocumentoSoporteDenunciado.Estado;
                            vTipoDocumentoSoporteDenunciado.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"]) : vTipoDocumentoSoporteDenunciado.NombreDocumento;
                            vTipoDocumentoSoporteDenunciado.IdTipoDocumentoSoporteDenuncia = vDataReaderResults["IdTipoDocumentoSoporteDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoSoporteDenuncia"].ToString()) : vTipoDocumentoSoporteDenunciado.IdTipoDocumentoSoporteDenuncia;
                            vTipoDocumentoSoporteDenunciado.DescripcionDocumento = vDataReaderResults["DescripcionDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDocumento"].ToString()) : vTipoDocumentoSoporteDenunciado.DescripcionDocumento;
                            vTipoDocumentoSoporteDenunciado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumentoSoporteDenunciado.UsuarioCrea;
                            vTipoDocumentoSoporteDenunciado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumentoSoporteDenunciado.FechaCrea;
                            vTipoDocumentoSoporteDenunciado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumentoSoporteDenunciado.UsuarioModifica;
                            vTipoDocumentoSoporteDenunciado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumentoSoporteDenunciado.FechaModifica;
                        }

                        return vTipoDocumentoSoporteDenunciado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
