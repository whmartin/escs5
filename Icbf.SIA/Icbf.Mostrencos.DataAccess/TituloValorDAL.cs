﻿//-----------------------------------------------------------------------
// <copyright file="TituloValorDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TituloValorDAL.</summary>
// <author>INGENIAN</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class TituloValorDAL : GeneralDAL
    {
        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public List<TituloValor> ConsultarTituloValorPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TituloValor_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TituloValor> vListTituloValor = new List<TituloValor>();
                        while (vDataReaderResults.Read())
                        {
                            TituloValor vTituloValor = new TituloValor();
                            vTituloValor.IdTituloValor = vDataReaderResults["IdTituloValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTituloValor"].ToString()) : vTituloValor.IdTituloValor;
                            vTituloValor.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vTituloValor.IdDenunciaBien;
                            vTituloValor.IdTipoTitulo = vDataReaderResults["IdTipoTitulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTitulo"].ToString()) : vTituloValor.IdTipoTitulo;
                            vTituloValor.EstadoBien = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"].ToString()) : vTituloValor.EstadoBien;
                            vTituloValor.NumeroCuentaBancaria = vDataReaderResults["NumeroCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCuentaBancaria"].ToString()) : vTituloValor.NumeroCuentaBancaria;
                            vTituloValor.EntidadBancaria = vDataReaderResults["EntidadBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadBancaria"].ToString()) : vTituloValor.EntidadBancaria;
                            vTituloValor.TipoCuentaBancaria = vDataReaderResults["TipoCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCuentaBancaria"]) : vTituloValor.TipoCuentaBancaria;
                            vTituloValor.ValorEfectivoEstimado = vDataReaderResults["ValorEfectivoEstimado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEfectivoEstimado"].ToString()) : vTituloValor.ValorEfectivoEstimado;
                            vTituloValor.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDepartamento"].ToString()) : vTituloValor.IdDepartamento;
                            vTituloValor.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"].ToString()) : vTituloValor.IdMunicipio;
                            vTituloValor.NumeroTituloValor = vDataReaderResults["NumeroTituloValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTituloValor"].ToString()) : vTituloValor.NumeroTituloValor;
                            vTituloValor.NumeroIdentificacionBien = vDataReaderResults["NumeroIdentificacionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionBien"].ToString()) : vTituloValor.NumeroIdentificacionBien;
                            vTituloValor.IdEntidadEmisora = vDataReaderResults["IdEntidadEmisora"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidadEmisora"].ToString()) : vTituloValor.IdEntidadEmisora;
                            vTituloValor.IdTipoIdentificacionEntidadEmisora = vDataReaderResults["IdTipoIdentificacionEntidadEmisora"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacionEntidadEmisora"].ToString()) : vTituloValor.IdTipoIdentificacionEntidadEmisora;
                            vTituloValor.CantidadTitulos = vDataReaderResults["CantidadTitulos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CantidadTitulos"].ToString()) : vTituloValor.CantidadTitulos;
                            vTituloValor.CantidadVendida = vDataReaderResults["CantidadVendida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CantidadVendida"].ToString()) : vTituloValor.CantidadVendida;
                            vTituloValor.ValorUnidadTitulo = vDataReaderResults["ValorUnidadTitulo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorUnidadTitulo"].ToString()) : vTituloValor.ValorUnidadTitulo;
                            vTituloValor.FechaVencimiento = vDataReaderResults["FechaVencimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimiento"].ToString()) : vTituloValor.FechaVencimiento;
                            vTituloValor.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vTituloValor.FechaRecibido;
                            vTituloValor.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vTituloValor.FechaVenta;
                            vTituloValor.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : vTituloValor.ClaseEntrada;
                            vTituloValor.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vTituloValor.FechaAdjudicado;
                            vTituloValor.DescripcionTitulo = vDataReaderResults["DescripcionTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTitulo"].ToString()) : vTituloValor.DescripcionTitulo;
                            vTituloValor.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vTituloValor.CodigoProducto;
                            vTituloValor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTituloValor.UsuarioCrea;
                            vTituloValor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTituloValor.FechaCrea;
                            vTituloValor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTituloValor.UsuarioModifica;
                            vTituloValor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTituloValor.FechaModifica;
                            vListTituloValor.Add(vTituloValor);
                            vTituloValor = null;
                        }

                        return vListTituloValor;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public TituloValor ConsultarTituloValorPorIdTituloValor(int pIdTituloValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TituloValor_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTituloValor", DbType.Int32, pIdTituloValor);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TituloValor vTituloValor = new TituloValor();
                        while (vDataReaderResults.Read())
                        {
                            vTituloValor.IdTituloValor = vDataReaderResults["IdTituloValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTituloValor"].ToString()) : vTituloValor.IdTituloValor;
                            vTituloValor.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vTituloValor.IdDenunciaBien;
                            vTituloValor.IdTipoTitulo = vDataReaderResults["IdTipoTitulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTitulo"].ToString()) : vTituloValor.IdTipoTitulo;
                            vTituloValor.EstadoBien = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"].ToString()) : vTituloValor.EstadoBien;
                            vTituloValor.NumeroCuentaBancaria = vDataReaderResults["NumeroCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCuentaBancaria"].ToString()) : vTituloValor.NumeroCuentaBancaria;
                            vTituloValor.EntidadBancaria = vDataReaderResults["EntidadBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadBancaria"].ToString()) : vTituloValor.EntidadBancaria;
                            vTituloValor.TipoCuentaBancaria = vDataReaderResults["TipoCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCuentaBancaria"]) : vTituloValor.TipoCuentaBancaria;
                            vTituloValor.ValorEfectivoEstimado = vDataReaderResults["ValorEfectivoEstimado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEfectivoEstimado"].ToString()) : vTituloValor.ValorEfectivoEstimado;
                            vTituloValor.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDepartamento"].ToString()) : vTituloValor.IdDepartamento;
                            vTituloValor.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"].ToString()) : vTituloValor.IdMunicipio;
                            vTituloValor.NumeroTituloValor = vDataReaderResults["NumeroTituloValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTituloValor"].ToString()) : vTituloValor.NumeroTituloValor;
                            vTituloValor.NumeroIdentificacionBien = vDataReaderResults["NumeroIdentificacionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionBien"].ToString()) : vTituloValor.NumeroIdentificacionBien;
                            vTituloValor.IdEntidadEmisora = vDataReaderResults["IdEntidadEmisora"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidadEmisora"].ToString()) : vTituloValor.IdEntidadEmisora;
                            vTituloValor.IdTipoIdentificacionEntidadEmisora = vDataReaderResults["IdTipoIdentificacionEntidadEmisora"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacionEntidadEmisora"].ToString()) : vTituloValor.IdTipoIdentificacionEntidadEmisora;
                            vTituloValor.CantidadTitulos = vDataReaderResults["CantidadTitulos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CantidadTitulos"].ToString()) : vTituloValor.CantidadTitulos;
                            vTituloValor.CantidadVendida = vDataReaderResults["CantidadVendida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CantidadVendida"].ToString()) : vTituloValor.CantidadVendida;
                            vTituloValor.ValorUnidadTitulo = vDataReaderResults["ValorUnidadTitulo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorUnidadTitulo"].ToString()) : vTituloValor.ValorUnidadTitulo;
                            vTituloValor.FechaVencimiento = vDataReaderResults["FechaVencimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimiento"].ToString()) : vTituloValor.FechaVencimiento;
                            vTituloValor.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vTituloValor.FechaRecibido;
                            vTituloValor.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vTituloValor.FechaVenta;
                            vTituloValor.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : vTituloValor.ClaseEntrada;
                            vTituloValor.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vTituloValor.FechaAdjudicado;
                            vTituloValor.DescripcionTitulo = vDataReaderResults["DescripcionTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTitulo"].ToString()) : vTituloValor.DescripcionTitulo;
                            vTituloValor.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vTituloValor.CodigoProducto;
                            vTituloValor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTituloValor.UsuarioCrea;
                            vTituloValor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTituloValor.FechaCrea;
                            vTituloValor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTituloValor.UsuarioModifica;
                            vTituloValor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTituloValor.FechaModifica;
                            vTituloValor.UsoBien = vDataReaderResults["UsoBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["UsoBien"].ToString()) : vTituloValor.UsoBien;
                            vTituloValor.IdInformacionVenta = vDataReaderResults["IdInformacionVenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionVenta"].ToString()) : vTituloValor.IdInformacionVenta;
                        }

                        return vTituloValor;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Inserta la información de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Variable de tipo TituloValor que contiene la información que se va a insertar</param>
        /// <returns>Id del TituloValor insertado</returns>
        public int InsertarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TituloValor_Insert"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTituloValor", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pTituloValor.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoTitulo", DbType.Int32, pTituloValor.IdTipoTitulo);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoBien", DbType.String, pTituloValor.EstadoBien);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCuentaBancaria", DbType.String, pTituloValor.NumeroCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadBancaria", DbType.String, pTituloValor.EntidadBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@TipoCuentaBancaria", DbType.String, pTituloValor.TipoCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.String, pTituloValor.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.String, pTituloValor.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@ValorEfectivoEstimado", DbType.Decimal, pTituloValor.ValorEfectivoEstimado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTituloValor", DbType.String, pTituloValor.NumeroTituloValor);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidadEmisora", DbType.Int32, pTituloValor.IdEntidadEmisora);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacionEntidadEmisora", DbType.Int32, pTituloValor.IdTipoIdentificacionEntidadEmisora);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionBien", DbType.String, pTituloValor.NumeroIdentificacionBien);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadTitulos", DbType.String, pTituloValor.CantidadTitulos);
                    vDataBase.AddInParameter(vDbCommand, "@ValorUnidadTitulo", DbType.Decimal, pTituloValor.ValorUnidadTitulo);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadVendida", DbType.String, pTituloValor.CantidadVendida);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVencimiento", DbType.DateTime, pTituloValor.FechaVencimiento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pTituloValor.FechaRecibido);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, pTituloValor.FechaVenta);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseEntrada", DbType.String, pTituloValor.ClaseEntrada);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicado", DbType.DateTime, pTituloValor.FechaAdjudicado);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionTitulo", DbType.String, pTituloValor.DescripcionTitulo);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoProducto", DbType.String, pTituloValor.CodigoProducto);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTituloValor.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pTituloValor.IdUsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTituloValor"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTituloValor;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pTituloValor, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// EdIta la información de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Variable de tipo TituloValor que contiene la información que se va a insertar</param>
        /// <returns>Numero de filas afectadas</returns>
        public int EditarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TituloValor_Update"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTituloValor", DbType.Int32, pTituloValor.IdTituloValor);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoBien", DbType.String, pTituloValor.EstadoBien);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroCuentaBancaria", DbType.String, pTituloValor.NumeroCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadBancaria", DbType.String, pTituloValor.EntidadBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@TipoCuentaBancaria", DbType.String, pTituloValor.TipoCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.String, pTituloValor.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.String, pTituloValor.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@ValorEfectivoEstimado", DbType.Decimal, pTituloValor.ValorEfectivoEstimado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTituloValor", DbType.String, pTituloValor.NumeroTituloValor);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidadEmisora", DbType.Int32, pTituloValor.IdEntidadEmisora);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacionEntidadEmisora", DbType.Int32, pTituloValor.IdTipoIdentificacionEntidadEmisora);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionBien", DbType.String, pTituloValor.NumeroIdentificacionBien);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadTitulos", DbType.String, pTituloValor.CantidadTitulos);
                    vDataBase.AddInParameter(vDbCommand, "@ValorUnidadTitulo", DbType.Decimal, pTituloValor.ValorUnidadTitulo);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadVendida", DbType.String, pTituloValor.CantidadVendida);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVencimiento", DbType.DateTime, pTituloValor.FechaVencimiento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pTituloValor.FechaRecibido);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, pTituloValor.FechaVenta);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseEntrada", DbType.String, pTituloValor.ClaseEntrada);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicado", DbType.DateTime, pTituloValor.FechaAdjudicado);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionTitulo", DbType.String, pTituloValor.DescripcionTitulo);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoProducto", DbType.String, pTituloValor.CodigoProducto);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTituloValor.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTituloValor;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pTituloValor, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public List<TituloValor> ConsultarTituloValor(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_TituloValor_Consulta"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TituloValor> vListTituloValor = new List<TituloValor>();

                        while (vDataReaderResults.Read())
                        {
                            TituloValor vTituloValor = new TituloValor();
                            vTituloValor.IdTituloValor = vDataReaderResults["IdTituloValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTituloValor"].ToString()) : vTituloValor.IdTituloValor;
                            vTituloValor.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vTituloValor.IdDenunciaBien;
                            vTituloValor.IdTipoTitulo = vDataReaderResults["IdTipoTitulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTitulo"].ToString()) : vTituloValor.IdTipoTitulo;
                            vTituloValor.EstadoBien = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"].ToString()) : vTituloValor.EstadoBien;
                            //vTituloValor.NumeroCuentaBancaria = vDataReaderResults["NumeroCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCuentaBancaria"].ToString()) : vTituloValor.NumeroCuentaBancaria;
                            //vTituloValor.EntidadBancaria = vDataReaderResults["EntidadBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadBancaria"].ToString()) : vTituloValor.EntidadBancaria;
                            //vTituloValor.TipoCuentaBancaria = vDataReaderResults["TipoCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCuentaBancaria"]) : vTituloValor.TipoCuentaBancaria;
                            //vTituloValor.ValorEfectivoEstimado = vDataReaderResults["ValorEfectivoEstimado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEfectivoEstimado"].ToString()) : vTituloValor.ValorEfectivoEstimado;
                            //vTituloValor.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDepartamento"].ToString()) : vTituloValor.IdDepartamento;
                            //vTituloValor.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"].ToString()) : vTituloValor.IdMunicipio;
                            //vTituloValor.NumeroTituloValor = vDataReaderResults["NumeroTituloValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTituloValor"].ToString()) : vTituloValor.NumeroTituloValor;
                            //vTituloValor.NumeroIdentificacionBien = vDataReaderResults["NumeroIdentificacionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionBien"].ToString()) : vTituloValor.NumeroIdentificacionBien;
                            //vTituloValor.IdEntidadEmisora = vDataReaderResults["IdEntidadEmisora"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidadEmisora"].ToString()) : vTituloValor.IdEntidadEmisora;
                            //vTituloValor.IdTipoIdentificacionEntidadEmisora = vDataReaderResults["IdTipoIdentificacionEntidadEmisora"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacionEntidadEmisora"].ToString()) : vTituloValor.IdTipoIdentificacionEntidadEmisora;
                            //vTituloValor.CantidadTitulos = vDataReaderResults["CantidadTitulos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CantidadTitulos"].ToString()) : vTituloValor.CantidadTitulos;
                            //vTituloValor.CantidadVendida = vDataReaderResults["CantidadVendida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CantidadVendida"].ToString()) : vTituloValor.CantidadVendida;
                            //vTituloValor.ValorUnidadTitulo = vDataReaderResults["ValorUnidadTitulo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorUnidadTitulo"].ToString()) : vTituloValor.ValorUnidadTitulo;
                            //vTituloValor.FechaVencimiento = vDataReaderResults["FechaVencimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimiento"].ToString()) : vTituloValor.FechaVencimiento;
                            //vTituloValor.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vTituloValor.FechaRecibido;
                            //vTituloValor.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vTituloValor.FechaVenta;
                            //vTituloValor.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : vTituloValor.ClaseEntrada;
                            //vTituloValor.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vTituloValor.FechaAdjudicado;
                            vTituloValor.DescripcionTitulo = vDataReaderResults["DescripcionTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTitulo"].ToString()) : vTituloValor.DescripcionTitulo;
                            vTituloValor.UsoBien = vDataReaderResults["UsoBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["UsoBien"].ToString()) : vTituloValor.UsoBien;
                            vTituloValor.IdInformacionVenta = vDataReaderResults["IdInformacionVenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionVenta"].ToString()) : vTituloValor.IdInformacionVenta;
                            //vTituloValor.CodigoProducto = vDataReaderResults["CodigoProducto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoProducto"].ToString()) : vTituloValor.CodigoProducto;
                            //vTituloValor.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTituloValor.UsuarioCrea;
                            //vTituloValor.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTituloValor.FechaCrea;
                            //vTituloValor.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTituloValor.UsuarioModifica;
                            //vTituloValor.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTituloValor.FechaModifica;
                            if (vTituloValor.UsoBien == 1)
                            {
                                vTituloValor.NombreUsoBien = "VENTA";
                            }
                            else if (vTituloValor.UsoBien == 2)
                            {
                                vTituloValor.NombreUsoBien = "USO PROPIO";
                            }
                            else
                            {
                                vTituloValor.NombreUsoBien = string.Empty;
                            }

                            vListTituloValor.Add(vTituloValor);
                            vTituloValor = null;
                        }

                        return vListTituloValor;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region CU 154

        /// <summary>
        /// Edita los campos UsoBien y IdInformacionVenta de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Entidad a modificar</param>
        /// <returns>Resultado de la Operación</returns>
        public int ModificarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_TituloValor_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTituloValor", DbType.Int32, pTituloValor.IdTituloValor);
                    vDataBase.AddInParameter(vDbCommand, "@UsoBien", DbType.Int32, pTituloValor.UsoBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionVenta", DbType.Int32, pTituloValor.IdInformacionVenta);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoBien", DbType.String, pTituloValor.EstadoBien);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTituloValor.UsuarioModifica);

                    if (pTituloValor.FechaVenta != Convert.ToDateTime("1/01/0001 00:00:00") && pTituloValor.FechaVenta != Convert.ToDateTime("1/01/1900 00:00:00"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, pTituloValor.FechaVenta);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, null);
                    }

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTituloValor;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pTituloValor, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}
