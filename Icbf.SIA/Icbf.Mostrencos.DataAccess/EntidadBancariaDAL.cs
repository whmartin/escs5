﻿

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class EntidadBancariaDAL : GeneralDAL
    {
        /// <summary>
        /// consulta los EntidadBancaria
        /// </summary>
        /// <returns>LISTA DE EntidadBancaria</returns>
        public List<EntidadBancaria> ConsultarEntidadBancaria()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EntidadFinanciera_ConsultarEntidadFinanciera"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EntidadBancaria> vListEntidadBancaria = new List<EntidadBancaria>();
                        while (vDataReaderResults.Read())
                        {
                            EntidadBancaria vEntidadBancaria = new EntidadBancaria();
                            vEntidadBancaria.IdEntidadBancaria = vDataReaderResults["IdEntidadFinanciera"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdEntidadFinanciera"].ToString()) : vEntidadBancaria.IdEntidadBancaria;
                            vEntidadBancaria.NombreEntidadBancaria = vDataReaderResults["NombreEntFin"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntFin"].ToString().ToUpper()) : vEntidadBancaria.NombreEntidadBancaria;
                            vListEntidadBancaria.Add(vEntidadBancaria);
                            vEntidadBancaria = null;
                        }

                        return vListEntidadBancaria;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta los tipos de cuentas
        /// </summary>
        /// <returns>LISTA DE TipoCuenta</returns>
        public List<TipoCuenta> ConsultarTipoCuenta()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EntidadFinanciera_ConsultarTipoCuenta"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCuenta> vListTipoCuenta = new List<TipoCuenta>();
                        while (vDataReaderResults.Read())
                        {
                            TipoCuenta vTipoCuenta = new TipoCuenta();
                            vTipoCuenta.IdTipoCta = vDataReaderResults["IdTipoCta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoCta"].ToString()) : vTipoCuenta.IdTipoCta;
                            vTipoCuenta.NombreTipoCta = vDataReaderResults["NombreTipoCta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoCta"].ToString().ToUpper()) : vTipoCuenta.NombreTipoCta;
                            vListTipoCuenta.Add(vTipoCuenta);
                            vTipoCuenta = null;
                        }

                        return vListTipoCuenta;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
