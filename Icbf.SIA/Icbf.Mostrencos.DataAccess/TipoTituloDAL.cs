﻿//-----------------------------------------------------------------------
// <copyright file="TipoTituloDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoTituloDAL.</summary>
// <author>INGENIAN</author>
// <date>02/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class TipoTituloDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta todos los TipoTitulo
        /// </summary>
        /// <returns>List tipo TipoTitulo con la información consultada</returns>
        public List<TipoTitulo> ConsultarTipoTituloTodos()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetSqlStringCommand("SELECT * FROM [BVMVH].[TipoTitulo]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoTitulo> vTipoTituloList = new List<TipoTitulo>();
                        while (vDataReaderResults.Read())
                        {
                            TipoTitulo vTipoTitulo = new TipoTitulo();

                            vTipoTitulo.IdTipoTitulo = vDataReaderResults["IdTipoTitulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTitulo"].ToString()) : vTipoTitulo.IdTipoTitulo;

                            vTipoTitulo.NombreTipoTitulo = vDataReaderResults["NombreTipoTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoTitulo"]) : string.Empty;

                            vTipoTituloList.Add(vTipoTitulo);
                        }

                        return vTipoTituloList.OrderBy(a => a.Nombre).ToList();
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TipoTituloDAL"/> class.
        /// </summary>
        public TipoTituloDAL()
        {
        }

        /// <summary>
        /// Consultar Titulos de Valor.
        /// </summary>
        /// <param name="pNombre">The p Nombre Titulo.</param>
        /// <param name="pEstado">The p Estado.</param>
        /// <returns>Lista de títulos de valor filtrada por nombre o estado</returns>
        /// <exception cref="GenericException"></exception>
        public List<TipoTitulo> ConsultarTipoTitulo(string pNombre, bool? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoTitulo_ConsultarTipoTitulo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoTitulo> vListaTipoTitulo = new List<TipoTitulo>();
                        while (vDataReaderResults.Read())
                        {
                            TipoTitulo vTipoTitulo = new TipoTitulo();
                            vTipoTitulo.IdTipoTitulo = vDataReaderResults["IdTipoTitulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTitulo"].ToString()) : vTipoTitulo.IdTipoTitulo;
                            vTipoTitulo.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTipoTitulo.Nombre;
                            vTipoTitulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoTitulo.Estado;
                            vListaTipoTitulo.Add(vTipoTitulo);
                        }
                        return vListaTipoTitulo.OrderBy(a => a.Nombre).ToList();
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los TipoTitulo por el IdTipoTitulo
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>TipoTitulo</returns>
        public TipoTitulo ConsultarTipoTituloPorIdTipoTitulo(int pIdTipoTitulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoTitulo_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoTitulo", DbType.Int32, pIdTipoTitulo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoTitulo vTipoTitulo = new TipoTitulo();

                        while (vDataReaderResults.Read())
                        {
                            vTipoTitulo.IdTipoTitulo = vDataReaderResults["IdTipoTitulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTitulo"].ToString()) : vTipoTitulo.IdTipoTitulo;
                            vTipoTitulo.NombreTipoTitulo = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTipoTitulo.NombreTipoTitulo;
                            vTipoTitulo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoTitulo.UsuarioCrea;
                            vTipoTitulo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoTitulo.FechaCrea;
                            vTipoTitulo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoTitulo.UsuarioModifica;
                            vTipoTitulo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoTitulo.FechaModifica;
                        }

                        return vTipoTitulo;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta los TipoTitulo 
        /// </summary>
        /// <returns>lISTA DE TipoTitulo</returns>
        public List<TipoTitulo> ConsultarTipoTitulo()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoTitulo_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoTitulo> vListTipoTitulo = new List<TipoTitulo>();
                        while (vDataReaderResults.Read())
                        {
                            TipoTitulo vTipoTitulo = new TipoTitulo();
                            vTipoTitulo.IdTipoTitulo = vDataReaderResults["IdTipoTitulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTitulo"].ToString()) : vTipoTitulo.IdTipoTitulo;
                            vTipoTitulo.NombreTipoTitulo = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTipoTitulo.NombreTipoTitulo;
                            vTipoTitulo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoTitulo.UsuarioCrea;
                            vTipoTitulo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoTitulo.FechaCrea;
                            vTipoTitulo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoTitulo.UsuarioModifica;
                            vTipoTitulo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoTitulo.FechaModifica;
                            vListTipoTitulo.Add(vTipoTitulo);
                            vTipoTitulo = null;
                        }

                        return vListTipoTitulo.OrderBy(a => a.Nombre).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Ingresar Titulos de valor.
        /// </summary>
        /// <param name="pTipoTitulo">The p Titulo con los datos a ingresar.</param>
        /// <returns>0 si el nombre se encuentra duplicado y 1 si se agregó correctamente</returns>
        /// <exception cref="GenericException"></exception>
        public int IngresarTipoTitulo(TipoTitulo pTipoTitulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoTitulo_CrearTipoTitulo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTipoTitulo.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pTipoTitulo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoTitulo.UsuarioCrea);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int existeTipoTitulo = 0;
                        while (vDataReaderResults.Read())
                        {
                            existeTipoTitulo = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Count"].ToString()) : existeTipoTitulo;
                            if (existeTipoTitulo > 0)
                            {
                                Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTipoTitulo;
                                vAuditoria.ProgramaGeneraLog = true;
                                vAuditoria.Operacion = "INSERT";
                                this.GenerarLogAuditoria(pTipoTitulo, vDbCommand);
                            }
                        }
                        return existeTipoTitulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Titulos de valor por id.
        /// </summary>
        /// <param name="pIdTipoTitulo">The p Id Titulo.</param>
        /// <returns>Tipo titulo consultado en detalle</returns>
        /// <exception cref="GenericException"></exception>
        public TipoTitulo ConsultarTipoTituloPorId(int pIdTipoTitulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoTitulo_ConsultarTipoTituloPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoTitulo", DbType.Int32, pIdTipoTitulo);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoTitulo vTipoTitulo = new TipoTitulo();
                        while (vDataReaderResults.Read())
                        {
                            vTipoTitulo.IdTipoTitulo = vDataReaderResults["IdTipoTitulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTitulo"].ToString()) : vTipoTitulo.IdTipoTitulo;
                            vTipoTitulo.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTipoTitulo.Nombre;
                            vTipoTitulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoTitulo.Estado;
                        }
                        return vTipoTitulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza Titulos de valor editado.
        /// </summary>
        /// <param name="pTipoTitulo">The p titulo de valor con los datos modificados.</param>
        /// <returns>Retorna 1 si el nombre se encuentra duplicado y 0 si se actualizó correctamente</returns>
        /// <exception cref="GenericException"></exception>
        public int ActualizarTipoTitulo(TipoTitulo pTipoTitulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoTitulo_ActualizarTipoTitulo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoTitulo", DbType.Int32, pTipoTitulo.IdTipoTitulo);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTipoTitulo.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pTipoTitulo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoTitulo.UsuarioModifica);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int existeTipoTitulo = 0;
                        while (vDataReaderResults.Read())
                        {
                            existeTipoTitulo = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Count"].ToString()) : existeTipoTitulo;
                            if (existeTipoTitulo == 0)
                            {
                                Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTipoTitulo;
                                vAuditoria.ProgramaGeneraLog = true;
                                vAuditoria.Operacion = "UPDATE";
                                this.GenerarLogAuditoria(pTipoTitulo, vDbCommand);
                            }
                        }
                        return existeTipoTitulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}