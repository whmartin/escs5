﻿
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase que permite el enlace entre el DAL y la BD 
    /// </summary>
    /// <seealso cref="Icbf.Mostrencos.DataAccess.GeneralDAL" />
    public class NotariasDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotariasDAL"/> class.
        /// </summary>
        public NotariasDAL()
        {
        }

        /// <summary>
        /// Consultar Despachos.
        /// </summary>
        /// <param name="pNombre">The p Nombre Despacho.</param>
        /// <param name="pDepartamento">The p Departamento.</param>
        /// <param name="pMunicipio">The p Municipio.</param>
        /// <param name="pEstado">The p Estado.</param>
        /// <returns>Lista de medios de comunicación filtrada por nombre o estado</returns>
        /// <exception cref="GenericException"></exception>
        public List<Notarias> ConsultarNotarias()
        {
            try
            {
                Database vDataBase = ObtenerInstanciaSAC();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_par_Parametricas_ConsultaNotarias"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Notarias> vNotariasList = new List<Notarias>();
                        while (vDataReaderResults.Read())
                        {
                            Notarias vNotarias = new Notarias();

                            vNotarias.CodNotaria = vDataReaderResults["CodigoNotaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNotaria"].ToString()) : string.Empty;
                            vNotarias.NombreNotaria = vDataReaderResults["Notaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Notaria"].ToString()) : string.Empty;
                            vNotarias.DepartamentoNotaria = vDataReaderResults["Departamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Departamento"].ToString()) : string.Empty;
                            vNotarias.CiudadNotaria = vDataReaderResults["Ciudad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ciudad"].ToString()) : string.Empty;
                            vNotariasList.Add(vNotarias);
                        }
                        return vNotariasList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
