﻿//-----------------------------------------------------------------------
// <copyright file="TiposDocumentosGlobalDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TiposDocumentosGlobalDAL.</summary>
// <author>INGENIAN SOFTWARE[Darío Beltrán]</author>
// <date>18/03/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase TiposDocumentosGlobalDAL
    /// </summary>
    public class TiposDocumentosGlobalDAL : GeneralDAL
    {
        /// <summary>
        /// Consultar Tipo Identificación.
        /// </summary>
        /// <param name="pTiposDocumentos">tipos de documentos a consultar</param>
        /// <returns>Lista resultado de la operación</returns>
        public List<TiposDocumentosGlobal> ConsultarTiposIdentificacionGlobal(string pTiposDocumentos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_Global_TiposDocumentos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@ids", DbType.String, pTiposDocumentos);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiposDocumentosGlobal> vListaTiposDocumentosGlobal = new List<TiposDocumentosGlobal>();
                        while (vDataReaderResults.Read())
                        {
                            TiposDocumentosGlobal vTiposDocumentosGlobal = new TiposDocumentosGlobal();
                            vTiposDocumentosGlobal.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTiposDocumentosGlobal.IdTipoDocumento;
                            vTiposDocumentosGlobal.CodDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vTiposDocumentosGlobal.CodDocumento;
                            vTiposDocumentosGlobal.NomTipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vTiposDocumentosGlobal.NomTipoDocumento;
                            vTiposDocumentosGlobal.codSiif = vDataReaderResults["codSiif"] != DBNull.Value ? Convert.ToString(vDataReaderResults["codSiif"].ToString()) : vTiposDocumentosGlobal.codSiif;
                            vListaTiposDocumentosGlobal.Add(vTiposDocumentosGlobal);
                        }

                        return vListaTiposDocumentosGlobal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Identificación por el id.
        /// </summary>
        /// <param name="pIdTipoDocumento">Id del tipo de identificación a consultar</param>
        /// <returns>resultado de la operación</returns>
        public TiposDocumentosGlobal ConsultarTiposIdentificacionGlobalPorId(int pIdTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TiposDocumentos_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pIdTipoDocumento);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiposDocumentosGlobal vTiposDocumentosGlobal = new TiposDocumentosGlobal();
                        while (vDataReaderResults.Read())
                        {
                            vTiposDocumentosGlobal.ValorSIRECI = vDataReaderResults["ValorSIRECI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorSIRECI"].ToString()) : vTiposDocumentosGlobal.ValorSIRECI;
                            vTiposDocumentosGlobal.CodDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vTiposDocumentosGlobal.CodDocumento;
                            vTiposDocumentosGlobal.NomTipoDocumento = vDataReaderResults["NomTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NomTipoDocumento"].ToString()) : vTiposDocumentosGlobal.NomTipoDocumento;
                            vTiposDocumentosGlobal.codSiif = vDataReaderResults["codSiif"] != DBNull.Value ? Convert.ToString(vDataReaderResults["codSiif"].ToString()) : vTiposDocumentosGlobal.codSiif;
                        }

                        vTiposDocumentosGlobal.IdTipoDocumento = pIdTipoDocumento;
                        return vTiposDocumentosGlobal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
