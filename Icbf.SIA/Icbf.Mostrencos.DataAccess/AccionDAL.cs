﻿//-----------------------------------------------------------------------
// <copyright file="AccionDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase AccionDAL.</summary>
// <author>Ingenian Software</author>
// <date>10/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo Accion.
    /// </summary>
    public class AccionDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccionDAL"/> class.
        /// </summary>
        public AccionDAL()
        {
        }

        /// <summary>
        /// Evento para Cargar la Lista de opciones de Acciones x IdActuacion
        /// </summary>
        /// <param name="sender">The DropDownList</param>
        /// <param name="e">The SelectedIndexChanged</param>
        public List<Accion> ConsultarAccionesActivasPorIdFase(int pIdActuacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Consultar_AccionesActivas"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdActuacion", DbType.Int32, pIdActuacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Accion> vListaAccion = new List<Accion>();
                        while (vDataReaderResults.Read())
                        {
                            Accion vAccion = new Accion();
                            vAccion.IdAccion = vDataReaderResults["IdAccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAccion"].ToString()) : vAccion.IdAccion;
                            vAccion.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vAccion.Nombre;
                            vListaAccion.Add(vAccion);
                        }

                        return vListaAccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
