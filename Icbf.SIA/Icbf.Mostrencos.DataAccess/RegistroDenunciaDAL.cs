//-----------------------------------------------------------------------
// <copyright file="RegistroDenunciaDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistroDenunciaDAL.</summary>
// <author>INGENIAN SOFTWARE[Dar�o Beltr�n]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo RegistroDenuncia.
    /// </summary>
    public class RegistroDenunciaDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegistroDenunciaDAL"/> class.
        /// </summary>
        public RegistroDenunciaDAL()
        {
        }

        /// <summary>
        /// M�todo para Insertar RegistroDenuncia.
        /// </summary>
        /// <param name="pRegistroDenuncia">Entidad a Insertar</param>
        /// <returns>resultado de la operaci�n</returns>
        public int InsertarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@RadicadoDenuncia", DbType.String, pRegistroDenuncia.RadicadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pRegistroDenuncia.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@Acepto", DbType.Int32, pRegistroDenuncia.Acepto);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionDenuncia", DbType.String, pRegistroDenuncia.DescripcionDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalUbicacion", DbType.Int32, pRegistroDenuncia.IdRegionalUbicacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pRegistroDenuncia.IdEstado);
                    if (pRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@DenunciaInterna", DbType.String, pRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna);
                    }

                    if (pRegistroDenuncia.RadicadoCorrespondencia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@RadicadoCorrespondencia", DbType.String, pRegistroDenuncia.RadicadoCorrespondencia);
                    }

                    if (pRegistroDenuncia.FechaRadicadoCorrespondencia != null && pRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/1900") && pRegistroDenuncia.FechaRadicadoCorrespondencia >= Convert.ToDateTime("1/01/1753"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaRadicadoCorrespondencia", DbType.DateTime, pRegistroDenuncia.FechaRadicadoCorrespondencia);
                    }

                    if (pRegistroDenuncia.FechaRadicadoDenuncia != null && pRegistroDenuncia.FechaRadicadoDenuncia != Convert.ToDateTime("01/01/0001"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaRadicadoDenuncia", DbType.DateTime, pRegistroDenuncia.FechaRadicadoDenuncia);
                    }

                    if (pRegistroDenuncia.HoraRadicadoCorrespondencia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@HoraRadicadoCorrespondencia", DbType.String, pRegistroDenuncia.HoraRadicadoCorrespondencia);
                    }

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRegistroDenuncia.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRegistroDenuncia.IdDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDenunciaBien").ToString());
                    this.GenerarLogAuditoria(pRegistroDenuncia, vDbCommand);

                    return pRegistroDenuncia.IdDenunciaBien;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para Modificar un Registro de denuncia.
        /// </summary>
        /// <param name="pRegistroDenuncia">Entidad a modificar</param>
        /// <returns>Resultado de la operaci�n</returns>
        public int ModificarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pRegistroDenuncia.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@Acepto", DbType.Int32, pRegistroDenuncia.Acepto);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionDenuncia", DbType.String, pRegistroDenuncia.DescripcionDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalUbicacion", DbType.Int32, pRegistroDenuncia.IdRegionalUbicacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRegistroDenuncia.UsuarioModifica);

                    if (pRegistroDenuncia.RadicadoCorrespondencia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@DenunciaRecibidaCorrespondenciaPorInterna", DbType.String, pRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna);
                    }

                    if (pRegistroDenuncia.RadicadoCorrespondencia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@RadicadoCorrespondencia", DbType.String, pRegistroDenuncia.RadicadoCorrespondencia);
                    }

                    if (pRegistroDenuncia.FechaRadicadoCorrespondencia != null && pRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/1900") && pRegistroDenuncia.FechaRadicadoCorrespondencia >= Convert.ToDateTime("1/01/1753"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaRadicadoCorrespondencia", DbType.DateTime, pRegistroDenuncia.FechaRadicadoCorrespondencia);
                    }

                    if (pRegistroDenuncia.FechaRadicadoDenuncia != null && pRegistroDenuncia.FechaRadicadoDenuncia != Convert.ToDateTime("01/01/0001"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaRadicadoDenuncia", DbType.DateTime, pRegistroDenuncia.FechaRadicadoDenuncia);
                    }

                    if (pRegistroDenuncia.HoraRadicadoCorrespondencia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@HoraRadicadoCorrespondencia", DbType.String, pRegistroDenuncia.HoraRadicadoCorrespondencia);
                    }

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    this.GenerarLogAuditoria(pRegistroDenuncia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para eliminar un Registro de denuncia.
        /// </summary>
        /// <param name="pRegistroDenuncia">Entidad a eliminar</param>
        /// <returns>Resultado de la operaci�n</returns>
        public int EliminarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pRegistroDenuncia.IdDenunciaBien);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    this.GenerarLogAuditoria(pRegistroDenuncia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar una denuncia por Id
        /// </summary>
        /// <param name="pIdDenunciaBien">Id a consultar</param>
        /// <returns>Resultadod e la consulta</returns>
        public RegistroDenuncia ConsultarRegistroDenuncia(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vRegistroDenuncia.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vRegistroDenuncia.IdDenunciaBien;
                            vRegistroDenuncia.CodDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vRegistroDenuncia.CodDocumento;
                            vRegistroDenuncia.NUMEROIDENTIFICACION = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NUMEROIDENTIFICACION"].ToString()) : vRegistroDenuncia.NUMEROIDENTIFICACION;
                            vRegistroDenuncia.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vRegistroDenuncia.NombreTipoPersona;
                            vRegistroDenuncia.PRIMERNOMBRE = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vRegistroDenuncia.PRIMERNOMBRE;
                            vRegistroDenuncia.SEGUNDONOMBRE = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vRegistroDenuncia.SEGUNDONOMBRE;
                            vRegistroDenuncia.PRIMERAPELLIDO = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vRegistroDenuncia.PRIMERAPELLIDO;
                            vRegistroDenuncia.SEGUNDOAPELLIDO = vDataReaderResults["SEGUNDOAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDOAPELLIDO"].ToString()) : vRegistroDenuncia.SEGUNDOAPELLIDO;
                            vRegistroDenuncia.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vRegistroDenuncia.NombreDepartamento;
                            vRegistroDenuncia.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vRegistroDenuncia.NombreMunicipio;
                            vRegistroDenuncia.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdZona"].ToString()) : vRegistroDenuncia.IdZona;
                            vRegistroDenuncia.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vRegistroDenuncia.Direccion;
                            vRegistroDenuncia.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"].ToString()) : vRegistroDenuncia.Indicativo;
                            vRegistroDenuncia.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vRegistroDenuncia.Telefono;
                            vRegistroDenuncia.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vRegistroDenuncia.Celular;
                            vRegistroDenuncia.CORREOELECTRONICO = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vRegistroDenuncia.CORREOELECTRONICO;
                            vRegistroDenuncia.Acepto = vDataReaderResults["Acepto"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Acepto"].ToString()) : vRegistroDenuncia.Acepto;
                            vRegistroDenuncia.DescripcionDenuncia = vDataReaderResults["DescripcionDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDenuncia"].ToString()) : vRegistroDenuncia.DescripcionDenuncia;
                            vRegistroDenuncia.IdRegionalUbicacion = vDataReaderResults["IdRegionalUbicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalUbicacion"].ToString()) : vRegistroDenuncia.IdRegionalUbicacion;
                            vRegistroDenuncia.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegistroDenuncia.NombreRegional;
                            vRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna = vDataReaderResults["DenunciaRecibidaCorrespondenciaPorInterna"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DenunciaRecibidaCorrespondenciaPorInterna"].ToString()) : vRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna;
                            vRegistroDenuncia.RadicadoCorrespondencia = vDataReaderResults["RadicadoCorrespondencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoCorrespondencia"].ToString()) : vRegistroDenuncia.RadicadoCorrespondencia;
                            vRegistroDenuncia.FechaRadicadoCorrespondencia = vDataReaderResults["FechaRadicadoCorrespondencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoCorrespondencia"].ToString()) : vRegistroDenuncia.FechaRadicadoCorrespondencia;
                            vRegistroDenuncia.HoraRadicadoCorrespondencia = vDataReaderResults["HoraRadicadoCorrespondencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["HoraRadicadoCorrespondencia"].ToString()) : vRegistroDenuncia.HoraRadicadoCorrespondencia;
                            vRegistroDenuncia.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vRegistroDenuncia.RazonSocial;
                            vRegistroDenuncia.FechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vRegistroDenuncia.FechaRadicadoDenuncia;
                            vRegistroDenuncia.Abogado.FechaAsignacionAbogado = vDataReaderResults["FechaAsignacionAbogado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacionAbogado"].ToString()) : vRegistroDenuncia.Abogado.FechaAsignacionAbogado;
                            vRegistroDenuncia.Abogado.NombreAbogado = vDataReaderResults["NombreAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbogado"].ToString()) : vRegistroDenuncia.Abogado.NombreAbogado;
                            vRegistroDenuncia.IdEstadoDenuncia = vDataReaderResults["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDenuncia"].ToString()) : vRegistroDenuncia.IdEstadoDenuncia;
                        }

                        return vRegistroDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar una denuncia por Tipo y n�mero de identificaci�n
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Id Tipo de documento</param>
        /// <param name="pNumeroIdentificacion">Numero de identificaci�n</param>
        /// <returns>Resultado de la operaci�n</returns>
        public RegistroDenuncia ConsultarRegistroDenunciante(int pIdTipoDocIdentifica, string pNumeroIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_ConsultarDenunciante"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocIdentifica", DbType.Int32, pIdTipoDocIdentifica);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vRegistroDenuncia.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vRegistroDenuncia.NombreTipoPersona;
                            vRegistroDenuncia.PRIMERNOMBRE = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vRegistroDenuncia.PRIMERNOMBRE;
                            vRegistroDenuncia.SEGUNDONOMBRE = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vRegistroDenuncia.SEGUNDONOMBRE;
                            vRegistroDenuncia.PRIMERAPELLIDO = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vRegistroDenuncia.PRIMERAPELLIDO;
                            vRegistroDenuncia.SEGUNDOAPELLIDO = vDataReaderResults["SEGUNDOAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDOAPELLIDO"].ToString()) : vRegistroDenuncia.SEGUNDOAPELLIDO;
                            vRegistroDenuncia.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vRegistroDenuncia.RazonSocial;
                            vRegistroDenuncia.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vRegistroDenuncia.NombreDepartamento;
                            vRegistroDenuncia.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vRegistroDenuncia.NombreMunicipio;
                            vRegistroDenuncia.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdZona"].ToString()) : vRegistroDenuncia.IdZona;
                            vRegistroDenuncia.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vRegistroDenuncia.Direccion;
                            vRegistroDenuncia.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"].ToString()) : vRegistroDenuncia.Indicativo;
                            vRegistroDenuncia.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vRegistroDenuncia.Telefono;
                            vRegistroDenuncia.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vRegistroDenuncia.Celular;
                            vRegistroDenuncia.CORREOELECTRONICO = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vRegistroDenuncia.CORREOELECTRONICO;
                            vRegistroDenuncia.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vRegistroDenuncia.IdTercero;
                        }

                        return vRegistroDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta una denuncia por parametros
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Id tipo documento identificaci�n</param>
        /// <param name="pNumeroIdentificacion">N�mero de identificaci�n</param>
        /// <param name="pDenunciante">Nombre del denunciante</param>
        /// <param name="pRadicadoDenuncia">N�mero de radicado de la denuncia</param>
        /// <returns>Lista del resultado de la operaci�n</returns>
        public List<RegistroDenuncia> ConsultarRegistroDenuncias(int? pIdTipoDocIdentifica, string pNumeroIdentificacion, string pDenunciante, string pRadicadoDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncias_Consultar"))
                {
                    if (pIdTipoDocIdentifica != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCIDENTIFICA", DbType.Int32, pIdTipoDocIdentifica);
                    }

                    if (pNumeroIdentificacion != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, pNumeroIdentificacion);
                    }

                    if (pDenunciante != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Denunciante", DbType.String, pDenunciante);
                    }

                    if (pRadicadoDenuncia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@RadicadoDenuncia", DbType.String, pRadicadoDenuncia);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroDenuncia> vListaRegistroDenuncia = new List<RegistroDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
                            vRegistroDenuncia.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vRegistroDenuncia.IdDenunciaBien;
                            vRegistroDenuncia.RadicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : vRegistroDenuncia.RadicadoDenuncia;
                            vRegistroDenuncia.CodDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vRegistroDenuncia.CodDocumento;
                            if (pIdTipoDocIdentifica == 7)
                            {
                                vRegistroDenuncia.NUMEROIDENTIFICACION = vDataReaderResults["NIT"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NIT"].ToString()) : vRegistroDenuncia.NUMEROIDENTIFICACION;
                            }
                            else
                            {
                                vRegistroDenuncia.NUMEROIDENTIFICACION = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NUMEROIDENTIFICACION"].ToString()) : vRegistroDenuncia.NUMEROIDENTIFICACION;
                            }

                            vRegistroDenuncia.NombreMostrar = vDataReaderResults["NombreMostrar"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMostrar"].ToString()) : vRegistroDenuncia.NombreMostrar;
                            vRegistroDenuncia.FechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vRegistroDenuncia.FechaRadicadoDenuncia;
                            vRegistroDenuncia.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vRegistroDenuncia.NombreDepartamento;
                            vRegistroDenuncia.NombreEstadoDenuncia = vDataReaderResults["NombreEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDenuncia"].ToString()) : vRegistroDenuncia.NombreEstadoDenuncia;
                            vListaRegistroDenuncia.Add(vRegistroDenuncia);
                        }

                        return vListaRegistroDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta una denuncia por id de la denuncia y tipo de documento
        /// </summary>
        /// <param name="pIdDenuncia">Id de la denuncia</param>
        /// <param name="pTipoDocumento">Tipo de documento</param>
        /// <returns>Resultado de la operaci�n</returns>
        public RegistroDenuncia ConsultarDocumentosBasicos(int pIdDenuncia, int pTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_ConsultarDocumentosBasicosDenunciante"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenuncia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vRegistroDenuncia.DocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();
                            vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo;
                            vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo;
                        }

                        return vRegistroDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta los documentos de la secci�n Otros 
        /// </summary>
        /// <param name="pIdDenuncia">Id de la denuncia</param>
        /// <param name="pTipoDocumento">Tipo de documento</param>
        /// <returns>Lista del resultado de la operaci�n</returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarOtrosDocumentos(int pIdDenuncia, int pTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_ConsultarDocumentosBasicosDenunciante"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTipoDocumento);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentosSolicitadosDenunciaBien> vListaRegistroDenuncia = new List<DocumentosSolicitadosDenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosDenunciaBien vRegistroDenuncia = new DocumentosSolicitadosDenunciaBien();
                            vRegistroDenuncia.ObservacionesDocumento = vDataReaderResults["ObservacionesDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumento"].ToString()) : vRegistroDenuncia.ObservacionesDocumento;
                            vRegistroDenuncia.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vRegistroDenuncia.NombreArchivo;
                            vRegistroDenuncia.ExisteArchivo = true;
                            vListaRegistroDenuncia.Add(vRegistroDenuncia);
                        }

                        return vListaRegistroDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta el ultimo id de la denuncia
        /// </summary>
        /// <returns>�ltimo Id Insertado</returns>
        public int ConsultarConsecutivoDenuncia()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_ConsecutivoDenuncia_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vRegistroDenuncia.ConsecutivoDenuncia = vDataReaderResults["ConsecutivoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoDenuncia"].ToString()) : vRegistroDenuncia.ConsecutivoDenuncia;
                        }

                        return vRegistroDenuncia.ConsecutivoDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta el ultimo id de la denuncia extrayendfo el a�o
        /// </summary>
        /// <returns>�ltimo Id Insertado</returns>
        public int ConsultarAnioUltimoRegistro()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_ConsecutivoDenunciaAnio_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vRegistroDenuncia.AnioRadicado = vDataReaderResults["AnioRadicado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnioRadicado"].ToString()) : vRegistroDenuncia.AnioRadicado;
                        }

                        return vRegistroDenuncia.AnioRadicado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ConsultarCorreoCoordinadorJuridico(int? pIdRegional, string pProviderKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_ConsultarCorreosCoordinadorJuridico"))
                {
                    if (pIdRegional != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    }

                    if (pProviderKey != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pProviderKey);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string vCorreoCoordinadorJuridico = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            vCorreoCoordinadorJuridico = string.Concat(vCorreoCoordinadorJuridico, (vDataReaderResults["CorreoElectronico"] != DBNull.Value ? vDataReaderResults["CorreoElectronico"] : vCorreoCoordinadorJuridico), "; ");
                        }

                        vCorreoCoordinadorJuridico = (!string.IsNullOrEmpty(vCorreoCoordinadorJuridico)) ? vCorreoCoordinadorJuridico.Remove(vCorreoCoordinadorJuridico.Length - 2) : vCorreoCoordinadorJuridico;

                        return vCorreoCoordinadorJuridico;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ConsultarNombreCoordinadorJuridico(string pCorreoCoordinadorJuridico)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_ConsultarNombreCoordinadorJuridico"))
                {
                    if (pCorreoCoordinadorJuridico != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@CorreoCoordinadorJuridico", DbType.String, pCorreoCoordinadorJuridico);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string vNombreCoordinadorJuridico = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            vNombreCoordinadorJuridico = string.Concat(vNombreCoordinadorJuridico, (vDataReaderResults["NOMBRE"] != DBNull.Value ? vDataReaderResults["NOMBRE"] : vNombreCoordinadorJuridico), "; ");
                        }

                        vNombreCoordinadorJuridico = (!string.IsNullOrEmpty(vNombreCoordinadorJuridico)) ? vNombreCoordinadorJuridico.Remove(vNombreCoordinadorJuridico.Length - 2) : vNombreCoordinadorJuridico;

                        return vNombreCoordinadorJuridico;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ConsultarCorreoAdministrador(string pProviderKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_ConsultarCorreosAdministrador"))
                {

                    if (pProviderKey != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pProviderKey);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string vCorreoCoordinadorJuridico = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            vCorreoCoordinadorJuridico = string.Concat(vCorreoCoordinadorJuridico, (vDataReaderResults["CorreoElectronico"] != DBNull.Value ? vDataReaderResults["CorreoElectronico"] : vCorreoCoordinadorJuridico), "; ");
                        }

                        vCorreoCoordinadorJuridico = (!string.IsNullOrEmpty(vCorreoCoordinadorJuridico)) ? vCorreoCoordinadorJuridico.Remove(vCorreoCoordinadorJuridico.Length - 2) : vCorreoCoordinadorJuridico;

                        return vCorreoCoordinadorJuridico;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
