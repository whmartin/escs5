﻿
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System.Collections.Generic;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class TerceroDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta un tercero por el id tercero
        /// </summary>
        /// <param name="pIdTercero">id del tercero que se va a consultar</param>
        /// <returns>Variable de tipo Tercero con la información consultada</returns>
        public Tercero ConsultarTercerolXId(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Tercero_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {
                            vTercero.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vTercero.Celular;
                            vTercero.ClaseActividad = vDataReaderResults["ClaseActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseActividad"]) : vTercero.ClaseActividad;
                            vTercero.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vTercero.ConsecutivoInterno;
                            vTercero.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vTercero.CorreoElectronico;
                            vTercero.CreadoPorInterno = vDataReaderResults["CreadoPorInterno"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CreadoPorInterno"].ToString()) : vTercero.CreadoPorInterno;
                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"]) : vTercero.DigitoVerificacion;
                            vTercero.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vTercero.Direccion;
                            vTercero.EsFundacion = vDataReaderResults["EsFundacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsFundacion"].ToString()) : vTercero.EsFundacion;
                            vTercero.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vTercero.Extension;
                            vTercero.FechaExpedicion = vDataReaderResults["FechaExpedicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicion"]) : vTercero.FechaExpedicion;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.IdEstadoTercero = vDataReaderResults["IdEstadoTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTercero"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"]) : vTercero.IdMunicipio;
                            vTercero.IdTerceroEnOferentes = vDataReaderResults["IdTerceroEnOferentes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTerceroEnOferentes"].ToString()) : vTercero.IdTerceroEnOferentes;
                            vTercero.IdTipoDocIdentifica = vDataReaderResults["IdTipoDocIdentifica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocIdentifica"].ToString()) : vTercero.IdTipoDocIdentifica;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"]) : vTercero.IdTipoPersona;
                            vTercero.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vTercero.IdZona;
                            vTercero.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"].ToString()) : vTercero.Indicativo;
                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"]) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"]) : vTercero.PrimerNombre;
                            vTercero.ProviderUserKey = vDataReaderResults["ProviderUserKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProviderUserKey"].ToString()) : vTercero.ProviderUserKey;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"]) : vTercero.SegundoApellido;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vTercero.Telefono;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTercero.NombreTipoPersona;
                            vTercero.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vTercero.NombreMunicipio;
                            vTercero.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vTercero.NombreDepartamento;
                        }

                        vTercero.IdTercero = pIdTercero;
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un tercero por el id tercero
        /// </summary>
        /// <param name="pIdTercero">id del tercero que se va a consultar</param>
        /// <returns>Variable de tipo Tercero con la información consultada</returns>
        public List<Tercero> ConsultarTercerolTodos()
        {
            try
            {


                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetSqlStringCommand("SELECT * FROM [SIA].[Tercero].[TERCERO]"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Tercero> vTerceroList = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"]) : vTercero.PrimerNombre;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"]) : vTercero.SegundoApellido;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTerceroList.Add(vTercero);
                        }

                        return vTerceroList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta del tercero por Tipo Juridico y Razon Social
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarJuridicoTercerosPorNombre(string nombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_Terceros_Consultar_Juridicos_PorNombre"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@nombre", DbType.String, nombre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Tercero> vTerceroLista = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.RazonSocial = vDataReaderResults["NombreEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidad"].ToString()) : vTercero.RazonSocial;
                            vTercero.Departamento = vDataReaderResults["Departamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Departamento"]) : vTercero.Departamento;
                            vTercero.Municipio = vDataReaderResults["Municipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Municipio"].ToString()) : vTercero.Municipio;
                            vTercero.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vTercero.Direccion;
                            vTerceroLista.Add(vTercero);
                        }
                        return vTerceroLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta del tercero por Tipo Juridico y Razon Social
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public Tercero ConsultarTipoEntidadPorIdTercero(int IdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_TipoEntidad_Consultar_PorIdTercero"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, IdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {                            
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdTipoSectorEntidad = vDataReaderResults["IdTipoSectorEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSectorEntidad"].ToString()) : vTercero.IdTipoSectorEntidad;
                            vTercero.Entidad = vDataReaderResults["TipoEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoEntidad"]) : vTercero.Entidad;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.NombreTipoPersona = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vTercero.NombreTipoPersona;
                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un tercero por el tipo de identificación , el número de identificación y el nombre del tipo de persona
        /// </summary>
        /// <param name="pIdTipoIdentificacion"> id del tipo de identificación</param>
        /// <param name="pNumeroIdentificacion">Número de identificación</param>
        /// <param name="pNombreTipoPersona">Nombre del tipo de persona</param>
        /// <returns>Variable de tipo Tercero con la información resultante de la consulta</returns>
        public Tercero ConsultarTercero(int pIdTipoIdentificacion, string pNumeroIdentificacion, string pNombreTipoPersona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Tercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pIdTipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoPersona", DbType.String, pNombreTipoPersona);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vTercero.Celular;
                            vTercero.ClaseActividad = vDataReaderResults["ClaseActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseActividad"]) : vTercero.ClaseActividad;
                            vTercero.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vTercero.ConsecutivoInterno;
                            vTercero.CorreoElectronico = vDataReaderResults["CorreoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoElectronico"].ToString()) : vTercero.CorreoElectronico;
                            vTercero.CreadoPorInterno = vDataReaderResults["CreadoPorInterno"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CreadoPorInterno"].ToString()) : vTercero.CreadoPorInterno;
                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"]) : vTercero.DigitoVerificacion;
                            vTercero.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vTercero.Direccion;
                            vTercero.EsFundacion = vDataReaderResults["EsFundacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsFundacion"].ToString()) : vTercero.EsFundacion;
                            vTercero.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vTercero.Extension;
                            vTercero.FechaExpedicion = vDataReaderResults["FechaExpedicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicion"]) : vTercero.FechaExpedicion;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.IdEstadoTercero = vDataReaderResults["IdEstadoTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTercero"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"]) : vTercero.IdMunicipio;
                            vTercero.IdTerceroEnOferentes = vDataReaderResults["IdTerceroEnOferentes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTerceroEnOferentes"].ToString()) : vTercero.IdTerceroEnOferentes;
                            vTercero.IdTipoDocIdentifica = vDataReaderResults["IdTipoDocIdentifica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocIdentifica"].ToString()) : vTercero.IdTipoDocIdentifica;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"]) : vTercero.IdTipoPersona;
                            vTercero.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vTercero.IdZona;
                            vTercero.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"].ToString()) : vTercero.Indicativo;
                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"]) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"]) : vTercero.PrimerNombre;
                            vTercero.ProviderUserKey = vDataReaderResults["ProviderUserKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProviderUserKey"].ToString()) : vTercero.ProviderUserKey;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"]) : vTercero.SegundoApellido;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vTercero.Telefono;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vTercero.NombreMunicipio;
                            vTercero.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vTercero.NombreDepartamento;
                            vTercero.NombreTipoPersona = pNombreTipoPersona;
                        }

                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
