﻿namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Text;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class DenunciaMuebleTituloDAL : GeneralDAL
    {
        //Cargar Lista Regionales
        public List<DenunciaMuebleTitulo> ConsultarValorSumaDenuncia(int IdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Campo_ValorDenuncia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdBienDenuncia", DbType.Int32, IdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DenunciaMuebleTitulo> vlstDenunciaMuebleTitulo = new List<DenunciaMuebleTitulo>();
                        while (vDataReaderResults.Read())
                        {
                            DenunciaMuebleTitulo vDenunciaMuebleTitulo = new DenunciaMuebleTitulo();
                            vDenunciaMuebleTitulo.IdBienDenuncia = vDataReaderResults["IdBienDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdBienDenuncia"].ToString()) : vDenunciaMuebleTitulo.IdBienDenuncia;
                            vDenunciaMuebleTitulo.ValorDenuncia = vDataReaderResults["ValorDenuncia"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorDenuncia"].ToString()) : vDenunciaMuebleTitulo.ValorDenuncia;
                            vlstDenunciaMuebleTitulo.Add(vDenunciaMuebleTitulo);
                        }
                        return vlstDenunciaMuebleTitulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Cargar Valor Denuncia
        public DenunciaMuebleTitulo ConsultarValorSumaDenunciaPorIdDenuncia(int IdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Campo_ValorDenuncia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdBienDenuncia", DbType.Int32, IdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DenunciaMuebleTitulo pDenunciaMuebleTitulo = new DenunciaMuebleTitulo();
                        while (vDataReaderResults.Read())
                        {
                            pDenunciaMuebleTitulo.IdBienDenuncia = vDataReaderResults["IdBienDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdBienDenuncia"].ToString()) : pDenunciaMuebleTitulo.IdBienDenuncia;
                            pDenunciaMuebleTitulo.ValorDenuncia = vDataReaderResults["ValorDenuncia"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorDenuncia"].ToString()) : pDenunciaMuebleTitulo.ValorDenuncia;
                        }
                        return pDenunciaMuebleTitulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

