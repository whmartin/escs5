﻿

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class DenunciaBienDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta una denuncia bien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">IdDenunciaBien que se va a consultar</param>
        /// <returns>Una variable de tipo DenunciaBien con la información encontrada</returns>
        public DenunciaBien ConsultarDenunciaBienXIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DenunciaBien_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DenunciaBien vDenunciaBien = new DenunciaBien();
                        while (vDataReaderResults.Read())
                        {
                            vDenunciaBien.Acepto = vDataReaderResults["Acepto"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Acepto"].ToString()) : vDenunciaBien.Acepto;
                            vDenunciaBien.DenunciaRecibidaCorrespondenciaPorInterna = vDataReaderResults["DenunciaRecibidaCorrespondenciaPorInterna"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DenunciaRecibidaCorrespondenciaPorInterna"]) : vDenunciaBien.DenunciaRecibidaCorrespondenciaPorInterna;
                            vDenunciaBien.IdEstadoDenuncia = vDataReaderResults["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDenuncia"].ToString()) : vDenunciaBien.IdEstadoDenuncia;
                            vDenunciaBien.DescripcionDenuncia = vDataReaderResults["DescripcionDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDenuncia"].ToString()) : vDenunciaBien.DescripcionDenuncia;
                            vDenunciaBien.FechaAsignacionAbogado = vDataReaderResults["FechaAsignacionAbogado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacionAbogado"].ToString()) : vDenunciaBien.FechaAsignacionAbogado;
                            vDenunciaBien.FechaRadicadoCorrespondencia = vDataReaderResults["FechaRadicadoCorrespondencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoCorrespondencia"]) : vDenunciaBien.FechaRadicadoCorrespondencia;
                            vDenunciaBien.FechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vDenunciaBien.FechaRadicadoDenuncia;
                            vDenunciaBien.HoraRadicadoCorrespondencia = vDataReaderResults["HoraRadicadoCorrespondencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["HoraRadicadoCorrespondencia"].ToString()) : vDenunciaBien.HoraRadicadoCorrespondencia;
                            vDenunciaBien.IdAbogado = vDataReaderResults["IdAbogado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAbogado"].ToString()) : vDenunciaBien.IdAbogado;
                            vDenunciaBien.IdClaseDenuncia = vDataReaderResults["IdClaseDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClaseDenuncia"]) : vDenunciaBien.IdClaseDenuncia;
                            vDenunciaBien.IdRegionalUbicacion = vDataReaderResults["IdRegionalUbicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalUbicacion"].ToString()) : vDenunciaBien.IdRegionalUbicacion;
                            vDenunciaBien.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDenunciaBien.IdTercero;
                            vDenunciaBien.RadicadoCorrespondencia = vDataReaderResults["RadicadoCorrespondencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoCorrespondencia"]) : vDenunciaBien.RadicadoCorrespondencia;
                            vDenunciaBien.RadicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : vDenunciaBien.RadicadoDenuncia;
                            vDenunciaBien.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDenunciaBien.IdTercero;
                            vDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDenunciaBien.UsuarioCrea;
                            vDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDenunciaBien.FechaCrea;
                            vDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDenunciaBien.UsuarioModifica;
                            vDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDenunciaBien.FechaModifica;
                            vDenunciaBien.ExisteDenuncia = vDataReaderResults["ExisteDenuncia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ExisteDenuncia"].ToString()) : false;
                            vDenunciaBien.InterposicionRecurso = vDataReaderResults["InterposicionRecurso"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["InterposicionRecurso"].ToString()) : vDenunciaBien.InterposicionRecurso;

                        }

                        vDenunciaBien.IdDenunciaBien = pIdDenunciaBien;
                        return vDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta las denuncias bien que coincidan con los parámetros de búsqueda
        /// </summary>
        /// <param name="pIdRegional">Id de la regional de la denuncia</param>
        /// <param name="pEstado">Estado de la denuncia</param>
        /// <param name="pRegistradoDesde">Fecha desde del registro de la denuncia</param>
        /// <param name="pRegistradoHasta">Fecha hasta del registro de la denuncia</param>
        /// <param name="pRadicadoDenuncia">radicado de la denuncia</param>
        /// <param name="pIdAbogado">id del abogado que esta asignado a la denuncia</param>
        /// <returns>Lista de denuncias que cumplen con los criterios de la búsqueda</returns>
        public List<DenunciaBien> ConsultarDenunciaBien(int pIdRegional, int pEstado, string pRegistradoDesde, string pRegistradoHasta, string pRadicadoDenuncia, int pIdAbogado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DenunciaBien_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    vDataBase.AddInParameter(vDbCommand, "@IdAbogado", DbType.Int32, pIdAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@RegistradoDesde", DbType.String, pRegistradoDesde);
                    vDataBase.AddInParameter(vDbCommand, "@RegistradoHasta", DbType.String, pRegistradoHasta);
                    vDataBase.AddInParameter(vDbCommand, "@RadicadoDenuncia", DbType.String, pRadicadoDenuncia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DenunciaBien> vListaDenunciaBien = new List<DenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            DenunciaBien vDenunciaBien = new DenunciaBien();
                            vDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDenunciaBien.IdDenunciaBien;
                            vDenunciaBien.Acepto = vDataReaderResults["Acepto"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Acepto"].ToString()) : vDenunciaBien.Acepto;
                            vDenunciaBien.DenunciaRecibidaCorrespondenciaPorInterna = vDataReaderResults["DenunciaRecibidaCorrespondenciaPorInterna"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DenunciaRecibidaCorrespondenciaPorInterna"]) : vDenunciaBien.DenunciaRecibidaCorrespondenciaPorInterna;
                            vDenunciaBien.IdEstadoDenuncia = vDataReaderResults["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDenuncia"].ToString()) : vDenunciaBien.IdEstadoDenuncia;
                            vDenunciaBien.DescripcionDenuncia = vDataReaderResults["DescripcionDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDenuncia"].ToString()) : vDenunciaBien.DescripcionDenuncia;
                            vDenunciaBien.FechaAsignacionAbogado = vDataReaderResults["FechaAsignacionAbogado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacionAbogado"].ToString()) : vDenunciaBien.FechaAsignacionAbogado;
                            vDenunciaBien.FechaRadicadoCorrespondencia = vDataReaderResults["FechaRadicadoCorrespondencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoCorrespondencia"]) : vDenunciaBien.FechaRadicadoCorrespondencia;
                            vDenunciaBien.FechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vDenunciaBien.FechaRadicadoDenuncia;
                            vDenunciaBien.HoraRadicadoCorrespondencia = vDataReaderResults["HoraRadicadoCorrespondencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["HoraRadicadoCorrespondencia"].ToString()) : vDenunciaBien.HoraRadicadoCorrespondencia;
                            vDenunciaBien.IdAbogado = vDataReaderResults["IdAbogado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAbogado"].ToString()) : vDenunciaBien.IdAbogado;
                            vDenunciaBien.IdClaseDenuncia = vDataReaderResults["IdClaseDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClaseDenuncia"]) : vDenunciaBien.IdClaseDenuncia;
                            vDenunciaBien.IdRegionalUbicacion = vDataReaderResults["IdRegionalUbicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalUbicacion"].ToString()) : vDenunciaBien.IdRegionalUbicacion;
                            vDenunciaBien.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDenunciaBien.IdTercero;
                            vDenunciaBien.RadicadoCorrespondencia = vDataReaderResults["RadicadoCorrespondencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoCorrespondencia"]) : vDenunciaBien.RadicadoCorrespondencia;
                            vDenunciaBien.RadicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : vDenunciaBien.RadicadoDenuncia;
                            vDenunciaBien.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDenunciaBien.IdTercero;
                            vDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDenunciaBien.UsuarioCrea;
                            vDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDenunciaBien.FechaCrea;
                            vDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDenunciaBien.UsuarioModifica;
                            vDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDenunciaBien.FechaModifica;
                            vListaDenunciaBien.Add(vDenunciaBien);
                            vDenunciaBien = null;
                        }

                        return vListaDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de una denuncia
        /// </summary>
        /// <param name="pDenunciaBien">variable de tipo DenunciaBien que contiene la información que se va a almacenar </param>
        /// <returns>1 si se actualizó correctamente la denuncia </returns>
        public int EditarDenunciaBien(DenunciaBien pDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DenunciaBien_Editar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@RadicadoDenuncia", DbType.String, pDenunciaBien.RadicadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pDenunciaBien.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@Acepto", DbType.Byte, pDenunciaBien.Acepto);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionDenuncia", DbType.String, pDenunciaBien.DescripcionDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalUbicacion", DbType.Int32, pDenunciaBien.IdRegionalUbicacion);
                    vDataBase.AddInParameter(vDbCommand, "@DenunciaRecibidaCorrespondenciaPorInterna", DbType.String, pDenunciaBien.DenunciaRecibidaCorrespondenciaPorInterna);
                    vDataBase.AddInParameter(vDbCommand, "@RadicadoCorrespondencia", DbType.String, pDenunciaBien.RadicadoCorrespondencia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRadicadoCorrespondencia", DbType.DateTime, pDenunciaBien.FechaRadicadoCorrespondencia);
                    vDataBase.AddInParameter(vDbCommand, "@HoraRadicadoCorrespondencia", DbType.DateTime, pDenunciaBien.HoraRadicadoCorrespondencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDenuncia", DbType.Int32, pDenunciaBien.IdEstadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRadicadoDenuncia", DbType.DateTime, pDenunciaBien.FechaRadicadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdAbogado", DbType.Int32, pDenunciaBien.IdAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAsignacionAbogado", DbType.DateTime, pDenunciaBien.FechaAsignacionAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@IdClaseDenuncia", DbType.Int32, pDenunciaBien.IdClaseDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDenunciaBien.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pDenunciaBien;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pDenunciaBien, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EditarDenunciaBienFase(int vDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DenunciaBien_Editar_Fase"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdFase", DbType.Int32, 3);
                    vDataBase.AddOutParameter(vDbCommand, "@IdActuacion", DbType.Int32, 3);
                    vDataBase.AddOutParameter(vDbCommand, "@IdAccion", DbType.Int32, 3);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vDenunciaBien);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EditarDocumentacionCompleta(int vDenunciaBien, bool vDocumentacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_Insertar_DocumentacionCompleta"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdActualizado", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentacionCompleta", DbType.Int32, vDocumentacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary> 
        /// Actualizar el Estado de la Denuncia Bien a Terminada
        /// </summary>
        /// <param name="vDenunciaBien">Id de la denuncia</param>
        /// <returns>Resultado de la operación</returns>
        public int EditarDenunciaBienEstado(int vDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EstadoDenunciaBien_Editar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEstadoDenuncia", DbType.Int32, 3);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vDenunciaBien);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
