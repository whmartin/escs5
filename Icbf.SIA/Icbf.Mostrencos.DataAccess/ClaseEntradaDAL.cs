﻿//-----------------------------------------------------------------------
// <copyright file="ClaseEntradaDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ClaseEntradaDAL.</summary>
// <author>INGENIAN</author>
// <date>06/01/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class ClaseEntradaDAL : GeneralDAL
    {

        private Database vDataBase;

        public ClaseEntradaDAL()
        {
            this.vDataBase = DatabaseFactory.CreateDatabase("DataBaseSEVEN");
        }

        /// <summary>
        /// consulta los ClaseEntrada
        /// </summary>
        /// <returns>LISTA DE ClaseEntrada</returns>
        public List<ClaseEntrada> ConsultarClaseEntrada()
        {
            try
            {
                using (DbConnection vDbConnection = vDataBase.CreateConnection())
                {
                    vDbConnection.Open();
                    DbCommand vDbCommand = vDbConnection.CreateCommand();
                    vDbCommand.CommandText = "SELECT [ITE_CODI] ,[ITE_NOMB] FROM [produccion].[dbo].[GN_VITEM] WHERE [ITE_ACTI] = N's' and [TIT_CONT] = 150";

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ClaseEntrada> vListClaseEntrada = new List<ClaseEntrada>();
                        while (vDataReaderResults.Read())
                        {
                            ClaseEntrada vClaseEntrada = new ClaseEntrada();
                            vClaseEntrada.IdClaseEntrada = vDataReaderResults["ITE_CODI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_CODI"].ToString()) : vClaseEntrada.IdClaseEntrada;
                            vClaseEntrada.NombreClaseEntrada = vDataReaderResults["ITE_NOMB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_NOMB"].ToString().ToUpper()) : vClaseEntrada.NombreClaseEntrada;
                            vListClaseEntrada.Add(vClaseEntrada);
                            vClaseEntrada = null;
                        }

                        return vListClaseEntrada;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
