using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.DataAccess
{
    public class RegionalesDAL : GeneralDAL
    {
        public RegionalesDAL()
        {
        }

        public List<Regionales> ConsultarRegionales()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_DIV_Regionaless_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Regionales> vListaRegionales = new List<Regionales>();
                        while (vDataReaderResults.Read())
                        {
                            Regionales vRegionales = new Regionales();
                            vRegionales.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vRegionales.IdRegional;
                            vRegionales.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vRegionales.CodigoRegional;
                            vRegionales.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegionales.NombreRegional;
                            vRegionales.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vRegionales.Estado;
                            vListaRegionales.Add(vRegionales);
                        }
                        return vListaRegionales;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public Regionales ConsultarRegionalesPorId(String idRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_DIV_Regionaless_Consultar_Id"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, idRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        Regionales vRegionales = new Regionales();
                        vDataReaderResults.Read();
                       vRegionales.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vRegionales.IdRegional;
                        vRegionales.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vRegionales.CodigoRegional;
                        vRegionales.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegionales.NombreRegional;
                        vRegionales.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vRegionales.Estado;

                        return vRegionales;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Cargar Lista Regionales
        public List<Regionales> ConsultarRegionalesAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Consultar_Regionales"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Regionales> vlstRegionales = new List<Regionales>();
                        while (vDataReaderResults.Read())
                        {
                            Regionales vRegionales = new Regionales();
                            vRegionales.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vRegionales.CodigoRegional;
                            vRegionales.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegionales.NombreRegional;
                            vlstRegionales.Add(vRegionales);
                        }

                        return vlstRegionales;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}