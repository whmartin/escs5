﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase que permite el enlace entre el DAL y la BD 
    /// </summary>
    /// <seealso cref="Icbf.Mostrencos.DataAccess.GeneralDAL" />
    public class MedioComunicacionDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MedioComunicacionDAL"/> class.
        /// </summary>
        public MedioComunicacionDAL()
        {
        }

        /// <summary>
        /// Consultar Medios de Comunicación.
        /// </summary>
        /// <param name="pNombre">The p Nombre Despacho.</param>
        /// <param name="pEstado">The p Estado.</param>
        /// <returns>Lista de medios de comunicación filtrados por nombre o estado</returns>
        /// <exception cref="GenericException"></exception>
        public List<MedioComunicacion> ConsultarMedioComunicacion(string pNombre, bool? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_MedioComunicacion_ConsultarMedioComunicacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<MedioComunicacion> vListaMedioComunicacion = new List<MedioComunicacion>();
                        while (vDataReaderResults.Read())
                        {
                            MedioComunicacion vMedioComunicacion = new MedioComunicacion();
                            vMedioComunicacion.IdMedioComunicacion = vDataReaderResults["IdMedioComunicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMedioComunicacion"].ToString()) : vMedioComunicacion.IdMedioComunicacion;
                            vMedioComunicacion.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vMedioComunicacion.Nombre;
                            vMedioComunicacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vMedioComunicacion.Estado;
                            vListaMedioComunicacion.Add(vMedioComunicacion);
                        }
                        return vListaMedioComunicacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Ingresar Medios de Comunicación.
        /// </summary>
        /// <param name="pMedioComunicacion">The p entidad con los datos a ingresar</param>
        /// <returns>0 si el nombre se encuentra duplicado y 1 si se agregó correctamente</returns>
        /// <exception cref="GenericException"></exception>
        public int IngresarMedioComunicacion(MedioComunicacion pMedioComunicacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_MedioComunicacion_CrearMedioComunicacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pMedioComunicacion.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pMedioComunicacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pMedioComunicacion.UsuarioCrea);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int existeMedioComunicacion = 0;
                        while (vDataReaderResults.Read())
                        {
                            existeMedioComunicacion = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Count"].ToString()) : existeMedioComunicacion;
                            if (existeMedioComunicacion > 0)
                            {
                                Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pMedioComunicacion;
                                vAuditoria.ProgramaGeneraLog = true;
                                vAuditoria.Operacion = "INSERT";
                                this.GenerarLogAuditoria(pMedioComunicacion, vDbCommand);
                            }
                        }
                        return existeMedioComunicacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar medios de comunicación por id.
        /// </summary>
        /// <param name="pIdMedioComunicacion">The p Id medio de comunicación.</param>
        /// <returns>medio de comunicación consultado en detalle</returns>
        /// <exception cref="GenericException"></exception>
        public MedioComunicacion ConsultarMedioComunicacionPorId(int pIdMedioComunicacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_MedioComunicacion_ConsultarMedioComunicacionPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdMedioComunicacion", DbType.Int32, pIdMedioComunicacion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        MedioComunicacion vMedioComunicacion = new MedioComunicacion();
                        while (vDataReaderResults.Read())
                        {
                            vMedioComunicacion.IdMedioComunicacion = vDataReaderResults["IdMedioComunicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMedioComunicacion"].ToString()) : vMedioComunicacion.IdMedioComunicacion;
                            vMedioComunicacion.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vMedioComunicacion.Nombre;
                            vMedioComunicacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vMedioComunicacion.Estado;
                        }
                        return vMedioComunicacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza medio de comunicación editado.
        /// </summary>
        /// <param name="pMedioComunicacion">The p medio de comunicación con los datos modificados.</param>
        /// <returns>Retorna 1 si el nombre se encuentra duplicado y 0 si se actualizó correctamente</returns>
        /// <exception cref="GenericException"></exception>
        public int ActualizarMedioComunicacion(MedioComunicacion pMedioComunicacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_MedioComunicacion_ActualizarMedioComunicacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdMedioComunicacion", DbType.Int32, pMedioComunicacion.IdMedioComunicacion);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pMedioComunicacion.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pMedioComunicacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pMedioComunicacion.UsuarioModifica);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int existeMedioComunicacion = 0;
                        while (vDataReaderResults.Read())
                        {
                            existeMedioComunicacion = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Count"].ToString()) : existeMedioComunicacion;
                            existeMedioComunicacion = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Count"].ToString()) : existeMedioComunicacion;
                            if (existeMedioComunicacion == 0)
                            {
                                Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pMedioComunicacion;
                                vAuditoria.ProgramaGeneraLog = true;
                                vAuditoria.Operacion = "UPDATE";
                                this.GenerarLogAuditoria(pMedioComunicacion, vDbCommand);
                            }
                        }
                        return existeMedioComunicacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
