﻿
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class EntidadEmisoraDAL : GeneralDAL
    {
        /// <summary>
        /// consulta los EntidadEmisora
        /// </summary>
        /// <returns>LISTA DE EntidadEmisora</returns>
        public List<EntidadEmisora> ConsultarEntidadEmisora()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Tercero_ConsultarEntidadEmisora"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EntidadEmisora> vListEntidadEmisora = new List<EntidadEmisora>();
                        while (vDataReaderResults.Read())
                        {
                            EntidadEmisora vEntidadEmisora = new EntidadEmisora();
                            vEntidadEmisora.IdEntidadEmisora = vDataReaderResults["IdEntidadEmisora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdEntidadEmisora"].ToString()) : vEntidadEmisora.IdEntidadEmisora;
                            vEntidadEmisora.NombreEntidadEmisora = vDataReaderResults["NombreEntidadEmisora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadEmisora"].ToString()) : vEntidadEmisora.NombreEntidadEmisora;
                            vListEntidadEmisora.Add(vEntidadEmisora);
                            vEntidadEmisora = null;
                        }

                        return vListEntidadEmisora;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
