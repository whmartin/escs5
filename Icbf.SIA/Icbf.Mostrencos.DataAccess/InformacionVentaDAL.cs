﻿//-----------------------------------------------------------------------
// <copyright file="InformacionVentaDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase InformacionVentaDAL.</summary>
// <author>Ingenian Software</author>
// <date>02/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo InformacionVenta.
    /// </summary>
    public class InformacionVentaDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InformacionVentaDAL"/> class.
        /// </summary>
        public InformacionVentaDAL()
        {
        }

        /// <summary>
        /// Método para consultar la InformacionVenta por Id
        /// </summary>
        /// <param name="pIdInformacionVenta">Id a consultar</param>
        /// <returns>Resultado de la consulta</returns>
        public InformacionVenta ConsultarInformacionVentaId(int pIdInformacionVenta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_InformacionVenta_ConsultaEntidad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdInformacionVenta", DbType.Int32, pIdInformacionVenta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        InformacionVenta vInformacionVenta = new InformacionVenta();
                        while (vDataReaderResults.Read())
                        {
                            vInformacionVenta.IdInformacionVenta = vDataReaderResults["IdInformacionVenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionVenta"].ToString()) : vInformacionVenta.IdInformacionVenta;
                            vInformacionVenta.IdTipoDocumentoVenta = vDataReaderResults["IdTipoDocumentoVenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoVenta"].ToString()) : vInformacionVenta.IdTipoDocumentoVenta;
                            vInformacionVenta.NumeroDocumentoVenta = vDataReaderResults["NumeroDocumentoVenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroDocumentoVenta"].ToString()) : vInformacionVenta.NumeroDocumentoVenta;
                            vInformacionVenta.ValorVenta = vDataReaderResults["ValorVenta"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorVenta"].ToString()) : vInformacionVenta.ValorVenta;
                            vInformacionVenta.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vInformacionVenta.FechaVenta;
                            vInformacionVenta.FechaSolicitudAvaluo = vDataReaderResults["FechaSolicitudAvaluo"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudAvaluo"].ToString()) : vInformacionVenta.FechaSolicitudAvaluo;
                            vInformacionVenta.FechaAvaluo = vDataReaderResults["FechaAvaluo"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAvaluo"].ToString()) : vInformacionVenta.FechaAvaluo;
                            vInformacionVenta.ValorAvaluo = vDataReaderResults["ValorAvaluo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAvaluo"].ToString()) : vInformacionVenta.ValorAvaluo;
                            vInformacionVenta.ProfesionalrealizaAvaluo = vDataReaderResults["ProfesionalrealizaAvaluo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProfesionalrealizaAvaluo"].ToString()) : vInformacionVenta.ProfesionalrealizaAvaluo;
                            vInformacionVenta.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vInformacionVenta.NombreTipoDocumento;
                        }

                        return vInformacionVenta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Insertar la información de la venta
        /// </summary>
        /// <param name="pInformacionVenta">Entidad a modificar</param>
        /// <returns>Resultado de la operación</returns>
        public int InsertarInformacionVenta(InformacionVenta pInformacionVenta)
        {
            int vResultado;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_InformacionVenta_Insertar"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumentoVenta", DbType.Int32, pInformacionVenta.NumeroDocumentoVenta);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoVenta", DbType.Int32, pInformacionVenta.IdTipoDocumentoVenta);
                    vDataBase.AddInParameter(vDbCommand, "@ValorVenta", DbType.Decimal, pInformacionVenta.ValorVenta);

                    if (pInformacionVenta.FechaVenta != Convert.ToDateTime("1/01/0001 00:00:00"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, pInformacionVenta.FechaVenta);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, null);
                    }

                    if (pInformacionVenta.FechaSolicitudAvaluo != Convert.ToDateTime("1/01/0001 00:00:00"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudAvaluo", DbType.DateTime, pInformacionVenta.FechaSolicitudAvaluo);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudAvaluo", DbType.DateTime, null);
                    }

                    if (pInformacionVenta.FechaAvaluo != Convert.ToDateTime("1/01/0001 00:00:00"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaAvaluo", DbType.DateTime, pInformacionVenta.FechaAvaluo);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaAvaluo", DbType.DateTime, null);
                    }

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInformacionVenta.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAvaluo", DbType.Decimal, pInformacionVenta.ValorAvaluo);
                    vDataBase.AddInParameter(vDbCommand, "@ProfesionalrealizaAvaluo", DbType.String, pInformacionVenta.ProfesionalrealizaAvaluo);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);

                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pInformacionVenta;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pInformacionVenta, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar la información de la venta
        /// </summary>
        /// <param name="pInformacionVenta">Entidad a modificar</param>
        /// <returns>Resultado de la operación</returns>
        public int ModificarInformacionVenta(InformacionVenta pInformacionVenta)
        {
            int vResultado;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_InformacionVenta_Modificar"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionVenta", DbType.Int32, pInformacionVenta.IdInformacionVenta);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumentoVenta", DbType.Int32, pInformacionVenta.NumeroDocumentoVenta);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoVenta", DbType.Int32, pInformacionVenta.IdTipoDocumentoVenta);
                    vDataBase.AddInParameter(vDbCommand, "@ValorVenta", DbType.Decimal, pInformacionVenta.ValorVenta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInformacionVenta.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAvaluo", DbType.Decimal, pInformacionVenta.ValorAvaluo);
                    vDataBase.AddInParameter(vDbCommand, "@ProfesionalrealizaAvaluo", DbType.String, pInformacionVenta.ProfesionalrealizaAvaluo);

                    if (pInformacionVenta.FechaVenta != Convert.ToDateTime("1/01/0001 00:00:00"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, pInformacionVenta.FechaVenta);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, null);
                    }

                    if (pInformacionVenta.FechaSolicitudAvaluo != Convert.ToDateTime("1/01/0001 00:00:00"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudAvaluo", DbType.DateTime, pInformacionVenta.FechaSolicitudAvaluo);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudAvaluo", DbType.DateTime, null);
                    }

                    if (pInformacionVenta.FechaAvaluo != Convert.ToDateTime("1/01/0001 00:00:00"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaAvaluo", DbType.DateTime, pInformacionVenta.FechaAvaluo);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaAvaluo", DbType.DateTime, null);
                    }

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);

                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pInformacionVenta;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pInformacionVenta, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
