﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.DataAccess
{
    public class RegistroCalidadDenuncianteDAL : GeneralDAL
    {
        public RegistroCalidadDenuncianteDAL()
        {
        }

        /// <summary>
        /// Método para Insertar RegistroDenuncia.
        /// </summary>
        /// <param name="vregistroCalidadDenunciante">Entidad a Insertar</param>
        /// <returns>resultado de la operación</returns>
        public int InsertarCalidadDenunciante(RegistroCalidadDenunciante vregistroCalidadDenunciante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarCalidadDenunciante_Insertar"))
                {
                    int vResultado, IdReconocimientoCalidadDenunciante;
                    vDataBase.AddOutParameter(vDbCommand, "@IdReconocimientoCalidadDenunciante", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vregistroCalidadDenunciante.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@ReconocimientoCalidad", DbType.String, vregistroCalidadDenunciante.ReconocimientoCalidad);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroResolucion", DbType.Int32, vregistroCalidadDenunciante.NumeroResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaResolucion", DbType.DateTime, vregistroCalidadDenunciante.FechaResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, vregistroCalidadDenunciante.Observaciones);

                    if (vregistroCalidadDenunciante.ReconocimientoCalidad == "No Reconoce")
                        vDataBase.AddInParameter(vDbCommand, "@NombreDetalle", DbType.String, vregistroCalidadDenunciante.NombreDetalle);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vregistroCalidadDenunciante.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    IdReconocimientoCalidadDenunciante = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdReconocimientoCalidadDenunciante").ToString());

                    if (vResultado > 0)
                    {
                        //vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdApoderado"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vregistroCalidadDenunciante;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(vAuditoria, vDbCommand);
                    }

                    return IdReconocimientoCalidadDenunciante;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Insertar RegistroDenuncia.
        /// </summary>
        /// <param name="vregistroCalidadDenunciante">Entidad a Insertar</param>
        /// <returns>resultado de la operación</returns>
        public int ModificarCalidadDenunciante(RegistroCalidadDenunciante vregistroCalidadDenunciante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarCalidadDenunciante_Modificar"))
                {
                    int vResultado, vIdRegistro;
                    vDataBase.AddOutParameter(vDbCommand, "@IdResolucion", DbType.Int32, vregistroCalidadDenunciante.IdRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@IdReconocimientoCalidadDenunciante", DbType.Int32, vregistroCalidadDenunciante.IdReconocimientoCalidadDenunciante);
                    vDataBase.AddInParameter(vDbCommand, "@ReconocimientoCalidad", DbType.String, vregistroCalidadDenunciante.ReconocimientoCalidad);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroResolucion", DbType.Int32, vregistroCalidadDenunciante.NumeroResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaResolucion", DbType.DateTime, vregistroCalidadDenunciante.FechaResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, vregistroCalidadDenunciante.Observaciones);

                    if (vregistroCalidadDenunciante.ReconocimientoCalidad == "No Reconoce")
                        vDataBase.AddInParameter(vDbCommand, "@NombreDetalle", DbType.String, vregistroCalidadDenunciante.NombreDetalle);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, vregistroCalidadDenunciante.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    vIdRegistro = (vDataBase.GetParameterValue(vDbCommand, "IdResolucion").ToString() != string.Empty) ? Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "IdResolucion").ToString()) : 0;


                    if (vIdRegistro > 0)
                    {
                        //vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdApoderado"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vregistroCalidadDenunciante;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        vAuditoria.IdRegistro = vIdRegistro;
                        this.GenerarLogAuditoria(vregistroCalidadDenunciante, vDbCommand);
                    }

                    return vIdRegistro;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroCalidadDenunciante> ConsultarCalidadDenunciante(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarCalidadDenunciante_ConsultarCalidad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroCalidadDenunciante> vListaCalidad = new List<RegistroCalidadDenunciante>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroCalidadDenunciante vRegistroCalidad = new RegistroCalidadDenunciante();
                            vRegistroCalidad.ReconocimientoCalidad = vDataReaderResults["ReconocimientoCalidadDenunciante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ReconocimientoCalidadDenunciante"].ToString()) : vRegistroCalidad.ReconocimientoCalidad;
                            vRegistroCalidad.NumeroResolucion = vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroResolucion"].ToString()) : vRegistroCalidad.NumeroResolucion;
                            vRegistroCalidad.FechaResolucion = vDataReaderResults["FechaResolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaResolucion"].ToString()) : vRegistroCalidad.FechaResolucion;
                            vRegistroCalidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vRegistroCalidad.Observaciones;
                            vRegistroCalidad.IdReconocimientoCalidadDenunciante = vDataReaderResults["IdReconocimientoCalidadDenunciante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReconocimientoCalidadDenunciante"].ToString()) : vRegistroCalidad.IdReconocimientoCalidadDenunciante;
                            vRegistroCalidad.NombreDetalle = vDataReaderResults["NombreDetalle"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDetalle"].ToString()) : vRegistroCalidad.NombreDetalle;
                            vListaCalidad.Add(vRegistroCalidad);
                        }
                        return vListaCalidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DenunciaDatosEntidad> BuscarDatosEntidadXNombre(string pNombreRazonSocial, int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Consultar_NombreEntidad"))
                {
                    //vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEntidad", DbType.String, pNombreRazonSocial);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DenunciaDatosEntidad> vlstDenunciaBienEntidad = new List<DenunciaDatosEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            DenunciaDatosEntidad vDenunciaBienEntidad = new DenunciaDatosEntidad();
                            vDenunciaBienEntidad.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDenunciaBienEntidad.IdDenunciaBien;
                            vDenunciaBienEntidad.NombreEntidad = vDataReaderResults["NombreEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidad"].ToString()) : vDenunciaBienEntidad.NombreEntidad;
                            vDenunciaBienEntidad.Departamento = vDataReaderResults["Departamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Departamento"].ToString()) : vDenunciaBienEntidad.Departamento;
                            vDenunciaBienEntidad.Municipio = vDataReaderResults["Municipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Municipio"].ToString()) : vDenunciaBienEntidad.Municipio;
                            vDenunciaBienEntidad.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vDenunciaBienEntidad.Direccion;
                            vlstDenunciaBienEntidad.Add(vDenunciaBienEntidad);
                        }
                        return vlstDenunciaBienEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}