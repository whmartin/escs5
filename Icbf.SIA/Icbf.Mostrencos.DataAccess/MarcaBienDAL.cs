﻿
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class MarcaBienDAL : GeneralDAL
    {
        private Database vDataBase;

        public MarcaBienDAL()
        {
            this.vDataBase = DatabaseFactory.CreateDatabase("DataBaseSEVEN");
        }

        /// <summary>
        /// consulta los MarcaBien
        /// </summary>
        /// <returns>LISTA DE MarcaBien</returns>
        public List<MarcaBien> ConsultarMarcaBien()
        {
            try
            {
                using (DbConnection vDbConnection = vDataBase.CreateConnection())
                {
                    vDbConnection.Open();
                    DbCommand vDbCommand = vDbConnection.CreateCommand();
                    vDbCommand.CommandText = "SELECT [ITE_CODI] ,[ITE_NOMB] FROM [produccion].[dbo].[GN_VITEM] WHERE [ITE_ACTI] = N's' and [TIT_CONT] = 177";

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<MarcaBien> vListMarcaBien = new List<MarcaBien>();
                        while (vDataReaderResults.Read())
                        {
                            MarcaBien vMarcaBien = new MarcaBien();
                            vMarcaBien.IdMarcaBien = vDataReaderResults["ITE_CODI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_CODI"].ToString()) : vMarcaBien.IdMarcaBien;
                            vMarcaBien.NombreMarcaBien = vDataReaderResults["ITE_NOMB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_NOMB"].ToString().ToUpper()) : vMarcaBien.NombreMarcaBien;
                            vListMarcaBien.Add(vMarcaBien);
                            vMarcaBien = null;
                        }

                        return vListMarcaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
