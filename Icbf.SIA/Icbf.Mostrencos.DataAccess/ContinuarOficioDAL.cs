﻿using System;
using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Collections.Generic;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase que permite el enlace entre el DAL y la BD 
    /// </summary>
    /// <seealso cref="Icbf.Mostrencos.DataAccess.GeneralDAL" />
    public class ContinuarOficioDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContinuarOficio"/> class.
        /// </summary>
        public ContinuarOficioDAL()
        {
        }

        public List<ContinuarOficio> ConsultarContinuarOficios(int? pIdDenunciaBien, int? pIdContinuarOficio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ContinuarOficio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdContinuarOficio", DbType.Int32, pIdContinuarOficio);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContinuarOficio> vLstContinuarOficio = new List<ContinuarOficio>();

                        while (vDataReaderResults.Read())
                        {
                            ContinuarOficio vContinuarOficio = new ContinuarOficio();
                            vContinuarOficio.IdContinuarOficio = ObtenerInt(vDataReaderResults["IdContinuarOficio"]);
                            vContinuarOficio.IdDenunciaBien = ObtenerInt(vDataReaderResults["IdDenunciaBien"]);
                            vContinuarOficio.NumeroResolucion = (vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroResolucion"].ToString()) : 0);
                            vContinuarOficio.Observaciones = ObtenerString(vDataReaderResults["Observaciones"]);
                            vContinuarOficio.IdMotivoContinuarOficio = ObtenerInt(vDataReaderResults["IdMotivoContinuarOficio"]);
                            vContinuarOficio.NombreMotivoContinuarOficio = ObtenerString(vDataReaderResults["NombreMotivoContinuarOficio"]);
                            vLstContinuarOficio.Add(vContinuarOficio);
                        }
                        return vLstContinuarOficio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarContinuarOficio(ContinuarOficio pContinuarOficio)
        {
            if (pContinuarOficio.NumeroResolucion == 0 && pContinuarOficio.Observaciones.Trim().Equals(string.Empty))
                throw new GenericException("Error en parametros de entrada");
            else
                try
                {
                    Database vDataBase = ObtenerInstancia();
                    using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ContinuarOficio_Insertar"))
                    {
                        int vResultado;
                        vDataBase.AddOutParameter(vDbCommand, "@IdContinuarOficio", DbType.Int32, pContinuarOficio.IdMotivoContinuarOficio);
                        vDataBase.AddInParameter(vDbCommand, "@IdMotivoContinuarOficio", DbType.Int32, pContinuarOficio.IdMotivoContinuarOficio);
                        vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pContinuarOficio.IdDenunciaBien);
                        vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pContinuarOficio.Observaciones);
                        vDataBase.AddInParameter(vDbCommand, "@NumeroResolucion", DbType.Decimal, pContinuarOficio.NumeroResolucion);
                        vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, pContinuarOficio.UsuarioCrea);
                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                        if (vResultado > 0)
                        {
                            vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContinuarOficio"));
                            Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Seguridad.Entity.EntityAuditoria)pContinuarOficio;
                            vAuditoria.ProgramaGeneraLog = true;
                            vAuditoria.Operacion = "INSERT";
                            GenerarLogAuditoria(pContinuarOficio, vDbCommand);
                        }
                        return vResultado;
                    }
                }
                catch (UserInterfaceException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new GenericException(ex);
                }
        }

        public int ModificarContinuarOficio(ContinuarOficio pContinuarOficio)
        {

            if (pContinuarOficio.NumeroResolucion == 0 && pContinuarOficio.Observaciones.Trim().Equals(string.Empty))
                throw new GenericException("Error en parametros de entrada");
            else
                try
                {
                    Database vDataBase = ObtenerInstancia();
                    using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ContinuarOficio_Modificar"))
                    {
                        int vResultado;
                        vDataBase.AddInParameter(vDbCommand, "@IdContinuarOficio", DbType.Int32, pContinuarOficio.IdContinuarOficio);
                        vDataBase.AddInParameter(vDbCommand, "@IdMotivoContinuarOficio", DbType.Int32, pContinuarOficio.IdMotivoContinuarOficio);
                        vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pContinuarOficio.IdDenunciaBien);
                        vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pContinuarOficio.Observaciones);
                        vDataBase.AddInParameter(vDbCommand, "@NumeroResolucion", DbType.Decimal, pContinuarOficio.NumeroResolucion);
                        vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, pContinuarOficio.UsuarioCrea);
                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                        if (vResultado > 0)
                        {
                            vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContinuarOficio"));
                            Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Seguridad.Entity.EntityAuditoria)pContinuarOficio;
                            vAuditoria.ProgramaGeneraLog = true;
                            vAuditoria.Operacion = "UPDATE";
                            GenerarLogAuditoria(pContinuarOficio, vDbCommand);
                        }
                        return vResultado;
                    }
                }
                catch (UserInterfaceException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new GenericException(ex);
                }
        }

        private String ObtenerString(object entrada)
        {
            return (entrada != DBNull.Value ? Convert.ToString(entrada.ToString()) : "");
        }

        private Int32 ObtenerInt(object entrada)
        {
            return (entrada != DBNull.Value ? Convert.ToInt32(entrada.ToString()) : 0);
        }
    }
}
