﻿

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class CausantesDAL : GeneralDAL
    {
        /// <summary>
        /// Inserta la información de un causante
        /// </summary>
        /// <param name="pCausante">Variable de tipo causante que contiene la información que se va a insertar</param>
        /// <returns>Id del causante insertado</returns>
        public int InsertarCausante(Causantes pCausante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Causantes_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCausante", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int64, pCausante.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pCausante.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistroDefuncion", DbType.String, pCausante.NumeroRegistroDefuncion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaDefuncion", DbType.DateTime, pCausante.FechaDefuncion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCausante.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pCausante.IdUsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCausante"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pCausante;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pCausante, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de un causante por el id causante
        /// </summary>
        /// <param name="pIdCaudante">Id causante que se va a consultar</param>
        /// <returns>Variable de tipo causante que contiene los datos de la consulta</returns>
        public Causantes ConsultarCausantesPorIdCaudante(int pIdCaudante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Causantes_ConsultarPorIdCausante"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCausante", DbType.Int32, pIdCaudante);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Causantes vCausante = new Causantes();
                        while (vDataReaderResults.Read())
                        {
                            vCausante.IdCausante = vDataReaderResults["IdCausante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCausante"].ToString()) : vCausante.IdCausante;
                            vCausante.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"]) : vCausante.IdTercero;
                            vCausante.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vCausante.IdDenunciaBien;
                            vCausante.NumeroRegistroDefuncion = vDataReaderResults["NumeroRegistroDefuncion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRegistroDefuncion"].ToString()) : vCausante.NumeroRegistroDefuncion;
                            vCausante.FechaDefuncion = vDataReaderResults["FechaDefuncion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDefuncion"].ToString()) : vCausante.FechaDefuncion;
                            vCausante.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCausante.UsuarioCrea;
                            vCausante.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCausante.FechaCrea;
                            vCausante.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCausante.UsuarioModifica;
                            vCausante.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCausante.FechaModifica;
                        }

                        return vCausante;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de un causante por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo Causantes con la información resultante de la consulta</returns>
        public List<Causantes> ConsultarCausantesPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Causantes_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Causantes> vListaCausantes = new List<Causantes>();
                        while (vDataReaderResults.Read())
                        {
                            Causantes vCausante = new Causantes();
                            vCausante.IdCausante = vDataReaderResults["IdCausante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCausante"].ToString()) : vCausante.IdCausante;
                            vCausante.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"]) : vCausante.IdTercero;
                            vCausante.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vCausante.IdDenunciaBien;
                            vCausante.NumeroRegistroDefuncion = vDataReaderResults["NumeroRegistroDefuncion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroRegistroDefuncion"].ToString()) : vCausante.NumeroRegistroDefuncion;
                            vCausante.FechaDefuncion = vDataReaderResults["FechaDefuncion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDefuncion"].ToString()) : vCausante.FechaDefuncion;
                            vCausante.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCausante.UsuarioCrea;
                            vCausante.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCausante.FechaCrea;
                            vCausante.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCausante.UsuarioModifica;
                            vCausante.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCausante.FechaModifica;
                            vListaCausantes.Add(vCausante);
                            vCausante = null;
                        }

                        return vListaCausantes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un causante
        /// </summary>
        /// <param name="pCausante">Variable de tipo causante que contiene la información que se va a insertar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarCausante(Causantes pCausante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Causantes_Editar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCausante", DbType.Int32, pCausante.IdCausante);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistroDefuncion", DbType.String, pCausante.NumeroRegistroDefuncion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaDefuncion", DbType.DateTime, pCausante.FechaDefuncion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCausante.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pCausante;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pCausante, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Valida si ya existe un Causante con el mismo id tercero y id denuncia bien
        /// </summary>
        /// <param name="pIdTercero">Id del tercero</param>
        /// <param name="pIdDenunciaBien"> id de la denuncia bien</param>
        /// <returns>true si existe o false si no existe</returns>
        public bool ValidarDuplicidad(int pIdTercero, int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Causantes_ValidarDuplicidad"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vResultado = vDataReaderResults["IdCausante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCausante"].ToString()) : 0;
                        }
                    }
                    return vResultado > 0;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
