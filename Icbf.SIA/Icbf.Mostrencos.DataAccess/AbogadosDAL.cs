﻿

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;    

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class AbogadosDAL : GeneralDAL
    {
        /// <summary>
        /// consulta los abogados por el IdAbogado
        /// </summary>
        /// <param name="pIdAbogado">Id del abogado a consultar</param>
        /// <returns>Lista de tipo Abogados con el resultado de la consulta</returns>
        public Abogados ConsultarAbogadoXId(int pIdAbogado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Abogados_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAbogado", DbType.Int32, pIdAbogado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Abogados vAbogados = new Abogados();
                        while (vDataReaderResults.Read())
                        {
                            vAbogados.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAbogados.Estado;
                            vAbogados.FechaAsignacionAbogado = vDataReaderResults["FechaAsignacionAbogado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacionAbogado"]) : vAbogados.FechaAsignacionAbogado;
                            vAbogados.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Identificacion"].ToString()) : vAbogados.Identificacion;
                            vAbogados.IdRegionalAbogado = vDataReaderResults["IdRegionalAbogado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalAbogado"].ToString()) : vAbogados.IdRegionalAbogado;
                            vAbogados.NombreAbogado = vDataReaderResults["NombreAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbogado"].ToString()) : vAbogados.NombreAbogado;
                            vAbogados.NombreRegionalAbogado = vDataReaderResults["NombreRegionalAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegionalAbogado"]) : vAbogados.NombreRegionalAbogado;
                            vAbogados.TipoFuncionarioAbogado = vDataReaderResults["TipoFuncionarioAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuncionarioAbogado"].ToString()) : vAbogados.TipoFuncionarioAbogado;
                            vAbogados.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vAbogados.IdUsuario;                            
                            vAbogados.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAbogados.UsuarioCrea;
                            vAbogados.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAbogados.FechaCrea;
                            vAbogados.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAbogados.UsuarioModifica;
                            vAbogados.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAbogados.FechaModifica;
                        }

                        vAbogados.IdAbogado = pIdAbogado;
                        return vAbogados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información del abogado por el id de la regional a la que esta asignado
        /// </summary>
        /// <param name="pIdRegional">id de la regional que se va a consultar</param>
        /// <returns>Lista de tipo Abogados con la información consultada</returns>
        public List<Abogados> ConsultarAbogadoXIdRegional(int pIdRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Abogados_ConsultarPorIdRegionalAbogado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalAbogado", DbType.Int32, pIdRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Abogados> vListaAbogados = new List<Abogados>();
                        while (vDataReaderResults.Read())
                        {
                            Abogados vAbogados = new Abogados();
                            vAbogados.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAbogados.Estado;
                            vAbogados.FechaAsignacionAbogado = vDataReaderResults["FechaAsignacionAbogado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacionAbogado"]) : vAbogados.FechaAsignacionAbogado;
                            vAbogados.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Identificacion"].ToString()) : vAbogados.Identificacion;
                            vAbogados.IdRegionalAbogado = pIdRegional;
                            vAbogados.NombreAbogado = vDataReaderResults["NombreAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbogado"].ToString()) : vAbogados.NombreAbogado;
                            vAbogados.NombreRegionalAbogado = vDataReaderResults["NombreRegionalAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegionalAbogado"]) : vAbogados.NombreRegionalAbogado;
                            vAbogados.TipoFuncionarioAbogado = vDataReaderResults["TipoFuncionarioAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuncionarioAbogado"].ToString()) : vAbogados.TipoFuncionarioAbogado;
                            vAbogados.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vAbogados.IdUsuario;
                            vAbogados.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAbogados.UsuarioCrea;
                            vAbogados.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAbogados.FechaCrea;
                            vAbogados.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAbogados.UsuarioModifica;
                            vAbogados.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAbogados.FechaModifica;
                            vAbogados.IdAbogado = vDataReaderResults["IdAbogado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAbogado"].ToString()) : vAbogados.IdAbogado;
                            vListaAbogados.Add(vAbogados);
                            vAbogados = null;
                        }
                        
                        return vListaAbogados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información del abogado por el IdUsuario
        /// </summary>
        /// <param name="pIdUsuario">Id usuario que se va a consultar</param>
        /// <returns>Variable de tipo Abogados con la información consultada</returns>
        public Abogados ConsultarAbogadosPorIdUsuario(int pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Abogados_ConsultarPorIdUsuario"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Abogados vAbogados = new Abogados();
                        while (vDataReaderResults.Read())
                        {
                            vAbogados.IdAbogado = vDataReaderResults["IdAbogado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAbogado"].ToString()) : vAbogados.IdAbogado;
                            vAbogados.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vAbogados.Estado;
                            vAbogados.FechaAsignacionAbogado = vDataReaderResults["FechaAsignacionAbogado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacionAbogado"]) : vAbogados.FechaAsignacionAbogado;
                            vAbogados.Identificacion = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Identificacion"].ToString()) : vAbogados.Identificacion;
                            vAbogados.IdRegionalAbogado = vDataReaderResults["IdRegionalAbogado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalAbogado"].ToString()) : vAbogados.IdRegionalAbogado;
                            vAbogados.NombreAbogado = vDataReaderResults["NombreAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbogado"].ToString()) : vAbogados.NombreAbogado;
                            vAbogados.NombreRegionalAbogado = vDataReaderResults["NombreRegionalAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegionalAbogado"]) : vAbogados.NombreRegionalAbogado;
                            vAbogados.TipoFuncionarioAbogado = vDataReaderResults["TipoFuncionarioAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuncionarioAbogado"].ToString()) : vAbogados.TipoFuncionarioAbogado;
                            vAbogados.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vAbogados.IdUsuario;
                            vAbogados.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAbogados.UsuarioCrea;
                            vAbogados.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAbogados.FechaCrea;
                            vAbogados.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAbogados.UsuarioModifica;
                            vAbogados.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAbogados.FechaModifica;
                        }
                        
                        return vAbogados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo abogado
        /// </summary>
        /// <param name="pAbogados">Variable de tipoAbogados que contiene la información que se va a insertar</param>
        /// <returns> id del abogado insertado</returns>
        public int InsetrarAbogados(Abogados pAbogados)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Abogados_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAbogado", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int64, pAbogados.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreAbogado", DbType.String, pAbogados.NombreAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalAbogado", DbType.Int32, pAbogados.IdRegionalAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRegionalAbogado", DbType.String, pAbogados.NombreRegionalAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAsignacionAbogado", DbType.DateTime, pAbogados.FechaAsignacionAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@TipoFuncionarioAbogado", DbType.String, pAbogados.TipoFuncionarioAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAbogados.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pAbogados.IdUsuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAbogado"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pAbogados;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pAbogados, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los abogados de la base de datos SIA
        /// </summary>
        /// <param name="pConsulta">variable con la información a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public List<AsignacionAbogado> ConsultarAbogadosSoloEnSIA(AsignacionAbogado pConsulta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Abogados_ConsultarSoloEnSia"))
                {
                    if (pConsulta.NombreAbogado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pConsulta.NombreAbogado);
                    if (pConsulta.IdRegionalAbogado != 0)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int16, pConsulta.IdRegionalAbogado);
                    if (pConsulta.Profesion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Profesion", DbType.String, pConsulta.Profesion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AsignacionAbogado> vListaAsignacionAbogado = new List<AsignacionAbogado>();
                        while (vDataReaderResults.Read())
                        {
                            AsignacionAbogado vAsignacionAbogado = new AsignacionAbogado();
                            vAsignacionAbogado.NombreAbogado = vDataReaderResults["Abogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Abogado"].ToString()) : vAsignacionAbogado.NombreAbogado;
                            vAsignacionAbogado.Identificacion = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vAsignacionAbogado.Identificacion;
                            vAsignacionAbogado.Profesion = vDataReaderResults["Profesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Profesion"].ToString()) : vAsignacionAbogado.Profesion;
                            vAsignacionAbogado.TipoFuncionario = vDataReaderResults["TipoFuncionario"] != DBNull.Value ? vDataReaderResults["TipoFuncionario"].ToString() : vAsignacionAbogado.TipoFuncionario;
                            vAsignacionAbogado.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vAsignacionAbogado.IdUsuario;
                            vAsignacionAbogado.IdRegionalAbogado = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vAsignacionAbogado.IdRegionalAbogado;
                            vAsignacionAbogado.NombreRegionalAbogado = vDataReaderResults["NombreRegional"] != DBNull.Value ? vDataReaderResults["NombreRegional"].ToString() : vAsignacionAbogado.NombreRegionalAbogado;
                            vAsignacionAbogado.Correo = vDataReaderResults["Correo"] != DBNull.Value ? vDataReaderResults["Correo"].ToString() : vAsignacionAbogado.Correo;
                            vListaAsignacionAbogado.Add(vAsignacionAbogado);
                        }

                        return vListaAsignacionAbogado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
