﻿//-----------------------------------------------------------------------
// <copyright file="TramiteNotarialDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase filtrosMostrencosDAL.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System.Collections.Generic;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class TramiteNotarialDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta ObtenerNotarias
        /// </summary>
        /// <returns>List tipo TramiteNotarial con la información consultada</returns>
        public List<TramiteNotarial> ObtenerNotarias()
        {
            try
            {
                Database vDataBase = ObtenerInstanciaSAC();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand(""))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TramiteNotarial> vTramiteNotarialList = new List<TramiteNotarial>();
                        while (vDataReaderResults.Read())
                        {
                            TramiteNotarial vTramiteNotarial = new TramiteNotarial();
                            //vTramiteNotarial.IdNotaria = vDataReaderResults["IdNotaria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNotaria"].ToString()) : vTramiteNotarial.IdNotaria;
                            //vTramiteNotarial.NombreNotaria = vDataReaderResults["NombreNotaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreNotaria"].ToString()) : string.Empty;
                            //vTramiteNotarial.DepartamentoNotaria = vDataReaderResults["DepartamentoNotaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DepartamentoNotaria"].ToString()) : string.Empty;
                            //vTramiteNotarial.MunicipioNotaria = vDataReaderResults["MunicipioNotaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MunicipioNotaria"].ToString()) : string.Empty;

                            vTramiteNotarialList.Add(vTramiteNotarial);
                        }
                        return vTramiteNotarialList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Notarial
        /// </summary>
        /// <param name="vTramiteNotarial">Variable de tipo TramiteNotarial que contiene la información que se va a insertar</param>
        /// <returns> id del TramiteNotarial insertado</returns>
        public int InsertarTramiteNotarial(TramiteNotarial vTramiteNotarial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TramiteNotarial_Insertar"))
                {

                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTramiteNotarial", DbType.Int32, 1);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vTramiteNotarial.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdNotaria", DbType.String, vTramiteNotarial.IdNotaria);
                    vDataBase.AddInParameter(vDbCommand, "@Notaria", DbType.String, vTramiteNotarial.Notaria);
                    vDataBase.AddInParameter(vDbCommand, "@DecisionTramiteNotarial", DbType.String, vTramiteNotarial.DecisionTramiteNotarial);
                    vDataBase.AddInParameter(vDbCommand, "@DecisionTramiteNotarialRechazado", DbType.String, vTramiteNotarial.DecisionTramiteNotarialRechazado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaActaTramiteNotarial", DbType.DateTime, vTramiteNotarial.FechaActaTramiteNotarial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIngresoICBF", DbType.DateTime, vTramiteNotarial.FechaIngresoICBF);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInstrumentosPublicos", DbType.DateTime, vTramiteNotarial.FechaInstrumentosPublicos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vTramiteNotarial.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@TipoTramite", DbType.String, vTramiteNotarial.TipoTramite);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTramiteNotarial"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vTramiteNotarial;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(vTramiteNotarial, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Notarial
        /// </summary>
        /// <param name="vTramiteNotarial">Variable de tipo TramiteNotarial que contiene la información que se va a insertar</param>
        /// <returns> id del TramiteNotarial insertado</returns>
        public int InsertarTramiteNotarialAceptado(TramiteNotarialAceptado vTramiteNotarialAceptado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TramiteNotarialAceptado_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@TramiteNotarialAceptado", DbType.Int32, 1);
                    vDataBase.AddInParameter(vDbCommand, "@IdTramiteNotarial", DbType.Int32, vTramiteNotarialAceptado.IdTramiteNotarial);
                    vDataBase.AddInParameter(vDbCommand, "@IdTerminacionAnormalTramiteNotarial", DbType.Int32, vTramiteNotarialAceptado.IdTerminacionAnormalTramiteNotarial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEdictoEmplazatorio", DbType.DateTime, vTramiteNotarialAceptado.FechaEdictoEmplazatorio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaComunicacionSecretariaHacienda", DbType.DateTime, vTramiteNotarialAceptado.FechaComunicacionScretariaHacienda);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolemnizacion", DbType.DateTime, vTramiteNotarialAceptado.FechaSolemnizacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacionEdicto", DbType.DateTime, vTramiteNotarialAceptado.FechaPublicacionEdicto);
                    vDataBase.AddInParameter(vDbCommand, "@FechaComunicacionDian", DbType.DateTime, vTramiteNotarialAceptado.FechaComunicacionDian);
                    vDataBase.AddInParameter(vDbCommand, "@TerminacionAnormalTramiteNotarial", DbType.String, vTramiteNotarialAceptado.TerminacionAnormalTramiteNotarial);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vTramiteNotarialAceptado.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@TramiteNotarialAceptado"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vTramiteNotarialAceptado;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(vTramiteNotarialAceptado, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Notarial
        /// </summary>
        /// <param name="vTramiteNotarial">Variable de tipo TramiteNotarial que contiene la información que se va a insertar</param>
        /// <returns> id del TramiteNotarial insertado</returns>
        public bool validarTramiteNotarial(int IdDenuncia, string Notaria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TramiteNotarial_ValidarDuplicidad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdNotaria", DbType.String, Notaria);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public TramiteNotarial ConsultarTramiteNotarial(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ConsultarTipoTramiteNotarial"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TramiteNotarial vTramiteNotarial = new TramiteNotarial();
                        while (vDataReaderResults.Read())
                        {
                            vTramiteNotarial.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vTramiteNotarial.IdDenunciaBien;
                            vTramiteNotarial.IdTramiteNotarial = vDataReaderResults["IdTramiteNotarial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTramiteNotarial"].ToString()) : vTramiteNotarial.IdTramiteNotarial;
                            vTramiteNotarial.TipoTramite = vDataReaderResults["TipoTramite"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoTramite"].ToString()) : vTramiteNotarial.TipoTramite;
                            vTramiteNotarial.DecisionTramiteNotarial = vDataReaderResults["DecisionTramiteNotarial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DecisionTramiteNotarial"].ToString()) : vTramiteNotarial.DecisionTramiteNotarial;
                            vTramiteNotarial.DecisionTramiteNotarialRechazado = vDataReaderResults["DecisionTramiteNotarialRechazado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DecisionTramiteNotarialRechazado"].ToString()) : vTramiteNotarial.DecisionTramiteNotarialRechazado;
                            vTramiteNotarial.IdNotaria = vDataReaderResults["IdNotaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdNotaria"].ToString()) : vTramiteNotarial.IdNotaria;
                            vTramiteNotarial.Notaria = vDataReaderResults["Notaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Notaria"].ToString()) : vTramiteNotarial.Notaria;
                            vTramiteNotarial.FechaActaTramiteNotarial = vDataReaderResults["FechaActaTramiteNotarial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaActaTramiteNotarial"].ToString()) : vTramiteNotarial.FechaActaTramiteNotarial;
                            vTramiteNotarial.FechaEscritura = vDataReaderResults["FechaEscritura"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEscritura"].ToString()) : vTramiteNotarial.FechaEscritura;
                            vTramiteNotarial.NumeroEscritura = vDataReaderResults["NumeroEscritura"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["NumeroEscritura"].ToString()) : vTramiteNotarial.NumeroEscritura;
                            vTramiteNotarial.NumeroRegistro = vDataReaderResults["NumeroRegistro"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["NumeroRegistro"].ToString()) : vTramiteNotarial.NumeroRegistro;
                            vTramiteNotarial.FechaIngresoICBF = vDataReaderResults["FechaIngresoICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIngresoICBF"].ToString()) : vTramiteNotarial.FechaIngresoICBF;
                            vTramiteNotarial.FechaInstrumentosPublicos = vDataReaderResults["FechaInstrumentosPublicos"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInstrumentosPublicos"].ToString()) : vTramiteNotarial.FechaInstrumentosPublicos;
                        }

                        return vTramiteNotarial;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public TramiteNotarialAceptado ConsultarTramiteNotarialAceptado(int pIdTramiteNotarial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ConsultarTipoTramiteNotarialAceptado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTramiteNotarial", DbType.Int32, pIdTramiteNotarial);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TramiteNotarialAceptado vTramiteNotarialAceptado = new TramiteNotarialAceptado();
                        while (vDataReaderResults.Read())
                        {
                            vTramiteNotarialAceptado.IdTramiteNotarialAceptado = vDataReaderResults["IdTramiteNotarialAceptado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTramiteNotarialAceptado"].ToString()) : vTramiteNotarialAceptado.IdTramiteNotarialAceptado;
                            vTramiteNotarialAceptado.IdTipoTramiteNotarial = vDataReaderResults["IdTramiteNotarial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTramiteNotarial"].ToString()) : vTramiteNotarialAceptado.IdTipoTramiteNotarial;
                            vTramiteNotarialAceptado.IdTerminacionAnormalTramiteNotarial = vDataReaderResults["IdTerminacionAnormalTramiteNotarial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTerminacionAnormalTramiteNotarial"].ToString()) : vTramiteNotarialAceptado.IdTerminacionAnormalTramiteNotarial;
                            vTramiteNotarialAceptado.FechaEdictoEmplazatorio = vDataReaderResults["FechaEdictoEmplazatorio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEdictoEmplazatorio"].ToString()) : vTramiteNotarialAceptado.FechaEdictoEmplazatorio;
                            vTramiteNotarialAceptado.FechaComunicacionScretariaHacienda = vDataReaderResults["FechaComunicacionSecretariaHacienda"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaComunicacionSecretariaHacienda"].ToString()) : vTramiteNotarialAceptado.FechaComunicacionScretariaHacienda;
                            vTramiteNotarialAceptado.FechaSolemnizacion = vDataReaderResults["FechaSolemnizacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolemnizacion"].ToString()) : vTramiteNotarialAceptado.FechaSolemnizacion;
                            vTramiteNotarialAceptado.FechaPublicacionEdicto = vDataReaderResults["FechaPublicacionEdicto"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacionEdicto"].ToString()) : vTramiteNotarialAceptado.FechaPublicacionEdicto;
                            vTramiteNotarialAceptado.FechaComunicacionDian = vDataReaderResults["FechaComunicacionDian"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaComunicacionDian"].ToString()) : vTramiteNotarialAceptado.FechaComunicacionDian;
                            vTramiteNotarialAceptado.TerminacionAnormalTramiteNotarial = vDataReaderResults["TerminacionAnormalTramiteNotarial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TerminacionAnormalTramiteNotarial"].ToString()) : vTramiteNotarialAceptado.TerminacionAnormalTramiteNotarial;
                        }

                        return vTramiteNotarialAceptado;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Notarial
        /// </summary>
        /// <param name="vTramiteNotarial">Variable de tipo TramiteNotarial que contiene la información que se va a insertar</param>
        /// <returns> id del TramiteNotarial insertado</returns>
        public int UpdateTramiteNotarial(TramiteNotarial vTramiteNotarial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TramiteNotarial_Update"))
                {

                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTramiteNotarial", DbType.Int32, vTramiteNotarial.IdTramiteNotarial);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vTramiteNotarial.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdNotaria", DbType.String, vTramiteNotarial.IdNotaria);
                    vDataBase.AddInParameter(vDbCommand, "@Notaria", DbType.String, vTramiteNotarial.Notaria);
                    vDataBase.AddInParameter(vDbCommand, "@DecisionTramiteNotarial", DbType.String, vTramiteNotarial.DecisionTramiteNotarial);
                    vDataBase.AddInParameter(vDbCommand, "@DecisionTramiteNotarialRechazado", DbType.String, vTramiteNotarial.DecisionTramiteNotarialRechazado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaActaTramiteNotarial", DbType.DateTime, vTramiteNotarial.FechaActaTramiteNotarial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIngresoICBF", DbType.DateTime, vTramiteNotarial.FechaIngresoICBF);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInstrumentosPublicos", DbType.DateTime, vTramiteNotarial.FechaInstrumentosPublicos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vTramiteNotarial.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@TipoTramite", DbType.String, vTramiteNotarial.TipoTramite);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTramiteNotarial"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vTramiteNotarial;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vTramiteNotarial, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

