﻿//-----------------------------------------------------------------------
// <copyright file="TramiteJudicialDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TramiteJudicialDAL.</summary>
// <author>INGENIAN</author>
// <date>19/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System.Collections.Generic;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class TramiteJudicialDAL : GeneralDAL
    {
        /// <summary>
        /// Inserta la información de un nuevo Tramite Judicial
        /// </summary>
        /// <param name="vTramiteJudicial">Variable de tipo TramiteJudicial que contiene la información que se va a insertar</param>
        /// <returns> id del vTramiteJudicial insertado</returns>
        public int InsertarTramiteJudicial(TramiteJudicial vTramiteJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TramiteJudicial_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTramiteJudicial", DbType.Int32, 1);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vTramiteJudicial.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdDespachoJudicial", DbType.Int32, vTramiteJudicial.IdDespachoJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@TipoTramite", DbType.String, vTramiteJudicial.TipoTramite);
                    vDataBase.AddInParameter(vDbCommand, "@TipoProceso", DbType.String, vTramiteJudicial.TipoProceso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRadicacion", DbType.DateTime, vTramiteJudicial.FechaRadicacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSentencia", DbType.DateTime, vTramiteJudicial.FechaSentencia);
                    vDataBase.AddInParameter(vDbCommand, "@DespachoJudicialAsignado", DbType.String, vTramiteJudicial.DespachoJudicialAsignado);
                    vDataBase.AddInParameter(vDbCommand, "@Sentencia", DbType.String, vTramiteJudicial.Sentencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vTramiteJudicial.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTramiteJudicial"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vTramiteJudicial;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(vTramiteJudicial, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Judicial
        /// </summary>
        /// <param name="vTramiteJudicial">Variable de tipo TramiteJudicial que contiene la información que se va a insertar</param>
        /// <returns> id del vTramiteJudicial insertado</returns>
        public int InsertarDesicionAuto(int IdDenunciaBien, int IdDesicion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_DecisionAuto_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdActualizado", DbType.Int32, 1);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdDesicion", DbType.Int32, IdDesicion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Judicial
        /// </summary>
        /// <param name="vTramiteJudicial">Variable de tipo TramiteJudicial que contiene la información que se va a insertar</param>
        /// <returns> id del vTramiteJudicial insertado</returns>
        public int InsertarTipoTramite(int IdDenunciaBien, int TipoTramite)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_Insertar_TipoTramite"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdActualizado", DbType.Int32, 1);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoTramite", DbType.Int32, TipoTramite);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Judicial
        /// </summary>
        /// <param name="vTramiteJudicial">Variable de tipo TramiteJudicial que contiene la información que se va a insertar</param>
        /// <returns> id del vTramiteJudicial insertado</returns>
        public int InsertarDesicionAutoInadmitida(int IdDenunciaBien, int idDecisionInad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DesicionInadmitida_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdActualizado", DbType.Int32, 1);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdDesicionInadmitida", DbType.Int32, idDecisionInad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Judicial
        /// </summary>
        /// <param name="vTramiteJudicial">Variable de tipo TramiteJudicial que contiene la información que se va a insertar</param>
        /// <returns> id del vTramiteJudicial insertado</returns>
        public int InsertarDesicionRecurso(int IdDenunciaBien, int IdRecurso, bool presenta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DesicionRecurso_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdActualizado", DbType.Int32, 1);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdDesicionRecurso", DbType.Int32, IdRecurso);
                    vDataBase.AddInParameter(vDbCommand, "@Presenta", DbType.Int32, presenta);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Notarial
        /// </summary>
        /// <param name="vTramiteNotarial">Variable de tipo TramiteNotarial que contiene la información que se va a insertar</param>
        /// <returns> id del TramiteNotarial insertado</returns>
        public bool validarTramiteJudicial(int IdDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TramiteJudicial_ValidarDuplicidad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenuncia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public TramiteJudicial ConsultarTramiteJudicial(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ConsultarTipoTramiteJudicial"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TramiteJudicial vTramiteJudicial = new TramiteJudicial();
                        while (vDataReaderResults.Read())
                        {
                            vTramiteJudicial.IdTramiteJudicial = vDataReaderResults["IdTramiteJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTramiteJudicial"].ToString()) : vTramiteJudicial.IdTramiteJudicial;
                            vTramiteJudicial.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vTramiteJudicial.IdDenunciaBien;
                            vTramiteJudicial.TipoTramite = vDataReaderResults["TipoTramite"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoTramite"].ToString()) : vTramiteJudicial.TipoTramite;
                            vTramiteJudicial.TipoProceso = vDataReaderResults["NombreSentidoSentencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSentidoSentencia"].ToString()) : vTramiteJudicial.TipoProceso;
                            vTramiteJudicial.IdDespachoJudicial = vDataReaderResults["IdDespachoJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDespachoJudicial"].ToString()) : vTramiteJudicial.IdDespachoJudicial;
                            vTramiteJudicial.IdSentidoSentencia = vDataReaderResults["IdSentidoSentencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSentidoSentencia"].ToString()) : vTramiteJudicial.IdSentidoSentencia;
                            vTramiteJudicial.FechaRadicacion = (vDataReaderResults["FechaRadicacion"] != DBNull.Value) ? Convert.ToDateTime(vDataReaderResults["FechaRadicacion"].ToString()) : vTramiteJudicial.FechaRadicacion;
                            vTramiteJudicial.DespachoJudicialAsignado = vDataReaderResults["DespachoJudicialAsignado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DespachoJudicialAsignado"].ToString()) : vTramiteJudicial.DespachoJudicialAsignado;
                            vTramiteJudicial.Sentencia = vDataReaderResults["Sentencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sentencia"].ToString()) : vTramiteJudicial.Sentencia;
                            vTramiteJudicial.FechaSentencia = (vDataReaderResults["FechaSentencia"] != DBNull.Value) ? Convert.ToDateTime(vDataReaderResults["FechaSentencia"].ToString()) : vTramiteJudicial.FechaSentencia;
                            vTramiteJudicial.FechaComunicacion = (vDataReaderResults["FechaComunicacion"] != DBNull.Value) ? Convert.ToDateTime(vDataReaderResults["FechaComunicacion"].ToString()) : vTramiteJudicial.FechaComunicacion;
                            vTramiteJudicial.SentidoSentencia = vDataReaderResults["SentidoSentencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SentidoSentencia"].ToString()) : vTramiteJudicial.SentidoSentencia;
                            vTramiteJudicial.FechaConstanciaEjecutoria = vDataReaderResults["FechaConstanciaEjecutoria"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaConstanciaEjecutoria"].ToString()) : vTramiteJudicial.FechaConstanciaEjecutoria;
                        }

                        return vTramiteJudicial;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public DesicionAuto consultarDesicionAutoId(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ConsultarDesicionAuto"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DesicionAuto vDesicionAuto = new DesicionAuto();
                        while (vDataReaderResults.Read())
                        {
                            vDesicionAuto.NombreDecisionAuto = vDataReaderResults["NombreDecisionAuto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDecisionAuto"].ToString()) : vDesicionAuto.NombreDecisionAuto;
                            //int a = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : 0;
                            vDesicionAuto.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vDesicionAuto.Estado;
                        }

                        return vDesicionAuto;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public DecisionAutoInadmitida consultarDecisionAutoInadmitidaId(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ConsultarDecisionAutoInadmitida"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DecisionAutoInadmitida vDecisionAutoInadmitida = new DecisionAutoInadmitida();
                        while (vDataReaderResults.Read())
                        {
                            vDecisionAutoInadmitida.NombreDecisionAutoInadmitida = vDataReaderResults["NombreDecisionAutoInadmitida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDecisionAutoInadmitida"].ToString()) : vDecisionAutoInadmitida.NombreDecisionAutoInadmitida;
                        }

                        return vDecisionAutoInadmitida;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public DecisionRecurso consultarDecisionRecursoId(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ConsultarDecisionRecurso"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DecisionRecurso vDecisionRecurso = new DecisionRecurso();
                        while (vDataReaderResults.Read())
                        {
                            vDecisionRecurso.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vDecisionRecurso.Nombre;
                        }

                        return vDecisionRecurso;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo Tramite Judicial
        /// </summary>
        /// <param name="vTramiteJudicial">Variable de tipo TramiteJudicial que contiene la información que se va a insertar</param>
        /// <returns> id del vTramiteJudicial insertado</returns>
        public int UpdateTramiteJudicial(TramiteJudicial vTramiteJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TramiteJudicial_UPDATE"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTramiteJudicial", DbType.Int32, vTramiteJudicial.IdTramiteJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vTramiteJudicial.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdDespachoJudicial", DbType.Int32, vTramiteJudicial.IdDespachoJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@TipoTramite", DbType.String, vTramiteJudicial.TipoTramite);
                    vDataBase.AddInParameter(vDbCommand, "@TipoProceso", DbType.String, vTramiteJudicial.TipoProceso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRadicacion", DbType.DateTime, vTramiteJudicial.FechaRadicacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSentencia", DbType.DateTime, vTramiteJudicial.FechaSentencia);
                    vDataBase.AddInParameter(vDbCommand, "@DespachoJudicialAsignado", DbType.String, vTramiteJudicial.DespachoJudicialAsignado);
                    vDataBase.AddInParameter(vDbCommand, "@Sentencia", DbType.String, vTramiteJudicial.Sentencia);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, vTramiteJudicial.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTramiteJudicial"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vTramiteJudicial;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vTramiteJudicial, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}

