﻿

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class DestinacionEconomicaDAL : GeneralDAL
    {

         private Database vDataBase;

         public DestinacionEconomicaDAL()
        {
            this.vDataBase = DatabaseFactory.CreateDatabase("DataBaseSEVEN");
        }

        /// <summary>
        /// consulta los DestinacionEconomica
        /// </summary>
        /// <returns>LISTA DE DestinacionEconomica</returns>
        public List<DestinacionEconomica> ConsultarDestinacionEconomica()
        {
            try
            {
                using (DbConnection vDbConnection = vDataBase.CreateConnection())
                {
                    vDbConnection.Open();
                    DbCommand vDbCommand = vDbConnection.CreateCommand();
                    vDbCommand.CommandText = "SELECT [ITE_CODI] ,[ITE_NOMB] FROM [produccion].[dbo].[GN_VITEM] WHERE [ITE_ACTI] = N's' and [TIT_CONT] = 154";

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DestinacionEconomica> vListDestinacionEconomica = new List<DestinacionEconomica>();
                        while (vDataReaderResults.Read())
                        {
                            DestinacionEconomica vDestinacionEconomica = new DestinacionEconomica();
                            vDestinacionEconomica.IdDestinacionEconomica = vDataReaderResults["ITE_CODI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_CODI"].ToString()) : vDestinacionEconomica.IdDestinacionEconomica;
                            vDestinacionEconomica.NombreDestinacionEconomica = vDataReaderResults["ITE_NOMB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_NOMB"].ToString().ToUpper()) : vDestinacionEconomica.NombreDestinacionEconomica;
                            vListDestinacionEconomica.Add(vDestinacionEconomica);
                            vDestinacionEconomica = null;
                        }

                        return vListDestinacionEconomica;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
