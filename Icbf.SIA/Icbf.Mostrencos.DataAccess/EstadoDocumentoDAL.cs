﻿//-----------------------------------------------------------------------
// <copyright file="EstadoDocumentoDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase EstadoDocumentoDAL.</summary>
// <author>INDESAP[Jesus Eduardo Cortes]</author>
// <date>03/10/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class EstadoDocumentoDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta el estado documento por el nombre del estado
        /// </summary>
        /// <param name="pNombreEstadoDocumento">nombre a consultar</param>
        /// <returns>variable de tipo EstadoDocumento con la información resultante de la consulta</returns>
        public EstadoDocumento ConsultarEstadoDocumentoPorNombre(string pNombreEstadoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EstadoDocumento_ConsultarPorNombre"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NombreEstadoDocumento", DbType.String, pNombreEstadoDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoDocumento vEstadoDocumento = new EstadoDocumento();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoDocumento.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoDocumento.Estado;
                            vEstadoDocumento.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"]) : vEstadoDocumento.IdEstadoDocumento;
                            vEstadoDocumento.CodigoEstadoDocumento = vDataReaderResults["CodigoEstadoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoDocumento"].ToString()) : vEstadoDocumento.CodigoEstadoDocumento;
                            vEstadoDocumento.NombreEstadoDocumento = vDataReaderResults["NombreEstadoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDocumento"].ToString()) : vEstadoDocumento.NombreEstadoDocumento;
                            vEstadoDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoDocumento.UsuarioCrea;
                            vEstadoDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoDocumento.FechaCrea;
                            vEstadoDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoDocumento.UsuarioModifica;
                            vEstadoDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoDocumento.FechaModifica;
                        }

                        return vEstadoDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el EstadoDocumento por el id del estado
        /// </summary>
        /// <param name="pIdEstadoDocumento">Id del estado que se va a consultar</param>
        /// <returns>variable de tipo EstadoDocumento con la información resultante de la consulta</returns>
        public EstadoDocumento ConsultarEstadoDocumentoPorId(int pIdEstadoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EstadoDocumento_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pIdEstadoDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoDocumento vEstadoDocumento = new EstadoDocumento();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoDocumento.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoDocumento.Estado;
                            vEstadoDocumento.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"]) : vEstadoDocumento.IdEstadoDocumento;
                            vEstadoDocumento.CodigoEstadoDocumento = vDataReaderResults["CodigoEstadoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoDocumento"].ToString()) : vEstadoDocumento.CodigoEstadoDocumento;
                            vEstadoDocumento.NombreEstadoDocumento = vDataReaderResults["NombreEstadoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDocumento"].ToString()) : vEstadoDocumento.NombreEstadoDocumento;
                            vEstadoDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoDocumento.UsuarioCrea;
                            vEstadoDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoDocumento.FechaCrea;
                            vEstadoDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoDocumento.UsuarioModifica;
                            vEstadoDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoDocumento.FechaModifica;
                        }

                        return vEstadoDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
