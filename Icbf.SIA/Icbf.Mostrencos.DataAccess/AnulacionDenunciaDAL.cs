﻿using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.DataAccess
{
    public class AnulacionDenunciaDAL : GeneralDAL
    {
        public AnulacionDenunciaDAL()
        {
        }

        /// <summary>
        /// Consulta el Histórico Anulacion Denuncias por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de anulaciones de la denuncia</returns>
        public List<AnulacionDenuncia> ConsultarHistoricoAnulacionDenunciaPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoAnulacionDenuncia_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AnulacionDenuncia> vListaHistoricoAnulacionDenuncias = new List<AnulacionDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            AnulacionDenuncia vHistoricoAnulacionDenuncias = new AnulacionDenuncia();
                            vHistoricoAnulacionDenuncias.IdSolicitudAnulacion = vDataReaderResults["IdSolicitudAnulacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSolicitudAnulacion"].ToString()) : vHistoricoAnulacionDenuncias.IdSolicitudAnulacion;
                            vHistoricoAnulacionDenuncias.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"]) : vHistoricoAnulacionDenuncias.IdDenunciaBien;
                            vHistoricoAnulacionDenuncias.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vHistoricoAnulacionDenuncias.FechaSolicitud;
                            vHistoricoAnulacionDenuncias.MotivoAnulacion = vDataReaderResults["MotivoAnulacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoAnulacion"].ToString()) : vHistoricoAnulacionDenuncias.MotivoAnulacion;
                            vHistoricoAnulacionDenuncias.DescripcionAnulacion = vDataReaderResults["DescripcionAnulacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionAnulacion"].ToString()) : vHistoricoAnulacionDenuncias.DescripcionAnulacion;
                            vHistoricoAnulacionDenuncias.FechaRespuestaSolicitud = vDataReaderResults["FechaRespuestaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuestaSolicitud"].ToString()) : vHistoricoAnulacionDenuncias.FechaRespuestaSolicitud;
                            vHistoricoAnulacionDenuncias.AnulacionAprobada = vDataReaderResults["AnulacionAprobada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AnulacionAprobada"].ToString()) : vHistoricoAnulacionDenuncias.AnulacionAprobada;
                            vHistoricoAnulacionDenuncias.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observacion"].ToString()) : vHistoricoAnulacionDenuncias.Observacion;
                            vHistoricoAnulacionDenuncias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoAnulacionDenuncias.UsuarioCrea;
                            vHistoricoAnulacionDenuncias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoAnulacionDenuncias.UsuarioModifica;
                            vHistoricoAnulacionDenuncias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoAnulacionDenuncias.FechaModifica;
                            vListaHistoricoAnulacionDenuncias.Add(vHistoricoAnulacionDenuncias);
                            vHistoricoAnulacionDenuncias = null;
                        }

                        return vListaHistoricoAnulacionDenuncias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertamos una solicitud de denuncia
        /// </summary>
        /// <param name="pAnulacionDenuncia"></param>
        /// <returns></returns>
        public int InsertarSolicitudAnulacion(AnulacionDenuncia pAnulacionDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_AnulacionDenuncia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAnulacionDenuncia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pAnulacionDenuncia.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pAnulacionDenuncia.IdUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoAnulacion", DbType.Int32, Convert.ToInt32(pAnulacionDenuncia.IdMotivoAnulacion));
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionAnulacion", DbType.String, pAnulacionDenuncia.DescripcionAnulacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAnulacionDenuncia.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAnulacionDenuncia"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pAnulacionDenuncia;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pAnulacionDenuncia, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la aprobacion de la solicitud de la anulacion
        /// </summary>
        /// <param name="pAnulacionDenuncia"></param>
        /// <returns></returns>
        public int InsertarAprobacionAnulacion(AnulacionDenuncia pAnulacionDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_AnulacionDenunciaAprobacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAprobacionAnulacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pAnulacionDenuncia.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@AprobacionAnulacion", DbType.Int32, pAnulacionDenuncia.IdAnulacionAprobada);
                    vDataBase.AddInParameter(vDbCommand, "@Observacion", DbType.String, pAnulacionDenuncia.Observacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAnulacionDenuncia.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pAnulacionDenuncia.IdUsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAprobacionAnulacion"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pAnulacionDenuncia;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pAnulacionDenuncia, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar el nombre y correo del abogado
        /// </summary>
        /// <param name="vIdDenunciaBien"></param>
        /// <returns></returns>
        public AnulacionDenuncia ConsultarAbogadoSolicitudAnulacion(int vIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_AnularDenunciaAbogado_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vIdDenunciaBien);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AnulacionDenuncia vAnulacionDenuncia = new AnulacionDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vAnulacionDenuncia.NombreaEnviar = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vAnulacionDenuncia.NombreaEnviar;
                            vAnulacionDenuncia.Para = vDataReaderResults["Correo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Correo"].ToString()) : vAnulacionDenuncia.Para;
                        }
                        return vAnulacionDenuncia;
                    }
                }


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar 
        /// </summary>
        /// <param name="vIdDenunciaBien"></param>
        /// <param name="vkeys"></param>
        /// <returns></returns>
        public AnulacionDenuncia ConsultarCoordinadorJuridicoSolicitudAnulacion(int vIdDenunciaBien, string vkeys)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_AnularDenunciaCoordinadorJuridico_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@keys", DbType.String, vkeys);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AnulacionDenuncia vAnulacionDenuncia = new AnulacionDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vAnulacionDenuncia.NombreaEnviar = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vAnulacionDenuncia.NombreaEnviar;
                            vAnulacionDenuncia.Para = vDataReaderResults["Correo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Correo"].ToString()) : vAnulacionDenuncia.Para;
                        }
                        return vAnulacionDenuncia;
                    }
                }


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
