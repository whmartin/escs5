﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.DataAccess
{
    public class RegistroCalidadDenuncianteCitacionDAL : GeneralDAL
    {
        public RegistroCalidadDenuncianteCitacionDAL()
        {
        }

        public RegistroCalidadDenuncianteCitacion ConsultarCalidadDenuncianteCitacion(int IdReconocimientoCalidadDenunciante)
        {
            try
            {
                RegistroCalidadDenuncianteCitacion vRegistroCalidad = new RegistroCalidadDenuncianteCitacion();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarCalidadDenunciante_ConsultarCitacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdReconocimientoCalidadDenunciante", DbType.Int32, IdReconocimientoCalidadDenunciante);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vRegistroCalidad.ReconocimientoCalidadDenunciante = vDataReaderResults["ReconocimientoCalidadDenunciante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ReconocimientoCalidadDenunciante"].ToString()) : vRegistroCalidad.ReconocimientoCalidadDenunciante;
                            vRegistroCalidad.IdResolucion = vDataReaderResults["IdResolucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResolucion"].ToString()) : vRegistroCalidad.NumeroResolucion;
                            vRegistroCalidad.NumeroResolucion = vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroResolucion"].ToString()) : vRegistroCalidad.NumeroResolucion;
                            vRegistroCalidad.FechaResolucion = vDataReaderResults["FechaResolucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaResolucion"].ToString()) : vRegistroCalidad.FechaResolucion;
                            vRegistroCalidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vRegistroCalidad.Observaciones;
                            vRegistroCalidad.SeNotifica = vDataReaderResults["SeNotifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SeNotifica"].ToString()) : vRegistroCalidad.SeNotifica;
                            vRegistroCalidad.FechaNotificacion = vDataReaderResults["FechaNotificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNotificacion"].ToString()) : vRegistroCalidad.FechaNotificacion;
                            vRegistroCalidad.FechaAviso = vDataReaderResults["FechaAviso"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAviso"].ToString()) : vRegistroCalidad.FechaAviso;
                            vRegistroCalidad.ObservacionCitacion = vDataReaderResults["ObservacionCitacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionCitacion"].ToString()) : vRegistroCalidad.ObservacionCitacion;
                            vRegistroCalidad.NombreDetalle = vDataReaderResults["NombreDetalle"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDetalle"].ToString()) : vRegistroCalidad.NombreDetalle;
                        }
                        return vRegistroCalidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int SetCalidadDenuncianteCitacion(RegistroCalidadDenuncianteCitacion vRegistroCalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarCalidadDenunciante_ModificarCitacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdResolucion", DbType.Int32, vRegistroCalidad.NumeroResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@SeNotifica", DbType.Int32, vRegistroCalidad.SeNotifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAviso", DbType.DateTime, vRegistroCalidad.FechaAviso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaNotificacionPersonal", DbType.DateTime, vRegistroCalidad.FechaNotificacion);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionEnvioCitacion", DbType.String, vRegistroCalidad.ObservacionCitacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, vRegistroCalidad.Usuario);

                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}