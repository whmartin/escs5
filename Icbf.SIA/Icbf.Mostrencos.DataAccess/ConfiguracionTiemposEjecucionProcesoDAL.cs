//-----------------------------------------------------------------------
// <copyright file="ConfiguracionTiemposEjecucionProcesoDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ConfiguracionTiemposEjecucionProcesoDAL.</summary>
// <author>Ingenian Software</author>
// <date>09/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo ConfiguracionTiemposEjecucionProcesoDAL.
    /// </summary>
    public class ConfiguracionTiemposEjecucionProcesoDAL : GeneralDAL
    {
        public ConfiguracionTiemposEjecucionProcesoDAL()
        {
        }

        /// <summary>
        /// M�todo que inserta ConfiguracionTiemposEjecucionProceso
        /// </summary>
        /// <param name="pConfiguracionTiemposEjecucionProceso">Entidad ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Resultado de la operaci�n</returns>
        public int InsertarConfiguracionTiemposEjecucionProceso(ConfiguracionTiemposEjecucionProceso pConfiguracionTiemposEjecucionProceso)
        {
            int vResultado;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ConfiguracionTiemposEjecucionProceso_Insertar"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;
                    
                    vDataBase.AddInParameter(vDbCommand, "@IdFase", DbType.Int32, pConfiguracionTiemposEjecucionProceso.IdFase);
                    vDataBase.AddInParameter(vDbCommand, "@IdActuacion", DbType.Int32, pConfiguracionTiemposEjecucionProceso.IdActuacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAccion", DbType.Int32, pConfiguracionTiemposEjecucionProceso.IdAccion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pConfiguracionTiemposEjecucionProceso.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@CalculoDiasEjecucion", DbType.Byte, pConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoEjecucioFaseActuacionAccion", DbType.Int32, pConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion);
                    vDataBase.AddInParameter(vDbCommand, "@CalculoDiasAlerta", DbType.Byte, pConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoEjecucionAlerta", DbType.Int32, pConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pConfiguracionTiemposEjecucionProceso.UsuarioCrea);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pConfiguracionTiemposEjecucionProceso;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que actualiza ConfiguracionTiemposEjecucionProceso
        /// </summary>
        /// <param name="pConfiguracionTiemposEjecucionProceso">Entidad ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Resultado de la operaci�n</returns>
        public int ModificarConfiguracionTiemposEjecucionProceso(ConfiguracionTiemposEjecucionProceso pConfiguracionTiemposEjecucionProceso)
        {
            int vResultado;

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ConfiguracionTiemposEjecucionProceso_Modificar"))
                {
                    SqlParameter vParametroRetorno = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar);
                    vParametroRetorno.Direction = ParameterDirection.ReturnValue;

                    vDataBase.AddInParameter(vDbCommand, "@IdConfiguracionTiemposEjecucionProceso", DbType.Int32, pConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso);
                    vDataBase.AddInParameter(vDbCommand, "@IdFase", DbType.Int32, pConfiguracionTiemposEjecucionProceso.IdFase);
                    vDataBase.AddInParameter(vDbCommand, "@IdActuacion", DbType.Int32, pConfiguracionTiemposEjecucionProceso.IdActuacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdAccion", DbType.Int32, pConfiguracionTiemposEjecucionProceso.IdAccion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pConfiguracionTiemposEjecucionProceso.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@CalculoDiasEjecucion", DbType.Byte, pConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoEjecucioFaseActuacionAccion", DbType.Int32, pConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion);
                    vDataBase.AddInParameter(vDbCommand, "@CalculoDiasAlerta", DbType.Byte, pConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta);
                    vDataBase.AddInParameter(vDbCommand, "@TiempoEjecucionAlerta", DbType.Int32, pConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pConfiguracionTiemposEjecucionProceso.UsuarioModifica);

                    vDbCommand.Parameters.Add(vParametroRetorno);
                    vDataBase.ExecuteNonQuery(vDbCommand);

                    vResultado = Convert.ToInt32(vDbCommand.Parameters["@RETURN_VALUE"].Value);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pConfiguracionTiemposEjecucionProceso;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vAuditorio, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta la ConfiguracionTiemposEjecucionProceso x Id
        /// </summary>
        /// <param name="pIdConfiguracionTiemposEjecucionProceso">Id de ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Entidad ConfiguracionTiemposEjecucionProceso</returns>
        public ConfiguracionTiemposEjecucionProceso ConsultarConfiguracionTiemposEjecucionProceso(int pIdConfiguracionTiemposEjecucionProceso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ConfiguracionTiemposEjecucionProceso_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConfiguracionTiemposEjecucionProceso", DbType.Int32, pIdConfiguracionTiemposEjecucionProceso);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ConfiguracionTiemposEjecucionProceso vConfiguracionTiemposEjecucionProceso = new ConfiguracionTiemposEjecucionProceso();
                        while (vDataReaderResults.Read())
                        {
                            vConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso = vDataReaderResults["IdConfiguracionTiemposEjecucionProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConfiguracionTiemposEjecucionProceso"].ToString()) : vConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso;
                            vConfiguracionTiemposEjecucionProceso.IdFase = vDataReaderResults["IdFase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFase"].ToString()) : vConfiguracionTiemposEjecucionProceso.IdFase;
                            vConfiguracionTiemposEjecucionProceso.IdActuacion = vDataReaderResults["IdActuacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActuacion"].ToString()) : vConfiguracionTiemposEjecucionProceso.IdActuacion;
                            vConfiguracionTiemposEjecucionProceso.IdAccion = vDataReaderResults["IdAccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAccion"].ToString()) : vConfiguracionTiemposEjecucionProceso.IdAccion;
                            vConfiguracionTiemposEjecucionProceso.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToByte(vDataReaderResults["Estado"]) : vConfiguracionTiemposEjecucionProceso.Estado;
                            vConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion = vDataReaderResults["CalculoDiasEjecucion"] != DBNull.Value ? Convert.ToByte(vDataReaderResults["CalculoDiasEjecucion"]) : vConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion;
                            vConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion = vDataReaderResults["TiempoEjecucioFaseActuacionAccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoEjecucioFaseActuacionAccion"].ToString()) : vConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion;
                            vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta = vDataReaderResults["CalculoDiasAlerta"] != DBNull.Value ? Convert.ToByte(vDataReaderResults["CalculoDiasAlerta"]) : vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta;
                            vConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta = vDataReaderResults["TiempoEjecucionAlerta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoEjecucionAlerta"].ToString()) : vConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta;
                            vConfiguracionTiemposEjecucionProceso.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConfiguracionTiemposEjecucionProceso.UsuarioCrea;
                            vConfiguracionTiemposEjecucionProceso.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConfiguracionTiemposEjecucionProceso.FechaCrea;
                            vConfiguracionTiemposEjecucionProceso.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConfiguracionTiemposEjecucionProceso.UsuarioModifica;
                            vConfiguracionTiemposEjecucionProceso.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConfiguracionTiemposEjecucionProceso.FechaModifica;
                            vConfiguracionTiemposEjecucionProceso.Fase = vDataReaderResults["Fase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Fase"].ToString()) : vConfiguracionTiemposEjecucionProceso.Fase;
                            vConfiguracionTiemposEjecucionProceso.Actuacion = vDataReaderResults["Actuacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Actuacion"].ToString()) : vConfiguracionTiemposEjecucionProceso.Actuacion;
                            vConfiguracionTiemposEjecucionProceso.Accion = vDataReaderResults["Accion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Accion"].ToString()) : vConfiguracionTiemposEjecucionProceso.Accion;
                        }

                        return vConfiguracionTiemposEjecucionProceso;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta  los registros de ConfiguracionTiemposEjecucionProceso x Filtros
        /// </summary>
        /// <param name="pIdFase">Id de la Fase</param>
        /// <param name="pIdActuacion">Id de la actuaci�n</param>
        /// <param name="pIdAccion">Id de la Acci�n</param>
        /// <param name="pEstado">Id del estado</param>
        /// <returns>Lista de Resultados</returns>
        public List<ConfiguracionTiemposEjecucionProceso> ConsultarListaConfiguracionTiemposEjecucionProceso(string pFase, string pActuacion, string pAccion, byte? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ConfiguracionTiemposEjecucionProceso_ConsultarLista"))
                {
                    if (!string.IsNullOrEmpty(pFase))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Fase", DbType.String, pFase);
                    }

                    if (!string.IsNullOrEmpty(pActuacion))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Actuacion", DbType.String, pActuacion);
                    }

                    if (!string.IsNullOrEmpty(pAccion))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Accion", DbType.String, pAccion);
                    }

                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConfiguracionTiemposEjecucionProceso> vListaConfiguracionTiemposEjecucionProceso = new List<ConfiguracionTiemposEjecucionProceso>();
                        while (vDataReaderResults.Read())
                        {
                            ConfiguracionTiemposEjecucionProceso vConfiguracionTiemposEjecucionProceso = new ConfiguracionTiemposEjecucionProceso();
                            vConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso = vDataReaderResults["IdConfiguracionTiemposEjecucionProceso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConfiguracionTiemposEjecucionProceso"].ToString()) : vConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso;
                            vConfiguracionTiemposEjecucionProceso.IdFase = vDataReaderResults["IdFase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFase"].ToString()) : vConfiguracionTiemposEjecucionProceso.IdFase;
                            vConfiguracionTiemposEjecucionProceso.Fase = vDataReaderResults["Fase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Fase"].ToString()) : vConfiguracionTiemposEjecucionProceso.Fase;
                            vConfiguracionTiemposEjecucionProceso.IdActuacion = vDataReaderResults["IdActuacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActuacion"].ToString()) : vConfiguracionTiemposEjecucionProceso.IdActuacion;
                            vConfiguracionTiemposEjecucionProceso.Actuacion = vDataReaderResults["Actuacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Actuacion"].ToString()) : vConfiguracionTiemposEjecucionProceso.Actuacion;
                            vConfiguracionTiemposEjecucionProceso.IdAccion = vDataReaderResults["IdAccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAccion"].ToString()) : vConfiguracionTiemposEjecucionProceso.IdAccion;
                            vConfiguracionTiemposEjecucionProceso.Accion = vDataReaderResults["Accion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Accion"].ToString()) : vConfiguracionTiemposEjecucionProceso.Accion;
                            vConfiguracionTiemposEjecucionProceso.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToByte(vDataReaderResults["Estado"]) : vConfiguracionTiemposEjecucionProceso.Estado;
                            vConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion = vDataReaderResults["CalculoDiasEjecucion"] != DBNull.Value ? Convert.ToByte(vDataReaderResults["CalculoDiasEjecucion"]) : vConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion;
                            vConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion = vDataReaderResults["TiempoEjecucioFaseActuacionAccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoEjecucioFaseActuacionAccion"].ToString()) : vConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion;
                            vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta = vDataReaderResults["CalculoDiasAlerta"] != DBNull.Value ? Convert.ToByte(vDataReaderResults["CalculoDiasAlerta"]) : vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta;
                            vConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta = vDataReaderResults["TiempoEjecucionAlerta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TiempoEjecucionAlerta"].ToString()) : vConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta;
                            vConfiguracionTiemposEjecucionProceso.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vConfiguracionTiemposEjecucionProceso.UsuarioCrea;
                            vConfiguracionTiemposEjecucionProceso.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vConfiguracionTiemposEjecucionProceso.FechaCrea;
                            vConfiguracionTiemposEjecucionProceso.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vConfiguracionTiemposEjecucionProceso.UsuarioModifica;
                            vConfiguracionTiemposEjecucionProceso.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vConfiguracionTiemposEjecucionProceso.FechaModifica;
                            vListaConfiguracionTiemposEjecucionProceso.Add(vConfiguracionTiemposEjecucionProceso);
                        }

                        return vListaConfiguracionTiemposEjecucionProceso;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
