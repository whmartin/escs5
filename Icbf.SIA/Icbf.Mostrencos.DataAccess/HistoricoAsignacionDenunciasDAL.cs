﻿
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class HistoricoAsignacionDenunciasDAL : GeneralDAL
    {
        /// <summary>
        /// inserta un nuevo histórico asignación denuncia
        /// </summary>
        /// <param name="pHistoricoAsignacionDenuncias">variable de tipo Histórico Asignación Denuncias que contiene la información a insertar</param>
        /// <returns>id del Histórico Asignación Denuncias insertado</returns>
        public int InsertarHistoricoAsignacionDenuncias(HistoricoAsignacionDenuncias pHistoricoAsignacionDenuncias)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoAsignacionDenuncias_Insert"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdHistoricoAsignacionDenuncias", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pHistoricoAsignacionDenuncias.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdAbogado", DbType.Int32, pHistoricoAsignacionDenuncias.IdAbogado);                    
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistoricoAsignacionDenuncias.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAsignacion", DbType.DateTime, pHistoricoAsignacionDenuncias.FechaAsignacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdHistoricoAsignacionDenuncias"));
                        pHistoricoAsignacionDenuncias.IdHistoricoAsignacionDenuncias = vResultado;
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pHistoricoAsignacionDenuncias;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pHistoricoAsignacionDenuncias, vDbCommand);
                        return vResultado;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Histórico Asignación Denuncias por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de asignación de abogados</returns>
        public List<HistoricoAsignacionDenuncias> ConsultarHistoricoAsignacionDenunciasPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_HistoricoAsignacionDenuncias_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<HistoricoAsignacionDenuncias> vListaHistoricoAsignacionDenuncias = new List<HistoricoAsignacionDenuncias>();
                        while (vDataReaderResults.Read())
                        {
                            HistoricoAsignacionDenuncias vHistoricoAsignacionDenuncias = new HistoricoAsignacionDenuncias();
                            vHistoricoAsignacionDenuncias.IdAbogado = vDataReaderResults["IdAbogado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAbogado"].ToString()) : vHistoricoAsignacionDenuncias.IdAbogado;
                            vHistoricoAsignacionDenuncias.IdHistoricoAsignacionDenuncias = vDataReaderResults["IdHistoricoAsignacionDenuncias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoAsignacionDenuncias"]) : vHistoricoAsignacionDenuncias.IdHistoricoAsignacionDenuncias;
                            vHistoricoAsignacionDenuncias.FechaAsignacion = vDataReaderResults["FechaAsignacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacion"].ToString()) : vHistoricoAsignacionDenuncias.FechaAsignacion;
                            vHistoricoAsignacionDenuncias.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vHistoricoAsignacionDenuncias.UsuarioCrea;
                            vHistoricoAsignacionDenuncias.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vHistoricoAsignacionDenuncias.FechaCrea;
                            vHistoricoAsignacionDenuncias.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vHistoricoAsignacionDenuncias.UsuarioModifica;
                            vHistoricoAsignacionDenuncias.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vHistoricoAsignacionDenuncias.FechaModifica;
                            vHistoricoAsignacionDenuncias.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vHistoricoAsignacionDenuncias.IdDenunciaBien;
                            vListaHistoricoAsignacionDenuncias.Add(vHistoricoAsignacionDenuncias);
                            vHistoricoAsignacionDenuncias = null;
                        }

                        return vListaHistoricoAsignacionDenuncias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
