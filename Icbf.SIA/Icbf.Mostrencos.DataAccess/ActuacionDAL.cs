﻿//-----------------------------------------------------------------------
// <copyright file="ActuacionDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ActuacionDAL.</summary>
// <author>Ingenian Software</author>
// <date>10/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase para manejar el acceso a la base de datos del modulo Actuacion.
    /// </summary>
    public class ActuacionDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActuacionDAL"/> class.
        /// </summary>
        public ActuacionDAL()
        {
        }

        /// <summary>
        /// Evento para Cargar la Lista de opciones de Actuaciones x IdFase
        /// </summary>
        /// <param name="sender">The DropDownList</param>
        /// <param name="e">The SelectedIndexChanged</param>
        public List<Actuacion> ConsultarActuacionesActivasPorIdFase(int pIdFase)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Consultar_ActuacionesActivas"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFase", DbType.Int32, pIdFase);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Actuacion> vListaActuacion = new List<Actuacion>();
                        while (vDataReaderResults.Read())
                        {
                            Actuacion vActuacion = new Actuacion();
                            vActuacion.IdActuacion = vDataReaderResults["IdActuacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActuacion"].ToString()) : vActuacion.IdActuacion;
                            vActuacion.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vActuacion.Nombre;
                            vListaActuacion.Add(vActuacion);
                        }

                        return vListaActuacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
