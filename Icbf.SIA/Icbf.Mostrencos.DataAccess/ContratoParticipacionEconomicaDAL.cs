﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    public class ContratoParticipacionEconomicaDAL : GeneralDAL
    {
        //Consultar Contratos X Denuncia
        public List<ContratoParticipacionEconomica> ConsultarContratosXDenuncia(int IdDenunciaBien)
        {
            try
            {
                List<ContratoParticipacionEconomica> vLstContratoDenunciaBien = new List<ContratoParticipacionEconomica>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Contrato_Denuncia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            ContratoParticipacionEconomica vContratoDenunciaBien = new ContratoParticipacionEconomica();
                            vContratoDenunciaBien.IdContratoParticipacionEconomica = vDataReaderResults["IdContratoParticipacionEconomica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratoParticipacionEconomica"].ToString()) : vContratoDenunciaBien.IdContratoParticipacionEconomica;
                            vContratoDenunciaBien.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratoDenunciaBien.IdContrato;
                            vContratoDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vContratoDenunciaBien.IdDenunciaBien;
                            vContratoDenunciaBien.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vContratoDenunciaBien.Vigencia;
                            vContratoDenunciaBien.IDRegional = vDataReaderResults["IDRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDRegional"].ToString()) : vContratoDenunciaBien.IDRegional;
                            vContratoDenunciaBien.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContratoDenunciaBien.NombreRegional;
                            vContratoDenunciaBien.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratoDenunciaBien.NumeroContrato;
                            vContratoDenunciaBien.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContratoDenunciaBien.FechaSuscripcion;
                            vContratoDenunciaBien.FechaInicialEjecucionDias = vDataReaderResults["FechaInicialEjecucionDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionDias"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionDias;
                            vContratoDenunciaBien.FechaInicialEjecucionMeses = vDataReaderResults["FechaInicialEjecucionMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionMeses"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionMeses;
                            vContratoDenunciaBien.FechaInicialEjecucionAnhios = vDataReaderResults["FechaInicialEjecucionAnhios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionAnhios"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionAnhios;
                            vContratoDenunciaBien.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratoDenunciaBien.FechaInicioEjecucion;
                            vContratoDenunciaBien.FechaTerminacionInicial = vDataReaderResults["FechaTerminacionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionInicial"].ToString()) : vContratoDenunciaBien.FechaTerminacionInicial;
                            vContratoDenunciaBien.FechaTerminacionFinal = vDataReaderResults["FechaTerminacionFinal"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionFinal"].ToString()) : vContratoDenunciaBien.FechaTerminacionFinal;
                            vContratoDenunciaBien.IDEstadoContrato = vDataReaderResults["IDEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadoContrato"].ToString()) : vContratoDenunciaBien.IDEstadoContrato;
                            vContratoDenunciaBien.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratoDenunciaBien.EstadoContrato;
                            vLstContratoDenunciaBien.Add(vContratoDenunciaBien);
                        }
                        return vLstContratoDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Información Contrato
        public List<ContratoParticipacionEconomica> ConsultarInformacionContrato(int IdDenunciaBien, int IdContrato)
        {
            try
            {
                List<ContratoParticipacionEconomica> vLstContratoDenunciaBien = new List<ContratoParticipacionEconomica>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_InformacionContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            ContratoParticipacionEconomica vContratoDenunciaBien = new ContratoParticipacionEconomica();
                            vContratoDenunciaBien.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratoDenunciaBien.IdContrato;
                            vContratoDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vContratoDenunciaBien.IdDenunciaBien;
                            vContratoDenunciaBien.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vContratoDenunciaBien.Vigencia;
                            vContratoDenunciaBien.IDRegional = vDataReaderResults["IDRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDRegional"].ToString()) : vContratoDenunciaBien.IDRegional;
                            vContratoDenunciaBien.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContratoDenunciaBien.NombreRegional;
                            vContratoDenunciaBien.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratoDenunciaBien.NumeroContrato;
                            vContratoDenunciaBien.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContratoDenunciaBien.FechaSuscripcion;
                            vContratoDenunciaBien.FechaInicialEjecucionDias = vDataReaderResults["FechaInicialEjecucionDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionDias"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionDias;
                            vContratoDenunciaBien.FechaInicialEjecucionMeses = vDataReaderResults["FechaInicialEjecucionMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionMeses"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionMeses;
                            vContratoDenunciaBien.FechaInicialEjecucionAnhios = vDataReaderResults["FechaInicialEjecucionAnhios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionAnhios"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionAnhios;
                            vContratoDenunciaBien.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratoDenunciaBien.FechaInicioEjecucion;
                            vContratoDenunciaBien.FechaTerminacionInicial = vDataReaderResults["FechaTerminacionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionInicial"].ToString()) : vContratoDenunciaBien.FechaTerminacionInicial;
                            vContratoDenunciaBien.FechaTerminacionFinal = vDataReaderResults["FechaTerminacionFinal"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionFinal"].ToString()) : vContratoDenunciaBien.FechaTerminacionFinal;
                            vContratoDenunciaBien.IDEstadoContrato = vDataReaderResults["IDEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadoContrato"].ToString()) : vContratoDenunciaBien.IDEstadoContrato;
                            vContratoDenunciaBien.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratoDenunciaBien.EstadoContrato;
                            vLstContratoDenunciaBien.Add(vContratoDenunciaBien);
                        }
                        return vLstContratoDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoParticipacionEconomica> ConsultarContratoXDenunciaMultiplesParametros(int IdVigencia, int IdRegional, string NumeroContrato)
        {
            try
            {
                List<ContratoParticipacionEconomica> vLstContratoDenunciaBien = new List<ContratoParticipacionEconomica>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Contrato_Denuncia_Consultar_MultiplesParametros"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, IdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, NumeroContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            ContratoParticipacionEconomica vContratoDenunciaBien = new ContratoParticipacionEconomica();
                            vContratoDenunciaBien.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratoDenunciaBien.IdContrato;
                            vContratoDenunciaBien.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vContratoDenunciaBien.Vigencia;
                            vContratoDenunciaBien.IDRegional = vDataReaderResults["IDRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDRegional"].ToString()) : vContratoDenunciaBien.IDRegional;
                            vContratoDenunciaBien.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContratoDenunciaBien.NombreRegional;
                            vContratoDenunciaBien.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratoDenunciaBien.NumeroContrato;
                            vContratoDenunciaBien.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContratoDenunciaBien.FechaSuscripcion;
                            vContratoDenunciaBien.FechaInicialEjecucionDias = vDataReaderResults["DiasFechaFinalCalculada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasFechaFinalCalculada"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionDias;
                            vContratoDenunciaBien.FechaInicialEjecucionMeses = vDataReaderResults["MesesFechaFinalCalculada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["MesesFechaFinalCalculada"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionMeses;
                            vContratoDenunciaBien.FechaInicialEjecucionAnhios = vDataReaderResults["EsFechaFinalCalculada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsFechaFinalCalculada"]) : vContratoDenunciaBien.FechaInicialEjecucionAnhios;
                            vContratoDenunciaBien.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratoDenunciaBien.FechaInicioEjecucion;
                            vContratoDenunciaBien.FechaTerminacionInicial = vDataReaderResults["FechaTerminacionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionInicial"].ToString()) : vContratoDenunciaBien.FechaTerminacionInicial;
                            vContratoDenunciaBien.FechaTerminacionFinal = vDataReaderResults["FechaTerminacionFinal"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionFinal"].ToString()) : vContratoDenunciaBien.FechaTerminacionFinal;
                            vContratoDenunciaBien.IDEstadoContrato = vDataReaderResults["IDEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadoContrato"].ToString()) : vContratoDenunciaBien.IDEstadoContrato;
                            vContratoDenunciaBien.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratoDenunciaBien.EstadoContrato;
                            vLstContratoDenunciaBien.Add(vContratoDenunciaBien);
                        }
                        return vLstContratoDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarContratoParticipacionEconomica(ContratoParticipacionEconomica pContratoParticipacionEconomica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_ContratoParticipacionEconomica_Insertar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContratoParticipacionEconomica", DbType.Int32, 18);

                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pContratoParticipacionEconomica.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pContratoParticipacionEconomica.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContratoParticipacionEconomica.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pContratoParticipacionEconomica.FechaCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pContratoParticipacionEconomica.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pContratoParticipacionEconomica.FechaModifica);

                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                    Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pContratoParticipacionEconomica;
                    vAuditorio.ProgramaGeneraLog = true;
                    vAuditorio.Operacion = "INSERT";
                    pContratoParticipacionEconomica.IdContratoParticipacionEconomica = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContratoParticipacionEconomica").ToString());
                    GenerarLogAuditoria(pContratoParticipacionEconomica, vDbCommand);
                    return pContratoParticipacionEconomica.IdContratoParticipacionEconomica;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Existencia Contratos X Denuncia
        public List<ContratoParticipacionEconomica> ConsultarExistenciaContratosXDenuncia(int IdDenunciaBien, int IdContrato)
        {
            try
            {
                List<ContratoParticipacionEconomica> vLstContratoDenunciaBien = new List<ContratoParticipacionEconomica>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Contrato_Denuncia_Consultar_Existencia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            ContratoParticipacionEconomica vContratoDenunciaBien = new ContratoParticipacionEconomica();
                            vContratoDenunciaBien.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratoDenunciaBien.IdContrato;
                            vContratoDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vContratoDenunciaBien.IdDenunciaBien;
                            vContratoDenunciaBien.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vContratoDenunciaBien.Vigencia;
                            vContratoDenunciaBien.IDRegional = vDataReaderResults["IDRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDRegional"].ToString()) : vContratoDenunciaBien.IDRegional;
                            vContratoDenunciaBien.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vContratoDenunciaBien.NombreRegional;
                            vContratoDenunciaBien.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vContratoDenunciaBien.NumeroContrato;
                            vContratoDenunciaBien.FechaSuscripcion = vDataReaderResults["FechaSuscripcion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSuscripcion"].ToString()) : vContratoDenunciaBien.FechaSuscripcion;
                            vContratoDenunciaBien.FechaInicialEjecucionDias = vDataReaderResults["FechaInicialEjecucionDias"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionDias"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionDias;
                            vContratoDenunciaBien.FechaInicialEjecucionMeses = vDataReaderResults["FechaInicialEjecucionMeses"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionMeses"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionMeses;
                            vContratoDenunciaBien.FechaInicialEjecucionAnhios = vDataReaderResults["FechaInicialEjecucionAnhios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaInicialEjecucionAnhios"].ToString()) : vContratoDenunciaBien.FechaInicialEjecucionAnhios;
                            vContratoDenunciaBien.FechaInicioEjecucion = vDataReaderResults["FechaInicioEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioEjecucion"].ToString()) : vContratoDenunciaBien.FechaInicioEjecucion;
                            vContratoDenunciaBien.FechaTerminacionInicial = vDataReaderResults["FechaTerminacionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionInicial"].ToString()) : vContratoDenunciaBien.FechaTerminacionInicial;
                            vContratoDenunciaBien.FechaTerminacionFinal = vDataReaderResults["FechaTerminacionFinal"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacionFinal"].ToString()) : vContratoDenunciaBien.FechaTerminacionFinal;
                            vContratoDenunciaBien.IDEstadoContrato = vDataReaderResults["IDEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDEstadoContrato"].ToString()) : vContratoDenunciaBien.IDEstadoContrato;
                            vContratoDenunciaBien.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vContratoDenunciaBien.EstadoContrato;
                            vLstContratoDenunciaBien.Add(vContratoDenunciaBien);
                        }
                        return vLstContratoDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

