﻿//-----------------------------------------------------------------------
// <copyright file="TipoDocumentoBienDenunciadoDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoDocumentoBienDenunciadoDAL.</summary>
// <author>INGENIAN SOFTWARE[Darío Beltrán]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase Tipo Documento bien denunciado dal
    /// </summary>
    public class TipoDocumentoBienDenunciadoDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoDocumentoBienDenunciadoDAL"/> class.
        /// </summary>
        public TipoDocumentoBienDenunciadoDAL()
        {
        }

        /// <summary>
        /// Consultars the tipo documento bien denunciados.
        /// </summary>
        /// <returns>Lista resultado de la operación</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoBienDenunciados()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoDocumentosBienDenunciados_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocumentoBienDenunciado> lstTipoDocumentosBien = new List<TipoDocumentoBienDenunciado>();
                        while (vDataReaderResults.Read())
                        {
                            TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
                            vTipoDocumento.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vTipoDocumento.IdTipoDocumentoBienDenunciado;
                            vTipoDocumento.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vTipoDocumento.NombreTipoDocumento;
                            vTipoDocumento.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vTipoDocumento.Estado;
                            vTipoDocumento.DescripcionDocumento = vDataReaderResults["DescripcionDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDocumento"].ToString()) : vTipoDocumento.DescripcionDocumento;
                            lstTipoDocumentosBien.Add(vTipoDocumento);
                        }

                        return lstTipoDocumentosBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Variable de tipo TipoDocumentoBienDenunciado con los datos a insertar</param>
        /// <returns>IdTipoDocumentoBienDenunciado del registro insertado</returns>
        public int InsertarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoDocumentoBienDenunciados_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoDocumento", DbType.String, pTipoDocumentoBienDenunciado.NombreTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionDocumento", DbType.String, pTipoDocumentoBienDenunciado.DescripcionDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pTipoDocumentoBienDenunciado.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoDocumentoBienDenunciado.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoDocumentoBienDenunciado"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTipoDocumentoBienDenunciado;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pTipoDocumentoBienDenunciado, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// edita un registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Variable de tipo TipoDocumentoBienDenunciado con los datos a insertar</param>
        /// <returns>el numero de registros editados</returns>
        public int EditarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoDocumentoBienDenunciado_Editar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pTipoDocumentoBienDenunciado.IdTipoDocumentoBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoDocumento", DbType.String, pTipoDocumentoBienDenunciado.NombreTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionDocumento", DbType.String, pTipoDocumentoBienDenunciado.DescripcionDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pTipoDocumentoBienDenunciado.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoDocumentoBienDenunciado.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTipoDocumentoBienDenunciado;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pTipoDocumentoBienDenunciado, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los TipoDocumentoBienDenunciado por el id
        /// </summary>
        /// <param name="pIdTipoDocumentoBienDenunciado">id del TipoDocumentoBienDenunciado</param>
        /// <returns>Variable de tipo TipoDocumentoBienDenunciado con los datos consultados</returns>
        public TipoDocumentoBienDenunciado ConsultarTipoDocumentoBienDenunciadoPorId(int pIdTipoDocumentoBienDenunciado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoDocumentoBienDenunciado_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pIdTipoDocumentoBienDenunciado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoDocumentoBienDenunciado vTipoDocumentoBienDenunciado = new TipoDocumentoBienDenunciado();
                        while (vDataReaderResults.Read())
                        {
                            vTipoDocumentoBienDenunciado.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vTipoDocumentoBienDenunciado.Estado;
                            vTipoDocumentoBienDenunciado.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"]) : vTipoDocumentoBienDenunciado.NombreTipoDocumento;
                            vTipoDocumentoBienDenunciado.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vTipoDocumentoBienDenunciado.IdTipoDocumentoBienDenunciado;
                            vTipoDocumentoBienDenunciado.DescripcionDocumento = vDataReaderResults["DescripcionDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDocumento"].ToString()) : vTipoDocumentoBienDenunciado.DescripcionDocumento;
                            vTipoDocumentoBienDenunciado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumentoBienDenunciado.UsuarioCrea;
                            vTipoDocumentoBienDenunciado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumentoBienDenunciado.FechaCrea;
                            vTipoDocumentoBienDenunciado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumentoBienDenunciado.UsuarioModifica;
                            vTipoDocumentoBienDenunciado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumentoBienDenunciado.FechaModifica;
                        }

                        return vTipoDocumentoBienDenunciado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los TipoDocumentoBienDenunciado que coincidan con los parámetros de entrada
        /// </summary>
        /// <param name="pNombre">Nombre del documento</param>
        /// <param name="pEstado">Estado del documento</param>
        /// <returns>Lista de resultados de la consulta</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoBienDenunciado(string pNombre, string pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoDocumentoBienDenunciado_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoDocumento", DbType.String, pNombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocumentoBienDenunciado> vListaTipoDocumentoBienDenunciado = new List<TipoDocumentoBienDenunciado>();
                        while (vDataReaderResults.Read())
                        {
                            TipoDocumentoBienDenunciado vTipoDocumentoBienDenunciado = new TipoDocumentoBienDenunciado();
                            vTipoDocumentoBienDenunciado.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vTipoDocumentoBienDenunciado.Estado;
                            vTipoDocumentoBienDenunciado.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"]) : vTipoDocumentoBienDenunciado.NombreTipoDocumento;
                            vTipoDocumentoBienDenunciado.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vTipoDocumentoBienDenunciado.IdTipoDocumentoBienDenunciado;
                            vTipoDocumentoBienDenunciado.DescripcionDocumento = vDataReaderResults["DescripcionDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDocumento"].ToString()) : vTipoDocumentoBienDenunciado.DescripcionDocumento;
                            vTipoDocumentoBienDenunciado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumentoBienDenunciado.UsuarioCrea;
                            vTipoDocumentoBienDenunciado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumentoBienDenunciado.FechaCrea;
                            vTipoDocumentoBienDenunciado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumentoBienDenunciado.UsuarioModifica;
                            vTipoDocumentoBienDenunciado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumentoBienDenunciado.FechaModifica;
                            vListaTipoDocumentoBienDenunciado.Add(vTipoDocumentoBienDenunciado);
                            vTipoDocumentoBienDenunciado = null;
                        }

                        return vListaTipoDocumentoBienDenunciado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Id del registro que se va a eliminar</param>
        /// <returns>1 si se ejecuta correctamente</returns>
        public int EliminarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TipoDocumentoBienDenunciado_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.String, pTipoDocumentoBienDenunciado.IdTipoDocumentoBienDenunciado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTipoDocumentoBienDenunciado;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "DELETE";
                        this.GenerarLogAuditoria(pTipoDocumentoBienDenunciado, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta los tipos de documento Activos
        /// </summary>
        /// <returns>Lista de Tipos de Documento</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoSoporte()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_TipoDocumentoSoporte_Consulta"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocumentoBienDenunciado> lstTipoDocumentosBien = new List<TipoDocumentoBienDenunciado>();
                        while (vDataReaderResults.Read())
                        {
                            TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
                            vTipoDocumento.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vTipoDocumento.IdTipoDocumentoBienDenunciado;
                            vTipoDocumento.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vTipoDocumento.NombreTipoDocumento;
                            lstTipoDocumentosBien.Add(vTipoDocumento);
                        }

                        return lstTipoDocumentosBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
