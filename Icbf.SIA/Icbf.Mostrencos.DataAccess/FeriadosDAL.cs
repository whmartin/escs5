﻿//-----------------------------------------------------------------------
// <copyright file="FeriadosDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase FeriadosDAL.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class FeriadosDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta ObtenerFeriados
        /// </summary>
        /// <returns>List tipo Feriados con la información consultada</returns>
        public List<Feriados> ObtenerFeriados()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ConsultarFeriados"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Feriados> vFeriadosList = new List<Feriados>();
                        while (vDataReaderResults.Read())
                        {
                            Feriados vFeriados = new Feriados();
                            vFeriados.fedID = vDataReaderResults["fedID"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["fedID"].ToString()) : vFeriados.fedID;
                            vFeriados.fed_descripcion = vDataReaderResults["fed_descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["fed_descripcion"].ToString()) : vFeriados.fed_descripcion;
                            vFeriadosList.Add(vFeriados);
                        }
                        return vFeriadosList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}