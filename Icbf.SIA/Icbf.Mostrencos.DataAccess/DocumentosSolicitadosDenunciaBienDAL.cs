﻿

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class DocumentosSolicitadosDenunciaBienDAL : GeneralDAL
    {
        /// <summary>
        /// Inserta un registro en la tabla DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int InsertarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Insert"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@IdCausante", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdCausante);
                    vDataBase.AddInParameter(vDbCommand, "@IdApoderado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@IdCalidadDenunciante", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdCalidadDenunciante);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosDenunciaBien;
                        vAuditoria.IdRegistro = vResultado;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pDocumentosSolicitadosDenunciaBien, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un registro de la tabla  DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EditarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Editar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.String, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaRecibido);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.RutaArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosDenunciaBien;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pDocumentosSolicitadosDenunciaBien, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un registro de la tabla  DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EditarDocumentosSolicitadosYRecibidosDenunciaBien(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Editar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.String, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaRecibido);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.RutaArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosDenunciaBien;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pDocumentosSolicitadosDenunciaBien, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pIdDocumentosSolicitadosDenunciaBien">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public DocumentosSolicitadosDenunciaBien ConsultarDocumentosSolicitadosDenunciaBienPorId(int pIdDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_ConsultarPorIdDocumentosSolicitadosDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, pIdDocumentosSolicitadosDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocumentosSolicitadosDenunciaBien vDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();
                        while (vDataReaderResults.Read())
                        {
                            vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado;
                            vDocumentosSolicitadosDenunciaBien.IdCausante = vDataReaderResults["IdCausante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCausante"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdCausante;
                            vDocumentosSolicitadosDenunciaBien.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"]) : vDocumentosSolicitadosDenunciaBien.FechaSolicitud;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada;
                            vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento;
                            vDocumentosSolicitadosDenunciaBien.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaRecibido;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionRecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionRecibida"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida;
                            vDocumentosSolicitadosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosDenunciaBien.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.RutaArchivo;
                            vDocumentosSolicitadosDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioCrea;
                            vDocumentosSolicitadosDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaCrea;
                            vDocumentosSolicitadosDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioModifica;
                            vDocumentosSolicitadosDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaModifica;
                        }

                        return vDocumentosSolicitadosDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosDenunciaBien vDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();
                            vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado;
                            vDocumentosSolicitadosDenunciaBien.IdCausante = vDataReaderResults["IdCausante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCausante"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdCausante;
                            vDocumentosSolicitadosDenunciaBien.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"]) : vDocumentosSolicitadosDenunciaBien.FechaSolicitud;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada;
                            vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento;
                            vDocumentosSolicitadosDenunciaBien.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaRecibido;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionRecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionRecibida"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida;
                            vDocumentosSolicitadosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosDenunciaBien.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.RutaArchivo;
                            vDocumentosSolicitadosDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioCrea;
                            vDocumentosSolicitadosDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaCrea;
                            vDocumentosSolicitadosDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioModifica;
                            vDocumentosSolicitadosDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaModifica;

                            if (vDocumentosSolicitadosDenunciaBien.FechaRecibido == null)
                            {
                                vDocumentosSolicitadosDenunciaBien.FechaRecibidoGrilla = "";
                            }
                            else
                            {
                                vDocumentosSolicitadosDenunciaBien.FechaRecibidoGrilla = Convert.ToDateTime(vDocumentosSolicitadosDenunciaBien.FechaRecibido).ToString("dd/MM/yyyy");
                            }

                            if (vDocumentosSolicitadosDenunciaBien.FechaSolicitud == new DateTime())
                            {
                                vDocumentosSolicitadosDenunciaBien.FechaSolicitudGrilla = "";
                            }
                            else
                            {
                                vDocumentosSolicitadosDenunciaBien.FechaSolicitudGrilla = vDocumentosSolicitadosDenunciaBien.FechaSolicitud.ToString("dd/MM/yyyy");
                            }
                            vList.Add(vDocumentosSolicitadosDenunciaBien);
                            vDocumentosSolicitadosDenunciaBien = null;
                        }

                        return vList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdCalidadDenunciante">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(int pIdCalidadDenunciante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_ConsultarPorIdCalidadDenunciante"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCalidadDenunciante", DbType.Int32, pIdCalidadDenunciante);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();

                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosDenunciaBien vDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();

                            vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado;
                            vDocumentosSolicitadosDenunciaBien.IdCalidadDenunciante = vDataReaderResults["IdCalidadDenunciante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCalidadDenunciante"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdCalidadDenunciante;
                            vDocumentosSolicitadosDenunciaBien.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"]) : vDocumentosSolicitadosDenunciaBien.FechaSolicitud;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada;
                            vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento;
                            vDocumentosSolicitadosDenunciaBien.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaRecibido;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionRecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionRecibida"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida;
                            vDocumentosSolicitadosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosDenunciaBien.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.RutaArchivo;
                            vDocumentosSolicitadosDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioCrea;
                            vDocumentosSolicitadosDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaCrea;
                            vDocumentosSolicitadosDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioModifica;
                            vDocumentosSolicitadosDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaModifica;
                            vDocumentosSolicitadosDenunciaBien.FechaSolicitudGrilla = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? vDocumentosSolicitadosDenunciaBien.FechaSolicitud.ToString("dd/MM/yyyy") : vDocumentosSolicitadosDenunciaBien.FechaSolicitudGrilla;
                            vDocumentosSolicitadosDenunciaBien.FechaRecibidoGrilla = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDocumentosSolicitadosDenunciaBien.FechaRecibido).ToString("dd/MM/yyyy") : vDocumentosSolicitadosDenunciaBien.FechaRecibidoGrilla;

                            vList.Add(vDocumentosSolicitadosDenunciaBien);
                        }
                        return vList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdCausante
        /// </summary>
        /// <param name="pIdCausante">id del registro a consulta</param>
        /// <returns>lista de tipo DocumentosSolicitadosDenunciaBien con la información consultada</returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdCausante(int pIdCausante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_ConsultarPorIdCausante"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCausante", DbType.Int32, pIdCausante);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitadosDenunciaBien = new List<DocumentosSolicitadosDenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosDenunciaBien vDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();
                            vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado;
                            vDocumentosSolicitadosDenunciaBien.IdCausante = vDataReaderResults["IdCausante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCausante"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdCausante;
                            vDocumentosSolicitadosDenunciaBien.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"]) : vDocumentosSolicitadosDenunciaBien.FechaSolicitud;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada;
                            vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento;
                            vDocumentosSolicitadosDenunciaBien.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaRecibido;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionRecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionRecibida"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida;
                            vDocumentosSolicitadosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosDenunciaBien.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.RutaArchivo;
                            vDocumentosSolicitadosDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioCrea;
                            vDocumentosSolicitadosDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaCrea;
                            vDocumentosSolicitadosDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioModifica;
                            vDocumentosSolicitadosDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaModifica;
                            vListaDocumentosSolicitadosDenunciaBien.Add(vDocumentosSolicitadosDenunciaBien);
                            vDocumentosSolicitadosDenunciaBien = null;
                        }

                        return vListaDocumentosSolicitadosDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdApoderado
        /// </summary>
        /// <param name="pIdApoderado">id del registro a consulta</param>
        /// <returns>lista de tipo DocumentosSolicitadosDenunciaBien con la información consultada</returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(int pIdApoderado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_ConsultarPorIdApoderado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdApoderado", DbType.Int32, pIdApoderado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitadosDenunciaBien = new List<DocumentosSolicitadosDenunciaBien>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosDenunciaBien vDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();
                            vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado;
                            vDocumentosSolicitadosDenunciaBien.IdCausante = vDataReaderResults["IdCausante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCausante"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdCausante;
                            vDocumentosSolicitadosDenunciaBien.IdApoderado = vDataReaderResults["IdApoderado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdApoderado"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdApoderado;
                            vDocumentosSolicitadosDenunciaBien.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"]) : vDocumentosSolicitadosDenunciaBien.FechaSolicitud;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada;
                            vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdDenunciaBien;
                            vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento;
                            vDocumentosSolicitadosDenunciaBien.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaRecibido;
                            vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionRecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionRecibida"].ToString()) : vDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida;
                            vDocumentosSolicitadosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosDenunciaBien.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vDocumentosSolicitadosDenunciaBien.RutaArchivo;
                            vDocumentosSolicitadosDenunciaBien.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioCrea;
                            vDocumentosSolicitadosDenunciaBien.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaCrea;
                            vDocumentosSolicitadosDenunciaBien.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.UsuarioModifica;
                            vDocumentosSolicitadosDenunciaBien.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentosSolicitadosDenunciaBien.FechaModifica;
                            vListaDocumentosSolicitadosDenunciaBien.Add(vDocumentosSolicitadosDenunciaBien);
                            vDocumentosSolicitadosDenunciaBien = null;
                        }

                        return vListaDocumentosSolicitadosDenunciaBien;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
