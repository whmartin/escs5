﻿//-----------------------------------------------------------------------
// <copyright file="filtrosMostrencosDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase filtrosMostrencosDAL.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class FiltrosMostrencosDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta filtroMueble
        /// </summary>
        /// <returns>List tipo resultFiltroMueble con la información consultada</returns>
        public List<ResultFiltroMueble> filtroMueble(FiltroMueble vfiltroMueble)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_FiltroMuebleInmueble"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@TipoBien", DbType.String, vfiltroMueble.tipoBien);
                    vDataBase.AddInParameter(vDbCommand, "@SubTipoBien", DbType.String, vfiltroMueble.subTipoBien);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseBien", DbType.String, vfiltroMueble.claseBien);
                    vDataBase.AddInParameter(vDbCommand, "@NroIdenBien", DbType.String, vfiltroMueble.nroIdenticacionBien);
                    vDataBase.AddInParameter(vDbCommand, "@MatriculaInmobiliaria", DbType.String, vfiltroMueble.matriculaInmobiliaria);
                    vDataBase.AddInParameter(vDbCommand, "@CedulaCatastral", DbType.Int64, vfiltroMueble.cedulaCatastral);
                    vDataBase.AddInParameter(vDbCommand, "@MarcaBien", DbType.String, vfiltroMueble.marcaBien);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultFiltroMueble> vResultFiltroMuebleList = new List<ResultFiltroMueble>();
                        while (vDataReaderResults.Read())
                        {
                            ResultFiltroMueble vResultFiltroMueble = new ResultFiltroMueble();
                            vResultFiltroMueble.radicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : string.Empty;
                            string vRazon = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : string.Empty;
                            if (vRazon.Length == 0 || vRazon == string.Empty)
                            {
                                string PrimerNombre = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : string.Empty;
                                string PrimerApellido = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : string.Empty;
                                string SegundoNombre = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : string.Empty;
                                string SegundoApellido = vDataReaderResults["SEGUNDOAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDOAPELLIDO"].ToString()) : string.Empty;
                                vResultFiltroMueble.NombRazonSocial = string.Concat(PrimerNombre, " ", SegundoNombre, " ", PrimerApellido, " ", SegundoApellido);
                            }
                            else
                            {
                                vResultFiltroMueble.NombRazonSocial = vRazon;
                            }
                            vResultFiltroMueble.fechaRadicado = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vResultFiltroMueble.fechaRadicado;
                            vResultFiltroMueble.tipoBien = (vDataReaderResults["TIPOBIEN"] != DBNull.Value) ? Convert.ToString(vDataReaderResults["TipoBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.subTipoBien = (vDataReaderResults["SubtipoBien"] != DBNull.Value) ? Convert.ToString(vDataReaderResults["SubtipoBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.ClaseBien = (vDataReaderResults["ClaseBien"] != DBNull.Value) ? Convert.ToString(vDataReaderResults["ClaseBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.matriculaInmobiliaria = vDataReaderResults["MatriculaInmobiliaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MatriculaInmobiliaria"].ToString()) : string.Empty;
                            vResultFiltroMueble.cedulaCatastral = vDataReaderResults["CedulaCatastral"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CedulaCatastral"].ToString()) : string.Empty;
                            vResultFiltroMueble.nroIdentifica = vDataReaderResults["NumeroIdentificacionINMueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionINMueble"].ToString()) : string.Empty;
                            vResultFiltroMueble.observaciones = vDataReaderResults["DescripcionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.DepartamentoSeven = vDataReaderResults["iddepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["iddepartamento"].ToString()) : string.Empty;
                            vResultFiltroMueble.MunicipioSeven = vDataReaderResults["idmunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["idmunicipio"].ToString()) : string.Empty;
                            vResultFiltroMueble.IdMuebleInmueble = vDataReaderResults["IdMuebleInmueble"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMuebleInmueble"].ToString()) : vResultFiltroMueble.IdMuebleInmueble;
                            vResultFiltroMueble.FechaDenuncia = vDataReaderResults["FechaDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDenuncia"].ToString()) : vResultFiltroMueble.FechaDenuncia;
                            vResultFiltroMuebleList.Add(vResultFiltroMueble);
                        }
                        return vResultFiltroMuebleList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta filtroTitulo
        /// </summary>
        /// <returns>List tipo resultFiltroTitulo con la información consultada</returns>
        public List<ResultFiltroTitulo> filtroTitulo(FiltroTitulo vfiltroTitulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_FiltroTitulo"))
                {
                    // vDataBase.AddInParameter(vDbCommand, "@RadicadoDenuncia", DbType.String, vfiltroTitulo.RadicadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@TipoTitulo", DbType.String, vfiltroTitulo.tipoTitulo);
                    vDataBase.AddInParameter(vDbCommand, "@CuentaBancaria", DbType.String, vfiltroTitulo.nroCuentaBancaria);
                    vDataBase.AddInParameter(vDbCommand, "@TituloValor", DbType.String, vfiltroTitulo.nroTituloValor);
                    vDataBase.AddInParameter(vDbCommand, "@Entidad", DbType.String, vfiltroTitulo.entidadEmisora);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultFiltroTitulo> vResultFiltroTituloList = new List<ResultFiltroTitulo>();
                        while (vDataReaderResults.Read())
                        {
                            ResultFiltroTitulo vResultFiltroTitulo = new ResultFiltroTitulo();

                            vResultFiltroTitulo.FechaDenuncia = vDataReaderResults["FechaDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDenuncia"].ToString()) : vResultFiltroTitulo.FechaDenuncia;

                            vResultFiltroTitulo.IdTituloValor = vDataReaderResults["IdTituloValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTituloValor"]) : 0;

                            vResultFiltroTitulo.radicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : string.Empty;

                            vResultFiltroTitulo.fechaRadicado = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vResultFiltroTitulo.fechaRadicado;

                            vResultFiltroTitulo.tipoTitulo = vDataReaderResults["NombreTipoTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoTitulo"].ToString()) : string.Empty;

                            vResultFiltroTitulo.cuentaBancaria = vDataReaderResults["NumeroCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCuentaBancaria"].ToString()) : string.Empty;

                            vResultFiltroTitulo.observaciones = vDataReaderResults["DescripcionTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTitulo"].ToString()) : string.Empty;

                            vResultFiltroTitulo.nroTituloValor = vDataReaderResults["NumeroTituloValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTituloValor"].ToString()) : string.Empty;

                            string vRazon = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : string.Empty;

                            if (vRazon.Length == 0 || vRazon == string.Empty)
                            {
                                string PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : string.Empty;

                                string PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : string.Empty;

                                string SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : string.Empty;

                                string SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : string.Empty;

                                vResultFiltroTitulo.NombRazonSocial = string.Concat(PrimerNombre, ' ', SegundoNombre, ' ', PrimerApellido, ' ', SegundoApellido);
                            }
                            else
                            {
                                vResultFiltroTitulo.NombRazonSocial = vRazon;
                            }

                            vResultFiltroTituloList.Add(vResultFiltroTitulo);
                        }

                        return vResultFiltroTituloList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta filtroDenunciante
        /// </summary>
        /// <returns>List tipo resultFiltroDenunciante con la información consultada</returns>
        public List<ResultFiltroDenunciante> filtroDenunciante(FiltroDenunciante vfiltroDenunciante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_FiltroDenuncianteCausante"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, vfiltroDenunciante.TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NroIdentificacion", DbType.String, vfiltroDenunciante.NroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombRazonSocial", DbType.String, vfiltroDenunciante.RazonSocial);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultFiltroDenunciante> vMarcaBienList = new List<ResultFiltroDenunciante>();
                        while (vDataReaderResults.Read())
                        {
                            ResultFiltroDenunciante vresultFiltroDenunciante = new ResultFiltroDenunciante();
                            vresultFiltroDenunciante.radicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : string.Empty;
                            vresultFiltroDenunciante.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : string.Empty;
                            string vRazon = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : string.Empty;
                            if (vRazon.Length == 0 || vRazon == string.Empty)
                            {
                                vresultFiltroDenunciante.RazonSocial = vDataReaderResults["NombreComp"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreComp"].ToString()) : string.Empty;
                            }
                            else
                            {
                                vresultFiltroDenunciante.RazonSocial = vRazon;
                            }
                            vresultFiltroDenunciante.fechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vresultFiltroDenunciante.fechaRadicadoDenuncia;
                            vMarcaBienList.Add(vresultFiltroDenunciante);
                        }
                        return vMarcaBienList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta filtroDenunciante
        /// </summary>
        /// <returns>List tipo resultFiltroDenunciante con la información consultada</returns>
        public List<ResultFiltroDenunciante> filtroCausante(FiltroDenunciante vfiltroDenunciante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_FiltroCausante"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, vfiltroDenunciante.TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NroIdentificacion", DbType.String, vfiltroDenunciante.NroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombRazonSocial", DbType.String, vfiltroDenunciante.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@NroRegDefuncion", DbType.Int64, Convert.ToInt64(vfiltroDenunciante.RegistroDefuncion));


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultFiltroDenunciante> vMarcaBienList = new List<ResultFiltroDenunciante>();
                        while (vDataReaderResults.Read())
                        {
                            ResultFiltroDenunciante vresultFiltroDenunciante = new ResultFiltroDenunciante();
                            vresultFiltroDenunciante.radicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : string.Empty;
                            vresultFiltroDenunciante.TipoIdentificacion = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : string.Empty;
                            string vRazon = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : string.Empty;
                            if (vRazon.Length == 0 || vRazon == string.Empty)
                            {
                                vresultFiltroDenunciante.RazonSocial = vDataReaderResults["NombreComp"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreComp"].ToString()) : string.Empty;
                            }
                            else
                            {
                                vresultFiltroDenunciante.RazonSocial = vRazon;
                            }
                            vresultFiltroDenunciante.fechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vresultFiltroDenunciante.fechaRadicadoDenuncia;
                            vresultFiltroDenunciante.NroRegDefuncion = vDataReaderResults["NumeroRegistroDefuncion"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["NumeroRegistroDefuncion"].ToString()) : vresultFiltroDenunciante.NroRegDefuncion;
                            vresultFiltroDenunciante.fechaDefuncion = vDataReaderResults["FechaDefuncion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDefuncion"].ToString()) : vresultFiltroDenunciante.fechaDefuncion;
                            vMarcaBienList.Add(vresultFiltroDenunciante);
                        }
                        return vMarcaBienList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta BusquedaMuebleInmueble
        /// </summary>
        /// <returns>List tipo ResultFiltroMueble con la información consultada</returns>
        public List<ResultFiltroMueble> BusquedaMuebleInmueble(string vInStringRadicado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_BusquedaInmueble"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Radicado", DbType.String, vInStringRadicado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultFiltroMueble> vResultFiltroMuebleList = new List<ResultFiltroMueble>();
                        while (vDataReaderResults.Read())
                        {
                            ResultFiltroMueble vResultFiltroMueble = new ResultFiltroMueble();
                            vResultFiltroMueble.tipoBien = vDataReaderResults["TipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.subTipoBien = vDataReaderResults["subTipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["subTipoBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.ClaseBien = vDataReaderResults["ClaseBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.DepartamentoSeven = vDataReaderResults["iddepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["iddepartamento"].ToString()) : string.Empty;
                            vResultFiltroMueble.MunicipioSeven = vDataReaderResults["idmunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["idmunicipio"].ToString()) : string.Empty;
                            vResultFiltroMueble.EstadoFisicoBien = vDataReaderResults["EstadoFisicoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoFisicoBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.ValorEstimadoComercial = vDataReaderResults["ValorEstimadoComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorEstimadoComercial"].ToString()) : string.Empty;
                            vResultFiltroMueble.PorcentajePertenenciaBien = vDataReaderResults["PorcentajePertenenciaBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajePertenenciaBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.DestinacionEconomica = vDataReaderResults["DestinacionEconomica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DestinacionEconomica"].ToString()) : string.Empty;
                            vResultFiltroMueble.matriculaInmobiliaria = vDataReaderResults["MatriculaInmobiliaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MatriculaInmobiliaria"].ToString()) : string.Empty;
                            vResultFiltroMueble.cedulaCatastral = vDataReaderResults["CedulaCatastral"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CedulaCatastral"].ToString()) : string.Empty;
                            vResultFiltroMueble.DireccionInmueble = vDataReaderResults["DireccionInmueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionInmueble"].ToString()) : string.Empty;
                            vResultFiltroMueble.nroIdentifica = vDataReaderResults["NumeroIdentificacionInMueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionInMueble"].ToString()) : string.Empty;
                            vResultFiltroMueble.MarcaBien = vDataReaderResults["MarcaBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MarcaBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"]) : vResultFiltroMueble.FechaRegistro;
                            vResultFiltroMueble.DescripcionBien = vDataReaderResults["DescripcionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBien"].ToString()) : string.Empty;
                            vResultFiltroMueble.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"]) : vResultFiltroMueble.FechaRegistro;
                            vResultFiltroMueble.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : string.Empty;
                            vResultFiltroMueble.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vResultFiltroMueble.FechaRegistro;
                            vResultFiltroMueble.estado = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"]) : "REGISTRO DENUNCIA";

                            vResultFiltroMuebleList.Add(vResultFiltroMueble);
                        }
                        return vResultFiltroMuebleList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta BusquedaTitulo
        /// </summary>
        /// <returns>List tipo ResultFiltroMueble con la información consultada</returns>
        public List<ResultFiltroTitulo> BusquedaTitulo(string vInStringRadicado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_BusquedaTitulo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTituloValor", DbType.String, vInStringRadicado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultFiltroTitulo> vResultFiltroTituloList = new List<ResultFiltroTitulo>();
                        while (vDataReaderResults.Read())
                        {
                            ResultFiltroTitulo vResultFiltroTitulo = new ResultFiltroTitulo();
                            vResultFiltroTitulo.nombreTitulo = vDataReaderResults["nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["nombre"].ToString()) : string.Empty;
                            vResultFiltroTitulo.cuentaBancaria = vDataReaderResults["NumeroCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCuentaBancaria"].ToString()) : string.Empty;
                            vResultFiltroTitulo.ValorEfectivoEstimado = vDataReaderResults["ValorEfectivoEstimado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorEfectivoEstimado"].ToString()) : string.Empty;
                            vResultFiltroTitulo.EntidadBancaria = vDataReaderResults["EntidadBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadBancaria"].ToString()) : string.Empty;
                            vResultFiltroTitulo.TipoCuentaBancaria = vDataReaderResults["TipoCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCuentaBancaria"].ToString()) : string.Empty;
                            vResultFiltroTitulo.Departamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : string.Empty;
                            vResultFiltroTitulo.Municipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : string.Empty;
                            vResultFiltroTitulo.EntidadEmisora = vDataReaderResults["IdEntidadEmisora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdEntidadEmisora"].ToString()) : string.Empty;
                            vResultFiltroTitulo.TipoIdentificacionEntidadEmisora = vDataReaderResults["IdTipoIdentificacionEntidadEmisora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoIdentificacionEntidadEmisora"].ToString()) : string.Empty;
                            vResultFiltroTitulo.nroTituloValor = vDataReaderResults["NumeroTituloValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTituloValor"].ToString()) : string.Empty;
                            vResultFiltroTitulo.NumeroIdentificacionBien = vDataReaderResults["NumeroIdentificacionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionBien"].ToString()) : string.Empty;
                            vResultFiltroTitulo.CantidadTitulos = vDataReaderResults["CantidadTitulos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CantidadTitulos"].ToString()) : string.Empty;
                            vResultFiltroTitulo.ValorUnitarioTitulo = vDataReaderResults["ValorUnidadTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorUnidadTitulo"].ToString()) : string.Empty;
                            vResultFiltroTitulo.FechaVencimiento = vDataReaderResults["FechaVencimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimiento"].ToString()) : vResultFiltroTitulo.FechaVencimiento;
                            vResultFiltroTitulo.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vResultFiltroTitulo.FechaRecibido;
                            vResultFiltroTitulo.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vResultFiltroTitulo.FechaVenta;
                            vResultFiltroTitulo.CantidadVendida = (!vDataReaderResults["CantidadVendida"].ToString().Equals(string.Empty)) ? Convert.ToString(vDataReaderResults["CantidadVendida"].ToString()) : string.Empty;
                            vResultFiltroTitulo.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : string.Empty;
                            vResultFiltroTitulo.DescripcionTitulo = vDataReaderResults["DescripcionTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTitulo"].ToString()) : string.Empty;
                            vResultFiltroTitulo.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vResultFiltroTitulo.FechaAdjudicado;
                            vResultFiltroTitulo.estado = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"]) : "REGISTRO DENUNCIA";
                            vResultFiltroTituloList.Add(vResultFiltroTitulo);
                        }
                        return vResultFiltroTituloList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta BusquedaTitulo
        /// </summary>
        /// <returns>List tipo ResultFiltroMueble con la información consultada</returns>
        public void Sino(bool ValorSiNo, int Iddenuncia)
        {
            try
            {
                int Existe = (ValorSiNo) ? 1 : 0;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ActualizarDenunciaSiNo"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@ExisteDenuncia", DbType.Int32, Existe);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, Iddenuncia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}