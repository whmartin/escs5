﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Icbf.Mostrencos.Entity;
using Icbf.Seguridad.Entity;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase que permite el enlace entre el DAL y la BD 
    /// </summary>
    /// <seealso cref="Icbf.Mostrencos.DataAccess.GeneralDAL" />
    public class DespachoJudicialDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DespachoJudicialDAL"/> class.
        /// </summary>
        public DespachoJudicialDAL()
        {
        }

        /// <summary>
        /// Consultar Despachos.
        /// </summary>
        /// <param name="pNombre">The p Nombre Despacho.</param>
        /// <param name="pDepartamento">The p Departamento.</param>
        /// <param name="pMunicipio">The p Municipio.</param>
        /// <param name="pEstado">The p Estado.</param>
        /// <returns>Lista de medios de comunicación filtrada por nombre o estado</returns>
        /// <exception cref="GenericException"></exception>
        public List<DespachoJudicial> ConsultarDespachoJudicial(string pNombre, int pDepartamento, int pMunicipio, bool? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DespachoJudicial_ConsultarDespachoJudicial"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    vDataBase.AddInParameter(vDbCommand, "@Departamento", DbType.Int32, pDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@Municipio", DbType.Int32, pMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DespachoJudicial> vListaDespachoJudicial = new List<DespachoJudicial>();
                        while (vDataReaderResults.Read())
                        {
                            DespachoJudicial vDespachoJudicial = new DespachoJudicial();
                            vDespachoJudicial.Departamento = new Departamento();
                            vDespachoJudicial.Municipio = new Municipio();
                            vDespachoJudicial.IdDespachoJudicial = vDataReaderResults["IdDespachoJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDespachoJudicial"].ToString()) : vDespachoJudicial.IdDespachoJudicial;
                            vDespachoJudicial.Departamento.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vDespachoJudicial.Departamento.IdDepartamento;
                            vDespachoJudicial.Departamento.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDespachoJudicial.Departamento.NombreDepartamento;
                            vDespachoJudicial.Municipio.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vDespachoJudicial.Municipio.IdMunicipio;
                            vDespachoJudicial.Municipio.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vDespachoJudicial.Municipio.NombreMunicipio;
                            vDespachoJudicial.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vDespachoJudicial.Nombre;
                            vDespachoJudicial.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDespachoJudicial.Descripcion;
                            vDespachoJudicial.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vDespachoJudicial.Estado;
                            vListaDespachoJudicial.Add(vDespachoJudicial);
                        }
                        return vListaDespachoJudicial.OrderBy(a => a.Nombre).ToList();
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar todos los Despachos.
        /// </summary>
        /// <returns> List de Tipo DespachoJudicial</returns>
        /// <exception cref="GenericException"></exception>
        public List<DespachoJudicial> ConsultarDespachoJudicialTodos()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_ConsultarDespachoJudicialTodos"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DespachoJudicial> vListaDespachoJudicial = new List<DespachoJudicial>();
                        while (vDataReaderResults.Read())
                        {
                            DespachoJudicial vDespachoJudicial = new DespachoJudicial();
                            vDespachoJudicial.Departamento = new Departamento();
                            vDespachoJudicial.Municipio = new Municipio();
                            vDespachoJudicial.IdDespachoJudicial = vDataReaderResults["IdDespachoJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDespachoJudicial"].ToString()) : vDespachoJudicial.IdDespachoJudicial;
                            vDespachoJudicial.Departamento.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDespachoJudicial.Departamento.NombreDepartamento;
                            vDespachoJudicial.Municipio.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vDespachoJudicial.Municipio.NombreMunicipio;
                            vDespachoJudicial.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vDespachoJudicial.Nombre;
                            vListaDespachoJudicial.Add(vDespachoJudicial);
                        }
                        return vListaDespachoJudicial;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Ingresar Despachos.
        /// </summary>
        /// <param name="pDespachoJudicial">The p Despacho.</param>
        /// <returns>0 si el nombre se encuentra duplicado y 1 si se agregó correctamente</returns>
        /// <exception cref="GenericException"></exception>
        public int IngresarDespachoJudicial(DespachoJudicial pDespachoJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DespachoJudicial_CrearDespachoJudicial"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pDespachoJudicial.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDespachoJudicial.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Departamento", DbType.Int32, pDespachoJudicial.Departamento.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@Municipio", DbType.Int32, pDespachoJudicial.Municipio.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pDespachoJudicial.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDespachoJudicial.UsuarioCrea);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int existeDespacho = 0;
                        while (vDataReaderResults.Read())
                        {
                            existeDespacho = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Count"].ToString()) : existeDespacho;
                            if (existeDespacho > 0)
                            {
                                EntityAuditoria vAuditoria = (EntityAuditoria)pDespachoJudicial;
                                vAuditoria.ProgramaGeneraLog = true;
                                vAuditoria.Operacion = "INSERT";
                                this.GenerarLogAuditoria(pDespachoJudicial, vDbCommand);
                            }
                        }
                        return existeDespacho;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar despacho judicial por id.
        /// </summary>
        /// <param name="pIdDespacho">The p Id despacho.</param>
        /// <returns>despacho judicial consultado en detalle</returns>
        /// <exception cref="GenericException"></exception>
        public DespachoJudicial ConsultarDespachoJudicialPorId(int pIdDespacho)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DespachoJudicial_ConsultarDespachoJudicialPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDespacho", DbType.Int32, pIdDespacho);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DespachoJudicial vDespachoJudicial = new DespachoJudicial();
                        while (vDataReaderResults.Read())
                        {
                            vDespachoJudicial.Departamento = new Departamento();
                            vDespachoJudicial.Municipio = new Municipio();
                            vDespachoJudicial.IdDespachoJudicial = vDataReaderResults["IdDespachoJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDespachoJudicial"].ToString()) : vDespachoJudicial.IdDespachoJudicial;
                            vDespachoJudicial.Departamento.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vDespachoJudicial.Departamento.IdDepartamento;
                            vDespachoJudicial.Departamento.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDespachoJudicial.Departamento.NombreDepartamento;
                            vDespachoJudicial.Municipio.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vDespachoJudicial.Municipio.IdMunicipio;
                            vDespachoJudicial.Municipio.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vDespachoJudicial.Municipio.NombreMunicipio;
                            vDespachoJudicial.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vDespachoJudicial.Nombre;
                            vDespachoJudicial.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDespachoJudicial.Descripcion;
                            vDespachoJudicial.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vDespachoJudicial.Estado;
                        }
                        return vDespachoJudicial;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza despacho judicial editado.
        /// </summary>
        /// <param name="pDespachoJudicial">The p despacho judicial con los datos modificados.</param>
        /// <returns>Retorna 1 si el nombre se encuentra duplicado y 0 si se actualizó correctamente</returns>
        /// <exception cref="GenericException"></exception>
        public int ActualizarDespachoJudicial(DespachoJudicial pDespachoJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DespachoJudicial_ActualizarDespachoJudicial"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDespacho", DbType.Int32, pDespachoJudicial.IdDespachoJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pDespachoJudicial.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDespachoJudicial.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Departamento", DbType.Int32, pDespachoJudicial.Departamento.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@Municipio", DbType.Int32, pDespachoJudicial.Municipio.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Byte, pDespachoJudicial.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDespachoJudicial.UsuarioModifica);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int existeDespacho = 0;
                        while (vDataReaderResults.Read())
                        {
                            existeDespacho = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Count"].ToString()) : existeDespacho;
                            if (existeDespacho == 0)
                            {
                                Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pDespachoJudicial;
                                vAuditoria.ProgramaGeneraLog = true;
                                vAuditoria.Operacion = "UPDATE";
                                this.GenerarLogAuditoria(pDespachoJudicial, vDbCommand);
                            }
                        }
                        return existeDespacho;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
