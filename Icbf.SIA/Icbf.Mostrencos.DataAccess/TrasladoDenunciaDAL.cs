﻿
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class TrasladoDenunciaDAL : GeneralDAL
    {
        /// <summary>
        /// consulta los traslados de las denuncias por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo TrasladoDenuncia con la información consultada </returns>
        public List<TrasladoDenuncia> ConsultarTrasladoDenunciaPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TrasladoDenuncia_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TrasladoDenuncia> vListaTrasladoDenuncia = new List<TrasladoDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            TrasladoDenuncia vTrasladoDenuncia = new TrasladoDenuncia();
                            vTrasladoDenuncia.DescripcionMotivo = vDataReaderResults["DescripcionMotivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionMotivo"].ToString()) : vTrasladoDenuncia.DescripcionMotivo;
                            vTrasladoDenuncia.IdEstadoTraslado = vDataReaderResults["IdEstadoTraslado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTraslado"]) : vTrasladoDenuncia.IdEstadoTraslado;
                            vTrasladoDenuncia.IdTrasladoDenuncia = vDataReaderResults["IdTrasladoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTrasladoDenuncia"].ToString()) : vTrasladoDenuncia.IdTrasladoDenuncia;
                            vTrasladoDenuncia.IdRegionalOrigen = vDataReaderResults["IdRegionalOrigen"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalOrigen"]) : vTrasladoDenuncia.IdRegionalOrigen;
                            vTrasladoDenuncia.IdRegionalTraslada = vDataReaderResults["IdRegionalTraslada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalTraslada"].ToString()) : vTrasladoDenuncia.IdRegionalTraslada;
                            vTrasladoDenuncia.FechaTraslado = vDataReaderResults["FechaTraslado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTraslado"].ToString()) : vTrasladoDenuncia.FechaTraslado;
                            vTrasladoDenuncia.MotivoTraslado = vDataReaderResults["MotivoTraslado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoTraslado"].ToString()) : vTrasladoDenuncia.MotivoTraslado;
                            vTrasladoDenuncia.NumeroTraslado = vDataReaderResults["NumeroTraslado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroTraslado"]) : vTrasladoDenuncia.NumeroTraslado;
                            vTrasladoDenuncia.ObservacionesDestino = vDataReaderResults["ObservacionesDestino"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDestino"].ToString()) : vTrasladoDenuncia.ObservacionesDestino;
                            vTrasladoDenuncia.ObservacionesOrigen = vDataReaderResults["ObservacionesOrigen"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesOrigen"].ToString()) : vTrasladoDenuncia.ObservacionesOrigen;
                            vTrasladoDenuncia.SolicitudTrasladoAprobadoDestino = vDataReaderResults["SolicitudTrasladoAprobadoDestino"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SolicitudTrasladoAprobadoDestino"].ToString()) : vTrasladoDenuncia.SolicitudTrasladoAprobadoDestino;
                            vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen = vDataReaderResults["SolicitudTrasladoAprobadoOrigen"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SolicitudTrasladoAprobadoOrigen"]) : vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen;
                            vTrasladoDenuncia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTrasladoDenuncia.UsuarioCrea;
                            vTrasladoDenuncia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTrasladoDenuncia.FechaCrea;
                            vTrasladoDenuncia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTrasladoDenuncia.UsuarioModifica;
                            vTrasladoDenuncia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTrasladoDenuncia.FechaModifica;
                            vTrasladoDenuncia.IdDenunciaBien = pIdDenunciaBien;
                            vListaTrasladoDenuncia.Add(vTrasladoDenuncia);
                            vTrasladoDenuncia = null;
                        }

                        return vListaTrasladoDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los traslados denuncia por varios parámetros
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia que contiene la información a consultar </param>
        /// <returns>lista de tipo TrasladoDenuncia con la información resultante de la consulta</returns>
        public List<TrasladoDenuncia> ConsultarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TrasladoDenuncia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pTrasladoDenuncia.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTraslado", DbType.Int32, pTrasladoDenuncia.NumeroTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTraslado", DbType.DateTime, pTrasladoDenuncia.FechaTraslado.Date);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoTraslado", DbType.String, pTrasladoDenuncia.MotivoTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalTraslada", DbType.Int32, pTrasladoDenuncia.IdRegionalTraslada);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TrasladoDenuncia> vListaTrasladoDenuncia = new List<TrasladoDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            TrasladoDenuncia vTrasladoDenuncia = new TrasladoDenuncia();
                            vTrasladoDenuncia.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vTrasladoDenuncia.IdDenunciaBien;
                            vTrasladoDenuncia.DescripcionMotivo = vDataReaderResults["DescripcionMotivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionMotivo"].ToString()) : vTrasladoDenuncia.DescripcionMotivo;
                            vTrasladoDenuncia.IdEstadoTraslado = vDataReaderResults["IdEstadoTraslado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTraslado"]) : vTrasladoDenuncia.IdEstadoTraslado;
                            vTrasladoDenuncia.IdTrasladoDenuncia = vDataReaderResults["IdTrasladoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTrasladoDenuncia"].ToString()) : vTrasladoDenuncia.IdTrasladoDenuncia;
                            vTrasladoDenuncia.IdRegionalOrigen = vDataReaderResults["IdRegionalOrigen"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalOrigen"]) : vTrasladoDenuncia.IdRegionalOrigen;
                            vTrasladoDenuncia.IdRegionalTraslada = vDataReaderResults["IdRegionalTraslada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalTraslada"].ToString()) : vTrasladoDenuncia.IdRegionalTraslada;
                            vTrasladoDenuncia.FechaTraslado = vDataReaderResults["FechaTraslado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTraslado"].ToString()) : vTrasladoDenuncia.FechaTraslado;
                            vTrasladoDenuncia.MotivoTraslado = vDataReaderResults["MotivoTraslado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoTraslado"].ToString()) : vTrasladoDenuncia.MotivoTraslado;
                            vTrasladoDenuncia.NumeroTraslado = vDataReaderResults["NumeroTraslado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroTraslado"]) : vTrasladoDenuncia.NumeroTraslado;
                            vTrasladoDenuncia.ObservacionesDestino = vDataReaderResults["ObservacionesDestino"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDestino"].ToString()) : vTrasladoDenuncia.ObservacionesDestino;
                            vTrasladoDenuncia.ObservacionesOrigen = vDataReaderResults["ObservacionesOrigen"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesOrigen"].ToString()) : vTrasladoDenuncia.ObservacionesOrigen;
                            vTrasladoDenuncia.SolicitudTrasladoAprobadoDestino = vDataReaderResults["SolicitudTrasladoAprobadoDestino"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SolicitudTrasladoAprobadoDestino"].ToString()) : vTrasladoDenuncia.SolicitudTrasladoAprobadoDestino;
                            vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen = vDataReaderResults["SolicitudTrasladoAprobadoOrigen"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SolicitudTrasladoAprobadoOrigen"]) : vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen;
                            vTrasladoDenuncia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTrasladoDenuncia.UsuarioCrea;
                            vTrasladoDenuncia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTrasladoDenuncia.FechaCrea;
                            vTrasladoDenuncia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTrasladoDenuncia.UsuarioModifica;
                            vTrasladoDenuncia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTrasladoDenuncia.FechaModifica;
                            vListaTrasladoDenuncia.Add(vTrasladoDenuncia);
                            vTrasladoDenuncia = null;
                        }

                        return vListaTrasladoDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo traslado de la denuncia
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia con la información a insertar</param>
        /// <returns>id del TrasladoDenuncia que se inserto</returns>
        public int InsertarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia, HistoricoEstadosDenunciaBien pHistorico)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TrasladoDenuncia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTrasladoDenuncia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pTrasladoDenuncia.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoTraslado", DbType.Int32, pTrasladoDenuncia.IdEstadoTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTraslado", DbType.DateTime, pTrasladoDenuncia.FechaTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoTraslado", DbType.String, pTrasladoDenuncia.MotivoTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalOrigen", DbType.Int32, pTrasladoDenuncia.IdRegionalOrigen);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalTraslada", DbType.Int32, pTrasladoDenuncia.IdRegionalTraslada);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionMotivo", DbType.String, pTrasladoDenuncia.DescripcionMotivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTrasladoDenuncia.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pHistorico.IdUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@Fase", DbType.Int32, pHistorico.IdFase);
                    vDataBase.AddInParameter(vDbCommand, "@Accion", DbType.Int32, pHistorico.IdAccion);
                    vDataBase.AddInParameter(vDbCommand, "@Actuacion", DbType.Int32, pHistorico.IdActuacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTrasladoDenuncia"));
                        pTrasladoDenuncia.IdTrasladoDenuncia = vResultado;
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTrasladoDenuncia;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pTrasladoDenuncia, vDbCommand);
                        return vResultado;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// edita un TrasladoDenuncia
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia con la información que se va aguardar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TrasladoDenuncia_Editar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTrasladoDenuncia", DbType.Int32, pTrasladoDenuncia.IdTrasladoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@SolicitudTrasladoAprobadoOrigen", DbType.String, pTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen);
                    vDataBase.AddInParameter(vDbCommand, "@SolicitudTrasladoAprobadoDestino", DbType.String, pTrasladoDenuncia.SolicitudTrasladoAprobadoDestino);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesOrigen", DbType.String, pTrasladoDenuncia.ObservacionesOrigen);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDestino", DbType.String, pTrasladoDenuncia.ObservacionesDestino);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoTraslado", DbType.Int32, pTrasladoDenuncia.IdEstadoTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTraslado", DbType.Int32, pTrasladoDenuncia.NumeroTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTraslado", DbType.DateTime, pTrasladoDenuncia.FechaTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoTraslado", DbType.String, pTrasladoDenuncia.MotivoTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalTraslada", DbType.Int32, pTrasladoDenuncia.IdRegionalTraslada);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionMotivo", DbType.String, pTrasladoDenuncia.DescripcionMotivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTrasladoDenuncia.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTrasladoDenuncia;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pTrasladoDenuncia, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// edita un TrasladoDenuncia, cambia el estado de la denuncia  e inserta en el historial de estados de la denuncia 
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia con la información que se va aguardar</param>
        /// <param name="pDenunciabien">variable de tipo DenunciaBien con la información que se va aguardar</param>
        /// <param name="pHistorico">variable de tipo HistóricoEstadosDenunciaBien con la información que se va aguardar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia, DenunciaBien pDenunciabien, HistoricoEstadosDenunciaBien pHistorico)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TrasladoDenuncia_EditarCompleto"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pHistorico.IdUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalUbicacion", DbType.Int32, pDenunciabien.IdRegionalUbicacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDenunciabien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTrasladoDenuncia", DbType.Int32, pTrasladoDenuncia.IdTrasladoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@SolicitudTrasladoAprobadoOrigen", DbType.String, pTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen);
                    vDataBase.AddInParameter(vDbCommand, "@SolicitudTrasladoAprobadoDestino", DbType.String, pTrasladoDenuncia.SolicitudTrasladoAprobadoDestino);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesOrigen", DbType.String, pTrasladoDenuncia.ObservacionesOrigen);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDestino", DbType.String, pTrasladoDenuncia.ObservacionesDestino);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoTraslado", DbType.Int32, pTrasladoDenuncia.IdEstadoTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTraslado", DbType.Int32, pTrasladoDenuncia.NumeroTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTraslado", DbType.DateTime, pTrasladoDenuncia.FechaTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@MotivoTraslado", DbType.String, pTrasladoDenuncia.MotivoTraslado);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalTraslada", DbType.Int32, pTrasladoDenuncia.IdRegionalTraslada);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionMotivo", DbType.String, pTrasladoDenuncia.DescripcionMotivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTrasladoDenuncia.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@IdFase", DbType.Int32, pHistorico.IdFase);
                    vDataBase.AddInParameter(vDbCommand, "@IdAccion", DbType.Int32, pHistorico.IdAccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdActuacion", DbType.Int32, pHistorico.IdActuacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pTrasladoDenuncia;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pTrasladoDenuncia, vDbCommand);
                        vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pDenunciabien;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pDenunciabien, vDbCommand);
                        vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pHistorico;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pHistorico, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta el ultimo numero consecutivo de los traslados
        /// </summary>
        /// <returns>ultimo numero consecutivo de los traslados</returns>
        public int ConsultarUltimoNumerotraslado()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TrasladoDenuncia_ConsultarUltimoNumeroTraslado"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vUltimoNumeroTraslado = 0;
                        while (vDataReaderResults.Read())
                        {
                            vUltimoNumeroTraslado = vDataReaderResults["NumeroTraslado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroTraslado"]) : vUltimoNumeroTraslado;
                        }

                        return vUltimoNumeroTraslado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta el TrasladoDenuncia por el idTrasladoDenuncia
        /// </summary>
        /// <param name="pIdTrasladoDenuncia">idTrasladoDenuncia a consultar</param>
        /// <returns>variable de tipo TrasladoDenuncia con la información consultada</returns>
        public TrasladoDenuncia ConsultarTrasladoDenunciaPorId(int pIdTrasladoDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_TrasladoDenuncia_ConsultarPorIdTrasladoDenuncia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTrasladoDenuncia", DbType.Int32, pIdTrasladoDenuncia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TrasladoDenuncia vTrasladoDenuncia = new TrasladoDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vTrasladoDenuncia.DescripcionMotivo = vDataReaderResults["DescripcionMotivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionMotivo"].ToString()) : vTrasladoDenuncia.DescripcionMotivo;
                            vTrasladoDenuncia.IdEstadoTraslado = vDataReaderResults["IdEstadoTraslado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTraslado"]) : vTrasladoDenuncia.IdEstadoTraslado;
                            vTrasladoDenuncia.IdTrasladoDenuncia = pIdTrasladoDenuncia;
                            vTrasladoDenuncia.FechaTraslado = vDataReaderResults["FechaTraslado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTraslado"].ToString()) : vTrasladoDenuncia.FechaTraslado;
                            vTrasladoDenuncia.IdRegionalTraslada = vDataReaderResults["IdRegionalTraslada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalTraslada"].ToString()) : vTrasladoDenuncia.IdRegionalTraslada;
                            vTrasladoDenuncia.MotivoTraslado = vDataReaderResults["MotivoTraslado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MotivoTraslado"].ToString()) : vTrasladoDenuncia.MotivoTraslado;
                            vTrasladoDenuncia.NumeroTraslado = vDataReaderResults["NumeroTraslado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroTraslado"]) : vTrasladoDenuncia.NumeroTraslado;
                            vTrasladoDenuncia.IdRegionalTraslada = vDataReaderResults["IdRegionalTraslada"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalTraslada"]) : vTrasladoDenuncia.IdRegionalTraslada;
                            vTrasladoDenuncia.IdRegionalOrigen = vDataReaderResults["IdRegionalOrigen"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalOrigen"]) : vTrasladoDenuncia.IdRegionalOrigen;
                            vTrasladoDenuncia.ObservacionesDestino = vDataReaderResults["ObservacionesDestino"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDestino"].ToString()) : vTrasladoDenuncia.ObservacionesDestino;
                            vTrasladoDenuncia.ObservacionesOrigen = vDataReaderResults["ObservacionesOrigen"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesOrigen"].ToString()) : vTrasladoDenuncia.ObservacionesOrigen;
                            vTrasladoDenuncia.SolicitudTrasladoAprobadoDestino = vDataReaderResults["SolicitudTrasladoAprobadoDestino"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SolicitudTrasladoAprobadoDestino"].ToString()) : vTrasladoDenuncia.SolicitudTrasladoAprobadoDestino;
                            vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen = vDataReaderResults["SolicitudTrasladoAprobadoOrigen"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SolicitudTrasladoAprobadoOrigen"]) : vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen;
                            vTrasladoDenuncia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTrasladoDenuncia.UsuarioCrea;
                            vTrasladoDenuncia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTrasladoDenuncia.FechaCrea;
                            vTrasladoDenuncia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTrasladoDenuncia.UsuarioModifica;
                            vTrasladoDenuncia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTrasladoDenuncia.FechaModifica;
                            vTrasladoDenuncia.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vTrasladoDenuncia.IdDenunciaBien;
                        }

                        return vTrasladoDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
