﻿using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.DataAccess
{
    public class RegistrarProtocolizacionDAL : GeneralDAL
    {
        /// <summary>
        /// Consultar Info Escrituración
        /// </summary>
        /// <param name="IdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns>Informacion detallada de la escrituracion relacionada con el Bien denunciado</returns>
        public InfoEscrituracion ConsultarDatosInfoEscrituracion(int IdDenunciaBien)
        {
            try
            {
                InfoEscrituracion vInfoEscrituracion = new InfoEscrituracion();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarProtocolizacion_ConsultarInfoEscrituracion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vInfoEscrituracion.FechaEscritura = vDataReaderResults["FechaEscritura"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEscritura"].ToString()) : vInfoEscrituracion.FechaEscritura;
                            vInfoEscrituracion.NumeroEscritura = vDataReaderResults["NumeroEscritura"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroEscritura"].ToString()) : vInfoEscrituracion.NumeroEscritura;
                            vInfoEscrituracion.Notaria = vDataReaderResults["Notaria"] != DBNull.Value ? (vDataReaderResults["Notaria"].ToString()) : vInfoEscrituracion.Notaria;
                            vInfoEscrituracion.NumeroRegistro = vDataReaderResults["NumeroRegistro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroRegistro"].ToString()) : vInfoEscrituracion.NumeroRegistro;
                            vInfoEscrituracion.FechaIngresoICBF = vDataReaderResults["FechaIngresoICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIngresoICBF"].ToString()) : vInfoEscrituracion.FechaIngresoICBF;
                            vInfoEscrituracion.FechaInstrumentosPublicos = vDataReaderResults["FechaInstrumentosPublicos"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInstrumentosPublicos"].ToString()) : vInfoEscrituracion.FechaInstrumentosPublicos;
                            vInfoEscrituracion.DecisionTramiteNotarialRechazado = vDataReaderResults["DecisionTramiteNotarialRechazado"] != DBNull.Value ? (vDataReaderResults["DecisionTramiteNotarialRechazado"].ToString()) : vInfoEscrituracion.DecisionTramiteNotarialRechazado;
                            vInfoEscrituracion.DecisionTramiteNotarial = vDataReaderResults["DecisionTramiteNotarial"] != DBNull.Value ? (vDataReaderResults["DecisionTramiteNotarial"].ToString()) : vInfoEscrituracion.DecisionTramiteNotarial;
                            vInfoEscrituracion.FechaComunicacionSecretariaHacienda = vDataReaderResults["FechaComunicacionSecretariaHacienda"] != DBNull.Value ? Convert.ToDateTime((vDataReaderResults["FechaComunicacionSecretariaHacienda"].ToString())) : vInfoEscrituracion.FechaComunicacionSecretariaHacienda;
                            vInfoEscrituracion.FechaSolemnizacion = vDataReaderResults["FechaSolemnizacion"] != DBNull.Value ? Convert.ToDateTime((vDataReaderResults["FechaSolemnizacion"].ToString())) : vInfoEscrituracion.FechaComunicacionSecretariaHacienda;
                        }
                    }
                }
                return vInfoEscrituracion;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Info Adjudicación
        /// </summary>
        /// <param name="IdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns>Informacion detallada de los datos de la adjudicacion relacionados con la denuncia del bien</returns>
        public InfoAdjudicacion ConsultarDatosInfoAdjudicacion(int IdDenunciaBien)
        {
            try
            {
                InfoAdjudicacion vInfoAdjudicacion = new InfoAdjudicacion();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarProtocolizacion_ConsultarInfoAdjudicacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vInfoAdjudicacion.IdTipoTramite = vDataReaderResults["IdTipoTramite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoTramite"].ToString()) : vInfoAdjudicacion.IdTipoTramite;
                            vInfoAdjudicacion.SentidoSentencia = vDataReaderResults["SentidoSentencia"] != DBNull.Value ? vDataReaderResults["SentidoSentencia"].ToString() : vInfoAdjudicacion.SentidoSentencia;
                            vInfoAdjudicacion.FechaSentencia = vDataReaderResults["FechaSentencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSentencia"].ToString()) : vInfoAdjudicacion.FechaSentencia;
                            vInfoAdjudicacion.FechaConstanciaEjecutoria = vDataReaderResults["FechaConstanciaEjecutoria"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaConstanciaEjecutoria"].ToString()) : vInfoAdjudicacion.FechaConstanciaEjecutoria;
                            vInfoAdjudicacion.NombreJuzgado = vDataReaderResults["NombreJuzgado"] != DBNull.Value ? vDataReaderResults["NombreJuzgado"].ToString() : vInfoAdjudicacion.NombreJuzgado;
                            vInfoAdjudicacion.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? vDataReaderResults["NombreDepartamento"].ToString() : vInfoAdjudicacion.NombreDepartamento;
                            vInfoAdjudicacion.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? vDataReaderResults["NombreMunicipio"].ToString() : vInfoAdjudicacion.NombreMunicipio;
                        }
                    }
                }
                return vInfoAdjudicacion;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Trámite
        /// </summary>
        /// <param name="IdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns>Tipo de tramite generado 1: Juridico - 2: Notarial</returns>
        public int ConsultarTipoTramite(int IdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarProtocolizacion_ConsultarTipoTramite"))
                {
                    int vResultado, Tipotramite;
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddOutParameter(vDbCommand, "@TipoTramite", DbType.Int32, 18);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    Tipotramite = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@TipoTramite").ToString());
                    return Tipotramite;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar la informacion de los muebles e inmuebles a partir del Id de la denuncia del bien
        /// </summary>
        /// <param name="IdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns>Lista de todos los muebles e inmuebles relacionados con el Id de la denuncia</returns>
        public List<InfoMuebleProtocolizacion> ConsultarInfoMuebleXIdBienDenuncia(int IdDenunciaBien)
        {
            try
            {
                List<InfoMuebleProtocolizacion> vListInfoMueble = new List<InfoMuebleProtocolizacion>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_InfoMueblexIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            InfoMuebleProtocolizacion vInfoMuebleProtocolizacion = new InfoMuebleProtocolizacion();
                            vInfoMuebleProtocolizacion.IdMuebleInmueble = vDataReaderResults["IdMuebleInmueble"] != DBNull.Value ? int.Parse(vDataReaderResults["IdMuebleInmueble"].ToString()) : 0;
                            vInfoMuebleProtocolizacion.TipoBien = vDataReaderResults["TipoBien"] != DBNull.Value ? (vDataReaderResults["TipoBien"].ToString()) : string.Empty;
                            vInfoMuebleProtocolizacion.SubTipoBien = vDataReaderResults["SubTipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SubTipoBien"].ToString()) : "0";
                            vInfoMuebleProtocolizacion.ClaseBien = vDataReaderResults["ClaseBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseBien"].ToString()) : "0";
                            vInfoMuebleProtocolizacion.EstadoBien = vDataReaderResults["EstadoBien"] != DBNull.Value ? vDataReaderResults["EstadoBien"].ToString() : string.Empty;
                            vInfoMuebleProtocolizacion.DescripcionBien = vDataReaderResults["DescripcionBien"] != DBNull.Value ? vDataReaderResults["DescripcionBien"].ToString() : string.Empty;
                            vInfoMuebleProtocolizacion.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vInfoMuebleProtocolizacion.FechaAdjudicado;
                            vInfoMuebleProtocolizacion.FechaEscritura = vDataReaderResults["FechaEscritura"] != DBNull.Value ? vDataReaderResults["FechaEscritura"].ToString() : string.Empty;
                            vInfoMuebleProtocolizacion.NumeroEscritura = vDataReaderResults["NumeroEscritura"] != DBNull.Value ? vDataReaderResults["NumeroEscritura"].ToString() : string.Empty;
                            vInfoMuebleProtocolizacion.Notaria = vDataReaderResults["Notaria"] != DBNull.Value ? vDataReaderResults["Notaria"].ToString() : string.Empty;
                            vInfoMuebleProtocolizacion.NumeroRegistro = vDataReaderResults["NumeroRegistro"] != DBNull.Value ? vDataReaderResults["NumeroRegistro"].ToString() : string.Empty;
                            vInfoMuebleProtocolizacion.FechaIngresoICBF = vDataReaderResults["FechaIngresoICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIngresoICBF"].ToString()) : vInfoMuebleProtocolizacion.FechaIngresoICBF;
                            vInfoMuebleProtocolizacion.FechaInstrumentosPublicos = vDataReaderResults["FechaInstrumentosPublicos"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInstrumentosPublicos"].ToString()) : vInfoMuebleProtocolizacion.FechaInstrumentosPublicos;

                            vListInfoMueble.Add(vInfoMuebleProtocolizacion);
                        }
                    }
                }
                return vListInfoMueble;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar la informacion del titulo de valor a partir del Id de la Denuncia
        /// </summary>
        /// <param name="vInIntIdDenunciaBien"></param>
        /// <returns>Lista de todos los titulos relacionados con el Id de la denuncia</returns>
        public List<ResultFiltroTitulo> ConsultarTituloXIdDenuncia(int vInIntIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_BusquedaTituloXIdDenuncia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, vInIntIdDenunciaBien);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultFiltroTitulo> vResultFiltroTituloList = new List<ResultFiltroTitulo>();
                        while (vDataReaderResults.Read())
                        {
                            ResultFiltroTitulo vResultFiltroTitulo = new ResultFiltroTitulo();
                            vResultFiltroTitulo.IdTituloValor = vDataReaderResults["IdTituloValor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTituloValor"].ToString()) : 0;
                            vResultFiltroTitulo.nombreTitulo = vDataReaderResults["nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["nombre"].ToString()) : string.Empty;
                            vResultFiltroTitulo.cuentaBancaria = vDataReaderResults["NumeroCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCuentaBancaria"].ToString()) : string.Empty;
                            vResultFiltroTitulo.ValorEfectivoEstimado = vDataReaderResults["ValorEfectivoEstimado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorEfectivoEstimado"].ToString()) : string.Empty;
                            vResultFiltroTitulo.EntidadBancaria = vDataReaderResults["EntidadBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadBancaria"].ToString()) : string.Empty;
                            vResultFiltroTitulo.TipoCuentaBancaria = vDataReaderResults["TipoCuentaBancaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoCuentaBancaria"].ToString()) : string.Empty;
                            vResultFiltroTitulo.Departamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : string.Empty;
                            vResultFiltroTitulo.Municipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : string.Empty;
                            vResultFiltroTitulo.EntidadEmisora = vDataReaderResults["IdEntidadEmisora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdEntidadEmisora"].ToString()) : string.Empty;
                            vResultFiltroTitulo.TipoIdentificacionEntidadEmisora = vDataReaderResults["IdTipoIdentificacionEntidadEmisora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTipoIdentificacionEntidadEmisora"].ToString()) : string.Empty;
                            vResultFiltroTitulo.nroTituloValor = vDataReaderResults["NumeroTituloValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTituloValor"].ToString()) : string.Empty;
                            vResultFiltroTitulo.NumeroIdentificacionBien = vDataReaderResults["NumeroIdentificacionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionBien"].ToString()) : string.Empty;
                            vResultFiltroTitulo.CantidadTitulos = vDataReaderResults["CantidadTitulos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CantidadTitulos"].ToString()) : string.Empty;
                            vResultFiltroTitulo.ValorUnitarioTitulo = vDataReaderResults["ValorUnidadTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorUnidadTitulo"].ToString()) : string.Empty;
                            vResultFiltroTitulo.FechaVencimiento = vDataReaderResults["FechaVencimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVencimiento"].ToString()) : vResultFiltroTitulo.FechaVencimiento;
                            vResultFiltroTitulo.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vResultFiltroTitulo.FechaRecibido;
                            vResultFiltroTitulo.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vResultFiltroTitulo.FechaVenta;
                            vResultFiltroTitulo.CantidadVendida = (!vDataReaderResults["CantidadVendida"].ToString().Equals(string.Empty)) ? Convert.ToString(vDataReaderResults["CantidadVendida"].ToString()) : string.Empty;
                            vResultFiltroTitulo.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : string.Empty;
                            vResultFiltroTitulo.DescripcionTitulo = vDataReaderResults["DescripcionTitulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTitulo"].ToString()) : string.Empty;
                            vResultFiltroTitulo.FechaAdjudicado = (vDataReaderResults["FechaAdjudicado"] != DBNull.Value) ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vResultFiltroTitulo.FechaAdjudicado;
                            vResultFiltroTitulo.estado = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"]) : "REGISTRO DENUNCIA";
                            vResultFiltroTituloList.Add(vResultFiltroTitulo);
                        }
                        return vResultFiltroTituloList;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del tramite judicial relacionado con el Id de la denuncia
        /// </summary>
        /// <param name="vDatoTramiteJudicial"></param>
        /// <returns>Numero de filas afectadas en la base de datos tras la actualizacion</returns>
        public int ActualizarTramiteJudicialxIdDenunciaBien(TramiteJudicial vDatoTramiteJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Protocolizacion_ActualizarTramiteJudicial"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@idSentidoSentencia", DbType.Int32, vDatoTramiteJudicial.IdSentidoSentencia);
                    vDataBase.AddInParameter(vDbCommand, "@fechaSentencia", DbType.DateTime, vDatoTramiteJudicial.FechaSentencia);
                    vDataBase.AddInParameter(vDbCommand, "@fechaComunicacion", DbType.DateTime, vDatoTramiteJudicial.FechaComunicacion);
                    vDataBase.AddInParameter(vDbCommand, "@fechaConstanciaEjecutoria", DbType.DateTime, vDatoTramiteJudicial.FechaSentencia);
                    vDataBase.AddInParameter(vDbCommand, "@sentidoSentencia", DbType.String, vDatoTramiteJudicial.SentidoSentencia);
                    vDataBase.AddInParameter(vDbCommand, "@usuarioModifica", DbType.String, vDatoTramiteJudicial.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@fechaModifica", DbType.DateTime, vDatoTramiteJudicial.FechaModifica);
                    vDataBase.AddInParameter(vDbCommand, "@idDenunciaBien", DbType.Int32, vDatoTramiteJudicial.IdDenunciaBien);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vDatoTramiteJudicial;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vDatoTramiteJudicial, vDbCommand);
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del estado del bien relacionado con el ID de la denuncia
        /// </summary>
        /// <param name="vEstadoBien">Estado del bien que se va a actualizar</param>
        /// <param name="vIdDenunciaBien">Id de la denuncia relacionada con el Id del bien</param>
        /// <returns></returns>
        public int ActualizarEstadoBienxIdDenuncia(string vEstadoBien, int vIdDenunciaBien, DateTime? vFechaAdjudica, int vIdMuebleInmueble)
        {
            try
            {
                TramiteNotarial vTramiteNotarial = new TramiteNotarial();
                vTramiteNotarial.IdDenunciaBien = vIdDenunciaBien;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ActualizaEstadoBien"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@vEstadoBien", DbType.String, vEstadoBien);
                    vDataBase.AddInParameter(vDbCommand, "@vIdDenunciaBien", DbType.Int32, vIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@vFechaAdjudicacion", DbType.DateTime, vFechaAdjudica);
                    vDataBase.AddInParameter(vDbCommand, "@vIdMuebleInmueble", DbType.Int32, vIdMuebleInmueble);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vTramiteNotarial;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vTramiteNotarial, vDbCommand);
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del estado del titulo del bien relacionado con el ID de la Denuncia
        /// </summary>
        /// <param name="vEstadoBien">Estado del bien a actualizar para el titulo del valor</param>
        /// <param name="vIdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns></returns>
        public int ActualizarTituloBienxIdDenuncia(string vEstadoBien, int vIdDenunciaBien, DateTime? vFechaAdjudica, int pIdTitulo)
        {
            try
            {
                TituloValor vTituloValor = new TituloValor();
                vTituloValor.IdDenunciaBien = vIdDenunciaBien;
                vTituloValor.EstadoBien = vEstadoBien;
                vTituloValor.IdTituloValor = pIdTitulo;

                int vResultado;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ActualizaTituloValorBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@vEstadoBien", DbType.String, vEstadoBien);
                    vDataBase.AddInParameter(vDbCommand, "@vIdDenunciaBien", DbType.Int32, vIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@vFechaAdjudicacion", DbType.DateTime, vFechaAdjudica);
                    vDataBase.AddInParameter(vDbCommand, "@vIdTitulo", DbType.Int32, pIdTitulo);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)vTituloValor;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(vTituloValor, vDbCommand);
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Actualiza la informacion del estado del titulo del bien relacionado con el ID de la Denuncia
        /// </summary>
        /// <param name="vEstadoBien">Estado del bien a actualizar para el titulo del valor</param>
        /// <param name="vIdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns></returns>
        public int ActualizarFechaComunicacionTitulo(int vIdDenunciaBien, DateTime vFechaAComunicacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ActualizaFechaComunicacionTituloValor"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@vIdDenunciaBien", DbType.Int32, vIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@vFechaComunicacion", DbType.DateTime, vFechaAComunicacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)new TramiteJudicial();
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(new TramiteJudicial(), vDbCommand);
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del estado del titulo del bien relacionado con el ID de la Denuncia
        /// </summary>
        /// <param name="vEstadoBien">Estado del bien a actualizar para el titulo del valor</param>
        /// <param name="vIdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns></returns>
        public int ActualizarFechaComunicacionNotarial(int vIdDenunciaBien, DateTime vFechaAComunicacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ActualizaFechaComunicacionTramiteNotarial"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@vIdDenunciaBien", DbType.Int32, vIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@vFechaComunicacion", DbType.DateTime, vFechaAComunicacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)new TramiteNotarial();
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(new TramiteNotarial(), vDbCommand);
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del tramite notarial relacionado con el Id de la denuncia
        /// </summary>
        /// <param name="vIdDenunciaBien">ID de la Denuncia relacionada con el Bien</param>
        /// <param name="vFechaEscritura">Fecha actualizada de la escritura</param>
        /// <param name="vNumeroEscritura">Numero actualizado de la escritura</param>
        /// <param name="vNotariaRegistra">Nombre actualizado de la notaria donde se registra la denuncia</param>
        /// <param name="vNumeroRegistro">Numero actualizado del registro relacionado con el Bien</param>
        /// <param name="vFechaIngresoICBF">Fecha actualizada del ingreso del registro al ICBF</param>
        /// <param name="vFechaInstrumentosP">Fecha actualizada de los instrumentos relacionados con el Id de la denuncia</param>
        /// <returns></returns>
        public int ActualizarTramiteNotarialxIdDenuncia(int vIdDenunciaBien, DateTime vFechaEscritura, decimal vNumeroEscritura,
            decimal vNumeroRegistro, DateTime vFechaIngresoICBF, DateTime vFechaInstrumentosP, DateTime vFechaComunicacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ActualizarTramiteNotarial"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@vIdDenunciaBien", DbType.Int32, vIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@vFechaEscritura", DbType.DateTime, vFechaEscritura);
                    vDataBase.AddInParameter(vDbCommand, "@vFechaComunicacion", DbType.DateTime, vFechaComunicacion);
                    vDataBase.AddInParameter(vDbCommand, "@vNumeroEscritura", DbType.Decimal, vNumeroEscritura);
                    vDataBase.AddInParameter(vDbCommand, "@vNumeroRegistro", DbType.Decimal, vNumeroRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@vFechaIngresoICBF", DbType.DateTime, vFechaIngresoICBF);
                    vDataBase.AddInParameter(vDbCommand, "@vFechaInstrumentosP", DbType.DateTime, vFechaInstrumentosP);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)new TramiteNotarial();
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(new TramiteNotarial(), vDbCommand);
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
