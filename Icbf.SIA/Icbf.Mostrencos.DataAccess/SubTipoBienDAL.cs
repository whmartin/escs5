﻿//-----------------------------------------------------------------------
// <copyright file="SubTipoBienDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase SubTipoBienDAL.</summary>
// <author>INGENIAN</author>
// <date>06/01/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class SubTipoBienDAL : GeneralDAL
    {
        private Database vDataBase;

        public SubTipoBienDAL()
        {
            this.vDataBase = DatabaseFactory.CreateDatabase("DataBaseSEVEN");
        }

        /// <summary>
        /// consulta los SubTipoBien
        /// </summary>
        /// <returns>LISTA DE SubTipoBien</returns>
        public List<SubTipoBien> ConsultarSubTipoBien(string pIdTipoBien)
        {
            try
            {
                using (DbConnection vDbConnection = vDataBase.CreateConnection())
                {
                    vDbConnection.Open();
                    DbCommand vDbCommand = vDbConnection.CreateCommand();
                    vDbCommand.CommandText = "SELECT DISTINCT MAX([pro_codi]) AS pro_codi  ,[pro_NO02] FROM [produccion].[dbo].[in_vprod] WHERE TIPO_BIEN_="+ pIdTipoBien+" GROUP BY [pro_NO02] ";
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SubTipoBien> vListSubTipoBien = new List<SubTipoBien>();
                        while (vDataReaderResults.Read())
                        {
                            SubTipoBien vSubTipoBien = new SubTipoBien();
                            vSubTipoBien.IdSubTipoBien = vDataReaderResults["pro_codi"] != DBNull.Value ? Convert.ToString(vDataReaderResults["pro_codi"].ToString()) : vSubTipoBien.IdSubTipoBien;
                            vSubTipoBien.NombreSubTipoBien = vDataReaderResults["pro_NO02"] != DBNull.Value ? Convert.ToString(vDataReaderResults["pro_NO02"].ToString().ToUpper()) : vSubTipoBien.NombreSubTipoBien;
                            vSubTipoBien.IdTipoBien = pIdTipoBien;
                            vListSubTipoBien.Add(vSubTipoBien);
                            vSubTipoBien = null;
                        }

                        vDbConnection.Close();
                        return vListSubTipoBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
