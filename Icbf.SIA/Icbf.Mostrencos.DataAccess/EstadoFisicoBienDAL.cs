﻿//-----------------------------------------------------------------------
// <copyright file="EstadoFisicoBienDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase EstadoFisicoBienDAL.</summary>
// <author>INDESAP[Jesus Eduardo Cortes]</author>
// <date>06/01/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class EstadoFisicoBienDAL : GeneralDAL
    {

        private Database vDataBase;

        public EstadoFisicoBienDAL()
        {
            this.vDataBase = DatabaseFactory.CreateDatabase("DataBaseSEVEN");
        }

        /// <summary>
        /// consulta los EstadoFisicoBien
        /// </summary>
        /// <returns>LISTA DE EstadoFisicoBien</returns>
        public List<EstadoFisicoBien> ConsultarEstadoFisicoBien()
        {
            try
            {
                using (DbConnection vDbConnection = vDataBase.CreateConnection())
                {
                    vDbConnection.Open();
                    DbCommand vDbCommand = vDbConnection.CreateCommand();
                    vDbCommand.CommandText = "SELECT [ITE_CODI] ,[ITE_NOMB] FROM [produccion].[dbo].[GN_VITEM] WHERE [ITE_ACTI] = N's' and [TIT_CONT] = 208";

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoFisicoBien> vListEstadoFisicoBien = new List<EstadoFisicoBien>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoFisicoBien vEstadoFisicoBien = new EstadoFisicoBien();
                            vEstadoFisicoBien.IdEstadoFisicoBien = vDataReaderResults["ITE_CODI"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_CODI"].ToString()) : vEstadoFisicoBien.IdEstadoFisicoBien;
                            vEstadoFisicoBien.NombreEstadoFisicoBien = vDataReaderResults["ITE_NOMB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ITE_NOMB"].ToString().ToUpper()) : vEstadoFisicoBien.NombreEstadoFisicoBien;
                            vListEstadoFisicoBien.Add(vEstadoFisicoBien);
                            vEstadoFisicoBien = null;
                        }

                        return vListEstadoFisicoBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
