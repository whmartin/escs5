﻿using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.DataAccess
{
    public class DocumentosSolicitadosYRecibidosDAL : GeneralDAL
    {
        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosDenunciaBienPorId(int IdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_DocumentosSolicitados_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentosSolicitadosYRecibidos> vlstDocumentosSolicitadosYRecibidosDenunciaBien = new List<DocumentosSolicitadosYRecibidos>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosYRecibidos vDocumentosSolicitadosYRecibidosDenunciaBien = new DocumentosSolicitadosYRecibidos();
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdDenunciaBien;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdTipoDocumentoBienDenunciado;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreTipoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.FechaSolicitud;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"]) : vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionSolicitada;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdEstadoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreEstadoDocumento = vDataReaderResults["NombreEstadoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreEstadoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.FechaRecibido;
                            //vDocumentosSolicitadosYRecibidosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(!string.IsNullOrEmpty(vDataReaderResults["NombreArchivo"].ToString()) ? vDataReaderResults["NombreArchivo"].ToString().Contains("/") ? vDataReaderResults["NombreArchivo"].ToString().Split('/')[1] : (vDataReaderResults["NombreArchivo"].ToString().Contains('\\') ? vDataReaderResults["NombreArchivo"].ToString().Split('\\')[1] : vDataReaderResults["NombreArchivo"].ToString()) : vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionrecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionrecibida"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionRecibida;
                            vlstDocumentosSolicitadosYRecibidosDenunciaBien.Add(vDocumentosSolicitadosYRecibidosDenunciaBien);
                        }

                        return vlstDocumentosSolicitadosYRecibidosDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(int IdContratoParticipacionEconomica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_DocumentosSolicitados_ConsultarporIdContratoParticipacionEconomica"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoParticipacionEconomica", DbType.Int32, IdContratoParticipacionEconomica);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentosSolicitadosYRecibidos> vlstDocumentosSolicitadosYRecibidosDenunciaBien = new List<DocumentosSolicitadosYRecibidos>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosYRecibidos vDocumentosSolicitadosYRecibidosDenunciaBien = new DocumentosSolicitadosYRecibidos();
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdDenunciaBien;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdTipoDocumentoBienDenunciado;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreTipoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.FechaSolicitud;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"]) : vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionSolicitada;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdEstadoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreEstadoDocumento = vDataReaderResults["NombreEstadoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreEstadoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.FechaRecibido;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.Estado;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(!string.IsNullOrEmpty(vDataReaderResults["NombreArchivo"].ToString()) ? vDataReaderResults["NombreArchivo"].ToString().Contains("/") ? vDataReaderResults["NombreArchivo"].ToString().Split('/')[1] : (vDataReaderResults["NombreArchivo"].ToString().Contains('\\') ? vDataReaderResults["NombreArchivo"].ToString().Split('\\')[1] : vDataReaderResults["NombreArchivo"].ToString()) : vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionrecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionrecibida"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionRecibida;
                            vlstDocumentosSolicitadosYRecibidosDenunciaBien.Add(vDocumentosSolicitadosYRecibidosDenunciaBien);
                        }

                        return vlstDocumentosSolicitadosYRecibidosDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosPorIdDenuncia(int IdDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_DocumentosSolicitados_ConsultarporIdDenunciaOrdenPago"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenuncia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentosSolicitadosYRecibidos> vlstDocumentosSolicitadosYRecibidosDenunciaBien = new List<DocumentosSolicitadosYRecibidos>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosYRecibidos vDocumentosSolicitadosYRecibidosDenunciaBien = new DocumentosSolicitadosYRecibidos();
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdDenunciaBien;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdTipoDocumentoBienDenunciado;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreTipoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.FechaSolicitud;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"]) : vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionSolicitada;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.IdEstadoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreEstadoDocumento = vDataReaderResults["NombreEstadoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDocumento"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreEstadoDocumento;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.FechaRecibido;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.Estado;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(!string.IsNullOrEmpty(vDataReaderResults["NombreArchivo"].ToString()) ? vDataReaderResults["NombreArchivo"].ToString().Contains("/") ? vDataReaderResults["NombreArchivo"].ToString().Split('/')[1] : (vDataReaderResults["NombreArchivo"].ToString().Contains('\\') ? vDataReaderResults["NombreArchivo"].ToString().Split('\\')[1] : vDataReaderResults["NombreArchivo"].ToString()) : vDataReaderResults["NombreArchivo"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.NombreArchivo;
                            vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionrecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionrecibida"].ToString()) : vDocumentosSolicitadosYRecibidosDenunciaBien.ObservacionesDocumentacionRecibida;
                            vlstDocumentosSolicitadosYRecibidosDenunciaBien.Add(vDocumentosSolicitadosYRecibidosDenunciaBien);
                        }

                        return vlstDocumentosSolicitadosYRecibidosDenunciaBien;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        //Insertar Documentos
        public int InsertarDocumentosSolicitadosDenunciaBienContratos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Documentacion_Insertar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, 18);

                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoParticipacionEcnomica", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdContratoParticipacionEcnomica);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdTipoDocumentoBienDenunciado);
                    if (pDocumentosSolicitadosYRecibidos.FechaSolicitud != null && pDocumentosSolicitadosYRecibidos.FechaSolicitud != Convert.ToDateTime("1/01/0001"))
                        vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaSolicitud);

                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosYRecibidos.ObservacionesDocumentacionSolicitada))
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumentacionSolicitada);

                    //vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosYRecibidos.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosYRecibidos.RutaArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentacionCompleta", DbType.String, pDocumentosSolicitadosYRecibidos.DocumentacionCompleta);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumentacionRecibida);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumento", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosYRecibidos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentosSolicitadosYRecibidos.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaModifica);
                    //vDataBase.AddInParameter(vDbCommand, "@IdOrdenesPago", DbType.Int32, null);

                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                    //Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosYRecibidos;
                    //vAuditorio.ProgramaGeneraLog = true;
                    //vAuditorio.Operacion = "INSERT";
                    pDocumentosSolicitadosYRecibidos.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien").ToString());
                    GenerarLogAuditoria(pDocumentosSolicitadosYRecibidos, vDbCommand);
                    return vResultadoArchivo;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Insertar Documentos para relación de pagos
        public int InsertarDocumentosSolicitadosDenunciaBienRelacionDePagos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Documentacion_Insertar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, 18);

                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdContratoParticipacionEcnomica", DbType.Int32, null);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdTipoDocumentoBienDenunciado);
                    if (pDocumentosSolicitadosYRecibidos.FechaSolicitud != null && pDocumentosSolicitadosYRecibidos.FechaSolicitud != Convert.ToDateTime("1/01/0001"))
                        vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaSolicitud);

                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosYRecibidos.ObservacionesDocumentacionSolicitada))
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumentacionSolicitada);

                    //vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosYRecibidos.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosYRecibidos.RutaArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentacionCompleta", DbType.String, pDocumentosSolicitadosYRecibidos.DocumentacionCompleta);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumentacionRecibida);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumento", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosYRecibidos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentosSolicitadosYRecibidos.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaModifica);

                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                    //Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosYRecibidos;
                    //vAuditorio.ProgramaGeneraLog = true;
                    //vAuditorio.Operacion = "INSERT";
                    //pDocumentosSolicitadosYRecibidos.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien").ToString());
                    //GenerarLogAuditoria(pDocumentosSolicitadosYRecibidos, vDbCommand);
                    //return vResultadoArchivo;

                    if (vResultadoArchivo > 0)
                    {
                        vResultadoArchivo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosYRecibidos;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pDocumentosSolicitadosYRecibidos, vDbCommand);
                    }

                    return vResultadoArchivo;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Actualizar Documentos
        public int ActualizarDocumentosSolicitadosDenunciaBienContratos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Documentacion_Editar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdDocumentosSolicitadosDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdCausante", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdCausante);
                    vDataBase.AddInParameter(vDbCommand, "@IdApoderado", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdTipoDocumentoBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumentacionSolicitada);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosYRecibidos.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaRecibido);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosYRecibidos.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosYRecibidos.RutaArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentacionCompleta", DbType.String, pDocumentosSolicitadosYRecibidos.DocumentacionCompleta);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumentacionRecibida);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumento", DbType.String, pDocumentosSolicitadosYRecibidos.ObservacionesDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosYRecibidos.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentosSolicitadosYRecibidos.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pDocumentosSolicitadosYRecibidos.FechaModifica);

                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                    Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosYRecibidos;
                    vAuditorio.ProgramaGeneraLog = true;
                    vAuditorio.Operacion = "UPDATE";
                    pDocumentosSolicitadosYRecibidos.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien").ToString());
                    GenerarLogAuditoria(pDocumentosSolicitadosYRecibidos, vDbCommand);
                    return vResultadoArchivo;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Eliminar Documentos
        public int EliminarDocumentosSolicitadosDenunciaBienContratos(int vIdDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Documentacion_Eliminar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, vIdDocumento);
                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultadoArchivo;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
