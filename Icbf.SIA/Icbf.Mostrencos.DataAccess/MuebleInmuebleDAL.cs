﻿//-----------------------------------------------------------------------
// <copyright file="MuebleInmuebleDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase MuebleInmuebleDAL.</summary>
// <author>INGENIAN</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class MuebleInmuebleDAL : GeneralDAL
    {
        /// <summary>
        /// consulta los MuebleInmueble por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de MuebleInmueble</returns>
        public List<MuebleInmueble> ConsultarMuebleInmueblePorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_MuebleInmueble_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<MuebleInmueble> vListMuebleInmueble = new List<MuebleInmueble>();
                        while (vDataReaderResults.Read())
                        {
                            MuebleInmueble vMuebleInmueble = new MuebleInmueble();
                            vMuebleInmueble.IdMuebleInmueble = vDataReaderResults["IdMuebleInmueble"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMuebleInmueble"].ToString()) : vMuebleInmueble.IdMuebleInmueble;
                            vMuebleInmueble.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vMuebleInmueble.IdDenunciaBien;
                            vMuebleInmueble.IdTipoBien = vDataReaderResults["TipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoBien"].ToString()) : vMuebleInmueble.IdTipoBien;
                            vMuebleInmueble.IdSubTipoBien = vDataReaderResults["SubTipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SubTipoBien"].ToString()) : vMuebleInmueble.IdSubTipoBien;
                            vMuebleInmueble.IdClaseBien = vDataReaderResults["ClaseBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseBien"].ToString()) : vMuebleInmueble.IdClaseBien;
                            vMuebleInmueble.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDepartamento"]) : vMuebleInmueble.IdDepartamento;
                            vMuebleInmueble.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"]) : vMuebleInmueble.IdMunicipio;
                            vMuebleInmueble.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : vMuebleInmueble.ClaseEntrada;
                            vMuebleInmueble.EstadoFisicoBien = vDataReaderResults["EstadoFisicoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoFisicoBien"].ToString()) : vMuebleInmueble.EstadoFisicoBien;
                            vMuebleInmueble.ValorEstimadoComercial = vDataReaderResults["ValorEstimadoComercial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEstimadoComercial"].ToString()) : vMuebleInmueble.ValorEstimadoComercial;
                            vMuebleInmueble.PorcentajePertenenciaBien = vDataReaderResults["PorcentajePertenenciaBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajePertenenciaBien"].ToString()) : vMuebleInmueble.PorcentajePertenenciaBien;
                            vMuebleInmueble.DestinacionEconomica = vDataReaderResults["DestinacionEconomica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DestinacionEconomica"].ToString()) : vMuebleInmueble.DestinacionEconomica;
                            vMuebleInmueble.MatriculaInmobiliaria = vDataReaderResults["MatriculaInmobiliaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MatriculaInmobiliaria"].ToString()) : vMuebleInmueble.MatriculaInmobiliaria;
                            vMuebleInmueble.CedulaCatastral = vDataReaderResults["CedulaCatastral"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CedulaCatastral"].ToString()) : vMuebleInmueble.CedulaCatastral;
                            vMuebleInmueble.DireccionInmueble = vDataReaderResults["DireccionInmueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionInmueble"].ToString()) : vMuebleInmueble.DireccionInmueble;
                            vMuebleInmueble.NumeroIdentificacionInmueble = vDataReaderResults["NumeroIdentificacionInmueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionInmueble"].ToString()) : vMuebleInmueble.NumeroIdentificacionInmueble;
                            vMuebleInmueble.MarcaBien = vDataReaderResults["MarcaBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MarcaBien"].ToString()) : vMuebleInmueble.MarcaBien;
                            vMuebleInmueble.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vMuebleInmueble.FechaVenta;
                            vMuebleInmueble.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vMuebleInmueble.FechaAdjudicado;
                            vMuebleInmueble.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vMuebleInmueble.FechaRegistro;
                            vMuebleInmueble.EstadoBien = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"].ToString()) : vMuebleInmueble.DescripcionBien;
                            vMuebleInmueble.DescripcionBien = vDataReaderResults["DescripcionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBien"].ToString()) : vMuebleInmueble.DescripcionBien;
                            vMuebleInmueble.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMuebleInmueble.UsuarioCrea;
                            vMuebleInmueble.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMuebleInmueble.FechaCrea;
                            vMuebleInmueble.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMuebleInmueble.UsuarioModifica;
                            vMuebleInmueble.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMuebleInmueble.FechaModifica;

                            vListMuebleInmueble.Add(vMuebleInmueble);
                            vMuebleInmueble = null;
                        }

                        return vListMuebleInmueble;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta los MuebleInmueble por el Id
        /// </summary>
        /// <param name="pIdMuebleInmueble">id que se va a consultar</param>
        /// <returns>MuebleInmueble</returns>
        public MuebleInmueble ConsultarMuebleInmueblePorId(int pIdMuebleInmueble)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_MuebleInmueble_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdMuebleInmueble", DbType.Int32, pIdMuebleInmueble);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        MuebleInmueble vMuebleInmueble = new MuebleInmueble();
                        while (vDataReaderResults.Read())
                        {
                            vMuebleInmueble.IdMuebleInmueble = vDataReaderResults["IdMuebleInmueble"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMuebleInmueble"].ToString()) : vMuebleInmueble.IdMuebleInmueble;
                            vMuebleInmueble.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vMuebleInmueble.IdDenunciaBien;
                            vMuebleInmueble.IdTipoBien = vDataReaderResults["TipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoBien"].ToString()) : vMuebleInmueble.IdTipoBien;
                            vMuebleInmueble.IdSubTipoBien = vDataReaderResults["SubTipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SubTipoBien"].ToString()) : vMuebleInmueble.IdSubTipoBien;
                            vMuebleInmueble.IdClaseBien = vDataReaderResults["ClaseBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseBien"].ToString()) : vMuebleInmueble.IdClaseBien;
                            vMuebleInmueble.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDepartamento"]) : vMuebleInmueble.IdDepartamento;
                            vMuebleInmueble.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"]) : vMuebleInmueble.IdMunicipio;
                            vMuebleInmueble.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : vMuebleInmueble.ClaseEntrada;
                            vMuebleInmueble.EstadoFisicoBien = vDataReaderResults["EstadoFisicoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoFisicoBien"].ToString()) : vMuebleInmueble.EstadoFisicoBien;
                            vMuebleInmueble.ValorEstimadoComercial = vDataReaderResults["ValorEstimadoComercial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEstimadoComercial"].ToString()) : vMuebleInmueble.ValorEstimadoComercial;
                            vMuebleInmueble.PorcentajePertenenciaBien = vDataReaderResults["PorcentajePertenenciaBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajePertenenciaBien"].ToString()) : vMuebleInmueble.PorcentajePertenenciaBien;
                            vMuebleInmueble.DestinacionEconomica = vDataReaderResults["DestinacionEconomica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DestinacionEconomica"].ToString()) : vMuebleInmueble.DestinacionEconomica;
                            vMuebleInmueble.MatriculaInmobiliaria = vDataReaderResults["MatriculaInmobiliaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MatriculaInmobiliaria"].ToString()) : vMuebleInmueble.MatriculaInmobiliaria;
                            vMuebleInmueble.CedulaCatastral = vDataReaderResults["CedulaCatastral"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CedulaCatastral"].ToString()) : vMuebleInmueble.CedulaCatastral;
                            vMuebleInmueble.DireccionInmueble = vDataReaderResults["DireccionInmueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionInmueble"].ToString()) : vMuebleInmueble.DireccionInmueble;
                            vMuebleInmueble.NumeroIdentificacionInmueble = vDataReaderResults["NumeroIdentificacionInmueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionInmueble"].ToString()) : vMuebleInmueble.NumeroIdentificacionInmueble;
                            vMuebleInmueble.MarcaBien = vDataReaderResults["MarcaBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MarcaBien"].ToString()) : vMuebleInmueble.MarcaBien;
                            vMuebleInmueble.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vMuebleInmueble.FechaVenta;
                            vMuebleInmueble.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vMuebleInmueble.FechaAdjudicado;
                            vMuebleInmueble.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vMuebleInmueble.FechaRegistro;
                            vMuebleInmueble.EstadoBien = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"].ToString()) : vMuebleInmueble.DescripcionBien;
                            vMuebleInmueble.DescripcionBien = vDataReaderResults["DescripcionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBien"].ToString()) : vMuebleInmueble.DescripcionBien;
                            vMuebleInmueble.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMuebleInmueble.UsuarioCrea;
                            vMuebleInmueble.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMuebleInmueble.FechaCrea;
                            vMuebleInmueble.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMuebleInmueble.UsuarioModifica;
                            vMuebleInmueble.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMuebleInmueble.FechaModifica;
                            vMuebleInmueble.UsoBien = vDataReaderResults["UsoBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["UsoBien"].ToString()) : vMuebleInmueble.UsoBien;
                            vMuebleInmueble.IdInformacionVenta = vDataReaderResults["IdInformacionVenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionVenta"].ToString()) : vMuebleInmueble.IdInformacionVenta;
                        }

                        return vMuebleInmueble;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// consulta la informacion que se va a cargar en los campos ocultos
        /// </summary>
        /// <param name="pIdSubTipoBien">Valos que se va a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public string ConsultarCamposOcultos(string pIdSubTipoBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_CamposOcultos_Consultar"))
                {
                    string vCodProducto = string.Empty;
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vCodProducto = vDataReaderResults["Cod_Prod"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cod_Prod"].ToString()) : string.Empty;
                        }
                    }

                    return vCodProducto;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// valida si el bien denunciado ya existe
        /// </summary>
        /// <param name="pIddenunciaBien">id de la denuncia</param>
        /// <param name="pBienDenunciado">tipo del bien denunciado</param>
        /// <param name="pDato">identificador del bien denunciado</param>
        /// <param name="pIdBienDenunciado">id del bien denunciado</param>
        /// <returns>thue si existe o false en caso contrario</returns>
        public bool ValidarExistenciaBienDenunciado(int pIddenunciaBien, string pBienDenunciado, string pDato, int pIdBienDenunciado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_BienesDenunciados_ValidarExistencia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@BienDenunciado", DbType.String, pBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@Dato", DbType.String, pDato);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIddenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdBienDenunciado", DbType.Int32, pIdBienDenunciado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExiste = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExiste = vDataReaderResults["Existe"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Existe"].ToString()) : 0;
                        }

                        return vExiste > 0;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// valida si el bien denunciado ya existe en otras regionales
        /// </summary>
        /// <param name="pIddenunciaBien">id de la denuncia</param>
        /// <param name="pBienDenunciado">tipo del bien denunciado</param>
        /// <param name="pDato">identificador del bien denunciado</param>
        /// <returns>thue si existe o false en caso contrario</returns>
        public string ValidarExistenciaBienDenunciadoOtrasRegionales(int pIddenunciaBien, string pBienDenunciado, string pDato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_BienesDenunciados_ValidarExistenciaEnOtrasRegionales"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@BienDenunciado", DbType.String, pBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@Dato", DbType.String, pDato);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIddenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string vMensaje = string.Empty;
                        while (vDataReaderResults.Read())
                        {
                            vMensaje = vDataReaderResults["Mensaje"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Mensaje"].ToString()) : string.Empty;
                        }

                        return vMensaje;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Inserta la información de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Variable de tipo MuebleInmueble que contiene la información que se va a insertar</param>
        /// <returns>Id del MuebleInmueble insertado</returns>
        public int InsertarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_MuebleInmueble_Insert"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdMuebleInmueble", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pMuebleInmueble.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@TipoBien", DbType.String, pMuebleInmueble.IdTipoBien);
                    vDataBase.AddInParameter(vDbCommand, "@SubTipoBien", DbType.String, pMuebleInmueble.IdSubTipoBien);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseBien", DbType.String, pMuebleInmueble.IdClaseBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pMuebleInmueble.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pMuebleInmueble.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseEntrada", DbType.String, pMuebleInmueble.ClaseEntrada);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoFisicoBien", DbType.String, pMuebleInmueble.EstadoFisicoBien);
                    vDataBase.AddInParameter(vDbCommand, "@ValorEstimadoComercial", DbType.Decimal, pMuebleInmueble.ValorEstimadoComercial);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajePertenenciaBien", DbType.String, pMuebleInmueble.PorcentajePertenenciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@DestinacionEconomica", DbType.String, pMuebleInmueble.DestinacionEconomica);
                    vDataBase.AddInParameter(vDbCommand, "@MatriculaInmobiliaria", DbType.String, pMuebleInmueble.MatriculaInmobiliaria);
                    vDataBase.AddInParameter(vDbCommand, "@CedulaCatastral", DbType.String, pMuebleInmueble.CedulaCatastral);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionInmueble", DbType.String, pMuebleInmueble.DireccionInmueble);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionInmueble", DbType.String, pMuebleInmueble.NumeroIdentificacionInmueble);
                    vDataBase.AddInParameter(vDbCommand, "@MarcaBien", DbType.String, pMuebleInmueble.MarcaBien);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, pMuebleInmueble.FechaVenta);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicado", DbType.DateTime, pMuebleInmueble.FechaAdjudicado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pMuebleInmueble.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoBien", DbType.String, pMuebleInmueble.EstadoBien);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionBien", DbType.String, pMuebleInmueble.DescripcionBien);
                    vDataBase.AddInParameter(vDbCommand, "@TipoParametro", DbType.String, pMuebleInmueble.TipoParametro);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoParametros", DbType.String, pMuebleInmueble.CodigoParametros);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pMuebleInmueble.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pMuebleInmueble.IdUsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdMuebleInmueble"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pMuebleInmueble;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pMuebleInmueble, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Variable de tipo MuebleInmueble que contiene la información que se va a insertar</param>
        /// <returns>numero de registros modificados</returns>
        public int EditarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_MuebleInmueble_Update"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdMuebleInmueble", DbType.Int32, pMuebleInmueble.IdMuebleInmueble);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pMuebleInmueble.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.String, pMuebleInmueble.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@ClaseEntrada", DbType.String, pMuebleInmueble.ClaseEntrada);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoFisicoBien", DbType.String, pMuebleInmueble.EstadoFisicoBien);
                    vDataBase.AddInParameter(vDbCommand, "@ValorEstimadoComercial", DbType.Decimal, pMuebleInmueble.ValorEstimadoComercial);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajePertenenciaBien", DbType.String, pMuebleInmueble.PorcentajePertenenciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@DestinacionEconomica", DbType.String, pMuebleInmueble.DestinacionEconomica);
                    vDataBase.AddInParameter(vDbCommand, "@MatriculaInmobiliaria", DbType.String, pMuebleInmueble.MatriculaInmobiliaria);
                    vDataBase.AddInParameter(vDbCommand, "@CedulaCatastral", DbType.String, pMuebleInmueble.CedulaCatastral);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionInmueble", DbType.String, pMuebleInmueble.DireccionInmueble);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacionInmueble", DbType.String, pMuebleInmueble.NumeroIdentificacionInmueble);
                    vDataBase.AddInParameter(vDbCommand, "@MarcaBien", DbType.String, pMuebleInmueble.MarcaBien);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, pMuebleInmueble.FechaVenta);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicado", DbType.DateTime, pMuebleInmueble.FechaAdjudicado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pMuebleInmueble.FechaRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoBien", DbType.String, pMuebleInmueble.EstadoBien);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionBien", DbType.String, pMuebleInmueble.DescripcionBien);
                    vDataBase.AddInParameter(vDbCommand, "@TipoParametro", DbType.String, pMuebleInmueble.TipoParametro);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoParametros", DbType.String, pMuebleInmueble.CodigoParametros);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pMuebleInmueble.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pMuebleInmueble;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pMuebleInmueble, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los MuebleInmueble por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de MuebleInmueble</returns>
        public List<MuebleInmueble> ConsultarMuebleInmueble(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_MuebleInmueble_Consulta"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<MuebleInmueble> vListMuebleInmueble = new List<MuebleInmueble>();
                        while (vDataReaderResults.Read())
                        {
                            MuebleInmueble vMuebleInmueble = new MuebleInmueble();
                            vMuebleInmueble.IdMuebleInmueble = vDataReaderResults["IdMuebleInmueble"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMuebleInmueble"].ToString()) : vMuebleInmueble.IdMuebleInmueble;
                            vMuebleInmueble.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vMuebleInmueble.IdDenunciaBien;
                            vMuebleInmueble.IdTipoBien = vDataReaderResults["TipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoBien"].ToString()) : vMuebleInmueble.IdTipoBien;
                            vMuebleInmueble.IdSubTipoBien = vDataReaderResults["SubTipoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SubTipoBien"].ToString()) : vMuebleInmueble.IdSubTipoBien;
                            vMuebleInmueble.IdClaseBien = vDataReaderResults["ClaseBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseBien"].ToString()) : vMuebleInmueble.IdClaseBien;
                            //vMuebleInmueble.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdDepartamento"]) : vMuebleInmueble.IdDepartamento;
                            //vMuebleInmueble.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdMunicipio"]) : vMuebleInmueble.IdMunicipio;
                            //vMuebleInmueble.ClaseEntrada = vDataReaderResults["ClaseEntrada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseEntrada"].ToString()) : vMuebleInmueble.ClaseEntrada;
                            //vMuebleInmueble.EstadoFisicoBien = vDataReaderResults["EstadoFisicoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoFisicoBien"].ToString()) : vMuebleInmueble.EstadoFisicoBien;
                            //vMuebleInmueble.ValorEstimadoComercial = vDataReaderResults["ValorEstimadoComercial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorEstimadoComercial"].ToString()) : vMuebleInmueble.ValorEstimadoComercial;
                            //vMuebleInmueble.PorcentajePertenenciaBien = vDataReaderResults["PorcentajePertenenciaBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajePertenenciaBien"].ToString()) : vMuebleInmueble.PorcentajePertenenciaBien;
                            //vMuebleInmueble.DestinacionEconomica = vDataReaderResults["DestinacionEconomica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DestinacionEconomica"].ToString()) : vMuebleInmueble.DestinacionEconomica;
                            //vMuebleInmueble.MatriculaInmobiliaria = vDataReaderResults["MatriculaInmobiliaria"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MatriculaInmobiliaria"].ToString()) : vMuebleInmueble.MatriculaInmobiliaria;
                            //vMuebleInmueble.CedulaCatastral = vDataReaderResults["CedulaCatastral"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CedulaCatastral"].ToString()) : vMuebleInmueble.CedulaCatastral;
                            //vMuebleInmueble.DireccionInmueble = vDataReaderResults["DireccionInmueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionInmueble"].ToString()) : vMuebleInmueble.DireccionInmueble;
                            //vMuebleInmueble.NumeroIdentificacionInmueble = vDataReaderResults["NumeroIdentificacionInmueble"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacionInmueble"].ToString()) : vMuebleInmueble.NumeroIdentificacionInmueble;
                            //vMuebleInmueble.MarcaBien = vDataReaderResults["MarcaBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MarcaBien"].ToString()) : vMuebleInmueble.MarcaBien;
                            //vMuebleInmueble.FechaVenta = vDataReaderResults["FechaVenta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVenta"].ToString()) : vMuebleInmueble.FechaVenta;
                            //vMuebleInmueble.FechaAdjudicado = vDataReaderResults["FechaAdjudicado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicado"].ToString()) : vMuebleInmueble.FechaAdjudicado;
                            //vMuebleInmueble.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vMuebleInmueble.FechaRegistro;
                            vMuebleInmueble.EstadoBien = vDataReaderResults["EstadoBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoBien"].ToString()) : vMuebleInmueble.DescripcionBien;
                            vMuebleInmueble.DescripcionBien = vDataReaderResults["DescripcionBien"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionBien"].ToString()) : vMuebleInmueble.DescripcionBien;
                            vMuebleInmueble.UsoBien = vDataReaderResults["UsoBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["UsoBien"].ToString()) : vMuebleInmueble.UsoBien;
                            vMuebleInmueble.IdInformacionVenta = vDataReaderResults["IdInformacionVenta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionVenta"].ToString()) : vMuebleInmueble.IdInformacionVenta;
                            //vMuebleInmueble.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vMuebleInmueble.UsuarioCrea;
                            //vMuebleInmueble.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vMuebleInmueble.FechaCrea;
                            //vMuebleInmueble.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vMuebleInmueble.UsuarioModifica;
                            //vMuebleInmueble.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vMuebleInmueble.FechaModifica;

                            if (vMuebleInmueble.UsoBien == 1)
                            {
                                vMuebleInmueble.NombreUsoBien = "VENTA";
                            }
                            else if (vMuebleInmueble.UsoBien == 2)
                            {
                                vMuebleInmueble.NombreUsoBien = "USO PROPIO";
                            }
                            else
                            {
                                vMuebleInmueble.NombreUsoBien = string.Empty;
                            }

                            vListMuebleInmueble.Add(vMuebleInmueble);
                            vMuebleInmueble = null;
                        }

                        return vListMuebleInmueble;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region CU 154

        /// <summary>
        /// Edita los campos UsoBien y IdInformacionVenta de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Entidad a modificar</param>
        /// <returns>Resultado de la Operación</returns>
        public int ModificarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_MuebleInmueble_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdMuebleInmueble", DbType.Int32, pMuebleInmueble.IdMuebleInmueble);
                    vDataBase.AddInParameter(vDbCommand, "@UsoBien", DbType.Int32, pMuebleInmueble.UsoBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionVenta", DbType.Int32, pMuebleInmueble.IdInformacionVenta);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoBien", DbType.String, pMuebleInmueble.EstadoBien);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pMuebleInmueble.UsuarioModifica);

                    if (pMuebleInmueble.FechaVenta != Convert.ToDateTime("1/01/0001 00:00:00") && pMuebleInmueble.FechaVenta != Convert.ToDateTime("1/01/1900 00:00:00"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, pMuebleInmueble.FechaVenta);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVenta", DbType.DateTime, null);
                    }

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pMuebleInmueble;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pMuebleInmueble, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}
