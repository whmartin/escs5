﻿
namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Seguridad.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class RolDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta los roles por el nombre de programa y el nombre de la función
        /// </summary>
        /// <param name="pNombrePrograma">Nombre del programa</param>
        /// <param name="pNombreFuncion">Nombre de la función</param>
        /// <returns>lista de roles</returns>
        public List<Rol> ConsultarRolesPorProgramaFuncion(string pNombrePrograma, string pNombreFuncion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Usuario_ConsultarPorProgramaFuncion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NombrePrograma", DbType.String, pNombrePrograma);
                    vDataBase.AddInParameter(vDbCommand, "@NombreFuncion", DbType.String, pNombreFuncion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Rol> vListaRoles = new List<Rol>();
                        while (vDataReaderResults.Read())
                        {
                            Rol vRol = new Rol();
                            vRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vRol.IdRol;
                            vRol.NombreRol = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vRol.NombreRol;
                            vListaRoles.Add(vRol);
                            vRol = null;
                        }

                        return vListaRoles;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
