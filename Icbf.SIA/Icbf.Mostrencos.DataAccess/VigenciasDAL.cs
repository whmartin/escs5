﻿namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Text;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class VigenciasDAL : GeneralDAL
    {
        //Cargar Lista Regionales
        public List<Vigencias> ConsultarVigenciasAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Consultar_Vigencias"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Vigencias> vlstVigencias = new List<Vigencias>();
                        while (vDataReaderResults.Read())
                        {
                            Vigencias vVigencias = new Vigencias();
                            vVigencias.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vVigencias.IdVigencia;
                            vVigencias.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vVigencias.AcnoVigencia;
                            
                            vlstVigencias.Add(vVigencias);
                        }
                        return vlstVigencias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
