﻿

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class ApoderadosDAL : GeneralDAL
    {
        /// <summary>
        /// Inserta la información de un Apoderado
        /// </summary>
        /// <param name="pApoderado">Variable de tipo Apoderado que contiene la información que se va a insertar</param>
        /// <returns>Id del Apoderado insertado</returns>
        public int InsertarApoderado(Apoderados pApoderado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Apoderados_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdApoderado", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pApoderado.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pApoderado.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@ExistePoder", DbType.String, pApoderado.ExisteApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@DenuncianteEsApoderado", DbType.String, pApoderado.DenuncianteEsApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioPoder", DbType.Date, pApoderado.FechaInicioPoder);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinPoder", DbType.Date, pApoderado.FechaFinPoder);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pApoderado.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoApoderado", DbType.String, pApoderado.EstadoApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTarjetaProfesional", DbType.String, pApoderado.NumeroTarjetaProfesional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pApoderado.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pApoderado.IdUsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pApoderado.IdApoderado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdApoderado").ToString());

                    if (vResultado > 0)
                    {
                        //vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdApoderado"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pApoderado;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pApoderado, vDbCommand);
                    }

                    return pApoderado.IdApoderado;
                }
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de un Apoderado por el id Apoderado
        /// </summary>
        /// <param name="pIdApoderado">Id Apoderado que se va a consultar</param>
        /// <returns>Variable de tipo Apoderado que contiene los datos de la consulta</returns>

        public Apoderados ConsultarApoderadosPorIdApoderado(int pIdApoderado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Apoderados_ConsultarPorIdApoderado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdApoderado", DbType.Int32, pIdApoderado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Apoderados vApoderado = new Apoderados();
                        while (vDataReaderResults.Read())
                        {
                            vApoderado.IdApoderado = vDataReaderResults["IdApoderado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdApoderado"].ToString()) : vApoderado.IdApoderado;
                            vApoderado.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"]) : vApoderado.IdTercero;
                            vApoderado.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vApoderado.IdDenunciaBien;
                            vApoderado.ExisteApoderado = vDataReaderResults["ExistePoder"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExistePoder"].ToString()) : vApoderado.ExisteApoderado;
                            vApoderado.DenuncianteEsApoderado = vDataReaderResults["DenuncianteEsApoderado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DenuncianteEsApoderado"].ToString()) : vApoderado.DenuncianteEsApoderado;
                            vApoderado.EstadoApoderado = vDataReaderResults["EstadoApoderado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoApoderado"].ToString()) : vApoderado.EstadoApoderado;
                            vApoderado.NumeroTarjetaProfesional = vDataReaderResults["NumeroTarjetaProfesional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTarjetaProfesional"].ToString()) : vApoderado.NumeroTarjetaProfesional;
                            vApoderado.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vApoderado.Observaciones;
                            vApoderado.FechaFinPoder = vDataReaderResults["FechaFinPoder"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinPoder"].ToString()) : vApoderado.FechaFinPoder;
                            vApoderado.FechaInicioPoder = vDataReaderResults["FechaInicioPoder"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioPoder"].ToString()) : vApoderado.FechaInicioPoder;
                            vApoderado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vApoderado.UsuarioCrea;
                            vApoderado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vApoderado.FechaCrea;
                            vApoderado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vApoderado.UsuarioModifica;
                            vApoderado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vApoderado.FechaModifica;
                        }

                        return vApoderado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de un Apoderado por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo Apoderados con la información resultante de la consulta</returns>
        public List<Apoderados> ConsultarApoderadosPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Apoderados_ConsultarPorIdDenunciaBien"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Apoderados> vListaApoderados = new List<Apoderados>();
                        while (vDataReaderResults.Read())
                        {
                            Apoderados vApoderado = new Apoderados();
                            vApoderado.IdApoderado = vDataReaderResults["IdApoderado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdApoderado"].ToString()) : vApoderado.IdApoderado;
                            vApoderado.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"]) : vApoderado.IdTercero;
                            vApoderado.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vApoderado.IdDenunciaBien;
                            vApoderado.ExisteApoderado = vDataReaderResults["ExistePoder"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExistePoder"].ToString()) : vApoderado.ExisteApoderado;
                            vApoderado.DenuncianteEsApoderado = vDataReaderResults["DenuncianteEsApoderado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DenuncianteEsApoderado"].ToString()) : vApoderado.DenuncianteEsApoderado;
                            vApoderado.EstadoApoderado = vDataReaderResults["EstadoApoderado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoApoderado"].ToString()) : vApoderado.EstadoApoderado;
                            vApoderado.NumeroTarjetaProfesional = vDataReaderResults["NumeroTarjetaProfesional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTarjetaProfesional"].ToString()) : vApoderado.NumeroTarjetaProfesional;
                            vApoderado.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vApoderado.Observaciones;
                            vApoderado.FechaFinPoder = vDataReaderResults["FechaFinPoder"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinPoder"].ToString()) : vApoderado.FechaFinPoder;
                            vApoderado.FechaInicioPoder = vDataReaderResults["FechaInicioPoder"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioPoder"].ToString()) : vApoderado.FechaInicioPoder;
                            vApoderado.NombreMostrar = vDataReaderResults["NombreMostrar"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMostrar"].ToString()) : vApoderado.NombreMostrar;
                            vApoderado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vApoderado.UsuarioCrea;
                            vApoderado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vApoderado.FechaCrea;
                            vApoderado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vApoderado.UsuarioModifica;
                            vApoderado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vApoderado.FechaModifica;
                            vListaApoderados.Add(vApoderado);
                            vApoderado = null;
                        }

                        return vListaApoderados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un Apoderado
        /// </summary>
        /// <param name="pApoderado">Variable de tipo Apoderado que contiene la información que se va a insertar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarApoderado(Apoderados pApoderado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Apoderados_Editar"))
                {
                    int vResultado;

                    vDataBase.AddInParameter(vDbCommand, "@IdApoderado", DbType.Int32, pApoderado.IdApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pApoderado.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@ExistePoder", DbType.String, pApoderado.ExisteApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@DenuncianteEsApoderado", DbType.String, pApoderado.DenuncianteEsApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioPoder", DbType.Date, pApoderado.FechaInicioPoder);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinPoder", DbType.Date, pApoderado.FechaFinPoder);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pApoderado.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoApoderado", DbType.String, pApoderado.EstadoApoderado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTarjetaProfesional", DbType.String, pApoderado.NumeroTarjetaProfesional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pApoderado.UsuarioModifica);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pApoderado;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pApoderado, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Valida si ya existe un Apoderado con el mismo id tercero y id denuncia bien
        /// </summary>
        /// <param name="pIdTercero">Id del tercero</param>
        /// <param name="pIdDenunciaBien"> id de la denuncia bien</param>
        /// <returns>true si existe o false si no existe</returns>
        public bool ValidarDuplicidad(int pIdTercero, int pIdDenunciaBien, int pIdApoderado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Apoderados_ValidarDuplicidad"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdApoderado", DbType.Int32, pIdApoderado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vResultado = vDataReaderResults["IdApoderado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdApoderado"].ToString()) : 0;
                        }
                    }
                    return vResultado > 0;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
