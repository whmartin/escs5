//-----------------------------------------------------------------------
// <copyright file="TipoIdentificacionDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoIdentificacionDAL.</summary>
// <author>INGENIAN SOFTWARE[Dar�o Beltr�n]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase TipoIdentificacionDAL
    /// </summary>
    public class TipoIdentificacionDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoIdentificacionDAL"/> class.
        /// </summary>
        public TipoIdentificacionDAL()
        {
        }

        /// <summary>
        /// Consultar Tipo Identificacion.
        /// </summary>
        /// <returns>Lista resultado de la operaci�n</returns>
        public List<TipoIdentificacion> ConsultarTipoIdentificacion()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_Tercero_TipoIdentificacion_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoIdentificacion> vListaTipoIdentificacion = new List<TipoIdentificacion>();
                        while (vDataReaderResults.Read())
                        {
                            TipoIdentificacion vTipoIdentificacion = new TipoIdentificacion();
                            vTipoIdentificacion.IdTipoIdentificacionPersonaNatural = vDataReaderResults["IdTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacion.IdTipoIdentificacionPersonaNatural;
                            vTipoIdentificacion.CodigoTipoIdentificacionPersonaNatural = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacion.CodigoTipoIdentificacionPersonaNatural;
                            vTipoIdentificacion.NombreTipoIdentificacionPersonaNatural = vDataReaderResults["NombreTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacion.NombreTipoIdentificacionPersonaNatural;
                            vTipoIdentificacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoIdentificacion.Estado;
                            vTipoIdentificacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoIdentificacion.UsuarioCrea;
                            vTipoIdentificacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoIdentificacion.FechaCrea;
                            vTipoIdentificacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoIdentificacion.UsuarioModifica;
                            vTipoIdentificacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoIdentificacion.FechaModifica;
                            vListaTipoIdentificacion.Add(vTipoIdentificacion);
                        }

                        return vListaTipoIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
