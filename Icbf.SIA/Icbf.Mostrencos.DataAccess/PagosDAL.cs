﻿//-----------------------------------------------------------------------
// <copyright file="PagosDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase PagosDAL.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>22/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Mostrencos.DataAccess
{
    public class PagosDAL : GeneralDAL
    {
        /// <summary>
        /// consulta los registros de la tabla Ordenes Pago
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarOrdenesPagoParticipacionById(int pIdDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_OrdenesPago_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<OrdenesPago> vListaOrdenesPago = new List<OrdenesPago>();
                        while (vDataReaderResults.Read())
                        {
                            OrdenesPago vOrdenesPago = new OrdenesPago();
                            vOrdenesPago.IdOrdenesPago = vDataReaderResults["IdOrdenesPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOrdenesPago"].ToString()) : vOrdenesPago.IdOrdenesPago;
                            vOrdenesPago.IdModalidadPago.IdModalidadPago = vDataReaderResults["IdModalidadPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadPago"].ToString()) : vOrdenesPago.IdModalidadPago.IdModalidadPago;
                            vOrdenesPago.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"]) : vOrdenesPago.IdDenunciaBien;
                            vOrdenesPago.FechaSolicitudLiquidacion = vDataReaderResults["FechaSolicitudLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudLiquidacion"]) : vOrdenesPago.FechaSolicitudLiquidacion;
                            vOrdenesPago.NumeroResolucion = vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroResolucion"].ToString()) : vOrdenesPago.NumeroResolucion;
                            vOrdenesPago.FechaResolucionPago = vDataReaderResults["FechaResolucionPago"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaResolucionPago"]) : vOrdenesPago.FechaResolucionPago;
                            vOrdenesPago.ValorAPagar = vDataReaderResults["ValorAPagar"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAPagar"].ToString()) : vOrdenesPago.ValorAPagar;
                            vOrdenesPago.NumeroComprobantePago = vDataReaderResults["NumeroComprobantePago"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroComprobantePago"].ToString()) : vOrdenesPago.NumeroComprobantePago;
                            vOrdenesPago.ValorPagado = vDataReaderResults["ValorPagado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPagado"].ToString()) : vOrdenesPago.ValorPagado;
                            vListaOrdenesPago.Add(vOrdenesPago);
                            vOrdenesPago = null;
                        }

                        return vListaOrdenesPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los registros de la tabla Ordenes Pago
        /// </summary>
        /// /// <param name="IdOrdenPago">id de la Orden de pago</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarOrdenesPagoParticipacionPorIdOrdenPago(int IdOrdenPago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_OrdenesPago_ConsultarPorIdOrdenPago"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdOrdenPago", DbType.Int32, IdOrdenPago);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<OrdenesPago> vListaOrdenesPago = new List<OrdenesPago>();
                        while (vDataReaderResults.Read())
                        {
                            OrdenesPago vOrdenesPago = new OrdenesPago();
                            vOrdenesPago.IdOrdenesPago = vDataReaderResults["IdOrdenesPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOrdenesPago"].ToString()) : vOrdenesPago.IdOrdenesPago;
                            vOrdenesPago.IdModalidadPago.IdModalidadPago = vDataReaderResults["IdModalidadPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadPago"].ToString()) : vOrdenesPago.IdModalidadPago.IdModalidadPago;
                            vOrdenesPago.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"]) : vOrdenesPago.IdDenunciaBien;
                            vOrdenesPago.FechaSolicitudLiquidacion = vDataReaderResults["FechaSolicitudLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudLiquidacion"]) : vOrdenesPago.FechaSolicitudLiquidacion;
                            vOrdenesPago.NumeroResolucion = vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroResolucion"].ToString()) : vOrdenesPago.NumeroResolucion;
                            vOrdenesPago.FechaResolucionPago = vDataReaderResults["FechaResolucionPago"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaResolucionPago"]) : vOrdenesPago.FechaResolucionPago;
                            vOrdenesPago.ValorAPagar = vDataReaderResults["ValorAPagar"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAPagar"].ToString()) : vOrdenesPago.ValorAPagar;
                            vOrdenesPago.NumeroComprobantePago = vDataReaderResults["NumeroComprobantePago"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroComprobantePago"].ToString()) : vOrdenesPago.NumeroComprobantePago;
                            vOrdenesPago.ValorPagado = vDataReaderResults["ValorPagado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPagado"].ToString()) : vOrdenesPago.ValorPagado;
                            vListaOrdenesPago.Add(vOrdenesPago);
                            vOrdenesPago = null;
                        }
                        return vListaOrdenesPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los registros de la tabla Ordenes Pago por IdDenuncia
        /// </summary>
        /// <param name="IdDenuncia">id de la Orden de la denuncia</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarOrdenesPagoParticipacionPorIdDenuncia(int IdDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_OrdenesPago_ConsultarPorIdDenuncia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenuncia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<OrdenesPago> vListaOrdenesPago = new List<OrdenesPago>();
                        while (vDataReaderResults.Read())
                        {
                            OrdenesPago vOrdenesPago = new OrdenesPago();
                            vOrdenesPago.IdOrdenesPago = vDataReaderResults["IdOrdenesPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOrdenesPago"].ToString()) : vOrdenesPago.IdOrdenesPago;
                            vOrdenesPago.IdModalidadPago.IdModalidadPago = vDataReaderResults["IdModalidadPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadPago"].ToString()) : vOrdenesPago.IdModalidadPago.IdModalidadPago;
                            vOrdenesPago.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"]) : vOrdenesPago.IdDenunciaBien;
                            vOrdenesPago.FechaSolicitudLiquidacion = vDataReaderResults["FechaSolicitudLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudLiquidacion"]) : vOrdenesPago.FechaSolicitudLiquidacion;
                            vOrdenesPago.NumeroResolucion = vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroResolucion"].ToString()) : vOrdenesPago.NumeroResolucion;
                            vOrdenesPago.FechaResolucionPago = vDataReaderResults["FechaResolucionPago"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaResolucionPago"]) : vOrdenesPago.FechaResolucionPago;
                            vOrdenesPago.ValorAPagar = vDataReaderResults["ValorAPagar"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAPagar"].ToString()) : vOrdenesPago.ValorAPagar;
                            vOrdenesPago.NumeroComprobantePago = vDataReaderResults["NumeroComprobantePago"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroComprobantePago"].ToString()) : vOrdenesPago.NumeroComprobantePago;
                            vOrdenesPago.ValorPagado = vDataReaderResults["ValorPagado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPagado"].ToString()) : vOrdenesPago.ValorPagado;
                            vListaOrdenesPago.Add(vOrdenesPago);
                            vOrdenesPago = null;
                        }
                        return vListaOrdenesPago;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Existencia de Orden de pago por número de comprobante
        /// </summary>
        /// <param name="IdDenunciaBien">Parámetro IdDenuncia</param>
        /// <param name="NoComprobante">Parámetro número de comprobante</param>
        /// <returns>Lista de Ordenes de pago</returns>
        public List<OrdenesPago> ConsultarExistenciaOrdenDePagoXComprobantePago(int IdDenunciaBien, decimal NoComprobantePago)
        {
            try
            {
                List<OrdenesPago> vListOrdenesDePago = new List<OrdenesPago>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_OrdenesPago_ConsultarPorNoComprobanteDePago"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@NoComprobanteDePago", DbType.Int32, NoComprobantePago);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            OrdenesPago vOrdenDePago = new OrdenesPago();
                            vOrdenDePago.IdOrdenesPago = vDataReaderResults["IdOrdenesPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOrdenesPago"].ToString()) : vOrdenDePago.IdOrdenesPago;
                            vOrdenDePago.IdModalidadPago.IdModalidadPago = vDataReaderResults["IdModalidadPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadPago"].ToString()) : vOrdenDePago.IdModalidadPago.IdModalidadPago;
                            vOrdenDePago.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"]) : vOrdenDePago.IdDenunciaBien;
                            vOrdenDePago.FechaSolicitudLiquidacion = vDataReaderResults["FechaSolicitudLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudLiquidacion"]) : vOrdenDePago.FechaSolicitudLiquidacion;
                            vOrdenDePago.NumeroResolucion = vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroResolucion"].ToString()) : vOrdenDePago.NumeroResolucion;
                            vOrdenDePago.FechaResolucionPago = vDataReaderResults["FechaResolucionPago"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaResolucionPago"]) : vOrdenDePago.FechaResolucionPago;
                            vOrdenDePago.ValorAPagar = vDataReaderResults["ValorAPagar"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAPagar"].ToString()) : vOrdenDePago.ValorAPagar;
                            vOrdenDePago.NumeroComprobantePago = vDataReaderResults["NumeroComprobantePago"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroComprobantePago"].ToString()) : vOrdenDePago.NumeroComprobantePago;
                            vOrdenDePago.ValorPagado = vDataReaderResults["ValorPagado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPagado"].ToString()) : vOrdenDePago.ValorPagado;
                            vListOrdenesDePago.Add(vOrdenDePago);
                        }
                        return vListOrdenesDePago;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Existencia de Orden de pago por número de resolución
        /// </summary>
        /// <param name="IdDenunciaBien">Parámetro IdDenuncia</param>
        /// <param name="NumeroDeResolucion">Parámetro número de resolucion</param>
        /// <returns>Lista de Ordenes de pago</returns>
        public List<OrdenesPago> ConsultarExistenciaOrdenDePagoXNumeroDeResolucion(int IdDenunciaBien, decimal NumeroDeResolucion)
        {
            try
            {
                List<OrdenesPago> vListOrdenesDePago = new List<OrdenesPago>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_OrdenesPago_ConsultarPorNumeroDeResolucion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroResolucion", DbType.Decimal, NumeroDeResolucion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            OrdenesPago vOrdenDePago = new OrdenesPago();
                            vOrdenDePago.IdOrdenesPago = vDataReaderResults["IdOrdenesPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOrdenesPago"].ToString()) : vOrdenDePago.IdOrdenesPago;
                            vOrdenDePago.IdModalidadPago.IdModalidadPago = vDataReaderResults["IdModalidadPago"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadPago"].ToString()) : vOrdenDePago.IdModalidadPago.IdModalidadPago;
                            vOrdenDePago.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"]) : vOrdenDePago.IdDenunciaBien;
                            vOrdenDePago.FechaSolicitudLiquidacion = vDataReaderResults["FechaSolicitudLiquidacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudLiquidacion"]) : vOrdenDePago.FechaSolicitudLiquidacion;
                            vOrdenDePago.NumeroResolucion = vDataReaderResults["NumeroResolucion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroResolucion"].ToString()) : vOrdenDePago.NumeroResolucion;
                            vOrdenDePago.FechaResolucionPago = vDataReaderResults["FechaResolucionPago"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaResolucionPago"]) : vOrdenDePago.FechaResolucionPago;
                            vOrdenDePago.ValorAPagar = vDataReaderResults["ValorAPagar"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAPagar"].ToString()) : vOrdenDePago.ValorAPagar;
                            vOrdenDePago.NumeroComprobantePago = vDataReaderResults["NumeroComprobantePago"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroComprobantePago"].ToString()) : vOrdenDePago.NumeroComprobantePago;
                            vOrdenDePago.ValorPagado = vDataReaderResults["ValorPagado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPagado"].ToString()) : vOrdenDePago.ValorPagado;
                            vListOrdenesDePago.Add(vOrdenDePago);
                        }
                        return vListOrdenesDePago;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo registro de la tabla OrdenesPago
        /// </summary>
        /// <param name="pOrdenDePago">Variable de tipo OrdenesPago con los datos a insertar</param>
        /// <returns>IdOrdenPago del registro insertado</returns>
        public int InsertarOrdenesDePago(OrdenesPago pOrdenDePago)
        {

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_OrdenesPagoInsertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdOrdenesPago", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadPago", DbType.Int32, pOrdenDePago.IdModalidadPago.IdModalidadPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pOrdenDePago.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudLiquidacion", DbType.DateTime, pOrdenDePago.FechaSolicitudLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroResolucion", DbType.Decimal, pOrdenDePago.NumeroResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaResolucionPago", DbType.DateTime, pOrdenDePago.FechaResolucionPago);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAPagar", DbType.Decimal, pOrdenDePago.ValorAPagar);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroComprobantePago", DbType.Decimal, pOrdenDePago.NumeroComprobantePago);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPagado", DbType.Decimal, pOrdenDePago.ValorPagado);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pOrdenDePago.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pOrdenDePago.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pOrdenDePago.FechaCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pOrdenDePago.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pOrdenDePago.FechaModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdOrdenesPago"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pOrdenDePago;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "INSERT";
                        this.GenerarLogAuditoria(pOrdenDePago, vDbCommand);
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita un nuevo registro de la tabla OrdenesPago
        /// </summary>
        /// <param name="pOrdenDePago">Variable de tipo OrdenesPago con los datos a insertar</param>
        /// <returns>Respuesta de la operación</returns>
        public int ActualizarOrdenesDePago(OrdenesPago pOrdenDePago)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_OrdenesPagoEditar"))
                {
                    int vResultado;

                    vDataBase.AddInParameter(vDbCommand, "@IdOrdenesPago", DbType.Int32, pOrdenDePago.IdOrdenesPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadPago", DbType.Int32, pOrdenDePago.IdModalidadPago.IdModalidadPago);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pOrdenDePago.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudLiquidacion", DbType.DateTime, pOrdenDePago.FechaSolicitudLiquidacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroResolucion", DbType.Decimal, pOrdenDePago.NumeroResolucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaResolucionPago", DbType.DateTime, pOrdenDePago.FechaResolucionPago);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAPagar", DbType.Decimal, pOrdenDePago.ValorAPagar);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroComprobantePago", DbType.Decimal, pOrdenDePago.NumeroComprobantePago);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPagado", DbType.Decimal, pOrdenDePago.ValorPagado);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pOrdenDePago.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pOrdenDePago.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pOrdenDePago.FechaCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pOrdenDePago.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pOrdenDePago.FechaModifica);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    if (vResultado > 0)
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdOrdenesPago"));
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pOrdenDePago;
                        vAuditoria.ProgramaGeneraLog = true;
                        vAuditoria.Operacion = "UPDATE";
                        this.GenerarLogAuditoria(pOrdenDePago, vDbCommand);
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
