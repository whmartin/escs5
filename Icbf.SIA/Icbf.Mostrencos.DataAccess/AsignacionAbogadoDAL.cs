using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.DataAccess
{
    public class AsignacionAbogadoDAL : GeneralDAL
    {
        public AsignacionAbogadoDAL()
        {
        }

        public int InsertarAsignacionAbogado(AsignacionAbogado pAsignacionAbogado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_AsignacionAbogado_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAbogado", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pAsignacionAbogado.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreAbogado", DbType.String, pAsignacionAbogado.NombreAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalAbogado", DbType.Int32, pAsignacionAbogado.IdRegionalAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRegionalAbogado", DbType.String, pAsignacionAbogado.NombreRegionalAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAsignacionAbogado", DbType.DateTime, pAsignacionAbogado.FechaAsignacionAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoFuncionarioAbogado", DbType.String, pAsignacionAbogado.TipoFuncionario);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pAsignacionAbogado.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAsignacionAbogado.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pAsignacionAbogado.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenuncia", DbType.Int32, pAsignacionAbogado.IdDenuncia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAsignacionAbogado.IdAbogado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAbogado"));
                    Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pAsignacionAbogado;
                    vAuditorio.ProgramaGeneraLog = true;
                    vAuditorio.Operacion = "INSERT";
                    GenerarLogAuditoria(pAsignacionAbogado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarAsignacionAbogado(AsignacionAbogado pAsignacionAbogado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_AsignacionAbogado_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAbogado", DbType.String, pAsignacionAbogado.IdAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@@Identificacion", DbType.String, pAsignacionAbogado.Identificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreAbogado", DbType.String, pAsignacionAbogado.NombreAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalAbogado", DbType.Int32, pAsignacionAbogado.IdRegionalAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRegionalAbogado", DbType.String, pAsignacionAbogado.NombreRegionalAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAsignacionAbogado", DbType.DateTime, pAsignacionAbogado.FechaAsignacionAbogado);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoFuncionarioAbogado", DbType.String, pAsignacionAbogado.TipoFuncionario);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pAsignacionAbogado.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAsignacionAbogado.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAsignacionAbogado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int EliminarAsignacionAbogado(AsignacionAbogado pAsignacionAbogado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_AsignacionAbogado_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAbogado", DbType.String, pAsignacionAbogado.IdAbogado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAsignacionAbogado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public List<AsignacionAbogado> ConsultarAbogados(AsignacionAbogado pConsulta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Abogados_Consultar"))
                {
                    if (pConsulta.NombreAbogado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pConsulta.NombreAbogado);
                    if (pConsulta.Profesion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Profesion", DbType.String, pConsulta.Profesion);
                    if (pConsulta.IdRegionalAbogado != 0)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int16, pConsulta.IdRegionalAbogado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AsignacionAbogado> vListaAsignacionAbogado  = new List<AsignacionAbogado>();
                        while (vDataReaderResults.Read())
                        {
                            AsignacionAbogado vAsignacionAbogado = new AsignacionAbogado();
                            //vAsignacionAbogado.IdAbogado = vDataReaderResults["IdAbogado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAbogado"].ToString()) : vAsignacionAbogado.IdAbogado;
                            vAsignacionAbogado.NombreAbogado = vDataReaderResults["Abogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Abogado"].ToString()) : vAsignacionAbogado.NombreAbogado;
                            vAsignacionAbogado.Identificacion = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vAsignacionAbogado.Identificacion;
                            vAsignacionAbogado.Profesion = vDataReaderResults["Profesion"] != DBNull.Value ? vDataReaderResults["Profesion"].ToString() : vAsignacionAbogado.Profesion;
                            vAsignacionAbogado.TipoFuncionario = vDataReaderResults["TipoFuncionario"] != DBNull.Value ? vDataReaderResults["TipoFuncionario"].ToString() : vAsignacionAbogado.TipoFuncionario;
                            vAsignacionAbogado.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vAsignacionAbogado.IdUsuario;
                            vAsignacionAbogado.IdRegionalAbogado = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vAsignacionAbogado.IdRegionalAbogado;
                            vAsignacionAbogado.NombreRegionalAbogado = vDataReaderResults["NombreRegional"] != DBNull.Value ? vDataReaderResults["NombreRegional"].ToString() : vAsignacionAbogado.NombreRegionalAbogado;
                            vAsignacionAbogado.Correo = vDataReaderResults["Correo"] != DBNull.Value ? vDataReaderResults["Correo"].ToString() : vAsignacionAbogado.Correo;
                            vListaAsignacionAbogado.Add(vAsignacionAbogado);
                        }
                        return vListaAsignacionAbogado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ConsultaAbogadoXDenuncia> ConsultarAsignacionAbogadosXDenuncia(ConsultaAbogadoXDenuncia pCosnultaAbogado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DenunciasAbogados_Consultar"))
                {
                    if (pCosnultaAbogado.IdRegional != 0)
                        vDataBase.AddInParameter(vDbCommand, "@Regional", DbType.Int16, pCosnultaAbogado.IdRegional);
                    if (pCosnultaAbogado.IdEstadoDenuncia != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstadoDenuncia", DbType.Int16, pCosnultaAbogado.IdEstadoDenuncia); 
                    if (pCosnultaAbogado.FechaDesde != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaDesde", DbType.DateTime, pCosnultaAbogado.FechaDesde);
                    if (pCosnultaAbogado.FechaHasta != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaHasta", DbType.DateTime, pCosnultaAbogado.FechaHasta);
                    if (pCosnultaAbogado.NumeroDenuncia != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroDenuncia", DbType.String, pCosnultaAbogado.NumeroDenuncia);
                   
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConsultaAbogadoXDenuncia> vListaAsignacionAbogado = new List<ConsultaAbogadoXDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            ConsultaAbogadoXDenuncia vAsignacionAbogado = new ConsultaAbogadoXDenuncia();
                            vAsignacionAbogado.IdDenuncia = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vAsignacionAbogado.IdDenuncia;
                            vAsignacionAbogado.NumeroDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : vAsignacionAbogado.NumeroDenuncia;
                            vAsignacionAbogado.TipoIdentificacion = vDataReaderResults["NombreTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoIdentificacionPersonaNatural"].ToString()) : vAsignacionAbogado.TipoIdentificacion;
                            vAsignacionAbogado.NumeroIdentificacion = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NUMEROIDENTIFICACION"].ToString()) : vAsignacionAbogado.NumeroIdentificacion;
                            vAsignacionAbogado.NombreTercero = vDataReaderResults["NombreTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTercero"].ToString()) : vAsignacionAbogado.NombreTercero;
                            vAsignacionAbogado.FechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vAsignacionAbogado.FechaRadicadoDenuncia;
                            vAsignacionAbogado.Regional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsignacionAbogado.Regional;
                            vAsignacionAbogado.EstadoDenuncia = vDataReaderResults["NombreEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDenuncia"].ToString()) : vAsignacionAbogado.EstadoDenuncia;
                            vAsignacionAbogado.AbogadoAsignado = vDataReaderResults["NombreAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbogado"].ToString()) : vAsignacionAbogado.AbogadoAsignado;
                            vAsignacionAbogado.FechaAsignacionAbogado = vDataReaderResults["FechaAsignacionAbogado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacionAbogado"].ToString()) : vAsignacionAbogado.FechaAsignacionAbogado;
                                vListaAsignacionAbogado.Add(vAsignacionAbogado);
                        }
                        return vListaAsignacionAbogado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string CosultarRegionalUsuario(int pIdRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegionalUsuario_Consultar"))
                {
                    string vResultado = string.Empty;
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int16, pIdRegional);
                    //IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand);
                    //vResultado = vDataReaderResults["NombreRegional"].ToString();

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vResultado = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"]) : vResultado;

                        }
                    }

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
