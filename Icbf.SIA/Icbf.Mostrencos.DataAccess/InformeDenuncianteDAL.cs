﻿using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.DataAccess
{
    public class InformeDenuncianteDAL : GeneralDAL
    {
        //Consultar Datos Informe Denunciante
        public List<InformeDenunciante> ConsultarDatosInformeDenunciante(int IdDenunciaBien, string FechaInicio, string FechaFinal)
        {
            try
            {
                List<InformeDenunciante> vlstInformeDenunciante = new List<InformeDenunciante>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_InformeDenunciante_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicial", DbType.String, FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFinal", DbType.String, FechaFinal);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            InformeDenunciante vInformeDenunciante = new InformeDenunciante();
                            vInformeDenunciante.IdInformeDenunciante = vDataReaderResults["IdInformeDenunciante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformeDenunciante"].ToString()) : vInformeDenunciante.IdInformeDenunciante;
                            vInformeDenunciante.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vInformeDenunciante.IdDenunciaBien;
                            vInformeDenunciante.FechaInforme = vDataReaderResults["FechaInforme"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInforme"].ToString()) : vInformeDenunciante.FechaInforme;
                            vInformeDenunciante.DescripcionInforme = vDataReaderResults["DescripcionInforme"] != DBNull.Value ? vDataReaderResults["DescripcionInforme"].ToString() : vInformeDenunciante.DescripcionInforme.ToString();
                            vInformeDenunciante.DocumentoInforme = vDataReaderResults["DocumentoInforme"] != DBNull.Value ? vDataReaderResults["DocumentoInforme"].ToString() : vInformeDenunciante.DocumentoInforme.ToString();
                            vInformeDenunciante.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? vDataReaderResults["NombreArchivo"].ToString() : vInformeDenunciante.NombreArchivo.ToString();
                            vlstInformeDenunciante.Add(vInformeDenunciante);
                        }
                        return vlstInformeDenunciante;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Datos Informe Denunciante por ID
        public List<InformeDenunciante> ConsultarDatosInformeDenuncianteXID(int IdInformeDenunciante)
        {
            try
            {
                List<InformeDenunciante> vlstInformeDenunciante = new List<InformeDenunciante>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_InformeDenunciante_Consultar_PorID"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdInformeDenunciante", DbType.Int32, IdInformeDenunciante);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            InformeDenunciante vInformeDenunciante = new InformeDenunciante();
                            vInformeDenunciante.IdInformeDenunciante = vDataReaderResults["IdInformeDenunciante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformeDenunciante"].ToString()) : vInformeDenunciante.IdInformeDenunciante;
                            vInformeDenunciante.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vInformeDenunciante.IdDenunciaBien;
                            vInformeDenunciante.IdDocumentoInforme = vDataReaderResults["IdDocumentoInforme"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoInforme"].ToString()) : vInformeDenunciante.IdDocumentoInforme;
                            vInformeDenunciante.FechaInforme = vDataReaderResults["FechaInforme"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInforme"].ToString()) : vInformeDenunciante.FechaInforme;
                            vInformeDenunciante.DescripcionInforme = vDataReaderResults["DescripcionInforme"] != DBNull.Value ? vDataReaderResults["DescripcionInforme"].ToString() : vInformeDenunciante.DescripcionInforme.ToString();
                            vInformeDenunciante.DocumentoInforme = vDataReaderResults["DocumentoInforme"] != DBNull.Value ? vDataReaderResults["DocumentoInforme"].ToString() : vInformeDenunciante.DocumentoInforme.ToString();
                            vInformeDenunciante.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? vDataReaderResults["NombreArchivo"].ToString() : vInformeDenunciante.NombreArchivo.ToString();
                            vlstInformeDenunciante.Add(vInformeDenunciante);
                        }
                        return vlstInformeDenunciante;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Validar Existencia del informe denunciante
        public List<InformeDenunciante> ValidarExistenciaDatosInformeDenunciante(int IdDenunciaBien, int IdDocumentoInforme, string FechaInforme)
        {
            try
            {
                List<InformeDenunciante> vlstInformeDenunciante = new List<InformeDenunciante>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_InformeDenunciante_Consultar_Existencia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoInforme", DbType.Int32, IdDocumentoInforme);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInforme", DbType.Date, FechaInforme);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            InformeDenunciante vInformeDenunciante = new InformeDenunciante();
                            vInformeDenunciante.IdInformeDenunciante = vDataReaderResults["IdInformeDenunciante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformeDenunciante"].ToString()) : vInformeDenunciante.IdInformeDenunciante;
                            vlstInformeDenunciante.Add(vInformeDenunciante);
                        }
                        return vlstInformeDenunciante;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Insertar Informe Denunciante
        public int InsertarInformeDenunciante(InformeDenunciante pInformeDenunciante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_InformeDenunciante_Insertar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddOutParameter(vDbCommand, "@IdInformeDenunciante", DbType.Int32, 18);

                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoInforme", DbType.Int32, pInformeDenunciante.IdDocumentoInforme);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pInformeDenunciante.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInforme", DbType.DateTime, pInformeDenunciante.FechaInforme);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionInforme", DbType.String, pInformeDenunciante.DescripcionInforme);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentoInforme", DbType.String, pInformeDenunciante.DocumentoInforme);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pInformeDenunciante.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInformeDenunciante.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCrea", DbType.Int32, pInformeDenunciante.idUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pInformeDenunciante.FechaCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInformeDenunciante.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pInformeDenunciante.FechaModifica);

                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                    if (vResultadoArchivo > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pInformeDenunciante;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        pInformeDenunciante.IdInformeDenunciante = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "IdInformeDenunciante").ToString());
                        GenerarLogAuditoria(pInformeDenunciante, vDbCommand);
                    }

                    return vResultadoArchivo;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Actualizar Informe Denunciante
        public int ActualizarInformeDenunciante(InformeDenunciante pInformeDenunciante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_InformeDenunciante_Actualizar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddInParameter(vDbCommand, "@IdInformeDenunciante", DbType.Int32, pInformeDenunciante.IdInformeDenunciante);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoInforme", DbType.Int32, pInformeDenunciante.IdDocumentoInforme);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pInformeDenunciante.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInforme", DbType.DateTime, pInformeDenunciante.FechaInforme);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionInforme", DbType.String, pInformeDenunciante.DescripcionInforme);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentoInforme", DbType.String, pInformeDenunciante.DocumentoInforme);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pInformeDenunciante.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInformeDenunciante.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pInformeDenunciante.FechaCrea);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInformeDenunciante.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pInformeDenunciante.FechaModifica);

                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                    if (vResultadoArchivo > 0)
                    {
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pInformeDenunciante;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "UPDATE";
                        pInformeDenunciante.IdInformeDenunciante = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "IdInformeDenunciante").ToString());
                        GenerarLogAuditoria(pInformeDenunciante, vDbCommand);
                    }

                    return vResultadoArchivo;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Actualizar Informe Denunciante
        public int EliminarInformeDenunciante(int IdInformeDenunciante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_InformeDenunciante_Eliminar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddInParameter(vDbCommand, "@IdInformeDenunciante", DbType.Int32, IdInformeDenunciante);
                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultadoArchivo;
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Datos Informe Denunciante por ID
        public List<InformeDenunciante> ConsultarDatosInformeDenuncianteIdDenuncia(int IdDenuncia)
        {
            try
            {
                List<InformeDenunciante> vlstInformeDenunciante = new List<InformeDenunciante>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_InformeDenunciante_Consultar_PorIdDenuncia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenuncia", DbType.Int32, IdDenuncia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            InformeDenunciante vInformeDenunciante = new InformeDenunciante();
                            vInformeDenunciante.IdInformeDenunciante = vDataReaderResults["IdInformeDenunciante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformeDenunciante"].ToString()) : vInformeDenunciante.IdInformeDenunciante;
                            vInformeDenunciante.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vInformeDenunciante.IdDenunciaBien;
                            vInformeDenunciante.IdDocumentoInforme = vDataReaderResults["IdDocumentoInforme"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoInforme"].ToString()) : vInformeDenunciante.IdDocumentoInforme;
                            vInformeDenunciante.FechaInforme = vDataReaderResults["FechaInforme"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInforme"].ToString()) : vInformeDenunciante.FechaInforme;
                            vInformeDenunciante.DescripcionInforme = vDataReaderResults["DescripcionInforme"] != DBNull.Value ? vDataReaderResults["DescripcionInforme"].ToString() : vInformeDenunciante.DescripcionInforme.ToString();
                            vInformeDenunciante.DocumentoInforme = vDataReaderResults["DocumentoInforme"] != DBNull.Value ? vDataReaderResults["DocumentoInforme"].ToString() : vInformeDenunciante.DocumentoInforme.ToString();
                            vInformeDenunciante.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? vDataReaderResults["NombreArchivo"].ToString() : vInformeDenunciante.NombreArchivo.ToString();
                            vlstInformeDenunciante.Add(vInformeDenunciante);
                        }
                        return vlstInformeDenunciante;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
