﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.DataAccess
{
    /// <summary>
    /// Clase que permite el enlace entre el DAL y la BD 
    /// </summary>
    /// <seealso cref="Icbf.Mostrencos.DataAccess.GeneralDAL" />
    public class RegistrosDocumentosBienDenunciadoDAL : GeneralDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegistrosDocumentosBienDenunciadoDAL"/> class.
        /// </summary>
        public RegistrosDocumentosBienDenunciadoDAL()
        {
        }

        /// <summary>
        /// Consultars the registro documentos bien denunciado.
        /// </summary>
        /// <param name="pIdRegional">The p identifier regional.</param>
        /// <param name="pEstado">The p estado.</param>
        /// <param name="pRegistradoDesde">The p registrado desde.</param>
        /// <param name="pRegistradoHasta">The p registrado hasta.</param>
        /// <param name="pRadicadoDenuncia">The p radicado denuncia.</param>
        /// <param name="pIdUsuario">The p identifier usuario.</param>
        /// <returns></returns>
        /// <exception cref="GenericException"></exception>
        public List<RegistroDenuncia> ConsultarRegistroDocumentosBienDenunciado(int pIdRegional, int pEstado, string pRegistradoDesde, string pRegistradoHasta, string pRadicadoDenuncia, int pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarDocumentosBienDenunciado_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    vDataBase.AddInParameter(vDbCommand, "@RegistradoDesde", DbType.String, pRegistradoDesde);
                    vDataBase.AddInParameter(vDbCommand, "@RegistradoHasta", DbType.String, pRegistradoHasta);
                    vDataBase.AddInParameter(vDbCommand, "@RadicadoDenuncia", DbType.String, pRadicadoDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroDenuncia> vListaRegistroDenuncia = new List<RegistroDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
                            vRegistroDenuncia.Abogado = new AsignacionAbogado();
                            vRegistroDenuncia.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vRegistroDenuncia.IdDenunciaBien;
                            vRegistroDenuncia.RadicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : vRegistroDenuncia.RadicadoDenuncia;
                            vRegistroDenuncia.NombreTipoIdentificacion = vDataReaderResults["NombreTipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoIdentificacion"].ToString()) : vRegistroDenuncia.NombreTipoIdentificacion;
                            vRegistroDenuncia.NUMEROIDENTIFICACION = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NUMEROIDENTIFICACION"].ToString()) : vRegistroDenuncia.NUMEROIDENTIFICACION;
                            vRegistroDenuncia.PRIMERNOMBRE = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vRegistroDenuncia.PRIMERNOMBRE;
                            vRegistroDenuncia.SEGUNDONOMBRE = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vRegistroDenuncia.SEGUNDONOMBRE;
                            vRegistroDenuncia.PRIMERAPELLIDO = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vRegistroDenuncia.PRIMERAPELLIDO;
                            vRegistroDenuncia.SEGUNDOAPELLIDO = vDataReaderResults["SEGUNDOAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDOAPELLIDO"].ToString()) : vRegistroDenuncia.SEGUNDOAPELLIDO;
                            vRegistroDenuncia.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vRegistroDenuncia.RazonSocial;
                            vRegistroDenuncia.NombreMostrar = vDataReaderResults["NombreMostrar"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMostrar"].ToString()) : vRegistroDenuncia.NombreMostrar;
                            vRegistroDenuncia.FechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vRegistroDenuncia.FechaRadicadoDenuncia.Date;
                            vRegistroDenuncia.NombreRegionalUbicacion = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegistroDenuncia.NombreRegionalUbicacion;
                            vRegistroDenuncia.NombreEstadoDenuncia = vDataReaderResults["NombreEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDenuncia"].ToString()) : vRegistroDenuncia.NombreEstadoDenuncia;
                            vRegistroDenuncia.Abogado.NombreAbogado = vDataReaderResults["NombreAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbogado"].ToString()) : vRegistroDenuncia.Abogado.NombreAbogado;
                            vRegistroDenuncia.Abogado.FechaAsignacionAbogado = vDataReaderResults["FechaAsignacionAbogado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAsignacionAbogado"].ToString()) : vRegistroDenuncia.Abogado.FechaAsignacionAbogado.Date;
                            vListaRegistroDenuncia.Add(vRegistroDenuncia);
                        }
                        return vListaRegistroDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Regional ConsultarRegionalDelAbogado(int pIdRegional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegionalAbogado_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Regional vRegional = new Regional();
                        while (vDataReaderResults.Read())
                        {
                            vRegional.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vRegional.IdRegional;
                            vRegional.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vRegional.NombreRegional;
                        }
                        return vRegional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultars the registro documentos bien denunciado.
        /// </summary>
        /// <param name="pIdRegional">The p identifier regional.</param>
        /// <returns></returns>
        /// <exception cref="GenericException"></exception>
        public RegistroDenuncia ConsultarDenunciaxId(int pIdDenuncia)
        {
            try
            {
                RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
                Database vDataBase = ObtenerInstancia();



                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarDocumentosBienDenunciado_ConsultarDenuncia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenuncia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {

                            vRegistroDenuncia.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vRegistroDenuncia.IdDenunciaBien;
                            vRegistroDenuncia.RadicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : vRegistroDenuncia.RadicadoDenuncia;
                            vRegistroDenuncia.FechaRadicadoDenuncia = vDataReaderResults["FechaRadicadoDenuncia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoDenuncia"].ToString()) : vRegistroDenuncia.FechaRadicadoDenuncia.Date;
                            vRegistroDenuncia.RadicadoCorrespondencia = vDataReaderResults["RadicadoCorrespondencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoCorrespondencia"].ToString()) : vRegistroDenuncia.RadicadoCorrespondencia;
                            vRegistroDenuncia.FechaRadicadoCorrespondencia = vDataReaderResults["FechaRadicadoCorrespondencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRadicadoCorrespondencia"].ToString()) : vRegistroDenuncia.FechaRadicadoCorrespondencia;
                            vRegistroDenuncia.IDTIPODOCIDENTIFICA = vDataReaderResults["IdTipoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacion"].ToString()) : vRegistroDenuncia.IDTIPODOCIDENTIFICA;
                            vRegistroDenuncia.CodigoTipoIdentificacion = vDataReaderResults["NombreTipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoIdentificacion"].ToString()) : vRegistroDenuncia.CodigoTipoIdentificacion;
                            vRegistroDenuncia.NUMEROIDENTIFICACION = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NUMEROIDENTIFICACION"].ToString()) : vRegistroDenuncia.NUMEROIDENTIFICACION;
                            vRegistroDenuncia.PRIMERNOMBRE = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vRegistroDenuncia.PRIMERNOMBRE;
                            vRegistroDenuncia.SEGUNDONOMBRE = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vRegistroDenuncia.SEGUNDONOMBRE;
                            vRegistroDenuncia.PRIMERAPELLIDO = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vRegistroDenuncia.PRIMERAPELLIDO;
                            vRegistroDenuncia.SEGUNDOAPELLIDO = vDataReaderResults["SEGUNDOAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDOAPELLIDO"].ToString()) : vRegistroDenuncia.SEGUNDOAPELLIDO;
                            vRegistroDenuncia.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vRegistroDenuncia.RazonSocial;
                            vRegistroDenuncia.NombreMostrar = vDataReaderResults["NombreMostrar"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMostrar"].ToString()) : vRegistroDenuncia.NombreMostrar;
                            vRegistroDenuncia.DescripcionDenuncia = vDataReaderResults["DescripcionDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDenuncia"].ToString()) : vRegistroDenuncia.DescripcionDenuncia;
                            vRegistroDenuncia.ClaseDenuncia = vDataReaderResults["ClaseDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ClaseDenuncia"].ToString()) : vRegistroDenuncia.ClaseDenuncia;
                            vRegistroDenuncia.CORREOELECTRONICO = vDataReaderResults["correoElectronico"] != DBNull.Value ? Convert.ToString(vDataReaderResults["correoElectronico"].ToString()) : vRegistroDenuncia.CORREOELECTRONICO;
                            vRegistroDenuncia.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vRegistroDenuncia.IdEstado;
                            vRegistroDenuncia.DIGITOVERIFICACION = vDataReaderResults["DIGITOVERIFICACION"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DIGITOVERIFICACION"].ToString()) : vRegistroDenuncia.DIGITOVERIFICACION;
                            vRegistroDenuncia.Existe = vDataReaderResults["ExisteDenuncia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ExisteDenuncia"].ToString()) : false;
                            vRegistroDenuncia.DocumentacionCompleta = vDataReaderResults["DocumentacionCompleta"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["DocumentacionCompleta"].ToString()) : false;
                            vRegistroDenuncia.TipoTramite = vDataReaderResults["TipoTramite"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoTramite"].ToString()) : vRegistroDenuncia.TipoTramite;

                            using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("[usp_BVMVH_HistoricoEstadosDenunciaBien_Consultar]"))
                            {
                                vDataBase.AddInParameter(vDbCommand2, "@IdDenunciaBien", DbType.Int32, pIdDenuncia);

                                using (IDataReader vDataReaderResults2 = vDataBase.ExecuteReader(vDbCommand2))
                                {
                                    List<HistoricoEstadosDenunciaBien> vListaHistoricoEstadosDenunciaBien = new List<HistoricoEstadosDenunciaBien>();
                                    while (vDataReaderResults2.Read())
                                    {
                                        HistoricoEstadosDenunciaBien vHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
                                        vHistoricoEstadosDenunciaBien.IdEstadoDenuncia = vDataReaderResults2["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults2["IdEstadoDenuncia"].ToString()) : vHistoricoEstadosDenunciaBien.IdEstadoDenuncia;
                                        vHistoricoEstadosDenunciaBien.NombreEstadoDenuncia = vDataReaderResults2["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults2["NombreEstado"]) : vHistoricoEstadosDenunciaBien.NombreEstadoDenuncia;
                                        vHistoricoEstadosDenunciaBien.FechaCrea = vDataReaderResults2["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults2["FechaCrea"].ToString()) : vHistoricoEstadosDenunciaBien.FechaCrea;
                                        vHistoricoEstadosDenunciaBien.IdFase = vDataReaderResults2["Fase"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults2["Fase"].ToString()) : vHistoricoEstadosDenunciaBien.IdFase;
                                        vHistoricoEstadosDenunciaBien.IdAccion = vDataReaderResults2["Accion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults2["Accion"].ToString()) : vHistoricoEstadosDenunciaBien.IdAccion;
                                        vHistoricoEstadosDenunciaBien.IdActuacion = vDataReaderResults2["Actuacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults2["Actuacion"].ToString()) : vHistoricoEstadosDenunciaBien.IdActuacion;

                                        vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Add(vHistoricoEstadosDenunciaBien);
                                    }

                                }
                            }

                        }
                        return vRegistroDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultars the denunciax identifier.
        /// </summary>
        /// <param name="pIdDenuncia">The p identifier denuncia.</param>
        /// <returns></returns>
        /// <exception cref="GenericException"></exception>
        //public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosXIdDenuncia(int pIdDenuncia, int pIdEstado)
        //{
        //    try
        //    {
        //        List<DocumentosSolicitadosDenunciaBien> vLstDocumentos  = new List<DocumentosSolicitadosDenunciaBien>();
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarDocumentosBienDenunciado_ConsultarDocumentos"))
        //        {
        //            vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenuncia);
        //            vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pIdEstado);

        //            using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
        //            {
        //                while (vDataReaderResults.Read())
        //                {
        //                    DocumentosSolicitadosDenunciaBien vDocumento = new DocumentosSolicitadosDenunciaBien();
        //                    vDocumento.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumento.IdDocumentosSolicitadosDenunciaBien;
        //                    vDocumento.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumento.IdDenunciaBien;
        //                    vDocumento.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumento.IdTipoDocumentoBienDenunciado;
        //                    vDocumento.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vDocumento.NombreTipoDocumento;
        //                    vDocumento.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vDocumento.FechaSolicitud;
        //                    vDocumento.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"].ToString()) : vDocumento.ObservacionesDocumentacionSolicitada;
        //                    vDocumento.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumento.IdEstadoDocumento;
        //                    vDocumento.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumento.FechaRecibido;
        //                    vDocumento.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(!string.IsNullOrEmpty(vDataReaderResults["NombreArchivo"].ToString()) ? vDataReaderResults["NombreArchivo"].ToString().Contains("/") ? vDataReaderResults["NombreArchivo"].ToString().Split('/')[1] : (vDataReaderResults["NombreArchivo"].ToString().Contains('\\') ? vDataReaderResults["NombreArchivo"].ToString().Split('\\')[1] : vDataReaderResults["NombreArchivo"].ToString()) : vDataReaderResults["NombreArchivo"].ToString()) : vDocumento.NombreArchivo;
        //                    vDocumento.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vDocumento.RutaArchivo;
        //                    vDocumento.DocumentacionCompleta = vDataReaderResults["DocumentacionCompleta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DocumentacionCompleta"].ToString()) : vDocumento.DocumentacionCompleta;
        //                    vDocumento.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionRecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionRecibida"].ToString()) : vDocumento.ObservacionesDocumentacionRecibida;
        //                    vDocumento.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vDocumento.NombreEstado;
        //                    if (vDocumento.FechaRecibido == null )
        //                    {
        //                        vDocumento.FechaRecibidoGrilla = "";
        //                    }
        //                    else
        //                    {
        //                        vDocumento.FechaRecibidoGrilla = Convert.ToDateTime(vDocumento.FechaRecibido).ToString("dd/MM/yyyy");
        //                    }

        //                    if (vDocumento.FechaSolicitud == new DateTime())
        //                    {
        //                        vDocumento.FechaSolicitudGrilla = "";
        //                    }
        //                    else
        //                    {
        //                        vDocumento.FechaSolicitudGrilla = vDocumento.FechaSolicitud.ToString("dd/MM/yyyy");
        //                    }
        //                    vLstDocumentos.Add(vDocumento);
        //                }
        //                return vLstDocumentos;
        //            }
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}


        /// <summary>
        /// Consultars the denunciax identifier.
        /// </summary>
        /// <param name="pIdDenuncia">The p identifier denuncia.</param>
        /// <returns></returns>
        /// <exception cref="GenericException"></exception>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosXIdDenuncia(int pIdDenuncia, int pIdEstado)
        {
            try
            {
                List<DocumentosSolicitadosDenunciaBien> vLstDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistrarDocumentosBienDenunciado_ConsultarDocumentos"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pIdDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pIdEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            DocumentosSolicitadosDenunciaBien vDocumento = new DocumentosSolicitadosDenunciaBien();
                            Tercero tercero = new Tercero();
                            vDocumento.IdDocumentosSolicitadosDenunciaBien = vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentosSolicitadosDenunciaBien"].ToString()) : vDocumento.IdDocumentosSolicitadosDenunciaBien;
                            vDocumento.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vDocumento.IdDenunciaBien;
                            vDocumento.IdTipoDocumentoBienDenunciado = vDataReaderResults["IdTipoDocumentoBienDenunciado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoBienDenunciado"].ToString()) : vDocumento.IdTipoDocumentoBienDenunciado;
                            vDocumento.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vDocumento.NombreTipoDocumento;
                            vDocumento.FechaSolicitud = vDataReaderResults["FechaSolicitud"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitud"].ToString()) : vDocumento.FechaSolicitud;
                            vDocumento.ObservacionesDocumentacionSolicitada = vDataReaderResults["ObservacionesDocumentacionSolicitada"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionSolicitada"].ToString()) : vDocumento.ObservacionesDocumentacionSolicitada;
                            vDocumento.IdEstadoDocumento = vDataReaderResults["IdEstadoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDocumento"].ToString()) : vDocumento.IdEstadoDocumento;
                            vDocumento.FechaRecibido = vDataReaderResults["FechaRecibido"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRecibido"].ToString()) : vDocumento.FechaRecibido;
                            vDocumento.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(!string.IsNullOrEmpty(vDataReaderResults["NombreArchivo"].ToString()) ? vDataReaderResults["NombreArchivo"].ToString().Contains("/") ? vDataReaderResults["NombreArchivo"].ToString().Split('/')[1] : (vDataReaderResults["NombreArchivo"].ToString().Contains('\\') ? vDataReaderResults["NombreArchivo"].ToString().Split('\\')[1] : vDataReaderResults["NombreArchivo"].ToString()) : vDataReaderResults["NombreArchivo"].ToString()) : vDocumento.NombreArchivo;
                            vDocumento.RutaArchivo = vDataReaderResults["RutaArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RutaArchivo"].ToString()) : vDocumento.RutaArchivo;
                            vDocumento.DocumentacionCompleta = vDataReaderResults["DocumentacionCompleta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DocumentacionCompleta"].ToString()) : vDocumento.DocumentacionCompleta;
                            vDocumento.ObservacionesDocumentacionRecibida = vDataReaderResults["ObservacionesDocumentacionRecibida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesDocumentacionRecibida"].ToString()) : vDocumento.ObservacionesDocumentacionRecibida;
                            vDocumento.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vDocumento.NombreEstado;

                            tercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"]) : tercero.IdTercero;
                            tercero.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vDocumento.Tercero.RazonSocial;
                            vDocumento.Tercero = tercero;
                            vDocumento.FechaOficio = vDataReaderResults["FechaOficio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaOficio"].ToString()) : vDocumento.FechaOficio;
                            vDocumento.AsuntoOficio = vDataReaderResults["AsuntoOficio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AsuntoOficio"].ToString()) : vDocumento.AsuntoOficio;
                            vDocumento.ResumenOficio = vDataReaderResults["ResumenOficio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ResumenOficio"].ToString()) : vDocumento.ResumenOficio;

                            if (vDocumento.FechaRecibido == null)
                            {
                                vDocumento.FechaRecibidoGrilla = "";
                            }
                            else
                            {
                                vDocumento.FechaRecibidoGrilla = Convert.ToDateTime(vDocumento.FechaRecibido).ToString("dd/MM/yyyy");
                            }

                            if (vDocumento.FechaSolicitud == new DateTime())
                            {
                                vDocumento.FechaSolicitudGrilla = "";
                            }
                            else
                            {
                                vDocumento.FechaSolicitudGrilla = vDocumento.FechaSolicitud.ToString("dd/MM/yyyy");
                            }
                            vLstDocumentos.Add(vDocumento);

                            if (vDocumento.FechaOficio == null)
                            {
                                vDocumento.FechaOficioGrilla = "";
                            }
                            else
                            {
                                vDocumento.FechaOficioGrilla = Convert.ToDateTime(vDocumento.FechaOficio).ToString("dd/MM/yyyy");
                            }
                        }
                        return vLstDocumentos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        //public int InsertarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia(); 
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Insertar"))
        //        {
        //            int vResultadoArchivo;
        //            vDataBase.AddOutParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, 18);
        //            vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDenunciaBien);
        //            vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado);

        //            if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada))
        //            {
        //                vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada);
        //            }

        //            vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);

        //            vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.NombreArchivo);
        //            vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.RutaArchivo);

        //            if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento))
        //            {
        //                vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumento", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento);
        //            }

        //            if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta))
        //            {
        //                vDataBase.AddInParameter(vDbCommand, "@DocumentacionCompleta", DbType.String, pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta);
        //            }

        //            if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida))
        //            {
        //                vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida);
        //            }

        //            if (pDocumentosSolicitadosDenunciaBien.FechaSolicitud != null && pDocumentosSolicitadosDenunciaBien.FechaSolicitud != Convert.ToDateTime("1/01/0001"))
        //            {
        //                vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaSolicitud);
        //            }

        //            if (pDocumentosSolicitadosDenunciaBien.FechaRecibido != null && pDocumentosSolicitadosDenunciaBien.FechaRecibido != Convert.ToDateTime("01/01/0001"))
        //            {
        //                vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaRecibido);
        //            }

        //            vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioCrea);

        //            vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

        //            Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosDenunciaBien;
        //            vAuditorio.ProgramaGeneraLog = true;
        //            vAuditorio.Operacion = "INSERT";
        //            pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien").ToString());
        //            GenerarLogAuditoria(pDocumentosSolicitadosDenunciaBien, vDbCommand);
        //            return vResultadoArchivo;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        public int InsertarDocumentosSolicitadosDenunciaBienObtieneId(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien, ref int IdDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Insertar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado);
                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.RutaArchivo);
                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumento", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento);
                    }
                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@DocumentacionCompleta", DbType.String, pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta);
                    }
                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida);
                    }
                    if (pDocumentosSolicitadosDenunciaBien.FechaSolicitud != null && pDocumentosSolicitadosDenunciaBien.FechaSolicitud != Convert.ToDateTime("1/01/0001"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaSolicitud);
                    }
                    if (pDocumentosSolicitadosDenunciaBien.FechaRecibido != null && pDocumentosSolicitadosDenunciaBien.FechaRecibido != Convert.ToDateTime("01/01/0001"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaRecibido);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioCrea);

                    if (pDocumentosSolicitadosDenunciaBien.Tercero.IdTercero > 0)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pDocumentosSolicitadosDenunciaBien.Tercero.IdTercero);
                    }

                    if (pDocumentosSolicitadosDenunciaBien.FechaOficio != null
                        && pDocumentosSolicitadosDenunciaBien.FechaOficio != Convert.ToDateTime("1/01/0001")
                        && pDocumentosSolicitadosDenunciaBien.FechaOficio != Convert.ToDateTime("1/01/1900"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaOficio", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaOficio);
                    }

                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.AsuntoOficio))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@AsuntoOficio", DbType.String, pDocumentosSolicitadosDenunciaBien.AsuntoOficio);
                    }

                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ResumenOficio))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ResumenOficio", DbType.String, pDocumentosSolicitadosDenunciaBien.ResumenOficio);
                    }

                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                    IdDocumentosSolicitadosDenunciaBien = (int)vDbCommand.Parameters["@IdDocumentosSolicitadosDenunciaBien"].Value;

                    return vResultadoArchivo;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Insertar"))
                {
                    int vResultadoArchivo;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado);
                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionSolicitada", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.RutaArchivo);
                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumento", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento);
                    }
                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@DocumentacionCompleta", DbType.String, pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta);
                    }
                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida);
                    }
                    if (pDocumentosSolicitadosDenunciaBien.FechaSolicitud != null && pDocumentosSolicitadosDenunciaBien.FechaSolicitud != Convert.ToDateTime("1/01/0001"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaSolicitud", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaSolicitud);
                    }
                    if (pDocumentosSolicitadosDenunciaBien.FechaRecibido != null && pDocumentosSolicitadosDenunciaBien.FechaRecibido != Convert.ToDateTime("01/01/0001"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaRecibido);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioCrea);

                    if (pDocumentosSolicitadosDenunciaBien.Tercero.IdTercero > 0)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pDocumentosSolicitadosDenunciaBien.Tercero.IdTercero);
                    }

                    if (pDocumentosSolicitadosDenunciaBien.FechaOficio != null
                        && pDocumentosSolicitadosDenunciaBien.FechaOficio != Convert.ToDateTime("1/01/0001")
                        && pDocumentosSolicitadosDenunciaBien.FechaOficio != Convert.ToDateTime("1/01/1900"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaOficio", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaOficio);
                    }

                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.AsuntoOficio))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@AsuntoOficio", DbType.String, pDocumentosSolicitadosDenunciaBien.AsuntoOficio);
                    }

                    if (!String.IsNullOrEmpty(pDocumentosSolicitadosDenunciaBien.ResumenOficio))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ResumenOficio", DbType.String, pDocumentosSolicitadosDenunciaBien.ResumenOficio);
                    }

                    vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultadoArchivo;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int InsertarDocumentosSolicitadosDenunciaBienOtros(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            int vResultadoArchivo = 0;
            try
            {
                Database vDataBase = ObtenerInstancia();

                //int vResultadoReturn = 1;
                List<DocumentosSolicitadosDenunciaBien> vListaOtrosDocumentos = pDocumentosSolicitadosDenunciaBien.OtrosDocumentos;
                foreach (DocumentosSolicitadosDenunciaBien vOtrosDocumentos in vListaOtrosDocumentos)
                {
                    using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Insertar"))
                    {
                        vDataBase.AddOutParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, 18);
                        vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDenunciaBien);
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado);
                        vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                        vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, vOtrosDocumentos.NombreArchivo);
                        vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.IdDenunciaBien + @"\" + vOtrosDocumentos.NombreArchivo);
                        vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumento", DbType.String, vOtrosDocumentos.ObservacionesDocumento);
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioCrea);

                        vResultadoArchivo = vDataBase.ExecuteNonQuery(vDbCommand);
                        //pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien").ToString());
                        Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosDenunciaBien;
                        vAuditorio.ProgramaGeneraLog = true;
                        vAuditorio.Operacion = "INSERT";
                        GenerarLogAuditoria(pDocumentosSolicitadosDenunciaBien, vDbCommand);

                    }
                }

                return vResultadoArchivo;

                //return vResultadoReturn;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoBienDenunciado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.RutaArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioModifica);

                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.String, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentosSolicitadosDenunciaBien.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.NombreArchivo);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    Icbf.Seguridad.Entity.EntityAuditoria vAuditorio = (Icbf.Seguridad.Entity.EntityAuditoria)pDocumentosSolicitadosDenunciaBien;
                    vAuditorio.ProgramaGeneraLog = true;
                    vAuditorio.Operacion = "UPDATE";
                    GenerarLogAuditoria(pDocumentosSolicitadosDenunciaBien, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarEstadoDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_ActualizarEstado"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoDenuncia", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocumentosSolicitadosDenunciaBien, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDocumentosSolicitadosDenunciaBienRecibidos(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_ModificarRecibido"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentosSolicitadosDenunciaBien", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDocumento", DbType.Int32, pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRecibido", DbType.DateTime, pDocumentosSolicitadosDenunciaBien.FechaRecibido);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesDocumentacionRecibida", DbType.String, pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentacionCompleta", DbType.String, pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo ", DbType.String, pDocumentosSolicitadosDenunciaBien.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@RutaArchivo", DbType.String, pDocumentosSolicitadosDenunciaBien.RutaArchivo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método para Modificar un Registro de denuncia.
        /// </summary>
        /// <param name="pRegistroDenuncia">Entidad a modificar</param>
        /// <returns>Resultado de la operación</returns>
        public int ModificarDenunciaDocumentosSolicitadosDenunciaBien(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_ModificarDenuncia"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pRegistroDenuncia.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pRegistroDenuncia.DescripcionDenuncia);
                    vDataBase.AddInParameter(vDbCommand, "@IdClaseDenuncia", DbType.String, pRegistroDenuncia.ClaseDenuncia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    this.GenerarLogAuditoria(pRegistroDenuncia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar un Registro de denuncia.
        /// </summary>
        /// <param name="pRegistroDenuncia">Entidad a modificar</param>
        /// <returns>Resultado de la operación</returns>
        public int EliminarDenunciaDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pRegistroDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_DocumentosSolicitadosDenunciaBien_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, pRegistroDenuncia.IdDenunciaBien);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.String, pRegistroDenuncia.IdTipoDocumentoBienDenunciado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    this.GenerarLogAuditoria(pRegistroDenuncia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
