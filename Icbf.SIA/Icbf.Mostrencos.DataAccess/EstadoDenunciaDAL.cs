﻿//-----------------------------------------------------------------------
// <copyright file="EstadoDenunciaDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase EstadoDenunciaDAL.</summary>
// <author>INDESAP[Jesus Eduardo Cortes]</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using Microsoft.Practices.EnterpriseLibrary.Data;

    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class EstadoDenunciaDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta todos los estados denuncia que estén activos
        /// </summary>
        /// <returns>Lista de estados denuncia</returns>
        public List<EstadoDenuncia> ConsultarEstadoDenuncia()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EstadoDenuncia_ConsultarActivos"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoDenuncia> vListaEstadosDenuncia = new List<EstadoDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoDenuncia vEstadodenuncia = new EstadoDenuncia();
                            vEstadodenuncia.CodigoEstadoDenuncia = vDataReaderResults["CodigoEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoDenuncia"].ToString()) : vEstadodenuncia.CodigoEstadoDenuncia;
                            vEstadodenuncia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vEstadodenuncia.Estado;
                            vEstadodenuncia.IdEstadoDenuncia = vDataReaderResults["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDenuncia"].ToString()) : vEstadodenuncia.IdEstadoDenuncia;
                            vEstadodenuncia.NombreEstadoDenuncia = vDataReaderResults["NombreEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDenuncia"].ToString()) : vEstadodenuncia.NombreEstadoDenuncia;
                            vEstadodenuncia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadodenuncia.UsuarioCrea;
                            vEstadodenuncia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadodenuncia.FechaCrea;
                            vEstadodenuncia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadodenuncia.UsuarioModifica;
                            vEstadodenuncia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadodenuncia.FechaModifica;
                            vListaEstadosDenuncia.Add(vEstadodenuncia);
                            vEstadodenuncia = null;
                        }

                        return vListaEstadosDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los estados denuncia por el IdEstadoDenuncia
        /// </summary>
        /// <param name="pIdEstadoDenuncia">Id del estado denuncia</param>
        /// <returns>Estado denuncia consultado</returns>
        public EstadoDenuncia ConsultarEstadoDenunciaPorId(int pIdEstadoDenuncia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EstadoDenuncia_ConsultarPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDenuncia", DbType.Int32, pIdEstadoDenuncia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoDenuncia vEstadodenuncia = new EstadoDenuncia();
                        while (vDataReaderResults.Read())
                        {
                            vEstadodenuncia.CodigoEstadoDenuncia = vDataReaderResults["CodigoEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoDenuncia"].ToString()) : vEstadodenuncia.CodigoEstadoDenuncia;
                            vEstadodenuncia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vEstadodenuncia.Estado;                            
                            vEstadodenuncia.NombreEstadoDenuncia = vDataReaderResults["NombreEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDenuncia"].ToString()) : vEstadodenuncia.NombreEstadoDenuncia;
                            vEstadodenuncia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadodenuncia.UsuarioCrea;
                            vEstadodenuncia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadodenuncia.FechaCrea;
                            vEstadodenuncia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadodenuncia.UsuarioModifica;
                            vEstadodenuncia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadodenuncia.FechaModifica;
                        }

                        vEstadodenuncia.IdEstadoDenuncia = pIdEstadoDenuncia;
                        return vEstadodenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta los Estados de la Denuncia para Repoorte
        /// </summary>
        /// <returns>Lista Filtrada de Estados</returns>
        public List<EstadoDenuncia> ConsultarEstadosDenunciasReportes()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_EstadoDenuncia_ConsultarEstadosDenunciaReporte"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoDenuncia> vListaEstadosDenuncia = new List<EstadoDenuncia>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoDenuncia vEstadodenuncia = new EstadoDenuncia();
                            vEstadodenuncia.IdEstadoDenuncia = vDataReaderResults["IdEstadoDenuncia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDenuncia"].ToString()) : vEstadodenuncia.IdEstadoDenuncia;
                            vEstadodenuncia.NombreEstadoDenuncia = vDataReaderResults["NombreEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDenuncia"].ToString()) : vEstadodenuncia.NombreEstadoDenuncia;
                            vListaEstadosDenuncia.Add(vEstadodenuncia);
                            vEstadodenuncia = null;
                        }

                        return vListaEstadosDenuncia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
