﻿//using Icbf.ActualizacionAutomatica.Entity;
using Icbf.ActualizacionAutomatica.Service;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ActualizacionAutomatica
{
    partial class Service1 : ServiceBase
    {
        ActualizacionAutomaticaService vActualizacionMasivaService = new ActualizacionAutomaticaService();
        ResultadoEstudioSectorService vEstudioSectorCostoService = new ResultadoEstudioSectorService();
        EstudioSectorCostoService vEstudioSectorCostoService1 = new EstudioSectorCostoService();
        ReporteConsultaSeguimientoService vSeguimientos = new ReporteConsultaSeguimientoService();
        public Thread objThread = null;
        public Service1()
        {
            InitializeComponent();
        }
        public void onDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            objThread = new Thread(Inicio);
            objThread.Start();
        }

        protected override void OnStop()
        {
        }
        public void Inicio()
        {
            try
            {
                //int timeOut = Convert.ToInt32(ConfigurationManager.AppSettings.Get("time-out"));
                string HoraInicio = ConfigurationManager.AppSettings.Get("Hora");
                
                if (DateTime.Now.Hour == Convert.ToInt16(HoraInicio.Split(':')[0]) )
                {
                    ActualizacionMasiva();
                    Thread.Sleep(60000 * 60);
                }
                Thread.Sleep(60000 * 20);
                GC.SuppressFinalize(this);
                Inicio();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error General " + ex.Message);
                Inicio();
            }
        }
        public void ActualizacionMasiva()
        {
            TiempoEntreActividadesEstudioSyC vTiempoEntreActividades = new TiempoEntreActividadesEstudioSyC();

            //string Userp = GetSessionUser().NombreUsuario;
            List<Icbf.ActualizacionAutomatica.Entity.RegistroSolicitudEstudioSectoryCaso> ListaRegistroSolicitudEstudioSectoryCaso = vActualizacionMasivaService.ConsultarRegistroSolicitudEstudioSectoryCasos();

            
            foreach (var item in ListaRegistroSolicitudEstudioSectoryCaso)//.Where(T => T.FechaCrea.Year == DateTime.Now.Year)
            {
                int vig = 0;
                if (item.VigenciaPACCO.Trim() != "")
                {
                    vig = Convert.ToInt32(item.VigenciaPACCO);
                }

                //actualiza campos
                wsContratos.WSContratosPACCOSoapClient ws = new wsContratos.WSContratosPACCOSoapClient();
                wsContratos.GetObjetosContractualesServicioXModalidad_Result[] res = ws.GetListaConsecutivosEncabezadosPACCOXModalidad(vig, null, 0, 0, item.IdRegistroSolicitudEstudioSectoryCaso, null, 0, 0, null, 0);

                //consulto tiempo entre actividades
                List<TiempoEntreActividadesConsulta> vTiempos =
                vSeguimientos.ConsultarTiempos(item.IdRegistroSolicitudEstudioSectoryCaso, 1);

                if (res != null && res.ToList().Count > 0)
                {
                    string area = res[0].area;
                    string codigoarea = res[0].codigoarea != null ? res[0].codigoarea.ToString() : "";
                    string estado = res[0].estado != null ? res[0].estado.ToString() : "";
                    string FechaInicioProceso = res[0].FechaInicioProceso.Value.ToString("yyyyMMdd");
                    string Id_modalidad = res[0].Id_modalidad == null ? "0" : res[0].Id_modalidad.Value.ToString().Trim();
                    decimal? Id_tipo_contrato = res[0].Id_tipo_contrato;
                    string modalidad = res[0].modalidad.Trim();
                    string objeto_contractual = res[0].objeto_contractual;
                    string tipo_contrato = res[0].tipo_contrato;
                    int vigenciapacco = res[0].anop_vigencia;

                    string FechaInicioEjecucion = res[0].FechaInicioEjecucion.Value.ToString("yyyyMMdd");

                    decimal valor_contrato = res[0].valor_contrato == null ? 0 : res[0].valor_contrato.Value;
                    string pFechaEstimadaInicioEjecucion = res[0].FechaInicioEjecucion.Value.ToString("dd/MM/yyyy");
                    string pIdModalidadSeleccionPACCO = res[0].Id_modalidad == null ? "0" : res[0].Id_modalidad.Value.ToString().Trim();
                    string pModalidadSeleccionPACCO = res[0].modalidad.Trim();
                    decimal pValorContrato = res[0].valor_contrato == null ? 0 : res[0].valor_contrato.Value;
                    //CU11
                    vActualizacionMasivaService.ActualizarRegistroSolicitudEstudioSectoryCasos(vigenciapacco, Convert.ToDecimal(item.IdRegistroSolicitudEstudioSectoryCaso), area, codigoarea, estado, FechaInicioProceso, Id_modalidad, Id_tipo_contrato, modalidad, objeto_contractual, tipo_contrato, FechaInicioEjecucion, valor_contrato);

                    //CU15
                    var vRes = new Icbf.SIA.Integrations.ClientObjectModel.EstudioSectoresCOM().ConsultaFechasPACCO<TiemposPACCO>(item.IdRegistroSolicitudEstudioSectoryCaso.ToString(), pFechaEstimadaInicioEjecucion, pIdModalidadSeleccionPACCO, pModalidadSeleccionPACCO, pValorContrato);

                    //pacco
                    
                    if (vRes != null)
                    {
                        TiemposPACCO vTiempoPACCO = new TiemposPACCO();
                        vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL = vRes.FEstimadaFCTPreliminarREGINICIAL;
                        vTiempoPACCO.FEstimadaFCTPreliminar = vRes.FEstimadaFCTPreliminar;

                        vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vRes.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;

                        vTiempoPACCO.FEestimadaES_EC = vRes.FEestimadaES_EC;

                        vTiempoPACCO.FEstimadaDocsRadicadosEnContratos = vRes.FEstimadaDocsRadicadosEnContratos;

                        vTiempoPACCO.FEstimadaComiteContratacion = vRes.FEstimadaComiteContratacion;

                        vTiempoPACCO.FEstimadaInicioDelPS = vRes.FEstimadaInicioDelPS;

                        vTiempoPACCO.FEstimadaPresentacionPropuestas = vRes.FEstimadaPresentacionPropuestas;

                        vTiempoPACCO.FEstimadaDeInicioDeEjecucion = pFechaEstimadaInicioEjecucion;
                        vTiempoPACCO.IdTiempoEntreActividades = vTiempos[0].IdTiempoEntreActividades;
                        vTiempoPACCO.UsuarioModifica = "";
                        vEstudioSectorCostoService.ModificarTiempoEntreActividadesPACCO(vTiempoPACCO);
                    }
                    /////
                    //ESyC
                    TiempoEntreActividadesEstudioSyC vTiemposESyC = new TiempoEntreActividadesEstudioSyC();
                    vTiemposESyC = vEstudioSectorCostoService.ConsultarTiemposESC(item.IdRegistroSolicitudEstudioSectoryCaso, "NO APLICA");                   

                    vTiemposESyC.FentregaES_ECTiemposEquipo = vTiemposESyC.FentregaES_ECTiemposEquipo;
                    vTiemposESyC.FEentregaES_ECTiemposIndicador = vTiemposESyC.FEentregaES_ECTiemposIndicador;
                    vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado = vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
                    //vTiemposESyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR = lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Visible; 
                    //vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor = txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.BackColor.Name;
                    vTiemposESyC.DiasHabilesEntreFCTFinalYSDC = vTiemposESyC.DiasHabilesEntreFCTFinalYSDC;
                    //vTiemposESyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR = lblDiasHabilesEntreFCTFinalYSDC.Visible;
                    //vTiemposESyC.DiasHabilesEntreFCTFinalYSDCColor = txtDiasHabilesEntreFCTFinalYSDC.BackColor.Name;
                    vTiemposESyC.DiasHabilesEnESOCOSTEO = vTiemposESyC.DiasHabilesEnESOCOSTEO;
                    //vTiemposESyC.NoAplicaDiasHabilesEnESOCOSTEOR = lblDiasHabilesEnESOCOSTEO.Visible;
                    //vTiemposESyC.DevueltoDiasHabilesEnESOCOSTEOR = vTiemposESyC.DiashabilesParaDevolucion;
                    //vTiemposESyC.DiasHabilesEnESOCOSTEOColor = txtDiasHabilesEnESOCOSTEO.BackColor.Name;
                    vTiemposESyC.DiashabilesParaDevolucion = vTiemposESyC.DiashabilesParaDevolucion;
                    //vTiemposESyC.NoAplicaDiasHabilesParaDevolucionR = lblDiashabilesParaDevolucion.Visible;
                    //vTiemposESyC.DiashabilesParaDevolucionColor = txtDiashabilesParaDevolucion.BackColor.Name;
                    vTiemposESyC.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                    vTiemposESyC.UsuarioModifica = "";
                    vEstudioSectorCostoService.ModificarTiempoEntreActividadesEstudioSyC(vTiemposESyC);

                    /////
                    //ESC
                    TiempoEntreActividadesEstudioSyC vTiemposESC = new TiempoEntreActividadesEstudioSyC();
                    vTiemposESC.FRealFCTPreliminarESC = vTiemposESyC.FEstimadaFCTPreliminarESC;
                    //vTiemposESC.NoAplicaFRealFCTPreliminarESC = lblFRealFCTPreliminarESC.Visible;
                    vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vTiemposESyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
                    //vTiemposESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Visible;
                    vTiemposESC.FEstimadaES_EC_ESC = vTiemposESyC.FEstimadaES_EC_ESC;
                    //vTiemposESC.NoAplicaFEstimadaES_EC_ESC = lblFEstimadaES_EC_ESC.Visible;
                    vTiemposESC.FEstimadaDocsRadicadosEnContratosESC = vTiemposESyC.FEstimadaDocsRadicadosEnContratosESC;
                    //vTiemposESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC = lblFEstimadaDocsRadicadosEnContratosESC.Visible;
                    vTiemposESC.FEstimadaComitecontratacionESC = vTiemposESyC.FEstimadaComitecontratacionESC;
                    //vTiemposESC.NoAplicaFEstimadaComitecontratacionESC = lblFEstimadaComitecontratacionESC.Visible;
                    vTiemposESC.FEstimadaInicioDelPS_ESC = vTiemposESyC.FEstimadaInicioDelPS_ESC;
                    //vTiemposESC.NoAplicaFEstimadaInicioDelPS_ESC = lblFEstimadaInicioDelPS_ESC.Visible;
                    vTiemposESC.FEstimadaPresentacionPropuestasESC = vTiemposESyC.FEstimadaPresentacionPropuestasESC.Equals("NO APLICA") ? string.Empty : vTiemposESyC.FEstimadaPresentacionPropuestasESC;
                    //vTiemposESC.NoAplicaFEstimadaPresentacionPropuestasESC = vTiemposESyC.NoAplicaFEstimadaPresentacionPropuestasESC;
                    vTiemposESC.FEstimadaDeInicioDeEjecucionESC = vTiemposESyC.FEstimadaDeInicioDeEjecucionESC;

                    vTiemposESC.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                    vTiemposESC.UsuarioModifica = "";
                    vEstudioSectorCostoService.ModificarTiempoEntreActividadesFechasESC(vTiemposESC);

                    //Reales
                    TiempoEntreActividadesEstudioSyC vTiemposReales = new TiempoEntreActividadesEstudioSyC();

                    vTiemposReales.FRealFCTPreliminar = vTiemposESyC.FRealFCTPreliminar;
                    vTiemposReales.FRealDocsRadicadosEnContratos = vTiemposESyC.FRealDocsRadicadosEnContratos == null ? string.Empty : vTiemposESyC.FRealDocsRadicadosEnContratos;
                    vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado = vTiemposESyC.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
                    vTiemposReales.FRealComiteContratacion = vTiemposESyC.FRealComiteContratacion == null ? string.Empty : vTiemposESyC.FRealComiteContratacion;
                    vTiemposReales.FRealES_EC = vTiemposESyC.FRealES_EC;
                    vTiemposReales.FRealInicioPS = vTiemposESyC.FRealInicioPS == null ? string.Empty : vTiemposESyC.FRealInicioPS;
                    vTiemposReales.FRealPresentacionPropuestas = vTiemposESyC.FRealPresentacionPropuestas == null ? string.Empty : vTiemposESyC.FRealPresentacionPropuestas;

                    vTiemposReales.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                    vTiemposReales.UsuarioModifica = "";
                    vEstudioSectorCostoService.ModificarTiempoEntreActividadesFechasReales(vTiemposReales);

                    //CU17
                    //obtengo numero de proceso
                    List<InformacionProcesoSeleccion> ProcesoSeleccion = null;
                    ProcesoSeleccion = vSeguimientos.ConsultarProcesoSeleccion_By_IdConsecutivoEstudio(Convert.ToInt32(item.IdRegistroSolicitudEstudioSectoryCaso), null, null, null, null, null, null, null);

                    if (ProcesoSeleccion.Count > 0)
                    {
                        //consulto contrato
                        List<InformacionProcesoSeleccion> ListaContratos = vEstudioSectorCostoService1.ConsultarContratoESC(item.IdRegistroSolicitudEstudioSectoryCaso, ProcesoSeleccion[0].NumeroDelProceso, null, null);

                        InformacionProcesoSeleccion vInformacionProcesoSeleccion = new InformacionProcesoSeleccion();

                        vInformacionProcesoSeleccion.IdConsecutivoEstudio = item.IdRegistroSolicitudEstudioSectoryCaso;
                        vInformacionProcesoSeleccion.ObjetoContrato = ListaContratos[0].ObjetoContrato.ToUpper();

                        vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso = ListaContratos[0].FechaAdjudicacionDelProceso;
                        vInformacionProcesoSeleccion.Contratista = ListaContratos[0].Contratista.ToUpper();

                        vInformacionProcesoSeleccion.PlazoDeEjecucion = ListaContratos[0].PlazoDeEjecucion;

                        vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion = Convert.ToInt32(ListaContratos[0].IdInformacionProcesoSeleccion);
                        vInformacionProcesoSeleccion.UsuarioModifica = "";
                        vEstudioSectorCostoService1.ModificarInformacionProcesoSeleccion(vInformacionProcesoSeleccion);
                    }
                }
            }
            

        }

    }
}
