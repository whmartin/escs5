﻿using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class ActualizacionMasivaDAL : GeneralDAL
    {
        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudes_Consultar"))
                {                   
                  

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroSolicitudEstudioSectoryCaso> vListaRegistroSolicitudEstudioSectoryCaso = new List<RegistroSolicitudEstudioSectoryCaso>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
                            vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso;

                            vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO = vDataReaderResults["VigenciaPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO;
                            
                            //vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudInicial"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial;
                           
                            vListaRegistroSolicitudEstudioSectoryCaso.Add(vRegistroSolicitudEstudioSectoryCaso);
                        }
                        return vListaRegistroSolicitudEstudioSectoryCaso;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LogActualizacionMasiva> ConsultarRegistroActualizaciones(String Usuario, string FI, string FF)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Consultar_LogActualizacionMasiva"))
                {

                   
                   vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, Usuario);

                    vDataBase.AddInParameter(vDbCommand, "@FechaInicial", DbType.String, FI);

                    vDataBase.AddInParameter(vDbCommand, "@FechaFinal", DbType.String, FF);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<LogActualizacionMasiva> vListaRegistroS = new List<LogActualizacionMasiva>();
                        while (vDataReaderResults.Read())
                        {
                            LogActualizacionMasiva vRegistro = new LogActualizacionMasiva();
                            vRegistro.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Fecha"].ToString()) : vRegistro.Fecha;

                            vRegistro.Hora = vDataReaderResults["Hora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Hora"].ToString()) : vRegistro.Hora;

                            vRegistro.UsuarioLog = vDataReaderResults["Usuario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Usuario"].ToString()) : vRegistro.UsuarioLog;

                            //vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudInicial"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial;

                            vListaRegistroS.Add(vRegistro);
                        }
                        return vListaRegistroS;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarRegistroSolicitudEstudioSectoryCasos(string Userp,int vigenciapacco,decimal consecutivo, string area, string codigoarea, string estado, string FechaInicioProceso, string Id_modalidad, decimal? Id_tipo_contrato, string modalidad, string objeto_contractual, string tipo_contrato, string FechaInicioEjecucion, decimal valor_contrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudes_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@consecutivo", DbType.Decimal, consecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@area", DbType.String, area);
                    vDataBase.AddInParameter(vDbCommand, "@vigenciapacco", DbType.Int32, vigenciapacco);
                    vDataBase.AddInParameter(vDbCommand, "@codigoarea", DbType.String, codigoarea);
                    vDataBase.AddInParameter(vDbCommand, "@estado", DbType.String, estado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioProceso", DbType.String, FechaInicioProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Id_modalidad", DbType.String, Id_modalidad);
                    vDataBase.AddInParameter(vDbCommand, "@Id_tipo_contrato", DbType.String, Id_tipo_contrato);
                    vDataBase.AddInParameter(vDbCommand, "@modalidad", DbType.String, modalidad);
                    vDataBase.AddInParameter(vDbCommand, "@objeto_contractual", DbType.String, objeto_contractual);
                    vDataBase.AddInParameter(vDbCommand, "@tipo_contrato", DbType.String, tipo_contrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.String, FechaInicioEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@valor_contrato", DbType.Decimal, valor_contrato);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    //CreaLogActualizacion(Userp);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int CreaLogActualizacion(string Userp)
        {
            try
            {
                string hora = DateTime.Now.ToString("HH:mm:ss");
                hora = hora.Substring(0, 5); // for 24hr format

                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_LogActualizacionMasiva_Registro"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@Usuario", DbType.String, Userp);
                    vDataBase.AddInParameter(vDbCommand, "@Fecha", DbType.DateTime, DateTime.Now.ToShortDateString());
                    vDataBase.AddInParameter(vDbCommand, "@Hora", DbType.String, hora);
                   
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);                   
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
