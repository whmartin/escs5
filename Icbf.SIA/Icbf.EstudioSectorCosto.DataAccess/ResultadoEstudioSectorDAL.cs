using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class ResultadoEstudioSectorDAL : GeneralDAL
    {
        public ResultadoEstudioSectorDAL()
        {
        }

        /// <summary>
        /// M�todo para insertar resultado de estudio
        /// </summary>
        /// <param name="pResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna el id del resultado insertado</returns>
        public long InsertarResultadoEstudioSector(ResultadoEstudioSector pResultadoEstudioSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudio_Insertar"))
                {
                    long vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdResultadoEstudio", DbType.Int64, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudio", DbType.Int64, pResultadoEstudioSector.IdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentosRevisadosNAS", DbType.String, pResultadoEstudioSector.DocumentosRevisadosNAS);
                    vDataBase.AddInParameter(vDbCommand, "@Fecha", DbType.DateTime, pResultadoEstudioSector.Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@IdRevisadoPor", DbType.Int32, pResultadoEstudioSector.IdRevisadoPor);
                    vDataBase.AddInParameter(vDbCommand, "@Observacion", DbType.String, pResultadoEstudioSector.Observacion);
                    vDataBase.AddInParameter(vDbCommand, "@ProveedoresConsultados", DbType.Int32, pResultadoEstudioSector.Consultados);
                    vDataBase.AddInParameter(vDbCommand, "@ProveedoresParticipantes", DbType.Int32, pResultadoEstudioSector.Participantes);
                    vDataBase.AddInParameter(vDbCommand, "@CotizacionesRecibidas", DbType.Int32, pResultadoEstudioSector.CotizacionesRecibidas);
                    vDataBase.AddInParameter(vDbCommand, "@CotizacionesParaPresupuesto", DbType.Int32, pResultadoEstudioSector.CotizacionesParaPresupuesto);
                    vDataBase.AddInParameter(vDbCommand, "@RadicadoOficioEntregaESyC", DbType.String, pResultadoEstudioSector.RadicadoOficioEntregaESyC);
                    vDataBase.AddInParameter(vDbCommand, "@FechaDeEntregaESyC", DbType.DateTime, pResultadoEstudioSector.FechaDeEntregaESyC);
                    vDataBase.AddInParameter(vDbCommand, "@TipoDeValor", DbType.String, pResultadoEstudioSector.TipoDeValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestoESyC", DbType.Decimal, pResultadoEstudioSector.ValorPresupuestoESyC);
                    vDataBase.AddInParameter(vDbCommand, "@IndiceLiquidez", DbType.String, pResultadoEstudioSector.IndiceLiquidez);
                    vDataBase.AddInParameter(vDbCommand, "@IndiceLiquidezDesc", DbType.String, pResultadoEstudioSector.IndiceLiquidezDesc);
                    vDataBase.AddInParameter(vDbCommand, "@NivelDeEndeudamiento", DbType.String, pResultadoEstudioSector.NivelDeEndeudamiento);
                    vDataBase.AddInParameter(vDbCommand, "@NivelDeEndeudamientoDesc", DbType.String, pResultadoEstudioSector.NivelDeEndeudamientoDesc);
                    vDataBase.AddInParameter(vDbCommand, "@RazonDeCoberturaDeIntereses ", DbType.String, pResultadoEstudioSector.RazonDeCoberturaDeIntereses);
                    vDataBase.AddInParameter(vDbCommand, "@RazonDeCoberturaDeInteresesDesc ", DbType.String, pResultadoEstudioSector.RazonDeCoberturaDeInteresesDesc);
                    vDataBase.AddInParameter(vDbCommand, "@CapitalDeTrabajo", DbType.String, pResultadoEstudioSector.CapitalDeTrabajo);
                    vDataBase.AddInParameter(vDbCommand, "@CapitalDeTrabajoDesc", DbType.String, pResultadoEstudioSector.CapitalDeTrabajoDesc);
                    vDataBase.AddInParameter(vDbCommand, "@RentabilidadDelPatrimonio", DbType.String, pResultadoEstudioSector.RentabilidadDelPatrimonio);
                    vDataBase.AddInParameter(vDbCommand, "@RentabilidadDelPatrimonioDesc", DbType.String, pResultadoEstudioSector.RentabilidadDelPatrimonioDesc);
                    vDataBase.AddInParameter(vDbCommand, "@RentabilidadDelActivo", DbType.String, pResultadoEstudioSector.RentabilidadDelActivo);
                    vDataBase.AddInParameter(vDbCommand, "@RentabilidadDelActivoDesc", DbType.String, pResultadoEstudioSector.RentabilidadDelActivoDesc);
                    vDataBase.AddInParameter(vDbCommand, "@TotalPatrimonio", DbType.Decimal, pResultadoEstudioSector.TotalPatrimonio);
                    vDataBase.AddInParameter(vDbCommand, "@TotalPatrimonioDesc", DbType.String, pResultadoEstudioSector.TotalPatrimonioDesc);
                    vDataBase.AddInParameter(vDbCommand, "@KResidual", DbType.String, pResultadoEstudioSector.KResidual);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, DateTime.Now);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pResultadoEstudioSector.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, DateTime.Now);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pResultadoEstudioSector.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pResultadoEstudioSector.IdConsecutivoResultadoEstudio = Convert.ToInt64(vDataBase.GetParameterValue(vDbCommand, "@IdResultadoEstudio").ToString());
                    GenerarLogAuditoria(pResultadoEstudioSector, vDbCommand);
                    return pResultadoEstudioSector.IdConsecutivoResultadoEstudio;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesEstudioSectorCostos">Objeto GestionesEstudioSectorCostos</param>
        /// <returns>Id del registro de gesti�n insertado</returns>
        public decimal InsertarGestionesEstudioSectorCostos(GestionesEstudioSectorCostos pGestionesEstudioSectorCostos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesEstudioSectorCostos_Insertar"))
                {
                    decimal vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdGestionEstudio", DbType.Decimal, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pGestionesEstudioSectorCostos.IdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaPreguntaOComentario", DbType.DateTime, pGestionesEstudioSectorCostos.FechaPreguntaOComentario);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuesta", DbType.DateTime, pGestionesEstudioSectorCostos.FechaRespuesta);
                    vDataBase.AddInParameter(vDbCommand, "@UltimaFechaPreguntaOComentario", DbType.DateTime, pGestionesEstudioSectorCostos.UltimaFechaPreguntaOComentario);
                    vDataBase.AddInParameter(vDbCommand, "@UltimaFechaRespuesta", DbType.DateTime, pGestionesEstudioSectorCostos.UltimaFechaRespuesta);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEntregaES_MC", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEntregaES_MC);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEntregaES_MCDef", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEntregaES_MCDef);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFechaEntregaES_MC", DbType.Boolean, pGestionesEstudioSectorCostos.NoAplicaFechaEntregaES_MC);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEntregaFCTORequerimiento", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEntregaFCTORequerimiento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEntregaFCTORequerimientoDef", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEntregaFCTORequerimientoDef);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFechaentregaFCTOrequerimiento", DbType.Boolean, pGestionesEstudioSectorCostos.NoAplicaFechaentregaFCTOrequerimiento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEnvioSDC", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEnvioSDC);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEnvioSDCDef", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEnvioSDCDef);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFechaEnvioSDC", DbType.Boolean, pGestionesEstudioSectorCostos.NoAplicaFechaEnvioSDC);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoEntregaCotizaciones", DbType.DateTime, pGestionesEstudioSectorCostos.PlazoEntregaCotizaciones);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoEntregaCotizacionesDef", DbType.DateTime, pGestionesEstudioSectorCostos.PlazoEntregaCotizacionesDef);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaPlazoEntregaCotizaciones", DbType.Boolean, pGestionesEstudioSectorCostos.NoAplicaPlazoEntregaCotizaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pGestionesEstudioSectorCostos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pGestionesEstudioSectorCostos.IdGestionEstudio = Convert.ToDecimal(vDataBase.GetParameterValue(vDbCommand, "@IdGestionEstudio").ToString());
                    GenerarLogAuditoria(pGestionesEstudioSectorCostos, vDbCommand);
                    return pGestionesEstudioSectorCostos.IdGestionEstudio;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// M�todo para modificar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesEstudioSectorCostos">Id del registro gestion estudio</param>
        /// <returns>Retorna n�mero de filas afectadas</returns>
        public decimal ModificarGestionesEstudioSectorCostos(GestionesEstudioSectorCostos pGestionesEstudioSectorCostos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesEstudioSectorCostos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudioSectoryCaso", DbType.Int64, pGestionesEstudioSectorCostos.IdRegistroSolicitudEstudioSectoryCaso);
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionEstudio", DbType.Decimal, pGestionesEstudioSectorCostos.IdGestionEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaPreguntaOComentario", DbType.DateTime, pGestionesEstudioSectorCostos.FechaPreguntaOComentario);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuesta", DbType.DateTime, pGestionesEstudioSectorCostos.FechaRespuesta);
                    vDataBase.AddInParameter(vDbCommand, "@UltimaFechaPreguntaOComentario", DbType.DateTime, pGestionesEstudioSectorCostos.UltimaFechaPreguntaOComentario);
                    vDataBase.AddInParameter(vDbCommand, "@UltimaFechaRespuesta", DbType.DateTime, pGestionesEstudioSectorCostos.UltimaFechaRespuesta);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEntregaES_MC", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEntregaES_MC);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEntregaES_MCDef", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEntregaES_MCDef);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFechaEntregaES_MC", DbType.Boolean, pGestionesEstudioSectorCostos.NoAplicaFechaEntregaES_MC);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEntregaFCTORequerimiento", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEntregaFCTORequerimiento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEntregaFCTORequerimientoDef", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEntregaFCTORequerimientoDef);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFechaentregaFCTOrequerimiento", DbType.Boolean, pGestionesEstudioSectorCostos.NoAplicaFechaentregaFCTOrequerimiento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEnvioSDC", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEnvioSDC);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEnvioSDCDef", DbType.DateTime, pGestionesEstudioSectorCostos.FechaEnvioSDCDef);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFechaEnvioSDC", DbType.Boolean, pGestionesEstudioSectorCostos.NoAplicaFechaEnvioSDC);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoEntregaCotizaciones", DbType.DateTime, pGestionesEstudioSectorCostos.PlazoEntregaCotizaciones);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoEntregaCotizacionesDef", DbType.DateTime, pGestionesEstudioSectorCostos.PlazoEntregaCotizacionesDef);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaPlazoEntregaCotizaciones", DbType.Boolean, pGestionesEstudioSectorCostos.NoAplicaPlazoEntregaCotizaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pGestionesEstudioSectorCostos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGestionesEstudioSectorCostos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar gestiones de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado del estudio</param>
        /// <param name="pObjeto">Objeto del estudio</param>
        /// <param name="pIdResponsableES">Responsable del estudio ES</param>
        /// <param name="pIdResponsableEC">Responsable del estudio EC</param>
        /// <param name="pIdModalidadDeSeleccion">Id modalidad selecci�n</param>
        /// <param name="pDireccionSolicitante">Direcci�n del solicitante</param>
        /// <param name="pEstado">Estado del estudiio</param>
        /// <returns>Retorna una lista tipo GestionesEstudioSectorCostos</returns>
        public List<GestionesEstudioSectorCostos> ConsultarGestionEstudioSectors(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, int? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesEstudioSectorCostoss_Consultar"))
                {
                    if (pIdConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pIdConsecutivoEstudio);
                    if (!pNombreAbreviado.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (!pObjeto.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdModalidadDeSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadDeSeleccion);
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pDireccionSolicitante);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GestionesEstudioSectorCostos> vGestionesEstudioSectorCostos = new List<GestionesEstudioSectorCostos>();
                        while (vDataReaderResults.Read())
                        {
                            GestionesEstudioSectorCostos res = new GestionesEstudioSectorCostos();
                            res.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : res.IdRegistroSolicitudEstudioSectoryCaso;
                            res.IdGestionEstudio = vDataReaderResults["IdGestionEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdGestionEstudio"].ToString()) : res.IdGestionEstudio;
                            res.NombreAbreviado = vDataReaderResults["NombreAbreviado"] != DBNull.Value ? (vDataReaderResults["NombreAbreviado"].ToString()) : res.NombreAbreviado;
                            res.Actividad = vDataReaderResults["Actividad"] != DBNull.Value ? (vDataReaderResults["Actividad"].ToString()) : res.Actividad;
                            res.FechaPreguntaOComentario = vDataReaderResults["FechaPreguntaOComentario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPreguntaOComentario"].ToString()) : res.FechaPreguntaOComentario;
                            res.FechaRespuesta = vDataReaderResults["FechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuesta"].ToString()) : res.FechaRespuesta;
                            res.NoAplica = vDataReaderResults["NoAplica"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplica"].ToString()) : res.NoAplica;


                            vGestionesEstudioSectorCostos.Add(res);
                        }
                        return vGestionesEstudioSectorCostos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar resultados de estudio
        /// </summary>
        /// <param name="vResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna n�mero de filas afectadas</returns>
        public long ModificarResultadoEstudioSector(ResultadoEstudioSector pResultadoEstudioSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudio_Modificar"))
                {
                    long vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdResultadoEstudio", DbType.Int64, pResultadoEstudioSector.IdConsecutivoResultadoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudio", DbType.Int64, pResultadoEstudioSector.IdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentosRevisadosNAS", DbType.String, pResultadoEstudioSector.DocumentosRevisadosNAS);
                    vDataBase.AddInParameter(vDbCommand, "@Fecha", DbType.DateTime, pResultadoEstudioSector.Fecha);
                    vDataBase.AddInParameter(vDbCommand, "@IdRevisadoPor", DbType.Int32, pResultadoEstudioSector.IdRevisadoPor);
                    vDataBase.AddInParameter(vDbCommand, "@Observacion", DbType.String, pResultadoEstudioSector.Observacion);
                    vDataBase.AddInParameter(vDbCommand, "@ProveedoresConsultados", DbType.Int32, pResultadoEstudioSector.Consultados);
                    vDataBase.AddInParameter(vDbCommand, "@ProveedoresParticipantes", DbType.Int32, pResultadoEstudioSector.Participantes);
                    vDataBase.AddInParameter(vDbCommand, "@CotizacionesRecibidas", DbType.Int32, pResultadoEstudioSector.CotizacionesRecibidas);
                    vDataBase.AddInParameter(vDbCommand, "@CotizacionesParaPresupuesto", DbType.Int32, pResultadoEstudioSector.CotizacionesParaPresupuesto);
                    vDataBase.AddInParameter(vDbCommand, "@RadicadoOficioEntregaESyC", DbType.String, pResultadoEstudioSector.RadicadoOficioEntregaESyC);
                    vDataBase.AddInParameter(vDbCommand, "@FechaDeEntregaESyC", DbType.DateTime, pResultadoEstudioSector.FechaDeEntregaESyC);
                    vDataBase.AddInParameter(vDbCommand, "@TipoDeValor", DbType.String, pResultadoEstudioSector.TipoDeValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestoESyC", DbType.Decimal, pResultadoEstudioSector.ValorPresupuestoESyC);
                    vDataBase.AddInParameter(vDbCommand, "@IndiceLiquidez", DbType.String, pResultadoEstudioSector.IndiceLiquidez);
                    vDataBase.AddInParameter(vDbCommand, "@IndiceLiquidezDesc", DbType.String, pResultadoEstudioSector.IndiceLiquidezDesc);
                    vDataBase.AddInParameter(vDbCommand, "@NivelDeEndeudamiento", DbType.String, pResultadoEstudioSector.NivelDeEndeudamiento);
                    vDataBase.AddInParameter(vDbCommand, "@NivelDeEndeudamientoDesc", DbType.String, pResultadoEstudioSector.NivelDeEndeudamientoDesc);
                    vDataBase.AddInParameter(vDbCommand, "@RazonDeCoberturaDeIntereses ", DbType.String, pResultadoEstudioSector.RazonDeCoberturaDeIntereses);
                    vDataBase.AddInParameter(vDbCommand, "@RazonDeCoberturaDeInteresesDesc ", DbType.String, pResultadoEstudioSector.RazonDeCoberturaDeInteresesDesc);
                    vDataBase.AddInParameter(vDbCommand, "@CapitalDeTrabajo", DbType.String, pResultadoEstudioSector.CapitalDeTrabajo);
                    vDataBase.AddInParameter(vDbCommand, "@CapitalDeTrabajoDesc", DbType.String, pResultadoEstudioSector.CapitalDeTrabajoDesc);
                    vDataBase.AddInParameter(vDbCommand, "@RentabilidadDelPatrimonio", DbType.String, pResultadoEstudioSector.RentabilidadDelPatrimonio);
                    vDataBase.AddInParameter(vDbCommand, "@RentabilidadDelPatrimonioDesc", DbType.String, pResultadoEstudioSector.RentabilidadDelPatrimonioDesc);
                    vDataBase.AddInParameter(vDbCommand, "@RentabilidadDelActivo", DbType.String, pResultadoEstudioSector.RentabilidadDelActivo);
                    vDataBase.AddInParameter(vDbCommand, "@RentabilidadDelActivoDesc", DbType.String, pResultadoEstudioSector.RentabilidadDelActivoDesc);
                    vDataBase.AddInParameter(vDbCommand, "@TotalPatrimonio", DbType.Decimal, pResultadoEstudioSector.TotalPatrimonio);
                    vDataBase.AddInParameter(vDbCommand, "@TotalPatrimonioDesc", DbType.String, pResultadoEstudioSector.TotalPatrimonioDesc);
                    vDataBase.AddInParameter(vDbCommand, "@KResidual", DbType.String, pResultadoEstudioSector.KResidual);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, DateTime.Now);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pResultadoEstudioSector.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, DateTime.Now);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pResultadoEstudioSector.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pResultadoEstudioSector, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para eliminar resultado estudio
        /// </summary>
        /// <param name="pResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna el n�mero de filas insertadas</returns>
        public int EliminarResultadoEstudioSector(ResultadoEstudioSector pResultadoEstudioSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudio_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdResultadoEstudio", DbType.Int64, pResultadoEstudioSector.IdConsecutivoResultadoEstudio);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pResultadoEstudioSector, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para eliminar gestiones de estudio
        /// </summary>
        /// <param name="pGestionEstudioSector">Objeto GestionesEstudioSectorCostos</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int EliminarGestionEstudioSector(GestionesEstudioSectorCostos pGestionEstudioSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesEstudioSectorCostos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionEstudio", DbType.Decimal, pGestionEstudioSector.IdGestionEstudio);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGestionEstudioSector, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar indicadores resultado estudio
        /// </summary>
        /// <param name="pIndicador">Objeto IndicadorAdicionalResultado</param>
        /// <returns>Retorna el id insertado</returns>
        public long InsertarIndicadoresResultadoEstudioSector(IndicadorAdicionalResultado pIndicadorAdicionalResultado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudioIndicadores_Insertar"))
                {
                    long vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdIndicadoresAdicionalesResultado", DbType.Int64, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudio", DbType.Int64, pIndicadorAdicionalResultado.IdResultadoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@Indicador", DbType.String, pIndicadorAdicionalResultado.Indicador);
                    vDataBase.AddInParameter(vDbCommand, "@Condicion", DbType.String, pIndicadorAdicionalResultado.Condicion);
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pIndicadorAdicionalResultado.Valor);
                    vDataBase.AddInParameter(vDbCommand, "@IndicadorAdicionalDesc", DbType.String, pIndicadorAdicionalResultado.IndicadorAdicionalDesc);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, DateTime.Now);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pIndicadorAdicionalResultado.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, DateTime.Now);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pIndicadorAdicionalResultado.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pIndicadorAdicionalResultado.IdIndicadoresAdicionalesResultado = Convert.ToInt64(vDataBase.GetParameterValue(vDbCommand, "@IdIndicadoresAdicionalesResultado").ToString());
                    GenerarLogAuditoria(pIndicadorAdicionalResultado, vDbCommand);
                    return pIndicadorAdicionalResultado.IdIndicadoresAdicionalesResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesActividadesAdicionales">Objeto GestionesActividadesAdicionales</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int InsertarActividadesEstudioSector(GestionesActividadesAdicionales pGestionesActividadesAdicionales)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesActividadesEstudioSectorCostos_Insertar"))
                {
                    int vResultado;

                    vDataBase.AddInParameter(vDbCommand, "@IdGestionEstudio", DbType.Decimal, pGestionesActividadesAdicionales.IdGestionEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int64, pGestionesActividadesAdicionales.IdActividad);
                    vDataBase.AddInParameter(vDbCommand, "@FechaActividadAdicional", DbType.DateTime, pGestionesActividadesAdicionales.FechaActividadAdicional);
                    vDataBase.AddInParameter(vDbCommand, "@Detalle", DbType.String, pGestionesActividadesAdicionales.Detalle);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaActividadAdicional", DbType.Boolean, pGestionesActividadesAdicionales.NoAplicaActividadAdicional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pGestionesActividadesAdicionales.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGestionesActividadesAdicionales, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo modificar actividades gesti�n estudio
        /// </summary>
        /// <param name="pGestionesActividadesAdicionales">Objeto GestionesActividadesAdicionales</param>
        /// <returns>retorna el n�mero de filas afectadas</returns>
        public int ModificarActividadesEstudioSector(GestionesActividadesAdicionales pGestionesActividadesAdicionales)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesActividadesEstudioSectorCostos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionActividadAdicional", DbType.Decimal, pGestionesActividadesAdicionales.IdGestionActividadAdicional);
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionEstudio", DbType.Decimal, pGestionesActividadesAdicionales.IdGestionEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int64, pGestionesActividadesAdicionales.IdActividad);
                    vDataBase.AddInParameter(vDbCommand, "@FechaActividadAdicional", DbType.DateTime, pGestionesActividadesAdicionales.FechaActividadAdicional);
                    vDataBase.AddInParameter(vDbCommand, "@Detalle", DbType.String, pGestionesActividadesAdicionales.Detalle);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaActividadAdicional", DbType.Boolean, pGestionesActividadesAdicionales.NoAplicaActividadAdicional);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pGestionesActividadesAdicionales.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pGestionesActividadesAdicionales, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar indicadores adcionales de resultado estudio
        /// </summary>
        /// <param name="pIndicador">Objeto IndicadorAdicionalResultado</param>
        /// <returns>retorna el n�mero de filas afectadas</returns>
        public long ModificarIndicadoresResultadoEstudioSector(IndicadorAdicionalResultado pIndicadorAdicionalResultado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudioIndicadores_Modificar"))
                {
                    long vResultado;
                    vDataBase.AddInParameter(vDbCommand, "IdIndicadoresAdicionalesResultado", DbType.Int64, pIndicadorAdicionalResultado.IdIndicadoresAdicionalesResultado);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudio", DbType.Int64, pIndicadorAdicionalResultado.IdResultadoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@Indicador", DbType.String, pIndicadorAdicionalResultado.Indicador);
                    vDataBase.AddInParameter(vDbCommand, "@Condicion", DbType.String, pIndicadorAdicionalResultado.Condicion);
                    vDataBase.AddInParameter(vDbCommand, "@Valor", DbType.String, pIndicadorAdicionalResultado.Valor);
                    vDataBase.AddInParameter(vDbCommand, "@IndicadorAdicionalDesc", DbType.String, pIndicadorAdicionalResultado.IndicadorAdicionalDesc);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, DateTime.Now);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pIndicadorAdicionalResultado.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, DateTime.Now);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pIndicadorAdicionalResultado.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pIndicadorAdicionalResultado.IdIndicadoresAdicionalesResultado = Convert.ToInt64(vDataBase.GetParameterValue(vDbCommand, "@IdIndicadoresAdicionalesResultado").ToString());
                    GenerarLogAuditoria(pIndicadorAdicionalResultado, vDbCommand);
                    return pIndicadorAdicionalResultado.IdIndicadoresAdicionalesResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarIndicadorResultadoEstudioSector(IndicadorAdicionalResultado pIndicadorAdicionalResultado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudioIndicadores_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdIndicadoresAdicionalesResultado", DbType.Int64, pIndicadorAdicionalResultado.IdIndicadoresAdicionalesResultado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pIndicadorAdicionalResultado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar resultado estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id registro resultado estudio</param>
        /// <returns>Objeto ResultadoEstudioSector</returns>
        public ResultadoEstudioSector ConsultarResultadoEstudioSector(long pIdConsecutivoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idResultadoEstudio", DbType.Int64, pIdConsecutivoEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ResultadoEstudioSector vResultadoEstudioSector = new ResultadoEstudioSector();
                        while (vDataReaderResults.Read())
                        {
                            vResultadoEstudioSector.IdConsecutivoResultadoEstudio = vDataReaderResults["IdResultadoEstudio"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdResultadoEstudio"].ToString()) : vResultadoEstudioSector.IdConsecutivoResultadoEstudio;
                            vResultadoEstudioSector.IdConsecutivoEstudio = vDataReaderResults["IdRegistroSolicitudEstudio"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdRegistroSolicitudEstudio"].ToString()) : vResultadoEstudioSector.IdConsecutivoEstudio;
                            vResultadoEstudioSector.DocumentosRevisadosNAS = vDataReaderResults["DocumentosRevisadosNAS"] != DBNull.Value ? (vDataReaderResults["DocumentosRevisadosNAS"].ToString()) : vResultadoEstudioSector.DocumentosRevisadosNAS;
                            vResultadoEstudioSector.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : vResultadoEstudioSector.Fecha;
                            vResultadoEstudioSector.IdRevisadoPor = vDataReaderResults["IdRevisadoPor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRevisadoPor"].ToString()) : vResultadoEstudioSector.IdRevisadoPor;
                            vResultadoEstudioSector.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? (vDataReaderResults["Observacion"].ToString()) : vResultadoEstudioSector.Observacion;
                            vResultadoEstudioSector.Consultados = vDataReaderResults["ProveedoresConsultados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ProveedoresConsultados"].ToString()) : vResultadoEstudioSector.Consultados;
                            vResultadoEstudioSector.Participantes = vDataReaderResults["ProveedoresParticipantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ProveedoresParticipantes"].ToString()) : vResultadoEstudioSector.Participantes;
                            vResultadoEstudioSector.CotizacionesRecibidas = vDataReaderResults["CotizacionesRecibidas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CotizacionesRecibidas"].ToString()) : vResultadoEstudioSector.CotizacionesRecibidas;
                            vResultadoEstudioSector.CotizacionesParaPresupuesto = vDataReaderResults["CotizacionesParaPresupuesto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CotizacionesParaPresupuesto"].ToString()) : vResultadoEstudioSector.CotizacionesParaPresupuesto;
                            vResultadoEstudioSector.RadicadoOficioEntregaESyC = vDataReaderResults["RadicadoOficioEntregaESyC"] != DBNull.Value ? (vDataReaderResults["RadicadoOficioEntregaESyC"].ToString()) : vResultadoEstudioSector.RadicadoOficioEntregaESyC;
                            vResultadoEstudioSector.FechaDeEntregaESyC = vDataReaderResults["FechaDeEntregaESyC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDeEntregaESyC"].ToString()) : vResultadoEstudioSector.FechaDeEntregaESyC;
                            vResultadoEstudioSector.TipoDeValor = vDataReaderResults["TipoDeValor"] != DBNull.Value ? (vDataReaderResults["TipoDeValor"].ToString()) : vResultadoEstudioSector.TipoDeValor;
                            vResultadoEstudioSector.ValorPresupuestoESyC = vDataReaderResults["ValorPresupuestoESyC"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestoESyC"].ToString()) : vResultadoEstudioSector.ValorPresupuestoESyC;
                            vResultadoEstudioSector.IndiceLiquidez = vDataReaderResults["IndiceDeLiquidez"] != DBNull.Value ? (vDataReaderResults["IndiceDeLiquidez"].ToString()) : vResultadoEstudioSector.IndiceLiquidez;
                            vResultadoEstudioSector.NivelDeEndeudamiento = vDataReaderResults["NivelDeEndeudamiento"] != DBNull.Value ? (vDataReaderResults["NivelDeEndeudamiento"].ToString()) : vResultadoEstudioSector.NivelDeEndeudamiento;
                            vResultadoEstudioSector.RazonDeCoberturaDeIntereses = vDataReaderResults["RazonDeCoberturaDeIntereses"] != DBNull.Value ? (vDataReaderResults["RazonDeCoberturaDeIntereses"].ToString()) : vResultadoEstudioSector.RazonDeCoberturaDeIntereses;
                            vResultadoEstudioSector.CapitalDeTrabajo = vDataReaderResults["CapitalDeTrabajo"] != DBNull.Value ? (vDataReaderResults["CapitalDeTrabajo"].ToString()) : vResultadoEstudioSector.CapitalDeTrabajo;
                            vResultadoEstudioSector.RentabilidadDelPatrimonio = vDataReaderResults["RentabilidadDelPatrimonio"] != DBNull.Value ? (vDataReaderResults["RentabilidadDelPatrimonio"].ToString()) : vResultadoEstudioSector.RentabilidadDelPatrimonio;
                            vResultadoEstudioSector.RentabilidadDelActivo = vDataReaderResults["RentabilidadDelActivo"] != DBNull.Value ? (vDataReaderResults["RentabilidadDelActivo"].ToString()) : vResultadoEstudioSector.RentabilidadDelActivo;
                            vResultadoEstudioSector.TotalPatrimonio = vDataReaderResults["TotalPatrimonio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalPatrimonio"].ToString()) : vResultadoEstudioSector.TotalPatrimonio;
                            vResultadoEstudioSector.KResidual = vDataReaderResults["KResidual"] != DBNull.Value ? (vDataReaderResults["KResidual"].ToString()) : vResultadoEstudioSector.KResidual;
                            vResultadoEstudioSector.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vResultadoEstudioSector.UsuarioCrea;
                            vResultadoEstudioSector.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vResultadoEstudioSector.FechaCrea;
                            vResultadoEstudioSector.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vResultadoEstudioSector.UsuarioModifica;
                            vResultadoEstudioSector.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vResultadoEstudioSector.FechaModifica;
                        }
                        return vResultadoEstudioSector;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consutar fechas definitivas de gestiones
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id del cosecutivo de estudio</param>
        /// <returns>Retorna objeto GestionesEstudioSectorCostos</returns>
        public GestionesEstudioSectorCostos ConsultarGestionDefinitivas(long pIdConsecutovoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesEstudioSectorCostos_ConsultarDefinitivos"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pIdConsecutovoEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        GestionesEstudioSectorCostos vGestionesEstudio = new GestionesEstudioSectorCostos();
                        while (vDataReaderResults.Read())
                        {
                            vGestionesEstudio.FechaPreguntaOComentarioDef = vDataReaderResults["FechaPreguntaOComentarioDef"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPreguntaOComentarioDef"].ToString()) : vGestionesEstudio.FechaPreguntaOComentarioDef;
                            vGestionesEstudio.FechaRespuestaDef = vDataReaderResults["FechaRespuestaDef"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuestaDef"].ToString()) : vGestionesEstudio.FechaRespuestaDef;
                            vGestionesEstudio.FechaEntregaES_MCDef = vDataReaderResults["FechaEntregaES_MCDef"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaES_MCDef"].ToString()) : vGestionesEstudio.FechaEntregaES_MCDef;
                            vGestionesEstudio.FechaEntregaFCTORequerimientoDef = vDataReaderResults["FechaEntregaFCTORequerimientoDef"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaFCTORequerimientoDef"].ToString()) : vGestionesEstudio.FechaEntregaFCTORequerimientoDef;
                            vGestionesEstudio.FechaEnvioSDCDef = vDataReaderResults["FechaEnvioSDCDef"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioSDCDef"].ToString()) : vGestionesEstudio.FechaEnvioSDCDef;
                            vGestionesEstudio.PlazoEntregaCotizacionesDef = vDataReaderResults["PlazoEntregaCotizacionesDef"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["PlazoEntregaCotizacionesDef"].ToString()) : vGestionesEstudio.PlazoEntregaCotizacionesDef;
                            vGestionesEstudio.NumeroReproceso = vDataReaderResults["NumeroReproceso"] != DBNull.Value ? (vDataReaderResults["NumeroReproceso"].ToString()) : vGestionesEstudio.NumeroReproceso;
                            vGestionesEstudio.NoAplicaFechaEnvioSDC = vDataReaderResults["NoAplicaFechaEnvioSDC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaEnvioSDC"].ToString()) : vGestionesEstudio.NoAplicaFechaEnvioSDC;
                        }
                        return vGestionesEstudio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar gesti�n de estudio
        /// </summary>
        /// <param name="pIdGestionEstudio">Id de la gesti�n de estudio</param>
        /// <returns>Retorna objeto GestionesEstudioSectorCostos</returns>
        public GestionesEstudioSectorCostos ConsultarGestionEstudioSector(decimal pIdGestionEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesEstudioSectorCostos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionEstudio", DbType.Int64, pIdGestionEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        GestionesEstudioSectorCostos vGestionesEstudio = new GestionesEstudioSectorCostos();
                        while (vDataReaderResults.Read())
                        {
                            vGestionesEstudio.IdGestionEstudio = vDataReaderResults["IdGestionEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdGestionEstudio"].ToString()) : vGestionesEstudio.IdGestionEstudio;
                            vGestionesEstudio.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vGestionesEstudio.IdRegistroSolicitudEstudioSectoryCaso;
                            vGestionesEstudio.FechaPreguntaOComentario = vDataReaderResults["FechaPreguntaOComentario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPreguntaOComentario"].ToString()) : vGestionesEstudio.FechaPreguntaOComentario;
                            vGestionesEstudio.FechaRespuesta = vDataReaderResults["FechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuesta"].ToString()) : vGestionesEstudio.FechaRespuesta;
                            vGestionesEstudio.UltimaFechaPreguntaOComentario = vDataReaderResults["UltimaFechaPreguntaOComentario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["UltimaFechaPreguntaOComentario"].ToString()) : vGestionesEstudio.UltimaFechaPreguntaOComentario;
                            vGestionesEstudio.UltimaFechaRespuesta = vDataReaderResults["UltimaFechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["UltimaFechaRespuesta"].ToString()) : vGestionesEstudio.UltimaFechaRespuesta;
                            vGestionesEstudio.FechaEntregaES_MC = vDataReaderResults["FechaEntregaES_MC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaES_MC"].ToString()) : vGestionesEstudio.FechaEntregaES_MC;
                            vGestionesEstudio.FechaEntregaES_MCDef = vDataReaderResults["FechaEntregaES_MCDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaES_MCDefinitiva"].ToString()) : vGestionesEstudio.FechaEntregaES_MCDef;
                            vGestionesEstudio.NoAplicaFechaEntregaES_MC = vDataReaderResults["NoAplicaFechaEntregaES_MC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaEntregaES_MC"].ToString()) : vGestionesEstudio.NoAplicaFechaEntregaES_MC;
                            vGestionesEstudio.FechaEntregaFCTORequerimiento = vDataReaderResults["FechaEntregaFCTORequerimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaFCTORequerimiento"].ToString()) : vGestionesEstudio.FechaEntregaFCTORequerimiento;
                            vGestionesEstudio.FechaEntregaFCTORequerimientoDef = vDataReaderResults["FechaEntregaFCTORequerimientoDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaFCTORequerimientoDefinitiva"].ToString()) : vGestionesEstudio.FechaEntregaFCTORequerimientoDef;
                            vGestionesEstudio.NoAplicaFechaentregaFCTOrequerimiento = vDataReaderResults["NoAplicaFechaentregaFCTOrequerimiento"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaentregaFCTOrequerimiento"].ToString()) : vGestionesEstudio.NoAplicaFechaentregaFCTOrequerimiento;
                            vGestionesEstudio.FechaEnvioSDC = vDataReaderResults["FechaEnvioSDC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioSDC"].ToString()) : vGestionesEstudio.FechaEnvioSDC;
                            vGestionesEstudio.FechaEnvioSDCDef = vDataReaderResults["FechaEnvioSDCDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioSDCDefinitiva"].ToString()) : vGestionesEstudio.FechaEnvioSDCDef;
                            vGestionesEstudio.NoAplicaFechaEnvioSDC = vDataReaderResults["NoAplicaFechaEnvioSDC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaEnvioSDC"].ToString()) : vGestionesEstudio.NoAplicaFechaEnvioSDC;
                            vGestionesEstudio.PlazoEntregaCotizaciones = vDataReaderResults["PlazoEntregaCotizaciones"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["PlazoEntregaCotizaciones"].ToString()) : vGestionesEstudio.PlazoEntregaCotizaciones;
                            vGestionesEstudio.PlazoEntregaCotizacionesDef = vDataReaderResults["PlazoEntregaCotizacionesDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["PlazoEntregaCotizacionesDefinitiva"].ToString()) : vGestionesEstudio.PlazoEntregaCotizacionesDef;
                            vGestionesEstudio.NoAplicaPlazoEntregaCotizaciones = vDataReaderResults["NoAplicaPlazoEntregaCotizaciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaPlazoEntregaCotizaciones"].ToString()) : vGestionesEstudio.NoAplicaPlazoEntregaCotizaciones;
                            vGestionesEstudio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? (vDataReaderResults["UsuarioCrea"].ToString()) : vGestionesEstudio.UsuarioCrea;
                            vGestionesEstudio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGestionesEstudio.FechaCrea;
                            vGestionesEstudio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? (vDataReaderResults["UsuarioModifica"].ToString()) : vGestionesEstudio.UsuarioModifica;
                            vGestionesEstudio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGestionesEstudio.FechaModifica;




                        }
                        return vGestionesEstudio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar indicadores adcionales de resultado estudio
        /// </summary>
        /// <param name="pIdResultadoEstudio">Id del registro resultado</param>
        /// <returns>Retorna lista de indicadores</returns>
        public List<IndicadorAdicionalResultado> ConsultarIndicadorResultado(long pIdResultadoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudioIndicadores_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idResultadoEstudio", DbType.Int64, pIdResultadoEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<IndicadorAdicionalResultado> vLstIndicador = new List<IndicadorAdicionalResultado>();
                        while (vDataReaderResults.Read())
                        {
                            IndicadorAdicionalResultado vIndicador = new IndicadorAdicionalResultado();
                            vIndicador.IdIndicadoresAdicionalesResultado = vDataReaderResults["IdIndicadoresAdicionalesResultado"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdIndicadoresAdicionalesResultado"].ToString()) : vIndicador.IdIndicadoresAdicionalesResultado;
                            vIndicador.IdResultadoEstudio = vDataReaderResults["IdResultadoEstudio"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdResultadoEstudio"].ToString()) : vIndicador.IdResultadoEstudio;
                            vIndicador.Indicador = vDataReaderResults["Indicador"] != DBNull.Value ? (vDataReaderResults["Indicador"].ToString()) : vIndicador.Indicador;
                            vIndicador.Valor = vDataReaderResults["Valor"] != DBNull.Value ? (vDataReaderResults["Valor"].ToString()) : vIndicador.Valor;
                            vIndicador.Condicion = vDataReaderResults["Condicion"] != DBNull.Value ? (vDataReaderResults["Condicion"].ToString()) : vIndicador.Condicion;
                            vIndicador.IndicadorAdicionalDesc = vDataReaderResults["IndicadorAdicionalDesc"] != DBNull.Value ? (vDataReaderResults["IndicadorAdicionalDesc"].ToString()) : vIndicador.IndicadorAdicionalDesc;
                            vIndicador.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? (vDataReaderResults["UsuarioCrea"].ToString()) : vIndicador.UsuarioCrea;
                            vIndicador.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vIndicador.FechaCrea;
                            vIndicador.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? (vDataReaderResults["UsuarioModifica"].ToString()) : vIndicador.UsuarioModifica;
                            vIndicador.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vIndicador.FechaModifica;
                            vLstIndicador.Add(vIndicador);
                        }
                        return vLstIndicador;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar actividades adicionales de gestiones
        /// </summary>
        /// <param name="pIdGestionEstudio">Id de la gestion de estudio</param>
        /// <returns>Retorna lista de GestionesActividadesAdicionales</returns>
        public List<GestionesActividadesAdicionales> ConsultarGestionesActividadesAdicionales(decimal pIdGestionEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_GestionesActividadesEstudioSectorCostos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionEstudio", DbType.Decimal, pIdGestionEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GestionesActividadesAdicionales> vLstGestionActividades = new List<GestionesActividadesAdicionales>();
                        while (vDataReaderResults.Read())
                        {
                            GestionesActividadesAdicionales vActividad = new GestionesActividadesAdicionales();
                            vActividad.IdGestionActividadAdicional = vDataReaderResults["IdGestionActividadAdicional"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdGestionActividadAdicional"].ToString()) : vActividad.IdGestionActividadAdicional;
                            vActividad.IdGestionEstudio = vDataReaderResults["IdGestionEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdGestionEstudio"].ToString()) : vActividad.IdGestionEstudio;
                            vActividad.IdActividad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividad.IdActividad;
                            vActividad.Actividad = vDataReaderResults["Nombre"] != DBNull.Value ? (vDataReaderResults["Nombre"].ToString()) : vActividad.Actividad;
                            vActividad.FechaActividadAdicional = vDataReaderResults["FechaActividadAdicional"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaActividadAdicional"].ToString()) : vActividad.FechaActividadAdicional;
                            vActividad.Detalle = vDataReaderResults["Detalle"] != DBNull.Value ? (vDataReaderResults["Detalle"].ToString()) : vActividad.Detalle;
                            vActividad.NoAplicaActividadAdicional = vDataReaderResults["NoAplicaActividadAdicional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaActividadAdicional"].ToString()) : vActividad.NoAplicaActividadAdicional;
                            vActividad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? (vDataReaderResults["UsuarioCrea"].ToString()) : vActividad.UsuarioCrea;
                            vActividad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividad.FechaCrea;
                            vActividad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? (vDataReaderResults["UsuarioModifica"].ToString()) : vActividad.UsuarioModifica;
                            vActividad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividad.FechaModifica;
                            vLstGestionActividades.Add(vActividad);
                        }
                        return vLstGestionActividades;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para listar ResultadoEstudioSectorConsulta
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado del estudio</param>
        /// <param name="pObjeto">Objeto del estudio</param>
        /// <param name="pIdResponsableES">Responsable del estudio ES</param>
        /// <param name="pIdResponsableEC">Responsable del estudio EC</param>
        /// <param name="pIdModalidadDeSeleccion">Id modalidad selecci�n</param>
        /// <param name="pDireccionSolicitante">Direcci�n del solicitante</param>
        /// <param name="pEstado">Estado del estudiio</param>
        /// <returns>Retorna una lista tipo ResultadoEstudioSectorConsulta</returns>
        public List<ResultadoEstudioSectorConsulta> ConsultarResultadoEstudioSectors(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResultadoEstudioSectors_Consultar"))
                {
                    if (pIdConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pIdConsecutivoEstudio);
                    if (!pNombreAbreviado.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (!pObjeto.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdModalidadDeSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadDeSeleccion);
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pDireccionSolicitante);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultadoEstudioSectorConsulta> vListaResultadoEstudioSector = new List<ResultadoEstudioSectorConsulta>();
                        while (vDataReaderResults.Read())
                        {
                            ResultadoEstudioSectorConsulta res = new ResultadoEstudioSectorConsulta();
                            res.IdConsecutivoEstudio = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : res.IdConsecutivoEstudio;
                            res.NombreAbreviado = vDataReaderResults["NombreAbreviado"] != DBNull.Value ? (vDataReaderResults["NombreAbreviado"].ToString()) : res.NombreAbreviado;
                            res.ProveedoresConsultados = vDataReaderResults["ProveedoresConsultados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ProveedoresConsultados"].ToString()) : res.ProveedoresConsultados;
                            res.ProveedoresParticipantes = vDataReaderResults["ProveedoresParticipantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ProveedoresParticipantes"].ToString()) : res.ProveedoresParticipantes;
                            res.CotizacionesRecibidas = vDataReaderResults["CotizacionesRecibidas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CotizacionesRecibidas"].ToString()) : res.CotizacionesRecibidas;
                            res.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? (vDataReaderResults["Observacion"].ToString()) : res.Observacion;
                            res.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : res.Fecha;
                            res.IdResultadoEstudio = vDataReaderResults["IdResultadoEstudio"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdResultadoEstudio"].ToString()) : res.IdResultadoEstudio;
                            res.FechaDeEntregaESyC = vDataReaderResults["FechaDeEntregaESyC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDeEntregaESyC"].ToString()) : res.FechaDeEntregaESyC;

                            vListaResultadoEstudioSector.Add(res);
                        }
                        return vListaResultadoEstudioSector;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region TIEMPO ENTRE ACTIVIDADES

        /// <summary>
        /// M�todo para validar par�metros para calcular tiempos
        /// </summary>
        /// <param name="IdConsecutivoEstudio">Id del cosecutivo de estudio</param>
        /// <returns>Retorna "OK" cuando se cumplen las condiciones</returns>
        public string ValidarCalculoTiempos(decimal IdConsecutivoEstudio)
        {
            try
            {
                string vRes;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesEstudioSyCs_Validar"))
                {

                    vDataBase.AddOutParameter(vDbCommand, "@Res", DbType.String, 200);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, IdConsecutivoEstudio);
                    
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    vRes=(vDataBase.GetParameterValue(vDbCommand, "@Res").ToString());

                    return vRes;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para insertar Tiempos entre actividades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id tiempo entre actividades insertado</returns>
        public decimal InsertarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividades_Insertar"))
                {

                    vDataBase.AddOutParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio);                    
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTiempoEntreActividadesEstudioSyC.UsuarioCrea);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    pTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades = Convert.ToDecimal(vDataBase.GetParameterValue(vDbCommand, "@IdTiempoEntreActividades").ToString());
                    GenerarLogAuditoria(pTiempoEntreActividadesEstudioSyC, vDbCommand);
                    return pTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar tiempos entre actividades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int ModificarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividades_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTiempoEntreActividadesEstudioSyC.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTiempoEntreActividadesEstudioSyC, vDbCommand);                    
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para eliminar tiempos entre activiades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int EliminarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividades_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTiempoEntreActividadesEstudioSyC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        /// <summary>
        /// M�todo para insertar tiempos SyC
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el id insertado</returns>
        public decimal InsertarTiempoEntreActividadesEstudioSyC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesEstudioSyC_Insertar"))
                {

                    
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades);                    
                    vDataBase.AddInParameter(vDbCommand, "@FentregaES_ECTiemposEquipo", DbType.DateTime, pTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo.Equals(string.Empty)?(DateTime?)null:Convert.ToDateTime(pTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo) );
                    vDataBase.AddInParameter(vDbCommand, "@FEentregaES_ECTiemposIndicador", DbType.DateTime, pTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador));
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreFCTFinalYSDC", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaDiasHabilesEntreFCTFinalYSDCR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreFCTFinalYSDCColor", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDCColor);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEnESOCOSTEO", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEO);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaDiasHabilesEnESOCOSTEOR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEnESOCOSTEOR);
                    vDataBase.AddInParameter(vDbCommand, "@DevueltoDiasHabilesEnESOCOSTEOR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.DevueltoDiasHabilesEnESOCOSTEOR);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEnESOCOSTEOColor", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEOColor);
                    vDataBase.AddInParameter(vDbCommand, "@DiashabilesParaDevolucion", DbType.String, pTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaDiasHabilesParaDevolucionR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesParaDevolucionR);
                    vDataBase.AddInParameter(vDbCommand, "@DiashabilesParaDevolucionColor", DbType.String, pTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucionColor);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTiempoEntreActividadesEstudioSyC.UsuarioCrea);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    pTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades = Convert.ToDecimal(vDataBase.GetParameterValue(vDbCommand, "@IdTiempoEntreActividades").ToString());
                    GenerarLogAuditoria(pTiempoEntreActividadesEstudioSyC, vDbCommand);
                    return pTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar tiempos SyC
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int ModificarTiempoEntreActividadesEstudioSyC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesEstudioSyC_Modificar"))
                {
                    int vResultado; 
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades);
                    vDataBase.AddInParameter(vDbCommand, "@FentregaES_ECTiemposEquipo", DbType.DateTime, pTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo));
                    vDataBase.AddInParameter(vDbCommand, "@FEentregaES_ECTiemposIndicador", DbType.DateTime, pTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador));
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreFCTFinalYSDC", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaDiasHabilesEntreFCTFinalYSDCR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreFCTFinalYSDCColor", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDCColor);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEnESOCOSTEO", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEO);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaDiasHabilesEnESOCOSTEOR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEnESOCOSTEOR);
                    vDataBase.AddInParameter(vDbCommand, "@DevueltoDiasHabilesEnESOCOSTEOR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.DevueltoDiasHabilesEnESOCOSTEOR);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEnESOCOSTEOColor", DbType.String, pTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEOColor);
                    vDataBase.AddInParameter(vDbCommand, "@DiashabilesParaDevolucion", DbType.String, pTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaDiasHabilesParaDevolucionR", DbType.Boolean, pTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesParaDevolucionR);
                    vDataBase.AddInParameter(vDbCommand, "@DiashabilesParaDevolucionColor", DbType.String, pTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucionColor);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTiempoEntreActividadesEstudioSyC.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTiempoEntreActividadesEstudioSyC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }        
        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesEstudioSyC(decimal pIdTiempoEntreActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesEstudioSyC_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pIdTiempoEntreActividades);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiempoEntreActividadesEstudioSyC vTiempoEntreActividadesEstudioSyC = new TiempoEntreActividadesEstudioSyC();
                        while (vDataReaderResults.Read())
                        {
                            


                            vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades = vDataReaderResults["IdTiempoEntreActividades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTiempoEntreActividades"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades;
                            vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio;
                            vTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo = vDataReaderResults["FentregaES_ECTiemposEquipo"] != DBNull.Value ? (vDataReaderResults["FentregaES_ECTiemposEquipo"].ToString()) : vTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo;
                            vTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador = vDataReaderResults["FEentregaES_ECTiemposIndicador"] != DBNull.Value ? (vDataReaderResults["FEentregaES_ECTiemposIndicador"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado = vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"] != DBNull.Value ? (vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR = vDataReaderResults["NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR"] != DBNull.Value ? Convert.ToBoolean (vDataReaderResults["NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor = vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC = vDataReaderResults["DiasHabilesEntreFCTFinalYSDC"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEntreFCTFinalYSDC"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR = vDataReaderResults["NoAplicaDiasHabilesEntreFCTFinalYSDCR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesEntreFCTFinalYSDCR"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDCColor = vDataReaderResults["DiasHabilesEntreFCTFinalYSDCColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEntreFCTFinalYSDCColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDCColor;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEO = vDataReaderResults["DiasHabilesEnESOCOSTEO"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEnESOCOSTEO"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEO;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEnESOCOSTEOR = vDataReaderResults["NoAplicaDiasHabilesEnESOCOSTEOR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesEnESOCOSTEOR"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEnESOCOSTEOR;
                            vTiempoEntreActividadesEstudioSyC.DevueltoDiasHabilesEnESOCOSTEOR = vDataReaderResults["DevueltoDiasHabilesEnESOCOSTEOR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["DevueltoDiasHabilesEnESOCOSTEOR"].ToString()) : vTiempoEntreActividadesEstudioSyC.DevueltoDiasHabilesEnESOCOSTEOR;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEOColor = vDataReaderResults["DiasHabilesEnESOCOSTEOColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEnESOCOSTEOColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEOColor;
                            vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucion = vDataReaderResults["DiashabilesParaDevolucion"] != DBNull.Value ? (vDataReaderResults["DiashabilesParaDevolucion"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucion;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesParaDevolucionR = vDataReaderResults["NoAplicaDiasHabilesParaDevolucionR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesParaDevolucionR"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesParaDevolucionR;
                            vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucionColor = vDataReaderResults["DiashabilesParaDevolucionColor"] != DBNull.Value ? (vDataReaderResults["DiashabilesParaDevolucionColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucionColor;

                        }
                        return vTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List< TiempoEntreActividadesEstudioSyC> ConsultarTiempoEntreActividadesEstudioSyCs(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesEstudioSyCs_Consultar"))
                {
                    if (pIdConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pIdConsecutivoEstudio);
                    if (!pNombreAbreviado.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (!pObjeto.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdModalidadDeSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadDeSeleccion);
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pDireccionSolicitante);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiempoEntreActividadesEstudioSyC> vListaTiempoEntreActividadesEstudioSyC = new List<TiempoEntreActividadesEstudioSyC>();
                        
                        while (vDataReaderResults.Read())
                        {
                            TiempoEntreActividadesEstudioSyC vTiempoEntreActividadesEstudioSyC = new TiempoEntreActividadesEstudioSyC();

                            vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades = vDataReaderResults["IdTiempoEntreActividades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTiempoEntreActividades"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades;
                            vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio;
                            vTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo = vDataReaderResults["FentregaES_ECTiemposEquipo"] != DBNull.Value ? (vDataReaderResults["FentregaES_ECTiemposEquipo"].ToString()) : vTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo;
                            vTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador = vDataReaderResults["FEentregaES_ECTiemposIndicador"] != DBNull.Value ? (vDataReaderResults["FEentregaES_ECTiemposIndicador"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado = vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"] != DBNull.Value ? (vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR = vDataReaderResults["NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor = vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC = vDataReaderResults["DiasHabilesEntreFCTFinalYSDC"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEntreFCTFinalYSDC"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR = vDataReaderResults["NoAplicaDiasHabilesEntreFCTFinalYSDCR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesEntreFCTFinalYSDCR"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDCColor = vDataReaderResults["DiasHabilesEntreFCTFinalYSDCColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEntreFCTFinalYSDCColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDCColor;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEO = vDataReaderResults["DiasHabilesEnESOCOSTEO"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEnESOCOSTEO"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEO;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEnESOCOSTEOR = vDataReaderResults["NoAplicaDiasHabilesEnESOCOSTEOR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesEnESOCOSTEOR"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesEnESOCOSTEOR;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEOColor = vDataReaderResults["DiasHabilesEnESOCOSTEOColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEnESOCOSTEOColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEOColor;
                            vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucion = vDataReaderResults["DiashabilesParaDevolucion"] != DBNull.Value ? (vDataReaderResults["DiashabilesParaDevolucion"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucion;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesParaDevolucionR = vDataReaderResults["NoAplicaDiasHabilesParaDevolucionR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesParaDevolucionR"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaDiasHabilesParaDevolucionR;
                            vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucionColor = vDataReaderResults["DiashabilesParaDevolucionColor"] != DBNull.Value ? (vDataReaderResults["DiashabilesParaDevolucionColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucionColor;
                            vListaTiempoEntreActividadesEstudioSyC.Add(vTiempoEntreActividadesEstudioSyC);
                        }
                        return vListaTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar tiempos ESC
        /// </summary>
        /// <param name="pTiempoEntreActividadesFechasESC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id insertado</returns>
        public decimal InsertarTiempoEntreActividadesFechasESC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesFechasESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesFechasESC_Insertar"))
                {     

                    int vResult;
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoEntreActividadesFechasESC.IdTiempoEntreActividades);
                    vDataBase.AddInParameter(vDbCommand, "@FRealFCTPreliminarESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FRealFCTPreliminarESC.Equals(string.Empty)?(DateTime?)null :Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FRealFCTPreliminarESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFRealFCTPreliminarESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFRealFCTPreliminarESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaES_EC_ESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaES_EC_ESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaES_EC_ESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaES_EC_ESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaES_EC_ESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaDocsRadicadosEnContratosESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaDocsRadicadosEnContratosESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaDocsRadicadosEnContratosESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaDocsRadicadosEnContratosESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaComitecontratacionESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaComitecontratacionESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaComitecontratacionESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaComitecontratacionESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaComitecontratacionESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaInicioDelPS_ESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaInicioDelPS_ESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaInicioDelPS_ESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaInicioDelPS_ESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaInicioDelPS_ESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaPresentacionPropuestasESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaPresentacionPropuestasESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaPresentacionPropuestasESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaPresentacionPropuestasESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaPresentacionPropuestasESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaDeInicioDeEjecucionESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaDeInicioDeEjecucionESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaDeInicioDeEjecucionESC));
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTiempoEntreActividadesFechasESC.UsuarioCrea);
                    vResult = vDataBase.ExecuteNonQuery(vDbCommand);

                    GenerarLogAuditoria(pTiempoEntreActividadesFechasESC, vDbCommand);
                    return vResult;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar tiempos ESC
        /// </summary>
        /// <param name="pTiempoEntreActividadesFechasESC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id insertado</returns>
        public decimal ModificarTiempoEntreActividadesFechasESC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesFechasESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesFechasESC_Modificar"))
                {
                    int vResult;
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoEntreActividadesFechasESC.IdTiempoEntreActividades);
                    vDataBase.AddInParameter(vDbCommand, "@FRealFCTPreliminarESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FRealFCTPreliminarESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FRealFCTPreliminarESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFRealFCTPreliminarESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFRealFCTPreliminarESC);                    
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaES_EC_ESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaES_EC_ESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaES_EC_ESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaES_EC_ESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaES_EC_ESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaDocsRadicadosEnContratosESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaDocsRadicadosEnContratosESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaDocsRadicadosEnContratosESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaDocsRadicadosEnContratosESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaComitecontratacionESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaComitecontratacionESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaComitecontratacionESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaComitecontratacionESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaComitecontratacionESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaInicioDelPS_ESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaInicioDelPS_ESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaInicioDelPS_ESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaInicioDelPS_ESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaInicioDelPS_ESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaPresentacionPropuestasESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaPresentacionPropuestasESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaPresentacionPropuestasESC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaPresentacionPropuestasESC", DbType.Boolean, pTiempoEntreActividadesFechasESC.NoAplicaFEstimadaPresentacionPropuestasESC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaDeInicioDeEjecucionESC", DbType.DateTime, pTiempoEntreActividadesFechasESC.FEstimadaDeInicioDeEjecucionESC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesFechasESC.FEstimadaDeInicioDeEjecucionESC));
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTiempoEntreActividadesFechasESC.UsuarioModifica);
                    vResult = vDataBase.ExecuteNonQuery(vDbCommand);

                    GenerarLogAuditoria(pTiempoEntreActividadesFechasESC, vDbCommand);
                    return vResult;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar tiempos ESC
        /// </summary>
        /// <param name="pIdTiempoEntreActividades">Id Tiempo entre activiades</param>
        /// <returns>Retorna teimpos ESC</returns>
        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesFechasESC(decimal pIdTiempoEntreActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesFechasESC_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pIdTiempoEntreActividades);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiempoEntreActividadesEstudioSyC vTiempoEntreActividadesEstudioSyC = new TiempoEntreActividadesEstudioSyC();
                        while (vDataReaderResults.Read())
                        {
                            vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades = vDataReaderResults["IdTiempoEntreActividades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTiempoEntreActividades"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades;
                            vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal (vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio;
                            vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminarESC = vDataReaderResults["FRealFCTPreliminarESC"] != DBNull.Value ? (vDataReaderResults["FRealFCTPreliminarESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminarESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFRealFCTPreliminarESC = vDataReaderResults["NoAplicaFRealFCTPreliminarESC"] != DBNull.Value ?Convert.ToBoolean (vDataReaderResults["NoAplicaFRealFCTPreliminarESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFRealFCTPreliminarESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaES_EC_ESC = vDataReaderResults["FEstimadaES_EC_ESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaES_EC_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaES_EC_ESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaES_EC_ESC = vDataReaderResults["NoAplicaFEstimadaES_EC_ESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaES_EC_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaES_EC_ESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratosESC = vDataReaderResults["FEstimadaDocsRadicadosEnContratosESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaDocsRadicadosEnContratosESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratosESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaDocsRadicadosEnContratosESC = vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratosESC"] != DBNull.Value ? Convert.ToBoolean (vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratosESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaDocsRadicadosEnContratosESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaComitecontratacionESC = vDataReaderResults["FEstimadaComitecontratacionESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaComitecontratacionESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaComitecontratacionESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaComitecontratacionESC = vDataReaderResults["NoAplicaFEstimadaComitecontratacionESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaComitecontratacionESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaComitecontratacionESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS_ESC = vDataReaderResults["FEstimadaInicioDelPS_ESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaInicioDelPS_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS_ESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaInicioDelPS_ESC = vDataReaderResults["NoAplicaFEstimadaInicioDelPS_ESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaInicioDelPS_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaInicioDelPS_ESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestasESC = vDataReaderResults["FEstimadaPresentacionPropuestasESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaPresentacionPropuestasESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestasESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaPresentacionPropuestasESC = vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestasESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestasESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaPresentacionPropuestasESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucionESC = vDataReaderResults["FEstimadaDeInicioDeEjecucionESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaDeInicioDeEjecucionESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucionESC;

                        }
                        return vTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiempoEntreActividadesEstudioSyC> ConsultarTiempoEntreActividadesFechasESCs(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesFechasESCs_Consultar"))
                {
                    if (pIdConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pIdConsecutivoEstudio);
                    if (!pNombreAbreviado.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (!pObjeto.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdModalidadDeSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadDeSeleccion);
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pDireccionSolicitante);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiempoEntreActividadesEstudioSyC> vListaTiempoEntreActividadesEstudioSyC = new List<TiempoEntreActividadesEstudioSyC>();

                        while (vDataReaderResults.Read())
                        {
                            TiempoEntreActividadesEstudioSyC vTiempoEntreActividadesEstudioSyC = new TiempoEntreActividadesEstudioSyC();
                            vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades = vDataReaderResults["IdTiempoEntreActividades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTiempoEntreActividades"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades;
                            vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio;
                            vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminarESC = vDataReaderResults["FRealFCTPreliminarESC"] != DBNull.Value ? (vDataReaderResults["FRealFCTPreliminarESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminarESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFRealFCTPreliminarESC = vDataReaderResults["NoAplicaFRealFCTPreliminarESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFRealFCTPreliminarESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFRealFCTPreliminarESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaES_EC_ESC = vDataReaderResults["FEstimadaES_EC_ESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaES_EC_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaES_EC_ESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaES_EC_ESC = vDataReaderResults["NoAplicaFEstimadaES_EC_ESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaES_EC_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaES_EC_ESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratosESC = vDataReaderResults["FEstimadaDocsRadicadosEnContratosESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaDocsRadicadosEnContratosESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratosESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaDocsRadicadosEnContratosESC = vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratosESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratosESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaDocsRadicadosEnContratosESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaComitecontratacionESC = vDataReaderResults["FEstimadaComitecontratacionESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaComitecontratacionESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaComitecontratacionESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaComitecontratacionESC = vDataReaderResults["NoAplicaFEstimadaComitecontratacionESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaComitecontratacionESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaComitecontratacionESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS_ESC = vDataReaderResults["FEstimadaInicioDelPS_ESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaInicioDelPS_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS_ESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaInicioDelPS_ESC = vDataReaderResults["NoAplicaFEstimadaInicioDelPS_ESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaInicioDelPS_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaInicioDelPS_ESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestasESC = vDataReaderResults["FEstimadaPresentacionPropuestasESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaPresentacionPropuestasESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestasESC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaPresentacionPropuestasESC = vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestasESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestasESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaPresentacionPropuestasESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucionESC = vDataReaderResults["FEstimadaDeInicioDeEjecucionESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaDeInicioDeEjecucionESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucionESC;
                            vListaTiempoEntreActividadesEstudioSyC.Add(vTiempoEntreActividadesEstudioSyC);
                        }
                        return vListaTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int InsertarTiempoEntreActividadesFechasReales(TiempoEntreActividadesEstudioSyC pTiempoActividadesFechasReales)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesFechasReales_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoActividadesFechasReales.IdTiempoEntreActividades);
                    vDataBase.AddInParameter(vDbCommand, "@FRealFCTPreliminar", DbType.DateTime, pTiempoActividadesFechasReales.FRealFCTPreliminar.Equals(string.Empty)?(DateTime?)null:Convert.ToDateTime(pTiempoActividadesFechasReales.FRealFCTPreliminar));
                    vDataBase.AddInParameter(vDbCommand, "@FRealDocsRadicadosEnContratos", DbType.DateTime, pTiempoActividadesFechasReales.FRealDocsRadicadosEnContratos.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealDocsRadicadosEnContratos));
                    vDataBase.AddInParameter(vDbCommand, "@FRealFCTDefinitivaParaMCFRealES_MCRevisado", DbType.DateTime, pTiempoActividadesFechasReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado));
                    vDataBase.AddInParameter(vDbCommand, "@FRealComiteContratacion", DbType.DateTime, pTiempoActividadesFechasReales.FRealComiteContratacion.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealComiteContratacion));
                    vDataBase.AddInParameter(vDbCommand, "@FRealES_EC", DbType.DateTime, pTiempoActividadesFechasReales.FRealES_EC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealES_EC));
                    vDataBase.AddInParameter(vDbCommand, "@FRealInicioPS", DbType.DateTime, pTiempoActividadesFechasReales.FRealInicioPS.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealInicioPS));
                    vDataBase.AddInParameter(vDbCommand, "@FRealPresentacionPropuestas", DbType.DateTime, pTiempoActividadesFechasReales.FRealPresentacionPropuestas.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealPresentacionPropuestas));
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTiempoActividadesFechasReales.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    GenerarLogAuditoria(pTiempoActividadesFechasReales, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTiempoEntreActividadesFechasReales(TiempoEntreActividadesEstudioSyC pTiempoActividadesFechasReales)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesFechasReales_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoActividadesFechasReales.IdTiempoEntreActividades);
                    vDataBase.AddInParameter(vDbCommand, "@FRealFCTPreliminar", DbType.DateTime, pTiempoActividadesFechasReales.FRealFCTPreliminar.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealFCTPreliminar));
                    vDataBase.AddInParameter(vDbCommand, "@FRealDocsRadicadosEnContratos", DbType.DateTime, pTiempoActividadesFechasReales.FRealDocsRadicadosEnContratos.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealDocsRadicadosEnContratos));
                    vDataBase.AddInParameter(vDbCommand, "@FRealFCTDefinitivaParaMCFRealES_MCRevisado", DbType.DateTime, pTiempoActividadesFechasReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado));
                    vDataBase.AddInParameter(vDbCommand, "@FRealComiteContratacion", DbType.DateTime, pTiempoActividadesFechasReales.FRealComiteContratacion.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealComiteContratacion));
                    vDataBase.AddInParameter(vDbCommand, "@FRealES_EC", DbType.DateTime, pTiempoActividadesFechasReales.FRealES_EC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealES_EC));
                    vDataBase.AddInParameter(vDbCommand, "@FRealInicioPS", DbType.DateTime, pTiempoActividadesFechasReales.FRealInicioPS.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealInicioPS));
                    vDataBase.AddInParameter(vDbCommand, "@FRealPresentacionPropuestas", DbType.DateTime, pTiempoActividadesFechasReales.FRealPresentacionPropuestas.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoActividadesFechasReales.FRealPresentacionPropuestas));
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTiempoActividadesFechasReales.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    GenerarLogAuditoria(pTiempoActividadesFechasReales, vDbCommand);
                    return vResultado;
                    
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesFechasReales(decimal pIdTiempoEntreActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesFechasReales_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pIdTiempoEntreActividades);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiempoEntreActividadesEstudioSyC vTiempoEntreActividadesEstudioSyC = new TiempoEntreActividadesEstudioSyC();
                        while (vDataReaderResults.Read())
                        {
                            vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminar = vDataReaderResults["FRealFCTPreliminar"] != DBNull.Value ? (vDataReaderResults["FRealFCTPreliminar"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminar;
                            vTiempoEntreActividadesEstudioSyC.FRealDocsRadicadosEnContratos = vDataReaderResults["FRealDocsRadicadosEnContratos"] != DBNull.Value ? (vDataReaderResults["FRealDocsRadicadosEnContratos"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealDocsRadicadosEnContratos;
                            vTiempoEntreActividadesEstudioSyC.FRealFCTDefinitivaParaMCFRealES_MCRevisado = vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"] != DBNull.Value ? (vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
                            vTiempoEntreActividadesEstudioSyC.FRealComiteContratacion = vDataReaderResults["FRealComiteContratacion"] != DBNull.Value ? (vDataReaderResults["FRealComiteContratacion"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealComiteContratacion;
                            vTiempoEntreActividadesEstudioSyC.FRealES_EC = vDataReaderResults["FRealES_EC"] != DBNull.Value ? (vDataReaderResults["FRealES_EC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealES_EC;
                            vTiempoEntreActividadesEstudioSyC.FRealInicioPS = vDataReaderResults["FRealInicioPS"] != DBNull.Value ? (vDataReaderResults["FRealInicioPS"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealInicioPS;
                            vTiempoEntreActividadesEstudioSyC.FRealPresentacionPropuestas = vDataReaderResults["FRealPresentacionPropuestas"] != DBNull.Value ? (vDataReaderResults["FRealPresentacionPropuestas"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealPresentacionPropuestas;
                        }
                        return vTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiempoEntreActividadesEstudioSyC> ConsultarTiempoEntreActividadesFechasRealess(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesFechasRealess_Consultar"))
                {

                    if (pIdConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pIdConsecutivoEstudio);
                    if (!pNombreAbreviado.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (!pObjeto.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdModalidadDeSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadDeSeleccion);
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pDireccionSolicitante);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiempoEntreActividadesEstudioSyC> vListaTiempoEntreActividadesEstudioSyC = new List<TiempoEntreActividadesEstudioSyC>();

                        while (vDataReaderResults.Read())
                        {
                            TiempoEntreActividadesEstudioSyC vTiempoEntreActividadesEstudioSyC = new TiempoEntreActividadesEstudioSyC();
                            vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio;
                            vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades = vDataReaderResults["IdTiempoEntreActividades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTiempoEntreActividades"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades;
                            vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminar = vDataReaderResults["FRealFCTPreliminar"] != DBNull.Value ? (vDataReaderResults["FRealFCTPreliminar"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminar;
                            vTiempoEntreActividadesEstudioSyC.FRealDocsRadicadosEnContratos = vDataReaderResults["FRealDocsRadicadosEnContratos"] != DBNull.Value ?(vDataReaderResults["FRealDocsRadicadosEnContratos"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealDocsRadicadosEnContratos;
                            vTiempoEntreActividadesEstudioSyC.FRealFCTDefinitivaParaMCFRealES_MCRevisado = vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"] != DBNull.Value ? (vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
                            vTiempoEntreActividadesEstudioSyC.FRealComiteContratacion = vDataReaderResults["FRealComiteContratacion"] != DBNull.Value ? (vDataReaderResults["FRealComiteContratacion"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealComiteContratacion;
                            vTiempoEntreActividadesEstudioSyC.FRealES_EC = vDataReaderResults["FRealES_EC"] != DBNull.Value ? (vDataReaderResults["FRealES_EC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealES_EC;
                            vTiempoEntreActividadesEstudioSyC.FRealInicioPS = vDataReaderResults["FRealInicioPS"] != DBNull.Value ? (vDataReaderResults["FRealInicioPS"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealInicioPS;
                            vTiempoEntreActividadesEstudioSyC.FRealPresentacionPropuestas = vDataReaderResults["FRealPresentacionPropuestas"] != DBNull.Value ? (vDataReaderResults["FRealPresentacionPropuestas"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealPresentacionPropuestas;
                            vListaTiempoEntreActividadesEstudioSyC.Add(vTiempoEntreActividadesEstudioSyC);
                        }
                        return vListaTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public decimal InsertarTiempoEntreActividadesPACCO(TiemposPACCO pTiempoEntreActividadesPACCO)
        {
            try
            {
  
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesPACCO_Insertar"))
                {
                    decimal vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoEntreActividadesPACCO.IdTiempoEntreActividades);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTPreliminarREGINICIAL", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaFCTPreliminarREGINICIAL.Equals(string.Empty)?(DateTime?)null:Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaFCTPreliminarREGINICIAL));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaFCTPreliminarREGINICIAL", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTPreliminar", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaFCTPreliminar.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaFCTPreliminar));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaFCTPreliminar", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaFCTPreliminar);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado);
                    vDataBase.AddInParameter(vDbCommand, "@FEestimadaES_EC", DbType.DateTime, pTiempoEntreActividadesPACCO.FEestimadaES_EC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEestimadaES_EC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEestimadaES_EC", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEestimadaES_EC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaDocsRadicadosEnContratos", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaDocsRadicadosEnContratos.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaDocsRadicadosEnContratos));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaDocsRadicadosEnContratos", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaComiteContratacion", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaComiteContratacion.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaComiteContratacion));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaComiteContratacion", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaComiteContratacion);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaInicioDelPS", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaInicioDelPS.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaInicioDelPS));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaInicioDelPS", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaInicioDelPS);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaPresentacionPropuestas", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaPresentacionPropuestas.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaPresentacionPropuestas));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaPresentacionPropuestas", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaPresentacionPropuestas);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaDeInicioDeEjecucion", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaDeInicioDeEjecucion.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaDeInicioDeEjecucion));
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTiempoEntreActividadesPACCO.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTiempoEntreActividadesPACCO, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTiempoEntreActividadesPACCO(TiemposPACCO pTiempoEntreActividadesPACCO)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesPACCO_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pTiempoEntreActividadesPACCO.IdTiempoEntreActividades);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTPreliminarREGINICIAL", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaFCTPreliminarREGINICIAL.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaFCTPreliminarREGINICIAL));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaFCTPreliminarREGINICIAL", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTPreliminar", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaFCTPreliminar.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaFCTPreliminar));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaFCTPreliminar", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaFCTPreliminar);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado);
                    vDataBase.AddInParameter(vDbCommand, "@FEestimadaES_EC", DbType.DateTime, pTiempoEntreActividadesPACCO.FEestimadaES_EC.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEestimadaES_EC));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEestimadaES_EC", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEestimadaES_EC);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaDocsRadicadosEnContratos", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaDocsRadicadosEnContratos.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaDocsRadicadosEnContratos));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaDocsRadicadosEnContratos", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaComiteContratacion", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaComiteContratacion.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaComiteContratacion));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaComiteContratacion", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaComiteContratacion);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaInicioDelPS", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaInicioDelPS.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaInicioDelPS));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaInicioDelPS", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaInicioDelPS);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaPresentacionPropuestas", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaPresentacionPropuestas.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaPresentacionPropuestas));
                    vDataBase.AddInParameter(vDbCommand, "@NoAplicaFEstimadaPresentacionPropuestas", DbType.Boolean, pTiempoEntreActividadesPACCO.NoAplicaFEstimadaPresentacionPropuestas);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaDeInicioDeEjecucion", DbType.DateTime, pTiempoEntreActividadesPACCO.FEstimadaDeInicioDeEjecucion.Equals(string.Empty) ? (DateTime?)null : Convert.ToDateTime(pTiempoEntreActividadesPACCO.FEstimadaDeInicioDeEjecucion));
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTiempoEntreActividadesPACCO.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTiempoEntreActividadesPACCO, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public TiemposPACCO ConsultarTiempoEntreActividadesPACCO(decimal pIdTiempoEntreActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesPACCO_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTiempoEntreActividades", DbType.Decimal, pIdTiempoEntreActividades);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiemposPACCO vTiempoEntreActividadesEstudioSyC = new TiemposPACCO();
                        while (vDataReaderResults.Read())
                        {

                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminarREGINICIAL = vDataReaderResults["FEstimadaFCTPreliminarREGINICIAL"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTPreliminarREGINICIAL"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminarREGINICIAL;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTPreliminarREGINICIAL = vDataReaderResults["NoAplicaFEstimadaFCTPreliminarREGINICIAL"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTPreliminarREGINICIAL"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTPreliminarREGINICIAL;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminar = vDataReaderResults["FEstimadaFCTPreliminar"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTPreliminar"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminar;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTPreliminar = vDataReaderResults["NoAplicaFEstimadaFCTPreliminar"] != DBNull.Value ?Convert.ToBoolean (vDataReaderResults["NoAplicaFEstimadaFCTPreliminar"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTPreliminar;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"] != DBNull.Value ?Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                            vTiempoEntreActividadesEstudioSyC.FEestimadaES_EC = vDataReaderResults["FEestimadaES_EC"] != DBNull.Value ?(vDataReaderResults["FEestimadaES_EC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEestimadaES_EC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEestimadaES_EC = vDataReaderResults["NoAplicaFEestimadaES_EC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEestimadaES_EC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEestimadaES_EC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratos = vDataReaderResults["FEstimadaDocsRadicadosEnContratos"] != DBNull.Value ? (vDataReaderResults["FEstimadaDocsRadicadosEnContratos"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratos;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaDocsRadicadosEnContratos = vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratos"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaDocsRadicadosEnContratos;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaComiteContratacion = vDataReaderResults["FEstimadaComiteContratacion"] != DBNull.Value ? (vDataReaderResults["FEstimadaComiteContratacion"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaComiteContratacion;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaComiteContratacion = vDataReaderResults["NoAplicaFEstimadaComiteContratacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaComiteContratacion"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaComiteContratacion;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS = vDataReaderResults["FEstimadaInicioDelPS"] != DBNull.Value ? (vDataReaderResults["FEstimadaInicioDelPS"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaInicioDelPS = vDataReaderResults["NoAplicaFEstimadaInicioDelPS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaInicioDelPS"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaInicioDelPS;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestas = vDataReaderResults["FEstimadaPresentacionPropuestas"] != DBNull.Value ? (vDataReaderResults["FEstimadaPresentacionPropuestas"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestas;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaPresentacionPropuestas = vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestas"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaPresentacionPropuestas;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucion = vDataReaderResults["FEstimadaDeInicioDeEjecucion"] != DBNull.Value ? (vDataReaderResults["FEstimadaDeInicioDeEjecucion"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucion;

                        }
                        return vTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiemposPACCO> ConsultarTiempoEntreActividadesPACCOs(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesPACCOs_Consultar"))
                {
                    if (pIdConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pIdConsecutivoEstudio);
                    if (!pNombreAbreviado.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (!pObjeto.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdModalidadDeSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadDeSeleccion);
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pDireccionSolicitante);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiemposPACCO> vListaTiempoEntreActividadesEstudioSyC = new List<TiemposPACCO>();

                        while (vDataReaderResults.Read())
                        {
                            TiemposPACCO vTiempoEntreActividadesEstudioSyC = new TiemposPACCO();
                            vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdConsecutivoEstudio;
                            vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades = vDataReaderResults["IdTiempoEntreActividades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTiempoEntreActividades"].ToString()) : vTiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminarREGINICIAL = vDataReaderResults["FEstimadaFCTPreliminarREGINICIAL"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTPreliminarREGINICIAL"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminarREGINICIAL;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTPreliminarREGINICIAL = vDataReaderResults["NoAplicaFEstimadaFCTPreliminarREGINICIAL"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTPreliminarREGINICIAL"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTPreliminarREGINICIAL;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminar = vDataReaderResults["FEstimadaFCTPreliminar"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTPreliminar"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminar;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTPreliminar = vDataReaderResults["NoAplicaFEstimadaFCTPreliminar"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTPreliminar"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTPreliminar;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                            vTiempoEntreActividadesEstudioSyC.FEestimadaES_EC = vDataReaderResults["FEestimadaES_EC"] != DBNull.Value ? (vDataReaderResults["FEestimadaES_EC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEestimadaES_EC;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEestimadaES_EC = vDataReaderResults["NoAplicaFEestimadaES_EC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEestimadaES_EC"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEestimadaES_EC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratos = vDataReaderResults["FEstimadaDocsRadicadosEnContratos"] != DBNull.Value ? (vDataReaderResults["FEstimadaDocsRadicadosEnContratos"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratos;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaDocsRadicadosEnContratos = vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratos"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaDocsRadicadosEnContratos;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaComiteContratacion = vDataReaderResults["FEstimadaComiteContratacion"] != DBNull.Value ? (vDataReaderResults["FEstimadaComiteContratacion"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaComiteContratacion;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaComiteContratacion = vDataReaderResults["NoAplicaFEstimadaComiteContratacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaComiteContratacion"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaComiteContratacion;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS = vDataReaderResults["FEstimadaInicioDelPS"] != DBNull.Value ? (vDataReaderResults["FEstimadaInicioDelPS"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaInicioDelPS = vDataReaderResults["NoAplicaFEstimadaInicioDelPS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaInicioDelPS"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaInicioDelPS;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestas = vDataReaderResults["FEstimadaPresentacionPropuestas"] != DBNull.Value ? (vDataReaderResults["FEstimadaPresentacionPropuestas"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestas;
                            vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaPresentacionPropuestas = vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestas"].ToString()) : vTiempoEntreActividadesEstudioSyC.NoAplicaFEstimadaPresentacionPropuestas;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucion = vDataReaderResults["FEstimadaDeInicioDeEjecucion"] != DBNull.Value ? (vDataReaderResults["FEstimadaDeInicioDeEjecucion"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucion;
                            vListaTiempoEntreActividadesEstudioSyC.Add(vTiempoEntreActividadesEstudioSyC);
                        }
                        return vListaTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para obtener lista responsables
        /// </summary>
        /// <returns>Retorna lista de reposnsables</returns>
        public List<BaseDTO> ConsultarResponsable()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResponsablesESCs_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, 1);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BaseDTO> vListDTO = new List<BaseDTO>();
                        while (vDataReaderResults.Read())
                        {
                            BaseDTO dto = new BaseDTO();

                            dto.Id = vDataReaderResults["IdResponsable"] != DBNull.Value ? (vDataReaderResults["IdResponsable"].ToString()) : "0";
                            dto.Name = vDataReaderResults["Responsable"] != DBNull.Value ? (vDataReaderResults["Responsable"].ToString().ToUpper()) : string.Empty;


                            vListDTO.Add(dto);
                        }
                        return vListDTO;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar modalidad selecci�n
        /// </summary>
        /// <returns>Lista de modalidad selecci�n</returns>
        public List<BaseDTO> ConsultarModalidadSeleccion()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ModalidadesSeleccionESCs_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, 1);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BaseDTO> vListDTO = new List<BaseDTO>();
                        while (vDataReaderResults.Read())
                        {
                            BaseDTO dto = new BaseDTO();

                            dto.Id = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? (vDataReaderResults["IdModalidadSeleccion"].ToString()) : "0";
                            dto.Name = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? (vDataReaderResults["ModalidadSeleccion"].ToString().ToUpper()) : string.Empty;

                            vListDTO.Add(dto);
                        }
                        return vListDTO;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar Diracci�n del solicitante
        /// </summary>
        /// <returns>Lista de direcciones</returns>
        public List<BaseDTO> ConsultarDireccion()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DireccionesSolicitantesESCs_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, 1);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BaseDTO> vListDTO = new List<BaseDTO>();
                        while (vDataReaderResults.Read())
                        {
                            BaseDTO dto = new BaseDTO();

                            dto.Id = vDataReaderResults["IdDireccionesSolicitantes"] != DBNull.Value ? (vDataReaderResults["IdDireccionesSolicitantes"].ToString()) : "0";
                            dto.Name = vDataReaderResults["DireccionSolicitante"] != DBNull.Value ? (vDataReaderResults["DireccionSolicitante"].ToString().ToUpper()) : string.Empty;

                            vListDTO.Add(dto);
                        }
                        return vListDTO;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar estados 
        /// </summary>
        /// <returns>Retorna lista de estados</returns>
        public List<BaseDTO> ConsultarEstado()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_EstadoEstudios_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, 1);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BaseDTO> vListDTO = new List<BaseDTO>();
                        while (vDataReaderResults.Read())
                        {
                            BaseDTO dto = new BaseDTO();

                            dto.Id = vDataReaderResults["IdEstadoEstudio"] != DBNull.Value ? (vDataReaderResults["IdEstadoEstudio"].ToString()) : "0";
                            dto.Name = vDataReaderResults["Nombre"] != DBNull.Value ? (vDataReaderResults["Nombre"].ToString().ToUpper()) : string.Empty;

                            vListDTO.Add(dto);
                        }
                        return vListDTO;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar actividades
        /// </summary>
        /// <returns>Retorna lista de actividades</returns>
        public List<BaseDTO> ConsultarActividades()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Actividadess_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, 1);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BaseDTO> vListDTO = new List<BaseDTO>();
                        while (vDataReaderResults.Read())
                        {
                            BaseDTO dto = new BaseDTO();

                            dto.Id = vDataReaderResults["IdActividad"] != DBNull.Value ? (vDataReaderResults["IdActividad"].ToString()) : "0";
                            dto.Name = vDataReaderResults["Nombre"] != DBNull.Value ? (vDataReaderResults["Nombre"].ToString().ToUpper()) : string.Empty;

                            vListDTO.Add(dto);
                        }
                        return vListDTO;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar d�as festivos
        /// </summary>
        /// <param name="pAnnio">A�o</param>
        /// <returns>Listas de d�as festivos</returns>
        public List<BaseDTO> ConsultarDiasFestivos(int pAnnio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Consultar_Dias_Festivos"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Anio", DbType.Int32, pAnnio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BaseDTO> vListDTO = new List<BaseDTO>();
                        while (vDataReaderResults.Read())
                        {
                            BaseDTO dto = new BaseDTO();

                            dto.Id = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()).Date.ToString("dd/MM/yyyy") : "0";
                            dto.Name = vDataReaderResults["Fiesta"] != DBNull.Value ? (vDataReaderResults["Fiesta"].ToString().ToUpper()) : string.Empty;

                            vListDTO.Add(dto);
                        }
                        return vListDTO;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar vigencia
        /// </summary>
        /// <returns>Retorna una lista de vigencias</returns>
        public List<BaseDTO> ConsultarVigencia()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ConsultaGeneralSeguimiento_ConsultarVigencia"))
                {
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BaseDTO> vListDTO = new List<BaseDTO>();
                        while (vDataReaderResults.Read())
                        {
                            BaseDTO dto = new BaseDTO();

                            dto.Id = vDataReaderResults["vigencia"] != DBNull.Value ? (vDataReaderResults["vigencia"].ToString()) : string.Empty;
                            dto.Name = vDataReaderResults["vigencia"] != DBNull.Value ? (vDataReaderResults["vigencia"].ToString()) : string.Empty;

                            vListDTO.Add(dto);
                        }
                        return vListDTO;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar a�o cierre
        /// </summary>
        /// <returns>Lista de a�os cierre</returns>
        public List<BaseDTO> ConsultarAnioCierre()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CierreEstudiosPorVigencias_AnioCierre_Consultar"))
                {

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BaseDTO> vListDTO = new List<BaseDTO>();
                        while (vDataReaderResults.Read())
                        {
                            BaseDTO dto = new BaseDTO();

                            dto.Id = vDataReaderResults["AnioCierre"] != DBNull.Value ? (vDataReaderResults["AnioCierre"].ToString()) : string.Empty;
                            dto.Name = vDataReaderResults["AnioCierre"] != DBNull.Value ? (vDataReaderResults["AnioCierre"].ToString()) : string.Empty;

                            vListDTO.Add(dto);
                        }
                        return vListDTO;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public TiempoEntreActividadesEstudioSyC ConsultarTiemposESC(decimal pIdConsecutivoEstudio, string pFEstimadaFCTPreliminar)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiempoEntreActividadesEstudioSyCs_Calcular"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@FEstimadaFCTPreliminar", DbType.String, pFEstimadaFCTPreliminar);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiempoEntreActividadesEstudioSyC vTiempoEntreActividadesEstudioSyC = new TiempoEntreActividadesEstudioSyC();
                        while (vDataReaderResults.Read())
                        {
                            vTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo = vDataReaderResults["FentregaES_ECTiemposEquipo"] != DBNull.Value ? (vDataReaderResults["FentregaES_ECTiemposEquipo"].ToString()) : vTiempoEntreActividadesEstudioSyC.FentregaES_ECTiemposEquipo;
                            vTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador = vDataReaderResults["FEentregaES_ECTiemposIndicador"] != DBNull.Value ? (vDataReaderResults["FEentregaES_ECTiemposIndicador"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEentregaES_ECTiemposIndicador;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado = vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"] != DBNull.Value ? (vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor = vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC = vDataReaderResults["DiasHabilesEntreFCTFinalYSDC"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEntreFCTFinalYSDC"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDCColor = vDataReaderResults["DiasHabilesEntreFCTFinalYSDCColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEntreFCTFinalYSDCColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEntreFCTFinalYSDC;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEO = vDataReaderResults["DiasHabilesEnESOCOSTEO"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEnESOCOSTEO"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEO;
                            vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEOColor = vDataReaderResults["DiasHabilesEnESOCOSTEOColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEnESOCOSTEOColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiasHabilesEnESOCOSTEOColor;
                            vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucion = vDataReaderResults["DiasHabilesParaDevolucion"] != DBNull.Value ? (vDataReaderResults["DiasHabilesParaDevolucion"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucion;
                            vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucionColor = vDataReaderResults["DiasHabilesParaDevolucionColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesParaDevolucionColor"].ToString()) : vTiempoEntreActividadesEstudioSyC.DiashabilesParaDevolucionColor;
                                                        
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminarESC = vDataReaderResults["FEstimadaFCTPreliminarESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTPreliminarESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTPreliminarESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaES_EC_ESC = vDataReaderResults["FEstimadaES_EC_ESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaES_EC_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaES_EC_ESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratosESC = vDataReaderResults["FEstimadaDocsRadicadosEnContratosESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaDocsRadicadosEnContratosESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDocsRadicadosEnContratosESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaComitecontratacionESC = vDataReaderResults["FEstimadaComitecontratacionESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaComitecontratacionESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaComitecontratacionESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS_ESC = vDataReaderResults["FEstimadaInicioDelPS_ESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaInicioDelPS_ESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaInicioDelPS_ESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestasESC = vDataReaderResults["FEstimadaPresentacionPropuestasESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaPresentacionPropuestasESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaPresentacionPropuestasESC;
                            vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucionESC = vDataReaderResults["FEstimadaDeInicioDeEjecucionESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaDeInicioDeEjecucionESC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FEstimadaDeInicioDeEjecucionESC;

                            vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminar = vDataReaderResults["FRealFCTPreliminar"] != DBNull.Value ? (vDataReaderResults["FRealFCTPreliminar"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealFCTPreliminar;
                            vTiempoEntreActividadesEstudioSyC.FRealFCTDefinitivaParaMCFRealES_MCRevisado = vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"] != DBNull.Value ? (vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
                            vTiempoEntreActividadesEstudioSyC.FRealES_EC = vDataReaderResults["FRealES_EC"] != DBNull.Value ? (vDataReaderResults["FRealES_EC"].ToString()) : vTiempoEntreActividadesEstudioSyC.FRealES_EC;

                        }
                        return vTiempoEntreActividadesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
