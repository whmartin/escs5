﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlTypes;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class CierreEstudiosPorVigenciaDAL : GeneralDAL
    {
        public CierreEstudiosPorVigenciaDAL()
        {
        }

        public int InsertarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CierreEstudiosPorVigencia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCierreEstudiosPorVigencia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@AnioCierre", DbType.Int32, pCierreEstudiosPorVigencia.AnioCierre);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEjecucion", DbType.DateTime, pCierreEstudiosPorVigencia.FechaEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pCierreEstudiosPorVigencia.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCierreEstudiosPorVigencia.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vDataBase.GetParameterValue(vDbCommand, "@IdCierreEstudiosPorVigencia").ToString() != "")
                    {
                        pCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCierreEstudiosPorVigencia").ToString());
                        GenerarLogAuditoria(pCierreEstudiosPorVigencia, vDbCommand);
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CierreEstudiosPorVigencia_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCierreEstudiosPorVigencia", DbType.Int32, pCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@AnioCierre", DbType.Int32, pCierreEstudiosPorVigencia.AnioCierre);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEjecucion", DbType.DateTime, pCierreEstudiosPorVigencia.FechaEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pCierreEstudiosPorVigencia.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCierreEstudiosPorVigencia.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCierreEstudiosPorVigencia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CierreEstudiosPorVigencia_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCierreEstudiosPorVigencia", DbType.Int32, pCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCierreEstudiosPorVigencia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CierreEstudiosPorVigencia ConsultarCierreEstudiosPorVigencia(Int32 pIdCierreEstudiosPorVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CierreEstudiosPorVigencia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCierreEstudiosPorVigencia", DbType.Int32, pIdCierreEstudiosPorVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CierreEstudiosPorVigencia vCierreEstudiosPorVigencia = new CierreEstudiosPorVigencia();
                        while (vDataReaderResults.Read())
                        {
                            vCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia = vDataReaderResults["IdCierreEstudiosPorVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCierreEstudiosPorVigencia"].ToString()) : vCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia;
                            vCierreEstudiosPorVigencia.AnioCierre = vDataReaderResults["AnioCierre"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnioCierre"].ToString()) : vCierreEstudiosPorVigencia.AnioCierre;
                            vCierreEstudiosPorVigencia.FechaEjecucion = vDataReaderResults["FechaEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEjecucion"].ToString()) : vCierreEstudiosPorVigencia.FechaEjecucion;
                            vCierreEstudiosPorVigencia.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? vDataReaderResults["Descripcion"].ToString() : vCierreEstudiosPorVigencia.Descripcion;
                            vCierreEstudiosPorVigencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCierreEstudiosPorVigencia.UsuarioCrea;
                            vCierreEstudiosPorVigencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCierreEstudiosPorVigencia.FechaCrea;
                            vCierreEstudiosPorVigencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCierreEstudiosPorVigencia.UsuarioModifica;
                            vCierreEstudiosPorVigencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCierreEstudiosPorVigencia.FechaModifica;
                        }
                        return vCierreEstudiosPorVigencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<CierreEstudiosPorVigencia> ConsultarCierreEstudiosPorVigencias(int? pAnioCierre, DateTime? pFechaEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CierreEstudiosPorVigencias_Consultar"))
                {
                    if (pAnioCierre != null)
                        vDataBase.AddInParameter(vDbCommand, "@AnioCierre", DbType.Int32, pAnioCierre);
                    if (pFechaEjecucion != null)
                        vDataBase.AddInParameter(vDbCommand, "@FechaEjecucion", DbType.DateTime, pFechaEjecucion);            

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CierreEstudiosPorVigencia> vListaCierreEstudiosPorVigencia = new List<CierreEstudiosPorVigencia>();
                        while (vDataReaderResults.Read())
                        {
                            CierreEstudiosPorVigencia vCierreEstudiosPorVigencia = new CierreEstudiosPorVigencia();
                            vCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia = vDataReaderResults["IdCierreEstudiosPorVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCierreEstudiosPorVigencia"].ToString()) : vCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia;
                            vCierreEstudiosPorVigencia.AnioCierre = vDataReaderResults["AnioCierre"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnioCierre"].ToString()) : vCierreEstudiosPorVigencia.AnioCierre;
                            vCierreEstudiosPorVigencia.FechaEjecucion = vDataReaderResults["FechaEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEjecucion"].ToString()) : vCierreEstudiosPorVigencia.FechaEjecucion;
                            vCierreEstudiosPorVigencia.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? vDataReaderResults["Descripcion"].ToString() : vCierreEstudiosPorVigencia.Descripcion;
                            vCierreEstudiosPorVigencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCierreEstudiosPorVigencia.UsuarioCrea;
                            vCierreEstudiosPorVigencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCierreEstudiosPorVigencia.FechaCrea;
                            vCierreEstudiosPorVigencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCierreEstudiosPorVigencia.UsuarioModifica;
                            vCierreEstudiosPorVigencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCierreEstudiosPorVigencia.FechaModifica; 
                            vListaCierreEstudiosPorVigencia.Add(vCierreEstudiosPorVigencia);
                        }
                        return vListaCierreEstudiosPorVigencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<int> ConsultarAnioCierre()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CierreEstudiosPorVigencias_AnioCierre_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<int> vListaAnioCierre = new List<int>();
                        while (vDataReaderResults.Read())
                        {
                            int AnioCierre = vDataReaderResults["AnioCierre"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnioCierre"].ToString()) : 0;

                            vListaAnioCierre.Add(AnioCierre);
                        }
                        return vListaAnioCierre;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DateTime> ConsultarFechaEjecucion()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CierreEstudiosPorVigencias_FechaEjecucion_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DateTime> vListaFechaEjecucion = new List<DateTime>();
                        while (vDataReaderResults.Read())
                        {
                            DateTime FechaEjecucion = vDataReaderResults["FechaEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEjecucion"].ToString()) : DateTime.Now;

                            vListaFechaEjecucion.Add(FechaEjecucion);
                        }
                        return vListaFechaEjecucion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
