using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class ActividadesDAL : GeneralDAL
    {
        public ActividadesDAL()
        {
        }
        public int InsertarActividades(Actividades pActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Actividades_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdActividad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pActividades.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pActividades.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pActividades.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pActividades.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pActividades.IdActividad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdActividad").ToString());
                    GenerarLogAuditoria(pActividades, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarActividades(Actividades pActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Actividades_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, pActividades.IdActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pActividades.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pActividades.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pActividades.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pActividades.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pActividades, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarActividades(Actividades pActividades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Actividades_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, pActividades.IdActividad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pActividades, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public Actividades ConsultarActividades(int pIdActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Actividades_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdActividad", DbType.Int32, pIdActividad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Actividades vActividades = new Actividades();
                        while (vDataReaderResults.Read())
                        {
                            vActividades.IdActividad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActividad;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vActividades.Nombre;
                            vActividades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vActividades.Descripcion;
                            vActividades.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vActividades.Estado;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                        }
                        return vActividades;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Actividades> ConsultarActividadess(String pNombre, String pDescripcion, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Actividadess_Consultar"))
                {
                    if (pNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Actividades> vListaActividades = new List<Actividades>();
                        while (vDataReaderResults.Read())
                        {
                            Actividades vActividades = new Actividades();
                            vActividades.IdActividad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActividad;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vActividades.Nombre;
                            vActividades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vActividades.Descripcion;
                            vActividades.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vActividades.Estado;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vActividades.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vActividades.NombreEstado = "INACTIVO";
                                }
                            }
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            vListaActividades.Add(vActividades);
                        }
                        return vListaActividades;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarActividad(String pNombreActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Actividades_Validar_Duplicados"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombreActividad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Actividades vActividades = new Actividades();
                        while (vDataReaderResults.Read())
                        {
                            vActividades.IdActividad = vDataReaderResults["IdActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdActividad"].ToString()) : vActividades.IdActividad;
                            vActividades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vActividades.Nombre;
                            vActividades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vActividades.Descripcion;
                            vActividades.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vActividades.Estado;
                            vActividades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vActividades.UsuarioCrea;
                            vActividades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vActividades.FechaCrea;
                            vActividades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vActividades.UsuarioModifica;
                            vActividades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vActividades.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
