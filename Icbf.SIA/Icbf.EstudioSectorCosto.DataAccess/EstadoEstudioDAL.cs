using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class EstadoEstudioDAL : GeneralDAL
    {
        public EstadoEstudioDAL()
        {
        }
        public int InsertarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_EstadoEstudio_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEstadoEstudio", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pEstadoEstudio.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Motivo", DbType.String, pEstadoEstudio.Motivo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoEstudio.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstadoEstudio.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEstadoEstudio.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEstadoEstudio.IdEstadoEstudio = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEstadoEstudio").ToString());
                    GenerarLogAuditoria(pEstadoEstudio, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_EstadoEstudio_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoEstudio", DbType.Int32, pEstadoEstudio.IdEstadoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pEstadoEstudio.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Motivo", DbType.String, pEstadoEstudio.Motivo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoEstudio.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstadoEstudio.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEstadoEstudio.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoEstudio, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_EstadoEstudio_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoEstudio", DbType.Int32, pEstadoEstudio.IdEstadoEstudio);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoEstudio, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public EstadoEstudio ConsultarEstadoEstudio(int pIdEstadoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_EstadoEstudio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoEstudio", DbType.Int32, pIdEstadoEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoEstudio vEstadoEstudio = new EstadoEstudio();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoEstudio.IdEstadoEstudio = vDataReaderResults["IdEstadoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoEstudio"].ToString()) : vEstadoEstudio.IdEstadoEstudio;
                            vEstadoEstudio.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vEstadoEstudio.Nombre;
                            vEstadoEstudio.Motivo = vDataReaderResults["Motivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Motivo"].ToString()) : vEstadoEstudio.Motivo;
                            vEstadoEstudio.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoEstudio.Descripcion;
                            vEstadoEstudio.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vEstadoEstudio.Estado;
                            vEstadoEstudio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoEstudio.UsuarioCrea;
                            vEstadoEstudio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoEstudio.FechaCrea;
                            vEstadoEstudio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoEstudio.UsuarioModifica;
                            vEstadoEstudio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoEstudio.FechaModifica;
                        }
                        return vEstadoEstudio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EstadoEstudio> ConsultarEstadoEstudios(String pNombre, String pMotivo, String pDescripcion, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_EstadoEstudios_Consultar"))
                {
                    if (pNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pMotivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Motivo", DbType.String, pMotivo);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoEstudio> vListaEstadoEstudio = new List<EstadoEstudio>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoEstudio vEstadoEstudio = new EstadoEstudio();
                            vEstadoEstudio.IdEstadoEstudio = vDataReaderResults["IdEstadoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoEstudio"].ToString()) : vEstadoEstudio.IdEstadoEstudio;
                            vEstadoEstudio.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vEstadoEstudio.Nombre;
                            vEstadoEstudio.Motivo = vDataReaderResults["Motivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Motivo"].ToString()) : vEstadoEstudio.Motivo;
                            vEstadoEstudio.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoEstudio.Descripcion;
                            vEstadoEstudio.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vEstadoEstudio.Estado;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vEstadoEstudio.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vEstadoEstudio.NombreEstado = "INACTIVO";
                                }
                            }
                            vEstadoEstudio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoEstudio.UsuarioCrea;
                            vEstadoEstudio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoEstudio.FechaCrea;
                            vEstadoEstudio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoEstudio.UsuarioModifica;
                            vEstadoEstudio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoEstudio.FechaModifica;
                            vListaEstadoEstudio.Add(vEstadoEstudio);
                        }
                        return vListaEstadoEstudio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarEstadoEstudio(String pNombre, String pMotivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_EstadoEstudio_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pMotivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Motivo", DbType.String, pMotivo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoEstudio vEstadoEstudio = new EstadoEstudio();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoEstudio.IdEstadoEstudio = vDataReaderResults["IdEstadoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoEstudio"].ToString()) : vEstadoEstudio.IdEstadoEstudio;
                            vEstadoEstudio.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vEstadoEstudio.Nombre;
                            vEstadoEstudio.Motivo = vDataReaderResults["Motivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Motivo"].ToString()) : vEstadoEstudio.Motivo;
                            vEstadoEstudio.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoEstudio.Descripcion;
                            vEstadoEstudio.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vEstadoEstudio.Estado;
                            vEstadoEstudio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoEstudio.UsuarioCrea;
                            vEstadoEstudio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoEstudio.FechaCrea;
                            vEstadoEstudio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoEstudio.UsuarioModifica;
                            vEstadoEstudio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoEstudio.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
