﻿using Icbf.EstudioSectorCosto.Entity;
using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class ConsultaDependenciasDAL : GeneralDAL
    {
        public ConsultaDependenciasDAL()
        {
        }

        /// <summary>
        /// Método para listar dependencias
        /// </summary>
        /// <param name="pDireccionSolicitante">Dependencia solicitante</param>
        /// <param name="pVigencia">Año de vigencia</param>
        public List<ConsultaDependencias> ConsultarConsultaDependenciass(string pDireccionSolicitante, int? pVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ConsultaDependenciasESyC_Consultar"))
                {
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDireccionesSolicitantes", DbType.String, pDireccionSolicitante);
                    if (pVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConsultaDependencias> vListaConsultaDependencias = new List<ConsultaDependencias>();
                        while (vDataReaderResults.Read())
                        {
                            ConsultaDependencias vConsultaDependencias = new ConsultaDependencias();
                            vConsultaDependencias.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Vigencia"].ToString()) : vConsultaDependencias.Vigencia;
                            vConsultaDependencias.DireccionSolicitante =  vDataReaderResults["DireccionSolicitante"] != DBNull.Value ? vDataReaderResults["DireccionSolicitante"].ToString() : vConsultaDependencias.DireccionSolicitante;
                            vConsultaDependencias.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? vDataReaderResults["Objeto"].ToString() : vConsultaDependencias.Objeto;
                            vConsultaDependencias.FEstimadaFCTPreliminarREGINICIAL = vDataReaderResults["FEstimadaFCTPreliminarREGINICIAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FEstimadaFCTPreliminarREGINICIAL"].ToString()) : vConsultaDependencias.FEstimadaFCTPreliminarREGINICIAL;
                            vConsultaDependencias.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado= vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"].ToString()) : vConsultaDependencias.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                            vConsultaDependencias.FEestimadaES_EC = vDataReaderResults["FEestimadaES_EC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FEestimadaES_EC"].ToString()) : vConsultaDependencias.FEestimadaES_EC;
                            vConsultaDependencias.FRealFCTDefinitivaParaMCFRealES_MCRevisado= vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"].ToString()) : vConsultaDependencias.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
                            vConsultaDependencias.FechaSolicitudInicial = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudInicial"].ToString()) : vConsultaDependencias.FechaSolicitudInicial;
                            vConsultaDependencias.FechaDeEntregaESyC= vDataReaderResults["FechaDeEntregaESyC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDeEntregaESyC"].ToString()) : vConsultaDependencias.FechaDeEntregaESyC;
                            vListaConsultaDependencias.Add(vConsultaDependencias);
                        }
                        return vListaConsultaDependencias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
