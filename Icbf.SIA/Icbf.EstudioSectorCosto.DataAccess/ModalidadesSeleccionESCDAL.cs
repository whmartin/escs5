using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class ModalidadesSeleccionESCDAL : GeneralDAL
    {
        public ModalidadesSeleccionESCDAL()
        {
        }
        public int InsertarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ModalidadesSeleccionESC_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, pModalidadesSeleccionESC.ModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pModalidadesSeleccionESC.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pModalidadesSeleccionESC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pModalidadesSeleccionESC.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@TipoModalidad", DbType.String, pModalidadesSeleccionESC.TipoModalidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pModalidadesSeleccionESC.IdModalidadSeleccion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdModalidadSeleccion").ToString());
                    GenerarLogAuditoria(pModalidadesSeleccionESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ModalidadesSeleccionESC_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pModalidadesSeleccionESC.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, pModalidadesSeleccionESC.ModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pModalidadesSeleccionESC.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pModalidadesSeleccionESC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pModalidadesSeleccionESC.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@TipoModalidad", DbType.String, pModalidadesSeleccionESC.TipoModalidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModalidadesSeleccionESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ModalidadesSeleccionESC_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pModalidadesSeleccionESC.IdModalidadSeleccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModalidadesSeleccionESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public ModalidadesSeleccionESC ConsultarModalidadesSeleccionESC(int pIdModalidadSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ModalidadesSeleccionESC_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ModalidadesSeleccionESC vModalidadesSeleccionESC = new ModalidadesSeleccionESC();
                        while (vDataReaderResults.Read())
                        {
                            vModalidadesSeleccionESC.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vModalidadesSeleccionESC.IdModalidadSeleccion;
                            vModalidadesSeleccionESC.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vModalidadesSeleccionESC.ModalidadSeleccion;
                            vModalidadesSeleccionESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vModalidadesSeleccionESC.Descripcion;
                            vModalidadesSeleccionESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vModalidadesSeleccionESC.Estado;
                            vModalidadesSeleccionESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadesSeleccionESC.UsuarioCrea;
                            vModalidadesSeleccionESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadesSeleccionESC.FechaCrea;
                            vModalidadesSeleccionESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadesSeleccionESC.UsuarioModifica;
                            vModalidadesSeleccionESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadesSeleccionESC.FechaModifica;
                            vModalidadesSeleccionESC.TipoModalidad = vDataReaderResults["TipoModalidad"] != DBNull.Value ? (vDataReaderResults["TipoModalidad"].ToString()) : vModalidadesSeleccionESC.TipoModalidad;
                        }
                        return vModalidadesSeleccionESC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModalidadesSeleccionESC> ConsultarModalidadesSeleccionESCs(String pModalidadSeleccion, String pDescripcion, int? pEstado, string TipoModalidad=null)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ModalidadesSeleccionESCs_Consultar"))
                {
                    if (pModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, pModalidadSeleccion);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    if (TipoModalidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@Tipo", DbType.String, TipoModalidad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ModalidadesSeleccionESC> vListaModalidadesSeleccionESC = new List<ModalidadesSeleccionESC>();
                        while (vDataReaderResults.Read())
                        {
                            ModalidadesSeleccionESC vModalidadesSeleccionESC = new ModalidadesSeleccionESC();
                            vModalidadesSeleccionESC.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vModalidadesSeleccionESC.IdModalidadSeleccion;
                            vModalidadesSeleccionESC.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vModalidadesSeleccionESC.ModalidadSeleccion;
                            vModalidadesSeleccionESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vModalidadesSeleccionESC.Descripcion;
                            vModalidadesSeleccionESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vModalidadesSeleccionESC.Estado;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vModalidadesSeleccionESC.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vModalidadesSeleccionESC.NombreEstado = "INACTIVO";
                                }
                            }
                            vModalidadesSeleccionESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadesSeleccionESC.UsuarioCrea;
                            vModalidadesSeleccionESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadesSeleccionESC.FechaCrea;
                            vModalidadesSeleccionESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadesSeleccionESC.UsuarioModifica;
                            vModalidadesSeleccionESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadesSeleccionESC.FechaModifica;
                            vModalidadesSeleccionESC.TipoModalidad = vDataReaderResults["TipoModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoModalidad"].ToString()) : vModalidadesSeleccionESC.TipoModalidad;
                            vListaModalidadesSeleccionESC.Add(vModalidadesSeleccionESC);
                        }
                        return vListaModalidadesSeleccionESC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarModalidadesSeleccionESC(String pModalidadSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ModalidadesSeleccionESC_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccion", DbType.String, pModalidadSeleccion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ModalidadesSeleccionESC vModalidadesSeleccionESC = new ModalidadesSeleccionESC();
                        while (vDataReaderResults.Read())
                        {
                            vModalidadesSeleccionESC.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vModalidadesSeleccionESC.IdModalidadSeleccion;
                            vModalidadesSeleccionESC.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vModalidadesSeleccionESC.ModalidadSeleccion;
                            vModalidadesSeleccionESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vModalidadesSeleccionESC.Descripcion;
                            vModalidadesSeleccionESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vModalidadesSeleccionESC.Estado;
                            vModalidadesSeleccionESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadesSeleccionESC.UsuarioCrea;
                            vModalidadesSeleccionESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadesSeleccionESC.FechaCrea;
                            vModalidadesSeleccionESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadesSeleccionESC.UsuarioModifica;
                            vModalidadesSeleccionESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadesSeleccionESC.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
