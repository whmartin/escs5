using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class TiposEstudioDAL : GeneralDAL
    {
        public TiposEstudioDAL()
        {
        }
        public int InsertarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiposEstudio_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoEstudio", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTiposEstudio.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTiposEstudio.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pTiposEstudio.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTiposEstudio.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTiposEstudio.IdTipoEstudio = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoEstudio").ToString());
                    GenerarLogAuditoria(pTiposEstudio, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiposEstudio_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEstudio", DbType.Int32, pTiposEstudio.IdTipoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pTiposEstudio.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTiposEstudio.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pTiposEstudio.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTiposEstudio.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTiposEstudio, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiposEstudio_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEstudio", DbType.Int32, pTiposEstudio.IdTipoEstudio);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTiposEstudio, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public TiposEstudio ConsultarTiposEstudio(int pIdTipoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiposEstudio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEstudio", DbType.Int32, pIdTipoEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiposEstudio vTiposEstudio = new TiposEstudio();
                        while (vDataReaderResults.Read())
                        {
                            vTiposEstudio.IdTipoEstudio = vDataReaderResults["IdTipoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstudio"].ToString()) : vTiposEstudio.IdTipoEstudio;
                            vTiposEstudio.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTiposEstudio.Nombre;
                            vTiposEstudio.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTiposEstudio.Descripcion;
                            vTiposEstudio.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vTiposEstudio.Estado;
                            vTiposEstudio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTiposEstudio.UsuarioCrea;
                            vTiposEstudio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTiposEstudio.FechaCrea;
                            vTiposEstudio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTiposEstudio.UsuarioModifica;
                            vTiposEstudio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTiposEstudio.FechaModifica;
                        }
                        return vTiposEstudio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposEstudio> ConsultarTiposEstudios(String pNombre, String pDescripcion, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiposEstudios_Consultar"))
                {
                    if (pNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiposEstudio> vListaTiposEstudio = new List<TiposEstudio>();
                        while (vDataReaderResults.Read())
                        {
                            TiposEstudio vTiposEstudio = new TiposEstudio();
                            vTiposEstudio.IdTipoEstudio = vDataReaderResults["IdTipoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstudio"].ToString()) : vTiposEstudio.IdTipoEstudio;
                            vTiposEstudio.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTiposEstudio.Nombre;
                            vTiposEstudio.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTiposEstudio.Descripcion;
                            vTiposEstudio.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vTiposEstudio.Estado;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vTiposEstudio.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vTiposEstudio.NombreEstado = "INACTIVO";
                                }
                            }
                            vTiposEstudio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTiposEstudio.UsuarioCrea;
                            vTiposEstudio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTiposEstudio.FechaCrea;
                            vTiposEstudio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTiposEstudio.UsuarioModifica;
                            vTiposEstudio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTiposEstudio.FechaModifica;
                            vListaTiposEstudio.Add(vTiposEstudio);
                        }
                        return vListaTiposEstudio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarTiposEstudio(String pNombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_TiposEstudio_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiposEstudio vTiposEstudio = new TiposEstudio();
                        while (vDataReaderResults.Read())
                        {
                            vTiposEstudio.IdTipoEstudio = vDataReaderResults["IdTipoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstudio"].ToString()) : vTiposEstudio.IdTipoEstudio;
                            vTiposEstudio.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vTiposEstudio.Nombre;
                            vTiposEstudio.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTiposEstudio.Descripcion;
                            vTiposEstudio.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vTiposEstudio.Estado;
                            vTiposEstudio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTiposEstudio.UsuarioCrea;
                            vTiposEstudio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTiposEstudio.FechaCrea;
                            vTiposEstudio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTiposEstudio.UsuarioModifica;
                            vTiposEstudio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTiposEstudio.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
