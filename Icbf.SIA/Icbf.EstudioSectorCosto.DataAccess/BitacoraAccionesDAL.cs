using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class BitacoraAccionesDAL : GeneralDAL
    {
        public BitacoraAccionesDAL()
        {
        }

        /// <summary>
        /// M�todo para insertar una bit�cora de acci�n
        /// </summary>
        /// <param name="pBitacoraAcciones">La bit�cora de acci�n</param>
        public int InsertarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraAcciones_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdBitacoraEstudio", DbType.Int64, pBitacoraAcciones.IdBitacoraEstudio);                    
                    vDataBase.AddInParameter(vDbCommand, "@UltimaAccion", DbType.String, pBitacoraAcciones.UltimaAccion);
                    vDataBase.AddInParameter(vDbCommand, "@ProximaAccion", DbType.String, pBitacoraAcciones.ProximaAccion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRegistro", DbType.DateTime, pBitacoraAcciones.FechaRegistro);                    
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pBitacoraAcciones.UsuarioCrea);
                   
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioBitacora", DbType.String, pBitacoraAcciones.UsuarioBitacora);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pBitacoraAcciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar una bit�cora de acci�n
        /// </summary>
        /// <param name="pBitacoraAcciones">La bit�cora de acci�n</param>
        public int ModificarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraAcciones_Modificar"))
                {
                    int vResultado; 

                    vDataBase.AddInParameter(vDbCommand, "@IdBitacoraAccion", DbType.String, pBitacoraAcciones.IdBitacoraAcciones);
                    vDataBase.AddInParameter(vDbCommand, "@UltimaAccion", DbType.String, pBitacoraAcciones.UltimaAccion);
                    vDataBase.AddInParameter(vDbCommand, "@ProximaAccion", DbType.String, pBitacoraAcciones.ProximaAccion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModificacion", DbType.DateTime, pBitacoraAcciones.FechaModificacion);                    
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pBitacoraAcciones.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioBitacora", DbType.String, pBitacoraAcciones.UsuarioBitacora);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pBitacoraAcciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo eliminar una bit�cora de acci�n
        /// </summary>
        /// <param name="pBitacoraAcciones">La bit�cora de acci�n</param>
        public int EliminarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_bitacoraAcciones_ESC_BitacoraAcciones_Eliminar"))
                {
                    int vResultado;
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pBitacoraAcciones, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar una bit�cora de acci�n por su id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El id de la bit�cora de acci�n</param>
        public List< BitacoraAcciones >ConsultarBitacoraAcciones(decimal pIdBitacoraEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraAccioness_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdBitacoraEstudio", DbType.Int64, pIdBitacoraEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BitacoraAcciones> vLstBitacoraAcciones = new List<BitacoraAcciones>();
                        while (vDataReaderResults.Read())
                        {
                            BitacoraAcciones vBitacoraAcciones = new BitacoraAcciones(); 
                            vBitacoraAcciones.IdBitacoraEstudio = vDataReaderResults["IdBitacoraEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdBitacoraEstudio"].ToString()) : vBitacoraAcciones.IdBitacoraEstudio;
                            vBitacoraAcciones.IdBitacoraAcciones = vDataReaderResults["IdBitacoraAccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdBitacoraAccion"].ToString()) : vBitacoraAcciones.IdBitacoraAcciones;
                            vBitacoraAcciones.UltimaAccion = vDataReaderResults["UltimaAccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UltimaAccion"].ToString()) : vBitacoraAcciones.UltimaAccion;
                            vBitacoraAcciones.ProximaAccion = vDataReaderResults["ProximaAccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProximaAccion"].ToString()) : vBitacoraAcciones.ProximaAccion;
                            vBitacoraAcciones.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vBitacoraAcciones.FechaRegistro;
                            vBitacoraAcciones.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vBitacoraAcciones.FechaModificacion;
                            vBitacoraAcciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vBitacoraAcciones.UsuarioCrea;
                            vBitacoraAcciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vBitacoraAcciones.FechaCrea;
                            vBitacoraAcciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vBitacoraAcciones.UsuarioModifica;
                            vBitacoraAcciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vBitacoraAcciones.FechaModifica;
                            vBitacoraAcciones.UsuarioBitacora = vDataReaderResults["UsuarioBitacora"] != DBNull.Value ? (vDataReaderResults["UsuarioBitacora"].ToString()) : vBitacoraAcciones.UsuarioBitacora;
                            vLstBitacoraAcciones.Add(vBitacoraAcciones);
                        }
                        return vLstBitacoraAcciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para listar la bit�cora de acciones
        /// </summary>
        /// <param name="pIdEstudio">El id del estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado</param>
        /// /// <param name="pObjeto">El objeto</param>
        /// <param name="pIdResponsableES">El id del responsable del estudio de sector</param>
        /// /// <param name="pIdResponsableEC">El id del responsable del est�dio de costos</param>
        /// <param name="pIdModalidadSeleccion">El id de la Modalidad de contrataci�n</param>
        /// /// <param name="pDireccionSolicitante">Dependencia solicitante</param>
        /// <param name="pEstado">El estado</param>
        public List<BitacoraAcciones> ConsultarBitacoraAccioness(long? pIdEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, int? pIdModalidadSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraAcciones_Consultar"))
                {
                    if (pIdEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pIdEstudio);
                    if (!pNombreAbreviado.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (!pObjeto.Equals(string.Empty))
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pDireccionSolicitante);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {                        
                        List<BitacoraAcciones> vListaBitacoraAcciones = new List<BitacoraAcciones>();
                        while (vDataReaderResults.Read())
                        {
                            BitacoraAcciones vBitacoraAcciones = new BitacoraAcciones(); 
                            vBitacoraAcciones.IdBitacoraEstudio = vDataReaderResults["IdBitacoraEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdBitacoraEstudio"].ToString()) : vBitacoraAcciones.IdBitacoraEstudio;
                            vBitacoraAcciones.IdBitacoraAcciones = vDataReaderResults["IdBitacoraAccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdBitacoraAccion"].ToString()) : vBitacoraAcciones.IdBitacoraAcciones;
                            vBitacoraAcciones.IdEstudio = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vBitacoraAcciones.IdEstudio;                            
                            vBitacoraAcciones.UsuarioBitacora = vDataReaderResults["UsuarioBitacora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioBitacora"].ToString()) : vBitacoraAcciones.UsuarioBitacora;
                            vBitacoraAcciones.NombreAbreviado= vDataReaderResults["NombreAbreviado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbreviado"].ToString()) : vBitacoraAcciones.NombreAbreviado;
                            vBitacoraAcciones.UltimaAccion = vDataReaderResults["UltimaAccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UltimaAccion"].ToString()) : vBitacoraAcciones.UltimaAccion;
                            vBitacoraAcciones.ProximaAccion = vDataReaderResults["ProximaAccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProximaAccion"].ToString()) : vBitacoraAcciones.ProximaAccion;
                            vBitacoraAcciones.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vBitacoraAcciones.FechaRegistro;
                            vBitacoraAcciones.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vBitacoraAcciones.FechaModificacion;
                            vBitacoraAcciones.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vBitacoraAcciones.UsuarioCrea;
                            vBitacoraAcciones.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vBitacoraAcciones.FechaCrea;
                            vBitacoraAcciones.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vBitacoraAcciones.UsuarioModifica;
                            vBitacoraAcciones.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vBitacoraAcciones.FechaModifica;
                            vListaBitacoraAcciones.Add(vBitacoraAcciones);
                        }
                        return vListaBitacoraAcciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar una bit�cora de est�dio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bit�cora de est�dio</param>
        public decimal InsertarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraEstudio_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdBitacoraEstudio",DbType.Decimal,0);                    
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pBitacoraEstudio.IdConsecutivoEstudio);                                                  
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pBitacoraEstudio.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pBitacoraEstudio.FechaCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pBitacoraEstudio .IdBitacoraEstudio= Convert.ToDecimal(vDataBase.GetParameterValue(vDbCommand, "@IdBitacoraEstudio").ToString());
                    GenerarLogAuditoria(pBitacoraEstudio, vDbCommand);
                    return pBitacoraEstudio.IdBitacoraEstudio;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar una bit�cora de est�dio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bit�cora de est�dio</param>
        public decimal ModificarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraEstudio_Modificar"))
                {                    
                    vDataBase.AddInParameter(vDbCommand, "@IdBitacoraEstudio", DbType.Decimal, pBitacoraEstudio.IdBitacoraEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pBitacoraEstudio.IdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pBitacoraEstudio.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@FechaModifica", DbType.DateTime, pBitacoraEstudio.FechaModifica);
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    pBitacoraEstudio.IdBitacoraEstudio = Convert.ToDecimal(vDataBase.GetParameterValue(vDbCommand, "@IdBitacoraEstudio").ToString());
                    GenerarLogAuditoria(pBitacoraEstudio, vDbCommand);
                    return pBitacoraEstudio.IdBitacoraEstudio;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para eliminar una bit�cora de est�dio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bit�cora de est�dio</param>
        public int EliminarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraEstudio_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdBitacoraEstudio", DbType.Decimal, pBitacoraEstudio.IdBitacoraEstudio);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pBitacoraEstudio, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar una bit�cora de est�dio por su Id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El Id de la bit�cora de est�dio</param>
        public BitacoraEstudio ConsultarBitacoraEstudio(decimal pIdBitacoraEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraEstudio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdBitacoraEstudio", DbType.Int64, pIdBitacoraEstudio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        BitacoraEstudio vBitacoraEstudio = new BitacoraEstudio();
                        while (vDataReaderResults.Read())
                        {
                            
                            vBitacoraEstudio.IdBitacoraEstudio = vDataReaderResults["IdBitacoraEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdBitacoraEstudio"].ToString()) : vBitacoraEstudio.IdBitacoraEstudio;
                            vBitacoraEstudio.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vBitacoraEstudio.IdConsecutivoEstudio;
                            vBitacoraEstudio.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? (vDataReaderResults["UsuarioCrea"].ToString()) : vBitacoraEstudio.UsuarioCrea;
                            vBitacoraEstudio.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? (vDataReaderResults["UsuarioModifica"].ToString()) : vBitacoraEstudio.UsuarioModifica;
                            vBitacoraEstudio.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vBitacoraEstudio.FechaCrea;
                            vBitacoraEstudio.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vBitacoraEstudio.FechaModifica;
                        }
                        return vBitacoraEstudio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar si una bit�cora de est�dio existe por su Id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El Id de la bit�cora de est�dio</param>
        public bool ConsultarBitacoraEstudioExiste(decimal pIdconsecutivoEstudio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraEstudio_ExisteConsecutivo"))
                {
                    bool vResultadoExiste;
                    vDataBase.AddOutParameter(vDbCommand, "@existe", DbType.Boolean, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdconsecutivoEstudio", DbType.Decimal, pIdconsecutivoEstudio);
                    
                    vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultadoExiste = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@existe").ToString());                    
                    return vResultadoExiste;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar la traza de una bit�cora de acci�n
        /// </summary>
        /// <param name="pIdBitacoraAccion">El Id de la bit�cora de acci�n</param>
        public List<BitacoraAcciones> ConsultarBitacoraAccionTraza(decimal pIdBitacoraAccion)
        {

            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_BitacoraAccionTraza_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdBitacoraAccion", DbType.Int64, pIdBitacoraAccion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BitacoraAcciones> vLstBitacoraAcciones = new List<BitacoraAcciones>();
                        while (vDataReaderResults.Read())
                        {
                            BitacoraAcciones vBitacoraAcciones = new BitacoraAcciones();
                            
                            vBitacoraAcciones.IdBitacoraAcciones = vDataReaderResults["IdBitacoraAccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdBitacoraAccion"].ToString()) : vBitacoraAcciones.IdBitacoraAcciones;
                            vBitacoraAcciones.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : vBitacoraAcciones.FechaRegistro;
                            vBitacoraAcciones.UsuarioCrea = vDataReaderResults["UsuarioRegistro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioRegistro"].ToString()) : vBitacoraAcciones.UsuarioCrea;
                            vBitacoraAcciones.FechaModifica = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vBitacoraAcciones.FechaModifica;
                            vBitacoraAcciones.UsuarioModifica = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vBitacoraAcciones.UsuarioModifica;
                            vBitacoraAcciones.UltimaAccion = vDataReaderResults["UltimaAccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UltimaAccion"].ToString()) : vBitacoraAcciones.UltimaAccion;
                            vBitacoraAcciones.ProximaAccion = vDataReaderResults["ProximaAccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProximaAccion"].ToString()) : vBitacoraAcciones.ProximaAccion;                          
                            vLstBitacoraAcciones.Add(vBitacoraAcciones);
                        }
                        return vLstBitacoraAcciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
