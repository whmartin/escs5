using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlTypes;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class RegistroSolicitudEstudioSectoryCasoDAL : GeneralDAL
    {
        public RegistroSolicitudEstudioSectoryCasoDAL()
        {
        }
        public int InsertarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudEstudioSectoryCaso_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRegistroSolicitudEstudioSectoryCaso", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@AplicaPACCO", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.AplicaPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoPACCO", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionsolicitantePACCO", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoPACCO", DbType.String, pRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadPACCO", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestalPACCO", DbType.Decimal, pRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaPACCO", DbType.String, pRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoEstudioRelacionado", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudInicial", DbType.DateTime, pRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial);
                    vDataBase.AddInParameter(vDbCommand, "@ActaCorreoNoRadicado", DbType.String, pRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado);
                    vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pRegistroSolicitudEstudioSectoryCaso.NombreAbreviado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroReproceso", DbType.String, pRegistroSolicitudEstudioSectoryCaso.NumeroReproceso);
                    vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pRegistroSolicitudEstudioSectoryCaso.Objeto);

                    vDataBase.AddInParameter(vDbCommand, "@VigenciasEjecucion", DbType.String, pRegistroSolicitudEstudioSectoryCaso.VigenciasEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@ReprocesoVincula", DbType.String, pRegistroSolicitudEstudioSectoryCaso.ReprocesoVincula);

                    vDataBase.AddInParameter(vDbCommand, "@CuentaVigenciasFuturasPACCO", DbType.String, pRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@AplicaProcesoSeleccion", DbType.String, pRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEstudio", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadInterna", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdResponsableES);
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdResponsableEC);
                    vDataBase.AddInParameter(vDbCommand, "@OrdenadorGasto", DbType.String, pRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoSolicitud", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdMotivoSolicitud", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccionSolicitante", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdAreaSolicitante", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@TipoValor", DbType.String, pRegistroSolicitudEstudioSectoryCaso.TipoValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestoEstimadoSolicitante", DbType.Decimal, pRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRegistroSolicitudEstudioSectoryCaso.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRegistroSolicitudEstudioSectoryCaso").ToString());
                    GenerarLogAuditoria(pRegistroSolicitudEstudioSectoryCaso, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudEstudioSectoryCaso_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudioSectoryCaso", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso);
                    vDataBase.AddInParameter(vDbCommand, "@AplicaPACCO", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.AplicaPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoPACCO", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionsolicitantePACCO", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoPACCO", DbType.String, pRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadPACCO", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestalPACCO", DbType.Decimal, pRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaPACCO", DbType.String, pRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ConsecutivoEstudioRelacionado", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudInicial", DbType.DateTime, pRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial);
                    vDataBase.AddInParameter(vDbCommand, "@ActaCorreoNoRadicado", DbType.String, pRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado);
                    vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pRegistroSolicitudEstudioSectoryCaso.NombreAbreviado);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroReproceso", DbType.String, pRegistroSolicitudEstudioSectoryCaso.NumeroReproceso);
                    vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pRegistroSolicitudEstudioSectoryCaso.Objeto);

                    vDataBase.AddInParameter(vDbCommand, "@VigenciasEjecucion", DbType.String, pRegistroSolicitudEstudioSectoryCaso.VigenciasEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@ReprocesoVincula", DbType.String, pRegistroSolicitudEstudioSectoryCaso.ReprocesoVincula);

                    vDataBase.AddInParameter(vDbCommand, "@CuentaVigenciasFuturasPACCO", DbType.String, pRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@AplicaProcesoSeleccion", DbType.String, pRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEstudio", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadInterna", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdResponsableES);
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdResponsableEC);
                    vDataBase.AddInParameter(vDbCommand, "@OrdenadorGasto", DbType.String, pRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoSolicitud", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdMotivoSolicitud", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccionSolicitante", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdAreaSolicitante", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@TipoValor", DbType.String, pRegistroSolicitudEstudioSectoryCaso.TipoValor);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestoEstimadoSolicitante", DbType.Decimal, pRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRegistroSolicitudEstudioSectoryCaso.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRegistroSolicitudEstudioSectoryCaso, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudEstudioSectoryCaso_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudioSectoryCaso", DbType.Int32, pRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRegistroSolicitudEstudioSectoryCaso, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public RegistroSolicitudEstudioSectoryCaso ConsultarRegistroSolicitudEstudioSectoryCaso(Int32 pIdRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudEstudioSectoryCaso_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudioSectoryCaso", DbType.Int32, pIdRegistroSolicitudEstudioSectoryCaso);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
                        while (vDataReaderResults.Read())
                        {
                            vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso;
                            vRegistroSolicitudEstudioSectoryCaso.AplicaPACCO = vDataReaderResults["AplicaPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AplicaPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.AplicaPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO = vDataReaderResults["ConsecutivoPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO = vDataReaderResults["DireccionsolicitantePACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DireccionsolicitantePACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO = vDataReaderResults["ObjetoPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO = vDataReaderResults["ModalidadPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ModalidadPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO = vDataReaderResults["ValorPresupuestalPACCO"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestalPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO = vDataReaderResults["VigenciaPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado = vDataReaderResults["ConsecutivoEstudioRelacionado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoEstudioRelacionado"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado;
                            vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudInicial"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial;
                            vRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado = vDataReaderResults["ActaCorreoNoRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ActaCorreoNoRadicado"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado;
                            vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado = vDataReaderResults["NombreAbreviado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbreviado"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado;
                            vRegistroSolicitudEstudioSectoryCaso.NumeroReproceso = vDataReaderResults["NumeroReproceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroReproceso"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.NumeroReproceso;
                            vRegistroSolicitudEstudioSectoryCaso.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Objeto"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.Objeto;

                            vRegistroSolicitudEstudioSectoryCaso.ReprocesoVincula = vDataReaderResults["ReprocesoVincula"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ReprocesoVincula"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ReprocesoVincula;

                            vRegistroSolicitudEstudioSectoryCaso.VigenciasEjecucion = vDataReaderResults["VigenciasEjecucion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciasEjecucion"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.VigenciasEjecucion;

                            vRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO = vDataReaderResults["CuentaVigenciasFuturasPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CuentaVigenciasFuturasPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion = vDataReaderResults["AplicaProcesoSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AplicaProcesoSeleccion"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion;
                            vRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion;
                            vRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio = vDataReaderResults["IdTipoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstudio"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio;
                            vRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna = vDataReaderResults["IdComplejidadInterna"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadInterna"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna;
                            vRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador;
                            vRegistroSolicitudEstudioSectoryCaso.IdResponsableES = vDataReaderResults["IdResponsableES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsableES"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdResponsableES;
                            vRegistroSolicitudEstudioSectoryCaso.IdResponsableEC = vDataReaderResults["IdResponsableEC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsableEC"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdResponsableEC;
                            vRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto = vDataReaderResults["OrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["OrdenadorGasto"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto;
                            vRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud;
                            vRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud = vDataReaderResults["IdMotivoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMotivoSolicitud"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud;
                            vRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante = vDataReaderResults["IdDireccionSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionSolicitante"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante;
                            vRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante = vDataReaderResults["IdAreaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAreaSolicitante"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante;
                            vRegistroSolicitudEstudioSectoryCaso.TipoValor = vDataReaderResults["TipoValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoValor"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.TipoValor;
                            vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante = vDataReaderResults["ValorPresupuestoEstimadoSolicitante"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestoEstimadoSolicitante"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante;
                            vRegistroSolicitudEstudioSectoryCaso.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.UsuarioCrea;
                            vRegistroSolicitudEstudioSectoryCaso.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaCrea;
                            vRegistroSolicitudEstudioSectoryCaso.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.UsuarioModifica;
                            vRegistroSolicitudEstudioSectoryCaso.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaModifica;
                            vRegistroSolicitudEstudioSectoryCaso.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadSeleccion"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.UsuarioModifica;                              
                        }
                        return vRegistroSolicitudEstudioSectoryCaso;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos(Int32? pConsecutivoEstudio, int? pAplicaPACCO, int? pConsecutivoPACCO, int? pDireccionsolicitantePACCO, String pObjetoPACCO, int? pModalidadPACCO, Decimal? pValorPresupuestalPACCO, String pVigenciaPACCO, int? pConsecutivoEstudioRelacionado, DateTime pFechaSolicitudInicial, String pActaCorreoNoRadicado, String pNombreAbreviado, String pNumeroReproceso, String pObjeto, String pCuentaVigenciasFuturasPACCO, String pAplicaProcesoSeleccion, int? pIdModalidadSeleccion, int? pIdTipoEstudio, int? pIdComplejidadInterna, int? pIdComplejidadIndicador, int? pIdResponsableES, int? pIdResponsableEC, String pOrdenadorGasto, int? pIdEstadoSolicitud, int? pIdMotivoSolicitud, int? pIdDireccionSolicitante, int? pIdAreaSolicitante, String pTipoValor, Decimal? pValorPresupuestoEstimadoSolicitante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudEstudioSectoryCasos_Consultar"))
                {
                    if (pConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegistroSolicitudEstudioSectoryCaso", DbType.Int32, pConsecutivoEstudio);
                    if(pAplicaPACCO != null)
                         vDataBase.AddInParameter(vDbCommand, "@AplicaPACCO", DbType.Int32, pAplicaPACCO);
                    if(pConsecutivoPACCO != null)
                         vDataBase.AddInParameter(vDbCommand, "@ConsecutivoPACCO", DbType.Int32, pConsecutivoPACCO);
                    if(pDireccionsolicitantePACCO != null)
                         vDataBase.AddInParameter(vDbCommand, "@DireccionsolicitantePACCO", DbType.Int32, pDireccionsolicitantePACCO);
                    if(pObjetoPACCO != null)
                         vDataBase.AddInParameter(vDbCommand, "@ObjetoPACCO", DbType.String, pObjetoPACCO);
                    if(pModalidadPACCO != null)
                         vDataBase.AddInParameter(vDbCommand, "@ModalidadPACCO", DbType.Int32, pModalidadPACCO);
                    if(pValorPresupuestalPACCO != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestalPACCO", DbType.Decimal, pValorPresupuestalPACCO);
                    if(pVigenciaPACCO != null)
                         vDataBase.AddInParameter(vDbCommand, "@VigenciaPACCO", DbType.String, pVigenciaPACCO);
                    if(pConsecutivoEstudioRelacionado != null)
                         vDataBase.AddInParameter(vDbCommand, "@ConsecutivoEstudioRelacionado", DbType.Int32, pConsecutivoEstudioRelacionado);
                    if (pFechaSolicitudInicial != Convert.ToDateTime("0009-01-01"))
                         vDataBase.AddInParameter(vDbCommand, "@FechaSolicitudInicial", DbType.DateTime, pFechaSolicitudInicial);
                    if(pActaCorreoNoRadicado != null)
                         vDataBase.AddInParameter(vDbCommand, "@ActaCorreoNoRadicado", DbType.String, pActaCorreoNoRadicado);
                    if(pNombreAbreviado != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if(pNumeroReproceso != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroReproceso", DbType.String, pNumeroReproceso);
                    if(pObjeto != null)
                         vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if(pCuentaVigenciasFuturasPACCO != null)
                         vDataBase.AddInParameter(vDbCommand, "@CuentaVigenciasFuturasPACCO", DbType.String, pCuentaVigenciasFuturasPACCO);
                    if(pAplicaProcesoSeleccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@AplicaProcesoSeleccion", DbType.String, pAplicaProcesoSeleccion);
                    if(pIdModalidadSeleccion != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    if(pIdTipoEstudio != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoEstudio", DbType.Int32, pIdTipoEstudio);
                    if(pIdComplejidadInterna != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdComplejidadInterna", DbType.Int32, pIdComplejidadInterna);
                    if(pIdComplejidadIndicador != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pIdComplejidadIndicador);
                    if(pIdResponsableES != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if(pIdResponsableEC != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if(pOrdenadorGasto != null)
                         vDataBase.AddInParameter(vDbCommand, "@OrdenadorGasto", DbType.String, pOrdenadorGasto);
                    if(pIdEstadoSolicitud != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdEstadoSolicitud", DbType.Int32, pIdEstadoSolicitud);
                    if(pIdMotivoSolicitud != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdMotivoSolicitud", DbType.Int32, pIdMotivoSolicitud);
                    if(pIdDireccionSolicitante != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdDireccionSolicitante", DbType.Int32, pIdDireccionSolicitante);
                    if(pIdAreaSolicitante != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdAreaSolicitante", DbType.Int32, pIdAreaSolicitante);
                    if(pTipoValor != null)
                         vDataBase.AddInParameter(vDbCommand, "@TipoValor", DbType.String, pTipoValor);
                    if(pValorPresupuestoEstimadoSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestoEstimadoSolicitante", DbType.Decimal, pValorPresupuestoEstimadoSolicitante);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroSolicitudEstudioSectoryCaso> vListaRegistroSolicitudEstudioSectoryCaso = new List<RegistroSolicitudEstudioSectoryCaso>();
                        while (vDataReaderResults.Read())
                        {
                                RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
                            vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso;
                            vRegistroSolicitudEstudioSectoryCaso.AplicaPACCO = vDataReaderResults["AplicaPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AplicaPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.AplicaPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO = vDataReaderResults["ConsecutivoPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO = vDataReaderResults["DireccionsolicitantePACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DireccionsolicitantePACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO = vDataReaderResults["ObjetoPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO = vDataReaderResults["ModalidadPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ModalidadPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO = vDataReaderResults["ValorPresupuestalPACCO"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestalPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO = vDataReaderResults["VigenciaPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado = vDataReaderResults["ConsecutivoEstudioRelacionado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoEstudioRelacionado"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado;
                            vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudInicial"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial;
                            vRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado = vDataReaderResults["ActaCorreoNoRadicado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ActaCorreoNoRadicado"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado;
                            vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado = vDataReaderResults["NombreAbreviado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbreviado"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado;
                            vRegistroSolicitudEstudioSectoryCaso.NumeroReproceso = vDataReaderResults["NumeroReproceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroReproceso"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.NumeroReproceso;
                            vRegistroSolicitudEstudioSectoryCaso.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Objeto"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.Objeto;
                            vRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO = vDataReaderResults["CuentaVigenciasFuturasPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CuentaVigenciasFuturasPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion = vDataReaderResults["AplicaProcesoSeleccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AplicaProcesoSeleccion"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion;
                            vRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion;
                            vRegistroSolicitudEstudioSectoryCaso.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? vDataReaderResults["ModalidadSeleccion"].ToString() : vRegistroSolicitudEstudioSectoryCaso.ModalidadSeleccion;
                            vRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio = vDataReaderResults["IdTipoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstudio"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio;
                            vRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna = vDataReaderResults["IdComplejidadInterna"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadInterna"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna;
                            vRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador;
                            vRegistroSolicitudEstudioSectoryCaso.IdResponsableES = vDataReaderResults["IdResponsableES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsableES"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdResponsableES;
                            vRegistroSolicitudEstudioSectoryCaso.IdResponsableEC = vDataReaderResults["IdResponsableEC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsableEC"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdResponsableEC;
                            vRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto = vDataReaderResults["OrdenadorGasto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["OrdenadorGasto"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto;
                            vRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud;
                            vRegistroSolicitudEstudioSectoryCaso.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? vDataReaderResults["EstadoSolicitud"].ToString() : vRegistroSolicitudEstudioSectoryCaso.EstadoSolicitud;
                            vRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud = vDataReaderResults["IdMotivoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMotivoSolicitud"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud;
                            vRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante = vDataReaderResults["IdDireccionSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionSolicitante"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante;
                            vRegistroSolicitudEstudioSectoryCaso.DireccionSolicitante = vDataReaderResults["DireccionSolicitante"] != DBNull.Value ? vDataReaderResults["DireccionSolicitante"].ToString() : vRegistroSolicitudEstudioSectoryCaso.DireccionSolicitante;
                            vRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante = vDataReaderResults["IdAreaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAreaSolicitante"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante;
                            vRegistroSolicitudEstudioSectoryCaso.TipoValor = vDataReaderResults["TipoValor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoValor"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.TipoValor;
                            vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante = vDataReaderResults["ValorPresupuestoEstimadoSolicitante"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestoEstimadoSolicitante"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante;
                            vRegistroSolicitudEstudioSectoryCaso.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.UsuarioCrea;
                            vRegistroSolicitudEstudioSectoryCaso.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaCrea;
                            vRegistroSolicitudEstudioSectoryCaso.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.UsuarioModifica;
                            vRegistroSolicitudEstudioSectoryCaso.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaModifica;
                                vListaRegistroSolicitudEstudioSectoryCaso.Add(vRegistroSolicitudEstudioSectoryCaso);
                        }
                        return vListaRegistroSolicitudEstudioSectoryCaso;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroSolicitudEstudioSectoryCaso> ConsultaGeneralSeguimiento(long ? pConsecutivoEstudio, string pObjeto, string pNombreAbreviado, long? pConsecutivoPACCO , int? pModalidadSeleccion,string pIsEstado,int? pAnioCierre,int ?pVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ConsultaGeneralSeguimientos_Consultar"))
                {
                    if (pConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int64, pConsecutivoEstudio);
                    if (pObjeto != null)
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pNombreAbreviado != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (pConsecutivoPACCO != null)
                        vDataBase.AddInParameter(vDbCommand, "@ConsecutivoPACCO", DbType.Int64, pConsecutivoPACCO);
                    if (pModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pModalidadSeleccion);
                    if (pIsEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstados", DbType.String, pIsEstado);
                    if (pAnioCierre != null)
                        vDataBase.AddInParameter(vDbCommand, "@AnioCierre", DbType.Int32, pAnioCierre);
                    if (pVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroSolicitudEstudioSectoryCaso> vListaRegistroSolicitudEstudioSectoryCaso = new List<RegistroSolicitudEstudioSectoryCaso>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
                            vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso;
                            vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO = vDataReaderResults["ObjetoPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO;                            
                            vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado = vDataReaderResults["NombreAbreviado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreAbreviado"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado;
                            vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO = vDataReaderResults["ConsecutivoPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? vDataReaderResults["ModalidadSeleccion"].ToString() : vRegistroSolicitudEstudioSectoryCaso.ModalidadSeleccion;
                            vRegistroSolicitudEstudioSectoryCaso.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? vDataReaderResults["EstadoSolicitud"].ToString() : vRegistroSolicitudEstudioSectoryCaso.EstadoSolicitud;
                            vRegistroSolicitudEstudioSectoryCaso.AnioCierre = vDataReaderResults["AnioCierre"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnioCierre"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.AnioCierre;
                            vRegistroSolicitudEstudioSectoryCaso.AnioVigencia = vDataReaderResults["vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["vigencia"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.AnioVigencia;
                          
                            vListaRegistroSolicitudEstudioSectoryCaso.Add(vRegistroSolicitudEstudioSectoryCaso);
                        }
                        return vListaRegistroSolicitudEstudioSectoryCaso;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<int> ConsultarFechaSolicitudInicial()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ListaFechaSolicitudInicial_Consultar"))
                {                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<int> vListaFechaRegistroSolicitud = new List<int>();
                        while (vDataReaderResults.Read())
                        {
                            int FechaRegistroSolicitud = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["FechaSolicitudInicial"].ToString()) : 0;

                            vListaFechaRegistroSolicitud.Add(FechaRegistroSolicitud);
                        }
                        return vListaFechaRegistroSolicitud;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
