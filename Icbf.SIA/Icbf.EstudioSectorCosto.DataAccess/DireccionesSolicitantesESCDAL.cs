using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class DireccionesSolicitantesESCDAL : GeneralDAL
    {
        public DireccionesSolicitantesESCDAL()
        {
        }
        public int InsertarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DireccionesSolicitantesESC_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDireccionesSolicitantes", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.String, pDireccionesSolicitantesESC.DireccionSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDireccionesSolicitantesESC.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pDireccionesSolicitantesESC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDireccionesSolicitantesESC.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDireccionesSolicitantesESC.IdDireccionesSolicitantes = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDireccionesSolicitantes").ToString());
                    GenerarLogAuditoria(pDireccionesSolicitantesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DireccionesSolicitantesESC_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccionesSolicitantes", DbType.Int32, pDireccionesSolicitantesESC.IdDireccionesSolicitantes);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.String, pDireccionesSolicitantesESC.DireccionSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDireccionesSolicitantesESC.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pDireccionesSolicitantesESC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDireccionesSolicitantesESC.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDireccionesSolicitantesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DireccionesSolicitantesESC_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccionesSolicitantes", DbType.Int32, pDireccionesSolicitantesESC.IdDireccionesSolicitantes);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDireccionesSolicitantesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        public DireccionesSolicitantesESC ConsultarDireccionesSolicitantesESC(int pIdDireccionesSolicitantes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DireccionesSolicitantesESC_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccionesSolicitantes", DbType.Int32, pIdDireccionesSolicitantes);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DireccionesSolicitantesESC vDireccionesSolicitantesESC = new DireccionesSolicitantesESC();
                        while (vDataReaderResults.Read())
                        {
                            vDireccionesSolicitantesESC.IdDireccionesSolicitantes = vDataReaderResults["IdDireccionesSolicitantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionesSolicitantes"].ToString()) : vDireccionesSolicitantesESC.IdDireccionesSolicitantes;
                            vDireccionesSolicitantesESC.DireccionSolicitante = vDataReaderResults["DireccionSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionSolicitante"].ToString()) : vDireccionesSolicitantesESC.DireccionSolicitante;
                            vDireccionesSolicitantesESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDireccionesSolicitantesESC.Descripcion;
                            vDireccionesSolicitantesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vDireccionesSolicitantesESC.Estado;
                            vDireccionesSolicitantesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDireccionesSolicitantesESC.UsuarioCrea;
                            vDireccionesSolicitantesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDireccionesSolicitantesESC.FechaCrea;
                            vDireccionesSolicitantesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDireccionesSolicitantesESC.UsuarioModifica;
                            vDireccionesSolicitantesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDireccionesSolicitantesESC.FechaModifica;
                        }
                        return vDireccionesSolicitantesESC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }        

        public List<DireccionesSolicitantesESC> ConsultarDireccionesSolicitantesESCs(String pDireccionSolicitante, String pDescripcion, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DireccionesSolicitantesESCs_Consultar"))
                {
                    if (pDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.String, pDireccionSolicitante);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DireccionesSolicitantesESC> vListaDireccionesSolicitantesESC = new List<DireccionesSolicitantesESC>();
                        while (vDataReaderResults.Read())
                        {
                            DireccionesSolicitantesESC vDireccionesSolicitantesESC = new DireccionesSolicitantesESC();
                            vDireccionesSolicitantesESC.IdDireccionesSolicitantes = vDataReaderResults["IdDireccionesSolicitantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionesSolicitantes"].ToString()) : vDireccionesSolicitantesESC.IdDireccionesSolicitantes;
                            vDireccionesSolicitantesESC.DireccionSolicitante = vDataReaderResults["DireccionSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionSolicitante"].ToString()) : vDireccionesSolicitantesESC.DireccionSolicitante;
                            vDireccionesSolicitantesESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDireccionesSolicitantesESC.Descripcion;
                            vDireccionesSolicitantesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vDireccionesSolicitantesESC.Estado;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vDireccionesSolicitantesESC.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vDireccionesSolicitantesESC.NombreEstado = "INACTIVO";
                                }
                            }
                            vDireccionesSolicitantesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDireccionesSolicitantesESC.UsuarioCrea;
                            vDireccionesSolicitantesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDireccionesSolicitantesESC.FechaCrea;
                            vDireccionesSolicitantesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDireccionesSolicitantesESC.UsuarioModifica;
                            vDireccionesSolicitantesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDireccionesSolicitantesESC.FechaModifica;
                            vListaDireccionesSolicitantesESC.Add(vDireccionesSolicitantesESC);
                        }
                        return vListaDireccionesSolicitantesESC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarDireccionesSolicitantesESC(String pDireccionSolicitante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DireccionesSolicitantesESC_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitantes", DbType.String, pDireccionSolicitante);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DireccionesSolicitantesESC vDireccionesSolicitantesESC = new DireccionesSolicitantesESC();
                        while (vDataReaderResults.Read())
                        {
                            vDireccionesSolicitantesESC.IdDireccionesSolicitantes = vDataReaderResults["IdDireccionesSolicitantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionesSolicitantes"].ToString()) : vDireccionesSolicitantesESC.IdDireccionesSolicitantes;
                            vDireccionesSolicitantesESC.DireccionSolicitante = vDataReaderResults["DireccionSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionSolicitante"].ToString()) : vDireccionesSolicitantesESC.DireccionSolicitante;
                            vDireccionesSolicitantesESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDireccionesSolicitantesESC.Descripcion;
                            vDireccionesSolicitantesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vDireccionesSolicitantesESC.Estado;
                            vDireccionesSolicitantesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDireccionesSolicitantesESC.UsuarioCrea;
                            vDireccionesSolicitantesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDireccionesSolicitantesESC.FechaCrea;
                            vDireccionesSolicitantesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDireccionesSolicitantesESC.UsuarioModifica;
                            vDireccionesSolicitantesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDireccionesSolicitantesESC.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
