﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlTypes;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class InformacionProcesoSeleccionDAL : GeneralDAL
    {
        public InformacionProcesoSeleccionDAL()
        {
        }

        public int InsertarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_InformacionProcesoSeleccion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdInformacionProcesoSeleccion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int32, pInformacionProcesoSeleccion.IdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pInformacionProcesoSeleccion.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDelProceso", DbType.String, pInformacionProcesoSeleccion.NumeroDelProceso);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoContrato", DbType.String, pInformacionProcesoSeleccion.ObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoDelProceso", DbType.String, pInformacionProcesoSeleccion.EstadoDelProceso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicacion", DbType.DateTime, pInformacionProcesoSeleccion.FechaAdjudicacionDelProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Contratista", DbType.String, pInformacionProcesoSeleccion.Contratista);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestadoInicial", DbType.Decimal, pInformacionProcesoSeleccion.ValorPresupuestadoInicial);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdjudicado", DbType.Decimal, pInformacionProcesoSeleccion.ValorAdjudicado);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoDeEjecucion", DbType.String, pInformacionProcesoSeleccion.PlazoDeEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@Observacion", DbType.String, pInformacionProcesoSeleccion.Observacion);
                    vDataBase.AddInParameter(vDbCommand, "@URLDelProceso", DbType.String, pInformacionProcesoSeleccion.URLDelProceso);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInformacionProcesoSeleccion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pInformacionProcesoSeleccion.IdInformacionProcesoSeleccion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdInformacionProcesoSeleccion").ToString());
                    GenerarLogAuditoria(pInformacionProcesoSeleccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_InformacionProcesoSeleccion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionProcesoSeleccion", DbType.Int32, pInformacionProcesoSeleccion.IdInformacionProcesoSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int32, pInformacionProcesoSeleccion.IdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pInformacionProcesoSeleccion.IdContrato);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDelProceso", DbType.String, pInformacionProcesoSeleccion.NumeroDelProceso);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoContrato", DbType.String, pInformacionProcesoSeleccion.ObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoDelProceso", DbType.String, pInformacionProcesoSeleccion.EstadoDelProceso);
                    vDataBase.AddInParameter(vDbCommand, "@FechaAdjudicacion", DbType.DateTime, pInformacionProcesoSeleccion.FechaAdjudicacionDelProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Contratista", DbType.String, pInformacionProcesoSeleccion.Contratista);
                    vDataBase.AddInParameter(vDbCommand, "@ValorPresupuestadoInicial", DbType.Decimal, pInformacionProcesoSeleccion.ValorPresupuestadoInicial);
                    vDataBase.AddInParameter(vDbCommand, "@ValorAdjudicado", DbType.Decimal, pInformacionProcesoSeleccion.ValorAdjudicado);
                    vDataBase.AddInParameter(vDbCommand, "@PlazoDeEjecucion", DbType.String, pInformacionProcesoSeleccion.PlazoDeEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@Observacion", DbType.String, pInformacionProcesoSeleccion.Observacion);          
                    vDataBase.AddInParameter(vDbCommand, "@URLDelProceso", DbType.String, pInformacionProcesoSeleccion.URLDelProceso);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInformacionProcesoSeleccion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInformacionProcesoSeleccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_InformacionProcesoSeleccion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionProcesoSeleccion", DbType.Int32, pInformacionProcesoSeleccion.IdInformacionProcesoSeleccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInformacionProcesoSeleccion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public InformacionProcesoSeleccion ConsultarInformacionProcesoSeleccion(Int32 pIdInformacionProcesoSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_InformacionProcesoSeleccion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdInformacionProcesoSeleccion", DbType.Int32, pIdInformacionProcesoSeleccion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        InformacionProcesoSeleccion vInformacionProcesoSeleccion = new InformacionProcesoSeleccion();
                        while (vDataReaderResults.Read())
                        {
                            vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion = vDataReaderResults["IdInformacionProcesoSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionProcesoSeleccion"].ToString()) : vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion;
                            vInformacionProcesoSeleccion.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vInformacionProcesoSeleccion.IdConsecutivoEstudio;
                            vInformacionProcesoSeleccion.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vInformacionProcesoSeleccion.IdContrato;
                            vInformacionProcesoSeleccion.NumeroDelProceso = vDataReaderResults["NumeroDelProceso"] != DBNull.Value ? vDataReaderResults["NumeroDelProceso"].ToString() : vInformacionProcesoSeleccion.NumeroDelProceso;
                            vInformacionProcesoSeleccion.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : vInformacionProcesoSeleccion.ObjetoContrato;
                            vInformacionProcesoSeleccion.EstadoDelProceso = vDataReaderResults["EstadoDelProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoDelProceso"].ToString()) : vInformacionProcesoSeleccion.EstadoDelProceso;
                            vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso;
                            vInformacionProcesoSeleccion.Contratista = vDataReaderResults["Contratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Contratista"].ToString()) : vInformacionProcesoSeleccion.Contratista;
                            vInformacionProcesoSeleccion.ValorPresupuestadoInicial = vDataReaderResults["ValorPresupuestadoInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestadoInicial"].ToString()) : vInformacionProcesoSeleccion.ValorPresupuestadoInicial;
                            vInformacionProcesoSeleccion.ValorAdjudicado = vDataReaderResults["ValorAdjudicado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdjudicado"].ToString()) : vInformacionProcesoSeleccion.ValorAdjudicado;
                            vInformacionProcesoSeleccion.PlazoDeEjecucion = vDataReaderResults["PlazoDeEjecucion"] != DBNull.Value ? vDataReaderResults["PlazoDeEjecucion"].ToString() : vInformacionProcesoSeleccion.PlazoDeEjecucion;
                            vInformacionProcesoSeleccion.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observacion"].ToString()) : vInformacionProcesoSeleccion.Observacion;
                            vInformacionProcesoSeleccion.URLDelProceso = vDataReaderResults["URLDelProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["URLDelProceso"].ToString()) : vInformacionProcesoSeleccion.URLDelProceso;                           
                            vInformacionProcesoSeleccion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInformacionProcesoSeleccion.UsuarioCrea;
                            vInformacionProcesoSeleccion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInformacionProcesoSeleccion.FechaCrea;
                            vInformacionProcesoSeleccion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInformacionProcesoSeleccion.UsuarioModifica;
                            vInformacionProcesoSeleccion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInformacionProcesoSeleccion.FechaModifica;
                        }
                        return vInformacionProcesoSeleccion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccions(Int32? pConsecutivoEstudio, String pNombreAbreviado, String pObjeto, int? pIdModalidadSeleccion, int? pIdResponsableES, int? pIdResponsableEC, int? pIdEstadoSolicitud, int? pIdDireccionSolicitante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_InformacionProcesoSeleccions_Consultar"))
                {
                    if (pConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int32, pConsecutivoEstudio);                    
                    if (pNombreAbreviado != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (pObjeto != null)
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdEstadoSolicitud != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pIdEstadoSolicitud);
                    if (pIdDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pIdDireccionSolicitante);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InformacionProcesoSeleccion> vListaProcesoSeleccionPorContrato = new List<InformacionProcesoSeleccion>();
                        while (vDataReaderResults.Read())
                        {
                            InformacionProcesoSeleccion vInformacionProcesoSeleccion = new InformacionProcesoSeleccion();
                            vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion = vDataReaderResults["IdInformacionProcesoSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionProcesoSeleccion"].ToString()) : vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion;
                            vInformacionProcesoSeleccion.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vInformacionProcesoSeleccion.IdConsecutivoEstudio;
                            vInformacionProcesoSeleccion.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vInformacionProcesoSeleccion.IdContrato;
                            vInformacionProcesoSeleccion.NumeroDelProceso = vDataReaderResults["NumeroDelProceso"] != DBNull.Value ? vDataReaderResults["NumeroDelProceso"].ToString() : vInformacionProcesoSeleccion.NumeroDelProceso;
                            vInformacionProcesoSeleccion.ObjetoContrato = vDataReaderResults["Objeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Objeto"].ToString()) : vInformacionProcesoSeleccion.ObjetoContrato;
                            vInformacionProcesoSeleccion.EstadoDelProceso = vDataReaderResults["EstadoDelProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoDelProceso"].ToString()) : vInformacionProcesoSeleccion.EstadoDelProceso;
                            vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso;
                            vInformacionProcesoSeleccion.Contratista = vDataReaderResults["Contratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Contratista"].ToString()) : vInformacionProcesoSeleccion.Contratista;
                            vInformacionProcesoSeleccion.ValorPresupuestadoInicial = vDataReaderResults["ValorPresupuestadoInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestadoInicial"].ToString()) : vInformacionProcesoSeleccion.ValorPresupuestadoInicial;
                            vInformacionProcesoSeleccion.ValorAdjudicado = vDataReaderResults["ValorAdjudicado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdjudicado"].ToString()) : vInformacionProcesoSeleccion.ValorAdjudicado;
                            vInformacionProcesoSeleccion.PlazoDeEjecucion = vDataReaderResults["PlazoDeEjecucion"] != DBNull.Value ? vDataReaderResults["PlazoDeEjecucion"].ToString() : vInformacionProcesoSeleccion.PlazoDeEjecucion;
                            vInformacionProcesoSeleccion.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observacion"].ToString()) : vInformacionProcesoSeleccion.Observacion;
                            vInformacionProcesoSeleccion.URLDelProceso = vDataReaderResults["URLDelProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["URLDelProceso"].ToString()) : vInformacionProcesoSeleccion.URLDelProceso;
                            vListaProcesoSeleccionPorContrato.Add(vInformacionProcesoSeleccion);
                        }
                        return vListaProcesoSeleccionPorContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<InformacionProcesoSeleccion> ConsultarContratoESC(int? pConsecutivo, string pNumeroProceso, int? pVigenciaProceso, string pObjetoContrato, int? pProcesoSeleccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Contratos_Consultar"))
                {
                    if (pNumeroProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroProceso", DbType.String, pNumeroProceso);
                    if (pVigenciaProceso != null)
                        vDataBase.AddInParameter(vDbCommand, "@VigenciaProceso", DbType.Int32, pVigenciaProceso);
                    if (pObjetoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@ObjetoContrato", DbType.String, pObjetoContrato);
                    if (pProcesoSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdInformacionProcesoSeleccion", DbType.Int32, pProcesoSeleccion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InformacionProcesoSeleccion> vListaProcesoSeleccionPorContrato = new List<InformacionProcesoSeleccion>();
                        while (vDataReaderResults.Read())
                        {
                            InformacionProcesoSeleccion vProcesoSeleccionPorContrato = new InformacionProcesoSeleccion();
                            if (pConsecutivo != null) vProcesoSeleccionPorContrato.IdConsecutivoEstudio = Convert.ToInt32(pConsecutivo);
                            vProcesoSeleccionPorContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vProcesoSeleccionPorContrato.IdContrato;
                            vProcesoSeleccionPorContrato.NumeroDelProceso = vDataReaderResults["NumeroProceso"] != DBNull.Value ? vDataReaderResults["NumeroProceso"].ToString() : vProcesoSeleccionPorContrato.NumeroDelProceso;
                            vProcesoSeleccionPorContrato.ObjetoContrato = vDataReaderResults["ObjetoDelContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoDelContrato"].ToString()) : vProcesoSeleccionPorContrato.ObjetoContrato;
                            vProcesoSeleccionPorContrato.FechaAdjudicacionDelProceso = vDataReaderResults["FechaAdjudicacionDelProceso"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacionDelProceso"].ToString()) : vProcesoSeleccionPorContrato.FechaAdjudicacionDelProceso;
                            vProcesoSeleccionPorContrato.Contratista = vDataReaderResults["Contratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Contratista"].ToString()) : vProcesoSeleccionPorContrato.Contratista;
                            vProcesoSeleccionPorContrato.ValorPresupuestadoInicial = vDataReaderResults["Valorpresupuestadoinicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Valorpresupuestadoinicial"].ToString()) : vProcesoSeleccionPorContrato.ValorPresupuestadoInicial;
                            vProcesoSeleccionPorContrato.ValorAdjudicado = vDataReaderResults["ValorAdjudicado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdjudicado"].ToString()) : vProcesoSeleccionPorContrato.ValorAdjudicado;
                            vProcesoSeleccionPorContrato.PlazoDeEjecucion = vDataReaderResults["PlazoDeEjecucion"] != DBNull.Value ? vDataReaderResults["PlazoDeEjecucion"].ToString() : vProcesoSeleccionPorContrato.PlazoDeEjecucion;
                            vProcesoSeleccionPorContrato.ValorAdjudicado = vDataReaderResults["ValorAdjudicado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdjudicado"].ToString()) : vProcesoSeleccionPorContrato.ValorAdjudicado;
                            vProcesoSeleccionPorContrato.ValorPresupuestadoInicial = vDataReaderResults["ValorPresupuestadoInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestadoInicial"].ToString()) : vProcesoSeleccionPorContrato.ValorPresupuestadoInicial;
                            vProcesoSeleccionPorContrato.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? (vDataReaderResults["Observacion"].ToString()) : vProcesoSeleccionPorContrato.Observacion;
                            vProcesoSeleccionPorContrato.URLDelProceso = vDataReaderResults["URLDelProceso"] != DBNull.Value ? (vDataReaderResults["URLDelProceso"].ToString()) : vProcesoSeleccionPorContrato.URLDelProceso;

                            vListaProcesoSeleccionPorContrato.Add(vProcesoSeleccionPorContrato);
                        }
                        return vListaProcesoSeleccionPorContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
