using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class AreasSolicitantesESCDAL : GeneralDAL
    {
        public AreasSolicitantesESCDAL()
        {
        }
        public int InsertarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_AreasSolicitantesESC_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAreasSolicitantes", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@AreaSolicitante", DbType.String, pAreasSolicitantesESC.AreaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccionSolicitanteESC", DbType.Int32, pAreasSolicitantesESC.IdDireccionSolicitanteESC);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pAreasSolicitantesESC.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pAreasSolicitantesESC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAreasSolicitantesESC.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAreasSolicitantesESC.IdAreasSolicitantes = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAreasSolicitantes").ToString());
                    GenerarLogAuditoria(pAreasSolicitantesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_AreasSolicitantesESC_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAreasSolicitantes", DbType.Int32, pAreasSolicitantesESC.IdAreasSolicitantes);
                    vDataBase.AddInParameter(vDbCommand, "@AreaSolicitante", DbType.String, pAreasSolicitantesESC.AreaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccionSolicitanteESC", DbType.Int32, pAreasSolicitantesESC.IdDireccionSolicitanteESC);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pAreasSolicitantesESC.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pAreasSolicitantesESC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAreasSolicitantesESC.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAreasSolicitantesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_AreasSolicitantesESC_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAreasSolicitantes", DbType.Int32, pAreasSolicitantesESC.IdAreasSolicitantes);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAreasSolicitantesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public AreasSolicitantesESC ConsultarAreasSolicitantesESC(int pIdAreasSolicitantes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_AreasSolicitantesESC_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAreasSolicitantes", DbType.Int32, pIdAreasSolicitantes);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AreasSolicitantesESC vAreasSolicitantesESC = new AreasSolicitantesESC();
                        while (vDataReaderResults.Read())
                        {
                            vAreasSolicitantesESC.IdAreasSolicitantes = vDataReaderResults["IdAreasSolicitantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAreasSolicitantes"].ToString()) : vAreasSolicitantesESC.IdAreasSolicitantes;
                            vAreasSolicitantesESC.AreaSolicitante = vDataReaderResults["AreaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AreaSolicitante"].ToString()) : vAreasSolicitantesESC.AreaSolicitante;
                            vAreasSolicitantesESC.IdDireccionSolicitanteESC = vDataReaderResults["IdDireccionSolicitanteESC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionSolicitanteESC"].ToString()) : vAreasSolicitantesESC.IdDireccionSolicitanteESC;
                            vAreasSolicitantesESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vAreasSolicitantesESC.Descripcion;
                            vAreasSolicitantesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vAreasSolicitantesESC.Estado;
                            vAreasSolicitantesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAreasSolicitantesESC.UsuarioCrea;
                            vAreasSolicitantesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAreasSolicitantesESC.FechaCrea;
                            vAreasSolicitantesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAreasSolicitantesESC.UsuarioModifica;
                            vAreasSolicitantesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAreasSolicitantesESC.FechaModifica;
                        }
                        return vAreasSolicitantesESC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<AreasSolicitantesESC> ConsultarAreasSolicitantesESCs(String pAreaSolicitante, int? pIdDireccionSolicitanteESC, String pDescripcion, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_AreasSolicitantesESCs_Consultar"))
                {
                    if (pAreaSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@AreaSolicitante", DbType.String, pAreaSolicitante);
                    if (pIdDireccionSolicitanteESC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDireccionSolicitanteESC", DbType.Int32, pIdDireccionSolicitanteESC);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AreasSolicitantesESC> vListaAreasSolicitantesESC = new List<AreasSolicitantesESC>();
                        while (vDataReaderResults.Read())
                        {
                            AreasSolicitantesESC vAreasSolicitantesESC = new AreasSolicitantesESC();
                            vAreasSolicitantesESC.IdAreasSolicitantes = vDataReaderResults["IdAreasSolicitantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAreasSolicitantes"].ToString()) : vAreasSolicitantesESC.IdAreasSolicitantes;
                            vAreasSolicitantesESC.AreaSolicitante = vDataReaderResults["AreaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AreaSolicitante"].ToString()) : vAreasSolicitantesESC.AreaSolicitante;
                            vAreasSolicitantesESC.IdDireccionSolicitanteESC = vDataReaderResults["IdDireccionSolicitanteESC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionSolicitanteESC"].ToString()) : vAreasSolicitantesESC.IdDireccionSolicitanteESC;
                            vAreasSolicitantesESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vAreasSolicitantesESC.Descripcion;
                            vAreasSolicitantesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vAreasSolicitantesESC.Estado;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vAreasSolicitantesESC.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vAreasSolicitantesESC.NombreEstado = "INACTIVO";
                                }
                            }
                            vAreasSolicitantesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAreasSolicitantesESC.UsuarioCrea;
                            vAreasSolicitantesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAreasSolicitantesESC.FechaCrea;
                            vAreasSolicitantesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAreasSolicitantesESC.UsuarioModifica;
                            vAreasSolicitantesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAreasSolicitantesESC.FechaModifica;
                            vAreasSolicitantesESC.DireccionSolicitante = vDataReaderResults["DireccionSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionSolicitante"].ToString()) : vAreasSolicitantesESC.DireccionSolicitante;

                            vListaAreasSolicitantesESC.Add(vAreasSolicitantesESC);
                        }
                        return vListaAreasSolicitantesESC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarAreasSolicitantesESC(String pAreaSolicitante, int pIdDireccionSolicitanteESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_AreasSolicitantesESC__Validar_Duplicados"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@AreaSolicitante", DbType.String, pAreaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccionSolicitanteESC", DbType.Int32, pIdDireccionSolicitanteESC);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AreasSolicitantesESC vAreasSolicitantesESC = new AreasSolicitantesESC();
                        while (vDataReaderResults.Read())
                        {
                            vAreasSolicitantesESC.IdAreasSolicitantes = vDataReaderResults["IdAreasSolicitantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAreasSolicitantes"].ToString()) : vAreasSolicitantesESC.IdAreasSolicitantes;
                            vAreasSolicitantesESC.AreaSolicitante = vDataReaderResults["AreaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AreaSolicitante"].ToString()) : vAreasSolicitantesESC.AreaSolicitante;
                            vAreasSolicitantesESC.IdDireccionSolicitanteESC = vDataReaderResults["IdDireccionSolicitanteESC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionSolicitanteESC"].ToString()) : vAreasSolicitantesESC.IdDireccionSolicitanteESC;
                            vAreasSolicitantesESC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vAreasSolicitantesESC.Descripcion;
                            vAreasSolicitantesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vAreasSolicitantesESC.Estado;
                            vAreasSolicitantesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAreasSolicitantesESC.UsuarioCrea;
                            vAreasSolicitantesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAreasSolicitantesESC.FechaCrea;
                            vAreasSolicitantesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAreasSolicitantesESC.UsuarioModifica;
                            vAreasSolicitantesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAreasSolicitantesESC.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
