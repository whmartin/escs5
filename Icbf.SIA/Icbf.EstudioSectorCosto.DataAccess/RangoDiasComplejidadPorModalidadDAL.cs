using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class RangoDiasComplejidadPorModalidadDAL : GeneralDAL
    {
        public RangoDiasComplejidadPorModalidadDAL()
        {
        }
        public int InsertarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RangoDiasComplejidadPorModalidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRangosDiasComplejidadPorModalidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pRangoDiasComplejidadPorModalidad.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@Operador", DbType.String, pRangoDiasComplejidadPorModalidad.Operador);
                    vDataBase.AddInParameter(vDbCommand, "@Limite", DbType.Int32, pRangoDiasComplejidadPorModalidad.Limite);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadInterna", DbType.Int32, pRangoDiasComplejidadPorModalidad.IdComplejidadInterna);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pRangoDiasComplejidadPorModalidad.IdComplejidadIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesInternos", DbType.Int32, pRangoDiasComplejidadPorModalidad.DiasHabilesInternos);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesCumplimientoIndicador", DbType.Int32, pRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pRangoDiasComplejidadPorModalidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRangoDiasComplejidadPorModalidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRangosDiasComplejidadPorModalidad").ToString());
                    GenerarLogAuditoria(pRangoDiasComplejidadPorModalidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RangoDiasComplejidadPorModalidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRangosDiasComplejidadPorModalidad", DbType.Int32, pRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pRangoDiasComplejidadPorModalidad.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@Operador", DbType.String, pRangoDiasComplejidadPorModalidad.Operador);
                    vDataBase.AddInParameter(vDbCommand, "@Limite", DbType.Int32, pRangoDiasComplejidadPorModalidad.Limite);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadInterna", DbType.Int32, pRangoDiasComplejidadPorModalidad.IdComplejidadInterna);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pRangoDiasComplejidadPorModalidad.IdComplejidadIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesInternos", DbType.Int32, pRangoDiasComplejidadPorModalidad.DiasHabilesInternos);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesCumplimientoIndicador", DbType.Int32, pRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pRangoDiasComplejidadPorModalidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRangoDiasComplejidadPorModalidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRangoDiasComplejidadPorModalidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RangoDiasComplejidadPorModalidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRangosDiasComplejidadPorModalidad", DbType.Int32, pRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRangoDiasComplejidadPorModalidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public RangoDiasComplejidadPorModalidad ConsultarRangoDiasComplejidadPorModalidad(int pIdRangosDiasComplejidadPorModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RangoDiasComplejidadPorModalidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRangosDiasComplejidadPorModalidad", DbType.Int32, pIdRangosDiasComplejidadPorModalidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RangoDiasComplejidadPorModalidad vRangoDiasComplejidadPorModalidad = new RangoDiasComplejidadPorModalidad();
                        while (vDataReaderResults.Read())
                        {
                            vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad = vDataReaderResults["IdRangosDiasComplejidadPorModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRangosDiasComplejidadPorModalidad"].ToString()) : vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad;
                            vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion;
                            vRangoDiasComplejidadPorModalidad.Operador = vDataReaderResults["Operador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Operador"].ToString()) : vRangoDiasComplejidadPorModalidad.Operador;
                            vRangoDiasComplejidadPorModalidad.Limite = vDataReaderResults["Limite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Limite"].ToString()) : vRangoDiasComplejidadPorModalidad.Limite;
                            vRangoDiasComplejidadPorModalidad.LimiteGrilla = vDataReaderResults["Limite"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Operador"].ToString() + Convert.ToInt32(vDataReaderResults["Limite"].ToString())) : Convert.ToString(vRangoDiasComplejidadPorModalidad.Operador + vRangoDiasComplejidadPorModalidad.Limite);
                            vRangoDiasComplejidadPorModalidad.IdComplejidadInterna = vDataReaderResults["IdComplejidadInterna"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadInterna"].ToString()) : vRangoDiasComplejidadPorModalidad.IdComplejidadInterna;
                            vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador;
                            vRangoDiasComplejidadPorModalidad.DiasHabilesInternos = vDataReaderResults["DiasHabilesInternos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesInternos"].ToString()) : vRangoDiasComplejidadPorModalidad.DiasHabilesInternos;
                            vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador = vDataReaderResults["DiasHabilesCumplimientoIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesCumplimientoIndicador"].ToString()) : vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador;
                            vRangoDiasComplejidadPorModalidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vRangoDiasComplejidadPorModalidad.Estado;
                            vRangoDiasComplejidadPorModalidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRangoDiasComplejidadPorModalidad.UsuarioCrea;
                            vRangoDiasComplejidadPorModalidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRangoDiasComplejidadPorModalidad.FechaCrea;
                            vRangoDiasComplejidadPorModalidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRangoDiasComplejidadPorModalidad.UsuarioModifica;
                            vRangoDiasComplejidadPorModalidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRangoDiasComplejidadPorModalidad.FechaModifica;
                        }
                        return vRangoDiasComplejidadPorModalidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RangoDiasComplejidadPorModalidad> ConsultarRangoDiasComplejidadPorModalidads(int? pIdModalidadSeleccion, String pOperador, int? pLimite, int? pIdComplejidadInterna, int? pIdComplejidadIndicador, int? pDiasHabilesInternos, int? pDiasHabilesCumplimientoIndicador, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RangoDiasComplejidadPorModalidads_Consultar"))
                {
                    if (pIdModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    if (pOperador != null)
                        vDataBase.AddInParameter(vDbCommand, "@Operador", DbType.String, pOperador);
                    if (pLimite != null)
                        vDataBase.AddInParameter(vDbCommand, "@Limite", DbType.Int32, pLimite);
                    if (pIdComplejidadInterna != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdComplejidadInterna", DbType.Int32, pIdComplejidadInterna);
                    if (pIdComplejidadIndicador != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pIdComplejidadIndicador);
                    if (pDiasHabilesInternos != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesInternos", DbType.Int32, pDiasHabilesInternos);
                    if (pDiasHabilesCumplimientoIndicador != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesCumplimientoIndicador", DbType.Int32, pDiasHabilesCumplimientoIndicador);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RangoDiasComplejidadPorModalidad> vListaRangoDiasComplejidadPorModalidad = new List<RangoDiasComplejidadPorModalidad>();
                        while (vDataReaderResults.Read())
                        {
                            RangoDiasComplejidadPorModalidad vRangoDiasComplejidadPorModalidad = new RangoDiasComplejidadPorModalidad();
                            vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad = vDataReaderResults["IdRangosDiasComplejidadPorModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRangosDiasComplejidadPorModalidad"].ToString()) : vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad;
                            vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion;
                            vRangoDiasComplejidadPorModalidad.Operador = vDataReaderResults["Operador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Operador"].ToString()) : vRangoDiasComplejidadPorModalidad.Operador;
                            vRangoDiasComplejidadPorModalidad.Limite = vDataReaderResults["Limite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Limite"].ToString()) : vRangoDiasComplejidadPorModalidad.Limite;
                            if (Convert.ToString(vDataReaderResults["Operador"].ToString()) != "0")
                            {
                                vRangoDiasComplejidadPorModalidad.LimiteGrilla = Convert.ToString(vRangoDiasComplejidadPorModalidad.Operador + vRangoDiasComplejidadPorModalidad.Limite);
                            }
                            else
                            {                                       
                                vRangoDiasComplejidadPorModalidad.LimiteGrilla = Convert.ToString(vRangoDiasComplejidadPorModalidad.Limite);
                            }                            
                            vRangoDiasComplejidadPorModalidad.IdComplejidadInterna = vDataReaderResults["IdComplejidadInterna"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadInterna"].ToString()) : vRangoDiasComplejidadPorModalidad.IdComplejidadInterna;
                            vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador;
                            vRangoDiasComplejidadPorModalidad.DiasHabilesInternos = vDataReaderResults["DiasHabilesInternos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesInternos"].ToString()) : vRangoDiasComplejidadPorModalidad.DiasHabilesInternos;
                            vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador = vDataReaderResults["DiasHabilesCumplimientoIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesCumplimientoIndicador"].ToString()) : vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador;
                            vRangoDiasComplejidadPorModalidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vRangoDiasComplejidadPorModalidad.Estado;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vRangoDiasComplejidadPorModalidad.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vRangoDiasComplejidadPorModalidad.NombreEstado = "INACTIVO";
                                }
                            }
                            vRangoDiasComplejidadPorModalidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRangoDiasComplejidadPorModalidad.UsuarioCrea;
                            vRangoDiasComplejidadPorModalidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRangoDiasComplejidadPorModalidad.FechaCrea;
                            vRangoDiasComplejidadPorModalidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRangoDiasComplejidadPorModalidad.UsuarioModifica;
                            vRangoDiasComplejidadPorModalidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRangoDiasComplejidadPorModalidad.FechaModifica;
                            vRangoDiasComplejidadPorModalidad.NombreComplejidadInterna = vDataReaderResults["NombreComplejidadInterna"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreComplejidadInterna"].ToString()) : vRangoDiasComplejidadPorModalidad.NombreComplejidadInterna;
                            vRangoDiasComplejidadPorModalidad.NombreComplejidadIndicador = vDataReaderResults["NombreComplejidadIndicador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreComplejidadIndicador"].ToString()) : vRangoDiasComplejidadPorModalidad.NombreComplejidadIndicador;
                            vRangoDiasComplejidadPorModalidad.Modalidad = vDataReaderResults["Modalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Modalidad"].ToString()) : vRangoDiasComplejidadPorModalidad.Modalidad;
                            vListaRangoDiasComplejidadPorModalidad.Add(vRangoDiasComplejidadPorModalidad);
                        }
                        return vListaRangoDiasComplejidadPorModalidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarRangoDiasComplejidadPorModalidad(int pIdModalidadSeleccion
                                                                , String pOperador
                                                                , int? pLimite
                                                                , int? pIdComplejidadInterna
                                                                , int? pIdComplejidadIndicador
                                                                , int? pDiasHabilesInternos
                                                                , int? pDiasHabilesCumplimientoIndicador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RangoDiasComplejidadPorModalidad_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@Operador", DbType.String, pOperador);
                    vDataBase.AddInParameter(vDbCommand, "@Limite", DbType.Int32, pLimite);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadInterna", DbType.Int32, pIdComplejidadInterna);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pIdComplejidadIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesInternos", DbType.Int32, pDiasHabilesInternos);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesCumplimientoIndicador", DbType.Int32, pDiasHabilesCumplimientoIndicador);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RangoDiasComplejidadPorModalidad vRangoDiasComplejidadPorModalidad = new RangoDiasComplejidadPorModalidad();
                        while (vDataReaderResults.Read())
                        {
                            vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad = vDataReaderResults["IdRangosDiasComplejidadPorModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRangosDiasComplejidadPorModalidad"].ToString()) : vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad;
                            vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion;
                            vRangoDiasComplejidadPorModalidad.Operador = vDataReaderResults["Operador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Operador"].ToString()) : vRangoDiasComplejidadPorModalidad.Operador;
                            vRangoDiasComplejidadPorModalidad.Limite = vDataReaderResults["Limite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Limite"].ToString()) : vRangoDiasComplejidadPorModalidad.Limite;
                            vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador;
                            vRangoDiasComplejidadPorModalidad.DiasHabilesInternos = vDataReaderResults["DiasHabilesInternos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesInternos"].ToString()) : vRangoDiasComplejidadPorModalidad.DiasHabilesInternos;
                            vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador = vDataReaderResults["DiasHabilesCumplimientoIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesCumplimientoIndicador"].ToString()) : vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador;
                            vRangoDiasComplejidadPorModalidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vRangoDiasComplejidadPorModalidad.Estado;
                            vRangoDiasComplejidadPorModalidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRangoDiasComplejidadPorModalidad.UsuarioCrea;
                            vRangoDiasComplejidadPorModalidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRangoDiasComplejidadPorModalidad.FechaCrea;
                            vRangoDiasComplejidadPorModalidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRangoDiasComplejidadPorModalidad.UsuarioModifica;
                            vRangoDiasComplejidadPorModalidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRangoDiasComplejidadPorModalidad.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
