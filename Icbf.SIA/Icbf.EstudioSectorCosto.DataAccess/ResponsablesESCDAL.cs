using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class ResponsablesESCDAL : GeneralDAL
    {
        public ResponsablesESCDAL()
        {
        }
        public int InsertarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResponsablesESC_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdResponsable", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Responsable", DbType.String, pResponsablesESC.Responsable);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pResponsablesESC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pResponsablesESC.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pResponsablesESC.IdResponsable = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdResponsable").ToString());
                    GenerarLogAuditoria(pResponsablesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResponsablesESC_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsable", DbType.Int32, pResponsablesESC.IdResponsable);
                    vDataBase.AddInParameter(vDbCommand, "@Responsable", DbType.String, pResponsablesESC.Responsable);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pResponsablesESC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pResponsablesESC.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pResponsablesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResponsablesESC_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsable", DbType.Int32, pResponsablesESC.IdResponsable);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pResponsablesESC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public ResponsablesESC ConsultarResponsablesESC(int pIdResponsable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResponsablesESC_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdResponsable", DbType.Int32, pIdResponsable);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ResponsablesESC vResponsablesESC = new ResponsablesESC();
                        while (vDataReaderResults.Read())
                        {
                            vResponsablesESC.IdResponsable = vDataReaderResults["IdResponsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsable"].ToString()) : vResponsablesESC.IdResponsable;
                            vResponsablesESC.Responsable = vDataReaderResults["Responsable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Responsable"].ToString()) : vResponsablesESC.Responsable;
                            vResponsablesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vResponsablesESC.Estado;
                            vResponsablesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vResponsablesESC.UsuarioCrea;
                            vResponsablesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vResponsablesESC.FechaCrea;
                            vResponsablesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vResponsablesESC.UsuarioModifica;
                            vResponsablesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vResponsablesESC.FechaModifica;
                        }
                        return vResponsablesESC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ResponsablesESC> ConsultarResponsablesESCs(String pResponsable, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResponsablesESCs_Consultar"))
                {
                    if (pResponsable != null)
                        vDataBase.AddInParameter(vDbCommand, "@Responsable", DbType.String, pResponsable);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResponsablesESC> vListaResponsablesESC = new List<ResponsablesESC>();
                        while (vDataReaderResults.Read())
                        {
                            ResponsablesESC vResponsablesESC = new ResponsablesESC();
                            vResponsablesESC.IdResponsable = vDataReaderResults["IdResponsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsable"].ToString()) : vResponsablesESC.IdResponsable;
                            vResponsablesESC.Responsable = vDataReaderResults["Responsable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Responsable"].ToString()) : vResponsablesESC.Responsable;
                            vResponsablesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vResponsablesESC.Estado;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vResponsablesESC.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vResponsablesESC.NombreEstado = "INACTIVO";
                                }
                            }
                            vResponsablesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vResponsablesESC.UsuarioCrea;
                            vResponsablesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vResponsablesESC.FechaCrea;
                            vResponsablesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vResponsablesESC.UsuarioModifica;
                            vResponsablesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vResponsablesESC.FechaModifica;
                            vListaResponsablesESC.Add(vResponsablesESC);
                        }
                        return vListaResponsablesESC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarResponsablesESC(String pResponsable)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ResponsablesESC_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Responsable", DbType.String, pResponsable);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ResponsablesESC vResponsablesESC = new ResponsablesESC();
                        while (vDataReaderResults.Read())
                        {
                            vResponsablesESC.IdResponsable = vDataReaderResults["IdResponsable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsable"].ToString()) : vResponsablesESC.IdResponsable;
                            vResponsablesESC.Responsable = vDataReaderResults["Responsable"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Responsable"].ToString()) : vResponsablesESC.Responsable;
                            vResponsablesESC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vResponsablesESC.Estado;
                            vResponsablesESC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vResponsablesESC.UsuarioCrea;
                            vResponsablesESC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vResponsablesESC.FechaCrea;
                            vResponsablesESC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vResponsablesESC.UsuarioModifica;
                            vResponsablesESC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vResponsablesESC.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
