using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class DiasEstimadosRealesPorModalidadDAL : GeneralDAL
    {
        public DiasEstimadosRealesPorModalidadDAL()
        {
        }
        public int InsertarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DiasEstimadosRealesPorModalidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDiasEstimadosRealesPorModalidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pDiasEstimadosRealesPorModalidad.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pDiasEstimadosRealesPorModalidad.IdComplejidadIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@Operador", DbType.String, pDiasEstimadosRealesPorModalidad.Operador);
                    vDataBase.AddInParameter(vDbCommand, "@Limite", DbType.Int32, pDiasEstimadosRealesPorModalidad.Limite);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntrePSeInicioEjecucion", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreComitePS", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreRadicacionContratoYComite", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreESyRadicacionContratos", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreFCTDefinitivaYES", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreRadicacionFCTyFCTDefinitiva", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pDiasEstimadosRealesPorModalidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDiasEstimadosRealesPorModalidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDiasEstimadosRealesPorModalidad").ToString());
                    GenerarLogAuditoria(pDiasEstimadosRealesPorModalidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DiasEstimadosRealesPorModalidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDiasEstimadosRealesPorModalidad", DbType.Int32, pDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pDiasEstimadosRealesPorModalidad.IdModalidadSeleccion);
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pDiasEstimadosRealesPorModalidad.IdComplejidadIndicador);
                    vDataBase.AddInParameter(vDbCommand, "@Operador", DbType.String, pDiasEstimadosRealesPorModalidad.Operador);
                    vDataBase.AddInParameter(vDbCommand, "@Limite", DbType.Int32, pDiasEstimadosRealesPorModalidad.Limite);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntrePSeInicioEjecucion", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreComitePS", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreRadicacionContratoYComite", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreESyRadicacionContratos", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreFCTDefinitivaYES", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES);
                    vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreRadicacionFCTyFCTDefinitiva", DbType.Int32, pDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pDiasEstimadosRealesPorModalidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDiasEstimadosRealesPorModalidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDiasEstimadosRealesPorModalidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DiasEstimadosRealesPorModalidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDiasEstimadosRealesPorModalidad", DbType.Int32, pDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDiasEstimadosRealesPorModalidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public DiasEstimadosRealesPorModalidad ConsultarDiasEstimadosRealesPorModalidad(int pIdDiasEstimadosRealesPorModalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DiasEstimadosRealesPorModalidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDiasEstimadosRealesPorModalidad", DbType.Int32, pIdDiasEstimadosRealesPorModalidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DiasEstimadosRealesPorModalidad vDiasEstimadosRealesPorModalidad = new DiasEstimadosRealesPorModalidad();
                        while (vDataReaderResults.Read())
                        {
                            vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad = vDataReaderResults["IdDiasEstimadosRealesPorModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDiasEstimadosRealesPorModalidad"].ToString()) : vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad;
                            vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion;
                            vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador;
                            vDiasEstimadosRealesPorModalidad.Operador = vDataReaderResults["Operador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Operador"].ToString()) : vDiasEstimadosRealesPorModalidad.Operador;
                            vDiasEstimadosRealesPorModalidad.Limite = vDataReaderResults["Limite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Limite"].ToString()) : vDiasEstimadosRealesPorModalidad.Limite;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion = vDataReaderResults["DiasHabilesEntrePSeInicioEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntrePSeInicioEjecucion"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS = vDataReaderResults["DiasHabilesEntreComitePS"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreComitePS"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite = vDataReaderResults["DiasHabilesEntreRadicacionContratoYComite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreRadicacionContratoYComite"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos = vDataReaderResults["DiasHabilesEntreESyRadicacionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreESyRadicacionContratos"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES = vDataReaderResults["DiasHabilesEntreFCTDefinitivaYES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreFCTDefinitivaYES"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva = vDataReaderResults["DiasHabilesEntreRadicacionFCTyFCTDefinitiva"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreRadicacionFCTyFCTDefinitiva"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva;
                            vDiasEstimadosRealesPorModalidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vDiasEstimadosRealesPorModalidad.Estado;
                            vDiasEstimadosRealesPorModalidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDiasEstimadosRealesPorModalidad.UsuarioCrea;
                            vDiasEstimadosRealesPorModalidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDiasEstimadosRealesPorModalidad.FechaCrea;
                            vDiasEstimadosRealesPorModalidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDiasEstimadosRealesPorModalidad.UsuarioModifica;
                            vDiasEstimadosRealesPorModalidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDiasEstimadosRealesPorModalidad.FechaModifica;
                        }
                        return vDiasEstimadosRealesPorModalidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DiasEstimadosRealesPorModalidad> ConsultarDiasEstimadosRealesPorModalidads(int? pIdModalidadSeleccion, int? pIdComplejidadIndicador, String pOperador, int? pLimite, int? pDiasHabilesEntrePSeInicioEjecucion, int? pDiasHabilesEntreComitePS, int? pDiasHabilesEntreRadicacionContratoYComite, int? pDiasHabilesEntreESyRadicacionContratos, int? pDiasHabilesEntreFCTDefinitivaYES, int? pDiasHabilesEntreRadicacionFCTyFCTDefinitiva, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DiasEstimadosRealesPorModalidads_Consultar"))
                {
                    if (pIdModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    if (pIdComplejidadIndicador != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pIdComplejidadIndicador);
                    if (pOperador != null)
                        vDataBase.AddInParameter(vDbCommand, "@Operador", DbType.String, pOperador);
                    if (pLimite != null)
                        vDataBase.AddInParameter(vDbCommand, "@Limite", DbType.Int32, pLimite);
                    if (pDiasHabilesEntrePSeInicioEjecucion != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntrePSeInicioEjecucion", DbType.Int32, pDiasHabilesEntrePSeInicioEjecucion);
                    if (pDiasHabilesEntreComitePS != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreComitePS", DbType.Int32, pDiasHabilesEntreComitePS);
                    if (pDiasHabilesEntreRadicacionContratoYComite != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreRadicacionContratoYComite", DbType.Int32, pDiasHabilesEntreRadicacionContratoYComite);
                    if (pDiasHabilesEntreESyRadicacionContratos != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreESyRadicacionContratos", DbType.Int32, pDiasHabilesEntreESyRadicacionContratos);
                    if (pDiasHabilesEntreFCTDefinitivaYES != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreFCTDefinitivaYES", DbType.Int32, pDiasHabilesEntreFCTDefinitivaYES);
                    if (pDiasHabilesEntreRadicacionFCTyFCTDefinitiva != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreRadicacionFCTyFCTDefinitiva", DbType.Int32, pDiasHabilesEntreRadicacionFCTyFCTDefinitiva);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DiasEstimadosRealesPorModalidad> vListaDiasEstimadosRealesPorModalidad = new List<DiasEstimadosRealesPorModalidad>();
                        while (vDataReaderResults.Read())
                        {
                            DiasEstimadosRealesPorModalidad vDiasEstimadosRealesPorModalidad = new DiasEstimadosRealesPorModalidad();
                            vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad = vDataReaderResults["IdDiasEstimadosRealesPorModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDiasEstimadosRealesPorModalidad"].ToString()) : vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad;
                            vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion;
                            vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador;
                            vDiasEstimadosRealesPorModalidad.Operador = vDataReaderResults["Operador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Operador"].ToString()) : vDiasEstimadosRealesPorModalidad.Operador;
                            vDiasEstimadosRealesPorModalidad.Limite = vDataReaderResults["Limite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Limite"].ToString()) : vDiasEstimadosRealesPorModalidad.Limite;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion = vDataReaderResults["DiasHabilesEntrePSeInicioEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntrePSeInicioEjecucion"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS = vDataReaderResults["DiasHabilesEntreComitePS"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreComitePS"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite = vDataReaderResults["DiasHabilesEntreRadicacionContratoYComite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreRadicacionContratoYComite"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos = vDataReaderResults["DiasHabilesEntreESyRadicacionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreESyRadicacionContratos"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES = vDataReaderResults["DiasHabilesEntreFCTDefinitivaYES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreFCTDefinitivaYES"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva = vDataReaderResults["DiasHabilesEntreRadicacionFCTyFCTDefinitiva"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreRadicacionFCTyFCTDefinitiva"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva;
                            vDiasEstimadosRealesPorModalidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vDiasEstimadosRealesPorModalidad.Estado;
                            if (Convert.ToString(vDataReaderResults["Operador"].ToString()) != "0")
                            {
                                vDiasEstimadosRealesPorModalidad.LimiteGrilla = Convert.ToString(vDiasEstimadosRealesPorModalidad.Operador + vDiasEstimadosRealesPorModalidad.Limite);
                            }
                            else
                            {
                                vDiasEstimadosRealesPorModalidad.LimiteGrilla = Convert.ToString(vDiasEstimadosRealesPorModalidad.Limite);
                            }
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vDiasEstimadosRealesPorModalidad.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vDiasEstimadosRealesPorModalidad.NombreEstado = "INACTIVO";
                                }
                            }
                            vDiasEstimadosRealesPorModalidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDiasEstimadosRealesPorModalidad.UsuarioCrea;
                            vDiasEstimadosRealesPorModalidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDiasEstimadosRealesPorModalidad.FechaCrea;
                            vDiasEstimadosRealesPorModalidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDiasEstimadosRealesPorModalidad.UsuarioModifica;
                            vDiasEstimadosRealesPorModalidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDiasEstimadosRealesPorModalidad.FechaModifica;
                            vDiasEstimadosRealesPorModalidad.Complejidad = vDataReaderResults["Complejidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Complejidad"].ToString()) : vDiasEstimadosRealesPorModalidad.Complejidad;
                            vDiasEstimadosRealesPorModalidad.Modalidad = vDataReaderResults["Modalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Modalidad"].ToString()) : vDiasEstimadosRealesPorModalidad.Modalidad;

                            vListaDiasEstimadosRealesPorModalidad.Add(vDiasEstimadosRealesPorModalidad);
                        }
                        return vListaDiasEstimadosRealesPorModalidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarDiasEstimadosRealesPorModalidad(int pIdModalidadSeleccion
                                                            , int? pIdComplejidadIndicador
                                                            , String pOperador
                                                            , int? pLimite
                                                            , int? pDiasHabilesEntrePSeInicioEjecucion
                                                            , int? pDiasHabilesEntreComitePS
                                                            , int? pDiasHabilesEntreRadicacionContratoYComite
                                                            , int? pDiasHabilesEntreESyRadicacionContratos
                                                            , int? pDiasHabilesEntreFCTDefinitivaYES
                                                            , int? pDiasHabilesEntreRadicacionFCTyFCTDefinitiva)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_DiasEstimadosRealesPorModalidad_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    if (pIdComplejidadIndicador != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdComplejidadIndicador", DbType.Int32, pIdComplejidadIndicador);
                    if (pOperador != null)
                        vDataBase.AddInParameter(vDbCommand, "@Operador", DbType.String, pOperador);
                    if (pLimite != null)
                        vDataBase.AddInParameter(vDbCommand, "@Limite", DbType.Int32, pLimite);
                    if (pDiasHabilesEntrePSeInicioEjecucion != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntrePSeInicioEjecucion", DbType.Int32, pDiasHabilesEntrePSeInicioEjecucion);
                    if (pDiasHabilesEntreComitePS != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreComitePS", DbType.Int32, pDiasHabilesEntreComitePS);
                    if (pDiasHabilesEntreRadicacionContratoYComite != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreRadicacionContratoYComite", DbType.Int32, pDiasHabilesEntreRadicacionContratoYComite);
                    if (pDiasHabilesEntreESyRadicacionContratos != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreESyRadicacionContratos", DbType.Int32, pDiasHabilesEntreESyRadicacionContratos);
                    if (pDiasHabilesEntreFCTDefinitivaYES != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreFCTDefinitivaYES", DbType.Int32, pDiasHabilesEntreFCTDefinitivaYES);
                    if (pDiasHabilesEntreRadicacionFCTyFCTDefinitiva != null)
                        vDataBase.AddInParameter(vDbCommand, "@DiasHabilesEntreRadicacionFCTyFCTDefinitiva", DbType.Int32, pDiasHabilesEntreRadicacionFCTyFCTDefinitiva);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DiasEstimadosRealesPorModalidad vDiasEstimadosRealesPorModalidad = new DiasEstimadosRealesPorModalidad();
                        while (vDataReaderResults.Read())
                        {
                            vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad = vDataReaderResults["IdDiasEstimadosRealesPorModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDiasEstimadosRealesPorModalidad"].ToString()) : vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad;
                            vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion;
                            vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador;
                            vDiasEstimadosRealesPorModalidad.Operador = vDataReaderResults["Operador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Operador"].ToString()) : vDiasEstimadosRealesPorModalidad.Operador;
                            vDiasEstimadosRealesPorModalidad.Limite = vDataReaderResults["Limite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Limite"].ToString()) : vDiasEstimadosRealesPorModalidad.Limite;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion = vDataReaderResults["DiasHabilesEntrePSeInicioEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntrePSeInicioEjecucion"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS = vDataReaderResults["DiasHabilesEntreComitePS"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreComitePS"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite = vDataReaderResults["DiasHabilesEntreRadicacionContratoYComite"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreRadicacionContratoYComite"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos = vDataReaderResults["DiasHabilesEntreESyRadicacionContratos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreESyRadicacionContratos"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES = vDataReaderResults["DiasHabilesEntreFCTDefinitivaYES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreFCTDefinitivaYES"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES;
                            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva = vDataReaderResults["DiasHabilesEntreRadicacionFCTyFCTDefinitiva"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DiasHabilesEntreRadicacionFCTyFCTDefinitiva"].ToString()) : vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva;
                            vDiasEstimadosRealesPorModalidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vDiasEstimadosRealesPorModalidad.Estado;
                            vDiasEstimadosRealesPorModalidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDiasEstimadosRealesPorModalidad.UsuarioCrea;
                            vDiasEstimadosRealesPorModalidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDiasEstimadosRealesPorModalidad.FechaCrea;
                            vDiasEstimadosRealesPorModalidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDiasEstimadosRealesPorModalidad.UsuarioModifica;
                            vDiasEstimadosRealesPorModalidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDiasEstimadosRealesPorModalidad.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
