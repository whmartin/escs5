using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class ComplejidadesDAL : GeneralDAL
    {
        public ComplejidadesDAL()
        {
        }
        public int InsertarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Complejidades_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdComplejidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pComplejidades.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@TipoComplejidad", DbType.Int32, pComplejidades.TipoComplejidad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pComplejidades.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pComplejidades.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pComplejidades.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pComplejidades.IdComplejidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdComplejidad").ToString());
                    GenerarLogAuditoria(pComplejidades, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Complejidades_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidad", DbType.Int32, pComplejidades.IdComplejidad);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pComplejidades.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@TipoComplejidad", DbType.Int32, pComplejidades.TipoComplejidad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pComplejidades.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pComplejidades.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pComplejidades.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pComplejidades, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Complejidades_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidad", DbType.Int32, pComplejidades.IdComplejidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pComplejidades, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public Complejidades ConsultarComplejidades(int pIdComplejidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Complejidades_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdComplejidad", DbType.Int32, pIdComplejidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Complejidades vComplejidades = new Complejidades();
                        while (vDataReaderResults.Read())
                        {
                            vComplejidades.IdComplejidad = vDataReaderResults["IdComplejidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidad"].ToString()) : vComplejidades.IdComplejidad;
                            vComplejidades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vComplejidades.Nombre;
                            vComplejidades.TipoComplejidad = vDataReaderResults["TipoComplejidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoComplejidad"].ToString()) : vComplejidades.TipoComplejidad;
                            vComplejidades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vComplejidades.Descripcion;
                            vComplejidades.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vComplejidades.Estado;
                            vComplejidades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vComplejidades.UsuarioCrea;
                            vComplejidades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vComplejidades.FechaCrea;
                            vComplejidades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vComplejidades.UsuarioModifica;
                            vComplejidades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vComplejidades.FechaModifica;
                        }
                        return vComplejidades;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Complejidades> ConsultarComplejidadess(String pNombre, int? pTipoComplejidad, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Complejidadess_Consultar"))
                {
                    if (pNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    if (pTipoComplejidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoComplejidad", DbType.Int32, pTipoComplejidad);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Complejidades> vListaComplejidades = new List<Complejidades>();
                        while (vDataReaderResults.Read())
                        {
                            Complejidades vComplejidades = new Complejidades();
                            vComplejidades.IdComplejidad = vDataReaderResults["IdComplejidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidad"].ToString()) : vComplejidades.IdComplejidad;
                            vComplejidades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vComplejidades.Nombre;
                            vComplejidades.TipoComplejidad = vDataReaderResults["TipoComplejidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoComplejidad"].ToString()) : vComplejidades.TipoComplejidad;
                            vComplejidades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vComplejidades.Descripcion;
                            vComplejidades.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vComplejidades.Estado;
                            vComplejidades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vComplejidades.UsuarioCrea;
                            vComplejidades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vComplejidades.FechaCrea;
                            vComplejidades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vComplejidades.UsuarioModifica;
                            vComplejidades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vComplejidades.FechaModifica;
                            if (vDataReaderResults["Estado"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["Estado"].ToString()) == 1)
                                {
                                    vComplejidades.NombreEstado = "ACTIVO";
                                }
                                else
                                {
                                    vComplejidades.NombreEstado = "INACTIVO";
                                }
                            }
                            if (vDataReaderResults["TipoComplejidad"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(vDataReaderResults["TipoComplejidad"].ToString()) == 1)
                                {
                                    vComplejidades.NombreTipoComplejidad = "INTERNA";
                                }
                                else
                                {
                                    vComplejidades.NombreTipoComplejidad = "INDICADOR";
                                }
                            }
                            vListaComplejidades.Add(vComplejidades);
                        }
                        return vListaComplejidades;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarComplejidades(String pNombre, int pTipoComplejidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                Boolean ExisteRegistro = true;
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Complejidades_Validar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);
                    vDataBase.AddInParameter(vDbCommand, "@TipoComplejidad", DbType.Int32, pTipoComplejidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Complejidades vComplejidades = new Complejidades();
                        while (vDataReaderResults.Read())
                        {
                            vComplejidades.IdComplejidad = vDataReaderResults["IdComplejidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidad"].ToString()) : vComplejidades.IdComplejidad;
                            vComplejidades.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vComplejidades.Nombre;
                            vComplejidades.TipoComplejidad = vDataReaderResults["TipoComplejidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoComplejidad"].ToString()) : vComplejidades.TipoComplejidad;
                            vComplejidades.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vComplejidades.Descripcion;
                            vComplejidades.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vComplejidades.Estado;
                            vComplejidades.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vComplejidades.UsuarioCrea;
                            vComplejidades.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vComplejidades.FechaCrea;
                            vComplejidades.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vComplejidades.UsuarioModifica;
                            vComplejidades.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vComplejidades.FechaModifica;
                            return ExisteRegistro = false;
                        }
                        return ExisteRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
