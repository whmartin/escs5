﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icbf.EstudioSectorCosto.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.DataAccess
{
    public class ReporteConsultaSeguimientoDAL : GeneralDAL
    {
        /// <summary>
        /// Método para consultar cierres por vigencia por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de CierreEstudiosPorVigencia</returns>
        public List<CierreEstudiosPorVigencia> ConsultarCierresEstudio(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Cierres_By_IdConsecutivoEstudio_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CierreEstudiosPorVigencia> vLstCierres = new List<CierreEstudiosPorVigencia>();
                        while (vDataReaderResults.Read())
                        {
                            CierreEstudiosPorVigencia res = new CierreEstudiosPorVigencia();
                            res.AnioCierre = vDataReaderResults["AnioCierre"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnioCierre"].ToString()) : res.AnioCierre;
                            res.FechaEjecucion = vDataReaderResults["FechaEjecucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEjecucion"].ToString()) : res.FechaEjecucion;
                            res.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? vDataReaderResults["Descripcion"].ToString() : res.Descripcion;


                            vLstCierres.Add(res);
                        }
                        return vLstCierres;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RegistroSolicitudEstudioSectoryCaso ConsultarRegistroInicial_By_IdConsecutivoEstudio(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroInicial_By_IdConsecutivoEstudio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSyC = new RegistroSolicitudEstudioSectoryCaso();
                        while (vDataReaderResults.Read())
                        {



                            vRegistroSolicitudEstudioSyC.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vRegistroSolicitudEstudioSyC.IdRegistroSolicitudEstudioSectoryCaso;

                            vRegistroSolicitudEstudioSyC.AplicaPACCO = vDataReaderResults["AplicaPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AplicaPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.AplicaPACCO;

                            vRegistroSolicitudEstudioSyC.ConsecutivoPACCO = vDataReaderResults["ConsecutivoPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.ConsecutivoPACCO;

                            vRegistroSolicitudEstudioSyC.DireccionsolicitantePACCO = vDataReaderResults["DireccionsolicitantePACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DireccionsolicitantePACCO"].ToString()) : vRegistroSolicitudEstudioSyC.DireccionsolicitantePACCO;

                            vRegistroSolicitudEstudioSyC.ObjetoPACCO = vDataReaderResults["ObjetoPACCO"] != DBNull.Value ? (vDataReaderResults["ObjetoPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.ObjetoPACCO;

                            vRegistroSolicitudEstudioSyC.ModalidadPACCO = vDataReaderResults["ModalidadPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ModalidadPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.ModalidadPACCO;

                            vRegistroSolicitudEstudioSyC.ValorPresupuestalPACCO = vDataReaderResults["ValorPresupuestalPACCO"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestalPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.ValorPresupuestalPACCO;

                            vRegistroSolicitudEstudioSyC.VigenciaPACCO = vDataReaderResults["VigenciaPACCO"] != DBNull.Value ? (vDataReaderResults["VigenciaPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.VigenciaPACCO;

                            vRegistroSolicitudEstudioSyC.ConsecutivoEstudioRelacionado = vDataReaderResults["ConsecutivoEstudioRelacionado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoEstudioRelacionado"].ToString()) : vRegistroSolicitudEstudioSyC.ConsecutivoEstudioRelacionado;

                            vRegistroSolicitudEstudioSyC.FechaSolicitudInicial = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudInicial"].ToString()) : vRegistroSolicitudEstudioSyC.FechaSolicitudInicial;

                            vRegistroSolicitudEstudioSyC.ActaCorreoNoRadicado = vDataReaderResults["ActaCorreoNoRadicado"] != DBNull.Value ? (vDataReaderResults["ActaCorreoNoRadicado"].ToString()) : vRegistroSolicitudEstudioSyC.ActaCorreoNoRadicado;

                            vRegistroSolicitudEstudioSyC.NombreAbreviado = vDataReaderResults["NombreAbreviado"] != DBNull.Value ? (vDataReaderResults["NombreAbreviado"].ToString()) : vRegistroSolicitudEstudioSyC.NombreAbreviado;

                            vRegistroSolicitudEstudioSyC.NumeroReproceso = vDataReaderResults["NumeroReproceso"] != DBNull.Value ? (vDataReaderResults["NumeroReproceso"].ToString()) : vRegistroSolicitudEstudioSyC.NumeroReproceso;

                            vRegistroSolicitudEstudioSyC.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? (vDataReaderResults["Objeto"].ToString()) : vRegistroSolicitudEstudioSyC.Objeto;

                            vRegistroSolicitudEstudioSyC.CuentaVigenciasFuturasPACCO = vDataReaderResults["CuentaVigenciasFuturasPACCO"] != DBNull.Value ? (vDataReaderResults["CuentaVigenciasFuturasPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.CuentaVigenciasFuturasPACCO;

                            vRegistroSolicitudEstudioSyC.AplicaProcesoSeleccion = vDataReaderResults["AplicaProcesoSeleccion"] != DBNull.Value ? (vDataReaderResults["AplicaProcesoSeleccion"].ToString()) : vRegistroSolicitudEstudioSyC.AplicaProcesoSeleccion;

                            vRegistroSolicitudEstudioSyC.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRegistroSolicitudEstudioSyC.IdModalidadSeleccion;

                            vRegistroSolicitudEstudioSyC.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRegistroSolicitudEstudioSyC.IdModalidadSeleccion;

                            vRegistroSolicitudEstudioSyC.IdTipoEstudio = vDataReaderResults["IdTipoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstudio"].ToString()) : vRegistroSolicitudEstudioSyC.IdTipoEstudio;

                            vRegistroSolicitudEstudioSyC.IdComplejidadInterna = vDataReaderResults["IdComplejidadInterna"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadInterna"].ToString()) : vRegistroSolicitudEstudioSyC.IdComplejidadInterna;

                            vRegistroSolicitudEstudioSyC.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vRegistroSolicitudEstudioSyC.IdComplejidadIndicador;

                            vRegistroSolicitudEstudioSyC.IdResponsableES = vDataReaderResults["IdResponsableES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsableES"].ToString()) : vRegistroSolicitudEstudioSyC.IdResponsableES;

                            vRegistroSolicitudEstudioSyC.IdResponsableEC = vDataReaderResults["IdResponsableEC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsableEC"].ToString()) : vRegistroSolicitudEstudioSyC.IdResponsableEC;

                            vRegistroSolicitudEstudioSyC.OrdenadorGasto = vDataReaderResults["OrdenadorGasto"] != DBNull.Value ? (vDataReaderResults["OrdenadorGasto"].ToString()) : vRegistroSolicitudEstudioSyC.OrdenadorGasto;

                            vRegistroSolicitudEstudioSyC.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vRegistroSolicitudEstudioSyC.IdEstadoSolicitud;

                            vRegistroSolicitudEstudioSyC.IdMotivoSolicitud = vDataReaderResults["IdMotivoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMotivoSolicitud"].ToString()) : vRegistroSolicitudEstudioSyC.IdMotivoSolicitud;

                            vRegistroSolicitudEstudioSyC.IdDireccionSolicitante = vDataReaderResults["IdDireccionSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionSolicitante"].ToString()) : vRegistroSolicitudEstudioSyC.IdDireccionSolicitante;

                            vRegistroSolicitudEstudioSyC.IdAreaSolicitante = vDataReaderResults["IdAreaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAreaSolicitante"].ToString()) : vRegistroSolicitudEstudioSyC.IdAreaSolicitante;

                            vRegistroSolicitudEstudioSyC.TipoValor = vDataReaderResults["TipoValor"] != DBNull.Value ? (vDataReaderResults["TipoValor"].ToString()) : vRegistroSolicitudEstudioSyC.TipoValor;

                            vRegistroSolicitudEstudioSyC.ValorPresupuestoEstimadoSolicitante = vDataReaderResults["ValorPresupuestoEstimadoSolicitante"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestoEstimadoSolicitante"].ToString()) : vRegistroSolicitudEstudioSyC.ValorPresupuestoEstimadoSolicitante;

                            vRegistroSolicitudEstudioSyC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? (vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroSolicitudEstudioSyC.UsuarioCrea;

                            vRegistroSolicitudEstudioSyC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroSolicitudEstudioSyC.FechaCrea;

                            vRegistroSolicitudEstudioSyC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? (vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroSolicitudEstudioSyC.UsuarioModifica;

                            vRegistroSolicitudEstudioSyC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroSolicitudEstudioSyC.FechaModifica;


                        }
                        return vRegistroSolicitudEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccion_By_IdConsecutivoEstudio(Int32? pConsecutivoEstudio, String pNombreAbreviado, String pObjeto, int? pIdModalidadSeleccion, int? pIdResponsableES, int? pIdResponsableEC, int? pIdEstadoSolicitud, int? pIdDireccionSolicitante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_InformacionProcesoSeleccion_porIdConsecutivoEstudio_Consultar"))
                {
                    if (pConsecutivoEstudio != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int32, pConsecutivoEstudio);
                    if (pNombreAbreviado != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreAbreviado", DbType.String, pNombreAbreviado);
                    if (pObjeto != null)
                        vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, pObjeto);
                    if (pIdModalidadSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdModalidadDeSeleccion", DbType.Int32, pIdModalidadSeleccion);
                    if (pIdResponsableES != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableES", DbType.Int32, pIdResponsableES);
                    if (pIdResponsableEC != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdResponsableEC", DbType.Int32, pIdResponsableEC);
                    if (pIdEstadoSolicitud != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pIdEstadoSolicitud);
                    if (pIdDireccionSolicitante != null)
                        vDataBase.AddInParameter(vDbCommand, "@DireccionSolicitante", DbType.Int32, pIdDireccionSolicitante);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InformacionProcesoSeleccion> vListaProcesoSeleccionPorContrato = new List<InformacionProcesoSeleccion>();
                        while (vDataReaderResults.Read())
                        {
                            InformacionProcesoSeleccion vInformacionProcesoSeleccion = new InformacionProcesoSeleccion();
                            vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion = vDataReaderResults["IdInformacionProcesoSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionProcesoSeleccion"].ToString()) : vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion;
                            vInformacionProcesoSeleccion.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : vInformacionProcesoSeleccion.IdConsecutivoEstudio;
                            vInformacionProcesoSeleccion.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vInformacionProcesoSeleccion.IdContrato;
                            vInformacionProcesoSeleccion.NumeroDelProceso = vDataReaderResults["NumeroDelProceso"] != DBNull.Value ? vDataReaderResults["NumeroDelProceso"].ToString() : vInformacionProcesoSeleccion.NumeroDelProceso;
                            vInformacionProcesoSeleccion.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : vInformacionProcesoSeleccion.ObjetoContrato;
                            vInformacionProcesoSeleccion.EstadoDelProceso = vDataReaderResults["EstadoDelProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoDelProceso"].ToString()) : vInformacionProcesoSeleccion.EstadoDelProceso;
                            vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso;
                            vInformacionProcesoSeleccion.Contratista = vDataReaderResults["Contratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Contratista"].ToString()) : vInformacionProcesoSeleccion.Contratista;
                            vInformacionProcesoSeleccion.ValorPresupuestadoInicial = vDataReaderResults["ValorPresupuestadoInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestadoInicial"].ToString()) : vInformacionProcesoSeleccion.ValorPresupuestadoInicial;
                            vInformacionProcesoSeleccion.ValorAdjudicado = vDataReaderResults["ValorAdjudicado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdjudicado"].ToString()) : vInformacionProcesoSeleccion.ValorAdjudicado;
                            vInformacionProcesoSeleccion.PlazoDeEjecucion = vDataReaderResults["PlazoDeEjecucion"] != DBNull.Value ? vDataReaderResults["PlazoDeEjecucion"].ToString() : vInformacionProcesoSeleccion.PlazoDeEjecucion;
                            vInformacionProcesoSeleccion.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observacion"].ToString()) : vInformacionProcesoSeleccion.Observacion;
                            vInformacionProcesoSeleccion.URLDelProceso = vDataReaderResults["URLDelProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["URLDelProceso"].ToString()) : vInformacionProcesoSeleccion.URLDelProceso;
                            vListaProcesoSeleccionPorContrato.Add(vInformacionProcesoSeleccion);
                        }
                        return vListaProcesoSeleccionPorContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<GestionesEstudioSectorCostos> ConsultarGestion_By_IdConsecutivoEstudio(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Gestion_By_IdConsecutivoEstudio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GestionesEstudioSectorCostos> LstGestionesEstudioSyC = new List<GestionesEstudioSectorCostos>();
                        while (vDataReaderResults.Read())
                        {
                            GestionesEstudioSectorCostos vGestionesEstudioSyC = new GestionesEstudioSectorCostos();
                            vGestionesEstudioSyC.IdGestionEstudio = vDataReaderResults["IdGestionEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionEstudio"].ToString()) : vGestionesEstudioSyC.IdGestionEstudio;

                            vGestionesEstudioSyC.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vGestionesEstudioSyC.IdRegistroSolicitudEstudioSectoryCaso;

                            vGestionesEstudioSyC.FechaPreguntaOComentario = vDataReaderResults["FechaPreguntaOComentario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPreguntaOComentario"].ToString()) : vGestionesEstudioSyC.FechaPreguntaOComentario;

                            vGestionesEstudioSyC.FechaRespuesta = vDataReaderResults["FechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuesta"].ToString()) : vGestionesEstudioSyC.FechaRespuesta;

                            vGestionesEstudioSyC.UltimaFechaPreguntaOComentario = vDataReaderResults["UltimaFechaPreguntaOComentario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["UltimaFechaPreguntaOComentario"].ToString()) : vGestionesEstudioSyC.UltimaFechaPreguntaOComentario;

                            vGestionesEstudioSyC.UltimaFechaRespuesta = vDataReaderResults["UltimaFechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["UltimaFechaRespuesta"].ToString()) : vGestionesEstudioSyC.UltimaFechaRespuesta;

                            vGestionesEstudioSyC.FechaEntregaES_MC = vDataReaderResults["FechaEntregaES_MC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaES_MC"].ToString()) : vGestionesEstudioSyC.FechaEntregaES_MC;

                            vGestionesEstudioSyC.FechaEntregaES_MCDef = vDataReaderResults["FechaEntregaES_MCDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaES_MCDefinitiva"].ToString()) : vGestionesEstudioSyC.FechaEntregaES_MCDef;

                            vGestionesEstudioSyC.NoAplicaFechaEntregaES_MC = vDataReaderResults["NoAplicaFechaEntregaES_MC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaEntregaES_MC"].ToString()) : vGestionesEstudioSyC.NoAplicaFechaEntregaES_MC;

                            vGestionesEstudioSyC.FechaEntregaFCTORequerimiento = vDataReaderResults["FechaEntregaFCTORequerimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaFCTORequerimiento"].ToString()) : vGestionesEstudioSyC.FechaEntregaFCTORequerimiento;

                            vGestionesEstudioSyC.FechaEntregaFCTORequerimientoDef = vDataReaderResults["FechaEntregaFCTORequerimientoDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaFCTORequerimientoDefinitiva"].ToString()) : vGestionesEstudioSyC.FechaEntregaFCTORequerimientoDef;

                            vGestionesEstudioSyC.NoAplicaFechaentregaFCTOrequerimiento = vDataReaderResults["NoAplicaFechaentregaFCTOrequerimiento"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaentregaFCTOrequerimiento"].ToString()) : vGestionesEstudioSyC.NoAplicaFechaentregaFCTOrequerimiento;

                            vGestionesEstudioSyC.FechaEnvioSDC = vDataReaderResults["FechaEnvioSDC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioSDC"].ToString()) : vGestionesEstudioSyC.FechaEnvioSDC;

                            vGestionesEstudioSyC.FechaEnvioSDCDef = vDataReaderResults["FechaEnvioSDCDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioSDCDefinitiva"].ToString()) : vGestionesEstudioSyC.FechaEnvioSDCDef;

                            vGestionesEstudioSyC.NoAplicaFechaEnvioSDC = vDataReaderResults["NoAplicaFechaEnvioSDC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaEnvioSDC"].ToString()) : vGestionesEstudioSyC.NoAplicaFechaEnvioSDC;

                            vGestionesEstudioSyC.PlazoEntregaCotizaciones = vDataReaderResults["PlazoEntregaCotizaciones"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["PlazoEntregaCotizaciones"].ToString()) : vGestionesEstudioSyC.PlazoEntregaCotizaciones;

                            vGestionesEstudioSyC.PlazoEntregaCotizacionesDef = vDataReaderResults["PlazoEntregaCotizacionesDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["PlazoEntregaCotizacionesDefinitiva"].ToString()) : vGestionesEstudioSyC.PlazoEntregaCotizacionesDef;

                            vGestionesEstudioSyC.NoAplicaPlazoEntregaCotizaciones = vDataReaderResults["NoAplicaPlazoEntregaCotizaciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaPlazoEntregaCotizaciones"].ToString()) : vGestionesEstudioSyC.NoAplicaPlazoEntregaCotizaciones;

                            vGestionesEstudioSyC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? (vDataReaderResults["UsuarioCrea"].ToString()) : vGestionesEstudioSyC.UsuarioCrea;

                            vGestionesEstudioSyC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGestionesEstudioSyC.FechaCrea;

                            vGestionesEstudioSyC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? (vDataReaderResults["UsuarioModifica"].ToString()) : vGestionesEstudioSyC.UsuarioModifica;

                            vGestionesEstudioSyC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGestionesEstudioSyC.FechaModifica;

                            LstGestionesEstudioSyC.Add(vGestionesEstudioSyC);

                        }
                        return LstGestionesEstudioSyC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar gestiones por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de GestionesEstudioSectorCostos</returns>
        public List< GestionesEstudioSectorCostos> ConsultarGestionEstudio(decimal pIdConsecutivoEstudio,int  pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Gestion_By_IdConsecutivoEstudio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<GestionesEstudioSectorCostos> vLstGestion = new List<GestionesEstudioSectorCostos>();
                        while (vDataReaderResults.Read())
                        {
                            
                            GestionesEstudioSectorCostos vGestionesEstudioSyC = new GestionesEstudioSectorCostos();
                            vGestionesEstudioSyC.IdGestionEstudio = vDataReaderResults["IdGestionEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionEstudio"].ToString()) : vGestionesEstudioSyC.IdGestionEstudio;

                            vGestionesEstudioSyC.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vGestionesEstudioSyC.IdRegistroSolicitudEstudioSectoryCaso;

                            vGestionesEstudioSyC.FechaPreguntaOComentario = vDataReaderResults["FechaPreguntaOComentario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPreguntaOComentario"].ToString()) : vGestionesEstudioSyC.FechaPreguntaOComentario;

                            vGestionesEstudioSyC.FechaRespuesta = vDataReaderResults["FechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuesta"].ToString()) : vGestionesEstudioSyC.FechaRespuesta;

                            vGestionesEstudioSyC.UltimaFechaPreguntaOComentario = vDataReaderResults["UltimaFechaPreguntaOComentario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["UltimaFechaPreguntaOComentario"].ToString()) : vGestionesEstudioSyC.UltimaFechaPreguntaOComentario;

                            vGestionesEstudioSyC.UltimaFechaRespuesta = vDataReaderResults["UltimaFechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["UltimaFechaRespuesta"].ToString()) : vGestionesEstudioSyC.UltimaFechaRespuesta;

                            vGestionesEstudioSyC.FechaEntregaES_MC = vDataReaderResults["FechaEntregaES_MC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaES_MC"].ToString()) : vGestionesEstudioSyC.FechaEntregaES_MC;

                            vGestionesEstudioSyC.FechaEntregaES_MCDef = vDataReaderResults["FechaEntregaES_MCDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaES_MCDefinitiva"].ToString()) : vGestionesEstudioSyC.FechaEntregaES_MCDef;

                            vGestionesEstudioSyC.NoAplicaFechaEntregaES_MC = vDataReaderResults["NoAplicaFechaEntregaES_MC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaEntregaES_MC"].ToString()) : vGestionesEstudioSyC.NoAplicaFechaEntregaES_MC;

                            vGestionesEstudioSyC.FechaEntregaFCTORequerimiento = vDataReaderResults["FechaEntregaFCTORequerimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaFCTORequerimiento"].ToString()) : vGestionesEstudioSyC.FechaEntregaFCTORequerimiento;

                            vGestionesEstudioSyC.FechaEntregaFCTORequerimientoDef = vDataReaderResults["FechaEntregaFCTORequerimientoDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEntregaFCTORequerimientoDefinitiva"].ToString()) : vGestionesEstudioSyC.FechaEntregaFCTORequerimientoDef;

                            vGestionesEstudioSyC.NoAplicaFechaentregaFCTOrequerimiento = vDataReaderResults["NoAplicaFechaentregaFCTOrequerimiento"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaentregaFCTOrequerimiento"].ToString()) : vGestionesEstudioSyC.NoAplicaFechaentregaFCTOrequerimiento;

                            vGestionesEstudioSyC.FechaEnvioSDC = vDataReaderResults["FechaEnvioSDC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioSDC"].ToString()) : vGestionesEstudioSyC.FechaEnvioSDC;

                            vGestionesEstudioSyC.FechaEnvioSDCDef = vDataReaderResults["FechaEnvioSDCDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaEnvioSDCDefinitiva"].ToString()) : vGestionesEstudioSyC.FechaEnvioSDCDef;

                            vGestionesEstudioSyC.NoAplicaFechaEnvioSDC = vDataReaderResults["NoAplicaFechaEnvioSDC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFechaEnvioSDC"].ToString()) : vGestionesEstudioSyC.NoAplicaFechaEnvioSDC;

                            vGestionesEstudioSyC.PlazoEntregaCotizaciones = vDataReaderResults["PlazoEntregaCotizaciones"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["PlazoEntregaCotizaciones"].ToString()) : vGestionesEstudioSyC.PlazoEntregaCotizaciones;

                            vGestionesEstudioSyC.PlazoEntregaCotizacionesDef = vDataReaderResults["PlazoEntregaCotizacionesDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["PlazoEntregaCotizacionesDefinitiva"].ToString()) : vGestionesEstudioSyC.PlazoEntregaCotizacionesDef;

                            vGestionesEstudioSyC.NoAplicaPlazoEntregaCotizaciones = vDataReaderResults["NoAplicaPlazoEntregaCotizaciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaPlazoEntregaCotizaciones"].ToString()) : vGestionesEstudioSyC.NoAplicaPlazoEntregaCotizaciones;

                            vGestionesEstudioSyC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? (vDataReaderResults["UsuarioCrea"].ToString()) : vGestionesEstudioSyC.UsuarioCrea;

                            vGestionesEstudioSyC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vGestionesEstudioSyC.FechaCrea;

                            vGestionesEstudioSyC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? (vDataReaderResults["UsuarioModifica"].ToString()) : vGestionesEstudioSyC.UsuarioModifica;

                            vGestionesEstudioSyC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vGestionesEstudioSyC.FechaModifica;

                            vLstGestion.Add(vGestionesEstudioSyC);
                        }
                        return vLstGestion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar registro inicial id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de RegistroSolicitudEstudioSectoryCaso</returns>
        public List< RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroInicial(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroInicial_By_IdConsecutivoEstudio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroSolicitudEstudioSectoryCaso> vLstRegistro = new List<RegistroSolicitudEstudioSectoryCaso>();
                        while (vDataReaderResults.Read())
                        {

                            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSyC = new RegistroSolicitudEstudioSectoryCaso();

                            vRegistroSolicitudEstudioSyC.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vRegistroSolicitudEstudioSyC.IdRegistroSolicitudEstudioSectoryCaso;

                            vRegistroSolicitudEstudioSyC.AplicaPACCO = vDataReaderResults["AplicaPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AplicaPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.AplicaPACCO;

                            vRegistroSolicitudEstudioSyC.ConsecutivoPACCO = vDataReaderResults["ConsecutivoPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.ConsecutivoPACCO;

                            vRegistroSolicitudEstudioSyC.DireccionsolicitantePACCO = vDataReaderResults["DireccionsolicitantePACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DireccionsolicitantePACCO"].ToString()) : vRegistroSolicitudEstudioSyC.DireccionsolicitantePACCO;

                            vRegistroSolicitudEstudioSyC.ObjetoPACCO = vDataReaderResults["ObjetoPACCO"] != DBNull.Value ? (vDataReaderResults["ObjetoPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.ObjetoPACCO;

                            vRegistroSolicitudEstudioSyC.ModalidadPACCO = vDataReaderResults["ModalidadPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ModalidadPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.ModalidadPACCO;

                            vRegistroSolicitudEstudioSyC.ValorPresupuestalPACCO = vDataReaderResults["ValorPresupuestalPACCO"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestalPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.ValorPresupuestalPACCO;

                            vRegistroSolicitudEstudioSyC.VigenciaPACCO = vDataReaderResults["VigenciaPACCO"] != DBNull.Value ? (vDataReaderResults["VigenciaPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.VigenciaPACCO;

                            vRegistroSolicitudEstudioSyC.ConsecutivoEstudioRelacionado = vDataReaderResults["ConsecutivoEstudioRelacionado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoEstudioRelacionado"].ToString()) : vRegistroSolicitudEstudioSyC.ConsecutivoEstudioRelacionado;

                            vRegistroSolicitudEstudioSyC.FechaSolicitudInicial = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudInicial"].ToString()) : vRegistroSolicitudEstudioSyC.FechaSolicitudInicial;

                            vRegistroSolicitudEstudioSyC.ActaCorreoNoRadicado = vDataReaderResults["ActaCorreoNoRadicado"] != DBNull.Value ? (vDataReaderResults["ActaCorreoNoRadicado"].ToString()) : vRegistroSolicitudEstudioSyC.ActaCorreoNoRadicado;

                            vRegistroSolicitudEstudioSyC.NombreAbreviado = vDataReaderResults["NombreAbreviado"] != DBNull.Value ? (vDataReaderResults["NombreAbreviado"].ToString()) : vRegistroSolicitudEstudioSyC.NombreAbreviado;

                            vRegistroSolicitudEstudioSyC.NumeroReproceso = vDataReaderResults["NumeroReproceso"] != DBNull.Value ? (vDataReaderResults["NumeroReproceso"].ToString()) : vRegistroSolicitudEstudioSyC.NumeroReproceso;

                            vRegistroSolicitudEstudioSyC.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? (vDataReaderResults["Objeto"].ToString()) : vRegistroSolicitudEstudioSyC.Objeto;

                            vRegistroSolicitudEstudioSyC.CuentaVigenciasFuturasPACCO = vDataReaderResults["CuentaVigenciasFuturasPACCO"] != DBNull.Value ? (vDataReaderResults["CuentaVigenciasFuturasPACCO"].ToString()) : vRegistroSolicitudEstudioSyC.CuentaVigenciasFuturasPACCO;

                            vRegistroSolicitudEstudioSyC.AplicaProcesoSeleccion = vDataReaderResults["AplicaProcesoSeleccion"] != DBNull.Value ? (vDataReaderResults["AplicaProcesoSeleccion"].ToString()) : vRegistroSolicitudEstudioSyC.AplicaProcesoSeleccion;

                            vRegistroSolicitudEstudioSyC.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRegistroSolicitudEstudioSyC.IdModalidadSeleccion;

                            vRegistroSolicitudEstudioSyC.IdModalidadSeleccion = vDataReaderResults["IdModalidadSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadSeleccion"].ToString()) : vRegistroSolicitudEstudioSyC.IdModalidadSeleccion;

                            vRegistroSolicitudEstudioSyC.IdTipoEstudio = vDataReaderResults["IdTipoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstudio"].ToString()) : vRegistroSolicitudEstudioSyC.IdTipoEstudio;

                            vRegistroSolicitudEstudioSyC.IdComplejidadInterna = vDataReaderResults["IdComplejidadInterna"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadInterna"].ToString()) : vRegistroSolicitudEstudioSyC.IdComplejidadInterna;

                            vRegistroSolicitudEstudioSyC.IdComplejidadIndicador = vDataReaderResults["IdComplejidadIndicador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComplejidadIndicador"].ToString()) : vRegistroSolicitudEstudioSyC.IdComplejidadIndicador;

                            vRegistroSolicitudEstudioSyC.IdResponsableES = vDataReaderResults["IdResponsableES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsableES"].ToString()) : vRegistroSolicitudEstudioSyC.IdResponsableES;

                            vRegistroSolicitudEstudioSyC.IdResponsableEC = vDataReaderResults["IdResponsableEC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdResponsableEC"].ToString()) : vRegistroSolicitudEstudioSyC.IdResponsableEC;

                            vRegistroSolicitudEstudioSyC.OrdenadorGasto = vDataReaderResults["OrdenadorGasto"] != DBNull.Value ? (vDataReaderResults["OrdenadorGasto"].ToString()) : vRegistroSolicitudEstudioSyC.OrdenadorGasto;

                            vRegistroSolicitudEstudioSyC.IdEstadoSolicitud = vDataReaderResults["IdEstadoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoSolicitud"].ToString()) : vRegistroSolicitudEstudioSyC.IdEstadoSolicitud;

                            vRegistroSolicitudEstudioSyC.IdMotivoSolicitud = vDataReaderResults["IdMotivoSolicitud"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMotivoSolicitud"].ToString()) : vRegistroSolicitudEstudioSyC.IdMotivoSolicitud;

                            vRegistroSolicitudEstudioSyC.IdDireccionSolicitante = vDataReaderResults["IdDireccionSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccionSolicitante"].ToString()) : vRegistroSolicitudEstudioSyC.IdDireccionSolicitante;

                            vRegistroSolicitudEstudioSyC.DireccionSolicitante = vDataReaderResults["DireccionSolicitante"] != DBNull.Value ? (vDataReaderResults["DireccionSolicitante"].ToString()) : vRegistroSolicitudEstudioSyC.DireccionSolicitante;

                            vRegistroSolicitudEstudioSyC.IdAreaSolicitante = vDataReaderResults["IdAreaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAreaSolicitante"].ToString()) : vRegistroSolicitudEstudioSyC.IdAreaSolicitante;

                            vRegistroSolicitudEstudioSyC.TipoValor = vDataReaderResults["TipoValor"] != DBNull.Value ? (vDataReaderResults["TipoValor"].ToString()) : vRegistroSolicitudEstudioSyC.TipoValor;

                            vRegistroSolicitudEstudioSyC.ValorPresupuestoEstimadoSolicitante = vDataReaderResults["ValorPresupuestoEstimadoSolicitante"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestoEstimadoSolicitante"].ToString()) : vRegistroSolicitudEstudioSyC.ValorPresupuestoEstimadoSolicitante;

                            vRegistroSolicitudEstudioSyC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? (vDataReaderResults["UsuarioCrea"].ToString()) : vRegistroSolicitudEstudioSyC.UsuarioCrea;

                            vRegistroSolicitudEstudioSyC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRegistroSolicitudEstudioSyC.FechaCrea;

                            vRegistroSolicitudEstudioSyC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? (vDataReaderResults["UsuarioModifica"].ToString()) : vRegistroSolicitudEstudioSyC.UsuarioModifica;

                            vRegistroSolicitudEstudioSyC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRegistroSolicitudEstudioSyC.FechaModifica;

                            vRegistroSolicitudEstudioSyC.TipoEstudio = vDataReaderResults["TipoEstudio"] != DBNull.Value ? (vDataReaderResults["TipoEstudio"].ToString()) : vRegistroSolicitudEstudioSyC.TipoEstudio;

                            vRegistroSolicitudEstudioSyC.ComplejidadInterna = vDataReaderResults["ComplejidadInterna"] != DBNull.Value ? (vDataReaderResults["ComplejidadInterna"].ToString()) : vRegistroSolicitudEstudioSyC.ComplejidadInterna;

                            vRegistroSolicitudEstudioSyC.ComplejidadIndicador = vDataReaderResults["ComplejidadIndicador"] != DBNull.Value ? (vDataReaderResults["ComplejidadIndicador"].ToString()) : vRegistroSolicitudEstudioSyC.ComplejidadIndicador;

                            vRegistroSolicitudEstudioSyC.ResponsableES = vDataReaderResults["ResponsableES"] != DBNull.Value ? (vDataReaderResults["ResponsableES"].ToString()) : vRegistroSolicitudEstudioSyC.ResponsableES;

                            vRegistroSolicitudEstudioSyC.ResponsableEC = vDataReaderResults["ResponsableEC"] != DBNull.Value ? (vDataReaderResults["ResponsableEC"].ToString()) : vRegistroSolicitudEstudioSyC.ResponsableEC;

                            vRegistroSolicitudEstudioSyC.AreaSolicitante = vDataReaderResults["AreaSolicitante"] != DBNull.Value ? (vDataReaderResults["AreaSolicitante"].ToString()) : vRegistroSolicitudEstudioSyC.AreaSolicitante;

                            vRegistroSolicitudEstudioSyC.MotivoSolicitud = vDataReaderResults["Motivo"] != DBNull.Value ? (vDataReaderResults["Motivo"].ToString()) : vRegistroSolicitudEstudioSyC.MotivoSolicitud;

                            vRegistroSolicitudEstudioSyC.ModalidadSeleccion = vDataReaderResults["ModalidadSeleccion"] != DBNull.Value ? (vDataReaderResults["ModalidadSeleccion"].ToString()) : vRegistroSolicitudEstudioSyC.ModalidadSeleccion;

                            vRegistroSolicitudEstudioSyC.EstadoSolicitud = vDataReaderResults["EstadoSolicitud"] != DBNull.Value ? (vDataReaderResults["EstadoSolicitud"].ToString()) : vRegistroSolicitudEstudioSyC.EstadoSolicitud;

                            vLstRegistro.Add(vRegistroSolicitudEstudioSyC);

                        }
                        return vLstRegistro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar información proceso selección por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de InformacionProcesoSeleccion</returns>
        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccion(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_ProcesoSeleccion_By_IdConsecutivoEstudio_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InformacionProcesoSeleccion> vLstProceso = new List<InformacionProcesoSeleccion>();
                        while (vDataReaderResults.Read())
                        {
                            InformacionProcesoSeleccion res = new InformacionProcesoSeleccion();
                            res.IdInformacionProcesoSeleccion = vDataReaderResults["IdInformacionProcesoSeleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInformacionProcesoSeleccion"].ToString()) : res.IdInformacionProcesoSeleccion;
                            res.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : res.IdConsecutivoEstudio;
                            res.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : res.IdContrato;
                            res.NumeroDelProceso = vDataReaderResults["NumeroDelProceso"] != DBNull.Value ? vDataReaderResults["NumeroDelProceso"].ToString() : res.NumeroDelProceso;
                            res.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : res.ObjetoContrato;
                            res.EstadoDelProceso = vDataReaderResults["EstadoDelProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoDelProceso"].ToString()) : res.EstadoDelProceso;
                            res.FechaAdjudicacionDelProceso = vDataReaderResults["FechaAdjudicacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaAdjudicacion"].ToString()) : res.FechaAdjudicacionDelProceso;
                            res.Contratista = vDataReaderResults["Contratista"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Contratista"].ToString()) : res.Contratista;
                            res.ValorPresupuestadoInicial = vDataReaderResults["ValorPresupuestadoInicial"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestadoInicial"].ToString()) : res.ValorPresupuestadoInicial;
                            res.ValorAdjudicado = vDataReaderResults["ValorAdjudicado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorAdjudicado"].ToString()) : res.ValorAdjudicado;
                            res.PlazoDeEjecucion = vDataReaderResults["PlazoDeEjecucion"] != DBNull.Value ? vDataReaderResults["PlazoDeEjecucion"].ToString() : res.PlazoDeEjecucion;
                            res.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observacion"].ToString()) : res.Observacion;
                            res.URLDelProceso = vDataReaderResults["URLDelProceso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["URLDelProceso"].ToString()) : res.URLDelProceso;

                            vLstProceso.Add(res);
                        }
                        return vLstProceso;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de BitacoraAcciones</returns>
        public List<BitacoraAcciones> ConsultarBitacoraAccion(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Bitacora_By_IdConsecutivoEstudio_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<BitacoraAcciones> vLstBitacora = new List<BitacoraAcciones>();
                        while (vDataReaderResults.Read())
                        {
                            BitacoraAcciones res = new BitacoraAcciones();
                            res.IdBitacoraEstudio = vDataReaderResults["IdBitacoraEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdBitacoraEstudio"].ToString()) : res.IdBitacoraEstudio;
                            res.IdBitacoraAcciones = vDataReaderResults["IdBitacoraAccion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdBitacoraAccion"].ToString()) : res.IdBitacoraAcciones;
                            res.UltimaAccion = vDataReaderResults["UltimaAccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UltimaAccion"].ToString()) : res.UltimaAccion;
                            res.ProximaAccion = vDataReaderResults["ProximaAccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProximaAccion"].ToString()) : res.ProximaAccion;
                            res.FechaRegistro = vDataReaderResults["FechaRegistro"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRegistro"].ToString()) : res.FechaRegistro;
                            res.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : res.FechaModificacion;

                            vLstBitacora.Add(res);
                        }
                        return vLstBitacora;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de TiempoEntreActividadesConsulta</returns>
        public List<TiempoEntreActividadesConsulta> ConsultarTiempos(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Tiempos_By_IdConsecutivoEstudio_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TiempoEntreActividadesConsulta> vLstTiempos = new List<TiempoEntreActividadesConsulta>();
                        while (vDataReaderResults.Read())
                        {
                            TiempoEntreActividadesConsulta res = new TiempoEntreActividadesConsulta();
                            ////tiempos SyC
                            res.IdTiempoEntreActividades = vDataReaderResults["IdTiempoEntreActividades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTiempoEntreActividades"].ToString()) : res.IdTiempoEntreActividades;
                            res.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : res.IdConsecutivoEstudio;
                            res.FentregaES_ECTiemposEquipo = vDataReaderResults["FentregaES_ECTiemposEquipo"] != DBNull.Value ? (vDataReaderResults["FentregaES_ECTiemposEquipo"].ToString()) : res.FentregaES_ECTiemposEquipo;
                            res.FEentregaES_ECTiemposIndicador = vDataReaderResults["FEentregaES_ECTiemposIndicador"] != DBNull.Value ? (vDataReaderResults["FEentregaES_ECTiemposIndicador"].ToString()) : res.FEentregaES_ECTiemposIndicador;
                            res.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado = vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"] != DBNull.Value ? (vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"].ToString()) : res.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
                            res.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR = vDataReaderResults["NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR"].ToString()) : res.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR;
                            res.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor = vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor"].ToString()) : res.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor;
                            res.DiasHabilesEntreFCTFinalYSDC = vDataReaderResults["DiasHabilesEntreFCTFinalYSDC"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEntreFCTFinalYSDC"].ToString()) : res.DiasHabilesEntreFCTFinalYSDC;
                            res.NoAplicaDiasHabilesEntreFCTFinalYSDCR = vDataReaderResults["NoAplicaDiasHabilesEntreFCTFinalYSDCR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesEntreFCTFinalYSDCR"].ToString()) : res.NoAplicaDiasHabilesEntreFCTFinalYSDCR;
                            res.DiasHabilesEntreFCTFinalYSDCColor = vDataReaderResults["DiasHabilesEntreFCTFinalYSDCColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEntreFCTFinalYSDCColor"].ToString()) : res.DiasHabilesEntreFCTFinalYSDCColor;
                            res.DiasHabilesEnESOCOSTEO = vDataReaderResults["DiasHabilesEnESOCOSTEO"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEnESOCOSTEO"].ToString()) : res.DiasHabilesEnESOCOSTEO;
                            res.NoAplicaDiasHabilesEnESOCOSTEOR = vDataReaderResults["NoAplicaDiasHabilesEnESOCOSTEOR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesEnESOCOSTEOR"].ToString()) : res.NoAplicaDiasHabilesEnESOCOSTEOR;
                            res.DevueltoDiasHabilesEnESOCOSTEOR = vDataReaderResults["DevueltoDiasHabilesEnESOCOSTEOR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["DevueltoDiasHabilesEnESOCOSTEOR"].ToString()) : res.DevueltoDiasHabilesEnESOCOSTEOR;
                            res.DiasHabilesEnESOCOSTEOColor = vDataReaderResults["DiasHabilesEnESOCOSTEOColor"] != DBNull.Value ? (vDataReaderResults["DiasHabilesEnESOCOSTEOColor"].ToString()) : res.DiasHabilesEnESOCOSTEOColor;
                            res.DiashabilesParaDevolucion = vDataReaderResults["DiashabilesParaDevolucion"] != DBNull.Value ? (vDataReaderResults["DiashabilesParaDevolucion"].ToString()) : res.DiashabilesParaDevolucion;
                            res.NoAplicaDiasHabilesParaDevolucionR = vDataReaderResults["NoAplicaDiasHabilesParaDevolucionR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaDiasHabilesParaDevolucionR"].ToString()) : res.NoAplicaDiasHabilesParaDevolucionR;
                            res.DiashabilesParaDevolucionColor = vDataReaderResults["DiashabilesParaDevolucionColor"] != DBNull.Value ? (vDataReaderResults["DiashabilesParaDevolucionColor"].ToString()) : res.DiashabilesParaDevolucionColor;

                            ////tiemposESC

                            res.IdTiempoEntreActividades = vDataReaderResults["IdTiempoEntreActividades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTiempoEntreActividades"].ToString()) : res.IdTiempoEntreActividades;
                            res.IdConsecutivoEstudio = vDataReaderResults["IdConsecutivoEstudio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdConsecutivoEstudio"].ToString()) : res.IdConsecutivoEstudio;
                            res.FRealFCTPreliminarESC = vDataReaderResults["FRealFCTPreliminarESC"] != DBNull.Value ? (vDataReaderResults["FRealFCTPreliminarESC"].ToString()) : res.FRealFCTPreliminarESC;
                            res.NoAplicaFRealFCTPreliminarESC = vDataReaderResults["NoAplicaFRealFCTPreliminarESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFRealFCTPreliminarESC"].ToString()) : res.NoAplicaFRealFCTPreliminarESC;
                            res.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"].ToString()) : res.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
                            res.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"].ToString()) : res.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
                            res.FEstimadaES_EC_ESC = vDataReaderResults["FEstimadaES_EC_ESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaES_EC_ESC"].ToString()) : res.FEstimadaES_EC_ESC;
                            res.NoAplicaFEstimadaES_EC_ESC = vDataReaderResults["NoAplicaFEstimadaES_EC_ESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaES_EC_ESC"].ToString()) : res.NoAplicaFEstimadaES_EC_ESC;
                            res.FEstimadaDocsRadicadosEnContratosESC = vDataReaderResults["FEstimadaDocsRadicadosEnContratosESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaDocsRadicadosEnContratosESC"].ToString()) : res.FEstimadaDocsRadicadosEnContratosESC;
                            res.NoAplicaFEstimadaDocsRadicadosEnContratosESC = vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratosESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratosESC"].ToString()) : res.NoAplicaFEstimadaDocsRadicadosEnContratosESC;
                            res.FEstimadaComitecontratacionESC = vDataReaderResults["FEstimadaComitecontratacionESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaComitecontratacionESC"].ToString()) : res.FEstimadaComitecontratacionESC;
                            res.NoAplicaFEstimadaComitecontratacionESC = vDataReaderResults["NoAplicaFEstimadaComitecontratacionESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaComitecontratacionESC"].ToString()) : res.NoAplicaFEstimadaComitecontratacionESC;
                            res.FEstimadaInicioDelPS_ESC = vDataReaderResults["FEstimadaInicioDelPS_ESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaInicioDelPS_ESC"].ToString()) : res.FEstimadaInicioDelPS_ESC;
                            res.NoAplicaFEstimadaInicioDelPS_ESC = vDataReaderResults["NoAplicaFEstimadaInicioDelPS_ESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaInicioDelPS_ESC"].ToString()) : res.NoAplicaFEstimadaInicioDelPS_ESC;
                            res.FEstimadaPresentacionPropuestasESC = vDataReaderResults["FEstimadaPresentacionPropuestasESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaPresentacionPropuestasESC"].ToString()) : res.FEstimadaPresentacionPropuestasESC;
                            res.NoAplicaFEstimadaPresentacionPropuestasESC = vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestasESC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestasESC"].ToString()) : res.NoAplicaFEstimadaPresentacionPropuestasESC;
                            res.FEstimadaDeInicioDeEjecucionESC = vDataReaderResults["FEstimadaDeInicioDeEjecucionESC"] != DBNull.Value ? (vDataReaderResults["FEstimadaDeInicioDeEjecucionESC"].ToString()) : res.FEstimadaDeInicioDeEjecucionESC;

                            ////fechas reales

                            res.FRealFCTPreliminar = vDataReaderResults["FRealFCTPreliminar"] != DBNull.Value ? (vDataReaderResults["FRealFCTPreliminar"].ToString()) : res.FRealFCTPreliminar;
                            res.FRealDocsRadicadosEnContratos = vDataReaderResults["FRealDocsRadicadosEnContratos"] != DBNull.Value ? (vDataReaderResults["FRealDocsRadicadosEnContratos"].ToString()) : res.FRealDocsRadicadosEnContratos;
                            res.FRealFCTDefinitivaParaMCFRealES_MCRevisado = vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"] != DBNull.Value ? (vDataReaderResults["FRealFCTDefinitivaParaMCFRealES_MCRevisado"].ToString()) : res.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
                            res.FRealComiteContratacion = vDataReaderResults["FRealComiteContratacion"] != DBNull.Value ? (vDataReaderResults["FRealComiteContratacion"].ToString()) : res.FRealComiteContratacion;
                            res.FRealES_EC = vDataReaderResults["FRealES_EC"] != DBNull.Value ? (vDataReaderResults["FRealES_EC"].ToString()) : res.FRealES_EC;
                            res.FRealInicioPS = vDataReaderResults["FRealInicioPS"] != DBNull.Value ? (vDataReaderResults["FRealInicioPS"].ToString()) : res.FRealInicioPS;
                            res.FRealPresentacionPropuestas = vDataReaderResults["FRealPresentacionPropuestas"] != DBNull.Value ? (vDataReaderResults["FRealPresentacionPropuestas"].ToString()) : res.FRealPresentacionPropuestas;


                            //tiempos PACCO
                            res.FEstimadaFCTPreliminarREGINICIAL = vDataReaderResults["FEstimadaFCTPreliminarREGINICIAL"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTPreliminarREGINICIAL"].ToString()) : res.FEstimadaFCTPreliminarREGINICIAL;
                            res.NoAplicaFEstimadaFCTPreliminarREGINICIAL = vDataReaderResults["NoAplicaFEstimadaFCTPreliminarREGINICIAL"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTPreliminarREGINICIAL"].ToString()) : res.NoAplicaFEstimadaFCTPreliminarREGINICIAL;
                            res.FEstimadaFCTPreliminar = vDataReaderResults["FEstimadaFCTPreliminar"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTPreliminar"].ToString()) : res.FEstimadaFCTPreliminar;
                            res.NoAplicaFEstimadaFCTPreliminar = vDataReaderResults["NoAplicaFEstimadaFCTPreliminar"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTPreliminar"].ToString()) : res.NoAplicaFEstimadaFCTPreliminar;
                            res.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"] != DBNull.Value ? (vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"].ToString()) : res.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                            res.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"].ToString()) : res.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                            res.FEestimadaES_EC = vDataReaderResults["FEestimadaES_EC"] != DBNull.Value ? (vDataReaderResults["FEestimadaES_EC"].ToString()) : res.FEestimadaES_EC;
                            res.NoAplicaFEestimadaES_EC = vDataReaderResults["NoAplicaFEestimadaES_EC"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEestimadaES_EC"].ToString()) : res.NoAplicaFEestimadaES_EC;
                            res.FEstimadaDocsRadicadosEnContratos = vDataReaderResults["FEstimadaDocsRadicadosEnContratos"] != DBNull.Value ? (vDataReaderResults["FEstimadaDocsRadicadosEnContratos"].ToString()) : res.FEstimadaDocsRadicadosEnContratos;
                            res.NoAplicaFEstimadaDocsRadicadosEnContratos = vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaDocsRadicadosEnContratos"].ToString()) : res.NoAplicaFEstimadaDocsRadicadosEnContratos;
                            res.FEstimadaComiteContratacion = vDataReaderResults["FEstimadaComiteContratacion"] != DBNull.Value ? (vDataReaderResults["FEstimadaComiteContratacion"].ToString()) : res.FEstimadaComiteContratacion;
                            res.NoAplicaFEstimadaComiteContratacion = vDataReaderResults["NoAplicaFEstimadaComiteContratacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaComiteContratacion"].ToString()) : res.NoAplicaFEstimadaComiteContratacion;
                            res.FEstimadaInicioDelPS = vDataReaderResults["FEstimadaInicioDelPS"] != DBNull.Value ? (vDataReaderResults["FEstimadaInicioDelPS"].ToString()) : res.FEstimadaInicioDelPS;
                            res.NoAplicaFEstimadaInicioDelPS = vDataReaderResults["NoAplicaFEstimadaInicioDelPS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaInicioDelPS"].ToString()) : res.NoAplicaFEstimadaInicioDelPS;
                            res.FEstimadaPresentacionPropuestas = vDataReaderResults["FEstimadaPresentacionPropuestas"] != DBNull.Value ? (vDataReaderResults["FEstimadaPresentacionPropuestas"].ToString()) : res.FEstimadaPresentacionPropuestas;
                            res.NoAplicaFEstimadaPresentacionPropuestas = vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestas"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["NoAplicaFEstimadaPresentacionPropuestas"].ToString()) : res.NoAplicaFEstimadaPresentacionPropuestas;
                            res.FEstimadaDeInicioDeEjecucion = vDataReaderResults["FEstimadaDeInicioDeEjecucion"] != DBNull.Value ? (vDataReaderResults["FEstimadaDeInicioDeEjecucion"].ToString()) : res.FEstimadaDeInicioDeEjecucion;
                            vLstTiempos.Add(res);
                        }
                        return vLstTiempos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de BitacoraAcciones</returns>
        public List<ResultadoEstudioSector> ConsultarResultado(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_Resultado_By_IdConsecutivoEstudio_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRegistros", DbType.Int32, pNumeroRegistros);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ResultadoEstudioSector> vLstResultados = new List<ResultadoEstudioSector>();
                        while (vDataReaderResults.Read())
                        {
                            ResultadoEstudioSector res = new ResultadoEstudioSector();

                            res.IdConsecutivoResultadoEstudio = vDataReaderResults["IdResultadoEstudio"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdResultadoEstudio"].ToString()) : res.IdConsecutivoResultadoEstudio;
                            res.IdConsecutivoEstudio = vDataReaderResults["IdRegistroSolicitudEstudio"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["IdRegistroSolicitudEstudio"].ToString()) : res.IdConsecutivoEstudio;
                            res.DocumentosRevisadosNAS = vDataReaderResults["DocumentosRevisadosNAS"] != DBNull.Value ? (vDataReaderResults["DocumentosRevisadosNAS"].ToString()) : res.DocumentosRevisadosNAS;
                            res.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : res.Fecha;
                            res.IdRevisadoPor = vDataReaderResults["IdRevisadoPor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRevisadoPor"].ToString()) : res.IdRevisadoPor;
                            res.RevisadoPor = vDataReaderResults["RevisadoPor"] != DBNull.Value ? (vDataReaderResults["RevisadoPor"].ToString()) : res.RevisadoPor;
                            res.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? (vDataReaderResults["Observacion"].ToString()) : res.Observacion;
                            res.Consultados = vDataReaderResults["ProveedoresConsultados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ProveedoresConsultados"].ToString()) : res.Consultados;
                            res.Participantes = vDataReaderResults["ProveedoresParticipantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ProveedoresParticipantes"].ToString()) : res.Participantes;
                            res.CotizacionesRecibidas = vDataReaderResults["CotizacionesRecibidas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CotizacionesRecibidas"].ToString()) : res.CotizacionesRecibidas;
                            res.CotizacionesParaPresupuesto = vDataReaderResults["CotizacionesParaPresupuesto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CotizacionesParaPresupuesto"].ToString()) : res.CotizacionesParaPresupuesto;
                            res.RadicadoOficioEntregaESyC = vDataReaderResults["RadicadoOficioEntregaESyC"] != DBNull.Value ? (vDataReaderResults["RadicadoOficioEntregaESyC"].ToString()) : res.RadicadoOficioEntregaESyC;
                            res.FechaDeEntregaESyC = vDataReaderResults["FechaDeEntregaESyC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaDeEntregaESyC"].ToString()) : res.FechaDeEntregaESyC;
                            res.TipoDeValor = vDataReaderResults["TipoDeValor"] != DBNull.Value ? (vDataReaderResults["TipoDeValor"].ToString()) : res.TipoDeValor;
                            res.ValorPresupuestoESyC = vDataReaderResults["ValorPresupuestoESyC"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorPresupuestoESyC"].ToString()) : res.ValorPresupuestoESyC;
                            res.IndiceLiquidez = vDataReaderResults["IndiceDeLiquidez"] != DBNull.Value ? (vDataReaderResults["IndiceDeLiquidez"].ToString()) : res.IndiceLiquidez;
                            res.NivelDeEndeudamiento = vDataReaderResults["NivelDeEndeudamiento"] != DBNull.Value ? (vDataReaderResults["NivelDeEndeudamiento"].ToString()) : res.NivelDeEndeudamiento;
                            res.RazonDeCoberturaDeIntereses = vDataReaderResults["RazonDeCoberturaDeIntereses"] != DBNull.Value ? (vDataReaderResults["RazonDeCoberturaDeIntereses"].ToString()) : res.RazonDeCoberturaDeIntereses;
                            res.CapitalDeTrabajo = vDataReaderResults["CapitalDeTrabajo"] != DBNull.Value ? (vDataReaderResults["CapitalDeTrabajo"].ToString()) : res.CapitalDeTrabajo;
                            res.RentabilidadDelPatrimonio = vDataReaderResults["RentabilidadDelPatrimonio"] != DBNull.Value ? (vDataReaderResults["RentabilidadDelPatrimonio"].ToString()) : res.RentabilidadDelPatrimonio;
                            res.RentabilidadDelActivo = vDataReaderResults["RentabilidadDelActivo"] != DBNull.Value ? (vDataReaderResults["RentabilidadDelActivo"].ToString()) : res.RentabilidadDelActivo;
                            res.TotalPatrimonio = vDataReaderResults["TotalPatrimonio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalPatrimonio"].ToString()) : res.TotalPatrimonio;
                            res.KResidual = vDataReaderResults["KResidual"] != DBNull.Value ? (vDataReaderResults["KResidual"].ToString()) : res.KResidual;

                            vLstResultados.Add(res);
                        }
                        return vLstResultados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
