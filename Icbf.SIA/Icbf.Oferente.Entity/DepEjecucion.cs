﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Clase DepEjecucion
    /// </summary>
    public class DepEjecucion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdDepEjecucion
        /// </summary>
        public int IdDepEjecucion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdExpEntidad
        /// </summary>
        public int IdExpEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDepartamento
        /// </summary>
        public int IdDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreDepartamento
        /// </summary>
        public string NombreDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Cupos
        /// </summary>
        public int Cupos
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase DepEjecucion
        /// </summary>
        public DepEjecucion()
        {
        }
    }
}