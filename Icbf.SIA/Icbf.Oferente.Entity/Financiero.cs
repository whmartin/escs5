using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase Financiero
    /// </summary>
    public class Financiero : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IDInfoFin
        /// </summary>
        public int IDInfoFin
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdOferente
        /// </summary>
        public int IdOferente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Anio
        /// </summary>
        public int Anio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad AcnoVigencia
        /// </summary>
        public string AcnoVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ActivoCorriente
        /// </summary>
        public Decimal ActivoCorriente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ActivoFijo
        /// </summary>
        public Decimal ActivoFijo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PasivoCorriente
        /// </summary>
        public Decimal PasivoCorriente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PasivoLargoPlazo
        /// </summary>
        public Decimal PasivoLargoPlazo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Patrimonio
        /// </summary>
        public Decimal Patrimonio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Ingresosporservicios
        /// </summary>
        public Decimal Ingresosporservicios
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ContratosICBFPrimeraInfancia
        /// </summary>
        public Decimal ContratosICBFPrimeraInfancia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ContratosICBFDiferentePrimeraInfancia
        /// </summary>
        public Decimal ContratosICBFDiferentePrimeraInfancia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ContratoOtrasEntidades
        /// </summary>
        public Decimal ContratoOtrasEntidades
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CostosServicios
        /// </summary>
        public Decimal CostosServicios
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UtilidadBruta
        /// </summary>
        public Decimal UtilidadBruta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad GastosOperacion
        /// </summary>
        public Decimal GastosOperacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad OtrosIngresos
        /// </summary>
        public Decimal OtrosIngresos
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad GastosFinancieros
        /// </summary>
        public Decimal GastosFinancieros
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad OtrosEgresos
        /// </summary>
        public Decimal OtrosEgresos
        {
            get;
            set;
        }
        /// <summary>
        /// Propeidad ProvisionImpuestosRentas
        /// </summary>
        public Decimal ProvisionImpuestosRentas
        {
            get;
            set;
        }
        /// <summary>
        /// Proopiedad ObligacionesFinancieras
        /// </summary>
        public Decimal ObligacionesFinancieras
        {
            get;
            set;
        }
        /// <summary>
        ///  propiedad TotalActivo
        /// </summary>
        public Decimal TotalActivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TotalPasivo
        /// </summary>
        public Decimal TotalPasivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TotalPasivoPatrimonio
        /// </summary>
        public Decimal TotalPasivoPatrimonio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UtilidadOperativa
        /// </summary>
        public Decimal UtilidadOperativa
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad totalOtrosIngresosyEgresos
        /// </summary>
        public Decimal totalOtrosIngresosyEgresos
        {
            get;
            set;
        }
        /// <summary>
        /// Propieda UtilidadAntesImpuestos
        /// </summary>
        public Decimal UtilidadAntesImpuestos
        {
            get;
            set;
        }
        /// <summary>
        /// Prpiedad UtilidadAntesDeImpuestos
        /// </summary>
        public Decimal UtilidadAntesDeImpuestos
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UtilidadNeta
        /// </summary>
        public Decimal UtilidadNeta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UtilidadBrutaTotal
        /// </summary>
        public Decimal UtilidadBrutaTotal
        {
            get;
            set;
        }
       /// <summary>
        /// Propiedad RUP
       /// </summary>
        public Boolean RUP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroRUP
        /// </summary>
        public int NumeroRUP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Prpiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase Financiero
        /// </summary>
        public Financiero()
        {
        }
    }
}
