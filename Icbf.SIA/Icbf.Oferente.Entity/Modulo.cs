﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase Modulo
    /// </summary>
    public class Modulo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdModulo
        /// </summary>
        public int IdModulo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreModulo
        /// </summary>
        public String NombreModulo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Orden
        /// </summary>
        public int Orden
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoModulo
        /// </summary>
        public int CodigoModulo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public String Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Ruta
        /// </summary>
        public String Ruta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase Modulo
        /// </summary>
        public Modulo()
        {
        }
    }
}
