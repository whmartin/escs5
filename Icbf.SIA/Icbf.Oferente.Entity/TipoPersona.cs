using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TipoPersona
    /// </summary>
    public class TipoPersona : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoPersona 
        /// </summary>
        public int IdTipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoPersona
        /// </summary>
        public String CodigoTipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoPersona
        /// </summary>
        public String NombreTipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase TipoPersona
        /// </summary>
        public TipoPersona()
        {
        }
    }
}
