using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Clase  AdministrativoDocumento
    /// </summary>
    public class AdministrativoDocumento : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdAdministrativoDocumento
        /// </summary>
        public int IdAdministrativoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdAdministrativo
        /// </summary>
        public int IdAdministrativo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoDocAdministrativo
        /// </summary>
        public int IdTipoDocAdministrativo
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad IdArchivo
        /// </summary>
        public Decimal IdArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Metodo constructor AdministrativoDocumento
        /// </summary>
        public AdministrativoDocumento()
        {
        }
    }
}
