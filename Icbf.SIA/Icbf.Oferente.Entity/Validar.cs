using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase Validar
    /// </summary>
    public class Validar : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdOferente
        /// </summary>
        public int IdOferente
        {
            get;
            set;
        }      
        /// <summary>
        /// Propiedad IdTercero
        /// </summary>
        public int IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoIdentificacion
        /// </summary>
        public int IdTipoIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoIdentificacion
        /// </summary>
        public String TipoIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacion
        /// </summary>
        public String NumeroIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Oferente
        /// </summary>
        public String Oferente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoPersona
        /// </summary>
        public String TipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoGuardado
        /// </summary>
        public String EstadoGuardado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoValidacion
        /// </summary>
        public String EstadoValidacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DesEstadoGuardado
        /// </summary>
        public String DesEstadoGuardado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DesEstadoValidacion
        /// </summary>
        public String DesEstadoValidacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioProtege
        /// </summary>
        public String UsuarioProtege
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase Validar
        /// </summary>
        public Validar()
        {
        }
    }
}
