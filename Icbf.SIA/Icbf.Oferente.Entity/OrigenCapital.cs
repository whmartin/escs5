using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase OrigenCapital
    /// </summary>
    public class OrigenCapital : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdOrigenCapital
        /// </summary>
        public int IdOrigenCapital
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoOrigenCapital
        /// </summary>
        public String CodigoOrigenCapital
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreOrigenCapital
        /// </summary>
        public String NombreOrigenCapital
        {
            get;
            set;
        }
        /// <summary>
        /// Propiead Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase OrigenCapital
        /// </summary>
        public OrigenCapital()
        {
        }
    }
}
