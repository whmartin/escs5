using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TipoOrganizacion
    /// </summary>
    public class TipoOrganizacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoOrganizacion
        /// </summary>
        public int IdTipoOrganizacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoOrganizacion
        /// </summary>
        public String CodigoTipoOrganizacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoOrganizacion
        /// </summary>
        public String NombreTipoOrganizacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase TipoOrganizacion
        /// </summary>
        public TipoOrganizacion()
        {
        }
    }
}
