using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Clase Administrativo
    /// </summary>
    public class Administrativo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdAdministrativo
        /// </summary>
        public int IdAdministrativo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public Decimal IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdVigencia
        /// </summary>
        public int IdVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Organigrama
        /// </summary>
        public Boolean Organigrama
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TotalPnalAnnoPrevio
        /// </summary>
        public Decimal TotalPnalAnnoPrevio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad VincLaboral
        /// </summary>
        public Decimal VincLaboral
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrestServicios
        /// </summary>
        public Decimal PrestServicios
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Voluntariado
        /// </summary>
        public Decimal Voluntariado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad VoluntPermanente
        /// </summary>
        public Decimal VoluntPermanente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Asociados
        /// </summary>
        public Decimal Asociados
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Mision
        /// </summary>
        public Decimal Mision
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PQRS
        /// </summary>
        public Boolean PQRS
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad GestionDocumental
        /// </summary>
        public Boolean GestionDocumental
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad AuditoriaInterna
        /// </summary>
        public Boolean AuditoriaInterna
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ManProcedimiento
        /// </summary>
        public Boolean ManProcedimiento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ManPracticasAmbiente
        /// </summary>
        public Boolean ManPracticasAmbiente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ManComportOrg
        /// </summary>
        public Boolean ManComportOrg
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ManFunciones
        /// </summary>
        public Boolean ManFunciones
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ProcRegInfoContable
        /// </summary>
        public Boolean ProcRegInfoContable
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PartMesasTerritoriales
        /// </summary>
        public Boolean PartMesasTerritoriales
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PartAsocAgremia
        /// </summary>
        public Boolean PartAsocAgremia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PartConsejosComun
        /// </summary>
        public Boolean PartConsejosComun
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConvInterInst
        /// </summary>
        public Boolean ConvInterInst
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ProcSeleccGral
        /// </summary>
        public Boolean ProcSeleccGral
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ProcSeleccEtnico
        /// </summary>
        public Boolean ProcSeleccEtnico
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PlanInduccCapac
        /// </summary>
        public Boolean PlanInduccCapac
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EvalDesemp
        /// </summary>
        public Boolean EvalDesemp
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PlanCualificacion
        /// </summary>
        public Boolean PlanCualificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase Administrativo
        /// </summary>
        public Administrativo()
        {
        }
    }
}
