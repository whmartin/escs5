using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TelTerceros
    /// </summary>
    public class TelTerceros : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTelTercero
        /// </summary>
        public int IdTelTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTercero
        /// </summary>
        public int? IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IndicativoTelefono
        /// </summary>
        public Int64? IndicativoTelefono
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroTelefono
        /// </summary>
        public long? NumeroTelefono
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ExtensionTelefono
        /// </summary>
        public long? ExtensionTelefono
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Movil
        /// </summary>
        public long? Movil
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IndicativoFax
        /// </summary>
        public int? IndicativoFax
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroFax
        /// </summary>
        public int? NumeroFax
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase TelTerceros
        /// </summary>
        public TelTerceros()
        {
        }
    }
}
