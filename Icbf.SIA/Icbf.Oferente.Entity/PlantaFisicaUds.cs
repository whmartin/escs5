using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase PlantaFisicaUds
    /// </summary>
    public class PlantaFisicaUds : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdPlantaFisicaUds
        /// </summary>
        public int IdPlantaFisicaUds
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoPlantaFisicaUds
        /// </summary>
        public String CodigoPlantaFisicaUds
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombrePlantaFisicaUds
        /// </summary>
        public String NombrePlantaFisicaUds
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Cosntructor de clase PlantaFisicaUds
        /// </summary>
        public PlantaFisicaUds()
        {
        }
    }
}
