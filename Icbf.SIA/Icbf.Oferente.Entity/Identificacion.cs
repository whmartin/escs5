using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase Identificacion
    /// </summary>
    public class Identificacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad Identificacion_
        /// </summary>
        public int Identificacion_
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Ano
        /// </summary>
        public String Ano
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad NombreEntidadOferente
        /// </summary>
        public String NombreEntidadOferente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Sigla
        /// </summary>
        public String Sigla
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Tipodeidentificación
        /// </summary>
        public String Tipodeidentificación
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NúmeroIdentificación
        /// </summary>
        public String NúmeroIdentificación
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DV
        /// </summary>
        public String DV
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Tipopersona
        /// </summary>
        public String Tipopersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreEntidadAcreditaExistencia
        /// </summary>
        public String NombreEntidadAcreditaExistencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoRégimenTributario
        /// </summary>
        public String TipoRégimenTributario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad OrigenCapital
        /// </summary>
        public String OrigenCapital 
        {
            get;
            set;
        }
        /// <summary>
        ///´Propiedad PorcentajePrivado
        /// </summary>
        public String PorcentajePrivado 
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Porcentajepublico
        /// </summary>
        public String Porcentajepublico 
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoOrganización
        /// </summary>
        public String TipoOrganización 
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Naturalezajurídica
        /// </summary>
        public String Naturalezajurídica 
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NúmeroTotalTrabajadores
        /// </summary>
        public String NúmeroTotalTrabajadores 
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ValoresActivosTotales
        /// </summary>
        public String ValoresActivosTotales 
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDListaTipoDocumento
        /// </summary>
        public int IdDListaTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacion
        /// </summary>
        public String NumeroIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerNombre
        /// </summary>
        public String PrimerNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoNombre
        /// </summary>
        public String SegundoNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerApellido
        /// </summary>
        public String PrimerApellido
        {
            get;
            set;
        }
        /// <summary>
        /// prpiedad SegundoApellido
        /// </summary>
        public String SegundoApellido
        {
            get;
            set;
        }
        /// <summary>
        /// prpiedad Email
        /// </summary>
        public String Email
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DepartamentoResidencia
        /// </summary>
        public String DepartamentoResidencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad MunicipioResidencia
        /// </summary>
        public String MunicipioResidencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ZonaResidencia
        /// </summary>
        public String ZonaResidencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreZonaResto
        /// </summary>
        public String NombreZonaResto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CentroPoblado
        /// </summary>
        public String CentroPoblado
        {
            get;
            set;
        }
        /// <summary>
        ///¨Propiedad ComunaLocalidad
        /// </summary>
        public String ComunaLocalidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Barrio
        /// </summary>
        public String Barrio
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad Zona
        /// </summary>
        public String Zona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiead UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase Identificacion
        /// </summary>
        public Identificacion()
        {
        }
    }


    //public class TipoRegimenTributario : Icbf.Seguridad.Entity.EntityAuditoria
    //{


    //    public int IdTipoRegimenTributario
    //    {
    //        get;
    //        set;
    //    }
    //    public String CodigoTipoRegimenTributario
    //    {
    //        get;
    //        set;
    //    }
    //    public String NombreTipoRegimenTributario
    //    {
    //        get;
    //        set;
    //    }


    //    public String UsuarioCrea
    //    {
    //        get;
    //        set;
    //    }
    //    public String UsuarioModifica
    //    {
    //        get;
    //        set;
    //    }
    //    public DateTime FechaCrea
    //    {
    //        get;
    //        set;
    //    }
    //    public DateTime FechaModifica
    //    {
    //        get;
    //        set;
    //    }
    //    public TipoRegimenTributario()
    //    {
    //    }
    //}

    //public class OrigenCapital : Icbf.Seguridad.Entity.EntityAuditoria
    //{

    //    public int IdOrigenCapital
    //    {
    //        get;
    //        set;
    //    }
    //    public String CodigoOrigenCapital
    //    {
    //        get;
    //        set;
    //    }
    //    public String NombreOrigenCapital
    //    {
    //        get;
    //        set;
    //    }


    //    public String UsuarioCrea
    //    {
    //        get;
    //        set;
    //    }
    //    public String UsuarioModifica
    //    {
    //        get;
    //        set;
    //    }
    //    public DateTime FechaCrea
    //    {
    //        get;
    //        set;
    //    }
    //    public DateTime FechaModifica
    //    {
    //        get;
    //        set;
    //    }
    //    public OrigenCapital()
    //    {
    //    }
    //}

    //public class NaturalezaJuridica : Icbf.Seguridad.Entity.EntityAuditoria
    //{

    //    public int IdNaturalezaJuridica
    //    {
    //        get;
    //        set;
    //    }
    //    public String CodigoNaturalezaJuridica
    //    {
    //        get;
    //        set;
    //    }
    //    public String NombreNaturalezaJuridica
    //    {
    //        get;
    //        set;
    //    }


    //    public String UsuarioCrea
    //    {
    //        get;
    //        set;
    //    }
    //    public String UsuarioModifica
    //    {
    //        get;
    //        set;
    //    }
    //    public DateTime FechaCrea
    //    {
    //        get;
    //        set;
    //    }
    //    public DateTime FechaModifica
    //    {
    //        get;
    //        set;
    //    }
    //    public NaturalezaJuridica()
    //    {
    //    }
    //}


    /// <summary>
    /// Definición de clase EntigaGenericaParametros
    /// </summary>
        public class EntigaGenericaParametros : Icbf.Seguridad.Entity.EntityAuditoria
        {
            /// <summary>
            /// Propiedad Id
            /// </summary>
            public int Id
            {
                get;
                set;
            }
            /// <summary>
            /// Propiedad Codigo
            /// </summary>
            public String Codigo
            {
                get;
                set;
            }
            /// <summary>
            /// Propiead Descripcion
            /// </summary>
            public String Descripcion
            {
                get;
                set;
            }

            /// <summary>
            /// Propiead UsuarioCrea
            /// </summary>
            public String UsuarioCrea
            {
                get;
                set;
            }
            /// <summary>
            /// Propiead UsuarioModifica
            /// </summary>
            public String UsuarioModifica
            {
                get;
                set;
            }
            /// <summary>
            /// Propíedad FechaCrea
            /// </summary>
            public DateTime FechaCrea
            {
                get;
                set;
            }
            /// <summary>
            /// Propiedad FechaModifica
            /// </summary>
            public DateTime FechaModifica
            {
                get;
                set;
            }
            /// <summary>
            /// Constructor de clase EntigaGenericaParametros
            /// </summary>
            public EntigaGenericaParametros()
            {
            }
        }





}
