﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Clase CargaArchivoResult
    /// </summary>
    public class CargaArchivoResult
    {
        /// <summary>
        /// Campo NumErrors
        /// </summary>
        /// <value>Valor campo</value>
        public int NumErrors { get; set; }

        /// <summary>
        /// Campo DataResult
        /// </summary>
        /// <value>Valor campo</value>
        public DataTable DataResult { get; set; }

        /// <summary>
        /// Campo DatosCarga
        /// </summary>
        /// <value>Valor campo</value>
        public DatosGeneralesCarga DatosCarga { get; set; }
    }   
}
