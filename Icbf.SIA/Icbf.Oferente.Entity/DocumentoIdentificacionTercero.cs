using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase DocumentoIdentificacionTercero
    /// </summary>
    public class DocumentoIdentificacionTercero : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdDocumentoIdentificacionTercero
        /// </summary>
        public int IdDocumentoIdentificacionTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTercero
        /// </summary>
        public int IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDocumentoIdentificacion
        /// </summary>
        public int IdDocumentoIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Certificado
        /// </summary>
        public Boolean Certificado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoCertificado
        /// </summary>
        public int EstadoCertificado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdArchivo
        /// </summary>
        public int IdArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaExpedicion
        /// </summary>
        public Nullable< DateTime> FechaExpedicion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaExpiracion
        /// </summary>
        public Nullable< DateTime> FechaExpiracion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreArchivo
        /// </summary>
        public String NombreArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
       
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public int Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        ///Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase DocumentoIdentificacionTercero
        /// </summary>
        public DocumentoIdentificacionTercero()
        {
        }
    }
}
