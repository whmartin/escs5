﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class ConfiguracionTareas : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdConfigAsignacionTareas { get; set; }
        public int CupoMaximoPorPersona { get; set; }
        public decimal PorcentajeMaximoAlerta { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }

        public ConfiguracionTareas() {}
    }
}
