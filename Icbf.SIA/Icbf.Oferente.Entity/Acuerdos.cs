using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Clase Acuerdos
    /// </summary>
    public class Acuerdos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdAcuerdo
        /// </summary>
        public int IdAcuerdo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Nombre
        /// </summary>
        public String Nombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ContenidoHtml
        /// </summary>
        public String ContenidoHtml
        {
            get;
            set;
        }
        //Propiedad UsuarioCrea
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        ///  Constructor de clase Acuerdos
        /// </summary>
        public Acuerdos()
        {
        }
    }
}
