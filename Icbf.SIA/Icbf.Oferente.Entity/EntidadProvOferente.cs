using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase EntidadProvOferente
    /// </summary>
    public class EntidadProvOferente : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IDENTIDAD
        /// </summary>
        public int IDENTIDAD
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TIPOENTOFPROV
        /// </summary>
        public Nullable<bool> TIPOENTOFPROV
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTERCERO
        /// </summary>
        public Nullable<int> IDTERCERO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTERCEROREPLEGAL
        /// </summary>
        public Nullable<int> IDTERCEROREPLEGAL
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOCIIUPRINCIPAL
        /// </summary>
        public Nullable<int> IDTIPOCIIUPRINCIPAL
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOCIIUSECUNDARIO
        /// </summary>
        public Nullable<int> IDTIPOCIIUSECUNDARIO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOREGTRIB
        /// </summary>
        public Nullable<int> IDTIPOREGTRIB
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOSECTOR
        /// </summary>
        public Nullable<int> IDTIPOSECTOR
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOACTIVIDAD
        /// </summary>
        public Nullable<int> IDTIPOACTIVIDAD
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOCLASEENTIDAD
        /// </summary>
        public Nullable<int> IDTIPOCLASEENTIDAD
        {
            get;
            set;
        }
        /// <summary>
        ///Propiedad IDTIPORAMAPUBLICA
        /// </summary>
        public Nullable<int> IDTIPORAMAPUBLICA
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOENTIDAD
        /// </summary>
        public Nullable<int> IDTIPOENTIDAD
        {
            get;
            set;
        }
        /// <summary>
        /// Propeidad IDTIPONIVELGOB
        /// </summary>
        public Nullable<int> IDTIPONIVELGOB
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPONIVELORGANIZACIONAL
        /// </summary>
        public Nullable<int> IDTIPONIVELORGANIZACIONAL
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOORIGENCAPITAL
        /// </summary>
        public Nullable<int> IDTIPOORIGENCAPITAL
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad IDTIPOCERTIFICADORCALIDAD
        /// </summary>
        public Nullable<int> IDTIPOCERTIFICADORCALIDAD
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPOCERTIFICATAMANO
        /// </summary>
        public Nullable<int> IDTIPOCERTIFICATAMANO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPONATURALEZAJURID
        /// </summary>
        public Nullable<int> IDTIPONATURALEZAJURID
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTIPORANGOSACTIVOS
        /// </summary>
        public Nullable<int> IDTIPORANGOSACTIVOS
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FECHACIIUPRINCIPAL
        /// </summary>
        public Nullable<DateTime> FECHACIIUPRINCIPAL
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FECHACIIUSECUNDARIO
        /// </summary>
        public Nullable<DateTime> FECHACIIUSECUNDARIO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SITIOWEB
        /// </summary>
        public String SITIOWEB
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FECHACONSTITUCION
        /// </summary>
        public Nullable<DateTime> FECHACONSTITUCION
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FECHAVIGENCIA
        /// </summary>
        public Nullable<DateTime> FECHAVIGENCIA
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FECHAMATRICULAMERC
        /// </summary>
        public Nullable<DateTime> FECHAMATRICULAMERC
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FECHAEXPIRACION
        /// </summary>
        public Nullable<DateTime> FECHAEXPIRACION
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NOMBRECOMERCIAL
        /// </summary>
        public Nullable<DateTime> NOMBRECOMERCIAL
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EXENMATRICULAMER
        /// </summary>
        public Nullable<Boolean> EXENMATRICULAMER
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad MATRICULAMERCANTIL
        /// </summary>
        public String MATRICULAMERCANTIL
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad OBSERVALIDADOR
        /// </summary>
        public String OBSERVALIDADOR
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad INSCRITORUP
        /// </summary>
        public Nullable<Boolean> INSCRITORUP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NUMRUP
        /// </summary>
        public Nullable<int> NUMRUP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NOMBREENTIDADACREDITADORA
        /// </summary>
        public String NOMBREENTIDADACREDITADORA
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PORCTJPRIVADO
        /// </summary>
        public String PORCTJPRIVADO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PORCTJPUBLICO
        /// </summary>
        public String PORCTJPUBLICO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NUMTOTALTRABAJADORES
        /// </summary>
        public Nullable<int> NUMTOTALTRABAJADORES
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ANOREGISTRO
        /// </summary>
        public Nullable<int> ANOREGISTRO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SIGLA
        /// </summary>
        public String SIGLA
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad RUPOBALANCE
        /// </summary>
        public Nullable<Boolean> RUPOBALANCE
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ESPACIOARCHIVO
        /// </summary>
        public Nullable<Boolean> ESPACIOARCHIVO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ESPACIOMATOFICINA
        /// </summary>
        public Nullable<Boolean> ESPACIOMATOFICINA
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad EQUIPOCOMPUTO
        /// </summary>
        public Nullable<Boolean> EQUIPOCOMPUTO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SISTOPERATIVO
        /// </summary>
        public String SISTOPERATIVO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PAGINAWEB
        /// </summary>
        public String PAGINAWEB
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad CONLINEAFIJA
        /// </summary>
        public Nullable<Boolean> CONLINEAFIJA
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CONLINEACELULAR
        /// </summary>
        public Nullable<Boolean> CONLINEACELULAR
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CONEMAIL
        /// </summary>
        public Nullable<Boolean> CONEMAIL
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CONACCESOINTERNET
        /// </summary>
        public Nullable<Boolean> CONACCESOINTERNET
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NUMSEDES
        /// </summary>
        public Nullable<int> NUMSEDES
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public Nullable<DateTime> FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public Nullable<DateTime> FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor de clase
        /// </summary>
        public EntidadProvOferente()
        {
        }
    }
}
