using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TerceroDatoAdicional
    /// </summary>
    public class TerceroDatoAdicional : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTercero
        /// </summary>
        public int IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NivelInteres
        /// </summary>
        public String NivelInteres
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdClaseActividad
        /// </summary>
        public int IdClaseActividad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Observaciones
        /// </summary>
        public String Observaciones
        {
            get;
            set;
        }
        


        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        
        /// <summary>
        ///  Constructor de clase DatoAdicional
        /// </summary>
        public TerceroDatoAdicional()
        {
        }
    }
}
