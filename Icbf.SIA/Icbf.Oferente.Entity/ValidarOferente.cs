﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase ValidarOferente
    /// </summary>
    public class ValidarOferente : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public decimal IdTercero
        {
            get;
            set;
        }
        public int IdModulo
        {
            get;
            set;
        }
        public String ComponenteaValidar
        {
            get;
            set;
        }
        public String ComponenteValidado
        {
            get;
            set;
        }
        public int NumRevisionActual
        {
            get;
            set;
        }
        public ValidarOferente()
        {
        }
    }
}
