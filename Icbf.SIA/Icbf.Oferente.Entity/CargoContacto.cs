using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Clase CargoContacto
    /// </summary>
    public class CargoContacto : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdCargoContacto
        /// </summary>
        public int IdCargoContacto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoCargoContacto
        /// </summary>
        public String CodigoCargoContacto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public String Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Cosntructor de clase CargoContacto
        /// </summary>
        public CargoContacto()
        {
        }
    }
}
