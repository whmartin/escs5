using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase FormaVisualizarReporte
    /// </summary>
    public class FormaVisualizarReporte : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdFormaVisualizarReporte
        /// </summary>
        public int IdFormaVisualizarReporte
        {
            get;
            set;
        }
        /// <summary>
        /// Propeidad CodigoFormaVisualizarReporte
        /// </summary>
        public String CodigoFormaVisualizarReporte
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreFormaVisualizarReporte
        /// </summary>
        public String NombreFormaVisualizarReporte
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase FormaVisualizarReporte
        /// </summary>
        public FormaVisualizarReporte()
        {
        }
    }
}
