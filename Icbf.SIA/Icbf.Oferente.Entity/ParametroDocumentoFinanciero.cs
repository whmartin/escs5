using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase ParametroDocumentoFinanciero
    /// </summary>
    public class ParametroDocumentoFinanciero : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IDParametroDocFinanciero
        /// </summary>
        public int IDParametroDocFinanciero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdFormatoArchivo
        /// </summary>
        public int IdFormatoArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public bool Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DocumentoRup
        /// </summary>
        public bool DocumentoRup
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Obligatorio
        /// </summary>
        public Boolean Obligatorio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdFinanciero
        /// </summary>
        public int? IdFinanciero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdArchivo
        /// </summary>
        public int IdArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdFinancieroDocumento
        /// </summary>
        public int? IdFinancieroDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreArchivoOri
        /// </summary>
        public string NombreArchivoOri
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad NombreArchivo
        /// </summary>
        public string NombreArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Extension
        /// </summary>
        public string Extension
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase ParametroDocumentoFinanciero
        /// </summary>
        public ParametroDocumentoFinanciero()
        {
        }
    }
}
