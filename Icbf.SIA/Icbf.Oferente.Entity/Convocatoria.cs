﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class Convocatoria
    {
        public int IdConvocatoria { get; set; }
        public string Objeto { get; set; }
        public string Nombre { get; set; }
        public int Numero { get; set; }
        public int IdRegional { get; set; }
        public string DescripcionRegional { get; set; }
        public int IdDependenciaSolicitante { get; set; }
        public string Dependencia { get; set; }
        public int IdEmpleadoSolicitante { get; set; }
        public string EmpleadoSolicitante { get; set; }
        public int IdRegionalSolicitante { get; set; }
        public int IdCargoSolicitante { get; set; }
        public string CargoSolicitante { get; set; }
        public int IdEstado { get; set; }
        public string Estado { get; set; }
        public string CodigoEstado { get; set; }
        public object UsuarioCrea { get; set; }
        public DateTime? FechaPublicacionP { get; set; }
        public DateTime? FechaObservacionesInicioP { get; set; }
        public DateTime? FechaObservacionesFinP { get; set; }
        public DateTime? FechaRespuestasInicioP { get; set; }
        public DateTime? FechaRespuestasFinP { get; set; }
        public DateTime? FechaPublicacionD { get; set; }
        public DateTime? FechaObservacionesInicioD { get; set; }
        public DateTime? FechaObservacionesFinD { get; set; }
        public DateTime? FechaRespuestasInicioD { get; set; }
        public DateTime? FechaRespuestasFinD { get; set; }
        public DateTime? FechaInicioInscripcionOferente { get; set; }
        public DateTime? FechaFinInscripcionOferente { get; set; }
        public string NombreRegional { get; set; }
    }

    [Serializable]
    public class ConvotatoriaObservacion
    {
        public string Id { get; set; }
        public int IdTipoObservacion { get; set; }
        public string Observacion { get; set; }
        public string TipoObservacion { get; set; }
        public string NombreSolicitante { get; set; }
        public string CorreoObservacion { get; set; }
    }
}
