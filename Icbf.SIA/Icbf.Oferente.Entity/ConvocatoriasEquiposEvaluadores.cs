﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class ConvocatoriasEquiposEvaluadores : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdConvocatoriasEquiposEvaluadores { get; set; }
        public int IdConvocatoria { get; set; }
        public int IdEquipoEvaluador { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
    }
}
