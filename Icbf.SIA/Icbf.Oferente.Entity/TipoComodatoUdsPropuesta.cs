using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TipoComodatoUdsPropuesta
    /// </summary>
    public class TipoComodatoUdsPropuesta : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoComodatoUdsPropuesta
        /// </summary>
        public int IdTipoComodatoUdsPropuesta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoComodatoUdsPropuesta
        /// </summary>
        public String CodigoTipoComodatoUdsPropuesta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoComodatoUdsPropuesta
        /// </summary>
        public String NombreTipoComodatoUdsPropuesta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase TipoComodatoUdsPropuesta
        /// </summary>
        public TipoComodatoUdsPropuesta()
        {
        }
    }
}
