using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definici�n de clase NombreTipoDocumento
    /// </summary>
    public class TipoIdentificacionPersonaNatural : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoIdentificacionPersonaNatural
        /// </summary>
        public int IdTipoIdentificacionPersonaNatural
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoIdentificacionPersonaNatural
        /// </summary>
        public String CodigoTipoIdentificacionPersonaNatural
        {
            get;
            set;
        }
        /// <summary>
        /// Prop�edad NombreTipoIdentificacionPersonaNatural
        /// </summary>
        public String NombreTipoIdentificacionPersonaNatural
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor TipoIdentificacionPersonaNatural
        /// </summary>
        public TipoIdentificacionPersonaNatural()
        {
        }
    }
}
