﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class Evaluador : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public Evaluador() { }

        public int IdEvaluador { get; set; }
        public int IdUsuario { get; set; }
        public int IdEquipoEvaluador { get; set; }
        public int IdTipoObservacion { get; set; }
        public bool Activo { get; set; }
        public string NombreEvaluador { get; set; }
        public string DescTipoObservacion { get; set; }
        public string NumeroDocumento { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string NombreArea { get; set; }
    }
}
