﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase ExperienciaMunicipio
    /// </summary>
    public class ExperienciaMunicipio : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdMunicipio
        /// </summary>
        public int IdMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreMunicipio
        /// </summary>
        public String NombreMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase ExperienciaMunicipio
        /// </summary>
        public ExperienciaMunicipio()
        {
        }
    }
}