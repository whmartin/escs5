using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TendenciaUdsPropuesta
    /// </summary>
    public class TendenciaUdsPropuesta : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTendenciaUdsPropuesta
        /// </summary>
        public int IdTendenciaUdsPropuesta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTendenciaUdsPropuesta
        /// </summary>
        public String CodigoTendenciaUdsPropuesta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTendenciaUdsPropuesta
        /// </summary>
        public String NombreTendenciaUdsPropuesta
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// propiedd DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase TendenciaUdsPropuesta
        /// </summary>
        public TendenciaUdsPropuesta()
        {
        }
    }
}
